﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Text.RegularExpressions;

public partial class vendor_edit : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    //อับโลหดไฟล์ไปที่Te,p
    const string TempDirectory = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //const string TempDirectory2 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //const string TempDirectory3 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //const string TempDirectory4 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //const string TempDirectory5 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //const string TempDirectory6 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //อับโลหดไฟล์ไปที่Saver
    const string SaverDirectory = "UploadFile/VendorDoc/Lettercarrier/{0}/{2}/{1}/";
    const string SaverDirectory2 = "UploadFile/VendorDoc/Transportlicense/{0}/{2}/{1}/";
    const string SaverDirectory3 = "UploadFile/VendorDoc/TaxDocumentssection12/{0}/{2}/{1}/";
    const string SaverDirectory4 = "UploadFile/VendorDoc/Counterfoil/{0}/{2}/{1}/";
    const string SaverDirectory5 = "UploadFile/VendorDoc/Signature/{0}/{2}/{1}/";
    const string SaverDirectory6 = "UploadFile/VendorDoc/Other/{0}/{2}/{1}/";
    //อับโลหดไฟล์History
    const string HistoryDirectory = "UploadFile/VendorDoc/History/Lettercarrier/{0}/{2}/{1}/";
    const string HistoryDirectory2 = "UploadFile/VendorDoc/History/Transportlicense/{0}/{2}/{1}/";
    const string HistoryDirectory3 = "UploadFile/VendorDoc/History/TaxDocumentssection12/{0}/{2}/{1}/";
    const string HistoryDirectory4 = "UploadFile/VendorDoc/History/Counterfoil/{0}/{2}/{1}/";
    const string HistoryDirectory5 = "UploadFile/VendorDoc/History/Signature/{0}/{2}/{1}/";
    const string HistoryDirectory6 = "UploadFile/VendorDoc/History/Other/{0}/{2}/{1}/";

    //static List<ListGrid> listGrid = new List<ListGrid>();
    //static List<ListGridother> listGridother = new List<ListGridother>();
    //static List<ListGridDoc> listGriddoc = new List<ListGridDoc>();
    //static List<ListGridsigh> listGridsigh = new List<ListGridsigh>();
    //static List<DeleteItem> DeleteGrid = new List<DeleteItem>();
    //static List<DeleteFilegvwsigh> Filegvwsigh = new List<DeleteFilegvwsigh>();
    //static List<Deletegvwsigh> DeleteGridsigh = new List<Deletegvwsigh>();
    //static List<gvwsighListColumn> Listindex = new List<gvwsighListColumn>();

    private List<ListGrid> listGrid { get { return SystemFunction.ConvertObject<ListGrid>(Session["listGrid"]); } set { Session["listGrid"] = value; } }
    private List<ListGridother> listGridother { get { return SystemFunction.ConvertObject<ListGridother>(Session["listGridother"]); } set { Session["listGridother"] = value; } }
    private List<ListGridDoc> listGriddoc { get { return SystemFunction.ConvertObject<ListGridDoc>(Session["listGriddoc"]); } set { Session["listGriddoc"] = value; } }
    private List<ListGridsigh> listGridsigh { get { return SystemFunction.ConvertObject<ListGridsigh>(Session["listGridsigh"]); } set { Session["listGridsigh"] = value; } }
    private List<DeleteItem> DeleteGrid { get { return SystemFunction.ConvertObject<DeleteItem>(Session["DeleteGrid"]); } set { Session["DeleteGrid"] = value; } }
    private List<DeleteFilegvwsigh> Filegvwsigh { get { return SystemFunction.ConvertObject<DeleteFilegvwsigh>(Session["Filegvwsigh"]); } set { Session["Filegvwsigh"] = value; } }
    private List<Deletegvwsigh> DeleteGridsigh { get { return SystemFunction.ConvertObject<Deletegvwsigh>(Session["DeleteGridsigh"]); } set { Session["DeleteGridsigh"] = value; } }
    private List<gvwsighListColumn> Listindex { get { return SystemFunction.ConvertObject<gvwsighListColumn>(Session["Listindex"]); } set { Session["Listindex"] = value; } }
   
    private void ClearSessionStatic()
    {
        Session["listGrid"] = "";
        Session["listGridother"] = "";
        Session["listGriddoc"] = "";
        Session["listGridsigh"] = "";
        Session["DeleteGrid"] = "";
        Session["Filegvwsigh"] = "";
        Session["DeleteGridsigh"] = "";
        Session["Listindex"] = "";
    }

    private void NewLstStatic()
    {
        listGrid = new List<ListGrid>();
        listGridother = new List<ListGridother>();
        listGriddoc = new List<ListGridDoc>();
        listGridsigh = new List<ListGridsigh>();
        DeleteGrid = new List<DeleteItem>();
        Filegvwsigh = new List<DeleteFilegvwsigh>();
        DeleteGridsigh = new List<Deletegvwsigh>();
        Listindex = new List<gvwsighListColumn>();

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        gvwsign.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwsign_CustomColumnDisplayText);
        if (!IsPostBack)
        {
            ClearSessionStatic();
            NewLstStatic();

            #region เช็ค Permission

            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            #endregion

            string str = Request.QueryString["str"];
            string[] strQuery;
            string VenId = "";
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                Session["VendorID"] = strQuery[0];
                VenId = strQuery[0];
            }
            else
            {
                Session["VendorID"] = Session["Backpage"] + "";
                Session.Remove("Backpage");
            }
            Setdata();

            ListgvwDoc();
            Listgvwdoc4();
            Listgvwother();
            VisibleControlUpload();
            //Setdata();
            SetdataSigh();
            deleteFiletemp();

            #region checkว่าเคยเข้ามาดุข้อมูลไหม
            //ViewState["CheckIn"] = "1";
            string strCheckdata = @"SELECT SVENDORID, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS,DESCRIPTION
                                            FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VenId) + @"' AND CHECKIN ='1' ";


            DataTable dtchk = CommonFunction.Get_Data(conn, strCheckdata);
            if (dtchk.Rows.Count > 0)
            {

            }
            else
            {
                string strCompany = @"UPDATE TVendor
                                         SET CHECKIN=:cCheckIn
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VenId) + "'";
                using (OracleConnection con = new OracleConnection(conn))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    else
                    {

                    }
                    DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                    if (dt.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        //AddhistoryVendor(VenId);
                        using (OracleCommand com1 = new OracleCommand(strCompany, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":cCheckIn", OracleType.Char).Value = "1";
                            com1.ExecuteNonQuery();
                        }
                    }
                }
            }

            #endregion

            LogUser("46", "R", "แก้ไขข้อมูลผู้ขนส่ง", Session["VendorID"]+"");
        }
        else
        {

        }
        Listgvwdoc4();
        Listgvwother();
        ListgvwDoc();
        Listgvwsign();
    }

    void gvwsign_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "รูปภาพ")
        {
            if (e.Value == null)
            {

            }

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Setdata();

    }

    private bool IsValidEmail(string _email)
    {
        return Regex.IsMatch(_email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            string VendorID = Session["VendorID"] + "";
            string USER = Session["UserID"] + "";
            //string Line_no = "";
            string SDOCID = Session["SDOCID"] + "";
            string SDOCTYPE = Session["SDOCTYPE"] + "";
            string SDOCID2 = Session["SDOCID2"] + "";
            string SDOCTYPE2 = Session["SDOCTYPE2"] + "";
            string SDOCID3 = Session["SDOCID3"] + "";
            string SDOCTYPE3 = Session["SDOCTYPE3"] + "";
            string SDOCID4 = Session["SDOCID4"] + "";
            string SDOCTYPE4 = Session["SDOCTYPE4"] + "";



            switch (param[0])
            {



                case "viewHistory":
                    Session["Backpage"] = VendorID;
                    xcpn.JSProperties["cpRedirectTo"] = "document_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + VendorID));
                    break;

                case "cancel":

                    deleteFiletemp();
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_list.aspx";
                    break;



                #region Save

                case "Save":

                    if (CanWrite)
                    {

                        /// Validate Email
                        if (!string.IsNullOrEmpty(txtEmailVendor.Text))
                        {
                            txtEmailVendor.Text = txtEmailVendor.Text.Trim();
                            var lstEmail = txtEmailVendor.Text.Split(',').Select(p => p.Trim()).ToList();
                            foreach (var lst in lstEmail)
                            {
                                if (!IsValidEmail(lst))
                                {
                                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ข้อมูลของบริษัท -> E-mail ไม่ถูกต้อง!');");
                                    xcpn.JSProperties["cpSetjava"] = "2";
                                    return;
                                }
                            }
                        }

                        string cCheckIn = Session["CheckIn"] + "";
                        using (OracleConnection con = new OracleConnection(conn))
                        {
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }
                            #region Saveข้อมูลบริษัท

                            string condition = "";
                            if (!string.IsNullOrEmpty(txtSABBREVIATION.Text))
                            {
                                condition += " AND SABBREVIATION = '" + CommonFunction.ReplaceInjection(txtSABBREVIATION.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtSTEL.Text))
                            {
                                condition += " AND STEL = '" + CommonFunction.ReplaceInjection(txtSTEL.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtSFAX.Text))
                            {
                                condition += " AND SFAX = '" + CommonFunction.ReplaceInjection(txtSFAX.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(dedtDSTARTPTT.Text))
                            {
                                condition += " AND TRUNC(DSTARTPTT) =   TO_DATE('" + CommonFunction.ReplaceInjection(dedtDSTARTPTT.Date.Day + "/" + dedtDSTARTPTT.Date.Month + "/" + dedtDSTARTPTT.Date.Year) + @"','dd/mm/yyyy')";
                            }
                            if (!string.IsNullOrEmpty(dedtDBEGINTRANSPORT.Text))
                            {
                                condition += " AND TRUNC(DBEGINTRANSPORT) =   TO_DATE('" + CommonFunction.ReplaceInjection(dedtDBEGINTRANSPORT.Date.Day + "/" + dedtDBEGINTRANSPORT.Date.Month + "/" + dedtDBEGINTRANSPORT.Date.Year) + @" ','dd/mm/yyyy')";
                            }
                            if (!string.IsNullOrEmpty(dedtDEXPIRETRANSPORT.Text))
                            {
                                condition += " AND TRUNC(DEXPIRETRANSPORT) =  TO_DATE('" + CommonFunction.ReplaceInjection((dedtDEXPIRETRANSPORT.Date.Day + "/" + dedtDEXPIRETRANSPORT.Date.Month + "/" + dedtDEXPIRETRANSPORT.Date.Year)) + @"','dd/mm/yyyy')";
                            }
                            if (!string.IsNullOrEmpty(dedtDBEGIN13BIS.Text))
                            {
                                condition += " AND TRUNC(DBEGIN13BIS) =   TO_DATE('" + CommonFunction.ReplaceInjection((dedtDBEGIN13BIS.Date.Day + "/" + dedtDBEGIN13BIS.Date.Month + "/" + dedtDBEGIN13BIS.Date.Year)) + @" ','dd/mm/yyyy')";
                            }
                            if (!string.IsNullOrEmpty(dedtDEXPIRE13BIS.Text))
                            {
                                condition += " AND TRUNC(DEXPIRE13BIS) =  TO_DATE('" + CommonFunction.ReplaceInjection((dedtDEXPIRE13BIS.Date.Day + "/" + dedtDEXPIRE13BIS.Date.Month + "/" + dedtDEXPIRE13BIS.Date.Year)) + @"','dd/mm/yyyy')";
                            }

                            if (!string.IsNullOrEmpty(txtNCAPITAL.Text))
                            {
                                condition += " AND nCapital =  " + CommonFunction.ReplaceInjection(txtNCAPITAL.Text) + "";
                            }
                            else
                            {
                                condition += " AND nCapital = 0";
                            }
                            if (!string.IsNullOrEmpty(txtDescriptionSigh.Text))
                            {
                                condition += " AND DESCRIPTION ='" + CommonFunction.ReplaceInjection(txtDescriptionSigh.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtSTEL.Text))
                            {
                                condition += " AND STEL ='" + CommonFunction.ReplaceInjection(txtSTEL.Text) + "'";
                            }

                            if (!string.IsNullOrEmpty(txtSFAX.Text))
                            {
                                condition += " AND SFAX ='" + CommonFunction.ReplaceInjection(txtSFAX.Text) + "'";
                            }

                            //สถานะว่าเติมเต็มไหม
                            if (chkNotfill.Checked == true)
                            {
                                condition += " AND NOTFILL = '1'";
                            }
                            else
                            {
                                condition += " AND NOTFILL = '0'";
                            }

                            switch (rblStatus.Value + "")
                            {
                                case "0": condition += " AND CACTIVE ='0'";
                                    break;
                                case "1": condition += " AND CACTIVE ='1'";
                                    break;
                                case "2": condition += " AND CACTIVE ='2'";
                                    break;
                            }
                            if (!string.IsNullOrEmpty(txtConfirm.Text))
                            {
                                condition += " AND CAUSESAPCOMMIT ='" + CommonFunction.ReplaceInjection(txtConfirm.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtComment.Text))
                            {
                                condition += " AND CAUSESAP ='" + CommonFunction.ReplaceInjection(txtComment.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtstatus2.Text))
                            {
                                condition += " AND CAUSESAPCANCEL ='" + CommonFunction.ReplaceInjection(txtstatus2.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtEmailVendor.Text))
                            {
                                condition += " AND EMAIL ='" + txtEmailVendor.Text + "'";
                            }
                            string strCheckdata = @"SELECT SVENDORID, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS,DESCRIPTION,NOTFILL,STEL,SFAX,CACTIVE,CAUSESAPCOMMIT,CAUSESAP,CAUSESAPCANCEL
                                            FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                           " + condition + "";




                            string strCompany = @"UPDATE TVendor
                                         SET dStartPTT=:dStartPTT
                                            ,nCapital=:nCapital
                                            ,dBeginTransport=:dBeginTransport
                                            ,dExpireTransport=:dExpireTransport
                                            ,dBegin13bis=:dBegin13bis
                                            ,dExpire13bis=:dExpire13bis
                                            ,DESCRIPTION=:DESCRIPTION
                                            ,SUPDATE=:SUPDATE
                                            ,DUPDATE=:DUPDATE
                                            ,NOTFILL=:NOTFILL
                                            ,STEL=:STEL
                                            ,SFAX=:SFAX
                                            ,CACTIVE=:CACTIVE
                                            ,CAUSESAPCOMMIT=:CAUSESAPCOMMIT
                                            ,CAUSESAP=:CAUSESAP
                                            ,CAUSESAPCANCEL=:CAUSESAPCANCEL
                                            ,CCATEGORY = 'C'
                                            ,EMAIL = :EMAIL
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'";

                            DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                            if (dt.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                //LogUser("46", "S", "เปิดดูข้อมูลหน้า ข้อมูลผู้ขนส่ง", "");
                                LogUser("46", "E", "แก้ไขข้อมูลผู้ขนส่ง", VendorID);
                                AddhistoryVendor(VendorID);

                                decimal num = 0;
                                DateTime? Date;
                                using (OracleCommand com1 = new OracleCommand(strCompany, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":dStartPTT", OracleType.DateTime).Value = dedtDSTARTPTT.Value != null ? dedtDSTARTPTT.Value : DBNull.Value;
                                    com1.Parameters.Add(":nCapital", OracleType.Number).Value = decimal.TryParse(txtNCAPITAL.Text, out num) ? num : 0;
                                    com1.Parameters.Add(":dBeginTransport", OracleType.DateTime).Value = dedtDBEGINTRANSPORT.Value != null ? dedtDBEGINTRANSPORT.Value : DBNull.Value;
                                    com1.Parameters.Add(":dExpireTransport", OracleType.DateTime).Value = dedtDEXPIRETRANSPORT.Value != null ? dedtDEXPIRETRANSPORT.Value : DBNull.Value;
                                    com1.Parameters.Add(":dBegin13bis", OracleType.DateTime).Value = dedtDBEGIN13BIS.Value != null ? dedtDBEGIN13BIS.Value : DBNull.Value;
                                    com1.Parameters.Add(":dExpire13bis", OracleType.DateTime).Value = dedtDEXPIRE13BIS.Value != null ? dedtDEXPIRE13BIS.Value : DBNull.Value;
                                    com1.Parameters.Add(":DESCRIPTION", OracleType.VarChar).Value = txtDescriptionSigh.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DUPDATE", OracleType.DateTime).Value = DateTime.Now;
                                    com1.Parameters.Add(":NOTFILL", OracleType.Char).Value = chkNotfill.Checked == true ? "1" : "0";
                                    com1.Parameters.Add(":STEL", OracleType.VarChar).Value = txtSTEL.Text;
                                    com1.Parameters.Add(":SFAX", OracleType.VarChar).Value = txtSFAX.Text;
                                    com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = rblStatus.Value + "";
                                    com1.Parameters.Add(":CAUSESAPCOMMIT", OracleType.VarChar).Value = txtConfirm.Text;
                                    com1.Parameters.Add(":CAUSESAP", OracleType.VarChar).Value = txtComment.Text;
                                    com1.Parameters.Add(":CAUSESAPCANCEL", OracleType.VarChar).Value = txtstatus2.Text;
                                    com1.Parameters.Add(":EMAIL", OracleType.VarChar).Value = txtEmailVendor.Text;

                                    com1.ExecuteNonQuery();
                                }
                            }



                            #endregion

                            #region saveUpload


                            string InsUpload = @"INSERT INTO TVENDOR_DOC(SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE ,SFILENAME
                                                           , SDESCRIPTION, CACTIVE ,DCREATE ,SCREATE , DEXPIRE,SPATH ,SSYSFILENAME) 
                                                           VALUES (:SVENDORID,FC_GENID_TVENDOR_DOC(), :SDOCVERSION , :SDOCTYPE ,:SFILENAME
                                                           ,  :SDESCRIPTION, :CACTIVE, sysdate,:SCREATE ,:DEXPIRE,:SPATH,:SSYSFILENAME)";

                            string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "'";

                            #region ลบไฟล์ในUpload ทั้งหมดที่มีการลบ

                            if (DeleteGrid.Count > 0)
                            {
                                foreach (var item in DeleteGrid)
                                {
                                    AddhistoryVendorDoc(item.SVENDORID, item.SDOCID, item.SDOCTYPE);

                                    string UpdateUpload4 = @"UPDATE TVENDOR_DOC
                                                                SET CACTIVE=:CACTIVE
                                                                WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(item.SVENDORID) + @"' 
                                                                AND SDOCID ='" + CommonFunction.ReplaceInjection(item.SDOCID) + @"'
                                                                AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(item.SDOCTYPE) + "'";

                                    using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                        com1.ExecuteNonQuery();
                                    }

                                    switch (item.SDOCTYPE)
                                    {
                                        case "1":
                                            LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  แนบหนังสือรับรองผู้ขนส่ง", item.SVENDORID);
                                            string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile = item.SPATHALL.Split('/');
                                            int ncol = OldFile.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath, OldFile[ncol]);
                                            break;
                                        case "2":
                                            //สร้างญPath
                                            LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  ลบใบอนุญาตขนส่ง", item.SVENDORID);
                                            string HistoryPath2 = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile2 = item.SPATHALL.Split('/');
                                            int ncol2 = OldFile2.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath2, OldFile2[ncol2]);
                                            break;
                                        case "3":
                                            //สร้างญPath
                                            LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  ลบเอกสารภาษีอากรมาตรา12", item.SVENDORID);
                                            string HistoryPath3 = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile3 = item.SPATHALL.Split('/');
                                            int ncol3 = OldFile3.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath3, OldFile3[ncol3]);
                                            break;
                                        case "4":
                                            //สร้างญPath
                                            LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  ลบเอกสารใบประจำต่อ", item.SVENDORID);
                                            string HistoryPath4 = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploaderDoc4", Session["UserID"] + "");
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile4 = item.SPATHALL.Split('/');
                                            int ncol4 = OldFile4.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath4, OldFile4[ncol4]);
                                            break;
                                        case "5":
                                            //สร้างญPath
                                            LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง  ลบเอกสารอื่นๆ", item.SVENDORID);
                                            string HistoryPath6 = string.Format(HistoryDirectory6, Session["SVDID"] + "", "uploaderother", Session["UserID"] + "");
                                            //splitเอาชื่อไฟล์เก่า
                                            string[] OldFile6 = item.SPATHALL.Split('/');
                                            int ncol6 = OldFile6.Length - 1;
                                            UploadFile2History(item.SPATHALL, HistoryPath6, OldFile6[ncol6]);
                                            break;
                                    }


                                }

                            }
                            #endregion

                            #region Upload1
                            string CheckUpload1 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename.Text) + "'";


                            if (!string.IsNullOrEmpty(txtFileName.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload1);
                                //เช็คว่ามีไฟล์นี้ในระบบไหม
                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate.Text))
                                    {
                                        LogUser("46", "E", "แก้ไขข้อมูลผู้ขนส่ง แนบหนังสือรับรองผู้ขนส่ง", VendorID);
                                        // AddhistoryVendorDoc(VendorID, SDOCID, SDOCTYPE);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }

                                        ////สร้างญPath
                                        //string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                        ////splitเอาชื่อไฟล์เก่า
                                        //string[] OldFile = Up1.Text.Split('/');
                                        //int ncol = OldFile.Length - 1;
                                        //UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);

                                        //txtchkUpdate.Text = "";
                                    }
                                    else
                                    {
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง แนบหนังสือรับรองผู้ขนส่ง", VendorID);
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "1";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "หนังสือรับรองผู้ขนส่ง";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง ลบสือรับรองผู้ขนส่ง", VendorID);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                //    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                //    com1.ExecuteNonQuery();

                                //}
                                ////สร้างญPath
                                //string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up1.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                            }

                            #endregion

                            #region Upload2

                            string UpdateUpload2 = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                                ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE2) + "'";
                            string sdedtUpload2 = "";
                            if (!string.IsNullOrEmpty(dedtUpload2.Text))
                            {
                                //sdedtUpload2 = "TO_DATE( '" + dedtUpload2.Date.Day + "/" + dedtUpload2.Date.Month + "/" + dedtUpload2.Date.Year + "','DD/MM/YYYY')";
                                sdedtUpload2 = CommonFunction.ReplaceInjection(dedtUpload2.Date.Day + "") + "/" + CommonFunction.ReplaceInjection(dedtUpload2.Date.Month + "") + "/" + CommonFunction.ReplaceInjection(dedtUpload2.Date.Year + "");

                            }
                            else
                            {
                                sdedtUpload2 = "Null";
                            }
                            string CheckUpload2 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE2) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName2.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath2.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename2.Text) + @"'
                                            AND DEXPIRE = TO_DATE( '" + sdedtUpload2 + "','DD/MM/YYYY') ";

                            if (!string.IsNullOrEmpty(txtFileName2.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload2);

                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate2.Text))
                                    {
                                        LogUser("46", "E", "แก้ไขข้อมูลผู้ขนส่ง แนบใบอนุญาตขนส่ง", VendorID);
                                        AddhistoryVendorDoc(VendorID, SDOCID2, SDOCTYPE2);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload2.Value != null ? dedtUpload2.Value : DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }
                                        ////สร้างญPath
                                        //string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                        ////splitเอาชื่อไฟล์เก่า
                                        //string[] OldFile = Up2.Text.Split('/');
                                        //int ncol = OldFile.Length - 1;
                                        //UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                                    }
                                    else
                                    {
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง แนบใบอนุญาตขนส่ง", VendorID);
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "2";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "ใบอนุญาตขนส่ง";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload2.Value != null ? dedtUpload2.Value : DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง ลบใบอนุญาตขนส่ง", VendorID);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;
                                //    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                //    com1.ExecuteNonQuery();

                                //}
                                ////สร้างญPath
                                //string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up2.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                                //txtchkUpdate2.Text = "";
                            }
                            #endregion

                            #region Upload3
                            string UpdateUpload3 = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                                 ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE3) + "'";
                            string sdedtUpload3 = "";
                            if (!string.IsNullOrEmpty(dedtUpload3.Text))
                            {
                                //sdedtUpload2 = "TO_DATE( '" + dedtUpload2.Date.Day + "/" + dedtUpload2.Date.Month + "/" + dedtUpload2.Date.Year + "','DD/MM/YYYY')";
                                sdedtUpload3 = CommonFunction.ReplaceInjection(dedtUpload3.Date.Day + "") + "/" + CommonFunction.ReplaceInjection(dedtUpload3.Date.Month + "") + "/" + CommonFunction.ReplaceInjection(dedtUpload3.Date.Year + "");

                            }
                            else
                            {
                                sdedtUpload3 = "Null";
                            }
                            string CheckUpload3 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE3) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName3.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath3.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename3.Text) + @"'
                                            AND DEXPIRE = TO_DATE( '" + sdedtUpload3 + "','DD/MM/YYYY') ";
                            if (!string.IsNullOrEmpty(txtFileName3.Text))
                            {

                                DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload3);

                                if (dtUpload.Rows.Count > 0)
                                {


                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(txtchkUpdate3.Text))
                                    {
                                        LogUser("46", "E", "แก้ไขข้อมูลผู้ขนส่ง แนบเอกสารภาษีอากรมาตรา12", VendorID);
                                        AddhistoryVendorDoc(VendorID, SDOCID3, SDOCTYPE3);
                                        using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload3.Value != null ? dedtUpload3.Value : DBNull.Value;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.ExecuteNonQuery();

                                        }
                                        ////สร้างญPath
                                        //string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                        ////splitเอาชื่อไฟล์เก่า
                                        //string[] OldFile = Up3.Text.Split('/');
                                        //int ncol = OldFile.Length - 1;
                                        //UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                                    }
                                    else
                                    {
                                        //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                        LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง แนบเอกสารภาษีอากรมาตรา12", VendorID);
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "3";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารภาษีอากรมาตรา12";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload3.Value != null ? dedtUpload3.Value : DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //LogUser("46", "D", "แก้ไขข้อมูลผู้ขนส่ง ลบเอกสารภาษีอากรมาตรา12", VendorID);
                                //using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                //{
                                //    com1.Parameters.Clear();
                                //    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                //    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                //    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                //    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                //    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                //    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                //    com1.ExecuteNonQuery();

                                //}
                                ////สร้างญPath
                                //string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                ////splitเอาชื่อไฟล์เก่า
                                //string[] OldFile = Up3.Text.Split('/');
                                //int ncol = OldFile.Length - 1;
                                //UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                                //txtchkUpdate3.Text = "";
                            }
                            #endregion

                            #region Upload4

                            var chknewDoc = listGrid.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
                            if (chknewDoc.Count() > 0)
                            {

                                foreach (var item in chknewDoc)
                                {
                                    //int index = int.Parse(item.INDEX + "");
                                    LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง แนบเอกสารใบประจำต่อ", VendorID);
                                    //ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilename4") as ASPxTextBox;
                                    //ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFileName4") as ASPxTextBox;
                                    //ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtTruePath4") as ASPxTextBox;


                                    using (OracleConnection con2 = new OracleConnection(conn))
                                    {

                                        if (con2.State == ConnectionState.Closed)
                                        {
                                            con2.Open();
                                        }
                                        else
                                        {

                                        }
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con2))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "4";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = item.SFILENAME;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารใบประจำต่อ";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = item.STRUEPATH;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = item.SSYSFILENAME;

                                            com1.ExecuteNonQuery();
                                        }
                                        con2.Close();
                                    }


                                }
                                //เมื่อทำการเซฟข้อมูลแล้ว1ครั้งจะมีใส่ค่า S
                                //FileToServer();
                                //txtUploadchk4.Text = "S";
                                //listGrid.RemoveAll(o => 1 == 1);
                                //Setdata();
                                //Listgvwdoc4();
                            }
                            else
                            {
                                Listgvwdoc4();
                            }
                            #endregion

                            #region Uploadother

                            var chknewDocother = listGridother.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
                            if (chknewDocother.Count() > 0)
                            {

                                foreach (var item in chknewDocother)
                                {
                                    LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง  แนบเอกสารอื่นๆ", item.SVENDORID);
                                    //int index = int.Parse(item.INDEX + "");

                                    //ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilenameother") as ASPxTextBox;
                                    //ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFileNameother") as ASPxTextBox;
                                    //ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtTruePathother") as ASPxTextBox;


                                    using (OracleConnection con2 = new OracleConnection(conn))
                                    {

                                        if (con2.State == ConnectionState.Closed)
                                        {
                                            con2.Open();
                                        }
                                        else
                                        {

                                        }
                                        using (OracleCommand com1 = new OracleCommand(InsUpload, con2))
                                        {
                                            decimal num = 0;
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                            com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                            com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "5";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = item.SFILENAME;
                                            com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารอื่นๆ";
                                            com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = item.STRUEPATH;
                                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = item.SSYSFILENAME;

                                            com1.ExecuteNonQuery();
                                        }
                                        con2.Close();
                                    }


                                }
                                //เมื่อทำการเซฟข้อมูลแล้ว1ครั้งจะมีใส่ค่า S
                                //FileToServer();
                                //txtUploadchk4.Text = "S";
                                //listGrid.RemoveAll(o => 1 == 1);
                                //Setdata();
                                //Listgvwdoc4();
                            }
                            else
                            {
                                Listgvwother();
                            }
                            #endregion

                            #endregion



                            FileToServer();
                            con.Close();
                            VisibleControlUpload();

                            gvwDoc.DataBind();
                            txtUploadchk4.Text = "S";
                            txtUploadother.Text = "S";
                            Deletefilegvwsigh();
                            Deletedatagvwsigh();
                            listGrid = new List<ListGrid>();
                            listGriddoc = new List<ListGridDoc>();
                            Filegvwsigh = new List<DeleteFilegvwsigh>();
                            listGridsigh = new List<ListGridsigh>();
                            //Setdata();
                            //Listgvwdoc4();
                            //ListgvwDoc();
                        }
                        Session.Remove("CheckPermission");
                        Session.Remove("VendorID");
                        Session.Remove("SDOCID");
                        Session.Remove("SDOCTYPE");
                        Session.Remove("SDOCID2");
                        Session.Remove("SDOCTYPE2");
                        Session.Remove("SDOCID3");
                        Session.Remove("SDOCTYPE3");
                        Session.Remove("SDOCID4");
                        Session.Remove("SDOCTYPE4");
                        //Session.Remove("SVDID");

                        xcpn.JSProperties["cpRedirectTo"] = "vendor_list.aspx";
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    break;

                #endregion


                #region ลบไฟล์ที่อับโหลด

                case "deleteFile":
                    if (CanWrite)
                    {
                        //เก็บข้อมูลลง List ลบ
                        DeleteGrid.Add(new DeleteItem
                        {
                            SVENDORID = VendorID,
                            SDOCID = SDOCID,
                            SDOCTYPE = SDOCTYPE,
                            SPATHALL = txtTruePath.Text + txtSysfilename.Text
                        });

                        string cNo = param[2];
                        if (cNo == "1")
                        {
                            txtFileName.Text = "";
                            txtFilePath.Text = "";
                            txtTruePath.Text = "";
                            txtSysfilename.Text = "";
                        }


                        VisibleControlUpload();
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    break;

                case "deleteFile2":
                    if (CanWrite)
                    {
                        //เก็บข้อมูลลง List ลบ
                        DeleteGrid.Add(new DeleteItem
                        {
                            SVENDORID = VendorID,
                            SDOCID = SDOCID2,
                            SDOCTYPE = SDOCTYPE2,
                            SPATHALL = txtTruePath2.Text + txtSysfilename2.Text
                        });

                        string cNo2 = param[2];
                        if (cNo2 == "1")
                        {
                            txtFileName2.Text = "";
                            txtFilePath2.Text = "";
                            txtTruePath2.Text = "";
                            txtSysfilename2.Text = "";
                            dedtUpload2.Value = "";
                        }

                        txtValidation1.Text = ".";
                        txtValidation2.Text = ".";
                        VisibleControlUpload();
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;

                case "deleteFile3":
                    if (CanWrite)
                    {
                        //เก็บข้อมูลลง List ลบ
                        DeleteGrid.Add(new DeleteItem
                        {
                            SVENDORID = VendorID,
                            SDOCID = SDOCID3,
                            SDOCTYPE = SDOCTYPE3,
                            SPATHALL = txtTruePath3.Text + txtSysfilename3.Text
                        });

                        string cNo3 = param[2];
                        if (cNo3 == "1")
                        {
                            txtFileName3.Text = "";
                            txtFilePath3.Text = "";
                            txtTruePath3.Text = "";
                            txtSysfilename3.Text = "";
                            dedtUpload3.Value = "";
                        }
                        txtValidation3.Text = ".";
                        txtValidation4.Text = ".";
                        VisibleControlUpload();
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    break;

                case "deleteFilegvwdoc":
                    if (CanWrite)
                    {
                        dynamic delete = gvwDoc.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SSYSFILENAME", "SPATH", "SDOCTYPE");
                        //เก็บType เพื่อไปเคลียคอนโทร
                        string TypeDoc = delete[4] + "";
                        //เก็บข้อมูลลง List ลบ
                        DeleteGrid.Add(new DeleteItem
                        {
                            SVENDORID = delete[0] + "",
                            SDOCID = delete[1] + "",
                            SDOCTYPE = delete[4] + "",
                            SPATHALL = delete[3] + "" + delete[2] + ""
                        });



                        //int index = int.Parse(param[3]);

                        //นำไอดีของเอกสารมาลบออกจาList listGrid คือlistที่เป็นคอนโทรไฟล์อัพโหลด listGriddoc เป็นList ที่แสดงเอกสาร
                        listGrid.RemoveAll(d => d.SDOCID == delete[1] + "");
                        listGriddoc.RemoveAll(d => d.SDOCID == delete[1] + "");
                        listGridother.RemoveAll(d => d.SDOCID == delete[1] + "");
                        //ทำการ Bind ข้อมูล gvwDoc ใหม่
                        gvwDoc.DataSource = listGriddoc.ToList();
                        gvwDoc.DataBind();


                        //ทำการ Bind ข้อมูล gvwdoc4 ใหม่
                        if (listGrid.Count > 0)
                        {
                            gvwdoc4.DataSource = listGrid.ToList();
                            gvwdoc4.DataBind();
                        }
                        else
                        {
                            //ในกรณีที่ไม่มีจะสร้าง คอนโทรนเพื่อให้อัพโหลดข้อมูล
                            //listGriddoc.RemoveAll(o => 1 == 1);
                            gvwdoc4.DataSource = listGrid.ToList();
                            gvwdoc4.DataBind();
                            gvwdoc4.AddNewRow();
                        }
                        //ทำการ Bind ข้อมูล gvwother ใหม่
                        if (listGridother.Count > 0)
                        {
                            gvwother.DataSource = listGridother.ToList();
                            gvwother.DataBind();
                        }
                        else
                        {
                            //ในกรณีที่ไม่มีจะสร้าง คอนโทรนเพื่อให้อัพโหลดข้อมูล
                            //listGriddoc.RemoveAll(o => 1 == 1);
                            gvwother.DataSource = listGridother.ToList();
                            gvwother.DataBind();
                            gvwother.AddNewRow();
                        }
                        //if (listGriddoc.Count > 0)
                        //{

                        //}
                        //else
                        //{
                        //    //listGriddoc.RemoveAll(o => 1 == 1);
                        //    gvwDoc.Visible = false;
                        //}


                        switch (TypeDoc)
                        {
                            case "1":
                                txtFileName.Text = "";
                                txtFilePath.Text = "";
                                txtTruePath.Text = "";
                                txtSysfilename.Text = "";
                                break;
                            case "2":
                                txtFileName2.Text = "";
                                txtFilePath2.Text = "";
                                txtTruePath2.Text = "";
                                txtSysfilename2.Text = "";
                                dedtUpload2.Value = "";
                                break;
                            case "3":
                                txtFileName3.Text = "";
                                txtFilePath3.Text = "";
                                txtTruePath3.Text = "";
                                txtSysfilename3.Text = "";
                                dedtUpload3.Value = "";
                                break;

                        }
                        VisibleControlUpload();
                        //แก้ที่จรงนี้นะๆๆๆๆ บัคตอนลิสข้อมูล

                        //ListgvwDoc();
                        //Listgvwdoc4();
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                        xcpn.JSProperties["cpSetjava"] = "2";
                    }
                    break;



                #endregion

            }
        }
    }

    void SetdataSigh()
    {
        listGridsigh = new List<ListGridsigh>();
        string SVENDORID = Session["VendorID"] + "";
        //เก็บข้อมูลเข้าListgvwsigh
        if (!string.IsNullOrEmpty(SVENDORID))
        {
            string strsqlgvwsigh = @"SELECT 
  SVENDORID, LINE_NO, SNAME, 
   SPOSITION, PHONE, PHONE2, 
   EMAIL, PICTURE, IS_ACTIVE, 
   DATE_CREATED, USER_CREATED, DATE_UPDATED, 
   USER_UPDATED, SFILENAME, SPATH,SSYSFILENAME
FROM TVENDOR_SIGNER WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND IS_ACTIVE='1'  ORDER BY LINE_NO DESC";
            DataTable dtsigh = CommonFunction.Get_Data(conn, strsqlgvwsigh);

            if (dtsigh.Rows.Count > 0)
            {
                for (int i = 0; i <= dtsigh.Rows.Count - 1; i++)
                {
                    DateTime? DATE_CREATED = null;
                    DateTime? DATE_UPDATED = null;
                    if (!string.IsNullOrEmpty(dtsigh.Rows[i]["DATE_CREATED"] + ""))
                    {
                        DATE_CREATED = DateTime.Parse(dtsigh.Rows[i]["DATE_CREATED"] + "");
                    }
                    if (!string.IsNullOrEmpty(dtsigh.Rows[i]["DATE_UPDATED"] + ""))
                    {
                        DATE_UPDATED = DateTime.Parse(dtsigh.Rows[i]["DATE_UPDATED"] + "");
                    }


                    listGridsigh.Add(new ListGridsigh
                    {
                        SVENDORID = dtsigh.Rows[i]["SVENDORID"] + "",
                        LINE_NO = dtsigh.Rows[i]["LINE_NO"] + "",
                        SNAME = dtsigh.Rows[i]["SNAME"] + "",
                        SPOSITION = dtsigh.Rows[i]["SPOSITION"] + "",
                        PHONE = dtsigh.Rows[i]["PHONE"] + "",
                        PHONE2 = dtsigh.Rows[i]["PHONE2"] + "",
                        EMAIL = dtsigh.Rows[i]["EMAIL"] + "",
                        PICTURE = dtsigh.Rows[i]["PICTURE"] + "",
                        IS_ACTIVE = dtsigh.Rows[i]["IS_ACTIVE"] + "",
                        DATE_CREATED = DATE_CREATED,
                        USER_CREATED = dtsigh.Rows[i]["USER_CREATED"] + "",
                        DATE_UPDATED = DATE_UPDATED,
                        USER_UPDATED = dtsigh.Rows[i]["USER_UPDATED"] + "",
                        SFILENAME = dtsigh.Rows[i]["SFILENAME"] + "",
                        SPATH = dtsigh.Rows[i]["SPATH"] + "",
                        SSYSFILENAME = dtsigh.Rows[i]["SSYSFILENAME"] + ""
                    });


                }
                //gvwsign.DataSource = listGridsigh;
                //gvwsign.DataBind();
            }




        }
    }

    void Setdata()
    {

        string SVENDORID = Session["VendorID"] + "";
        string USER = Session["UserID"] + "";
        #region set รายละเอียดบริษัท

        string strsql = @"SELECT vensap.DUPDATE, ven.SVENDORID,ven.SABBREVIATION,vensap.SNO , vensap.SDISTRICT, vensap.SREGION , vensap.SPROVINCE
, vensap.SPROVINCECODE , ven.STEL , ven.SFAX,ven.DSTARTPTT,ven.NCAPITAL,ven.DBEGINTRANSPORT,ven.DEXPIRETRANSPORT,ven.DBEGIN13BIS,ven.DEXPIRE13BIS,ven.DESCRIPTION,ven.NOTFILL
,ven.CACTIVE,ven.CAUSESAPCOMMIT,ven.CAUSESAPCANCEL,ven.CAUSESAP,ven.EMAIL
FROM TVendor ven
LEFT  JOIN TVENDOR_SAP vensap
ON ven.SVENDORID = vensap.SVENDORID
WHERE ven.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";

        DateTime? DSTARTPTT = null;
        DateTime? DBEGINTRANSPORT = null;
        DateTime? DEXPIRETRANSPORT = null;
        DateTime? DBEGIN13BIS = null;
        DateTime? DEXPIRE13BIS = null;

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        if (dt.Rows.Count > 0)
        {
            txtSVENDORID.Text = dt.Rows[0]["SVENDORID"] + "";
             txtSABBREVIATION.Text = dt.Rows[0]["SABBREVIATION"] + "";
            txtSNO.Text = dt.Rows[0]["SNO"] + "";
            txtSDISTRICT.Text = dt.Rows[0]["SDISTRICT"] + "";
            txtSREGION.Text = dt.Rows[0]["SREGION"] + "";
            txtSPROVINCE.Text = dt.Rows[0]["SPROVINCE"] + "";
            txtSPROVINCECODE.Text = dt.Rows[0]["SPROVINCECODE"] + "";
            txtSTEL.Text = dt.Rows[0]["STEL"] + "";
            txtSFAX.Text = dt.Rows[0]["SFAX"] + "";
            txtEmailVendor.Text = dt.Rows[0]["EMAIL"] + "";
            if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
            {
                DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
            {
                DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
            {
                DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
            {
                DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
            {
                DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
            }
            if (dt.Rows[0]["NOTFILL"] + "" == "1")
            {
                chkNotfill.Checked = true;
            }
            else
            {
                chkNotfill.Checked = false;
            }

            dedtDSTARTPTT.Value = DSTARTPTT;
            txtNCAPITAL.Text = dt.Rows[0]["NCAPITAL"] + "";
            dedtDBEGINTRANSPORT.Value = DBEGINTRANSPORT;
            dedtDEXPIRETRANSPORT.Value = DEXPIRETRANSPORT;
            dedtDBEGIN13BIS.Value = DBEGIN13BIS;
            dedtDEXPIRE13BIS.Value = DEXPIRE13BIS;
            txtDescriptionSigh.Text = dt.Rows[0]["DESCRIPTION"] + "";


            switch (dt.Rows[0]["CACTIVE"] + "")
            {
                case "0": rblStatus.SelectedIndex = 1;
                    //Typedefault.Visible = true;
                    //status.Style. = false;
                    //status2.Visible = false;
                    break;
                case "1": rblStatus.SelectedIndex = 0;
                    //Typedefault.Visible = false;
                    //status.Visible = true;
                    //status2.Visible = false;
                    break;
                case "2": rblStatus.SelectedIndex = 2;
                    //Typedefault.Visible = false;
                    //status.Visible = false;
                    //status2.Visible = true;
                    break;
                default: rblStatus.SelectedIndex = 0;
                    //Typedefault.Visible = true;
                    //status.Visible = false;
                    //status2.Visible = false;
                    break;
            }

            txtConfirm.Text = dt.Rows[0]["CAUSESAPCOMMIT"] + "";
            txtstatus2.Text = dt.Rows[0]["CAUSESAPCANCEL"] + "";
            txtComment.Text = dt.Rows[0]["CAUSESAP"] + "";

        }
        #endregion

        #region รายละเอียดUpload



        //string VendorID = Session["VendorID"] + "";
        if (!string.IsNullOrEmpty(SVENDORID))
        {
            string strUpload = @"SELECT 
    SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL  ORDER BY SDOCTYPE ASC";
            DataTable dt2 = CommonFunction.Get_Data(conn, strUpload);


            DateTime? DEXPIRE2 = null;
            DateTime? DEXPIRE3 = null;

            //if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
            //{
            //    DEXPIRE3 = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
            //}


            #region setUpload1
            DataRow[] dtrow = dt2.Select("SDOCTYPE = '1'");
            if (dtrow.Count() > 0)
            {
                txtFileName.Text = dtrow[0][4] + "";
                txtFilePath.Text = dtrow[0][13] + "";
                txtTruePath.Text = dtrow[0][13] + "";
                txtSysfilename.Text = dtrow[0][5] + "";
                Up1.Text = dtrow[0][13] + "" + dtrow[0][5] + "";
                txtchkUpdate.Text = "1";
                Session["SDOCID"] = dtrow[0][1] + "";
                Session["SDOCTYPE"] = dtrow[0][3] + "";
            }
            else
            {
                txtFileName.Text = "";
                txtFilePath.Text = "";
                txtTruePath.Text = "";
                txtSysfilename.Text = "";
                Up1.Text = "";
                Session["SDOCID"] = "";
                Session["SDOCTYPE"] = "";
                txtchkUpdate.Text = "";
            }
            #endregion

            #region setupload2
            DataRow[] dtrow2 = dt2.Select("SDOCTYPE = '2'");
            if (dtrow2.Count() > 0)
            {

                if (!string.IsNullOrEmpty(dtrow2[0][12] + ""))
                {
                    DEXPIRE2 = DateTime.Parse(dtrow2[0][12] + "");

                }

                if (DEXPIRE2 <= DateTime.Now.Date)
                {
                    AddhistoryVendorDoc(dtrow2[0][0] + "", dtrow2[0][1] + "", dtrow2[0][3] + "");
                    LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง  ใบอนุญาตขนส่งหมดอายุ", dtrow2[0][0] + "");

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }
                        string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(dtrow2[0][0] + "") + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(dtrow2[0][1] + "") + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(dtrow2[0][3] + "") + "'";

                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "0";
                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า

                        UploadFile2History(dtrow2[0][13] + "" + dtrow2[0][5] + "", HistoryPath, dtrow2[0][5] + "");
                    }
                }
                else
                {
                    txtFileName2.Text = dtrow2[0][4] + "";
                    txtFilePath2.Text = dtrow2[0][13] + "";
                    txtTruePath2.Text = dtrow2[0][13] + "";
                    txtSysfilename2.Text = dtrow2[0][5] + "";
                    Up2.Text = dtrow2[0][13] + "" + dtrow2[0][5] + "";
                    txtchkUpdate2.Text = "1";
                    Session["SDOCID2"] = dtrow2[0][1] + "";
                    Session["SDOCTYPE2"] = dtrow2[0][3] + "";
                    dedtUpload2.Value = DEXPIRE2;

                    if (!string.IsNullOrEmpty(txtSysfilename2.Text))
                    {
                        if (!string.IsNullOrEmpty(dedtUpload2.Text))
                        {
                            txtValidation1.Text = ".";
                            txtValidation2.Text = ".";
                            txtValidation1.ClientVisible = false;
                            txtValidation2.ClientVisible = false;
                        }
                        else
                        {
                            txtValidation1.Text = ".";
                            txtValidation2.Text = "";
                            txtValidation1.ClientVisible = false;
                            txtValidation2.ClientVisible = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dedtUpload2.Text))
                        {
                            txtValidation1.Text = "";
                            txtValidation2.Text = ".";
                            txtValidation1.ClientVisible = true;
                            txtValidation2.ClientVisible = false;
                        }
                        else
                        {
                            txtValidation1.Text = ".";
                            txtValidation2.Text = ".";
                            txtValidation1.ClientVisible = false;
                            txtValidation2.ClientVisible = false;
                        }
                    }



                }
            }
            else
            {
                txtFileName2.Text = "";
                txtFilePath2.Text = "";
                txtTruePath2.Text = "";
                txtSysfilename2.Text = "";
                Up2.Text = "";
                Session["SDOCID2"] = "";
                Session["SDOCTYPE2"] = "";
                dedtUpload2.Value = "";
                txtchkUpdate2.Text = "";

                if (!string.IsNullOrEmpty(txtSysfilename2.Text))
                {
                    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                    {
                        txtValidation1.Text = ".";
                        txtValidation2.Text = ".";
                        txtValidation1.ClientVisible = false;
                        txtValidation2.ClientVisible = false;
                    }
                    else
                    {
                        txtValidation1.Text = ".";
                        txtValidation2.Text = "";
                        txtValidation1.ClientVisible = false;
                        txtValidation2.ClientVisible = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                    {
                        txtValidation1.Text = "";
                        txtValidation2.Text = ".";
                        txtValidation1.ClientVisible = true;
                        txtValidation2.ClientVisible = false;
                    }
                    else
                    {
                        txtValidation1.Text = ".";
                        txtValidation2.Text = ".";
                        txtValidation1.ClientVisible = false;
                        txtValidation2.ClientVisible = false;
                    }
                }


            }
            #endregion

            #region setupload3
            DataRow[] dtrow3 = dt2.Select("SDOCTYPE = '3'");
            if (dtrow3.Count() > 0)
            {
                if (!string.IsNullOrEmpty(dtrow3[0][12] + ""))
                {
                    DEXPIRE3 = DateTime.Parse(dtrow3[0][12] + "");

                }

                //if (DEXPIRE3 <= DateTime.Now.Date)
                //เซ้ตเป็น False เพราะ ไม่มีวันหมดอายุเอกสารมาตรา12แล้ว
                if (false)
                {
                    AddhistoryVendorDoc(dtrow3[0][0] + "", dtrow3[0][1] + "", dtrow3[0][3] + "");
                    LogUser("46", "I", "แก้ไขข้อมูลผู้ขนส่ง  เอกสารภาษีอากรมาตรา12หมดอายุ", dtrow3[0][0] + "");

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }
                        string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(dtrow3[0][0] + "") + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(dtrow3[0][1] + "") + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(dtrow3[0][3] + "") + "'";

                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "0";

                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า

                        UploadFile2History(dtrow3[0][13] + "" + dtrow3[0][5] + "", HistoryPath, dtrow3[0][5] + "");
                    }
                }
                else
                {
                    txtFileName3.Text = dtrow3[0][4] + "";
                    txtFilePath3.Text = dtrow3[0][13] + "";
                    txtTruePath3.Text = dtrow3[0][13] + "";
                    txtSysfilename3.Text = dtrow3[0][5] + "";
                    Up3.Text = dtrow3[0][13] + "" + dtrow3[0][5] + "";
                    txtchkUpdate3.Text = "1";
                    Session["SDOCID3"] = dtrow3[0][1] + "";
                    Session["SDOCTYPE3"] = dtrow3[0][3] + "";
                    dedtUpload3.Value = DEXPIRE3;
                    //ใช้ค่าลงtext เพื่อเช็ค validation
                    if (!string.IsNullOrEmpty(txtSysfilename3.Text))
                    {
                        if (!string.IsNullOrEmpty(dedtUpload3.Text))
                        {
                            txtValidation3.Text = ".";
                            txtValidation4.Text = ".";
                            txtValidation3.ClientVisible = false;
                            txtValidation4.ClientVisible = false;
                        }
                        else
                        {
                            txtValidation3.Text = ".";
                            txtValidation4.Text = "";
                            txtValidation3.ClientVisible = false;
                            txtValidation4.ClientVisible = true;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dedtUpload3.Text))
                        {
                            txtValidation3.Text = "";
                            txtValidation4.Text = ".";
                            txtValidation3.ClientVisible = true;
                            txtValidation4.ClientVisible = false;
                        }
                        else
                        {
                            txtValidation3.Text = ".";
                            txtValidation4.Text = ".";
                            txtValidation3.ClientVisible = false;
                            txtValidation4.ClientVisible = false;
                        }
                    }
                }
            }
            else
            {
                txtFileName3.Text = "";
                txtFilePath3.Text = "";
                txtTruePath3.Text = "";
                txtSysfilename3.Text = "";
                Up3.Text = "";
                Session["SDOCID3"] = "";
                Session["SDOCTYPE3"] = "";
                dedtUpload3.Value = "";
                txtchkUpdate3.Text = "";
                //ใช้ค่าลง text เพื่อเช็ค validation
                if (!string.IsNullOrEmpty(txtSysfilename3.Text))
                {
                    if (!string.IsNullOrEmpty(dedtUpload3.Text))
                    {
                        txtValidation3.Text = ".";
                        txtValidation4.Text = ".";
                        txtValidation3.ClientVisible = false;
                        txtValidation4.ClientVisible = false;
                    }
                    else
                    {
                        txtValidation3.Text = ".";
                        txtValidation4.Text = "";
                        txtValidation3.ClientVisible = false;
                        txtValidation4.ClientVisible = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(dedtUpload3.Text))
                    {
                        txtValidation3.Text = "";
                        txtValidation4.Text = ".";
                        txtValidation3.ClientVisible = true;
                        txtValidation4.ClientVisible = false;
                    }
                    else
                    {
                        txtValidation3.Text = ".";
                        txtValidation4.Text = ".";
                        txtValidation3.ClientVisible = false;
                        txtValidation4.ClientVisible = false;
                    }
                }
            }
            #endregion

        }
        #endregion

        //เก็บข้อมูลจากเบสลง List listgrid
        string sql = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                          SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH 
                          FROM TVENDOR_DOC 
                          WHERE CACTIVE ='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL AND SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND SDOCTYPE ='4'";
        DataTable dtDoc4 = CommonFunction.Get_Data(conn, sql);
        //เก็บข้อมูลเข้า list เพื่อสแดงใน Upload4
        if (dtDoc4.Rows.Count > 0)
        {
            DateTime? DCREATE = null;
            DateTime? DUPDATE = null;
            DateTime? DEXPIRE = null;

            int i;
            for (i = 0; i <= dtDoc4.Rows.Count - 1; i++)
            {

                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DCREATE"] + ""))
                {
                    DCREATE = DateTime.Parse(dtDoc4.Rows[i]["DCREATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DUPDATE"] + ""))
                {
                    DUPDATE = DateTime.Parse(dtDoc4.Rows[i]["DUPDATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DEXPIRE"] + ""))
                {
                    DEXPIRE = DateTime.Parse(dtDoc4.Rows[i]["DEXPIRE"] + "");
                }


                listGrid.Add(new ListGrid
                {
                    SVENDORID = dtDoc4.Rows[i]["SVENDORID"] + "",
                    SDOCID = dtDoc4.Rows[i]["SDOCID"] + "",
                    SDOCVERSION = int.Parse(dtDoc4.Rows[i]["SDOCVERSION"] + ""),
                    SDOCTYPE = dtDoc4.Rows[i]["SDOCTYPE"] + "",
                    SFILENAME = dtDoc4.Rows[i]["SFILENAME"] + "",
                    SSYSFILENAME = dtDoc4.Rows[i]["SSYSFILENAME"] + "",
                    SDESCRIPTION = dtDoc4.Rows[i]["SDESCRIPTION"] + "",
                    CACTIVE = dtDoc4.Rows[i]["CACTIVE"] + "",
                    DCREATE = DCREATE,
                    SCREATE = dtDoc4.Rows[i]["SCREATE"] + "",
                    DUPDATE = DUPDATE,
                    SUPDATE = dtDoc4.Rows[i]["SUPDATE"] + "",
                    SPATH = dtDoc4.Rows[i]["SPATH"] + ""
                });
            }
        }
        txtUploadchk4.Text = "";

        //เก็บข้อมูลเข้า list เพื่อสแดงใน Uploadother

        string sqlother = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                          SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH 
                          FROM TVENDOR_DOC 
                          WHERE CACTIVE ='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL AND SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND SDOCTYPE ='5'";
        DataTable dtDocother = CommonFunction.Get_Data(conn, sqlother);
        if (dtDocother.Rows.Count > 0)
        {
            DateTime? DCREATE = null;
            DateTime? DUPDATE = null;
            DateTime? DEXPIRE = null;

            int i;
            for (i = 0; i <= dtDocother.Rows.Count - 1; i++)
            {

                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DCREATE"] + ""))
                {
                    DCREATE = DateTime.Parse(dtDocother.Rows[i]["DCREATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DUPDATE"] + ""))
                {
                    DUPDATE = DateTime.Parse(dtDocother.Rows[i]["DUPDATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DEXPIRE"] + ""))
                {
                    DEXPIRE = DateTime.Parse(dtDocother.Rows[i]["DEXPIRE"] + "");
                }


                listGridother.Add(new ListGridother
                {
                    SVENDORID = dtDocother.Rows[i]["SVENDORID"] + "",
                    SDOCID = dtDocother.Rows[i]["SDOCID"] + "",
                    SDOCVERSION = int.Parse(dtDocother.Rows[i]["SDOCVERSION"] + ""),
                    SDOCTYPE = dtDocother.Rows[i]["SDOCTYPE"] + "",
                    SFILENAME = dtDocother.Rows[i]["SFILENAME"] + "",
                    SSYSFILENAME = dtDocother.Rows[i]["SSYSFILENAME"] + "",
                    SDESCRIPTION = dtDocother.Rows[i]["SDESCRIPTION"] + "",
                    CACTIVE = dtDocother.Rows[i]["CACTIVE"] + "",
                    DCREATE = DCREATE,
                    SCREATE = dtDocother.Rows[i]["SCREATE"] + "",
                    DUPDATE = DUPDATE,
                    SUPDATE = dtDocother.Rows[i]["SUPDATE"] + "",
                    SPATH = dtDocother.Rows[i]["SPATH"] + ""
                });
            }
        }
        txtUploadother.Text = "";


        //เก็บข้อมูลเข้า list เพื่อสแดงใน gvwDoc
        if (!string.IsNullOrEmpty(SVENDORID))
        {
            string strsqldoc = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                 SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH
                                 FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + @"' 
                                 AND CACTIVE = '1' AND SSYSFILENAME IS NOT NULL ORDER BY SDOCTYPE ASC";
            DataTable dtDoc = CommonFunction.Get_Data(conn, strsqldoc);
            //var dblist = listGrid;

            if (dtDoc.Rows.Count > 0)
            {


                int i;
                for (i = 0; i < dtDoc.Rows.Count; i++)
                {
                    DateTime? DCREATE = null;
                    DateTime? DUPDATE = null;
                    DateTime? DEXPIRE = null;
                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DCREATE"] + ""))
                    {
                        DCREATE = DateTime.Parse(dtDoc.Rows[i]["DCREATE"] + "");
                    }
                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DUPDATE"] + ""))
                    {
                        DUPDATE = DateTime.Parse(dtDoc.Rows[i]["DUPDATE"] + "");
                    }
                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DEXPIRE"] + ""))
                    {
                        DEXPIRE = DateTime.Parse(dtDoc.Rows[i]["DEXPIRE"] + "");
                    }


                    listGriddoc.Add(new ListGridDoc
                    {
                        SVENDORID = dtDoc.Rows[i]["SVENDORID"] + "",
                        SDOCID = dtDoc.Rows[i]["SDOCID"] + "",
                        SDOCVERSION = int.Parse(dtDoc.Rows[i]["SDOCVERSION"] + ""),
                        SDOCTYPE = dtDoc.Rows[i]["SDOCTYPE"] + "",
                        SFILENAME = dtDoc.Rows[i]["SFILENAME"] + "",
                        SSYSFILENAME = dtDoc.Rows[i]["SSYSFILENAME"] + "",
                        SDESCRIPTION = dtDoc.Rows[i]["SDESCRIPTION"] + "",
                        CACTIVE = dtDoc.Rows[i]["CACTIVE"] + "",
                        DCREATE = DCREATE,
                        SCREATE = dtDoc.Rows[i]["SCREATE"] + "",
                        DUPDATE = DUPDATE,
                        SUPDATE = dtDoc.Rows[i]["SUPDATE"] + "",
                        SPATH = dtDoc.Rows[i]["SPATH"] + "",
                        DEXPIRE = DEXPIRE
                    });
                }
            }
        }



    }

    #region อัพโหลดแนบหนังสือรับรองผู้ขนส่ง
    protected void upload_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }


    #endregion

    #region อัพโหลดใบอนุญาตขนส่ง

    protected void upload2_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        //dedtUpload2.ValidationSettings.RequiredField.IsRequired = true;
        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
            //dedtUpload2.ValidationSettings.RequiredField.IsRequired = true;
            //dedtUpload2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
            //dedtUpload2.ValidationSettings.Display = Display.Dynamic;
            //dedtUpload2.ValidationSettings.ErrorText = "555555";
        }
        else
        {

            return;

        }
    }

    #endregion

    #region อัพโหลดเอกสารภาษีอากรมาตรา12

    protected void upload3_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
            //dedtUpload3.ValidationSettings.RequiredField.IsRequired = true;
        }
        else
        {

            return;

        }
    }

    #endregion

    #region อัพโหลดเอกสารใบประจำต่อ
    protected void gvwdoc4_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePath4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePath4") as ASPxTextBox;
            ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileName4") as ASPxTextBox;
            ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtTruePath4") as ASPxTextBox;
            ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtSysfilename4") as ASPxTextBox;
            ASPxLabel lblData2 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwdoc4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwdoc4") as ASPxButton;
            ASPxButton btnDelFilegvwdoc4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwdoc4") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePath4.Text))
            {
                btnViewgvwdoc4.ClientVisible = true;
                btnDelFilegvwdoc4.ClientVisible = true;
                btnViewgvwdoc4.ClientEnabled = true;
                btnDelFilegvwdoc4.ClientEnabled = true;

            }
            else
            {
                btnViewgvwdoc4.ClientVisible = true;
                btnDelFilegvwdoc4.ClientVisible = true;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileName4.ClientInstanceName = txtFileName4.ClientInstanceName + "_" + VisibleIndex;
            txtFilePath4.ClientInstanceName = txtFilePath4.ClientInstanceName + "_" + VisibleIndex;
            txtTruePath4.ClientInstanceName = txtTruePath4.ClientInstanceName + "_" + VisibleIndex;
            txtSysfilename4.ClientInstanceName = txtSysfilename4.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwdoc4.ClientInstanceName = btnViewgvwdoc4.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwdoc4.ClientInstanceName = btnDelFilegvwdoc4.ClientInstanceName + "_" + VisibleIndex;

            // Add Event
            btnViewgvwdoc4.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePath4.ClientInstanceName + ".GetValue()+" + txtSysfilename4.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwdoc4.ClientSideEvents.Click = "function (s, e) { gvwdoc4.PerformCallback(\"deleteUpload4; " + txtFilePath4.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + ";" + txtFileName4.ClientInstanceName.ToString() + ";" + txtFilePath4.ClientInstanceName.ToString() + " ;" + btnViewgvwdoc4.ClientInstanceName.ToString() + " ; " + btnDelFilegvwdoc4.ClientInstanceName.ToString() + "\");}";
            //btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void uploaderDoc4_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string VendorID = Session["VendorID"] + "";
        string USER = Session["UserID"] + "";
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        int ncol = _Filename.Length - 1;

        //if (_Filename[ncol] == "xlsx" || _Filename[ncol] == "xls" || _Filename[ncol] == "doc" || _Filename[ncol] == "docx" || _Filename[ncol] == "pdf" || _Filename[ncol] == "jpg" || _Filename[ncol] == "jpeg")
        //{

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGrid.Add(new ListGrid
                {
                    SFILENAME = e.UploadedFile.FileName,
                    SSYSFILENAME = genName + "." + _Filename[count],
                    SPATH = data,
                    STRUEPATH = data2,
                    SVENDORID = VendorID,
                    NEWPICTRUEFLAG = "1",
                    SDOCTYPE = "4",
                    INDEX = listGrid.Count
                });
            }
        }
        else
        {

            return;

        }
        //}
        //else
        //{
        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอับเอกสารประเภทนี้ได้');");
        //}
    }



    protected void gvwdoc4_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxUploadControl Upload = gvwdoc4.FindEditRowCellTemplateControl(null, "uploaderDoc4") as ASPxUploadControl;
        Upload.ClientVisible = true;
        e.Cancel = true;
        //Listgvwdoc4();
        gvwdoc4.CancelEdit();

    }
    #endregion

    #region อัพโหลดเอกสารอื่นๆ
    protected void gvwother_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathother") as ASPxTextBox;
            ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNameother") as ASPxTextBox;
            ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtTruePathother") as ASPxTextBox;
            ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtSysfilenameother") as ASPxTextBox;
            ASPxLabel lblData2 = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwother") as ASPxButton;
            ASPxButton btnDelFilegvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwother") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathother.Text))
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                btnViewgvwother.ClientEnabled = true;
                btnDelFilegvwother.ClientEnabled = true;

            }
            else
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileNameother.ClientInstanceName = txtFileNameother.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathother.ClientInstanceName = txtFilePathother.ClientInstanceName + "_" + VisibleIndex;
            txtTruePathother.ClientInstanceName = txtTruePathother.ClientInstanceName + "_" + VisibleIndex;
            txtSysfilenameother.ClientInstanceName = txtSysfilenameother.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwother.ClientInstanceName = btnViewgvwother.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwother.ClientInstanceName = btnDelFilegvwother.ClientInstanceName + "_" + VisibleIndex;

            // Add Event
            btnViewgvwother.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathother.ClientInstanceName + ".GetValue()+" + txtSysfilenameother.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwother.ClientSideEvents.Click = "function (s, e) { gvwother.PerformCallback(\"deleteUploadother; " + txtFilePathother.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + ";" + txtFileNameother.ClientInstanceName.ToString() + ";" + txtFilePathother.ClientInstanceName.ToString() + " ;" + btnViewgvwother.ClientInstanceName.ToString() + " ; " + btnDelFilegvwother.ClientInstanceName.ToString() + "\");}";
            //btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void uploaderother_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string VendorID = Session["VendorID"] + "";
        string USER = Session["UserID"] + "";
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        int ncol = _Filename.Length - 1;

        //if (_Filename[ncol] == "xlsx" || _Filename[ncol] == "xls" || _Filename[ncol] == "doc" || _Filename[ncol] == "docx" || _Filename[ncol] == "pdf" || _Filename[ncol] == "jpg" || _Filename[ncol] == "jpeg")
        //{

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory6, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGridother.Add(new ListGridother
                {
                    SFILENAME = e.UploadedFile.FileName,
                    SSYSFILENAME = genName + "." + _Filename[count],
                    SPATH = data,
                    STRUEPATH = data2,
                    SVENDORID = VendorID,
                    NEWPICTRUEFLAG = "1",
                    SDOCTYPE = "5",
                    INDEX = listGridother.Count
                });
            }
        }
        else
        {

            return;

        }
        //}
        //else
        //{
        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอับเอกสารประเภทนี้ได้');");
        //}
    }



    protected void gvwother_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxUploadControl Upload = gvwother.FindEditRowCellTemplateControl(null, "uploaderother") as ASPxUploadControl;
        Upload.ClientVisible = true;
        e.Cancel = true;
        //Listgvwdoc4();
        gvwother.CancelEdit();

    }
    #endregion

    #region อัพโหลดในEditFrom

    protected void uploadgvw5_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                //string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //string data2 = string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }

    }

    private void VisibleControlUpload5()
    {
        #region เก็ตคอนโทรน
        ASPxUploadControl uploader5 = (ASPxUploadControl)gvwsign.FindEditFormTemplateControl("uploader5");
        ASPxTextBox txtFileName5 = (ASPxTextBox)gvwsign.FindEditFormTemplateControl("txtFileName5");
        ASPxUploadControl suploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
        ASPxTextBox schkUpload5 = gvwsign.FindEditFormTemplateControl("chkUpload5") as ASPxTextBox;
        ASPxTextBox txtsFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
        ASPxTextBox txtsFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
        ASPxButton btnView5 = gvwsign.FindEditFormTemplateControl("btnView5") as ASPxButton;
        ASPxButton btnDelFile5 = gvwsign.FindEditFormTemplateControl("btnDelFile5") as ASPxButton;
        #endregion
        bool visible = string.IsNullOrEmpty(txtsFilePath5.Text);
        suploader5.Visible = true;
        txtsFileName5.ClientVisible = !visible;
        btnView5.ClientEnabled = !visible;
        btnDelFile5.ClientEnabled = !visible;

        if (!(string.IsNullOrEmpty(txtsFilePath5.Text)))
        {
            schkUpload5.Value = "1";
        }
        else
        {
            schkUpload5.Value = "";
        }


    }
    #endregion

    private void VisibleControlUpload()
    {
        if (!(string.IsNullOrEmpty(txtFilePath.Text)))
        {
            bool visible = string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = visible;
            txtFileName.ClientVisible = !visible;
            btnView.ClientEnabled = !visible;
            btnDelFile.ClientEnabled = !visible;


            chkUpload1.Value = "1";
        }
        else
        {
            bool visible = !string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = !visible;
            txtFileName.ClientVisible = visible;
            btnView.ClientEnabled = visible;
            btnDelFile.ClientEnabled = visible;
            chkUpload1.Value = "";
        }

        if (!(string.IsNullOrEmpty(txtFilePath2.Text)))
        {
            bool visible2 = string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = visible2;
            txtFileName2.ClientVisible = !visible2;
            btnView2.ClientEnabled = !visible2;
            btnDelFile2.ClientEnabled = !visible2;


            chkUpload2.Value = "1";
        }
        else
        {
            bool visible2 = !string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = !visible2;
            txtFileName2.ClientVisible = visible2;
            btnView2.ClientEnabled = visible2;
            btnDelFile2.ClientEnabled = visible2;
            chkUpload2.Value = "";
        }


        if (!(string.IsNullOrEmpty(txtFilePath3.Text)))
        {
            bool visible3 = string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = visible3;
            txtFileName3.ClientVisible = !visible3;
            btnView3.ClientEnabled = !visible3;
            btnDelFile3.ClientEnabled = !visible3;


            chkUpload3.Value = "1";
        }
        else
        {
            bool visible3 = !string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = !visible3;
            txtFileName3.ClientVisible = visible3;
            btnView3.ClientEnabled = visible3;
            btnDelFile3.ClientEnabled = visible3;
            chkUpload3.Value = "";
        }


        //if (!(string.IsNullOrEmpty(txtFilePath4.Text)))
        //{

        //    bool visible4 = string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = visible4;
        //    txtFileName4.ClientVisible = !visible4;
        //    btnView4.ClientEnabled = !visible4;
        //    btnDelFile4.ClientEnabled = !visible4;


        //    chkUpload4.Value = "1";
        //}
        //else
        //{
        //    bool visible4 = !string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = !visible4;
        //    txtFileName4.ClientVisible = visible4;
        //    btnView4.ClientEnabled = visible4;
        //    btnDelFile4.ClientEnabled = visible4;
        //    chkUpload4.Value = "";
        //}

    }

    void FileToServer()
    {
        #region เก็บPathลงviewstate
        if (txtFilePath.Text != "")
        {
            UploadFileToServer(txtFilePath.Text, txtTruePath.Text, txtSysfilename.Text);
        }
        if (txtFilePath2.Text != "")
        {
            UploadFileToServer(txtFilePath2.Text, txtTruePath2.Text, txtSysfilename2.Text);
        }
        if (txtFilePath3.Text != "")
        {
            UploadFileToServer(txtFilePath3.Text, txtTruePath3.Text, txtSysfilename3.Text);
        }
        var chknewDoc = listGrid.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
        if (chknewDoc.Count() > 0)
        {

            foreach (var item in chknewDoc)
            {
                ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilename4") as ASPxTextBox;
                ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFileName4") as ASPxTextBox;
                ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtTruePath4") as ASPxTextBox;
                ASPxTextBox txtFilePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFilePath4") as ASPxTextBox;
                try
                {
                    if (!string.IsNullOrEmpty(txtFilePath4.Text))
                    {

                        UploadFileToServer(txtFilePath4.Text, txtTruePath4.Text, txtSysfilename4.Text);
                    }
                }
                catch
                {

                }
            }
        }

        var chknewDocother = listGridother.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
        if (chknewDocother.Count() > 0)
        {

            foreach (var item in chknewDocother)
            {
                ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilenameother") as ASPxTextBox;
                ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFileNameother") as ASPxTextBox;
                ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtTruePathother") as ASPxTextBox;
                ASPxTextBox txtFilePathother = gvwother.FindRowCellTemplateControl(item.INDEX, null, "txtFilePathother") as ASPxTextBox;
                try
                {
                    if (!string.IsNullOrEmpty(txtFilePathother.Text))
                    {

                        UploadFileToServer(txtFilePathother.Text, txtTruePathother.Text, txtSysfilenameother.Text);
                    }
                }
                catch
                {

                }
            }
        }


        if (!string.IsNullOrEmpty(txtLineNo.Text))
        {//ถ้าแก้ไขจะเข้าตัวบน
            ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
            ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
            ASPxTextBox txtTruePath5 = gvwsign.FindEditFormTemplateControl("txtTruePath5") as ASPxTextBox;
            ASPxTextBox txtSysfilename5 = gvwsign.FindEditFormTemplateControl("txtSysfilename5") as ASPxTextBox;
            try
            {
                if (!string.IsNullOrEmpty(txtFilePath5.Text))
                {
                    UploadFileToServer(txtFilePath5.Text, txtTruePath5.Text, txtSysfilename5.Text);
                }
            }
            catch
            {

            }
        }
        else
        {//ถ้าไม่ได้แก้ไข
            ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
            ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
            ASPxTextBox txtTruePath5 = gvwsign.FindEditFormTemplateControl("txtTruePath5") as ASPxTextBox;
            ASPxTextBox txtSysfilename5 = gvwsign.FindEditFormTemplateControl("txtSysfilename5") as ASPxTextBox;

            try
            {
                if (!string.IsNullOrEmpty(txtFilePath5.Text))
                {
                    UploadFileToServer(txtFilePath5.Text, txtTruePath5.Text, txtSysfilename5.Text);
                }
            }
            catch
            {

            }


        }


        #endregion
    }

    private void UploadFileToServer(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath + filename))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old + filename, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath + filename.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath + filename);
            }
        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void UploadFile2History(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath);
            }
        }
    }

    void Listgvwsign()
    {


        var dblist = listGridsigh.ToList();


        if (dblist.Count > 0)
        {
            gvwsign.DataSource = dblist;
            gvwsign.DataBind();
        }
        else
        {
            //gvwsign.DataSource = dblist;
            //gvwsign.DataBind();
            gvwsign.AddNewRow();
        }



    }

    void ListgvwDoc()
    {
        //        string VendorID = Session["VendorID"] + "";
        //        if (!string.IsNullOrEmpty(VendorID))
        //        {
        //            string strsql = @"SELECT 
        //    SVENDORID, SDOCID, SDOCVERSION, 
        //   SDOCTYPE, SFILENAME, SSYSFILENAME, 
        //   SDESCRIPTION, CACTIVE, DCREATE, 
        //   SCREATE, DUPDATE, SUPDATE, 
        //   DEXPIRE, SPATH
        //FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "' AND CACTIVE = '1' AND SSYSFILENAME IS NOT NULL ORDER BY SDOCTYPE ASC";
        //            DataTable dt = CommonFunction.Get_Data(conn, strsql);
        //            //var dblist = listGrid;

        //            if (dt.Rows.Count > 0)
        //            {
        //                gvwDoc.DataSource = dt;
        //                gvwDoc.DataBind();
        //            }
        //            else
        //            {
        //                gvwDoc.Visible = false;
        //            }
        //        }
        var dblist = listGriddoc.ToList();
        if (dblist.Count > 0)
        {
            gvwDoc.DataSource = dblist;
            gvwDoc.DataBind();
        }
        else
        {
            gvwDoc.Visible = false;
        }
    }

    protected void gvwsign_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("รูปภาพ"))
        {
            int VisibleIndex = e.VisibleIndex;
            dynamic Lineno = gvwsign.GetRowValues(e.VisibleIndex, "LINE_NO");

            var dblist = Listindex.Where(w => w.Index == Lineno + "").ToList();

            if (dblist.Count() > 0)
            {
                ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwsign") as ASPxTextBox;
                ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwsign") as ASPxTextBox;
                ASPxLabel lblData = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData") as ASPxLabel;
                ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwsign") as ASPxButton;
                ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwsign") as ASPxButton;

                btnViewgvwsign.ClientVisible = false;
                btnDelFilegvwsign.ClientVisible = false;
                lblData.ClientVisible = true;
                lblData.Text = " ";
            }
            else
            {
                ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwsign") as ASPxTextBox;
                ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwsign") as ASPxTextBox;
                ASPxLabel lblData = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData") as ASPxLabel;
                ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwsign") as ASPxButton;
                ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwsign") as ASPxButton;

                if (!string.IsNullOrEmpty(txtFilePathgvwsign.Text))
                {

                    btnViewgvwsign.ClientVisible = true;
                    btnDelFilegvwsign.ClientVisible = true;
                    btnViewgvwsign.ClientEnabled = true;
                    btnDelFilegvwsign.ClientEnabled = true;
                    //if (!string.IsNullOrEmpty())
                    //{
                    //    btnViewgvwsign.ClientVisible = false;
                    //    btnDelFilegvwsign.ClientVisible = false;
                    //    btnViewgvwsign.ClientEnabled = false;
                    //    btnDelFilegvwsign.ClientEnabled = false;
                    //}

                }
                else
                {
                    btnViewgvwsign.ClientVisible = false;
                    btnDelFilegvwsign.ClientVisible = false;
                    lblData.ClientVisible = true;
                    lblData.Text = " ";

                }
                txtFileNamegvwsign.ClientInstanceName = txtFileNamegvwsign.ClientInstanceName + "_" + VisibleIndex;
                txtFilePathgvwsign.ClientInstanceName = txtFilePathgvwsign.ClientInstanceName + "_" + VisibleIndex;
                btnViewgvwsign.ClientInstanceName = btnViewgvwsign.ClientInstanceName + "_" + VisibleIndex;
                btnDelFilegvwsign.ClientInstanceName = btnDelFilegvwsign.ClientInstanceName + "_" + VisibleIndex;

                //Add Event
                btnViewgvwsign.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathgvwsign.ClientInstanceName + ".GetValue()+" + txtFileNamegvwsign.ClientInstanceName + ".GetValue());}";
                btnDelFilegvwsign.ClientSideEvents.Click = "function (s, e) { gvwsign.PerformCallback(\"deleteFilegvwsign; " + txtFilePathgvwsign.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwsign.ClientInstanceName.ToString() + ";" + txtFilePathgvwsign.ClientInstanceName.ToString() + " ;" + btnViewgvwsign.ClientInstanceName.ToString() + " ; " + btnDelFilegvwsign.ClientInstanceName.ToString() + "\");}";
            }

            // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void gvwDoc_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwDoc") as ASPxTextBox;
            ASPxTextBox txtFileNamegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwDoc") as ASPxTextBox;
            ASPxLabel lblData2 = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwDoc") as ASPxButton;
            ASPxButton btnDelFilegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwDoc") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathgvwDoc.Text))
            {
                btnViewgvwDoc.ClientVisible = true;
                btnDelFilegvwDoc.ClientVisible = true;
                btnViewgvwDoc.ClientEnabled = true;
                btnDelFilegvwDoc.ClientEnabled = true;

            }
            else
            {
                btnViewgvwDoc.ClientVisible = false;
                btnDelFilegvwDoc.ClientVisible = false;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileNamegvwDoc.ClientInstanceName = txtFileNamegvwDoc.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathgvwDoc.ClientInstanceName = txtFilePathgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwDoc.ClientInstanceName = btnViewgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwDoc.ClientInstanceName = btnDelFilegvwDoc.ClientInstanceName + "_" + VisibleIndex;

            //Add Event
            btnViewgvwDoc.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathgvwDoc.ClientInstanceName + ".GetValue() +" + txtFileNamegvwDoc.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwDoc.ClientSideEvents.Click = "function (s, e) { xcpn.PerformCallback(\"deleteFilegvwdoc; " + txtFilePathgvwDoc.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwDoc.ClientInstanceName.ToString() + ";" + txtFilePathgvwDoc.ClientInstanceName.ToString() + " ;" + btnViewgvwDoc.ClientInstanceName.ToString() + " ; " + btnDelFilegvwDoc.ClientInstanceName.ToString() + "\");}";
            // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    private void AddhistoryVendorsigner(string VendorID, string LINE_NO)
    {


        #region เก็ตคอนโทรน
        //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
        //ASPxUploadControl uploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
        ASPxTextBox txtName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
        ASPxTextBox txtPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
        ASPxTextBox txtTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
        ASPxTextBox txtTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
        ASPxTextBox txtEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
        ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
        ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
        #endregion


        string senddatatohistory = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH
                                      FROM TVENDOR_SIGNER WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND LINE_NO ='" + CommonFunction.ReplaceInjection(LINE_NO + "") + "'";




        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_signer มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH ,NVERSION
                                      FROM TVENDOR_SIGNER_HISTORY 
                                      WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'  AND LINE_NO ='" + CommonFunction.ReplaceInjection(LINE_NO + "") + @"' 
                                      ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

                    string strQuery = @"INSERT INTO TVENDOR_SIGNER_HISTORY(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED,SFILENAME,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["LINE_NO"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SNAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPOSITION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE2"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["EMAIL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PICTURE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["IS_ACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_SIGNER_HISTORY(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED,SFILENAME,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["LINE_NO"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SNAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPOSITION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE2"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["EMAIL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PICTURE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["IS_ACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void AddhistoryVendorDoc(string VendorID, string SDOCID, string SDOCTYPE)
    {
        string senddatatohistory = @"SELECT  SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
                                      FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "' ";

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                      SCREATE, DUPDATE, SUPDATE,DEXPIRE, NVERSION
                                      FROM TVENDOR_DOC_HISTORY WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"' 
                                      ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

                    string strQuery = @"INSERT INTO TVENDOR_DOC_HISTORY(SVENDORID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_DOC_HISTORY(SVENDORID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE ,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void AddhistoryVendor(string VendorID)
    {
        string senddatatohistory = @"SELECT SVENDORID, SABBREVIATION, STEL, SFAX, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, 
                                     DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS, NCAR, NDRIVER, NEXT_CAR, NEXT_DRIVER, 
                                     NSALARYPERMONTH, NSALARYPERTRIP, NBONUS, NTEAMPERYEAR, CACTIVE, DCREATE, SCREATE, 
                                     SUPDATE, DUPDATE, CCATEGORY ,DESCRIPTION,CAUSESAPCOMMIT,CAUSESAPCANCEL,CAUSESAP
                                     FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'";



        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_signer มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {
                string datatohistory = @"SELECT SVENDORID, SABBREVIATION, STEL, SFAX, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, 
                                     DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS, NCAR, NDRIVER, NEXT_CAR, NEXT_DRIVER, 
                                     NSALARYPERMONTH, NSALARYPERTRIP, NBONUS, NTEAMPERYEAR, CACTIVE, DCREATE, SCREATE, 
                                     SUPDATE, DUPDATE, CCATEGORY, NVERSION,DESCRIPTION,CAUSESAPCOMMIT,CAUSESAPCANCEL,CAUSESAP
                                     FROM TVENDOR_HISTORY 
                                     WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                     ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    int num = 0;
                    DateTime? DSTARTPTT = null;
                    DateTime? DBEGINTRANSPORT = null;
                    DateTime? DEXPIRETRANSPORT = null;
                    DateTime? DBEGIN13BIS = null;
                    DateTime? DEXPIRE13BIS = null;

                    int NCAPITAL = int.TryParse((dt.Rows[0]["NCAPITAL"] + ""), out num) ? num : 0;
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
                    {
                        DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
                    {
                        DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
                    {
                        DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
                    {
                        DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
                    {
                        DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
                    }
                    int NCAR = int.TryParse((dt.Rows[0]["NCAR"] + ""), out num) ? num : 0;
                    int NDRIVER = int.TryParse((dt.Rows[0]["NDRIVER"] + ""), out num) ? num : 0;
                    int NEXT_CAR = int.TryParse((dt.Rows[0]["NEXT_CAR"] + ""), out num) ? num : 0;
                    int NEXT_DRIVER = int.TryParse((dt.Rows[0]["NEXT_DRIVER"] + ""), out num) ? num : 0;
                    int NSALARYPERMONTH = int.TryParse((dt.Rows[0]["NSALARYPERMONTH"] + ""), out num) ? num : 0;
                    int NSALARYPERTRIP = int.TryParse((dt.Rows[0]["NSALARYPERTRIP"] + ""), out num) ? num : 0;
                    int NBONUS = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    int NTEAMPERYEAR = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";


                    string strQuery = @"INSERT INTO TVENDOR_HISTORY(SVENDORID,SABBREVIATION,STEL,SFAX,DSTARTPTT,NCAPITAL,DBEGINTRANSPORT
                                      ,DEXPIRETRANSPORT,DBEGIN13BIS,DEXPIRE13BIS,NCAR,NDRIVER,NEXT_CAR,NEXT_DRIVER,NSALARYPERMONTH,NSALARYPERTRIP
                                      ,NBONUS,NTEAMPERYEAR,CACTIVE,DCREATE,SCREATE,SUPDATE,DUPDATE,CCATEGORY,NVERSION,DESCRIPTION,CAUSESAPCOMMIT,CAUSESAPCANCEL,CAUSESAP) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["STEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFAX"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DSTARTPTT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ," + CommonFunction.ReplaceInjection(NCAPITAL + "") + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGINTRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRETRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGIN13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRE13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ," + CommonFunction.ReplaceInjection(NCAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NDRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_CAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_DRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERMONTH + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERTRIP + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NBONUS + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NTEAMPERYEAR + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months(TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months(TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CCATEGORY"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAPCOMMIT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAPCANCEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAP"] + "") + @"') ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    int num = 0;
                    DateTime? DSTARTPTT = null;
                    DateTime? DBEGINTRANSPORT = null;
                    DateTime? DEXPIRETRANSPORT = null;
                    DateTime? DBEGIN13BIS = null;
                    DateTime? DEXPIRE13BIS = null;

                    int NCAPITAL = int.TryParse((dt.Rows[0]["NCAPITAL"] + ""), out num) ? num : 0;
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
                    {
                        DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
                    {
                        DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
                    {
                        DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
                    {
                        DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
                    {
                        DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
                    }
                    int NCAR = int.TryParse((dt.Rows[0]["NCAR"] + ""), out num) ? num : 0;
                    int NDRIVER = int.TryParse((dt.Rows[0]["NDRIVER"] + ""), out num) ? num : 0;
                    int NEXT_CAR = int.TryParse((dt.Rows[0]["NEXT_CAR"] + ""), out num) ? num : 0;
                    int NEXT_DRIVER = int.TryParse((dt.Rows[0]["NEXT_DRIVER"] + ""), out num) ? num : 0;
                    int NSALARYPERMONTH = int.TryParse((dt.Rows[0]["NSALARYPERMONTH"] + ""), out num) ? num : 0;
                    int NSALARYPERTRIP = int.TryParse((dt.Rows[0]["NSALARYPERTRIP"] + ""), out num) ? num : 0;
                    int NBONUS = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    int NTEAMPERYEAR = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_HISTORY(SVENDORID,SABBREVIATION,STEL,SFAX,DSTARTPTT,NCAPITAL,DBEGINTRANSPORT
                                      ,DEXPIRETRANSPORT,DBEGIN13BIS,DEXPIRE13BIS,NCAR,NDRIVER,NEXT_CAR,NEXT_DRIVER,NSALARYPERMONTH,NSALARYPERTRIP
                                      ,NBONUS,NTEAMPERYEAR,CACTIVE,DCREATE,SCREATE,SUPDATE,DUPDATE,CCATEGORY,NVERSION,DESCRIPTION,CAUSESAPCOMMIT,CAUSESAPCANCEL,CAUSESAP) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["STEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFAX"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DSTARTPTT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ," + CommonFunction.ReplaceInjection(NCAPITAL + "") + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGINTRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRETRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGIN13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRE13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ," + CommonFunction.ReplaceInjection(NCAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NDRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_CAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_DRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERMONTH + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERTRIP + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NBONUS + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NTEAMPERYEAR + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CCATEGORY"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAPCOMMIT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAPCANCEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CAUSESAP"] + "") + @"') ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    protected void gvwsign_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string VendorID = Session["VendorID"] + "";
            string USER = Session["UserID"] + "";
            string Line_no = "";
            string SDOCID = Session["SDOCID"] + "";
            string SDOCTYPE = Session["SDOCTYPE"] + "";
            string SDOCID2 = Session["SDOCID2"] + "";
            string SDOCTYPE2 = Session["SDOCTYPE2"] + "";
            string SDOCID3 = Session["SDOCID3"] + "";
            string SDOCTYPE3 = Session["SDOCTYPE3"] + "";
            string SDOCID4 = Session["SDOCID4"] + "";
            string SDOCTYPE4 = Session["SDOCTYPE4"] + "";

            switch (e.CallbackName)
            {

                case "CUSTOMCALLBACK":


                    string[] param = e.Args[0].Split(';');
                    if (param[0] == "editgvwsign")
                    {
                        Line_no = gvwsign.GetRowValues(int.Parse(param[1]), "LINE_NO") + "";
                    }
                    if (CanWrite)
                    {
                        switch (param[0])
                        {
                            case "New": gvwsign.AddNewRow();
                                VisibleControlUpload();
                                break;

                            #region การทำงานใน gvwsigh
                            case "Savegvw":
                                LogUser("46", "E", "แก้ไขข้อมูลผู้ขนส่ง ส่วนผู้ลงนาม", VendorID);
                                #region เก็ตคอนโทรน
                                //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                                //ASPxUploadControl uploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
                                ASPxTextBox txtName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                                ASPxTextBox txtPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
                                ASPxTextBox txtTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
                                ASPxTextBox txtTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
                                ASPxTextBox txtEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
                                ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
                                ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
                                ASPxTextBox txtTruePath5 = gvwsign.FindEditFormTemplateControl("txtTruePath5") as ASPxTextBox;
                                ASPxTextBox txtSysfilename5 = gvwsign.FindEditFormTemplateControl("txtSysfilename5") as ASPxTextBox;
                                ASPxTextBox txtLINE_NO = gvwsign.FindEditFormTemplateControl("txtLINE_NO") as ASPxTextBox;
                                #endregion

                                string Checkitem = @"SELECT SVENDORID||LINE_NO||SNAME||SPOSITION||PHONE||PHONE2||EMAIL||PICTURE||SFILENAME||SPATH||SSYSFILENAME  FROM TVENDOR_SIGNER
                                    WHERE SVENDORID||LINE_NO||SNAME||SPOSITION||PHONE||PHONE2||EMAIL||PICTURE||SFILENAME||SPATH||SSYSFILENAME = '" + CommonFunction.ReplaceInjection(VendorID) + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + CommonFunction.ReplaceInjection(txtName.Text) + CommonFunction.ReplaceInjection(txtPosition.Text) + CommonFunction.ReplaceInjection(txtTel.Text) + CommonFunction.ReplaceInjection(txtTel2.Text) + CommonFunction.ReplaceInjection(txtEmail.Text) + CommonFunction.ReplaceInjection(txtFileName5.Text) + CommonFunction.ReplaceInjection(txtFileName5.Text) + CommonFunction.ReplaceInjection(txtFilePath5.Text) + "'";



                                using (OracleConnection con = new OracleConnection(conn))
                                {

                                    if (con.State == ConnectionState.Closed)
                                    {
                                        con.Open();
                                    }
                                    else
                                    {

                                    }

                                    //ลบไฟล์
                                    Deletedatagvwsigh();
                                    Deletefilegvwsigh();


                                    DataTable chkgvw = CommonFunction.Get_Data(con, Checkitem);

                                    int Numgvw = 0;
                                    //เช็คว่าข้อมูลที่แสดงกับข้อมูลในเบสว่าตรงกันไหม
                                    if (chkgvw.Rows.Count > 0)
                                    {


                                    }
                                    else
                                    {
                                        //เช็คว่ามีคนที่บันทึกข้อมูลเป็นใครถ้ามีอยู่แล้วให้อับเดทข้อมูลและเก็บลงประวัติ แต่ถ้าไม่มีให้เซฟเข้าไป

                                        string chkVendor = "";
                                        if (txtLINE_NO.Text != "")
                                        {
                                            chkVendor = @"SELECT * FROM TVENDOR_SIGNER
                                            WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                            AND LINE_NO ='" + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + @"'
                                            ORDER BY LINE_NO DESC";
                                        }
                                        else
                                        {
                                            chkVendor = @"SELECT * FROM TVENDOR_SIGNER
                                            WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                            ORDER BY LINE_NO DESC";
                                        }
                                        DataTable dt = CommonFunction.Get_Data(con, chkVendor);
                                        ///เช็คว่าจะเซฟหรืออับเดท ถ้าtxtLINE_NOเป็นค่าว่างเป็นเซฟ แล้วดึงค่า LINE_NO ที่มากที่สุดมาสร้างไอดีใหม่
                                        if (txtLINE_NO.Text != "")
                                        {

                                        }
                                        else
                                        {
                                            if (dt.Rows.Count > 0)
                                            {
                                                Numgvw = int.Parse(dt.Rows[0]["LINE_NO"] + "") + 1;
                                            }
                                            else
                                            {
                                                Numgvw = 1;
                                            }
                                        }

                                        if (txtLINE_NO.Text != "")
                                        {
                                            //แอดข้อมูลลง History


                                            AddhistoryVendorsigner(VendorID, txtLINE_NO.Text);
                                            //อัพเดทข้อมูล
                                            LogUser("46", "E", "ข้อมูลผู้ขนส่ง บักทึกข้อมูลผู้มีสิทธิ์ลงนาม", VendorID);
                                            string strUpdatevendor = @"UPDATE TVENDOR_SIGNER
                                         SET SNAME='" + CommonFunction.ReplaceInjection(txtName.Text) + @"'
                                            ,SPOSITION='" + CommonFunction.ReplaceInjection(txtPosition.Text) + @"'
                                            ,PHONE='" + CommonFunction.ReplaceInjection(txtTel.Text) + @"'
                                            ,PHONE2='" + CommonFunction.ReplaceInjection(txtTel2.Text) + @"'
                                            ,EMAIL='" + CommonFunction.ReplaceInjection(txtEmail.Text) + @"'
                                            ,PICTURE='" + CommonFunction.ReplaceInjection(txtFileName5.Text) + @"'
                                            ,IS_ACTIVE='1'
                                            ,DATE_UPDATED= sysdate
                                            ,USER_UPDATED='" + CommonFunction.ReplaceInjection(USER) + @"'
                                            ,SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName5.Text) + @"'
                                            ,SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath5.Text) + @"'
                                            ,SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename5.Text) + @"'
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "' AND LINE_NO = '" + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + "'";

                                            using (OracleCommand com = new OracleCommand(strUpdatevendor, con))
                                            {
                                                com.ExecuteNonQuery();
                                                Filegvwsigh.RemoveAll(w => w.LINE_NO == txtLINE_NO.Text);
                                            }

                                        }
                                        else
                                        {
                                            //ถ้าไม่มีข้อมูลจะบันทึกทันที
                                            LogUser("46", "I", "ข้อมูลผู้ขนส่ง บักทึกข้อมูลผู้มีสิทธิ์ลงนาม", VendorID);
                                            string strQuery = @"INSERT INTO TVENDOR_SIGNER(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,SFILENAME,SPATH,SSYSFILENAME) 
                                       VALUES('" + CommonFunction.ReplaceInjection(VendorID) + "'," + CommonFunction.ReplaceInjection(Numgvw + "") + ",'" + CommonFunction.ReplaceInjection(txtName.Text) + "','" + CommonFunction.ReplaceInjection(txtPosition.Text) + "','" + CommonFunction.ReplaceInjection(txtTel.Text) + "','" + CommonFunction.ReplaceInjection(txtTel2.Text) + "','" + CommonFunction.ReplaceInjection(txtEmail.Text) + "','" + CommonFunction.ReplaceInjection(txtFileName5.Text) + "','1',SYSDATE,'" + CommonFunction.ReplaceInjection(USER) + "','" + CommonFunction.ReplaceInjection(txtFileName5.Text) + "','" + CommonFunction.ReplaceInjection(txtTruePath5.Text) + "','" + CommonFunction.ReplaceInjection(txtSysfilename5.Text) + "') ";

                                            using (OracleCommand com = new OracleCommand(strQuery, con))
                                            {
                                                com.ExecuteNonQuery();
                                            }

                                        }
                                    }
                                    con.Close();
                                    FileToServer();
                                    gvwsign.CancelEdit();
                                    // Listgvwsign();

                                    //listGridsigh.Clear();
                                    SetdataSigh();
                                    Listgvwsign();
                                    //if (DeleteGridsigh.Count > 0)
                                    //{
                                    //    foreach (var item in DeleteGridsigh)
                                    //    {
                                    //        listGridsigh.RemoveAll(w => w.SVENDORID == item.SVENDORID && w.LINE_NO == item.LINE_NO);
                                    //    }
                                    //}
                                    //gvwsign.DataSource = listGridsigh;
                                    //gvwsign.DataBind();
                                    //Listgvwsign();

                                }
                                break;


                            //เมื่อกดแก้ไขในข้อมูลผู้ลงนาม
                            case "editgvwsign":

                                int index = int.Parse(param[1]);
                                gvwsign.StartEdit(index);

                                //ค้นหาข้อมูลโดยใช้ VendorId และ LINE_NO เพื่อเซ็ตค่า
                                string strsetdata = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH ,SSYSFILENAME
                                      FROM TVENDOR_SIGNER WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'  AND LINE_NO ='" + CommonFunction.ReplaceInjection(Line_no + "") + "'";



                                using (OracleConnection con = new OracleConnection(conn))
                                {

                                    if (con.State == ConnectionState.Closed)
                                    {
                                        con.Open();
                                    }
                                    else
                                    {

                                    }
                                    #region เก็ตคอนโทรน
                                    //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                                    ASPxUploadControl suploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
                                    ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                                    ASPxTextBox txtsPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
                                    ASPxTextBox txtsTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
                                    ASPxTextBox txtsTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
                                    ASPxTextBox txtsEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
                                    ASPxTextBox txtsFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
                                    ASPxTextBox txtsFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
                                    ASPxTextBox txtsTruePath5 = gvwsign.FindEditFormTemplateControl("txtTruePath5") as ASPxTextBox;
                                    ASPxTextBox txtsSysfilename5 = gvwsign.FindEditFormTemplateControl("txtSysfilename5") as ASPxTextBox;
                                    ASPxTextBox txtsLINE_NO = gvwsign.FindEditFormTemplateControl("txtLINE_NO") as ASPxTextBox;
                                    ASPxButton btnView5 = gvwsign.FindEditFormTemplateControl("btnView5") as ASPxButton;
                                    ASPxButton btnDelFile5 = gvwsign.FindEditFormTemplateControl("btnDelFile5") as ASPxButton;

                                    #endregion

                                    //แก้ไขข้อมูลเซ็ตค่าให้คอนโทรน
                                    DataTable data = CommonFunction.Get_Data(con, strsetdata);
                                    if (data.Rows.Count > 0)
                                    {
                                        gvwsign.JSProperties["cpTest"] = data.Rows[0]["LINE_NO"] + "";
                                        txtsLINE_NO.Text = data.Rows[0]["LINE_NO"] + "";
                                        txtsName.Text = data.Rows[0]["SNAME"] + "";
                                        txtsPosition.Text = data.Rows[0]["SPOSITION"] + "";
                                        txtsTel.Text = data.Rows[0]["PHONE"] + "";
                                        txtsTel2.Text = data.Rows[0]["PHONE2"] + "";
                                        txtsEmail.Text = data.Rows[0]["EMAIL"] + "";

                                        if (!string.IsNullOrEmpty(data.Rows[0]["SPATH"] + ""))
                                        {
                                            var Gvwcheck = Filegvwsigh.Where(w => w.LINE_NO == txtsLINE_NO.Text);
                                            if (Gvwcheck.Count() > 0)
                                            {
                                                //string Path = "";
                                                txtsFileName5.Text = "";
                                                txtsFilePath5.Text = "";
                                                txtsTruePath5.Text = "";
                                                txtsSysfilename5.Text = "";
                                                suploader5.ClientVisible = true;
                                                txtsFilePath5.ClientVisible = false;
                                                //btnView5.ClientEnabled = true;
                                                //btnDelFile5.ClientEnabled = true;
                                                txtsFileName5.ClientEnabled = false;
                                                txtsFileName5.ClientVisible = false;

                                                //btnView5.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + Path + ");}";
                                            }
                                            else
                                            {
                                                //string Path = data.Rows[0]["SPATH"] + "" + data.Rows[0]["SSYSFILENAME"] + "";
                                                txtsFileName5.Text = data.Rows[0]["SFILENAME"] + "";
                                                txtsFilePath5.Text = data.Rows[0]["SPATH"] + "";
                                                txtsTruePath5.Text = data.Rows[0]["SPATH"] + "";
                                                txtsSysfilename5.Text = data.Rows[0]["SSYSFILENAME"] + "";
                                                suploader5.ClientVisible = false;
                                                txtsFilePath5.ClientVisible = false;
                                                //btnView5.ClientEnabled = true;
                                                //btnDelFile5.ClientEnabled = true;
                                                txtsFileName5.ClientEnabled = false;
                                                txtsFileName5.ClientVisible = true;
                                                //btnView5.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + Path + ");}";
                                            }
                                        }
                                        else
                                        {
                                            suploader5.ClientVisible = true;
                                            txtsFilePath5.ClientVisible = false;
                                            //btnView5.ClientEnabled = false;
                                            //btnDelFile5.ClientEnabled = false;
                                        }

                                        VisibleControlUpload5();

                                    }
                                    else
                                    {

                                    }
                                    con.Close();
                                }
                                break;


                            //ลบไฟล์
                            case "deleteFilegvwsign":

                                dynamic delete = gvwsign.GetRowValues(int.Parse(param[3]), "SVENDORID", "LINE_NO", "SFILENAME", "SPATH", "SSYSFILENAME");
                                string FilePathgvwsign = param[1];

                                Listindex.Add(new gvwsighListColumn { Index = delete[1] + "" });

                                ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "txtFileNamegvwsign") as ASPxTextBox;
                                ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "txtFilePathgvwsign") as ASPxTextBox;
                                ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "btnViewgvwsign") as ASPxButton;
                                ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "btnDelFilegvwsign") as ASPxButton;

                                Filegvwsigh.Add(new DeleteFilegvwsigh
                                {
                                    SVENDORID = delete[0] + "",
                                    LINE_NO = delete[1] + "",
                                    Index = param[3] + "",
                                    SFILENAME = delete[2] + "",
                                    SPATH = delete[3] + "",
                                    SSYSFILENAME = delete[4] + ""

                                });

                                var SetNewData = listGridsigh.Where(w => w.LINE_NO == delete[1] + "").FirstOrDefault();

                                if (SetNewData != null)
                                {
                                    SetNewData.SSYSFILENAME = "";
                                    SetNewData.SPATH = "";
                                    SetNewData.SFILENAME = "";
                                }

                                //listGridsigh.RemoveAll(w=> w.)

                                string cNogvwsign = param[2];
                                if (cNogvwsign == "1")
                                {
                                    txtFilePathgvwsign.Text = "";
                                    txtFileNamegvwsign.Text = "";
                                    btnViewgvwsign.Visible = false;
                                    btnDelFilegvwsign.Visible = false;
                                }

                                gvwsign.CancelEdit();

                                Listgvwsign();
                                break;


                            case "Cancelgvw": gvwsign.CancelEdit();
                                Listindex = new List<gvwsighListColumn>();
                                //SetdataSigh();
                                Listgvwsign();
                                break;

                            //ลบแถว
                            case "deletegvwsign":
                                //int index = int.Parse(param[1]);


                                string DSVENDORID = gvwsign.GetRowValues(int.Parse(param[1]), "SVENDORID").ToString();
                                string DLINE_NO = gvwsign.GetRowValues(int.Parse(param[1]), "LINE_NO").ToString();

                                DeleteGridsigh.Add(new Deletegvwsigh
                                {
                                    SVENDORID = DSVENDORID,
                                    LINE_NO = DLINE_NO
                                });


                                listGridsigh.RemoveAll(w => w.SVENDORID == DSVENDORID && w.LINE_NO == DLINE_NO);

                                if (listGridsigh.Count > 0)
                                {

                                    gvwsign.DataSource = listGridsigh;
                                    gvwsign.DataBind();
                                }
                                else
                                {
                                    gvwsign.DataSource = listGridsigh;
                                    gvwsign.DataBind();
                                    gvwsign.AddNewRow();
                                }
                                //var Dblist = listGridsigh;
                                //gvwsign.DataSource = Dblist;
                                //gvwsign.DataBind();

                                //Listgvwsign();
                                //using (OracleConnection con = new OracleConnection(conn))
                                //{

                                //    if (con.State == ConnectionState.Closed)
                                //    {
                                //        con.Open();
                                //    }
                                //    else
                                //    {

                                //    }

                                //    string steDelete = @"UPDATE TVENDOR_SIGNER SET IS_ACTIVE='0' WHERE SVENDORID='" + DSVENDORID + "' AND LINE_NO = '" + DLINE_NO + "' ";
                                //    using (OracleCommand com = new OracleCommand(steDelete, con))
                                //    {
                                //        com.ExecuteNonQuery();
                                //    }
                                //    con.Close();
                                //}
                                //gvwsign.DataSource = dblist;
                                //gvwsign.DataBind();

                                break;


                            case "deleteEditform":
                                ASPxTextBox stxtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
                                ASPxTextBox stxtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
                                ASPxTextBox stxtTruePath5 = gvwsign.FindEditFormTemplateControl("txtTruePath5") as ASPxTextBox;
                                ASPxTextBox stxtSysfilename5 = gvwsign.FindEditFormTemplateControl("txtSysfilename5") as ASPxTextBox;

                                stxtFilePath5.Text = "";
                                stxtFileName5.Text = "";
                                stxtTruePath5.Text = "";
                                stxtSysfilename5.Text = "";



                                break;
                            #endregion
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(gvwsign, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }

                    break;





            }
        }
    }

    protected void gvwdoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string VendorID = Session["VendorID"] + "";
            string USER = Session["UserID"] + "";


            switch (e.CallbackName)
            {

                case "CUSTOMCALLBACK":


                    string[] param = e.Args[0].Split(';');
                    if (CanWrite)
                    {
                        switch (param[0])
                        {

                            case "Newgvwdoc4": gvwdoc4.AddNewRow();
                                gvwdoc4.Visible = true;


                                //VisibleControlUpload();
                                break;
                            case "deleteUpload4":
                                dynamic delete = gvwdoc4.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SDOCTYPE", "SPATH", "SSYSFILENAME");

                                try
                                {
                                    DeleteGrid.Add(new DeleteItem
                                    {
                                        SVENDORID = delete[0],
                                        SDOCID = delete[1],
                                        SDOCTYPE = delete[2],
                                        SPATHALL = delete[3] + delete[4]
                                    });

                                    int index = int.Parse(param[3]);
                                    listGrid.RemoveAt(index);
                                    Session["nosavedelete4"] = "deletenotsave";
                                    Listgvwdoc4();
                                }
                                catch
                                {

                                }
                                //listGriddoc.RemoveAll(d => d.SDOCID == delete[1] + "");


                                //gvwDoc.DataSource = listGriddoc.ToList();
                                //gvwDoc.DataBind();

                                break;
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(gvwdoc4, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }

                    break;

            }
        }
    }

    protected void gvwother_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string VendorID = Session["VendorID"] + "";
            string USER = Session["UserID"] + "";


            switch (e.CallbackName)
            {

                case "CUSTOMCALLBACK":


                    string[] param = e.Args[0].Split(';');
                    if (CanWrite)
                    {
                        switch (param[0])
                        {

                            case "Newgvwother": gvwother.AddNewRow();
                                gvwother.Visible = true;


                                //VisibleControlUpload();
                                break;
                            case "deleteUploadother":


                                dynamic delete = gvwother.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SDOCTYPE", "SPATH", "SSYSFILENAME");

                                try
                                {
                                    DeleteGrid.Add(new DeleteItem
                                    {
                                        SVENDORID = delete[0],
                                        SDOCID = delete[1],
                                        SDOCTYPE = delete[2],
                                        SPATHALL = delete[3] + delete[4]
                                    });

                                    int index = int.Parse(param[3]);
                                    listGridother.RemoveAt(index);
                                    Session["nosavedeleteother"] = "deletenotsave";
                                    Listgvwother();
                                }
                                catch
                                {

                                }

                                break;
                        }

                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(gvwother, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    }
                    break;





            }
        }
    }

    void Listgvwdoc4()
    {
        string SVENDORID = Session["VendorID"] + "";

        //เช็คว่าค่าในList กับในดาต้าเบสเท่ากันไหม ถ้าเมท่ากันคือSaveแล้วให้Setdata ใหม่ แต่ถ้าไม่เท่ากันสแดงว่ายังไม่ได้เซฟให้เอาลิสมาริสข้อมูลใหม่
        if (txtUploadchk4.Text != "S")
        {
            string chkDocInDB = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                  SCREATE, DUPDATE, SUPDATE,  DEXPIRE, SPATH FROM TVENDOR_DOC 
                                  WHERE  SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SDOCTYPE = '4'";
            DataTable chkData = new DataTable();
            chkData = CommonFunction.Get_Data(conn, chkDocInDB);
            var dblist = listGrid.Where(w => w.SDOCTYPE == "4").ToList();
            if (listGrid.Count > 0)
            {//เช็คว่าข้อมูลมีในเบสไหม




                if (dblist.Count > 0)
                {//เช็คว่าในลิสมีข้อมูลไหม ถ้าในเบสมีข้อมูลแต่ถ้าใน List ไม่มี คือบายข้อมูลผิด

                    gvwdoc4.DataSource = dblist;
                    gvwdoc4.DataBind();

                }
                else
                {
                    gvwdoc4.DataBind();
                    gvwdoc4.Visible = true;
                    gvwdoc4.AddNewRow();

                }

            }
            else
            {
                //if (ViewState["nosavedelete4"] + "" == "deletenotsave")//เช็คว่าลบแล้วแต่ยังไม่ได้เซฟ
                //{
                var dblist2 = listGrid.ToList();
                if (dblist2.Count > 0)
                {//เช็คว่าข้อมูลมีในเบสไหม
                    gvwdoc4.DataSource = dblist2;
                    gvwdoc4.DataBind();




                }
                else
                {
                    gvwdoc4.DataSource = dblist2;
                    gvwdoc4.DataBind();
                    gvwdoc4.Visible = true;
                    gvwdoc4.AddNewRow();

                }

                //}
                //else
                //{
                //    if (chkData.Rows.Count > 0)//เช็คจากข้อมูลในเบสจริงว่ามีจริงถ้ามีจริงให้ลิสแต่ถ้า
                //    {
                //        Setdata();
                //    }
                //    else
                //    {
                //        gvwdoc4.DataSource = dblist;
                //        gvwdoc4.DataBind();
                //        gvwdoc4.Visible = true;
                //        gvwdoc4.AddNewRow();
                //    }
                //}

            }
        }

    }

    void Listgvwother()
    {
        string SVENDORID = Session["VendorID"] + "";

        //เช็คว่าค่าในList กับในดาต้าเบสเท่ากันไหม ถ้าเมท่ากันคือSaveแล้วให้Setdata ใหม่ แต่ถ้าไม่เท่ากันสแดงว่ายังไม่ได้เซฟให้เอาลิสมาริสข้อมูลใหม่
        if (txtUploadother.Text != "S")
        {
            string chkDocInDB = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                  SCREATE, DUPDATE, SUPDATE,  DEXPIRE, SPATH FROM TVENDOR_DOC 
                                  WHERE  SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SDOCTYPE = '5'";
            DataTable chkData = new DataTable();
            chkData = CommonFunction.Get_Data(conn, chkDocInDB);
            var dblist = listGridother.Where(w => w.SDOCTYPE == "5").ToList();
            if (listGridother.Count > 0)
            {//เช็คว่าข้อมูลมีในเบสไหม




                if (dblist.Count > 0)
                {//เช็คว่าในลิสมีข้อมูลไหม ถ้าในเบสมีข้อมูลแต่ถ้าใน List ไม่มี คือบายข้อมูลผิด

                    gvwother.DataSource = dblist;
                    gvwother.DataBind();

                }
                else
                {
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

            }
            else
            {
                //if (ViewState["nosavedeleteother"] + "" == "deletenotsave")//เช็คว่าลบแล้วแต่ยังไม่ได้เซฟ
                //{
                var dblist2 = listGridother.ToList();
                if (dblist2.Count > 0)
                {//เช็คว่าข้อมูลมีในเบสไหม
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();




                }
                else
                {
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

                //}
                //else
                //{
                //    if (chkData.Rows.Count > 0)//เช็คจากข้อมูลในเบสจริงว่ามีจริงถ้ามีจริงให้ลิสแต่ถ้า
                //    {
                //        Setdata();
                //    }
                //    else
                //    {
                //        gvwother.DataSource = dblist;
                //        gvwother.DataBind();
                //        gvwother.Visible = true;
                //        gvwother.AddNewRow();
                //    }
                //}

            }
        }

    }

    void Deletefilegvwsigh()
    {


        var dblist = Filegvwsigh.ToList();
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            foreach (var item in dblist)
            {
                LogUser("46", "D", "ข้อมูลผู้ขนส่ง ลบไฟล์ผู้มีสิทธิ์ลงนาม", item.SVENDORID + "," + item.LINE_NO);
                string SVENDORID = item.SVENDORID;
                string LINE_NO = item.LINE_NO;
                int index = int.Parse(item.Index);

                AddhistoryVendorsigner(SVENDORID, LINE_NO);
                string HistoryPath = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploader5", Session["UserID"] + "");
                UploadFile2History(item.SPATH, HistoryPath, item.SSYSFILENAME);
                //string SVENDORID = gvwsign.GetRowValues(index, "SVENDORID").ToString();
                //string LINE_NO = gvwsign.GetRowValues(index, "LINE_NO").ToString();

                string strQuery = @"UPDATE TVENDOR_SIGNER
SET PICTURE='" + CommonFunction.ReplaceInjection("") + @"'
,SFILENAME='" + CommonFunction.ReplaceInjection("") + @"'
,SPATH='" + CommonFunction.ReplaceInjection("") + @"'
,SSYSFILENAME='" + CommonFunction.ReplaceInjection("") + @"'
WHERE SVENDORID='" + SVENDORID + "' AND LINE_NO = '" + LINE_NO + "' ";

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }
            }



            // int Numgvw = gvwsign.VisibleRowCount + 1;


            con.Close();
        }
    }

    void Deletedatagvwsigh()
    {

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            foreach (var item in DeleteGridsigh.ToList())
            {
                LogUser("46", "D", "ข้อมูลผู้ขนส่ง ลบข้อมูลผู้มีสิทธิ์ลงนาม", item.SVENDORID + "," + item.LINE_NO);
                string steDelete = @"UPDATE TVENDOR_SIGNER SET IS_ACTIVE='0' WHERE SVENDORID='" + item.SVENDORID + "' AND LINE_NO = '" + item.LINE_NO + "' ";
                using (OracleCommand com = new OracleCommand(steDelete, con))
                {
                    com.ExecuteNonQuery();
                }
            }
            con.Close();
        }
    }

    void deleteFiletemp()
    {
        string VendorID = Session["SVDID"] + "";


        string deletepath = (Server.MapPath("./") + "UploadFile/VendorDoc/Temp/" + CommonFunction.ReplaceInjection(VendorID) + "").Replace("/", "\\");
        if (Directory.Exists(deletepath))
        {
            try
            {
                System.IO.Directory.Delete(deletepath.Replace("/", "\\"), true);
            }
            catch
            {

            }
        }



    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    [Serializable]
    class ListGrid
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }
    [Serializable]
    class ListGridother
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }
    [Serializable]
    class ListGridDoc
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }
    [Serializable]
    class ListGridsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }
        public string SNAME { get; set; }
        public string SPOSITION { get; set; }
        public string PHONE { get; set; }
        public string PHONE2 { get; set; }
        public string EMAIL { get; set; }
        public string PICTURE { get; set; }
        public string IS_ACTIVE { get; set; }
        public DateTime? DATE_CREATED { get; set; }
        public string USER_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }
        public string USER_UPDATED { get; set; }
        public string SFILENAME { get; set; }
        public string SPATH { get; set; }
        public string SSYSFILENAME { get; set; }

    }
    [Serializable]
    class DeleteItem
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public string SDOCTYPE { get; set; }
        public string SPATHALL { get; set; }

    }
    [Serializable]
    class DeleteFilegvwsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }
        public string Index { get; set; }
        public string SPATH { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
    }
    [Serializable]
    class Deletegvwsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }

    }
    [Serializable]
    class gvwsighListColumn
    {
        public string Index { get; set; }
    }

}