﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.Report;

public partial class RequireField : PageBase
{
    #region + View State +
    private int TopicID
    {
        get
        {
            if ((int)ViewState["TopicID"] != null)
                return (int)ViewState["TopicID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }

    private DataTable dtField
    {
        get
        {
            if ((DataTable)ViewState["dtField"] != null)
                return (DataTable)ViewState["dtField"];
            else
                return null;
        }
        set
        {
            ViewState["dtField"] = value;
        }
    }

    private DataTable dtFile
    {
        get
        {
            if ((DataTable)ViewState["dtFile"] != null)
                return (DataTable)ViewState["dtFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtFile"] = value;
        }
    }

    private DataTable dtSubject2
    {
        get
        {
            if ((DataTable)ViewState["dtSubject2"] != null)
                return (DataTable)ViewState["dtSubject2"];
            else
                return null;
        }
        set
        {
            ViewState["dtSubject2"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.InitialScreen();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlScreen_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.InitialScreen();
    }

    private void InitialScreen()
    {
        try
        {
            DataTable dtSubject1 = new DataTable();

            if (ddlScreen.SelectedIndex == 0 || ddlScreen.SelectedIndex == 1)
            {//Complain
                lblSubject1.Visible = true;
                ddlSubject1.Visible = true;
                lblSubject2.Visible = true;
                ddlSubject2.Visible = true;
                tblComplain.Visible = false;
                tblMonthlyReport.Visible = false;

                lblSubject1.Text = "ประเภทร้องเรียน :";
                lblSubject2.Text = "หัวข้อร้องเรียน :";

                dtSubject1 = TopicBLL.Instance.TopicSelectAllBLL(string.Empty);
                DropDownListHelper.BindDropDownList(ref ddlSubject1, dtSubject1, "TOPIC_ID", "TOPIC_NAME", true);
            }
            else if (ddlScreen.SelectedIndex == 2)
            {//Driver
                lblSubject1.Visible = true;
                ddlSubject1.Visible = true;
                lblSubject2.Visible = false;
                ddlSubject2.Visible = false;
                tblComplain.Visible = false;
                tblMonthlyReport.Visible = false;

                lblSubject1.Text = "ตำแหน่ง :";

                dtSubject1 = VendorBLL.Instance.PositionSelectAllBLL(string.Empty);
                DropDownListHelper.BindDropDownList(ref ddlSubject1, dtSubject1, "PERSON_TYPE", "PERSON_TYPE_DESC", true);
            }
            else if (ddlScreen.SelectedValue == "ACCIDENTTAB1")
            {
                lblSubject1.Visible = true;
                ddlSubject1.Visible = true;
                lblSubject2.Visible = false;
                ddlSubject2.Visible = false;
                tblComplain.Visible = false;
                tblMonthlyReport.Visible = false;

                dtSubject1 = AccidentBLL.Instance.AccidentTypeSelect(string.Empty,"1");
                DropDownListHelper.BindDropDownList(ref ddlSubject1, dtSubject1, "ID", "NAME", true);
                lblSubject1.Text = "ขั้นตอนการขนส่งขณะเกิดเหตุ :";
            }
            else
            {//Other
                lblSubject1.Visible = false;
                ddlSubject1.Visible = false;
                lblSubject2.Visible = false;
                ddlSubject2.Visible = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlSubject1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlScreen.SelectedIndex == 0 || ddlScreen.SelectedIndex == 1)
            {//Complain
                if (ddlSubject1.SelectedIndex == 0)
                    DropDownListHelper.BindDropDownList(ref ddlSubject2, null, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
                else
                {
                    dtSubject2 = ComplainTypeBLL.Instance.ComplainTypeSelectAllBLL(" AND M_TOPIC.TOPIC_ID = '" + ddlSubject1.SelectedValue + "'");
                    DropDownListHelper.BindDropDownList(ref ddlSubject2, dtSubject2, "COMPLAIN_TYPE_ID", "COMPLAIN_TYPE_NAME", true);
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSearch();

            #region + Require Field +
            dtField = RequireBLL.Instance.RequireTemplateSelectBLL(ddlScreen.SelectedValue);
            DataTable dtRequireField = new DataTable();

            if (ddlScreen.SelectedIndex == 0)
                dtRequireField = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlSubject2.SelectedValue), ddlScreen.SelectedValue);
            else if (ddlScreen.SelectedIndex == 1)
                dtRequireField = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlSubject1.SelectedValue), ddlScreen.SelectedValue);
            else if (ddlScreen.SelectedIndex == 2 || ddlScreen.SelectedValue == "ACCIDENTTAB1")
                dtRequireField = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlSubject1.SelectedValue), ddlScreen.SelectedValue);
            else if (ddlScreen.SelectedValue == "ACCIDENTTAB2")
            {
                dtRequireField = ComplainBLL.Instance.ComplainRequireFieldBLL(0, ddlScreen.SelectedValue);
            }
            GridViewHelper.BindGridView(ref dgvField, dtField);

            DropDownList ddl = new DropDownList();

            if (dtRequireField.Rows.Count == 0)
            {
                for (int i = 0; i < dtField.Rows.Count; i++)
                {
                    ddl = (DropDownList)dgvField.Rows[i].FindControl("ddlRequire");
                    ddl.SelectedValue = "2";                    //OPTIONAL
                }
            }

            for (int i = 0; i < dtRequireField.Rows.Count; i++)
            {
                for (int j = 0; j < dtField.Rows.Count; j++)
                {
                    ddl = (DropDownList)dgvField.Rows[j].FindControl("ddlRequire");
                    if (string.Equals(dtRequireField.Rows[i]["CONTROL_ID"].ToString(), dgvField.DataKeys[j].Value.ToString()))
                        ddl.SelectedValue = dtRequireField.Rows[i]["REQUIRE_TYPE"].ToString();
                }
            }
            #endregion

            #region + Request File +
            DataTable dtRequestFile = new DataTable();

            dtFile = UploadTypeBLL.Instance.UploadTypeSelectBLL(ddlScreen.SelectedValue);

            if (ddlScreen.SelectedIndex == 0)
                dtRequestFile = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionRequestFile());
            if (ddlScreen.SelectedIndex == 1)
                dtRequestFile = ComplainBLL.Instance.ComplainRequestFileBLL(this.GetConditionRequestFile());
            else if (ddlScreen.SelectedIndex == 2)
                dtRequestFile = VendorBLL.Instance.VendorRequestFile(ddlSubject1.SelectedValue);
            if (ddlScreen.SelectedIndex == 3)
                dtRequestFile = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = '" + ddlScreen.SelectedValue + "'");
            else if ((ddlScreen.SelectedIndex == 4) || (ddlScreen.SelectedIndex == 5) || (ddlScreen.SelectedIndex == 6))
                dtRequestFile = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = '" + ddlScreen.SelectedValue + "'");
            else if (ddlScreen.SelectedValue == "ACCIDENTTAB2" || ddlScreen.SelectedValue == "ACCIDENTTAB3VENDOR")
                dtRequestFile = AccidentBLL.Instance.AccidentRequestFile(ddlScreen.SelectedValue);

            GridViewHelper.BindGridView(ref dgvUploadFile, dtFile);

            RadioButtonList rad = new RadioButtonList();
            for (int i = 0; i < dtRequestFile.Rows.Count; i++)
            {
                for (int j = 0; j < dtFile.Rows.Count; j++)
                {
                    rad = (RadioButtonList)dgvUploadFile.Rows[j].FindControl("radRequire");
                    if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dgvUploadFile.DataKeys[j].Value.ToString()))
                        rad.SelectedValue = "1";
                }
            }
            #endregion

            #region + Other +
            if (ddlScreen.SelectedIndex == 0)
            {
                tblComplain.Visible = true;

                radDisableDriver.SelectedValue = dtSubject2.Rows[ddlSubject2.SelectedIndex]["LOCK_DRIVER"].ToString();
                radMultiplecar.SelectedValue = dtSubject2.Rows[ddlSubject2.SelectedIndex]["IS_ADD_MULTIPLE_CAR"].ToString();
            }
            else if ((ddlScreen.SelectedIndex == 3) || (ddlScreen.SelectedIndex == 4) || (ddlScreen.SelectedIndex == 5))
            {
                tblMonthlyReport.Visible = true;
                DataTable dtConfig = MonthlyReportBLL.Instance.ReportSelectConfigBLL();
                if (dtConfig.Rows.Count > 0)
                {
                    txtStartUploadDay.Text = dtConfig.Rows[0]["START_UPLOAD_DAY"].ToString();
                    txtEndUploadDay.Text = dtConfig.Rows[0]["END_UPLOAD_DAY"].ToString();
                }
            }
            #endregion

            ddlScreen.Enabled = false;
            ddlSubject1.Enabled = false;
            ddlSubject2.Enabled = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionRequestFile()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID = " + ddlSubject2.SelectedValue);
            sb.Append(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = '" + ddlScreen.SelectedValue + "'");
            sb.Append(" ORDER BY M_COMPLAIN_TYPE.COMPLAIN_TYPE_ID");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSearch()
    {
        try
        {
            if (ddlScreen.SelectedIndex == 0)
            {
                if (ddlSubject1.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือก " + lblSubject1.Text.Replace(":", string.Empty));

                if (ddlSubject2.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือก " + lblSubject2.Text.Replace(":", string.Empty));
            }
            else if (ddlScreen.SelectedIndex == 1)
            {
                if (ddlSubject1.SelectedIndex < 1)
                    throw new Exception("กรุณาเลือก " + lblSubject1.Text.Replace(":", string.Empty));
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            string ComplainTypeID = "-1";
            if (ddlScreen.SelectedIndex == 0)
                ComplainTypeID = ddlSubject2.SelectedValue;
            else if (ddlScreen.SelectedIndex == 1 || ddlScreen.SelectedValue == "ACCIDENTTAB1")
                ComplainTypeID = ddlSubject1.SelectedValue;
            else if (ddlScreen.SelectedValue == "ACCIDENTTAB2")
                ComplainTypeID = "0";
            DataTable dtFieldFinal = new DataTable();
            DataTable dtFileFinal = new DataTable();
            DataTable dtComplainConfig = new DataTable();
            DataTable dtMonthlyReportConfig = new DataTable();

            //Require Field
            if ((ddlScreen.SelectedIndex == 0) || (ddlScreen.SelectedIndex == 1) || ddlScreen.SelectedValue == "ACCIDENTTAB1" || ddlScreen.SelectedValue == "ACCIDENTTAB2")
            {//Complain, Vendor
                dtFieldFinal.Columns.Add("COMPLAIN_TYPE_ID");
                dtFieldFinal.Columns.Add("CONTROL_ID");
                dtFieldFinal.Columns.Add("REQUIRE_TYPE");
                dtFieldFinal.Columns.Add("ISACTIVE");
                dtFieldFinal.Columns.Add("CREATE_BY");
                dtFieldFinal.Columns.Add("CONTROL_ID_REQ");
                dtFieldFinal.Columns.Add("FIELD_TYPE");
                dtFieldFinal.Columns.Add("DESCRIPTION");

                DropDownList ddl = new DropDownList();
                for (int i = 0; i < dtField.Rows.Count; i++)
                {
                    ddl = (DropDownList)dgvField.Rows[i].FindControl("ddlRequire");
                    dtFieldFinal.Rows.Add(ComplainTypeID
                                        , dtField.Rows[i]["CONTROL_ID"].ToString()
                                        , ddl.SelectedValue
                                        , 1
                                        , Session["UserID"].ToString()
                                        , dtField.Rows[i]["CONTROL_ID_REQ"].ToString()
                                        , ddlScreen.SelectedValue
                                        , dtField.Rows[i]["DESCRIPTION"].ToString());
                }
            }

            //Request File
            dtFileFinal.Columns.Add("COMPLAIN_TYPE_ID");
            dtFileFinal.Columns.Add("UPLOAD_ID");
            dtFileFinal.Columns.Add("ISACTIVE");
            dtFileFinal.Columns.Add("CREATE_BY");
            dtFileFinal.Columns.Add("REQUEST_TYPE");

            RadioButtonList rad = new RadioButtonList();
            for (int i = 0; i < dtFile.Rows.Count; i++)
            {
                rad = (RadioButtonList)dgvUploadFile.Rows[i].FindControl("radRequire");
                if (string.Equals(rad.SelectedValue, "1"))
                    dtFileFinal.Rows.Add(ComplainTypeID
                                       , dtFile.Rows[i]["UPLOAD_ID"].ToString()
                                       , 1
                                       , Session["UserID"].ToString()
                                       , ddlScreen.SelectedValue);
            }

            //Complain Config
            dtComplainConfig.Columns.Add("LOCK_DRIVER");
            dtComplainConfig.Columns.Add("IS_ADD_MULTIPLE_CAR");
            if (ddlScreen.SelectedIndex == 0)
                dtComplainConfig.Rows.Add(radDisableDriver.SelectedValue, radMultiplecar.SelectedValue);

            //Monthly Report Config
            dtMonthlyReportConfig.Columns.Add("START_UPLOAD_DAY");
            dtMonthlyReportConfig.Columns.Add("END_UPLOAD_DAY");
            if ((ddlScreen.SelectedIndex == 3) || (ddlScreen.SelectedIndex == 4) || (ddlScreen.SelectedIndex == 5))
                dtMonthlyReportConfig.Rows.Add(txtStartUploadDay.Text.Trim(), txtEndUploadDay.Text.Trim());

            RequireBLL.Instance.RequireInsertBLL(dtFieldFinal, dtFileFinal, ComplainTypeID, dtComplainConfig, dtMonthlyReportConfig);

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "RequireField.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlScreen.Enabled)
                throw new Exception("กรุณา กดปุ่ม ค้นหา ก่อน");

            if ((ddlScreen.SelectedIndex == 3) || (ddlScreen.SelectedIndex == 4) || (ddlScreen.SelectedIndex == 5))
            {
                if (string.Equals(txtStartUploadDay.Text, string.Empty))
                    throw new Exception("กรุณาป้อน วันที่เริ่มส่งรายงาน");

                if (string.Equals(txtEndUploadDay.Text, string.Empty))
                    throw new Exception("กรุณาป้อน วันที่สิ้นสุดส่งรายงาน");

                int tmp;
                if (!int.TryParse(txtStartUploadDay.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน วันที่เริ่มส่งรายงาน เป็นตัวเลขเท่านั้น");

                if (!int.TryParse(txtEndUploadDay.Text.Trim(), out tmp))
                    throw new Exception("กรุณาป้อน วันที่สิ้นสุดส่งรายงาน เป็นตัวเลขเท่านั้น");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("RequireField.aspx");
    }
}