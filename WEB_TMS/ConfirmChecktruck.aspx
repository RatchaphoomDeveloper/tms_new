﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="ConfirmChecktruck.aspx.cs" Inherits="ConfirmChecktruck" StylesheetTheme="Aqua" %>

<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="65%" class="style13">
                        </td>
                        <td align="right" class="style13" width="16%">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา"
                                Style="margin-left: 0px" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="9%" class="style13">
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="STRUCKID" SkinID="_gvw" Style="margin-top: 0px"
                                Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                        VisibleIndex="0">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัสรถ" FieldName="STRUCKID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO"
                                        ShowInCustomizationForm="True" VisibleIndex="3" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME"
                                        ShowInCustomizationForm="True" VisibleIndex="5" Width="25%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO"
                                        ShowInCustomizationForm="True" VisibleIndex="4" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="12" Width="8%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit0" runat="server" AutoPostBack="false" CausesValidation="False"
                                                Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" SkinID="dd">
                                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('view1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Height="16px" Url="Images/search.png" Width="16px">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                               
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT TTR.STRUCKID, TTR.SHEADREGISTERNO, TTR.STRAILERREGISTERNO, TTR.SCHASIS,tc.SCONTRACTNO,v.SVENDORNAME,
                                                                                TCT.SCONTRACTID,tc.SVENDORID,IS_YEAR
                                                                                FROM  (((TTRUCK TTR INNER JOIN TContract_Truck TCT ON TTR.STRUCKID = TCT.STRUCKID) 
                                                                                INNER JOIN   (SELECT c.STRUCKID,c.IS_YEAR FROM (TCHECKTRUCK c INNER JOIN    TCHECKTRUCKITEM ct ON c.SCHECKID =  CT.SCHECKID) INNER JOIN  TCHECKTRUCKFILE cl ON CT.SCHECKID = CL.SCHECKID   WHERE nvl(CCHECKED,0) != 1 GROUP BY c.STRUCKID,c.IS_YEAR) cc ON TTR.STRUCKID = cc.STRUCKID )
                                                                                INNER JOIN TContract TC ON TCT.SCONTRACTID = TC.SCONTRACTID) LEFT JOIN TVENDOR_SAP v ON tc.SVENDORID = v.SVENDORID
                                                                                WHERE  NVL(TC.CACTIVE,'Y') = 'Y' AND (TTR.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR TTR.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TTR.SCHASIS LIKE '%' || :oSearch || '%' OR tc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR v.SVENDORNAME LIKE '%' || :oSearch || '%') ORDER BY  v.SVENDORNAME">
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
