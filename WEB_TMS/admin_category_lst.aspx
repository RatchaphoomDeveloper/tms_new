﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_category_lst.aspx.cs" Inherits="admin_category_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>

    <style type="text/css">
        .style13
        {
            height: 23px;
        }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="75%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" 
                                NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา" Style="margin-left: 0px" 
                                Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            ค้นหาจาก&nbsp;&nbsp; </td>
                        <td width="8%" class="style13">
                            <dx:ASPxComboBox ID="cbxGroup" runat="server" Width="90px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="นิยาม" Value="0" />
                                    <dx:ListEditItem Text="กลุ่มของผลกระทบ" Value="1" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds"  SettingsPager-PageSize="10" OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption = "ที่" HeaderStyle-HorizontalAlign="Center" Width="5%" VisibleIndex="1">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                       <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="1" 
                                        FieldName="SCATEGORYID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="กลุ่มของผลกระทบ" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SCATEGORYTYPENAME" Width="23%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <DataItemTemplate>
                                           <dx:ASPxLabel ID="sCateHead" runat="server" Text='<%# (Eval("CATEHEAD").ToString() == null || Eval("CATEHEAD").ToString() == "")? "" : Eval("CATEHEAD") + "/" %>'></dx:ASPxLabel><dx:ASPxLabel ID="sCateName" runat="server" Text='<%# Eval("SCATEGORYTYPENAME") %>'></dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ตัวคูณ" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" Width="5%" FieldName="NIMPACTLEVEL" >
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ความรุนแรง<br>ของผลกระทบ" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SIMPACTLEVEL" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Category" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SCATEGORYNAME" Width="6%">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="นิยาม" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                        Width="25%" FieldName="SDEFINE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataCheckColumn VisibleIndex="6" Width="5%" Caption="สถานะ" HeaderStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'  runat="server" ReadOnly="true">
                                            </dx:ASPxCheckBox>
                                        </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                     </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="7">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>

                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT  ROW_NUMBER () OVER (ORDER BY C.SCATEGORYID) AS ID1, C.SCATEGORYID, C.SDEFINE, C.NIMPACTLEVEL, C.SIMPACTLEVEL, C.SCATEGORYNAME,(SELECT SCATEGORYTYPENAME FROM TCATEGORYTYPE WHERE SCATEGORYTYPEID = CT.SHEADTYPEID ) AS CATEHEAD,CT. SCATEGORYTYPENAME, C.CACTIVE FROM TCATEGORY C  INNER JOIN  TCATEGORYTYPE CT ON C.SCATEGORYTYPEID = CT.SCATEGORYTYPEID WHERE C.SDEFINE LIKE '%' || :oSearch || '%'"
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" 
                                DeleteCommand="DELETE FROM TCATEGORY WHERE (SCATEGORYID = :SCATEGORYID)"
                                
                                
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                               
                                <DeleteParameters>
                                   <asp:SessionParameter Name="SCATEGORYID" SessionField="delCategory" Type="String" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch"  PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
