﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ExcelReportKpi.aspx.cs" Inherits="ExcelReportKpi" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ผลประเมิน KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        ประจำปี :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblYear" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        เดือน :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblMonth" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        ชื่อแบบประเมิน :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblNameKpi" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        บริษัทผู้ขนส่ง :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblVendor" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        เลขที่สัญญา :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblContract" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</asp:Content>


