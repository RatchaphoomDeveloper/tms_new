﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;
using System.Text;
using TMS_BLL.Master;
using TMS_Entity.CarEntity;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.Report;
using EmailHelper;
public partial class truck_add : PageBase
{
    const string UploadDirectory = "UploadFile/Truck/";
    private string registerno = "";
    #region + ViewState +

    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }
    private DataTable dtUploadTypeTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTPN"] != null)
                return (DataTable)ViewState["dtUploadTypeTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTPN"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUploadTPN
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTPN"] != null)
                return (DataTable)ViewState["dtUploadTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTPN"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }
    private DataTable dtRequestFileTPN
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTPN"] != null)
                return (DataTable)ViewState["dtRequestFileTPN"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTPN"] = value;
        }
    }
    private DataTable dtHeader
    {
        get
        {
            if ((DataTable)ViewState["dtHeader"] != null)
                return (DataTable)ViewState["dtHeader"];
            else
                return null;
        }
        set
        {
            ViewState["dtHeader"] = value;
        }
    }

    private DataTable dtTopic
    {
        get
        {
            if ((DataTable)ViewState["dtTopic"] != null)
                return (DataTable)ViewState["dtTopic"];
            else
                return null;
        }
        set
        {
            ViewState["dtTopic"] = value;
        }
    }



    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private DataTable dtRequire
    {
        get
        {
            if ((DataTable)ViewState["dtRequire"] != null)
                return (DataTable)ViewState["dtRequire"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequire"] = value;
        }
    }
    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        this.Culture = "en-US";
        this.UICulture = "en-US";
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
        }
        else
        {
            CGROUP = Session["CGROUP"].ToString();
            SVDID = Session["SVDID"].ToString();
        }

        if (!IsPostBack)
        {
            this.InitialForm(CGROUP);
            string str = Request.QueryString["str"];

            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");

            dtRequestFile = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 0");
            GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);

            //dtRequestFileTPN = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 1");
            //GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);

            //dteStart.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            //dteEnd.Text = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            DataTable dtWarning = MonthlyReportBLL.Instance.TruckSelectSpecialValueBLL();
            lblWarning.Text = "หมายเหตุ ในกรณีที่ยังไม่ได้รับ ธพ.น.2 สามารถแนบ ธพ.น.3 (แบบคําขอต่ออายุใบอนุญาตประกอบกิจการควบคุมประเภทที่ ๓) ทดแทนก่อนได้ และขอความอนุเคราะห์แนบ เอกสาร ธพ.น.2 ปี " + DateTime.Now.Year.ToString() + " ก่อนวันที " + dtWarning.Rows[0]["SPECIALDATE"];

            this.AssignAuthen();
        }
        CreateHeadCapacity(int.Parse(capacityCell.SelectedValue));
        this.GenRowCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));

    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
                btnUpload.Enabled = false;
                btnCreateRow.Enabled = false;
                btncomment.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckUploadFileMV(int TruckType, DataTable dtUpload)
    {
        try
        {
            if (radSpot.SelectedIndex == 0)
            {
                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                    {
                        dtUpload.Rows.RemoveAt(i);

                        radPlaceMV.ClearSelection();
                        lblPlaceMV.Visible = false;
                        radPlaceMV.Visible = false;

                        break;
                    }
                }
            }
            else if (radSpot.SelectedIndex == 1)
            {
                if (TruckType == 2)                     //หางลาก
                {
                    for (int i = 0; i < dtUpload.Rows.Count; i++)
                    {
                        if (string.Equals(dtUpload.Rows[i]["UPLOAD_ID"].ToString(), "41"))
                        {
                            dtUpload.Rows.RemoveAt(i);

                            radPlaceMV.ClearSelection();
                            lblPlaceMV.Visible = false;
                            radPlaceMV.Visible = false;

                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ChoseTruck(object sender, EventArgs e)
    {
        //if (cmbCarcate.SelectedValue == "0" || cmbCarcate.SelectedValue == "")
        //    this.SetShaftDriven(txtRnShaftDriven, 4);
        //else
        //    this.SetShaftDrivenSemi(txtRnShaftDriven, 4);

        try
        {
            lblPlaceMV.Visible = true;
            radPlaceMV.Visible = true;

            if (cmbCarcate.SelectedValue == "0" || cmbCarcate.SelectedValue == "")
                this.SetShaftDriven(txtRnShaftDriven, 4);
            else
                this.SetShaftDriven(txtRnShaftDriven, 4);

            switch (cmbCarcate.SelectedIndex)
            {
                case 1:
                    Truck.Visible = true;
                    insurance.Visible = true;
                    carrier.Visible = true;
                    capacity.Visible = true;
                    semitruck.Visible = false;
                    document.Visible = true;
                    documentTPN.Visible = true;
                    SaveButton.Visible = true;
                    companyTank.Visible = true;

                    dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                    this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                    GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);

                    InitialUpload();

                    break;
                case 2:
                    Truck.Visible = true;
                    semitruck.Visible = false;
                    insurance.Visible = true;
                    carrier.Visible = true;
                    capacity.Visible = false;
                    document.Visible = true;
                    documentTPN.Visible = true;
                    SaveButton.Visible = true;
                    companyTank.Visible = false;

                    dtRequestFileTPN = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'TRUCK' AND NVL(M_UPLOAD_TYPE.UPLOAD_TYPE_IND, '0') = 1 AND M_UPLOAD_TYPE.UPLOAD_ID <> 344");
                    this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                    GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);

                    InitialUpload();

                    break;
                case 3:
                    semitruck.Visible = true;
                    insurance.Visible = true;
                    carrier.Visible = true;
                    capacity.Visible = true;
                    Truck.Visible = false;
                    document.Visible = true;
                    documentTPN.Visible = true;
                    SaveButton.Visible = true;

                    dtRequestFileTPN = MonthlyReportBLL.Instance.TruckTPNRequestFileBLL();
                    this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtRequestFileTPN);
                    GridViewHelper.BindGridView(ref dgvRequestFileTPN, dtRequestFileTPN);

                    InitialUpload();

                    break;
                default:
                    semitruck.Visible = false;
                    insurance.Visible = false;
                    carrier.Visible = false;
                    capacity.Visible = false;
                    Truck.Visible = false;
                    document.Visible = false;
                    documentTPN.Visible = true;
                    SaveButton.Visible = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSaveTab1_Click(object sender, EventArgs e)
    {
        try
        {
            #region Validate Data

            if (radSpot.SelectedIndex < 0)
                throw new Exception("กรุณาเลือก ประเภทการจ้างงาน (สัญญา, จ้างพิเศษ) !!!");

            if (radSpot.SelectedValue == "1")
            {
                if (cmbCarcate.SelectedIndex != 2)
                {
                    if (radPlaceMV.SelectedIndex < 0)
                        throw new Exception("กรุณาเลือก สถานที่วัดน้ำ !!!");
                }
            }

            this.ValidateCapacity(int.Parse(ddl_addition.SelectedValue), int.Parse(capacityCell.SelectedValue));
            this.ValidateUploadFile();
            #endregion
            #region บันทึกเกี่ยวกับหัวรถ
            DataTable retrunValue = CarBLL.Instance.TruckSave(cmbCarcate.SelectedValue.ToString(), DataToDatatable(), dtUpload, dtUploadTPN, DataInsurance(), this.DataCapacity(int.Parse(capacityCell.SelectedValue)), radSpot.SelectedValue);
            #endregion
            #region บันทึก Log
            LOG.Instance.SaveLog("เพิ่มข้อมูลรถทะเบียน", registerno, Session["UserID"].ToString(), "<span class=\"green\">" + registerno + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())) + "</span>", "");
            #endregion

            if (string.Equals(CGROUP, "1"))
            {
                if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        SystemFunction.AddToTREQ_DATACHANGE(retrunValue.Rows[0][1].ToString(), "T", Session["UserID"] + "", "0", registerno + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())), string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID, "Y");
                        //ส่งชนิดรถเข้าอีเมลล์บอกว่าเป็นหัวลาก หางลาก รถเดี่ยว
                        registerno += LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString()));
                        this.SendEmail(ConfigValue.EmailTruckAdd, retrunValue.Rows[0][1].ToString());
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                    }
                    catch (Exception)
                    {

                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ", "truck_info.aspx");

                    }
                    finally
                    {
                        if (radSpot.SelectedValue == "1")           //SPOT
                            this.AutoApprove(retrunValue.Rows[0][1].ToString());
                    }

                    //บันทึกข้อมูลสำเร็จ
                }
                else
                {
                    DataTable retrunValue2 = CarBLL.Instance.DeleteTruckBLL(registerno.ToString() + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())));
                    if (string.Equals(retrunValue2.Rows[0][0].ToString(), "01"))
                    {
                        retrunValue = new DataTable();
                        retrunValue = CarBLL.Instance.TruckSave(cmbCarcate.SelectedValue.ToString(), DataToDatatable(), dtUpload, dtUploadTPN, DataInsurance(), this.DataCapacity(int.Parse(capacityCell.SelectedValue)), radSpot.SelectedValue);
                        if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                        {
                            SystemFunction.AddToTREQ_DATACHANGE(retrunValue.Rows[0][1].ToString(), "T", Session["UserID"] + "", "0", registerno + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())), string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID, "Y");
                            //ส่งชนิดรถเข้าอีเมลล์บอกว่าเป็นหัวลาก หางลาก รถเดี่ยว
                            registerno += LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString()));

                            if (radSpot.SelectedValue == "1")           //SPOT
                                this.AutoApprove(retrunValue.Rows[0][1].ToString());

                            this.SendEmail(ConfigValue.EmailTruckAdd, retrunValue.Rows[0][1].ToString());
                            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                        }
                        else
                        {
                            alertFail("มีข้อมูล \"ทะเบียนรถ\" หรือ \"CHASIS\" ซ้ำภายในระบบ");
                        }
                    }
                    else
                    {
                        alertFail("มีข้อมูล \"ทะเบียนรถ\" หรือ \"CHASIS\" ซ้ำภายในระบบ");
                    }
                }
            }
            else
            {
                if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        SystemFunction.AddToTREQ_DATACHANGE(retrunValue.Rows[0][1].ToString(), "T", Session["UserID"] + "", "0", registerno + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())), string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID, "Y");
                        //ส่งชนิดรถเข้าอีเมลล์บอกว่าเป็นหัวลาก หางลาก รถเดี่ยว
                        registerno += LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString()));
                        this.SendEmail(ConfigValue.EmailTruckAdd, retrunValue.Rows[0][1].ToString());
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ <br /> รอการอนุมัติ", "vendor_request.aspx");
                    }
                    catch (Exception)
                    {

                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ <br /> รอการอนุมัติ", "vendor_request.aspx");
                    }

                }
                else
                {
                    DataTable retrunValue2 = CarBLL.Instance.DeleteTruckBLL(registerno.ToString() + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())));
                    if (string.Equals(retrunValue2.Rows[0][0].ToString(), "01"))
                    {
                        retrunValue = new DataTable();
                        retrunValue = CarBLL.Instance.TruckSave(cmbCarcate.SelectedValue.ToString(), DataToDatatable(), dtUpload, dtUploadTPN, DataInsurance(), this.DataCapacity(int.Parse(capacityCell.SelectedValue)), radSpot.SelectedValue);
                        if (string.Equals(retrunValue.Rows[0][0].ToString(), "01"))
                        {
                            SystemFunction.AddToTREQ_DATACHANGE(retrunValue.Rows[0][1].ToString(), "T", Session["UserID"] + "", "0", registerno + LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString())), string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID, "Y");
                            //ส่งชนิดรถเข้าอีเมลล์บอกว่าเป็นหัวลาก หางลาก รถเดี่ยว
                            registerno += LangeTypeCar(int.Parse(cmbCarcate.SelectedValue.ToString()));
                            this.SendEmail(ConfigValue.EmailTruckAdd, retrunValue.Rows[0][1].ToString());
                            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                        }
                        else
                        {
                            alertFail("มีข้อมูล \"ทะเบียนรถ\" หรือ \"CHASIS\" ซ้ำภายในระบบ");
                        }
                    }
                    else
                    {
                        alertFail("มีข้อมูล \"ทะเบียนรถ\" หรือ \"CHASIS\" ซ้ำภายในระบบ");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void AutoApprove(string STTRUCKID)
    {
        try
        {
            string URL = "truck_edit.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
            Session["AutoApprove"] = true;
            Response.Redirect(URL);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateCapacity(int RowNo, int columnNo)
    {
        StringBuilder sb = new StringBuilder();
        if (columnNo > 1)
        {
            if (columnNo == 2)
            {

                for (int i = 1; i < RowNo + 1; i++)
                {

                    int capacity1 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_1")).Text.ToString());
                    int capacity2 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_2")).Text.ToString());

                    if (capacity1 < capacity2)
                    {
                        sb.Clear();
                        sb.Append("ปริมาตรช่องเติมมากกว่าช่องสูงสุด");
                    }
                }
            }
            else
            {
                for (int i = 1; i < RowNo + 1; i++)
                {
                    int capacity1 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_1")).Text.ToString());
                    int capacity2 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_2")).Text.ToString());
                    int capacity3 = this.NullToInt(((TextBox)FindControlRecursive(Page, "TxT" + i + "_3")).Text.ToString());

                    if (capacity1 < capacity2 || capacity2 > capacity3 || capacity1 < capacity3)
                    {
                        sb.Clear();
                        sb.Append("ปริมาตรช่องเติมมากกว่าช่องสูงสุด");
                    }
                }
            }
            if (!string.IsNullOrEmpty(sb.ToString()))
                throw new Exception(sb.ToString());

        }
    }

    private void InitialForm(string CGROUP)
    {
        try
        {
            switch (CGROUP)
            {

                case "0":
                    DataTable dt_vendor = VendorBLL.Instance.SelectName(SVDID);
                    NameVendor.Text = dt_vendor.Rows[0]["SABBREVIATION"].ToString();
                    STRANSPORTID.Visible = false;
                    btnSave.Text = "บันทึกและส่งอนุมัติ";
                    break;
                case "2":
                    STRANSPORTID.Visible = true;
                    NameVendor.Visible = false;
                    break;
                case "1":
                    DataTable test = VendorBLL.Instance.TVendorSapSelect();
                    STRANSPORTID.Visible = true;
                    NameVendor.Visible = false;
                    break;
                default:
                    break;
            }
            this.InitialData();
            this.InitialUpload();

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void InitialData()
    {
        try
        {
            DataTable dt = CarBLL.Instance.LoadDataRoute();
            DataTable dt_CartypeSap = CarBLL.Instance.LoadDataCarSap();
            DataTable dt_typecar = CarBLL.Instance.LoadDataCartype();
            DataTable dt_brand = CarBLL.Instance.LoadBrand();
            DataTable dt_gps = CarBLL.Instance.LoadGps();
            // รอรับการเลือกพนักงานว่าสังกัดพนักงานอะไร
            string condition = string.Empty;
            DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
            DropDownListHelper.BindDropDownList(ref cmbCarcate, dt_typecar, "config_value", "config_name", true);
            DropDownListHelper.BindDropDownList(ref cmbHRoute, dt, "ROUTE", "DESCRIPTION", false);
            DropDownListHelper.BindDropDownList(ref cmbHTru_Cate, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
            DropDownListHelper.BindDropDownList(ref cmbCarSemiSap, dt_CartypeSap, "TYPE_ID", "TYPE_NAME", false);
            DropDownListHelper.BindDropDownList(ref txtHsModel, dt_brand, "BRANDID", "BRANDNAME", true);
            DropDownListHelper.BindDropDownList(ref txtsemimodal, dt_brand, "BRANDID", "BRANDNAME", false);
            DropDownListHelper.BindDropDownList(ref cboHGPSProvider, dt_gps, "GPSID", "GPSNAME", true);
            DropDownListHelper.BindDropDownList(ref STRANSPORTID, dt_vendor, "SVENDORID", "SABBREVIATION", true);
            this.GetCapacityNum(ref ddl_addition, 20);
            this.CalculateWheel(wheel, 26);
            this.CalculateWheel(SemiWheel, 26);

            this.SetShaftDriven(txtRwightDriven, 10);
            this.SetShaftDriven(txtSemiShaftDriven, 10);


        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {

                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileName.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

                this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Truck" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

            this.CheckRequestFile(dgvRequestFile, dtRequestFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvUploadFile.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
    }

    private void DownloadFile(string Path, string FileName, string FileNameDownload)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileNameDownload);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL2("TRUCK", "0");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUploadTypeTPN = UploadTypeBLL.Instance.UploadTypeSelectBLL3("TRUCK", "1");
        this.CheckUploadFileMV(cmbCarcate.SelectedIndex, dtUploadTypeTPN);
        DropDownListHelper.BindDropDownList(ref ddlUploadTypeTPN, dtUploadTypeTPN, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");

        dtUploadTPN = new DataTable();
        dtUploadTPN.Columns.Add("UPLOAD_ID");
        dtUploadTPN.Columns.Add("UPLOAD_NAME");
        dtUploadTPN.Columns.Add("FILENAME_SYSTEM");
        dtUploadTPN.Columns.Add("FILENAME_USER");
        dtUploadTPN.Columns.Add("FULLPATH");
        dtUploadTPN.Columns.Add("START_DATE");
        dtUploadTPN.Columns.Add("STOP_DATE");
        dtUploadTPN.Columns.Add("DOC_NUMBER");
        dtUploadTPN.Columns.Add("YEAR");
    }
    #endregion
    #region Miscellaneous ฟังชันเปลี่ยนค่า
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    private string RadioToTextBox(string p, string Returnstring)
    {
        if (string.Equals(p, "3"))
        {
            return Returnstring;
        }
        else
        {
            return p;
        }
    }

    private string nullvalue(string p)
    {
        if (string.Equals(p, ""))
        {
            return "0";
        }
        else
        {
            return p;
        }

    }
    //ฟังชันล้อ
    private void CalculateWheel(DropDownList dropdown, Int32 max_wheel)
    {
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 6; i <= max_wheel; i += 2)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    //สร้างแถว
    public void GetCapacityNum(ref DropDownList Name, int Num)
    {
        int count = 20;
        for (int i = 1; i < count + 1; i++)
        {

            Name.Items.Add(i.ToString());

        }

    }
    private void SetShaftDriven(DropDownList dropdown, int Num)
    {
        dropdown.Items.Clear();
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 1; i < Num; i++)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    private void SetShaftDrivenSemi(DropDownList dropdown, int Num)
    {
        dropdown.Items.Clear();
        dropdown.Items.Add(new ListItem("-กรุณาเลือก-", ""));

        for (int i = 2; i < Num; i++)
        {
            dropdown.Items.Add(i.ToString());
        }
    }
    #endregion

    #region RowAndDataCapacity สร้าง ตาราง
    #region InitalTable
    public void CreateHeadCapacity(int Columns)
    {
        if (!int.Equals(Columns, 0))
        {
            TableRow tRow;
            // Total number of rows.
            int cellCtr;
            // Current cell counter.
            int cellCnt;
            cellCnt = Columns;
            // Create a new row and add it to the table.
            tRow = new TableRow();

            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "CompartmentNo.";

            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            //กำหนดาช่องเติม
            string stringText = "";

            for (cellCtr = 1; cellCtr <= cellCnt; cellCtr++)
            {

                tCell = new TableCell();
                switch (cellCtr)
                {
                    case 1:
                        stringText = " (High) ";
                        break;
                    case 2:
                        stringText = " (Low) ";
                        break;
                    case 3:
                        stringText = " (Middle) ";
                        break;
                    default:
                        break;
                }
                label = new Label();
                // tCell = new TableCell();
                label.Text = "ระดับแป้นที่" + cellCtr.ToString() + stringText.ToString();
                tCell.Controls.Add(label);
                tCell.HorizontalAlign = HorizontalAlign.Center;
                tRow.Cells.Add(tCell);
            }
            tRow.Font.Bold = true;
            table_capacity.Rows.Add(tRow);
        }
    }
    public void GenRowCapacity(int RowsData, int ColumnData)
    {
        // Total number of rows.
        int rowCnt;
        // Current row count.
        int rowCtr;
        // Total number of cells per row (columns).
        int cellCtr;
        // Current cell counter.
        int cellCnt;
        rowCnt = RowsData;
        cellCnt = ColumnData;
        for (rowCtr = 0; rowCtr < rowCnt; rowCtr++)
        {
            // Create a new row and add it to the table.
            //No.
            TableRow tRow = new TableRow();
            TableCell tCell = new TableCell();
            Label label = new Label();
            label.Text = "ช่องที่ " + int.Parse((rowCtr + 1).ToString());
            tCell.Controls.Add(label);
            tCell.HorizontalAlign = HorizontalAlign.Center;
            tCell.Width = Unit.Pixel(150);
            tRow.Cells.Add(tCell);
            table_capacity.Rows.Add(tRow);
            for (cellCtr = 1; cellCtr < cellCnt + 1; cellCtr++)
            {

                // Create a new cell and add it to the row.
                tCell = new TableCell();
                tRow.Cells.Add(tCell);
                // Mock up a product ID.

                TextBox txt = new TextBox();
                txt.Width = 150;
                txt.EnableViewState = true;
                txt.CssClass = "placehoderKg form-control number required";
                txt.MaxLength = 5;
                tCell.HorizontalAlign = HorizontalAlign.Center;

                //txt.Text = "TxT" + (i + 1).ToString() + "_" + (j + 1).ToString();
                txt.ID = "TxT" + (rowCtr + 1).ToString() + "_" + (cellCtr).ToString();
                // pnl.Controls.Add(txt);
                tCell.Controls.Add(txt);
            }
        }

    }
    #endregion
    #region SetCapacityToDatatable
    public DataTable DataCapacity(int Columncapacity)
    {
        DataTable dt = new DataTable();
        DataTable Data_dt = new DataTable();
        // Get columns from first row
        if (table_capacity.Rows.Count > 0)
        {
            for (int i = 1; i < Columncapacity + 1; i++)
            {
                dt.Columns.Add("Column_" + i);
            }
            int rowIndex = 1;
            table_capacity.Rows.RemoveAt(0);
            foreach (TableRow row in table_capacity.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int i = 1; i < Columncapacity + 1; i++)
                {
                    dr["Column_" + i] = ((TextBox)table_capacity.FindControl("TxT" + rowIndex + "_" + i)).Text;

                }
                dt.Rows.Add(dr);
                rowIndex++;
            }

        }
        int rowsindex = 0;
        Data_dt.Columns.Add("NCOMPARTNO");
        Data_dt.Columns.Add("NPANLEVEL");
        Data_dt.Columns.Add("NCAPACITY");
        for (int row = 0; row < dt.Rows.Count; row++)
        {
            for (int column = 1; column < dt.Columns.Count + 1; column++)
            {
                string value_capacity = dt.Rows[row]["Column_" + column].ToString();
                Data_dt.Rows.Add(rowsindex + 1, int.Parse(column.ToString()), value_capacity);
            }
            rowsindex++;
        }
        return Data_dt;
    }
    #endregion
    #endregion

    #region Ajax
    [System.Web.Services.WebMethod]
    public static List<string> VendorJson(string Vendor)
    {
        DataTable dtVendor = VendorBLL.Instance.LoadNameVendor(Vendor);
        List<string> DataVendor = new List<string>();
        for (int i = 0; i < dtVendor.Rows.Count; i++)
        {
            //DataVendor.Add(dtVendor.Rows[i][0].ToString());
            DataVendor.Add(dtVendor.Rows[i][0].ToString());
        }

        return DataVendor;

    }
    #endregion
    #region Alert การผิดพลาด
    protected void CreateRowsCapacity(object sender, EventArgs e)
    {
        if (string.Equals(ddl_addition.SelectedValue, "0"))
        {
            alertFail("กรุณาเลือกช่องเติมก่อน");

        }


    }
    #endregion
    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect("truck_info.aspx");
        }
        else
        {
            Response.Redirect("Vendor_Detail.aspx");
        }
    }
    #endregion

    private void ValidateUploadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }

            StringBuilder sb2 = new StringBuilder();
            sb2.Append("<table>");
            if (dtUploadTPN.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFileTPN.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td>โปรดแนบเอกสาร</td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                    else
                    {
                        sb2.Append("<tr>");
                        sb2.Append("<td></td>");
                        sb2.Append("<td>");
                        sb2.Append(">" + dtRequestFileTPN.Rows[j]["UPLOAD_NAME"].ToString());
                        sb2.Append("</td>");
                        sb2.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFileTPN.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUploadTPN.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), dtUploadTPN.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUploadTPN.Rows.Count - 1)
                        {
                            string UploadIDTPN2 = string.Empty;
                            string UploadIDTPN3 = string.Empty;
                            bool IsFoundTPN3 = false;

                            for (int k = 0; k < dtUploadTypeTPN.Rows.Count; k++)
                            {//ถ้าไม่ได้แนบ ธพ.น.2 ให้แนบ ธพ.น.3 แทนได้
                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "1"))
                                    UploadIDTPN2 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();

                                if (string.Equals(dtUploadTypeTPN.Rows[k]["SPECIAL_FLAG"].ToString(), "2"))
                                    UploadIDTPN3 = dtUploadTypeTPN.Rows[k]["UPLOAD_ID"].ToString();
                            }

                            if (string.Equals(dtRequestFileTPN.Rows[i]["UPLOAD_ID"].ToString(), UploadIDTPN2))
                            {
                                for (int m = 0; m < dtUploadTPN.Rows.Count; m++)
                                {
                                    if (string.Equals(UploadIDTPN3, dtUploadTPN.Rows[m]["UPLOAD_ID"].ToString()))
                                    {
                                        IsFoundTPN3 = true;
                                        break;
                                    }
                                }
                            }

                            if (!IsFoundTPN3)
                            {
                                if (string.Equals(sb2.ToString(), "<table>"))
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td>โปรดแนบเอกสาร</td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                                else
                                {
                                    sb2.Append("<tr>");
                                    sb2.Append("<td></td>");
                                    sb2.Append("<td>");
                                    sb2.Append(">" + dtRequestFileTPN.Rows[i]["UPLOAD_NAME"].ToString());
                                    sb2.Append("</td>");
                                    sb2.Append("</tr>");
                                }
                            }
                        }
                    }
                }
            }
            sb2.Append("</table>");

            if (!string.Equals(sb2.ToString(), "<table></table>"))
            {
                throw new Exception(sb2.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region DataToDatatable
    #region บันทึกข้อมูลTRUCK
    protected DataTable DataToDatatable()
    {
        DataTable dt;
        switch (cmbCarcate.SelectedValue.ToString())
        {
            case "4":
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("semifirst_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("txtsemimodal");
                dt.Columns.Add("wheel");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");
                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(semifirst_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtSemiNo.Text.Trim(),
                    cmbSemiHolder.Text.Trim(),
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbCarSemiSap.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtSemiShaftDriven.SelectedValue.ToString(),
                    txtSemiChasis.Text.Trim().ToUpper(),
                    txtsemimodal.SelectedValue,
                    SemiWheel.SelectedValue,
                    txtSemiWeight.Text.ToString(),
                    txtownTank.Text.Trim().ToString(),
                    cboRTankMaterial.SelectedValue,
                    rblSemiPumpPower.SelectedValue.ToString(),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),

                    (radPlaceMV.Visible) ? radPlaceMV.SelectedValue : placewater.SelectedValue,

                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)
                    );
                registerno = txtSemiNo.Text.Trim();
                break;
            default:
                dt = new DataTable();
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("first_regis");
                dt.Columns.Add("txtHRegNo");
                dt.Columns.Add("STRANSPORTID");
                dt.Columns.Add("cmbHsHolder");
                dt.Columns.Add("cmbHRoute");
                dt.Columns.Add("txtHsEngine");
                dt.Columns.Add("cmbHTru_Cate");
                dt.Columns.Add("cboHLoadMethod");
                dt.Columns.Add("txtHsModel");
                dt.Columns.Add("txtHsChasis");
                dt.Columns.Add("cboHsVibration");
                dt.Columns.Add("wheel");
                dt.Columns.Add("cboRMaterialOfPressure");
                dt.Columns.Add("txtRnShaftDriven");
                dt.Columns.Add("txtRwightDriven");
                dt.Columns.Add("txtHPowermover");
                dt.Columns.Add("txtRnWeight");
                dt.Columns.Add("cboHPumpPower_type");
                dt.Columns.Add("txtVEH_Volume");
                dt.Columns.Add("cboHGPSProvider");
                dt.Columns.Add("txtownTank");
                dt.Columns.Add("cboRTankMaterial");
                dt.Columns.Add("rblRValveType");
                dt.Columns.Add("rblRFuelType");
                dt.Columns.Add("nslot");
                dt.Columns.Add("sessionId");
                //ข้อมูลวัดน้ำ
                dt.Columns.Add("PLACE_WATER_MEASURE");
                dt.Columns.Add("SLAST_REQ_ID");
                dt.Columns.Add("DPREV_SERV");
                dt.Columns.Add("DWATEREXPIRE");
                dt.Rows.Add(
                    int.Parse(cmbCarcate.SelectedValue.ToString()),
                    DateTime.ParseExact(first_regis.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    txtHRegNo.Text.Trim(),
                    string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID,
                    cmbHsHolder.Text.ToString(),
                    cmbHRoute.SelectedValue.ToString(),
                    txtHsEngine.Text.Trim(),
                    cmbHTru_Cate.SelectedValue.ToString(),
                    cboHLoadMethod.SelectedValue.ToString(),
                    txtHsModel.SelectedValue,
                    txtHsChasis.Text.Trim().ToUpper(),
                    cboHsVibration.SelectedValue,
                    wheel.SelectedValue,
                    cboPressure.SelectedValue.ToString(),
                    txtRnShaftDriven.SelectedValue,
                    txtRwightDriven.SelectedValue,
                    txtHPowermover.Text.Trim().ToString(),
                    int.Parse(nullvalue(txtRnWeight.Text)),
                    cboHPumpPower_type.SelectedValue.ToString(),
                    txtVEH_Volume.SelectedValue.ToString(),
                    cboHGPSProvider.SelectedValue.ToString(),
                    TruckTank.Text.Trim().ToString(),
                    materialTank.SelectedValue.ToString(),
                    RadioToTextBox(rblRValveType.Text, txtRValveType.Text),
                    RadioToTextBox(rblRFuelType.Text.ToString(), txtRFuelType.Text),
                    ddl_addition.SelectedValue.ToString(),
                    Session["UserID"].ToString(),

                    (radPlaceMV.Visible) ? radPlaceMV.SelectedValue : placewater.SelectedValue,

                    txtcodewater.Text.Trim(),
                    string.Equals(scale_start.Text, string.Empty) ? "" : DateTime.ParseExact(scale_start.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
                    string.Equals(scale_end.Text, string.Empty) ? "" : DateTime.ParseExact(scale_end.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)
                    );
                registerno = txtHRegNo.Text.Trim();
                break;
        }
        return dt;

    }
    #endregion
    #region Insurance
    protected DataTable DataInsurance()
    {
        string selectedValue = "";
        foreach (ListItem item in cbHolding.Items)
        {
            if (item.Selected)
            {
                selectedValue += "," + item.Value;
            }
        }
        if (selectedValue.Trim() != "")
            selectedValue = selectedValue.Substring(1);
        DataTable dt = new DataTable();
        dt.Columns.Add("SCOMPANY");
        dt.Columns.Add("SDETAIL");
        dt.Columns.Add("INSURE_BUDGET");
        dt.Columns.Add("INSURE_TYPE");
        dt.Columns.Add("HOLDING");
        dt.Columns.Add("NINSURE");
        dt.Columns.Add("START_DURATION");
        dt.Columns.Add("END_DURATION");
        dt.Rows.Add(
            company_name.Text.Trim(), detailinsurance.Text.Trim(), txtbudget.Text.Trim(), ddlTypeInsurance.SelectedValue
            , selectedValue, txtninsure.Text.Trim(),
            string.Equals(start_duration.Text, string.Empty) ? "" : DateTime.ParseExact(start_duration.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture),
            string.Equals(end_duration.Text, string.Empty) ? "" : DateTime.ParseExact(end_duration.Text.Trim().ToString(), "dd/mm/yyyy", CultureInfo.InvariantCulture).ToString("yyyy/mm/dd", CultureInfo.InvariantCulture)
       );
        return dt;
    }
    #endregion
    #endregion
    #region SentMail
    private void SendEmail(int TemplateID, string STTRUCKID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                Subject = Subject.Replace("{regisNo}", registerno);
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailTruckAdd)
                {
                    string MonthName1 = string.Empty;
                    string MonthName2 = string.Empty;
                    string MonthName3 = string.Empty;
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "truck_edit.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(cmbCarcate.SelectedValue.ToString());
                    //string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "truck_edit.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("str"+"&" + STTRUCKID + "&" + "" + "&" + cmbCarcate.SelectedValue.ToString()));
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                    //MailService.SendMail("apipat.k@pttor.com,nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), Session["SVDID"].ToString(), true, false), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), Session["SVDID"].ToString(), true, false), Subject, Body, "", "VendorEmployeeAdd", ColumnEmailName);

                }
            }
        }
        catch (Exception ex)
        {
            alertFail("error ส่งเมลล์" + ex.Message);
        }
    }
    #endregion
    #region Miscellaneous
    protected string LangeTypeCar(int type_num)
    {
        switch (type_num)
        {
            case 0:
                return " (รถเดี่ยว)";

            case 3:
                return " (หัวลาก)";
            case 4:
                return " (หางลาก) ";

            default:
                return " (รถเดี่ยว)";

        }
    }
    private int NullToInt(string value)
    {
        if (string.Equals(value.ToString(), string.Empty))
        {
            return 0;
        }
        else
        {
            return int.Parse(value.ToString());
        }
    }
    #endregion
    #region Class
    //class TruckLogAdd
    //{


    //    public void Addlog()
    //    {
    //        LOG.Instance.SaveLog
    //        Log.SaveLog(string headder, string regisno, string user_id, string newdata, string olddata)
    //    }

    //}

    #endregion



    protected void dgvUploadFileTPN_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUploadTPN.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

            this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string FullPath = dgvUploadFileTPN.DataKeys[e.RowIndex]["FULLPATH"].ToString();
            string FileName = dgvUploadFileTPN.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
            this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath), FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFileTPN_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnUploadTPN_Click(object sender, EventArgs e)
    {
        this.StartUploadTPN();
    }

    private void StartUploadTPN()
    {
        try
        {
            if (fileUploadTPN.HasFile)
            {

                if (string.Equals(txtDocUploadTPN.Text.Trim(), string.Empty))
                    throw new Exception("กรุณาป้อน หมายเลขเอกสาร");

                string FileNameUser = fileUploadTPN.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                if (!string.Equals(txtSuggestFileNameTPN.Text.Trim(), System.IO.Path.GetFileNameWithoutExtension(FileNameUser)))
                    throw new Exception("ชื่อไฟล์ไม่ถูกต้อง ตามที่กำหนด");

                if (txtdateStartTPN.Enabled)
                {
                    if (string.Equals(txtdateStartTPN.Text.Trim(), string.Empty))
                        throw new Exception("กรุณาป้อน วันที่เอกสาร เริ่มต้น");
                }

                if (txtdateEndTPN.Enabled)
                {
                    if (string.Equals(txtdateEndTPN.Text.Trim(), string.Empty))
                        throw new Exception("กรุณาป้อน วันที่เอกสาร สิ้นสุด");
                }

                this.ValidateUploadFileTPN(System.IO.Path.GetExtension(FileNameUser), fileUploadTPN.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadTPN.SaveAs(Path + "\\" + FileNameSystem);

                string Year = string.Empty;
                if (ddlYearTPN.Items.Count > 0)
                    Year = ddlYearTPN.SelectedValue;

                string DateStart = string.Empty;
                if (!string.Equals(txtdateStartTPN.Text.Trim(), string.Empty))
                    DateStart = txtdateStartTPN.Text.Trim();

                string DateStop = string.Empty;
                if (!string.Equals(txtdateEndTPN.Text.Trim(), string.Empty))
                    DateStop = txtdateEndTPN.Text.Trim();

                //if (string.Equals(ddlUploadTypeTPN.SelectedValue, "41"))                //MV Doc
                //{
                //    DataTable dtMVUpload = new DataTable();
                //    dtMVUpload.Columns.Add("DOC_ID");
                //    dtMVUpload.Columns.Add("START_DATE");
                //    dtMVUpload.Columns.Add("END_DATE");

                //    dtMVUpload.Rows.Add(txtDocUploadTPN.Text.Trim(), DateStart, DateStop);
                //    Session["dtMVUpload"] = dtMVUpload;
                //}

                dtUploadTPN.Rows.Add(ddlUploadTypeTPN.SelectedValue, ddlUploadTypeTPN.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, DateStart, DateStop, txtDocUploadTPN.Text.Trim(), Year);
                GridViewHelper.BindGridView(ref dgvUploadFileTPN, dtUploadTPN);

                this.CheckRequestFile(dgvRequestFileTPN, dtRequestFileTPN, dtUploadTPN);
            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFileTPN(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void ddlUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(cmbCarcate.SelectedValue, "4"))
            {
                if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
                {
                    ddlUploadType.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }

                if (ddlUploadType.SelectedIndex < 1)
                    txtSuggestFileName.Text = string.Empty;
                else
                    txtSuggestFileName.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
            }
            else
            {
                if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
                {
                    ddlUploadType.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }

                if (ddlUploadType.SelectedIndex < 1)
                    txtSuggestFileName.Text = string.Empty;
                else
                    txtSuggestFileName.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadType.SelectedItem.ToString();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CheckRequestFile(GridView dgvRequestFile, DataTable dtRequestFile, DataTable dtUpload)
    {
        try
        {
            string UploadID = string.Empty;
            CheckBox chk = new CheckBox();

            for (int j = 0; j < dtRequestFile.Rows.Count; j++)
            {
                chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                if (chk != null)
                    chk.Checked = false;
            }

            for (int i = 0; i < dtUpload.Rows.Count; i++)
            {
                UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (string.Equals(UploadID, dtRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                    {
                        chk = (CheckBox)dgvRequestFile.Rows[j].FindControl("chkUploaded");
                        if (chk != null)
                            chk.Checked = true;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void ddlUploadTypeTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (!string.Equals(cmbCarcate.SelectedValue, "4"))
            {
                if (string.Equals(txtHRegNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }
            else
            {
                if (string.Equals(txtSemiNo.Text.Trim(), string.Empty))
                {
                    ddlUploadTypeTPN.SelectedIndex = 0;
                    throw new Exception("กรุณาป้อนทะเบียนรถก่อนแนบไฟล์เอกสาร");
                }
            }


            this.ClearUploadTPN();

            if (ddlUploadTypeTPN.SelectedIndex > 0)
            {
                if (!string.Equals(cmbCarcate.SelectedValue, "4"))
                    txtSuggestFileNameTPN.Text = txtHRegNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();
                else
                    txtSuggestFileNameTPN.Text = txtSemiNo.Text.Trim() + "_" + ddlUploadTypeTPN.SelectedItem.ToString();

                DataTable dtYear = new DataTable();
                dtYear.Columns.Add("YEAR");
                dtYear.Columns.Add("YEAR_DISPLAY");

                //if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "1"))
                //{//ธพ.น.2
                //    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                //    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                //    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                //    ddlYearTPN.SelectedIndex = 0;
                //    ddlYearTPN.Enabled = true;

                //    txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
                //    txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
                //}
                //else if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "2"))
                //{//ธพ.น.3
                //    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                //    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                //    ddlYearTPN.SelectedIndex = 0;
                //}
                //else
                //{
                //    txtdateStartTPN.Enabled = true;
                //    txtdateEndTPN.Enabled = true;
                //}

                if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "1"))
                {//ธพ.น.2
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;

                    txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
                    txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
                }
                else if (string.Equals(dtUploadTypeTPN.Rows[ddlUploadTypeTPN.SelectedIndex]["SPECIAL_FLAG"].ToString(), "2"))
                {//ธพ.น.3
                    dtYear.Rows.Add(DateTime.Now.ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.ToString("yyyy", new CultureInfo("en-US")));
                    dtYear.Rows.Add(DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")), "ปี " + DateTime.Now.AddYears(1).ToString("yyyy", new CultureInfo("en-US")));
                    DropDownListHelper.BindDropDownList(ref ddlYearTPN, dtYear, "YEAR", "YEAR_DISPLAY", false);
                    ddlYearTPN.SelectedIndex = 0;
                    ddlYearTPN.Enabled = true;
                }
                else
                {
                    txtdateStartTPN.Enabled = true;
                    txtdateEndTPN.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearUploadTPN()
    {
        try
        {
            txtSuggestFileNameTPN.Text = string.Empty;
            txtDocUploadTPN.Text = string.Empty;

            ddlYearTPN.Items.Clear();
            ddlYearTPN.Enabled = false;

            txtdateStartTPN.Text = string.Empty;
            txtdateStartTPN.Enabled = false;

            txtdateEndTPN.Text = string.Empty;
            txtdateEndTPN.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYearTPN_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtdateStartTPN.Text = "01/01/" + ddlYearTPN.SelectedValue;
            txtdateEndTPN.Text = "31/12/" + ddlYearTPN.SelectedValue;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void radSpot_SelectedIndexChanged(object sender, EventArgs e)
    {
        ChoseTruck(null, null);
    }
}