﻿<%@ Page MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true" EnableViewState="true"
    CodeFile="Quartery_Report.aspx.cs" Inherits="Quartery_Report" StylesheetTheme="Aqua" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">

    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.__OriginalDXUpdateRowCellsHandler = ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility;
        ASPxClientTableFixedColumnsHelper.prototype.ChangeCellsVisibility = function (row, startIndex, endIndex, display) {
            if ((row.cells.length == 0) || (row.cells[0].getAttribute("ci") == null))
                window.__OriginalDXUpdateRowCellsHandler(row, startIndex, endIndex - 1, display); // base call
            else {
                //custom processing
                for (var i = startIndex; i <= endIndex; i++) {
                    var cell = FindCellWithColumnIndex(row, i);
                    if (cell != null)
                        cell.style.display = display;
                }
            }
        };

        function FindCellWithColumnIndex(row, colIndex) {
            for (var i = 0; i < row.cells.length; i++) {
                if (row.cells[i].getAttribute("ci") == colIndex)
                    return row.cells[i];
            }
            return null;
        }
    </script>

    <style type="text/css">
       a[disabled] > input[type=button]{
            background-color: #fff;
            border-color: #ccc;
            cursor:not-allowed;
                color: graytext;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ค้นหา
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-4 text-right">
                        ปี :

                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-4 text-right">
                        ไตรมาส :

                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlQuerter" runat="server" CssClass="form-control">
                            <asp:ListItem Text="ทั้งหมด" Value="0" />
                            <asp:ListItem Text="ไตรมาส 1" Value="1" />
                            <asp:ListItem Text="ไตรมาส 2" Value="2" />
                            <asp:ListItem Text="ไตรมาส 3" Value="3" />
                            <asp:ListItem Text="ไตรมาส 4" Value="4" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-4 text-right">
                        ผู้ขนส่ง :

                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control">
                            
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-4 text-right">
                        หัวข้อ :

                    </div>
                    <div class="col-md-8">
                            <asp:CheckBoxList ID="chkaccident" runat="server" RepeatColumns="2">
                               
                            </asp:CheckBoxList>
                        
                    </div>
                    
                </div>
                <div class="clearfix"></div>
                <p>&nbsp;</p>
                <div class="form-group">
                    <center>
                        <asp:Button Text="Preview" CssClass="btn btn-default" ID="btnSearch" runat="server" OnClick="btnSearch_Click" UseSubmitBehavior="false" />   
                        &nbsp;&nbsp;
                        <asp:Button Text="Export PDF" CssClass="btn btn-default" ID="btnExportPDF" runat="server" OnClick="btn_ExportPDF_Click" />
                        &nbsp;&nbsp;
                        <asp:Button Text="Export Excel" ID="btnExportExcel" CssClass="btn btn-default" runat="server" OnClick="btnExportExcel_Click" />
                         &nbsp;&nbsp;
                        <a id="aQuarterResult" runat="server" href="QuarterResult.aspx" target="_blank">
                            <input type="button" name="name" class="btn btn-default" value="ดูประวัติการแจ้งผล" />
                            </a>
                         &nbsp;&nbsp;
                        <a id="aConfigQuarterReport" runat="server" href="Config_QuarterReport.aspx" target="_blank">
                            <input type="button" name="name" class="btn btn-default" value="ตั้งค่าการแสดงผลของผู้ขนส่ง" />
                            </a>
                         &nbsp;&nbsp;
                    </center>
                     
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>    
    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false"  SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" OnHtmlRowCreated="grid_HtmlRowCreated" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" KeyFieldName="SCONTRACTNO"  >
        <Columns>
            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SABBREVIATION"  >
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="กลุ่มที่" FieldName="GROUPNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 1" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER1" >
                <Columns>
                    <dx:GridViewDataColumn Caption="ม.ค."  Width="3%" FieldName="JAN" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="ก.พ."  Width="3%" FieldName="FEB" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="มี.ค." Width="3%" FieldName="MAR" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Average" FieldName="QUARTER1" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER1">
                        <DataItemTemplate>
                            <asp:Button Text='<%# Eval("VIEW1") %>' CssClass="btn btn-default" ID="btnView1" runat="server" OnClick="btnView_Click" CommandArgument='<%# "1;" + Container.VisibleIndex  %>' />   
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
           <dx:GridViewBandColumn Caption="ไตรมาส 2" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER2">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="เม.ย."  ShowInCustomizationForm="True"  Width="3%" FieldName="APR" Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="พ.ค."   Width="3%" FieldName="MAY"  Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="มิ.ย."  ShowInCustomizationForm="True" Width="3%" FieldName="JUN" Name="COL_QUARTER2" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Average"  FieldName="QUARTER2" Name="COL_QUARTER2" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER2">
                        <DataItemTemplate>
                            <asp:Button Text='<%# Eval("VIEW2") %>' CssClass="btn btn-default" ID="btnView2" runat="server" OnClick="btnView_Click" CommandArgument='<%# "2;" + Container.VisibleIndex  %>' />   
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 3" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER3">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ก.ค."  ShowInCustomizationForm="True" Width="3%" FieldName="JUL" Name="COL_QUARTER3" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ส.ค."  ShowInCustomizationForm="True" Width="3%" FieldName="AUG" Name="COL_QUARTER3" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ก.ย."  ShowInCustomizationForm="True" Width="3%" FieldName="SEP" Name="COL_QUARTER3" >
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Average"  FieldName="QUARTER3" Name="COL_QUARTER3">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER3">
                        <DataItemTemplate>
                            <asp:Button Text='<%# Eval("VIEW3") %>' CssClass="btn btn-default" ID="btnView3" runat="server" OnClick="btnView_Click" CommandArgument='<%# "3;" + Container.VisibleIndex  %>' />   
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 4" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER4">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ต.ค."  ShowInCustomizationForm="True" Width="3%" FieldName="OCT" Name="COL_QUARTER4" >
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="พ.ย."  ShowInCustomizationForm="True"    Width="3%" FieldName="NOV" Name="COL_QUARTER4" >
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ธ.ค."  ShowInCustomizationForm="True" Width="3%" FieldName="DEC" Name="COL_QUARTER4" >
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Average" FieldName="QUARTER4" Name="COL_QUARTER4">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn> 
                    <dx:GridViewDataColumn Name="COL_QUARTER4">
                        <DataItemTemplate>
                            <asp:Button Text='<%# Eval("VIEW4") %>' CssClass="btn btn-default" ID="btnView4" runat="server" OnClick="btnView_Click" CommandArgument='<%# "4;" + Container.VisibleIndex  %>' />   
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>                                           <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTID" Visible="false">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>    
            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORID" Visible="false">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="" FieldName="ID1" Visible="false">
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="" FieldName="ID2" Visible="false">
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="" FieldName="ID3" Visible="false">
            </dx:GridViewDataTextColumn> 
            <dx:GridViewDataTextColumn Caption="" FieldName="ID4" Visible="false">
            </dx:GridViewDataTextColumn>                      
        </Columns>
        <Paddings />
    </dx:ASPxGridView>
</asp:Content>
