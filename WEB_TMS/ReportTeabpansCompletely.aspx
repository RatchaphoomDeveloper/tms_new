﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportTeabpansCompletely.aspx.cs" Inherits="ReportTeabpansCompletely" %>

<%@ Register Assembly="DevExpress.XtraCharts.v11.2.Web, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="72%" align="right">
                                                    <dx:ASPxComboBox ID="cboVendor" runat="server" Width="40%" ClientInstanceName="cboVendor"
                                                        TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="True"
                                                        OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" ValueField="SVENDORID" AutoPostBack="false">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                    </asp:SqlDataSource>
                                                </td>
                                                <td width="10%" align="right">
                                                    <dx:ASPxComboBox runat="server" ID="cboMonth" Width="100%">
                                                        <Items>
                                                            <dx:ListEditItem Text="มกราคม" Value="01" />
                                                            <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                                                            <dx:ListEditItem Text="มีนาคม" Value="03" />
                                                            <dx:ListEditItem Text="เมษายน" Value="04" />
                                                            <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                                                            <dx:ListEditItem Text="มิถุนายน" Value="06" />
                                                            <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                                                            <dx:ListEditItem Text="สิงหาคม" Value="08" />
                                                            <dx:ListEditItem Text="กันยายน" Value="09" />
                                                            <dx:ListEditItem Text="ตุลาคม" Value="10" />
                                                            <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                                                            <dx:ListEditItem Text="ธันวาคม" Value="12" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="2%">-</td>
                                                <td width="8%">
                                                    <dx:ASPxComboBox runat="server" ID="cboYear" Width="100%">
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="8%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(){ xcpn.PerformCallback('search');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานการออกใบเทียบแป้น ช่วงวันที่"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text="" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="37%"></td>
                                                <td width="37%"></td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnPDF" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="PDF" OnClick="btnPDF_Click">
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnExcel" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="Excel" OnClick="btnExcel_Click">
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--    <dx:ASPxTextBox runat="server" ID="txtStatus" ClientInstanceName="txtStatus" ClientVisible="false"></dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtReqID" ClientInstanceName="txtReqID" ClientVisible="false"></dx:ASPxTextBox>
                                        <dx:ASPxButton runat="server" ID="btnReport" ClientInstanceName="btnReport" OnClick="btnReport_Click" ClientVisible="false"></dx:ASPxButton>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <%--  <dx:ReportViewer ID="rvw" runat="server" ClientInstanceName="rvw" Width="100%">
                                        </dx:ReportViewer>--%>
                                        <dx:ASPxTextBox runat="server" ID="txtSeriesName" ClientInstanceName="txtSeriesName"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtargument" ClientInstanceName="txtargument"
                                            ClientVisible="false">
                                        </dx:ASPxTextBox>
                                        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" ClientInstanceName="WebChartControl1"
                                            Height="400px" Width="900px">
                                            <fillstyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</fillstyle>
                                            <seriestemplate><ViewSerializable>
<cc1:SideBySideBarSeriesView></cc1:SideBySideBarSeriesView>
</ViewSerializable>
<LabelSerializable>
<cc1:SideBySideBarSeriesLabel LineVisible="True">
<FillStyle><OptionsSerializable>
<cc1:SolidFillOptions></cc1:SolidFillOptions>
</OptionsSerializable>
</FillStyle>
<PointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</PointOptionsSerializable>
</cc1:SideBySideBarSeriesLabel>
</LabelSerializable>
<LegendPointOptionsSerializable>
<cc1:PointOptions></cc1:PointOptions>
</LegendPointOptionsSerializable>
</seriestemplate>
                                            <clientsideevents objectselected="function(s, e) {
		 if (e.hitInfo.inSeries) {
       var obj = e.additionalHitObject;
        txtSeriesName.SetText(e.additionalHitObject.series.name);
      txtargument.SetText(e.hitInfo.seriesPoint.argument);
        btnReport2.DoClick();
       if (obj != null){
           pcX = e.absoluteX;
           pcY = e.absoluteY;

           //cbp.PerformCallback(obj.argument);
       }
    }
}" />
                                        </dxchartsui:WebChartControl>
                                        <dx:ASPxButton runat="server" ID="btnReport2" ClientInstanceName="btnReport2" AutoPostBack="false"
                                            ClientVisible="false">
                                            <ClientSideEvents Click=" function(){gvw.PerformCallback('ListData;');}" />
                                        </dx:ASPxButton>
                                    </td>
                                    <%-- <td>
                                        <dx:ASPxLabel runat="server" ID="lblTitle1" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblTitle2" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblTitle3" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" AutoGenerateColumns="false"
                                            ClientInstanceName="gvw" OnAfterPerformCallback="gvw_AfterPerformCallback">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันที่ รข.แจ้ง" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="APPROVE_DATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="หมดอายุวัดน้ำ" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="WATER_EXPIRE_DATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันที่วัดน้ำ" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="SERVICE_DATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ข่นส่ง" Width="18%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="SABBREVIATION">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทรถ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="CARCATE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทคำขอ" Width="18%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="CAUSE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สาเหตุ" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="CAUSE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
