﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="contract_info.aspx.cs"
    Inherits="contract_info" StylesheetTheme="Aqua" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td>
                                        ค้นหาตามบริษัทผู้ขนส่ง: </td>
                                    <td style="white-space: nowrap;">
                                        <dx:ASPxComboBox ID="cmbVendor" runat="server"  ClientInstanceName="cmbVendor" 
                                            SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID" Width="200px">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="360px" />
                                            </Columns>
                                        </dx:ASPxComboBox>
                                        <asp:SqlDataSource ID="sdsVendor" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                            CacheKeyDependency="ckdVendor" SelectCommand="SELECt * FROM TVENDOR WHERE NVL(CACTIVE,'1')='1'">
                                        </asp:SqlDataSource>
                                    </td>
                                    <td>
                                        เลขที่สัญญา: </td>
                                    <td style="white-space: nowrap;">
                                        <dx:ASPxTextBox ID="txtContractNo" runat="server" ClientInstanceName="txtContractNo" NullText="เลขที่สัญญา ,รหัสสัญญา"
                                            Width="160px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        สถานะสัญญา: </td>
                                    <td style="white-space: nowrap;">
                                        <%--<dx:ASPxRadioButtonList ID="rblCactive" runat="server" ClientInstanceName="rblCACTIVE" RepeatColumns="2">
                                                    <Items>
                                                        <dx:ListEditItem Text="ใช้งาน" Value="Y" Selected="true" />
                                                        <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="N" />
                                                    </Items>
                                                    <Border BorderWidth="0px" />
                                                </dx:ASPxRadioButtonList>--%>
                                        <asp:RadioButtonList ID="radActive" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="ใช้งาน" Value="Y" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="ยกเลิกใช้งาน" Value="N"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search" OnClick="btnSearch_Click">
                                       
                                        </dx:ASPxButton>
                                    </td>

                                    <td>
                                        <dx:ASPxButton ID="btnAddContract" ClientInstanceName="btnAddContract" runat="server" OnClick="btnAddContract_Click" SkinID="_addcontract">
                                        </dx:ASPxButton>
                                    </td>
                                    <td>

                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        ประเภทสัญญาจ้าง: </td>
                                    <td style="white-space: nowrap;">
                                        <asp:RadioButtonList ID="radSpot" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                    </td>
                                    <td>
                                        
                                    </td>

                                    <td>
                                        
                                    </td>
                                    <td>

                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <asp:GridView runat="server" ID="gvwContract" AutoGenerateColumns="false" OnRowDataBound="gvwContract_RowDataBound" CssClass="table table-striped table-bordered" AllowPaging="True" HeaderStyle-CssClass="GridColorHeader"
                             ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="gvwContract_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="ที่">
                                       <ItemTemplate>
                                           <%# Container.DataItemIndex + 1 %>.
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SABBREVIATION" HeaderText="ผู้ขนส่ง" />
                                    <asp:TemplateField HeaderText="เลขที่สัญญา">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="hlView" Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SCONTRACTTYPENAME" HeaderText="กลุ่มงานขนส่ง" />
                                    <asp:BoundField DataField="IS_SPACIALCONTRAC" HeaderText="การจัดจ้าง" />
                                    <asp:BoundField DataField="WORKGROUPNAME" HeaderText="กลุ่มงาน" />
                                    <asp:BoundField DataField="GROUPNAME" HeaderText="กลุ่มที่" />
                                    <asp:BoundField DataField="DBEGIN" HeaderText="เริ่มต้นสัญญา" Visible="false" />
                                     <asp:BoundField DataField="DEND" HeaderText="สิ้นสุดสัญญา" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>  
                                    <div class="col-sm-1">
                                    <asp:ImageButton CssClass=""  ID="imgEdit" runat="server"  ImageUrl="~/Images/edit.png" Style="cursor: pointer" CommandArgument='<%# Eval("SCONTRACTID")+"_"+Eval("SVENDORID") %>' OnClick="imgEdit_Click" /></div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                </Columns>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            </asp:GridView>
                            
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
</ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
