﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;

public partial class accident_check : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/Accident/{0}/{2}/{1}/";

    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            txtUserCreate.Text = Session["UserName"] + "";
            dteDateAccident.Value = DateTime.Now;
            dteDateNotify.Value = DateTime.Now;

            Session["addSACCIDENTID"] = null;
            if (Session["oSACCIDENTID"] != null)
            {
                Session["addSACCIDENTID"] = Session["oSACCIDENTID"];
                BindData();
            }
        }
    }


    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void ClearControl()
    {

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "Save":

                if (Session["addSACCIDENTID"] != null)
                {
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();

                        string strsql = @"UPDATE TACCIDENT SET SPRODUCTTYPEID = :SPRODUCTTYPEID,SVENDORID = :SVENDORID,SHEADREGISTERNO = :SHEADREGISTERNO,STRAILERREGISTERNO = :STRAILERREGISTERNO,SDELIVERYNO = :SDELIVERYNO,SCONTRACTID = :SCONTRACTID,STRUCKID = :STRUCKID,NOILVALUE = :NOILVALUE,STRANSPORTINVOICE = :STRANSPORTINVOICE,STRANSPORTDETAIL = :STRANSPORTDETAIL,DUPDATE = sysdate,SUPDATE = :SUPDATE WHERE SACCIDENTID = :SACCIDENTID";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            int num = 0;
                            double doublenum = 0.0;
                            com.Parameters.Clear();
                            com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                            com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = rblTypeProduct.Value;
                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                            com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value;
                            com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                            com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = txtContractID.Text;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                            com.Parameters.Add(":NOILVALUE", OracleType.Number).Value = Int32.TryParse(txtOilValue.Text, out num) ? num : 0;
                            com.Parameters.Add(":STRANSPORTINVOICE", OracleType.VarChar).Value = txtTransportNO.Text;
                            com.Parameters.Add(":STRANSPORTDETAIL", OracleType.VarChar).Value = txtTransportDetail.Text;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();

                        }
                    }
                }
                else
                {

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();
                        string GenID = CommonFunction.Gen_ID(con, "SELECT SACCIDENTID FROM (SELECT SACCIDENTID FROM TACCIDENT ORDER BY SACCIDENTID DESC)  WHERE ROWNUM <= 1");

                        string strsql = @"INSERT INTO TACCIDENT(SACCIDENTID,SPRODUCTTYPEID,SVENDORID,SHEADREGISTERNO,STRAILERREGISTERNO,SDELIVERYNO,SCONTRACTID,STRUCKID,NOILVALUE,STRANSPORTINVOICE,STRANSPORTDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE,CSENDBY) 
                                      VALUES(:SACCIDENTID,:SPRODUCTTYPEID,:SVENDORID,:SHEADREGISTERNO,:STRAILERREGISTERNO,:SDELIVERYNO,:SCONTRACTID,:STRUCKID,:NOILVALUE,:STRANSPORTINVOICE,:STRANSPORTDETAIL,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,'2')";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {

                            int num = 0;
                            double doublenum = 0.0;
                            com.Parameters.Clear();
                            com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = rblTypeProduct.Value;
                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                            com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value;
                            com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                            com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = txtContractID.Text;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                            com.Parameters.Add(":NOILVALUE", OracleType.Number).Value = Int32.TryParse(txtOilValue.Text, out num) ? num : 0;
                            com.Parameters.Add(":STRANSPORTINVOICE", OracleType.VarChar).Value = txtTransportNO.Text;
                            com.Parameters.Add(":STRANSPORTDETAIL", OracleType.VarChar).Value = txtTransportDetail.Text;
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();

                        }

                        Session["addSACCIDENTID"] = GenID;
                    }
                }



                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");


                break;

            case "CheckScore":
                double num1 = 0.0;
                double sum1 = 0.0;
                double c2 = double.TryParse("" + cmbOrganizationEffect.Value, out num1) ? num1 : 0;
                double c3 = double.TryParse("" + cmbDriverEffect.Value, out num1) ? num1 : 0;
                double c4 = double.TryParse("" + cmbValueEffect.Value, out num1) ? num1 : 0;
                double c6 = double.TryParse("" + cmbEnvironmentEffect.Value, out num1) ? num1 : 0;
                sum1 = c2 * c3 * c4 * c6;

                txtScoreSubtract.Text = CommonFunction.Get_Value(sql, "SELECT NSCORE FROM LSTMULTIPLEIMPACT WHERE " + sum1 + " BETWEEN NSTART AND CASE WHEN NEND = 0 THEN " + sum1 + " ELSE NEND END");
                break;
            case "GetShipment":
                txtShipment.Text = CommonFunction.Get_Value(sql, "SELECT SHIPMENT_NO FROM TSHIPMENT WHERE DELIVERY_NO = '" + cboDelivery.Value + "'");
                break;
            case "Save1":

                if (Session["addSACCIDENTID"] != null)
                {
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();

                        string strsql = @"UPDATE TACCIDENT SET DACCIDENT = :DACCIDENT,TACCIDENTTIME = :TACCIDENTTIME,SEVENT = :SEVENT,SCAUSE = :SCAUSE,SLOWER = :SLOWER,DDATENOTIFY = :DDATENOTIFY,TTIMENOTIFY = :TTIMENOTIFY,SCITIZENID = :SCITIZENID,SEMPLOYEEID = :SEMPLOYEEID,SEMPLOYEE = :SEMPLOYEE,SLITIGANT = :SLITIGANT,CNOLITIGANT = :CNOLITIGANT,STOPIC = :STOPIC,STYPEACCIDENT = :STYPEACCIDENT,SADDRESS = :SADDRESS,STYPEEVENT = :STYPEEVENT,STYPEOPERATE = :STYPEOPERATE,SPRIMARYOPERATE = :SPRIMARYOPERATE,SACCOMPLICE = :SACCOMPLICE,SCAUSEOTHER = :SCAUSEOTHER,DUPDATE = sysdate,SUPDATE = :SUPDATE WHERE SACCIDENTID = :SACCIDENTID";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            int num = 0;
                            double doublenum = 0.0;
                            com.Parameters.Clear();
                            com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                            com.Parameters.Add(":DACCIDENT", OracleType.DateTime).Value = dteDateAccident.Value;
                            com.Parameters.Add(":TACCIDENTTIME", OracleType.VarChar).Value = tdeAcccidentTime.Text;
                            com.Parameters.Add(":SEVENT", OracleType.VarChar).Value = txtDetail.Text;
                            com.Parameters.Add(":SCAUSE", OracleType.VarChar).Value = cbxCause.Value + "";
                            com.Parameters.Add(":SLOWER", OracleType.VarChar).Value = txtLowStandard.Text;
                            com.Parameters.Add(":DDATENOTIFY", OracleType.DateTime).Value = dteDateNotify.Value;
                            com.Parameters.Add(":TTIMENOTIFY", OracleType.VarChar).Value = dteTimeNotify.Text;
                            com.Parameters.Add(":SCITIZENID", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                            com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = txtEmployeeID.Text;
                            com.Parameters.Add(":SEMPLOYEE", OracleType.VarChar).Value = txtEmployee.Text;
                            com.Parameters.Add(":SLITIGANT", OracleType.VarChar).Value = txtLitigant.Text;
                            com.Parameters.Add(":CNOLITIGANT", OracleType.Char).Value = (chkNOLitigant.Checked) ? '1' : '0';
                            com.Parameters.Add(":STOPIC", OracleType.VarChar).Value = cmbTopic.Value + "";
                            com.Parameters.Add(":STYPEACCIDENT", OracleType.VarChar).Value = cmbTypeAccident.Value + "";
                            com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                            com.Parameters.Add(":STYPEEVENT", OracleType.VarChar).Value = cmbTypeEvent.Value + "";
                            com.Parameters.Add(":STYPEOPERATE", OracleType.VarChar).Value = cmbTypeOperate.Value + "";
                            com.Parameters.Add(":SPRIMARYOPERATE", OracleType.VarChar).Value = txtPrimaryOperate.Text;
                            com.Parameters.Add(":SACCOMPLICE", OracleType.VarChar).Value = txtAccomplice.Text;
                            com.Parameters.Add(":SCAUSEOTHER", OracleType.VarChar).Value = txtCauseOther.Text;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();

                        }

                        using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID = '" + Session["addSACCIDENTID"] + "'",con))
                        {
                            comDelfile.ExecuteNonQuery();
                        }

                        string strsql1 = "INSERT INTO TACCIDENTIMPORTFILE(NID,SACCIDENTID,SDOCUMENT,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES (:NID,:SACCIDENTID,:SDOCUMENT,:SFILENAME,'',:SFILEPATH,:SCREATE,SYSDATE)";

                        string[] sDucumentName = new string[6];
                       
                                sDucumentName[0] = "บันทึกแจ้งความ หรือใบเครมประกัน";
                                sDucumentName[1] = "แผนที่เกิดเหตุ และรูปถ่าย(สถานที่เกิดเหตุ/รูปรถที่เกิดเหตุ)";
                                sDucumentName[2] = "ผลตรวจสารเสพติด";
                                sDucumentName[3] = "สรุปข้อมูล GPS";
                                sDucumentName[4] = "ใบกำกับการขนส่ง";
                                sDucumentName[5] = "เอกสารอื่นๆ";
                        
                        for (int i = 0; i < 6; i++)
                        {
                            using (OracleCommand com1 = new OracleCommand(strsql1, con))
                            {

                                ASPxTextBox txtFileName = (ASPxTextBox)ASPxPageControl1.FindControl("txtFileName" + i);
                                ASPxTextBox txtFilePath = (ASPxTextBox)ASPxPageControl1.FindControl("txtFilePath" + i);

                                if (txtFileName != null && txtFilePath != null)
                                {
                                    if ("" + txtFilePath.Text != "")
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NID", OracleType.Number).Value = i + 1;
                                        com1.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                                        com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                        com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                        com1.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[i];
                                        com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }
                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                        VisibleControlUpload();
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลทั่วไปก่อน');");
                }


                break;

            case "Save2":

                if (Session["addSACCIDENTID"] != null)
                {
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();

                        string strsql = @"UPDATE TACCIDENT SET CFAULT = :CFAULT, CUSE = '1', NSCORESUBTRACT = :NSCORESUBTRACT, DUPDATE = sysdate,SUPDATE = :SUPDATE,NVALUE = :NVALUE,NDRIVER = :NDRIVER,NVICTIMS = :NVICTIMS,SPRODUCT = :SPRODUCT,NAMOUNT = :NAMOUNT,SUNIT = :SUNIT,NORGANIZATIONEFFECT = :CORGANIZATIONEFFECT,NDRIVEREFFECT = :CDRIVEREFFECT,NVALUEEFFECT = :CVALUEEFFECT,NENVIRONMENTEFFECT = :CENVIRONMENTEFFECT,NFORECASTDAMAGE = :NFORECASTDAMAGE,NPENALTY = :NPENALTY,SBEGINHUMAN = :SBEGINHUMAN,SBEGINTERMINAL = :SBEGINTERMINAL,SCOMMENT = :SCOMMENT,SDETAILWRONGCONTRACT = :SDETAILWRONGCONTRACT,SMANAGEANDEDIT = :SMANAGEANDEDIT,SPROTECTION = :SPROTECTION,NWRONGCONTRACTNO = :NWRONGCONTRACTNO,SCONTRACTDETAIL = :SCONTRACTDETAIL WHERE SACCIDENTID = :SACCIDENTID";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            int num = 0;
                            double doublenum = 0.0;
                            com.Parameters.Clear();
                            com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtnValue.Text, out num) ? num : 0;
                            com.Parameters.Add(":NDRIVER", OracleType.Double).Value = double.TryParse(txtnDriver.Text, out doublenum) ? doublenum : 0;
                            com.Parameters.Add(":NVICTIMS", OracleType.Double).Value = double.TryParse(txtnVictims.Text, out doublenum) ? doublenum : 0;
                            com.Parameters.Add(":SPRODUCT", OracleType.VarChar).Value = cmbsProduct.Value + "";
                            com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = Int32.TryParse(txtnAmount.Text, out num) ? num : 0;
                            com.Parameters.Add(":SUNIT", OracleType.VarChar).Value = cmbUnit.Value + "";
                            com.Parameters.Add(":CORGANIZATIONEFFECT", OracleType.Number).Value = Int32.TryParse(cmbOrganizationEffect.Value + "", out num) ? num : 0;
                            com.Parameters.Add(":CDRIVEREFFECT", OracleType.Number).Value = Int32.TryParse(cmbDriverEffect.Value + "", out num) ? num : 0;
                            com.Parameters.Add(":CVALUEEFFECT", OracleType.Number).Value = Int32.TryParse(cmbValueEffect.Value + "", out num) ? num : 0;
                            com.Parameters.Add(":CENVIRONMENTEFFECT", OracleType.Number).Value = Int32.TryParse(cmbEnvironmentEffect.Value + "", out num) ? num : 0;
                            com.Parameters.Add(":NFORECASTDAMAGE", OracleType.Number).Value = Int32.TryParse(txtForecastDamage.Text, out num) ? num : 0;
                            com.Parameters.Add(":NPENALTY", OracleType.Number).Value = Int32.TryParse(txtPenalty.Text, out num) ? num : 0;
                            com.Parameters.Add(":SBEGINHUMAN", OracleType.VarChar).Value = txtBeginHuman.Text;
                            com.Parameters.Add(":SBEGINTERMINAL", OracleType.VarChar).Value = txtBeginTerminal.Text;
                            com.Parameters.Add(":SCOMMENT", OracleType.VarChar).Value = txtComment.Text;
                            com.Parameters.Add(":SDETAILWRONGCONTRACT", OracleType.VarChar).Value = txtDetailWrongContract.Text;
                            com.Parameters.Add(":SMANAGEANDEDIT", OracleType.VarChar).Value = txtManageAndEdit.Text;
                            com.Parameters.Add(":SPROTECTION", OracleType.VarChar).Value = txtProtection.Text;
                            com.Parameters.Add(":NWRONGCONTRACTNO", OracleType.Double).Value = double.TryParse(txtWrongContractNO.Text, out doublenum) ? doublenum : 0;
                            com.Parameters.Add(":SCONTRACTDETAIL", OracleType.VarChar).Value = txtContractDetail.Text;
                            com.Parameters.Add(":NSCORESUBTRACT", OracleType.Number).Value = double.TryParse(txtScoreSubtract.Text, out doublenum) ? doublenum : 0;
                            com.Parameters.Add(":CFAULT", OracleType.Char).Value = cbxTruePK.Value + "";
                            com.ExecuteNonQuery();

                        }



                        int count = CommonFunction.Count_Value(sql, "SELECT * FROM TREDUCEPOINT WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'");
                        if (count > 0)
                        {
                            using (OracleConnection con11 = new OracleConnection(sql))
                            {
                                if (con11.State == ConnectionState.Closed) con11.Open();
                                if (cbxTruePK.Value != "0")
                                {
                                    using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET NPOINT = " + (("" + txtScoreSubtract.Text != "") ? ("" + txtScoreSubtract.Text) : "0") + " WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con11))
                                    {
                                        comUpdate.ExecuteNonQuery();
                                    }
                                }
                                else if (cbxTruePK.Value == "0")
                                {
                                    using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con11))
                                    {
                                        comUpdate.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID    ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                            if (cbxTruePK.Value != "0")
                                ReducePoint("06", "090", cbxCause.Text + " " + txtCauseOther.Text, "", "", "", "" + (("" + txtScoreSubtract.Text != "") ? ("" + txtScoreSubtract.Text) : "0"), Session["addSACCIDENTID"] + "",
                                   txtContractID.Text, txtTruckID.Text, cboHeadRegist.Value + "", "", cboTrailerRegist.Value + "", txtEmployeeID.Text);

                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลทั่วไปก่อน');");
                }

                break;

            case "Save3":

                if (Session["addSACCIDENTID"] != null)
                {
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();

                        string strsql = @"UPDATE TACCIDENT SET DUPDATE = sysdate,SUPDATE = :SUPDATE,CUSE = :CUSE,CDELIVERY = :CDELIVERY,CSTATUS = :CSTATUS,CRETURNSCORE = :CRETURNSCORE,SRETURNSCOREREMARK = :SRETURNSCOREREMARK WHERE SACCIDENTID = :SACCIDENTID";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            int num = 0;
                            double doublenum = 0.0;
                            com.Parameters.Clear();
                            com.Parameters.Add(":SACCIDENTID", OracleType.Number).Value = Session["addSACCIDENTID"] + "";
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":CUSE", OracleType.Char).Value = rblUse.Value + "";
                            com.Parameters.Add(":CDELIVERY", OracleType.Char).Value = rblDelivery.Value + "";
                            com.Parameters.Add(":CSTATUS", OracleType.Char).Value = rblStatus.Value + "";
                            com.Parameters.Add(":CRETURNSCORE", OracleType.Char).Value = (chkReturnScore.Checked) ? '1' : '0';
                            com.Parameters.Add(":SRETURNSCOREREMARK", OracleType.VarChar).Value = txtReturnScoreRemark.Text;
                            com.ExecuteNonQuery();

                        }

                        if (chkReturnScore.Checked)
                        {

                            using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                            {
                                comUpdate.ExecuteNonQuery();
                            }
                        }


                        if ("" + rblUse.Value == "0")
                        {
                            using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                            {
                                comUpdate.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '1' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                            {
                                comUpdate.ExecuteNonQuery();
                            }
                        }

                    }
                    //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO



                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='accident_lst.aspx';});");
                  
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลทั่วไปก่อน');");
                }

                break;

            case "deleteFile":

                string FilePath = paras[1];

                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                }

                string cNo = paras[2];
                if (cNo == "1")
                {
                    txtFileName0.Text = "";
                    txtFilePath0.Text = "";
                }
                else if (cNo == "2")
                {
                    txtFileName1.Text = "";
                    txtFilePath1.Text = "";
                }
                else if (cNo == "3")
                {
                    txtFileName2.Text = "";
                    txtFilePath2.Text = "";
                }
                else if (cNo == "4")
                {
                    txtFileName3.Text = "";
                    txtFilePath3.Text = "";
                }
                else if (cNo == "5")
                {
                    txtFileName4.Text = "";
                    txtFilePath4.Text = "";
                }
                else if (cNo == "6")
                {
                    txtFileName5.Text = "";
                    txtFilePath5.Text = "";
                }

                VisibleControlUpload();
                break;

        }

    }


    void listData()
    {

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT SOUTBOUNDNO,DBAN,SHEADREGISTERNO,STRAILERREGISTERNO,SREMARK,CUNBAN,DUNBAN,STIMEUNBAN,DCREATE,STERMINALID from TCARBAN WHERE NCARBANID = '" + Session["NCARBANID"] + "" + "'");
        if (dt.Rows.Count > 0)
        {

        }

    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID,SCONTRACTID,SCONTRACTNO,SVENDORID,SVENDORNAME,SCONTRACTTYPENAME 
FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME,CTT.SCONTRACTTYPENAME FROM (((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID) LEFT JOIN TCONTRACTTYPE ctt 
ON c.SCONTRACTTYPEID = ctt.SCONTRACTTYPEID
WHERE t.SHEADREGISTERNO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }

    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.STRAILERREGISTERNO) AS RN , 
T.STRAILERREGISTERNO FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID 
WHERE T.STRAILERREGISTERNO LIKE :fillter AND c.SVENDORID = :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, txtVendorID.Text);
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }

    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }


    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsTruck.SelectCommand = @"SELECT SDELIVERYNO,NVALUE,SHIPMENT_NO,SVENDORID,SHEADREGISTERNO, STRAILERREGISTERNO FROM(SELECT pl.SDELIVERYNO, p.STRAILERREGISTERNO, SUM(nvl(pl.NVALUE,0)) AS NVALUE,s.SHIPMENT_NO,p.SVENDORID,p.SHEADREGISTERNO,ROW_NUMBER()OVER(ORDER BY pl.SDELIVERYNO) AS RN 
FROM (TPlanScheduleList pl LEFT JOIN TPLANSCHEDULE p ON PL.NPLANID = P.NPLANID LEFT JOIN (SELECT SHIPMENT_NO,DELIVERY_NO FROM TSHIPMENT  GROUP BY SHIPMENT_NO,DELIVERY_NO) s ON PL.SDELIVERYNO =  S.DELIVERY_NO ) 
WHERE pl.CACTIVE = '1' AND p.CACTIVE = '1' AND pl.SDELIVERYNO LIKE :fillter AND p.SHEADREGISTERNO LIKE :fillter2
GROUP BY pl.SDELIVERYNO,s.SHIPMENT_NO,p.SVENDORID,p.SHEADREGISTERNO,p.STRAILERREGISTERNO ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();


    }
    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    private void BindData()
    {
        DataTable dt = new DataTable();
        string strsql = @"SELECT a.SACCIDENTID , a.SPRODUCTTYPEID, a.DACCIDENT, a.SVENDORID, a.SHEADREGISTERNO, a.STRAILERREGISTERNO, a.TACCIDENTTIME, a.SEVENT, a.SCAUSE, a.SLOWER, a.NVALUE, a.NDRIVER,a.NVICTIMS,  a.CPOUR, a.SPRODUCT, a.NAMOUNT, a.SUNIT, a.SDELIVERYNO, a.SCONTRACTID, a.STRUCKID, a.NOILVALUE, a.STRANSPORTDETAIL, a.STRANSPORTINVOICE, a.DDATENOTIFY, a.TTIMENOTIFY, a.SCITIZENID, a.SEMPLOYEEID, a.SEMPLOYEE, a.SLITIGANT, a.CNOLITIGANT, a.STOPIC, a.STYPEACCIDENT, a.SADDRESS, a.STYPEEVENT,
 a.STYPEOPERATE, a.SPRIMARYOPERATE, a.SACCOMPLICE, a.SCAUSEOTHER, a.CFAULT,  a.NORGANIZATIONEFFECT, a.NDRIVEREFFECT, a.NVALUEEFFECT, a.NENVIRONMENTEFFECT, a.NFORECASTDAMAGE, a.NPENALTY, a.SBEGINHUMAN, a.SBEGINTERMINAL, a.SCOMMENT, a.SDETAILWRONGCONTRACT, a.SMANAGEANDEDIT, a.SPROTECTION, a.CUSE, a.NSCORESUBTRACT, a.CDELIVERY, a.CSTATUS, a.CRETURNSCORE, a.SRETURNSCOREREMARK, a.NWRONGCONTRACTNO, a.SCONTRACTDETAIL, a.CSENDBY,C.SCONTRACTNO,VS.SVENDORNAME,
 CTT.SCONTRACTTYPENAME ,
 CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - A.DCREATE)) > 0 THEN '1' ELSE '3' END  ELSE
CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN '2' ELSE
CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN '3'  END END END  As STATUS

 FROM (((TACCIDENT a LEFT JOIN TCONTRACT c ON A.SCONTRACTID = C.SCONTRACTID)LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = VS.SVENDORID)
  LEFT JOIN TCONTRACTTYPE ctt ON C.SCONTRACTTYPEID = CTT.SCONTRACTTYPEID)
LEFT JOIN TAPPEAL ap ON A.SACCIDENTID = AP.SREFERENCEID AND AP.SAPPEALTYPE='090' WHERE a.SACCIDENTID = '" + Session["oSACCIDENTID"] + "'";
       
        dt = CommonFunction.Get_Data(sql, strsql);

        if (dt.Rows.Count > 0)
        {
            DateTime date;

            rblTypeProduct.Value = dt.Rows[0]["SPRODUCTTYPEID"] + "";
            dteDateAccident.Date = DateTime.TryParse(dt.Rows[0]["DACCIDENT"] + "", out date) ? date : DateTime.Now;
            txtVendorID.Text = dt.Rows[0]["SVENDORID"] + "";
            txtVendorName.Text = dt.Rows[0]["SVENDORNAME"] + "";
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            cboTrailerRegist.Value = dt.Rows[0]["STRAILERREGISTERNO"] + "";
            tdeAcccidentTime.Text = dt.Rows[0]["TACCIDENTTIME"] + "";
            txtDetail.Text = dt.Rows[0]["SEVENT"] + "";
            cbxCause.Value = dt.Rows[0]["SCAUSE"] + "";
            txtLowStandard.Text = dt.Rows[0]["SLOWER"] + "";
            cboDelivery.Value = dt.Rows[0]["SDELIVERYNO"] + "";
            txtContractID.Text = dt.Rows[0]["SCONTRACTID"] + "";
            txtContractNO.Text = dt.Rows[0]["SCONTRACTNO"] + "";
            txtTypeContract.Text = dt.Rows[0]["SCONTRACTTYPENAME"] + "";
            txtTruckID.Text = dt.Rows[0]["STRUCKID"] + "";
            txtOilValue.Text = dt.Rows[0]["NOILVALUE"] + "";
            txtTransportNO.Text = dt.Rows[0]["STRANSPORTINVOICE"] + "";
            txtTransportDetail.Text = dt.Rows[0]["STRANSPORTDETAIL"] + "";
            dteDateNotify.Date = DateTime.TryParse(dt.Rows[0]["DDATENOTIFY"] + "", out date) ? date : DateTime.Now;
            dteTimeNotify.Text = dt.Rows[0]["TTIMENOTIFY"] + "";
            cmbPersonalNo.Value = dt.Rows[0]["SCITIZENID"] + "";
            txtEmployeeID.Text = dt.Rows[0]["SEMPLOYEEID"] + "";
            txtEmployee.Text = dt.Rows[0]["SEMPLOYEE"] + "";
            txtLitigant.Text = dt.Rows[0]["SLITIGANT"] + "";
            chkNOLitigant.Checked = ("" + dt.Rows[0]["CNOLITIGANT"] == "1") ? true : false;
            cmbTopic.DataBind();
            cmbTopic.Value = dt.Rows[0]["STOPIC"] + "";
            cmbTypeAccident.DataBind();
            cmbTypeAccident.Value = dt.Rows[0]["STYPEACCIDENT"] + "";
            txtAddress.Text = dt.Rows[0]["SADDRESS"] + "";
            cmbTypeEvent.DataBind();
            cmbTypeEvent.Value = dt.Rows[0]["STYPEEVENT"] + "";
            cmbTypeOperate.DataBind();
            cmbTypeOperate.Value = dt.Rows[0]["STYPEOPERATE"] + "";
            txtPrimaryOperate.Text = dt.Rows[0]["SPRIMARYOPERATE"] + "";
            txtAccomplice.Text = dt.Rows[0]["SACCOMPLICE"] + "";
            txtCauseOther.Text = dt.Rows[0]["SCAUSEOTHER"] + "";
            cbxTruePK.Value = dt.Rows[0]["CFAULT"] + "";
            txtnValue.Text = dt.Rows[0]["NVALUE"] + "";
            txtnDriver.Text = dt.Rows[0]["NDRIVER"] + "";
            txtnVictims.Text = dt.Rows[0]["NVICTIMS"] + "";
            cmbsProduct.Value = dt.Rows[0]["SPRODUCT"] + "";
            txtnAmount.Text = dt.Rows[0]["NAMOUNT"] + "";
            cmbUnit.Value = dt.Rows[0]["SUNIT"] + "";
            cmbOrganizationEffect.Value = dt.Rows[0]["NORGANIZATIONEFFECT"] + "";
            cmbDriverEffect.Value = dt.Rows[0]["NDRIVEREFFECT"] + "";
            cmbValueEffect.Value = dt.Rows[0]["NVALUEEFFECT"] + "";
            cmbEnvironmentEffect.Value = dt.Rows[0]["NENVIRONMENTEFFECT"] + "";
            txtForecastDamage.Text = dt.Rows[0]["NFORECASTDAMAGE"] + "";
            txtPenalty.Text = dt.Rows[0]["NPENALTY"] + "";
            txtBeginHuman.Text = dt.Rows[0]["SBEGINHUMAN"] + "";
            txtBeginTerminal.Text = dt.Rows[0]["SBEGINTERMINAL"] + "";
            txtComment.Text = dt.Rows[0]["SCOMMENT"] + "";
            txtDetailWrongContract.Text = dt.Rows[0]["SDETAILWRONGCONTRACT"] + "";
            txtManageAndEdit.Text = dt.Rows[0]["SMANAGEANDEDIT"] + "";
            txtProtection.Text = dt.Rows[0]["SPROTECTION"] + "";
            rblUse.Value = dt.Rows[0]["CUSE"] + "";
            txtScoreSubtract.Text = dt.Rows[0]["NSCORESUBTRACT"] + "";
            rblDelivery.Value = dt.Rows[0]["CDELIVERY"] + "";
            rblStatus.Value = dt.Rows[0]["STATUS"] + "";
            chkReturnScore.Checked = ("" + dt.Rows[0]["CRETURNSCORE"] == "1") ? true : false;
            txtReturnScoreRemark.Text = dt.Rows[0]["SRETURNSCOREREMARK"] + "";
            txtWrongContractNO.Text = dt.Rows[0]["NWRONGCONTRACTNO"] + "";
            txtContractDetail.Text = dt.Rows[0]["SCONTRACTDETAIL"] + "";

            if ((dt.Rows[0]["CSENDBY"] + "").Contains("1"))
            {
                rblDelivery.Value = "1";
                rblDelivery.ClientEnabled = false;
            }

            DataTable dtFile = CommonFunction.Get_Data(sql, "SELECT NID, SFILENAME,SFILEPATH FROM TACCIDENTIMPORTFILE WHERE SACCIDENTID ='" + Session["oSACCIDENTID"] + "'");
            foreach (DataRow dr in dtFile.Rows)
            {
                switch ("" + dr["NID"])
                {
                    case "1":
                        txtFileName0.Text = dr["SFILENAME"] + "";
                        txtFilePath0.Text = dr["SFILEPATH"] + "";
                        break;
                    case "2":
                        txtFileName1.Text = dr["SFILENAME"] + "";
                        txtFilePath1.Text = dr["SFILEPATH"] + "";
                        break;
                    case "3":
                        txtFileName2.Text = dr["SFILENAME"] + "";
                        txtFilePath2.Text = dr["SFILEPATH"] + "";
                        break;
                    case "4":
                        txtFileName3.Text = dr["SFILENAME"] + "";
                        txtFilePath3.Text = dr["SFILEPATH"] + "";
                        break;
                    case "5":
                        txtFileName4.Text = dr["SFILENAME"] + "";
                        txtFilePath4.Text = dr["SFILEPATH"] + "";
                        break;
                    case "6":
                        txtFileName5.Text = dr["SFILENAME"] + "";
                        txtFilePath5.Text = dr["SFILEPATH"] + "";
                        break;
                }


            }

            VisibleControlUpload();
        }
    }


    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID, SPERSONELNO,FULLNAME FROM (SELECT E.SEMPLOYEEID, E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND  E.SPERSONELNO LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            int count = _Filename.Count() - 1;

            string genName = "accident" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }


    private void VisibleControlUpload()
    {
        uplExcel0.ClientVisible = false;
        txtFileName0.ClientVisible = true;
        txtFileName0.ClientEnabled = false;
        btnView0.ClientEnabled = true;
        btnDelFile0.ClientVisible = false;

        uplExcel1.ClientVisible = false;
        txtFileName1.ClientVisible = true;
        txtFileName1.ClientEnabled = false;
        btnView1.ClientEnabled = true;
        btnDelFile1.ClientVisible = false;

        uplExcel2.ClientVisible = false;
        txtFileName2.ClientVisible = true;
        txtFileName2.ClientEnabled = false;
        btnView2.ClientEnabled = true;
        btnDelFile2.ClientVisible = false;

        uplExcel3.ClientVisible = false;
        txtFileName3.ClientVisible = true;
        txtFileName3.ClientEnabled = false;
        btnView3.ClientEnabled = true;
        btnDelFile3.ClientVisible = false;

        uplExcel4.ClientVisible = false;
        txtFileName4.ClientVisible = true;
        txtFileName4.ClientEnabled = false;
        btnView4.ClientEnabled = true;
        btnDelFile4.ClientVisible = false;

        uplExcel5.ClientVisible = false;
        txtFileName5.ClientVisible = true;
        txtFileName5.ClientEnabled = false;
        btnView5.ClientEnabled = true;
        btnDelFile5.ClientVisible = false;

    }


    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {//SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        repoint.SDELIVERYNO = "" + sArrayParams[11];
        repoint.Insert();
        return IsReduce;
    }

    protected void InActiveReducePoint()
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();
            if (chkReturnScore.Checked)
            {

                using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '06' AND SPROCESSID = '090' AND SREFERENCEID = '" + Session["addSACCIDENTID"] + "'", con))
                {
                    comUpdate.ExecuteNonQuery();
                }
            }

        }

    }
}