﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportTeabpans.aspx.cs" Inherits="ReportTeabpans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function OpenPage(sUrl) {
            alert(sUrl);
            window.open(sUrl);
        }

        function openFilebill(str) {

            var ss = window["txtUrlDownloadBill_" + str + ""].GetText();
            window.open("" + ss + "");
        }
        function openFilecharge(str) {

            var ss = window["txtChecking_" + str + ""].GetText();
            txtStatus.SetText(ss)
            var ss = window["txtRequestID_" + str + ""].GetText();
            txtReqID.SetText(ss)
            btnReport.DoClick();
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">
                                                    <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="เลขทะเบียนหัว-ท้าย, ชื่อบริษัทผู้ข่นส่ง"
                                                        Width="50%">
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="2%">-</td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(){ xcpn.PerformCallback('search');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานการออกใบเทียบแป้น ช่วงวันที่"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text="" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <%--        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="37%"></td>
                                                <td width="37%"></td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnPDF" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="PDF" OnClick="btnPDF_Click">
                                                       
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnExcel" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" Text="Excel" OnClick="btnExcel_Click">
                                                      
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>--%>
                                        <dx:ASPxTextBox runat="server" ID="txtStatus" ClientInstanceName="txtStatus" ClientVisible="false"></dx:ASPxTextBox>
                                        <dx:ASPxTextBox runat="server" ID="txtReqID" ClientInstanceName="txtReqID" ClientVisible="false"></dx:ASPxTextBox>
                                        <dx:ASPxButton runat="server" ID="btnReport" ClientInstanceName="btnReport" OnClick="btnReport_Click" ClientVisible="false"></dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" AutoGenerateColumns="false">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนหัว" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="VEH_NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนท้าย" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="TU_NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ข่นส่ง" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="SABBREVIATION">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทคำขอ" Width="16%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="REQTYPE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สาเหตุ" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="CAUSE_NAME">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันที่ตรวจ" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="APPOINTMENT_DATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ใบรับรอง" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" >
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton runat="server" ID="btnCHARGE" AutoPostBack="false" EnableTheming="false"
                                                            EnableDefaultAppearance="false" Cursor="pointer" ClientVisible='<%# Eval("CCHECKING_WATER").ToString()=="x"? false : true%>'>
                                                            <ClientSideEvents Click="function(s,e){openFilecharge(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                            <Image Url="images/ic_pdf2.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxTextBox runat="server" ID="txtChecking" Text='<%# Eval("CCHECKING_WATER").ToString()%>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                           <dx:ASPxTextBox runat="server" ID="txtRequestID" Text='<%# Eval("REQUEST_ID").ToString()%>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
