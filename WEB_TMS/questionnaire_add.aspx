﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="questionnaire_add.aspx.cs" Inherits="questionnaire_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 20%">
                            <span style="color: Red">กรุณากรอกข้อมูลให้ครบถ้วน</span>
                        </td>
                        <td style="width: 35%">
                            &nbsp;
                        </td>
                        <td bgcolor="#FFFFFF" style="width: 15%">
                        </td>
                        <td style="width: 30%">
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            สถานประกอบการ
                        </td>
                        <td align="left" class="style27">
                            <dx:ASPxComboBox ID="cboVendor" runat="server" Width="180px" ClientInstanceName="cboVendor"
                                TextFormatString="{0}" CallbackPageSize="30" EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" ValueField="SVENDORID">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF" class="style24">
                            วันที่ตรวจประเมิน
                        </td>
                        <td align="left">
                            <dx:ASPxDateEdit ID="dteDateStart1" runat="server" SkinID="xdte" ClientInstanceName="dteDateCheck">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            สถานที่ตรวจประเมิน
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtAddress" runat="server" Width="260px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            รายชื่อคณะผู้ตรวจ
                        </td>
                        <td>
                            <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Border-BorderStyle="None" Settings-ShowColumnHeaders="false" Style="margin-top: 0px"
                                ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtsID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                SkinID="_gvw" OnAfterPerformCallback="sgvw_AfterPerformCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="0">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtsID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn VisibleIndex="3" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtName" runat="server" Width="150px" Text='<%# Eval("dtsName") %>'>
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                    ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="30%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" SkinID="_delete"
                                                Width="10px">
                                                <ClientSideEvents Click="function (s, e) {sgvw.PerformCallback('deleteList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="False" GridLines="None"></Settings>
                                <Border BorderStyle="None"></Border>
                            </dx:ASPxGridView>
                            <dx:ASPxButton ID="btnAddd" runat="server" Text="More..." AutoPostBack="false" CssClass="dxeLineBreakFix"
                                Width="70px">
                                <ClientSideEvents Click="function (s, e) {sgvw.PerformCallback('AddClick'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            รายชื่อผู้รับการตรวจ
                        </td>
                        <td>
                            &nbsp;
                            <dx:ASPxGridView ID="sgvw1" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw1"
                                KeyFieldName="dtsID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                SkinID="_gvw" Style="margin-top: 0px" Width="100%" 
                                OnAfterPerformCallback="sgvw1_AfterPerformCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtsID" ShowInCustomizationForm="True"
                                        Visible="False">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="3" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtName1" runat="server" Text='<%# Eval("dtsName") %>' Width="150px">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                    ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" ShowInCustomizationForm="True" VisibleIndex="5"
                                        Width="30%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbDel1" runat="server" CausesValidation="false" SkinID="_delete"
                                                Width="10px">
                                                <ClientSideEvents Click="function (s, e) {sgvw1.PerformCallback('deleteList1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings GridLines="None" ShowColumnHeaders="False" />
                                <Border BorderStyle="None" />
                            </dx:ASPxGridView>
                            <dx:ASPxButton ID="btnAddd1" runat="server" Text="More..." AutoPostBack="false" CssClass="dxeLineBreakFix"
                                Width="70px">
                                <ClientSideEvents Click="function (s, e) {sgvw1.PerformCallback('AddClick1'); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ใบบันทึกเข้ารับการตรวจ
                        </td>
                        <td colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <span style="text-align: left">ชื่อหลักฐาน</span>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="170px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <span style="text-align: left">ไฟล์แนบ </span>
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="uploader" runat="server" ClientInstanceName="uploader"
                                            NullText="Click here to browse files..." Size="35" OnFileUploadComplete="UploadControl_FileUploadComplete">
                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                            </ValidationSettings>
                                            <BrowseButton Text="แนบไฟล์">
                                            </BrowseButton>
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add">
                                            <ClientSideEvents Click="function(s,e){uploader.Upload();sgvwFile.PerformCallback('Upload');}">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="3">
                            <dx:ASPxGridView ID="sgvwFile" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="false"
                                Style="margin-top: 0px" ClientInstanceName="sgvwFile" Width="100%" KeyFieldName="dtID"
                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SkinID="_gvw" 
                                OnAfterPerformCallback="sgvwFile_AfterPerformCallback">
                                <ClientSideEvents EndCallback="function(s,e){if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="0">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="FilePath" FieldName="dtFilePath" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblScore" runat="server" Text="ชื่อหลักฐาน">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle ForeColor="#0066FF" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="85%">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxLabel ID="lblEvidence" runat="server" Text='<%# Eval("dtEvidenceName") %>'
                                                            Width="250">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                            Text="Delete" Width="15px">
                                                            <ClientSideEvents Click="function (s, e) {sgvwFile.PerformCallback('DeleteFileList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="imbView0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                            Text="View" Width="15px">
                                                            <ClientSideEvents Click="function (s, e) {sgvwFile.PerformCallback('ViewFileList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings GridLines="None" ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            ผลการประเมินแบบสอบถาม
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ระดับคะแนน
                        </td>
                        <td>
                            <dx:ASPxRadioButtonList ID="rblSelectAll" ClientInstanceName="rblSelectAll" runat="server"
                                SkinID="rblStatus">
                                <ClientSideEvents ValueChanged="function(s,e){txtTotal.SetValue('0');xcpn.PerformCallback();}"></ClientSideEvents>
                                <Items>
                                    <dx:ListEditItem Text="น้อย" Value="1" />
                                    <dx:ListEditItem Text="ปานกลาง" Value="2" />
                                    <dx:ListEditItem Text="ดี" Value="3" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" DataSourceID="sdsMaster"
                                SettingsBehavior-AllowSort="false" KeyFieldName="NGROUPID" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หัวข้อแบบประเมิน" FieldName="SGROUPNAME" VisibleIndex="0"
                                        Width="50%" />
                                    <dx:GridViewDataColumn Caption="ระดับคะแนน" VisibleIndex="1" Width="20%" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="เหตุผลเพิ่มเติม" VisibleIndex="2" Width="30%" />
                                </Columns>
                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                <SettingsDetail ShowDetailButtons="False" ShowDetailRow="True" />
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="sdsDetail" KeyFieldName="NTYPEVISITFORMID"
                                            Width="100%" OnBeforePerformDataSelect="detailGrid_DataSelect" Settings-ShowColumnHeaders="false"
                                            OnHtmlDataCellPrepared="detailGrid_HtmlDataCellPrepared" SettingsBehavior-AllowSort="false" SettingsPager-PageSize="100">
                                            <Columns>
                                                <dx:GridViewDataColumn FieldName="SVISITFORMNAME" VisibleIndex="2" Width="51%" />
                                                <dx:GridViewDataColumn VisibleIndex="3" Width="21%" CellStyle-HorizontalAlign="Center"
                                                    Caption="#">
                                                    <DataItemTemplate>
                                                        <dx:ASPxRadioButtonList ID="rblWEIGHT" runat="server" ClientInstanceName="rblWEIGHT"
                                                            TextField="STYPEVISITLISTNAME" ValueField="NTYPEVISITLISTSCORE" DataSourceID="sdsRedio"
                                                            SkinID="rblStatus">
                                                        </dx:ASPxRadioButtonList>
                                                        <dx:ASPxTextBox ID="txtWEIGHT" runat="server" ClientInstanceName="txtWEIGHT" Text='<%#Eval("NWEIGHT")%>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox ID="txtOldSum" runat="server" ClientInstanceName="txtOldSum" Text="0"
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn VisibleIndex="4" Width="28%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox ID="txtRemark" runat="server" Width="100%">
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NWEIGHT" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NTYPEVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <Settings GridLines="None"></Settings>
                                            <Border BorderStyle="None" />
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <Styles Cell-BackColor="#ccffff">
                                    <Cell BackColor="#CCFFFF">
                                    </Cell>
                                </Styles>
                                <Settings GridLines="None"></Settings>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sdsMaster" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT GV.NGROUPID ,GV.NNO || '. ' || GV.SGROUPNAME AS SGROUPNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TGROUPOFVISITFORM gv ON TV.NTYPEVISITFORMID = GV.NTYPEVISITFORMID where tv.CACTIVE = '1' AND gv.CACTIVE = '1' ORDER BY GV.NNO">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsDetail" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT NVISITFORMID,'&nbsp;&nbsp;&nbsp;' || rownum || ') ' || SVISITFORMNAME AS SVISITFORMNAME, nvl(NWEIGHT,0) AS NWEIGHT, NVISITFORMID ,NGROUPID,NTYPEVISITFORMID
 FROM TVISITFORM WHERE (NGROUPID = :oNGROUPID) AND (CACTIVE = '1')">
                                <SelectParameters>
                                    <asp:SessionParameter Name="oNGROUPID" SessionField="ONGROUPID" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsRedio" runat="server" EnableCaching="true" CacheKeyDependency="chkRedio"
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT TVL.NTYPEVISITLISTSCORE , TVL.STYPEVISITLISTNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TTYPEOFVISITFORMLIST tvl ON TV.NTYPEVISITFORMID = TVL.NTYPEVISITFORMID WHERE TV.CACTIVE = '1' ORDER BY TVL.NTYPEVISITLISTSCORE">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            รายละเอียดเพิ่มเติม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxMemo ID="txtDetail" runat="server" Height="100px" Width="400px">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <table>
                                <tr>
                                    <td>
                                        คะแนนรวม
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtTotal" ClientInstanceName="txtTotal" ClientEnabled="false"
                                            Border-BorderColor="White" runat="server" Width="80px" Text="0" CssClass="dxeLineBreakFix"
                                            ForeColor="Black" RightToLeft="True">
                                            <Border BorderColor="White"></Border>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        คะแนน
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'questionnaire.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
