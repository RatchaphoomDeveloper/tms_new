﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS_BLL.Master;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TMS_BLL.Transaction.Complain;
using System.Text;
using EmailHelper;
using System.Globalization;
using TMS_BLL.Transaction.Report;
using System.Web.Security;

public partial class Vehicle_approve : PageBase
{
    #region ViewState    
    public string VendorId
    {
        get
        {
            if (ViewState["VendorId"] != null)
                return (string)ViewState["VendorId"];
            else
                return "";
        }
        set
        {
            ViewState["VendorId"] = value;
        }
    }
    public string CGROUP
    {
        get
        {
            if (ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return "";
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    public string STRUCKID
    {
        get
        {
            if (ViewState["STRUCKID"] != null)
                return (string)ViewState["STRUCKID"];
            else
                return "";
        }
        set
        {
            ViewState["STRUCKID"] = value;
        }
    }
    public string STRANSPORTID
    {
        get
        {
            if (ViewState["STRANSPORTID"] != null)
                return (string)ViewState["STRANSPORTID"];
            else
                return "";
        }
        set
        {
            ViewState["STRANSPORTID"] = value;
        }
    }
    #endregion
    private string registerno = "";
    private string CartypeId;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        this.Culture = "en-US";
        this.UICulture = "en-US";
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
            
        }
        else
        {
            VendorId = Session["SVDID"].ToString();
            CGROUP = Session["CGROUP"].ToString();
        }

            
        if (!IsPostBack)
        {

            
            string str = Request.QueryString["str"];

            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");

            DataTable dtClassGroup = VehicleBLL.Instance.ClassGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlClassGroup, dtClassGroup, "CLASS_GROUP_CODE", "CLASS_GROUP_CODE", true);

            #region BindData
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                str = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["STTRUCKID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                STRUCKID = decryptedValue;
                decryptedBytes = MachineKey.Decode(Request.QueryString["CarTypeID"], MachineKeyProtection.All);
                decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                Session["SCARTYPEID"] = decryptedValue;
                CartypeId = string.Equals(Session["SCARTYPEID"], "0") ? "0" : "1";
                Rdo_TypeCar.SelectedValue =CartypeId ;
                Rdo_TypeCar.Enabled = false;
                radSpot.Enabled = false;
                DataTable dt = VehicleBLL.Instance.LoadDataSelectEdit(STRUCKID);

                if (string.Equals(dt.Rows[0]["CSPACIALCONTRAC"].ToString(), "N"))
                {
                    radSpot.SelectedValue = "0";
                    ddlClassGroup.Enabled = false;
                }
                else
                {
                    radSpot.SelectedValue = "1";
                    ddlClassGroup.Enabled = true;
                }

                if (!string.Equals(dt.Rows[0]["CLASSGRP"].ToString(), string.Empty))
                    ddlClassGroup.SelectedValue = dt.Rows[0]["CLASSGRP"].ToString();

                VendorId = dt.Rows[0]["STRANSPORTID"].ToString();
                STRANSPORTID=dt.Rows[0]["STRANSPORTID"].ToString();
                this.initalFrom(CGROUP);
                this.ChoseTypeCar(CartypeId);
                this.BindDataInForm(STRUCKID, Rdo_TypeCar.SelectedIndex.ToString(),dt);
                this.CheckAlterEmail(STRUCKID);
            }
            #endregion

            if (Session["AutoApprove"] != null)
            {
                if ((bool)Session["AutoApprove"])
                {
                    cmdSave_Click(null, null);

                    Session["AutoApprove"] = false;
                }
            }
        }
    }

    private void CheckAlterEmail(string STRUCKID)
    {
        DataTable Dt = CarBLL.Instance.CheckalterEmail(STRUCKID);
        if (Dt.Rows.Count > 0)
        {
            alertSuccess("เนื่องจากรายการนี้มีการอนุมัติแล้วกรุณาดูที่หน้ารายละเอียด", "approve_pk.aspx");
        }
    }

    private void BindDataInForm(string STRUCKID,string TypeCar,DataTable dt)
    {
        
        switch (TypeCar)
        {
            case "0":
                ddlVendorTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                ddl_truck.SelectedValue = dt.Rows[0]["SHEADREGISTERNO"].ToString();
                Truck_contract.SelectedValue = dt.Rows[0]["TMPCONTRACTID"].ToString();
                VEH_TEXT.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                break;
            case "1":
                ddlvendorSemiTruck.SelectedValue = dt.Rows[0]["STRANSPORTID"].ToString();
                rdo_contratType1.SelectedValue = dt.Rows[0]["CARCATE_ID"].ToString();
                truck_head.SelectedValue = dt.Rows[0]["SHEADREGISTERNO"].ToString();
                Truck_Semi.SelectedValue = dt.Rows[0]["STRAILERREGISTERNO"].ToString();
                TruckSemi_contract.SelectedValue =  dt.Rows[0]["TMPCONTRACTID"].ToString();
                semi_veh_text.Text = dt.Rows[0]["VEH_TEXT"].ToString();
                break;
            
        }     


    }
    #region EVENT     
    protected void ChoseTypeCar(string CartypeId)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, VendorId, radSpot.SelectedValue);
        ConfrimStatus.Visible = true;
        switch (CartypeId)
        {
            case "0":
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(VendorId, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);  
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case "1":
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(VendorId, radSpot.SelectedValue);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(VendorId, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "SHEADREGISTERNO", "SHEADREGISTERNO", false);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
        
        
    }

    protected void ddlTruckSelectedValue(object sender,EventArgs e)
    {
        this.initalTruck(ddlVendorTruck.SelectedValue);
        
    }

    protected void ddlSemiSelectValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlvendorSemiTruck.SelectedValue);
    }

    protected void initalTruck(string value)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, value, radSpot.SelectedValue);
        DataTable dt_classgrp = VehicleBLL.Instance.LoadDataClassgrp();
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(value, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);                                
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case 1:
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(value, radSpot.SelectedValue);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(value, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
    }
    protected void TruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(ddl_truck.SelectedValue);
        int totalcapacity=0 ;
        string veh_text ="" ;
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
			{
			    totalcapacity +=int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
                veh_text+=LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() +",";
			}
        veh_text = veh_text.TrimEnd(',');
        VEH_TEXT.Text = "SU-" + totalcapacity + "(" + veh_text + ")";
    }
    #region คำนวณ VEH_TEXT Semi
    
    
    protected void SemitTruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(Truck_Semi.SelectedValue);
        int totalcapacity=0;
        string veh_text = "";
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            totalcapacity += int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
            veh_text += LoadGetVeh_Text.Rows[i]["VEH_TEXT"].ToString() + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        semi_veh_text.Text = "SM-" + totalcapacity + "(" + veh_text + ")";
    }
    #endregion
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlClassGroup.Enabled && ddlClassGroup.SelectedIndex < 1)
                throw new Exception("กรุณาระบุ Class Group");

            switch (active_blacklist.SelectedValue)
            {
                case "Y":
                    if (this.FunctionSaveSap(active_blacklist.SelectedValue))
                    {
                        this.Save_TMS();
                    }
                    else
                    {
                        alertFail("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ");
                    }
                    break;
                default:
                    this.ChangeVehicle();
                    break;
            }
        }
        catch (Exception ex)
        {

            lblerror.Text = ex.Message;
        }                 
    }

    #endregion
    #region InitalData
    private void initalFrom(string CGROUP)
    {
        DataTable dt_vehicle_car = VehicleBLL.Instance.LoadDataVehicleCar();
        string condition = VendorId;
        DataTable dt_vendor = VendorBLL.Instance.SelectName(condition);
        switch (CGROUP)
        {
            case "1":
                
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);  
            break;
            default:
                
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car, "config_value", "config_name", false);
                break;
        }
        
    }
    #endregion
    #region Micellouse
        private Control FindControlRecursive(Control rootControl, string controlID)
        {
            if (rootControl.ID == controlID) return rootControl;

            foreach (Control controlToSearch in rootControl.Controls)
            {
                Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
                if (controlToReturn != null) return controlToReturn;
            }
            return null;
        }
        private string GetConditionVendor()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (ddlVendorTruck.SelectedIndex > 0)
                    sb.Append(" AND TCONTRACT.SVENDORID = '" + Truck_contract.SelectedValue + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    #endregion
    #region บันทึกข้อมูลลง
        protected DataTable DataToDatatable()
        {
            DataTable dt ;
            switch (Rdo_TypeCar.SelectedIndex)
	        {
                case 0:
                    dt = new DataTable();
                    dt.Columns.Add("STRUCKID");
                    dt.Columns.Add("typecar");
                    dt.Columns.Add("transportid");
                    dt.Columns.Add("contrattype");
                    dt.Columns.Add("truck");
                    dt.Columns.Add("contratid");
                    dt.Columns.Add("veh_text");
                    dt.Columns.Add("ACTIVE");
                    //userId
                    dt.Columns.Add("UserId");
                                
                    
                    dt.Rows.Add(STRUCKID,Rdo_TypeCar.SelectedIndex, ddlVendorTruck.SelectedValue, rdo_contratType.SelectedValue, ddl_truck.SelectedItem.Text.Trim(), Truck_contract.SelectedValue, VEH_TEXT.Text.Trim(),
                        active_blacklist.SelectedValue,
                        //date_startdata, date_enddata, detaill_cencal.Text.Trim(),
                        Session["UserID"]
                        );
                   
                    break;
		        default:
                    dt = new DataTable();
                    dt.Columns.Add("STRUCKID");
                    dt.Columns.Add("typecar");
                    dt.Columns.Add("transportid");
                    dt.Columns.Add("contrattype");
                    dt.Columns.Add("truck");
                    dt.Columns.Add("semitruck");
                    dt.Columns.Add("contratid");
                    dt.Columns.Add("Semiveh_text");
                    dt.Columns.Add("ACTIVE");
                    //userId
                    dt.Columns.Add("UserId");
                    
                    dt.Rows.Add(STRUCKID,Rdo_TypeCar.SelectedIndex, ddlvendorSemiTruck.SelectedValue, rdo_contratType1.SelectedValue, truck_head.SelectedItem.Text.Trim(), Truck_Semi.SelectedItem.Text.Trim(), TruckSemi_contract.SelectedValue, semi_veh_text.Text.Trim(),
                        active_blacklist.SelectedValue,                        
                        Session["UserID"]
                        );                    
                    break;
	            }
            return dt;
            
        }        
    #endregion
    #region Function SAVE_TMS
        private void Save_TMS()
        {
            try
            {
            //บันทึกข้อมูลลงTMS
            string ClassGroup = string.Empty;
            if (ddlClassGroup.Enabled && ddlClassGroup.SelectedIndex > 0)
                ClassGroup = ddlClassGroup.SelectedItem.Text;

            int ContractID = 0;
            string ContractName = string.Empty;
            if (Rdo_TypeCar.SelectedIndex == 0)
            {
                if (Truck_contract.SelectedIndex < 0)
                    throw new Exception("กรุณาเลือกสัญญาก่อน");

                ContractID = int.Parse(Truck_contract.SelectedValue);
                ContractName = Truck_contract.SelectedItem.Text;
            }
            else
            {
                if (TruckSemi_contract.SelectedIndex < 0)
                    throw new Exception("กรุณาเลือกสัญญาก่อน");

                ContractID = int.Parse(TruckSemi_contract.SelectedValue);
                ContractName = TruckSemi_contract.SelectedItem.Text;
            }

            DataTable valuereturn = VehicleBLL.Instance.EditVehicle(Rdo_TypeCar.SelectedIndex.ToString(), DataToDatatable(), radSpot.SelectedValue, ClassGroup, ContractID);
                this.GetTypeTruckSave(Rdo_TypeCar.SelectedIndex.ToString(), active_blacklist.SelectedValue.ToString());
                if (string.Equals(CGROUP, "1"))
                {
                    if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
                    {
                        try
                        {
                            if (string.Equals(active_blacklist.SelectedValue.ToString(), "Y"))
                            {
                                SystemFunction.AddToTREQ_DATACHANGE(valuereturn.Rows[0][1].ToString(), "H", Session["UserID"] + "", "1", string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)", valuereturn.Rows[0][2].ToString(), "Y", "");
                                //ส่งทะเบียนเข้าอีเมลล์
                                registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";

                                //LOG.Instance.SaveLog("นำรถเข้าสัญญา", registerno.Replace(" (รถเดี่ยว)", string.Empty).Replace(" (รถกึ่งพวง)", string.Empty), Session["UserID"].ToString(), "<span class=\"green\">" + ContractName + "</span>", "");

                                this.SendEmail(ConfigValue.EmailVehicleApprove, STRUCKID, valuereturn.Rows[0][2].ToString());
                                alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                            }
                            else
                            {
                                SystemFunction.AddToTREQ_DATACHANGE(valuereturn.Rows[0][1].ToString(), "H", Session["UserID"] + "", "2", string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)", valuereturn.Rows[0][2].ToString(), "Y", "");
                                //ส่งทะเบียนเข้าอีเมลล์
                                registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
                                this.SendEmail(ConfigValue.EmailVehicleNoApprove, STRUCKID, valuereturn.Rows[0][2].ToString());
                                alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                            }
                            
                                
                            
                        }
                        catch (Exception)
                        {

                            alertSuccess("แก้ไขข้อมูลรถใน SAP สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ", "truck_info.aspx");
                        }

                        //บันทึกข้อมูลสำเร็จ
                    }
                    else
                    {
                        alertFail("แก้ไขข้อมูลรถใน SAP ไม่สำเร็จ <br/> แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ" + valuereturn.Rows[0][0].ToString());
                    }
                }
                else
                {
                    if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
                    {
                        try
                        {
                            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "Vendor_Detail.aspx");
                        }
                        catch (Exception)
                        {

                            alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "Vendor_Detail.aspx");
                        }

                    }
                    else
                    {
                        alertFail(valuereturn.Rows[0][0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }   
        }
    #endregion
    #region Function Save_SAP
        private bool FunctionSaveSap(string ACTIVE)
        {
            if(string.Equals(ACTIVE,"Y"))
            {
                string sMsg;
                string VEH_NO = string.Equals(Rdo_TypeCar.SelectedIndex.ToString(), "0") ? ddl_truck.SelectedItem.Text.ToString() : truck_head.SelectedItem.Text.ToString();
                SAP_UPDATE_VEH veh_syncout = new SAP_UPDATE_VEH();
                veh_syncout.sVehicle = VEH_NO;

                if (radSpot.SelectedValue == "1")
                    veh_syncout.sClass_Grp = ddlClassGroup.SelectedValue;

                sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                if (!string.Equals(sMsg, ""))
                {
                    SAP_SYNCOUT_SI veh_update = new SAP_SYNCOUT_SI();
                    veh_update.VEH_NO = VEH_NO + "";
                     sMsg = veh_update.UDT_Vehicle_SYNC_OUT_SI();
                    if (string.Equals(sMsg, ""))
                    {
                        return true;
                    }
                    else
                    {
                        alertFail(sMsg);
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            return false;
        }   
    
    #endregion
        #region ResponseMenu
        protected void ResponseMenu(object sender, EventArgs e)
        {
            Response.Redirect("approve_pk.aspx");           
        }
        #endregion        
        private void ChangeVehicle()
        {
            try
            {
                //บันทึกข้อมูลลงTMS(ให้ผู้ขนส่งสลับหางได้เท่านั้น
                DataTable valuereturn = VehicleBLL.Instance.ChangeVehicle(Rdo_TypeCar.SelectedIndex.ToString(), DataToDatatable());
                if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        this.registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
                        this.SendEmail(ConfigValue.EmailVehicleAdd, valuereturn.Rows[0][1].ToString(), Session["SVDID"].ToString());
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ <br />", "approve_pk.aspx");
                    }
                    catch (Exception)
                    {
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br/>ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ <br /> รอการอนุมัติ", "approve_pk.aspx");
                    }

                }
                else
                {
                    alertFail(valuereturn.Rows[0][0].ToString());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #region SentMail
        private void SendEmail(int TemplateID, string STTRUCKID, string VendorID)
        {
            try
            {
                DataSet dsSendEmail = new DataSet();
                string EmailList = string.Empty;

                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    Subject = Subject.Replace("{regisNo}", registerno);
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    if (TemplateID == ConfigValue.EmailVehicleApprove)
                    {
                        string MonthName1 = string.Empty;
                        string MonthName2 = string.Empty;
                        string MonthName3 = string.Empty;
                        string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Vehicle_detail.aspx?str=" + ConfigValue.GetEncodeText("EDIT") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedIndex.ToString());
                        dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, VendorID);
                        EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                        Body = Body.Replace("{ownner_name}", CarBLL.Instance.LoadVendorName(VendorID)); 
                        Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                        Body = Body.Replace("{regisNo}", registerno);
                        Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                        //MailService.SendMail("raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailVehicleApprove", ColumnEmailName);
                    
                    }
                    if (TemplateID == ConfigValue.EmailVehicleNoApprove)
                    {
                        string MonthName1 = string.Empty;
                        string MonthName2 = string.Empty;
                        string MonthName3 = string.Empty;
                        string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Vehicle_detail.aspx?str=" + ConfigValue.GetEncodeText("EDIT") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedIndex.ToString());
                        dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, VendorID);
                        EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                        Body = Body.Replace("{ownner_name}", CarBLL.Instance.LoadVendorName(VendorID)); 
                        Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                        Body = Body.Replace("{regisNo}", registerno);
                        Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                        //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body);
                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, ConfigValue.DeliveryDepartmentCode, int.Parse(Session["UserID"].ToString()), VendorID, true, false), Subject, Body, "", "EmailVehicleNoApprove", ColumnEmailName);
                }
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        #endregion
    #region Log
        private void GetTypeTruckSave(string TypeTruck,string Approve)
        {
            try
            {
                if (string.Equals(Approve, "Y"))
                {
                    switch (TypeTruck)
                    {
                        case "0":
                            #region บันทึก Log
                            LOG.Instance.SaveLog("รข. อนุมัติจับคู่ TC หมายเลขทะเบียนรถ", ddl_truck.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + ddl_truck.SelectedItem.Text + "</span>", "");
                            #endregion
                            break;
                        default:
                            #region บันทึก Logหัว
                            LOG.Instance.SaveLog("รข. อนุมัติจับคู่ TC หมายเลขทะเบียนรถ", truck_head.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก) <br/>" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก)</span>", "");
                            #endregion
                            #region บันทึก Logหาง
                            LOG.Instance.SaveLog("รข. อนุมัติจับคู่ TC หมายเลขทะเบียนรถ", Truck_Semi.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก) <br/>" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก)</span>", "");
                            #endregion
                            break;
                    }
                }
                else
                {
                    switch (TypeTruck)
                    {
                        case "0":
                            #region บันทึก Log
                            LOG.Instance.SaveLog("รข. ปฏิเสธจับคู่ TC หมายเลขทะเบียนรถ", ddl_truck.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + ddl_truck.SelectedItem.Text.ToString() + "</span>", "");
                            #endregion
                            break;
                        default:
                            #region บันทึก Logหัว
                            LOG.Instance.SaveLog("รข. ปฏิเสธจับคู่ TC หมายเลขทะเบียนรถ", truck_head.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก) <br/>" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก)</span>", "");
                            #endregion
                            #region บันทึก Logหาง
                            LOG.Instance.SaveLog("รข. ปฏิเสธจับคู่ TC หมายเลขทะเบียนรถ", Truck_Semi.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก) <br/>" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก)</span>", "");
                            #endregion
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }            
        }
    #endregion
}
    
