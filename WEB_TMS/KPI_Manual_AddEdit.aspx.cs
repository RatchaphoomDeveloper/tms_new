﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using System.Web.UI.WebControls;

public partial class KPI_Manual_AddEdit : PageBase
{
    #region + View State +
    private string VendorID
    {
        get
        {
            if (ViewState["VendorID"] != null)
                return ViewState["VendorID"].ToString();
            else
                return string.Empty;
        }
        set
        {
            ViewState["VendorID"] = value;
        }
    }

    private string Year
    {
        get
        {
            if (ViewState["Year"] != null)
                return ViewState["Year"].ToString();
            else
                return string.Empty;
        }
        set
        {
            ViewState["Year"] = value;
        }
    }

    private int MonthID
    {
        get
        {
            if (ViewState["MonthID"] != null)
                return (int)ViewState["MonthID"];
            else
                return 0;
        }
        set
        {
            ViewState["MonthID"] = value;
        }
    }

    private int ContractID
    {
        get
        {
            if (ViewState["ContractID"] != null)
                return (int)ViewState["ContractID"];
            else
                return 0;
        }
        set
        {
            ViewState["ContractID"] = value;
        }
    }

    private DataTable dtMapping
    {
        get
        {
            if ((DataTable)ViewState["dtMapping"] != null)
                return (DataTable)ViewState["dtMapping"];
            else
                return null;
        }
        set
        {
            ViewState["dtMapping"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                cmdSave.Visible = false;
                cmdSaveDraft.Visible = false;
            }
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                cmdSave.Enabled = false;
                cmdSaveDraft.Enabled = false;
                cmdRecalculate.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] QueryString = decryptedValue.Split('&');

                string Type = QueryString[0];
                Year = QueryString[1];
                MonthID = int.Parse(QueryString[2]);
                ContractID = int.Parse(QueryString[3]);
                VendorID = QueryString[4];

                DataTable dtMonth = KPI2BLL.Instance.MonthSelect();
                DataRow[] Month = dtMonth.Select("MONTH_ID = " + MonthID.ToString());

                DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DataRow[] Vendor = dtVendor.Select("SVENDORID = " + VendorID);

                DataTable dtContract = ComplainBLL.Instance.ContractSelectBLL(string.Empty);
                DataRow[] Contract = dtContract.Select("SCONTRACTID = " + ContractID.ToString());

                txtYear.Text = Year;
                txtMonth.Text = Month[0].ItemArray[2].ToString();
                txtVendor.Text = Vendor[0].ItemArray[2].ToString();
                txtContract.Text = Contract[0].ItemArray[1].ToString();

                dtMapping = KPIBLL.Instance.KPIMappingFormSelectBLL(Year, MonthID, VendorID, ContractID, string.Empty);

                if (dtMapping.Rows.Count == 0)
                    alertSuccess("ยังไม่ได้มีการ Mapping ข้อมูลเดือน กับ ชุดคำถาม", "KPI_Manual.aspx");

                dgvMapping.Columns[14].HeaderText = dtMapping.Rows[0]["VARIABLE_A"].ToString();
                dgvMapping.Columns[15].HeaderText = dtMapping.Rows[0]["VARIABLE_B"].ToString();

                GridViewHelper.BindGridView(ref dgvMapping, dtMapping);
                this.Calculate();

                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    dgvMapping.Columns[GridViewHelper.GetColumnIndexByName(dgvMapping, "METHOD_NAME")].Visible = false;
                    dgvMapping.Columns[GridViewHelper.GetColumnIndexByName(dgvMapping, "FORMULA_NAME")].Visible = false;
                }

                txtFormName.Text = dtMapping.Rows[0]["HEADER_DETAIL"].ToString();

                DataTable dtLogin = new DataTable();
                dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");
                if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                {
                    cmdSave.Visible = false;
                    cmdSaveDraft.Visible = false;
                    cmdRecalculate.Visible = false;

                    TextBox txtA = new TextBox();
                    TextBox txtB = new TextBox();

                    for (int i = 0; i < dgvMapping.Rows.Count; i++)
                    {
                        txtA = (TextBox)dgvMapping.Rows[i].FindControl("txtA");
                        txtB = (TextBox)dgvMapping.Rows[i].FindControl("txtB");

                        txtA.Enabled = false;
                        txtB.Enabled = false;
                    }
                }
                else
                {
                    TextBox tmp = new TextBox();
                    for (int i = 0; i < dtMapping.Rows.Count; i++)
                    {
                        if (string.Equals(dtMapping.Rows[i]["FORMULA_ID"].ToString(), "2"))
                        {
                            tmp = (TextBox)dgvMapping.Rows[i].FindControl("txtB");
                            tmp.Text = "1";
                            tmp.Enabled = false;
                        }
                    }
                }

                if (string.Equals(Type, "View"))
                {
                    cmdSave.Visible = false;
                    cmdSaveDraft.Visible = false;
                    cmdRecalculate.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        this.KPISave(true, 2);
    }

    private void ValidateSave()
    {
        try
        {
            double tmp;

            TextBox txtA = new TextBox();
            TextBox txtB = new TextBox();
            Label lblPoint = new Label();

            for (int i = 0; i < dgvMapping.Rows.Count; i++)
            {
                txtA = (TextBox)dgvMapping.Rows[i].FindControl("txtA");
                txtB = (TextBox)dgvMapping.Rows[i].FindControl("txtB");
                lblPoint = (Label)dgvMapping.Rows[i].FindControl("lblPoint");

                if ((string.Equals(txtA.Text.Trim(), string.Empty) || (!double.TryParse(txtA.Text.Trim(), out tmp))))
                    throw new Exception("ค่าตัวแปร ต้องใส่ค่า และต้องเป็นตัวเลขเท่านั้น");

                if ((string.Equals(txtB.Text.Trim(), string.Empty) || (!double.TryParse(txtB.Text.Trim(), out tmp))))
                    throw new Exception("ค่าตัวแปร ต้องใส่ค่า และต้องเป็นตัวเลขเท่านั้น");

                if (string.Equals(lblPoint.Text.Trim(), string.Empty))
                    throw new Exception("ไม่สามารถคำนวณคะแนนได้");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Manual.aspx");
    }
    protected void cmdSaveDraft_Click(object sender, EventArgs e)
    {
        this.KPISave(false, 1);
    }

    private void KPISave(bool ValidateSave, int StatusID)
    {
        try
        {
            this.Calculate();

            if (ValidateSave)
                this.ValidateSave();

            TextBox txtA = new TextBox();
            TextBox txtB = new TextBox();
            Label lblPoint = new Label();

            for (int i = 0; i < dtMapping.Rows.Count; i++)
            {
                txtA = (TextBox)dgvMapping.Rows[i].FindControl("txtA");
                txtB = (TextBox)dgvMapping.Rows[i].FindControl("txtB");
                lblPoint = (Label)dgvMapping.Rows[i].FindControl("lblPoint");

                dtMapping.Rows[i]["VALUE_A"] = txtA.Text;
                dtMapping.Rows[i]["VALUE_B"] = txtB.Text;
            }
            KPIBLL.Instance.KPISaveTransactionBLL(Year, MonthID, VendorID, ContractID, dtMapping, StatusID, int.Parse(Session["UserID"].ToString()));

            alertSuccess("บันทึกข้อมูลเรียบร้อย");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRecalculate_Click(object sender, EventArgs e)
    {
        try
        {
            this.Calculate();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void Calculate()
    {
        try
        {
            decimal tmp;
            string PointBefore = string.Empty;

            TextBox txtA = new TextBox();
            TextBox txtB = new TextBox();
            Label lblPoint = new Label();
            Label lblPointBefore = new Label();

            int FormulaID = 0;

            for (int i = 0; i < dgvMapping.Rows.Count; i++)
            {
                txtA = (TextBox)dgvMapping.Rows[i].FindControl("txtA");
                txtB = (TextBox)dgvMapping.Rows[i].FindControl("txtB");

                if ((!string.Equals(txtA.Text.Trim(), string.Empty) && (decimal.TryParse(txtA.Text.Trim(), out tmp))))
                {
                    if ((!string.Equals(txtB.Text.Trim(), string.Empty) && (decimal.TryParse(txtB.Text.Trim(), out tmp))))
                    {
                        //txtA.Text = Math.Round(decimal.Parse(txtA.Text.Trim()), 3).ToString();
                        //txtB.Text = Math.Round(decimal.Parse(txtB.Text.Trim()), 3).ToString();

                        FormulaID = int.Parse(dtMapping.Rows[i]["FORMULA_ID"].ToString());

                        lblPoint = (Label)dgvMapping.Rows[i].FindControl("lblPoint");
                        lblPointBefore = (Label)dgvMapping.Rows[i].FindControl("lblPoint_Before");

                        KPI_Helper Helper = new KPI_Helper();
                        lblPoint.Text = Helper.Calculate(FormulaID, decimal.Parse(txtA.Text.Trim()), decimal.Parse(txtB.Text.Trim()), Year, MonthID, VendorID, ContractID, dtMapping, i, ref PointBefore);
                        lblPointBefore.Text = PointBefore;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string FindPoint(int i, decimal Point)
    {
        try
        {
            string ReturnValue = string.Empty;

            string Level1Start = dtMapping.Rows[i]["LEVEL1_START"].ToString();
            string Level1End = dtMapping.Rows[i]["LEVEL1_END"].ToString();

            string Level2Start = dtMapping.Rows[i]["LEVEL2_START"].ToString();
            string Level2End = dtMapping.Rows[i]["LEVEL2_END"].ToString();

            string Level3Start = dtMapping.Rows[i]["LEVEL3_START"].ToString();
            string Level3End = dtMapping.Rows[i]["LEVEL3_END"].ToString();

            string Level4Start = dtMapping.Rows[i]["LEVEL4_START"].ToString();
            string Level4End = dtMapping.Rows[i]["LEVEL4_END"].ToString();

            string Level5Start = dtMapping.Rows[i]["LEVEL5_START"].ToString();
            string Level5End = dtMapping.Rows[i]["LEVEL5_END"].ToString();

            if (!string.Equals(Level1Start, string.Empty) && !string.Equals(Level1End, string.Empty))
            {
                if (Point >= decimal.Parse(Level1Start) && Point <= decimal.Parse(Level1End))
                    ReturnValue = "1";
            }
            if (!string.Equals(Level2Start, string.Empty) && !string.Equals(Level2End, string.Empty))
            {
                if (Point >= decimal.Parse(Level2Start) && Point <= decimal.Parse(Level2End))
                    ReturnValue = "2";
            }
            if (!string.Equals(Level3Start, string.Empty) && !string.Equals(Level3End, string.Empty))
            {
                if (Point >= decimal.Parse(Level3Start) && Point <= decimal.Parse(Level3End))
                    ReturnValue = "3";
            }
            if (!string.Equals(Level4Start, string.Empty) && !string.Equals(Level4End, string.Empty))
            {
                if (Point >= decimal.Parse(Level4Start) && Point <= decimal.Parse(Level4End))
                    ReturnValue = "4";
            }
            if (!string.Equals(Level5Start, string.Empty) && !string.Equals(Level5End, string.Empty))
            {
                if (Point >= decimal.Parse(Level5Start) && Point <= decimal.Parse(Level5End))
                    ReturnValue = "5";
            }

            return ReturnValue;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}