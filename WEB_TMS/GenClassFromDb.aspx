<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GenClassFromDb.aspx.cs" Inherits="GenClassFromDb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Gen ClassFrom Database</title>
    <link href="research.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" style="width: 100%">
                <tr>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td colspan="2" style="text-align: center">
                        <span style="font-size: 14pt"><strong>Generate
                            Class From Database</strong></span></td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        Generate Class From Database : ���ҧ���ʨҡ���ҧ㹰ҹ������</td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        �кت��͵��ҧ
                        <asp:TextBox ID="txttable" runat="server"></asp:TextBox>
                        <asp:Button ID="btngen" runat="server" Text="Gen Class" OnClick="btngen_Click" />
                        <asp:RequiredFieldValidator ID="rfv" runat="server" ControlToValidate="txttable"
                            ErrorMessage="Required Table Name" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        &nbsp;</td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <strong>Description (͸Ժ��)</strong></td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        ��ͧ�кت��͵��ҧ���������㹰ҹ���������к����ѧ�Դ���������� ������ҧ���������п�ͧ
                        error �����辺���ҧ 
                        <br />
                        ��ѧ�ҡ���������ӧҹ���稨����������ʡ�� <strong>.cs</strong> ����ժ��͵ҵ��ҧ������к����
                        ��觨�������� Folder <strong>Create_Class</strong> 
                        <br />
                        ����ѧ�������ҧ Folder ���������зӡ�����ҧ�ͧ���ѵ��ѵ� �. Path �Ѩ�غѹ���
                        ������������</td>
                </tr>
                <tr>
                    <td colspan="8">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <strong>Use (�����)</strong></td>
                </tr>
                <tr>
                    <td colspan="8">
                        copy ���˹�ҹ�� �Ѩ�غѹ������ Path ��� <strong>\\Develop3\Project\==Research==\ASP.Net</strong> ������������ժ������ <strong>GenClassFromDb.aspx</strong> ��� <strong>GenClassFromDb.aspx.cs</strong></td>
                </tr>
                <tr>
                    <td colspan="8">
                        ��� copy ����� 2 �������礷����ѧ�Ѳ������ �������¡�������ҹ�к����Ѳ������
                        �� ���礷��Ѳ���ժ������ <strong>uou</strong> �������ö���¡��ѧ��� <strong>http://develop3/uou/GenClassFromDb.aspx</strong><br />
                        ��ҹ������ѹ���º���� ����������� ���</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <span style="color: #ff0033">* �����˵�</span> : �ѧ��ѹ��� Insert, Update ��� Delete
                        �����ҡ��� Gen �������¡�� Store Procedure 㹡�� Insert, Update ��� Delete</td>
                </tr>
                <tr>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
                <tr>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                    <td style="width: 12%">
                    </td>
                </tr>
            </table>
            <br />
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        </div>
    </form>
</body>
</html>
