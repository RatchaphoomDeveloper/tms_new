﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using TMS_BLL.Transaction.Appeal;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Web.UI.HtmlControls;
using TMS_BLL.Master;

public partial class admin_suppliant_Add : PageBase
{
    #region Member
    DataSet ds;
    DataTable dt;
    DataRow dr;
    string FieldType = "SUPPLIANT", str = string.Empty, mode = Mode.Edit.ToString();
    #region + View State +

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUploadRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtUploadRequestFile"] != null)
                return (DataTable)ViewState["dtUploadRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadRequestFile"] = value;
        }
    }

    private DataTable dtSUPPLIANT
    {
        get
        {
            if ((DataTable)ViewState["dtSUPPLIANT"] != null)
                return (DataTable)ViewState["dtSUPPLIANT"];
            else
                return null;
        }
        set
        {
            ViewState["dtSUPPLIANT"] = value;
        }
    }
    #endregion
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetText();
            SetDropDownList();
            //dtUserLogin = (DataTable)Session["UserLogin"];
            //hidSYSTEM_ID.Value = SystemID + string.Empty;
            dtUploadType = GetUploadType(ref ddlUploadType, FieldType);
            dtUpload = GetTableUpload();
            dtUploadRequestFile = GetUploadRequire(ref dgvUploadRequestFile, FieldType);

            if (Request.QueryString["str"] != null && !string.IsNullOrEmpty(Request.QueryString["str"]))
            {
                //mode & SAPPEALID & ID1
                string[] str = DecodeQueryString(Request.QueryString["str"]);
                if (str.Count() > 1)
                {
                    hidITYPE.Value = str[3];
                    hidV_ID.Value = str[2];
                    hidSAPPEALID.Value = str[1];
                    mode = str[0];

                    GetData();
                }
            }
            else
            {
                ViewState["DataField"] = GetField(FieldType);
                lblHeader.Text = SetField((DataTable)ViewState["DataField"]);
            }
            SetVisible();

        }
    }

    #endregion

    #region Method
    #region SetDropDownList
    private void SetDropDownList()
    {

    }
    #endregion

    #region SetVisible
    private void SetVisible()
    {
        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
        {

        }
        else
        {
            if (hidCHKSTATUS.Value == "I" || hidCHKSTATUS.Value == "F")
            {
                PTTDocTab.Visible = true;
            }
            else
            {
                PTTDocTab.Visible = false;
            }
        }
    }
    #endregion

    #region SetText
    private void SetText()
    {
        btnCancel.Text = "ยกเลิก";
        btnSave.Text = "บันทึก";


    }
    #endregion

    #region GetData
    private void GetData()
    {
        //ds = ContractBLL.Instance.ContractSelectByID(hidID.Value, SystemID);
        if (!string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
        {
            // dt = AppealBLL.Instance.AppealSelect(txtSEARCH.Text.Trim(), SystemID, StartDate, EndDate, hidDocID.Value, ddlSTATUS.SelectedValue, hideAppealID.Value);
        }
        else
        {
            dtSUPPLIANT = AppealBLL.Instance.AppealVendorSelectByID(hidITYPE.Value, hidV_ID.Value);

            if (dtSUPPLIANT.Rows.Count > 0)
            {
                string CHKSTATUS = dtSUPPLIANT.Rows[0]["CHKSTATUS"] + string.Empty;
                if (CHKSTATUS == "I" || CHKSTATUS == "F")
                {
                    btnSave.Enabled = false;
                }
            }
        }

        ViewState["DataField"] = GetField(FieldType);
        gvData.DataSource = dtSUPPLIANT;
        gvData.DataBind();
        lblHeader.Text = SetFieldValue((DataTable)ViewState["DataField"], dtSUPPLIANT, mode);

        #region UploadFile
        dtUpload = UploadBLL.Instance.UploadSelectBLL(hidSAPPEALID.Value, FieldType, string.Empty);
        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);
        CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
        #endregion

    }

    protected void Button2_Click(object sender, System.EventArgs e)
    {
        btnSave.Visible = false;
    }

    private string GetConditionSearchComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            //if (!string.IsNullOrEmpty(hidID1.Value))
            //    return sb.Append(" AND TCOMPLAIN.DOC_ID || '/' || TO_CHAR(TCOMPLAIN_SCORE_CAR.DELIVERY_DATE, 'YYYY-MM-DD', 'NLS_DATE_LANGUAGE = ENGLISH') || '/' || NVL(TCOMPLAIN_SCORE_CAR.STRUCKID, '') ='" + hidID1.Value + "'").ToString();

            if (!string.Equals(hidSAPPEALID.Value, string.Empty))
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hidSAPPEALID.Value + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #endregion

    #region Event
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            btnSave.Visible = false;
            //dt = GetDataToSave(FieldType);
            dr = dtSUPPLIANT.Rows[0];
                dr["SAPPEALTEXT"] = txtSAPPEALTEXT.Text.Trim();
                dr["SWAITDOCUMENT"] = txtSWAITDOCUMENT.Text.Trim();
                double ndouble = 0.0;
                dr["SUMPOINT"] = double.TryParse((dr["SUMPOINT"] + "").Replace("-", ""), out ndouble) ? ndouble : 0.0;
                dr["DCREATE"] = ((dr["DCREATE"] + "" == "") ? DateTime.Today.AddDays(7) : ((DateTime)dr["DCREATE"]).AddDays(7));

                string mess = Validate(FieldType, string.Empty, dtSUPPLIANT);
                mess += ValidateFile(dgvUploadRequestFile);
                if (!string.IsNullOrEmpty(mess))
                {
                    alertFail(mess);
                    return;
                }
                ds = new DataSet("DS");
                dtSUPPLIANT.TableName = "DT";
                //dr = dt.Rows[0];

                ds.Tables.Add(dtSUPPLIANT.Copy());

                dt = AppealBLL.Instance.AppealVendorSave(hidSAPPEALID.Value, int.Parse(Session["UserID"].ToString()), ds, dtUpload);

                btnSave.Visible = false;
                if (dt.Rows.Count > 0)
                {
                    dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        if (dtSUPPLIANT.Rows[0]["SPROCESS"] + "" == "090")//เฉพาะอุบัติเหตุ
                        {
                            //CACTIVE = 10 เป็น ผขส. ยื่นอุทธรณ์ ดูใน table acc_status
                            AccidentBLL.Instance.UpdateStatus(dtSUPPLIANT.Rows[0]["SCHECKID"] + "", 10, Session["UserID"] + string.Empty);
                            SendEmail(ConfigValue.EmailAccident7, dtSUPPLIANT.Rows[0]["SCHECKID"] + "");
                        }
                    ////AppealBLL.Instance.UpdateStatusComplainBLL(DATA[11] + "", ConfigValue.DocStatus6, DATA[5] + "");

                    //Send Email แจ้งการอุทธรณ์

                            btnSave.Visible = false;
                            txtSWAITDOCUMENT.Text = "";
                            if (txtSWAITDOCUMENT.Text == "") {
                                btnSave.Visible = false;
                                this.SendEmail(ConfigValue.EmailComplainAppeal, dtSUPPLIANT.Rows[0]["SCHECKID"] + "");
                                alertSuccess("บันทึกสำเร็จ", "admin_suppliant_lst_New.aspx");
                            }
                            
                        
                      
                    }
                    else if (dr["MESSAGE"] + string.Empty == "CODE")
                    {
                        alertFail("ไม่สามารถเพิ่มสัญญาได้<br/>เนื่องจาก รหัส สัญญา ซ้ำ !!!");
                    }
                }
            
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    private void SendEmail(int TemplateID, string DocID)
    {
        btnSave.Visible = false;
        try
        {
            if (btnSave.Visible == false)
            {
                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
                DataTable dtComplainEmail = new DataTable();
                if (dtTemplate.Rows.Count > 0)
                {
                    string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                    string Body = dtTemplate.Rows[0]["BODY"].ToString();

                    if (TemplateID == ConfigValue.EmailComplainAppeal)
                    {
                        dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                        Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                        Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                        Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                        Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                        Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                        Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                        Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                        Body = Body.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());
                        string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_suppliant_lst.aspx";
                        Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                        //string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;
                        string CreateMail = string.Empty;
                        btnSave.Visible = false;
                        MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, false) + CreateMail, Subject, Body, "", "EmailComplainAppeal", ColumnEmailName);
                    }
                    else if (TemplateID == ConfigValue.EmailAccident7)
                    {
                        
                        #region EmailAccident7
                        string accID = DocID;
                        DataTable dt = AccidentBLL.Instance.AccidentTab1Select(accID);
                        DataRow dr = dt.Rows[0];
                        string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, false);
                        //if (!string.IsNullOrEmpty(EmailList))
                        //{
                        //    EmailList += ",";
                        //}

                        //EmailList += "TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com,TMS_nut.t@pttor.com,TMS_terapat.p@pttor.com,TMS_sarun.c@pttor.com";
                        //EmailList += "yutasak.c@pttor.com,kittipong.l@pttor.com,suphakit.k@pttor.com, apipat.k@pttor.com, nut.t@pttor.com, terapat.p@pttor.com, nataporn.c@pttor.com";
                        //EmailList += "raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com";
                        Subject = Subject.Replace("{ACCID}", accID);
                        Body = Body.Replace("{ACCID}", accID);
                        Body = Body.Replace("{DATE}", dr["ACCIDENT_DATE_PREVIEW"] + string.Empty);
                        Body = Body.Replace("{TIME}", dr["ACCIDENT_TIME_PREVIEW"] + string.Empty);
                        Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                        Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                        Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                        Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                        Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                        Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                        Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                        //Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                        Body = Body.Replace("{REMARK}", "");
                        Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                        Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);

                        byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                        string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                        string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_suppliant_lst.aspx?str=" + ID;
                        Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                        MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident7", ColumnEmailName);
                        #endregion
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    #region mpCancel_ClickOK
    protected void mpCancel_ClickOK(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "close", "javascript:window.close();", true);
    }
    #endregion
    #endregion

    #region UploadFile
    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        try
        {
            dtUpload = StartUpload(ref ddlUploadType, fileUpload1, dtUploadType, dtUploadRequestFile, dtUpload, ref dgvUploadFile, ref dgvUploadRequestFile);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);

            CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    #endregion

}
