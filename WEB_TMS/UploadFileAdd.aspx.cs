﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class UploadFileAdd : PageBase
{
    #region + View State +
    private int UploadID
    {
        get
        {
            if ((int)ViewState["UploadID"] != null)
                return (int)ViewState["UploadID"];
            else
                return 0;
        }
        set
        {
            ViewState["UploadID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadUploadType(string.Empty);

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["UPLOAD_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                UploadID = int.Parse(decryptedValue);

                DataTable dt = new DataTable();
                dt = UploadTypeBLL.Instance.UploadTypeSelectSearchBLL_Full(" AND UPLOAD_ID = " + UploadID.ToString());

                if (dt.Rows.Count > 0)
                {
                    ddlUploadType.SelectedValue = dt.Rows[0]["UPLOAD_TYPE_VALUE"].ToString();
                    ddlUploadType.Enabled = false;
                    txtUploadTypeName.Enabled = false;

                    dt = UploadTypeBLL.Instance.UploadTypeSelectAllBLL(" AND UPLOAD_ID = " + UploadID.ToString());
                    txtUploadTypeName.Text = dt.Rows[0]["UPLOAD_NAME"].ToString();
                    txtEntention.Text = dt.Rows[0]["EXTENTION"].ToString();
                    txtMaxFileSize.Text = dt.Rows[0]["MAX_FILE_SIZE"].ToString();
                    radVendorDownload.SelectedValue = dt.Rows[0]["IS_VENDOR_DOWNLOAD"].ToString();
                    radStatus.SelectedValue = dt.Rows[0]["ISACTIVE_VALUE"].ToString();
                    radCheckEndDate.SelectedValue = dt.Rows[0]["IS_CHECK_UPLOAD_END_DATE_IND"].ToString();
                    radCheckAlertType.SelectedValue = dt.Rows[0]["REQUIRE_DOC_ALERT_TYPE"].ToString();
                    hGetUploadType.Value = dt.Rows[0]["UPLOAD_TYPE_IND"].ToString();

                    if (string.Equals(dt.Rows[0]["SPECIAL_FLAG"].ToString(), "1"))
                    {//ธพ.น.2
                        rowTPN2Date.Visible = true;
                        txtTPN2Date.Text = dt.Rows[0]["SPECIAL_FLAG_VALUE"].ToString();
                    }
                    else
                        rowTPN2Date.Visible = false;

                    this.CheckEnableVendorDownload();
                }
            }
            else
            {
                txtEntention.Text = "*.*";
                radStatus.SelectedValue = "1";
                radCheckEndDate.SelectedValue = "0";
                radCheckAlertType.SelectedValue = "";
                radVendorDownload.SelectedValue = "0";
                this.CheckEnableVendorDownload();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadUploadType(string Condition)
    {
        try
        {
            DataTable dt = UploadTypeBLL.Instance.UploadTypeSelectSearchBLL(Condition);
            DropDownListHelper.BindDropDownList(ref ddlUploadType, dt, "UPLOAD_TYPE_VALUE", "UPLOAD_TYPE", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            int VendorDownload = 0;
            int AlertType = 0;
            if (radVendorDownload.Visible)
                VendorDownload = int.Parse(radVendorDownload.SelectedValue);
            if (radCheckEndDate.SelectedValue != "")
            {
                hGetUploadType.Value = radCheckEndDate.SelectedValue;
            }
            if (radCheckAlertType.SelectedValue != "")
            {
                AlertType = int.Parse(radCheckAlertType.SelectedValue);
            }
            if (ddlUploadType.Enabled)
                UploadTypeBLL.Instance.UpdateTypeInsertBLL2(ddlUploadType.SelectedValue, txtUploadTypeName.Text.Trim(), txtEntention.Text.Trim(), txtMaxFileSize.Text.Trim(), VendorDownload, int.Parse(radStatus.SelectedValue), int.Parse(Session["UserID"].ToString()), int.Parse(hGetUploadType.Value), int.Parse(radCheckEndDate.SelectedValue), AlertType);
            else
                UploadTypeBLL.Instance.UpdateTypeUpdateBLL3(ddlUploadType.SelectedValue, txtUploadTypeName.Text.Trim(), txtEntention.Text.Trim(), txtMaxFileSize.Text.Trim(), VendorDownload, int.Parse(radStatus.SelectedValue), int.Parse(Session["UserID"].ToString()), int.Parse(hGetUploadType.Value), int.Parse(radCheckEndDate.SelectedValue), AlertType, txtTPN2Date.Text.Trim());

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "UploadFile.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlUploadType.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก กลุ่มเอกสาร");

            if (string.Equals(txtUploadTypeName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ประเภทไฟล์อัพโหลด");

            if (string.Equals(txtEntention.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน นามสกุลที่รองรับ");

            if (string.Equals(txtMaxFileSize.Text.Trim(), string.Empty))
                throw new Exception("กรุณาป้อน ขนาดไฟล์สูงสุด (MB)");

            int tmp;
            if (!int.TryParse(txtMaxFileSize.Text.Trim(), out tmp))
                throw new Exception("กรุณาป้อน ขนาดไฟล์สูงสุด (MB) เป็นตัวเลขเท่านั้น");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadFile.aspx");
    }
    protected void ddlUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.CheckEnableVendorDownload();
    }

    private void CheckEnableVendorDownload()
    {
        try
        {
            if ((ddlUploadType.SelectedIndex < 1) || (!string.Equals(ddlUploadType.SelectedValue, "COMPLAIN_SCORE")))
            {
                lblVendorDownload.Visible = false;
                radVendorDownload.Visible = false;
            }
            else
            {
                lblVendorDownload.Visible = true;
                radVendorDownload.Visible = true;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}