﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AnnualReportEmailFormat.aspx.cs" ValidateRequest="false" EnableEventValidation="false" Inherits="AnnualReportEmailFormat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script src="Javascript/suyati/editor.js" type="text/javascript"></script>
    <link href="Javascript/suyati/editor.css" rel="stylesheet" />
    <link href="Javascript/suyati/font-awesome.min.css" rel="stylesheet" />
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <ul class="nav nav-tabs" runat="server" id="tabtest" clientidmode="Static">
        <li class="active" id="Tab1" runat="server" clientidmode="Static"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab" clientidmode="Static">แบบฟอร์มเอกสาร</a></li>
        <li class="" id="Tab2" runat="server" clientidmode="Static"><a href="#TabGroup" data-toggle="tab" aria-expanded="false" runat="server" id="ProcessTab" clientidmode="Static">แบบฟอร์มอีเมล์</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane active" id="TabGeneral" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                    <asp:PostBackTrigger ControlID="btnSaveForm" />
                </Triggers>
                <ContentTemplate>
                    <center>
                        <h5>แบบฟอร์มเอกสารแจ้งผลการประเมินประจำปี</h5>
                    </center>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 nopadding">
                                        <textarea id="txtEditor" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-horizontal row">
                            <div style="text-align: right;">
                                <div style="display:none;">
                                <asp:Button ID="btnExport" runat="server" Text="Export Word" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" OnClientClick="SetVal()" /></div>
                                <asp:Button ID="btnSaveForm" runat="server" Text="Save File Format" CssClass="btn btn-hover btn-info" OnClick="btnSaveForm_Click" OnClientClick="SetVal()" />
                            </div>
                        </div>
                    </div>
                    <div id="DvSource" runat="server"></div>
                    <input type="hidden" clientidmode="Static" id="hdnDivContents" runat="server">
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="TabGroup" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveMail" />
                </Triggers>
                <ContentTemplate>
                     <center>
                        <h5>แบบฟอร์มอีเมล์แจ้งผลการประเมินประจำปี</h5>
                    </center>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 nopadding">
                                        <textarea id="txtEmailContent" runat="server"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group form-horizontal row">
                            <div style="text-align: right;">
                                <asp:Button ID="btnSaveMail" runat="server" Text="Save E-mail" CssClass="btn btn-hover btn-info" OnClick="btnSaveMail_Click" OnClientClick="SetValEmail()" />
                            </div>
                        </div>
                    </div>
                    <div id="Div1" runat="server"></div>
                    <input type="hidden" clientidmode="Static" id="hdnEmailContent" runat="server">
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtEditor.ClientID%>").Editor();
            $("#<%=txtEmailContent.ClientID%>").Editor();

            $("#<%=txtEmailContent.ClientID%>").Editor("setText", $("#hdnEmailContent").val());

            $("#<%=txtEditor.ClientID%>").Editor("setText", $("#hdnDivContents").val());
        });


        function SetTextInEdit() {

        }

        function SetVal() {
            var txt = $("#<%=txtEditor.ClientID%>").Editor("getText");
            $("#hdnDivContents").val(txt);
        }

        function SetValEmail() {
            var txt = $("#<%=txtEmailContent.ClientID%>").Editor("getText");
            $("#hdnEmailContent").val(txt);
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

