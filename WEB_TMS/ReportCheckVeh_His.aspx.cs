﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.XtraPrinting;
using System.Drawing.Printing;
using System.Globalization;


public partial class ReportCheckVeh_His : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Events
        gvwVehHis.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwVehHis_CustomColumnDisplayText);
        gvwVehHis.DataBinding += new EventHandler(gvwVehHis_DataBinding);
        #endregion
        if (!IsPostBack)
        {
            lblReport.Text = GetMessage();
            BindGrid();
        }
    }
    protected void xcpn_Callback(object sender, CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch ("" + paras[0])
        {
            case "SEARCH":
                lblReport.Text = GetMessage();
                BindGrid();
                break;
            case "CLEARSEARCH":
                txtKeyword.Text = "";
                dteStart.Value = "";
                dteEnd.Value = "";
                lblReport.Text = GetMessage();
                BindGrid();
                break;
        }
    }
    protected void gvwVehHis_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void gvwVehHis_DataBinding(object sender, EventArgs e)
    {
        if (ViewState["vsVehicle"] != null)
            gvwVehHis.DataSource = (DataTable)ViewState["vsVehicle"];
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WritePdfToResponse();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WriteXlsToResponse();
    }
    protected void btnResult_Click(object sender, EventArgs e)
    {
        ASPxButton __btn = (ASPxButton)sender;
        if (!string.IsNullOrEmpty("" + __btn.CommandArgument))
            PrintDoc("" + __btn.CommandArgument);
    }
    #region Other Function
    private void PrintDoc(string REQ_ID)
    {
        #region Prepare SOURCE

        string _sql = "";

        _sql = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,TRU.SHEADREGISTERNO As REG_NO,TRU.SCERT_NO As SCERT_NO,TRU.SENGINE,TRU.SCHASIS,TYP.SCARCATEGORY
,CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.VEH_NO END As HTRAILER
,(CASE WHEN TRU.SCARTYPEID IN ('3','4') THEN (SELECT SBRAND FROM TTRUCK WHERE STRUCKID=REQ.STRUCKID AND ROWNUM=1) ELSE TRU.SBRAND END) As SBRAND
,CASE WHEN NVL(REQ.TU_CHASSIS,'')<>'' THEN REQ.VEH_CHASSIS END As VEH_CHASSIS
,REQ.TOTLE_SLOT,REQ.TOTLE_CAP,TRU.NWEIGHT,TRU.NLOAD_WEIGHT,TRU.DLAST_SERV,TRU.NTANK_HIGH_HEAD,TRU.NTANK_HIGH_TAIL,TRU.STANK_MATERAIL
,(SELECT SUM(NCAPACITY) FROM TTRUCK_COMPART WHERE STRUCKID=TRU.STRUCKID) As TCAPACITY
FROM TBL_REQUEST REQ 
LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
LEFT JOIN TTRUCKTYPE TYP ON TRU.SCARTYPEID=TYP.SCARTYPEID
LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'";

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        dt.TableName = "dtReport";

        _sql = @"SELECT REQ.REQUEST_ID,COMP.NCOMPARTNO,COMP.NPANLEVEL,COMP.NCAPACITY FROM TBL_REQUEST REQ
                 LEFT JOIN TTRUCK_COMPART COMP ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=COMP.STRUCKID
                 WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                 ORDER BY COMP.NCOMPARTNO,COMP.NPANLEVEL";
        DataTable dt2 = CommonFunction.Get_Data(conn, _sql);
        dt2.TableName = "dtCompart";

        dsReportRequesting ds = new dsReportRequesting();
        ds.dtReport.Merge(dt);
        ds.dtCompart.Merge(dt2);

        #endregion

        xrtReportRequesting report = new xrtReportRequesting();
        report.DataSource = ds;
        report.Parameters["sDate"].Value = DateTime.Now.Date;
        report.Parameters["Auditor"].Value = "(นายอำนาจ  บุญเรือง)";
        report.Parameters["Position"].Value = "ช่างเทคนิค";

        #region Replace Font
        PrivateFontCollection fontColl = new PrivateFontCollection();
        fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

        foreach (Band b in report.Bands)
        {
            foreach (XRControl c in b.Controls)
            {
                c.Font = new Font(fontColl.Families[0].Name, 14F, c.Font.Style);
            }
        }
        #endregion
        string fileName = "FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    private void BindGrid()
    {
        string _sql = "", _condi = "";

        if (txtKeyword.Text.Trim() != "")
            _condi += string.Format(@" AND (REQ.VEH_NO LIKE '%{0}%' OR REQ.TU_NO LIKE '%{0}%' OR VEN.SABBREVIATION LIKE '%{0}%')", txtKeyword.Text.Trim());
        if ("" + dteStart.Value != "" && "" + dteEnd.Value != "")
            _condi += string.Format(@" AND REQ.SERVICE_DATE BETWEEN TO_DATE('{0}', 'yyyy/mm/dd') AND TO_DATE('{1}', 'yyyy/mm/dd')"
                                    , Convert.ToDateTime("" + dteStart.Value).ToString("yyyy/MM/dd", new CultureInfo("en-US"))
                                    , Convert.ToDateTime("" + dteEnd.Value).ToString("yyyy/MM/dd", new CultureInfo("en-US")));
        else if ("" + dteStart.Value != "" && "" + dteEnd.Value == "")
            _condi += string.Format(@" AND REQ.SERVICE_DATE >= TO_DATE('{0}', 'yyyy/mm/dd')", Convert.ToDateTime("" + dteStart.Value).ToString("yyyy/MM/dd", new CultureInfo("en-US")));
        if ("" + dteStart.Value == "" && "" + dteEnd.Value != "")
            _condi += string.Format(@" AND REQ.SERVICE_DATE <= TO_DATE('{0}', 'yyyy/mm/dd')", Convert.ToDateTime("" + dteEnd.Value).ToString("yyyy/MM/dd", new CultureInfo("en-US")));

        _sql = @"SELECT REQ.REQUEST_ID,REQ.VEH_NO,REQ.TU_NO,REQ.VENDOR_ID,VEN.SABBREVIATION,REQ.REQTYPE_ID,TYP.REQTYPE_NAME,REQ.CAUSE_ID,CAU.CAUSE_NAME,REQ.SERVICE_DATE
FROM TBL_REQUEST REQ LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID LEFT JOIN TBL_REQTYPE TYP ON REQ.REQTYPE_ID=TYP.REQTYPE_ID LEFT JOIN TBL_CAUSE CAU ON REQ.CAUSE_ID=CAU.CAUSE_ID
WHERE 1=1 " + _condi;
        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        ViewState["vsVehicle"] = dt;
        gvwVehHis.DataBind();
    }
    private void PreparePrinting()
    {
        PrivateFontCollection fontColl = new PrivateFontCollection();
        fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

        BindGrid();
        gridExport.GridView.Columns[7].Visible = false;
        gridExport.Landscape = true;
        gridExport.PaperKind = PaperKind.A4;
        gridExport.PageHeader.Center = GetMessage();
        gridExport.PageHeader.Font.Name = fontColl.Families[0].Name;
        gridExport.PageHeader.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.PageHeader.Font.Bold = true;
        gridExport.TopMargin = 1;
        gridExport.RightMargin = 1;
        gridExport.BottomMargin = 1;
        gridExport.LeftMargin = 1;
        gridExport.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.True;
        gridExport.Styles.Default.Font.Name = fontColl.Families[0].Name;
        gridExport.Styles.Default.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.Styles.Header.Font.Bold = true;

        gridExport.FileName = "รายงานผลตรวจสอบรายคันประวัติเก่า_" + DateTime.Now.ToString("MMddyyyyHHmmss");
    }
    private string GetMessage()
    {
        DateTime dStart = (!string.IsNullOrEmpty("" + dteStart.Value) ? Convert.ToDateTime("" + dteStart.Value) : new DateTime());
        DateTime dEnd = (!string.IsNullOrEmpty("" + dteEnd.Value) ? Convert.ToDateTime("" + dteEnd.Value) : new DateTime());
        string sMessage = "", sCondi = "";

        if (txtKeyword.Text.Trim() != "" && (!string.IsNullOrEmpty("" + dteStart.Value) || !string.IsNullOrEmpty("" + dteEnd.Value)))
            sCondi = string.Format(@"คำค้น :{0} ,ช่วงวันที่ {1:d/M/yyyy} - {2:d/M/yyyy}", txtKeyword.Text, dStart, dEnd);
        else if (txtKeyword.Text.Trim() != "")
            sCondi = string.Format(@"คำค้น :{0} ", txtKeyword.Text);
        else if (!string.IsNullOrEmpty("" + dteStart.Value) || !string.IsNullOrEmpty("" + dteEnd.Value))
            sCondi = string.Format(@"ช่วงวันที่ {0:d/M/yyyy} - {1:d/M/yyyy}", dStart, dEnd);

        sMessage = string.Format(@"รายงานผลตรวจสอบรายคัน ประวัติเก่า {1} (จัดทำรายงานวันที่ {0:d/M/yyyy})", DateTime.Now.Date, (sCondi == "" ? @"""ทั้งหมด""" : sCondi));

        return sMessage;
    }
    #endregion
}