﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    EnableViewState="true" CodeFile="admin_Complain_add.aspx.cs" Inherits="admin_Complain_add"
    StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/cButtonDelete.ascx" TagPrefix="uc2" TagName="cButtonDelete" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphIE" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=11" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script type="text/javascript">
        function closeModal() {
            $('#ShowDriverSearch').modal('hide');
        }

        function closeModalDeliveryNo() {
            $('#ShowDeliveryNoSearch').modal('hide');
        }

        function closeModalDeliveryNoAll() {
            $('#ShowDeliveryNoSearchAll').modal('hide');
        }

        function closeModalCar() {
            $('#ShowCarSearch').modal('hide');
        }

        function closeModalOTP() {
            $('#ShowOTP').modal('hide');
        }
    </script>
    <%--<asp:Panel ID="pnlMain" runat="server" EnableViewState="true"></asp:Panel>--%>
    <%--<asp:Button ID="cmdTest" runat="server" Text="Get" onclick="cmdTest_Click" />--%>
    <br />
    <br />
    <ul class="nav nav-tabs" runat="server" id="tabtest">
        <li class="active" id="Tab1" runat="server"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab">ข้อมูลทั่วไป</a></li>
        <li class="" id="Tab2" runat="server"><a href="#TabProcess" data-toggle="tab" aria-expanded="false"
            runat="server" id="ProcessTab">เอกสารหลักฐาน</a></li>
        <li class="" id="Tab3" runat="server"><a href="#TabFinal" data-toggle="tab" aria-expanded="false"
            runat="server" id="FinalTab">ผลการพิจารณา</a></li>
        <li class="" id="Tab4" runat="server"><a href="#TabChangeStatus" data-toggle="tab"
            aria-expanded="false" runat="server" id="ChangeStatusTab">เปลี่ยนสถานะเอกสาร</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="ข้อมูลทั่วไป"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="tblAddHeader" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>หมายเลขเอกสาร</asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtDocID" runat="server" style="width: 250px; text-align: center"
                                                    readonly="readonly" value="Generate by System" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:Label ID="lblEditDate" runat="server" Text="(เอกสารนี้สามารถแก้ไขได้ ภายในวันที่&nbsp;{0})"
                                                    Visible="false" ForeColor="Blue" Font-Underline="true"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ประเภทร้องเรียน
                                                <asp:Label ID="lblReqcmbTopic" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cmbTopic" runat="server" class="form-control" DataTextField="TOPIC_NAME"
                                                    DataValueField="TOPIC_ID" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="cmbTopic_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell>
                                                คลังต้นทาง
                                                <asp:Label ID="lblReqcbxOrganiz" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cbxOrganiz" runat="server" class="form-control" DataTextField="STERMINALNAME"
                                                    DataValueField="STERMINALID" Width="250px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                หัวข้อร้องเรียน
                                                <asp:Label ID="lblReqcboComplainType" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboComplainType" runat="server" class="form-control" DataTextField="COMPLAIN_TYPE_NAME"
                                                    DataValueField="COMPLAIN_TYPE_ID" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="cboComplainType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:CheckBox ID="chkLock" runat="server" Text="หยุดพักการขนส่งชั่วคราว (พขร.)" ForeColor="Red" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                บริษัทผู้ประกอบการขนส่ง
                                                <asp:Label ID="lblReqcboVendor" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboVendor" runat="server" class="form-control" DataTextField="SVENDORNAME"
                                                    AutoPostBack="true" DataValueField="SVENDORID" Width="250px" OnSelectedIndexChanged="cboVendor_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>ประเภทผลิตภัณฑ์</asp:TableCell>
                                            <asp:TableCell>
                                                <dx:ASPxRadioButtonList ID="rblTypeProduct" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="น้ำมัน" Value="O" />
                                                        <dx:ListEditItem Text="ก๊าซ" Value="G" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                เลขที่สัญญา
                                                <asp:Label ID="lblReqcboContract" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboContract" runat="server" class="form-control" DataTextField="SCONTRACTNO"
                                                    AutoPostBack="true" DataValueField="SCONTRACTID" Width="250px" OnSelectedIndexChanged="cboContract_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                Delivery No
                                                <asp:Label ID="lblReqcboDelivery" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboDelivery" runat="server" class="form-control" DataTextField="SDELIVERYNO"
                                                                DataValueField="SDELIVERYNO" Width="215px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>&nbsp;<asp:Button ID="cmdDeliveryNoSearch" runat="server" Text="..." Width="30px"
                                                            OnClick="cmdDeliveryNoSearch_Click" />
                                                        </td>
                                                        <td>&nbsp;<asp:Button ID="cmdDOSearhAll" runat="server" Text="ค้นหาข้อมูลจากเลข DO" Width="145px" UseSubmitBehavior="false" OnClick="cmdDOSearchAll_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--<asp:TextBox ID="txtDelivery" runat="server"></asp:TextBox>--%>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ทะเบียนรถ (หัว)
                                                <asp:Label ID="lblReqcboHeadRegist" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboHeadRegist" runat="server" class="form-control" DataTextField="SHEADREGISTERNO"
                                                                AutoPostBack="true" DataValueField="STRUCKID" Width="215px" OnSelectedIndexChanged="cboHeadRegist_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>&nbsp;<asp:Button ID="cmdCarSearch" runat="server" Text="..." Width="30px" OnClick="cmdCarSearch_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell> ทะเบียนรถ (ท้าย) </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" id="txtTrailerRegist" runat="server" disabled="disabled"
                                                                style="width: 250px"  value=""/>
                                                        </td>
                                                        <td style="padding-left: 4px;">
                                                            <asp:Button ID="btnSearchByCarLicense" runat="server" Text="ค้นหาจากทะเบียนรถ" OnClick="btnSearchByCarLicense_Click" />
                                                            <asp:Button OnClick="btnSearchLicenseReset_Click" ID="btnSearchLicenseReset" runat="server" Text="x" />&nbsp;&nbsp;
                                                            <asp:CheckBox ID="chkTruckOutContract" runat="server" Text="แสดงรถที่นำออกจากสัญญา" AutoPostBack="true" OnCheckedChanged="chkTruckOutContract_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน
                                                <asp:Label ID="lblReqcmbPersonalNo" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbPersonalNo" runat="server" CssClass="form-control" DataTextField="FULLNAME"
                                                                DataValueField="SPERSONELNO" Width="215px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>&nbsp;<asp:Button ID="cmdDriverSearch" runat="server" Text="..." Width="30px" OnClick="cmdDriverSearch_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                สถานที่เกิดเหตุ
                                                <asp:Label ID="lblReqtxtComplainAddress" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtComplainAddress" runat="server" style="width: 250px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                วันที่เกิดเหตุ
                                                <asp:Label ID="lblReqtxtDateTrans" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                <asp:TextBox ID="txtDateTrans" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                จำนวนรถ (คัน)
                                                <asp:Label ID="lblReqtxtTotalCar" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtTotalCar" runat="server" style="width: 250px; text-align: center" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ปลายทาง
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                <asp:TextBox ID="txtShipTo" runat="server" CssClass="form-control" TextMode="MultiLine" Height="90px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <div style="width: 99%">
                                        <asp:GridView ID="dgvHeader" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowDeleting="dgvHeader_RowDeleting"
                                            OnRowCommand="dgvHeader_RowCommand" OnRowDataBound="dgvHeader_RowDataBound" >
                                            <Columns>
                                                <asp:BoundField DataField="TopicID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TopicName" HeaderText="ประเภทร้องเรียน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ComplainID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ComplainName" HeaderText="หัวข้อร้องเรียน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ContractID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ContractName" HeaderText="เลขที่สัญญา">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CarHead" HeaderText="ทะเบียนรถ(หัว)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TotalCar" HeaderText="จำนวนรถ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DeliveryDate" HeaderText="วันที่เกิดเหตุ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgViewComplain" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                Width="16px" Height="16px" Style="cursor: pointer" CommandName="Select" />&nbsp;
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/btnEdit.png" Width="16px"
                                                                Height="16px" Style="cursor: pointer" CommandName="EditHeader" />&nbsp;
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgDeleteComplain" runat="server" ImageUrl="~/Images/bin1.png"
                                                                Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" />&nbsp;
                                                        </div>
                                                        <%--<uc2:cButtonDelete runat="server" ID="cButtonDelete" OnClickDelete="dgvHeader_RowDeleting2" />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <div style="text-align: left">
                                <asp:Label ID="lblDOShow" runat="server" Text="* กรณีไม่พบ DO จากการค้นหา ให้ระบุในช่อง สาเหตุ / รายละเอียด"></asp:Label>
                            </div>
                            <asp:Button ID="cmdAddHeader" runat="server" Text="เพิ่มรายการ" class="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Width="100px" OnClick="cmdAddHeader_Click" />
                            &nbsp;
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" class="btn btn-md btn-hover btn-danger"
                                UseSubmitBehavior="false" Style="width: 80px" OnClick="cmdCancel_Click" Visible="false" />
                        </div>
                    </div>
<div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>รายละเอียดและสาเหตุ
                            </div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <asp:Table ID="Table2" Width="100%" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    ชื่อผู้ร้องเรียน
                                                    <asp:Label ID="lblReqtxtFName" runat="server" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox CssClass="form-control" ID="txtFName" runat="server" Width="250px" />
                                                </asp:TableCell><asp:TableCell>
                                    &nbsp;
                                                </asp:TableCell><asp:TableCell>
                                    หน่วยงาน
                                    <font color="#ff0000">&nbsp;*</font>
                                                </asp:TableCell><asp:TableCell>
                                                    <asp:TextBox CssClass="form-control" ID="txtFNameDivision" runat="server" Width="250px" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    วันที่รับร้องเรียน
                                                    <asp:Label ID="lblReqtxtDateComplain" runat="server" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                </asp:TableCell><asp:TableCell Width="100px">
                                                    <asp:TextBox runat="server" CssClass="datepicker" ID="txtDateComplain" Style="text-align: center"></asp:TextBox>
                                                </asp:TableCell><asp:TableCell>
                                        &nbsp;
                                                </asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                            
                                                </asp:TableCell>
                                                <asp:TableCell Width="100px"></asp:TableCell>
                                                <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>

                                ชื่อผู้บันทึก
                                                </asp:TableCell><asp:TableCell>
                                                    <input class="form-control" id="txtComplainBy" runat="server" style="width: 250px;"
                                                        disabled="disabled" />
                                                </asp:TableCell><asp:TableCell>
                                &nbsp;
                                                </asp:TableCell><asp:TableCell>
                                หน่วยงาน
                                                </asp:TableCell><asp:TableCell>
                                                    <input class="form-control" id="txtComplainDivisionBy" runat="server" style="width: 250px;"
                                                        disabled="disabled" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                            วันที่สร้างเอกสาร
                                                </asp:TableCell><asp:TableCell Width="100px">
                                                    <asp:TextBox runat="server" CssClass="datetimepicker" ID="txtCreateDate" Enabled="false"></asp:TextBox>
                                                </asp:TableCell><asp:TableCell>
                                        &nbsp;
                                                </asp:TableCell><asp:TableCell>

                                                </asp:TableCell><asp:TableCell>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                    สาเหตุ / รายละเอียด <font color="#ff0000">&nbsp;*</font>
                                                </asp:TableCell><asp:TableCell ColumnSpan="4">
                                                    <textarea id="txtDetail" runat="server" cols="1" rows="20" class="form-control" style="width: 100%; height: 260px;"></textarea>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right">
                                <asp:Button ID="cmdSaveTab1" runat="server" Enabled="false" Text="ดำเนินการหน้าถัดไป"
                                    CssClass="btn btn-md btn-hover btn-info" Style="width: 150px" data-toggle="modal"
                                    data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" />
                                &nbsp;
                                <asp:Button ID="cmdCancelDoc" runat="server" Enabled="false" Text="ยกเลิกเอกสาร"
                                    CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" data-toggle="modal"
                                    UseSubmitBehavior="false" data-target="#ModalConfirmBeforeCancel" />
                                &nbsp;
                                <asp:Button ID="cmdCloseTab1" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info"
                                    UseSubmitBehavior="false" OnClick="cmdCloseTab1_Click" Style="width: 100px" />
                            </div>
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <div class="tab-pane fade" id="TabProcess">
            <br />
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>การดำเนินการ และ วิธีการป้องกัน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table4" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                การดำเนินการ<font color="#ff0000">*</font>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:TextBox CssClass="form-control" ID="txtOperate" runat="server" Width="100%"
                                            Height="75px" TextMode="MultiLine" />
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                วิธีการป้องกันการเกิดปัญหาซ้ำ
                                    </asp:TableCell><asp:TableCell>
                                        <asp:TextBox CssClass="form-control" ID="txtProtect" runat="server" Width="100%"
                                            Height="75px" TextMode="MultiLine" />
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:Label ID="lblEmployeeTab2" runat="server" Text="พนักงานผู้รับผิดชอบ"></asp:Label>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:TextBox CssClass="form-control" ID="txtsEmployee" runat="server" Width="100%" />
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:Label ID="lblCCEmail" runat="server" Text="CC Email"></asp:Label>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:TextBox CssClass="form-control" ID="txtCCEmail" runat="server" Width="100%" />
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                    </asp:TableCell><asp:TableCell>
                                        &nbsp;<asp:Label ID="lblMailDesc" runat="server" Text="ในกรณีที่มีหลายอีเมล์ ให้คั่นด้วย , เช่น Email1@ptt.com,Email2@ptt.com,Email3@ptt.com"></asp:Label>
                                    </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                    &nbsp; </div></div><div id="pnlPTT" runat="server">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ) </div><div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                    AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                    <Columns>
                                        <asp:BoundField DataField="COMPLAIN_TYPE_NAME" HeaderText="หัวข้อ">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COMPLAIN_TYPE_ID" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: left">
                        &nbsp;* กรณีเอกสารแนบเป็นไฟล์เดียวกัน สามารถแนบเอกสารเพียงครั้งเดียวได้ </div><div class="panel-footer" style="text-align: left">
                        &nbsp;* ถ้าเอกสารแนบเป็นคนละไฟล์ แต่มีชื่อประเภทไฟล์เอกสารตรงกัน กรุณาตั้งชื่อไฟล์
                    (ตามผู้ใช้งาน) ให้แตกต่างกัน </div></div><div class="panel panel-info">
                </div>
                <div class="panel-heading">
                    <i class="fa fa-table"></i>แนบเอกสาร หลักฐาน </div><div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table3" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:Label ID="lblUploadTypeTab3" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:FileUpload ID="fileUpload" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                    </asp:TableCell></asp:TableRow></asp:Table><br />
                            <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSaveTab2" runat="server" Enabled="false" Text="ส่งเรื่องพิจารณา"
                        UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdSaveTab2_Click"
                        Style="width: 120px" />
                    &nbsp; <asp:Button ID="cmdSaveTab2Draft" runat="server" Enabled="false" Text="บันทึก (ร่าง)"
                        UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="cmdSaveTab2Draft_Click"
                        Style="width: 100px" />
                    &nbsp; </div><br />
                <dx:ASPxDateEdit ID="dteFinish" runat="server" SkinID="xdte" Visible="false">
                </dx:ASPxDateEdit>
            </div>
        </div>
        <div class="tab-pane fade" id="TabFinal">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblCusScoreHeader" runat="server" Text="การตัดคะแนน (เลขที่สัญญา {0})"></asp:Label></div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table111" Width="100%" runat="server">
                                        <asp:TableRow ID="Row1" runat="server">
                                            <asp:TableCell Width="15%">&nbsp;</asp:TableCell><asp:TableCell Width="25%" ColumnSpan="2"> <asp:CheckBox ID="chkSaveTab3Draft" runat="server" Text="รับเรื่องรอพิจารณา" ForeColor="Red" />
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="Row2" runat="server">
                                            <asp:TableCell Width="15%"> <div id="divTopic" runat="server">
                                                    หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font> </div></asp:TableCell><asp:TableCell Width="25%" ColumnSpan="4"> <asp:DropDownList ID="cboScore" runat="server" class="form-control" DataTextField="STOPICNAME"
                                                    OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                    AutoPostBack="true" Width="100%">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="Row3" runat="server">
                                            <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell ColumnSpan="4"> <asp:Label ID="lblShowOption" runat="server" Text="[พขร. ทุจริต]" Visible="false"
                                                    ForeColor="Red"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="Row4" runat="server">
                                            <asp:TableCell> <asp:Label ID="lblScore1" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore1" runat="server" Width="200px"
                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore1_OnKeyPress');" />
                                                </asp:TableCell><asp:TableCell
                                                    Width="20%">&nbsp;</asp:TableCell><asp:TableCell Width="15%"> <asp:Label ID="lblScore2" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell
                                                            Width="25%"> <asp:TextBox CssClass="form-control" ID="txtScore2" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore2_OnKeyPress');" />
                                                        </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="Row5" runat="server">
                                            <asp:TableCell> <asp:Label ID="lblScore3" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore3" runat="server" Width="200px"
                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore3_OnKeyPress');" />
                                                </asp:TableCell><asp:TableCell
                                                    Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="lblScore4" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore4" runat="server" Width="200px"
                                                                Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore4_OnKeyPress');" />
                                                        </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="rowListCarTab3" Visible="false">
                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;"> <asp:GridView ID="dgvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                    OnCheckedChanged="chkChooseScoreAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemStyle Width="36px" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TRUCKID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CARHEAD" HeaderText="ทะเบียนรถ (หัว)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CARDETAIL" HeaderText="ทะเบียนรถ (หาง)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="rowListTotalCarTab3" runat="server" Visible="false">
                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;"> <asp:GridView ID="dgvScoreTotalCar" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                    OnCheckedChanged="chkChooseScoreListCarAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemStyle Width="36px" />
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TOTAL_CAR" HeaderText="จำนวนรถ (ตามเอกสาร)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="จำนวนรถ (หักคะแนน)">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtChooseTotalCar" runat="server" Width="120px"></asp:TextBox></ItemTemplate></asp:TemplateField></Columns><EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="rowTotalCarTab3" Visible="false">
                                            <asp:TableCell> <asp:Label ID="Label12" runat="server" Text="จำนวนรถ (คัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtTotalCarTab3" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowOil" Visible="false">
                                            <asp:TableCell> <asp:Label ID="Label10" runat="server" Text="ค่าน้ำมัน / ลิตร"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtOilPrice" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilPrice_OnKeyPress');"
                                                        CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label11" runat="server" Text="จำนวนน้ำมัน (ลิตร)"></asp:Label></asp:TableCell><asp:TableCell
                                                    Width="20%"> <asp:TextBox ID="txtOilQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilQty_OnKeyPress');"
                                                        CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow runat="server" ID="rowZeal" Visible="false">
                                            <asp:TableCell> <asp:Label ID="lblZeal1" runat="server" Text="ค่าปรับต่อ 1 ซิล"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtZeal" runat="server" Enabled="false" CssClass="form-control"
                                                        Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label9" runat="server" Text="จำนวน ซิล"></asp:Label></asp:TableCell><asp:TableCell
                                                    Width="20%"> <asp:TextBox ID="txtZealQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtZealQty_OnKeyPress');"
                                                        CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow ID="Row6" runat="server">
                                            <asp:TableCell> <asp:Label ID="lblCost" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="200px" ReadOnly="true"
                                                        Enabled="false" Style="text-align: center" />
                                                </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="lblDisableDriverDay" runat="server" Text="ระงับ พชร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtDisableDriver" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="Row7" runat="server">
                                            <asp:TableCell> <asp:Label ID="lblScore5" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore5" runat="server" Width="200px"
                                                        Enabled="false" Style="text-align: center" />
                                                </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="lblScore6" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore6" runat="server" Width="200px"
                                                            Enabled="false" Style="text-align: center" />
                                                    </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell ColumnSpan="5"> <asp:GridView ID="dgvScoreList" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    OnRowDeleting="dgvScore_RowDeleting" OnRowCommand="dgvScoreList_RowCommand" AlternatingRowStyle-BackColor="White"
                                                    RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:BoundField DataField="STOPICID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="430px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgViewScore" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Select" />&nbsp; 
                                                                <asp:ImageButton ID="imgDeleteScore" runat="server" ImageUrl="~/Images/bin1.png"
                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdAddScore" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px"
                                UseSubmitBehavior="false" runat="server" Text="เพิ่มรายการ" OnClick="cmdAddScore_Click" />
                            <br />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="Label2" runat="server" Text="เอกสารหลักฐานที่แนบ"></asp:Label></div><div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="dgvUploadFile_flag" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false" OnRowUpdating="dgvUploadFile_flag_RowUpdating" OnRowDataBound="dgvUploadFile_flag_RowDataBound" OnRowCreated="dgvUploadFile_flag_RowCreated" OnRowCommand="dgvUploadFile_flag_RowCommand"
                                EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH,IS_VENDOR_VIEW,F_ID" ForeColor="#333333" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px" Height="25px" Style="cursor: pointer" CommandName="Update" />
                                            <asp:LinkButton ID="lnkDownload" runat="server" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' Text="Download" CommandName="Download" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Download (ผขส)" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="grvHeaderFlag">
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkShowToVendor" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="lblEmptyData" runat="server" Text="[ ไม่มีข้อมูล ]" />
                                </EmptyDataTemplate>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <%--<PagerTemplate>
                            <div class="left">
                                <asp:Label ID="lblTotalCount" runat="server" /> 
                                รายการ (แสดง <asp:TextBox ID="txtPageSize" runat="server" Width="50px" 
                                    AutoPostBack="true" OnTextChanged="txtPageSize_TextChanged" onkeypress="return validateNumber(event);" /> รายการ/หน้า)
                            </div>
                            <div class="right">
                                <asp:LinkButton ID="btnFirst" runat="server" CssClass="page-first" CommandArgument="First" CommandName="Page" 
                                    OnCommand="Paginate" value="" /> <asp:LinkButton ID="btnPrev" runat="server" CssClass="page-prev"
                                    CommandArgument="prev" CommandName="Page" OnCommand="Paginate" value="" /> Page
                                <asp:TextBox ID="txtPage" runat="server" OnTextChanged="txtPage_TextChanged"
                                    Width="50px" onkeypress="return validateNumber(event);" onBlur="return validatePageNo($jq(this));" />
                                    of <asp:Label ID="lblPageCount" runat="server" />
                                <asp:LinkButton ID="btnNext" runat="server" CssClass="page-next" value=""
                                    CommandArgument="next" CommandName="Page" OnCommand="Paginate" /> <asp:LinkButton ID="btnLast" runat="server" 
                                    CssClass="page-last" CommandArgument="last" CommandName="Page" OnCommand="Paginate" value="" />
                            </div>
                            <div class="clear"></div>
                        </PagerTemplate>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblAttachFileTab3" runat="server" Text="แนบเอกสารพิจารณาโทษ - อื่นๆ"></asp:Label></div><div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table5" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%"> ประเภทไฟล์เอกสาร </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadTypeTab3" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:FileUpload ID="fileUploadTab3" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUploadTab3" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" OnClick="cmdUploadTab3_Click" Style="width: 100px" />
                                    </asp:TableCell></asp:TableRow></asp:Table><br />
                            <asp:GridView ID="dgvUploadFileTab3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" DataKeyNames="UPLOAD_ID,FULLPATH"
                                OnRowDeleting="dgvUploadFileTab3_RowDeleting" OnRowUpdating="dgvUploadFileTab3_RowUpdating"
                                OnRowDataBound="dgvUploadFileTab3_RowDataBound">
                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; 
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info" id="divSecond" runat="server" visible="false">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลการอุทธรณ์ </div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table1" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="lblComplainDate" runat="server" Text="พิจารณาแล้ววันที่"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtComplainDate" runat="server" CssClass="form-control" Width="240px"
                                                        Style="text-align: center" Enabled="false"></asp:TextBox></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="lblSecondDate" runat="server" Text="อุทธรณ์ได้ภายในวันที่"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox ID="txtSecondDate" runat="server" CssClass="form-control" Width="240px"
                                                        Style="text-align: center" Enabled="false"></asp:TextBox></asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                            <br />
                        </div>
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info" id="divScore" runat="server">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>สรุปผลการพิจารณา </div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell> <div class="panel panel-info" id="div1" runat="server">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-table"></i>ก่อนอุทธรณ์ </div><div class="panel-body">
                                                        <div class="dataTable_wrapper">
                                                            <div class="panel-body">
                                                                <asp:Table runat="server" Width="100%">
                                                                    <asp:TableRow ID="Row8" runat="server">
                                                                        <asp:TableCell> <asp:Label ID="lblSentencerType" runat="server" Text="พิจารณาโดย"></asp:Label></asp:TableCell><asp:TableCell> <asp:RadioButtonList ID="radSentencerType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                    OnSelectedIndexChanged="radSentencerType_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="0" Text="ทีมงาน รข."></asp:ListItem><asp:ListItem Value="1" Text="คทง. พิจารณาโทษฯ"></asp:ListItem></asp:RadioButtonList></asp:TableCell></asp:TableRow><asp:TableRow ID="Row9" runat="server">
                                                                        <asp:TableCell> <asp:Label ID="lblSentencer" runat="server" Text="คณะกรรมการพิจารณาโทษ"></asp:Label></asp:TableCell><asp:TableCell> <asp:Table runat="server">
                                                                                    <asp:TableRow>
                                                                                        <asp:TableCell> <asp:DropDownList ID="ddlSentencer" runat="server" class="form-control" Width="350px" Enabled="false"></asp:DropDownList>
                                                                                        </asp:TableCell>
                                                                                        <asp:TableCell>&nbsp;&nbsp;<asp:LinkButton ID="lblSentencerDetail" runat="server" Text="รายชื่อคณะทำงาน" OnClick="lblSentencerDetail_Click"></asp:LinkButton></asp:TableCell></asp:TableRow></asp:Table></asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:CheckBox ID="chkBlacklist" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell Width="30%"> <asp:Label ID="lblCusScoreType" runat="server" Text="เงื่อนไขการตัดคะแนน"></asp:Label></asp:TableCell><asp:TableCell Width="50%"> <asp:RadioButtonList ID="radCusScoreType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                    OnSelectedIndexChanged="radCusScoreType_SelectedIndexChanged">
                                                                                </asp:RadioButtonList>
                                                                            </asp:TableCell>
                                                                        <asp:TableCell Width="20%">&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell> <asp:Label ID="lblShowSumPoint" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell> <asp:Label ID="lblShowSumCost" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtCostOther" runat="server" Width="200px" Style="text-align: center"> </asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="Label84" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px" Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell> <asp:Label ID="lblShowSumDisable" Visible="false" runat="server" Text="(ผลรวม : {0})"></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtOilLose" runat="server" Width="200px" Style="text-align: center"> </asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblRemarkTab3" runat="server" Text="หมายเหตุ"></asp:Label></asp:TableCell><asp:TableCell Width="100%" ColumnSpan="2"> <asp:TextBox CssClass="form-control" ID="txtRemarkTab3" runat="server" Width="100%" Height="100px"> </asp:TextBox></asp:TableCell></asp:TableRow></asp:Table></div></div></div></div></asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell> <div class="panel panel-info" id="div2" runat="server">
                                                    <div class="panel-heading">
                                                        <i class="fa fa-table"></i>หลังอุทธรณ์ </div><div class="panel-body">
                                                        <div class="dataTable_wrapper">
                                                            <div class="panel-body">
                                                                <asp:Table runat="server" Width="100%">
                                                                    <asp:TableRow>
                                                                        <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell> <asp:CheckBox ID="chkBlacklistFinal" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" Visible="false" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell Width="30%"> <asp:Label ID="lblAppealPoint" runat="server" Text="หักคะแนน" Visible="false"></asp:Label></asp:TableCell><asp:TableCell Width="50%"> <asp:TextBox CssClass="form-control" ID="txtAppealPoint" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblAppealCost" runat="server" Text="ค่าปรับ" Visible="false"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtAppealCost" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblAppealCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย" Visible="false"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtAppealCostOther" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblAppealDisable" runat="server" Text="ระงับ พขร. (วัน)" Visible="false"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtAppealDisable" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                                                        <asp:TableCell> <asp:Label ID="lblAppealOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)" Visible="false"></asp:Label></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtAppealOilLose" runat="server" Width="200px" Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow></asp:Table></div></div></div></div></asp:TableCell></asp:TableRow></asp:Table><asp:Table runat="server" ID="tblScoreFinal" Width="100%" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label3" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore1_SelectedIndexChanged"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore1Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore1Final_OnKeyPress');" />
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label4" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore2_SelectedIndexChanged"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore2Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore2Final_OnKeyPress');" />
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label5" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore3_SelectedIndexChanged"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore3Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore3Final_OnKeyPress');" />
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label6" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlScore4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlScore4_SelectedIndexChanged"
                                                        CssClass="form-control">
                                                    </asp:DropDownList>
                                                </asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore4Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore4Final_OnKeyPress');" />
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label7" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore5Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" />
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="Label8" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell> <asp:TextBox CssClass="form-control" ID="txtScore6Final" runat="server" Width="200px"
                                                        ReadOnly="true" Style="text-align: center" />
                                                </asp:TableCell><asp:TableCell Style="padding-left: 5px"> </asp:TableCell></asp:TableRow></asp:Table></div></div></div><div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdShowAppeal" runat="server" Text="ข้อมูลการยื่นอุทธรณ์" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdShowAppeal_Click"
                                Visible="false" />
                            &nbsp; <asp:Button ID="cmdSaveTab3" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmSaveTab3" />
                            &nbsp; <asp:Button ID="cmdSaveTab3Draft" runat="server" Text="บันทึก (ร่าง)" CssClass="btn btn-md btn-hover btn-warning"
                                UseSubmitBehavior="false" OnClick="cmdSaveTab3Draft_Click" Style="width: 100px" />
                            &nbsp; <asp:Button ID="cmdChangeStatus" runat="server" Text="ขอเอกสารเพิ่มเติม" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdChangeStatus_Click" />
                            &nbsp; <asp:Button ID="cmdCloseTab3" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" OnClick="cmdCloseTab2_Click" Style="width: 100px" />
                            <br />
                        </div>
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>--%>
            <div class="panel panel-info" id="divCost" runat="server">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค่าปรับ + ค่าเสียหายและหรือค่าใช้จ่าย </div><div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table10" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Style="width: 30%">
                                        <asp:Label ID="lblTotalCostAll" runat="server" Text="ค่าปรับ + ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label>
                                    </asp:TableCell><asp:TableCell
                                        Style="width: 40%">
                                        <asp:RadioButtonList ID="radCostCheck" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="ชำระครบถ้วน"> </asp:ListItem>
                                            <asp:ListItem Value="0" Selected="True" Text="ยังไม่ได้ชำระ/ชำระยังไม่ครบถ้วน"> </asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="lblCostCheckDate" runat="server" Text="วันที่ชำระครบถ้วน "></asp:Label>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:TextBox ID="txtCostCheckDate" runat="server" CssClass="datepicker" Style="text-align: center"> </asp:TextBox>
                                    </asp:TableCell></asp:TableRow></asp:Table><br />
                            <asp:Table ID="Table12" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="30%">
                                        <asp:Label ID="Label1" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadCost" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="30%">
                                        <asp:FileUpload ID="fileUploadCost" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUploadCost" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" OnClick="cmdUploadCost_Click" Style="width: 100px" />
                                    </asp:TableCell></asp:TableRow></asp:Table><asp:GridView ID="dgvUploadCost" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" DataKeyNames="UPLOAD_ID,FULLPATH"
                                OnRowDeleting="dgvUploadCost_RowDeleting" OnRowUpdating="dgvUploadCost_RowUpdating"
                                OnRowDataBound="dgvUploadCost_RowDataBound">
                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSaveIsCost" runat="server" Text="บันทึกค่าปรับ" Width="120px"
                        class="btn btn-md btn-hover btn-info" OnClick="cmdSaveIsCost_Click" />
                    <br />
                </div>
            </div>
            <br />
            <br />
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
            <br />
        </div>
        <div class="tab-pane fade" id="TabChangeStatus">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info" id="div3" runat="server">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>สถานะเอกสาร </div><div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table277" runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell Width="22%"> <asp:Label ID="lblDocIDTab4" runat="server" Text="หมายเลขเอกสาร"></asp:Label></asp:TableCell><asp:TableCell
                                                    Width="26%"> <input class="form-control" id="txtDocIDTab4" runat="server" style="width: 250px; text-align: center"
                                                        readonly="readonly" /></asp:TableCell><asp:TableCell Width="4%">&nbsp;</asp:TableCell><asp:TableCell
                                                Width="22%">&nbsp;</asp:TableCell><asp:TableCell Width="26%">&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow>
                                            <asp:TableCell> <asp:Label ID="lblStatusOldTab4" runat="server" Text="สถานะเอกสาร (เดิม)"></asp:Label></asp:TableCell><asp:TableCell> <input class="form-control" id="txtDocIDStatusOld" runat="server" style="width: 250px; text-align: center"
                                                        readonly="readonly" /></asp:TableCell><asp:TableCell Width="4%">&nbsp;</asp:TableCell><asp:TableCell> <asp:Label ID="Label16" runat="server" Text="สถานะเอกสาร (เปลี่ยนเป็น)"></asp:Label></asp:TableCell><asp:TableCell> <asp:DropDownList ID="ddlDocIDStatusNew" runat="server" class="form-control" DataTextField="DOC_STATUS_NAME"
                                                        DataValueField="DOC_STATUS_ID" Width="250px">
                                                    </asp:DropDownList>
                                                </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                            <br />
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdAdminChangeStatus" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmChangeStatus"
                                Enabled="false" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab1_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <%--<uc1:ModelPopup runat="server" ID="mpChangeStatus" IDModel="ModalConfirmBeforeChange"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdChangeStatus_Click"
        TextTitle="ยืนยันการเปลี่ยนสถานะ" TextDetail="คุณต้องการขอเอกสารเพิ่มเติมใช่หรือไม่ ?" />--%>
    <uc1:ModelPopup runat="server" ID="mpConfirmCancel" IDModel="ModalConfirmBeforeCancel"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdCancelDoc_Click"
        TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกเอกสารใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="ModelPopup1" IDModel="ModalConfirmBeforeCancel"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdCancelDoc_Click"
        TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกเอกสารใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmChangeStatus" IDModel="ModalConfirmChangeStatus"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdAdminChangeStatus_Click"
        TextTitle="ยืนยันการเปลี่ยนสถานะ" TextDetail="คุณต้องการเปลี่ยนสถานะเอกสารใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab3" IDModel="ModalConfirmSaveTab3"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab3_Click"
        TextTitle="บันทึกตัดคะแนน" TextDetail="คุณต้องการยืนยันบันทึกการตัดคะแนนใช่หรือไม่ ?" />
    <div class="modal fade" id="ShowWithAutoPostBack" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="upModal3" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="cmdClose" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblModalTitle3" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:TextBox ID="txtEmailTab3" runat="server" Width="99%" TextMode="MultiLine" Height="100px"></asp:TextBox></div><div class="modal-footer">
                            <asp:Button ID="cmdChangeStatusConfirm" runat="server" Text="ยืนยันข้อมูล" CssClass="btn btn-md btn-hover btn-info"
                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdChangeStatusConfirm_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--Driver Search--%>
    <div class="modal fade" id="ShowDriverSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updDriverSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblDriverSearchTitle" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table3333" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="110"> <asp:Label ID="lblSearchDriver" runat="server" Text="ชื่อ, นามสกุล, รหัส"></asp:Label></asp:TableCell><asp:TableCell
                                            Width="210"> <asp:TextBox ID="txtSearchDriver" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></asp:TableCell><asp:TableCell
                                        Width="200"> <asp:Button ID="cmdStartSearchDriver" runat="server" Text="ค้นหา" OnClick="cmdStartSearchDriver_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:Table ID="Table44444" runat="server" Width="99%">
                                <asp:TableRow>
                                    <asp:TableCell> <asp:GridView ID="dgvDriverSearch" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="dgvDriverSearch_PageIndexChanging" DataKeyNames="SEMPLOYEEID"
                                            OnRowUpdating="dgvDriverSearch_RowUpdating">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="SEMPLOYEEID" HeaderText="รหัสพนักงาน" ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLNAME" HeaderText="ชื่อ-นามสกุล (รหัสบัตรประชาชน)"
                                                    ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseDriver" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <button type="button" id="Button2" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--Delivery No Search--%>
    <div class="modal fade" id="ShowDeliveryNoSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updDeliveryNoSearch" runat="server" ChildrenAsTriggers="false"
                UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button3" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblDeliveryNoSearchTitle" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table5555" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="110"> <asp:Label ID="Label17" runat="server" Text="Delivery No"></asp:Label></asp:TableCell><asp:TableCell
                                            Width="210"> <asp:TextBox ID="txtSearchDeliveryNo" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></asp:TableCell><asp:TableCell
                                        Width="200"> <asp:Button ID="cmdStartSearchDeliveryNo" runat="server" Text="ค้นหา" OnClick="cmdStartSearchDeliveryNo_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:Table ID="Table6" runat="server" Width="99%">
                                <asp:TableRow>
                                    <asp:TableCell> <asp:GridView ID="dgvDeliveryNoSearch" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="dgvDeliveryNoSearch_PageIndexChanging"
                                            DataKeyNames="SDELIVERYNO" OnRowUpdating="dgvDeliveryNoSearch_RowUpdating">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="SDELIVERYNO" HeaderText="Delivery No" ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseDriver" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <button type="button" id="Button5" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--Car Search--%>
    <div class="modal fade" id="ShowCarSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updCarSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button4" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblCarSearchTitle" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table7" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="110"> <asp:Label ID="lblSearchCar" runat="server" Text="ทะเบียนรถ (หัว)"></asp:Label></asp:TableCell><asp:TableCell
                                            Width="210"> <asp:TextBox ID="txtSearchCar" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></asp:TableCell><asp:TableCell
                                        Width="200"> <asp:Button ID="cmdStartSearchCar" runat="server" Text="ค้นหา" OnClick="cmdStartSearchCar_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:Table ID="Table8" runat="server" Width="99%">
                                <asp:TableRow>
                                    <asp:TableCell> <asp:GridView ID="dgvCarSearch" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="dgvCarSearch_PageIndexChanging" DataKeyNames="STRUCKID"
                                            OnRowUpdating="dgvCarSearch_RowUpdating">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="STRUCKID" ItemStyle-Wrap="false" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียนรถ (หัว)" ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="STRAILERREGISTERNO" HeaderText="ทะเบียนรถ (ท้าย)" ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseCar" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <button type="button" id="Button7" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--Confirm OTP--%>
    <div class="modal fade" id="ShowOTP" role="dialog" aria-labelledby="myModalLabel"
        data-keyboard="false" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updOTP" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" id="Button6" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>--%>
                            <h4 class="modal-title">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                    Width="48" Height="48" />
                                <asp:Label ID="lblOTP" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table9" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="520" ColumnSpan="3"> <asp:Label ID="lblComplainUrgent" runat="server" Text=""></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell Width="110" ColumnSpan="3"> <asp:Label ID="lblComplainUrgent2" runat="server" Text=""></asp:Label></asp:TableCell></asp:TableRow><asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell Width="210"> <input id="txtOTP" runat="server" class="form-control" style="width: 200px; text-align: center"
                                            maxlength="6" placeholder="ใส่รหัส OTP" /></asp:TableCell><asp:TableCell Width="200"> <asp:Button ID="cmdOTP" runat="server" Text="ยืนยัน" OnClick="cmdOTP_Click" class="btn btn-md btn-hover btn-info" />
                                        <asp:Button ID="cmdOTPCancel" runat="server" Text="ยกเลิก" OnClick="cmdOTPCancel_Click"
                                            class="btn btn-md btn-hover btn-warning" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <button type="button" id="Button9" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <%--Delivery No Search All--%>
    <div class="modal fade" id="ShowDeliveryNoSearchAll" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="updDeliveryNoSearchAll" runat="server" ChildrenAsTriggers="false"
                UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button3" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblDeliveryNoSearchAllTitle" runat="server" Text=""></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table11" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="110"> <asp:Label ID="Label15" runat="server" Text="Delivery No"></asp:Label></asp:TableCell><asp:TableCell
                                            Width="210"> <asp:TextBox ID="txtDeliveryNoAll" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></asp:TableCell><asp:TableCell
                                        Width="200"> <asp:Button ID="Button6" runat="server" Text="ค้นหา" OnClick="cmdStartSearchDeliveryNoAll_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:Table ID="Table13" runat="server" Width="99%">
                                <asp:TableRow>
                                    <asp:TableCell> <asp:GridView ID="dgvDeliveryNoSearchAll" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="dgvDeliveryNoSearchAll_PageIndexChanging"
                                            DataKeyNames="SDELIVERYNO" OnRowUpdating="dgvDeliveryNoSearchAll_RowUpdating">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:BoundField DataField="SDELIVERYNO" HeaderText="Delivery No" ItemStyle-Wrap="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseDriver" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton></ItemTemplate></asp:TemplateField></Columns><EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <button type="button" id="Button5" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <%-- Search By ทะเบียนรถ  (หัว)--%>
    <div class="modal fade" id="dialogHEADREGISTERNOSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="uplSearchLicense" runat="server" ChildrenAsTriggers="false"
                UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearchLicenseAll" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            &times;</button><h4 class="modal-title">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                    Width="48" Height="48" />
                                <asp:Label ID="lblt" runat="server" Text="ค้นหาข้อมูลจาก ทะเบียนรถ (หัว)"></asp:Label></h4></div><div class="modal-body">
                            <asp:Table ID="Table14" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Width="110"> <asp:Label ID="Label18" runat="server" Text="ทะเบียนรถ (หัว)"></asp:Label></asp:TableCell><asp:TableCell
                                            Width="210"> <asp:TextBox ID="txtSearchLicense" runat="server" CssClass="form-control" Width="200px"></asp:TextBox></asp:TableCell><asp:TableCell
                                        Width="200"> <asp:Button ID="btnSearchLicenseAll" runat="server" Text="ค้นหา" OnClick="btnSearchLicenseAll_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:Table ID="Table15" runat="server" Width="99%">
                                <asp:TableRow>
                                    <asp:TableCell> <asp:GridView ID="grvSearchHEADREGISTERNO" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" AllowPaging="True"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowCommand="grvSearchHEADREGISTERNO_RowCommand" OnPageIndexChanging="grvSearchHEADREGISTERNO_PageIndexChanging" OnRowDataBound="grvSearchHEADREGISTERNO_RowDataBound" PageSize="10" DataSourceID="odsHeadRegisterNo"
                                            DataKeyNames="SHEADREGISTERNO,STRAILERREGISTERNO">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField ItemStyle-CssClass="center" ItemStyle-Width="100px" HeaderText="ทะเบียนรถ">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="lblSH" runat="server" Text='<%# Eval("SHEADREGISTERNO") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="center" ItemStyle-Width="150px" HeaderText="ทะเบียนรถ (ท้าย)">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="lblTL" runat="server" Text='<%# Eval("STRAILERREGISTERNO") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseNo" runat="server" CommandName="View" CssClass="view" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' ToolTip="เลือกข้อมูล" Text="เลือก" />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                            <PagerStyle CssClass="pagination-ys" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                        </asp:GridView>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:ObjectDataSource ID="odsHeadRegisterNo" runat="server" EnablePaging="True"
                SelectMethod="GetList" TypeName="TMS_BLL.ObjectDataSource.HeadRegisterNo" OnObjectCreating="odsHeadRegisterNo_ObjectCreating" OnSelected="odsHeadRegisterNo_Selected">
                <SelectParameters>
                    <asp:Parameter Name="HeadRegisterNo" Type="String" />
                    <asp:Parameter Name="pageSize" Type="Int32" DefaultValue="10" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:HiddenField ID="hdTotalRecords" runat="server" />
        </div>
    </div>

    <%--</div>--%><%--<script>

        $(document).ready(function () {
            
            cmdSaveTabremove();
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function cmdSaveTabremove() {
            $("#<%=cmdSaveTab1.ClientID%>,#<%=cmdCancelDoc.ClientID%>").removeAttr('onclick');

        }
    </script>--%>
</asp:Content>
