﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.IO;
using TMS_BLL.Transaction.KM;
using EmailHelper;

public partial class KMAddEdit : PageBase
{
    #region + View State +
    private string Type
    {
        get
        {
            if ((string)ViewState["Type"] != null)
                return (string)ViewState["Type"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Type"] = value;
        }
    }

    private string TopicID
    {
        get
        {
            if ((string)ViewState["TopicID"] != null)
                return (string)ViewState["TopicID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }
    private string MailAll
    {
        get
        {
            if ((string)ViewState["MailAll"] != null)
                return (string)ViewState["MailAll"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["MailAll"] = value;
        }
    }
    private int KM
    {
        get
        {
            if ((int)ViewState["KM"] != null)
                return (int)ViewState["KM"];
            else
                return 0;
        }
        set
        {
            ViewState["KM"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    private DataTable dtAllShare
    {
        get
        {
            if ((DataTable)ViewState["dtAllShare"] != null)
                return (DataTable)ViewState["dtAllShare"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtAllShare"] = value;
        }
    }

    private DataTable dtEmployee
    {
        get
        {
            if ((DataTable)ViewState["dtEmployee"] != null)
                return (DataTable)ViewState["dtEmployee"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtEmployee"] = value;
        }
    }
    private DataTable dtUsergroup
    {
        get
        {
            if ((DataTable)ViewState["dtUsergroup"] != null)
                return (DataTable)ViewState["dtUsergroup"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtUsergroup"] = value;
        }
    }
    private DataTable dtDivis
    {
        get
        {
            if ((DataTable)ViewState["dtDivis"] != null)
                return (DataTable)ViewState["dtDivis"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDivis"] = value;
        }
    }
    private DataTable dtDepart
    {
        get
        {
            if ((DataTable)ViewState["dtDepart"] != null)
                return (DataTable)ViewState["dtDepart"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtDepart"] = value;
        }
    }

    private DataTable dtContractUsername
    {
        get
        {
            if ((DataTable)ViewState["dtContractUsername"] != null)
                return (DataTable)ViewState["dtContractUsername"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtContractUsername"] = value;
        }
    }
    private DataTable dtContractGroup
    {
        get
        {
            if ((DataTable)ViewState["dtContractGroup"] != null)
                return (DataTable)ViewState["dtContractGroup"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtContractGroup"] = value;
        }
    }
    private DataTable dtTransportGroup
    {
        get
        {
            if ((DataTable)ViewState["dtTransportGroup"] != null)
                return (DataTable)ViewState["dtTransportGroup"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtTransportGroup"] = value;
        }
    }
    private DataTable dtContractName
    {
        get
        {
            if ((DataTable)ViewState["dtContractName"] != null)
                return (DataTable)ViewState["dtContractName"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtContractName"] = value;
        }
    }
    private DataTable dtContractNumber
    {
        get
        {
            if ((DataTable)ViewState["dtContractNumber"] != null)
                return (DataTable)ViewState["dtContractNumber"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtContractNumber"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtItem
    {
        get
        {
            if ((DataTable)ViewState["dtItem"] != null)
                return (DataTable)ViewState["dtItem"];
            else
                return null;
        }
        set
        {
            ViewState["dtItem"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadDepartment();
            this.LoadUserGroup();
            this.LoadUser();
            this.LoadWorkgroup();
            this.LoadVendor();
            this.LoadContract();
            this.LoadContractGroup();
            this.LoadContractUser();
            this.LoadKnowledgeType();
            this.LoadProductTagHead();
            this.LoadDivis();
            this.LoadDepart();
            this.AddDataTable();
            ddlDepartment.Enabled = true;
            this.InitialUpload();
            this.InitialShare();
            this.CheckDataTable();
            this.CheckQueryString();
            this.GetData();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadKnowledgeType()
    {
        try
        {
            DataTable dtKnowledgeType = KnowledgeTypeBLL.Instance.KnowledgeTypeSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlKnowledgeType, dtKnowledgeType, "KNOWLEDGE_TYPE_ID", "KNOWLEDGE_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadUserGroup()
    {
        try
        {
            DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(" AND IS_ADMIN IN (1,2,3,4,5) AND IS_ACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlUsergroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadUser()
    {
        try
        {
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND CGROUP IN (1,2,3,4,5) AND CACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlEmployeeCode, dtUser, "SUID", "DDL_FULLNAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dtVendor = KMBLL.Instance.SelectKMVendorBLL(" AND CACTIVE = '1' ");
            DropDownListHelper.BindDropDownList(ref ddlContractName, dtVendor, "SVENDORID", "SABBREVIATION", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadContract()
    {
        try
        {
            DataTable dtContract = KMBLL.Instance.SelectKMContractBLL(" AND CACTIVE = 'Y' ");
            DropDownListHelper.BindDropDownList(ref ddlContractNumber, dtContract, "SCONTRACTID", "SCONTRACTNO", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadWorkgroup()
    {
        try
        {
            DataTable dtWorkgroup = KMBLL.Instance.SelectKMWorkgroupBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlTransportGroup, dtWorkgroup, "ID", "NAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadContractGroup()
    {
        try
        {
            DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(" AND IS_ADMIN = 0 AND IS_ACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlContractGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadContractUser()
    {
        try
        {
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND CGROUP = 0 AND CACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlContractUserName, dtUser, "SUID", "DDL_FULLNAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadProductTagHead()
    {
        try
        {
            DataTable dtProductTagHead = ProductTagBLL.Instance.ProductTagHeadSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlProductTagHead, dtProductTagHead, "KM_TAG_LVL1_ID", "KM_TAG_LVL1_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadProductTagItem(string ProductTagHead)
    {
        try
        {
            DataTable dtProductTagItem = ProductTagBLL.Instance.ProductTagItemSelectBLL(ProductTagHead);
            DropDownListHelper.BindDropDownList(ref ddlProductTagItem, dtProductTagItem, "KM_TAG_LVL2_ID", "KM_TAG_LVL2_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDepartment()
    {
        try
        {
            DataTable dtDepartment = DepartmentBLL.Instance.DepartmentSelectBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlDepartment, dtDepartment, "DEPARTMENT_ID", "DEPARTMENT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDivision(string department)
    {
        try
        {
            DataTable dtDivision = DivisionBLL.Instance.DivisionSelectBLL(" AND CACTIVE = 1 AND DEPARTMENT_ID = " + department);
            DropDownListHelper.BindDropDownList(ref ddlDivision, dtDivision, "DIVISION_ID", "DIVISION_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDepart()
    {
        try
        {
            DataTable dtDepartment = DepartmentBLL.Instance.DepartmentSelectBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlDepart, dtDepartment, "DEPARTMENT_ID", "DEPARTMENT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDivis()
    {
        try
        {
            DataTable dtDivision = DivisionBLL.Instance.DivisionSelectBLL(" AND CACTIVE = 1 ");
            DropDownListHelper.BindDropDownList(ref ddlDivis, dtDivision, "DIVISION_ID", "DIVISION_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckQueryString()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                Type = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["type"], MachineKeyProtection.All));
                TopicID = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All));
            }
            else
                Response.Redirect("../Other/NotAuthorize.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetData()
    {
        if (string.Equals(Type, Mode.View.ToString()))
        {
            dt = KMBLL.Instance.SelectKMBLL(" AND TOPIC_ID = '" + TopicID + "'");
            if (dt.Rows.Count > 0)
            {
                KM = int.Parse(dt.Rows[0]["KM_ID"].ToString());
                txtTopicName.Text = dt.Rows[0]["TOPIC_NAME"].ToString();
                txtTopicID.Text = TopicID;
                txtAuthorName.Text = dt.Rows[0]["FULLNAME"].ToString();
                ddlDepartment.SelectedValue = dt.Rows[0]["DEPARTMENT_ID"].ToString();
                ddlDepartment_SelectedIndexChanged(null, null);
                ddlDivision.SelectedValue = dt.Rows[0]["DIVISION_ID"].ToString();
                txtCoP.Text = dt.Rows[0]["COP"].ToString();
                ddlKnowledgeType.Text = dt.Rows[0]["KNOWLEDGE_TYPE_ID"].ToString();
                txtKeyword.Text = dt.Rows[0]["OTHER_KEYWORDS"].ToString();
                txtExecutive.Text = dt.Rows[0]["EXECUTIVE"].ToString();
                txtCreatedDate.Text = dt.Rows[0]["CREATE_DATE"].ToString();
                txtUpdatedDate.Text = dt.Rows[0]["UPDATE_DATE"].ToString();
                txtOteherEmail.Text = dt.Rows[0]["OTHER_EMAIL"].ToString();

                dtItem = KMBLL.Instance.SelectKMTagBLL(" AND KM_ID = " + dt.Rows[0]["KM_ID"].ToString());
                if (dtItem.Rows.Count > 0)
                {
                    dgvItem.DataSource = dtItem;
                    dgvItem.DataBind();
                }

                dtUpload = KMBLL.Instance.SelectKMUploadBLL(" AND REF_STR = '" + TopicID + "' ");
                if (dtUpload.Rows.Count > 0)
                {
                    dgvUploadFile.DataSource = dtUpload;
                    dgvUploadFile.DataBind();
                }

                dtEmployee = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 1");
                if (dtEmployee.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtUsergroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 2");
                if (dtUsergroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtDivis = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 3");
                if (dtDivis.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtDepart = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 4");
                if (dtDepart.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", true);

                dtContractUsername = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 1");
                if (dtContractUsername.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractGroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 2");
                if (dtContractGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtTransportGroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 3");
                if (dtTransportGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractNumber = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 5");
                if (dtContractNumber.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractName = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 4");
                if (dtContractName.Rows.Count > 0)
                {
                    if (string.Equals(dtContractName.Rows[0]["SHARE_DISPLAY"].ToString().Trim(), "ALL"))
                    {
                        chkAll.Checked = true;
                        chkAll_CheckedChanged(null, null);
                    }
                    else
                    {
                        DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
                    }
                }

                KM_Master.Enabled = false;
                cmdSave.Enabled = false;
                cmdSaveDraft.Enabled = false;
                ddlDepartment.Enabled = false;
                ddlDivision.Enabled = false;
                cmdBack.Visible = true;
                cmdCancel.Visible = false;
            }
        }
        else if (string.Equals(Type, Mode.Edit.ToString()))
        {
            dt = KMBLL.Instance.SelectKMBLL(" AND TOPIC_ID = '" + TopicID + "'");
            if (dt.Rows.Count > 0)
            {
                KM = int.Parse(dt.Rows[0]["KM_ID"].ToString());
                txtTopicName.Text = dt.Rows[0]["TOPIC_NAME"].ToString();
                txtTopicID.Text = TopicID;
                txtAuthorName.Text = dt.Rows[0]["FULLNAME"].ToString();
                ddlDepartment.SelectedValue = dt.Rows[0]["DEPARTMENT_ID"].ToString();
                ddlDepartment_SelectedIndexChanged(null, null);
                ddlDivision.SelectedValue = dt.Rows[0]["DIVISION_ID"].ToString();
                txtCoP.Text = dt.Rows[0]["COP"].ToString();
                ddlKnowledgeType.Text = dt.Rows[0]["KNOWLEDGE_TYPE_ID"].ToString();
                txtKeyword.Text = dt.Rows[0]["OTHER_KEYWORDS"].ToString();
                txtExecutive.Text = dt.Rows[0]["EXECUTIVE"].ToString();
                txtCreatedDate.Text = dt.Rows[0]["CREATE_DATE"].ToString();
                txtUpdatedDate.Text = dt.Rows[0]["UPDATE_DATE"].ToString();
                txtOteherEmail.Text = dt.Rows[0]["OTHER_EMAIL"].ToString();

                dtItem = KMBLL.Instance.SelectKMTagBLL(" AND KM_ID = " + dt.Rows[0]["KM_ID"].ToString());
                if (dtItem.Rows.Count > 0)
                {
                    dgvItem.DataSource = dtItem;
                    dgvItem.DataBind();
                }

                dtUpload = KMBLL.Instance.SelectKMUploadBLL(" AND REF_STR = '" + TopicID + "' ");
                if (dtUpload.Rows.Count > 0)
                {
                    dgvUploadFile.DataSource = dtUpload;
                    dgvUploadFile.DataBind();
                }

                dtEmployee = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 1");
                if (dtEmployee.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtUsergroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 2");
                if (dtUsergroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtDivis = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 3");
                if (dtDivis.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtDepart = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_PTT_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 4");
                if (dtDepart.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", true);

                dtContractUsername = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 1");
                if (dtContractUsername.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractGroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 2");
                if (dtContractGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtTransportGroup = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 3");
                if (dtTransportGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractNumber = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 5");
                if (dtContractNumber.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", true);
                dtContractName = KMBLL.Instance.SelectKMSharetoBLL(" SELECT TO_CHAR(SHARE_VALUE) AS SHARE_VALUE,TO_CHAR(SHARE_DISPLAY) AS SHARE_DISPLAY FROM VW_M_KM_SHARE_VENDOR_SELECT WHERE KM_ID = '" + KM + "' AND SHARE_TYPE = 4");
                if (dtContractName.Rows.Count > 0)
                {
                    if (string.Equals(dtContractName.Rows[0]["SHARE_DISPLAY"].ToString().Trim(), "ALL"))
                    {
                        chkAll.Checked = true;
                        chkAll_CheckedChanged(null, null);
                    }
                    else
                    {
                        DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
                    }
                }

                ddlDepartment.Enabled = false;
                ddlDivision.Enabled = false;
                cmdBack.Visible = true;
                cmdCancel.Visible = false;
            }
        }
        else
        {
            KM = 0;
            dt = UserBLL.Instance.UserSelectBLL(" AND SUID = " + Session["UserID"].ToString());
            if (dt.Rows.Count > 0)
            {
                if (!string.Equals(dt.Rows[0]["DEPARTMENT_ID"].ToString(), "") && !string.Equals(dt.Rows[0]["DIVISION_ID"].ToString(), ""))
                {
                    txtAuthorName.Text = dt.Rows[0]["SFIRSTNAME"].ToString() + " " + dt.Rows[0]["SLASTNAME"].ToString();
                    ddlDepartment.SelectedValue = dt.Rows[0]["DEPARTMENT_ID"].ToString();
                    ddlDepartment_SelectedIndexChanged(null, null);
                    ddlDivision.SelectedValue = dt.Rows[0]["DIVISION_ID"].ToString();
                    ddlDepartment.Enabled = false;
                    ddlDivision.Enabled = false;
                }
            }

            cmdSendAll.Enabled = false;
            cmdSendContGroup.Enabled = false;
            cmdSendContName.Enabled = false;
            cmdSendContNumber.Enabled = false;
            cmdSendContTransgroup.Enabled = false;
            cmdSendContUsername.Enabled = false;
            cmdSendDepart.Enabled = false;
            cmdSendDivis.Enabled = false;
            cmdSendEmployee.Enabled = false;
            cmdSendOther.Enabled = false;
            cmdSendUserGroup.Enabled = false;
            cmdBack.Visible = false;
            cmdCancel.Visible = true;
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.SelectedIndex > 0)
            {
                this.LoadDivision(ddlDepartment.SelectedValue);
                ddlDivision.Enabled = true;
            }
            else
            {
                this.LoadDivision(string.Empty);
                ddlDivision.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlProductTagHead_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlProductTagHead.SelectedIndex > 0)
            {
                this.LoadProductTagItem(" AND KM_TAG_LVL1_ID = " + ddlProductTagHead.SelectedValue.Trim());
                ddlProductTagItem.Enabled = true;
            }
            else
            {
                DropDownListHelper.BindDropDownList(ref ddlProductTagItem, null, "KM_TAG_LVL2_ID", "KM_TAG_LVL2_NAME", true);
                ddlProductTagItem.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {

                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("KM");
                if (dtUploadType.Rows.Count > 0)
                    dtUpload.Rows.Add(dtUploadType.Rows[0]["UPLOAD_ID"].ToString(), "KM", System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                else
                    throw new Exception("ไม่พบประเภทไฟล์ที่ต้องการอัพโหลด");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

            }

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Truck" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            //string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            //int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            //if (FileSize > MaxSize)
            //    throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            //if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
            //    throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialUpload()
    {
        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
    }

    private void InitialShare()
    {
        dtAllShare = new DataTable();
        dtAllShare.Columns.Add("SHARE_VALUE");
        dtAllShare.Columns.Add("SHARE_DISPLAY");
        dtAllShare.Columns.Add("SHARE_GROUP");
        dtAllShare.Columns.Add("SHARE_TYPE");
    }
    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }
    protected void cmdAddTag_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateAddItem();

            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                if (string.Equals(dtItem.Rows[i]["KM_TAG_LVL1_ID"].ToString(), ddlProductTagHead.SelectedValue) && string.Equals(dtItem.Rows[i]["KM_TAG_LVL2_ID"].ToString(), ddlProductTagItem.SelectedValue))
                {
                    dtItem.Rows.RemoveAt(i);
                    dtItem.Rows.Add(ddlProductTagHead.SelectedValue, ddlProductTagHead.SelectedIndex > 0 ? ddlProductTagHead.SelectedItem.ToString() : "", ddlProductTagItem.SelectedValue, ddlProductTagItem.SelectedIndex > 0 ? ddlProductTagItem.SelectedItem.ToString() : "");
                    GridViewHelper.BindGridView(ref dgvItem, dtItem);
                    this.ClearSelectedItem();
                    return;
                }
            }

            dtItem.Rows.Add(ddlProductTagHead.SelectedValue, ddlProductTagHead.SelectedIndex > 0 ? ddlProductTagHead.SelectedItem.ToString() : "", ddlProductTagItem.SelectedValue, ddlProductTagItem.SelectedIndex > 0 ? ddlProductTagItem.SelectedItem.ToString() : "");
            GridViewHelper.BindGridView(ref dgvItem, dtItem);
            this.ClearSelectedItem();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ClearSelectedItem()
    {
        ddlProductTagHead.ClearSelection();
        ddlProductTagItem.ClearSelection();
        ddlProductTagItem.Enabled = false;
    }

    private void ValidateAddItem()
    {
        try
        {
            if (ddlProductTagHead.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก Keywords (Tags)");

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckDataTable()
    {
        try
        {
            if (dtItem == null)
            {
                dtItem = new DataTable();
                dtItem.Columns.Add("KM_TAG_LVL1_ID");
                dtItem.Columns.Add("KM_TAG_LVL1_NAME");
                dtItem.Columns.Add("KM_TAG_LVL2_ID");
                dtItem.Columns.Add("KM_TAG_LVL2_NAME");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvItem_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtItem.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvItem, dtItem);

            if (dgvItem.Rows.Count == 0)
            {

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            this.ValidateEmailSave();
            this.ValidateUploadFile();

            DataTable dt = KMBLL.Instance.InsertKMBLL(KM, txtTopicName.Text.Trim(), txtTopicID.Text.Trim(), int.Parse(Session["UserID"].ToString()), int.Parse(ddlDepartment.SelectedValue), int.Parse(ddlDivision.SelectedValue), txtCoP.Text.Trim(), int.Parse(ddlKnowledgeType.SelectedValue), dtItem, txtKeyword.Text.Trim(), txtExecutive.Text.Trim(), dtUpload, 1, dtEmployee, dtUsergroup, dtDivis, dtDepart, dtContractUsername, dtContractGroup, dtTransportGroup, dtContractName, dtContractNumber, txtOteherEmail.Text.Trim());
            if (dt.Rows.Count > 0)
            {
                string KM_ID = dt.Rows[0]["MESSAGE"].ToString();
                alertSuccess("บันทึก และ แชร์ เอกสารสำเร็จ ขั้นตอนต่อไป กรุณาระบุอีเมล์ เพื่อส่งข้อมูลให้ผู้ที่สนใจ", "KMAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Edit.ToString()), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes(KM_ID), MachineKeyProtection.All));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void mpConfirmCancel_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("KM.aspx");
    }
    protected void mpConfirmSaveDraft_ClickOK(object sender, EventArgs e)
    {
        try
        {
            if (txtTopicName.Text.Trim() == "")
                throw new Exception("กรุณากรอก Topic Name");

            this.ValidateEmailSave();

            DataTable dt = KMBLL.Instance.InsertKMBLL(KM, txtTopicName.Text.Trim(), txtTopicID.Text.Trim(), int.Parse(Session["UserID"].ToString()), ddlDepartment.SelectedIndex > 0 ? int.Parse(ddlDepartment.SelectedValue) : 0, ddlDivision.SelectedIndex > 0 ? int.Parse(ddlDivision.SelectedValue) : 0, txtCoP.Text.Trim(), ddlKnowledgeType.SelectedIndex > 0 ? int.Parse(ddlKnowledgeType.SelectedValue) : 0, dtItem, txtKeyword.Text.Trim(), txtExecutive.Text.Trim(), dtUpload, 0, dtEmployee, dtUsergroup, dtDivis, dtDepart, dtContractUsername, dtContractGroup, dtTransportGroup, dtContractName, dtContractNumber, txtOteherEmail.Text.Trim());
            if (dt.Rows.Count > 0)
                alertSuccess("บันทึกผลสำเร็จ", "KM.aspx");

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateEmailSave()
    {
        try
        {
            if (dtEmployee.Rows.Count > 0)
            {
                dtEmployee.Rows.RemoveAt(0);
            }

            if (dtUsergroup.Rows.Count > 0)
            {
                dtUsergroup.Rows.RemoveAt(0);
            }

            if (dtDivis.Rows.Count > 0)
            {
                dtDivis.Rows.RemoveAt(0);
            }

            if (dtDepart.Rows.Count > 0)
            {
                dtDepart.Rows.RemoveAt(0);
            }


            if (chkAll.Checked == false)
            {
                if (dtContractUsername.Rows.Count > 0)
                {
                    dtContractUsername.Rows.RemoveAt(0);
                }

                if (dtContractGroup.Rows.Count > 0)
                {
                    dtContractGroup.Rows.RemoveAt(0);
                }

                if (dtTransportGroup.Rows.Count > 0)
                {
                    dtTransportGroup.Rows.RemoveAt(0);
                }

                if (dtContractName.Rows.Count > 0)
                {
                    dtContractName.Rows.RemoveAt(0);
                }

                if (dtContractNumber.Rows.Count > 0)
                {
                    dtContractNumber.Rows.RemoveAt(0);
                }
            }
            else
            {
                dtContractUsername = null;
                dtContractGroup = null;
                dtTransportGroup = null;
                dtContractName.Rows.Add(0, "ALL");
                dtContractNumber = null;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if ((dtItem == null) || dtItem.Rows.Count == 0)
                throw new Exception("ไม่มีข้อมูล Keywords (Tags) ที่ต้องการบันทึก");

            if (txtTopicName.Text.Trim() == "")
                throw new Exception("กรุณากรอก Topic Name");

            if (txtExecutive.Text.Trim() == "")
                throw new Exception("กรุณากรอก Executive summary/abstract");

            if (ddlDepartment.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก Department");

            if (ddlDivision.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก Division");

            if (ddlKnowledgeType.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก Knowledge Type");

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile()
    {
        try
        {
            if (dtUpload.Rows.Count == 0)
                throw new Exception("ไม่มีข้อมูลไฟล์อัพโหลด ที่ต้องการบันทึก");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void AddDataTable()
    {
        try
        {
            dtEmployee = new DataTable();
            dtEmployee.Columns.Add("SHARE_VALUE");
            dtEmployee.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtUsergroup = new DataTable();
            dtUsergroup.Columns.Add("SHARE_VALUE");
            dtUsergroup.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtDivis = new DataTable();
            dtDivis.Columns.Add("SHARE_VALUE");
            dtDivis.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtDepart = new DataTable();
            dtDepart.Columns.Add("SHARE_VALUE");
            dtDepart.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", true);


            dtContractUsername = new DataTable();
            dtContractUsername.Columns.Add("SHARE_VALUE");
            dtContractUsername.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtContractGroup = new DataTable();
            dtContractGroup.Columns.Add("SHARE_VALUE");
            dtContractGroup.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtTransportGroup = new DataTable();
            dtTransportGroup.Columns.Add("SHARE_VALUE");
            dtTransportGroup.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtContractName = new DataTable();
            dtContractName.Columns.Add("SHARE_VALUE");
            dtContractName.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);

            dtContractNumber = new DataTable();
            dtContractNumber.Columns.Add("SHARE_VALUE");
            dtContractNumber.Columns.Add("SHARE_DISPLAY");
            DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdAddEmployeeCode_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtEmployee.Rows.Count; i++)
            {
                if (string.Equals(dtEmployee.Rows[i]["SHARE_VALUE"].ToString(), ddlEmployeeCode.SelectedValue))
                {
                    dtEmployee.Rows.RemoveAt(i);
                    dtEmployee.Rows.Add(ddlEmployeeCode.SelectedValue, ddlEmployeeCode.SelectedIndex > 0 ? ddlEmployeeCode.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlEmployeeCode.ClearSelection();
                    return;
                }
            }
            if (dtEmployee.Rows.Count > 0)
            {
                dtEmployee.Rows.Add(ddlEmployeeCode.SelectedValue, ddlEmployeeCode.SelectedIndex > 0 ? ddlEmployeeCode.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtEmployee.Rows.Add(ddlEmployeeCode.SelectedValue, ddlEmployeeCode.SelectedIndex > 0 ? ddlEmployeeCode.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlEmployeeCode.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddUsergroup_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtUsergroup.Rows.Count; i++)
            {
                if (string.Equals(dtUsergroup.Rows[i]["SHARE_VALUE"].ToString(), ddlUsergroup.SelectedValue))
                {
                    dtUsergroup.Rows.RemoveAt(i);
                    dtUsergroup.Rows.Add(ddlUsergroup.SelectedValue, ddlUsergroup.SelectedIndex > 0 ? ddlUsergroup.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlUsergroup.ClearSelection();
                    return;
                }
            }
            if (dtUsergroup.Rows.Count > 0)
            {
                dtUsergroup.Rows.Add(ddlUsergroup.SelectedValue, ddlUsergroup.SelectedIndex > 0 ? ddlUsergroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtUsergroup.Rows.Add(ddlUsergroup.SelectedValue, ddlUsergroup.SelectedIndex > 0 ? ddlUsergroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlUsergroup.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddDivision_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtDivis.Rows.Count; i++)
            {
                if (string.Equals(dtDivis.Rows[i]["SHARE_VALUE"].ToString(), ddlDivis.SelectedValue))
                {
                    dtDivis.Rows.RemoveAt(i);
                    dtDivis.Rows.Add(ddlDivis.SelectedValue, ddlDivis.SelectedIndex > 0 ? ddlDivis.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlDivis.ClearSelection();
                    return;
                }
            }
            if (dtDivis.Rows.Count > 0)
            {
                dtDivis.Rows.Add(ddlDivis.SelectedValue, ddlDivis.SelectedIndex > 0 ? ddlDivis.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtDivis.Rows.Add(ddlDivis.SelectedValue, ddlDivis.SelectedIndex > 0 ? ddlDivis.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlDivis.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddDepartment_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtDepart.Rows.Count; i++)
            {
                if (string.Equals(dtDepart.Rows[i]["SHARE_VALUE"].ToString(), ddlDepart.SelectedValue))
                {
                    dtDepart.Rows.RemoveAt(i);
                    dtDepart.Rows.Add(ddlDepart.SelectedValue, ddlDepart.SelectedIndex > 0 ? ddlDepart.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlDepart.ClearSelection();
                    return;
                }
            }
            if (dtDepart.Rows.Count > 0)
            {
                dtDepart.Rows.Add(ddlDepart.SelectedValue, ddlDepart.SelectedIndex > 0 ? ddlDepart.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtDepart.Rows.Add(ddlDepart.SelectedValue, ddlDepart.SelectedIndex > 0 ? ddlDepart.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlDepart.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddContractUserName_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtContractUsername.Rows.Count; i++)
            {
                if (string.Equals(dtContractUsername.Rows[i]["SHARE_VALUE"].ToString(), ddlContractUserName.SelectedValue))
                {
                    dtContractUsername.Rows.RemoveAt(i);
                    dtContractUsername.Rows.Add(ddlContractUserName.SelectedValue, ddlContractUserName.SelectedIndex > 0 ? ddlContractUserName.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlContractUserName.ClearSelection();
                    return;
                }
            }
            if (dtContractUsername.Rows.Count > 0)
            {
                dtContractUsername.Rows.Add(ddlContractUserName.SelectedValue, ddlContractUserName.SelectedIndex > 0 ? ddlContractUserName.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtContractUsername.Rows.Add(ddlContractUserName.SelectedValue, ddlContractUserName.SelectedIndex > 0 ? ddlContractUserName.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlContractUserName.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddContractGroup_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtContractGroup.Rows.Count; i++)
            {
                if (string.Equals(dtContractGroup.Rows[i]["SHARE_VALUE"].ToString(), ddlContractGroup.SelectedValue))
                {
                    dtContractGroup.Rows.RemoveAt(i);
                    dtContractGroup.Rows.Add(ddlContractGroup.SelectedValue, ddlContractGroup.SelectedIndex > 0 ? ddlContractGroup.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlContractGroup.ClearSelection();
                    return;
                }
            }
            if (dtContractGroup.Rows.Count > 0)
            {
                dtContractGroup.Rows.Add(ddlContractGroup.SelectedValue, ddlContractGroup.SelectedIndex > 0 ? ddlContractGroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtContractGroup.Rows.Add(ddlContractGroup.SelectedValue, ddlContractGroup.SelectedIndex > 0 ? ddlContractGroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlContractGroup.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddTransportGroup_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtTransportGroup.Rows.Count; i++)
            {
                if (string.Equals(dtTransportGroup.Rows[i]["SHARE_VALUE"].ToString(), ddlTransportGroup.SelectedValue))
                {
                    dtTransportGroup.Rows.RemoveAt(i);
                    dtTransportGroup.Rows.Add(ddlTransportGroup.SelectedValue, ddlTransportGroup.SelectedIndex > 0 ? ddlTransportGroup.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlTransportGroup.ClearSelection();
                    return;
                }
            }
            if (dtTransportGroup.Rows.Count > 0)
            {
                dtTransportGroup.Rows.Add(ddlTransportGroup.SelectedValue, ddlTransportGroup.SelectedIndex > 0 ? ddlTransportGroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtTransportGroup.Rows.Add(ddlTransportGroup.SelectedValue, ddlTransportGroup.SelectedIndex > 0 ? ddlTransportGroup.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlTransportGroup.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddContractName_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtContractName.Rows.Count; i++)
            {
                if (string.Equals(dtContractName.Rows[i]["SHARE_VALUE"].ToString(), ddlContractName.SelectedValue))
                {
                    dtContractName.Rows.RemoveAt(i);
                    dtContractName.Rows.Add(ddlContractName.SelectedValue, ddlContractName.SelectedIndex > 0 ? ddlContractName.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlContractName.ClearSelection();
                    return;
                }
            }
            if (dtContractName.Rows.Count > 0)
            {
                dtContractName.Rows.Add(ddlContractName.SelectedValue, ddlContractName.SelectedIndex > 0 ? ddlContractName.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtContractName.Rows.Add(ddlContractName.SelectedValue, ddlContractName.SelectedIndex > 0 ? ddlContractName.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlContractName.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdAddContractNumber_Click(object sender, EventArgs e)
    {
        try
        {
            for (int i = 0; i < dtContractNumber.Rows.Count; i++)
            {
                if (string.Equals(dtContractNumber.Rows[i]["SHARE_VALUE"].ToString(), ddlContractNumber.SelectedValue))
                {
                    dtContractNumber.Rows.RemoveAt(i);
                    dtContractNumber.Rows.Add(ddlContractNumber.SelectedValue, ddlContractNumber.SelectedIndex > 0 ? ddlContractNumber.SelectedItem.ToString() : "");
                    DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", false);
                    ddlContractNumber.ClearSelection();
                    return;
                }
            }
            if (dtContractNumber.Rows.Count > 0)
            {
                dtContractNumber.Rows.Add(ddlContractNumber.SelectedValue, ddlContractNumber.SelectedIndex > 0 ? ddlContractNumber.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            else
            {
                dtContractNumber.Rows.Add(ddlContractNumber.SelectedValue, ddlContractNumber.SelectedIndex > 0 ? ddlContractNumber.SelectedItem.ToString() : "");
                DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", true);
            }
            ddlContractNumber.ClearSelection();
            this.SetShareName();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void chkEmployee_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkEmployee.Checked == true)
            {
                if (dtEmployee.Rows.Count > 0)
                {
                    for (int i = 0; i < dtEmployee.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtEmployee.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtEmployee.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(1, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtEmployee.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtEmployee.Enabled = true;
                }
            }
            else
            {
                txtEmployee.Text = email;
                txtEmployee.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkUsergroup_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkUsergroup.Checked == true)
            {
                if (dtUsergroup.Rows.Count > 0)
                {
                    for (int i = 0; i < dtUsergroup.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtUsergroup.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtUsergroup.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(2, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtUsergroup.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtUsergroup.Enabled = true;
                }
            }
            else
            {
                txtUsergroup.Text = email;
                txtUsergroup.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkDivis_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkDivis.Checked == true)
            {
                if (dtDivis.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDivis.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtDivis.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtDivis.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(3, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtDivis.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtDivis.Enabled = true;
                }
            }
            else
            {
                txtDivis.Text = email;
                txtDivis.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkDepart_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkDepart.Checked == true)
            {
                if (dtDepart.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDepart.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtDepart.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtDepart.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(4, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtDepart.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtDepart.Enabled = true;
                }
            }
            else
            {
                txtDepart.Text = email;
                txtDepart.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkContUsername_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkContUsername.Checked == true)
            {
                if (dtContractUsername.Rows.Count > 0)
                {
                    for (int i = 0; i < dtContractUsername.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtContractUsername.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtContractUsername.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(5, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtContUsername.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtContUsername.Enabled = true;
                }
            }
            else
            {
                txtContUsername.Text = email;
                txtContUsername.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkContGroup_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = "";
            if (chkContGroup.Checked == true)
            {
                if (dtContractGroup.Rows.Count > 0)
                {
                    for (int i = 0; i < dtContractGroup.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtContractGroup.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtContractGroup.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(6, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtContGroup.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtContGroup.Enabled = true;
                }
            }
            else
            {
                txtContGroup.Text = email;
                txtContGroup.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }
    protected void chkContTransgroup_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkContTransgroup.Checked == true)
            {
                if (dtTransportGroup.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTransportGroup.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtTransportGroup.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtTransportGroup.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(7, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtContTransgroup.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtContTransgroup.Enabled = true;
                }
            }
            else
            {
                txtContTransgroup.Text = email;
                txtContTransgroup.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkContName_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkContName.Checked == true)
            {
                if (chkAll.Checked == false)
                {
                    if (dtContractName.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtContractName.Rows.Count; i++)
                        {
                            if (i == 0)
                                email = dtContractName.Rows[i]["SHARE_VALUE"].ToString();
                            else
                                email = email + "','" + dtContractName.Rows[i]["SHARE_VALUE"].ToString();
                        }
                    }
                }
                else
                {
                    DataTable kmvendorall = KMBLL.Instance.SelectKMVendorBLL(" AND CACTIVE = '1' ");
                    if (kmvendorall.Rows.Count > 0)
                    {
                        for (int i = 0; i < kmvendorall.Rows.Count; i++)
                        {
                            if (i == 0)
                                email = kmvendorall.Rows[i]["SVENDORID"].ToString();
                            else
                                email = email + "','" + kmvendorall.Rows[i]["SVENDORID"].ToString();
                        }
                    }
                }

                if (!string.Equals(email.Trim(), ""))
                    email = email.Remove(0, 3);

                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(8, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtContName.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtContName.Enabled = true;
                }
            }
            else
            {
                txtContName.Text = email;
                txtContName.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkContNumber_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            string email = string.Empty;
            if (chkContNumber.Checked == true)
            {
                if (dtContractNumber.Rows.Count > 0)
                {
                    for (int i = 0; i < dtContractNumber.Rows.Count; i++)
                    {
                        if (i == 0)
                            email = dtContractNumber.Rows[i]["SHARE_VALUE"].ToString();
                        else
                            email = email + "," + dtContractNumber.Rows[i]["SHARE_VALUE"].ToString();
                    }
                    if (!string.Equals(email.Trim(), ""))
                        email = email.Remove(0, 1);
                }
                DataTable dtEmail = KMBLL.Instance.SelectKMEmailBLL(9, email.Trim() == "" ? "''" : email.Trim());
                if (dtEmail.Rows.Count > 0)
                {
                    txtContNumber.Text = dtEmail.Rows[0]["SEMAIL"].ToString();
                    txtContNumber.Enabled = true;
                }
            }
            else
            {
                txtContNumber.Text = email;
                txtContNumber.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveEmployeeCode_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmployeeCodeDisplay.SelectedIndex > 0)
            {
                int Index = ddlEmployeeCodeDisplay.SelectedIndex;
                dtEmployee.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlEmployeeCodeDisplay, dtEmployee, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkEmployee_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveUsergroup_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlUsergroupDisplay.SelectedIndex > 0)
            {
                int Index = ddlUsergroupDisplay.SelectedIndex;
                dtUsergroup.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlUsergroupDisplay, dtUsergroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }
            chkUsergroup_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveDivision_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlDivisDisplay.SelectedIndex > 0)
            {
                int Index = ddlDivisDisplay.SelectedIndex;
                dtDivis.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlDivisDisplay, dtDivis, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkDivis_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveDepartment_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartDisplay.SelectedIndex > 0)
            {
                int Index = ddlDepartDisplay.SelectedIndex;
                dtDepart.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlDepartDisplay, dtDepart, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkDepart_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveContractUserName_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlContractUserNameDisplay.SelectedIndex > 0)
            {
                int Index = ddlContractUserNameDisplay.SelectedIndex;
                dtContractUsername.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkContUsername_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveContractGroup_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlContractGroupDisplay.SelectedIndex > 0)
            {
                int Index = ddlContractGroupDisplay.SelectedIndex;
                dtContractGroup.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkContGroup_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveTransportGroup_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlTransportGroupDisplay.SelectedIndex > 0)
            {
                int Index = ddlTransportGroupDisplay.SelectedIndex;
                dtTransportGroup.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkContTransgroup_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveContractName_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlContractNameDisplay.SelectedIndex > 0)
            {
                int Index = ddlContractNameDisplay.SelectedIndex;
                dtContractName.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkContName_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdRemoveContractNumber_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlContractNumberDisplay.SelectedIndex > 0)
            {
                int Index = ddlContractNumberDisplay.SelectedIndex;
                dtContractNumber.Rows.RemoveAt(Index);
                DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", false);
            }

            chkContNumber_CheckedChanged(null, null);
            this.SetShareName();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SendEmail(int TemplateID, string Txt)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("EMAIL");
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                Subject = Subject.Replace("{TopicName}", txtTopicName.Text.Trim());
                Subject = Subject.Replace("{KM_Type}", ddlKnowledgeType.SelectedItem.ToString());
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                string Link = ConfigValue.GetClickHere(Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "KMView.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + Request.QueryString["id"]);
                //dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? STRANSPORTID.SelectedValue.ToString() : SVDID);
                //EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                Body = Body.Replace("{Department}", ddlDepartment.SelectedItem.ToString());
                Body = Body.Replace("{KM_Type}", ddlKnowledgeType.SelectedItem.ToString());
                Body = Body.Replace("{TopicName}", txtTopicName.Text.Trim());
                Body = Body.Replace("{KM_Link}", Link);
                Body = Body.Replace("{TopicID}", TopicID);
                Body = Body.Replace("{AuthorName}", txtAuthorName.Text);
                Body = Body.Replace("{Abstract}", txtExecutive.Text.Trim());
                Body = Body.Replace("{CreatedDate}", txtCreatedDate.Text);
                Body = Body.Replace("{Division}", ddlDivision.SelectedItem.ToString());
                dt.Rows.Add("");
                MailService.SendMail(Txt, Subject, Body);

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSendEmployee_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtEmployee.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendUserGroup_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtUsergroup.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendDivis_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtDivis.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendDepart_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtDepart.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendContUsername_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtContUsername.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendContGroup_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtContGroup.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendContTransgroup_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtContTransgroup.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendContName_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtContName.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendContNumber_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtContNumber.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendAll_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtMailAll.Text.Trim());
            alertSuccess("ส่งอีเมล์สำเร็จ");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkSendAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            MailAll = string.Empty;
            if (chkSendAll.Checked == true)
            {
                if (chkAll.Checked == true)
                {
                    chkEmployee.Checked = true;
                    chkEmployee_CheckedChanged(null, null);
                    chkUsergroup.Checked = true;
                    chkUsergroup_CheckedChanged(null, null);
                    chkDivis.Checked = true;
                    chkDivis_CheckedChanged(null, null);
                    chkDepart.Checked = true;
                    chkDepart_CheckedChanged(null, null);
                    chkContName.Checked = true;
                    chkContName_CheckedChanged(null, null);

                    if (!string.Equals(txtEmployee.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtEmployee.Text.Trim();
                    if (!string.Equals(txtUsergroup.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtUsergroup.Text.Trim();
                    if (!string.Equals(txtDivis.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtDivis.Text.Trim();
                    if (!string.Equals(txtDepart.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtDepart.Text.Trim();
                    if (!string.Equals(txtContName.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContName.Text.Trim();

                    if (!string.Equals(MailAll.Trim(), ""))
                        MailAll = MailAll.Remove(0, 1);

                    txtMailAll.Text = MailAll.Trim();
                    txtMailAll.Enabled = true;
                }
                else
                {
                    chkEmployee.Checked = true;
                    chkEmployee_CheckedChanged(null, null);
                    chkUsergroup.Checked = true;
                    chkUsergroup_CheckedChanged(null, null);
                    chkDivis.Checked = true;
                    chkDivis_CheckedChanged(null, null);
                    chkDepart.Checked = true;
                    chkDepart_CheckedChanged(null, null);
                    chkContUsername.Checked = true;
                    chkContUsername_CheckedChanged(null, null);
                    chkContGroup.Checked = true;
                    chkContGroup_CheckedChanged(null, null);
                    chkContTransgroup.Checked = true;
                    chkContTransgroup_CheckedChanged(null, null);
                    chkContName.Checked = true;
                    chkContName_CheckedChanged(null, null);
                    chkContNumber.Checked = true;
                    chkContNumber_CheckedChanged(null, null);

                    if (!string.Equals(txtEmployee.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtEmployee.Text.Trim();
                    if (!string.Equals(txtUsergroup.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtUsergroup.Text.Trim();
                    if (!string.Equals(txtDivis.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtDivis.Text.Trim();
                    if (!string.Equals(txtDepart.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtDepart.Text.Trim();
                    if (!string.Equals(txtContUsername.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContUsername.Text.Trim();
                    if (!string.Equals(txtContGroup.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContGroup.Text.Trim();
                    if (!string.Equals(txtContTransgroup.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContTransgroup.Text.Trim();
                    if (!string.Equals(txtContName.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContName.Text.Trim();
                    if (!string.Equals(txtContNumber.Text.Trim(), ""))
                        MailAll = MailAll + "," + txtContNumber.Text.Trim();

                    if (!string.Equals(MailAll.Trim(), ""))
                        MailAll = MailAll.Remove(0, 1);

                    txtMailAll.Text = MailAll.Trim();
                    txtMailAll.Enabled = true;
                }
            }
            else
            {
                chkEmployee.Checked = false;
                chkEmployee_CheckedChanged(null, null);
                chkUsergroup.Checked = false;
                chkUsergroup_CheckedChanged(null, null);
                chkDivis.Checked = false;
                chkDivis_CheckedChanged(null, null);
                chkDepart.Checked = false;
                chkDepart_CheckedChanged(null, null);
                chkContUsername.Checked = false;
                chkContUsername_CheckedChanged(null, null);
                chkContGroup.Checked = false;
                chkContGroup_CheckedChanged(null, null);
                chkContTransgroup.Checked = false;
                chkContTransgroup_CheckedChanged(null, null);
                chkContName.Checked = false;
                chkContName_CheckedChanged(null, null);
                chkContNumber.Checked = false;
                chkContNumber_CheckedChanged(null, null);

                txtMailAll.Text = string.Empty;
                txtMailAll.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSendOther_Click(object sender, EventArgs e)
    {
        try
        {
            this.SendEmail(ConfigValue.EmailKnowledgeManagement, txtOteherEmail.Text.Trim());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (chkAll.Checked == true)
            {
                ddlContractUserName.Enabled = false;
                cmdAddContractUserName.Enabled = false;
                ddlContractUserNameDisplay.Enabled = false;
                DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, null, "SHARE_VALUE", "SHARE_DISPLAY", false);
                cmdRemoveContractUserName.Enabled = false;

                ddlContractGroup.Enabled = false;
                cmdAddContractGroup.Enabled = false;
                ddlContractGroupDisplay.Enabled = false;
                DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, null, "SHARE_VALUE", "SHARE_DISPLAY", false);
                cmdRemoveContractGroup.Enabled = false;

                ddlTransportGroup.Enabled = false;
                cmdAddTransportGroup.Enabled = false;
                ddlTransportGroupDisplay.Enabled = false;
                DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, null, "SHARE_VALUE", "SHARE_DISPLAY", false);
                cmdRemoveTransportGroup.Enabled = false;

                ddlContractName.Enabled = false;
                cmdAddContractName.Enabled = false;
                ddlContractNameDisplay.Enabled = false;
                DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, null, "SHARE_VALUE", "SHARE_DISPLAY", false);
                cmdRemoveContractName.Enabled = false;

                ddlContractNumber.Enabled = false;
                cmdAddContractNumber.Enabled = false;
                ddlContractNumberDisplay.Enabled = false;
                DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, null, "SHARE_VALUE", "SHARE_DISPLAY", false);
                cmdRemoveContractNumber.Enabled = false;

                chkContUsername.Checked = false;
                chkContUsername.Enabled = false;
                txtContUsername.Text = string.Empty;
                txtContUsername.Enabled = false;

                chkContGroup.Checked = false;
                chkContGroup.Enabled = false;
                txtContGroup.Text = string.Empty;
                txtContGroup.Enabled = false;

                chkContTransgroup.Checked = false;
                chkContTransgroup.Enabled = false;
                txtContTransgroup.Text = string.Empty;
                txtContTransgroup.Enabled = false;

                chkContName.Checked = false;
                chkContName.Enabled = false;
                txtContName.Text = string.Empty;
                txtContName.Enabled = false;

                chkContNumber.Checked = false;
                chkContNumber.Enabled = false;
                txtContNumber.Text = string.Empty;
                txtContNumber.Enabled = false;

                cmdSendContUsername.Enabled = false;
                cmdSendContGroup.Enabled = false;
                cmdSendContTransgroup.Enabled = false;
                cmdSendContName.Enabled = false;
                cmdSendContNumber.Enabled = false;

                //dtContractName.Rows.Add("", "ALL");
                this.SetShareName();
            }
            else
            {
                ddlContractUserName.Enabled = true;
                cmdAddContractUserName.Enabled = true;
                ddlContractUserNameDisplay.Enabled = true;
                if (dtContractUsername.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", false);
                else
                    DropDownListHelper.BindDropDownList(ref ddlContractUserNameDisplay, dtContractUsername, "SHARE_VALUE", "SHARE_DISPLAY", true);
                cmdRemoveContractUserName.Enabled = true;

                ddlContractGroup.Enabled = true;
                cmdAddContractGroup.Enabled = true;
                ddlContractGroupDisplay.Enabled = true;
                if (dtContractGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
                else
                    DropDownListHelper.BindDropDownList(ref ddlContractGroupDisplay, dtContractGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                cmdRemoveContractGroup.Enabled = true;

                ddlTransportGroup.Enabled = true;
                cmdAddTransportGroup.Enabled = true;
                ddlTransportGroupDisplay.Enabled = true;
                if (dtTransportGroup.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", false);
                else
                    DropDownListHelper.BindDropDownList(ref ddlTransportGroupDisplay, dtTransportGroup, "SHARE_VALUE", "SHARE_DISPLAY", true);
                cmdRemoveTransportGroup.Enabled = true;

                ddlContractName.Enabled = true;
                cmdAddContractName.Enabled = true;
                ddlContractNameDisplay.Enabled = true;
                if (dtContractName != null)
                {
                    if (dtContractName.Rows.Count > 0)
                    {
                        if (string.Equals(dtContractName.Rows[0]["SHARE_DISPLAY"].ToString().Trim(), "ALL") || string.Equals(dtContractName.Rows[0]["SHARE_DISPLAY"].ToString().Trim(), "- เลือกข้อมูล -"))
                        {
                            dtContractName.Clear();
                            DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
                        }
                        else
                        {
                            DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", false);
                        }
                    }
                    else
                        DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
                }
                else
                {
                    DropDownListHelper.BindDropDownList(ref ddlContractNameDisplay, dtContractName, "SHARE_VALUE", "SHARE_DISPLAY", true);
                }
                cmdRemoveContractName.Enabled = true;

                ddlContractNumber.Enabled = true;
                cmdAddContractNumber.Enabled = true;
                ddlContractNumberDisplay.Enabled = true;
                if (dtContractNumber.Rows.Count > 0)
                    DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", false);
                else
                    DropDownListHelper.BindDropDownList(ref ddlContractNumberDisplay, dtContractNumber, "SHARE_VALUE", "SHARE_DISPLAY", true);
                cmdRemoveContractNumber.Enabled = true;

                chkContUsername.Enabled = true;
                cmdSendContUsername.Enabled = true;

                chkContGroup.Enabled = true;
                cmdSendContGroup.Enabled = true;

                chkContTransgroup.Enabled = true;
                cmdSendContTransgroup.Enabled = true;

                chkContName.Enabled = true;
                cmdSendContName.Enabled = true;

                chkContNumber.Enabled = true;
                cmdSendContNumber.Enabled = true;

                this.SetShareName();

                chkSendAll.Checked = false;
                chkSendAll_CheckedChanged(null, null);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("KM.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetShareName()
    {
        try
        {
            dtAllShare.Clear();
            DataTable dt1 = new DataTable();
            DataTable dt2 = new DataTable();
            DataTable dt3 = new DataTable();
            DataTable dt4 = new DataTable();
            DataTable dt5 = new DataTable();
            DataTable dt6 = new DataTable();
            DataTable dt7 = new DataTable();
            DataTable dt8 = new DataTable();
            DataTable dt9 = new DataTable();

            if (chkAll.Checked == false)
            {
                if (dtEmployee.Rows.Count > 0)
                {
                    dt1 = dtEmployee.Copy();
                    dt1.Rows.RemoveAt(0);
                    dt1.Columns.Add("SHARE_GROUP");
                    dt1.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt1.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Employee code";
                    }
                }
                if (dtUsergroup.Rows.Count > 0)
                {
                    dt2 = dtUsergroup.Copy();
                    dt2.Rows.RemoveAt(0);
                    dt2.Columns.Add("SHARE_GROUP");
                    dt2.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt2.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "User group (TMS)";
                    }
                }
                if (dtDepart.Rows.Count > 0)
                {
                    dt3 = dtDepart.Copy();
                    dt3.Rows.RemoveAt(0);
                    dt3.Columns.Add("SHARE_GROUP");
                    dt3.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt3.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Department";
                    }
                }
                if (dtDivis.Rows.Count > 0)
                {
                    dt4 = dtDivis.Copy();
                    dt4.Rows.RemoveAt(0);
                    dt4.Columns.Add("SHARE_GROUP");
                    dt4.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt4.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Division";
                    }
                }
                if (dtContractUsername.Rows.Count > 0)
                {
                    dt5 = dtContractUsername.Copy();
                    dt5.Rows.RemoveAt(0);
                    dt5.Columns.Add("SHARE_GROUP");
                    dt5.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt5.Rows)
                    {
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "Name";
                    }
                }
                if (dtContractGroup.Rows.Count > 0)
                {
                    dt6 = dtContractGroup.Copy();
                    dt6.Rows.RemoveAt(0);
                    dt6.Columns.Add("SHARE_GROUP");
                    dt6.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt6.Rows)
                    {
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "User group (TMS)";
                    }
                }
                if (dtTransportGroup.Rows.Count > 0)
                {
                    dt7 = dtTransportGroup.Copy();
                    dt7.Rows.RemoveAt(0);
                    dt7.Columns.Add("SHARE_GROUP");
                    dt7.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt7.Rows)
                    {
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "Transportation group";
                    }
                }
                if (dtContractName.Rows.Count > 0)
                {
                    dt8 = dtContractName.Copy();
                    dt8.Rows.RemoveAt(0);
                    dt8.Columns.Add("SHARE_GROUP");
                    dt8.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt8.Rows)
                    {
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "Contractor name";
                    }
                }
                if (dtContractNumber.Rows.Count > 0)
                {
                    dt9 = dtContractNumber.Copy();
                    dt9.Rows.RemoveAt(0);
                    dt9.Columns.Add("SHARE_GROUP");
                    dt9.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt9.Rows)
                    {
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "Contract number";
                    }
                }
            }
            else
            {
                if (dtEmployee.Rows.Count > 0)
                {
                    dt1 = dtEmployee.Copy();
                    dt1.Rows.RemoveAt(0);
                    dt1.Columns.Add("SHARE_GROUP");
                    dt1.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt1.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Employee code";
                    }
                }
                if (dtUsergroup.Rows.Count > 0)
                {
                    dt2 = dtUsergroup.Copy();
                    dt2.Rows.RemoveAt(0);
                    dt2.Columns.Add("SHARE_GROUP");
                    dt2.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt2.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "User group (TMS)";
                    }
                }
                if (dtDepart.Rows.Count > 0)
                {
                    dt3 = dtDepart.Copy();
                    dt3.Rows.RemoveAt(0);
                    dt3.Columns.Add("SHARE_GROUP");
                    dt3.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt3.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Department";
                    }
                }
                if (dtDivis.Rows.Count > 0)
                {
                    dt4 = dtDivis.Copy();
                    dt4.Rows.RemoveAt(0);
                    dt4.Columns.Add("SHARE_GROUP");
                    dt4.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in dt4.Rows)
                    {
                        row["SHARE_GROUP"] = "PTT/BSA";
                        row["SHARE_TYPE"] = "Division";
                    }
                }
                DataTable kmvendorall = KMBLL.Instance.SelectKMVendorBLL(" AND CACTIVE = '1' ");
                if (kmvendorall.Rows.Count > 0)
                {
                    kmvendorall.Columns.Add("SHARE_VALUE");
                    kmvendorall.Columns.Add("SHARE_DISPLAY");
                    kmvendorall.Columns.Add("SHARE_GROUP");
                    kmvendorall.Columns.Add("SHARE_TYPE");
                    foreach (DataRow row in kmvendorall.Rows)
                    {
                        row["SHARE_VALUE"] = row["SVENDORID"];
                        row["SHARE_DISPLAY"] = row["SABBREVIATION"];
                        row["SHARE_GROUP"] = "Contractor";
                        row["SHARE_TYPE"] = "Contractor name";
                    }

                    dt8 = kmvendorall.Copy();
                }
            }

            foreach (DataRow dr in dt1.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt2.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt3.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt4.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt5.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt6.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            foreach (DataRow dr in dt7.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }
            if (chkAll.Checked == false)
            {
                foreach (DataRow dr in dt8.Rows)
                {
                    dtAllShare.Rows.Add(dr.ItemArray);
                }
            }
            else
            {
                foreach (DataRow dr in dt8.Rows)
                {
                    dtAllShare.Rows.Add(dr["SHARE_VALUE"], dr["SHARE_DISPLAY"], dr["SHARE_GROUP"], dr["SHARE_TYPE"]);
                }
            }
            foreach (DataRow dr in dt9.Rows)
            {
                dtAllShare.Rows.Add(dr.ItemArray);
            }

            dgvShareName.DataSource = dtAllShare;
            dgvShareName.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvShareName_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvShareName.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvShareName, dtAllShare);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}