﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" 
    CodeFile="UserGroup1.aspx.cs" Inherits="UserGroup1" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15
        {
            color: #03527C;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">   
     <div class="container" style="width: 100%;">
                <div class="jumbotron" style="background-color: white">
                    <asp:UpdatePanel ID="udpMain" runat="server">
                        <ContentTemplate>
                            <h2 class="page-header" style="margin-top: 10px">
                                <font style="font-family: 'Angsana New'; color: #009120"> กำหนดกลุ่มผู้ใช้งาน</font>
                            </h2>
                             <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">เงื่อนไขการค้นหา&#8711;</a>
                                        <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                    </div>

                                    <div class="panel-collapse collapse in" id="collapseFindContract">
                                        <div>
                                            <br />
                                            <asp:Table runat="server" Width="100%">
                                                <asp:TableRow>
                                                    <asp:TableCell Width="10%" Style="text-align: right">
                                                        <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน :&nbsp;"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="15%">
                                                        <asp:TextBox ID="txtUserGroup" runat="server" Width="90%" CssClass="form-control"></asp:TextBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="10%" Style="text-align: right">
                                                        <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="15%">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" Width="90%" CssClass="form-control"></asp:DropDownList>
                                                    </asp:TableCell>                                                  
                                                                                     
                                                    <asp:TableCell Width="15%" Style="text-align: right">
                                                        <asp:Label ID="Label1" runat="server" Text="สถานะพิเศษ :&nbsp;"></asp:Label>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="25%">
                                                        <asp:DropDownList ID="ddlSpecialStatus" runat="server" Width="90%" CssClass="form-control"></asp:DropDownList>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <br />
                                            <div class="panel-footer" style="text-align: right">
                                                <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" CssClass="btn btn-success" Width="90px" OnClick="cmdSearch_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                                        <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                                    </div>
                                    <div class="panel-collapse collapse in" id="collapseFindContract2">
                                        <div class="panel-body" align="center">
                                            <br />
                                            <asp:GridView ID="dgvData" runat="server" Width="65%" AutoGenerateColumns="False" OnRowCommand="dgvData_RowCommand"
                                                AllowPaging="true" OnPageIndexChanging="dgvData_PageIndexChanging" PageSize="10"
                                                GridLines="None" HeaderStyle-CssClass="gvHeader" CssClass="gvRow" AlternatingRowStyle-CssClass="gvAltRow" ShowHeaderWhenEmpty="True" DataKeyNames="USERGROUP_ID">
                                                <AlternatingRowStyle CssClass="gvAltRow"></AlternatingRowStyle>
                                                <Columns>
                                                    <%--<asp:BoundField />--%>
                                                    <asp:TemplateField HeaderText="View">
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/64/blue-37.png" Width="24px" Height="24px" Style="cursor: pointer" CommandName="View" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <HeaderStyle Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/64/blue-23.png" Width="24px" Height="24px" Style="cursor: pointer" CommandName="EditData" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="USERGROUP_ID" HeaderText="" Visible="false">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="USERGROUP_NAME" HeaderText="กลุ่มผู้ใช้งาน">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IS_ADMIN_TEXT" HeaderText="สถานะพิเศษ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="สถานะ">
                                                        <HeaderStyle Width="100px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox runat="server" Checked='<%# Convert.ToString(Eval("IS_ACTIVE")) == "1" ? true : false %>' Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gvHeader"></HeaderStyle>
                                                <PagerStyle CssClass="pagination-ys" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            </asp:GridView>
                         
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="panel-footer" style="text-align: right">
                                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่มรายการ" CssClass="btn btn-success" Width="90px" OnClick="cmdAdd_Click" />
                                        <asp:Button ID="cmdRefresh" runat="server" Text="รีเฟรช" CssClass="btn btn-success" Width="90px" OnClick="cmdRefresh_Click" />
                                    </div>
                                </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
     <script>
                  $(document).ready(function () {

                      $(window).resize(function () {

                      });
                  });
            </script>
</asp:Content>

