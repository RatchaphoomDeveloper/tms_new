﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallbackPanel;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using TMS_BLL.Master;
using System.Web.Security;
using TMS_BLL.Transaction.Complain;
using EmailHelper;

public partial class checktruck : PageBase
{
    int defaultInt = 0;
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string TempDirectory = "UploadFile/checktruck/{0}/{2}/{1}/";
    string Title = string.Empty;
    string hideType;
    DataTable dtAddCheckListItems;
    DataTable dtAddCheckListItems2;
    DataTable dtAddCheckListItems22;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            Response.Redirect("Default.aspx");
        }
        if (!IsPostBack)
        {
            Session["HaveIssue"] = false;

            Session["ConfirmChecktruck"] = null;
            var dtFile = new List<dtFile>();
            Session["dtFile"] = dtFile;

            var dtFileTemp = new List<dtFileTemp>();
            Session["dtFileTemp"] = dtFileTemp;

            var dtFileOther = new List<dtFileOther>();
            Session["dtFileOther"] = dtFileOther;

            var dtCHECKED = new List<dtCHECKED>();
            Session["dtCHECKED"] = dtCHECKED;

            Session["dtAddCheckListItems"] = null;
            dtAddCheckListItems = new DataTable();
            dtAddCheckListItems.Columns.Add("imagename", typeof(string));
            dtAddCheckListItems.Columns.Add("filename", typeof(string));
            dtAddCheckListItems.Columns.Add("memo", typeof(string));
            Session["dtAddCheckListItems"] = dtAddCheckListItems;

            Session["dtAddCheckListItems2"] = null;
            dtAddCheckListItems2 = new DataTable();
            dtAddCheckListItems2.Columns.Add("dtID", typeof(string));
            dtAddCheckListItems2.Columns.Add("imagename", typeof(string));
            dtAddCheckListItems2.Columns.Add("filename", typeof(string));
            dtAddCheckListItems2.Columns.Add("memo", typeof(string));
            Session["dtAddCheckListItems2"] = dtAddCheckListItems2;

            Session["dtAddCheckListItems22"] = null;
            dtAddCheckListItems22 = new DataTable();
            //dtAddCheckListItems22.Columns.Add("dtID", typeof(string));
            dtAddCheckListItems22.Columns.Add("imagename", typeof(string));
            dtAddCheckListItems22.Columns.Add("filename", typeof(string));
            dtAddCheckListItems22.Columns.Add("memo", typeof(string));
            Session["dtAddCheckListItems22"] = dtAddCheckListItems22;

            //Cache.Remove(sdsCheckLists.CacheKeyDependency);
            //Cache[sdsCheckLists.CacheKeyDependency] = new object();
            //sdsCheckLists.Select(new System.Web.UI.DataSourceSelectArguments());
            BindData();

            Session["FileUpload"] = null;
            Session["dtCheckData"] = null;
            this.AssignAuthen();


        }
        else
        {
            BindData();

            //dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];

        }
        //updMyModal.Update();
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {
                //btnAddOther.Enabled = false;
                //btnsubmit.Enabled = false;
                //btnsubmitTab2.Enabled = false;
                //uploaderOther.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region Serializable
    [Serializable]

    struct dtFile
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
        public string dtSCHECKLISTID { get; set; }
        public string dtSVERSIONLIST { get; set; }
        public string dtSTYPECHECKLISTID { get; set; }
        public string dtProcess { get; set; }
        public string dtPicIDdiff { get; set; }
        public string dtNewFile { get; set; }
    }

    struct dtFileTemp
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
        public string dtSCHECKLISTID { get; set; }
        public string dtSVERSIONLIST { get; set; }
        public string dtSTYPECHECKLISTID { get; set; }
        public string dtProcess { get; set; }
        public string dtPicIDdiff { get; set; }
        public string dtNewFile { get; set; }
    }

    struct dtFileOther
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
    }

    struct dtCHECKED
    {
        public string dtSCHECKLISTID { get; set; }
        public string dtSVERSIONLIST { get; set; }
        public string dtSTYPECHECKLISTID { get; set; }
        public string dtCHECK { get; set; }
        public string dtRemark { get; set; }
    }

    #endregion

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void gvw_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[5].Visible = false;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string str = Request.QueryString["str"];
            string[] xgvwData = new string[9];
            if (!string.IsNullOrEmpty(str))
            {
                //string[] xgvwData = new string[9];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                xgvwData = strQuery.Split('&');
            }



            DataTable dtCheckData;
            if (Session["dtCheckData"] == null)
            {
                string qry = @"SELECT CHKTRCK.*, ICHKTRCK.*
, NVL(ICHKTRCK.SCREATENAME,TUSR.SFIRSTNAME||' '||TUSR.SLASTNAME) SCREATENAMED
,ICHKTRCK.CMA ICMA,ICHKTRCK.NDAY_MA INDAY_MA
FROM (
    SELECT distinct * FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + xgvwData[1] + @"'
)CHKTRCK
LEFT JOIN (
    SELECT TCHECKTRUCKITEM.*, TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SCREATENAME
    FROM TCHECKTRUCKITEM LEFT JOIN TUser ON TCHECKTRUCKITEM.SCREATE=TUSER.SUID
    WHERE 1=1 --AND NVL(CEDITED,'0')='0'
     and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + xgvwData[1] + @"')
)ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
LEFT JOIN TUSER TUSR ON CHKTRCK.SCREATE=TUSR.SUID
WHERE  NVL(COK,'0')='0'";

                dtCheckData = CommonFunction.Get_Data(connection, qry);

                if (dtCheckData.Rows.Count > 0)
                {
                    Session["dtCheckData"] = dtCheckData;
                }
            }
            else
            {
                dtCheckData = (DataTable)Session["dtCheckData"];
            }

            string CBAN = "", CCUT = "", NDAY_MA = "", ReportBy = "", SCHECKID = ""; int HasCheckList = 0;

            string[] ArrayRowValues = new string[] { (e.Row.DataItem as DataRowView).Row["SCHECKLISTID"].ToString(), (e.Row.DataItem as DataRowView).Row["SCHECKLISTNAME"].ToString() };

            foreach (DataRow drCheckData in dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND CCHECKED = '0'"))
            {
                HasCheckList = 1;
                SCHECKID = drCheckData["SCHECKID"] + "";
                ReportBy = drCheckData["SCREATENAMED"] + "";
                break;
            }
            //string CheckID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SCHECKLISTID"));

            //Session["IssueData"] = null;
            //Session["IssueData"] = "" + xgvwData[0] + "^" + xgvwData[1] + "^" + xgvwData[2] + "^" + xgvwData[3] + "^" + xgvwData[4] + "^" + xgvwData[5] + "^" + xgvwData[6] + "^" + xgvwData[7] + "^" + SCHECKID;

            string[] Data = ("" + Session["IssueData"]).Split('^');
            CBAN = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'").Length > 0 ? "1" : "0";
            CCUT = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'").Length > 0 ? "1" : "0";
            DataRow[] drBan = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'", "DEND_LIST ,DBEGIN_LIST");
            DataRow[] drNDAYMA = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'", "DEND_LIST ,DBEGIN_LIST");
            NDAY_MA = drNDAYMA.Length > 0 ? "" + drNDAYMA[0]["INDAY_MA"] : "0";
            bool lblCHoldShow = false;
            string lblCHoldShowTxt = "";
            bool lblMAnDayShow = false;
            string lblMAnDayShowTxt = "";
            bool lblCheckby = false;
            if (HasCheckList > 0)// (dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "'").Length > 0)
            {
                string SCHECKLISTID = ((CBAN == "1") ? "2" : ((CCUT == "1") ? "1" : "0"));
                lblCHoldShow = (SCHECKLISTID == "2") ? true : false;
                lblCHoldShowTxt = "ห้ามวิ่ง" + (drBan.Length > 0 ? "(เมื่อ " + Convert.ToDateTime("" + drBan[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "");
                lblMAnDayShow = (SCHECKLISTID == "1") ? true : false;
                lblMAnDayShowTxt = (NDAY_MA != "") ? "แก้ไขภายใน " + NDAY_MA + " วัน" + (drNDAYMA.Length > 0 ? "(ภายใน " + Convert.ToDateTime("" + drNDAYMA[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "") : "";
                ReportBy = CommonFunction.Get_Value(sql, @"   SELECT TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SCREATENAME
    FROM TCHECKTRUCKITEM LEFT JOIN TUser ON TCHECKTRUCKITEM.SCREATE=TUSER.SUID
    WHERE 1=1  AND ROWNUM <=1  and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + Data[1] + @"')
    AND SCHECKLISTID='" + ArrayRowValues[0] + "' ORDER BY TCHECKTRUCKITEM.DCHECKLIST DESC");
                //lblCheckBy.Text = "<img title='" + ReportBy + "' src='images/ic_owner.gif' />";
                Title += "," + ArrayRowValues[1];
            }

            if (lblCHoldShow || lblMAnDayShow)
            {
                lblCheckby = true;
            }
            HtmlTable SubTB = new HtmlTable();
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            SubTB.Style.Add("width", "100% !important");
            row.Style.Add("width", "100% !important");

            Label lblItem = new Label();
            lblItem.ID = "items";
            lblItem.Text = (e.Row.DataItem as DataRowView).Row["SCHECKLISTNAME"].ToString();
            cell.Style.Add("width", "60% !important");
            cell.Controls.Add(lblItem);
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            Label lblCHOLD = new Label();
            lblCHOLD.ID = "holds";
            lblCHOLD.Visible = lblCHoldShow;
            lblCHOLD.ForeColor = System.Drawing.Color.Red;
            lblCHOLD.Text = lblCHoldShowTxt;
            //cell.Style.Add("width", "0% !important");
            cell.Controls.Add(lblCHOLD);

            Label lblManDay = new Label();
            lblManDay.ID = "Manday";
            lblManDay.Visible = lblMAnDayShow;
            lblManDay.ForeColor = System.Drawing.Color.Red;
            lblManDay.Text = lblMAnDayShowTxt;
            cell.Controls.Add(lblManDay);

            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            Label lblCheckBy = new Label();
            lblCheckBy.Visible = lblCheckby;
            lblCheckBy.ID = "CheckBy";
            lblCheckBy.ForeColor = System.Drawing.Color.Red;
            lblCheckBy.Text = "<img title='" + ReportBy + "' src='images/ic_owner.gif' />";
            cell.Controls.Add(lblCheckBy);
            row.Cells.Add(cell);

            SubTB.Rows.Add(row);
            e.Row.Cells[1].Controls.Add(SubTB);
            if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2" || "" + Session["CGROUP"] == "0")
            {
                Button btnIssue = (Button)(e.Row.Cells[2].Controls[0]);// gvw.Rows[e.Row.RowIndex].Cells[2].Controls[0].FindControl("btnfield");
                Button btnEdit = (Button)(e.Row.Cells[3].Controls[0]);
                Button btnCheckEdit = (Button)(e.Row.Cells[4].Controls[0]);

                //btnIssue.Text = "แจ้งพบปัญหา";
                if ("" + Session["ConfirmChecktruck"] == "1")
                {
                    btnIssue.Enabled = false;
                }

                if ("" + Session["CGROUP"] == "0")
                {
                    btnCheckEdit.Enabled = false;
                    btnEdit.Enabled = true;
                }

                if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
                {
                    btnEdit.Enabled = false;
                }


                if (Session["dtFile"] != null)
                {
                    //ASPxImage imgPic0 = gvwItemCheckList.FindRowCellTemplateControl(VisibleIndex, null, "imgPic0") as ASPxImage;
                    //ASPxImage imgPic = gvwItemCheckList.FindRowCellTemplateControl(VisibleIndex, null, "imgPic") as ASPxImage;
                    //ASPxImage imgPic1 = gvwItemCheckList.FindRowCellTemplateControl(VisibleIndex, null, "imgPic1") as ASPxImage;

                    string SCHECKLISTID = (e.Row.DataItem as DataRowView).Row["SCHECKLISTID"].ToString();//["SCHECKLISTID"] + "";
                    string STYPECHECKLISTID = (e.Row.DataItem as DataRowView).Row["STYPECHECKLISTID"].ToString(); //e.GetValue("STYPECHECKLISTID") + "";
                    string SVERSIONLIST = (e.Row.DataItem as DataRowView).Row["SVERSIONLIST"].ToString(); //e.GetValue("SVERSIONLIST") + "";
                    var MyData = (List<dtFile>)Session["dtFile"];//STYPECHECKLISTID,SVERSIONLIST
                    var DataDetail = MyData.Where(o => o.dtSCHECKLISTID == SCHECKLISTID && o.dtSTYPECHECKLISTID == STYPECHECKLISTID && o.dtSVERSIONLIST == SVERSIONLIST);

                    if (DataDetail.Where(o => o.dtProcess == "1" && o.dtNewFile == "1").Count() > 0)
                    {
                        //imgPic0.Enabled = true;
                        btnIssue.Text = "แจ้งพบปัญหา";
                        //btnIssue.BackColor = System.Drawing.Color.Red;
                    }

                    if (DataDetail.Where(o => o.dtProcess == "1" && o.dtNewFile == "0").Count() == 0)
                    {
                        btnEdit.Enabled = false;
                    }
                    else
                    {
                        //imgPic.ClientVisible = true;
                        btnIssue.Text = "แจ้งพบปัญหา";
                        //btnIssue.BackColor = System.Drawing.Color.Red;
                    }

                    if (DataDetail.Where(o => o.dtProcess == "2" && o.dtNewFile == "0").Count() == 0)
                    {
                        btnCheckEdit.Enabled = false;
                    }
                    else
                    {
                        btnCheckEdit.Text = "ผลตรวจแก้ไข";
                        //btnCheckEdit.BackColor = System.Drawing.Color.Red;
                        //imgPic1.ClientVisible = true;
                        //imgPic.ClientVisible = false;
                    }
                }
                else
                {
                    btnIssue.Enabled = false;
                    btnEdit.Enabled = false;
                    btnCheckEdit.Enabled = false;
                }
            }
            else
            {
                Button btnIssue = (Button)(e.Row.Cells[2].Controls[0]);
                Button btnEdit = (Button)(e.Row.Cells[3].Controls[0]);
                Button btnCheckEdit = (Button)(e.Row.Cells[4].Controls[0]);

                btnIssue.Enabled = false;
                btnEdit.Enabled = false;
                btnCheckEdit.Enabled = false;
            }
        }
        if (e.Row.RowType == DataControlRowType.Header)
        {
            int NumCells = e.Row.Cells.Count;
            for (int i = 0; i < NumCells - 1; i++)
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
            }
        }
    }

    protected void btnCheck1_OnClick(object sender, EventArgs e)
    {
        int index = gvw.SelectedIndex;
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
        //UpdatePanel1.Update();
    }

    //protected void PartialPostBack(object sender, EventArgs e)
    //{
    //    int id = Convert.ToInt32(((sender as Button).NamingContainer as GridViewRow).Cells[0].Text);
    //    int row_index = ((id - 1) + ((id - 1) + 3));
    //    //string message = "alert('Partial PostBack: You clicked " + fruitName + "');";
    //    //ScriptManager.RegisterClientScriptBlock(sender as Control, this.GetType(), "alert", message, true);
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "showPanel", "javascript: row_num = " + row_index
    //            + "; ctl_row = " + row_index + " - 1; rows = document.getElementById('"
    //            + gvw.ClientID + "').rows; rowElement = rows[ctl_row];img = rowElement.cells[0].firstChild; if (rows[row_num].className !== 'hidden') "
    //            + " { rows[row_num].className = 'hidden'; img.src = '../Images/autocomplete.gif';} "
    //            + " else { rows[row_num].className = ''; img.src = '../Images/autocomplete.gif';} return false;", true);
    //}

    protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView ProductGrid = (GridView)sender;

            // Creating a Row
            GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "ที่";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "รายการตรวจสอบ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "STEP 1: (ทั้งหมด)";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 1;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "STEP 2: (ผู้ขนส่ง)";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 1;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "STEP 3: (ปง.และคลัง)";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 1;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            ProductGrid.Controls[0].Controls.AddAt(0, HeaderRow);
        }
    }

    public DataTable DataLoad()
    {
        DataTable result = new DataTable();
        string message = string.Empty;
        try
        {
            string query = "SELECT rownum SKEYID, TOCL.STYPECHECKLISTID,TOCL.STYPECHECKLISTNAME,TOCL.CACTIVE TOCL_CACTIVE,TOCL.CCASE,TOCL.CDESPOA,TOCL.CDESPOB,TOCL.CSUREPRISE,TOCL.CVENDOR,TOCL.CTYPE,TOCL.SMENUID"
                        + " , GOCL.SGROUPID ,GOCL.SGROUPNAME ,GOCL.CGAS ,GOCL.COIL ,GOCL.CACTIVE GOCL_CACTIVE"
                        + " , CL.SVERSIONLIST ,CL.SVERSION ,CL.STOPICID ,CL.SCHECKLISTNAME ,CL.SCHECKLISTID ,CL.NLIST ,CL.NDAY_MA ,CL.CCUT ,CL.CBAN ,CL.CACTIVE CL_CACTIVE, TP.NPOINT"
                        + "  FROM TTYPEOFCHECKLIST TOCL "
                        + " LEFT JOIN TGROUPOFCHECKLIST GOCL ON TOCL.STYPECHECKLISTID = GOCL.STYPECHECKLISTID "
                        + " LEFT JOIN TCHECKLIST CL ON TOCL.STYPECHECKLISTID = CL.STYPECHECKLISTID AND GOCL.SGROUPID = CL.SGROUPID "
                        + " LEFT JOIN TTOPIC TP ON CL.STOPICID = TP.STOPICID "
                        + " where 1 = 1 "
                        + " AND NVL(TOCL.CACTIVE,'0')= '1' AND NVL(GOCL.CACTIVE,'0')= '1' AND NVL(CL.CACTIVE,'0')= '1' AND NVL(TP.CACTIVE,'0')= '1' "
                        + " AND TOCL.CTYPE = '" + hideType.ToString() + "' AND TOCL.CDESPOA = '1'";
            // --AND TOCL.STYPECHECKLISTID = '1'">";

            result = CommonFunction.Get_Data(sql, query);
            //gvw.DataSource = dt;
            //gvw.DataBind();

            //gvw.HeaderRow.Cells[7].Attributes["data-class"] = "expand";
            //gvw.HeaderRow.TableSection = TableRowSection.TableHeader;

            //lblCarCount.Text = dt.Rows.Count.ToString();
        }

        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            //GlobalClass.CloseConnection(out message);

        }

        return result;
    }

    protected void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var obj = (GridView)sender;
        int rowindex = obj.SelectedIndex;
        //ViewState["gvw_" + groupID] = dtmain;
        string[] myarray = e.CommandName.Split('_');
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        DataTable dt = (DataTable)ViewState["gvw_" + myarray[1]];
        DataRow dr = dt.Rows[rowIndex];
        Session["dtCheckList"] = dt;
        Session["DrCheckList"] = dr;
        hidRowIndex.Value = rowIndex + string.Empty;
        Session["GroupID"] = myarray[1];
        string name = dr["SCHECKLISTNAME"].ToString();
        gvw_AddCheckList.DataSource = null;
        gvw_AddCheckList.DataBind();
        lblCheckerItems.Text = name;

        dtAddCheckListItems = null;
        Session["dtAddCheckListItems"] = null;

        if (e.CommandName.IndexOf("checker") > -1)
        {
            //var dtfile = new List<dtFile>();

            var dtfile = (List<dtFile>)Session["dtFile"];
            if (dtfile == null || dtfile.Count == 0)
            {
                string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + hidSTRUCKID.Value + @"'";

                string SCHECKID = "";
                DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
                if (dtCheckData.Rows.Count > 0)
                {
                    SCHECKID = dtCheckData.Rows[0]["SCHECKID"] + "";

                    //var dtfile = (List<dtFile>)Session["dtFile"];
                    foreach (DataRow drData in dtCheckData.Rows)
                    {
                        dtfile.Add(new dtFile
                        {
                            dtID = Convert.ToInt32(drData["NNO"]),
                            dtSCHECKLISTID = drData["SCHECKLISTID"] + "",
                            dtSVERSIONLIST = drData["SVERSIONLIST"] + "",
                            dtSTYPECHECKLISTID = drData["STYPECHECKLISTID"] + "",
                            dtEvidenceName = drData["SEVIDENCENAME"] + "",
                            dtFileName = drData["SFILENAME"] + "",
                            dtFilePath = drData["SFILEPATH"] + "",
                            dtGenFileName = drData["SGENFILENAME"] + "",
                            dtProcess = drData["SPROCESS"] + "",
                            dtPicIDdiff = drData["SMAINPICTURE"] + "",
                            dtNewFile = "0"

                        });
                    }

                    Session["dtFile"] = dtfile;
                }
            }
            //var dtfile = (List<dtFile>)Session["dtFile"];
            var ml = dtfile.Where(o => o.dtSCHECKLISTID == dt.Rows[rowIndex]["SCHECKLISTID"].ToString()).ToList();
            Session["NewIssue"] = true;
            if (ml != null && ml.Count > 0)
            {
                if (ml[0].dtNewFile == "0")
                {
                    //btnSave.Enabled = false;
                    btnAdd.Enabled = false;
                    Session["NewIssue"] = false;
                }
                else
                {
                    //btnSave.Enabled = true;
                    btnAdd.Enabled = true;
                    Session["NewIssue"] = true;
                }
                dtAddCheckListItems = new DataTable();
                dtAddCheckListItems.Columns.Add("imagename", typeof(string));
                dtAddCheckListItems.Columns.Add("filename", typeof(string));
                dtAddCheckListItems.Columns.Add("memo", typeof(string));

                //dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];
                //if (dtAddCheckListItems == null)
                //{

                //}

                for (int i = 0; i < ml.Count; i++)
                {
                    dtAddCheckListItems.Rows.Add(("~\\" + ml[i].dtFilePath).ToString().Replace("/", "\\"), ml[i].dtGenFileName, ml[i].dtEvidenceName);
                }

                Session["dtAddCheckListItems"] = dtAddCheckListItems;

                gvw_AddCheckList.DataSource = dtAddCheckListItems;
                gvw_AddCheckList.DataBind();

                if (Session["dtFile"] != null && Session["dtFileTemp"] != null)
                {
                    var vdtfiletemp = (List<dtFileTemp>)Session["dtFileTemp"];
                    vdtfiletemp.RemoveAll(o => 1 == 1);

                    string SCHECKLISTID = dt.Rows[rowIndex]["SCHECKLISTID"].ToString();//.GetRowValues(rowIndex, "SCHECKLISTID") + "";
                    string SVERSIONLIST = dt.Rows[rowIndex]["SVERSIONLIST"].ToString();//gvwItemCheckList.GetRowValues(rowIndex, "SVERSIONLIST") + "";
                    string STYPECHECKLISTID = dt.Rows[rowIndex]["STYPECHECKLISTID"].ToString();//gvwItemCheckList.GetRowValues(rowIndex, "STYPECHECKLISTID") + "";


                    var detailfile = (List<dtFile>)Session["dtFile"];
                    var detailTemp = detailfile.Where(o => o.dtSCHECKLISTID == SCHECKLISTID && o.dtSVERSIONLIST == SVERSIONLIST && o.dtSTYPECHECKLISTID == STYPECHECKLISTID && o.dtProcess == "1");
                    if (detailTemp.Count() > 0)
                    {

                        foreach (var detail in detailTemp)
                        {
                            vdtfiletemp.Add(new dtFileTemp
                            {
                                dtID = detail.dtID,
                                dtEvidenceName = detail.dtEvidenceName,
                                dtFileName = detail.dtFileName,
                                dtGenFileName = detail.dtGenFileName,
                                dtFilePath = detail.dtFilePath,
                                dtSCHECKLISTID = detail.dtSCHECKLISTID,
                                dtSTYPECHECKLISTID = detail.dtSTYPECHECKLISTID,
                                dtSVERSIONLIST = detail.dtSVERSIONLIST,
                                dtProcess = "1",
                                dtNewFile = detail.dtNewFile
                            });
                        }

                        //ASPxCallbackPanel xcpnT = SubPageControl.FindControl("xcpnT1") as ASPxCallbackPanel;
                        //ASPxGridView sgvwFile = xcpnT.FindControl("sgvwFile") as ASPxGridView;
                        //sgvwFile.DataSource = vdtfiletemp;
                        //sgvwFile.DataBind();

                    }

                    Session["dtFileTemp"] = vdtfiletemp;
                }
            }
            else
            {
                var vdtfiletemp = (List<dtFileTemp>)Session["dtFileTemp"];
                vdtfiletemp.RemoveAll(o => 1 == 1);
                Session["dtFileTemp"] = vdtfiletemp;

                btnSave.Enabled = true;
                btnAdd.Enabled = true;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
            //updMyModal.Update();
        }
        else if (e.CommandName.IndexOf("revised") > -1)
        {
            var dtfile = new List<dtFile>();
            Session["dtFile"] = dtfile;
            if (dtfile == null || dtfile.Count == 0)
            {
                string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + hidSTRUCKID.Value + @"'";

                string SCHECKID = "";
                DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
                if (dtCheckData.Rows.Count > 0)
                {
                    SCHECKID = dtCheckData.Rows[0]["SCHECKID"] + "";

                    //var dtfile = (List<dtFile>)Session["dtFile"];
                    foreach (DataRow drData in dtCheckData.Rows)
                    {
                        dtfile.Add(new dtFile
                        {
                            dtID = Convert.ToInt32(drData["NNO"]),
                            dtSCHECKLISTID = drData["SCHECKLISTID"] + "",
                            dtSVERSIONLIST = drData["SVERSIONLIST"] + "",
                            dtSTYPECHECKLISTID = drData["STYPECHECKLISTID"] + "",
                            dtEvidenceName = drData["SEVIDENCENAME"] + "",
                            dtFileName = drData["SFILENAME"] + "",
                            dtFilePath = drData["SFILEPATH"] + "",
                            dtGenFileName = drData["SGENFILENAME"] + "",
                            dtProcess = drData["SPROCESS"] + "",
                            dtPicIDdiff = drData["SMAINPICTURE"] + "",
                            dtNewFile = "0"

                        });
                    }

                    Session["dtFile"] = dtfile;
                }
            }
            //var dtfile = (List<dtFile>)Session["dtFile"];
            var ml = dtfile.Where(o => o.dtSCHECKLISTID == dt.Rows[rowIndex]["SCHECKLISTID"].ToString() && o.dtProcess == "1").ToList();
            Session["NewIssue"] = true;
            if (ml != null && ml.Count > 0)
            {
                if (ml[0].dtNewFile == "0")
                {
                    //btnSave2.Enabled = false;
                    btnAdd2.Enabled = false;
                    Session["NewIssue"] = false;
                }
                else
                {
                    //btnSave2.Enabled = true;
                    btnAdd2.Enabled = true;
                    Session["NewIssue"] = true;
                }
                Session["dtAddCheckListItems"] = null;

                dtAddCheckListItems = new DataTable();
                dtAddCheckListItems.Columns.Add("imagename", typeof(string));
                dtAddCheckListItems.Columns.Add("filename", typeof(string));
                dtAddCheckListItems.Columns.Add("memo", typeof(string));
                //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));

                for (int i = 0; i < ml.Count; i++)
                {
                    dtAddCheckListItems.Rows.Add(("~\\" + ml[i].dtFilePath).ToString().Replace("/", "\\"), ml[i].dtGenFileName, ml[i].dtEvidenceName);
                }

                Session["dtAddCheckListItems"] = dtAddCheckListItems;

                gvw_AddCheckList2_Main.DataSource = dtAddCheckListItems;
                gvw_AddCheckList2_Main.DataBind();

            }
            else
            {
                //btnSave2.Enabled = true;
                btnAdd2.Enabled = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal2", "$('#myModal2').modal('show');", true);
            //updMyModal2.Update();
        }
        else if (e.CommandName.IndexOf("approved") > -1)
        {
            int VisibleIndex = int.Parse(hidRowIndex.Value);

            //ASPxGridView gvwItemCheckList = new ASPxGridView();
            //gvwItemCheckList.DataSource = dt;
            //gvwItemCheckList.DataBind();

            string SCHECKLISTID = dt.Rows[VisibleIndex]["SCHECKLISTID"] + "";
            string SVERSIONLIST = dt.Rows[VisibleIndex]["SVERSIONLIST"] + "";
            string STYPECHECKLISTID = dt.Rows[VisibleIndex]["STYPECHECKLISTID"] + "";

            var detailfile11 = (List<dtFile>)Session["dtFile"];

            var detailIssue = detailfile11.Where(o => o.dtSCHECKLISTID == SCHECKLISTID && o.dtSVERSIONLIST == SVERSIONLIST && o.dtSTYPECHECKLISTID == STYPECHECKLISTID && o.dtProcess == "1");
            var detailEdit = detailfile11.Where(o => o.dtSCHECKLISTID == SCHECKLISTID && o.dtSVERSIONLIST == SVERSIONLIST && o.dtSTYPECHECKLISTID == STYPECHECKLISTID && o.dtProcess == "2");

            if (detailIssue.Count() > 0)
            {
                StringBuilder sb = new StringBuilder();

                string format1 = @"<tr align='center' valign='top' ><td style='border-bottom: thin solid #CCCCCC;' ><br/><a href='openFile.aspx?str={0}' target='_new'><img src='{0}' alt='{1}' width='250px' height='200px' border='1'/></a><br /><br /><div style='width:100%; text-align:left'>{2}</div></td>
                                             <td style='border-bottom: thin solid #CCCCCC;'>{3} &nbsp;</td></tr> ";

                string formatPic = "<br/><a href='openFile.aspx?str={0}' target='_new'><img src='{0}' alt='{1}' width='250px' height='200px' border='1'/></a><br /><br /><div style='width:100%; text-align:left'>{2}</div>";

                foreach (var drr in detailIssue)
                {
                    var listEdit = detailEdit.Where(o => o.dtPicIDdiff == drr.dtID + "");
                    if (listEdit.Count() > 0)
                    {
                        string editdk1 = "";
                        foreach (var dd in listEdit)
                        {
                            editdk1 += string.Format(formatPic, dd.dtFilePath, dd.dtFileName, StringEnter("คำอธิบายเพิ่มเติม : " + dd.dtEvidenceName, 65)) + "<br />";
                        }


                        sb.Append(string.Format(format1, drr.dtFilePath, drr.dtFileName, StringEnter("คำอธิบายเพิ่มเติม : " + drr.dtEvidenceName, 65) + "<br /><br />", editdk1));
                    }
                    else
                    {
                        sb.Append(string.Format(format1, drr.dtFilePath, drr.dtFileName, StringEnter("คำอธิบายเพิ่มเติม : " + drr.dtEvidenceName, 65) + "<br /><br />", ""));
                    }

                }

                lblPicture.Text = sb.ToString();

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal3", "$('#myModal3').modal('show');", true);
            //updMyModal3.Update();
        }


    }

    protected string StringEnter(string inputValue, int maxValue)
    {
        if (inputValue.Length < maxValue) return inputValue;
        double o = 0.0 + inputValue.Length / (double)maxValue;
        int count = Convert.ToInt16(Math.Ceiling(o));
        int length = inputValue.Length;
        int IndexTemp = 0;
        string valuetemp = "";


        for (int i = 1; i <= count; i++)
        {
            valuetemp += inputValue.Substring(IndexTemp, (length < maxValue) ? length : maxValue) + "<br />";
            length -= maxValue;
            IndexTemp += maxValue;
        }

        return valuetemp;
    }

    protected void gvw_AddCheckList2_Main_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];
        var dtfile = (List<dtFile>)Session["dtFile"];


        if (e.CommandName == "select")
        {
            var dtFileProcess = new List<dtFile>();
            DataTable dt = (DataTable)Session["dtCheckList"];
            if (dtfile != null)
            {
                dtFileProcess = dtfile.Where(it => it.dtProcess == "1" && it.dtSCHECKLISTID == dt.Rows[int.Parse(hidRowIndex.Value)]["SCHECKLISTID"] + string.Empty).ToList();
                txtdtID.Text = dtFileProcess[rowIndex].dtID.ToString();
                Image_show.ImageUrl = dtAddCheckListItems.Rows[rowIndex]["imagename"].ToString();
                Memo_show.Text = dtAddCheckListItems.Rows[rowIndex]["memo"].ToString();

                fileUpload2.Enabled = true;
                txtMemo2.Enabled = true;
                btnAdd2.Enabled = true;

                dtFileProcess = dtfile.Where(it => it.dtProcess == "2" && it.dtSCHECKLISTID == dt.Rows[int.Parse(hidRowIndex.Value)]["SCHECKLISTID"] + string.Empty && it.dtPicIDdiff == txtdtID.Text).ToList();

                Session["NewIssue"] = true;
                if (dtFileProcess.Any() && dtFileProcess.Count > 0)
                {
                    Session["dtAddCheckListItems2"] = null;

                    dtAddCheckListItems2 = new DataTable();
                    dtAddCheckListItems2.Columns.Add("dtID", typeof(string));
                    dtAddCheckListItems2.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));

                    for (int i = 0; i < dtFileProcess.Count; i++)
                    {
                        dtAddCheckListItems2.Rows.Add(dtFileProcess[i].dtID, ("~\\" + dtFileProcess[i].dtFilePath).ToString().Replace("/", "\\"), dtFileProcess[i].dtGenFileName, dtFileProcess[i].dtEvidenceName);
                    }
                }

                if (dtAddCheckListItems2 == null)
                {
                    dtAddCheckListItems2 = new DataTable();
                    dtAddCheckListItems2.Columns.Add("dtID", typeof(string));
                    dtAddCheckListItems2.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));
                }

                Session["NewIssue"] = true;

                gvw_AddCheckList2.DataSource = dtAddCheckListItems2;
                gvw_AddCheckList2.DataBind();
            }
            //string imageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])dtAddCheckListItems.Rows[rowIndex]["imagename"]);



        }
        //updMyModal2.Update();
    }

    protected void gvw_AddCheckList2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        dtAddCheckListItems2 = (DataTable)Session["dtAddCheckListItems2"];

        if (e.CommandName == "del")
        {
            //ASPxGridView sgvwFile1 = gvw_AddCheckList;

            var deldt = (List<dtFile>)Session["dtFile"];

            int index1 = rowIndex;

            string FilePath1 = dtAddCheckListItems2.Rows[index1]["imagename"].ToString();
            //string FileID1 = sgvwFile1.GetRowValues(index1, "dtID") + "";
            string[] mylist = FilePath1.Replace("\\", "/").Split('/');

            var dtfordel = deldt.Where(o => o.dtGenFileName == mylist[mylist.Length - 1]);


            if (File.Exists(FilePath1))
            {
                File.Delete(FilePath1);
            }

            deldt.RemoveAll(o => o.dtGenFileName == mylist[mylist.Length - 1] && o.dtProcess == "1");
            Session["dtFile"] = deldt;

            dtAddCheckListItems.Rows.RemoveAt(rowIndex);

            Session["dtAddCheckListItems"] = dtAddCheckListItems;
            gvw_AddCheckList.DataSource = dtAddCheckListItems;
            gvw_AddCheckList.DataBind();
            //updMyModal.Update();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
    }

    protected void gvw_AddCheckList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];

        if (e.CommandName == "del")
        {
            //ASPxGridView sgvwFile1 = gvw_AddCheckList;

            var deldt = (List<dtFile>)Session["dtFile"];

            int index1 = rowIndex;

            string FilePath1 = dtAddCheckListItems.Rows[index1]["imagename"].ToString();
            //string FileID1 = sgvwFile1.GetRowValues(index1, "dtID") + "";
            string[] mylist = FilePath1.Replace("\\", "/").Split('/');

            var dtfordel = deldt.Where(o => o.dtGenFileName == mylist[mylist.Length - 1]);


            if (File.Exists(FilePath1))
            {
                File.Delete(FilePath1);
            }

            deldt.RemoveAll(o => o.dtGenFileName == mylist[mylist.Length - 1] && o.dtProcess == "1");
            Session["dtFile"] = deldt;

            dtAddCheckListItems.Rows.RemoveAt(rowIndex);

            Session["dtAddCheckListItems"] = dtAddCheckListItems;
            gvw_AddCheckList.DataSource = dtAddCheckListItems;
            gvw_AddCheckList.DataBind();
            //updMyModal.Update();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
    }

    protected void gvw_AddCheckList_tab2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        dtAddCheckListItems22 = (DataTable)Session["dtAddCheckListItems22"];

        if (e.CommandName == "del")
        {
            var deldt = (List<dtFile>)Session["dtFile"];

            int index1 = rowIndex;

            string FilePath1 = dtAddCheckListItems22.Rows[index1]["imagename"].ToString();
            string[] mylist = FilePath1.Replace("\\", "/").Split('/');

            var dtfordel = deldt.Where(o => o.dtGenFileName == mylist[mylist.Length - 1]);


            if (File.Exists(FilePath1))
            {
                File.Delete(FilePath1);
            }

            deldt.RemoveAll(o => o.dtGenFileName == mylist[mylist.Length - 1] && o.dtProcess == "1");
            Session["dtFile"] = deldt;

            dtAddCheckListItems22.Rows.RemoveAt(rowIndex);

            Session["dtAddCheckListItems22"] = dtAddCheckListItems22;
            gvw_AddCheckList_tab2.DataSource = dtAddCheckListItems22;
            gvw_AddCheckList_tab2.DataBind();
            //UpdatePanel2.Update();
        }
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
    }

    GridView gvw = new GridView();

    #region BindData

    protected void BindData()
    {
        #region Tab1

        //SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUPCHECK,SPROCESS

        string str = Request.QueryString["str"];
        string[] xgvwData = new string[9];
        if (!string.IsNullOrEmpty(str))
        {
            //string[] xgvwData = new string[9];
            var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
            string strQuery = Encoding.UTF8.GetString(decryptedBytes);
            xgvwData = strQuery.Split('&');

            //string[] xgvwData = ("54$TR4901409$กท.74-9077$$$1$1$030").Split('$');

            if ("" + xgvwData[7] == "060")
            {
                hideType = "2";
            }
            else
            {
                hideType = "1";
            }

            string name1 = "";
            string date = "";
            switch ("" + xgvwData[7])
            {
                case "040":
                    name1 = "ตรวจสภาพรถระหว่างทาง(SurpriseCheck)";
                    date = "วันที่ทำการ surprisecheck " + (("" + xgvwData[4] == "") ? DateTime.Now.ToString("dd/MM/yyyy") : "" + xgvwData[4]);
                    break;
                case "030":
                    name1 = "ตรวจสภาพรถหน้าคลัง";
                    date = "วันที่เดินทาง " + (("" + xgvwData[4] == "") ? DateTime.Now.ToString("dd/MM/yyyy") : "" + xgvwData[4]);
                    break;
                case "060":
                    name1 = "ตรวจผลิตภัณฑ์";
                    date = "วันที่เดินทาง " + (("" + xgvwData[4] == "") ? DateTime.Now.ToString("dd/MM/yyyy") : "" + xgvwData[4]);
                    break;
                case "011":
                    name1 = "ยืนยันรถตามสัญญา-ตรวจสภาพรถ";
                    date = "วันที่ยืนยัน " + (("" + xgvwData[4] == "") ? DateTime.Now.ToString("dd/MM/yyyy") : "" + xgvwData[4]);
                    break;
                case "999":
                    name1 = "ตรวจสอบการตรวจสภาพรถ";
                    date = "วันที่ตรวจสอบ " + DateTime.Now.ToString("dd/MM/yyyy");
                    Session["ConfirmChecktruck"] = "1";
                    break;
            }


            lblHeader.Text = name1;
            lblHeaderNumber.Text = ("" + xgvwData[2] != "") ? "" + xgvwData[2] : "-";
            lblTailerNumber.Text = ("" + xgvwData[3] != "") ? "" + xgvwData[3] : "-";

            hidMCONTRACTID.Value = xgvwData[0];
            hidSTRUCKID.Value = xgvwData[1];
            lblCheckerDate.Text = date;

            DataTable dtv = CommonFunction.Get_Data(sql, @"SELECT TCONTRACT.SCONTRACTNO,TCONTRACT.SVENDORID,TVENDOR.SABBREVIATION FROM TCONTRACT
                    LEFT JOIN TVENDOR ON TVENDOR.SVENDORID = TCONTRACT.SVENDORID WHERE TCONTRACT.SCONTRACTID = '" + xgvwData[0] + "'");

            if (dtv != null && dtv.Rows.Count > 0)
            {
                DataRow dr = dtv.Rows[0];
                hidSVENDORID.Value = dr["SVENDORID"] + string.Empty;
                hidSVENDORNAME.Value = dr["SABBREVIATION"] + string.Empty;
                lblContactNumber.Text = dr["SCONTRACTNO"] + string.Empty;
            }
            if (xgvwData.Length > 8)
            {
                hidMCONTRACTID.Value = xgvwData[8];
                hidNSURPRISECHECKID.Value = xgvwData[8];
                hidDocID.Value = xgvwData[9];
            }

            //DataTable dt = DataLoad();
            //DataView dvCheckLists = dt;
            DataTable dtCheckLists = DataLoad();

            string SCOIL = "";
            string SCGAS = "";
            DataTable dtGetData = CommonFunction.Get_Data(connection, @"SELECT  TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL 
                                    FROm TTRUCKCONFIRMLIST 
                                    LEFT JOIN TTRUCKCONFIRM ON TTRUCKCONFIRMLIST.NCONFIRMID=TTRUCKCONFIRM.NCONFIRMID
                                    LEFT JOIN TCONTRACT ON TTRUCKCONFIRM.SCONTRACTID=TCONTRACT.SCONTRACTID
                                    where STRUCKID='" + CommonFunction.ReplaceInjection(xgvwData[1] + "") + @"'
                                    GROUP BY TTRUCKCONFIRM.SCONTRACTID ,TCONTRACT.CGAS,TCONTRACT.COIL
                                    ORDER BY MAX(TTRUCKCONFIRM.DDATE) DESC");
            if (dtGetData.Rows.Count > 0)
            {
                //xgvwData[0] = dtGetData.Rows[0]["SCONTRACTID"] + "";
                SCOIL = dtGetData.Rows[0]["COIL"] + "";//OIL
                SCGAS = dtGetData.Rows[0]["CGAS"] + "";//GAS
            }

            string scondition = ("" + SCOIL == "1" ? " AND COIL='1'" : "") + ("" + SCGAS == "1" ? " AND CGAS='1'" : "");

            DataTable dtGroupCheckLists = CommonFunction.GroupDatable("grpdtCheckList", dtCheckLists, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE", "GOCL_CACTIVE='1'" + scondition, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE");
            // AND SGROUPID = '1'
            //gvwGroupCheckList.DataSource = dtGroupCheckLists;
            //gvwGroupCheckList.DataBind();

            var dtfile = (List<dtFile>)Session["dtFile"];
            if (dtfile == null || dtfile.Count == 0)
            {
                string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + xgvwData[1] + @"'";

                string SCHECKID = "";
                DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
                if (dtCheckData.Rows.Count > 0)
                {
                    SCHECKID = dtCheckData.Rows[0]["SCHECKID"] + "";

                    //var dtfile = (List<dtFile>)Session["dtFile"];
                    foreach (DataRow drData in dtCheckData.Rows)
                    {
                        dtfile.Add(new dtFile
                        {
                            dtID = Convert.ToInt32(drData["NNO"]),
                            dtSCHECKLISTID = drData["SCHECKLISTID"] + "",
                            dtSVERSIONLIST = drData["SVERSIONLIST"] + "",
                            dtSTYPECHECKLISTID = drData["STYPECHECKLISTID"] + "",
                            dtEvidenceName = drData["SEVIDENCENAME"] + "",
                            dtFileName = drData["SFILENAME"] + "",
                            dtFilePath = drData["SFILEPATH"] + "",
                            dtGenFileName = drData["SGENFILENAME"] + "",
                            dtProcess = drData["SPROCESS"] + "",
                            dtPicIDdiff = drData["SMAINPICTURE"] + "",
                            dtNewFile = "0"

                        });
                    }

                    Session["dtFile"] = dtfile;
                }

                Session["IssueData"] = null;
                ////SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUPCHECK,SPROCESSS,CHECKID
                Session["IssueData"] = "" + xgvwData[0] + "^" + xgvwData[1] + "^" + xgvwData[2] + "^" + xgvwData[3] + "^" + xgvwData[4] + "^" + xgvwData[5] + "^" + xgvwData[6] + "^" + xgvwData[7] + "^" + SCHECKID;
            }
            for (int i = 0; i < dtfile.Count; i++)
            {
                if (dtfile[i].dtNewFile == "1")
                {
                    btnSave.Enabled = true;
                    break;
                }
            }

            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            int groupID = 0;

            ViewState["dtGroupCheckLists"] = dtGroupCheckLists;

            for (int i = 0; i < dtGroupCheckLists.Rows.Count; i++)
            {
                row = new HtmlTableRow();
                cell = new HtmlTableCell();

                //cell.ColSpan = 1;
                cell.InnerText = dtGroupCheckLists.Rows[i]["SGROUPNAME"].ToString();
                groupID = Convert.ToInt32(dtGroupCheckLists.Rows[i]["SGROUPID"]);
                row.Cells.Add(cell);
                MainTable.Rows.Add(row);

                gvw = new GridView();
                gvw.ID = "gvw_" + groupID;
                gvw.AutoGenerateColumns = false;
                gvw.CssClass = "table table-primary";
                gvw.ShowFooter = false;
                gvw.CellPadding = 4;
                gvw.ForeColor = System.Drawing.Color.Black;
                gvw.GridLines = GridLines.Both;
                gvw.BackColor = System.Drawing.Color.White;
                gvw.BorderColor = System.Drawing.ColorTranslator.FromHtml("#DEDFDE");
                gvw.BorderStyle = BorderStyle.None;
                gvw.BorderWidth = 1;
                gvw.RowCommand += new GridViewCommandEventHandler(gvw_RowCommand);
                gvw.RowDataBound += new GridViewRowEventHandler(gvw_RowDataBound);
                gvw.RowCreated += new GridViewRowEventHandler(gvw_RowCreated);
                System.Drawing.Color header_color = System.Drawing.ColorTranslator.FromHtml("#c6defb");
                gvw.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

                BoundField field = new BoundField();
                field.HeaderText = "ที่";
                field.HeaderStyle.BackColor = header_color;
                field.DataField = "ID";
                field.HeaderStyle.Width = 100;
                field.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                gvw.Columns.Add(field);

                TemplateField tmpf = new TemplateField();
                tmpf.HeaderText = "รายการตรวจสอบ";
                tmpf.HeaderStyle.Width = 400;
                tmpf.HeaderStyle.BackColor = header_color;
                tmpf.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                gvw.Columns.Add(tmpf);

                ButtonField btnfield = new ButtonField();
                btnfield.HeaderText = "แจ้งตรวจพบปัญหา";
                btnfield.Text = "แจ้งตรวจพบปัญหา";
                btnfield.ButtonType = ButtonType.Button;
                btnfield.CommandName = "checker_" + groupID;
                btnfield.HeaderStyle.BackColor = header_color;
                btnfield.HeaderStyle.Width = 200;
                btnfield.ControlStyle.Width = 150;
                btnfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                btnfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ControlStyle.CssClass = "btn btn-dark";
                gvw.Columns.Add(btnfield);

                btnfield = new ButtonField();
                btnfield.HeaderText = "แจ้งผลการแก้ไข";
                btnfield.Text = "แจ้งผลการแก้ไข";
                btnfield.ButtonType = ButtonType.Button;
                btnfield.CommandName = "revised_" + groupID;
                btnfield.HeaderStyle.BackColor = header_color;
                btnfield.HeaderStyle.Width = 200;
                btnfield.ControlStyle.Width = 150;
                btnfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                btnfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ControlStyle.CssClass = "btn btn-dark";
                gvw.Columns.Add(btnfield);

                btnfield = new ButtonField();
                btnfield.HeaderText = "ตรวจสอบผลการแก้ไข";
                btnfield.Text = "ตรวจสอบผลการแก้ไข";
                btnfield.ButtonType = ButtonType.Button;
                btnfield.CommandName = "approved_" + groupID;
                btnfield.HeaderStyle.BackColor = header_color;
                btnfield.HeaderStyle.Width = 200;
                btnfield.ControlStyle.Width = 150;
                btnfield.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ItemStyle.VerticalAlign = VerticalAlign.Middle;
                btnfield.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                btnfield.ControlStyle.CssClass = "btn btn-dark";
                gvw.Columns.Add(btnfield);

                field = new BoundField();
                field.DataField = "SCHECKLISTID";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "NLIST";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "SVERSIONLIST";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "SVERSION";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "STYPECHECKLISTID";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "STOPICID";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "SCHECKLISTNAME";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "NDAY_MA";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "NPOINT";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "CCUT";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "CBAN";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.DataField = "CL_CACTIVE";
                field.Visible = false;
                gvw.Columns.Add(field);

                field = new BoundField();
                field.HeaderText = "";
                field.Visible = false;
                field.DataField = "SCHECKLISTID";
                gvw.Columns.Add(field);

                Session["dtCheckLists"] = dtCheckLists;

                DataTable dt = dtCheckLists.Select("SGROUPID='" + groupID.ToString() + "'").CopyToDataTable<DataRow>();
                dt.DefaultView.Sort = "SCHECKLISTID";

                DataTable dtmain = new DataTable();
                dtmain.Columns.Add("ID", typeof(Int32));
                dtmain.Columns.Add("SCHECKLISTNAME", typeof(string));
                dtmain.Columns.Add("SCHECKLISTID", typeof(string));
                dtmain.Columns.Add("NLIST", typeof(string));
                dtmain.Columns.Add("STYPECHECKLISTID", typeof(string));
                dtmain.Columns.Add("SVERSIONLIST", typeof(string));
                //STYPECHECKLISTID,SVERSIONLIST
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    dtmain.Rows.Add(k + 1, dt.Rows[k]["SCHECKLISTNAME"].ToString(), dt.Rows[k]["SCHECKLISTID"].ToString(), dt.Rows[k]["NLIST"].ToString(), dt.Rows[k]["STYPECHECKLISTID"].ToString(), dt.Rows[k]["SVERSIONLIST"].ToString());
                }



                gvw.DataSource = dtmain;
                gvw.DataBind();

                ViewState["gvw_" + groupID] = dt;

                row = new HtmlTableRow();
                cell = new HtmlTableCell();

                cell.Controls.Add(gvw);

                row.Cells.Add(cell);
                MainTable.Rows.Add(row);



            }

            string qqry = @"select *  FROM
                (SELECT tt.SCONTRACTID,ROW_NUMBER () OVER (ORDER BY  t.SHEADREGISTERNO) AS ID1, tt.STRUCKID, t.SHEADREGISTERNO,t.STRAILERREGISTERNO,
                CASE WHEN tt.CSTANDBY = 'Y' THEN 'รถสำรอง' ELSE 'รถในสัญญา' END AS CARTYPE,o.CHECKCONFIRM,o.DCHECK ,
                (SELECT SSURPRISECHECKBY FROM TSURPRISECHECKHISTORY WHERE NSURPRISECHECKID = o.CHECKID) AS SSURPRISECHECKBY
                 FROM (TCONTRACT_TRUCK tt INNER JOIN TTRUCK t ON TT.STRUCKID = T.STRUCKID) LEFT JOIN
                 (SELECT MAX(NSURPRISECHECKID) AS CHECKID, SCONTRACTID,STRUCKID,COUNT(*) AS CHECKCONFIRM,MAX(DCHECK) AS DCHECK FROM TSURPRISECHECKHISTORY
                  WHERE nvl(CNOCHECK,'0') = '0' AND COK = '1' GROUP BY SCONTRACTID,STRUCKID) o ON o.SCONTRACTID = TT.SCONTRACTID AND o.STRUCKID = TT.STRUCKID 
                ) WHERE STRUCKID='" + xgvwData[1] + @"'";

            DataTable dtMyData = CommonFunction.Get_Data(connection, qqry);

            DataTable dtt = new DataTable();
            dtt.Columns.Add("dcheck", typeof(string));
            dtt.Columns.Add("ssurprisecheckby", typeof(string));
            dtt.Columns.Add("cartype", typeof(string));
            dtt.Columns.Add("checker", typeof(string));
            for (int i = 0; i < dtMyData.Rows.Count; i++)
            {
                string myStatus = "ยังไม่แก้ไข";
                if (dtMyData.Rows[i]["dcheck"].ToString() == "0")
                {
                    myStatus = "แก้ไขแล้ว";
                }
                dtt.Rows.Add(dtMyData.Rows[i]["dcheck"].ToString(), dtMyData.Rows[i]["ssurprisecheckby"].ToString(), dtMyData.Rows[i]["cartype"].ToString(), myStatus);
            }
            if (dtt.Rows.Count > 0)
            {
                gvwHistory.DataSource = dtt;
                gvwHistory.DataBind();
            }

        #endregion

            #region Tab2
            var plaintextBytes = Encoding.UTF8.GetBytes(hidNSURPRISECHECKID.Value + "_" + hidDocID.Value);
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            //hlTab2.NavigateUrl = "~/checktruck_Complain.aspx?id=" + encryptedValue;
            #endregion
            #region Tab3
            #region Other Remark
            DataTable dtOtherRemark = CommonFunction.Get_Data(connection, @"SELECT ichktrck.* 
     FROm TCHECKTRUCK chktrck
     LEFT JOIN TCHECKTRUCKITEM ichktrck ON chktrck.SCHECKID=ichktrck.SCHECKID
    WHERE 1=1 AND ichktrck.COTHER='1' AND SCONTRACTID='" + xgvwData[0] + "' AND STRUCKID='" + xgvwData[1] + "' ");
            if (dtOtherRemark.Rows.Count > 0)
            {
                string sOtherRemark = "", sRemark = "";

                foreach (DataRow drOtherRemark in dtOtherRemark.Rows)
                {
                    sOtherRemark += "," + drOtherRemark["sOtherRemark"];
                    sRemark += "," + drOtherRemark["sRemark"];
                    break;

                }//(Eval("CBAN")+""=="1")?"2":((Eval("NDAY_MA")+""!="")?"1":"0")

                cbxOtherIssue.Checked = (true) ? true : false;
                if (cbxOtherIssue.Checked)
                {
                    txtOtherIssue.Text = sOtherRemark.Length > 0 ? sOtherRemark.Remove(0, 1) : "";
                    txtDetailIssue.Text = sRemark.Length > 0 ? sRemark.Remove(0, 1) : "";
                    txtOtherIssue.Enabled = true;
                    txtDetailIssue.Enabled = true;

                }

                var FileOther = (List<dtFileOther>)Session["dtFileOther"];
                DataTable dtOtherFile = CommonFunction.Get_Data(connection, @"SELECT NATTACHMENT, SATTACHTYPEID, SPATH, 
SFILE, SSYSFILE,  SEVIDENCENAME FROM TCHECKTRUCKATTACHMENT WHERE SCONTRACTID = '" + xgvwData[0] + "' AND STRUCKID='" + xgvwData[1] + "' ");
                if (dtOtherRemark.Rows.Count > 0)
                {
                    foreach (DataRow drOther in dtOtherFile.Rows)
                    {
                        FileOther.Add(new dtFileOther
                        {
                            dtID = Convert.ToInt32(drOther["NATTACHMENT"]),
                            dtEvidenceName = drOther["SEVIDENCENAME"] + "",
                            dtFileName = drOther["SFILE"] + "",
                            dtFilePath = drOther["SPATH"] + "",
                            dtGenFileName = drOther["SSYSFILE"] + ""
                        });
                        if (dtAddCheckListItems22 == null)
                        {
                            dtAddCheckListItems22 = new DataTable();
                            dtAddCheckListItems22.Columns.Add("imagename", typeof(string));
                            dtAddCheckListItems22.Columns.Add("filename", typeof(string));
                            dtAddCheckListItems22.Columns.Add("memo", typeof(string));
                            //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));
                        }
                        dtAddCheckListItems22.Rows.Add(("~\\" + drOther["SPATH"] + "").ToString().Replace("/", "\\"), drOther["SSYSFILE"] + "", drOther["SEVIDENCENAME"] + "");
                    }

                    gvw_AddCheckList_tab2.DataSource = dtAddCheckListItems22;
                    gvw_AddCheckList_tab2.DataBind();

                    Session["dtFileOther"] = FileOther;
                }
            }

            dtOtherRemark.Dispose();

            #endregion
            #endregion
            dtCheckLists.Dispose();
            dtGroupCheckLists.Dispose();
        }
    }
    #endregion

    #region Tab 1 แจ้งตรวจพบปัญหา
    protected void btnAdd_Onclick(object sender, EventArgs e)
    {
        try
        {
            //updMyModal.Update();
            string[] _Filename = fileUpload.PostedFile.FileName.Split('.'); //e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                //ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;
                string data = "";
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(fileUpload, genName, string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "")))
                {
                    data = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "") + genName + "." + _Filename[count];
                    //e.CallbackData = data;

                    Session["FileUpload"] = fileUpload.PostedFile.FileName + ";" + genName + "." + _Filename[count] + ";" + data;

                }
                DataTable dt = (DataTable)Session["dtCheckList"];
                int VisibleIndex = int.Parse(hidRowIndex.Value);
                int GroupID = Convert.ToInt32(Session["GroupID"]);

                //dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];
                //var dtfile = (List<dtFile>)Session["dtFile"];
                //var ml = dtfile.Where(o => o.dtSCHECKLISTID == dt.Rows[VisibleIndex]["SCHECKLISTID"].ToString()).ToList();
                Session["NewIssue"] = true;
                dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];
                //if (ml != null && ml.Count > 0)
                //{
                //    //Session["dtAddCheckListItems"] = null;

                //    dtAddCheckListItems = new DataTable();
                //    dtAddCheckListItems.Columns.Add("imagename", typeof(string));
                //    dtAddCheckListItems.Columns.Add("filename", typeof(string));
                //    dtAddCheckListItems.Columns.Add("memo", typeof(string));
                //    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));

                //    for (int i = 0; i < ml.Count; i++)
                //    {
                //        dtAddCheckListItems.Rows.Add(("~\\" + ml[i].dtFilePath).ToString().Replace("/", "\\"), ml[i].dtGenFileName, ml[i].dtEvidenceName);
                //    }
                //}

                if (dtAddCheckListItems == null)
                {
                    dtAddCheckListItems = new DataTable();
                    dtAddCheckListItems.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));
                }

                dtAddCheckListItems.Rows.Add(("~\\" + data).ToString().Replace("/", "\\"), genName, txtMemo.Text);
                Session["dtAddCheckListItems"] = dtAddCheckListItems;

                Session["NewIssue"] = true;

                gvw_AddCheckList.DataSource = dtAddCheckListItems;
                gvw_AddCheckList.DataBind();

                //Session["dtCheckList"] = dt;
                //Session["DrCheckList"] = dr;
                //hidRowIndex.Value = rowIndex;


                ASPxGridView gvwItemCheckList = new ASPxGridView();
                gvwItemCheckList.DataSource = dt;
                gvwItemCheckList.DataBind();

                //int VisibleIndex = int.Parse(hidRowIndex.Value);

                string SCHECKLISTID = gvwItemCheckList.GetRowValues(VisibleIndex, "SCHECKLISTID") + "";
                string SVERSIONLIST = gvwItemCheckList.GetRowValues(VisibleIndex, "SVERSIONLIST") + "";
                string STYPECHECKLISTID = gvwItemCheckList.GetRowValues(VisibleIndex, "STYPECHECKLISTID") + "";
                //TextBox txtDetail = this.FindControl("txtMemo") as TextBox;
                GridView sgvwFile = this.FindControl("gvw_AddCheckList") as GridView;

                var addData = (List<dtFileTemp>)Session["dtFileTemp"];
                int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                var fileupload = (Session["FileUpload"] + "").Split(';');

                if (fileupload.Count() == 3)
                {
                    addData.Add(new dtFileTemp
                    {
                        dtID = max,
                        dtEvidenceName = txtMemo.Text,
                        dtFileName = fileupload[0],
                        dtGenFileName = fileupload[1],
                        dtFilePath = fileupload[2],
                        dtSCHECKLISTID = SCHECKLISTID,
                        dtSTYPECHECKLISTID = STYPECHECKLISTID,
                        dtSVERSIONLIST = SVERSIONLIST,
                        dtProcess = "1",
                        dtNewFile = "1"
                    });

                    Session["dtFileTemp"] = addData;
                    //gvw_AddCheckList.DataSource = addData;
                    //gvw_AddCheckList.DataBind();
                    txtMemo.Text = "";
                }



                string SCHECKLISTID2 = gvwItemCheckList.GetRowValues(VisibleIndex, "SCHECKLISTID") + "";
                string SVERSIONLIST2 = gvwItemCheckList.GetRowValues(VisibleIndex, "SVERSIONLIST") + "";
                string STYPECHECKLISTID2 = gvwItemCheckList.GetRowValues(VisibleIndex, "STYPECHECKLISTID") + "";

                if (Session["dtFile"] != null && Session["dtFileTemp"] != null)
                {
                    var vdetailfile = (List<dtFile>)Session["dtFile"];
                    var vdetailfileTemp = (List<dtFileTemp>)Session["dtFileTemp"];

                    vdetailfile.RemoveAll(o => o.dtSCHECKLISTID == SCHECKLISTID2 && o.dtSVERSIONLIST == SVERSIONLIST2 && o.dtSTYPECHECKLISTID == STYPECHECKLISTID2 && o.dtProcess == "1");
                    foreach (var filetemp in vdetailfileTemp)
                    {
                        vdetailfile.Add(new dtFile
                        {
                            dtID = filetemp.dtID,
                            dtEvidenceName = filetemp.dtEvidenceName,
                            dtFileName = filetemp.dtFileName,
                            dtGenFileName = filetemp.dtGenFileName,
                            dtFilePath = filetemp.dtFilePath,
                            dtSCHECKLISTID = filetemp.dtSCHECKLISTID,
                            dtSTYPECHECKLISTID = filetemp.dtSTYPECHECKLISTID,
                            dtSVERSIONLIST = filetemp.dtSVERSIONLIST,
                            dtProcess = "1",
                            dtNewFile = filetemp.dtNewFile
                        });

                    }

                    Session["dtFile"] = vdetailfile;

                    //vdetailfileTemp.RemoveAll(o => 1 == 1);

                    Session["dtFileTemp"] = vdetailfileTemp;

                    //gvwItemCheckList.CancelEdit();
                }

            }
            else
            {
                Session["NewIssue"] = false;
                return;

            }
        }
        catch (Exception ex)
        {
            //e.IsValid = false;
            //e.ErrorText = ex.Message;
        }
        txtMemo.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
        //updMyModal.Update();
    }
    #endregion

    #region Tab 1 แจ้งผลการแก้ไข

    protected void btnAdd2_Onclick(object sender, EventArgs e)
    {
        try
        {
            //updMyModal.Update();
            string[] _Filename = fileUpload2.PostedFile.FileName.Split('.'); //e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                //ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;
                string data = "";
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(fileUpload2, genName, string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "")))
                {
                    data = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "") + genName + "." + _Filename[count];
                    //e.CallbackData = data;

                }
                DataTable dt = (DataTable)Session["dtCheckList"];
                int VisibleIndex = int.Parse(hidRowIndex.Value);

                //dtAddCheckListItems = (DataTable)Session["dtAddCheckListItems"];
                var dtfile = (List<dtFile>)Session["dtFile"];
                var ml = dtfile.Where(o => o.dtSCHECKLISTID == dt.Rows[VisibleIndex]["SCHECKLISTID"].ToString() && o.dtPicIDdiff == txtdtID.Text && o.dtProcess == "2").ToList();
                Session["NewIssue"] = true;
                if (ml != null && ml.Count > 0)
                {
                    Session["dtAddCheckListItems2"] = null;

                    dtAddCheckListItems2 = new DataTable();
                    dtAddCheckListItems2.Columns.Add("dtID", typeof(string));
                    dtAddCheckListItems2.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));

                    for (int i = 0; i < ml.Count; i++)
                    {
                        dtAddCheckListItems2.Rows.Add(ml[i].dtID, ("~\\" + ml[i].dtFilePath).ToString().Replace("/", "\\"), ml[i].dtGenFileName, ml[i].dtEvidenceName);
                    }
                }

                if (dtAddCheckListItems2 == null)
                {
                    dtAddCheckListItems2 = new DataTable();
                    dtAddCheckListItems2.Columns.Add("dtID", typeof(string));
                    dtAddCheckListItems2.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems2.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));
                }

                dtAddCheckListItems2.Rows.Add(txtdtID.Text, ("~\\" + data).ToString().Replace("/", "\\"), genName, txtMemo2.Text);
                dtfile.Add(new dtFile
                {
                    //dtID = filetemp.dtID,
                    dtEvidenceName = txtMemo.Text.Trim(),
                    dtFileName = fileUpload2.PostedFile.FileName,
                    dtGenFileName = genName,
                    dtFilePath = data,
                    dtSCHECKLISTID = dt.Rows[VisibleIndex]["SCHECKLISTID"].ToString(),
                    dtSTYPECHECKLISTID = dt.Rows[VisibleIndex]["STYPECHECKLISTID"] + "",
                    dtSVERSIONLIST = dt.Rows[VisibleIndex]["SVERSIONLIST"] + "",
                    dtProcess = "2",
                    dtNewFile = "1",
                    dtPicIDdiff = txtdtID.Text,
                });
                //Session["dtAddCheckListItems2"] = dtAddCheckListItems2;

                Session["NewIssue"] = true;

                gvw_AddCheckList2.DataSource = dtAddCheckListItems2;
                gvw_AddCheckList2.DataBind();

            }
            else
            {
                Session["NewIssue"] = false;
                return;

            }
        }
        catch (Exception ex)
        {
            //e.IsValid = false;
            //e.ErrorText = ex.Message;
        }
        txtMemo2.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal2", "$('#myModal2').modal('show');", true);
        //updMyModal2.Update();
    }
    #endregion

    #region Tab 1 ตรวจสอบผลการแก้ไข

    #endregion

    #region Tab 2 หลักฐานการกระทำผิดและปัญหาอื่นๆ
    protected void btnAdd22_Onclick(object sender, EventArgs e)
    {
        try
        {
            //updMyModal.Update();
            string[] _Filename = fileUpload1.PostedFile.FileName.Split('.'); //e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                //ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;
                string data = "";
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(fileUpload1, genName, string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "")))
                {
                    data = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "") + genName + "." + _Filename[count];
                    //e.CallbackData = data;

                    Session["FileUpload1"] = fileUpload1.PostedFile.FileName + ";" + genName + "." + _Filename[count] + ";" + data;

                }
                //DataTable dt = (DataTable)Session["dtCheckList"];
                //int VisibleIndex = int.Parse(hidRowIndex.Value);

                dtAddCheckListItems22 = (DataTable)Session["dtAddCheckListItems22"];
                var dtfile = (List<dtFileOther>)Session["dtFileOther"];
                var ml = dtfile.ToList();
                Session["NewIssue"] = true;
                if (ml != null && ml.Count > 0)
                {
                    Session["dtAddCheckListItems22"] = null;

                    dtAddCheckListItems22 = new DataTable();
                    dtAddCheckListItems22.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems22.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems22.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));

                    for (int i = 0; i < ml.Count; i++)
                    {
                        dtAddCheckListItems22.Rows.Add(("~\\" + ml[i].dtFilePath).ToString().Replace("/", "\\"), ml[i].dtGenFileName, ml[i].dtEvidenceName);
                    }
                }

                if (dtAddCheckListItems22 == null)
                {
                    dtAddCheckListItems22 = new DataTable();
                    dtAddCheckListItems22.Columns.Add("imagename", typeof(string));
                    dtAddCheckListItems22.Columns.Add("filename", typeof(string));
                    dtAddCheckListItems22.Columns.Add("memo", typeof(string));
                    //dtAddCheckListItems.Columns.Add("IssueDescript", typeof(string));
                }

                dtAddCheckListItems22.Rows.Add(("~\\" + data).ToString().Replace("/", "\\"), genName + "." + _Filename[count], TextBox1.Text);
                Session["dtAddCheckListItems22"] = dtAddCheckListItems22;

                Session["NewIssue"] = true;

                gvw_AddCheckList_tab2.DataSource = dtAddCheckListItems22;
                gvw_AddCheckList_tab2.DataBind();

                //Session["dtCheckList"] = dt;
                //Session["DrCheckList"] = dr;
                //hidRowIndex.Value = rowIndex;


                //ASPxGridView gvwItemCheckList = new ASPxGridView();
                //gvwItemCheckList.DataSource = dt;
                //gvwItemCheckList.DataBind();

                ////int VisibleIndex = int.Parse(hidRowIndex.Value);

                //string SCHECKLISTID = gvwItemCheckList.GetRowValues(VisibleIndex, "SCHECKLISTID") + "";
                //string SVERSIONLIST = gvwItemCheckList.GetRowValues(VisibleIndex, "SVERSIONLIST") + "";
                //string STYPECHECKLISTID = gvwItemCheckList.GetRowValues(VisibleIndex, "STYPECHECKLISTID") + "";
                ////TextBox txtDetail = this.FindControl("txtMemo") as TextBox;
                //GridView sgvwFile = this.FindControl("gvw_AddCheckList") as GridView;

                var addData = (List<dtFileOther>)Session["dtFileOther"];
                int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                var fileupload1 = (Session["FileUpload1"] + "").Split(';');

                if (fileupload1.Count() == 3)
                {
                    addData.Add(new dtFileOther
                    {
                        dtID = max,
                        dtEvidenceName = TextBox1.Text,
                        dtFileName = fileupload1[0],
                        dtGenFileName = fileupload1[1],
                        dtFilePath = fileupload1[2],

                    });

                    Session["dtFileOther"] = addData;
                    //gvw_AddCheckList.DataSource = addData;
                    //gvw_AddCheckList.DataBind();
                    TextBox1.Text = "";
                }



                //string SCHECKLISTID2 = gvwItemCheckList.GetRowValues(VisibleIndex, "SCHECKLISTID") + "";
                //string SVERSIONLIST2 = gvwItemCheckList.GetRowValues(VisibleIndex, "SVERSIONLIST") + "";
                //string STYPECHECKLISTID2 = gvwItemCheckList.GetRowValues(VisibleIndex, "STYPECHECKLISTID") + "";

                //if (Session["dtFile"] != null && Session["dtFileTemp"] != null)
                //{
                //    var vdetailfile = (List<dtFile>)Session["dtFile"];
                //    var vdetailfileTemp = (List<dtFileTemp>)Session["dtFileTemp"];

                //    vdetailfile.RemoveAll(o => o.dtSCHECKLISTID == SCHECKLISTID2 && o.dtSVERSIONLIST == SVERSIONLIST2 && o.dtSTYPECHECKLISTID == STYPECHECKLISTID2 && o.dtProcess == "1");
                //    foreach (var filetemp in vdetailfileTemp)
                //    {
                //        vdetailfile.Add(new dtFile
                //        {
                //            dtID = filetemp.dtID,
                //            dtEvidenceName = filetemp.dtEvidenceName,
                //            dtFileName = filetemp.dtFileName,
                //            dtGenFileName = filetemp.dtGenFileName,
                //            dtFilePath = filetemp.dtFilePath,
                //            dtSCHECKLISTID = filetemp.dtSCHECKLISTID,
                //            dtSTYPECHECKLISTID = filetemp.dtSTYPECHECKLISTID,
                //            dtSVERSIONLIST = filetemp.dtSVERSIONLIST,
                //            dtProcess = "1",
                //            dtNewFile = filetemp.dtNewFile
                //        });

                //    }

                //    Session["dtFile"] = vdetailfile;

                //    vdetailfileTemp.RemoveAll(o => 1 == 1);

                //    Session["dtFileTemp"] = vdetailfileTemp;

                //    //gvwItemCheckList.CancelEdit();
                //}

            }
            else
            {
                Session["NewIssue"] = false;
                return;

            }
        }
        catch (Exception ex)
        {
            //e.IsValid = false;
            //e.ErrorText = ex.Message;
        }
        txtMemo.Text = "";
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
        //UpdatePanel2.Update();
    }

    #endregion


    protected void gvw_AddCheckList_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            string imageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])dr["imagename"]);
            (e.Row.FindControl("Image1") as Image).ImageUrl = imageUrl;
        }
    }

    protected void gvw_AddCheckList2_Main_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //DataRowView dr = (DataRowView)e.Row.DataItem;
            //string imageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])dr["imagename"]);
            //(e.Row.FindControl("Image1") as Image).ImageUrl = imageUrl;
        }
    }

    //protected void UploadFile_OnClick(object sender, EventArgs e)
    //{

    //    try
    //    {
    //        string[] _Filename = fileUpload.PostedFile.FileName.Split('.'); //e.UploadedFile.FileName.Split('.');

    //        if (_Filename[0] != "")
    //        {
    //            //ASPxUploadControl upl = (ASPxUploadControl)sender;
    //            int count = _Filename.Count() - 1;

    //            string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
    //            if (UploadFile2Server(fileUpload, genName, string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "")))
    //            {
    //                string data = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "") + genName + "." + _Filename[count];
    //                //e.CallbackData = data;

    //                Session["FileUpload"] = fileUpload.PostedFile + ";" + genName + "." + _Filename[count] + ";" + data;

    //            }
    //        }
    //        else
    //        {

    //            return;

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        //e.IsValid = false;
    //        //e.ErrorText = ex.Message;
    //    }
    //}

    private bool UploadFile2Server(FileUpload ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.PostedFile.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.PostedFile.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.PostedFile.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {   //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO 
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        //repoint.SDELIVERYNO = "" ;
        repoint.Insert();
        return IsReduce;
    }

    protected DataTable PrepareDataTable(string ss_name, string sdate, string contractid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] + "" != "")
        {
            dtData = (DataTable)Session[ss_name];
            foreach (DataRow drDel in dtData.Select("DDATE='" + sdate + "' AND SCONTRACTID='" + contractid + "'"))
            {
                int idx = dtData.Rows.IndexOf(drDel);
                dtData.Rows[idx].Delete();
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }

    #region SendEmail
    private bool SendEmail(int TemplateID)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                string EmailList = string.Empty, Link = string.Empty, str = string.Empty, status = string.Empty;
                byte[] plaintextBytes;
                //if (TemplateID == ConfigValue.SurpriseCheck1)
                //{
                //    str = hidMCONTRACTID.Value + "&" + hidSTRUCKID.Value + "&" + txtT1.Text + "&" + txtT2.Text + "&" + DateTime.Now + "&&5&999 ";
                //}
                //else if (TemplateID == ConfigValue.SurpriseCheck2)
                //{
                //    str = hidMCONTRACTID.Value + "&" + hidSTRUCKID.Value + "&" + txtT1.Text + "&" + txtT2.Text + "&" + DateTime.Now + "&&3&040&" + hidNSURPRISECHECKID.Value+ "&" + hidDocID.Value;
                //}
                //else 
                if (TemplateID == ConfigValue.SurpriseCheck3)
                {
                    status = ViewState["Status3"] + string.Empty;
                    if (!string.IsNullOrEmpty(status))
                    {
                        status = status.Remove(0, 1);
                        Title = Session["TitleName"] + string.Empty;
                        string[] statuss = status.Split(',');
                        if (!string.IsNullOrEmpty(Title))
                        {
                            string[] titles = Title.Split(',');
                            status = string.Empty;
                            for (int i = 0; i < titles.Count(); i++)
                            {
                                Title = titles[i];
                                status += Title + " สถานะ : " + statuss[i] + "<br/>";
                            }
                        }
                    }
                }
                plaintextBytes = Encoding.UTF8.GetBytes(str);
                str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "checktruck.aspx?str=" + str;
                #region SurpriseCheck
                EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, false, false);
                //Body = Body.Replace("{Email}", "");
                if (!string.IsNullOrEmpty(EmailList))
                {
                    EmailList += ",";
                }
                //EmailList += "terapat.p@pttor.com, wasupol.p@pttor.com";
                //EmailList += "TMS_terapat.p@pttor.com, TMS_wasupol.p@pttor.com";
                Body = Body.Replace("{Title}", Session["TitleName"] + string.Empty);
                Body = Body.Replace("{SCONTRACTID}", lblContactNumber.Text);
                Body = Body.Replace("{SHEADREGISTERNO}", lblHeaderNumber.Text + (!string.IsNullOrEmpty(lblTailerNumber.Text) && lblTailerNumber.Text != "-" ? " - " + lblTailerNumber.Text : ""));
                Body = Body.Replace("{Vendor}", hidSVENDORNAME.Value);
                Body = Body.Replace("{Status}", status);
                Body = Body.Replace("{VendorAccountName}", Session["vendoraccountname"] + string.Empty);


                //byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                //string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                //plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                Body = Body.Replace("{LINK}", "");
                if (TemplateID == ConfigValue.SurpriseCheck3)
                {
                    EmailList = string.Empty;
                    //EmailList = "nut.t@pttor.com,sake.k@pttor.com,thanyavit.k@pttor.com,terapat.p@pttor.com,bodin.a@pttor.com,surapol.s@pttor.com,wasupol.p@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, false, false);
                    MailService.SendMail(EmailList, Subject, Body, "", "SurpriseCheck3", ColumnEmailName);
                }
                else
                {
                    MailService.SendMail(EmailList, Subject, Body, "", "SurpriseCheck", ColumnEmailName);
                }
                Session["TitleName"] = null;
                #endregion


                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    protected void btnSave_Onclick(object sender, EventArgs e)
    {
        string Detail = string.Empty;
        //DataRow dr = (DataRow)Session["CheckList"];

        var Data = (List<dtFile>)Session["dtFile"];
        var Issue = Data.Where(o => o.dtProcess == "1");
        TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
        TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(Page, connection);
        TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection);
        double NPOINT = 0;
        string SCHECKID = "";
        string[] TruckData = ("" + Session["IssueData"]).Split('^');
        SCHECKID = TruckData[8] + "";

        DataTable dt = (DataTable)Session["dtCheckList"];

        DataTable dtg = (DataTable)ViewState["dtGroupCheckLists"];

        List<String> AddIssue = new List<String>();
        for (int ii = 0; ii < dtg.Rows.Count; ii++)
        {
            string groupid = dtg.Rows[ii]["SGROUPID"].ToString();
            DataTable dtt = (DataTable)ViewState["gvw_" + groupid];

            ASPxGridView gvwItemCheckList_SAVE = new ASPxGridView();
            //gvwGroupCheckList.FindRowCellTemplateControl(idxGroupGrid, (GridViewDataColumn)gvwGroupCheckList.Columns[0], "gvwItemCheckList") as ASPxGridView;
            gvwItemCheckList_SAVE.DataSource = dtt;
            gvwItemCheckList_SAVE.DataBind();
            for (int idxItemGrid = 0; idxItemGrid < gvwItemCheckList_SAVE.VisibleRowCount; idxItemGrid++)
            {
                dynamic datagriditem = gvwItemCheckList_SAVE.GetRowValues(idxItemGrid, "SVERSIONLIST", "SVERSION", "STOPICID", "SCHECKLISTID", "NLIST", "NDAY_MA", "CCUT", "CBAN", "CL_CACTIVE", "NPOINT", "STYPECHECKLISTID", "SCHECKLISTNAME", "SVERSION");

                var IssueFile = Issue.Where(o => o.dtSCHECKLISTID == datagriditem[3] + "" && o.dtSTYPECHECKLISTID == datagriditem[10] + "" && o.dtSVERSIONLIST == datagriditem[0] + "");


                if (IssueFile.Count() > 0)
                {

                    if (IssueFile.Where(o => o.dtNewFile == "0").Count() == 0) //ถ้าเคยมีรายการมาก่อนไม่ให้ทำรายการเพิ่ม
                    {
                        //AddIssue.Add(datagriditem[3]);
                        //SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUP,SPROCESS,SCHECKID

                        ///1.Call StoreProcedure for insert into DB
                        #region TCHECKTRUCK
                        ///TCHECKTRUCK
                        ctrck.SCHECKID = SCHECKID != "" ? SCHECKID : "";//send blank for gen new id
                        ctrck.NPLANID = "" + TruckData[5]; /////NPLANID
                        ctrck.STERMINALID = "" + Session["SVDID"];
                        ctrck.SCONTRACTID = "" + TruckData[0];         /////SCONTRACTID
                        ctrck.CGROUPCHECK = "" + TruckData[6];//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                        ctrck.CCLEAN = "0";
                        ctrck.STRUCKID = "" + TruckData[1]; /////STRUCKID
                        ctrck.SHEADERREGISTERNO = "" + TruckData[2]; /////SHEADERREGISTERNO
                        ctrck.STRAILERREGISTERNO = "" + TruckData[3]; /////STRAILERREGISTERNO
                        ctrck.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                        ctrck.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                        ctrck.SCREATE = "" + Session["UserID"];
                        ctrck.SUPDATE = "" + Session["UserID"];

                        ctrck.Insert();
                        SCHECKID = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.
                        #endregion
                        #region///TCHECKTRUCKITEM
                        ctrckItem.SCHECKID = SCHECKID;
                        ctrckItem.SCHECKLISTID = "" + datagriditem[3];
                        ctrckItem.SVERSIONLIST = "" + datagriditem[0];
                        ctrckItem.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                        ctrckItem.NPOINT = ("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0";
                        ctrckItem.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                        ctrckItem.COTHER = "0";
                        ctrckItem.SOTHERREMARK = "";
                        ctrckItem.STYPECHECKLISTID = "" + datagriditem[10];
                        ctrckItem.SCREATE = "" + Session["UserID"];
                        ctrckItem.SUPDATE = "" + Session["UserID"];
                        ctrckItem.Insert();
                        #endregion
                        #region บันทึกห้ามวิ่ง+ตัดแต้ม

                        if ("" + datagriditem[7] == "1")
                        {//ถ้าเป้นเคส ห้ามวิ่ง 
                            #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                            using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                            {
                                if (con.State == ConnectionState.Closed) con.Open();
                                OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_HEADERREGISTERNO", con);
                                ora_cmd.Parameters.Add(":S_HEADERREGISTERNO", OracleType.VarChar).Value = ctrck.SHEADERREGISTERNO.Trim();
                                ora_cmd.ExecuteNonQuery();

                                OracleCommand ora_trail = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_TRAILERREGISTERNO", con);
                                ora_trail.Parameters.Add(":S_TRAILERREGISTERNO", OracleType.VarChar).Value = ctrck.STRAILERREGISTERNO.Trim();
                                ora_trail.ExecuteNonQuery();

                                AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                                onHold.VehNo = ctrck.SHEADERREGISTERNO;
                                onHold.TUNo = ctrck.STRAILERREGISTERNO;
                                onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                onHold.VehID = ctrck.STRUCKID;
                                onHold.SendTo();
                            }
                            #endregion
                        }
                        if ("" + datagriditem[6] == "1")
                        {//ถ้าตัดคะแนนทันที
                            //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                            //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                            ReducePoint("01", "" + TruckData[7], "" + datagriditem[11], "" + datagriditem[3], "" + datagriditem[2], "" + datagriditem[0], "" + (("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0")
                        , SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");
                        }
                        #endregion
                        NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;
                        Title += "," + datagriditem[11];
                        ///2.List Data for future
                    }
                }//End Checklist Item        


            }
        }


        if (!string.IsNullOrEmpty(Title))
        {
            Session["TitleName"] = Title;
        }
        #region Update คะแนนรวมที่ตัดที่ รายการหลัก
        ctrck.SCHECKID = SCHECKID;
        ctrck.UpdateMA();
        #endregion

        string[] checkData = ("" + Session["IssueData"]).Split('^');
        string CheckID = (ctrck.SCHECKID == "") ? checkData[8] + "" : ctrck.SCHECKID;
        ///FileDetail แจ้งตรวจพบปัญหา

        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            string sqlInsertFile = @"INSERT INTO TCHECKTRUCKFILE ( NID, SCHECKID, SCHECKLISTID, 
                                                             SVERSIONLIST, STYPECHECKLISTID, SPROCESS, NNO, SEVIDENCENAME, SFILENAME, 
                                                            SGENFILENAME, SFILEPATH, SCREATE, DCREATE) 
                                                            VALUES (:NID, :SCHECKID, :SCHECKLISTID, 
                                                             :SVERSIONLIST, :STYPECHECKLISTID, :SPROCESS, :NNO, :SEVIDENCENAME, :SFILENAME, 
                                                            :SGENFILENAME, :SFILEPATH, :SCREATE, SYSDATE)";

            foreach (var dataFile in Issue)
            {

                if (dataFile.dtNewFile == "1")
                {
                    string GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TCHECKTRUCKFILE ORDER BY NID DESC)  WHERE ROWNUM <= 1");

                    using (OracleCommand com = new OracleCommand(sqlInsertFile, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                        com.Parameters.Add(":SCHECKID", OracleType.Number).Value = CheckID;
                        com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = dataFile.dtSCHECKLISTID;
                        com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = dataFile.dtSVERSIONLIST;
                        com.Parameters.Add(":STYPECHECKLISTID", OracleType.Number).Value = dataFile.dtSTYPECHECKLISTID;
                        com.Parameters.Add(":SPROCESS", OracleType.VarChar).Value = "1";
                        com.Parameters.Add(":NNO", OracleType.Number).Value = dataFile.dtID;
                        com.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = dataFile.dtEvidenceName;
                        com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = dataFile.dtFileName;
                        com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = dataFile.dtGenFileName;
                        com.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = dataFile.dtFilePath;
                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                        com.ExecuteNonQuery();
                    }
                }
            }
        }

        ///FileDetail แจ้งผลการแก้ไข
        if ("" + Session["CGROUP"] == "0")
        {
            if ("" + checkData[8] != "")
            {
                var EditFile = Data.Where(o => o.dtProcess == "2");
                var EditGroup = Data.Select(i => new { i.dtSCHECKLISTID, i.dtSTYPECHECKLISTID, i.dtSVERSIONLIST }).Distinct();

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    foreach (var updateEdit in EditGroup)
                    {
                        string sqlEDITED = @"UPDATE TCHECKTRUCKITEM SET CEDITED = '1',SUPDATE = :SUPDATE WHERE SCHECKID = :SCHECKID AND SCHECKLISTID = :SCHECKLISTID AND SVERSIONLIST = :SVERSIONLIST AND STYPECHECKLISTID = :STYPECHECKLISTID";

                        using (OracleCommand com = new OracleCommand(sqlEDITED, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                            com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = updateEdit.dtSCHECKLISTID;
                            com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = updateEdit.dtSVERSIONLIST;
                            com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = updateEdit.dtSTYPECHECKLISTID;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();
                        }
                    }

                    foreach (var dataFile in EditFile)
                    {
                        if (dataFile.dtNewFile == "1")
                        {
                            string sqlInsertFile = @"INSERT INTO TCHECKTRUCKFILE ( NID, SCHECKID, SCHECKLISTID, 
                                                             SVERSIONLIST, STYPECHECKLISTID, SPROCESS, NNO, SEVIDENCENAME, SFILENAME, 
                                                           SGENFILENAME, SFILEPATH, SCREATE, DCREATE,SMAINPICTURE) 
                                                            VALUES (:NID, :SCHECKID, :SCHECKLISTID, 
                                                           :SVERSIONLIST, :STYPECHECKLISTID, :SPROCESS, :NNO, :SEVIDENCENAME, :SFILENAME, 
                                                            :SGENFILENAME, :SFILEPATH, :SCREATE, SYSDATE,:SMAINPICTURE)";

                            string GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TCHECKTRUCKFILE ORDER BY NID DESC)  WHERE ROWNUM <= 1");

                            using (OracleCommand com = new OracleCommand(sqlInsertFile, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                                com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = dataFile.dtSCHECKLISTID;
                                com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = dataFile.dtSVERSIONLIST;
                                com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = dataFile.dtSTYPECHECKLISTID;
                                com.Parameters.Add(":SPROCESS", OracleType.VarChar).Value = "2";
                                com.Parameters.Add(":NNO", OracleType.Number).Value = dataFile.dtID;
                                com.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = dataFile.dtEvidenceName;
                                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = dataFile.dtFileName;
                                com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = dataFile.dtGenFileName;
                                com.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = dataFile.dtFilePath;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SMAINPICTURE", OracleType.Number).Value = dataFile.dtPicIDdiff == null ? (object)DBNull.Value : dataFile.dtPicIDdiff;
                                com.ExecuteNonQuery();
                            }
                        }
                    }
                }
                //ส่งเมลล์ขั้นที่สอง
                //ผขส. แก้ไขข้อมูล
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "แก้ไขปัญหาสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"orange\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(lblTailerNumber.Text))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "แก้ไขปัญหาสภาพรถ Surprise Check", lblTailerNumber.Text, Session["UserID"].ToString(), "<span class=\"orange\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                }
                #endregion
                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck2))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }
            }
        }

        ///FileDetail ตรวจสอบผลแก้ไข
        if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
        {


            if ("" + checkData[8] != "")
            {

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var CHECKEDDETAIL = (List<dtCHECKED>)Session["dtCHECKED"];

                    string _cCheck = "0";


                    foreach (var DataCheck in CHECKEDDETAIL)
                    {
                        string sqlCHECKED = @"UPDATE TCHECKTRUCKITEM SET CCHECKED = :CCHECKED,SREMARKCHECKED = :SREMARKCHECKED,DUPDATECHECKED = sysdate,SUPDATECHECKED = :SUPDATECHECKED WHERE SCHECKID = :SCHECKID AND SCHECKLISTID = :SCHECKLISTID AND SVERSIONLIST = :SVERSIONLIST AND STYPECHECKLISTID = :STYPECHECKLISTID";

                        using (OracleCommand com = new OracleCommand(sqlCHECKED, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                            com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = DataCheck.dtSCHECKLISTID;
                            com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = DataCheck.dtSVERSIONLIST;
                            com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = DataCheck.dtSTYPECHECKLISTID;
                            com.Parameters.Add(":CCHECKED", OracleType.Char).Value = DataCheck.dtCHECK;
                            com.Parameters.Add(":SREMARKCHECKED", OracleType.VarChar).Value = DataCheck.dtRemark;
                            com.Parameters.Add(":SUPDATECHECKED", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();
                        }


                        if (DataCheck.dtCHECK == "1")
                        {
                            OracleCommand oraCmd = new OracleCommand();
                            oraCmd.Connection = con;
                            if (con.State == ConnectionState.Closed) con.Open();
                            oraCmd.CommandType = CommandType.StoredProcedure;
                            oraCmd.CommandText = "UTCHECKTRUCKITEM";
                            oraCmd.Parameters.Add("S_CHECKID", OracleType.Number).Value = int.Parse(checkData[8] + "");
                            oraCmd.Parameters.Add("S_CHECKLISTID", OracleType.Number).Value = DataCheck.dtSCHECKLISTID;
                            oraCmd.ExecuteNonQuery();

                            ViewState["Status3"] += ",อนุมัติผลการแก้ไข";
                        }
                        else
                        {
                            ViewState["Status3"] += ",ไม่อนุมัติ";
                        }

                    }

                    string CheckItemRepair = @"SELECT TCT.STRUCKID,TCL.CBAN FROM TCHECKTRUCK TCT
        LEFT JOIN TCHECKTRUCKITEM TCI
        ON TCI.SCHECKID = TCT.SCHECKID
        LEFT JOIN TCHECKLIST TCL
        ON TCL.SCHECKLISTID = TCI.SCHECKLISTID
        WHERE STRUCKID = '" + checkData[1] + "' AND TCI.CCHECKED ='0' AND TCL.CBAN ='1'";

                    DataTable dt_RepairTruck = CommonFunction.Get_Data(con, CheckItemRepair);
                    if (dt_RepairTruck.Rows.Count == 0)
                    {
                        DataTable dt_Type = CommonFunction.Get_Data(con, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(checkData[1] + "") + "'");
                        string SCARTYPE = dt_Type.Rows[0]["SCARTYPEID"] + "";
                        AlertTruckOnHold AT = new AlertTruckOnHold(Page, con); ;
                        AT.Lock_SAP(checkData[1] + "", SCARTYPE);
                    }
                }
                //ส่งเมลล์ขั้นตอนที่สาม
                //Log ปตท.อนุญาตใช้งาน
                string oldData = string.Empty, newData = string.Empty;
                if (ViewState["Status3"] != null)
                {
                    oldData = (ViewState["Status3"] + string.Empty).Substring(1);
                }
                if (Session["TitleName"] != null)
                {
                    newData = (Session["TitleName"] + string.Empty).Substring(1);
                }
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "พิจารณาแก้ไขสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"green\">" + newData + "</span>", oldData);
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(lblTailerNumber.Text))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "พิจารณาแก้ไขสภาพรถ Surprise Check", lblTailerNumber.Text, Session["UserID"].ToString(), "<span class=\"green\">" + newData + "</span>", oldData);
                }
                #endregion
                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck3))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }
            }
            else
            {
                //ส่งเมลล์ขั้นตอนแรก
                // ปตท. แจ้งปัญหารถ
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "แจ้งปัญหาสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"red\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(ctrck.STRAILERREGISTERNO))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "แจ้งปัญหาสภาพรถ Surprise Check", ctrck.STRAILERREGISTERNO, Session["UserID"].ToString(), "<span class=\"red\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                }
                #endregion

                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck1))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }

            }
        }
        var dtfile = (List<dtFile>)Session["dtFile"];
        if (dtfile == null || dtfile.Count == 0)
        {
            string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + hidSTRUCKID.Value + @"'";

            //string SCHECKID = "";
            DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
            if (dtCheckData.Rows.Count > 0)
            {
                SCHECKID = dtCheckData.Rows[0]["SCHECKID"] + "";

                //var dtfile = (List<dtFile>)Session["dtFile"];
                foreach (DataRow drData in dtCheckData.Rows)
                {
                    dtfile.Add(new dtFile
                    {
                        dtID = Convert.ToInt32(drData["NNO"]),
                        dtSCHECKLISTID = drData["SCHECKLISTID"] + "",
                        dtSVERSIONLIST = drData["SVERSIONLIST"] + "",
                        dtSTYPECHECKLISTID = drData["STYPECHECKLISTID"] + "",
                        dtEvidenceName = drData["SEVIDENCENAME"] + "",
                        dtFileName = drData["SFILENAME"] + "",
                        dtFilePath = drData["SFILEPATH"] + "",
                        dtGenFileName = drData["SGENFILENAME"] + "",
                        dtProcess = drData["SPROCESS"] + "",
                        dtPicIDdiff = drData["SMAINPICTURE"] + "",
                        dtNewFile = "0"

                    });
                }

                Session["dtFile"] = dtfile;
            }
        }

        BindData();
        //updMyModal.Update();
        if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
        {
            Response.Redirect("surpriseChk_lst.aspx?s=1");
        }
        else
        {
            Response.Redirect("VendorConfirmChecktruck.aspx");
        }
    }

    protected void btnSave2_Onclick(object sender, EventArgs e)
    {
        string Detail = string.Empty;
        //DataRow dr = (DataRow)Session["CheckList"];

        var Data = (List<dtFile>)Session["dtFile"];
        var Issue = Data.Where(o => o.dtProcess == "1");
        TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
        TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(Page, connection);
        TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection);
        double NPOINT = 0;
        string SCHECKID = "";
        string[] TruckData = ("" + Session["IssueData"]).Split('^');
        SCHECKID = TruckData[8] + "";

        //Session["dtCheckList"] = dt;
        //Session["DrCheckList"] = dr;
        //hidRowIndex.Value = rowIndex;
        int idxItemGrid = int.Parse(hidRowIndex.Value);

        DataTable dt = (DataTable)Session["dtCheckList"];

        ASPxGridView gvwItemCheckList_SAVE = new ASPxGridView();
        //gvwGroupCheckList.FindRowCellTemplateControl(idxGroupGrid, (GridViewDataColumn)gvwGroupCheckList.Columns[0], "gvwItemCheckList") as ASPxGridView;
        gvwItemCheckList_SAVE.DataSource = dt;
        gvwItemCheckList_SAVE.DataBind();

        dynamic datagriditem = gvwItemCheckList_SAVE.GetRowValues(idxItemGrid, "SVERSIONLIST", "SVERSION", "STOPICID", "SCHECKLISTID", "NLIST", "NDAY_MA", "CCUT", "CBAN", "CL_CACTIVE", "NPOINT", "STYPECHECKLISTID", "SCHECKLISTNAME", "SVERSION");

        var IssueFile = Issue.Where(o => o.dtSCHECKLISTID == datagriditem[3] + "" && o.dtSTYPECHECKLISTID == datagriditem[10] + "" && o.dtSVERSIONLIST == datagriditem[0] + "");
        if (IssueFile.Count() > 0)
        {

            if (IssueFile.Where(o => o.dtNewFile == "0").Count() == 0) //ถ้าเคยมีรายการมาก่อนไม่ให้ทำรายการเพิ่ม
            {
                //SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUP,SPROCESS,SCHECKID

                ///1.Call StoreProcedure for insert into DB
                #region TCHECKTRUCK
                ///TCHECKTRUCK
                ctrck.SCHECKID = SCHECKID != "" ? SCHECKID : "";//send blank for gen new id
                ctrck.NPLANID = "" + TruckData[5]; /////NPLANID
                ctrck.STERMINALID = "" + Session["SVDID"];
                ctrck.SCONTRACTID = "" + TruckData[0];         /////SCONTRACTID
                ctrck.CGROUPCHECK = "" + TruckData[6];//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                ctrck.CCLEAN = "0";
                ctrck.STRUCKID = "" + TruckData[1]; /////STRUCKID
                ctrck.SHEADERREGISTERNO = "" + TruckData[2]; /////SHEADERREGISTERNO
                ctrck.STRAILERREGISTERNO = "" + TruckData[3]; /////STRAILERREGISTERNO
                ctrck.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                ctrck.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                ctrck.SCREATE = "" + Session["UserID"];
                ctrck.SUPDATE = "" + Session["UserID"];

                ctrck.Insert();
                SCHECKID = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.
                #endregion
                #region///TCHECKTRUCKITEM
                ctrckItem.SCHECKID = SCHECKID;
                ctrckItem.SCHECKLISTID = "" + datagriditem[3];
                ctrckItem.SVERSIONLIST = "" + datagriditem[0];
                ctrckItem.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                ctrckItem.NPOINT = ("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0";
                ctrckItem.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                ctrckItem.COTHER = "0";
                ctrckItem.SOTHERREMARK = "";
                ctrckItem.STYPECHECKLISTID = "" + datagriditem[10];
                ctrckItem.SCREATE = "" + Session["UserID"];
                ctrckItem.SUPDATE = "" + Session["UserID"];
                ctrckItem.Insert();
                #endregion
                #region บันทึกห้ามวิ่ง+ตัดแต้ม

                if ("" + datagriditem[7] == "1")
                {//ถ้าเป้นเคส ห้ามวิ่ง 
                    #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();
                        OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_HEADERREGISTERNO", con);
                        ora_cmd.Parameters.Add(":S_HEADERREGISTERNO", OracleType.VarChar).Value = ctrck.SHEADERREGISTERNO.Trim();
                        ora_cmd.ExecuteNonQuery();

                        OracleCommand ora_trail = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_TRAILERREGISTERNO", con);
                        ora_trail.Parameters.Add(":S_TRAILERREGISTERNO", OracleType.VarChar).Value = ctrck.STRAILERREGISTERNO.Trim();
                        ora_trail.ExecuteNonQuery();

                        AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                        onHold.VehNo = ctrck.SHEADERREGISTERNO;
                        onHold.TUNo = ctrck.STRAILERREGISTERNO;
                        onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                        onHold.VehID = ctrck.STRUCKID;
                        onHold.SendTo();
                    }
                    #endregion
                }
                if ("" + datagriditem[6] == "1")
                {//ถ้าตัดคะแนนทันที
                    //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                    //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                    ReducePoint("01", "" + TruckData[7], "" + datagriditem[11], "" + datagriditem[3], "" + datagriditem[2], "" + datagriditem[0], "" + (("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0")
                , SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");
                }
                #endregion
                NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;
                Title += "," + datagriditem[11];
                ///2.List Data for future
            }
        }//End Checklist Item        

        if (!string.IsNullOrEmpty(Title))
        {
            Session["TitleName"] = Title;
        }
        #region Update คะแนนรวมที่ตัดที่ รายการหลัก
        ctrck.SCHECKID = SCHECKID;
        ctrck.UpdateMA();
        #endregion

        string[] checkData = ("" + Session["IssueData"]).Split('^');
        string CheckID = (ctrck.SCHECKID == "") ? checkData[8] + "" : ctrck.SCHECKID;
        ///FileDetail แจ้งตรวจพบปัญหา

        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            string sqlInsertFile = @"INSERT INTO TCHECKTRUCKFILE ( NID, SCHECKID, SCHECKLISTID, 
                                                             SVERSIONLIST, STYPECHECKLISTID, SPROCESS, NNO, SEVIDENCENAME, SFILENAME, 
                                                            SGENFILENAME, SFILEPATH, SCREATE, DCREATE) 
                                                            VALUES (:NID, :SCHECKID, :SCHECKLISTID, 
                                                             :SVERSIONLIST, :STYPECHECKLISTID, :SPROCESS, :NNO, :SEVIDENCENAME, :SFILENAME, 
                                                            :SGENFILENAME, :SFILEPATH, :SCREATE, SYSDATE)";

            foreach (var dataFile in Issue)
            {

                if (dataFile.dtNewFile == "1")
                {
                    string GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TCHECKTRUCKFILE ORDER BY NID DESC)  WHERE ROWNUM <= 1");

                    using (OracleCommand com = new OracleCommand(sqlInsertFile, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                        com.Parameters.Add(":SCHECKID", OracleType.Number).Value = CheckID;
                        com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = dataFile.dtSCHECKLISTID;
                        com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = dataFile.dtSVERSIONLIST;
                        com.Parameters.Add(":STYPECHECKLISTID", OracleType.Number).Value = dataFile.dtSTYPECHECKLISTID;
                        com.Parameters.Add(":SPROCESS", OracleType.VarChar).Value = "1";
                        com.Parameters.Add(":NNO", OracleType.Number).Value = dataFile.dtID;
                        com.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = dataFile.dtEvidenceName;
                        com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = dataFile.dtFileName;
                        com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = dataFile.dtGenFileName;
                        com.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = dataFile.dtFilePath;
                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                        com.ExecuteNonQuery();
                    }
                }
            }
        }

        ///FileDetail แจ้งผลการแก้ไข
        if ("" + Session["CGROUP"] == "0")
        {
            if ("" + checkData[8] != "")
            {
                var EditFile = Data.Where(o => o.dtProcess == "2");
                var EditGroup = Data.Select(i => new { i.dtSCHECKLISTID, i.dtSTYPECHECKLISTID, i.dtSVERSIONLIST }).Distinct();

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    foreach (var updateEdit in EditGroup)
                    {
                        string sqlEDITED = @"UPDATE TCHECKTRUCKITEM SET CEDITED = '1',SUPDATE = :SUPDATE WHERE SCHECKID = :SCHECKID AND SCHECKLISTID = :SCHECKLISTID AND SVERSIONLIST = :SVERSIONLIST AND STYPECHECKLISTID = :STYPECHECKLISTID";

                        using (OracleCommand com = new OracleCommand(sqlEDITED, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                            com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = updateEdit.dtSCHECKLISTID;
                            com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = updateEdit.dtSVERSIONLIST;
                            com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = updateEdit.dtSTYPECHECKLISTID;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();
                        }
                    }

                    foreach (var dataFile in EditFile)
                    {
                        if (dataFile.dtNewFile == "1")
                        {
                            string sqlInsertFile = @"INSERT INTO TCHECKTRUCKFILE ( NID, SCHECKID, SCHECKLISTID, 
                                                             SVERSIONLIST, STYPECHECKLISTID, SPROCESS, NNO, SEVIDENCENAME, SFILENAME, 
                                                           SGENFILENAME, SFILEPATH, SCREATE, DCREATE,SMAINPICTURE) 
                                                            VALUES (:NID, :SCHECKID, :SCHECKLISTID, 
                                                           :SVERSIONLIST, :STYPECHECKLISTID, :SPROCESS, :NNO, :SEVIDENCENAME, :SFILENAME, 
                                                            :SGENFILENAME, :SFILEPATH, :SCREATE, SYSDATE,:SMAINPICTURE)";

                            string GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TCHECKTRUCKFILE ORDER BY NID DESC)  WHERE ROWNUM <= 1");

                            using (OracleCommand com = new OracleCommand(sqlInsertFile, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                                com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = dataFile.dtSCHECKLISTID;
                                com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = dataFile.dtSVERSIONLIST;
                                com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = dataFile.dtSTYPECHECKLISTID;
                                com.Parameters.Add(":SPROCESS", OracleType.VarChar).Value = "2";
                                com.Parameters.Add(":NNO", OracleType.Number).Value = dataFile.dtID;
                                com.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = dataFile.dtEvidenceName;
                                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = dataFile.dtFileName;
                                com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = dataFile.dtGenFileName;
                                com.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = dataFile.dtFilePath;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SMAINPICTURE", OracleType.Number).Value = dataFile.dtPicIDdiff;
                                com.ExecuteNonQuery();
                            }
                        }
                    }
                }
                //ส่งเมลล์ขั้นที่สอง
                //ผขส. แก้ไขข้อมูล
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "แก้ไขปัญหาสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"orange\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(lblTailerNumber.Text))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "แก้ไขปัญหาสภาพรถ Surprise Check", lblTailerNumber.Text, Session["UserID"].ToString(), "<span class=\"orange\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                }
                #endregion
                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck2))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }
            }
        }

        ///FileDetail ตรวจสอบผลแก้ไข
        if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
        {


            if ("" + checkData[8] != "")
            {

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    var CHECKEDDETAIL = (List<dtCHECKED>)Session["dtCHECKED"];

                    string _cCheck = "0";


                    foreach (var DataCheck in CHECKEDDETAIL)
                    {
                        string sqlCHECKED = @"UPDATE TCHECKTRUCKITEM SET CCHECKED = :CCHECKED,SREMARKCHECKED = :SREMARKCHECKED,DUPDATECHECKED = sysdate,SUPDATECHECKED = :SUPDATECHECKED WHERE SCHECKID = :SCHECKID AND SCHECKLISTID = :SCHECKLISTID AND SVERSIONLIST = :SVERSIONLIST AND STYPECHECKLISTID = :STYPECHECKLISTID";

                        using (OracleCommand com = new OracleCommand(sqlCHECKED, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCHECKID", OracleType.Number).Value = checkData[8] + "";
                            com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = DataCheck.dtSCHECKLISTID;
                            com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = DataCheck.dtSVERSIONLIST;
                            com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = DataCheck.dtSTYPECHECKLISTID;
                            com.Parameters.Add(":CCHECKED", OracleType.Char).Value = DataCheck.dtCHECK;
                            com.Parameters.Add(":SREMARKCHECKED", OracleType.VarChar).Value = DataCheck.dtRemark;
                            com.Parameters.Add(":SUPDATECHECKED", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.ExecuteNonQuery();
                        }


                        if (DataCheck.dtCHECK == "1")
                        {
                            OracleCommand oraCmd = new OracleCommand();
                            oraCmd.Connection = con;
                            if (con.State == ConnectionState.Closed) con.Open();
                            oraCmd.CommandType = CommandType.StoredProcedure;
                            oraCmd.CommandText = "UTCHECKTRUCKITEM";
                            oraCmd.Parameters.Add("S_CHECKID", OracleType.Number).Value = int.Parse(checkData[8] + "");
                            oraCmd.Parameters.Add("S_CHECKLISTID", OracleType.Number).Value = DataCheck.dtSCHECKLISTID;
                            oraCmd.ExecuteNonQuery();

                            ViewState["Status3"] += ",อนุมัติผลการแก้ไข";
                        }
                        else
                        {
                            ViewState["Status3"] += ",ไม่อนุมัติ";
                        }

                    }

                    string CheckItemRepair = @"SELECT TCT.STRUCKID,TCL.CBAN FROM TCHECKTRUCK TCT
        LEFT JOIN TCHECKTRUCKITEM TCI
        ON TCI.SCHECKID = TCT.SCHECKID
        LEFT JOIN TCHECKLIST TCL
        ON TCL.SCHECKLISTID = TCI.SCHECKLISTID
        WHERE STRUCKID = '" + checkData[1] + "' AND TCI.CCHECKED ='0' AND TCL.CBAN ='1'";

                    DataTable dt_RepairTruck = CommonFunction.Get_Data(con, CheckItemRepair);
                    if (dt_RepairTruck.Rows.Count == 0)
                    {
                        DataTable dt_Type = CommonFunction.Get_Data(con, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(checkData[1] + "") + "'");
                        string SCARTYPE = dt_Type.Rows[0]["SCARTYPEID"] + "";
                        AlertTruckOnHold AT = new AlertTruckOnHold(Page, con); ;
                        AT.Lock_SAP(checkData[1] + "", SCARTYPE);
                    }
                }
                //ส่งเมลล์ขั้นตอนที่สาม
                //Log ปตท.อนุญาตใช้งาน
                string oldData = string.Empty, newData = string.Empty;
                if (ViewState["Status3"] != null)
                {
                    oldData = (ViewState["Status3"] + string.Empty).Substring(1);
                }
                if (Session["TitleName"] != null)
                {
                    newData = (Session["TitleName"] + string.Empty).Substring(1);
                }
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "พิจารณาแก้ไขสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"green\">" + newData + "</span>", oldData);
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(lblTailerNumber.Text))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "พิจารณาแก้ไขสภาพรถ Surprise Check", lblTailerNumber.Text, Session["UserID"].ToString(), "<span class=\"green\">" + newData + "</span>", oldData);
                }
                #endregion
                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck3))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }
            }
            else
            {
                //ส่งเมลล์ขั้นตอนแรก
                // ปตท. แจ้งปัญหารถ
                LOG.Instance.SaveLog(Session["vendoraccountname"] + "แจ้งปัญหาสภาพรถ Surprise Check", lblHeaderNumber.Text, Session["UserID"].ToString(), "<span class=\"red\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                #region บันทึกหางลาก Log
                if (!string.IsNullOrEmpty(ctrck.STRAILERREGISTERNO))
                {
                    LOG.Instance.SaveLog(Session["vendoraccountname"] + "แจ้งปัญหาสภาพรถ Surprise Check", ctrck.STRAILERREGISTERNO, Session["UserID"].ToString(), "<span class=\"red\">" + Session["TitleName"].ToString().Substring(1) + "</span>", "");
                }
                #endregion

                if (!string.IsNullOrEmpty(Session["TitleName"] + string.Empty))
                {
                    Session["TitleName"] = (Session["TitleName"] + string.Empty).Remove(0, 1);
                    if (SendEmail(ConfigValue.SurpriseCheck1))
                    {
                        Detail += MessEmailSuccess;
                    }
                    else
                    {
                        Detail += MessEmailFail;
                    }
                }

            }
        }
        var dtfile = (List<dtFile>)Session["dtFile"];
        if (dtfile == null || dtfile.Count == 0)
        {
            string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + hidSTRUCKID.Value + @"'";

            //string SCHECKID = "";
            DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);
            if (dtCheckData.Rows.Count > 0)
            {
                SCHECKID = dtCheckData.Rows[0]["SCHECKID"] + "";

                //var dtfile = (List<dtFile>)Session["dtFile"];
                foreach (DataRow drData in dtCheckData.Rows)
                {
                    dtfile.Add(new dtFile
                    {
                        dtID = Convert.ToInt32(drData["NNO"]),
                        dtSCHECKLISTID = drData["SCHECKLISTID"] + "",
                        dtSVERSIONLIST = drData["SVERSIONLIST"] + "",
                        dtSTYPECHECKLISTID = drData["STYPECHECKLISTID"] + "",
                        dtEvidenceName = drData["SEVIDENCENAME"] + "",
                        dtFileName = drData["SFILENAME"] + "",
                        dtFilePath = drData["SFILEPATH"] + "",
                        dtGenFileName = drData["SGENFILENAME"] + "",
                        dtProcess = drData["SPROCESS"] + "",
                        dtPicIDdiff = drData["SMAINPICTURE"] + "",
                        dtNewFile = "0"

                    });
                }

                Session["dtFile"] = dtfile;
            }
        }
        //updMyModal2.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal2", "closeModal2();", true);
        //BindData();
        //UpdatePanel2.Update();
    }

    protected void btnSave3_Onclick(object sender, EventArgs e)
    {
        int rowIndex = int.Parse(hidRowIndex.Value);
        DataTable dt = (DataTable)Session["dtCheckList"];
        string SCHECKLISTID = dt.Rows[rowIndex]["SCHECKLISTID"].ToString();
        string STYPECHECKLISTID = dt.Rows[rowIndex]["STYPECHECKLISTID"].ToString();
        string SVERSIONLIST = dt.Rows[rowIndex]["SVERSIONLIST"].ToString();
        var dataChecked = (List<dtCHECKED>)Session["dtCHECKED"];

        //ASPxPageControl SubPageControl11 = gvwItemCheckList.FindEditFormTemplateControl("SubPageControl") as ASPxPageControl;
        //ASPxRadioButtonList rblStatus11 = SubPageControl11.FindControl("rblStatus") as ASPxRadioButtonList;
        //ASPxTextBox txtRemark11 = SubPageControl11.FindControl("txtRemark") as ASPxTextBox;

        dataChecked.Add(new dtCHECKED
        {
            dtCHECK = rblStatus.SelectedValue + "",
            dtRemark = txtRemark.Text,
            dtSCHECKLISTID = SCHECKLISTID,
            dtSVERSIONLIST = SVERSIONLIST,
            dtSTYPECHECKLISTID = STYPECHECKLISTID
        });

        Session["dtCHECKED"] = dataChecked;
        //gvwItemCheckList.CancelEdit();

        //updMyModal3.Update();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal3", "closeModal3();", true);
    }
    protected void gvwHistory_RowCreated(object sender, GridViewRowEventArgs e)
    {

    }

    protected void gvwHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataTable dtCheckData;
            string qry = @"SELECT chkF.NID, chkF.SCHECKID, chkF.SCHECKLISTID, 
                       chkF.SVERSIONLIST, chkF.STYPECHECKLISTID, chkF.SPROCESS, chkF.NNO, chkF.SEVIDENCENAME, chkF.SFILENAME, 
                       chkF.SGENFILENAME, chkF.SFILEPATH, chkF.SCREATE, 
                       chkF.DCREATE, chkF.SMAINPICTURE
                       FROM (TCHECKTRUCKITEM chkI INNER JOIN TCHECKTRUCK chkT ON CHKI.SCHECKID = chkT.SCHECKID ) INNER JOIN 
                       TCHECKTRUCKFILE chkF ON chkT.SCHECKID = CHKF.SCHECKID AND CHKI.SCHECKLISTID = CHKF.SCHECKLISTID AND 
                       CHKI.SVERSIONLIST = CHKF.SVERSIONLIST AND CHKI.STYPECHECKLISTID = CHKF.STYPECHECKLISTID
                        WHERE nvl(chkI.CCHECKED,'0') = '0' AND CHKT.STRUCKID='" + hidSTRUCKID.Value + "'";

            dtCheckData = CommonFunction.Get_Data(connection, qry);

            DataTable dtt = (DataTable)Session["dtCheckLists"];
            DataTable dt = new DataTable();
            dt.Columns.Add("ID1", typeof(string));
            dt.Columns.Add("CheckListID", typeof(string));
            dt.Columns.Add("ItemsName", typeof(string));
            int rownumm = 0;

            for (int i = 0; i < dtCheckData.Rows.Count; i++)
            {
                for (int j = 0; j < dtt.Rows.Count; j++)
                {
                    if (dtCheckData.Rows[i]["SCHECKLISTID"].ToString() == dtt.Rows[j]["SCHECKLISTID"].ToString())
                    {
                        rownumm = rownumm + 1;
                        dt.Rows.Add(rownumm.ToString(), dtCheckData.Rows[i]["SCHECKLISTID"].ToString(), dtt.Rows[j]["SCHECKLISTNAME"].ToString());
                        break;
                    }
                }
            }

            Session["MyCheckList"] = dt;
            //dt.Rows[k]["SCHECKLISTNAME"].ToString(), dt.Rows[k]["SCHECKLISTID"].ToString(), dt.Rows[k]["NLIST"].ToString(), dt.Rows[k]["STYPECHECKLISTID"].ToString(), dt.Rows[k]["SVERSIONLIST"].ToString());
            GridView sgvw = new GridView();
            sgvw.DataSource = dt;
            sgvw.ID = "ListItems";
            sgvw.AutoGenerateColumns = false;
            sgvw.CssClass = "table table-primary";
            sgvw.HeaderStyle.CssClass = "HeaderStyle";
            sgvw.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.BorderColor = System.Drawing.ColorTranslator.FromHtml("#DEDFDE");
            sgvw.BorderStyle = BorderStyle.None;
            sgvw.BorderWidth = 1;
            sgvw.ShowHeader = false;
            // BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
            //sgvw.RowDataBound += new GridViewRowEventHandler(sgvw_RowDataBound);
            sgvw.RowCommand += new GridViewCommandEventHandler(sgvw_history_RowCommand);

            BoundField bf1 = new BoundField();
            bf1.DataField = "ID1";
            bf1.HeaderText = "";
            //bf1.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf1);

            BoundField bf2 = new BoundField();
            bf2.DataField = "ItemsName";
            bf2.HeaderText = "";
            //bf2.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            sgvw.Columns.Add(bf2);

            ButtonField btn = new ButtonField();
            //btn.ID = "bnDetail";
            btn.ImageUrl = "~/Images/autocomplete.gif";
            btn.CommandName = "select1";
            sgvw.Columns.Add(btn);

            ButtonField btn2 = new ButtonField();
            //btn2.ID = "bnDetail2";
            btn2.ImageUrl = "~/Images/autocomplete.gif";
            btn.CommandName = "select2";
            sgvw.Columns.Add(btn2);


            Image btnx = new Image();
            btnx.ImageUrl = "~/Images/autocomplete.gif";
            btnx.Attributes.Add("onclick", "javascript: gvrowtoggle(" + (e.Row.RowIndex + (e.Row.RowIndex + 2)) + ")");
            //btnx.CssClass = "btn btn-info";

            Table tbl = (Table)e.Row.Parent;
            GridViewRow tr = new GridViewRow(e.Row.RowIndex + 1, -1, DataControlRowType.EmptyDataRow, DataControlRowState.Normal);
            tr.CssClass = "hidden";
            TableCell tc = new TableCell();
            tc.ColumnSpan = gvw.Columns.Count;
            tc.BorderStyle = BorderStyle.None;
            tc.BackColor = System.Drawing.Color.White;//System.Drawing.Color.AliceBlue;
            tc.Controls.Add(sgvw);
            tr.Cells.Add(tc);
            tbl.Rows.Add(tr);
            e.Row.Cells[4].Controls.Add(btnx);

            sgvw.DataBind();
        }
    }

    protected void gvwHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //DataLoad();
        //gvw.PageIndex = e.NewPageIndex;
        //gvw.DataBind();
    }

    protected void gvw_AddCheckList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var HaveIssue = (bool)Session["NewIssue"];
        if (!HaveIssue)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[3].Visible = false;
            }
        }
    }

    protected void gvw_AddCheckList2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var HaveIssue = (bool)Session["NewIssue"];
        if (!HaveIssue)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[3].Visible = false;
            }
        }
    }

    protected void gvw_AddCheckList_tab2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (Session["NewIssue"] != null)
        {
            var HaveIssue = (bool)Session["NewIssue"];
            if (!HaveIssue)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[3].Visible = false;
                }
            }
        }

    }

    protected void CheckBox_OtherIssue_CheckedChanged(object sender, EventArgs e)
    {
        if (cbxOtherIssue.Checked)
        {
            txtOtherIssue.Enabled = true;
            txtDetailIssue.Enabled = true;
        }
        else
        {
            txtOtherIssue.Enabled = false;
            txtDetailIssue.Enabled = false;
        }
    }

    protected void btnSaveOtherIssue_Click(object sender, EventArgs e)
    {
        string Detail = string.Empty;
        #region TAB2
        string ss_IssueData = Session["IssueData"] + "";
        string[] ArrayIssueData = ss_IssueData.Split('^');

        //SCONTRACTID,STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,DDELIVERY,NPLANID,CGROUP,SPROCESS,SCHECKID
        if (ArrayIssueData.Length <= 0) return;

        string SCHECKID_TAB2 = "";

        TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
        TCHECKTRUCKITEM chkItem = new TCHECKTRUCKITEM(this.Page, connection);


        SCHECKID_TAB2 = CommonFunction.Get_Value(connection, @"SELECT TCHECKTRUCK.SCHECKID FROM TCHECKTRUCK  WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection("" + ArrayIssueData[0]) + @"' 
AND STRUCKID='" + CommonFunction.ReplaceInjection("" + ArrayIssueData[1]) + @"' AND NPLANID='" + CommonFunction.ReplaceInjection("" + ArrayIssueData[5]) + "'");
        if (SCHECKID_TAB2 == "")
        {
            #region TCHECKTRUCK
            ///TCHECKTRUCK
            ctrck.SCHECKID = SCHECKID_TAB2 != "" ? SCHECKID_TAB2 : "";//send blank for gen new id
            ctrck.NPLANID = "" + ArrayIssueData[5];
            ctrck.STERMINALID = "" + Session["SVDID"];
            ctrck.SCONTRACTID = "" + ArrayIssueData[0];
            ctrck.CGROUPCHECK = "" + ArrayIssueData[6];//fix หน้านี้เป็นการทำงานของ หน้าคลัง
            ctrck.CCLEAN = "0";
            ctrck.STRUCKID = "" + ArrayIssueData[1];
            ctrck.SHEADERREGISTERNO = "" + ArrayIssueData[2];
            ctrck.STRAILERREGISTERNO = "" + ArrayIssueData[3];
            ctrck.CMA = "0";//2 hold ,1 mainternent ,0 ปกติ
            ctrck.NDAY_MA = "0";
            ctrck.SCREATE = "" + Session["UserID"];
            ctrck.SUPDATE = "" + Session["UserID"];

            ctrck.Insert();
            SCHECKID_TAB2 = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.

            #endregion
        }
        #region TCHECKTRUCKITEM
        DateTime date;
        chkItem.SCHECKID = SCHECKID_TAB2;
        chkItem.SCHECKLISTID = "0";
        chkItem.CMA = "0";
        chkItem.NPOINT = "0";
        chkItem.SCHECKLISTID = "0";
        chkItem.SVERSIONLIST = "0";
        chkItem.NDAY_MA = txtNMA.Text == "" ? "0" : txtNMA.Text;
        chkItem.COTHER = cbxOtherIssue.Checked ? "1" : "0";
        chkItem.SOTHERREMARK = cbxOtherIssue.Checked ? txtOtherIssue.Text : "";
        chkItem.SREMARK = txtDetailIssue.Text;
        chkItem.SCREATE = "" + Session["UserID"];
        chkItem.SUPDATE = "" + Session["UserID"];
        chkItem.InsertTab2(ArrayIssueData[0] + "", ArrayIssueData[1] + "", (DateTime.TryParse("" + ArrayIssueData[4], out date) ? date : DateTime.Now).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "");
        #endregion
        #region Attchment File

        if (Session["dtFileOther"] != null)
        {
            using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                OracleCommand ora_del = new OracleCommand("DELETE TCHECKTRUCKATTACHMENT WHERE SCHECKID = :SCHECKID AND STRUCKID = :STRUCKID", con);
                ora_del.Parameters.Add(":SCHECKID", OracleType.Number).Value = SCHECKID_TAB2;
                ora_del.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + ArrayIssueData[1];
                ora_del.ExecuteNonQuery();

                var FileOther = (List<dtFileOther>)Session["dtFileOther"];
                foreach (var detailFile in FileOther)
                {
                    OracleCommand ora_cmd = new OracleCommand(@"INSERT INTO TCHECKTRUCKATTACHMENT ( SEVIDENCENAME,SCHECKID, NATTACHMENT, STRUCKID,SCONTRACTID, SATTACHTYPEID, SPATH,SFILE, SSYSFILE, DCREATE,SCREATE, DUPDATE, SUPDATE) 
                     VALUES (:SEVIDENCENAME, :S_CHECKID,FC_GENID_TCHECKTRUCKATTACHMENT(:S_CONTRACTID, :S_TRUCKID ,SYSDATE),:S_TRUCKID, :S_CONTRACTID,:S_ATTACHTYPEID,:S_PATH, :S_FILE,:S_SYSFILE, SYSDATE, :S_CREATE, SYSDATE,:S_UPDATE ) ", con);
                    ora_cmd.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = detailFile.dtEvidenceName;
                    ora_cmd.Parameters.Add(":S_CHECKID", OracleType.VarChar).Value = SCHECKID_TAB2;
                    ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = "" + ArrayIssueData[1];
                    ora_cmd.Parameters.Add(":S_CONTRACTID", OracleType.VarChar).Value = "" + ArrayIssueData[0];
                    ora_cmd.Parameters.Add(":S_ATTACHTYPEID", OracleType.VarChar).Value = "";
                    ora_cmd.Parameters.Add(":S_PATH", OracleType.VarChar).Value = "" + detailFile.dtFilePath;
                    ora_cmd.Parameters.Add(":S_FILE", OracleType.VarChar).Value = "" + detailFile.dtFileName;
                    ora_cmd.Parameters.Add(":S_SYSFILE", OracleType.VarChar).Value = "" + detailFile.dtGenFileName;
                    ora_cmd.Parameters.Add(":S_CREATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                    ora_cmd.Parameters.Add(":S_UPDATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                    ora_cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
        #endregion
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("surpriseChk_lst.aspx?s=1");
    }

    protected void btnCloseCancelApprove_Click(object sender, EventArgs e)
    {
        Response.Redirect("surpriseChk_lst.aspx?s=1");
    }

    protected void sgvw_history_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        DataTable dt = (DataTable)Session["MyCheckList"];
        Session["MyCheckList"] = dt;
        var dtfile = (List<dtFile>)Session["dtFile"];

        if (e.CommandName == "select1")
        {
            ////string imageUrl = "data:image/jpg;base64," + Convert.ToBase64String((byte[])dtAddCheckListItems.Rows[rowIndex]["imagename"]);
            //txtdtID.Text = dtfile[rowIndex].dtID.ToString();
            //Image_show.ImageUrl = dtAddCheckListItems.Rows[rowIndex]["imagename"].ToString();
            //Memo_show.Text = dtAddCheckListItems.Rows[rowIndex]["memo"].ToString();

            //fileUpload2.Enabled = true;
            //txtMemo2.Enabled = true;
            //btnAdd2.Enabled = true;
        }
        else if (e.CommandName == "select2")
        {

        }
        //updMyModal2.Update();
    }
}

