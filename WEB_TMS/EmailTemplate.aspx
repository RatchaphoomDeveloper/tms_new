﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="EmailTemplate.aspx.cs"
    Inherits="EmailTemplate" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblHeaderTab1" runat="server" Text="รูปแบบการส่งอีเมล์"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" Width="120px">
                                        <asp:Label ID="Label1" runat="server" Text="ประเภทอีเมล์ :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="210px">
                                        <asp:DropDownList ID="ddlEmailType" runat="server" CssClass="form-control" Width="200px"></asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right" Width="120px">
                                        <asp:Label ID="lblTemplateName" runat="server" Text="Template Name :&nbsp;"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="210px">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" Width="80px" OnClick="cmdSearch_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>

                            <br />

                            <asp:GridView ID="dgvTemplate" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" PageSize="10" AllowPaging="true"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="dgvTemplate_PageIndexChanging"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="TEMPLATE_ID" OnRowUpdating="dgvTemplate_RowUpdating"
                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับที่">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TEMPLATE_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EMAIL_TYPE_NAME" HeaderText="ประเภทอีเมล์">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TEMPLATE_NAME" HeaderText="Template Name">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SUBJECT" HeaderText="Subject">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="การกระทำ">
                                        <ItemTemplate>
                                            <asp:Button ID="cmdEdit" runat="server" Text="แก้ไข" CommandName="update" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn" CausesValidation="False"
        OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%">
                    <tr>
                        <td style="width:130px">Email Template: </td>
                        <td style="width:220px">
                            <dx:ASPxTextBox ID="txtContractNo" runat="server" ClientInstanceName="txtContractNo" NullText="Template Name" Width="200px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function(s,e){ gvwContract.PerformCallback('SEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <dx:ASPxGridView ID="gvwContract" ClientInstanceName="gvwContract" runat="server" SkinID="_gvw" AutoGenerateColumns="False"
                                KeyFieldName="EMAIL_TEMPLATE_ID" Width="99%" OnAfterPerformCallback="gvwContract_AfterPerformCallback">
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}"></ClientSideEvents>
                                <Columns>
                                    <dx:GridViewDataTextColumn FieldName="No" Caption="ที่.">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="EMAIL_TEMPLATE_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="EMAIL_TEMPLATE_NAME" Caption="Template Name">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DETAIL" Caption="Detail">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Left" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="Status" Caption="สถานะ">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="btnEdit" ClientInstanceName="btnEdit" runat="server" SkinID="_edit">
                                                <ClientSideEvents Click="function (s, e) {gvwContract.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
