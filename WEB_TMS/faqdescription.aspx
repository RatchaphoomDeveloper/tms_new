﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="faqdescription.aspx.cs" Inherits="faqdescription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .style14
        {
            width: 458px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table width='100%' border='0' cellpadding='3' cellspacing='2'>
        <tr>
            <td class="style14" >ปัญหา :
                <dx:ASPxLabel ID="lblQuest" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="style14">ผู้โพส :
                <dx:ASPxLabel ID="lblUsercreated" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
            <td class='tdBlue'>วันเวลาที่โพส :
                <dx:ASPxLabel ID="lblDatecreated" runat="server" Text="">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan='2' class="style1">รายละเอียด :
                <dx:ASPxLabel ID="lblAnswer" runat="server" Text="" EncodeHtml="false">
                </dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td colspan='2' class="style1" >รายการที่เคยอับโหลด :
                <asp:LinkButton ID="lbnFile" runat="server" Text="" onclick="lbnFile_Click"></asp:LinkButton>

                <dx:ASPxTextBox runat="server" Width="200px" ClientInstanceName="txtFilePath" ClientVisible="False"
                    ID="txtFilePath">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox runat="server" Width="200px" ClientInstanceName="txtFileName" ClientVisible="False"
                    ClientEnabled="False" ID="txtFileName">
                </dx:ASPxTextBox>
               <%-- <dx:ASPxTextBox runat="server" Width="1px" ClientInstanceName="chkUpload1" CssClass="dxeLineBreakFix"
                    ForeColor="White" ID="chkUpload1">
                    <Border BorderStyle="None"></Border>
                </dx:ASPxTextBox>--%>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
