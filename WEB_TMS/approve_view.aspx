﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="approve_view.aspx.cs" Inherits="approve_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">

        //เช็คMemo
        function maxlength() {
            var Text = txtDescription.GetText();

            if (Text.length > 250) {
                txtDescription.SetText(Text.substring(0, 250));
            }
            else {

            }

        }

    </script>
    <style type="text/css">
        .cWidth1
        {
            width: 90px;
        }
        .cWidth2
        {
            width: 150px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td width="50%" colspan="2">
                            <%--<dx:ASPxButton runat="server" ID="btnRequest" AutoPostBack="false" Text="แก้ไขใบเรียกเก็บ/คำนวณค่าธรรมเนียมใหม่">
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText('editData'); xcpn.PerformCallback();}" />
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText(&#39;editData&#39;); xcpn.PerformCallback();}">
                                </ClientSideEvents>
                            </dx:ASPxButton>--%>
                            <dx:ASPxTextBox ID="txtRk_flag" runat="server" ClientInstanceName="txtRk_flag" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td colspan="2" align="right" width="50%">
                            <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onclick="javascript:location.href='home_vendor.htm'">--%>
                            <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onclick="javascript:location.href='home_vendor.htm'">--%>
                            <dx:ASPxTextBox runat="server" ID="txtSTATUSREQ_NAME" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtCallbackType" ClientVisible="false" ClientInstanceName="txtCallbackType">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF" width="20%">วันที่ยืนคำขอ </td>
                        <td width="30%">
                            <dx:ASPxLabel runat="server" ID="lbldatereq">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF" width="20%" style="white-space: nowrap;">วันที่หมดอายุวัดน้ำ
                        </td>
                        <td width="30%">
                            <dx:ASPxLabel runat="server" ID="lblDateexp">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">วันที่นัดหมาย </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxLabel runat="server" ID="lblDuedate" ClientInstanceName="lblCalendarDate">
                                        </dx:ASPxLabel>
                                        <%-- <dx:ASPxLabel runat="server" ID="lblCalendarDate" ClientInstanceName="lblCalendarDate">
                                        </dx:ASPxLabel>--%>
                                        <dx:ASPxTextBox runat="server" ID="txtUserSelectDate" ClientVisible="false">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                        <dx:ASPxButton ID="btnShowCalendar" runat="server" SkinID="_changeappointdate" ClientVisible="false">
                                        </dx:ASPxButton>
                                        <dx:ASPxTextBox ID="txtCalendarDate" runat="server" ClientVisible="false" ClientInstanceName="txtCalendarDate">
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td bgcolor="#B9EAEF">สาเหตุ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblCause">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ประเภทคำขอ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblReq">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">ประเภทรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblCar">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ทะเบียนรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblRegis">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblVendorname">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><img src="images/btnQuest1.gif" width="23" height="23" alt="" /> รายละเอียดความจุ
                        </td>
                        <td colspan="2" align="right">
                            <%--<span class="active">* รายการเพิ่มแป้น</span>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Caption=" " FieldName="SNAME" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 1" FieldName="SLOT1" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 2" FieldName="SLOT2" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 3" FieldName="SLOT3" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 4" FieldName="SLOT4" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 5" FieldName="SLOT5" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 6" FieldName="SLOT6" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 7" FieldName="SLOT7" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 8" FieldName="SLOT8" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 9" FieldName="SLOT9" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 10" FieldName="SLOT10" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsBehavior AllowSort="false" />
                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                <SettingsPager AlwaysShowPager="false" PageSize="3" Visible="false">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/credit.png" width="16" height="16" alt="" />รายละเอียดค่าธรรมเนียม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#6AC6DF">
                            <dx:ASPxGridView runat="server" ID="gvwService" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Width="30%" CellStyle-BackColor="#EAEAEA">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceName" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICE_NAME") %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblncount" Text='<%# Eval("NITEM") != null ? Eval("NITEM").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text='<%# Eval("UNIT") != null ? Eval("UNIT").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle BackColor="#EAEAEA">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="70%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceprice" Text='<%# DataBinder.Eval(Container.DataItem,"NPRICE") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblUnit" Text='<%# DataBinder.Eval(Container.DataItem,"UNITSERVICE") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Settings ShowColumnHeaders="False"></Settings>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr runat="server" id="trdocshow">
                        <td colspan="4"><img src="images/cv.png" width="16" height="16" alt="" /> แนบไฟล์เอกสารสำคัญ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView ID="gvwdoc" runat="server" AutoGenerateColumns="false" Width="100%"
                                Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ประเภท" CellStyle-BackColor="#b9eaef" CellStyle-Border-BorderColor="White">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblDes" Text='<%# Eval("DOC_DESCRIPTION") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblstar" Text="*" ForeColor="Red" Visible='<%# Eval("CDYNAMIC").ToString() == "Y" ? true : false %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle BackColor="#B9EAEF">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="คอนโทรนดูเอกสาร" CellStyle-Border-BorderColor="White">
                                        <DataItemTemplate>
                                            <table width="96%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td width="7%" align="right">
                                                        <dx:ASPxButton runat="server" ID="btnpdf" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer">
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel runat="server" ID="lblFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_NAME") %>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxButton runat="server" ID="btnView" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer">
                                                            <Image Url="Images/ic_search.gif" Height="17px" Width="17px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxTextBox runat="server" ID="txtFilePath" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_PATH") %>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox runat="server" ID="txtFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_SYSNAME") %>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle>
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ListRdl" CellStyle-Border-BorderColor="White" CellStyle-HorizontalAlign="Right">
                                        <DataItemTemplate>
                                            <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal"
                                                SkinID="rblStatus" ClientVisible='<%# (Eval("CDYNAMIC")).ToString() == "Y" ? true : false %>'
                                                ClientEnabled="false">
                                                <Items>
                                                    <dx:ListEditItem Text="ผ่าน" Value="1" />
                                                    <dx:ListEditItem Text="ไม่ผ่าน" Value="0" />
                                                </Items>
                                                <ClientSideEvents ValueChanged="function(s,e){PLUS_MINUS(s.GetValue())}" />
                                            </dx:ASPxRadioButtonList>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Right">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CDYNAMIC" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_DESCRIPTION" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CONSIDER" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Settings ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" BorderColor="White" BorderWidth="0px"></Border>
                            </dx:ASPxGridView>
                            <dx:ASPxTextBox runat="server" ID="txtDYNAMIC" ClientInstanceName="txtDYNAMIC" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtDYNAMICALL" ClientInstanceName="txtDYNAMICALL"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/i_newtopic.gif" width="16" height="16" alt="" /> บันทึกข้อมูลการดำเนินงาน
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">หมายเหตุ</td>
                        <td colspan="2">ข้อความแจ้งผู้ขนส่ง</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxMemo runat="server" ID="txtDescription" Rows="6" AutoResizeWithContainer="false"
                                ClientInstanceName="txtDescription" MinValue="0" MaxValue="255" Width="95%">
                                <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }"></ClientSideEvents>
                                <%-- <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip"
                                                            RequiredField-ErrorText="ระบุหมายเหตุ" ValidationGroup="add">
                                                            <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุ"></RequiredField>
                                                        </ValidationSettings>--%>
                            </dx:ASPxMemo>
                        </td>
                        <td colspan="2">
                            <dx:ASPxMemo runat="server" ID="txtUserDescription" Rows="6" AutoResizeWithContainer="false"
                                ClientInstanceName="txtUserDescription" MinValue="0" MaxValue="255" Width="95%">
                                <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }"></ClientSideEvents>
                                <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip"
                                    RequiredField-ErrorText="ระบุหมายเหตุ" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ชื่อผู้ยื่นคำขอ<font color="red">*</font></td>
                        <td colspan="3">
                            <dx:ASPxLabel runat="server" ID="txtName" Width="40%">
                                <%--  <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุชื่อผู้ยื่นคำขอ"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุชื่อผู้ยื่นคำขอ"></RequiredField>
                                            </ValidationSettings>--%>
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">เบอร์โทรศัพท์<font color="red">*</font></td>
                        <td colspan="3">
                            <dx:ASPxLabel runat="server" ID="txtPhone" Width="40%">
                                <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" RequiredField-ErrorText="ระบุเบอร์โทรศัพท์"
                                                RequiredField-IsRequired="true">
                                                <RequiredField IsRequired="True" ErrorText="ระบุเบอร์โทรศัพท์"></RequiredField>
                                            </ValidationSettings>--%>
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ASPxButton runat="server" ID="btnApprove" Text="อนุมัติคำขอ" CssClass="dxeLineBreakFix"
                                AutoPostBack="false" ValidationGroup="add" ClientInstanceName="btnApprove" ClientVisible="false">
                                <%--    <ClientSideEvents Click="function (s, e) 
                                            { 
                                                txtCallbackType.SetText('Approve');
                                                txtUserDescription.isValid = true;
                                             
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false;  dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่', function (s, e) { xcpn.PerformCallback(); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>--%>
                                <ClientSideEvents Click="function (s, e) 
                                            { 
                                            txtUserDescription.HideErrorCell();
                                               txtCallbackType.SetText('Approve');
                                                dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่', function (s, e) { xcpn.PerformCallback(); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnEdit" Text="แจ้งปรับแก้คำขอ" CssClass="dxeLineBreakFix"
                                AutoPostBack="false" ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) 
                                            {
                                            txtUserDescription.ShowErrorCell();
                                             txtCallbackType.SetText('Edit');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่','คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Edit'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )    
                                            }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnCancel" Text="ยกเลิกคำขอ" CssClass="dxeLineBreakFix"
                                AutoPostBack="false" ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) 
                                            {
                                             txtCallbackType.SetText('Cancel');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่','คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Cancel'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnBack" SkinID="_back" CssClass="dxeLineBreakFix"
                                AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) {  xcpn.PerformCallback('Back');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/comment.png" width="16" height="16" alt="" /> หมายเหตุ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ASPxGridView runat="server" ID="gvwdescription" Width="100%" AutoGenerateColumns="false"
                                EnableViewState="false">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="วันที่-เวลา" FieldName="REMARK_DATE" Width="17%">
                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy} - {0:HH:mm} น.">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="ผู้บันทึก" FieldName="SNAME" Width="24%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ขั้นตอน" FieldName="SDESCRIPTION" Width="25%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หมายเหตุ" FieldName="REMARKS" Width="39%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">&nbsp; </td>
                    </tr>
                </table>
                <%--<table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td width="50%" colspan="2">
                       
                        </td>
                        <td colspan="2" align="right" width="50%">
                        
                            <dx:ASPxTextBox runat="server" ID="txtSTATUSREQ_NAME" ClientVisible="false">
                            </dx:ASPxTextBox>
                   
                        </td>
                    </tr>

                    <tr>
                        <td width="50%" colspan="2">
                            <dx:ASPxButton runat="server" ID="btnRequest" AutoPostBack="false" Text="แก้ไขใบเรียกเก็บ/คำนวณค่าธรรมเนียมใหม่" ClientVisible="false">
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText('editData'); xcpn.PerformCallback();}" />
                                <ClientSideEvents Click="function (s, e) {  txtCallbackType.SetText(&#39;editData&#39;); xcpn.PerformCallback();}">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" align="right" width="50%">
                           
                            <dx:ASPxTextBox runat="server" ID="ASPxTextBox1" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtCallbackType" ClientVisible="false" ClientInstanceName="txtCallbackType">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF" width="18%">วันที่ยืนคำขอ </td>
                        <td width="32%">
                            <dx:ASPxLabel runat="server" ID="lbldatereq">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF" class="cWidth1" style="white-space: nowrap;" width="18%">วันที่หมดอายุวัดน้ำ
                        </td>
                        <td width="32%">
                            <dx:ASPxLabel runat="server" ID="lblDateexp">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">วันที่นัดหมาย </td>
                        <td width="32%">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxLabel runat="server" ID="lblDuedate" ClientInstanceName="lblCalendarDate">
                                        </dx:ASPxLabel>
                                       
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>
                                      
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td bgcolor="#B9EAEF" class="cWidth1">สาเหตุ </td>
                        <td class="cWidth2">
                            <dx:ASPxLabel runat="server" ID="lblCause">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                   
                    <tr>
                        <td bgcolor="#B9EAEF">ประเภทคำขอ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblReq">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF" class="cWidth1">ประเภทรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblCar">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#B9EAEF">ทะเบียนรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblRegis">
                            </dx:ASPxLabel>
                        </td>
                        <td bgcolor="#B9EAEF" class="cWidth1">บริษัทขนส่ง </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblVendorname">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><img src="images/btnQuest1.gif" width="23" height="23" alt="" /> รายละเอียดความจุ
                        </td>
                        <td colspan="2" align="right">
                          
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Caption=" " FieldName="SNAME" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 1" FieldName="SLOT1" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 2" FieldName="SLOT2" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 3" FieldName="SLOT3" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 4" FieldName="SLOT4" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 5" FieldName="SLOT5" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 6" FieldName="SLOT6" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 7" FieldName="SLOT7" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 8" FieldName="SLOT8" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 9" FieldName="SLOT9" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ช่อง 10" FieldName="SLOT10" Width="8%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Right">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsBehavior AllowSort="false" />
                                <SettingsPager Visible="false" AlwaysShowPager="false">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/credit.png" width="16" height="16" alt="" />รายละเอียดค่าธรรมเนียม
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#6AC6DF">
                            <dx:ASPxGridView runat="server" ID="gvwService" Width="100%">
                                <Columns>
                                    <dx:GridViewDataColumn Width="30%" CellStyle-BackColor="#EAEAEA">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceName" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICE_NAME") %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblncount" Text='<%# Eval("NITEM") != null ? Eval("NITEM").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text='<%# Eval("UNIT") != null ? Eval("UNIT").ToString() : "" %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>

<CellStyle BackColor="#EAEAEA"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="70%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblServiceprice" Text='<%# DataBinder.Eval(Container.DataItem,"NPRICE") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblUnit" Text='<%# DataBinder.Eval(Container.DataItem,"UNITSERVICE") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                            </dx:ASPxGridView>
             
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/cv.png" width="16" height="16" alt="" /> แนบไฟล์เอกสารสำคัญ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                      
                            <dx:ASPxGridView ID="gvwdoc" runat="server" AutoGenerateColumns="false" Width="100%"
                                Border-BorderWidth="0px" Border-BorderStyle="None" Border-BorderColor="White">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ประเภท" CellStyle-BackColor="#b9eaef" CellStyle-Border-BorderColor="White">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblDes" Text='<%# Eval("DOC_DESCRIPTION") %>'>
                                            </dx:ASPxLabel>
                                            <dx:ASPxLabel runat="server" ID="lblstar" Text="*" ForeColor="Red" Visible='<%# Eval("CDYNAMIC").ToString() == "Y" ? true : false %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle BackColor="#B9EAEF">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="คอนโทรนดูเอกสาร" CellStyle-Border-BorderColor="White">
                                        <DataItemTemplate>
                                            <table width="96%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td width="7%" align="right">
                                                        <dx:ASPxButton runat="server" ID="btnpdf" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer">
                                                          
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel runat="server" ID="lblFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_NAME") %>'
                                                            CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                        <dx:ASPxButton runat="server" ID="btnView" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                            EnableDefaultAppearance="false" EnableTheming="false" Cursor="pointer">
                                                            <Image Url="Images/ic_search.gif" Height="17px" Width="17px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxTextBox runat="server" ID="txtFilePath" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_PATH") %>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                        <dx:ASPxTextBox runat="server" ID="txtFileName" Text='<%# DataBinder.Eval(Container.DataItem,"FILE_SYSNAME") %>'
                                                            ClientVisible="false">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle>
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ListRdl" CellStyle-Border-BorderColor="White" CellStyle-HorizontalAlign="Right">
                                        <DataItemTemplate>
                                            <dx:ASPxRadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal"
                                                SkinID="rblStatus" ClientVisible='<%# (Eval("CDYNAMIC")).ToString() == "Y" ? true : false %>' ClientEnabled="false">
                                                <Items>
                                                    <dx:ListEditItem Text="ผ่าน" Value="1" />
                                                    <dx:ListEditItem Text="ไม่ผ่าน" Value="0" />
                                                </Items>
                                                <ClientSideEvents ValueChanged="function(s,e){PLUS_MINUS(s.GetValue())}" />
                                            </dx:ASPxRadioButtonList>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Right">
                                            <Border BorderColor="White"></Border>
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CDYNAMIC" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_DESCRIPTION" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOC_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CONSIDER" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Settings ShowColumnHeaders="False"></Settings>
                                <Border BorderStyle="None" BorderColor="White" BorderWidth="0px"></Border>
                            </dx:ASPxGridView>
                            <dx:ASPxTextBox runat="server" ID="txtDYNAMIC" ClientInstanceName="txtDYNAMIC" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtDYNAMICALL" ClientInstanceName="txtDYNAMICALL"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/i_newtopic.gif" width="16" height="16" alt="" /> บันทึกข้อมูลการดำเนินงาน
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                      
                            <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                <tr>
                                   
                                    <td width="50%">
                                        <table width="100%">
                                            <tr>
                                                <td>หมายเหตุ</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxMemo runat="server" ID="txtDescription" Columns="80" Rows="6" AutoResizeWithContainer="false"
                                                        ClientInstanceName="txtDescription" MinValue="0" MaxValue="255" ClientEnabled="false">
                                                        <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }"></ClientSideEvents>
                                                      
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="50%">
                                        <table width="100%">
                                            <tr>
                                                <td>ข้อความแจ้งผู้ขนส่ง</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxMemo runat="server" ID="txtUserDescription" Columns="80" Rows="6" AutoResizeWithContainer="false"
                                                        ClientInstanceName="txtUserDescription" MinValue="0" MaxValue="255" ClientEnabled="false">
                                                        <ClientSideEvents KeyUp="function(s, e) {return maxlength(); }"></ClientSideEvents>
                                                        <ValidationSettings RequiredField-IsRequired="true" ErrorDisplayMode="ImageWithTooltip"
                                                            RequiredField-ErrorText="ระบุหมายเหตุ" ValidationGroup="add">
                                                            <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุ"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxMemo>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td colspan="2" align="center">
                                        <dx:ASPxButton runat="server" ID="btnApprove" Text="อนุมัติคำขอ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false" ValidationGroup="add" ClientVisible="false">
                                            <ClientSideEvents Click="function (s, e) 
                                            { 
                                                txtCallbackType.SetText('Approve');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false;  dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่', function (s, e) { xcpn.PerformCallback(); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="btnEdit" Text="แจ้งปรับแก้คำขอ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false" ClientVisible="false">
                                            <ClientSideEvents Click="function (s, e) 
                                            {
                                             txtCallbackType.SetText('Edit');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่','คุณต้องการยืนยันการแจ้งปรับแก้คำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Edit'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )    
                                            }"></ClientSideEvents>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="btnCancel" Text="ยกเลิกคำขอ" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false" ClientVisible="false">
                                            <ClientSideEvents Click="function (s, e) 
                                            {
                                             txtCallbackType.SetText('Cancel');
                                               if(!ASPxClientEdit.ValidateGroup('add')) return false; dxConfirm('คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่','คุณต้องการยืนยันการยกเลิกคำขอใช่หรือไม่', function (s, e) { xcpn.PerformCallback('Cancel'); dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="btnBack" SkinID="_back" CssClass="dxeLineBreakFix"
                                            AutoPostBack="false">
                                            <ClientSideEvents Click="function (s, e) {  xcpn.PerformCallback('Back');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><img src="images/comment.png" width="16" height="16" alt="" /> หมายเหตุ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ASPxGridView runat="server" ID="gvwdescription" Width="100%" AutoGenerateColumns="false"
                                EnableViewState="false">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="วันที่-เวลา" FieldName="REMARK_DATE" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ผู้บันทึก" FieldName="SNAME" Width="29%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ขั้นตอน" FieldName="SDESCRIPTION" Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="หมายเหตุ" FieldName="REMARKS" Width="39%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">&nbsp;</td>
                    </tr>
                </table>--%>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
