﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_suppliant_PTT_Add.aspx.cs" Inherits="admin_suppliant_PTT_Add" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="container" style="width: 100%">
        <div class="form-horizontal">
            <h2 class="page-header" style="margin-top: 10px; font-family: 'Angsana New'; color: #009120">
                <asp:Label Text="" ID="lblHeader" runat="server" />
            </h2>
            <ul class="nav nav-tabs" runat="server" id="tabtest">
                <li class="active" id="Tab1" runat="server">
                    <a href="#TabSuppliant" data-toggle="tab"
                        aria-expanded="true" runat="server" id="SuppliantTab">ข้อมูลทั่วไป</a>

                </li>
                <li class="" id="Tab2" runat="server">
                    <a href="#TabPTTDoc" data-toggle="tab" aria-expanded="false"
                        runat="server" id="PTTDocTab">หลักฐานของปตท.</a>
                </li>
                <li class="" id="Tab3" runat="server">
                    <a href="#TabVendorDoc" data-toggle="tab" aria-expanded="false"
                        runat="server" id="VendorDocTab">หลักฐานของผู้ประกอบการ</a>
                </li>
                <li class="" id="Tab4" runat="server">
                    <a href="#TabConsider" data-toggle="tab" aria-expanded="false"
                        runat="server" id="ConsiderTab">รข.รับเรื่อง/พิจารณา</a>
                </li>
                <li class="" id="Tab5" runat="server">
                    <a href="#TabCommittee" data-toggle="tab" aria-expanded="false"
                        runat="server" id="CommitteeTab">คณะกรรมการพิจารณาฯ</a>
                </li>
                <li class="" id="Tab6" runat="server">
                    <a href="#TabChange" data-toggle="tab" aria-expanded="false"
                        runat="server" id="ChangeTab">เปลี่ยนคำตัดสิน</a>
                </li>

            </ul>
            <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
                <br />
                <br />
                <div class="tab-pane fade active in" id="TabSuppliant">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">
                                        <asp:Label Text="ข้อมูลอุทธรณ์" runat="server" ID="lblSearchResult" />&#8711;</a></a>
                            <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseFindContract2">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="gvData"
                                                    runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:BoundField HeaderText="รหัส" DataField="SAPPEALID" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="วันที่เกิดเหตุ" DataField="DINCIDENT" ShowHeader="true" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ชื่อผู้ประกอบการขนส่ง" DataField="SABBREVIATION" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ประเภท" DataField="SPROCESSNAME" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="หัวข้อปัญหา" DataField="STOPICNAME" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="Delivery No" DataField="SDELIVERYNO" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="คะแนนที่ตัดก่อน" DataField="NPOINT">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="คะแนนที่ตัดหลัง" DataField="TOTAL_POINT">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ค่าปรับก่อน" DataField="COST">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="ค่าปรับหลัง" DataField="TOTAL_COST">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>

                                                        <asp:BoundField HeaderText="เวลาดำเนินการ<br/>(นับจากเกิดเหตุ)" HtmlEncode="false" DataField="DINCIDENT" ShowHeader="true" DataFormatString="{0:dd/MM/yyyy}">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField HeaderText="สถานะ<br/>การอุทธรณ์" HtmlEncode="false" DataField="STATUS" ShowHeader="true">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        </asp:BoundField>
                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView ID="dgvScoreListData" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    OnRowDeleting="dgvScoreListData_RowDeleting" OnRowCommand="dgvScoreListData_RowCommand" AlternatingRowStyle-BackColor="White"
                                                    RowStyle-ForeColor="#284775">
                                                    <Columns>
                                                        <asp:BoundField DataField="STOPICID" Visible="false">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="430px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="SuspendDriver" HeaderText="พขร. ที่กระทำความผิด">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="BLACKLIST" HeaderText="BLACKLIST">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Wrap="false" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                </asp:GridView>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseContractData" id="acollapseContractData">
                                        <asp:Label Text="ข้อมูลทั่วไป" ID="lblHeaderContract" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hiddencollapseContractData" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseContractData">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="เลขที่คดี" runat="server" ID="lblSAPPEALNO" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtSAPPEALNO" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="เลขที่ outbound" runat="server" ID="lblSDELIVERYNO" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtSDELIVERYNO" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่เกิดเหตุ" runat="server" ID="lblDINCIDENT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtDINCIDENT" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ชื่อลูกค้า" runat="server" ID="lblCUST_NAME" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtCUST_NAME" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="สถานที่เกิดเหตุ" runat="server" ID="lblSAREAACCIDENT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtSAREAACCIDENT" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="เลขที่สัญญา" runat="server" ID="lblSCONTRACTNO" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtSCONTRACTNO" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่เริ่มต้น - สิ้นสุดสัญญา" runat="server" ID="lblDBEGIN" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtDBEGIN" CssClass="datepicker" Style="text-align: center" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="-" runat="server" ID="lblDEND" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtDEND" CssClass="datepicker" Style="text-align: center" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="กระทำความผิดมาแล้ว" runat="server" ID="lblNCOUNT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtNCOUNT" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="จำนวนคะแนนที่ถูกตัด" runat="server" ID="lblNPOINT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox Text="" runat="server" ID="txtNPOINT" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="hidSAPPEALID" runat="server" />
                            <asp:HiddenField ID="hidSYSTEM_ID" runat="server" />
                            <asp:HiddenField ID="hidCHKSTATUS" runat="server" />
                            <asp:HiddenField ID="hidNREDUCEID" runat="server" />
                            <asp:HiddenField ID="hidCSTATUS" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="tab-pane fade " id="TabPTTDoc">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapsePTTDoc" id="acollapsePTTDoc">
                                        <asp:Label Text="หลักฐานของปตท." ID="Label1" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden1" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapsePTTDoc">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="รายละเอียดปัญหา" runat="server" ID="lblSDETAIL" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSDETAIL" TextMode="MultiLine" Rows="4" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผู้บันทึกข้อมูล" runat="server" ID="lblFULLNAME" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtFULLNAME" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapse1" id="acollapse1">หลักฐานการกระทำผิด&#8711;</a>

                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <asp:GridView ID="dgvUploadFilePTTDoc" OnRowDeleting="dgvUploadFilePTTDoc_RowDeleting" OnRowUpdating="dgvUploadFilePTTDoc_RowUpdating"
                                            DataKeyNames="UPLOAD_ID,FULLPATH" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/imgView.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/imgDelete.png" Width="23px"
                                                Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Width="80px" Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน/ชื่อหลักฐาน)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLPATH" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="dgvUploadFilePTTDoc" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <footer class="form-group" runat="server" id="Footer1">
                        <%--navbar-fixed-bottom --%>
                    </footer>
                </div>
                <div class="tab-pane fade " id="TabVendorDoc">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapsePTTDoc" id="acollapseVendorDoc">
                                        <asp:Label Text="หลักฐานของผู้ประกอบการ" ID="Label2" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden2" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseVendorDoc">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่ผู้ประกอบการยื่นหลักฐานเพิ่มเติม" runat="server" ID="lblDUPDATE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtDUPDATE" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="คะแนนที่ตัด(คะแนน)" runat="server" ID="lblNPOINT2" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtNPOINT2" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="รายละเอียดการขออุทธรณ์" runat="server" ID="lblSAPPEALTEXT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSAPPEALTEXT" TextMode="MultiLine" Rows="4" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผู้บันทึกข้อมูล" runat="server" ID="lblSCREATENAME" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSCREATENAME" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseVendorDoc2" id="acollapseVendorDoc2">หลักฐานการกระทำผิด&#8711;</a>

                                </div>
                                <div id="collapseVendorDoc2" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <asp:GridView ID="dgvUploadFileVendor" OnRowDeleting="dgvUploadFileVendor_RowDeleting" OnRowUpdating="dgvUploadFileVendor_RowUpdating"
                                            DataKeyNames="UPLOAD_ID,FULLPATH" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/imgView.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Width="80px" Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน/ชื่อหลักฐาน)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLPATH" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="dgvUploadFileVendor" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="tab-pane fade " id="TabConsider">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseConsider" id="acollapseConsider">
                                        <asp:Label Text="รข.รับเรื่อง/พิจารณา" ID="Label33" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden3" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseConsider">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่พิจารณาตัดสิน" runat="server" ID="lblDSENTENCE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtDSENTENCE" CssClass="datepicker" Style="text-align: center" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="สถานะ การรับเรื่องอุทธรณ์" runat="server" ID="lblCSENTENCE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" ID="ddlCSENTENCE" AutoPostBack="true" OnSelectedIndexChanged="ddlCSENTENCE_SelectedIndexChanged">
                                                        <asp:ListItem Text="รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)" Value="1" Selected="True" />
                                                        <asp:ListItem Text="รับเรื่องอุทธรณ์ (ส่งให้คณะกรรมการพิจารณา)" Value="2" />
                                                        <asp:ListItem Text="ไม่รับการอุทธรณ์" Value="3" />
                                                        <asp:ListItem Text="ขอเอกสารเพิ่มเติม" Value="4" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-6">
                                                    <asp:DropDownList runat="server" ID="ddlSENTENCERNAME" Visible="false" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:TextBox runat="server" ID="txtSWAITDOCUMENT" TextMode="MultiLine" Rows="4" CssClass="form-control" />
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Button ID="btnSENTENCERNAME" runat="server" Text="View" CssClass="btn btn-success" UseSubmitBehavior="false" OnClick="btnSENTENCERNAME_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผลการพิจารณา" runat="server" ID="lblSSENTENCER" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" ID="rblSSENTENCER" OnSelectedIndexChanged="rblSSENTENCER_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="ยืนยันผลการพิจารณา (ตัดคะแนน)" Value="2" />
                                                        <asp:ListItem Text="พิจารณาให้ไม่มีความผิด (คืนคะแนน)" Value="1" />
                                                        <asp:ListItem Text="เปลี่ยนผลการพิจารณา" Value="3" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Button ID="btnSSENTENCER" runat="server" Text="ผลการพิจารณาใหม่" CssClass="btn btn-success" OnClick="cmdAppeal_Click" />
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ข้อมูลประกอบการตัดสิน (ถ้ามี)" runat="server" ID="lblSSENTENCETEXT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSSENTENCETEXT" TextMode="MultiLine" Rows="4" CssClass="form-control" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseConsider2" id="acollapseConsider2">เอกสารแนบ&#8711;</a>

                                </div>
                                <div id="collapseConsider2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="panel panel-info">
                                            <div id="collapseConsider3" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="dgvUploadRequestFile" DataKeyNames="UPLOAD_ID" Visible="false"
                                                                runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="แนบเอกสาร">
                                                                        <HeaderStyle Width="110px" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>

                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <div class="row form-group">
                                            <div class="col-md-12 ">
                                                <label class="control-label col-md-4">
                                                    <asp:Label ID="Label38" runat="server" Text="ประเภทไฟล์เอกสาร :&nbsp;"></asp:Label></label>
                                                <div class="col-md-3">
                                                    <asp:DropDownList ID="ddlUploadType" runat="server" CssClass="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:FileUpload ID="fileUpload1" runat="server" Width="100%" />
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" OnClick="cmdUpload_Click" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:GridView ID="dgvUploadFile" OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                            DataKeyNames="UPLOAD_ID,FULLPATH" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/imgView.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/imgDelete.png" Width="23px"
                                                Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Width="80px" Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน/ชื่อหลักฐาน)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLPATH" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="cmdUpload" />
                            <asp:PostBackTrigger ControlID="dgvUploadFile" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <footer class="form-group" runat="server" id="divButton">
                        <%--navbar-fixed-bottom --%>
                        <div class="container text-right">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" runat="server" ID="btnSave" data-toggle="modal" data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" />
                                    <asp:Button Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-danger" runat="server" ID="btnCancel" data-toggle="modal" data-target="#ModalConfirmBeforeCancel" UseSubmitBehavior="false" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </footer>
                </div>
                <div class="tab-pane fade " id="TabCommittee">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseCommittee" id="acollapseCommittee">
                                        <asp:Label Text="คณะกรรมการพิจารณาฯ" ID="Label40" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden4" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseCommittee">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่พิจารณาตัดสิน" runat="server" ID="lblDSENTENCER" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtDSENTENCER" CssClass="datepicker" Style="text-align: center" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="คณะกรรมการพิจารณา" runat="server" ID="lblSENTENCER" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:CheckBoxList runat="server" ID="cblSENTENCER">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผลการพิจารณา" runat="server" ID="lblSSENTENCER2" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" ID="rblSSENTENCER2">
                                                        <asp:ListItem Text="ตัดสินให้มีความผิด" Value="2" />
                                                        <asp:ListItem Text="ไม่มีความผิด" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ข้อมูลประกอบการตัดสิน (ถ้ามี)" runat="server" ID="lblSSENTENCERTEXT" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSSENTENCERTEXT" TextMode="MultiLine" Rows="4" CssClass="form-control" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                    <footer class="form-group" runat="server" id="Footer3">
                        <%--navbar-fixed-bottom --%>
                        <div class="container text-right">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button Text="บันทึก" runat="server" ID="cmdSaveTabCommittee" CssClass="btn btn-info" data-toggle="modal" data-target="#ModalConfirmBeforeSaveTabCommittee" UseSubmitBehavior="false" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </footer>
                </div>
                <div class="tab-pane fade " id="TabChange">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseChange" id="acollapseChange">
                                        <asp:Label Text="เปลี่ยนคำตัดสิน" ID="Label43" runat="server" />&#8711;</a>
                                    <input type="hidden" runat="server" id="hidden5" />
                                </div>
                                <div class="panel-collapse collapse in" id="collapseChange">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="วันที่เปลี่ยนคำตัดสิน" runat="server" ID="lblDCHANGE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtDCHANGE" CssClass="form-control" />
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-8">
                                                    <asp:CheckBox Text="เปลี่ยนแปลงคำตัดสิน" runat="server" ID="cbCHANGE" AutoPostBack="true" OnCheckedChanged="cbCHANGE_CheckedChanged" />
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ระบุเหตุผลในการเปลี่ยนแปลงคำตัดสิน" runat="server" ID="lblSBECUASE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="4" ID="txtSBECUASE" CssClass="form-control" />
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผลการพิจารณา" runat="server" ID="lblSSENTENCER_CHANGE" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" ID="rblSSENTENCER_CHANGE">
                                                        <asp:ListItem Text="ตัดสินให้มีความผิด" Value="2" />
                                                        <asp:ListItem Text="ไม่มีความผิด" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <%--<label class="control-label col-md-4">
                                                    <asp:Label Text="ผลการพิจารณา(เดิม)" runat="server" ID="Label50" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:RadioButtonList runat="server" ID="RadioButtonList4">
                                                        <asp:ListItem Text="ตัดสินให้มีความผิด" Value="2" />
                                                        <asp:ListItem Text="ไม่มีความผิด" Value="1"/>
                                                    </asp:RadioButtonList>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-6">
                                                <label class="control-label col-md-4">
                                                    <asp:Label Text="ผู้บันทึกเปลี่ยนแปลงคำตัดสิน" runat="server" ID="lblSAPPROVEBY" />
                                                </label>
                                                <div class="col-md-8">
                                                    <asp:TextBox runat="server" ID="txtSAPPROVEBY" CssClass="form-control" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i>
                                    <a data-toggle="collapse" href="#collapseChange2" id="acollapseChange2">หนังสืออนุมัติให้เปลี่ยนแปลงคำตัดสิน&#8711;</a>

                                </div>
                                <div id="collapseChange2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseChange3" id="acollapseChange3">เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)&#8711;</a>
                                            </div>
                                            <div id="collapseChange3" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <div class="col-md-12">
                                                            <asp:GridView ID="dgvUploadRequestFileChange" DataKeyNames="UPLOAD_ID"
                                                                runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="แนบเอกสาร">
                                                                        <HeaderStyle Width="110px" />
                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>

                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 ">
                                                <label class="control-label col-md-4">
                                                    <asp:Label ID="Label51" runat="server" Text="ประเภทไฟล์เอกสาร :&nbsp;"></asp:Label></label>
                                                <div class="col-md-3">
                                                    <asp:DropDownList ID="ddlUploadTypeChange" runat="server" CssClass="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:FileUpload ID="fileUploadChange" runat="server" Width="100%" />
                                                </div>
                                                <div class="col-md-2">
                                                    <asp:Button ID="cmdUploadChange" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" OnClick="cmdUploadChange_Click" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:GridView ID="dgvUploadFileChange" OnRowDeleting="dgvUploadFileChange_RowDeleting" OnRowUpdating="dgvUploadFileChange_RowUpdating"
                                            DataKeyNames="UPLOAD_ID,FULLPATH" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/imgView.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/imgDelete.png" Width="23px"
                                                Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Width="80px" Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน/ชื่อหลักฐาน)">
                                                    <HeaderStyle Wrap="False" />
                                                    <ItemStyle Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FULLPATH" Visible="false">
                                                    <HeaderStyle Wrap="False" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="cmdUploadChange" />
                            <asp:PostBackTrigger ControlID="dgvUploadFileChange" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <footer class="form-group" runat="server" id="Footer5">
                        <%--navbar-fixed-bottom --%>
                        <div class="container text-right">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Button Text="บันทึก" runat="server" ID="Button1" data-toggle="modal" data-target="#ModalConfirmBeforeSaveChange" UseSubmitBehavior="false" />
                                    <asp:Button Text="ยกเลิก" runat="server" ID="Button2" data-toggle="modal" data-target="#ModalConfirmBeforeCancelChange" UseSubmitBehavior="false" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                    </footer>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="modal fade" id="ShowScore" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"
        aria-hidden="true">
        <div class="modal-dialog" style="width: 975px; height: 700px">
            <asp:UpdatePanel ID="updShowScore" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                    Width="48" Height="48" />
                                <asp:Label ID="lblScoreTitle" runat="server" Text="พิจารณาตัดคะแนน (อุทธรณ์)"></asp:Label></h4>
                        </div>
                        <div class="modal-body" style="width: 950px; height: 700px; overflow: scroll">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>
                                            <asp:Label ID="lblCusScoreHeader" runat="server" Text="การตัดคะแนน (เลขที่สัญญา {0})"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                    <asp:Table ID="Table111" Width="100%" runat="server">
                                                        <asp:TableRow ID="Row2" runat="server">
                                                            <asp:TableCell Width="15%">
                                                                <div id="divTopic" runat="server">
                                                                    หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font>
                                                                </div>
                                                            </asp:TableCell><asp:TableCell Width="25%" ColumnSpan="4">
                                                                <asp:DropDownList ID="cboScore" runat="server" CssClass="form-control" DataTextField="STOPICNAME"
                                                                    OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                                    AutoPostBack="true" Width="100%">
                                                                </asp:DropDownList>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row3" runat="server">
                                                            <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell ColumnSpan="4">
                                                                <asp:Label ID="lblShowOption" runat="server" Text="[พขร. ทุจริต]" Visible="false"
                                                                    ForeColor="Red"></asp:Label>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row4" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblScore1" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtScore1" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore1_OnKeyPress');" />
                                                            </asp:TableCell><asp:TableCell
                                                                Width="20%">&nbsp;</asp:TableCell><asp:TableCell Width="15%">
                                                                    <asp:Label ID="lblScore2" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label>
                                                                </asp:TableCell><asp:TableCell
                                                                    Width="25%">
                                                                    <asp:TextBox CssClass="form-control" ID="txtScore2" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore2_OnKeyPress');" />
                                                                </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row5" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label4" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtScore3" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore3_OnKeyPress');" />
                                                            </asp:TableCell><asp:TableCell
                                                                Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                    <asp:Label ID="lblScore4" runat="server" Text="แผนงานขนส่ง"></asp:Label>
                                                                </asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtScore4" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore4_OnKeyPress');" />
                                                                </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="rowListCarTab3" Visible="false">
                                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;">
                                                                <asp:GridView ID="dgvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                                    OnCheckedChanged="chkChooseScoreAll_CheckedChanged" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle Width="36px" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TRUCKID" Visible="false">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CARHEAD" HeaderText="ทะเบียนรถ (หัว)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CARDETAIL" HeaderText="ทะเบียนรถ (หาง)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="rowListTotalCarTab3" runat="server" Visible="false">
                                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px; vertical-align: top;">
                                                                <asp:GridView ID="dgvScoreTotalCar" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                                    OnCheckedChanged="chkChooseScoreListCarAll_CheckedChanged" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle Width="36px" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TOTAL_CAR" HeaderText="จำนวนรถ (ตามเอกสาร)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="จำนวนรถ (หักคะแนน)">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtChooseTotalCar" runat="server" Width="120px"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="rowTotalCarTab3" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label12" runat="server" Text="จำนวนรถ (คัน)"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox ID="txtTotalCarTab3" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow runat="server" ID="rowOil" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label10" runat="server" Text="ค่าน้ำมัน / ลิตร"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox ID="txtOilPrice" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilPrice_OnKeyPress');"
                                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="Label11" runat="server" Text="จำนวนน้ำมัน (ลิตร)"></asp:Label>
                                                            </asp:TableCell><asp:TableCell
                                                                Width="20%">
                                                                <asp:TextBox ID="txtOilQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilQty_OnKeyPress');"
                                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow runat="server" ID="rowZeal" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblZeal1" runat="server" Text="ค่าปรับต่อ 1 ซิล"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox ID="txtZeal" runat="server" Enabled="false" CssClass="form-control"
                                                                    Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="Label9" runat="server" Text="จำนวน ซิล"></asp:Label>
                                                            </asp:TableCell><asp:TableCell
                                                                Width="20%">
                                                                <asp:TextBox ID="txtZealQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtZealQty_OnKeyPress');"
                                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row6" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCost" runat="server" Text="ค่าปรับ"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="200px" ReadOnly="true"
                                                                    Enabled="false" Style="text-align: center" />
                                                            </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblDisableDriverDay" runat="server" Text="ระงับ พชร. (วัน)"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtDisableDriver" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center" />
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row7" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblScore5" runat="server" Text="ผลคูณความรุนแรง"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtScore5" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center" />
                                                            </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblScore6" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtScore6" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center" />
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row10" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblSuspainDriver" runat="server" Text="พขร. ที่กระทำความผิด"></asp:Label>
                                                            </asp:TableCell>
                                                            <asp:TableCell ColumnSpan="3">
                                                                <asp:CheckBoxList ID="chkSuspainDriver" runat="server"></asp:CheckBoxList>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="TableRow1" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblBlacklist" runat="server" Text="Blacklist" Visible="false"></asp:Label>
                                                            </asp:TableCell>
                                                            <asp:TableCell ColumnSpan="3">
                                                                <asp:CheckBox ID="chkBlacklistTopic" runat="server" Visible="false" />
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="5">
                                                                <asp:GridView ID="dgvScoreList" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    OnRowDeleting="dgvScore_RowDeleting" OnRowCommand="dgvScoreList_RowCommand" AlternatingRowStyle-BackColor="White"
                                                                    RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="STOPICID" Visible="false">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle Width="430px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="false" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SuspendDriver" HeaderText="พขร. ที่กระทำความผิด">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle Wrap="false" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle Wrap="false" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="BLACKLIST" HeaderText="BLACKLIST">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle Wrap="false" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Action">
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="false" />
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgViewScore" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Select" />&nbsp;
                                                                                <asp:ImageButton ID="imgDeleteScore" runat="server" ImageUrl="~/Images/bin1.png"
                                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer" style="text-align: right">
                                            <asp:Button ID="cmdAddScore" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px"
                                                UseSubmitBehavior="false" runat="server" Text="เพิ่มรายการ" OnClick="cmdAddScore_Click" />
                                            <br />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info" id="divScore" runat="server">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>สรุปผลการพิจารณา (หลังอุทธรณ์)
                                        </div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                    <asp:Table ID="Table222" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:CheckBox ID="chkBlacklist" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" />
                                                            </asp:TableCell>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="27%">
                                                                <asp:Label ID="lblCusScoreType" runat="server" Text="เงื่อนไขการตัดคะแนน"></asp:Label>
                                                            </asp:TableCell><asp:TableCell
                                                                ColumnSpan="2" Width="27%">
                                                                <asp:RadioButtonList ID="radCusScoreType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                    OnSelectedIndexChanged="radCusScoreType_SelectedIndexChanged">
                                                                </asp:RadioButtonList>
                                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblShowSumPoint" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblAppealPoint" runat="server" Text="หักคะแนน (หลังอุทธรณ์)" Visible="false"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtAppealPoint" runat="server" Width="140px"
                                                                    Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblShowSumCost" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblAppealCost" runat="server" Text="ค่าปรับ (หลังอุทธรณ์)" Visible="false"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtAppealCost" runat="server" Width="140px"
                                                                    Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtCostOther" runat="server" Width="200px"
                                                                    Style="text-align: center"> </asp:TextBox>
                                                            </asp:TableCell><asp:TableCell> </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label84" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px"
                                                                    Enabled="false" Style="text-align: center"></asp:TextBox>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblShowSumDisable" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:Label ID="lblAppealDisable" runat="server" Text="ระงับ พขร. (หลังอุทธรณ์)" Visible="false"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtAppealDisable" runat="server" Width="140px"
                                                                    Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)"></asp:Label>
                                                            </asp:TableCell><asp:TableCell>
                                                                <asp:TextBox CssClass="form-control" ID="txtOilLose" runat="server" Width="200px"
                                                                    Style="text-align: center"> </asp:TextBox>
                                                            </asp:TableCell><asp:TableCell> </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblRemarkTab3" runat="server" Text="หมายเหตุ"></asp:Label>
                                                            </asp:TableCell><asp:TableCell
                                                                ColumnSpan="4">
                                                                <asp:TextBox CssClass="form-control" ID="txtRemarkTab3" runat="server" Width="100%"
                                                                    Height="70px"> </asp:TextBox>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer" style="text-align: right">
                                            <asp:Button ID="cmdSaveTab3" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdSaveTab3_Click" />
                                            &nbsp;
                                            <%--<asp:Button ID="cmdCloseTab3" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCloseTab3_Click" />--%>
                                            <asp:Button ID="cmdCloseTab3" runat="server" class="btn btn-md btn-hover btn-info" data-dismiss="modal" Width="90px" aria-hidden="true" Text="ปิด" UseSubmitBehavior="false" />
                                            <br />
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />
                            <button type="button" id="Button7" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <input id="btnSave2" type="button" value="บันทึกและส่งข้อมูล" data-toggle="modal" data-target="#ModalSENTENCERNAM" class="btn btn-md bth-hover btn-info hidden" />
    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpCancel" IDModel="ModalConfirmBeforeCancel" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpCancel_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveChange" IDModel="ModalConfirmBeforeSaveChange" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveChange_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpCancelChange" IDModel="ModalConfirmBeforeCancelChange" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpCancel_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveTabCommittee" IDModel="ModalConfirmBeforeSaveTabCommittee" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveTabCommittee_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpCancelTabCommittee" IDModel="ModalConfirmBeforeCancelTabCommittee" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpCancelTabCommittee_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <div class="modal fade" style="z-index: 1060" id='ModalSENTENCERNAM' role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        <asp:Image ID="imgPopup" runat="server" ImageUrl="~/Images/imgInformation.png" Width="48" Height="48" />
                        <asp:Label ID="lblModalTitle" runat="server" CssClass="TextTitle">รายชื่อคณะกรรมการ</asp:Label>
                    </h4>
                </div>
                <div class="modal-body TextDetail" id="TextDetail" style="height: 400px">
                </div>
            </div>
        </div>
    </div>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);

        $(document).ready(function () {

            fcollapse();
        });



        function EndRequestHandler(sender, args) {

            fcollapse();
            console.log('sss');
        }

        function fcollapse() {

        }
    </script>
</asp:Content>
