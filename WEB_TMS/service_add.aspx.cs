﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;
using System.Data.OracleClient;
using System.Configuration;
using TMS_BLL.Master;


public partial class service_add : System.Web.UI.Page
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    //private List<STDataReq01> _dataReq01 = new List<STDataReq01>();

    private List<STDataReq01> _dataReq01
    {
        get
        {
            return (List<STDataReq01>)Session["_dataReq01"];
            //return _dataReq01;
        }
        set
        {
            Session["_dataReq01"] = value;
        }
    }

    //private static List<STStatusPan> _dataStatusPan = new List<STStatusPan>();
    //string _sServiceWaterID = "00001";//รหัสบริการวัดน้ำ
    //string _sServiceAddPanID = "00002";//รหัสบริการเพิ่มแป้น
    //string _sServiceAddSeal = "00003";//ตีซีล
    //string _sServicePaint = "00004";//ค่าบริการพ่นสาระสำคัญ
    //string _sServiceDoc = "00005";//ค่าสำเนาใบรับรองการตรวจ
    //string _sServiceNewCar = "00005";//รถใหม่ไม่เคยวัดน้ำ
    //string _sConfigID = "001";//รหัสค่าวันที่คอนฟิก
    //private static string sUserID = "";//User
    //private static string _sTotalCap = "0";
    //private static string VENDOR_ID = "";

    [Serializable]
    private class STDataReq01
    {
        public string sUserID { get; set; }
        public string sVendorID { get; set; }
        public string sVendorName { get; set; }
        public string sCarCateID { get; set; }
        public string sCarCate { get; set; }
        public string sCauseID { get; set; }
        public string sReqTypeID { get; set; }
        public string VEH_ID { get; set; }
        public string Tu_ID { get; set; }
        public string VEH_No { get; set; }
        public string Tu_No { get; set; }
        public string sVehChasis { get; set; }
        public string sTuChasis { get; set; }
        public string sStatusCar { get; set; }
        public string sCarTypeID { get; set; }
        public string dWaterExpire { get; set; }
        public string nPanAll { get; set; }
        public string nCompartAll { get; set; }
        public string nCapacityAll { get; set; }
        public string nTotal { get; set; }
        public string nServicePrice { get; set; }
        public string sServicePriceID { get; set; }
        public string nPricePan { get; set; }
    }
    private string _sTotalCap
    {
        get
        {
            if ((string)ViewState["_sTotalCap"] != null)
                return (string)ViewState["_sTotalCap"];
            else
                return "0";
        }
        set
        {
            ViewState["_sTotalCap"] = value;
        }
    }
    private string sUserID
    {
        get
        {
            if ((string)ViewState["sUserID"] != null)
                return (string)ViewState["sUserID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["sUserID"] = value;
        }
    }
    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    private string VENDOR_ID
    {
        get
        {
            if ((string)ViewState["VENDOR_ID"] != null)
                return (string)ViewState["VENDOR_ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["VENDOR_ID"] = value;
        }
    }
    private string _sServiceWaterID
    {
        get
        {
            if ((string)ViewState["_sServiceWaterID"] != null)
                return (string)ViewState["_sServiceWaterID"];
            else
                return "00001";
        }
        set
        {
            ViewState["_sServiceWaterID"] = value;
        }
    }
    private string _sServiceAddPanID
    {
        get
        {
            if ((string)ViewState["_sServiceAddPanID"] != null)
                return (string)ViewState["_sServiceAddPanID"];
            else
                return "00002";
        }
        set
        {
            ViewState["_sServiceAddPanID"] = value;
        }
    }
    private string _sServiceAddSeal
    {
        get
        {
            if ((string)ViewState["_sServiceAddSeal"] != null)
                return (string)ViewState["_sServiceAddSeal"];
            else
                return "00003";
        }
        set
        {
            ViewState["_sServiceAddSeal"] = value;
        }
    }
    private string _sServicePaint
    {
        get
        {
            if ((string)ViewState["_sServicePaint"] != null)
                return (string)ViewState["_sServicePaint"];
            else
                return "00004";
        }
        set
        {
            ViewState["_sServicePaint"] = value;
        }
    }
    private string _sServiceDoc
    {
        get
        {
            if ((string)ViewState["_sServiceDoc"] != null)
                return (string)ViewState["_sServiceDoc"];
            else
                return "00005";
        }
        set
        {
            ViewState["_sServiceDoc"] = value;
        }
    }
    private string _sServiceNewCar
    {
        get
        {
            if ((string)ViewState["_sServiceNewCar"] != null)
                return (string)ViewState["_sServiceNewCar"];
            else
                return "00005";
        }
        set
        {
            ViewState["_sServiceNewCar"] = value;
        }
    }
    private string _sConfigID
    {
        get
        {
            if ((string)ViewState["_sConfigID"] != null)
                return (string)ViewState["_sConfigID"];
            else
                return "001";
        }
        set
        {
            ViewState["_sConfigID"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["UserID"] + "") && !string.IsNullOrEmpty(Session["SVDID"] + ""))
        {

            if (!IsPostBack)
            {

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

                sUserID = Session["UserID"] + "";

                //เซ็ต ประเภทคำขอ และสาเหตุ
                SetComBoReqType(cboRequestType, "01");
                SetComBoCause(cboCause);

                //เซ็ตข้อมูลพื้นฐาน โดยจะเช็คว่า ถ้าประเภทรถเป็น 0 จะส่งหัวไปหาข้อมูล แต่ถ้าเป็นหางจะส่งหางไปหาข้อมูล
                //_dataReq01.Clear();
                _dataReq01 = new List<STDataReq01>(100);
                _dataReq01.Add(new STDataReq01
                {
                    sReqTypeID = "",
                    VEH_ID = "",
                    Tu_ID = "",
                    sUserID = sUserID,
                    sCauseID = "",
                    sCarTypeID = "",
                });

                VENDOR_ID = SystemFunction.GET_VENDORID(sUserID);

                SetVendorName();

                //SetBasicData(sCarTypeID, vehID, tuID);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
    }

    private void SetVendorName()
    {
        DataTable dt = CommonFunction.Get_Data(_conn, "SELECT * FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VENDOR_ID) + "'");
        if (dt.Rows.Count > 0)
        {
            lblVendorName.Text = dt.Rows[0]["SABBREVIATION"] + "";
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {



    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "ListData":

                DataTable dtchk1 = VehicleBLL.Instance.CHECKTBL_REQ_BY_TTRUCKBLL(cboCarregis.Value.ToString().Trim());

                if (dtchk1.Rows[0]["STATUS"].ToString().Trim() == "S")//ถ้ามีใบคำขอทะเบียนนี้ยุแล้ว บันทึกซ้ำไม่ได้ หรือต้องมีสถานะ ปิดงานแล้วหรือยกเลิกคำขอเท่านั้น
                {

                    string Check_SCHASIS = SystemFunction.Check_SCHASIS(cboCarregis.Value + "");
                    //เช็คว่ารถมีเลข แซสสซีย์ ไหม
                    if (!string.IsNullOrEmpty(Check_SCHASIS))
                    {

                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Check_SCHASIS + "' );");
                        cboCarregis.Text = "";
                        ClearCaPacity();
                        StepShowReq();
                        return;
                    }

                    string Query = "SELECT * FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(cboCarregis.Value + "") + "'";

                    DataTable dt = CommonFunction.Get_Data(_conn, Query);

                    if (dt.Rows.Count > 0)
                    {

                        SetBasicData(dt.Rows[0]["SCARTYPEID"] + "", dt.Rows[0]["STRUCKID"] + "", dt.Rows[0]["STRAILERID"] + "");
                    }
                    else
                    {
                        ClearCaPacity();
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','เลขทะเบียนนี้เป็นใบคำขออยู่แล้วกรุณาตรวจสอบ' );");
                    cboCarregis.Text = "";
                    ClearCaPacity();
                    StepShowReq();
                    return;
                }





                break;


            case "saveNewReq":
                DataTable dtchk = VehicleBLL.Instance.CHECKTBL_REQ_BY_TTRUCKBLL(cboCarregis.Value.ToString().Trim());
                if (dtchk.Rows[0]["STATUS"].ToString().Trim() == "S")//ถ้ามีใบคำขอทะเบียนนี้ยุแล้ว บันทึกซ้ำไม่ได้ หรือต้องมีสถานะ ปิดงานแล้วหรือยกเลิกคำขอเท่านั้น
                {
                    foreach (var _i in _dataReq01.ToList())
                    {

                        string _VehChasisID = _i.sVehChasis;
                        string _carcatID = _i.sCarCateID;
                        string _causeID = _i.sCauseID;
                        string _reqTypeID = _i.sReqTypeID;
                        string _vendorID = _i.sVendorID;
                        string _VehID = _i.VEH_ID;
                        string _VehNo = _i.VEH_No;

                        string _TuID = _i.Tu_ID;
                        string _TuNo = _i.Tu_No;
                        string _TuChasisID = _i.sTuChasis;
                        string _total = _i.nTotal;
                        string _nCapTotal = _i.nCapacityAll;
                        string _CODE = _i.sUserID;
                        string _servicePrice = _i.nServicePrice;
                        string _servicePriceID = _i.sServicePriceID;
                        string _REQUEST_OTHER_FLAG = "O";
                        string _TOTLE_CAP = _i.nCapacityAll;
                        string _nCompart = _i.nCompartAll;

                        string CodiTionDateWaterExpire = "";
                        string sTempDate = "";
                        if (!string.IsNullOrEmpty(txtWaterExpire.Text))
                        {
                            string[] ArrDate = txtWaterExpire.Text.Split('/');
                            sTempDate = (int.Parse(ArrDate[2]) - 543) + "/" + ArrDate[1] + "/" + ArrDate[0];// + " 00:00:00";
                            CodiTionDateWaterExpire = "TO_DATE('" + sTempDate + "', 'YYYY-MM-DD')";
                        }
                        else
                        {
                            CodiTionDateWaterExpire = "null";
                        }

                        StepShowReq();

                        using (OracleConnection con = new OracleConnection(_conn))
                        {
                            OracleTransaction tran;
                            con.Open();
                            tran = con.BeginTransaction();
                            try
                            {
                                DataTable dtgen = VehicleBLL.Instance.GENERATE_REQ_MV(cboRequestType.Value.ToString());
                                string _reqID = dtgen.Rows[0]["REQUEST_ID_F"].ToString();
                                #region Query
                                string _qryInsertReq = @"INSERT INTO TBL_REQUEST(REQUEST_ID,REQTYPE_ID,CAUSE_ID , CARCATE_ID ,STRUCKID,VEH_ID,VEH_NO ,TU_ID,TU_NO,VEH_CHASSIS,TU_CHASSIS,VENDOR_ID 
                            ,TOTLE_CAP,TOTLE_SLOT,TOTLE_SERVCHAGE,STATUS_FLAG,CREATE_CODE,CREATE_DATE,REQUEST_OTHER_FLAG,RK_FLAG,REMARK_CAUSE,WATER_EXPIRE_DATE) VALUES ("
                                 + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_reqTypeID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_causeID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_carcatID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_VehID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_VehID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_VehNo) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_TuID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_TuNo) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_VehChasisID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_TuChasisID) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_vendorID) + "'"
                                 + "," + CommonFunction.ReplaceInjection(_TOTLE_CAP + "") + ""
                                 + ",'" + CommonFunction.ReplaceInjection(_nCompart + "") + "'"
                                   + ",'" + CommonFunction.ReplaceInjection(_i.nTotal + "") + "'"
                                 + ",'" + CommonFunction.ReplaceInjection("09") + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(_CODE) + "'"
                                 + ",SYSDATE"
                                 + ",'" + CommonFunction.ReplaceInjection(_REQUEST_OTHER_FLAG) + "'"
                                 + ",'" + CommonFunction.ReplaceInjection("N") + "'"
                                 + ",'" + CommonFunction.ReplaceInjection(txtRemarkCause.Text) + "'"
                                    + ", " + CodiTionDateWaterExpire + @""
                                 + ")";

                                using (OracleCommand cmd = new OracleCommand(_qryInsertReq, con))
                                {
                                    //_reqID = SystemFunction.genID_TBL_REQUEST(cboRequestType.Value + "");

                                    cmd.Transaction = tran;
                                    //cmd.CommandText = _qryInsertReq;

                                    //int result = ;

                                #endregion


                                    if (cmd.ExecuteNonQuery() > 0)
                                    {

                                        SaveCapacityDetail(_reqID, int.Parse(_nCompart));

                                        switch (cboRequestType.Value + "")
                                        {
                                            case "01":
                                                SaveTBLReqItem(_reqID, _sServiceWaterID, "1", _i.nServicePrice);
                                                string nNewPan = !string.IsNullOrEmpty(_i.nPanAll) ? _i.nPanAll : "0";
                                                if (nNewPan != "0")
                                                {
                                                    SaveTBLReqItem(_reqID, _sServiceAddPanID, nNewPan, _i.nPricePan);
                                                }

                                                break;
                                            case "02":
                                                SaveTBLReqItem(_reqID, _sServiceAddSeal, "1", _i.nServicePrice);
                                                break;
                                            case "03":
                                                SaveTBLReqItem(_reqID, _sServiceDoc, "1", _i.nServicePrice);
                                                break;
                                            case "04":
                                                SaveTBLReqItem(_reqID, _sServicePaint, "1", _i.nServicePrice);
                                                break;
                                        }


                                        SystemFunction.Add_To_TBL_REQREMARK(_reqID, "Y", SystemFunction.GetRemark_WorkFlowRequest(1), "09", "S", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(1), "", "N");
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='download_invoice_request.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "';});");
                                    }
                                }
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                tran.Rollback();
                            }
                            finally
                            {
                                con.Close();
                            }
                        }
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','เลขทะเบียนนี้เป็นใบคำขออยู่แล้วกรุณาตรวจสอบ' );");
                }
                break;

            case "ChangeReq":
                SetComBoCause(cboCause);
                StepShowReq();
                ClearCodition();
                ClearCaPacity();

                break;

            case "back":
                xcpn.JSProperties["cpRedirectTo"] = "vendor_HomeAlert.aspx";
                break;

        }
    }

    private void SetComBoReqType(ASPxComboBox comboBox, string ReqTypeID)
    {
        comboBox.Items.Clear();
        comboBox.DataSource = sdsReqType;
        comboBox.ValueField = "ID";
        comboBox.TextField = "NAME";
        comboBox.DataBind();
        comboBox.Value = ReqTypeID;
    }

    private void SetComBoCause(ASPxComboBox comboBox)
    {
        comboBox.Items.Clear();

        DataTable dt = CommonFunction.Get_Data(_conn, @"
                                SELECT CAUSE_ID as ID, REQTYPE_ID as TYPE ,CAUSE_NAME as NAME FROM TBL_CAUSE
                                Where ISACTIVE_FLAG = 'Y' AND REQTYPE_ID IN('" + CommonFunction.ReplaceInjection(cboRequestType.Value + "") + @"','99')
                                ORDER BY TYPE ASC, ID ASC");

        comboBox.DataSource = dt;
        comboBox.ValueField = "ID";
        comboBox.TextField = "NAME";
        comboBox.DataBind();
    }

    public void SetBasicData(string sCarTypeID, string STRUCKID, string STRUCKIDTAIL)
    {

        string VENDOR_ID = SystemFunction.GET_VENDORID(sUserID);

        #region เซตความจุรวม
        string _qrySumCap = @"
                SELECT STRUCKID ,COUNT(NCOMPARTNO) as NCOMPARTNO,SUM(CAP) as NCAPTOTAL
                FROM 
                (
                    SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) as CAP
                    FROM TTRUCK_COMPART 
                    WHERE STRUCKID ='" + CommonFunction.ReplaceInjection(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL) + @"' AND NPANLEVEL <> 0
                    GROUP BY STRUCKID,NCOMPARTNO
                )
                GROUP BY STRUCKID";


        _sTotalCap = "";
        DataTable _dtSumCap = new DataTable();
        _dtSumCap = CommonFunction.Get_Data(_conn, _qrySumCap);
        if (_dtSumCap.Rows.Count > 0)
        {
            _sTotalCap = _dtSumCap.Rows[0]["NCAPTOTAL"] + "";
            lblCompartTotal.Text = _dtSumCap.Rows[0]["NCOMPARTNO"] + "";
            lblCapacityTotal.Text = !string.IsNullOrEmpty(_sTotalCap) ? int.Parse(_sTotalCap).ToString("#,###,###,###") : "0";
        }
        #endregion

        //เซ็ตความจุของรถ
        SetCapacityData(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL);

        //เซ็ตข้อมูลรถ
        SetDetailCar(sCarTypeID, STRUCKID, STRUCKIDTAIL, VENDOR_ID);

    }

    //เซ็ตรถ
    private void SetDetailCar(string sCarTypeID, string STRUCKID, string STRUCKIDTAIL, string VENDOR_ID)
    {
        string _qry = "";
        switch (sCarTypeID)
        {
            case "0"://รถบรรทุก ไม่มีหาง
                _qry = @"
                SELECT VEH_ID,VEH_CHASIS, VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO,MAX(SLOT1) as SLOT1,SUM(CAP1) as CAP1, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID FROM
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID,T.SHEADREGISTERNO  as REGISNO
                    ,TC.NCOMPARTNO as SLOT1,MAX(TC.NCAPACITY) as CAP1,NULL as SLOT2,NULL as CAP2,NULL as TU_ID,NULL as TU_CHASIS,NULL as TU_No,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO ,V.SVENDORID
                )
                GROUP BY VEH_ID,VEH_CHASIS,VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID";
                break;
            case "3":
                _qry = @"
                SELECT * FROM 
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,T.STRAILERREGISTERNO as TU_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID
                ,T.SHEADREGISTERNO||'/'||T.STRAILERREGISTERNO  as REGISNO,COUNT(TC.NCOMPARTNO) as SLOT1 ,SUM(TC.NCAPACITY) as CAP1
                FROM  TTRUCK T 
                LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID
                WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,V.SVENDORID
                )T1,
                (
                    SELECT TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,COUNT(SLOT2) as SLOT2,SUM(CAP2) as CAP2,DWATEREXPIRE
                    FROM
                    (
                        SELECT T.STRUCKID as TU_ID,T.SCHASIS as TU_CHASIS,CT.CARCATE_NAME,CT.CARCATE_ID,TC.NCOMPARTNO as SLOT2,MAX(TC.NCAPACITY) as CAP2,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKIDTAIL) + @"%'

                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO
                )
                GROUP BY TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,DWATEREXPIRE
                ) T2";
                break;
        }



        DataTable _dtVendor = new DataTable();
        _dtVendor = CommonFunction.Get_Data(_conn, _qry);
        if (_dtVendor.Rows.Count > 0)
        {
            lblWaterExpire.Text = !string.IsNullOrEmpty("" + _dtVendor.Rows[0]["DWATEREXPIRE"]) ? DateTime.Parse(_dtVendor.Rows[0]["DWATEREXPIRE"] + "").ToString("dd/MM/yyyy") : " - ";
            txtWaterExpire.Text = !string.IsNullOrEmpty("" + _dtVendor.Rows[0]["DWATEREXPIRE"]) ? DateTime.Parse(_dtVendor.Rows[0]["DWATEREXPIRE"] + "").ToString("dd/MM/yyyy") : "";
            //lblLicenseNumber.Text = _dtVendor.Rows[0]["REGISNO"].ToString();
            lblTruckType.Text = _dtVendor.Rows[0]["CARCATE_NAME"].ToString();
            string _nSlot1 = _dtVendor.Rows[0]["SLOT1"].ToString();
            string _nSlot2 = _dtVendor.Rows[0]["SLOT2"].ToString();
            string _sChasis1 = _dtVendor.Rows[0]["VEH_CHASIS"].ToString();
            string _sChasis2 = _dtVendor.Rows[0]["TU_CHASIS"].ToString();

            #region Set รถใหม่/รถเก่า
            string _statusCar = "";

            #region เช็คประเภทรถแบบเก่า
            //if (sCarTypeID == "0")
            //{
            //    if (!string.IsNullOrEmpty(_nSlot1))
            //    {
            //        _statusCar = "0";
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(_sChasis1))
            //        {
            //            _statusCar = "0";
            //        }
            //        else
            //        {
            //            _statusCar = "1";
            //        }

            //    }

            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) && !string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (!string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) || string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) || !string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) && string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "1";
            //    }
            //}
            #endregion

            #region เช็คประเภทรถแบบใหม่ เช็คจาก SCERT_NO

            _statusCar = CheckNewCarOrOldCar(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL);

            #endregion

            #endregion

            #region Update List

            _dataReq01.ToList().ForEach(f => f.sReqTypeID = cboRequestType.Value + "");
            _dataReq01.ToList().ForEach(f => f.sCauseID = cboCause.Value + "");
            _dataReq01.ToList().ForEach(f => f.VEH_ID = _dtVendor.Rows[0]["VEH_ID"].ToString());//update VEH_No
            _dataReq01.ToList().ForEach(f => f.Tu_ID = _dtVendor.Rows[0]["TU_ID"].ToString());//update Tu_No
            _dataReq01.ToList().ForEach(f => f.VEH_No = _dtVendor.Rows[0]["VEH_No"].ToString());//update VEH_No
            _dataReq01.ToList().ForEach(f => f.Tu_No = _dtVendor.Rows[0]["Tu_No"].ToString());//update Tu_No
            _dataReq01.ToList().ForEach(f => f.sVehChasis = _dtVendor.Rows[0]["VEH_CHASIS"].ToString());//update VEH_CHASIS
            _dataReq01.ToList().ForEach(f => f.sTuChasis = _dtVendor.Rows[0]["TU_CHASIS"].ToString());//update TU_CHASIS
            _dataReq01.ToList().ForEach(f => f.sVendorName = _dtVendor.Rows[0]["VENDORNAME"].ToString());//update VENDORNAME
            _dataReq01.ToList().ForEach(f => f.sVendorID = _dtVendor.Rows[0]["SVENDORID"].ToString());//update VENDORNAME
            _dataReq01.ToList().ForEach(f => f.nCompartAll = (sCarTypeID == "0" ? _dtVendor.Rows[0]["SLOT1"] + "" : _dtVendor.Rows[0]["SLOT2"] + ""));//update SLOT   
            _dataReq01.ToList().ForEach(f => f.nCapacityAll = (sCarTypeID == "0" ? _dtVendor.Rows[0]["CAP1"] + "" : _dtVendor.Rows[0]["CAP2"] + ""));//update CAP
            _dataReq01.ToList().ForEach(f => f.sCarCate = _dtVendor.Rows[0]["CARCATE_NAME"].ToString());//update CARCATE_NAME
            _dataReq01.ToList().ForEach(f => f.sCarCateID = _dtVendor.Rows[0]["CARCATE_ID"].ToString());//update CARCATE_NAME
            _dataReq01.ToList().ForEach(f => f.sStatusCar = _statusCar);//update sStatusCar
            _dataReq01.ToList().ForEach(f => f.nPricePan = GetPricePan());//update nPricePan
            _dataReq01.ToList().ForEach(f => f.nCapacityAll = !string.IsNullOrEmpty(_sTotalCap) ? _sTotalCap : "0");
            _dataReq01.ToList().ForEach(f => f.dWaterExpire = _dtVendor.Rows[0]["DWATEREXPIRE"].ToString());
            #endregion



            int Service = 0;
            switch (cboRequestType.Value + "")
            {
                case "01":
                    Service = int.Parse(GetServicePrice(_sTotalCap, _statusCar, cboRequestType.Value + "", "00001", _dtVendor.Rows[0]["CARCATE_ID"].ToString()));
                    int PricePan = int.Parse(GetPricePan());
                    int CountPan = int.Parse(GetCalculatePan(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL));

                    _dataReq01.ToList().ForEach(f => f.nServicePrice = Service + "");
                    _dataReq01.ToList().ForEach(f => f.nPricePan = PricePan + "");
                    _dataReq01.ToList().ForEach(f => f.nTotal = (Service + (PricePan * CountPan)) + "");
                    //เคลียลาเบลเพื่อไม่ให้ค่าของคันอื่นมาทับ
                    lblTotal.Text = "";
                    lblTotal.Text = (Service + (PricePan * CountPan)).ToString(SystemFunction.CheckFormatNuberic(0));
                    break;
                case "02":
                    Service = int.Parse(GetStdPrice(cboRequestType.Value + "", _sServiceAddSeal, ""));
                    lblTotal.Text = (Service).ToString(SystemFunction.CheckFormatNuberic(0));
                    _dataReq01.ToList().ForEach(f => f.nServicePrice = Service + "");
                    _dataReq01.ToList().ForEach(f => f.nTotal = Service + "");
                    break;
                case "03":
                    Service = int.Parse(GetStdPrice(cboRequestType.Value + "", _sServiceDoc, ""));
                    lblTotal.Text = (Service).ToString(SystemFunction.CheckFormatNuberic(0));
                    _dataReq01.ToList().ForEach(f => f.nServicePrice = Service + "");
                    _dataReq01.ToList().ForEach(f => f.nTotal = Service + "");
                    break;
                case "04":
                    Service = int.Parse(GetStdPrice(cboRequestType.Value + "", _sServicePaint, ""));
                    lblTotal.Text = (Service).ToString(SystemFunction.CheckFormatNuberic(0));
                    _dataReq01.ToList().ForEach(f => f.nServicePrice = Service + "");
                    _dataReq01.ToList().ForEach(f => f.nTotal = Service + "");
                    break;
            }

            StepShowReq();
        }
    }

    //เซ็ตความจุของรถ
    private void SetCapacityData(string STRUCKID)
    {
        ClearCaPacity();
        DataTable _dt = new DataTable();
        string _selectPan = "";


        _selectPan = @"SELECT 
 STRUCKID, NCOMPARTNO, NPANLEVEL
 ,NVL(NCAPACITY,0) AS NCAPACITY
FROM TTRUCK_COMPART
WHERE
NPANLEVEL = {0}
 AND STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";

        // statusPan 0 = แป้นเก่า 1 = เพิ่มแป้นใหม่ 
        DataTable dtPan1 = new DataTable();
        dtPan1 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "1"));
        DataTable dtPan2 = new DataTable();
        dtPan2 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "2"));
        DataTable dtPan3 = new DataTable();
        dtPan3 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "3"));

        #region Pan1
        if (dtPan1.Rows.Count > 0)
        {
            for (int i = 0; i < dtPan1.Rows.Count; i++)
            {
                switch (dtPan1.Rows[i]["NCOMPARTNO"].ToString())//ถ้าไม่มีค่าเป็น ถ้ามีค่าเป็น 0 = แป้นเก่า 1 = แป้นใหม่
                {
                    case "1":
                        cboCapacity11.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2":
                        cboCapacity12.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3":
                        cboCapacity13.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4":
                        cboCapacity14.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5":
                        cboCapacity15.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6":
                        cboCapacity16.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7":
                        cboCapacity17.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8":
                        cboCapacity18.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9":
                        cboCapacity19.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10":
                        cboCapacity110.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }
        #endregion

        #region Pan2
        if (dtPan2.Rows.Count > 0)
        {
            for (int i = 0; i < dtPan2.Rows.Count; i++)
            {
                switch (dtPan2.Rows[i]["NCOMPARTNO"].ToString())
                {
                    case "1":
                        cboCapacity21.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2":
                        cboCapacity22.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3":
                        cboCapacity23.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4":
                        cboCapacity24.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5":
                        cboCapacity25.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6":
                        cboCapacity26.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7":
                        cboCapacity27.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8":
                        cboCapacity28.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9":
                        cboCapacity29.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10":
                        cboCapacity210.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }

        #endregion

        #region Pan3
        if (dtPan3.Rows.Count > 0)
        {
            tbPan3.Style.Add("display", "block");
            for (int i = 0; i < dtPan3.Rows.Count; i++)
            {
                switch (dtPan3.Rows[i]["NCOMPARTNO"].ToString())
                {
                    case "1":
                        lblCapacity31.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2":
                        lblCapacity32.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3":
                        lblCapacity33.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4":
                        lblCapacity34.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5":
                        lblCapacity35.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6":
                        lblCapacity36.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7":
                        lblCapacity37.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8":
                        lblCapacity38.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9":
                        lblCapacity39.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10":
                        lblCapacity310.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }
        else
        {
            tbPan3.Style.Add("display", "none");
            lblCapacity31.Text = "";
            lblCapacity32.Text = "";
            lblCapacity33.Text = "";
            lblCapacity34.Text = "";
            lblCapacity35.Text = "";
            lblCapacity36.Text = "";
            lblCapacity37.Text = "";
            lblCapacity38.Text = "";
            lblCapacity39.Text = "";
            lblCapacity310.Text = "";
        }
        #endregion
    }

    //ข้อมูลราคาของการเพิ่มแป้น
    public string GetPricePan()
    {
        string _price = "0";

        string _qryPrice = @"SELECT P.SERVICECOST_ID, P.SERVICECHARGE
FROM  TBL_SERVICECOST P
WHERE P.SERVICE_ID='" + CommonFunction.ReplaceInjection("00002") + "' "
    + "AND P.ISACTIVE_FLAG='Y'";
        DataTable _dtPrice = new DataTable();
        _dtPrice = CommonFunction.Get_Data(_conn, _qryPrice);
        if (_dtPrice.Rows.Count > 0)
        {
            _price = _dtPrice.Rows[0]["SERVICECHARGE"].ToString();

        }
        return _price;
    }

    //เช็ครถเก่ารถใหม่
    private string CheckNewCarOrOldCar(string STRUCKID)
    {
        string Result = "";

        if (cboCause.Value == _sServiceNewCar)
        {
            Result = "1";
        }
        else
        {

            string Query = "SELECT STRUCKID,SCAR_NUM, SCERT_NO,SCHASIS FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
            DataTable dt = CommonFunction.Get_Data(_conn, Query);
            if (dt.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dt.Rows[0]["SCAR_NUM"] + ""))
                {
                    Result = "0";
                }
                else
                {
                    Result = "1";
                }

            }
            else
            {
                Result = "1";
            }
        }
        return Result;
    }

    //เช็คราคาการให้บริการของรถ
    public string GetServicePrice(string _capacity, string _carType, string _reqType, string _serviceID, string _carCate)
    {
        string _price = "0";
        string Codition = "";
        if (!string.IsNullOrEmpty(_capacity))
        {
            Codition = " AND (" + CommonFunction.ReplaceInjection(_capacity) + " BETWEEN P.START_CAPACITY AND  P.END_CAPACITY)";
        }
        else
        {
            return _price;
        }

        string _qryPrice = @"SELECT P.SERVICECOST_ID, P.SERVICECHARGE
                            FROM  TBL_SERVICECOST P
                            WHERE P.CAR_GRADE ='" + CommonFunction.ReplaceInjection(_carType) + "'"
                            + " AND P.REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + "' "
                            + " AND P.SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + "' "
                            + " AND P.ISACTIVE_FLAG='Y'"
                            + " AND P.CARCATE_ID ='" + CommonFunction.ReplaceInjection(_carCate) + "' " + Codition + "";

        DataTable _dtPrice = new DataTable();
        _dtPrice = CommonFunction.Get_Data(_conn, _qryPrice);
        if (_dtPrice.Rows.Count > 0)
        {
            _price = _dtPrice.Rows[0]["SERVICECHARGE"].ToString();
            _dataReq01.ToList().ForEach(f => f.nServicePrice = _dtPrice.Rows[0]["SERVICECHARGE"].ToString());//update _priceService
            _dataReq01.ToList().ForEach(f => f.sServicePriceID = _dtPrice.Rows[0]["SERVICECOST_ID"].ToString());//update SERVICECOST_ID
        }
        else
        {
            string _qryPrice2 = @"
            SELECT S.SERVICECHARGE   ,L.SERVICECOST_ID 
            FROM  
            (
                SELECT MAX(SERVICECHARGE)  as SERVICECHARGE FROM  TBL_SERVICECOST  WHERE CAR_GRADE ='" + CommonFunction.ReplaceInjection(_carType) + @"' AND REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + @"'  AND SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + @"'  AND ISACTIVE_FLAG='Y' AND CARCATE_ID ='" + CommonFunction.ReplaceInjection(_carCate) + @"'
            )S
            INNER  JOIN TBL_SERVICECOST l 
            ON s.SERVICECHARGE = l.SERVICECHARGE AND  L.CAR_GRADE = '" + CommonFunction.ReplaceInjection(_carType) + @"' AND  L.CARCATE_ID = '" + CommonFunction.ReplaceInjection(_carCate) + "' AND L.REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + "' AND L.SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + "'   AND L.ISACTIVE_FLAG='Y'";

            DataTable _dtPrice2 = new DataTable();
            _dtPrice2 = CommonFunction.Get_Data(_conn, _qryPrice2);

            if (_dtPrice2.Rows.Count > 0)
            {
                _price = _dtPrice2.Rows[0]["SERVICECHARGE"].ToString();
                _dataReq01.ToList().ForEach(f => f.nServicePrice = _dtPrice2.Rows[0]["SERVICECHARGE"].ToString());//update _priceService
                _dataReq01.ToList().ForEach(f => f.sServicePriceID = _dtPrice2.Rows[0]["SERVICECOST_ID"].ToString());//update SERVICECOST_ID
            }
        }

        return _price;
    }

    private string GetStdPrice(string sREQTypeID, string _sService, string p1) // ราคาของประเภทคำขอ
    {
        string nTemp = "0";
        string sql = @"SELECT * FROM TBL_SERVICECOST WHERE REQTYPE_ID = '{0}' AND {1} AND ISACTIVE_FLAG = 'Y' ";
        string scondition = !string.IsNullOrEmpty(_sService) ? "SERVICE_ID = '" + CommonFunction.ReplaceInjection(_sService) + "'" : "1=1";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(_conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQTypeID), scondition));
        DataRow dr = null;
        if (dt.Rows.Count > 0)
        {
            dr = dt.Rows[0];
            nTemp = dr["SERVICECHARGE"] + "";
        }

        return nTemp;
    }

    //คำนวนว่าแป้นสองมีกี่แป้น
    public string GetCalculatePan(string STRUCKID)
    {
        string Result = "0";

        string _selectPan = @"SELECT 
 STRUCKID, NCOMPARTNO, NPANLEVEL
 ,NCAPACITY
FROM TTRUCK_COMPART
WHERE
NPANLEVEL = {0}
 AND STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
        DataTable dtPan2 = new DataTable();
        dtPan2 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "2"));
        //เหตุผลที่ Count Row เนื่องจากมีบางคันที่รถเกินใส่ช่องไฟสลับกันทำให้เกิดช่องว่าง จะทำให้นับช่องเกินจริง
        Result = dtPan2.Rows.Count + "";

        _dataReq01.ToList().ForEach(f => f.nPanAll = Result);

        return Result;
    }

    public void SaveCapacityDetail(string _reqID, int _nCompart)
    {

        string _pan1 = "";
        string _pan2 = "";
        string _pan3 = "";


        //ที่ให้วนหมดเพราะต้องการให้ใส่ข้อมูลเข้าไปถูก อาจจะมีการเว้น่ช่องของข้อมูล
        for (int _compartNo = 1; _compartNo <= 10; _compartNo++)
        {
            switch (_compartNo)
            {
                case 1:

                    _pan1 = cboCapacity11.Text;
                    _pan2 = cboCapacity21.Text;
                    _pan3 = lblCapacity31.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }

                    break;
                case 2:
                    _pan1 = cboCapacity12.Text;
                    _pan2 = cboCapacity22.Text;
                    _pan3 = lblCapacity32.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 3:
                    _pan1 = cboCapacity13.Text;
                    _pan2 = cboCapacity23.Text;
                    _pan3 = lblCapacity33.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 4:
                    _pan1 = cboCapacity14.Text;
                    _pan2 = cboCapacity24.Text;
                    _pan3 = lblCapacity34.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 5:
                    _pan1 = cboCapacity15.Text;
                    _pan2 = cboCapacity25.Text;
                    _pan3 = lblCapacity35.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 6:
                    _pan1 = cboCapacity16.Text;
                    _pan2 = cboCapacity26.Text;
                    _pan3 = lblCapacity36.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 7:
                    _pan1 = cboCapacity17.Text;
                    _pan2 = cboCapacity27.Text;
                    _pan3 = lblCapacity37.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 8:
                    _pan1 = cboCapacity18.Text;
                    _pan2 = cboCapacity28.Text;
                    _pan3 = lblCapacity38.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 9:
                    _pan1 = cboCapacity19.Text;
                    _pan2 = cboCapacity29.Text;
                    _pan3 = lblCapacity39.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 10:
                    _pan1 = cboCapacity110.Text;
                    _pan2 = cboCapacity210.Text;
                    _pan3 = lblCapacity310.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
            }
        }

    }

    public void SaveTBLReqSlot(string _reqID, string _slot, string _pan, string _capacity, string _status)
    {
        if (_capacity.Contains(','))
        {
            _capacity = _capacity.Replace(",", "");
        }


        string Check = @"SELECT REQUEST_ID, SLOT_NO, LEVEL_NO,  CAPACITY, LEVEL_STATUS
FROM TBL_REQSLOT WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";

        int cCount = CommonFunction.Count_Value(_conn, Check);


        string _qryInsertDetail = "";

        string _qryUpdateDetail = @"UPDATE TBL_REQSLOT
SET   CAPACITY     = " + CommonFunction.ReplaceInjection(_capacity + "") + @"
WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";


        _qryInsertDetail = @" INSERT INTO TBL_REQSLOT VALUES ("
              + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
              + "," + CommonFunction.ReplaceInjection(_slot + "")
              + "," + CommonFunction.ReplaceInjection(_pan + "")
              + "," + CommonFunction.ReplaceInjection(!string.IsNullOrEmpty(_capacity + "") ? _capacity + "" : "null")
              + ",'" + CommonFunction.ReplaceInjection(_status + "") + "'"
              + ",''"
              + ")";
        using (OracleConnection con = new OracleConnection(_conn))
        {
            con.Open();
            if (cCount > 0)
            {
                using (OracleCommand cmd = new OracleCommand(_qryUpdateDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (OracleCommand cmd = new OracleCommand(_qryInsertDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public void SaveTBLReqItem(string _reqID, string _serviceID, string _nItem, string _nPrice)
    {
        if (string.IsNullOrEmpty(_nPrice))
        {
            _nPrice = "0";
            if (string.IsNullOrEmpty(_nItem))
            {
                _nItem = "0";
            }
        }

        string _nTotal = (int.Parse(_nItem) * int.Parse(_nPrice)) + "";
        string _qryInsertDetail = "";
        _qryInsertDetail = @" INSERT INTO TBL_REQUEST_ITEM (REQUEST_ID, SERVICE_ID, NITEM, 
   NPRICE) VALUES ("
              + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
              + ",'" + CommonFunction.ReplaceInjection(_serviceID) + "'"
              + "," + CommonFunction.ReplaceInjection(_nItem)
              + "," + CommonFunction.ReplaceInjection(_nTotal)

              + ")";
        using (OracleConnection con = new OracleConnection(_conn))
        {
            con.Open();
            using (OracleCommand cmd = new OracleCommand(_qryInsertDetail, con))
            {
                cmd.ExecuteNonQuery();
            }
        }
    }

    void ClearCodition()
    {
        cboCause.Text = "";
        //cboVendor.Text = "";
        cboCarregis.Text = "";

    }

    void ClearCaPacity()
    {
        cboCapacity11.Text = "";
        cboCapacity12.Text = "";
        cboCapacity13.Text = "";
        cboCapacity14.Text = "";
        cboCapacity15.Text = "";
        cboCapacity16.Text = "";
        cboCapacity17.Text = "";
        cboCapacity18.Text = "";
        cboCapacity19.Text = "";
        cboCapacity110.Text = "";
        cboCapacity21.Text = "";
        cboCapacity22.Text = "";
        cboCapacity23.Text = "";
        cboCapacity24.Text = "";
        cboCapacity25.Text = "";
        cboCapacity26.Text = "";
        cboCapacity27.Text = "";
        cboCapacity28.Text = "";
        cboCapacity29.Text = "";
        cboCapacity210.Text = "";
        lblCapacity31.Text = "";
        lblCapacity32.Text = "";
        lblCapacity33.Text = "";
        lblCapacity34.Text = "";
        lblCapacity35.Text = "";
        lblCapacity36.Text = "";
        lblCapacity37.Text = "";
        lblCapacity38.Text = "";
        lblCapacity39.Text = "";
        lblCapacity310.Text = "";
    }

    void StepShowReq()
    {
        trCause.Visible = true;
        cboCause.ClientEnabled = true;
        trHead.Visible = true;
        tbPan1.Style.Add("display", "block");
        tbPan2.Style.Add("display", "block");
        tbPan3.Style.Add("display", "block");
        tbTotal.Visible = true;
        switch (cboRequestType.Value + "")
        {
            case "01":

                break;
            case "02":
                trHead.Visible = false;
                tbPan1.Style.Add("display", "none");
                tbPan2.Style.Add("display", "none");
                tbPan3.Style.Add("display", "none");
                tbTotal.Visible = false;
                break;
            case "03":
                trCause.Visible = false;
                cboCause.ClientEnabled = false;
                trHead.Visible = false;
                tbPan1.Style.Add("display", "none");
                tbPan2.Style.Add("display", "none");
                tbPan3.Style.Add("display", "none");
                tbTotal.Visible = false;
                break;
            case "04":
                trCause.Visible = false;
                cboCause.ClientEnabled = false;
                trHead.Visible = false;
                tbPan1.Style.Add("display", "none");
                tbPan2.Style.Add("display", "none");
                tbPan3.Style.Add("display", "none");
                tbTotal.Visible = false;
                break;
        }
    }

    #region AutoComplete

    protected void cboCarregis_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        //string vehID = SystemFunction.GET_VENDORID(sUserID);

        string vehID = VENDOR_ID;
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsCarregis.SelectCommand = @"SELECT STRUCKID, head,tail,tail1  FROM (
          SELECT ROW_NUMBER()OVER(ORDER BY Head.STRUCKID) AS RN, Head.STRUCKID, 
                               Head.SHEADREGISTERNO as head,
                               Tail.SHEADREGISTERNO as tail,
                               CASE  WHEN  NVL(Tail.SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||Tail.SHEADREGISTERNO ELSE ' ' END as tail1,
                               HEAD.STRANSPORTID ,
                               NVL(TREQ.STRUCKID,'xxx') as Flag,
                                NVL(TREQ.STATUS_FLAG,'xxx') STATUS_FLAG,
                               CASE WHEN HEAD.SCARTYPEID = '0' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Head.DREGISTER,Head.DSIGNIN),sysdate))  
                               WHEN HEAD.SCARTYPEID = '3' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Tail.DREGISTER,Tail.DSIGNIN),sysdate))  
                               ELSE 0 END as NYEAR
                               FROM 
                                 TTRUCK Head-- ON C.SCONTRACTID = HEAD.SCONTRACTID AND TO_DATE(C.DEND ) - TO_DATE(sysdate)  >= 0
                               LEFT JOIN TTRUCK Tail
                               ON Head.STRAILERID = Tail.STRUCKID AND Head.STRANSPORTID = Tail.STRANSPORTID
                               LEFT JOIN (SELECT STRUCKID,STATUS_FLAG FROM TBL_REQUEST 
WHERE  REQUEST_DATE  IN (select max(NVL(REQUEST_DATE,SYSDATE)) as REQUEST_DATE from TBL_REQUEST group by STRUCKID)) TREQ
                               ON  Head.STRUCKID = TREQ.STRUCKID
                               WHERE Head.SCARTYPEID in (0,3) AND Head.VIEHICLE_TYPE IN('00','34') AND  HEAD.STRANSPORTID  = '" + vehID + @"' AND NVL(TREQ.STATUS_FLAG,'xxx')  IN ('11','99','xxx','10') 
         AND (Head.STRUCKID||Head.SHEADREGISTERNO||Tail.SHEADREGISTERNO  LIKE :fillter)) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsCarregis.SelectParameters.Clear();
        sdsCarregis.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsCarregis.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        sdsCarregis.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));

        //if(sdsCarregis.c)
        comboBox.DataSource = sdsCarregis;
        comboBox.DataBind();

    }

    protected void cboCarregis_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    #endregion

    #region Structure
    //public class STDataReq01
    //{
    //    public string sUserID { get; set; }
    //    public string sVendorID { get; set; }
    //    public string sVendorName { get; set; }
    //    public string sCarCateID { get; set; }
    //    public string sCarCate { get; set; }
    //    public string sCauseID { get; set; }
    //    public string sReqTypeID { get; set; }
    //    public string VEH_ID { get; set; }
    //    public string Tu_ID { get; set; }
    //    public string VEH_No { get; set; }
    //    public string Tu_No { get; set; }
    //    public string sVehChasis { get; set; }
    //    public string sTuChasis { get; set; }
    //    public string sStatusCar { get; set; }
    //    public string sCarTypeID { get; set; }
    //    public string dWaterExpire { get; set; }
    //    public string nPanAll { get; set; }
    //    public string nCompartAll { get; set; }
    //    public string nCapacityAll { get; set; }
    //    public string nTotal { get; set; }
    //    public string nServicePrice { get; set; }
    //    public string sServicePriceID { get; set; }
    //    public string nPricePan { get; set; }

    //}
    #endregion

}