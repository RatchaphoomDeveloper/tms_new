﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Evaluation_Kit_AddEdit.aspx.cs" Inherits="KPI_Evaluation_Kit_AddEdit" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server"> 
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="เพิ่ม - แก้ไข ชุดประเมิน KPI"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblFormName" runat="server" Text="ชื่อแบบฟอร์ม :&nbsp;"></asp:Label><label style="color:red">*</label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtFormName" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell style="text-align:right;">
                                                <asp:Label ID="lblDetail" runat="server" Text="รายละเอียดแบบประเมิน :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblStatus" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="ใช้งาน" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="ไม่ใช้งาน" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCancel_Click" />
                        </div>
                    </div>

                    <br />

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="Label1" runat="server" Text="เพิ่มหัวข้อประเมิน"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:GridView ID="dgvTopic" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" OnRowCommand="dgvTopic_RowCommand"
                                        CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowDeleting="dgvTopic_RowDeleting"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                        <Columns>
                                            <asp:TemplateField HeaderText = "ที่" ItemStyle-Width="60">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="แก้ไข">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <div class="col-sm-1">
                                                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/btnEdit.png"
                                                            Width="16px" Height="16px" Style="cursor: pointer" CommandName="EditData" />&nbsp;</div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ลบหัวข้อ">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <div class="col-sm-1">
                                                        <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png"
                                                            Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" />&nbsp;</div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FORMULA_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="METHOD_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOPIC_NAME" HeaderText="ชื่อหัวข้อประเมิน">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FORMULA_NAME" HeaderText="สูตรการคำนวณ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL1_START" HeaderText="1 (เริ่มต้น)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL1_END" HeaderText="1 (สิ้นสุด)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL2_START" HeaderText="2 (เริ่มต้น)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL2_END" HeaderText="2 (สิ้นสุด)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL3_START" HeaderText="3 (เริ่มต้น)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL3_END" HeaderText="3 (สิ้นสุด)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL4_START" HeaderText="4 (เริ่มต้น)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL4_END" HeaderText="4 (สิ้นสุด)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL5_START" HeaderText="5 (เริ่มต้น)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LEVEL5_END" HeaderText="5 (สิ้นสุด)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="WEIGHT" HeaderText="Weight">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    </asp:GridView>

                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="Label2" runat="server" Text="ชื่อหัวข้อประเมิน :&nbsp;"></asp:Label><label style="color:red">*</label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtTopicName" runat="server" CssClass="form-control" Width="350px"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblMethod" runat="server" Text="วิธีคำนวณผลรวม :&nbsp;"></asp:Label><label style="color:red">*</label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:DropDownList ID="ddlMethod" runat="server" CssClass="form-control" Width="350px"></asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblFormula" runat="server" Text="สูตรการคำนวณ :&nbsp;"></asp:Label><label style="color:red">*</label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:DropDownList ID="ddlFormula" runat="server" CssClass="form-control" Width="350px" AutoPostBack="true" OnSelectedIndexChanged="ddlFormula_SelectedIndexChanged"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblFrequency" runat="server" Text="ความถี่ :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtFrequency" runat="server" CssClass="form-control" Width="350px" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblLevel1Start" runat="server" Text="คำแนนช่วงที่ 1 (เริ่มต้น) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel1Start" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblLevel1End" runat="server" Text="คำแนนช่วงที่ 1 (สิ้นสุด) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel1End" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblLevel2Start" runat="server" Text="คำแนนช่วงที่ 2 (เริ่มต้น) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel2Start" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblLevel2End" runat="server" Text="คำแนนช่วงที่ 2 (สิ้นสุด) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel2End" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblLevel3Start" runat="server" Text="คำแนนช่วงที่ 3 (เริ่มต้น) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel3Start" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblLevel3End" runat="server" Text="คำแนนช่วงที่ 3 (สิ้นสุด) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel3End" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblLevel4Start" runat="server" Text="คำแนนช่วงที่ 4 (เริ่มต้น) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel4Start" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblLevel4End" runat="server" Text="คำแนนช่วงที่ 4 (สิ้นสุด) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel4End" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblLevel5Start" runat="server" Text="คำแนนช่วงที่ 5 (เริ่มต้น) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel5Start" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                <asp:Label ID="lblLevel5End" runat="server" Text="คำแนนช่วงที่ 5 (สิ้นสุด) :&nbsp;"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtLevel5End" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell style="width:18%; text-align:right">
                                                <asp:Label ID="lblWeight" runat="server" Text="Weight :&nbsp;"></asp:Label><label style="color:red">*</label>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control" Width="350px" style="text-align:center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell style="width:18%; text-align:right;">
                                                
                                            </asp:TableCell>
                                            <asp:TableCell style="width:30%;">
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdFormula" runat="server" Text="รายละเอียดสูตรการตัดคะแนน" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 210px" OnClick="cmdFormula_Click" />
                            <asp:Button ID="cmdAdd" runat="server" Text="เพิ่มหัวข้อประเมิน" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdAdd_Click" />
                            <asp:Button ID="cmdSaveEdit" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Visible="false" Style="width: 140px" OnClick="cmdSaveEdit_Click" />
                            <asp:Button ID="cmdCancelEdit" runat="server" Text="ยกเลิกแก้ไข" CssClass="btn btn-md btn-hover btn-danger" UseSubmitBehavior="false" Visible="false" Style="width: 140px" OnClick="cmdCancelEdit_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
   
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>