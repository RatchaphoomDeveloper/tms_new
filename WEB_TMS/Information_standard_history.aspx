﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="Information_standard_history.aspx.cs" Inherits="Information_standard_history" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="ประวัติการเปลี่ยนแปลงข้อมูลพนักงาน">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <table id="Table1" width="100%" runat="server">
                                            <tr>
                                                <td width="20%">จัดการมาตราฐานสำหรับรถขนส่ง :</td>
                                                <td width="80%">
                                                    <dx:ASPxComboBox runat="server" DataSourceID="sds" ID="cboTruckType" TextField="SCARTYPENAME"
                                                        ValueField="SCARTYPEID">
                                                        <ClientSideEvents SelectedIndexChanged="function (s, e) { xcpn.PerformCallback('chagevalidate'); }" />
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        SelectCommand="SELECT &quot;SCARTYPEID&quot;, &quot;SCARTYPENAME&quot; FROM &quot;TTRUCKTYPEINFORMATION&quot;">
                                                    </asp:SqlDataSource>
                                                    <dx:ASPxTextBox runat="server" ID="txtchkEditgrid" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" Width="100%" SkinID="_gvw">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="DateUpdate" Caption="วันที่อัพเดทข้อมูล" Width="16%"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                    <PropertiesTextEdit EncodeHtml="false">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="SUPDATE" Caption="ผู้อัพเดทข้อมูล" Width="20%"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Left">
                                                    <PropertiesTextEdit EncodeHtml="false">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="datachange" Caption="ข้อมูลที่เปลี่ยน" Width="20%"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <PropertiesTextEdit EncodeHtml="false">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="dataold" Caption="ข้อมูลเก่า" Width="22%" HeaderStyle-HorizontalAlign="Center">
                                                    <PropertiesTextEdit EncodeHtml="false">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="datanew" Caption="ข้อมูลใหม่" Width="22%" HeaderStyle-HorizontalAlign="Center">
                                                    <PropertiesTextEdit EncodeHtml="false">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
