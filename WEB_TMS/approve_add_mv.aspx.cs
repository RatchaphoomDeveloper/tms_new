﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using System.Globalization;
using System.Configuration;
using System.IO;
using DevExpress.Web.ASPxUploadControl;

public partial class approve_add_mv : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<DocApprove> lstDocStatus = new List<DocApprove>();
    private static string SUID = "";
    private static string sREQTYPE_ID = "";
    private static string sStatusID = "";
    private static string RK_FLAG = "";
    private static List<DOCUMENT> lstDocument = new List<DOCUMENT>();
    private static List<DOCUMENT> lstDocumentOther = new List<DOCUMENT>();
    private static string CARCATE_ID = "";
    private const string sPathSave = "UploadFile/RequestMV/{0}/File/{1}/{2}/";
    private const string sPathTemp = "UploadFile/RequestMV/{0}/Temp/{1}/{2}/";
    private static string _SysFileName = "", _sFileName = "", _sPathNow = "";
    private static string sDocOther = "0002"; //sDocBill = "0001",
    private static string sReq_ID = "";
    private static string VENDOR_ID = "";
    private static string sTruckID_CheckWater = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvwdoc.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwdoc_HtmlDataCellPrepared);

        if (!IsPostBack)
        {


            SUID = Session["UserID"] + "";
            string str = Request.QueryString["str"];
            string type = Request.QueryString["type"];
            //string reqid = Request.QueryString["reqid"];
            string stype = "";
            string sreqid = "";
            string[] strType;
            //คำขอทั่วไป หรือ อื่นๆๆ
            //if (!string.IsNullOrEmpty(type))
            //{
            //    strType = STCrypt.DecryptURL(type);
            //    if (strType[0] == "O")
            //    {
            //        //btnCancel.Enabled = true;

            //        sreqid = "1";
            //    }
            //    else
            //    {
            //      //  btnCancel.Enabled = false;

            //        sreqid = "0";
            //    }
            //}
            ////ประเภทวัดน้ำ หรือ ตีซีล
            //if (!string.IsNullOrEmpty(reqid))
            //{
            //    strType = STCrypt.DecryptURL(reqid);
            //    if (strType[0] == "01")
            //    {

            //        sreqid = "0";
            //    }
            //    else
            //    {

            //        sreqid = "1";
            //    }
            //}



            string[] strQuery;
            string ReqID = "";
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                ReqID = strQuery[0];
                Session["ReqID"] = strQuery[0];
                sReq_ID = strQuery[0];
                strType = strQuery[0].Split('/');
                sreqid = strType[1].Substring(0, 2);
                switch (sreqid)
                {
                    case "01": sreqid = "0";
                        break;
                    case "02": sreqid = "1";
                        break;
                    default: sreqid = "3";
                        break;
                }

            }
            else
            {

            }
            lstDocument.Clear();
            lstDocumentOther.Clear();

            Listdata(ReqID);

            //รหัส;typeหน้า;ความจุ;SEAL_HIT
            //00001;01;170000;1

            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            btnShowCalendar.ClientSideEvents.Click = "function(s,e){ CheckDateChage(); $('tr[id$=trVisible]').attr('class', 'displayTR');reloadCal(" + month + "," + year + ");}";
            Session["sCalendar"] = "" + ReqID + ";SR;" + Water(ReqID) + ";" + sreqid + "";

            //ListDescriptionData(ReqID);

            this.AssignAuthen();
            // Session["sCalendarDate"]  session สำหรับ return วันที่ calendar
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }

            if (!CanWrite)
            {
                btnShowCalendar.Enabled = false;
                btnEdit.Enabled = false;
                btnApprove.Enabled = false;
                btnAddOtherDoc.Enabled = false;      
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    //แอด Javascript เพื่อใช้เปิดดูไฟล์
    void gvwdoc_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("คอนโทรนดูเอกสาร"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePath = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePath") as ASPxTextBox;
            ASPxTextBox txtFileName = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileName") as ASPxTextBox;
            ASPxButton btnView = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnView") as ASPxButton;

            txtFilePath.ClientInstanceName = txtFilePath.ID + "_" + VisibleIndex;
            txtFileName.ClientInstanceName = txtFileName.ID + "_" + VisibleIndex;
            btnView.ClientInstanceName = btnView.ID + "_" + VisibleIndex;

            //Add Event
            btnView.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePath.ClientInstanceName + ".GetValue() +" + txtFileName.ClientInstanceName + ".GetValue());}";
        }

        if (e.DataColumn.Caption.Equals("ListRdl"))
        {
            int VisibleIndex = e.VisibleIndex;


            //ASPxRadioButtonList rblStatus = gvwdoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "rblStatus") as ASPxRadioButtonList;
            //string rblValue = !string.IsNullOrEmpty(gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "") ? gvwdoc.GetRowValues(VisibleIndex, "CONSIDER") + "" : "";

            //if (!string.IsNullOrEmpty(rblValue))
            //{
            //    if (rblValue == "Y")
            //    {
            //        rblStatus.SelectedIndex = 0;
            //    }
            //    else
            //    {
            //        rblStatus.SelectedIndex = 1;
            //    }

            //}

        }
    }

    //แอดสีFont
    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        //if (e.DataColumn.Caption == "ช่อง 1")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 2")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 3")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 4")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 5")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 6")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 7")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 8")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 9")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 10")
        //{
        //    string Text = e.CellValue + "";
        //    if (Text.Contains("*"))
        //    {
        //        e.Cell.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            //string SUID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
            string Req_ID = CommonFunction.ReplaceInjection(Session["ReqID"] + "");
            string Description = CommonFunction.ReplaceInjection(txtDescription.Text);
            string REMARK_STEP = CommonFunction.ReplaceInjection(txtSTATUSREQ_NAME.Text);

            string[] param = e.Parameter.Split(';');


            DateTime dTemp;
            string DateChange = "";
            decimal nntemp;
            decimal nAddition = 0;

            ////เช็คว่าเอกสารผ่านหมดหรือไม่
            //AddDataCheckDocument(Req_ID, "N");
            switch (txtCallbackType.Text)
            {
                case "Approve":

                    if (CheckAttachFile())
                    {
                        string MVUPDATECONTACT = "";

                        //ถ้าเป็น มว. ถึงจะมีเอกสารมาให้ใส่ และsaveลงเบส
                        if (RK_FLAG == "M")
                        {
                            Update_TBL_REQDOC(Req_ID);
                            //ในกรณีที่เป็น มว. จะต้องกรอกผู้ติดต่อ
                            MVUPDATECONTACT = "CONTACTNAME = '" + CommonFunction.ReplaceInjection(txtMVNAME.Text) + "',CONTACTPHONE = '" + CommonFunction.ReplaceInjection(txtMVPHONE.Text) + "',";
                        }


                        string UpdateRequest = @"UPDATE TBL_REQUEST
                                    SET    STATUS_FLAG = '" + CommonFunction.ReplaceInjection((sStatusID == "06" ? sStatusID : "04")) + @"',
                                           ACCEPT_FLAG = '{0}',
                                           ACCEPT_DATE = Sysdate,
                                           SERVICE_DATE =  TO_DATE('{2}','fmdd/mm/yyyy'),
                                           ACCEPT_BY = '{3}',
                                           EVENTMOVE = '{4}'," + MVUPDATECONTACT + @"
                                           EVENTMOVE_DESCRIPTION = '{5}'
                                    WHERE  REQUEST_ID   = '{1}'";

                        if (("" + lblCalendarDate.Text) != "" && ("" + Session["sCalendar"]) != "")
                        {
                            DateTime dBooking = DateTime.TryParse(("" + txtUserSelectDate.Text), out dTemp) ? dTemp : DateTime.Now;
                            DateChange = dBooking.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                        }


                        if (chkServiceDelivery.Checked)
                        {
                            nAddition = decimal.TryParse(CommonFunction.ReplaceInjection(txtDeliveryCharge.Text), out nntemp) ? nntemp : 0;
                        }
                        else
                        {
                            nAddition = 0;
                        }


                        //booking calendar
                        if (("" + txtUserSelectDate.Text) != ("" + txtCalendarDate.Text))
                        {

                            //รหัส;typeหน้า;ความจุ;SEAL_HIT
                            //00001;01;170000;1
                            string[] Para = (Session["sCalendar"] + "").Split(';');


                            DateTime dBooking = DateTime.TryParse(("" + txtCalendarDate.Text), out dTemp) ? dTemp : DateTime.Now;
                            DateChange = dBooking.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                            if (Para.Length >= 4)
                            {
                                if (Calendar.BookingCalendar(dBooking, Para[0], Para[1], Para[2], Para[3]) == 0)
                                {
                                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','มีรายการจองรถในวันที่ " + dBooking.ToString("d MMMM yyyy") + " เต็มแล้ว! กรุณาเลือกวันที่จองรถใหม่' );");
                                    return;
                                }
                            }


                        }
                        else
                        {
                            if (RK_FLAG == "M")
                            {
                                //รหัส;typeหน้า;ความจุ;SEAL_HIT
                                //00001;01;170000;1
                                string[] Para = (Session["sCalendar"] + "").Split(';');


                                DateTime dBooking = DateTime.TryParse(("" + txtCalendarDate.Text), out dTemp) ? dTemp : DateTime.Now;
                                DateChange = dBooking.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                                if (Para.Length >= 4)
                                {
                                    if (Calendar.BookingCalendar(dBooking, Para[0], Para[1], Para[2], Para[3]) == 0)
                                    {
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','มีรายการจองรถในวันที่ " + dBooking.ToString("d MMMM yyyy") + " เต็มแล้ว! กรุณาเลือกวันที่จองรถใหม่' );");
                                        return;
                                    }
                                }
                            }
                        }

                        //เพิ่ม การบริการเร่งด่วน   

                        string sSqlOpDelete = @"DELETE TBL_REQUEST_ITEM2 WHERE REQUEST_ID = '{0}' AND SERVICE_ID = '00016'";
                        AddTODB(string.Format(sSqlOpDelete, Req_ID));

                        if (chkServiceDelivery.Checked)
                        {

                            string sSqlOpInsert = @"INSERT INTO TBL_REQUEST_ITEM2 (
   CTYPE_ITEM, FLAGE_SERVICECANCEL, NITEM, 
   NPRICE, REQUEST_ID, SERVICE_ID) 
VALUES ('1','',1, {0},
 '{1}',
 '00016' )";
                            decimal nServiceDelivery = decimal.TryParse(CommonFunction.ReplaceInjection(txtDeliveryCharge.Text), out nntemp) ? nntemp : 0;

                            AddTODB(string.Format(sSqlOpInsert, nServiceDelivery, Req_ID));
                        }



                        AddTODB(string.Format(UpdateRequest, "Y", Req_ID, DateChange, SUID, (rblChangedate.Value + ""), MemoChageDate.Text));


                        SystemFunction.Add_To_TBL_REQREMARK(Req_ID, "Y", SystemFunction.GetRemark_WorkFlowRequest(9), "02", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(14), CommonFunction.ReplaceInjection(txtDescription.Text), "M");



                        //ส่งเมล์
                        if (SendMail(Req_ID, "0"))
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "',function(){window.location='approve_mv.aspx';});");
                        }
                        else // Error send mail
                        {
                            // CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้');");
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถส่ง E-mail ได้ในขณะนี้',function(){window.location='approve_mv.aspx';});");
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาแนบเอกสารให้ครบ');");
                        ListDoc(sReq_ID, "");
                        ListDocOther(sReq_ID, "");
                    }

                    break;
                case "Edit":

                    //string strMail = @"SELECT  SEMAIL FROM TUSER WHERE SUID = '" + SUID + "' ";
                    //DataTable dtmail = CommonFunction.Get_Data(conn, strMail);
                    //string Email = dtmail.Rows[0]["SEMAIL"] + "";


                    string sEditRequest = "UPDATE TBL_REQUEST SET STATUS_FLAG = '12', UPDATE_CODE = '{1}', UPDATE_DATE = Sysdate ,EVENTMOVE = '{2}',EVENTMOVE_DESCRIPTION = '{3}' WHERE REQUEST_ID = '{0}'";
                    AddTODB(string.Format(sEditRequest, Req_ID, SUID, (rblChangedate.Value + ""), MemoChageDate.Text));

                    SystemFunction.Add_To_TBL_REQREMARK(Req_ID, "Y", SystemFunction.GetRemark_WorkFlowRequest(3), "02", "S", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(10), CommonFunction.ReplaceInjection(txtDescription.Text), "M");

                    //ส่งเมล์
                    if (SendMail(Req_ID, "1"))
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "',function(){window.location='approve_mv.aspx';});");
                    }
                    else // Error send mail
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้');");
                    }


                    break;

                case "Back":
                    xcpn.JSProperties["cpRedirectTo"] = "approve_mv.aspx";
                    break;
            }
            txtCallbackType.Text = "";
            //Listdata(Req_ID);
            // ListDescriptionData(Req_ID);
        }
    }

    protected void xcpnService_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string SUID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
            string Req_ID = CommonFunction.ReplaceInjection(Session["ReqID"] + "");

            string[] param = e.Parameter.Split(';');
            if (param[0] == "service")
            {
                if (chkServiceDelivery.Checked)
                {
                    txtDeliveryCharge.Text = CommonFunction.Get_Value(conn, "SELECT MAX(SERVICECHARGE) FROM TBL_SERVICECOST WHERE SERVICE_ID = '00016'");
                }
                else
                {
                    txtDeliveryCharge.Text = "";
                }
            }
        }
    }

    private void Listdata(string Req_ID)
    {
        #region DATA#1 ข้อมูลพิ้นฐาน และ รายละเอียดค่าธรรมเนียม

        //        string QueryReq = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.STRUCKID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
        //                        ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE , CASE WHEN NVL(TREQ.ACCEPT_FLAG,'xxx') = 'Y' THEN  TREQ.SERVICE_DATE ELSE TREQ.APPOINTMENT_DATE END  APPOINTMENT_DATE ,NVL(TVEN.SABBREVIATION,TRUCK.SOWNER_NAME) as SABBREVIATION
        //                        ,TSTATUS.STATUSREQ_NAME,TREQ.REQTYPE_ID,TREQTYPE.REQTYPE_NAME  ,TCAR.CARCATE_NAME
        //                        ,serPRICE.NPRICE   as SERVCHAGE_PRICE
        //                        ,addPRICE.NPRICE  as ADDITIONAL_PRICE
        //                        ,TREQ.TOTLE_SERVCHAGE,TSTATUS.STATUSREQ_ID,TREQCAUSE.CAUSE_NAME,TREQ.TOTLE_CAP,TREQ.TOTLE_SLOT,TREQ.APPOINTMENT_BY,TREQ.CONTACTNAME,TREQ.CONTACTPHONE,TREQ.CARCATE_ID,TREQ.RK_FLAG
        //                        FROM TBL_Request TREQ
        //                        LEFT JOIN 
        //                        (
        //                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR 
        //                        ) TVEN
        //                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
        //                        LEFT JOIN TBL_REQTYPE TREQTYPE
        //                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
        //                        LEFT JOIN TBL_CAUSE TREQCAUSE
        //                       ON TREQCAUSE.CAUSE_ID = TREQ.CAUSE_ID
        //                        LEFT JOIN TBL_STATUSREQ TSTATUS
        //                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
        //                        LEFT JOIN TBL_CARCATE TCAR
        //                        ON TCAR.CARCATE_ID = TREQ.CARCATE_ID
        //                       LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID = '00002') addPRICE
        //                       ON addPRICE.REQUEST_ID = TREQ.REQUEST_ID
        //                          LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID <> '00002') serPRICE
        //                        ON serPRICE.REQUEST_ID = TREQ.REQUEST_ID
        //                        --ใช้หัวของ TBL_REQUEST จอย
        //                        LEFT JOIN  
        //                        (
        //                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,SOWNER_NAME,
        //                            CASE  SCARTYPEID
        //                            WHEN 0 THEN SHEADREGISTERNO
        //                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
        //                            ELSE ''
        //                            END REGISTERNO
        //                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
        //                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
        //                            FROM
        //                            (
        //                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
        //                             ) TRUCK
        //                             LEFT JOIN 
        //                             (
        //                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
        //                             )WATER
        //                            ON  WATER.SHEADID = TRUCK.STRUCKID
        //                        )TRUCK
        //                        ON TRUCK.STRUCKID = TREQ.VEH_ID
        //                        WHERE 1=1 AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";
        string QueryReq = @"SELECT   TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.STRUCKID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE , CASE WHEN NVL(TREQ.ACCEPT_FLAG,'xxx') = 'Y' THEN  TREQ.SERVICE_DATE ELSE TREQ.APPOINTMENT_DATE END  APPOINTMENT_DATE ,NVL(TVEN.SABBREVIATION,TRUCK.SOWNER_NAME) as SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQ.REQTYPE_ID,TREQTYPE.REQTYPE_NAME  ,TCAR.CARCATE_NAME
                        ,serPRICE.NPRICE   as SERVCHAGE_PRICE
                        ,addPRICE.NPRICE  as ADDITIONAL_PRICE
                        ,TREQ.TOTLE_SERVCHAGE,TSTATUS.STATUSREQ_ID,TREQCAUSE.CAUSE_NAME,TREQ.TOTLE_CAP,TREQ.TOTLE_SLOT,TREQ.APPOINTMENT_BY,TREQ.CONTACTNAME,TREQ.CONTACTPHONE,TREQ.CARCATE_ID,TREQ.RK_FLAG,TRUCK.SSTRUCK
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR 
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_CAUSE TREQCAUSE
                       ON TREQCAUSE.CAUSE_ID = TREQ.CAUSE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        LEFT JOIN TBL_CARCATE TCAR
                        ON TCAR.CARCATE_ID = TREQ.CARCATE_ID
                       LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID = '00002') addPRICE
                       ON addPRICE.REQUEST_ID = TREQ.REQUEST_ID
                          LEFT JOIN (SELECT REQUEST_ID, SERVICE_ID, NITEM,  NPRICE FROM TBL_REQUEST_ITEM WHERE SERVICE_ID <> '00002') serPRICE
                        ON serPRICE.REQUEST_ID = TREQ.REQUEST_ID
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT TRUCK.STRUCKID,TRUCK.SHEADREGISTERNO,TRUCK.STRAILERREGISTERNO,NVL(TRUCK.SOWNER_NAME,WATER.SOWNER_NAME) as SOWNER_NAME,
                            CASE  TRUCK.SCARTYPEID  WHEN 0 THEN TRUCK.STRUCKID WHEN 3 THEN WATER.STRUCKID ELSE '' END SSTRUCK,
                            CASE  TRUCK.SCARTYPEID WHEN 0 THEN TRUCK.SHEADREGISTERNO WHEN 3 THEN TRUCK.SHEADREGISTERNO|| '/' || TRUCK.STRAILERREGISTERNO  ELSE ''  END REGISTERNO ,
                            CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE  WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                             FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT STRUCKID,SHEADID, DWATEREXPIRE,SOWNER_NAME FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID
                        WHERE 1=1 AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";
        DataTable dt = CommonFunction.Get_Data(conn, QueryReq);
        if (dt.Rows.Count > 0)
        {
           
            sREQTYPE_ID = dt.Rows[0]["REQTYPE_ID"] + "";
            sStatusID = dt.Rows[0]["STATUS_FLAG"] + "";
            VENDOR_ID = dt.Rows[0]["VENDOR_ID"] + "";

            //เซ็ต SCAR_NUM
            GenScarNum(dt.Rows[0]["SSTRUCK"] + "");

           

            CARCATE_ID = dt.Rows[0]["CARCATE_ID"] + "";
            RK_FLAG = dt.Rows[0]["RK_FLAG"] + "";

            if (RK_FLAG == "M")
            {
                ListDoc(sReq_ID, "Y");
                ListDocOther(sReq_ID, "Y");
                trshowdoc1.Visible = false;
                trdoc2.Visible = true;
                trdoc3.Visible = true;
                txtRkFlag.Text = RK_FLAG;
                btnEdit.ClientVisible = false;
                txtMVNAME.ClientVisible = true;
                txtMVPHONE.ClientVisible = true;
                txtName.ClientVisible = false;
                txtPhone.ClientVisible = false;
                txtMVNAME.Text = dt.Rows[0]["CONTACTNAME"] + "";
                txtMVPHONE.Text = dt.Rows[0]["CONTACTPHONE"] + "";
            }
            else
            {
                trshowdoc1.Visible = true;
                trdoc2.Visible = false;
                trdoc3.Visible = false;
                txtRkFlag.Text = RK_FLAG;
                //btnEdit.ClientVisible = true;
                //เซ็ต Enable เพื่อไม่ให้ Validate ทำงาน
                txtMVNAME.ClientEnabled = false;
                txtMVPHONE.ClientEnabled = false;
                //
                txtMVNAME.ClientVisible = false;
                txtMVPHONE.ClientVisible = false;
                txtName.ClientVisible = true;
                txtPhone.ClientVisible = true;
                txtName.Text = dt.Rows[0]["CONTACTNAME"] + "";
                txtPhone.Text = dt.Rows[0]["CONTACTPHONE"] + "";
            }

            if (sStatusID == "10")
            {
                btnApprove.ClientVisible = false;
                btnEdit.ClientVisible = false;
            }
            else
            {
                btnApprove.ClientVisible = true;
                btnEdit.ClientVisible = true;
            }

            lbldatereq.Text = ChecknullDate(dt.Rows[0]["REQUEST_DATE"] + "");
            lblDateexp.Text = ChecknullDate(dt.Rows[0]["DWATEREXPIRE"] + "");
            lblCause.Text = Checknull(dt.Rows[0]["CAUSE_NAME"] + "");
            lblReq.Text = Checknull(dt.Rows[0]["REQTYPE_NAME"] + "");
            lblCar.Text = Checknull(dt.Rows[0]["CARCATE_NAME"] + "");
            lblRegis.Text = dt.Rows[0]["VEH_NO"] + "" + (!string.IsNullOrEmpty(dt.Rows[0]["TU_NO"] + "") ? " / " + dt.Rows[0]["TU_NO"] + "" : "");
            lblVendorname.Text = Checknull(dt.Rows[0]["SABBREVIATION"] + "");

            //lblService.Text = CheckNum(dt.Rows[0]["SERVCHAGE_PRICE"] + "");
            //lblAddition.Text = CheckNum(dt.Rows[0]["ADDITIONAL_PRICE"] + "");
            //lblTotal.Text = CheckNum(dt.Rows[0]["TOTLE_SERVCHAGE"] + "");

            //ใช้ในการบอกวันที่นัดหมาย ที่ใช้ APPOINTMENT_DATE เพราะในกรณีที่เป็น มว. ร้องขอจะยังไม่มี ServiceDate
            lblCalendarDate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            txtCalendarDate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            txtDateOld.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            //เซ็ตเพื่อใช้เช็คว่า ที่ User เลือกคือวันอะไร
            txtUserSelectDate.Text = ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            //เซ็ทเพื่อนำไปใช้เก็บค่าลง TBL_REMARk
            txtSTATUSREQ_NAME.Text = dt.Rows[0]["STATUSREQ_ID"] + "";

            lblCompartTotal.Text = dt.Rows[0]["TOTLE_SLOT"] + "";
            lblCapacityTotal.Text = !string.IsNullOrEmpty(dt.Rows[0]["TOTLE_CAP"] + "") ? int.Parse(dt.Rows[0]["TOTLE_CAP"] + "").ToString("#,###,###,###") : "-";
            lblCap.Text = !string.IsNullOrEmpty(dt.Rows[0]["TOTLE_CAP"] + "") ? int.Parse(dt.Rows[0]["TOTLE_CAP"] + "").ToString("#,###,###,###") + " ลิตร" : "-"; ;
            lblRk.Text += ChecknullDate(dt.Rows[0]["APPOINTMENT_DATE"] + "");
            #region ชื่อ
            string strName = @"SELECT 
 REQ.REQUEST_ID,SUSER.SEMAIL,SUSER.SFIRSTNAME||' '||SUSER.SLASTNAME as SNAME
FROM 
(
    SELECT REQUEST_ID, APPOINTMENT_BY as USERID FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'
) REQ
INNER JOIN TUSER SUSER
ON REQ.USERID = SUSER.SUID";

            DataTable dtName = CommonFunction.Get_Data(conn, strName);
            if (dtName.Rows.Count > 0)
            {
                lblRk.Text += " - " + dtName.Rows[0]["SNAME"] + "";
            }

            #endregion


            DataTable dtt = CommonFunction.Get_Data(conn, string.Format(@"SELECT NPRICE FROM TBL_REQUEST_ITEM2 WHERE REQUEST_ID = '{0}' AND SERVICE_ID = '00016'", CommonFunction.ReplaceInjection(Req_ID)));

            if (dtt.Rows.Count > 0)
            {
                chkServiceDelivery.Checked = true;
                txtDeliveryCharge.Text = dtt.Rows[0]["NPRICE"] + "";
            }
            else
            {
                chkServiceDelivery.Checked = false;
                txtDeliveryCharge.Text = "";
            }

        }
        #endregion

        #region DATA#2 ข้อมูลความจุรายการเพิ่มแป้น

        #region DATA#2 ข้อมูลความจุรายการเพิ่มแป้น

        DataTable _dtCompacity = new DataTable();

        _dtCompacity = SystemFunction.LISTCAPACITY(Req_ID);
        if (_dtCompacity.Rows.Count > 0)
        {
            gvw.DataSource = _dtCompacity;
            gvw.DataBind();

            #region เช็คว่ามีแป้นเท่าไหร่
            string _chkPan3 = @"SELECT MAX(R.LEVEL_NO)
FROM TBL_REQSLOT r  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"' AND NVL(STATUS_PAN3,'xxx') <> '0'";
            DataTable _dtChkPan3 = new DataTable();

            _dtChkPan3 = CommonFunction.Get_Data(conn, _chkPan3);
            if (_dtChkPan3.Rows.Count > 0)
            {
                string _nPan = _dtChkPan3.Rows[0][0].ToString();
                if (_nPan != "3")
                {
                    //ถ้าไม่มีแป้นสามให้แสดง 2 แป้น
                    gvw.SettingsPager.PageSize = 2;
                }
            }
            #endregion

        }




        #endregion




        #endregion

        string CheckTypeDoc = "";

        #region DATA#3 เอกสาร
        //        string QueryDoc = @"SELECT TBL_REQDOC.REQUEST_ID, TBL_REQDOC.DOC_ID, TBL_REQDOC.DOC_TYPE, TBL_REQDOC.DOC_ITEM, TBL_REQDOC.FILE_NAME, TBL_REQDOC.FILE_SYSNAME, 
        //TBL_REQDOC.FILE_PATH, TBL_REQDOC.CONSIDER,TBL_DOCTYPE.DOC_DESCRIPTION
        //FROM TBL_REQDOC
        //LEFT JOIN TBL_DOCTYPE
        //ON TBL_REQDOC.DOC_TYPE = TBL_DOCTYPE.DOCTYPE_ID
        //WHERE NVL(TBL_REQDOC.FILE_NAME,'xxx') <> 'xxx' AND NVL(TBL_REQDOC.FILE_SYSNAME,'xxx') <> 'xxx' AND 
        //NVL(TBL_REQDOC.FILE_PATH,'xxx') <> 'xxx' AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";
        string QueryDoc = @"SELECT TBL_REQDOC.REQUEST_ID, TBL_REQDOC.DOC_ID, TBL_REQDOC.DOC_TYPE, TBL_REQDOC.DOC_ITEM, TBL_REQDOC.FILE_NAME, TBL_REQDOC.FILE_SYSNAME, 
TBL_REQDOC.FILE_PATH,TBL_REQDOC.CONSIDER,TBL_DOCTYPE.DOC_DESCRIPTION,TBL_DOCTYPE.CATTACH_DOC01,TBL_DOCTYPE.CATTACH_DOC02,TBL_DOCTYPE.CATTACH_DOC03,TBL_DOCTYPE.CATTACH_DOC04,TBL_DOCTYPE.CDYNAMIC
FROM TBL_REQDOC
LEFT JOIN TBL_DOCTYPE
ON TBL_REQDOC.DOC_TYPE = TBL_DOCTYPE.DOCTYPE_ID
WHERE NVL(TBL_REQDOC.FILE_NAME,'xxx') <> 'xxx' AND NVL(TBL_REQDOC.FILE_SYSNAME,'xxx') <> 'xxx' AND 
NVL(TBL_REQDOC.FILE_PATH,'xxx') <> 'xxx' AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'  " + CheckTypeDoc + @"
ORDER BY  TBL_DOCTYPE.CDYNAMIC DESC,TBL_REQDOC.DOC_TYPE ASC ,TBL_REQDOC.DOC_ID ASC";

        DataTable dtDoc = CommonFunction.Get_Data(conn, QueryDoc);
        if (dtDoc.Rows.Count > 0)
        {
            gvwdoc.ClientVisible = true;
            gvwdoc.DataSource = dtDoc;
            gvwdoc.DataBind();
        }
        else
        {
            gvwdoc.ClientVisible = false;
        }

        #endregion

        #region DATA#4 หมายเหตุ

        ListDescriptionData(Req_ID, Session["UserID"] + "");
        #endregion

        #region DATA#5 ค่าบริการ

        DataTable dtService = SystemFunction.List_SERVICE(Req_ID, "");
        if (dtService.Rows.Count > 0)
        {
            gvwService.DataSource = dtService;
        }
        gvwService.DataBind();

        #endregion
    }

    private void ListDescriptionData(string Req_ID, string USERID)
    {


        DataTable dt = SystemFunction.List_TBL_REQREMARK(Req_ID, "");
        if (dt.Rows.Count > 0)
        {

            gvwdescription.DataSource = dt;
            gvwdescription.DataBind();
            //txtDescription.Text = dt.Select().Rows[0]["REMARKS"] + "";
        }
        else
        {

        }

    }

    private string Checknull(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = Data;
        }
        else
        {
            Result = "-";
        }


        return Result;
    }

    private string ChecknullDate(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = DateTime.Parse(Data).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));


        }
        else
        {
            Result = "-";
        }


        return Result;
    }

    private string CheckNum(string Data)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(Data))
        {
            if (Data != "0")
            {
                decimal num = decimal.Parse(Data);
                Result = num.ToString("#,###,###,###,###,###") + " บาท";
            }
            else
            {
                Result = "0 บาท";
            }
        }
        else
        {
            Result = "-";
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    //    private string Gen_ID()
    //    {
    //        string Result = "";

    //        string strsql = @"SELECT REMARK_ID, REQUEST_ID, REMARK_DATE, 
    //   REMARK_STEP, REMARKS, REMARK_STATUS, 
    //   REMARK_BY, REPORT2VEND
    //FROM TBL_REQREMARK ORDER BY REMARK_ID DESC";

    //        string ID = "SELECT REMARK_ID FROM (SELECT REMARK_ID FROM TBL_REQREMARK ORDER BY REMARK_ID DESC) WHERE ROWNUM <= 1";

    //        DataTable dt = CommonFunction.Get_Data(conn, strsql);




    //        if (dt.Rows.Count > 0)
    //        {

    //            string sID = dt.Rows[0]["REMARK_ID"] + "";
    //            int nID = int.Parse(sID) + 1;

    //            string NewID = nID.ToString();


    //            Result = NewID;
    //        }
    //        else
    //        {
    //            Result = "1";
    //        }

    //        return Result;
    //    }

    private void AddDataCheckDocument(string Req_ID, string ToDB)
    {
        lstDocStatus.Clear();

        int gvwcount = gvwdoc.VisibleRowCount;

        string UpdateCheckFile = @"UPDATE TBL_REQDOC
                                SET    
                                       CONSIDER     = '{2}'
                                WHERE  REQUEST_ID   = '{0}'
                                AND    DOC_ID       = {1}";
        for (int i = 0; i < gvwcount; i++)
        {
            string DOC_ID = CommonFunction.ReplaceInjection(gvwdoc.GetRowValues(i, "DOC_ID") + "");
            ASPxRadioButtonList rblStatus = gvwdoc.FindRowCellTemplateControl(i, null, "rblStatus") as ASPxRadioButtonList;
            string rblValue = rblStatus.Value + "" == "" ? "" : (rblStatus.Value + "" == "1" ? "Y" : "N");

            lstDocStatus.Add(new DocApprove
            {
                FlageDocApprove = rblValue
            });
            if (ToDB == "Y")
            {
                AddTODB(string.Format(UpdateCheckFile, CommonFunction.ReplaceInjection(Req_ID), DOC_ID, rblValue));
            }
        }
    }

    private string Water(string Req_ID)
    {
        string result = "0";
        string sqlTruckID = @"SELECT 
   REQUEST_ID, REQTYPE_ID,NVL(TU_ID,VEH_ID) STRUCKID , REQUEST_OTHER_FLAG
FROM TBL_REQUEST
WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";

        DataTable dt = CommonFunction.Get_Data(conn, sqlTruckID);

        if (dt.Rows.Count > 0)
        {

            string sql = @"SELECT  SUM(CAP) as NCAPTOTAL
FROM 
(SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) as CAP
FROM TTRUCK_COMPART 
WHERE STRUCKID ='" + CommonFunction.ReplaceInjection(dt.Rows[0]["STRUCKID"] + "") + @"'
GROUP BY STRUCKID,NCOMPARTNO)";

            DataTable dtCapacity = CommonFunction.Get_Data(conn, sql);

            result = dtCapacity.Rows[0]["NCAPTOTAL"] + "";
        }

        return result;
    }

    private bool SendMail(string Req_ID, string Type)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
               , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
               , sSubject = (Type == "0" ? "แจ้งยืนยันกำหนดวันนำรถบรรทุกน้ำมันเข้าตรวจสอบ" + lblReq.Text : "แจ้งปรับแก้ไขเนื่องจากมีข้อผิดพลาดของรถ" + lblRegis.Text);

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = SystemFunction.GetUserMailForSend(SUID, sStatusID, VENDOR_ID, "M");
        }

        //ชื่อเป็นค่าว่าง
        if (string.IsNullOrEmpty(MemoChageDate.Text))
        {
            #region html
            //{0}ประเภทคำขอ {1}ทะเบียนรถ {2}วันที่นัดหมาย
            sHTML = (Type == "0" ? string.Format(SystemFunction.Form_Email("3", SUID), lblReq.Text, lblRegis.Text, txtCalendarDate.Text) : txtDescription.Text);
            #endregion
        }
        else
        {
            sSubject = "แจ้งเปลี่ยนกำหนดวันนำรถบรรทุกน้ำมันเข้าตรวจสอบ" + lblReq.Text;
            sHTML = string.Format(SystemFunction.Form_Email("4", SUID), lblReq.Text, lblRegis.Text, txtUserSelectDate.Text, txtCalendarDate.Text);
        }


        sMsg = sHTML;

        OracleConnection con = new OracleConnection(conn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }

    #region Upload

    //สร้างโฟลเดอร์เพื่อไว้เก็บไฟล์
    private void CreateFolder(string Path)
    {
        try
        {

            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + Path.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + Path.Replace("/", "\\"));
            }
            #endregion
        }
        catch (Exception e)
        {
            // Console.WriteLine("The process failed: {0}", e.ToString());
        }
    }

    protected bool CheckTypeFileUpLoad(UploadedFile ful)
    {
        string[] nameFile = ful.FileName.Split('.');
        if (nameFile[nameFile.Length - 1].ToLower() == "pdf" || nameFile[nameFile.Length - 1].ToLower() == "jpg" || nameFile[nameFile.Length - 1].ToLower() == "jpeg" || nameFile[nameFile.Length - 1].ToLower() == "bmp" || nameFile[nameFile.Length - 1].ToLower() == "gif" || nameFile[nameFile.Length - 1].ToLower() == "png" || nameFile[nameFile.Length - 1].ToLower() == "doc" || nameFile[nameFile.Length - 1].ToLower() == "docx" || nameFile[nameFile.Length - 1].ToLower() == "xls" || nameFile[nameFile.Length - 1].ToLower() == "xlsx")
        {
            return true;
        }
        else
        {
            //SetBodyEventOnLoad("jAlertBox('" + SystemFunction.Msg_HeadAlert() + "','" + (string.Format(Resources.CommonResource.Msg_UploadFileOnly, "PDF")) + "')");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", string.Format("ShowZErrorDialog('{1}','{0}');", Resources.CommonResource.Msg_Alert_Title_Error, (string.Format(Resources.CommonResource.Msg_UploadFileOnly, "PDF"))), true);
            return false;
        }
    }

    protected void uclBill_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string Name = "";
        switch (sREQTYPE_ID)
        {
            case "01":
                Name = "Measure";
                break;
            case "02":
                Name = "AddSeal";
                break;
            case "03":
                Name = "Paint";
                break;
            case "04":
                Name = "Doc";
                break;
        }

        string Path = string.Format(sPathTemp, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), SUID);
        CreateFolder(Path);
        string[] FILETYPE = e.UploadedFile.FileName.Split('.');

        //ฟังชั่นเช็คประเภทรูปและขนาดในการอับโหลด
        string AlertText = SystemFunction.DOC_CheckSize(FILETYPE[FILETYPE.Length - 1], e.UploadedFile.FileBytes.Length);
        if (CheckTypeFileUpLoad(e.UploadedFile))
        {
            if (string.IsNullOrEmpty(AlertText))
            {
                // สร้างชื่อไฟล์
                string FILENAME = SystemFunction.SplitFileName(e.UploadedFile.FileName);
                //string[] FILETYPE = e.UploadedFile.FileName.Split('.');
                string SYSFILENAME = "REQ_" + @"_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + "." + FILETYPE[FILETYPE.Length - 1];

                //เก็บเอกสารลง Temp
                e.UploadedFile.SaveAs(Server.MapPath(Path + "/" + SYSFILENAME));
                if (File.Exists(Server.MapPath(Path + "/" + SYSFILENAME)))
                {
                    _SysFileName = SYSFILENAME;
                    _sFileName = FILENAME;
                    _sPathNow = Path;

                }
                else
                {

                }
                e.CallbackData = "";
            }
            else
            {
                e.CallbackData = AlertText;
            }
        }
        else
        {
            e.CallbackData = "เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น";
        }
    }

    protected void gvwBill_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] paras = e.Args[0].Split(';');
                int index = int.Parse(paras[1] + "");
                string DOCTYPE_ID = gvwBill.GetRowValues(index, "DOCTYPE_ID") + "";
                switch (paras[0])
                {
                    case "UPLOAD":

                        var doc = lstDocument.Where(d => d.DOCTYPE_ID == DOCTYPE_ID).FirstOrDefault();
                        if (doc != null)
                        {

                            doc.FILE_NAME = _sFileName;
                            doc.FILE_PATH = _sPathNow;
                            doc.FILE_SYSNAME = _SysFileName;
                            doc.SVISIBLE = !string.IsNullOrEmpty(_sFileName) ? "Y" : "N";
                            doc.CONSIDER = "";
                            doc.OPENFILE = _sPathNow + _SysFileName;
                            doc.REQUEST_ID = sReq_ID;
                            doc.DOC_ID = "1";
                        }


                        ListDoc("", "N");


                        break;

                    case "DEL":

                        var del = lstDocument.Where(d => d.DOCTYPE_ID == DOCTYPE_ID).FirstOrDefault();
                        if (del != null)
                        {
                            del.FILE_NAME = "";
                            del.FILE_PATH = "";
                            del.FILE_SYSNAME = "";
                            del.REQUEST_ID = sReq_ID;
                            del.SVISIBLE = "N";
                        }
                        ListDoc("", "N");


                        break;
                }
                break;
        }
    }

    private void ListDoc(string REQUEST_ID, string Getdatasql)
    {

        string Codition = "";

        switch (sREQTYPE_ID)
        {
            case "01": Codition = " AND MS.DOC_01 = 'Y'";
                break;
            case "02": Codition = " AND MS.DOC_02 = 'Y'";
                break;
            case "03": Codition = " AND MS.DOC_03 = 'Y'";
                break;
            case "04": Codition = " AND MS.DOC_04 = 'Y'";
                break;
        }

        Codition += SystemFunction.DOC_CLOSEWORK(sStatusID, REQUEST_ID);

        string Query = @"SELECT  MS.DOCTYPE_ID, MS.DOC_DESCRIPTION,MS.DOC_01,  MS.DOC_02, MS.DOC_03, MS.DOC_04, MS.ISACTIVE_FLAG, MS.DESCRIPTION,MS.CDYNAMIC
,MS.CDYNAMIC,  MS.CATTACH_DOC01, MS.CATTACH_DOC02, MS.CATTACH_DOC03, MS.CATTACH_DOC04
, DOC.FILE_NAME, DOC.FILE_SYSNAME, DOC.FILE_PATH, DOC.CONSIDER,CASE  WHEN REQ.STATUS_FLAG IN ('09','02') THEN 'Y' ELSE 'N' END as SALLOWEDIT,
CASE WHEN NVL(DOC.FILE_NAME,'xxx') <> 'xxx' THEN 'Y' ELSE 'N' END as SVISIBLE ,DOC.DOC_ID,DOC.DOC_ITEM
FROM TBL_DOCTYPE MS
LEFT JOIN 
(
    SELECT REQUEST_ID, DOC_ID, DOC_TYPE,  DOC_ITEM, FILE_NAME, FILE_SYSNAME, FILE_PATH, CONSIDER
    FROM TBL_REQDOC 
    WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQUEST_ID) + @"'
)DOC
ON MS.DOCTYPE_ID = DOC.DOC_TYPE
LEFT JOIN TBL_REQUEST REQ
ON DOC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 AND MS.CDYNAMIC = 'Y' AND MS.ISACTIVE_FLAG = 'Y' " + Codition + @" AND  MS.CARCATE_ID LIKE '%" + CommonFunction.ReplaceInjection(CARCATE_ID) + @"%'
ORDER BY DOCTYPE_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            if (Getdatasql == "Y")
            {
                lstDocument.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    lstDocument.Add(new DOCUMENT
                    {
                        DOCTYPE_ID = dr["DOCTYPE_ID"] + "",
                        DOC_ID = dr["DOC_ID"] + "",
                        DOC_DESCRIPTION = dr["DOC_DESCRIPTION"] + "",
                        DOC_ITEM = dr["DOC_ITEM"] + "",
                        CONSIDER = dr["CONSIDER"] + "",
                        CDYNAMIC = dr["CDYNAMIC"] + "",
                        SVISIBLE = !string.IsNullOrEmpty(dr["SVISIBLE"] + "") ? dr["SVISIBLE"] + "" : "N",
                        FILE_NAME = dr["FILE_NAME"] + "",
                        FILE_PATH = dr["FILE_PATH"] + "",
                        FILE_SYSNAME = dr["FILE_SYSNAME"] + "",
                        OPENFILE = dr["FILE_PATH"] + "" + dr["FILE_SYSNAME"] + "",
                        CATTACH_DOC01 = dr["DOC_01"] + "",
                        CATTACH_DOC02 = dr["DOC_02"] + "",
                        CATTACH_DOC03 = dr["DOC_03"] + "",
                        CATTACH_DOC04 = dr["DOC_04"] + "",
                    });
                }
            }

            //กรณีที่เป้นรถลูกค้าไม่ต้องแนบเทียบแป้น
            if (CARCATE_ID == "01")
            {
                lstDocument.RemoveAll(w => w.DOCTYPE_ID == "0006");
            }

            gvwBill.DataSource = lstDocument;
            gvwBill.DataBind();
        }
        else
        {
            gvwBill.DataSource = lstDocument;
            gvwBill.DataBind();
        }
    }

    protected void ulcOtherDoc_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string Name = "";
        switch (sREQTYPE_ID)
        {
            case "01":
                Name = "Measure_Other";
                break;
            case "02":
                Name = "AddSeal_Other";
                break;
            case "03":
                Name = "Paint_Other";
                break;
            case "04":
                Name = "Doc_Other";
                break;
        }

        string Path = string.Format(sPathTemp, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), SUID);
        CreateFolder(Path);

        string[] FILETYPE = e.UploadedFile.FileName.Split('.');

        //ฟังชั่นเช็คประเภทรูปและขนาดในการอับโหลด
        string AlertText = SystemFunction.DOC_CheckSize(FILETYPE[FILETYPE.Length - 1], e.UploadedFile.FileBytes.Length);
        if (CheckTypeFileUpLoad(e.UploadedFile))
        {
            if (string.IsNullOrEmpty(AlertText))
            {
                // สร้างชื่อไฟล์
                string FILENAME = SystemFunction.SplitFileName(e.UploadedFile.FileName);
                string SYSFILENAME = "REQ_OHTER_" + @"_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + "." + FILETYPE[FILETYPE.Length - 1];

                //เก็บเอกสารลง Temp
                e.UploadedFile.SaveAs(Server.MapPath(Path + "/" + SYSFILENAME));
                if (File.Exists(Server.MapPath(Path + "/" + SYSFILENAME)))
                {
                    _SysFileName = SYSFILENAME;
                    _sFileName = FILENAME;
                    _sPathNow = Path;
                }
                else
                {

                }
                e.CallbackData = "";
            }
            else
            {
                e.CallbackData = AlertText;
            }
        }
        else
        {
            e.CallbackData = "เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น";
        }
    }

    protected void xcpnGvwotherDoc_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        //string Req_ID = txtReqID.Text;
        int inx = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
        switch (paras[0])
        {
            case "UPLOAD":

                lstDocumentOther.Add(new DOCUMENT
                {
                    DOCTYPE_ID = "0002",
                    FILE_NAME = _sFileName,
                    FILE_PATH = _sPathNow,
                    FILE_SYSNAME = _SysFileName,
                    SVISIBLE = !string.IsNullOrEmpty(_sFileName) ? "Y" : "N",
                    DOC_ID = lstDocumentOther.Count > 0 ? (int.Parse(lstDocumentOther.OrderByDescending(s => s.DOC_ID).FirstOrDefault().DOC_ID) + 1) + "" : "1",
                    CONSIDER = "",
                    OPENFILE = _sPathNow + _SysFileName,
                    REQUEST_ID = sReq_ID
                });

                ListDocOther("", "N");

                break;

            case "DEL":
                string DOCTYPE_ID = gvwOtherDoc.GetRowValues(inx, "DOCTYPE_ID") + "";
                string DOC_ID = gvwOtherDoc.GetRowValues(inx, "DOC_ID") + "";

                lstDocumentOther.RemoveAll(w => w.DOC_ID == DOC_ID);
                ListDocOther("", "N");
                // DelFileUpLoad(txtReqID.Text, DOCTYPE_ID, DOC_ID);

                break;
        }

    }

    private void ListDocOther(string REQUEST_ID, string Getdatasql)
    {

        string Query = @"SELECT  MS.DOCTYPE_ID, MS.DOC_DESCRIPTION,MS.DOC_01,  MS.DOC_02, MS.DOC_03, MS.DOC_04, MS.ISACTIVE_FLAG, MS.DESCRIPTION
,MS.CDYNAMIC,  MS.CATTACH_DOC01, MS.CATTACH_DOC02, MS.CATTACH_DOC03, MS.CATTACH_DOC04
, DOC.FILE_NAME, DOC.FILE_SYSNAME, DOC.FILE_PATH, DOC.CONSIDER,CASE  WHEN REQ.STATUS_FLAG IN ('09','02') THEN 'Y' ELSE 'N' END as SALLOWEDIT,
CASE WHEN NVL(DOC.FILE_SYSNAME,'xxx') <> 'xxx' THEN 'Y' ELSE 'N' END as SVISIBLE ,DOC.DOC_ID,DOC.DOC_ITEM
FROM TBL_DOCTYPE MS
Inner JOIN 
(
    SELECT REQUEST_ID, DOC_ID, DOC_TYPE,  DOC_ITEM, FILE_NAME, FILE_SYSNAME, FILE_PATH, CONSIDER
    FROM TBL_REQDOC 
    WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQUEST_ID) + @"'
)DOC
ON MS.DOCTYPE_ID = DOC.DOC_TYPE
LEFT JOIN TBL_REQUEST REQ
ON DOC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 AND MS.DOCTYPE_ID = '0002' AND MS.ISACTIVE_FLAG = 'Y'  
ORDER BY DOCTYPE_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            if (Getdatasql == "Y")
            {
                lstDocumentOther.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    lstDocumentOther.Add(new DOCUMENT
                    {
                        DOCTYPE_ID = dr["DOCTYPE_ID"] + "",
                        DOC_ID = dr["DOC_ID"] + "",
                        DOC_DESCRIPTION = dr["DOC_DESCRIPTION"] + "",
                        DOC_ITEM = dr["DOC_ITEM"] + "",
                        CONSIDER = dr["CONSIDER"] + "",
                        SVISIBLE = dr["SVISIBLE"] + "",
                        FILE_NAME = dr["FILE_NAME"] + "",
                        FILE_PATH = dr["FILE_PATH"] + "",
                        FILE_SYSNAME = dr["FILE_SYSNAME"] + "",
                        OPENFILE = dr["FILE_PATH"] + "" + dr["FILE_SYSNAME"] + ""
                    });
                }
            }




        }
        else
        {
            gvwOtherDoc.Visible = false;
        }


        if (lstDocumentOther.Where(w => w.FILE_PATH != null && w.FILE_NAME != null).Count() > 0)
        {

            gvwOtherDoc.Visible = true;
        }
        else
        {
            gvwOtherDoc.Visible = false;
        }

        gvwOtherDoc.DataSource = lstDocumentOther.OrderBy(s => s.DOC_ID);
        gvwOtherDoc.DataBind();

    }

    #endregion

    //เช็คว่าแนบเอกสารที่ต้องแนบครบไหม
    private bool CheckAttachFile()
    {
        bool sResult = false;
        if (RK_FLAG == "M")
        {
            var query = lstDocument.Where(w => w.CDYNAMIC == "Y" && (sREQTYPE_ID == "01" ? w.CATTACH_DOC01 == "Y" : sREQTYPE_ID == "02" ? w.CATTACH_DOC02 == "Y" : sREQTYPE_ID == "03" ? w.CATTACH_DOC03 == "Y" : sREQTYPE_ID == "04" ? w.CATTACH_DOC04 == "Y" : false)).ToList();
            if (query.Count > 0)
            {
                var queryCheck = from q in query
                                 from l in lstDocument.Where(w => w.DOCTYPE_ID == q.DOCTYPE_ID && w.FILE_SYSNAME != "" && w.FILE_PATH != "")
                                 select new { q.DOCTYPE_ID };

                sResult = (query.Count == queryCheck.Count());
            }
        }
        else
        {
            sResult = true;
        }
        return sResult;
    }

    //TBL_REQDOC ข้อมูลเอกสาร
    private void Update_TBL_REQDOC(string sREQID)
    {

        // Delete
        string sqlDel = @"DELETE  FROM TBL_REQDOC WHERE REQUEST_ID = '{0}' AND DOC_TYPE = '{1}'";

        // insert
        string sqlIns = @"INSERT INTO TBL_REQDOC(REQUEST_ID,DOC_ID,DOC_TYPE,DOC_ITEM,FILE_NAME,FILE_SYSNAME,FILE_PATH,CONSIDER,DATTACH_FILE)
                                       VALUES(:REQUEST_ID,:DOC_ID,:DOC_TYPE,:DOC_ITEM,:FILE_NAME,:FILE_SYSNAME,:FILE_PATH,:CONSIDER,:DATTACH_FILE)";

        DataTable dt = CommonFunction.Get_Data(conn, "SELECT  REQUEST_ID, DOC_TYPE,DOC_ID, FILE_NAME, FILE_SYSNAME, FILE_PATH FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

        #region ไฟล์ เอกสารทั่วไป
        using (OracleConnection con = new OracleConnection(conn))
        {
            con.Open();

            foreach (var item1 in lstDocument)
            {
                // ตรวจสอบการลบไฟล์ทั่วไป


                //ถ้ามากกว่า 0 แสดงว่ายังเป็นเอกสารเดิม
                if (dt.Select("DOC_TYPE = '" + CommonFunction.ReplaceInjection(item1.DOCTYPE_ID) + "' AND FILE_NAME = '" + CommonFunction.ReplaceInjection(item1.FILE_NAME) + "' AND FILE_SYSNAME = '" + CommonFunction.ReplaceInjection(item1.FILE_SYSNAME) + "' AND FILE_PATH = '" + CommonFunction.ReplaceInjection(item1.FILE_PATH) + "'").Count() > 0)
                {

                }
                else
                {
                    SystemFunction.SQLExecuteNonQuery(conn, string.Format(sqlDel, CommonFunction.ReplaceInjection(sREQID), CommonFunction.ReplaceInjection(item1.DOCTYPE_ID)));

                    // ไฟล์ใหม่
                    string Name = "";
                    switch (sREQTYPE_ID)
                    {
                        case "01":
                            Name = "Measure";
                            break;
                        case "02":
                            Name = "AddSeal";
                            break;
                        case "03":
                            Name = "Paint";
                            break;
                        case "04":
                            Name = "Doc";
                            break;
                    }

                    string Path = string.Format(sPathSave, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), SUID);

                    string sPath = UpFile2Server_CreateDirectory(item1.FILE_SYSNAME, Path, item1.FILE_PATH);
                    using (OracleCommand com = new OracleCommand(sqlIns, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;
                        com.Parameters.Add(":DOC_ID", OracleType.Number).Value = item1.DOC_ID /*genDocID(sREQID)*/; //รหัสเอกสาร 
                        com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = item1.DOCTYPE_ID; //ประเภทเอกสาร
                        com.Parameters.Add(":DOC_ITEM", OracleType.Number).Value = 1; //เอกสารที่
                        com.Parameters.Add(":FILE_NAME", OracleType.VarChar).Value = item1.FILE_NAME; // ชื่อ ไฟล์ที่อัพโหลด
                        com.Parameters.Add(":FILE_SYSNAME", OracleType.VarChar).Value = item1.FILE_SYSNAME; // ชื่อที่ระบบจัดเก็บ
                        com.Parameters.Add(":FILE_PATH", OracleType.VarChar).Value = Path; //สถานที่เก็บ เอกสาร
                        com.Parameters.Add(":CONSIDER", OracleType.VarChar).Value = "Y";//สถานะการตรวจสอบเอกสาร
                        com.Parameters.Add(":DATTACH_FILE", OracleType.DateTime).Value = DateTime.Now;//วันที่แนบเอกสาร
                        com.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion

        #region ไฟล์อื่นๆ

        // ตรวจสอบการลบไฟล์อื่นๆ
        foreach (var item2 in lstDocumentOther)
        {

            //var QcheckOtherDoc = lstDocumentOther.Where(w => w.DOCTYPE_ID == sDocOther).ToList();
            if (dt.Select("DOC_TYPE = '" + CommonFunction.ReplaceInjection(sDocOther) + "' AND FILE_NAME = '" + CommonFunction.ReplaceInjection(item2.FILE_NAME) + "' AND FILE_SYSNAME = '" + CommonFunction.ReplaceInjection(item2.FILE_SYSNAME) + "' AND FILE_PATH = '" + CommonFunction.ReplaceInjection(item2.FILE_PATH) + "' AND DOC_ID = " + CommonFunction.ReplaceInjection(item2.DOC_ID) + "").Count() > 0)
            {

            }
            else
            {
                string sqlDelDocOther = sqlDel + " AND DOC_ID = {2}";

                SystemFunction.SQLExecuteNonQuery(conn, string.Format(sqlDelDocOther, CommonFunction.ReplaceInjection(sREQID), CommonFunction.ReplaceInjection(sDocOther), item2.DOC_ID));
                // เพิ่มไฟล์
                if (lstDocumentOther.Count > 0)
                {

                    string Name = "";
                    switch (sREQTYPE_ID)
                    {
                        case "01":
                            Name = "Measure_Other";
                            break;
                        case "02":
                            Name = "AddSeal_Other";
                            break;
                        case "03":
                            Name = "Paint_Other";
                            break;
                        case "04":
                            Name = "Doc_Other";
                            break;
                    }

                    string Path = string.Format(sPathSave, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), SUID);

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        con.Open();
                        //var QnMax = lstDocumentOther.OrderByDescending(o => o.DOC_ID).FirstOrDefault();
                        //decimal i = QnMax != null ? int.Parse(QnMax.DOC_ID) : 1;
                        //i = i > 0 ? i : 1;

                        string sPath = UpFile2Server_CreateDirectory(item2.FILE_SYSNAME, Path, item2.FILE_PATH);
                        using (OracleCommand com = new OracleCommand(sqlIns, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;
                            com.Parameters.Add(":DOC_ID", OracleType.Number).Value = /*item2.DOC_ID*/genDocID(sREQID, sDocOther); //รหัสเอกสาร 
                            com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = sDocOther; //ประเภทเอกสาร
                            com.Parameters.Add(":DOC_ITEM", OracleType.Number).Value = 1; //เอกสารที่
                            com.Parameters.Add(":FILE_NAME", OracleType.VarChar).Value = item2.FILE_NAME; // ชื่อ ไฟล์ที่อัพโหลด
                            com.Parameters.Add(":FILE_SYSNAME", OracleType.VarChar).Value = item2.FILE_SYSNAME; // ชื่อที่ระบบจัดเก็บ
                            com.Parameters.Add(":FILE_PATH", OracleType.VarChar).Value = Path; //สถานที่เก็บ เอกสาร
                            com.Parameters.Add(":CONSIDER", OracleType.VarChar).Value = "Y";//สถานะการตรวจสอบเอกสาร
                            com.Parameters.Add(":DATTACH_FILE", OracleType.DateTime).Value = DateTime.Now;//วันที่แนบเอกสาร
                            com.ExecuteNonQuery();
                        }



                    }
                }
            }
        }
        #endregion

        // ลบไฟล์ที่มีการเปลี่ยนแปลงทั้งหมดในแต่ละโฟล์เดอร์
        //DeleteFileFromListDel(lstTempFileDel);
        //deleteFiletemp();
    }

    private string UpFile2Server_CreateDirectory(string sFileName, string _PathSave, string pathFileNow)
    {
        string sResult = "";
        if (File.Exists(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + sFileName))
        {
            CreateFolder(_PathSave); // Create Directory

            sResult = _PathSave;

            try
            {
                Directory.Move(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + sFileName, Server.MapPath("./") + _PathSave.Replace("/", "\\") + sFileName);
            }
            catch
            {
                //File.Copy(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + "\\" + sFileName, Server.MapPath("./") + _PathSave.Replace("/", "\\") + "\\" + sFileName);

                //if (Directory.Exists(Server.MapPath("./") + pathFileNow.Replace("/", "\\")))
                //{
                //    File.Delete(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + "\\" + sFileName);
                //}
            }
        }

        return sResult;
    }

    private decimal genDocID(string sREQID, string DOC_TYPE)
    {
        string sql = @"SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '{0}' AND DOC_TYPE = '" + DOC_TYPE + "' ORDER BY DOC_ID DESC";
        decimal nTemp = 0;
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        if (dt.Rows.Count > 0)
        {
            dr = dt.Rows[0];
            nTemp = decimal.TryParse(dr["DOC_ID"] + "", out nTemp) ? nTemp : 1;
            nTemp = nTemp + 1;
        }
        else
        {
            nTemp = 1;
        }

        return nTemp;
    }

    private void GenScarNum(string STRUCKID)//ไอดีรถที่ส่งมาถ้าเป็น 3 จะเอาหางมาหา
    {

        //ในกรณีที่ปิดงานแล้วจะอัพเดท SCAENUM รหัสวัดน้ำ และ SLAST_REQ 
        if (sREQTYPE_ID == "01")
        {
            string QUERY = "SELECT STRUCKID,SCAR_NUM FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
            DataTable dt = CommonFunction.Get_Data(conn, QUERY);
            //เช็คว่ามีรถไหม
            if (dt.Rows.Count > 0)
            {
                string SCAR_NUM = dt.Rows[0]["SCAR_NUM"] + "";
                if (!string.IsNullOrEmpty(SCAR_NUM))//เช็คว่ามี Scarnum ไหม มีให้อัพเดทลงไปใน Tbl_request
                {
                    //แอดลง TBL_REQUEST
                    string UPDATE = @"UPDATE TBL_REQUEST
                                      SET    SCAR_NUM                = '" + SCAR_NUM + @"'
                                      WHERE  REQUEST_ID              = '" + sReq_ID + "'";

                    SystemFunction.SQLExecuteNonQuery(conn, UPDATE);

                    //แอดลง TTRUCK
                    string UPDATE_TTRUCK = @"UPDATE TTRUCK
                                      SET    SCAR_NUM                = '" + SCAR_NUM + @"'
                                      WHERE  STRUCKID              = '" + STRUCKID + "'";

                    SystemFunction.SQLExecuteNonQuery(conn, UPDATE_TTRUCK);

                    lblSCARNUM.Text = SCAR_NUM;
                }
                else //ถ้าไม่มี ให้เจนแล้วอัพเดทไป
                {
                    string SCARNUM = SystemFunction.GenCARNUM(STRUCKID);
                    //แอดลง TBL_REQUEST
                    string UPDATE = @"UPDATE TBL_REQUEST
                                      SET    SCAR_NUM                = '" + SCARNUM + @"'
                                      WHERE  REQUEST_ID              = '" + sReq_ID + "'";

                    SystemFunction.SQLExecuteNonQuery(conn, UPDATE);

                    //แอดลง TTRUCK
                    string UPDATE_TTRUCK = @"UPDATE TTRUCK
                                      SET    SCAR_NUM                = '" + SCARNUM + @"'
                                      WHERE  STRUCKID              = '" + STRUCKID + "'";

                    SystemFunction.SQLExecuteNonQuery(conn, UPDATE_TTRUCK);


                    lblSCARNUM.Text = SCARNUM;
                }

            }
            else
            {
                lblSCARNUM.Text = "-";
            }


        }
        else
        {
            lblSCARNUM.Text = "-";
        }


    }

    #region Structure
    public class DocApprove
    {
        public string FlageDocApprove { get; set; }

    }

    public class DOCUMENT
    {
        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string REQUEST_ID { get; set; }
        public string DOC_ID { get; set; }
        public string DOC_ITEM { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_SYSNAME { get; set; }
        public string FILE_PATH { get; set; }
        public string CONSIDER { get; set; }
        public string SVISIBLE { get; set; }
        public string CDYNAMIC { get; set; }
        public string OPENFILE { get; set; }
        public string CATTACH_DOC01 { get; set; }
        public string CATTACH_DOC02 { get; set; }
        public string CATTACH_DOC03 { get; set; }
        public string CATTACH_DOC04 { get; set; }
    }
    #endregion
}