﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Kpi_lst : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            SetRownullDefualt();
        }
    }
    private void SetRownullDefualt()
    {
        try
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("ROWCOLUMN", typeof(string)));
            dt.Columns.Add(new DataColumn("ROWMERGE", typeof(string)));
            dt.Columns.Add(new DataColumn("JAN", typeof(string)));
            dt.Columns.Add(new DataColumn("FEB", typeof(string)));
            dt.Columns.Add(new DataColumn("MAR", typeof(string)));
            dt.Columns.Add(new DataColumn("APR", typeof(string)));
            dt.Columns.Add(new DataColumn("MAY", typeof(string)));
            dt.Columns.Add(new DataColumn("JUN", typeof(string)));
            dt.Columns.Add(new DataColumn("JUL", typeof(string)));
            dt.Columns.Add(new DataColumn("AUG", typeof(string)));
            dt.Columns.Add(new DataColumn("SEP", typeof(string)));
            dt.Columns.Add(new DataColumn("OCT", typeof(string)));
            dt.Columns.Add(new DataColumn("NOV", typeof(string)));
            dt.Columns.Add(new DataColumn("DEC", typeof(string)));
            string[] rowcolumn = { "รอบันทึกผล", "บันทึกผลครบถ้วน", "บันทึกผลครบถ้วน", "บันทึกผลครบถ้วน" };
            string[] rowmerge = { "รอบันทึกผล", "บันทึกผลครบถ้วน", "รอแจ้งผล", "แจ้งผลแล้ว" };
            for (int i = 0; i < rowcolumn.Length; i++)
            {
                dr = dt.NewRow();
                dr["ROWCOLUMN"] = rowcolumn[i];
                dr["ROWMERGE"] = rowmerge[i];
                dt.Rows.Add(dr);
            }
            foreach (string item in rowcolumn)
            {
                
            }
            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            gvd_kpi.DataSource = dt;
            gvd_kpi.DataBind();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #region Event

    protected void list_kpi_Click(object sender, EventArgs e)
    {
        Response.Redirect("report_kpi.aspx");
    }
    protected void config_kpi_Click(object sender, EventArgs e)
    {
        Response.Redirect("Config_reportKPI.aspx");
    }
    
    protected void save_kpi_Click(object sender, EventArgs e)
    {
        Response.Redirect("Kpi_process.aspx");
    }
    #endregion
    


    

    protected void gvd_kpi_PreRender(object sender, EventArgs e)
    {
        var gridView = (GridView)sender;
        var header = (GridViewRow)gridView.Controls[0].Controls[0];
        header.Cells[0].Visible = false;
        header.Cells[1].ColumnSpan = 2;
        header.Cells[1].Text = "";
    }
    protected void gvd_kpi_DataBound(object sender, EventArgs e)
    {
        
        for (int i =0; i < gvd_kpi.Rows.Count - 1;  i++)
        {
            GridViewRow CellNow = gvd_kpi.Rows[i];
            GridViewRow previousCell = gvd_kpi.Rows[i];           
            if (CellNow.Cells[0].Text == previousCell.Cells[1].Text)
            {
                if (CellNow.Cells[i].ColumnSpan == 0)
                {
                    if (CellNow.Cells[0].ColumnSpan == 0)
                    {
                        CellNow.Cells[0].ColumnSpan = 2;
                        
                    }                    
                    previousCell.Cells[1].Visible = false;   
                       
                }
            }            
        }
        gvd_kpi.Rows[2].Cells[0].RowSpan = 2;
        gvd_kpi.Rows[2].Cells[0].Style.Add("vertical-align", "Middle");
        gvd_kpi.Rows[3].Cells[0].Visible = false;
    }
   
}