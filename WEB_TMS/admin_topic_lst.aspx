﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_topic_lst.aspx.cs" Inherits="admin_topic_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
    <script type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="65%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" 
                                NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา" Style="margin-left: 0px" 
                                Width="220px">
                            </dx:ASPxTextBox> 
                            </td>
                        <td align="right" class="style13" width="16%">
                        <dx:ASPxComboBox ID="cmbProcess" runat="server" Width="150px" TextField="SPROCESSNAME" ValueField="SPROCESSID" DataSourceID="sdsProcess" >
                            </dx:ASPxComboBox>
                             <asp:SqlDataSource ID="sdsProcess" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                                SelectCommand="SELECT SPROCESSID, SPROCESSNAME FROM TPROCESS WHERE (SPROCESSID LIKE '%0')"></asp:SqlDataSource>
                        </td>
                        <td width="9%" class="style13">
                            <dx:ASPxComboBox ID="cboStatus" runat="server" Width="80px">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="Active" Value="1" />
                                    <dx:ListEditItem Text="InActive" Value="0" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds" 
                                OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="2" FieldName="STOPICID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัวข้อตัดคะแนนประเมินผล" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        Width="33%" FieldName="STOPICNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ประเมินความรุนแรงของแต่ละปัญหาที่เกิดขึ้น" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ภาพลักษณ์<br>องค์กร" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                                Width="10%" FieldName="SCOL02">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ความพึงพอใจ<br>ลูกค้า" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                                Width="10%" FieldName="SCOL03">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="กฏ/ระเบียบ<br>ข้อห้าม" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                Width="10%" FieldName="SCOL04">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="แผนงาน<br>ขนส่ง" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                                Width="10%" FieldName="SCOL06">
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn Caption="Process" Width="20%" VisibleIndex="10" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                     <DataItemTemplate>
                                       <dx:ASPxComboBox ID="cmbProcess" runat="server" ReadOnly="true" >
                                       </dx:ASPxComboBox>
                                     </DataItemTemplate>
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="ผลคูณ<br>ความรุนแรง" VisibleIndex="11" HeaderStyle-HorizontalAlign="Center"
                                        Width="15%" FieldName="NMULTIPLEIMPACT">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัก<br>คะแนน<br>ต่อครั้ง" VisibleIndex="12" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="NPOINT" Width="5%">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataCheckColumn VisibleIndex="13" Width="5%" Caption="สถานะ" HeaderStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'  runat="server" ReadOnly="true">
                                            </dx:ASPxCheckBox>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                     </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn Caption="เวอร์ชั่น" VisibleIndex="14" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SVERSION" Width="5%" Visible="False">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn  FieldName="CPROCESS" Visible="False">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="15">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY TOPIC.sTopicID) AS ID1,TOPIC.sTopicID, TOPIC.sTopicName,TOPI.CPROCESS,TOPIC.sVersion, TOPI.sCol02, TOPI.sCol03,TOPI.sCol04,TOPI.sCol06, TOPI.nMultipleImpact,TOPI.nPoint,TOPI.CACTIVE
                            FROM (
                            SELECT ROW_NUMBER () OVER (ORDER BY TOPIC.sTopicID) AS ID1,TOPIC.sTopicID, TOPIC.sTopicName,MAX(TOPIC.sVersion) sVersion
                            FROM tTopic TOPIC
                            Where 1=1 
                            AND sTopicName LIKE '%' || :oSearch || '%'
                            GROUP BY TOPIC.sTopicID,TOPIC.sTopicName
                            ) TOPIC
                            LEFT JOIN tTopic  topi ON TOPIC.STOPICID=TOPI.STOPICID AND TOPIC.sVersion=TOPI.sVersion WHERE NVL(TOPI.CPROCESS,'-') LIKE '%' || :oPROCESS || '%' AND TOPI.CACTIVE = :oCACTIVE"
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" DeleteCommand="DELETE FROM tTopic WHERE sTopicID = :sTopicID"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="sTopicID" SessionField="dTopicID" Type="Int16" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch" PropertyName="Text" />
                                    <asp:ControlParameter Name="oCACTIVE" ControlID="cboStatus" PropertyName="Value" />
                                    <asp:ControlParameter Name="oPROCESS" ControlID="cmbProcess" PropertyName="Value" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                         
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
