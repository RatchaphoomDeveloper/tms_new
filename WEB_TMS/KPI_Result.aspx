﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Result.aspx.cs" Inherits="KPI_Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>สรุปสถานะการแจ้งผลประเมิน KPI (รายเดือน)
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">ปี</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" >
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เดือน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 control-label">สถานะ</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlStatus">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label"></label>
                            <div class="col-md-3">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />

                    </div>
                </div>

                
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ผลการค้นหา จำนวน <asp:Label Text="0" runat="server" ID="lblItem" /> รายการ
            </div>
            <div class="panel-body">
                <div class="row form-group">
                <asp:GridView runat="server" ID="gvKPI"
                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false" PageSize="40"
                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowCommand="gvKPI_RowCommand"
                    AllowPaging="True" OnRowUpdating="gvKPI_RowUpdating" DataKeyNames="KPI_HEADER_ID,YEAR,MONTH_ID,SCONTRACTID,SVENDORID" OnPageIndexChanging="gvKPI_PageIndexChanging" OnRowDataBound="gvKPI_RowDataBound">
                    <Columns>
                        <%--<asp:BoundField HeaderText="KPI_HEADER_ID" DataField="KPI_HEADER_ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                        
                        <asp:BoundField HeaderText="ชื่อผู้ขนส่ง" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"  />
                        <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="สถานะ" DataField="STATUS_NAME" HeaderStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="แจ้งผล" ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:Label ID="lblSendToVendor" runat="server" Text="แจ้งผล"></asp:Label>
                                <asp:CheckBox Text="" runat="server" ID="cbCheckAll" AutoPostBack="true" OnCheckedChanged="cbCheckAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                    <asp:CheckBox Text="" runat="server" ID="cbResult" />
                                    <asp:HiddenField runat="server" ID="hidKPI_HEADER_ID" Value='<%# Eval("KPI_HEADER_ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="วันที่แจ้งล่าสุด" DataField="SEND_TO_VENDOR_DATETIME" HeaderStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="ผลการประเมิน KPI" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                    <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="View" CommandName="update"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="สรุปคะแนนประจำปี" DataField="POINT" HeaderStyle-HorizontalAlign="Center" />--%>
                        <asp:TemplateField HeaderText="ROW_COLOR" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblRowColor" runat="server" Text='<%# Eval("ROW_COLOR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="KPI_HEADER_ID" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblKPIHeaderID" runat="server" Text='<%# Eval("KPI_HEADER_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ข้อมูลเดือน">
                            <ItemTemplate>
                                <asp:Label ID="lblMonth" runat="server" Text='<%# Eval("NAME_TH") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="บริษัทผู้ขนส่ง">
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("SABBREVIATION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="เลขที่สัญญา">
                            <ItemTemplate>
                                <asp:Label ID="lblContractName" runat="server" Text='<%# Eval("SCONTRACTNO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะ">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("STATUS_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="แจ้งผล" ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:Label ID="lblSendToVendor" runat="server" Text="แจ้งผล"></asp:Label>
                                <asp:CheckBox Text="" runat="server" ID="cbCheckAll" AutoPostBack="true" OnCheckedChanged="cbCheckAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                
                                    <asp:CheckBox Text="" runat="server" ID="cbResult" />
                                    <asp:HiddenField runat="server" ID="hidKPI_HEADER_ID" Value='<%# Eval("KPI_HEADER_ID") %>' />
                                <asp:HiddenField runat="server" ID="hidSVENDORID" Value='<%# Eval("SVENDORID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่แจ้งล่าสุด">
                            <ItemTemplate>
                                <asp:Label ID="lblVendorDateTime" runat="server" Text='<%# Eval("SEND_TO_VENDOR_DATETIME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ผลการประเมิน KPI" ItemStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="View" CommandName="update"></asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkEditData" runat="server" Text="Edit" CommandName="editData"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="สรุปคะแนนประจำปี">
                            <ItemTemplate>
                                <asp:Label ID="lblPointYear" runat="server" Text='<%# Eval("POINT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                </asp:GridView>
                    </div>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">

                        <asp:Button Text="แจ้งผล" runat="server" ID="btnResult" class="btn btn-md bth-hover btn-info" OnClick="btnResult_Click" />
                        <asp:Button Text="ยกเลิก" runat="server" ID="btnCancle" class="btn btn-md bth-hover btn-info" OnClick="btnCancle_Click" />

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

