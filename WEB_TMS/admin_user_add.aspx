﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_user_add.aspx.cs" Inherits="admin_user_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .style14
        {
            height: 24px;
        }
        .style15
        {
            width: 14px;
            background-color: #FFFFFF;
        }
        .style17
        {
            width: 10px;
        }
        .style18
        {
            height: 27px;
            text-align: left;
            background-color: #FFFFFF;
        }
        .style20
        {
            height: 24px;
            width: 142px;
        }
        .style21
        {
            height: 30px;
        }
    </style>
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td width="17%" bgcolor="#FFFFFF">
                            กลุ่มผู้ใช้งาน <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" colspan="3">
                            <dx:ASPxRadioButtonList ID="rblgroup" runat="server" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" ClientInstanceName="rblgroup" SkinID="rblStatus">
                                <ClientSideEvents SelectedIndexChanged="function (s, e) {xcpn.PerformCallback('SelectGroup')}">
                                </ClientSideEvents>
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="ผู้ประกอบการขนส่ง" Value="0" />
                                    <dx:ListEditItem Text="เจ้าหน้าที่ ปตท." Value="1" />
                                    <dx:ListEditItem Text="คลัง" Value="2" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td width="14%" align="center" bgcolor="#FFFFFF" style="text-align: center">
                            &nbsp;
                        </td>
                        <td align="center" bgcolor="#FFFFFF" style="text-align: center" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF">
                            ชื่อ - สกุล<font color="#FF0000">*</font>
                        </td>
                        <td class="style15">
                            <dx:ASPxTextBox ID="txtName" runat="server" Width="140px" ClientInstanceName="txtName"
                                MaxLength="50">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Name %>" ></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                        <td bgcolor="#FFFFFF" class="style17">
                            -
                        </td>
                        <td bgcolor="#FFFFFF" class="style15">
                            <dx:ASPxTextBox ID="txtLastName" runat="server" Width="140px" ClientInstanceName="txtLastName"
                                MaxLength="50">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_LName %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left">
                            หน่วยงาน
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" colspan="2">
                            <dx:ASPxComboBox ID="cbxOrganiz" runat="server" ClientInstanceName="cbxOrganiz" TextFormatString="{0}"
                                ValueType="System.String" ValueField="VENDOR_CODE" Width="140px" SelectedIndex="0"
                                CallbackPageSize="10" OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                                OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL" EnableCallbackMode="true"
                                SkinID="xcbbATC" AutoPostBack="True">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="VENDOR_ABBR" Caption="ชื่อหน่วยงาน" Width="200px" />
                                </Columns>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_Unit %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="style18">
                            ตำแหน่ง
                        <font color="#FF0000">*</font></td>
                        <td align="left" colspan="3" class="style18">
                            <dx:ASPxTextBox ID="txtPosition" runat="server" ClientInstanceName="txtPosition"
                                Width="140px" MaxLength="50">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Position %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            หมายเลขโทรศัพท์
                        <font color="#FF0000">*</font></td>
                        <td align="left" class="style18" colspan="2">
                            <dx:ASPxTextBox ID="txtPhone" runat="server" Width="140px" MaxLength="20">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Tel %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style14">
                            username<font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style14" colspan="3">
                            <dx:ASPxTextBox ID="txtUsername" runat="server" ClientInstanceName="txtUsername"
                                Width="140px" MaxLength="20">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Username %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" class="style14">
                            Password<font color="#FF0000">*</font>
                        </td>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" class="style20">
                            <dx:ASPxTextBox ID="txtPassword" runat="server" Password="True" Width="140px" ClientInstanceName="txtPassword"
                                MaxLength="15">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Password %>">
                                    </RequiredField>
                                    <RegularExpression ErrorText="ความยาวตัวอักษรต้องมากกว่าหรือเท่ากับ 8 ตัวอักษร และต้องผสม ตัวอักษรภาษาอังกฤษตัวเล็ก/ใหญ่ ตัวเลขและอักขระพิเศษ อย่างน้อย 3 ใน 4 แบบ" ValidationExpression="^(((?=.*[a-z])(?=.*[A-Z])(?=.*[\d]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\W]))|((?=.*[a-z])(?=.*[\d])(?=.*[\W]))|((?=.*[A-Z])(?=.*[\d])(?=.*[\W]))).{8,15}$" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtMD5Password" runat="server" Width="140px" ClientInstanceName="txtPassword"
                                ClientVisible="false" MaxLength="50">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtMD5OldPassword" runat="server" Width="140px" ClientVisible="false"
                                MaxLength="50">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" class="style14">
                            <dx:ASPxButton ID="btnReset" runat="server" Text="- Reset Password -" Visible="false"
                                Width="130px" SkinID="btnButtonLink" AutoPostBack="false" UseSubmitBehavior="false">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('ResetPassword'); }" ></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtEmail" runat="server" ClientInstanceName="txtEmail" Width="140px"
                                MaxLength="50">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_emp_Email %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style21">
                            สถานะ <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" colspan="3" class="style21">
                            <dx:ASPxRadioButtonList ID="rblStatus" runat="server" ClientInstanceName="rblStatus"
                                RepeatDirection="Horizontal" RepeatLayout="Flow" SelectedIndex="0" EnableDefaultAppearance="False"
                                SkinID="rblStatus">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                    <dx:ListEditItem Text="InActive" Value="0" />
                                    <dx:ListEditItem Text="LockUser" Value="2" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" class="style21">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" colspan="2" class="style21">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" class="style21">
                            หน่วยงาน (ทีม) <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" colspan="5" class="style21">
                            <dx:ASPxRadioButtonList ID="radTeam" runat="server" ClientInstanceName="radTeam"
                                RepeatDirection="Horizontal" RepeatLayout="Flow" SelectedIndex="0" EnableDefaultAppearance="False"
                                SkinID="rblStatus">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="ไม่ใช่ทีม รข." Value="0" />
                                    <dx:ListEditItem Text="ทีมมาตรฐาน" Value="1" />
                                    <dx:ListEditItem Text="ทีมสัญญา" Value="2" />
                                    <dx:ListEditItem Text="ทีมค่าขนส่ง" Value="3" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" class="style21">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left" colspan="2" class="style21">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top">
                            สิทธิการใช้งาน <font color="#FF0000">*</font>
                        </td>
                        <td align="left" colspan="6">
                            <asp:DataGrid ID="dgdPermission" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" Width="100%">
                                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="รายการเมนู / ฟังค์ชันการใช้งาน">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltrName" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                        <HeaderStyle Width="38%" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="สิทธิการทำงานที่ได้รับ">
                                        <ItemTemplate>
                                            <dx:ASPxRadioButtonList ID="rblPermission" runat="server" RepeatDirection="Horizontal"
                                                SkinID="rblStatus">
                                                <Items>
                                                    <dx:ListEditItem Selected="True" Text=" Disable " Value="0" />
                                                    <dx:ListEditItem Text=" View " Value="1" />
                                                    <dx:ListEditItem Text=" Add / Edit " Value="2" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>
                                        </ItemTemplate>
                                        <HeaderStyle Width="50%" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="รับEmail">
                                        <ItemTemplate>
                                           <dx:ASPxCheckBox id="cbxMail2Me" ClientInstanceName="cbxMail2Me" runat="server" ></dx:ASPxCheckBox>
                                        </ItemTemplate>
                                        <HeaderStyle Width="12%" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SMENUID" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SMENUNAME" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CTYPE" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NLEVEL" Visible="False"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="SMENUHEAD" Visible="False"></asp:BoundColumn> 
                                </Columns>
                                <HeaderStyle BackColor="#DADADA" />
                                <ItemStyle BackColor="White" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#FFFFFF" align="right" class="style14">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" ></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_user_lst.aspx'; }" ></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="3" class="style14">
                            &nbsp;
                        </td>
                        <td colspan="3" align="left" bgcolor="#FFFFFF" style="text-align: center" class="style14">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF">
                            &nbsp;
                        </td>
                        <td align="center" bgcolor="#FFFFFF" colspan="3" style="text-align: right">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: center" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" InsertCommand="INSERT INTO TUSER(IS_CONTRACT, SUID, SVENDORID, CGROUP, SPOSITION, STEL, SFIRSTNAME, SLASTNAME, SUSERNAME, SPASSWORD, SOLDPASSWORD, CACTIVE, DCREATE, SUSERCREATE, DUPDATE, SUSERUPDATE, SEMAIL,DCHANGEPASSWORD) VALUES (:sIsContract, :sUID, :sVendorID, :cGroup, :sPosition, :sTel, :sFirstName, :sLastName, :sUserName, :sPassword, :sOldPassword, :cActive, SYSDATE,:sUserCreate, SYSDATE,:sUserUpdate,:sEmail,SYSDATE)"
                    UpdateCommand="UPDATE TUSER SET IS_CONTRACT = :sIsContract, SVENDORID = :sVendorID, CGROUP = :cGroup, SPOSITION = :sPosition, STEL = :sTel, SFIRSTNAME = :sFirstName, SLASTNAME = :sLastName, SUSERNAME = :sUserName, SPASSWORD = :sPassword, SOLDPASSWORD = :sOldPassword, CACTIVE = :cActive, DUPDATE = SYSDATE, SUSERUPDATE =  :sUserUpdate, SEMAIL = :sEmail WHERE SUID =:sUID"
                    OnInserted="sds_Inserted" OnInserting="sds_Inserting" OnUpdated="sds_Updated"
                    OnUpdating="sds_Updating" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                    <InsertParameters>
                        <asp:SessionParameter Name="sUID" SessionField="suid" />
                        <asp:ControlParameter Name="sVendorID" ControlID="cbxOrganiz" PropertyName="Value" />
                        <asp:ControlParameter Name="cGroup" ControlID="rblgroup" PropertyName="Value" />
                        <asp:ControlParameter Name="sPosition" ControlID="txtPosition" PropertyName="Text" />
                        <asp:ControlParameter Name="sTel" ControlID="txtPhone" PropertyName="Text" />
                        <asp:ControlParameter Name="sFirstName" ControlID="txtName" PropertyName="Text" />
                        <asp:ControlParameter Name="sLastName" ControlID="txtLastName" PropertyName="Text" />
                        <asp:ControlParameter Name="sUserName" ControlID="txtUsername" PropertyName="Text" />
                        <asp:ControlParameter Name="sPassword" ControlID="txtMD5Password" PropertyName="Text" />
                        <asp:ControlParameter Name="sOldPassword" ControlID="txtMD5Password" PropertyName="Text" />
                        <asp:ControlParameter Name="cActive" ControlID="rblStatus" PropertyName="Value" />
                        <asp:SessionParameter Name="sUserCreate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:SessionParameter Name="sUserUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:ControlParameter Name="sEmail" ControlID="txtEmail" PropertyName="Text" />
                        <asp:ControlParameter Name="sIsContract" ControlID="radTeam" PropertyName="Value" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter Name="sVendorID" ControlID="cbxOrganiz" PropertyName="Value" />
                        <asp:ControlParameter Name="cGroup" ControlID="rblgroup" PropertyName="Value" />
                        <asp:ControlParameter Name="sPosition" ControlID="txtPosition" PropertyName="Text" />
                        <asp:ControlParameter Name="sTel" ControlID="txtPhone" PropertyName="Text" />
                        <asp:ControlParameter Name="sFirstName" ControlID="txtName" PropertyName="Text" />
                        <asp:ControlParameter Name="sLastName" ControlID="txtLastName" PropertyName="Text" />
                        <asp:ControlParameter Name="sUserName" ControlID="txtUsername" PropertyName="Text" />
                        <asp:ControlParameter Name="sPassword" ControlID="txtMD5Password" PropertyName="Text" />
                        <asp:ControlParameter Name="sOldPassword" ControlID="txtMD5OldPassword" PropertyName="Text" />
                        <asp:ControlParameter Name="cActive" ControlID="rblStatus" PropertyName="Value" />
                        <asp:SessionParameter Name="sUserUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:SessionParameter Name="sUID" SessionField="suid" />
                        <asp:ControlParameter Name="sEmail" ControlID="txtEmail" PropertyName="Text" />
                        <asp:ControlParameter Name="sIsContract" ControlID="radTeam" PropertyName="Value" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
