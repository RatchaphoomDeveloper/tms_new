﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="result-add3.aspx.cs" Inherits="result_add3" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Jquery/Number/jquery.number.js" type="text/javascript"></script>
    <script src="Javascript/Jquery/Number/jquery.number.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function HideValidate() {
            txtNumNewPhase.HideErrorCell();
            txtNumNewPhaseCompartNo1_Case1.HideErrorCell();
            txtNumNewPhaseCompartNo2_Case1.HideErrorCell();
        }

        /*แป้นที่ 1*/
        function CalPan1(sCase) {
            var sShow = "";
            if (sCase == "1") {
                //ระยะห่างจากก้นถังถึงแป้น (2)
                if (txtNumNewPhaseCompartNo1_Case1.GetValue() != null && txtNumNewPhaseCompartNo1_Case1.GetValue() != "") {
                    var values = txtNumNewPhaseCompartNo1_Case1.GetValue().replace(/,/g, '');
                    if ($.isNumeric(values)) {
                        var v1 = parseFloat(lblNumOldPhaseCompartNo1_Case1.GetText().replace(/,/g, ''));
                        var v2 = parseFloat(values);
                        var vCal = v1 - v2;

                        if (vCal > 0) {
                            sShow = "+" + $.number(vCal, 0);
                        }
                        else {
                            sShow = $.number(vCal, 0);
                        }

                        txtNumNewPhaseCompartNo1_Case1.SetText($.number(values, 0));
                    }
                    else {
                        txtNumNewPhaseCompartNo1_Case1.SetText("");
                    }

                    lblDiffPhaseCompartNo1_Case1.SetText(sShow);
                }
                else {
                    lblDiffPhaseCompartNo1_Case1.SetText("");
                }

                CalculateData("1");
            }
            else if (sCase == "2") {
                //ระยะจากปากถังถึงแป้น(1) - (2)
                sShow = "";
                if (lblNumNewPhaseCompartNo1_Case2.GetValue() != null && lblNumNewPhaseCompartNo1_Case2.GetValue() != "") {
                    var values = lblNumNewPhaseCompartNo1_Case2.GetValue().replace(/,/g, '');
                    if ($.isNumeric(values)) {
                        var v1 = parseFloat(lblNumOldPhaseCompartNo1_Case2.GetText().replace(/,/g, ''));
                        var v2 = parseFloat(values);
                        var vCal = v2 - v1;

                        if (vCal > 0) {
                            sShow = "+" + $.number(vCal, 0);
                        }
                        else {
                            sShow = $.number(vCal, 0);
                        }

                        lblNumNewPhaseCompartNo1_Case2.SetText($.number(values, 0));
                    }
                    else {
                        lblNumNewPhaseCompartNo1_Case2.SetText("");
                    }

                    lblDiffPhaseCompartNo1_Case2.SetText(sShow);

                }
                else {
                    lblDiffPhaseCompartNo1_Case2.SetText("");
                }
            }
        }

        /*แป้นที่ 2*/
        function CalPan2(sCase) {
            var sShow = "";
            if (sCase == "1") {
                //ระยะห่างจากก้นถังถึงแป้น (2)
                if (txtNumNewPhaseCompartNo2_Case1.GetValue() != null && txtNumNewPhaseCompartNo2_Case1.GetValue() != "") {
                    var values = txtNumNewPhaseCompartNo2_Case1.GetValue().replace(/,/g, '');
                    if ($.isNumeric(values)) {
                        var v1 = parseFloat(lblNumOldPhaseCompartNo2_Case1.GetText().replace(/,/g, ''));
                        var v2 = parseFloat(values);
                        var vCal = v1 - v2;

                        if (vCal > 0) {
                            sShow = "+" + $.number(vCal, 0);
                        }
                        else {
                            sShow = $.number(vCal, 0);
                        }

                        txtNumNewPhaseCompartNo2_Case1.SetText($.number(values, 0));
                    }
                    else {
                        txtNumNewPhaseCompartNo2_Case1.SetText("");
                    }

                    lblDiffPhaseCompartNo2_Case1.SetText(sShow);

                }
                else {
                    lblDiffPhaseCompartNo2_Case1.SetText("");
                }

                CalculateData("2");
            }
            else if (sCase == "2") {
                //ระยะจากปากถังถึงแป้น(1) - (2)
                sShow = "";
                if (lblNumNewPhaseCompartNo2_Case2.GetValue() != null && lblNumNewPhaseCompartNo2_Case2.GetValue() != "") {
                    var values = lblNumNewPhaseCompartNo2_Case2.GetValue().replace(/,/g, '');
                    if ($.isNumeric(values)) {
                        var v1 = parseFloat(lblNumOldPhaseCompartNo2_Case2.GetText().replace(/,/g, ''));
                        var v2 = parseFloat(values);
                        var vCal = v2 - v1;

                        if (vCal > 0) {
                            sShow = "+" + $.number(vCal, 0);
                        }
                        else {
                            sShow = $.number(vCal, 0);
                        }

                        lblNumNewPhaseCompartNo2_Case2.SetText($.number(values, 0));
                    }
                    else {
                        lblNumNewPhaseCompartNo2_Case2.SetText("");
                    }

                    lblDiffPhaseCompartNo2_Case2.SetText(sShow);

                }
                else {
                    lblDiffPhaseCompartNo2_Case2.SetText("");
                }
            }
        }

        function SetTimeEqual(ctr, type) {

            if (ctr == "1") {
                if (type == "H") {
                    cmbTime_WaterChecking_H.SetText(cmbTime_EndInnerChecking_H.GetText());

                }
                else {
                    cmbTime_WaterChecking_M.SetText(cmbTime_EndInnerChecking_M.GetText());
                }
            }
            else if (ctr == "2") {
                if (type == "H") {
                    cmbTime_StopWaterChecking_H.SetText(cmbTime_EndWaterChecking_H.GetText());

                }
                else {
                    cmbTime_StopWaterChecking_M.SetText(cmbTime_EndWaterChecking_M.GetText());
                }
            }
            else if (ctr == "3") {
                if (type == "H") {
                    cmbTime_ReturnWaterChecking_H.SetText(cmbTime_EndStopWaterChecking_H.GetText());

                }
                else {
                    cmbTime_ReturnWaterChecking_M.SetText(cmbTime_EndStopWaterChecking_M.GetText());
                }

            }
        }

        function isNumber(evt) {

            if (evt != null) {
                var iKeyCode = evt.keyCode;

                if (iKeyCode == '46' || (iKeyCode > 47 && iKeyCode < 58)) {

                }
                else {
                    evt.preventDefault();

                }

            }
        }

    </script>
    <script language="javascript" type="text/javascript">

        var sShow = "";
        function CalculateData(span) {

            if (txtNumNewPhase.GetValue() != null && txtNumNewPhase.GetValue() != "") {

                var vSumSlot = txtNumNewPhase.GetValue().replace(/,/g, ''); // ค่ารวม
                if ($.isNumeric(vSumSlot)) {

                    // ระยะรวม
                    if (lblNumOldPhase.GetText() != null && lblNumOldPhase.GetText() != "") {
                        var vOldSumSlot = lblNumOldPhase.GetText().replace(/,/g, '');
                        var v1 = parseFloat(vSumSlot);
                        var v2 = parseFloat(vOldSumSlot);
                        var vCal = v1 - v2;
                        if (vCal > 0) {
                            sShow = "+" + $.number(vCal, 0);
                        }
                        else {
                            sShow = $.number(vCal, 0);
                        }

                        lblNumDifferencePhase.SetText(sShow);
                    }

                    if (span == "1") {
                        if (txtNumNewPhaseCompartNo1_Case1.GetValue() != null && txtNumNewPhaseCompartNo1_Case1.GetValue() != "") {
                            var vNew = txtNumNewPhaseCompartNo1_Case1.GetValue().replace(/,/g, '');
                            if ($.isNumeric(vNew)) {
                                var v1 = parseFloat(vSumSlot);
                                var v2 = parseFloat(vNew);
                                var vCal = v1 - v2;
                                lblNumNewPhaseCompartNo1_Case2.SetText($.number(vCal, 0));
                                CalPan1("2");
                            }
                        }
                        else {
                            lblNumNewPhaseCompartNo1_Case2.SetText("");
                            lblDiffPhaseCompartNo1_Case2.SetText("");
                        }

                    }
                    else if (span == "2") {
                        if (txtNumNewPhaseCompartNo2_Case1.GetValue() != null && txtNumNewPhaseCompartNo2_Case1.GetValue() != "") {
                            var vNew = txtNumNewPhaseCompartNo2_Case1.GetValue().replace(/,/g, '');
                            if ($.isNumeric(vNew)) {
                                var v1 = parseFloat(vSumSlot);
                                var v2 = parseFloat(vNew);
                                var vCal = v1 - v2;
                                lblNumNewPhaseCompartNo2_Case2.SetText($.number(vCal, 0));
                                CalPan2("2");
                            }
                        }
                        else {
                            lblNumNewPhaseCompartNo2_Case2.SetText("");
                            lblDiffPhaseCompartNo2_Case2.SetText("");
                        }

                    }

                    txtNumNewPhase.SetText($.number(vSumSlot, 0));
                }
            }
            else {

                lblNumOldPhase.SetText("");

                lblNumNewPhaseCompartNo1_Case2.SetText("");
                lblDiffPhaseCompartNo1_Case2.SetText("");

                lblNumNewPhaseCompartNo2_Case2.SetText("");
                lblDiffPhaseCompartNo2_Case2.SetText("");
            }
        }


        function ClearValidationResultInContainer(container, validationGroup) {
            __aspxInvalidEditorElementToBeFocused = null;
            _aspxProcessEditorsInContainer(container, _aspxClearValidationProcessingProc,
        _aspxClearChoiceCondition, validationGroup);
        }


        _aspxClearValidationProcessingProc = function (edit) {
            edit.SetIsValid(true);
            return true;
        }
    </script>
    <script language="javascript" type="text/javascript">

        var nTotalTimeChecking_H = 0, nTotalTimeChecking_M = 0;

        /*คำนวณเวลา*/
        function CompareTime(vStartH, vStartM, vEndH, vEndM, sshow) {

            var timeStart = new Date(0, 0, 0, vStartH, vStartM, 0);
            var timeEnd = new Date(0, 0, 0, vEndH, vEndM, 0);
            var diff = timeEnd.getTime() - timeStart.getTime(),
                hours = Math.floor(diff / 1000 / 60 / 60);
            diff -= hours * 1000 * 60 * 60;
            var minutes = Math.floor(diff / 1000 / 60);

            var sShowH = hours; //(hours < 10 ? "0" : "") + hours; // + ":" + (minutes < 9 ? "0" : "") + minutes;
            var sShowM = minutes; //(minutes < 10 ? "0" : "") + minutes;

            var sShowAll = sShowH + "  ชม.  " + sShowM + "  นาที";

            switch (sshow) {
                case "1": lblTimeInnerCheckingH.SetText(sShowAll); break;
                case "2": lblTimeWaterCheckingH.SetText(sShowAll); break;
                case "3": lblStopWaterChecking_H.SetText(sShowAll); break;
                case "4": lblReturnWaterChecking_H.SetText(sShowAll); break;
            };

            CalculateTotalTime();

        }


        function CalculateTotalTime() {

            nTotalTimeChecking_H = 0;
            nTotalTimeChecking_M = 0;

            var sTimeDiff = "";
            var sTime_H1 = "", sTime_M1 = "";
            var NSTART_HOUR = "", NSTART_MINUTE = "", NEND_HOUR = "", NEND_MINUTE = "";
            var i = 0;
            for (i = 1; i <= 5; i++) {
                NSTART_HOUR = ""; NSTART_MINUTE = ""; NEND_HOUR = ""; NEND_MINUTE = "";
                switch (i) {
                    case 1:
                        NSTART_HOUR = cmbTimeCheckIn_H.GetValue();
                        NSTART_MINUTE = cmbTimeCheckIn_M.GetValue();
                        break;
                    case 2:
                        NSTART_HOUR = cmbTime_InnerChecking_H.GetValue();
                        NSTART_MINUTE = cmbTime_InnerChecking_M.GetValue();
                        NEND_HOUR = cmbTime_EndInnerChecking_H.GetValue();
                        NEND_MINUTE = cmbTime_EndInnerChecking_M.GetValue();
                        break;
                    case 3:
                        NSTART_HOUR = cmbTime_WaterChecking_H.GetValue();
                        NSTART_MINUTE = cmbTime_WaterChecking_M.GetValue();
                        NEND_HOUR = cmbTime_EndWaterChecking_H.GetValue();
                        NEND_MINUTE = cmbTime_EndWaterChecking_M.GetValue();
                        break;
                    case 4:
                        NSTART_HOUR = cmbTime_StopWaterChecking_H.GetValue();
                        NSTART_MINUTE = cmbTime_StopWaterChecking_M.GetValue();
                        NEND_HOUR = cmbTime_EndStopWaterChecking_H.GetValue();
                        NEND_MINUTE = cmbTime_EndStopWaterChecking_M.GetValue();
                        break;
                    case 5:
                        NSTART_HOUR = cmbTime_ReturnWaterChecking_H.GetValue();
                        NSTART_MINUTE = cmbTime_ReturnWaterChecking_M.GetValue();
                        NEND_HOUR = cmbTime_EndReturnWaterChecking_H.GetValue();
                        NEND_MINUTE = cmbTime_EndReturnWaterChecking_M.GetValue();
                        break;
                }

                sTimeDiff = CompareTimeTotal(NSTART_HOUR, NSTART_MINUTE, NEND_HOUR, NEND_MINUTE, "Y");
                switch (i) {
                    case 1: sTime_H1 = NSTART_HOUR;
                        sTime_M1 = NSTART_MINUTE;
                        break;
                    case 2: /*lblTimeInnerCheckingH.SetText(sTimeDiff);*/break;
                    case 3: /*lblTimeWaterCheckingH.SetText(sTimeDiff);*/break;
                    case 4: /*lblStopWaterChecking_H.SetText(sTimeDiff);*/break;
                    case 5: /*lblReturnWaterChecking_H.SetText(sTimeDiff);*/
                        lblnTotalTimeCheckingInArea.SetText(CompareTimeTotal(sTime_H1, sTime_M1, NEND_HOUR, NEND_MINUTE, "N"));
                        break;
                }
            }

            var hours = Math.floor(nTotalTimeChecking_M / 60);
            var minutes = nTotalTimeChecking_M % 60;

            nTotalTimeChecking_H = parseInt(nTotalTimeChecking_H) + parseInt(hours);
            nTotalTimeChecking_M = minutes;

            lblnTotalTimeChecking.SetText(nTotalTimeChecking_H + "  ชม.  " + nTotalTimeChecking_M + "  นาที");
        }

        function CompareTimeTotal(vStartH, vStartM, vEndH, vEndM, cSum) {

            var sResult = "";
            if (vStartH != "" && vStartM != "" && vEndH != "" && vEndM != "") {
                var timeStart = new Date(0, 0, 0, vStartH, vStartM, 0);
                var timeEnd = new Date(0, 0, 0, vEndH, vEndM, 0);
                var diff = timeEnd.getTime() - timeStart.getTime(),
                hours = Math.floor(diff / 1000 / 60 / 60);
                diff -= hours * 1000 * 60 * 60;
                var minutes = Math.floor(diff / 1000 / 60);

                var sShowH = hours;
                var sShowM = minutes;

                sResult = sShowH + "  ชม.  " + sShowM + "  นาที";

                if (cSum == "Y") {
                    nTotalTimeChecking_H += hours;
                    nTotalTimeChecking_M += minutes;
                }
            }

            return sResult;
        }

        function IsValidNumber() {
            //ตรวจเช็ค keyCode
            if ((event.keyCode >= 48) && (event.keyCode <= 57))
            { }
            else
            { event.returnValue = false; } //หรือ { event.keyCode = 0 ; }
        }
    </script>
    <style type="text/css">
        .ActiveSlot {
            background-color: #00CC33;
        }

        .InActiveSlot {
            background-color: #E5E5E5;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" HideContentOnCallback="false"
        CausesValidation="False" OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){if(s.cpValidate != undefined)HideValidate(); eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <ClientSideEvents EndCallback="function(s,e){if(s.cpValidate != undefined)HideValidate(); eval(s.cpPopup); s.cpPopup = &#39;&#39;;if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onclick="javascript:location.href='car_history.htm'">--%>
                                                    <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onclick="javascript:location.href='service_history.htm'">--%>
                                                    <dx:ASPxTextBox runat="server" ID="txtNversion" ClientVisible="false">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">วันที่ยืนคำขอ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQUEST_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">&nbsp;วันที่วัดน้ำครั้งล่าสุด</td>
                                                <td>
                                                    <%-- <dx:ASPxTextBox ID="lblDLAST_SERV" runat="server">
                                                    </dx:ASPxTextBox>    --%>


                                                    <dx:ASPxDateEdit runat="server" ID="lblDLAST_SERV" SkinID="xdte"
                                                        ClientInstanceName="edtCheck">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="18%" bgcolor="#B9EAEF">วันที่นัดหมาย
                                                </td>
                                                <td width="32%" class="active">
                                                    <dx:ASPxLabel ID="lblSERVICE_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="19%" bgcolor="#B9EAEF">&nbsp;วันที่หมดอายุวัดน้ำ</td>
                                                <td>
                                                    <dx:ASPxDateEdit ID="lblDateNextCheckWater" runat="server"
                                                        ClientInstanceName="edtCheck" SkinID="xdte">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ประเภทคำขอ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQTYPE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">วันที่ รข. อนุมัติคำขอ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblAPPOINTMENT_BY_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ทะเบียนรถ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREGISTERNO" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">สาเหตุ </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblCAUSE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">บริษัทขนส่ง
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblVendorName" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>

                                                <td bgcolor="#B9EAEF">ประเภทรถ
                                                </td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblTypeCar" runat="server">
                                                    </dx:ASPxLabel>
                                                    - ความจุ
                                                    <dx:ASPxLabel ID="lblTotalCap" runat="server">
                                                    </dx:ASPxLabel>
                                                    ลิตร
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">รหัสวัดน้ำ
                                                  
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox runat="server" ID="txtSCAR_NUM">
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td bgcolor="#B9EAEF">เลขที่เอกสาร
                                                  
                                                </td>
                                                <td>
                                                    <dx:ASPxTextBox runat="server" ID="txtDOCNO" AutoPostBack="true" OnValueChanged="txtDOCNO_ValueChanged">
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trtab">
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT1" runat="server" Text="ประวัติการตรวจ" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT1;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectT1;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT2;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectT2;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#00CC33">
                                                    <dx:ASPxHyperLink ID="hplT3" runat="server" Text="ผลตรวจลงน้ำ" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectT4;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectImg;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectImg;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="15%" align="center" bgcolor="#EDEDED">
                                                    <dx:ASPxHyperLink ID="hplT5" runat="server" Text="บันทึกผลปิดงาน" Style="text-decoration: none; cursor: pointer; color: Blue;">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT5;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectT5;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                                <td width="10%">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                            <tr>
                                                <td width="6%" align="left" bgcolor="#B9EAEF">หน่วย: มิลลิเมตร
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                        <tr>
                                                            <td width="12%" valign="top">
                                                                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                                                    <tr>
                                                                        <td align="center" id="tdSlot1" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot1" runat="server" ClientInstanceName="btnSlot1" Text="ช่องที่ 1"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('ChangeSlot;' + '1');}" />
                                                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;1&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot2" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot2" runat="server" ClientInstanceName="btnSlot2" Text="ช่องที่ 2"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '2');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;2&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot3" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot3" runat="server" ClientInstanceName="btnSlot3" Text="ช่องที่ 3"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '3');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;3&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot4" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot4" runat="server" ClientInstanceName="btnSlot4" Text="ช่องที่ 4"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '4');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;4&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot5" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot5" runat="server" ClientInstanceName="btnSlot5" Text="ช่องที่ 5"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '5');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;5&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot6" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot6" runat="server" ClientInstanceName="btnSlot6" Text="ช่องที่ 6"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '6');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;6&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot7" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot7" runat="server" ClientInstanceName="btnSlot7" Text="ช่องที่ 7"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '7');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;7&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot8" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot8" runat="server" ClientInstanceName="btnSlot8" Text="ช่องที่ 8"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '8');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;8&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot9" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot9" runat="server" ClientInstanceName="btnSlot9" Text="ช่องที่ 9"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '9');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;9&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" id="tdSlot10" runat="server">
                                                                            <dx:ASPxButton ID="btnSlot10" runat="server" ClientInstanceName="btnSlot10" Text="ช่องที่ 10"
                                                                                AutoPostBack="false" Width="99%">
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('ChangeSlot;' + '10');}" />
                                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback(&#39;ChangeSlot;&#39; + &#39;10&#39;);}"></ClientSideEvents>
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="88%" valign="top">
                                                                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                                    <tr>
                                                                        <td>พิกัดแป้นรวม :
                                                                            <dx:ASPxLabel ID="lblCapacityCompart" runat="server" CssClass="dxeLineBreakFix">
                                                                            </dx:ASPxLabel>
                                                                            ลิตร
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                                                                <tr>
                                                                                    <td width="33%">
                                                                                        <dx:ASPxCheckBox ID="ckbEmergencyValve" runat="server" ClientInstanceName="ckbEmergencyValve"
                                                                                            Text="วาล์วฉุกเฉิน">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td width="33%">
                                                                                        <dx:ASPxCheckBox ID="ckbRuinedSel1" runat="server" ClientInstanceName="ckbRuinedSel1"
                                                                                            Text="สภาพซีลฝา 1 ชำรุด">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td width="33%">
                                                                                        <dx:ASPxCheckBox ID="ckbRuinedSel2" runat="server" ClientInstanceName="ckbRuinedSel2"
                                                                                            Text="สภาพซีลฝา 2 ชำรุด">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="33%"></td>
                                                                                    <td width="33%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="120px">
                                                                                                    <dx:ASPxLabel runat="server" ID="lblmanhold1" Text="หมายเลขซีลฝา manhold 1 :" CssClass="dxeLineBreakFix">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dx:ASPxTextBox runat="server" ID="txtMANHOLD_NO1" Width="100px" MaxLength="10">
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="33%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="120px">
                                                                                                    <dx:ASPxLabel runat="server" ID="lblmanhold2" Text="หมายเลขซีลฝา manhold 2 :" CssClass="dxeLineBreakFix">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dx:ASPxTextBox runat="server" ID="txtMANHOLD_NO2" Width="100px" MaxLength="10">
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="33%">
                                                                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" ClientInstanceName="ASPxLabel1" Text="หมายเลขซีล แป้นเดิม">
                                                                                        </dx:ASPxLabel>
                                                                                        <dx:ASPxLabel ID="lblOldNoSel" runat="server" ClientInstanceName="lblOldNoSel">
                                                                                        </dx:ASPxLabel>
                                                                                        <dx:ASPxTextBox ID="txtOldNoSel" runat="server" ClientInstanceName="txtOldNoSel"
                                                                                            Width="100px" ClientVisible="false" Text="xx4454">
                                                                                        </dx:ASPxTextBox>
                                                                                    </td>
                                                                                    <td width="33%">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td width="120px">หมายเลขซีล แป้นใหม่
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dx:ASPxTextBox ID="txtNewNoSel" runat="server" ClientInstanceName="txtNewNoSel"
                                                                                                        Width="100px" MaxLength="10">
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="33%">
                                                                                        <dx:ASPxCheckBox ID="ckbOldSelRuine" runat="server" ClientInstanceName="ckbOldSelRuine"
                                                                                            Text="สภาพซีลเดิมชำรุด">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#E5E5E5">ระยะรวม (1)
                                                                                    </td>
                                                                                    <td height="25" colspan="3" align="center">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#E5E5E5">ระยะเดิม(มม.)
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#E5E5E5">ระยะใหม่(มม.)
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#E5E5E5">
                                                                                                    <p>
                                                                                                        ผลต่าง(มม.)
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="33%" align="center">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhase" runat="server" ClientInstanceName="lblNumOldPhase">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td width="33%" align="center">
                                                                                                    <dx:ASPxTextBox ID="txtNumNewPhase" runat="server" ClientInstanceName="txtNumNewPhase"
                                                                                                        Width="50%" HorizontalAlign="Right" AutoCompleteType="Disabled">
                                                                                                        <ClientSideEvents TextChanged="function(s,e) { CalculateData('1'); CalculateData('2'); }"
                                                                                                            KeyPress="function(){IsValidNumber() ;}" />
                                                                                                        <ClientSideEvents KeyPress="function(){IsValidNumber() ;}" TextChanged="function(s,e) { CalculateData(&#39;1&#39;); CalculateData(&#39;2&#39;); }"></ClientSideEvents>

                                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กำหนดระยะใหม่"
                                                                                                            RequiredField-IsRequired="true" ValidationGroup="add1" Display="Dynamic" ErrorTextPosition="Right">
                                                                                                            <RequiredField IsRequired="True" ErrorText="กำหนดระยะใหม่"></RequiredField>
                                                                                                        </ValidationSettings>
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                                <td width="33%" align="center">
                                                                                                    <dx:ASPxLabel ID="lblNumDifferencePhase" runat="server" ClientInstanceName="lblNumDifferencePhase">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="20%">&nbsp;
                                                                                    </td>
                                                                                    <td width="28%" height="25" align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="center" height="25" bgcolor="#0099FF">แป้นที่ 1
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="25%" align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td align="center" height="25" bgcolor="#33FF33">แป้นที่ 2
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="27%" align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tb31" runat="server">
                                                                                            <tr>
                                                                                                <td align="center" height="25" bgcolor="#0099FF">แป้นที่ 3 (แป้นเดิม)
                                                                                                </td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top" bgcolor="#E5E5E5">พิกัดแป้น(ลิตร)
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td height="24px" bgcolor="#F7F7F7" align="center">
                                                                                                    <dx:ASPxLabel ID="lblCapPan1" runat="server" ClientInstanceName="lblCapPan1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td height="24px" bgcolor="#F7F7F7" align="center">
                                                                                                    <dx:ASPxLabel ID="lblCapPan2" runat="server" ClientInstanceName="lblCapPan2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tb32" runat="server">
                                                                                            <tr>
                                                                                                <td height="24px" bgcolor="#F7F7F7" align="center">
                                                                                                    <dx:ASPxLabel ID="lblCapPan3" runat="server" ClientInstanceName="lblCapPan3">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top" bgcolor="#E5E5E5">ระยะห่างจากก้นถังถึงแป้น (2)
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo1_Case1" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo1_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxTextBox ID="txtNumNewPhaseCompartNo1_Case1" runat="server" ClientInstanceName="txtNumNewPhaseCompartNo1_Case1"
                                                                                                        Width="95%" HorizontalAlign="Right" AutoCompleteType="Disabled">
                                                                                                        <ClientSideEvents TextChanged="function(s,e){ CalPan1('1'); }" KeyPress="function(){IsValidNumber() ;}" />
                                                                                                        <ClientSideEvents KeyPress="function(){IsValidNumber() ;}" TextChanged="function(s,e){ CalPan1(&#39;1&#39;); }"></ClientSideEvents>

                                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กำหนดระยะใหม่"
                                                                                                            RequiredField-IsRequired="true" ValidationGroup="add1" ErrorTextPosition="Right"
                                                                                                            Display="Dynamic">
                                                                                                            <RequiredField IsRequired="True" ErrorText="กำหนดระยะใหม่"></RequiredField>
                                                                                                        </ValidationSettings>
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo1_Case1" runat="server" ClientInstanceName="lblDiffPhaseCompartNo1_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo2_Case1" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo2_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxTextBox ID="txtNumNewPhaseCompartNo2_Case1" runat="server" ClientInstanceName="txtNumNewPhaseCompartNo2_Case1"
                                                                                                        Width="95%" HorizontalAlign="Right" AutoCompleteType="Disabled">
                                                                                                        <ClientSideEvents TextChanged="function(s,e){ CalPan2('1'); }" KeyPress="function(){IsValidNumber() ;}" />
                                                                                                        <ClientSideEvents KeyPress="function(){IsValidNumber() ;}" TextChanged="function(s,e){ CalPan2(&#39;1&#39;); }"></ClientSideEvents>

                                                                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="กำหนดระยะใหม่"
                                                                                                            RequiredField-IsRequired="true" ValidationGroup="add1" ErrorTextPosition="Right"
                                                                                                            Display="Dynamic">
                                                                                                            <RequiredField IsRequired="True" ErrorText="กำหนดระยะใหม่"></RequiredField>
                                                                                                        </ValidationSettings>
                                                                                                    </dx:ASPxTextBox>
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo2_Case1" runat="server" ClientInstanceName="lblDiffPhaseCompartNo2_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1" id="tb33" runat="server">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo3_Case1" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo3_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumNewPhaseCompartNo3_Case1" runat="server" ClientInstanceName="lblNumNewPhaseCompartNo3_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo3_Case1" runat="server" ClientInstanceName="lblDiffPhaseCompartNo3_Case1">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" valign="top" bgcolor="#E5E5E5">ระยะจากปากถังถึงแป้น<br>
                                                                                        (1) - (2)
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo1_Case2" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo1_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumNewPhaseCompartNo1_Case2" runat="server" ClientInstanceName="lblNumNewPhaseCompartNo1_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo1_Case2" runat="server" ClientInstanceName="lblDiffPhaseCompartNo1_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#33FF33">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo2_Case2" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo2_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumNewPhaseCompartNo2_Case2" runat="server" ClientInstanceName="lblNumNewPhaseCompartNo2_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo2_Case2" runat="server" ClientInstanceName="lblDiffPhaseCompartNo2_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="1" id="tb34" runat="server">
                                                                                            <tr>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะเดิม
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">ระยะใหม่
                                                                                                </td>
                                                                                                <td width="33%" align="center" bgcolor="#0099FF">
                                                                                                    <p>
                                                                                                        ผลต่าง
                                                                                                    </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumOldPhaseCompartNo3_Case2" runat="server" ClientInstanceName="lblNumOldPhaseCompartNo3_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblNumNewPhaseCompartNo3_Case2" runat="server" ClientInstanceName="lblNumNewPhaseCompartNo3_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                                <td height="21" width="33%" align="center" bgcolor="#F7F7F7">
                                                                                                    <dx:ASPxLabel ID="lblDiffPhaseCompartNo3_Case2" runat="server" ClientInstanceName="lblDiffPhaseCompartNo3_Case2">
                                                                                                    </dx:ASPxLabel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="display: none">
                                                                                    <td align="center" bgcolor="#E5E5E5">น้ำท่วมแป้น
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <dx:ASPxCheckBox ID="ckbWaterOverPlant1" runat="server" ClientInstanceName="ckbWaterOverPlant1">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <dx:ASPxCheckBox ID="ckbWaterOverPlant2" runat="server" ClientInstanceName="ckbWaterOverPlant2">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="display: none">
                                                                                    <td align="center" bgcolor="#E5E5E5">น้ำขาดแป้น
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <dx:ASPxCheckBox ID="ckbWaterLackPlant1" runat="server" ClientInstanceName="ckbWaterLackPlant1">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <dx:ASPxCheckBox ID="ckbWaterLackPlant2" runat="server" ClientInstanceName="ckbWaterLackPlant2">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#E5E5E5">สภาพแป้นชำรุด
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7" colspan="2" align="left">
                                                                                        <dx:ASPxCheckBox runat="server" ID="chkPandefective" Text="สภาพแป้นชำรุด">
                                                                                        </dx:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <%-- <dx:ASPxRadioButtonList ID="ASPxRadioButtonList2" runat="server" ClientInstanceName="rblStatusP3"
                                                                                            RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                                            <Items>
                                                                                                <dx:ListEditItem Text="ผ่าน" Value="1" />
                                                                                                <dx:ListEditItem Text="ไม่ผ่าน" Value="0" />
                                                                                            </Items>
                                                                                        </dx:ASPxRadioButtonList>--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#E5E5E5">ผ่านเกณฑ์มาตรฐาน
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7" colspan="2" align="left">
                                                                                        <dx:ASPxRadioButtonList ID="rblStatus" runat="server" ClientInstanceName="rblStatus"
                                                                                            RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                                            <Items>
                                                                                                <dx:ListEditItem Text="ผ่าน" Value="1" />
                                                                                                <dx:ListEditItem Text="ไม่ผ่าน" Value="0" />
                                                                                            </Items>
                                                                                        </dx:ASPxRadioButtonList>
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#F7F7F7">
                                                                                        <dx:ASPxRadioButtonList ID="rblStatusP3" runat="server" ClientInstanceName="rblStatusP3"
                                                                                            RepeatDirection="Horizontal" SkinID="rblStatus">
                                                                                            <Items>
                                                                                                <dx:ListEditItem Text="ผ่าน" Value="1" />
                                                                                                <dx:ListEditItem Text="ไม่ผ่าน" Value="0" />
                                                                                            </Items>
                                                                                        </dx:ASPxRadioButtonList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="4" align="center">
                                                                                        <dx:ASPxButton ID="btnSaveThisSlot" runat="server" ClientInstanceName="btnSaveThisSlot"
                                                                                            Text="บันทึกช่องนี้" AutoPostBack="false">
                                                                                            <ClientSideEvents Click="function(s,e) {if(!ASPxClientEdit.ValidateGroup('add1')) return false;  xcpn.PerformCallback('SAVETHISSLOT;'); }" />
                                                                                            <ClientSideEvents Click="function(s,e) {if(!ASPxClientEdit.ValidateGroup(&#39;add1&#39;)) return false;  xcpn.PerformCallback(&#39;SAVETHISSLOT;&#39;); }"></ClientSideEvents>
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" bgcolor="#B9EAEF">เวลาปฏิบัติงาน
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="24%">เวลาเข้าพื้นที่หอวัดน้ำ:
                                                            </td>
                                                            <td width="19%">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTimeCheckIn_H" runat="server" ClientInstanceName="cmbTimeCheckIn_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function(s,e) { CalculateTotalTime(); }" />
                                                                                <ClientSideEvents ValueChanged="function(s,e) { CalculateTotalTime(); }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTimeCheckIn_M" runat="server" ClientInstanceName="cmbTimeCheckIn_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function(s,e) { CalculateTotalTime(); }" />
                                                                                <ClientSideEvents ValueChanged="function(s,e) { CalculateTotalTime(); }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="15%">&nbsp;
                                                            </td>
                                                            <td width="18%">&nbsp;
                                                            </td>
                                                            <td width="10%">&nbsp;
                                                            </td>
                                                            <td width="14%">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">เวลาตรวจสภาพภายนอก/ใน:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_InnerChecking_H" runat="server" ClientInstanceName="cmbTime_InnerChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),'1');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),&#39;1&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_InnerChecking_M" runat="server" ClientInstanceName="cmbTime_InnerChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),'1');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),&#39;1&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">เวลาเสร็จสิ้น:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndInnerChecking_H" runat="server" ClientInstanceName="cmbTime_EndInnerChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('1','H')  }" ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),'1');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;1&#39;,&#39;H&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),&#39;1&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndInnerChecking_M" runat="server" ClientInstanceName="cmbTime_EndInnerChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('1','T')  }" ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),'1');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;1&#39;,&#39;T&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_InnerChecking_H.GetValue(),cmbTime_InnerChecking_M.GetValue(),cmbTime_EndInnerChecking_H.GetValue(),cmbTime_EndInnerChecking_M.GetValue(),&#39;1&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">เวลาที่ใช้:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxLabel ID="lblTimeInnerCheckingH" runat="server" ClientInstanceName="lblTimeInnerCheckingH">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>เวลาเริ่มต้นลงน้ำ:
                                                            </td>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_WaterChecking_H" runat="server" ClientInstanceName="cmbTime_WaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),'2');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),&#39;2&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_WaterChecking_M" runat="server" ClientInstanceName="cmbTime_WaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),'2');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),&#39;2&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>เวลาเสร็จสิ้น:
                                                            </td>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndWaterChecking_H" runat="server" ClientInstanceName="cmbTime_EndWaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('2','H')  }" ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),'2');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;2&#39;,&#39;H&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),&#39;2&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndWaterChecking_M" runat="server" ClientInstanceName="cmbTime_EndWaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('2','T')  }" ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),'2');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;2&#39;,&#39;T&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_WaterChecking_H.GetValue(),cmbTime_WaterChecking_M.GetValue(),cmbTime_EndWaterChecking_H.GetValue(),cmbTime_EndWaterChecking_M.GetValue(),&#39;2&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>เวลาที่ใช้:
                                                            </td>
                                                            <td>
                                                                <dx:ASPxLabel ID="lblTimeWaterCheckingH" runat="server" ClientInstanceName="lblTimeWaterCheckingH">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F7F7F7">สูบน้ำคืนกลับ:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_StopWaterChecking_H" runat="server" ClientInstanceName="cmbTime_StopWaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),'3');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),&#39;3&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_StopWaterChecking_M" runat="server" ClientInstanceName="cmbTime_StopWaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),'3');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),&#39;3&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">เวลาเสร็จสิ้น:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndStopWaterChecking_H" runat="server" ClientInstanceName="cmbTime_EndStopWaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('3','H')  }" ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),'3');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;3&#39;,&#39;H&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),&#39;3&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndStopWaterChecking_M" runat="server" ClientInstanceName="cmbTime_EndStopWaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual('3','T')  }" ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),'3');  }" />
                                                                                <ClientSideEvents TextChanged="function(s,e) { SetTimeEqual(&#39;3&#39;,&#39;T&#39;)  }" ValueChanged="function() {  CompareTime(cmbTime_StopWaterChecking_H.GetValue(),cmbTime_StopWaterChecking_M.GetValue(),cmbTime_EndStopWaterChecking_H.GetValue(),cmbTime_EndStopWaterChecking_M.GetValue(),&#39;3&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td bgcolor="#F7F7F7">เวลาที่ใช้:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxLabel ID="lblStopWaterChecking_H" runat="server" ClientInstanceName="lblStopWaterChecking_H">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>วัดแป้น:
                                                            </td>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_ReturnWaterChecking_H" runat="server" ClientInstanceName="cmbTime_ReturnWaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),'4');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),&#39;4&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_ReturnWaterChecking_M" runat="server" ClientInstanceName="cmbTime_ReturnWaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),'4');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),&#39;4&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>เวลาเสร็จสิ้น:
                                                            </td>
                                                            <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndReturnWaterChecking_H" runat="server" ClientInstanceName="cmbTime_EndReturnWaterChecking_H"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nHour">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),'4');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),&#39;4&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left" width="4px">:
                                                                        </td>
                                                                        <td align="left" width="50px">
                                                                            <dx:ASPxComboBox ID="cmbTime_EndReturnWaterChecking_M" runat="server" ClientInstanceName="cmbTime_EndReturnWaterChecking_M"
                                                                                CssClass="dxeLineBreakFix" Width="50px" TextField="sTXT" ValueField="nMinute">
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),'4');  }" />
                                                                                <ClientSideEvents ValueChanged="function() {  CompareTime(cmbTime_ReturnWaterChecking_H.GetValue(),cmbTime_ReturnWaterChecking_M.GetValue(),cmbTime_EndReturnWaterChecking_H.GetValue(),cmbTime_EndReturnWaterChecking_M.GetValue(),&#39;4&#39;);  }"></ClientSideEvents>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                        <td align="left">น.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>เวลาที่ใช้:
                                                            </td>
                                                            <td>
                                                                <dx:ASPxLabel ID="lblReturnWaterChecking_H" runat="server" ClientInstanceName="lblReturnWaterChecking_H">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" align="right" bgcolor="#F7F7F7">รวมเวลาการตรวจสอบ:
                                                            </td>
                                                            <td bgcolor="#F7F7F7">
                                                                <dx:ASPxLabel ID="lblnTotalTimeChecking" runat="server" ClientInstanceName="lblnTotalTimeChecking">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" align="right" class="corp">รวมเวลาที่รถอยู่ในพื้นที่:
                                                            </td>
                                                            <td class="corp">
                                                                <dx:ASPxLabel ID="lblnTotalTimeCheckingInArea" runat="server" ClientInstanceName="lblnTotalTimeCheckingInArea">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                        <tr>
                                                            <td align="left" bgcolor="#E5E5E5" width="20%">ความสูงจากพื้นถึงท้อง/หลังถัง ที่หัว (มม.)
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxTextBox runat="server" ID="txtNTANK_HIGHT_HEAD">
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }"></ClientSideEvents>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" bgcolor="#E5E5E5" width="20%">ความสูงจากพื้นถึงท้อง/หลังถัง ที่หาง (มม.)
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxTextBox runat="server" ID="txtNTANK_HIGHT_TAIL">
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }"></ClientSideEvents>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" bgcolor="#E5E5E5" width="20%">สรุปผลตรวจวัดน้ำ
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxRadioButtonList ID="rblStatusCheckingWater" runat="server" ClientInstanceName="rblStatusCheckingWater"
                                                                    SkinID="rblStatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="ผ่าน" Selected="true" />
                                                                        <dx:ListEditItem Value="N" Text="ไม่ผ่านโดยสมบูรณ์และต้องนัดหมายใหม่" />
                                                                    </Items>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" bgcolor="#E5E5E5" width="20%">วันที่ตรวจ
                                                            </td>
                                                            <td>
                                                                <dx:ASPxDateEdit runat="server" ID="edtCheck" SkinID="xdte" ClientInstanceName="edtCheck">
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุวันที่ตรวจ"
                                                                        RequiredField-IsRequired="true" ValidationGroup="add">
                                                                        <RequiredField IsRequired="True" ErrorText="ระบุวันที่ตรวจ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" bgcolor="#E5E5E5" width="20%">ผู้ตรวจ
                                                            </td>
                                                            <td align="left">
                                                                <dx:ASPxComboBox ID="cmbUser" runat="server" ClientInstanceName="cmbUser" Width="300px"
                                                                    TextFormatString="{0}" CallbackPageSize="30" EnableCallbackMode="True" CssClass="dxeLineBreakFix"
                                                                    SkinID="xcbbATC" ValueField="SUID" OnItemsRequestedByFilterCondition="cmbUser_ItemsRequestedByFilterCondition"
                                                                    OnItemRequestedByValue="cmbUser_ItemRequestedByValue">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="ชือ - สกุล" FieldName="SFULLNAME" Width="300px" />
                                                                    </Columns>
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุผู้ตรวจ"
                                                                        RequiredField-IsRequired="true" ValidationGroup="add">
                                                                        <RequiredField IsRequired="True" ErrorText="ระบุผู้ตรวจ"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sdsEmpWorker" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" bgcolor="#E5E5E5">ความคิดเห็นผู้ตรวจ
                                                            </td>
                                                            <td align="left">
                                                                <table width="100%" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxMemo ID="txtComment" runat="server" ClientInstanceName="txtComment" Height="100"
                                                                                Width="400">
                                                                            </dx:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxCheckBox ID="ckbSendMailToVendor" runat="server" ClientInstanceName="ckbSendToVender"
                                                                                Text="ส่งเมล์แจ้งผู้ขนส่ง">
                                                                            </dx:ASPxCheckBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <dx:ASPxButton ID="btnSaveT3" runat="server" ClientInstanceName="btnSaveT3" AutoPostBack="false"
                                                        Text="บันทึกข้อมูล" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e) {   if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('SAVET3;'); }" />
                                                        <ClientSideEvents Click="function(s,e) {   if(!ASPxClientEdit.ValidateGroup(&#39;add&#39;)) return false; xcpn.PerformCallback(&#39;SAVET3;&#39;); }"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnPrintT3" runat="server" ClientInstanceName="btnPrintT3" AutoPostBack="true"
                                                        ValidationGroup="add" Text="พิมพ์ใบรายงานวัดน้ำ" CssClass="dxeLineBreakFix" OnClick="btnPrintT3_Click">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnNopass" runat="server" AutoPostBack="true" ValidationGroup="add"
                                                        Text="พิมพ์ใบรายงานไม่ผ่านวัดน้ำ" CssClass="dxeLineBreakFix" OnClick="btnNopass_Click">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnPrintTCompare" runat="server" ClientInstanceName="btnPrintTCompare"
                                                        AutoPostBack="true" Text="พิมพ์ใบรายงานเปรียบเทียบแป้น" CssClass="dxeLineBreakFix"
                                                        OnClick="btnPrintTCompare_Click">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnReportTeabapn" runat="server" ClientInstanceName="btnReportTeabapn"
                                                        Text="พิมพ์ใบเทียบแป้น" CssClass="dxeLineBreakFix" OnClick="btnReportTeabapn_Click">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnRedirectT4" runat="server" ClientInstanceName="btnRedirectT4"
                                                        AutoPostBack="false" Text="คำนวณค่าธรรมเนียมเพิ่มเติม" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" />
                                                        <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback(&#39;RedirectT4;&#39;)}"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <%--<dx:ASPxButton ID="bntExitT3" runat="server" ClientInstanceName="bntExitT3" AutoPostBack="false"
                                                        Text="ออกจากหน้านี้" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e) {  window.location = 'approve_mv.aspx';}" />
                                                    </dx:ASPxButton>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField runat="server" ID="hidHEAD" />
                <asp:HiddenField runat="server" ID="hidDETAIL" />
                <asp:HiddenField runat="server" ID="hidDOCNO" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
