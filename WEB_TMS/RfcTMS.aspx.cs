﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.Configuration;
using SAP.Middleware.Connector;
using System.IO;

public partial class RfcTMS : System.Web.UI.Page
{
    string ORA_ConnectingString = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
//        int RecordPerAround = int.Parse(ConfigurationSettings.AppSettings["RFC_RecordPerAround"] + "");
//        string IP_DO = "", OP_DO = "", IS_TRY = "0";
//        DataTable dt = new DataTable();
//        SAPSystemConnect SapConfig = new SAPSystemConnect();
//        try
//        {
//            try
//            {
//                RfcDestinationManager.RegisterDestinationConfiguration(SapConfig);
//            }
//            catch (Exception ectRegisterDestinationConfiguration) { }

//            RfcDestination rfcDest = RfcDestinationManager.GetDestination("TMS");
//            RfcRepository repo = rfcDest.Repository;
//            IRfcFunction ShipmentList = repo.CreateFunction("ZISTDI052");//

//            using (OracleConnection ora_conn = new OracleConnection(ORA_ConnectingString))
//            {
//                string[] ArrayString_IP; DataTable dtIP;
//                if (ora_conn.State == ConnectionState.Closed) ora_conn.Open();
//                if (ConfigurationSettings.AppSettings["RFC_IsCallMode"] + "" == "1")
//                {
//                    dtIP = FunctionUtilities.Get_Data(ora_conn, @"SELECT DELIVERY_NO ,TRUNC(DELIVERY_DATE)  DELIVERY_DATE
//FROM TDELIVERY 
//WHERE NVL(RFC_FLAG,'0')='0'
//AND rownum <= 50000   
//AND TRUNC(DELIVERY_DATE) >= TRUNC(SYSDATE-7 ) AND TRUNC(DELIVERY_DATE) <= TRUNC(SYSDATE)
//GROUP BY DELIVERY_NO ,TRUNC(DELIVERY_DATE) ");
//                    //ltrLog.Text += "InPut Records : " + dtIP.Rows.Count;

//                    ArrayString_IP = new string[dtIP.Rows.Count];
//                    for (int idxArray = 0; idxArray < dtIP.Rows.Count; idxArray++) ArrayString_IP[idxArray] = "" + dtIP.Rows[idxArray]["DELIVERY_NO"];

//                }
//                else
//                {
//                    ArrayString_IP = new string[300];
//                    /*"0080000012", "0080000014", "0080000021", "0080000028", "0080000029", "0080000031", "7000000035", "7000000036", "7000000042", "7000000084", "7000000090", "7000000113", "7000000118", "7000000119", "7000000120", "7000000121", "7000000122", "7000000135", "7000000285", "7000000748", "7000000750", "7000000751", "7000000752", "7000000753", "7000000756", "7000000971", "7000000992", "0080000026", "7000000094", "7000001598", "7000001449", "0080000262", "0080000350"
//                    * "80000178","80000181","80000182","80000184","80000185","80000186","80000437","80000720","80000922","80000924","80000927","80000988","80001135","80001136","80001160","80001211","80001228","7000000276","7000000348","7000000352","7000000353","7000000354","7000000357","7000000374","7000000375","7000000377","7000000398","7000000626","7000000704","7000000712","7000000743","7000000744","7000000952","7000001027","7000001360","7000001409","7000002045","7000002058","7000002062","7000002065","7000002254","7000002292","7000002295","7000002296","7000002297","7000002298","7000002299","7000002302","7000002314","7000002341","7000002343","7000002371","7000002386","7000002580","7000002607","7000002608","7000002665","7000002884","7000002887","7000003014","7000003030","7000003031","7000003034","7000003070","7000003074","7000003082","7000003090","7000003222","7000003234" */
//                    ArrayString_IP = new string[] { "0080000178", "0080000181", "0080000182", "0080000184", "0080000185", "0080000186", "0080000437", "0080000720"
//                        , "0080000922", "0080000924", "0080000927", "0080000988", "0080001135", "0080001136", "0080001160", "0080001211", "0080001228", "7000000276"
//                        , "7000000348", "7000000352", "7000000353", "7000000354", "7000000357", "7000000374", "7000000375", "7000000377", "7000000398", "7000000626"
//                        , "7000000704", "7000000712", "7000000743", "7000000744", "7000000952", "7000001027", "7000001360", "7000001409", "7000002045", "7000002058"
//                        , "7000002062", "7000002065", "7000002254", "7000002292", "7000002295", "7000002296", "7000002297", "7000002298", "7000002299", "7000002302"
//                        , "7000002314", "7000002341", "7000002343", "7000002371", "7000002386", "7000002580", "7000002607", "7000002608", "7000002665", "7000002884"
//                        , "7000002887", "7000003014", "7000003030", "7000003031", "7000003034", "7000003070", "7000003074", "7000003082", "7000003090", "7000003222"
//                        , "7000003234" };
//                }
//                int nMaxLoop = (ArrayString_IP.Length / RecordPerAround) + ((ArrayString_IP.Length % RecordPerAround) > 0 ? 1 : 0);
//                //ltrLog.Text += "<hr>nLoop: " + nMaxLoop + " ArrayString_IP.Length: " + ArrayString_IP.Length;
//                int Record = 0;
//                for (int nLoop = 0; nLoop < nMaxLoop; nLoop++)
//                {///strat loop Call function
//                    IP_DO = "";
//                    IRfcTable IP_IRfcTable = ShipmentList.GetTable(0);
//                    int nLastIndex = (((nLoop + 1) * RecordPerAround) > ArrayString_IP.Length) ? ArrayString_IP.Length : ((nLoop + 1) * RecordPerAround);
//                    IP_IRfcTable.Clear();
//                    //ltrLog.Text += "<br><hr>Loop(" + nLoop + ") " + Record + "-" + nLastIndex + " IP_IRfcTable.RowCount: " + IP_IRfcTable.RowCount;
//                    for (int nIndex = Record; nIndex < nLastIndex; nIndex++)
//                    {///loop set IRfcTable Parameter
//                        IP_IRfcTable.Append();
//                        IP_IRfcTable.SetValue("VBELN", "" + ArrayString_IP[nIndex]);
//                        IP_DO += "," + ArrayString_IP[nIndex];
//                        Record += 1;
//                    }
//                    //ltrLog.Text += "<br>" + IP_DO; //dgdRFC.DataSource = FunctionUtilities.GetDataTableFromRFCTable(IP_IRfcTable); //dgdRFC.DataBind();
//                    ///Set Parameter to Function
//                    ShipmentList.SetValue(0, IP_IRfcTable);
//                    RfcSessionManager.BeginContext(rfcDest);
//                    ///Call Function
//                    ShipmentList.Invoke(rfcDest);
//                    //Get Output
//                    IRfcTable OP_IRfcTable = ShipmentList.GetTable(ShipmentList.Count - 1);
//                    DataTable OP_DataTable = CommonFunction.GroupDatable("OP_GroupDataTable", FunctionUtilities.GetDataTableFromRFCTable(OP_IRfcTable), "DELIVERY_NO,SHIP_TO,SUM(DLV) DLV", "", "DELIVERY_NO,SHIP_TO,SHIPMENT_STS");
//                    //dgdResult.DataSource = FunctionUtilities.GetDataTableFromRFCTable(OP_IRfcTable); //dgdResult.DataBind(); //ltrLog.Text += "<hr>Output Records : " + OP_IRfcTable.RowCount; //return;
//                    int liElement = 0;
//                    OP_DO = "";

//                    #region loop OP_IRfcTable for add/Edit
//                    foreach (IRfcStructure Row in OP_IRfcTable)
//                    {
//                        OP_DO += ",[" + Row.GetString("DELIVERY_NO") + "]";
//                        if (!string.IsNullOrEmpty("" + Row.GetString("SHIPMENT_NO")) && !string.IsNullOrEmpty("" + Row.GetString("SHIPMENT_ITEM"))
//                            && !string.IsNullOrEmpty("" + Row.GetString("DELIVERY_NO")) && !string.IsNullOrEmpty("" + Row.GetString("DELIVERY_ITEM"))
//                            && (Row.GetString("SHIPMENT_STS").Equals("4") || Row.GetString("SHIPMENT_STS").Equals("6")))
//                        {// ถ้า SHIPMENT_NO SHIPMENT_ITEM DELIVERY_NO DELIVERY_ITEM มีค่า และ status in 4 6
//                            #region ถ้า SHIPMENT_NO SHIPMENT_ITEM DELIVERY_NO DELIVERY_ITEM มีค่า และ status in 4 6
//                            System.Data.OracleClient.OracleCommand oraCmd = new System.Data.OracleClient.OracleCommand("IUTSHIPMENT", ora_conn);
//                            oraCmd.CommandType = CommandType.StoredProcedure;
//                            oraCmd.Parameters.AddWithValue("I_SHIPMENT_NO", "" + Row.GetString("SHIPMENT_NO"));
//                            oraCmd.Parameters.AddWithValue("I_SHIPMENT_ITEM", "" + Row.GetString("SHIPMENT_ITEM"));
//                            oraCmd.Parameters.AddWithValue("I_DELIVERY_NO", "" + Row.GetString("DELIVERY_NO"));
//                            oraCmd.Parameters.AddWithValue("I_DELIVERY_ITEM", "" + Row.GetString("DELIVERY_ITEM"));
//                            oraCmd.Parameters.AddWithValue("I_CUSTOMER_NO", "" + (Row.GetString("CUSTOMER_NO").Equals("") ? Row.GetString("SHIP_TO") : Row.GetString("CUSTOMER_NO")));
//                            oraCmd.Parameters.AddWithValue("I_SHIP_TO", "" + Row.GetString("SHIP_TO"));
//                            oraCmd.Parameters.AddWithValue("I_PLANT", "" + Row.GetString("PLANT"));
//                            oraCmd.Parameters.AddWithValue("I_MATERIAL_NO", "" + Row.GetString("MATERIAL_NO"));
//                            oraCmd.Parameters.AddWithValue("I_MAT_FRT_GRP", "" + Row.GetString("MAT_FRT_GRP"));
//                            oraCmd.Parameters.AddWithValue("I_LOAD_CONF", "" + Row.GetString("LOAD_CONF"));
//                            oraCmd.Parameters.AddWithValue("I_LOAD_CONF_UNIT", "" + Row.GetString("LOAD_CONF_UNIT"));
//                            oraCmd.Parameters.AddWithValue("I_LOAD_BASE", "" + Row.GetString("LOAD_BASE"));
//                            oraCmd.Parameters.AddWithValue("I_LOAD_BASE_UNIT", "" + Row.GetString("LOAD_BASE_UNIT"));
//                            oraCmd.Parameters.AddWithValue("I_DLV", "" + Row.GetString("DLV"));
//                            oraCmd.Parameters.AddWithValue("I_DLV_UNIT", "" + Row.GetString("DLV_UNIT"));
//                            oraCmd.Parameters.AddWithValue("I_DLV_BASE", "" + Row.GetString("DLV_BASE"));
//                            oraCmd.Parameters.AddWithValue("I_DLV_BASE_UNIT", "" + Row.GetString("DLV_BASE_UNIT"));
//                            oraCmd.Parameters.AddWithValue("I_RETURN_FLAG", "" + Row.GetString("RETURN_FLAG"));
//                            oraCmd.Parameters.AddWithValue("I_VEH_HEAD", "" + Row.GetString("VEH_HEAD"));
//                            oraCmd.Parameters.AddWithValue("I_VEH_TRAIL", (("" + Row.GetString("VEH_HEAD") == "" + Row.GetString("VEH_TRAIL")) ? "" : "" + Row.GetString("VEH_TRAIL")));
//                            oraCmd.Parameters.AddWithValue("I_SHIPMET_STS", "" + Row.GetString("SHIPMENT_STS"));

//                            int res = oraCmd.ExecuteNonQuery();
//                            //                            OracleCommand oraCmd = new OracleCommand(@"INSERT INTO TSHIPMENT ( SHIPMENT_NO, SHIPMENT_ITEM, DELIVERY_NO, DELIVERY_ITEM, 
//                            //CUSTOMER_NO, SHIP_TO, PLANT, MATERIAL_NO, 
//                            //MAT_FRT_GRP, LOAD_CONF, LOAD_CONF_UNIT, LOAD_BASE, LOAD_BASE_UNIT, DLV, DLV_UNIT, DLV_BASE, DLV_BASE_UNIT, RETURN_FLAG, VEH_HEAD, VEH_TRAIL, SHIPMET_STS ) 
//                            // VALUES ( '" + Row.GetString("SHIPMENT_NO") + "', '" + Row.GetString("SHIPMENT_ITEM") + "', '" + Row.GetString("DELIVERY_NO") + "', '" + Row.GetString("DELIVERY_ITEM") + @"', 
//                            //'" + (Row.GetString("CUSTOMER_NO").Equals("") ? Row.GetString("SHIP_TO") : Row.GetString("CUSTOMER_NO")) + "', '" + Row.GetString("SHIP_TO") + "', '" + Row.GetString("PLANT") + "', '" + Row.GetString("MATERIAL_NO") + @"', 
//                            //'" + Row.GetString("MAT_FRT_GRP") + "', '" + Row.GetString("LOAD_CONF") + "', '" + Row.GetString("LOAD_CONF_UNIT") + "', '" + Row.GetString("LOAD_BASE") + @"', 
//                            //'" + Row.GetString("LOAD_BASE_UNIT") + "', '" + Row.GetString("DLV") + "', '" + Row.GetString("DLV_UNIT") + "', '" + Row.GetString("DLV_BASE") + @"', 
//                            //'" + Row.GetString("DLV_BASE_UNIT") + "', '" + Row.GetString("RETURN_FLAG") + "', '" + Row.GetString("VEH_HEAD") + "', '" + Row.GetString("VEH_TRAIL") + @"', 
//                            //'" + Row.GetString("SHIPMENT_STS") + "'  )", ora_conn);
//                            //                            oraCmd.ExecuteNonQuery();

//                            //                            OracleCommand OraCmdUdp = new OracleCommand("UPDATE TDELIVERY SET RFC_FLAG='" + ((Row.GetString("SHIPMENT_STS").Equals("6")) ? "1" : "0") + "' WHERE  DELIVERY_NO='" + Row.GetString("DELIVERY_NO") + "'", ora_conn);
//                            //                            OraCmdUdp.ExecuteNonQuery();
//                            #endregion
//                        }
//                        else
//                        {
//                            #region SHIPMENT_STS in 1 2
//                            switch (Row.GetString("SHIPMENT_STS"))
//                            {
//                                case "1":
//                                case "2":
//                                    string SHIP2 = "";
//                                    foreach (DataRow _dr in OP_DataTable.Select("DELIVERY_NO='" + Row.GetString("DELIVERY_NO") + "'"))
//                                    {
//                                        //ltrLog.Text += "<br>UPDATE TDELIVERY SET SHIP_TO='" + _dr["SHIP_TO"] + "' ,OTHER='" + _dr["DLV"] + "' WHERE DELIVERY_NO='" + _dr["DELIVERY_NO"] + "'";
//                                        OracleCommand OraCmdUdp = new OracleCommand("UPDATE TDELIVERY SET SHIP_TO='" + _dr["SHIP_TO"] + "' ,OTHER='" + _dr["DLV"] + "' WHERE DELIVERY_NO='" + _dr["DELIVERY_NO"] + "' ", ora_conn);
//                                        OraCmdUdp.ExecuteNonQuery();
//                                    }
//                                    break;
//                                default: break;
//                            }
//                            #endregion
//                        }
//                    }//end loop ADD/Update with OP_IRfcTable
//                    #endregion
//                    //ltrLog.Text += "<hr /> OP List : " + OP_DO;
//                    OP_IRfcTable.Clear();
//                    RfcSessionManager.EndContext(rfcDest);
//                }//end loop Call function
//                try
//                { RfcDestinationManager.UnregisterDestinationConfiguration(SapConfig); }
//                catch (Exception ectUnregisterDestinationConfiguration) { }

//            }// END OracleConnection
//            IS_TRY = "1";
//        }
//        catch (Exception ect)
//        {
//            IS_TRY = "0";
//            ltrLog.Text = "<b>Message : </b><font color='red' >" + ect.Message + "</font><br /><b>StackTrace : </b>" + ect.StackTrace.Replace("\r\n", "<br />");
//            try
//            { RfcDestinationManager.UnregisterDestinationConfiguration(SapConfig); }
//            catch (Exception ectUnregisterDestinationConfiguration) { }
//            #region insert logg error
//            {
//                Exception err = ect;
//                ///Paths File
//                string strPathName = "./UploadFile/ErrorLog/";

//                #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
//                if (!Directory.Exists(Server.MapPath(strPathName)))
//                {
//                    Directory.CreateDirectory(Server.MapPath(strPathName));
//                }
//                #endregion
//                ///Create a text file containing error details
//                string strFileName = DateTime.Now.ToString("ddMMyyyy", new System.Globalization.CultureInfo("th-TH")) + ".txt";
//                string errorText = "\r\nError Message: " + err.Message + "\r\nStack Trace: " + err.StackTrace + "\r\n";
//                string logMessage = String.Format(@"[{0}] :: {1} ", DateTime.Now.ToString("HH:mm:ss", new System.Globalization.CultureInfo("th-TH")), errorText);
//                FileInfo file = new FileInfo(Request.MapPath(strPathName + strFileName));
//                StreamWriter _sw = file.AppendText();
//                _sw.Write(logMessage);
//                _sw.Write(_sw.NewLine);
//                _sw.WriteLine(("*").PadLeft(logMessage.Length, '*'));
//                _sw.Write(_sw.NewLine);
//                _sw.Close();

//            }
//            #endregion
//        }
//        finally
//        {
//            if (IS_TRY == "1")
//            {
//                string script = "window.opener = 'Self';window.open('','_parent',''); window.close();";
//                Page.ClientScript.RegisterStartupScript(this.GetType(), "CloseWindow", script, true);
//            }
//        }

    }
    private Type GetDataType(RfcDataType rfcDataType)
    {
        switch (rfcDataType)
        {
            case RfcDataType.DATE:
                return typeof(string);
            case RfcDataType.CHAR:
                return typeof(string);
            case RfcDataType.STRING:
                return typeof(string);
            case RfcDataType.BCD:
                return typeof(decimal);
            case RfcDataType.INT2:
                return typeof(int);
            case RfcDataType.INT4:
                return typeof(int);
            case RfcDataType.FLOAT:
                return typeof(double);
            default:
                return typeof(string);
        }
    }
}