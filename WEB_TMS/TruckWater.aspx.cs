﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class TruckWater : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string UploadDirectory = "UploadFile/Truck/";
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        gvwHCompart.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwHCompart_AfterPerformCallback);

        gvwSTRUCKDoc1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc1_CustomColumnDisplayText);
        gvwSTRUCKDoc1.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc1_AfterPerformCallback);

        gvwSTRUCKDoc2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc2_CustomColumnDisplayText);
        gvwSTRUCKDoc2.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc2_AfterPerformCallback);

        gvwSTRUCKDoc3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc3_CustomColumnDisplayText);
        gvwSTRUCKDoc3.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc3_AfterPerformCallback);

        gvwSTRUCKDoc4.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKDoc4_CustomColumnDisplayText);
        gvwSTRUCKDoc4.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKDoc4_AfterPerformCallback);

        gvwSTRUCKSDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKSDoc_CustomColumnDisplayText);
        gvwSTRUCKSDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKSDoc_AfterPerformCallback);

        gvwSTRUCKIDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKIDoc_CustomColumnDisplayText);
        gvwSTRUCKIDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKIDoc_AfterPerformCallback);

        gvwRCompart.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRCompart_AfterPerformCallback);

        gvwRTRUCKDoc1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc1_CustomColumnDisplayText);
        gvwRTRUCKDoc1.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc1_AfterPerformCallback);

        gvwRTRUCKDoc2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc2_CustomColumnDisplayText);
        gvwRTRUCKDoc2.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc2_AfterPerformCallback);

        gvwRTRUCKDoc3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc3_CustomColumnDisplayText);
        gvwRTRUCKDoc3.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc3_AfterPerformCallback);

        gvwRTRUCKDoc4.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKDoc4_CustomColumnDisplayText);
        gvwRTRUCKDoc4.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKDoc4_AfterPerformCallback);

        gvwRTRUCKSDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKSDoc_CustomColumnDisplayText);
        gvwRTRUCKSDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKSDoc_AfterPerformCallback);

        gvwRTRUCKIDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwRTRUCKIDoc_CustomColumnDisplayText);
        gvwRTRUCKIDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwRTRUCKIDoc_AfterPerformCallback);

        gvwSTRUCKMWaterDoc.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwSTRUCKMWaterDoc_CustomColumnDisplayText);
        gvwSTRUCKMWaterDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSTRUCKMWaterDoc_AfterPerformCallback);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            if (!Permissions("53"))
            { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

            string str = Request.QueryString["str"];

            #region BindData
            if (!string.IsNullOrEmpty(str))
            {
                ClearSession();
                string[] QueryString = STCrypt.DecryptURL(str);
                Session["SCARTYPEID"] = "" + QueryString[3];
                txtGlobal_STRUCKID.Text = "" + QueryString[1];//รหัสหัว
                txtGlobal_RTRUCKID.Text = "" + QueryString[2];//รหัสหาง
                //ในกรณีที่เป็นรถเทรลเลอร์ จะดึงข้อมูลการวัดน้ำ,การถือครอง,และการอนุญาติใช้งาน มาจากรถส่วนหาง ให้เซ็ตค่าเป็นรหัสหาง
                txtGlobal_TRUCKID.Text = ((QueryString[3] + "" == "1" || QueryString[3] + "" == "3") && "" + QueryString[2] != "" ? "" + QueryString[2] : "" + QueryString[1]);

                BindData("SET_FORMAT_PAGE");
                BindData("SET_DATA_CONTROL");
                if ("" + txtGlobal_STRUCKID.Text != "")
                {
                    BindData("BIND_TRUCK_HINFO");
                    BindData("BIND_HINSURE_DB");
                    if (QueryString[3] + "" != "3")
                        BindData("BIND_TRUCK_MWATER");
                }
                if ("" + txtGlobal_RTRUCKID.Text != "")
                {
                    BindData("BIND_TRUCK_RINFO");
                    BindData("BIND_RINSURE_DB");
                    BindData("BIND_TRUCK_MWATER");
                }
                BindData("BIND_TRUCK_CONTRACT");
                BindData("BIND_TRUCK_PERMIT");
                ChangeMode("" + QueryString[0]);
            }
            #endregion
        }
    }
    #region CB Panel
    protected void xcpnMain_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "FullData":
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("WATER&" + txtGlobal_STRUCKID.Text.Trim() + "&" + txtGlobal_RTRUCKID.Text.Trim()))
                       , sUrl = "Truck_History_Info.aspx?str=";
                xcpnMain.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
            case "Save":
                SaveData();
                break;
        }
    }
    protected void xcpnHead_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Calc":
                DataTable dtCompart = (DataTable)Session["SHCompart"];
                xlbHnSlot.Text = dtCompart.Select("CDEL='0'").Length + "";
                int nTotalCapDel = 0;
                foreach (DataRow dr in dtCompart.Select("CDEL='0'"))
                {
                    DataTable dtComp = new DataTable();
                    dtComp.Columns.Add("Capacity", typeof(int));
                    for (int i = 1; i <= 3; i++)
                    {
                        if (dr["NPANLEVEL" + i] + "" != "")
                            dtComp.Rows.Add(new object[] { dr["NPANLEVEL" + i] });
                    }
                    nTotalCapDel += int.Parse(dtComp.Compute("Max(Capacity)", string.Empty) + "");
                }
                xlbHnTatolCapacity.Text = string.Format("{0:n0}", nTotalCapDel);
                if (nTotalCapDel + "" != "" && cboHProdGRP.Value + "" != "")
                {
                    Double HnWeight = (txtHnWeight.Text != "" ? Double.Parse(txtHnWeight.Text) : 0.00)
                            , nProdWeight = Double.Parse(nTotalCapDel + "") * Double.Parse(cboHProdGRP.Value + "");
                    xlbHnLoadWeight.Text = string.Format("{0:n0}", nProdWeight);
                    xlbHCalcWeight.Text = string.Format("{0:n0}", (HnWeight + nProdWeight));
                }
                break;
        }
    }
    protected void xcpnTrail_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Calc":
                DataTable dtCompart = (DataTable)Session["SRCompart"];
                xlbRnSlot.Text = dtCompart.Select("CDEL='0'").Length + "";
                int nTotalCapDel = 0;
                foreach (DataRow dr in dtCompart.Select("CDEL='0'"))
                {
                    DataTable dtComp = new DataTable();
                    dtComp.Columns.Add("Capacity", typeof(int));
                    for (int i = 1; i <= 3; i++)
                    {
                        if (dr["NPANLEVEL" + i] + "" != "")
                            dtComp.Rows.Add(new object[] { dr["NPANLEVEL" + i] });
                    }
                    nTotalCapDel += int.Parse(dtComp.Compute("Max(Capacity)", string.Empty) + "");
                }
                xlbRnTatolCapacity.Text = string.Format("{0:n0}", nTotalCapDel);
                if (nTotalCapDel + "" != "" && cboRProdGRP.Value + "" != "")
                {
                    Double HnWeight = (paras[1] != "" ? Double.Parse(paras[1]) : 0.00)
                            , RnWeight = (txtRnWeight.Text != "" ? Double.Parse(txtRnWeight.Text) : 0.00)
                            , nProdWeight = Double.Parse(nTotalCapDel + "") * Double.Parse(cboRProdGRP.Value + "");
                    xlbRnLoadWeight.Text = string.Format("{0:n0}", nProdWeight);
                    xlbRCalcWeight.Text = string.Format("{0:n0}", (HnWeight + RnWeight + nProdWeight));
                }
                break;
        }
    }
    protected void xcpnMWater_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "AddWater":
                break;
            case "ViewWater":
                break;
        }
    }
    protected void xcpnContract_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "VIEW":
                if (txtContractID.Text.Trim() != "")
                {
                    string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + txtContractID.Text.Trim() + "&" + txtVenderID.Text.Trim()))
                        , sUrl = "contract_add.aspx?str=";
                    xcpnContract.JSProperties["cpRedirectOpen"] = sUrl + sEncrypt;
                }
                break; ;
        }
    }
    protected void xcpnPermit_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "Permit":
                break;
        }
    }
    #endregion
    #region Combobox
    protected void cmbHsHolder_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbHsHolder_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID||NVL(v.SVENDORNAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cmbRsHolder_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbRsHolder_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID||NVL(v.SVENDORNAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cboHProdGRP_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboHProdGRP_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT PROD_ID,PROD_NAME,PROD_CATEGORY,NVL(DENSITY,0) AS DENSITY FROM (SELECT ROW_NUMBER()OVER(ORDER BY PROS.PROD_GRP) AS RN , PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY FROM TPRODUCT PRO
LEFT JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID WHERE PROS.PROD_ID||NVL(PROS.PROD_GRP_NAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cboRProdGRP_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboRProdGRP_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT PROD_ID,PROD_NAME,PROD_CATEGORY,NVL(DENSITY,0) AS DENSITY FROM (SELECT ROW_NUMBER()OVER(ORDER BY PROS.PROD_GRP) AS RN , PROS.PROD_ID,PROS.PROD_NAME,PRO.PROD_CATEGORY,PRO.DENSITY FROM TPRODUCT PRO
LEFT JOIN TPRODUCT_SAP PROS ON PRO.PROD_ID=PROS.PROD_ID WHERE PROS.PROD_ID||NVL(PROS.PROD_GRP_NAME,'') LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    #endregion
    #region GridView
    protected void gvwHCompart_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        DataTable dtCompart;
        DataView dvCompart;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwHCompart.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwHCompart.StartEdit(visibleindex);
                dynamic dyData = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox txtNCOMPARTNO = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox txtNPANLEVEL1 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox txtNPANLEVEL2 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox txtNPANLEVEL3 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL3");
                txtNCOMPARTNO.Text = "" + dyData[0];
                txtNPANLEVEL1.Text = "" + dyData[1];
                txtNPANLEVEL2.Text = "" + dyData[2];
                txtNPANLEVEL3.Text = "" + dyData[3];
                #endregion
                break;
            case "NEWCOMPART":
                gvwHCompart.AddNewRow();
                break;
            case "DELCOMPART":
                dynamic dyDataDel = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                dtCompart = PrepareDataTable("SHCompart", "", "" + dyDataDel[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCompart.Select("NCOMPARTNO='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCompart.Rows.IndexOf(_drDataDel);
                        if (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCompart.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCompart.Rows[idx_drTemp].BeginEdit();
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "1";
                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["SHCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwHCompart.DataSource = dvCompart.ToTable();
                gvwHCompart.DataBind();
                gvwHCompart.JSProperties["cpCalc"] = "Calc";
                break;
            case "SAVECOMPART":
                dynamic dyDataSubmit = gvwHCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                string Compart_mode = (visibleindex == -1) ? "Add" : "Edit";
                dtCompart = PrepareDataTable("SHCompart", "", "" + dyDataSubmit[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox save_txtNCOMPARTNO = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox save_txtNPANLEVEL1 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox save_txtNPANLEVEL2 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox save_txtNPANLEVEL3 = (ASPxTextBox)gvwHCompart.FindEditFormTemplateControl("txtNPANLEVEL3");

                switch (Compart_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */
                        if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "' AND CDEL='1'").Length > 0)
                        {
                            DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                            if (drData.Length > 0)
                            {
                                int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                dtCompart.Rows[idx_drTemp].BeginEdit();

                                dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                dtCompart.Rows[idx_drTemp].EndEdit();
                            }

                        }
                        else
                        {
                            DataRow drNewRow = dtCompart.NewRow();
                            drNewRow["NCOMPARTNO"] = "" + save_txtNCOMPARTNO.Text;
                            drNewRow["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            drNewRow["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            drNewRow["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            drNewRow["CNEW"] = "1";
                            drNewRow["CCHANGE"] = "0";
                            drNewRow["CDEL"] = "0";
                            dtCompart.Rows.Add(drNewRow);
                        }
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtCompart.Select("NCOMPARTNO='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtCompart.Rows.IndexOf(_drData[0]);
                            dtCompart.Rows[idx_drTemp].BeginEdit();

                            dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                            dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["SHCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwHCompart.CancelEdit();
                gvwHCompart.DataSource = dvCompart.ToTable();
                gvwHCompart.DataBind();
                gvwHCompart.JSProperties["cpCalc"] = "Calc";
                break;
        }
    }

    protected void gvwSTRUCKDoc1_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");

        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc1.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC1");
                break;
            case "BIND_TRUCK_HINFODOC1":
                BindData("BIND_TRUCK_HINFODOC1");
                break;
        }
    }

    protected void gvwSTRUCKDoc2_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc2.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC2");
                break;
            case "BIND_TRUCK_HINFODOC2":
                BindData("BIND_TRUCK_HINFODOC2");
                break;
        }
    }

    protected void gvwSTRUCKDoc3_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc3_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc3.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC3");
                break;
            case "BIND_TRUCK_HINFODOC3":
                BindData("BIND_TRUCK_HINFODOC3");
                break;
        }
    }

    protected void gvwSTRUCKDoc4_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKDoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKDoc4.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINFODOC4");
                break;
            case "BIND_TRUCK_HINFODOC4":
                BindData("BIND_TRUCK_HINFODOC4");
                break;
        }
    }

    protected void gvwSTRUCKSDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKSDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKSDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                BindData("BIND_TRUCK_HSTATUTEDOC");
                break;
            case "BIND_TRUCK_HSTATUTEDOC":
                BindData("BIND_TRUCK_HSTATUTEDOC");
                break;
        }
    }

    protected void gvwSTRUCKIDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKIDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwSTRUCKIDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("HISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_HINSUREDOC");
                break;
            case "BIND_TRUCK_HINSUREDOC":
                BindData("BIND_TRUCK_HINSUREDOC");
                break;
        }
    }

    protected void gvwRCompart_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        DataTable dtCompart;
        DataView dvCompart;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwRCompart.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwRCompart.StartEdit(visibleindex);
                dynamic dyData = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox txtNCOMPARTNO = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox txtNPANLEVEL1 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox txtNPANLEVEL2 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox txtNPANLEVEL3 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL3");
                txtNCOMPARTNO.Text = "" + dyData[0];
                txtNPANLEVEL1.Text = "" + dyData[1];
                txtNPANLEVEL2.Text = "" + dyData[2];
                txtNPANLEVEL3.Text = "" + dyData[3];
                #endregion
                break;
            case "NEWCOMPART":
                gvwRCompart.AddNewRow();
                break;
            case "DELCOMPART":
                dynamic dyDataDel = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                dtCompart = PrepareDataTable("SRCompart", "", "" + dyDataDel[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCompart.Select("NCOMPARTNO='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCompart.Rows.IndexOf(_drDataDel);
                        if (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCompart.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCompart.Rows[idx_drTemp].BeginEdit();
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "1";
                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["SRCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL =0";
                gvwRCompart.DataSource = dvCompart.ToTable();
                gvwRCompart.DataBind();
                gvwRCompart.JSProperties["cpCalc"] = "Calc";
                break;
            case "SAVECOMPART":
                dynamic dyDataSubmit = gvwRCompart.GetRowValues(visibleindex, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");

                string Compart_mode = (visibleindex == -1) ? "Add" : "Edit";
                dtCompart = PrepareDataTable("SRCompart", "", "" + dyDataSubmit[4], "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                ASPxTextBox save_txtNCOMPARTNO = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNCOMPARTNO");
                ASPxTextBox save_txtNPANLEVEL1 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL1");
                ASPxTextBox save_txtNPANLEVEL2 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL2");
                ASPxTextBox save_txtNPANLEVEL3 = (ASPxTextBox)gvwRCompart.FindEditFormTemplateControl("txtNPANLEVEL3");

                switch (Compart_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */
                        if (dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "' AND CDEL='1'").Length > 0)
                        {
                            DataRow[] drData = dtCompart.Select("NCOMPARTNO='" + save_txtNCOMPARTNO.Text.Trim() + "'");

                            if (drData.Length > 0)
                            {
                                int idx_drTemp = dtCompart.Rows.IndexOf(drData[0]);
                                dtCompart.Rows[idx_drTemp].BeginEdit();

                                dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                                dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                                dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                                dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                                dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                                dtCompart.Rows[idx_drTemp].EndEdit();
                            }

                        }
                        else
                        {
                            DataRow drNewRow = dtCompart.NewRow();
                            drNewRow["NCOMPARTNO"] = "" + save_txtNCOMPARTNO.Text;
                            drNewRow["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            drNewRow["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            drNewRow["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            drNewRow["CNEW"] = "1";
                            drNewRow["CCHANGE"] = "0";
                            drNewRow["CDEL"] = "0";
                            dtCompart.Rows.Add(drNewRow);
                        }
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtCompart.Select("NCOMPARTNO='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtCompart.Rows.IndexOf(_drData[0]);
                            dtCompart.Rows[idx_drTemp].BeginEdit();

                            dtCompart.Rows[idx_drTemp]["NPANLEVEL1"] = "" + save_txtNPANLEVEL1.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL2"] = "" + save_txtNPANLEVEL2.Text;
                            dtCompart.Rows[idx_drTemp]["NPANLEVEL3"] = "" + save_txtNPANLEVEL3.Value;
                            dtCompart.Rows[idx_drTemp]["CNEW"] = dtCompart.Rows[idx_drTemp]["CNEW"] + "";
                            dtCompart.Rows[idx_drTemp]["CCHANGE"] = (dtCompart.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCompart.Rows[idx_drTemp]["CDEL"] = "0";

                            dtCompart.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["SRCompart"] = dtCompart;
                dvCompart = new DataView(dtCompart);
                dvCompart.RowFilter = "CDEL=0";
                gvwRCompart.CancelEdit();
                gvwRCompart.DataSource = dvCompart.ToTable();
                gvwRCompart.DataBind();
                gvwRCompart.JSProperties["cpCalc"] = "Calc";
                break;
        }
    }

    protected void gvwRTRUCKDoc1_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc1.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC1");
                break;
            case "BIND_TRUCK_RINFODOC1":
                BindData("BIND_TRUCK_RINFODOC1");
                break;
        }
    }

    protected void gvwRTRUCKDoc2_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc2_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc2.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC2");
                break;
            case "BIND_TRUCK_RINFODOC2":
                BindData("BIND_TRUCK_RINFODOC2");
                break;
        }
    }

    protected void gvwRTRUCKDoc3_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc3_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc3.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC3");
                break;
            case "BIND_TRUCK_RINFODOC3":
                BindData("BIND_TRUCK_RINFODOC3");
                break;
        }
    }

    protected void gvwRTRUCKDoc4_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKDoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKDoc4.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINFODOC4");
                break;
            case "BIND_TRUCK_RINFODOC4":
                BindData("BIND_TRUCK_RINFODOC4");
                break;
        }
    }

    protected void gvwRTRUCKSDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKSDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKSDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RSTATUTEDOC");
                break;
            case "BIND_TRUCK_RSTATUTEDOC":
                BindData("BIND_TRUCK_RSTATUTEDOC");
                break;
        }
    }

    protected void gvwRTRUCKIDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwRTRUCKIDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[10] + "" + dyData[4];
                gvwRTRUCKIDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("RISDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_RINSUREDOC");
                break;
            case "BIND_TRUCK_RINSUREDOC":
                BindData("BIND_TRUCK_RINSUREDOC");
                break;
        }
    }

    protected void gvwSTRUCKMWaterDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwSTRUCKMWaterDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE"
            , "CACTIVE", "SPATH", "CNEW");
        switch (CallbackName.ToUpper())
        {
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[9] + "" + dyData[3];
                gvwSTRUCKMWaterDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DELDOC":
                DataTable dtTRUCK = PrepareDataTable("MWaterDoc", "", "" + dyData[1], "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                DataRow[] _drDataDels = dtTRUCK.Select("DOCID='" + dyData[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtTRUCK.Rows.IndexOf(_drDataDel);
                        if (dtTRUCK.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            DeleteFileNotUse(dtTRUCK.Rows[idx_drTemp]["SPATH"] + "" + dtTRUCK.Rows[idx_drTemp]["DOC_SYSNAME"]);
                            dtTRUCK.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtTRUCK.Rows[idx_drTemp].BeginEdit();
                            dtTRUCK.Rows[idx_drTemp]["CACTIVE"] = "0";
                            dtTRUCK.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_TRUCK_MWATERDOC");
                break;
            case "BIND_TRUCK_MWATERDOC":
                BindData("BIND_TRUCK_MWATERDOC");
                break;
        }
    }

    protected void gvwBlacklist_PageIndexChanged(object sender, EventArgs e)
    {
        DataTable dtTruckInfo;
        DataView dvTruckInfo;
        DataSourceSelectArguments args = new DataSourceSelectArguments();
        args = new DataSourceSelectArguments();
        dvTruckInfo = (DataView)sdsSTRUCKBlacklist.Select(args);
        dtTruckInfo = dvTruckInfo.ToTable();

        DataTable dtBlackList = new DataTable();
        dtBlackList.Columns.Add("BLACKLISTID", typeof(int));
        dtBlackList.Columns.Add("REMARK", typeof(string));
        dtBlackList.Columns.Add("SDATE", typeof(string));
        dtBlackList.Columns.Add("TDATE", typeof(string));
        dtBlackList.Columns.Add("SBLACKLIST", typeof(string));
        foreach (DataRow drBL in dtTruckInfo.Rows)
        {
            DataRow drBlackList = dtBlackList.NewRow();
            drBlackList["BLACKLISTID"] = drBL["BLACKLISTID"].ToString();
            drBlackList["REMARK"] = drBL["REMARK"].ToString();
            if (drBL["DBLACKLIST_START"] + "" != "")
                drBlackList["SDATE"] = Convert.ToDateTime(drBL["DBLACKLIST_START"]).ToString("dd/MM/yyyy - HH:mm", new CultureInfo("th-TH")) + " น.";
            if (drBL["DBLACKLIST_END"] + "" != "")
                drBlackList["TDATE"] = Convert.ToDateTime(drBL["DBLACKLIST_END"]).ToString("dd/MM/yyyy - HH:mm", new CultureInfo("th-TH")) + " น.";
            drBlackList["SBLACKLIST"] = CommonFunction.Get_Value(conn, "SELECT SFIRSTNAME||'  '||SLASTNAME FROM TUSER WHERE SUID='" + drBL["SBLACKLIST"].ToString() + "'");
            dtBlackList.Rows.Add(drBlackList);
        }
        gvwBlacklist.DataSource = dtBlackList;
        gvwBlacklist.DataBind();
    }
    #endregion
    #region UploadFiles
    protected void uldHDoc01_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "1";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc02_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "2";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc03_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "3";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHDoc04_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "4";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HDoc"] = dtTruckDoc;
    }
    protected void uldHSDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/STATUTE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "STATUTE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HISDoc"] = dtTruckDoc;
    }
    protected void uldHIDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INSURANCE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("HISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "INSURANCE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["HISDoc"] = dtTruckDoc;
    }
    protected void uldRDoc01_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "1";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc02_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "2";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc03_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "3";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRDoc04_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INFO/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_TYPE"] = "4";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RDoc"] = dtTruckDoc;
    }
    protected void uldRSDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/STATUTE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "STATUTE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RISDoc"] = dtTruckDoc;
    }
    protected void uldRIDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/INSURANCE/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("RISDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["CTYPE"] = "INSURANCE";
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["RISDoc"] = dtTruckDoc;
    }
    protected void uldMWaterDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string TRUCK_ID = txtGlobal_TRUCKID.Text.Trim();
        string SPATH = UploadDirectory + "Temp/" + TRUCK_ID + "/MWATER/";
        e.CallbackData = UploadFile2Server(e.UploadedFile
               , "TruckDoc_" + TRUCK_ID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
               , SPATH);

        DataTable dtTruckDoc = PrepareDataTable("MWaterDoc", "", "" + TRUCK_ID
     , "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");

        string[] sFileArray = e.CallbackData.Split('$');

        DataRow drNewRow = dtTruckDoc.NewRow();
        string DOCID = "" + dtTruckDoc.Compute("MAX(DOCID)", string.Empty) + 1;
        drNewRow["DOCID"] = "" + DOCID;
        drNewRow["STRUCKID"] = "" + TRUCK_ID;
        drNewRow["DOC_NAME"] = "" + sFileArray[1];
        drNewRow["DOC_SYSNAME"] = "" + sFileArray[0];
        drNewRow["DCREATE"] = "";
        drNewRow["SCREATE"] = Session["UserID"] + "";
        drNewRow["DUPDATE"] = "";
        drNewRow["SUPDATE"] = Session["UserID"] + "";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = SPATH;
        drNewRow["CNEW"] = "1";
        dtTruckDoc.Rows.Add(drNewRow);
        Session["MWaterDoc"] = dtTruckDoc;
    }
    #endregion
    #region DataSource
    protected void sdsSTRUCK_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCK_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsSTRUCKCompart_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKCompart_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsSTRUCKCompart_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKCompart_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsSTRUCKDoc_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKDoc_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsSTRUCKINSURE_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKINSURE_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsSTRUCKISDoc_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKISDoc_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void sdsRTRUCK_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCK_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsRTRUCKCompart_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCKCompart_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsRTRUCKCompart_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCKCompart_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsRTRUCKDoc_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCKDoc_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsRTRUCKINSURE_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCKINSURE_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsRTRUCKISDoc_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsRTRUCKISDoc_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void sdsSTRUCKMWaterDoc_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sdsSTRUCKMWaterDoc_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    #endregion
    protected void xbnEditMode_Click(object sender, EventArgs e)
    {
        ChangeMode("Edit");
    }
    protected void xbnViewMode_Click(object sender, EventArgs e)
    {
        ChangeMode("View");
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    private void SaveData()
    {
        string STRUCK = txtGlobal_STRUCKID.Text.Trim();
        string RTRUCK = txtGlobal_RTRUCKID.Text.Trim();
        string ScarType = Session["SCARTYPEID"].ToString();
        bool v = false;
        #region Keep History
        //ข้อมูลรถหรือรถส่วนหัว
        if (STRUCK.Trim() != "")
        {
            string nitem = CommonFunction.Get_Value(conn, "SELECT MAX(NITEM)+1 FROM TTRUCK_HIS WHERE STRUCKID='" + STRUCK + "'");
            if (string.IsNullOrEmpty(nitem))
                txtGlobal_SNITEM.Text = "1";
            else
                txtGlobal_SNITEM.Text = nitem;
            /*sdsSTRUCK.Insert();
            sdsSTRUCKCompart.Insert();
            sdsSTRUCKDoc.Insert();
            sdsSTRUCKINSURE.Insert();
            sdsSTRUCKISDoc.Insert();*/
            if (ScarType != "3")
            {
                txtGlobal_TNITEM.Text = nitem;
                sdsSTRUCKMWaterDoc.Insert();
            }
        }

        //ข้อมูลรถส่วนหาง
        if (RTRUCK.Trim() != "")
        {
            string nitem = CommonFunction.Get_Value(conn, "SELECT MAX(NITEM)+1 FROM TTRUCK_HIS WHERE STRUCKID='" + RTRUCK + "'");
            if (string.IsNullOrEmpty(nitem))
                txtGlobal_RNITEM.Text = "1";
            else
                txtGlobal_RNITEM.Text = nitem;
            /*sdsRTRUCK.Insert();
            sdsRTRUCKCompart.Insert();
            sdsRTRUCKDoc.Insert();
            sdsRTRUCKINSURE.Insert();
            sdsRTRUCKISDoc.Insert();*/
            if (ScarType == "3" || ScarType == "4")
            {
                txtGlobal_TNITEM.Text = nitem;
                sdsSTRUCKMWaterDoc.Insert();
            }
        }
        #endregion
        if (v)
        {
            #region ข้อมูลรถหรือข้อมูลรถส่วนหัว
            //ข้อมูลรถหรือข้อมูลรถส่วนหัว
            if (STRUCK.Trim() != "")
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = @"UPDATE TTRUCK SET DSIGNIN = :DSIGNIN,DREGISTER = :DREGISTER,SHOLDERID=:SHOLDERID,SHOLDERNAME=:SHOLDERNAME,SCHASIS = :SCHASIS,SENGINE = :SENGINE,SBRAND = :SBRAND,SMODEL = :SMODEL,NWHEELS = :NWHEELS,POWERMOVER = :POWERMOVER
                         ,NSHAFTDRIVEN = :NSHAFTDRIVEN,SVIBRATION = :SVIBRATION,PUMPPOWER = :PUMPPOWER,PUMPPOWER_TYPE = :PUMPPOWER_TYPE,MATERIALOFPRESSURE = :MATERIALOFPRESSURE,VALVETYPE = :VALVETYPE
                         ,FUELTYPE = :FUELTYPE,GPS_SERVICE_PROVIDER = :GPS_SERVICE_PROVIDER,NWEIGHT = :NWEIGHT,NSLOT = :NSLOT,NTOTALCAPACITY = :NTOTALCAPACITY,STANK_MATERAIL = :STANK_MATERAIL
                         ,STANK_MAKER = :STANK_MAKER,SLOADING_METHOD = :SLOADING_METHOD,SPROD_GRP = :SPROD_GRP,NLOAD_WEIGHT = :NLOAD_WEIGHT,NCALC_WEIGHT = :NCALC_WEIGHT
                         ,CACTIVE = :CACTIVE,DBLACKLIST = :DBLACKLIST,DBLACKLIST2 = :DBLACKLIST2,BLACKLIST_CAUSE = :BLACKLIST_CAUSE,DUPDATE = SYSDATE,SUPDATE = :SUPDATE,TRUCK_CATEGORY=:TRUCK_CATEGORY,VOL_UOM=:VOL_UOM,VEH_TEXT=:VEH_TEXT
                          WHERE STRUCKID = :STRUCKID";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                        com.Parameters.Add(":DSIGNIN", OracleType.DateTime).Value = Convert.ToDateTime(xDetdHFRegNo.Value);
                        com.Parameters.Add(":DREGISTER", OracleType.DateTime).Value = Convert.ToDateTime(xDetdHRegNo.Value);
                        string[] arrHolder = cmbHsHolder.Text.ToString().Split('-');
                        com.Parameters.Add(":SHOLDERID", OracleType.VarChar).Value = arrHolder[0].Trim();
                        com.Parameters.Add(":SHOLDERNAME", OracleType.VarChar).Value = arrHolder[1].Trim();
                        com.Parameters.Add(":SCHASIS", OracleType.VarChar).Value = txtHsChasis.Text;
                        com.Parameters.Add(":SENGINE", OracleType.VarChar).Value = txtHsEngine.Text;
                        com.Parameters.Add(":SBRAND", OracleType.VarChar).Value = cboHsBrand.Text + "";
                        com.Parameters.Add(":SMODEL", OracleType.VarChar).Value = txtHsModel.Text;
                        if (txtHnWheel.Text.Trim() != "")
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = txtHnWheel.Text.Trim();
                        else
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = OracleNumber.Null;
                        if (txtHPowermover.Text.Trim() != "")//กำลังเครื่องยนต์
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = txtHPowermover.Text.Trim();
                        else
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = OracleNumber.Null;
                        if (txtHnShaftDriven.Text.Trim() != "")//จำนวนเพลาขับเคลื่อน
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = txtHnShaftDriven.Text.Trim();
                        else
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SVIBRATION", OracleType.VarChar).Value = cboHsVibration.Value + "";
                        com.Parameters.Add(":PUMPPOWER", OracleType.VarChar).Value = rblHPumpPower.Value + "";
                        com.Parameters.Add(":PUMPPOWER_TYPE", OracleType.VarChar).Value = cboHPumpPower_type.Value + "";
                        com.Parameters.Add(":MATERIALOFPRESSURE", OracleType.VarChar).Value = cboHMaterialOfPressure.Value + "";
                        com.Parameters.Add(":VALVETYPE", OracleType.VarChar).Value = rblHValveType.Value == "Other" ? txtHValveType.Text : rblHValveType.Value + "";
                        com.Parameters.Add(":FUELTYPE", OracleType.VarChar).Value = rblHFuelType.Value == "Other" ? txtHFuelType.Text : rblHFuelType.Value + "";
                        com.Parameters.Add(":GPS_SERVICE_PROVIDER", OracleType.VarChar).Value = cboHGPSProvider.Text + "";
                        if (txtHnWeight.Text.Trim() != "")
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtHnWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbHnSlot.Text.Trim() != "")
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = xlbHnSlot.Text.Trim();
                        else
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbHnTatolCapacity.Text.Trim() != "")
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = xlbHnTatolCapacity.Text.Trim();
                        else
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":STANK_MATERAIL", OracleType.VarChar).Value = cboHTankMaterial.Value + "";
                        com.Parameters.Add(":STANK_MAKER", OracleType.VarChar).Value = txtHTank_Maker.Text;
                        com.Parameters.Add(":SLOADING_METHOD", OracleType.VarChar).Value = cboHLoadMethod.Value + "";
                        string[] arrProd = cboHProdGRP.Text.ToString().Split('-');
                        com.Parameters.Add(":SPROD_GRP", OracleType.VarChar).Value = arrProd[0].Trim();
                        if (xlbHnLoadWeight.Text.Trim() != "")
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = xlbHnLoadWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbHCalcWeight.Text.Trim() != "")
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = xlbHCalcWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = rblPermit.Value + "";
                        //การอนุญาติใช้งานรถ
                        if (rblPermit.Value == "N")//ระงับการใช้งาน
                        {
                            com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = DateTime.Now.Date;
                            com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = Convert.ToDateTime(xDetdBlackListExp.Value);
                            com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = xMemBlackListCause.Text;
                        }
                        else
                        {
                            com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = OracleDateTime.Null;
                            com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = OracleDateTime.Null;
                            com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = "";
                        }
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                        com.Parameters.Add(":VOL_UOM", OracleType.VarChar).Value = txtVEH_Volume.Text;
                        com.Parameters.Add(":VEH_TEXT", OracleType.VarChar).Value = txtVEH_Text.Text;
                        com.ExecuteNonQuery();
                    }
                }
            }

            if (ScarType != "3" && Session["SHCompart"] != null)
            {
                //ข้อมูลความจุ
                sdsSTRUCKCompart.Delete();
                using (OracleConnection con = new OracleConnection(conn))
                {
                    DataTable dt = (DataTable)Session["SHCompart"];
                    con.Open();
                    string strSql = @"INSERT INTO TTRUCK_COMPART(STRUCKID,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                        (:STRUCKID,:NCOMPARTNO,:NPANLEVEL,:NCAPACITY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE)";
                    foreach (DataRow dr in dt.Select("NCOMPARTNO<>''", "NCOMPARTNO ASC"))
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            if (dr["NPANLEVEL" + i] + "" != "")
                            {
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                                    com.Parameters.Add(":NCOMPARTNO", OracleType.Number).Value = dr["NCOMPARTNO"] + "";
                                    com.Parameters.Add(":NPANLEVEL", OracleType.Number).Value = i;
                                    com.Parameters.Add(":NCAPACITY", OracleType.Number).Value = dr["NPANLEVEL" + i] + "";
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
            }

            //เอกสารข้อมูลรถ
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();
                DataTable dt = (DataTable)Session["HDoc"];
                string dest = UploadDirectory + "" + STRUCK + "/INFO/";
                foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "DOC_TYPE ASC"))
                {
                    if (dr["CNEW"] == "1")
                    {
                        string GenID = CommonFunction.Gen_ID(con, "SELECT DOCID FROM (SELECT DOCID FROM TTRUCK_DOC ORDER BY DOCID DESC)  WHERE ROWNUM <= 1");

                        string strSql = @"INSERT INTO TTRUCK_DOC(DOCID,STRUCKID,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) Values
                                            (:DOCID,:STRUCKID,:DOC_TYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME)";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = dr["DOC_TYPE"] + "";
                            com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                            com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.ExecuteNonQuery();
                        }
                        ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                    }
                    else
                    {
                        string strSql = @"UPDATE TTRUCK_DOC SET DUPDATE = SYSDATE, SUPDATE = :SUPDATE, CACTIVE = :CACTIVE WHERE DOCID = :DOCID AND STRUCKID = :STRUCKID";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.ExecuteNonQuery();
                        }
                    }
                }
            }

            //ข้อมูลพรบ.
            using (OracleConnection con = new OracleConnection(conn))
            {
                string getStatute = CommonFunction.Get_Value(conn, "SELECT STRUCKID FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(STRUCK) + "' AND CTYPE='STATUTE'");
                con.Open();
                string strSql = "";
                if (string.IsNullOrEmpty(getStatute))
                    strSql = @"INSERT INTO TTRUCK_INSURANCE(STRUCKID,CTYPE,SCOMPANY,START_DURATION,END_DURATION,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                            (:STRUCKID,:CTYPE,:SCOMPANY,:START_DURATION,:END_DURATION,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE)";
                else
                    strSql = @"UPDATE TTRUCK_INSURANCE SET CTYPE = :CTYPE ,SCOMPANY = :SCOMPANY ,START_DURATION = :START_DURATION ,END_DURATION = :END_DURATION ,SDETAIL = :SDETAIL ,DUPDATE=SYSDATE ,SUPDATE=:SUPDATE
                                    WHERE STRUCKID=:STRUCKID";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                    com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "STATUTE";
                    com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtHSCompany.Text + "";
                    if (xDetHSDuration_Start.Value + "" != "")
                        com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetHSDuration_Start.Value);
                    else
                        com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                    if (xDetHSDuration_End.Value + "" != "")
                        com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetHSDuration_End.Value);
                    else
                        com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                    com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemHSDetail.Text + "";
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                    com.ExecuteNonQuery();
                }
            }

            //ข้อมูลประกันภัย.
            using (OracleConnection con = new OracleConnection(conn))
            {
                string getInsure = CommonFunction.Get_Value(conn, "SELECT STRUCKID FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(STRUCK) + "' AND CTYPE='INSURANCE'");
                con.Open();
                string strSql = "";
                if (string.IsNullOrEmpty(getInsure))
                    strSql = @"INSERT INTO TTRUCK_INSURANCE(STRUCKID,CTYPE,SCOMPANY,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                        (:STRUCKID,:CTYPE,:SCOMPANY,:INSURE_BUDGET,:INSURE_TYPE,:HOLDING,:NINSURE,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE)";
                else
                    strSql = @"UPDATE TTRUCK_INSURANCE SET CTYPE = :CTYPE ,SCOMPANY = :SCOMPANY ,INSURE_BUDGET = :INSURE_BUDGET ,INSURE_TYPE = :INSURE_TYPE ,HOLDING = :HOLDING ,NINSURE=:NINSURE ,SDETAIL = :SDETAIL ,DUPDATE=SYSDATE ,SUPDATE=:SUPDATE
                                    WHERE STRUCKID=:STRUCKID";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                    com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "INSURANCE";
                    com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtHICompany.Text + "";
                    if (txtHIBudget.Text + "" != "")
                        com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = txtHIBudget.Text + "";
                    else
                        com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":INSURE_TYPE", OracleType.VarChar).Value = cbmHIType.Value + "";
                    string sHolding = "";
                    foreach (ListEditItem items in chkHIHolding.Items)
                    {
                        if (items.Selected)
                            sHolding += "," + items.Value;
                    }
                    if (sHolding.Trim() != "")
                        sHolding = sHolding.Substring(1);
                    com.Parameters.Add(":HOLDING", OracleType.VarChar).Value = sHolding.ToString();
                    if (txtHInInsure.Text + "" != "")
                        com.Parameters.Add(":NINSURE", OracleType.Number).Value = txtHInInsure.Text + "";
                    else
                        com.Parameters.Add(":NINSURE", OracleType.Number).Value = OracleNumber.Null;
                    com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemHIDetail.Text + "";
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                    com.ExecuteNonQuery();
                }
            }

            //เอกสารข้อมูลพรบ. และประกันภัย
            using (OracleConnection con = new OracleConnection(conn))
            {
                DataTable dt = (DataTable)Session["HISDoc"];
                con.Open();
                foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "CTYPE ASC"))
                {
                    if (dr["CNEW"] + "" == "1")
                    {
                        string dest = UploadDirectory + "" + STRUCK + "/" + dr["CTYPE"] + "/";
                        string GenID = CommonFunction.Gen_ID(con, "SELECT DOCID FROM (SELECT DOCID FROM TTRUCK_INSUREDOC ORDER BY DOCID DESC)  WHERE ROWNUM <= 1");

                        string strSql = @"INSERT INTO TTRUCK_INSUREDOC(DOCID,STRUCKID,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) Values
                                    (:DOCID,:STRUCKID,:CTYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME)";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["CNEW"] + "" == "1" ? GenID : dr["DOCID"] + "";
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = dr["CTYPE"] + "";
                            com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                            com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.ExecuteNonQuery();
                            ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                        }
                    }
                    else
                    {
                        string strSql = @"UPDATE TTRUCK_INSUREDOC SET DUPDATE = SYSDATE ,SUPDATE = :SUPDATE ,CACTIVE = :CACTIVE
                                    WHERE DOCID=:DOCID AND STRUCKID=:STRUCKID";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();

                            com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = STRUCK;
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                            com.ExecuteNonQuery();
                        }
                    }
                }
            }
            #endregion

            #region ข้อมูลรถส่วนหาง
            if (!string.IsNullOrEmpty(RTRUCK) && (ScarType == "1" || ScarType == "3"))
            {
                //ข้อมูลรถหรือข้อมูลรถส่วนหัว
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    string strSql = @"UPDATE TTRUCK SET DSIGNIN = :DSIGNIN,DREGISTER = :DREGISTER,SHOLDERID=:SHOLDERID,SHOLDERNAME=:SHOLDERNAME
                 ,SCHASIS = :SCHASIS,SENGINE = :SENGINE,SBRAND = :SBRAND,SMODEL = :SMODEL,NWHEELS = :NWHEELS,POWERMOVER = :POWERMOVER
                 ,NSHAFTDRIVEN = :NSHAFTDRIVEN,SVIBRATION = :SVIBRATION,PUMPPOWER = :PUMPPOWER,PUMPPOWER_TYPE = :PUMPPOWER_TYPE,MATERIALOFPRESSURE = :MATERIALOFPRESSURE,VALVETYPE = :VALVETYPE
                 ,FUELTYPE = :FUELTYPE,GPS_SERVICE_PROVIDER = :GPS_SERVICE_PROVIDER,NWEIGHT = :NWEIGHT,NSLOT = :NSLOT,NTOTALCAPACITY = :NTOTALCAPACITY,STANK_MATERAIL = :STANK_MATERAIL
                 ,STANK_MAKER = :STANK_MAKER,SLOADING_METHOD = :SLOADING_METHOD,SPROD_GRP = :SPROD_GRP,NLOAD_WEIGHT = :NLOAD_WEIGHT,NCALC_WEIGHT = :NCALC_WEIGHT
                 ,CACTIVE = :CACTIVE,DBLACKLIST = :DBLACKLIST,DBLACKLIST2 = :DBLACKLIST2,BLACKLIST_CAUSE = :BLACKLIST_CAUSE,DUPDATE = SYSDATE,SUPDATE = :SUPDATE,TRUCK_CATEGORY=:TRUCK_CATEGORY,VOL_UOM=:VOL_UOM,VEH_TEXT=:VEH_TEXT
                  WHERE STRUCKID = :STRUCKID";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":DSIGNIN", OracleType.DateTime).Value = Convert.ToDateTime(xDetdRFRegNo.Value);
                        com.Parameters.Add(":DREGISTER", OracleType.DateTime).Value = Convert.ToDateTime(xDetdRRegNo.Value);
                        string[] arrHolder = cmbRsHolder.Text.ToString().Split('-');
                        com.Parameters.Add(":SHOLDERID", OracleType.VarChar).Value = arrHolder[0].Trim();
                        com.Parameters.Add(":SHOLDERNAME", OracleType.VarChar).Value = arrHolder[1].Trim();
                        com.Parameters.Add(":SCHASIS", OracleType.VarChar).Value = txtRsChasis.Text;
                        com.Parameters.Add(":SENGINE", OracleType.VarChar).Value = txtRsEngine.Text;
                        com.Parameters.Add(":SBRAND", OracleType.VarChar).Value = cboRsBrand.Text + "";
                        com.Parameters.Add(":SMODEL", OracleType.VarChar).Value = txtRsModel.Text;
                        if (txtRnWheel.Text.Trim() != "")
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = txtRnWheel.Text.Trim();
                        else
                            com.Parameters.Add(":NWHEELS", OracleType.Number).Value = OracleNumber.Null;
                        if (txtRPowermover.Text.Trim() != "")
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = txtRPowermover.Text.Trim();
                        else
                            com.Parameters.Add(":POWERMOVER", OracleType.Number).Value = OracleNumber.Null;
                        if (txtRnShaftDriven.Text.Trim() != "")
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = txtRnShaftDriven.Text.Trim();
                        else
                            com.Parameters.Add(":NSHAFTDRIVEN", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SVIBRATION", OracleType.VarChar).Value = cboRsVibration.Value + "";
                        com.Parameters.Add(":PUMPPOWER", OracleType.VarChar).Value = rblRPumpPower.Value + "";
                        com.Parameters.Add(":PUMPPOWER_TYPE", OracleType.VarChar).Value = cboRPumpPower_type.Value + "";
                        com.Parameters.Add(":MATERIALOFPRESSURE", OracleType.VarChar).Value = cboRMaterialOfPressure.Value + "";
                        com.Parameters.Add(":VALVETYPE", OracleType.VarChar).Value = rblRValveType.Value == "Other" ? txtRValveType.Text : rblRValveType.Value + "";
                        com.Parameters.Add(":FUELTYPE", OracleType.VarChar).Value = rblRFuelType.Value == "Other" ? txtRFuelType.Text : rblHFuelType.Value + "";
                        com.Parameters.Add(":GPS_SERVICE_PROVIDER", OracleType.VarChar).Value = cboRGPSProvider.Text + "";
                        if (txtRnWeight.Text.Trim() != "")
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = txtRnWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NWEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRnSlot.Text.Trim() != "")
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = xlbRnSlot.Text.Trim();
                        else
                            com.Parameters.Add(":NSLOT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRnTatolCapacity.Text.Trim() != "")
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = xlbRnTatolCapacity.Text.Trim();
                        else
                            com.Parameters.Add(":NTOTALCAPACITY", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":STANK_MATERAIL", OracleType.VarChar).Value = cboRTankMaterial.Value + "";
                        com.Parameters.Add(":STANK_MAKER", OracleType.VarChar).Value = txtRTank_Maker.Text;
                        com.Parameters.Add(":SLOADING_METHOD", OracleType.VarChar).Value = cboRLoadMethod.Value + "";
                        string[] arrProd = cboRProdGRP.Text.ToString().Split('-');
                        com.Parameters.Add(":SPROD_GRP", OracleType.VarChar).Value = arrProd[0].Trim();
                        if (xlbRnLoadWeight.Text.Trim() != "")
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = xlbRnLoadWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NLOAD_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        if (xlbRCalcWeight.Text.Trim() != "")
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = xlbRCalcWeight.Text.Trim();
                        else
                            com.Parameters.Add(":NCALC_WEIGHT", OracleType.Number).Value = OracleNumber.Null;
                        //การอนุญาตใช้งานรถ
                        com.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = rblPermit.Value + "";
                        if (rblPermit.SelectedIndex == 1)//ระงับการใช้งาน
                        {
                            com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = DateTime.Now.Date;
                            com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = Convert.ToDateTime(xDetdBlackListExp.Value);
                            com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = xMemBlackListCause.Text;
                        }
                        else
                        {
                            com.Parameters.Add(":DBLACKLIST", OracleType.DateTime).Value = OracleDateTime.Null;
                            com.Parameters.Add(":DBLACKLIST2", OracleType.DateTime).Value = OracleDateTime.Null;
                            com.Parameters.Add(":BLACKLIST_CAUSE", OracleType.VarChar).Value = "";
                        }

                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                        com.Parameters.Add(":TRUCK_CATEGORY", OracleType.VarChar).Value = txtRTru_Cate.Text;
                        com.Parameters.Add(":VOL_UOM", OracleType.VarChar).Value = txtTU_Volume.Text;
                        com.Parameters.Add(":VEH_TEXT", OracleType.VarChar).Value = txtTU_Text.Text;
                        com.ExecuteNonQuery();
                    }
                }

                //ข้อมูลความจุ
                sdsRTRUCKCompart.Delete();
                using (OracleConnection con = new OracleConnection(conn))
                {
                    DataTable dt = (DataTable)Session["SRCompart"];
                    con.Open();
                    string strSql = @"INSERT INTO TTRUCK_COMPART(STRUCKID,NCOMPARTNO,NPANLEVEL,NCAPACITY,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                    (:STRUCKID,:NCOMPARTNO,:NPANLEVEL,:NCAPACITY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE)";
                    foreach (DataRow dr in dt.Select("NCOMPARTNO<>''", "NCOMPARTNO ASC"))
                    {
                        for (int i = 1; i <= 3; i++)
                        {
                            if (dr["NPANLEVEL" + i] + "" != "")
                            {
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                    com.Parameters.Add(":NCOMPARTNO", OracleType.Number).Value = dr["NCOMPARTNO"] + "";
                                    com.Parameters.Add(":NPANLEVEL", OracleType.Number).Value = i;
                                    com.Parameters.Add(":NCAPACITY", OracleType.Number).Value = dr["NPANLEVEL" + i] + "";
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }


                //เอกสารข้อมูลรถ
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    DataTable dt = (DataTable)Session["RDoc"];
                    string dest = UploadDirectory + "" + RTRUCK + "/INFO/";
                    foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "DOC_TYPE ASC"))
                    {
                        if (dr["CNEW"] == "1")
                        {
                            string GenID = CommonFunction.Gen_ID(con, "SELECT DOCID FROM (SELECT DOCID FROM TTRUCK_DOC ORDER BY DOCID DESC)  WHERE ROWNUM <= 1");
                            string strSql = @"INSERT INTO TTRUCK_DOC(DOCID,STRUCKID,DOC_TYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) Values
                                    (:DOCID,:STRUCKID,:DOC_TYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME)";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = dr["DOC_TYPE"] + "";
                                com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.ExecuteNonQuery();
                                ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                            }
                        }
                        else
                        {
                            string strSql = @"UPDATE TTRUCK_DOC SET DUPDATE = SYSDATE,SUPDATE = :SUPDATE, CACTIVE=:CACTIVE WHERE DOCID=:DOCID AND STRUCKID=:STRUCKID";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.ExecuteNonQuery();
                            }
                        }
                    }
                }

                //ข้อมูลพรบ.
                using (OracleConnection con = new OracleConnection(conn))
                {
                    string getStatute = CommonFunction.Get_Value(conn, "SELECT STRUCKID FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(RTRUCK) + "' AND CTYPE='STATUTE'");
                    con.Open();
                    string strSql = "";
                    if (string.IsNullOrEmpty(getStatute))
                        strSql = @"INSERT INTO TTRUCK_INSURANCE(STRUCKID,CTYPE,SCOMPANY,START_DURATION,END_DURATION,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                    (:STRUCKID,:CTYPE,:SCOMPANY,:START_DURATION,:END_DURATION,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE)";
                    else
                        strSql = @"UPDATE TTRUCK_INSURANCE SET CTYPE = :CTYPE ,SCOMPANY = :SCOMPANY ,START_DURATION = :START_DURATION ,END_DURATION = :END_DURATION ,SDETAIL = :SDETAIL ,DUPDATE=SYSDATE ,SUPDATE=:SUPDATE
                                        WHERE STRUCKID=:STRUCKID";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "STATUTE";
                        com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtRSCompany.Text + "";
                        if (xDetRSDuration_Start.Value + "" != "")
                            com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetRSDuration_Start.Value);
                        else
                            com.Parameters.Add(":START_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                        if (xDetRSDuration_End.Value + "" != "")
                            com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = Convert.ToDateTime(xDetRSDuration_End.Value);
                        else
                            com.Parameters.Add(":END_DURATION", OracleType.DateTime).Value = OracleDateTime.Null;
                        com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemRSDetail.Text + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                        com.ExecuteNonQuery();
                    }
                }

                //ข้อมูลประกันภัย.
                using (OracleConnection con = new OracleConnection(conn))
                {
                    string getInsure = CommonFunction.Get_Value(conn, "SELECT STRUCKID FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(RTRUCK) + "' AND CTYPE='INSURANCE'");
                    con.Open();
                    string strSql = "";
                    if (string.IsNullOrEmpty(getInsure))
                        strSql = @"INSERT INTO TTRUCK_INSURANCE(STRUCKID,CTYPE,SCOMPANY,INSURE_BUDGET,INSURE_TYPE,HOLDING,NINSURE,SDETAIL,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                            (:STRUCKID,:CTYPE,:SCOMPANY,:INSURE_BUDGET,:INSURE_TYPE,:HOLDING,:NINSURE,:SDETAIL,SYSDATE,:SUPDATE,SYSDATE,:SUPDATE)";
                    else
                        strSql = @"UPDATE TTRUCK_INSURANCE SET CTYPE = :CTYPE ,SCOMPANY = :SCOMPANY ,INSURE_BUDGET = :INSURE_BUDGET ,INSURE_TYPE = :INSURE_TYPE ,HOLDING = :HOLDING ,NINSURE=:NINSURE ,SDETAIL = :SDETAIL ,DUPDATE=SYSDATE ,SUPDATE=:SUPDATE
                                            WHERE STRUCKID=:STRUCKID";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                        com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = "INSURANCE";
                        com.Parameters.Add(":SCOMPANY", OracleType.VarChar).Value = txtRICompany.Text + "";
                        if (txtRIBudget.Text + "" != "")
                            com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = txtRIBudget.Text + "";
                        else
                            com.Parameters.Add(":INSURE_BUDGET", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":INSURE_TYPE", OracleType.VarChar).Value = cbmRIType.Value + "";
                        string sHolding = "";
                        foreach (ListEditItem items in chkRIHolding.Items)
                        {
                            if (items.Selected)
                                sHolding += "," + items.Value;
                        }
                        if (sHolding.Trim() != "")
                            sHolding = sHolding.Substring(1);
                        com.Parameters.Add(":HOLDING", OracleType.VarChar).Value = sHolding.ToString();
                        if ("" + txtRInInsure.Text != "")
                            com.Parameters.Add(":NINSURE", OracleType.Number).Value = txtRInInsure.Text + "";
                        else
                            com.Parameters.Add(":NINSURE", OracleType.Number).Value = OracleNumber.Null;
                        com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = xMemRIDetail.Text + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                        com.ExecuteNonQuery();
                    }
                }

                //เอกสารข้อมูลพรบ. และประกันภัย
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    DataTable dt = (DataTable)Session["RISDoc"];
                    foreach (DataRow dr in dt.Select("DOC_SYSNAME<>''", "CTYPE ASC"))
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            string dest = UploadDirectory + "" + STRUCK + "/" + dr["CTYPE"] + "/";
                            string GenID = CommonFunction.Gen_ID(con, "SELECT DOCID FROM (SELECT DOCID FROM TTRUCK_INSUREDOC ORDER BY DOCID DESC)  WHERE ROWNUM <= 1");

                            string strSql = @"INSERT INTO TTRUCK_INSUREDOC(DOCID,STRUCKID,CTYPE,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) Values
                                            (:DOCID,:STRUCKID,:CTYPE,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME)";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":CTYPE", OracleType.VarChar).Value = dr["CTYPE"] + "";
                                com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.ExecuteNonQuery();
                                ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                            }
                        }
                        else
                        {
                            string strSql = @"UPDATE TTRUCK_INSUREDOC SET DUPDATE = SYSDATE ,SUPDATE = :SUPDATE ,CACTIVE = :CACTIVE
                                             WHERE DOCID=:DOCID AND STRUCKID=:STRUCKID";
                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = RTRUCK;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                com.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            #endregion

            #region เก็บประวัติการระงับ
            //เก็บประวัติBlackList หากโดน BlackList
            if (rblPermit.Value == "N")//ระงับการใช้งาน
            {
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();
                    if (txtPermit.Text.Trim() != "N")
                    {
                        string GenID = CommonFunction.Gen_ID(con, "SELECT BLACKLISTID FROM (SELECT BLACKLISTID FROM TTRUCK_BLACKLIST ORDER BY BLACKLISTID DESC)  WHERE ROWNUM <= 1");

                        string strSql = @"INSERT INTO TTRUCK_BLACKLIST(BLACKLISTID,STRUCKID,REMARK,DBLACKLIST_START,DBLACKLIST_END,SBLACKLIST) Values
                                (:BLACKLISTID,:STRUCKID,:REMARK,SYSDATE,:DBLACKLIST_END,:SBLACKLIST)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":BLACKLISTID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = ScarType != "1" || ScarType != "3" ? STRUCK : RTRUCK;
                            com.Parameters.Add(":REMARK", OracleType.VarChar).Value = xMemBlackListCause.Text;
                            com.Parameters.Add(":DBLACKLIST_END", OracleType.DateTime).Value = Convert.ToDateTime(xDetdBlackListExp.Value);
                            com.Parameters.Add(":SBLACKLIST", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        string strSql = @"UPDATE TTRUCK_BLACKLIST SET REMARK=:REMARK , DBLACKLIST_END=:DBLACKLIST_END WHERE BLACKLISTID=:BLACKLISTID AND STRUCKID=:STRUCKID";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":BLACKLISTID", OracleType.Number).Value = CommonFunction.Get_Value(conn, "SELECT MAX(BLACKLISTID) FROM TTRUCK_BLACKLIST WHERE STRUCKID='" + (ScarType != "1" || ScarType != "3" ? STRUCK : RTRUCK) + "'");
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = ScarType != "1" || ScarType != "3" ? STRUCK : RTRUCK;
                            com.Parameters.Add(":REMARK", OracleType.VarChar).Value = xMemBlackListCause.Text;
                            com.Parameters.Add(":DBLACKLIST_END", OracleType.DateTime).Value = Convert.ToDateTime(xDetdBlackListExp.Value);
                            com.ExecuteNonQuery();
                        }
                    }
                }
            }
            #endregion
        }
        #region ข้อมูลวัดน้ำ

        if (ScarType != "3" || !string.IsNullOrEmpty(RTRUCK))//ตรวจสอบสิทธิ์
        {
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();

                string sQuery = @"UPDATE TTRUCK SET PLACE_WATER_MEASURE=:PLACE_WATER_MEASURE,SCAR_NUM=:SCAR_NUM, DPREV_SERV=:DPREV_SERV, DNEXT_SERV=:DNEXT_SERV, SWUPDATE=:SWUPDATE, DWUPDATE=SYSDATE WHERE STRUCKID=:STRUCKID";

                using (OracleCommand com = new OracleCommand(sQuery, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtGlobal_TRUCKID.Text.Trim();
                    com.Parameters.Add(":PLACE_WATER_MEASURE", OracleType.VarChar).Value = rblPlaceMWater.Value + "";
                    //วัดน้ำภายใน ปตท.
                    if (rblPlaceMWater.Value == "INTERNAL")
                    {
                        com.Parameters.Add(":SCAR_NUM", OracleType.VarChar).Value = txtIsCar_Num.Text;
                        com.Parameters.Add(":DPREV_SERV", OracleType.DateTime).Value = Convert.ToDateTime(xDetIDPREV_SERV.Value);
                        com.Parameters.Add(":DNEXT_SERV", OracleType.DateTime).Value = Convert.ToDateTime(xDetIDNEXT_SERV.Value);
                    }
                    else//วัดน้ำภายนอก ปตท.
                    {
                        com.Parameters.Add(":SCAR_NUM", OracleType.VarChar).Value = txtEsCar_Num.Text;
                        com.Parameters.Add(":DPREV_SERV", OracleType.DateTime).Value = Convert.ToDateTime(xDetEDPREV_SERV.Value);
                        com.Parameters.Add(":DNEXT_SERV", OracleType.DateTime).Value = Convert.ToDateTime(xDetEDNEXT_SERV.Value);
                    }
                    com.Parameters.Add(":SWUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                    com.ExecuteNonQuery();
                }

                #region เอกสารวัดน้ำ
                if (rblPlaceMWater.Value == "EXTERNAL")
                {
                    //เอกสารวัดน้ำ สำหรับกรณีที่มีการวัดน้ำภายนอกปตท.
                    DataTable dt = (DataTable)Session["MWaterDoc"];
                    string dest = UploadDirectory + "" + (ScarType == "3" ? RTRUCK : STRUCK) + "/MWATER/";
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["CNEW"] == "1")
                            {
                                string GenID = CommonFunction.Gen_ID(con, "SELECT DOCID FROM (SELECT DOCID FROM TTRUCK_MWATERDOC ORDER BY DOCID DESC)  WHERE ROWNUM <= 1");

                                string strSql = @"INSERT INTO TTRUCK_MWATERDOC(DOCID,STRUCKID,DOC_NAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DOC_SYSNAME) Values
                                                (:DOCID,:STRUCKID,:DOC_NAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:DOC_SYSNAME)";
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":DOCID", OracleType.Number).Value = GenID;
                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtGlobal_TRUCKID.Text.Trim();
                                    com.Parameters.Add(":DOC_NAME", OracleType.VarChar).Value = dr["DOC_NAME"] + "";
                                    com.Parameters.Add(":DOC_SYSNAME", OracleType.VarChar).Value = dr["DOC_SYSNAME"] + "";
                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                    com.ExecuteNonQuery();
                                    ServerMoveFile(dr["SPATH"].ToString(), dest, dr["DOC_SYSNAME"].ToString());
                                }
                            }
                            else
                            {
                                string strSql = @"UPDATE TTRUCK_MWATERDOC SET DUPDATE = SYSDATE ,SUPDATE = :SUPDATE ,CACTIVE = :CACTIVE
                                                 WHERE DOCID=:DOCID AND STRUCKID=:STRUCKID";
                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":DOCID", OracleType.Number).Value = dr["DOCID"];
                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = ScarType == "3" ? RTRUCK : STRUCK;
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = dr["CACTIVE"] + "";
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }

        #endregion

        #region เคลียร์ไฟล์
        ServerClearFile(UploadDirectory + "Temp/" + STRUCK);
        if (RTRUCK != "")
            ServerClearFile(UploadDirectory + "Temp/" + RTRUCK);
        #endregion

        #region Redirect Page
        string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("View&" + STRUCK + "&" + RTRUCK + "&" + ScarType))
           , sUrl = "truck_edit.aspx?str=";
        ClearSession();
        CommonFunction.SetPopupOnLoad(xcpnMain, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){ window.location='" + (sUrl + sEncrypt) + "';});");
        #endregion
    }
    private void BindData(string mode)
    {
        DataTable dtTruckInfo, dtTruckDoc;
        DataView dvTruckInfo;
        DataSourceSelectArguments args = new DataSourceSelectArguments();
        string STRUCK_ID = txtGlobal_STRUCKID.Text.Trim();
        string RTRUCK_ID = txtGlobal_RTRUCKID.Text.Trim();
        switch (mode)
        {
            case "SET_FORMAT_PAGE":
                #region HeaderText RoundPanel
                if (Session["SCARTYPEID"].ToString() == "1" || Session["SCARTYPEID"].ToString() == "3")//กรณีที่เป็นรถเทรลเลอร์
                {
                    //ตรวจสอบว่ามีรถส่วนหัวหรือไม่ ถ้าไม่มีแสดงว่าหางลอยให้ซ่อนPanelของหัว
                    if ("" + txtGlobal_STRUCKID.Text != "")
                    {
                        dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                        dtTruckInfo = dvTruckInfo.ToTable();
                        if (dtTruckInfo.Rows.Count > 0)
                        {
                            foreach (DataRow drInfo in dtTruckInfo.Rows)
                            {
                                dvHead.Visible = true;
                                rpnHInfomation.HeaderText = "ข้อมูล" + drInfo["SCARTYPENAME"].ToString();
                                rpnHStatute.HeaderText = "พรบ." + drInfo["SCARTYPENAME"].ToString();
                                rpnHInsurance.HeaderText = "การประกันภัย" + drInfo["SCARTYPENAME"].ToString();
                                break;
                            }
                        }
                    }
                    else
                        dvHead.Visible = false;
                    //ตรวจสอบว่ามีรถส่วนหางหรือไม่ ถ้าไม่มีแสดงว่าหัวลอยให้ซ่อนPanelของหาง
                    if ("" + txtGlobal_RTRUCKID != "")
                    {
                        dvTruckInfo = (DataView)sdsRTRUCK.Select(args);
                        dtTruckInfo = dvTruckInfo.ToTable();
                        if (dtTruckInfo.Rows.Count > 0)
                        {
                            foreach (DataRow drInfo in dtTruckInfo.Rows)
                            {
                                dvTrail.Visible = true;
                                rpnTInfomation.HeaderText = "ข้อมูล" + drInfo["SCARTYPENAME"].ToString();
                                rpnTStatute.HeaderText = "พรบ." + drInfo["SCARTYPENAME"].ToString();
                                rpnTInsurance.HeaderText = "การประกันภัย" + drInfo["SCARTYPENAME"].ToString();
                                break;
                            }
                        }
                    }
                    else
                        dvTrail.Visible = false;
                }
                else
                {
                    dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                    dtTruckInfo = dvTruckInfo.ToTable();
                    if (dtTruckInfo.Rows.Count > 0)
                    {
                        foreach (DataRow drInfo in dtTruckInfo.Rows)
                        {
                            dvHead.Visible = true;
                            rpnHInfomation.HeaderText = "ข้อมูล" + drInfo["SCARTYPENAME"].ToString();
                            rpnHStatute.HeaderText = "พรบ." + drInfo["SCARTYPENAME"].ToString();
                            rpnHInsurance.HeaderText = "การประกันภัย" + drInfo["SCARTYPENAME"].ToString();
                            dvTrail.Visible = false;
                            break;
                        }
                    }
                }
                #endregion
                break;
            case "SET_DATA_CONTROL":
                #region SET DDL
                //ยี่ห้อรถ
                dvTruckInfo = (DataView)sdsTBrand.Select(args);
                cboHsBrand.DataSource = dvTruckInfo.ToTable();
                cboHsBrand.DataBind();
                cboRsBrand.DataSource = dvTruckInfo.ToTable();
                cboRsBrand.DataBind();
                //ผู้ให้บริการ GPS
                dvTruckInfo = (DataView)sdsTGPS.Select(args);
                cboHGPSProvider.DataSource = dvTruckInfo.ToTable();
                cboHGPSProvider.DataBind();
                cboRGPSProvider.DataSource = dvTruckInfo.ToTable();
                cboRGPSProvider.DataBind();
                //กลุ่มผลิตภัณฑ์ที่บรรทุก
                dvTruckInfo = (DataView)sdsProdGRP.Select(args);
                cboHProdGRP.DataSource = dvTruckInfo.ToTable();
                cboHProdGRP.DataBind();
                cboRProdGRP.DataSource = dvTruckInfo.ToTable();
                cboRProdGRP.DataBind();
                #endregion
                break;
            case "BIND_TRUCK_HINFO":
                #region รายละเอียดข้อมูลรถ(ส่วนหัว)
                dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Rows)
                    {
                        txtHRegNo.Text = "" + drInfo["SHEADREGISTERNO"];
                        if (!string.IsNullOrEmpty(drInfo["DREGISTER"].ToString()))
                            xDetdHRegNo.Text = Convert.ToDateTime(drInfo["DREGISTER"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        DateTime dcurdate, dSignin;
                        if (!string.IsNullOrEmpty(drInfo["DSIGNIN"].ToString()))
                        {
                            xDetdHFRegNo.Text = Convert.ToDateTime(drInfo["DSIGNIN"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                            dSignin = Convert.ToDateTime("" + drInfo["DSIGNIN"]);
                            dcurdate = DateTime.Now.Date;
                            double nPeroid = (dcurdate - dSignin).TotalDays;
                            if (nPeroid / 365 >= 1)
                                xlbHnPeriod.Text = ((int)(nPeroid / 365)).ToString() + " ปี " + (nPeroid % 365 != 0 ? ((int)(nPeroid % 365) / 30).ToString() + " เดือน " : "");
                            else
                            {
                                if (nPeroid / 30 >= 1)
                                    xlbHnPeriod.Text = ((int)(nPeroid / 30)).ToString() + " เดือน";
                                else
                                    xlbHnPeriod.Text = ((int)(nPeroid)).ToString() + " วัน";
                            }
                        }
                        else
                            xlbHnPeriod.Text = "-";
                        if ("" + drInfo["SHOLDERID"] != "")
                            cmbHsHolder.Value = "" + drInfo["SHOLDERID"];// + " - " + drInfo["SHOLDERNAME"];
                        txtHTru_Cate.Text = "" + drInfo["TRUCK_CATEGORY"];
                        txtVEH_Text.Text = "" + drInfo["VEH_TEXT"];
                        txtVEH_Volume.Text = "" + drInfo["VOL_UOM"];
                        txtHsChasis.Text = "" + drInfo["SCHASIS"];
                        txtHsEngine.Text = "" + drInfo["SENGINE"];
                        if ("" + drInfo["SBRAND"] != "")
                            cboHsBrand.Text = "" + drInfo["SBRAND"];
                        txtHsModel.Text = "" + drInfo["SMODEL"];
                        txtHnWheel.Text = "" + drInfo["NWHEELS"];
                        txtHPowermover.Text = "" + drInfo["POWERMOVER"];
                        txtHnShaftDriven.Text = "" + drInfo["NSHAFTDRIVEN"];
                        cboHsVibration.Text = "" + drInfo["SVIBRATION"];
                        switch (drInfo["PUMPPOWER"].ToString())
                        {
                            case "มี":
                                rblHPumpPower.SelectedIndex = 0;
                                break;
                            case "ไม่มี":
                                rblHPumpPower.SelectedIndex = 1;
                                break;
                            default:
                                rblHPumpPower.SelectedIndex = -1;
                                break;
                        }
                        cboHPumpPower_type.Text = "" + drInfo["PUMPPOWER_TYPE"];
                        cboHMaterialOfPressure.Text = "" + drInfo["MATERIALOFPRESSURE"];
                        if (!string.IsNullOrEmpty(drInfo["VALVETYPE"].ToString()))
                            switch (drInfo["VALVETYPE"].ToString())
                            {
                                case "วาล์วสลิง":
                                    rblHValveType.SelectedIndex = 0;
                                    txtHValveType.ClientEnabled = false;
                                    break;
                                case "วาล์วลม":
                                    rblHValveType.SelectedIndex = 1;
                                    txtHValveType.ClientEnabled = false;
                                    break;
                                default:
                                    rblHValveType.SelectedIndex = 2;
                                    txtHValveType.ClientEnabled = true;
                                    txtHValveType.Text = "" + drInfo["VALVETYPE"];
                                    break;
                            }
                        if (!string.IsNullOrEmpty(drInfo["FUELTYPE"].ToString()))
                            switch (drInfo["FUELTYPE"].ToString())
                            {
                                case "DIESEL":
                                    rblHFuelType.SelectedIndex = 0;
                                    txtHFuelType.ClientEnabled = false;
                                    break;
                                case "NGV":
                                    rblHFuelType.SelectedIndex = 1;
                                    txtHFuelType.ClientEnabled = false;
                                    break;
                                default:
                                    rblHFuelType.SelectedIndex = 2;
                                    txtHFuelType.ClientEnabled = true;
                                    txtHFuelType.Text = "" + drInfo["FUELTYPE"];
                                    break;
                            }
                        if ("" + drInfo["GPS_SERVICE_PROVIDER"] != "")
                            cboHGPSProvider.Text = "" + drInfo["GPS_SERVICE_PROVIDER"];
                        txtHnWeight.Text = "" + drInfo["NWEIGHT"];
                        cboHTankMaterial.Text = "" + drInfo["STANK_MATERAIL"];
                        cboHLoadMethod.Text = "" + drInfo["SLOADING_METHOD"];
                        txtHTank_Maker.Text = "" + drInfo["STANK_MAKER"];
                        if ("" + drInfo["SPROD_GRP"] != "")
                            cboHProdGRP.SelectedItem.SetValue("PROD_ID", "" + drInfo["SPROD_GRP"]);
                        if ("" + drInfo["NLOAD_WEIGHT"] != "")
                            xlbHnLoadWeight.Text = string.Format("{0:n0}", int.Parse("" + drInfo["NLOAD_WEIGHT"]));
                        if ("" + drInfo["NCALC_WEIGHT"] != "")
                            xlbHCalcWeight.Text = string.Format("{0:n0}", int.Parse("" + drInfo["NCALC_WEIGHT"]));

                        break;
                    }
                }
                if (Session["SCARTYPEID"] + "" == "3")
                {
                    tblCapacity.Visible = false;
                    lblHTru_Cate.Visible = false;
                    txtHTru_Cate.Visible = false;
                }
                else
                    BindData("BIND_TRUCK_HCOMPART");
                BindData("BIND_TRUCK_HINFODOC1");
                BindData("BIND_TRUCK_HINFODOC2");
                BindData("BIND_TRUCK_HINFODOC3");
                BindData("BIND_TRUCK_HINFODOC4");
                #endregion
                break;
            case "BIND_TRUCK_HCOMPART":
                #region ความจุรถ(หัว)
                dvTruckInfo = (DataView)sdsSTRUCKCompart.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    xlbHnCap.Text = "";
                    DataTable dtHCompart = PrepareDataTable("SHCompart", "", "" + STRUCK_ID, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                    int maxHComarpt, totalHCompart = 0;
                    for (int i = 1; i <= int.Parse(dtTruckInfo.Compute("MAX(NCOMPARTNO)", string.Empty) + ""); i++)
                    {
                        DataRow dr = dtHCompart.NewRow();
                        dr["NCOMPARTNO"] = i;
                        dr["STRUCKID"] = STRUCK_ID;
                        maxHComarpt = 0;
                        foreach (DataRow drInfo in dtTruckInfo.Select("NCOMPARTNO='" + i + "'", "NPANLEVEL"))
                        {
                            switch (drInfo["NPANLEVEL"].ToString())
                            {
                                case "1":
                                    dr["NPANLEVEL1"] = drInfo["NCAPACITY"].ToString();
                                    break;
                                case "2":
                                    dr["NPANLEVEL2"] = drInfo["NCAPACITY"].ToString();
                                    break;
                                case "3":
                                    dr["NPANLEVEL3"] = drInfo["NCAPACITY"].ToString();
                                    break;
                            }
                            maxHComarpt = (drInfo["NCAPACITY"] + "" == "" ? maxHComarpt : (int.Parse(drInfo["NCAPACITY"] + "") > maxHComarpt ? int.Parse(drInfo["NCAPACITY"] + "") : maxHComarpt));
                        }
                        dr["CNEW"] = "0";
                        dr["CCHANGE"] = "0";
                        dr["CDEL"] = "0";
                        dtHCompart.Rows.Add(dr);
                        totalHCompart += maxHComarpt;
                    }
                    Session["SHCompart"] = dtHCompart;
                    gvwHCompart.DataSource = dtHCompart;
                    gvwHCompart.DataBind();
                    xlbHnSlot.Text = dtTruckInfo.Compute("MAX(NCOMPARTNO)", string.Empty) + "";
                    xlbHnTatolCapacity.Text = string.Format("{0:n0}", totalHCompart);
                }
                #endregion
                break;
            case "BIND_HDOC_DB":
                #region BIND_HDOC_DB
                if (Session["HDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvTruckInfo = (DataView)sdsSTRUCKDoc.Select(args);
                    dtTruckDoc = PrepareDataTable("HDoc", "", "" + STRUCK_ID
                    , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                    foreach (DataRow dr in dvTruckInfo.ToTable().Rows)
                    {
                        DataRow drNewRow = dtTruckDoc.NewRow();
                        drNewRow["DOCID"] = "" + dr["DOCID"];
                        drNewRow["STRUCKID"] = "" + dr["STRUCKID"];
                        drNewRow["DOC_TYPE"] = "" + dr["DOC_TYPE"];
                        drNewRow["DOC_NAME"] = "" + dr["DOC_NAME"];
                        drNewRow["DOC_SYSNAME"] = "" + dr["DOC_SYSNAME"];
                        drNewRow["DCREATE"] = "" + dr["DCREATE"];
                        drNewRow["SCREATE"] = "" + dr["SCREATE"];
                        drNewRow["DUPDATE"] = "" + dr["DUPDATE"];
                        drNewRow["SUPDATE"] = "" + dr["SUPDATE"];
                        drNewRow["CACTIVE"] = "" + dr["CACTIVE"];
                        drNewRow["SPATH"] = "" + dr["SPATH"];
                        drNewRow["CNEW"] = "" + dr["CNEW"];
                        dtTruckDoc.Rows.Add(drNewRow);
                    }
                    Session["HDoc"] = dtTruckDoc;
                }
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC1":
                #region แนบเอกสารใบจดทะเบียนรถ(หัว)
                BindData("BIND_HDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='1'"));
                gvwSTRUCKDoc1.DataSource = dtTruckDoc;
                gvwSTRUCKDoc1.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC2":
                #region แนบหลักฐานการเสียภาษี(หัว)
                BindData("BIND_HDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='2'"));
                gvwSTRUCKDoc2.DataSource = dtTruckDoc;
                gvwSTRUCKDoc2.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC3":
                #region แนบเอกสาร Compartment(หัว)
                BindData("BIND_HDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='3'"));
                gvwSTRUCKDoc3.DataSource = dtTruckDoc;
                gvwSTRUCKDoc3.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_HINFODOC4":
                #region แนบเอกสารใบจดทะเบียนรถครั้งแรก(หัว)
                BindData("BIND_HDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HDoc"], ((DataTable)Session["HDoc"]).Select("CACTIVE='1' AND DOC_TYPE='4'"));
                gvwSTRUCKDoc4.DataSource = dtTruckDoc;
                gvwSTRUCKDoc4.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_HINSURE_DB":
                #region ข้อมูลพรบ.และประกันภัย(หัว)
                dvTruckInfo = (DataView)sdsSTRUCKINSURE.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Select("CTYPE='STATUTE'"))
                    {
                        txtHSCompany.Text = drInfo["SCOMPANY"].ToString();
                        if (!string.IsNullOrEmpty(drInfo["START_DURATION"].ToString()))
                            xDetHSDuration_Start.Text = Convert.ToDateTime(drInfo["START_DURATION"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        if (!string.IsNullOrEmpty(drInfo["END_DURATION"].ToString()))
                            xDetHSDuration_End.Text = Convert.ToDateTime(drInfo["END_DURATION"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        xMemHSDetail.Text = drInfo["SDETAIL"].ToString();
                        break;
                    }

                    foreach (DataRow drInfo in dtTruckInfo.Select("CTYPE='INSURANCE'"))
                    {
                        txtHICompany.Text = drInfo["SCOMPANY"].ToString();
                        txtHIBudget.Text = drInfo["INSURE_BUDGET"].ToString();
                        cbmHIType.Value = drInfo["INSURE_TYPE"].ToString();
                        string[] sHolding = drInfo["HOLDING"].ToString().Split(',');
                        if (!string.IsNullOrEmpty(sHolding[0]))
                        {
                            foreach (ListEditItem items in chkHIHolding.Items)
                            {
                                for (int i = 0; i < sHolding.Length; i++)
                                {
                                    if (items.Text == sHolding[i])
                                        items.Selected = true;
                                }
                            }
                        }
                        txtHInInsure.Text = drInfo["NINSURE"].ToString();
                        xMemHIDetail.Text = drInfo["SDETAIL"].ToString();
                        break;
                    }
                }
                BindData("BIND_TRUCK_HSTATUTEDOC");
                BindData("BIND_TRUCK_HINSUREDOC");
                #endregion
                break;
            case "BIND_HISDOC_DB":
                #region BIND_HISDOC_DB
                if (Session["HISDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvTruckInfo = (DataView)sdsSTRUCKISDoc.Select(args);
                    dtTruckDoc = PrepareDataTable("HISDoc", "", "" + STRUCK_ID
                    , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                    foreach (DataRow dr in dvTruckInfo.ToTable().Rows)
                    {
                        DataRow drNewRow = dtTruckDoc.NewRow();
                        drNewRow["DOCID"] = "" + dr["DOCID"];
                        drNewRow["STRUCKID"] = "" + dr["STRUCKID"];
                        drNewRow["CTYPE"] = "" + dr["CTYPE"];
                        drNewRow["DOC_NAME"] = "" + dr["DOC_NAME"];
                        drNewRow["DOC_SYSNAME"] = "" + dr["DOC_SYSNAME"];
                        drNewRow["DCREATE"] = "" + dr["DCREATE"];
                        drNewRow["SCREATE"] = "" + dr["SCREATE"];
                        drNewRow["DUPDATE"] = "" + dr["DUPDATE"];
                        drNewRow["SUPDATE"] = "" + dr["SUPDATE"];
                        drNewRow["CACTIVE"] = "" + dr["CACTIVE"];
                        drNewRow["SPATH"] = "" + dr["SPATH"];
                        drNewRow["CNEW"] = "" + dr["CNEW"];
                        dtTruckDoc.Rows.Add(drNewRow);
                    }
                    Session["HISDoc"] = dtTruckDoc;
                }
                #endregion
                break;
            case "BIND_TRUCK_HSTATUTEDOC":
                #region แนบเอกสารพรบ.(หัว)
                BindData("BIND_HISDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HISDoc"], ((DataTable)Session["HISDoc"]).Select("CACTIVE='1' AND CTYPE='STATUTE'"));
                gvwSTRUCKSDoc.DataSource = dtTruckDoc;
                gvwSTRUCKSDoc.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_HINSUREDOC":
                #region แนบเอกสารประกันภัย(หัว)
                BindData("BIND_HISDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["HISDoc"], ((DataTable)Session["HISDoc"]).Select("CACTIVE='1' AND CTYPE='INSURANCE'"));
                gvwSTRUCKIDoc.DataSource = dtTruckDoc;
                gvwSTRUCKIDoc.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;

            case "BIND_TRUCK_RINFO":
                #region รายละเอียดข้อมูลรถ(หาง)
                dvTruckInfo = (DataView)sdsRTRUCK.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Rows)
                    {
                        txtRRegNo.Text = drInfo["SHEADREGISTERNO"].ToString();
                        if (!string.IsNullOrEmpty(drInfo["DREGISTER"].ToString()))
                            xDetdRRegNo.Text = Convert.ToDateTime(drInfo["DREGISTER"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        DateTime dcurdate, dSignin;
                        if (!string.IsNullOrEmpty(drInfo["DSIGNIN"].ToString()))
                        {
                            xDetdRFRegNo.Text = Convert.ToDateTime(drInfo["DSIGNIN"], new CultureInfo("th-th")).ToString("dd/MM/yyyy");
                            dSignin = Convert.ToDateTime(drInfo["DSIGNIN"].ToString());
                            dcurdate = DateTime.Now.Date;
                            double nPeroid = (dcurdate - dSignin).TotalDays;
                            if (nPeroid / 365 >= 1)
                                xlbRnPeriod.Text = ((int)(nPeroid / 365)).ToString() + " ปี " + (nPeroid % 365 != 0 ? ((int)(nPeroid % 365) / 30).ToString() + " เดือน " : "");
                            else
                            {
                                if (nPeroid / 30 >= 1)
                                    xlbRnPeriod.Text = ((int)(nPeroid / 30)).ToString() + " เดือน";
                                else
                                    xlbRnPeriod.Text = ((int)(nPeroid)).ToString() + " วัน";
                            }
                        }
                        else
                            xlbRnPeriod.Text = "-";
                        if ("" + drInfo["SHOLDERID"] != "")
                            cmbRsHolder.Value = "" + drInfo["SHOLDERID"];// + " - " + drInfo["SHOLDERNAME"];
                        txtRTru_Cate.Text = "" + drInfo["TRUCK_CATEGORY"];
                        txtTU_Text.Text = "" + drInfo["VEH_TEXT"];
                        txtTU_Volume.Text = "" + drInfo["VOL_UOM"];
                        txtRsChasis.Text = drInfo["SCHASIS"].ToString();
                        txtRsEngine.Text = drInfo["SENGINE"].ToString();
                        if ("" + drInfo["SBRAND"] != "")
                            cboRsBrand.Text = drInfo["SBRAND"].ToString();
                        txtRsModel.Text = drInfo["SMODEL"].ToString();
                        txtRnWheel.Text = drInfo["NWHEELS"].ToString();
                        txtRPowermover.Text = drInfo["POWERMOVER"].ToString();
                        txtRnShaftDriven.Text = drInfo["NSHAFTDRIVEN"].ToString();
                        cboRsVibration.Text = drInfo["SVIBRATION"].ToString();
                        switch (drInfo["PUMPPOWER"].ToString())
                        {
                            case "มี":
                                rblRPumpPower.SelectedIndex = 0;
                                break;
                            case "ไม่มี":
                                rblRPumpPower.SelectedIndex = 1;
                                break;
                            default:
                                rblRPumpPower.SelectedIndex = -1;
                                break;
                        }
                        cboRPumpPower_type.Text = drInfo["PUMPPOWER_TYPE"].ToString();
                        cboRMaterialOfPressure.Text = drInfo["MATERIALOFPRESSURE"].ToString();
                        if (!string.IsNullOrEmpty(drInfo["VALVETYPE"].ToString()))
                            switch (drInfo["VALVETYPE"].ToString())
                            {
                                case "วาล์วสลิง":
                                    rblRValveType.SelectedIndex = 0;
                                    txtRValveType.ClientEnabled = false;
                                    break;
                                case "วาล์วลม":
                                    rblRValveType.SelectedIndex = 1;
                                    txtRValveType.ClientEnabled = false;
                                    break;
                                default:
                                    rblRValveType.SelectedIndex = 2;
                                    txtRValveType.ClientEnabled = true;
                                    txtRValveType.Text = drInfo["VALVETYPE"].ToString();
                                    break;
                            }
                        if (!string.IsNullOrEmpty(drInfo["FUELTYPE"].ToString()))
                            switch (drInfo["FUELTYPE"].ToString())
                            {
                                case "DIESEL":
                                    rblRFuelType.SelectedIndex = 0;
                                    txtRFuelType.ClientEnabled = false;
                                    break;
                                case "NGV":
                                    rblRFuelType.SelectedIndex = 1;
                                    txtRFuelType.ClientEnabled = false;
                                    break;
                                default:
                                    rblRFuelType.SelectedIndex = 2;
                                    txtRFuelType.ClientEnabled = true;
                                    txtRFuelType.Text = drInfo["FUELTYPE"].ToString();
                                    break;
                            }
                        if ("" + drInfo["GPS_SERVICE_PROVIDER"] != "")
                            cboRGPSProvider.Text = drInfo["GPS_SERVICE_PROVIDER"].ToString();
                        txtRnWeight.Text = drInfo["NWEIGHT"].ToString();
                        cboRTankMaterial.Text = drInfo["STANK_MATERAIL"].ToString();
                        cboRLoadMethod.Text = drInfo["SLOADING_METHOD"].ToString();
                        txtRTank_Maker.Text = drInfo["STANK_MAKER"].ToString();
                        if ("" + drInfo["SPROD_GRP"] != "")
                            cboRProdGRP.SelectedItem.SetValue("PROD_ID", "" + drInfo["SPROD_GRP"]);
                        if ("" + drInfo["NLOAD_WEIGHT"] != "")
                            xlbRnLoadWeight.Text = string.Format("{0:n0}", "" + drInfo["NLOAD_WEIGHT"]);
                        if ("" + drInfo["NCALC_WEIGHT"] != "")
                            xlbRCalcWeight.Text = string.Format("{0:n0}", "" + drInfo["NCALC_WEIGHT"]);
                        break;
                    }
                }
                BindData("BIND_TRUCK_RCOMPART");
                BindData("BIND_TRUCK_RINFODOC1");
                BindData("BIND_TRUCK_RINFODOC2");
                BindData("BIND_TRUCK_RINFODOC3");
                BindData("BIND_TRUCK_RINFODOC4");
                #endregion
                break;
            case "BIND_TRUCK_RCOMPART":
                #region ความจุรถ(หาง)
                dvTruckInfo = (DataView)sdsRTRUCKCompart.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    xlbRnCap.Text = "";
                    DataTable dtRCompart = PrepareDataTable("SRCompart", "", "" + RTRUCK_ID, "NCOMPARTNO", "NPANLEVEL1", "NPANLEVEL2", "NPANLEVEL3", "STRUCKID", "CNEW", "CCHANGE", "CDEL");
                    int maxRComarpt, totalRCompart = 0;
                    for (int i = 1; i <= int.Parse(dtTruckInfo.Compute("MAX(NCOMPARTNO)", string.Empty) + ""); i++)
                    {
                        DataRow dr = dtRCompart.NewRow();
                        dr["NCOMPARTNO"] = i;
                        dr["STRUCKID"] = RTRUCK_ID;
                        maxRComarpt = 0;
                        foreach (DataRow drInfo in dtTruckInfo.Select("NCOMPARTNO='" + i + "'", "NPANLEVEL"))
                        {
                            switch (drInfo["NPANLEVEL"].ToString())
                            {
                                case "1":
                                    dr["NPANLEVEL1"] = drInfo["NCAPACITY"].ToString();
                                    break;
                                case "2":
                                    dr["NPANLEVEL2"] = drInfo["NCAPACITY"].ToString();
                                    break;
                                case "3":
                                    dr["NPANLEVEL3"] = drInfo["NCAPACITY"].ToString();
                                    break;
                            }
                            maxRComarpt = (drInfo["NCAPACITY"] + "" == "" ? maxRComarpt : (int.Parse(drInfo["NCAPACITY"] + "") > maxRComarpt ? int.Parse(drInfo["NCAPACITY"] + "") : maxRComarpt));
                        }
                        dr["CNEW"] = "0";
                        dr["CCHANGE"] = "0";
                        dr["CDEL"] = "0";
                        dtRCompart.Rows.Add(dr);
                        totalRCompart += maxRComarpt;
                    }
                    Session["SRCompart"] = dtRCompart;
                    gvwRCompart.DataSource = dtRCompart;
                    gvwRCompart.DataBind();
                    xlbRnSlot.Text = dtTruckInfo.Compute("MAX(NCOMPARTNO)", string.Empty) + "";
                    xlbRnTatolCapacity.Text = string.Format("{0:n0}", totalRCompart);
                }
                #endregion
                break;
            case "BIND_RDOC_DB":
                #region BIND_HDOC_DB
                if (Session["RDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvTruckInfo = (DataView)sdsRTRUCKDoc.Select(args);
                    dtTruckDoc = PrepareDataTable("RDoc", "", "" + STRUCK_ID
                    , "DOCID", "STRUCKID", "DOC_TYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                    foreach (DataRow dr in dvTruckInfo.ToTable().Rows)
                    {
                        DataRow drNewRow = dtTruckDoc.NewRow();
                        drNewRow["DOCID"] = "" + dr["DOCID"];
                        drNewRow["STRUCKID"] = "" + dr["STRUCKID"];
                        drNewRow["DOC_TYPE"] = "" + dr["DOC_TYPE"];
                        drNewRow["DOC_NAME"] = "" + dr["DOC_NAME"];
                        drNewRow["DOC_SYSNAME"] = "" + dr["DOC_SYSNAME"];
                        drNewRow["DCREATE"] = "" + dr["DCREATE"];
                        drNewRow["SCREATE"] = "" + dr["SCREATE"];
                        drNewRow["DUPDATE"] = "" + dr["DUPDATE"];
                        drNewRow["SUPDATE"] = "" + dr["SUPDATE"];
                        drNewRow["CACTIVE"] = "" + dr["CACTIVE"];
                        drNewRow["SPATH"] = "" + dr["SPATH"];
                        drNewRow["CNEW"] = "" + dr["CNEW"];
                        dtTruckDoc.Rows.Add(drNewRow);
                    }
                    Session["RDoc"] = dtTruckDoc;
                }
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC1":
                #region แนบเอกสารใบจดทะเบียนรถ(หาง)
                BindData("BIND_RDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='1'"));
                gvwRTRUCKDoc1.DataSource = dtTruckDoc;
                gvwRTRUCKDoc1.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC2":
                #region แนบหลักฐานการเสียภาษี(หาง)
                BindData("BIND_RDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='2'"));
                gvwRTRUCKDoc2.DataSource = dtTruckDoc;
                gvwRTRUCKDoc2.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC3":
                #region แนบเอกสาร Compartment(หาง)
                BindData("BIND_RDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='3'"));
                gvwRTRUCKDoc3.DataSource = dtTruckDoc;
                gvwRTRUCKDoc3.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_RINFODOC4":
                #region แนบเอกสารใบจดทะเบียนรถครั้งแรก(หาง)
                BindData("BIND_RDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RDoc"], ((DataTable)Session["RDoc"]).Select("CACTIVE='1' AND DOC_TYPE='4'"));
                gvwRTRUCKDoc4.DataSource = dtTruckDoc;
                gvwRTRUCKDoc4.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_RINSURE_DB":
                #region ข้อมูลพรบ.และประกันภัย(หาง)
                dvTruckInfo = (DataView)sdsRTRUCKINSURE.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Select("CTYPE='STATUTE'"))
                    {
                        txtRSCompany.Text = drInfo["SCOMPANY"].ToString();
                        if (!string.IsNullOrEmpty(drInfo["START_DURATION"].ToString()))
                            xDetRSDuration_Start.Text = Convert.ToDateTime(drInfo["START_DURATION"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        if (!string.IsNullOrEmpty(drInfo["END_DURATION"].ToString()))
                            xDetRSDuration_End.Text = Convert.ToDateTime(drInfo["END_DURATION"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        xMemRSDetail.Text = drInfo["SDETAIL"].ToString();
                        break;
                    }

                    foreach (DataRow drInfo in dtTruckInfo.Select("CTYPE='INSURANCE'"))
                    {
                        txtRICompany.Text = drInfo["SCOMPANY"].ToString();
                        txtRIBudget.Text = drInfo["INSURE_BUDGET"].ToString();
                        cbmRIType.Value = drInfo["INSURE_TYPE"].ToString();
                        string[] sHolding = drInfo["HOLDING"].ToString().Split(',');
                        if (!string.IsNullOrEmpty(sHolding[0]))
                        {
                            foreach (ListEditItem items in chkRIHolding.Items)
                            {
                                for (int i = 0; i < sHolding.Length; i++)
                                {
                                    if (items.Text == sHolding[i])
                                        items.Selected = true;
                                }
                            }
                        }
                        txtRInInsure.Text = drInfo["NINSURE"].ToString();
                        xMemRIDetail.Text = drInfo["SDETAIL"].ToString();
                        break;
                    }
                }
                BindData("BIND_TRUCK_RSTATUTEDOC");
                BindData("BIND_TRUCK_RINSUREDOC");
                #endregion
                break;
            case "BIND_RISDOC_DB":
                #region BIND_RISDOC_DB
                if (Session["RISDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvTruckInfo = (DataView)sdsRTRUCKISDoc.Select(args);
                    dtTruckDoc = PrepareDataTable("RISDoc", "", "" + STRUCK_ID
                    , "DOCID", "STRUCKID", "CTYPE", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                    foreach (DataRow dr in dvTruckInfo.ToTable().Rows)
                    {
                        DataRow drNewRow = dtTruckDoc.NewRow();
                        drNewRow["DOCID"] = "" + dr["DOCID"];
                        drNewRow["STRUCKID"] = "" + dr["STRUCKID"];
                        drNewRow["CTYPE"] = "" + dr["CTYPE"];
                        drNewRow["DOC_NAME"] = "" + dr["DOC_NAME"];
                        drNewRow["DOC_SYSNAME"] = "" + dr["DOC_SYSNAME"];
                        drNewRow["DCREATE"] = "" + dr["DCREATE"];
                        drNewRow["SCREATE"] = "" + dr["SCREATE"];
                        drNewRow["DUPDATE"] = "" + dr["DUPDATE"];
                        drNewRow["SUPDATE"] = "" + dr["SUPDATE"];
                        drNewRow["CACTIVE"] = "" + dr["CACTIVE"];
                        drNewRow["SPATH"] = "" + dr["SPATH"];
                        drNewRow["CNEW"] = "" + dr["CNEW"];
                        dtTruckDoc.Rows.Add(drNewRow);
                    }
                    Session["RISDoc"] = dtTruckDoc;
                }
                #endregion
                break;
            case "BIND_TRUCK_RSTATUTEDOC":
                #region แนบเอกสารพรบ.(หาง)
                BindData("BIND_RISDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RISDoc"], ((DataTable)Session["RISDoc"]).Select("CACTIVE='1' AND CTYPE='STATUTE'"));
                gvwRTRUCKSDoc.DataSource = dtTruckDoc;
                gvwRTRUCKSDoc.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_RINSUREDOC":
                #region แนบเอกสารประกันภัย(หาง)
                BindData("BIND_RISDOC_DB");
                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["RISDoc"], ((DataTable)Session["RISDoc"]).Select("CACTIVE='1' AND CTYPE='INSURANCE'"));
                gvwRTRUCKIDoc.DataSource = dtTruckDoc;
                gvwRTRUCKIDoc.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_MWATER":
                #region ข้อมูลวัดน้ำ
                if (Session["SCARTYPEID"].ToString() != "1" && Session["SCARTYPEID"].ToString() != "3")
                    dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                else
                {
                    if ("" + txtGlobal_RTRUCKID.Text == "") break;
                    dvTruckInfo = (DataView)sdsRTRUCK.Select(args);
                }
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Rows)
                    {
                        if (drInfo["PLACE_WATER_MEASURE"].ToString().Trim() == "INTERNAL")
                        {
                            rblPlaceMWater.SelectedIndex = 0;
                            txtEsCar_Num.ClientEnabled = false;
                            xDetEDPREV_SERV.ClientEnabled = false;
                            xDetEDNEXT_SERV.ClientEnabled = false;
                            uldMWaterDoc.Enabled = false;
                            xbnMWaterDoc.ClientEnabled = false;
                            txtIsCar_Num.Text = !string.IsNullOrEmpty(drInfo["SCAR_NUM"].ToString()) ? drInfo["SCAR_NUM"].ToString() : "";
                            if (!string.IsNullOrEmpty(drInfo["DPREV_SERV"].ToString()))
                                xDetIDPREV_SERV.Text = Convert.ToDateTime("" + drInfo["DPREV_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                            if (!string.IsNullOrEmpty(drInfo["DNEXT_SERV"].ToString()))
                                xDetIDNEXT_SERV.Text = Convert.ToDateTime("" + drInfo["DNEXT_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        }
                        else if (drInfo["PLACE_WATER_MEASURE"].ToString() == "EXTERNAL")
                        {
                            rblPlaceMWater.SelectedIndex = 1;
                            txtIsCar_Num.ClientEnabled = false;
                            xDetIDPREV_SERV.ClientEnabled = false;
                            xDetIDNEXT_SERV.ClientEnabled = false;
                            txtEsCar_Num.Text = !string.IsNullOrEmpty(drInfo["SCAR_NUM"].ToString()) ? drInfo["SCAR_NUM"].ToString() : "";
                            if (!string.IsNullOrEmpty(drInfo["DPREV_SERV"].ToString()))
                                xDetEDPREV_SERV.Text = Convert.ToDateTime("" + drInfo["DPREV_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                            if (!string.IsNullOrEmpty(drInfo["DNEXT_SERV"].ToString()))
                                xDetEDNEXT_SERV.Text = Convert.ToDateTime("" + drInfo["DNEXT_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                            BindData("BIND_TRUCK_MWATERDOC");
                        }
                        else
                        {
                            rblPlaceMWater.SelectedIndex = -1;
                            txtIsCar_Num.Text = !string.IsNullOrEmpty(drInfo["SCAR_NUM"].ToString()) ? drInfo["SCAR_NUM"].ToString() : "";
                            if (!string.IsNullOrEmpty(drInfo["DPREV_SERV"].ToString()))
                                xDetIDPREV_SERV.Text = Convert.ToDateTime("" + drInfo["DPREV_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                            if (!string.IsNullOrEmpty(drInfo["DNEXT_SERV"].ToString()))
                                xDetIDNEXT_SERV.Text = Convert.ToDateTime("" + drInfo["DNEXT_SERV"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        }
                        break;
                    }
                }
                #endregion
                break;
            case "BIND_TRUCK_MWATERDOC":
                #region แนบไฟล์ข้อมูลวัดน้ำ
                if (Session["MWaterDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvTruckInfo = (DataView)sdsSTRUCKMWaterDoc.Select(args);
                    dtTruckDoc = PrepareDataTable("MWaterDoc", "", "" + (!string.IsNullOrEmpty(RTRUCK_ID) ? RTRUCK_ID : STRUCK_ID)
                    , "DOCID", "STRUCKID", "DOC_NAME", "DOC_SYSNAME", "DCREATE", "SCREATE", "DUPDATE", "SUPDATE", "CACTIVE", "SPATH", "CNEW");
                    foreach (DataRow dr in dvTruckInfo.ToTable().Rows)
                    {
                        DataRow drNewRow = dtTruckDoc.NewRow();
                        drNewRow["DOCID"] = "" + dr["DOCID"];
                        drNewRow["STRUCKID"] = "" + dr["STRUCKID"];
                        drNewRow["DOC_NAME"] = "" + dr["DOC_NAME"];
                        drNewRow["DOC_SYSNAME"] = "" + dr["DOC_SYSNAME"];
                        drNewRow["DCREATE"] = "" + dr["DCREATE"];
                        drNewRow["SCREATE"] = "" + dr["SCREATE"];
                        drNewRow["DUPDATE"] = "" + dr["DUPDATE"];
                        drNewRow["SUPDATE"] = "" + dr["SUPDATE"];
                        drNewRow["CACTIVE"] = "" + dr["CACTIVE"];
                        drNewRow["SPATH"] = "" + dr["SPATH"];
                        drNewRow["CNEW"] = "" + dr["CNEW"];
                        dtTruckDoc.Rows.Add(drNewRow);
                    }
                    Session["MWaterDoc"] = dtTruckDoc;
                }

                dtTruckDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["MWaterDoc"], ((DataTable)Session["MWaterDoc"]).Select("CACTIVE='1'"));
                gvwSTRUCKMWaterDoc.DataSource = dtTruckDoc;
                gvwSTRUCKMWaterDoc.DataBind();
                dtTruckDoc.Dispose();
                #endregion
                break;
            case "BIND_TRUCK_CONTRACT":
                #region ข้อมูลการใช้งานและถือครอง
                if (Session["SCARTYPEID"].ToString() != "1" && Session["SCARTYPEID"].ToString() != "3")
                    dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                else
                    dvTruckInfo = (DataView)sdsRTRUCK.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Rows)
                    {
                        txtContractID.Text = "" + drInfo["CONTRACKID"];
                        txtVenderID.Text = "" + drInfo["SVENDORID"];
                        lbkContract.Text = !string.IsNullOrEmpty(drInfo["SCONTRACTNO"].ToString()) ? drInfo["SCONTRACTNO"].ToString() : "-";
                        string sTranSport = CommonFunction.Get_Value(conn, "SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID='" + "" + drInfo["SVENDORID"] + "'");
                        xlbsTransport.Text = !string.IsNullOrEmpty(sTranSport) ? sTranSport : "-";
                        break;
                    }
                }
                #endregion
                break;
            case "BIND_TRUCK_PERMIT":
                #region การอนุญาตรับงาน
                if (Session["SCARTYPEID"].ToString() != "1" && Session["SCARTYPEID"].ToString() != "3")
                    dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                else
                {
                    if ("" + txtGlobal_RTRUCKID.Text != "")
                        dvTruckInfo = (DataView)sdsRTRUCK.Select(args);
                    else
                        dvTruckInfo = (DataView)sdsSTRUCK.Select(args);
                }
                dtTruckInfo = dvTruckInfo.ToTable();
                if (dtTruckInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtTruckInfo.Rows)
                    {
                        string cActive = !string.IsNullOrEmpty(drInfo["CACTIVE"].ToString()) ? drInfo["CACTIVE"].ToString() : "";
                        txtPermit.Text = cActive;
                        switch (cActive)
                        {
                            case "Y":
                                rblPermit.SelectedIndex = 0;
                                xDetdBlackListExp.ClientEnabled = false;
                                xMemBlackListCause.ClientEnabled = false;
                                break;
                            case "N":
                                rblPermit.SelectedIndex = 1;
                                xDetdBlackListExp.ClientEnabled = true;
                                xMemBlackListCause.ClientEnabled = true;
                                if (!string.IsNullOrEmpty(drInfo["DBLACKLIST2"].ToString()))
                                    xDetdBlackListExp.Text = Convert.ToDateTime("" + drInfo["DBLACKLIST2"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                                xMemBlackListCause.Text = !string.IsNullOrEmpty(drInfo["BLACKLIST_CAUSE"].ToString()) ? drInfo["BLACKLIST_CAUSE"].ToString() : "";
                                break;
                            case "D":
                                rblPermit.SelectedIndex = 2;
                                xDetdBlackListExp.ClientEnabled = false;
                                xMemBlackListCause.ClientEnabled = false;
                                break;
                            default:
                                rblPermit.SelectedIndex = -1;
                                xDetdBlackListExp.ClientEnabled = false;
                                xMemBlackListCause.ClientEnabled = false;
                                break;
                        }
                        break;
                    }
                }
                args = new DataSourceSelectArguments();
                dvTruckInfo = (DataView)sdsSTRUCKBlacklist.Select(args);
                dtTruckInfo = dvTruckInfo.ToTable();

                DataTable dtBlackList = new DataTable();
                dtBlackList.Columns.Add("BLACKLISTID", typeof(int));
                dtBlackList.Columns.Add("REMARK", typeof(string));
                dtBlackList.Columns.Add("SDATE", typeof(string));
                dtBlackList.Columns.Add("TDATE", typeof(string));
                dtBlackList.Columns.Add("SBLACKLIST", typeof(string));
                foreach (DataRow drBL in dtTruckInfo.Rows)
                {
                    DataRow drBlackList = dtBlackList.NewRow();
                    drBlackList["BLACKLISTID"] = drBL["BLACKLISTID"].ToString();
                    drBlackList["REMARK"] = drBL["REMARK"].ToString();
                    if (drBL["DBLACKLIST_START"] + "" != "")
                        drBlackList["SDATE"] = Convert.ToDateTime(drBL["DBLACKLIST_START"]).ToString("dd/MM/yyyy - HH:mm", new CultureInfo("th-TH")) + " น.";
                    if (drBL["DBLACKLIST_END"] + "" != "")
                        drBlackList["TDATE"] = Convert.ToDateTime(drBL["DBLACKLIST_END"]).ToString("dd/MM/yyyy - HH:mm", new CultureInfo("th-TH")) + " น.";
                    drBlackList["SBLACKLIST"] = CommonFunction.Get_Value(conn, "SELECT SFIRSTNAME||'  '||SLASTNAME FROM TUSER WHERE SUID='" + drBL["SBLACKLIST"].ToString() + "'");
                    dtBlackList.Rows.Add(drBlackList);
                }
                gvwBlacklist.DataSource = dtBlackList;
                gvwBlacklist.DataBind();
                #endregion
                break;
        }
    }
    private void ChangeMode(string mode)
    {
        string cPermission = Session["chkurl"] + "";
        if (cPermission == "1")
            xbnEditMode.ClientVisible = false;
        if (mode == "Edit" && cPermission == "1")
            mode = "View";
        bool V = true, W;
        string sColor = "#EEEEEE", wColor = "";
        if (mode == "View")
        {
            W = true;
            wColor = "#EEEEEE";
            xbnEditMode.Visible = W;
            xbnViewMode.Visible = !W;
        }
        else
        {
            W = false;
            wColor = "#FFFFFF";
            xbnEditMode.Visible = W;
            xbnViewMode.Visible = !W;
        }

        //ข้อมูลรถส่วนหัว
        rpnHInfomation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cmbHsHolder.ReadOnly = V;
        xDetdHRegNo.ReadOnly = V;
        xDetdHFRegNo.ReadOnly = V;
        txtHTru_Cate.ReadOnly = V;
        txtVEH_Text.ReadOnly = V;
        txtVEH_Volume.ReadOnly = V;
        txtHsChasis.ReadOnly = V;
        txtHsEngine.ReadOnly = V;
        cboHsBrand.ReadOnly = V;
        txtHsModel.ReadOnly = V;
        txtHnWheel.ReadOnly = V;
        txtHPowermover.ReadOnly = V;
        txtHnShaftDriven.ReadOnly = V;
        cboHsVibration.ReadOnly = V;
        rblHPumpPower.ReadOnly = V;
        cboHPumpPower_type.ReadOnly = V;
        cboHMaterialOfPressure.ReadOnly = V;
        rblHValveType.ReadOnly = V;
        if (rblHValveType.Value == "Other")
            txtHValveType.ClientEnabled = !V;
        rblHFuelType.ReadOnly = V;
        if (rblHFuelType.Value == "Other")
            txtHFuelType.ClientEnabled = !V;
        cboHGPSProvider.ReadOnly = V;
        txtHnWeight.ReadOnly = V;
        gvwHCompart.Columns[4].Visible = !V;
        tblHNewCap.Visible = !V;
        cboHTankMaterial.ReadOnly = V;
        cboHLoadMethod.ReadOnly = V;
        txtHTank_Maker.ReadOnly = V;
        cboHProdGRP.ReadOnly = V;
        uldHDoc01.Enabled = !V;
        xbnHDoc01.Enabled = !V;
        gvwSTRUCKDoc1.Columns[2].Visible = !V;
        gvwSTRUCKDoc1.Columns[3].Visible = V;
        uldHDoc02.Enabled = !V;
        xbnHDoc02.Enabled = !V;
        gvwSTRUCKDoc2.Columns[2].Visible = !V;
        gvwSTRUCKDoc2.Columns[3].Visible = V;
        uldHDoc03.Enabled = !V;
        xbnHDoc03.Enabled = !V;
        gvwSTRUCKDoc3.Columns[2].Visible = !V;
        gvwSTRUCKDoc3.Columns[3].Visible = V;
        uldHDoc04.Enabled = !V;
        xbnHDoc04.Enabled = !V;
        gvwSTRUCKDoc4.Columns[2].Visible = !V;
        gvwSTRUCKDoc4.Columns[3].Visible = V;

        //ข้อมูลพ.ร.บ.รถส่วนหัว
        rpnHStatute.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtHSCompany.ReadOnly = V;
        xDetHSDuration_Start.ReadOnly = V;
        xDetHSDuration_End.ReadOnly = V;
        xMemHSDetail.ReadOnly = V;
        uldHSDoc.Enabled = !V;
        xbnHSDoc.Enabled = !V;
        gvwSTRUCKSDoc.Columns[2].Visible = !V;
        gvwSTRUCKSDoc.Columns[3].Visible = V;

        //ข้อมูลประกันภัยรถส่วนหัว
        rpnHInsurance.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtHICompany.ReadOnly = V;
        txtHIBudget.ReadOnly = V;
        cbmHIType.ReadOnly = V;
        chkHIHolding.ReadOnly = V;
        txtHInInsure.ReadOnly = V;
        xMemHIDetail.ReadOnly = V;
        uldHIDoc.Enabled = !V;
        xbnHIDoc.Enabled = !V;
        gvwSTRUCKIDoc.Columns[2].Visible = !V;
        gvwSTRUCKIDoc.Columns[3].Visible = V;

        //ข้อมูลรถส่วนหาง
        rpnTInfomation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cmbRsHolder.ReadOnly = V;
        xDetdRFRegNo.ReadOnly = V;
        xDetdRRegNo.ReadOnly = V;
        txtRTru_Cate.ReadOnly = V;
        txtTU_Text.ReadOnly = V;
        txtTU_Volume.ReadOnly = V;
        txtRsChasis.ReadOnly = V;
        txtRsEngine.ReadOnly = V;
        cboRsBrand.ReadOnly = V;
        txtRsModel.ReadOnly = V;
        txtRnWheel.ReadOnly = V;
        txtRPowermover.ReadOnly = V;
        txtRnShaftDriven.ReadOnly = V;
        cboRsVibration.ReadOnly = V;
        rblRPumpPower.ReadOnly = V;
        cboRPumpPower_type.ReadOnly = V;
        cboRMaterialOfPressure.ReadOnly = V;
        rblRValveType.ReadOnly = V;
        if (rblRValveType.Value == "Other")
            txtRValveType.ClientEnabled = !V;
        rblRFuelType.ReadOnly = V;
        if (rblRFuelType.Value == "Other")
            txtRFuelType.ClientEnabled = !V;
        cboRGPSProvider.ReadOnly = V;
        txtRnWeight.ReadOnly = V;
        gvwRCompart.Columns[4].Visible = !V;
        tblRNewCap.Visible = !V;
        cboRTankMaterial.ReadOnly = V;
        cboRLoadMethod.ReadOnly = V;
        txtRTank_Maker.ReadOnly = V;
        cboRProdGRP.ReadOnly = V;
        uldRDoc01.Enabled = !V;
        xbnRDoc01.Enabled = !V;
        gvwRTRUCKDoc1.Columns[2].Visible = !V;
        gvwRTRUCKDoc1.Columns[3].Visible = V;
        uldRDoc02.Enabled = !V;
        xbnRDoc02.Enabled = !V;
        gvwRTRUCKDoc2.Columns[2].Visible = !V;
        gvwRTRUCKDoc2.Columns[3].Visible = V;
        uldRDoc03.Enabled = !V;
        xbnRDoc03.Enabled = !V;
        gvwRTRUCKDoc3.Columns[2].Visible = !V;
        gvwRTRUCKDoc3.Columns[3].Visible = V;
        uldRDoc04.Enabled = !V;
        xbnRDoc04.Enabled = !V;
        gvwRTRUCKDoc4.Columns[2].Visible = !V;
        gvwRTRUCKDoc4.Columns[3].Visible = V;

        //ข้อมูลพ.ร.บ.รถส่วนหาง
        rpnTStatute.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtRSCompany.ReadOnly = V;
        xDetRSDuration_Start.ReadOnly = V;
        xDetRSDuration_End.ReadOnly = V;
        xMemRSDetail.ReadOnly = V;
        uldRSDoc.Enabled = !V;
        xbnRSDoc.Enabled = !V;
        gvwRTRUCKSDoc.Columns[2].Visible = !V;
        gvwRTRUCKSDoc.Columns[3].Visible = V;

        //ข้อมูลประกันภัยรถส่วนหาง
        rpnTInsurance.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        txtRICompany.ReadOnly = V;
        txtRIBudget.ReadOnly = V;
        cbmRIType.ReadOnly = V;
        chkRIHolding.ReadOnly = V;
        txtRInInsure.ReadOnly = V;
        xMemRIDetail.ReadOnly = V;
        uldRIDoc.Enabled = !V;
        xbnRIDoc.Enabled = !V;
        gvwRTRUCKIDoc.Columns[2].Visible = !V;
        gvwRTRUCKIDoc.Columns[3].Visible = V;

        //ข้อมูลวัดน้ำ
        rpnWater.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(wColor);
        rblPlaceMWater.ReadOnly = W;
        if (rblPlaceMWater.Value == "INTERNAL")
        {
            txtIsCar_Num.Enabled = !W;
            xDetIDPREV_SERV.Enabled = !W;
            xDetIDNEXT_SERV.Enabled = !W;
            txtEsCar_Num.Enabled = false;
            xDetEDPREV_SERV.Enabled = false;
            xDetEDNEXT_SERV.Enabled = false;
            uldMWaterDoc.Enabled = false;
            xbnMWaterDoc.Enabled = false;
            gvwSTRUCKMWaterDoc.Columns[2].Visible = !W;
            gvwSTRUCKMWaterDoc.Columns[3].Visible = W;
        }
        else if (rblPlaceMWater.Value == "EXTERNAL")
        {
            txtIsCar_Num.ClientEnabled = false;
            xDetIDPREV_SERV.ClientEnabled = false;
            xDetIDNEXT_SERV.ClientEnabled = false;
            txtEsCar_Num.ClientEnabled = !W;
            xDetEDPREV_SERV.ClientEnabled = !W;
            xDetEDNEXT_SERV.ClientEnabled = !W;
            uldMWaterDoc.Enabled = !W;
            xbnMWaterDoc.ClientEnabled = !W;
            gvwSTRUCKMWaterDoc.Columns[2].Visible = !W;
            gvwSTRUCKMWaterDoc.Columns[3].Visible = W;
        }
        else
        {
            txtIsCar_Num.ClientEnabled = !W;
            xDetIDPREV_SERV.ClientEnabled = !W;
            xDetIDNEXT_SERV.ClientEnabled = !W;
            txtEsCar_Num.ClientEnabled = !W;
            xDetEDPREV_SERV.ClientEnabled = !W;
            xDetEDNEXT_SERV.ClientEnabled = !W;
            uldMWaterDoc.Enabled = !W;
            xbnMWaterDoc.ClientEnabled = !W;
            gvwSTRUCKMWaterDoc.Columns[2].Visible = !W;
            gvwSTRUCKMWaterDoc.Columns[3].Visible = W;
        }

        //ข้อมูลการใช้งานและถือครอง
        rpnHolding.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);

        //ข้อมูลการอนุญาตรับงาน
        rpnPermit.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        rblPermit.ReadOnly = V;
        if (rblPermit.Value == "N")
        {
            xDetdBlackListExp.ClientEnabled = !V;
            xMemBlackListCause.ClientEnabled = !V;
        }
        else
        {
            xDetdBlackListExp.ClientEnabled = false;
            xMemBlackListCause.ClientEnabled = false;
        }


        xbnSubmit.Visible = !W;
        xbnCancel.Visible = !W;

        rpnWater.Focus();
    }
    private void ClearSession()
    {
        string[] sSession = new string[] { "SCARTYPEID", "SHCompart", "SRCompart", "HDoc", "RDoc", "HISDoc", "RISDoc", "MWaterDoc" };
        foreach (string s in sSession)
            Session.Remove(s);
    }
    protected DataTable PrepareDataTable(string ss_name, string IsDel, string struckid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] != null)
        {
            dtData = (DataTable)Session[ss_name];
            if (IsDel == "0")
            {
                foreach (DataRow drDel in dtData.Select("CNEW='1' AND CACTIVE='0'"))
                {
                    int idx = dtData.Rows.IndexOf(drDel);
                    dtData.Rows[idx].Delete();
                }
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }
    private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
        FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
        string ResultFileName = ServerMapPath + File.Name;
        string sPath = Path.GetDirectoryName(ResultFileName)
                , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                , sFileType = Path.GetExtension(ResultFileName);

        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(ServerMapPath))
            {
                Directory.CreateDirectory(ServerMapPath);
            }
            #endregion
            string fileName = (GenFileName + "" + sFileType.Trim());
            ful.SaveAs(ServerMapPath + fileName);

            return fileName + "$" + sFileName.Replace("$", "") + sFileType;
        }
        else
            return "$";
    }
    private bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    #region manage file on server
    //this event occur when press submit
    private void ServerMoveFile(string sourceFile, string destFile, string fileName)
    {
        if (!Directory.Exists(Server.MapPath(destFile)))//ก่อนจะย้าย ดูก่อนว่ามี directory ป่าว 
        {
            Directory.CreateDirectory(Server.MapPath(destFile));//สร้าง directory ขึนมาสำหรับจะย้ายไฟล์เข้า
        }
        if (isFileExist(sourceFile + fileName))//ก่อนจะย้าย เชคก่อนว่า มีไฟล์อยู่หรือป่าว?
        {
            Directory.Move(Server.MapPath(sourceFile) + fileName, Server.MapPath(destFile) + fileName);
        }
    }
    private void ServerClearFile(string pathFile)
    {
        if (Directory.Exists(Server.MapPath(pathFile)))
        {
            Directory.Delete(Server.MapPath(pathFile), true);
        }
    }
    private bool isFileExist(string checkPath)
    {
        if (File.Exists(Server.MapPath(checkPath)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void DeleteFileNotUse(string pathFile)
    {
        if (isFileExist(pathFile))
        {
            File.Delete(Server.MapPath(pathFile));
        }
    }
    #endregion
}