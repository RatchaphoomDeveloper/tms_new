﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using Utility;
using System.Data.OracleClient;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using TMS_Entity;

public partial class admin_VisitForm_lst : PageBase
{
    string sqlConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string PagePermission = "admin_VisitForm_lst.aspx";
    string GroupPermission = "01";

    #region " Prop "
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }

    protected SearhTTYPEOFVISITFORM SearhTTYPEOFVISITFORM
    {
        get { return (SearhTTYPEOFVISITFORM)ViewState[this.ToString() + "SearhTTYPEOFVISITFORM"]; }
        set { ViewState[this.ToString() + "SearhTTYPEOFVISITFORM"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    #endregion " Prop "


    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled=false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnSaveCopy.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public void InitForm()
    {
        txtDateStart.Text = DateTime.Now.Date.AddDays(-1).ToString("dd/MM/yyyy");
        txtDateEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_VisitForm_add.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        txtSearchFormName.Text = string.Empty;
        rdoStatus.SelectedValue = "1";
        LoadMain();
    }

    public void LoadMain()
    {
        string _err = string.Empty;
        SearhTTYPEOFVISITFORM _s = new TMS_BLL.Master.SearhTTYPEOFVISITFORM();

        bool IsSelStart = (!string.IsNullOrEmpty(txtDateStart.Text.Trim()));
        bool IsSelEnd = (!string.IsNullOrEmpty(txtDateEnd.Text.Trim()));

        if (IsSelStart)
        {
            _s.SDUPDATE_START = txtDateStart.Text.Trim();
            DateTime? dStart = DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim());
            if (dStart > DateTime.Now.AddDays(1))
            {
                alertFail("วันที่เริ่มต้นมากกว่าวันที่ปัจจุบัน");
                return;
            }
        }

        if (IsSelStart != IsSelEnd)
        {
            alertFail("เลือกวันที่เริ่มต้น/วันที่สิ้นสุด ไม่ถูกต้อง");
            return;
        }

        if (IsSelEnd)
        {
            _s.SDUPDATE_END = txtDateEnd.Text.Trim();
            DateTime? dEnd = DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim());
            if (DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim()) > dEnd)
            {
                alertFail("วันที่เริ่มต้นมากกว่าวันที่สิ้นสุด");
                return;
            }
        }

        if (!string.IsNullOrEmpty(txtSearchFormName.Text.Trim()))
        {
            _s.STYPEVISITFORMNAME = txtSearchFormName.Text.Trim();
        }

        _s.CACTIVE = rdoStatus.SelectedItem.Value;

        SearhData = new QuestionnaireBLL().GetTTYPEOFVISITFORM(ref _err, _s);

        grvMain.DataSource = SearhData;
        grvMain.DataBind();
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            GotoDefault();
            return;
        }

        //string _err = string.Empty;
        //Page_Status = new QuestionnaireBLL().CheckPermission(ref _err, Session["UserID"].ToString(), PagePermission);
        //if (Page_Status == USER_PERMISSION.DISABLE)
        //{
        //    GotoDefault();
        //    return;
        //}
        //else if (Page_Status == USER_PERMISSION.VIEW)
        //{
        //    btnAdd.Enabled = false;
        //    btnAdd.Visible = false;
        //}
    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "RowSelect")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            int _keyNTYPEVISITFORMID = int.Parse(grvMain.DataKeys[rowIndex.Value]["NTYPEVISITFORMID"].ToString());
            if (_keyNTYPEVISITFORMID != null)
            {
                Session["EditNTYPEVISITFORMID"] = _keyNTYPEVISITFORMID;
                Response.Redirect("admin_VisitForm_add.aspx");
            }
        }
        else if (e.CommandName == "View")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            int _keyNTYPEVISITFORMID = int.Parse(grvMain.DataKeys[rowIndex.Value]["NTYPEVISITFORMID"].ToString());
            if (_keyNTYPEVISITFORMID != null)
            {
                Session["ViewNTYPEVISITFORMID"] = _keyNTYPEVISITFORMID;
                Response.Redirect("admin_VisitForm_add.aspx");
            }
        }
    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chk = e.Row.Cells[1].FindControl("chkActive") as CheckBox;
            if (chk != null)
            {
                string CACTIVE = DataBinder.Eval(e.Row.DataItem, "CACTIVE").ToString();
                chk.Checked = CACTIVE == "1" ? true : false;
            }
        }
    }
    protected void btnSetRadio_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "btnSetRadioScr", "<script>window.location='admin_VisitForm_SetOption.aspx';<script>");
    }
    protected void btnSetYear_Click(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "btnSetYearScr", "<script>window.location='admin_VisitForm_config.aspx';<script>");
    }
    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {

    }
    public bool CopyForm(int RefFormID)
    {
        bool result = true;
        try
        {
            string _err = string.Empty;
            int NewFormID = new QuestionnaireBLL().GetTTYPEOFVISITFORM_RefID(ref _err);
            Session["EditNTYPEVISITFORMID"] = NewFormID;
            using (OracleConnection con = new OracleConnection(sqlConn))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                using (OracleTransaction tran = con.BeginTransaction())
                {
                    try
                    {
                        string strsql = @" INSERT
                        INTO TTYPEOFVISITFORM
                          (
                            NTYPEVISITFORMID, STYPEVISITFORMNAME,
                            DUSE, DCREATE, SCREATE,  DUPDATE, SUPDATE, CACTIVE, SHOWINLIST
                          ) SELECT  :REF_NTYPEVISITFORMID,
                            STYPEVISITFORMNAME || ' COPY', DUSE,  sysdate, :SCREATE,
                            sysdate, :SCREATE, '1', '1' FROM TTYPEOFVISITFORM WHERE NTYPEVISITFORMID=:NTYPEVISITFORMID ";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.Transaction = tran;
                            com.Parameters.Clear();
                            com.Parameters.Add(":REF_NTYPEVISITFORMID", OracleType.VarChar).Value = NewFormID.ToString();
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = RefFormID.ToString();
                            com.ExecuteNonQuery();
                        }

                        strsql = @" INSERT
                        INTO TTYPEOFVISITFORMLIST
                          (
                            NTYPEVISITLISTID,  NTYPEVISITFORMID, STYPEVISITLISTNAME,
                            NTYPEVISITLISTSCORE, CONFIG_VALUE, CACTIVE
                          ) Select NTYPEVISITLISTID, :REF_NTYPEVISITFORMID, STYPEVISITLISTNAME,
                            NTYPEVISITLISTSCORE, CONFIG_VALUE,
                            CACTIVE from TTYPEOFVISITFORMLIST where NTYPEVISITFORMID=:NTYPEVISITFORMID";

                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.Transaction = tran;
                            com.Parameters.Clear();
                            com.Parameters.Add(":REF_NTYPEVISITFORMID", OracleType.VarChar).Value = NewFormID.ToString();
                            com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = RefFormID.ToString();
                            com.ExecuteNonQuery();
                        }

                        DataTable DTGRoup = new QuestionnaireBLL().GetTGROUPOFVISITFORM(ref _err, RefFormID);
                        foreach (DataRow r in DTGRoup.Rows)
                        {
                            int GroupID = int.Parse(r["NGROUPID"].ToString());
                            int? NewGroupID = new QuestionnaireBLL().GetGroupRefID(ref _err);
                            strsql = @"INSERT INTO TGROUPOFVISITFORM
                              (
                                NGROUPID, NTYPEVISITFORMID, SGROUPNAME, CACTIVE,
                                SCREATE, DCREATE, SUPDATE, DUPDATE,
                                NNO, GROUP_INDEX, SHORT_DESC
                              ) select  :NEW_NGROUPID, :REF_NTYPEVISITFORMID, SGROUPNAME, CACTIVE,
                                :SCREATE,sysdate, :SCREATE,  sysdate,
                                NNO, GROUP_INDEX, SHORT_DESC from TGROUPOFVISITFORM 
                              where NTYPEVISITFORMID=:NTYPEVISITFORMID and NGROUPID=:NGROUPID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":NEW_NGROUPID", OracleType.VarChar).Value = NewGroupID.ToString();
                                com.Parameters.Add(":REF_NTYPEVISITFORMID", OracleType.VarChar).Value = NewFormID.ToString();
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = RefFormID.ToString();
                                com.Parameters.Add(":NGROUPID", OracleType.VarChar).Value = GroupID.ToString();
                                com.ExecuteNonQuery();
                            }
                            strsql = @"INSERT INTO TVISITFORM
                              (NTYPEVISITFORMID, NGROUPID, 
                                SVISITFORMNAME, CACTIVE, SCREATE, DCREATE, SUPDATE,  DUPDATE, NWEIGHT, ITEM_INDEX, FORM_CODE
                              ) select  :REF_NTYPEVISITFORMID, :NEW_NGROUPID,
                                SVISITFORMNAME, CACTIVE, :SCREATE, sysdate, :SCREATE, sysdate, NWEIGHT, ITEM_INDEX, FORM_CODE from  TVISITFORM where NTYPEVISITFORMID=:NTYPEVISITFORMID and NGROUPID=:NGROUPID ";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":REF_NTYPEVISITFORMID", OracleType.VarChar).Value = NewFormID.ToString();
                                com.Parameters.Add(":NEW_NGROUPID", OracleType.VarChar).Value = NewGroupID.ToString();
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = RefFormID.ToString();
                                com.Parameters.Add(":NGROUPID", OracleType.VarChar).Value = GroupID.ToString();
                                com.ExecuteNonQuery();
                            }

                        }
                        tran.Commit();
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        alertFail(ex.Message);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            result = false;
            alertFail(ex.Message);
        }
        return result;

    }
    protected void btnSaveCopy_Click(object sender, EventArgs e)
    {
        int ID = int.Parse(hdnNum.Value);
        bool result = CopyForm(ID);
        if (result)
        {
            alertSuccess("บันทึกข้อมูลเรียบร้อย", "admin_VisitForm_add.aspx");
        }

    }
    protected void btnNotSave_Click(object sender, EventArgs e)
    {

    }
}