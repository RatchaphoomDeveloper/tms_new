﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentTab3_Vendor.aspx.cs" Inherits="AccidentTab3_Vendor" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">

    <div class="form-horizontal">
        <ul class="nav nav-tabs" id="tabtest">
            <li class="" id="liTab1" runat="server"><a href="#TabGeneral" runat="server" id="GeneralTab1">แจ้งเรื่องอุบัติเหตุ</a></li>
            <li class="" id="liTab2" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">รายงานเบื้องต้น</a></li>
            <li class="active" id="liTab3" runat="server"><a href="#TabGeneral3" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab3">รายงานวิเคราะห์สาเหตุ</a></li>
            <li class="" id="liTab4" visible="false" runat="server"><a href="#TabGeneral4" runat="server" id="GeneralTab4">รายงานผลการพิจารณา</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="TabGeneral">
                <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse8" id="acollapse8">ความเสียหาย(ประเมินโดยคณะกรรมการสอบสวนอุบัติเหตุร้ายแรง)</a>
                            <asp:HiddenField runat="server" ID="hidcollapse8" />
                        </div>
                        <div id="collapse8" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row form-group ">
                                    <div class="col-md-12">
                                        <h5>
                                            <asp:Label Text="" ID="lblType" runat="server" /></h5>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลการเกิดอุบัติเหตุ</a>
                        <asp:HiddenField runat="server" ID="hidcollapse1" />
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row form-group">
                                <label class="col-md-3 control-label">Accident ID</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtAccidentID" CssClass="form-control" ReadOnly="true" Text="Generate by System" />
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">วันที่-เวลาเกิดเหตุ</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtAccidentDate" CssClass="form-control datetimepicker" ReadOnly="true" />
                                </div>
                                <label class="col-md-2 control-label">วันที่-เวลาที่แจ้งเรื่องในระบบ</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtAccidentDateSystem" CssClass="form-control datetimepicker" ReadOnly="true" />
                                </div>

                            </div>
                            <div class="row form-group hidden" id="divREPORTER">
                                <label class="col-md-3 control-label">ผขส. แจ้งเหตุให้ ปตท.</label>
                                <div class="col-md-3">
                                    <div class="col-md-4 PaddingLeftRight0">
                                        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblREPORTER" Enabled="false">
                                            <asp:ListItem Text="No" Value="0" />
                                            <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-md-8 PaddingLeftRight0">
                                        <asp:TextBox runat="server" ID="txtAccidentDatePtt" CssClass="form-control datetimepicker" ReadOnly="true" />
                                    </div>

                                </div>
                                <label class="col-md-2 control-label">ชื่อผู้แจ้ง(ผู้ขนส่ง)</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtAccidentName" CssClass="form-control" ReadOnly="true" />
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">วันที่-เวลาจัดทำอุบัติเหตุเบื้องต้น</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtDatePrimary" CssClass="form-control datetimepicker" ReadOnly="true" />
                                </div>
                                <label class="col-md-2 control-label">ผขส. ส่งรายงานวิเคราะห์สาเหตุ</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtDateAnalysis" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row form-group">
                                    <label class="col-md-3 control-label">วันที่-เวลา เจ้าหน้าที่ ปง.อนุมัติเอกสาร</label>
                                    <div class="col-md-3">
                                        <asp:TextBox runat="server" CssClass="form-control datetimepicker" ID="txtApproveDate" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">ประมาณการความเสียหาย</a>
                        <asp:HiddenField runat="server" ID="hidcollapse4" />
                    </div>
                    <div id="collapse4" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row form-group">
                                <label class="col col-md-5 control-label">ประมาณการความเสียหาย (บาท)</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" CssClass="form-control number" ID="txtDAMAGE" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">เอกสารที่ต้องแนบประกอบการพิจารณา&nbsp;(บังคับ)</a>
                        <asp:HiddenField runat="server" ID="hidcollapse2" />
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" CssClass="table table-striped table-bordered" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowDataBound="dgvRequestFile_RowDataBound" >
                                        <Columns>

                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Template">
                                                <ItemTemplate>
                                                    <center>
                                                        <asp:ImageButton ImageUrl="~/Images/blue-document-download-icon.png" OnClick="btnDownload_Click" runat="server" Width="24px" ID="btnDownload" Visible="false"  />
                                                    </center>
                                                    
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3">แนบเอกสารหลักฐาน</a>
                        <asp:HiddenField runat="server" ID="hidcollapse3" />
                    </div>
                    <div id="collapse3" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row form-group">
                                <label class="col col-md-5 control-label">ประเภทไฟล์เอกสาร</label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col col-md-5 control-label">เลือกไฟล์</label>
                                <div class="col-md-3">
                                    <asp:FileUpload ID="fileUpload" runat="server" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                        HorizontalAlign="Center" AutoGenerateColumns="false"
                                        CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                        OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound" EmptyDataText="[ ไม่มีข้อมูล ]">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                                            <asp:BoundField DataField="STATUSNAME" HeaderText="STATUS" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="STATUS" HeaderText="STATUS" Visible="false" />
                                            <asp:BoundField DataField="REMARK" HeaderText="หมายเหตุ" />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                        Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <input id="btnApprove" type="button" value="ส่งพิจารณา" data-toggle="modal" data-target="#ModalConfirmBeforeApprove" class="btn btn-md bth-hover btn-info" />
                                    <input id="btnSave" type="button" value="บันทึกร่าง" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveSend_ClickOK" TextTitle="ยืนยันการส่งพิจารณา" TextDetail="คุณต้องการส่งพิจารณาใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveSend" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันบันทึกร่าง" TextDetail='คุณต้องการบันทึกร่างใช่หรือไม่ ?<br/>กรณีต้องการส่งเอกสารให้กับเจ้าหน้าที่ ปตท. พิจารณา กรุณากดปุ่ม <font color="red">"ส่งพิจารณา"</font> อีกครั้ง' />
    <asp:HiddenField ID="hidID" runat="server" />
    <asp:HiddenField ID="hidCactive" runat="server" />
    <asp:HiddenField ID="hidCGROUP" runat="server" />
    <script type="text/javascript">
        function SetEnabledControlByID(id) {
            if ($('#<%=hidCGROUP.ClientID%>').val() == '0') {
                $('#divREPORTER').addClass("hide");
            }
            if (id == '6') {
                $('#btnApprove').prop('disabled', true);
                $('#btnSave').prop('disabled', true);
            }
            else if (id == '4' || id == '7') {
                $('#btnApprove').prop('disabled', false);
                $('#btnSave').prop('disabled', false);
            }
            else {
                $('#btnSave,#btnApprove').prop('disabled', true);
            }
            var last = $('#<%= hidcollapse1.ClientID %>').val();
             if (last.indexOf("in") > -1) {
                 $("#collapse1").removeClass('in');
             }
             last = $('#<%= hidcollapse2.ClientID %>').val();
             if (last.indexOf("in") > -1) {
                 $("#collapse2").removeClass('in');
             }
             last = $('#<%= hidcollapse3.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse3").removeClass('in');
            }
            last = $('#<%= hidcollapse4.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse4").removeClass('in');
            }

        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        $(document).ready(function () {

            fcollapse();
        });


        function EndRequestHandler(sender, args) {
            fcollapse();
        }
        function fcollapse() {
            $("#acollapse1").on('click', function () {
                var active = $("#collapse1").attr('class');
                //console.log(active);
                $('#<%= hidcollapse1.ClientID %>').val(active);
            });
            $("#acollapse2").on('click', function () {
                var active = $("#collapse2").attr('class');
                $('#<%= hidcollapse2.ClientID %>').val(active);
            });
            $("#acollapse3").on('click', function () {
                var active = $("#collapse3").attr('class');
                $('#<%= hidcollapse3.ClientID %>').val(active);
            });
            $("#acollapse4").on('click', function () {
                var active = $("#collapse4").attr('class');
                $('#<%= hidcollapse4.ClientID %>').val(active);
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

