﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    StylesheetTheme="Aqua" CodeFile="ReportSolvePurchase.aspx.cs" Inherits="ReportSolvePurchase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td style="width: 6%; white-space: nowrap;">
                            ช่วงเวลาที่ต้องการค้นหา :
                        </td>
                        <td style="width: 4%; white-space: nowrap;">
                            &nbsp;วันที่เริ่มต้น&nbsp;
                        </td>
                        <td style="width: 5%;">
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" CausesValidation="true" ValidationGroup="search">
                            <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                            </ValidationSettings>
                            </dx:ASPxDateEdit> 
                        </td>
                        <td style="width: 4%; white-space: nowrap;">
                            &nbsp;วันที่สิ้นสุด&nbsp;
                        </td>
                        <td style="width: 5%;">
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" CausesValidation="true" ValidationGroup="search">
                            <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                            </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td style="width: 36%;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Product Gorup :
                        </td>
                        <td colspan="2">
                            <dx:ASPxComboBox ID="cboProdGrp" ClientInstanceName="cboProdGrp" runat="server" DataSourceID="sdsProdGrp"
                                TextField="SPRODUCTTYPENAME" ValueField="SPRODUCTTYPEID">
                                <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" CausesValidation="true" ValidationGroup="search">
                                <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsProdGrp" runat="server" SelectCommand="SELECT CPRODUCT_GROUP
        ,RTRIM (
          XMLAGG (XMLELEMENT (e, SPRODUCTTYPENAME || ',')).EXTRACT ('//text()'),
          ',')
          SPRODUCTTYPENAME
          ,CPRODUCT_GROUP||':'||RTRIM (
          XMLAGG (XMLELEMENT (e, SPRODUCTTYPEID || ',')).EXTRACT ('//text()'),
          ',')
          SPRODUCTTYPEID
  FROM TPRODUCTTYPE
  GROUP BY CPRODUCT_GROUP" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td colspan="3">
                            <div style="float: left">
                                <dx:ASPxButton ID="btnSearch" runat="server" OnClick="btnSubmit_Click" SkinID="_search" ValidationGroup="search"> 

                                </dx:ASPxButton>
                            </div>
                            <div style="float: left">
                                &nbsp;</div>
                            <div style="float: left">
                                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                    <ClientSideEvents Click="function(s,e){ window.location = 'Report.aspx'}" />
                                </dx:ASPxButton>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                        <dx:ReportToolbar ID="rpttlb" ClientInstanceName="rpttlb" runat="server" ReportViewerID="rptvw"  ShowDefaultButtons="False" Width="80%">
                                <Items>
                                    <dx:ReportToolbarButton ItemKind="Search" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="PrintReport" />
                                    <dx:ReportToolbarButton ItemKind="PrintPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                    <dx:ReportToolbarLabel ItemKind="PageLabel" />
                                    <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                    </dx:ReportToolbarComboBox>
                                    <dx:ReportToolbarLabel ItemKind="OfLabel" />
                                    <dx:ReportToolbarTextBox ItemKind="PageCount" />
                                    <dx:ReportToolbarButton ItemKind="NextPage" />
                                    <dx:ReportToolbarButton ItemKind="LastPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                                    <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                                    <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                        <Elements>
                                            <dx:ListElement Value="pdf" />
                                            <dx:ListElement Value="xls" />
                                            <dx:ListElement Value="xlsx" />
                                            <dx:ListElement Value="rtf" />
                                            <dx:ListElement Value="mht" />
                                            <dx:ListElement Value="html" />
                                            <dx:ListElement Value="txt" />
                                            <dx:ListElement Value="csv" />
                                            <dx:ListElement Value="png" />
                                        </Elements>
                                    </dx:ReportToolbarComboBox>
                                </Items>
                                <Styles>
                                    <LabelStyle>
                                        <Margins MarginLeft="3px" MarginRight="3px" />
                                    </LabelStyle>
                                </Styles></dx:ReportToolbar>
                        <dx:ReportViewer ID="rptvw" ClientInstanceName="rptvw" runat="server"></dx:ReportViewer>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
