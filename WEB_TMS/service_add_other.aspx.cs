﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.Globalization;

public partial class service_add_other : PageBase
{
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string CarTypeID = "";
    private static string VEH_ID = "";
    private static string TU_ID = "";
    private static string VEH_No = "";
    private static string Tu_No = "";
    private static string VEH_CHASIS = "";
    private static string TU_CHASIS = "";
    private static string SVENDORID = "";
    private static string TOTLE_SLOT = "";
    private static string TOTLE_CAP = "";
    private static string TOTLE_SERVCHAGE = "";
    private static string CARCATE_ID = "";
    private static string WaterExpire = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
                this.AssignAuthen();
            }

        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                // gvwWaiting.Columns[7].Visible = false;
            }

            if (!CanWrite)
            {
                btnSubmit.Enabled = false;               
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        string SUSERID = Session["UserID"] + "";
        string ReqType_ID = cboRequestType.Value + "";
        string Cause_ID = cboCause.Value + "";

        if (paras.Length > 0)
        {
            switch (paras[0])
            {
                case "ChangeReq":
                    SetCboCause(ReqType_ID);
                    break;
                case "List":
                    SetCboCause(ReqType_ID);
                    break;
                case "SetDetail":
                    SetCboCause(ReqType_ID);
                    string Query = "SELECT * FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(cboCarregis.Value + "") + "'";

                    DataTable dt = CommonFunction.Get_Data(strConn, Query);

                    if (dt.Rows.Count > 0)
                    {
                        //เซ็ตข้อมูลรถ
                        SetDetailCar(dt.Rows[0]["SCARTYPEID"] + "", dt.Rows[0]["STRUCKID"] + "", dt.Rows[0]["STRAILERID"] + "", dt.Rows[0]["STRANSPORTID"] + "");
                    }
                    break;
                case "save":
                    SetCboCause(ReqType_ID);
                    string ReqID = SystemFunction.genID_TBL_REQUEST(ReqType_ID);

                    string CodiTionDateWaterExpire = "";
                    string sTempDate = "";
                    if (!string.IsNullOrEmpty(WaterExpire))
                    {
                        string[] ArrDate = WaterExpire.Split('/');
                        sTempDate = (int.Parse(ArrDate[2]) - 543) + "/" + ArrDate[1] + "/" + ArrDate[0];// + " 00:00:00";
                        CodiTionDateWaterExpire = "TO_DATE('" + sTempDate + "', 'YYYY-MM-DD')";
                    }
                    else
                    {
                        CodiTionDateWaterExpire = "null";
                    }

                    //sql TBL_REQUEST
                    string sqlInsert_REQUEST = @"INSERT INTO TBL_REQUEST(REQUEST_ID,REQTYPE_ID,CAUSE_ID,CARCATE_ID,REQUEST_DATE,VEH_NO,TU_NO,VEH_CHASSIS,TU_CHASSIS,VENDOR_ID,TOTLE_CAP,TOTLE_SLOT,TOTLE_SERVCHAGE,STATUS_FLAG,CREATE_CODE,CREATE_DATE,STRUCKID,VEH_ID,TU_ID,REQUEST_OTHER_FLAG,RK_FLAG,REMARK_CAUSE,WATER_EXPIRE_DATE)
                                                                  VALUES('" + CommonFunction.ReplaceInjection(ReqID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(ReqType_ID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(Cause_ID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(CARCATE_ID) + @"',
                                                                        SYSDATE,
                                                                        '" + CommonFunction.ReplaceInjection(VEH_No) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(Tu_No) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(VEH_CHASIS) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(TU_CHASIS) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(SVENDORID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(TOTLE_CAP) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(TOTLE_SLOT) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(TOTLE_SERVCHAGE) + @"',
                                                                        '09',
                                                                        '" + CommonFunction.ReplaceInjection(SUSERID) + @"',
                                                                        SYSDATE,
                                                                        '" + CommonFunction.ReplaceInjection(VEH_ID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(VEH_ID) + @"',
                                                                        '" + CommonFunction.ReplaceInjection(TU_ID) + @"',
                                                                        'O',
                                                                        'Y',
                                                                        '" + CommonFunction.ReplaceInjection(txtRemarkCause.Text) + "'," + CodiTionDateWaterExpire + ")";
                    //sql TBL_REQUEST_ITEM
                    string sqlInsert_RQ_ITEM = @"INSERT INTO TBL_REQUEST_ITEM(REQUEST_ID,SERVICE_ID,NITEM,NPRICE)
                                                               VALUES('" + CommonFunction.ReplaceInjection(ReqID) + "','00023',1,0)";

                    SystemFunction.SQLExecuteNonQuery(strConn, sqlInsert_REQUEST);
                    SaveCapacityDetail(ReqID, CarTypeID == "0" ? VEH_ID : TU_ID);
                    SystemFunction.SQLExecuteNonQuery(strConn, sqlInsert_RQ_ITEM);
                    SystemFunction.Add_To_TBL_REQREMARK(ReqID, "N", SystemFunction.GetRemark_WorkFlowRequest(1), "09", "S", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(9), "", "Y");


                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='download_invoice_request_rk.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(ReqID)) + "';});");
                    ////ส่งเมล์
                    //if (SendMailToUser(ReqID))
                    //{
                    //    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='vendor_HomeAlert.aspx';});");
                    //}
                    //else // Error send mail
                    //{
                    //    //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้');");
                    //    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='vendor_HomeAlert.aspx';});");
                    //}

                    break;
                case "back": xcpn.JSProperties["cpRedirectTo"] = "approve.aspx";
                    break;
            }
        }
    }

    private void SetCboCause(string Reqtype_ID)
    {
        if (Reqtype_ID == "01")
        {
            string case01 = @"SELECT 
   CAUSE_ID, REQTYPE_ID, CAUSE_HEADID, 
   CAUSE_LEVEL, CAUSE_NAME, CAUSE_PRICE, 
   CAUSE_UNIT, CAUSE_ISALL, ISACTIVE_FLAG
FROM TBL_CAUSE WHERE CAUSE_ID = '00011' OR CAUSE_ID = '00010' ORDER BY CAUSE_ID DESC";

            DataTable dtcmb = CommonFunction.Get_Data(strConn, case01);
            if (dtcmb.Rows.Count > 0)
            {

                cboCause.DataSource = dtcmb;
                cboCause.TextField = "CAUSE_NAME";
                cboCause.ValueField = "CAUSE_ID";
                cboCause.DataBind();


            }
        }
        else
        {
            string case02 = @"SELECT 
   CAUSE_ID, REQTYPE_ID, CAUSE_HEADID, 
   CAUSE_LEVEL, CAUSE_NAME, CAUSE_PRICE, 
   CAUSE_UNIT, CAUSE_ISALL, ISACTIVE_FLAG
FROM TBL_CAUSE WHERE CAUSE_ID = '00011' OR CAUSE_ID = '00010' ORDER BY CAUSE_ID DESC";

            DataTable dtcmb = CommonFunction.Get_Data(strConn, case02);
            if (dtcmb.Rows.Count > 0)
            {

                cboCause.DataSource = dtcmb;
                cboCause.TextField = "CAUSE_NAME";
                cboCause.ValueField = "CAUSE_ID";
                cboCause.DataBind();


            }
        }
    }

    //เซ็ตรถ
    private void SetDetailCar(string sCarTypeID, string STRUCKID, string STRUCKIDTAIL, string VENDOR_ID)
    {
        string _qry = "";
        //เว็ตประเภทรถเพื่อนำไปใช้ในการแอดค่าความจุรถ
        CarTypeID = sCarTypeID;
        switch (sCarTypeID)
        {
            case "0"://รถบรรทุก ไม่มีหาง
                _qry = @"
                SELECT VEH_ID,VEH_CHASIS, VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO,MAX(SLOT1) as SLOT1,SUM(CAP1) as CAP1, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID FROM
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID,T.SHEADREGISTERNO  as REGISNO
                    ,TC.NCOMPARTNO as SLOT1,MAX(TC.NCAPACITY) as CAP1,NULL as SLOT2,NULL as CAP2,NULL as TU_ID,NULL as TU_CHASIS,NULL as TU_No,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO ,V.SVENDORID
                )
                GROUP BY VEH_ID,VEH_CHASIS,VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID";
                break;
            case "3":
                _qry = @"
                SELECT * FROM 
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,T.STRAILERREGISTERNO as TU_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID
                ,T.SHEADREGISTERNO||'/'||T.STRAILERREGISTERNO  as REGISNO,COUNT(TC.NCOMPARTNO) as SLOT1 ,SUM(TC.NCAPACITY) as CAP1
                FROM  TTRUCK T 
                LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID
                WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,V.SVENDORID
                )T1,
                (
                    SELECT TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,COUNT(SLOT2) as SLOT2,SUM(CAP2) as CAP2,DWATEREXPIRE
                    FROM
                    (
                        SELECT T.STRUCKID as TU_ID,T.SCHASIS as TU_CHASIS,CT.CARCATE_NAME,CT.CARCATE_ID,TC.NCOMPARTNO as SLOT2,MAX(TC.NCAPACITY) as CAP2,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKIDTAIL) + @"%'

                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO
                )
                GROUP BY TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,DWATEREXPIRE
                ) T2";
                break;
        }



        DataTable _dtVendor = new DataTable();
        _dtVendor = CommonFunction.Get_Data(strConn, _qry);
        if (_dtVendor.Rows.Count > 0)
        {
            lblWaterExpire.Text = !string.IsNullOrEmpty("" + _dtVendor.Rows[0]["DWATEREXPIRE"]) ? DateTime.Parse(_dtVendor.Rows[0]["DWATEREXPIRE"] + "").ToString(ConfigurationManager.AppSettings["FormatDate"]) : " - ";
            WaterExpire = !string.IsNullOrEmpty("" + _dtVendor.Rows[0]["DWATEREXPIRE"]) ? DateTime.Parse(_dtVendor.Rows[0]["DWATEREXPIRE"] + "").ToString(ConfigurationManager.AppSettings["FormatDate"]) : "";
            lblTruckType.Text = _dtVendor.Rows[0]["CARCATE_NAME"].ToString();


            VEH_ID = _dtVendor.Rows[0]["VEH_ID"].ToString();
            TU_ID = _dtVendor.Rows[0]["TU_ID"].ToString();
            VEH_No = _dtVendor.Rows[0]["VEH_No"].ToString();
            Tu_No = _dtVendor.Rows[0]["Tu_No"].ToString();
            VEH_CHASIS = _dtVendor.Rows[0]["VEH_CHASIS"].ToString();//update VEH_CHASIS
            TU_CHASIS = _dtVendor.Rows[0]["TU_CHASIS"].ToString();//update TU_CHASIS
            SVENDORID = _dtVendor.Rows[0]["SVENDORID"].ToString();//update VENDORID
            TOTLE_SLOT = (sCarTypeID == "0" ? _dtVendor.Rows[0]["SLOT1"] + "" : _dtVendor.Rows[0]["SLOT2"] + "");//update SLOT   
            TOTLE_CAP = (sCarTypeID == "0" ? _dtVendor.Rows[0]["CAP1"] + "" : _dtVendor.Rows[0]["CAP2"] + "");//update CAP

            CARCATE_ID = _dtVendor.Rows[0]["CARCATE_ID"].ToString();


            switch (cboRequestType.Value + "")
            {
                case "01":
                    //เคลียลาเบลเพื่อไม่ให้ค่าของคันอื่นมาทับ
                    lblTotal.Text = "0";
                    TOTLE_SERVCHAGE = "0";
                    break;
                case "02":
                    lblTotal.Text = "0";
                    TOTLE_SERVCHAGE = "0";
                    break;
            }

        }
    }

    public void SaveCapacityDetail(string _reqID, string STRUCKID)
    {

        string _selectPan = @"SELECT 
 STRUCKID, NCOMPARTNO, NPANLEVEL
 ,NCAPACITY
FROM TTRUCK_COMPART
WHERE
NPANLEVEL = {0}
 AND STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";

        // statusPan 0 = แป้นเก่า 1 = เพิ่มแป้นใหม่ 
        DataTable dtPan1 = new DataTable();
        dtPan1 = CommonFunction.Get_Data(strConn, string.Format(_selectPan, "1"));
        DataTable dtPan2 = new DataTable();
        dtPan2 = CommonFunction.Get_Data(strConn, string.Format(_selectPan, "2"));
        DataTable dtPan3 = new DataTable();
        dtPan3 = CommonFunction.Get_Data(strConn, string.Format(_selectPan, "3"));

        string _pan1 = "";
        string _pan2 = "";
        string _pan3 = "";


        //ที่ให้วนหมดเพราะต้องการให้ใส่ข้อมูลเข้าไปถูก อาจจะมีการเว้น่ช่องของข้อมูล
        for (int _compartNo = 1; _compartNo <= 10; _compartNo++)
        {
            switch (_compartNo)
            {
                case 1:

                    _pan1 = dtPan1.Select("NCOMPARTNO = 1").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 1").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 1").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 1").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 1").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 1").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }

                    break;
                case 2:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 2").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 2").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 2").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 2").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 2").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 2").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 3:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 3").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 3").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 3").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 3").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 3").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 3").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 4:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 4").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 4").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 4").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 4").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 4").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 4").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 5:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 5").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 5").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 5").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 5").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 5").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 5").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 6:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 6").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 6").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 6").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 6").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 6").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 6").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 7:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 7").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 7").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 7").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 7").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 7").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 7").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 8:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 8").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 8").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 8").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 8").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 8").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 8").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 9:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 9").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 9").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 9").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 9").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 9").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 9").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 10:
                    _pan1 = dtPan1.Select("NCOMPARTNO = 10").FirstOrDefault() != null ? dtPan1.Select("NCOMPARTNO = 10").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan2 = dtPan2.Select("NCOMPARTNO = 10").FirstOrDefault() != null ? dtPan2.Select("NCOMPARTNO = 10").FirstOrDefault().ItemArray[3] + "" : "";
                    _pan3 = dtPan3.Select("NCOMPARTNO = 10").FirstOrDefault() != null ? dtPan3.Select("NCOMPARTNO = 10").FirstOrDefault().ItemArray[3] + "" : "";
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
            }
        }

    }

    public void SaveTBLReqSlot(string _reqID, string _slot, string _pan, string _capacity, string _status)
    {
        if (_capacity.Contains(','))
        {
            _capacity = _capacity.Replace(",", "");
        }


        string Check = @"SELECT REQUEST_ID, SLOT_NO, LEVEL_NO,  CAPACITY, LEVEL_STATUS
FROM TBL_REQSLOT WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";

        int cCount = CommonFunction.Count_Value(strConn, Check);


        string _qryInsertDetail = "";

        string _qryUpdateDetail = @"UPDATE TBL_REQSLOT
SET   CAPACITY     = " + CommonFunction.ReplaceInjection(_capacity + "") + @"
WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";


        _qryInsertDetail = @" INSERT INTO TBL_REQSLOT VALUES ("
              + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
              + "," + CommonFunction.ReplaceInjection(_slot + "")
              + "," + CommonFunction.ReplaceInjection(_pan + "")
              + "," + CommonFunction.ReplaceInjection(!string.IsNullOrEmpty(_capacity + "") ? _capacity + "" : "null")
              + ",'" + CommonFunction.ReplaceInjection(_status + "") + "'"
              + ",''"
              + ")";
        using (OracleConnection con = new OracleConnection(strConn))
        {
            con.Open();
            if (cCount > 0)
            {
                using (OracleCommand cmd = new OracleCommand(_qryUpdateDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (OracleCommand cmd = new OracleCommand(_qryInsertDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    //ส่งเมลล์
    private bool SendMailToUser(string sREQID)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ส่งเมลล์ให้ รข";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = "";
        }

        #region html
        sHTML = @"<table width=""100%"" border=""0"" cellpadding=""3"" cellspacing=""1"">
            <tr>
                <td style=""font-size:15px;"">
                      ประเภทคำขอ  : " + cboRequestType.Text + @"
                      สาเหตุ : " + cboCause.Text + (!string.IsNullOrEmpty(txtRemarkCause.Text) ? "หมายเหตุ :" + txtRemarkCause.Text : "") + @"
                </td>
            </tr>
            <tr>
                <td style=""font-size:15px;"">
                     หมายเหตุ :  " + cboCause.Text + (!string.IsNullOrEmpty(txtRemarkCause.Text) ? "หมายเหตุ :" + txtRemarkCause.Text : "") + @"
                </td>
            </tr>
          </table>";
        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(strConn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }

    #region AutoComplete
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboCarregis_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        string vehID = CommonFunction.ReplaceInjection(cboVendor.Value + "");
        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.DataSource = null;
        comboBox.DataBind();

        //จะไม่เช็คว่ารถเกิน 8 ปีไหม
        //        sdsCarregis.SelectCommand = @"SELECT STRUCKID, head,tail,tail1  FROM (
        //          SELECT ROW_NUMBER()OVER(ORDER BY Head.STRUCKID) AS RN, Head.STRUCKID, 
        //                               Head.SHEADREGISTERNO as head,
        //                               Tail.SHEADREGISTERNO as tail,
        //                               CASE  WHEN  NVL(Tail.SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||Tail.SHEADREGISTERNO ELSE ' ' END as tail1,
        //                               HEAD.STRANSPORTID ,
        //                               NVL(TREQ.STRUCKID,'xxx') as Flag,
        //                                NVL(TREQ.STATUS_FLAG,'xxx') STATUS_FLAG,
        //                               CASE WHEN HEAD.SCARTYPEID = '0' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Head.DREGISTER,Head.DSIGNIN),sysdate))  
        //                               WHEN HEAD.SCARTYPEID = '3' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Tail.DREGISTER,Tail.DSIGNIN),sysdate))  
        //                               ELSE 0 END as NYEAR
        //                               FROM 
        //                                 TTRUCK Head-- ON C.SCONTRACTID = HEAD.SCONTRACTID AND TO_DATE(C.DEND ) - TO_DATE(sysdate)  >= 0
        //                               LEFT JOIN TTRUCK Tail
        //                               ON Head.STRAILERID = Tail.STRUCKID AND Head.STRANSPORTID = Tail.STRANSPORTID
        //                               LEFT JOIN (SELECT STRUCKID,STATUS_FLAG FROM TBL_REQUEST 
        //WHERE  REQUEST_DATE  IN (select max(NVL(REQUEST_DATE,SYSDATE)) as REQUEST_DATE from TBL_REQUEST group by STRUCKID)) TREQ
        //                               ON  Head.STRUCKID = TREQ.STRUCKID
        //                               WHERE Head.SCARTYPEID in (0,3) AND  HEAD.STRANSPORTID  = '" + vehID + @"' AND NVL(TREQ.STATUS_FLAG,'xxx')  IN ('11','99','xxx','10') --OR Head.STRUCKID = '/*txtSTRUCKID.Text*/'
        //         AND (Head.STRUCKID||Head.SHEADREGISTERNO||Tail.SHEADREGISTERNO  LIKE :fillter)) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsCarregis.SelectCommand = @"SELECT STRUCKID, head,tail,tail1  FROM (
          SELECT ROW_NUMBER()OVER(ORDER BY Head.STRUCKID) AS RN, Head.STRUCKID, 
                               Head.SHEADREGISTERNO as head,
                               Tail.SHEADREGISTERNO as tail,
                               CASE  WHEN  NVL(Tail.SHEADREGISTERNO,'xxx') <> 'xxx' THEN '/'||Tail.SHEADREGISTERNO ELSE ' ' END as tail1,
                               HEAD.STRANSPORTID ,
                               NVL(TREQ.STRUCKID,'xxx') as Flag,
                                NVL(TREQ.STATUS_FLAG,'xxx') STATUS_FLAG,
                               CASE WHEN HEAD.SCARTYPEID = '0' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Head.DREGISTER,Head.DSIGNIN),sysdate))  
                               WHEN HEAD.SCARTYPEID = '3' THEN  extract(year from sysdate) - extract(year from NVL(NVL(Tail.DREGISTER,Tail.DSIGNIN),sysdate))  
                               ELSE 0 END as NYEAR
                               FROM TCONTRACT C
                             LEFT JOIN TCONTRACT_TRUCK SCT-- ON C.SCONTRACTID = HEAD.SCONTRACTID AND TO_DATE(C.DEND ) - TO_DATE(sysdate)  >= 0
ON SCT.SCONTRACTID = C.SCONTRACTID AND NVL(C.CACTIVE,'N') = 'Y'
LEFT JOIN TTRUCK Head-- ON C.SCONTRACTID = HEAD.SCONTRACTID AND TO_DATE(C.DEND ) - TO_DATE(sysdate)  >= 0
ON HEAD.STRUCKID = SCT.STRUCKID
LEFT JOIN TTRUCK Tail
ON NVL(Head.STRAILERID,'xxx') = NVL(Tail.STRUCKID,'xxx') AND Head.STRANSPORTID = Tail.STRANSPORTID
                               LEFT JOIN (SELECT STRUCKID,STATUS_FLAG FROM TBL_REQUEST 
WHERE  REQUEST_DATE  IN (select max(NVL(REQUEST_DATE,SYSDATE)) as REQUEST_DATE from TBL_REQUEST group by STRUCKID)) TREQ
                               ON  Head.STRUCKID = TREQ.STRUCKID
                               WHERE NVL(C.CACTIVE,'xxx')  = 'Y' AND Head.SCARTYPEID in (0,3) AND  HEAD.STRANSPORTID  = '" + vehID + @"' AND NVL(TREQ.STATUS_FLAG,'xxx')  IN ('11','99','xxx','10') --OR Head.STRUCKID = '/*txtSTRUCKID.Text*/'
         AND (Head.STRUCKID||Head.SHEADREGISTERNO||Tail.SHEADREGISTERNO  LIKE :fillter)) WHERE RN BETWEEN :startIndex AND :endIndex";


        sdsCarregis.SelectParameters.Clear();
        sdsCarregis.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsCarregis.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsCarregis.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        //if(sdsCarregis.c)
        comboBox.DataSource = sdsCarregis;
        comboBox.DataBind();

    }

    protected void cboCarregis_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    #endregion

    #region class
    [Serializable]

    class TData_Car
    {
        public string SVENDORID { get; set; }
        public string SABBREVIATION { get; set; }
        public string STRUCKID { get; set; }
        public string SHEADREGISTERNO { get; set; }
        public string STRAILERREGISTERNO { get; set; }
        public decimal? SCARTYPEID { get; set; }
        public string SCHASIS_H { get; set; }
        public string SCHASIS_T { get; set; }
        public string TXT { get; set; }
        public string CARCATE_ID { get; set; }
        public string CARCATE_NAME { get; set; }
        public decimal? NSLOT { get; set; }
        public decimal? NTOTALCAPACITY { get; set; }
        public string VEH_ID { get; set; } // id หัว
        public string TU_ID { get; set; } //id หาง

    }

    public class DOCUMENT
    {
        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string REQUEST_ID { get; set; }
        public string DOC_ID { get; set; }
        public string DOC_ITEM { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_SYSNAME { get; set; }
        public string FILE_PATH { get; set; }
        public string CONSIDER { get; set; }
        public string SVISIBLE { get; set; }
        public string CDYNAMIC { get; set; }
    }

    class TData_DocType
    {
        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string DOC_01 { get; set; }
        public string DOC_02 { get; set; }
        public string DOC_03 { get; set; }
        public string DOC_04 { get; set; }
        public string ISACTIVE_FLAG { get; set; }
        public string CDYNAMIC { get; set; }
        public string CATTACH_DOC01 { get; set; }
        public string CATTACH_DOC02 { get; set; }
        public string CATTACH_DOC03 { get; set; }
        public string CATTACH_DOC04 { get; set; }
    }
    #endregion
}