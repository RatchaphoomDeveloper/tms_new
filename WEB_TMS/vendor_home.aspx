﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_home.aspx.cs" Inherits="vendor_home" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPanel" Assembly="DevExpress.Web.v11.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <%--แบ่งหน้าHTML,Sort หัว แบบเจน HTML--%>
    <link rel="stylesheet" type="text/css" href="jQuery/tablePagination.css" />
    <script type="text/javascript" src="jQuery/jquery.tablePagination.0.2.js"></script>
    <link rel="stylesheet" href="jQuery/sorting/style.css" type="text/css" media="print, projection, screen" />
    <script type="text/javascript" src="jQuery/jquery.tablesorter.js"></script>
    <%--จบ แบ่งหน้าHTML,Sort หัว แบบเจน HTML--%>
    <style type="text/css">
        .mainTable
        {
            background-image: url(Images/background.png);
            width: 224px;
        }
        
        .refreshButton
        {
            margin-left: 2px;
            margin-top: 2px;
            border-style: none;
            cursor: pointer;
            position: relative;
            z-index: 2;
        }
        
        .captchaDiv
        {
            margin-top: -17px;
            margin-left: 9px;
            position: relative;
            z-index: 1;
        }
        
        .labelCell
        {
            padding-top: 20px;
            padding-left: 71px;
            font-family: Tahoma;
            font-size: 9pt;
            color: #ffffff;
        }
        
        .textBoxCell
        {
            /*padding-left: 12px;
            padding-top: 6px;*/
            padding-bottom: 10px;
        }
    </style>
    <!--[if lte IE 6]>
    <style type="text/css">
        .mainTable {
            background-image: url(Images/backgroundIE6.gif);
            width: 224px;
        }
    </style>
    <![endif]-->
    <style type="text/css">
        .reload
        {
            float: right;
            cursor: pointer;
            background: url(Images/reload.gif) no-repeat center right;
            width: 16px;
        }
    </style>
    <script type="text/javascript">

        function setPageforTable(tableID)//แบ่งหน้าในตาราง
        {
            var options = {
                currPage: 1,
                optionsForRows: [5, 10, 20],
                rowsPerPage: 5,
                firstArrow: (new Image()).src = "./images/Pagination/first.gif",
                prevArrow: (new Image()).src = "./images/Pagination/prev.gif",
                lastArrow: (new Image()).src = "./images/Pagination/last.gif",
                nextArrow: (new Image()).src = "./images/Pagination/next.gif"
            }
            $('#' + tableID).tablePagination(options);
            $('#' + tableID).tablesorter({ headers: { 0: { sorter: false}} });
        }
        $(document).ready(function () {
            setPageforTable('mytable'); 
            setPageforTable('mytablealert');
        });


        // <![CDATA[
        function GetRefreshButton() {
            return document.getElementById("refreshButton");
        }
        function OnCaptchaBeginCallback(s, e) {
            var refreshButton = GetRefreshButton();
            refreshButton.src = "Images/refreshButtonAnimated.gif";
        }
        function OnCaptchaEndCallback(s, e) {
            var refreshButton = GetRefreshButton();
            refreshButton.src = "Images/refreshButton.gif";
            tbCode.SetValue("");
            if (typeof (lblCorrectCodeMessage) != "undefined")
                lblCorrectCodeMessage.SetVisible(false);
            if (typeof (lblIncorrectCodeMessage) != "undefined")
                lblIncorrectCodeMessage.SetVisible(false);
        }
        // ]]>
        function reload(id) {
            switch (id) {
                case "vendor_conftruck":
                    xcpn_conftruck.PerformCallback('' + id);
                    break;
                case "vendor_confplan":
                    xcpn_confplan.PerformCallback('' + id);
                    break;
                case "vendor_accident":
                    xcpn_accident.PerformCallback('' + id);
                    break;
                case "vendor_approve":
                    xcpn_accident.PerformCallback('' + id);
                    break;
                case "vendor_plan2day":
                    xcpn_plan2day.PerformCallback('' + id);
                    
                    break;
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table border="0" width="100%">
        <tbody>
            <tr>
                <td width="33%">
                    <dx:ASPxCallbackPanel ID="xcpn_conftruck" runat="server" HideContentOnCallback="False"
                        ClientInstanceName="xcpn_conftruck" OnCallback="xcpn_conftruck_Callback">
                        <ClientSideEvents EndCallback="function(s,e){ setToolTip(); } " />
                        <PanelCollection>
                            <dx:PanelContent ID="xpct_conftruck" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0" height="123" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td valign="top" width="100%">
                                                <table border="0" width="100%" class="vendor_conftruck_review">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="images/mousex.png" height="16" width="16" /><span id="spn_ConfirmContract" runat='server' class="style16"> สรุปรถยืนยันตามสัญญา 
                                                                            </span></td>
                                                                        <td style="width: 16px; white-space: nowrap;">
                                                                            <div class="reload" id="vendor_conftruck" onclick="reload(this.id);">
                                                                                &nbsp;</div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Literal ID="ltrvendor_conftruck" runat="server">
                                                        <table border="0" width="100%">
                                                        <tr>
                                                            <td width="67%">
                                                                รถตามสัญญา</td>
                                                            <td align="center" width="33%">
                                                                0 คัน </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ยืนยันแล้ว</td>
                                                            <td align="center">
                                                                0 คัน </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ไม่ยืนยัน</td>
                                                            <td align="center">
                                                                0 คัน </td>
                                                        </tr>
                                                    </table>
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">
                                                                <a href="vendor_confirm_contracts.aspx">ดูรายละเอียด</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </td>
                <td width="33%">
                    <dx:ASPxCallbackPanel ID="xcpn_confplan" runat="server" HideContentOnCallback="False"
                        ClientInstanceName="xcpn_confplan" OnCallback="xcpn_conftruck_Callback">
                        <ClientSideEvents EndCallback="function(s,e){ setToolTip(); } " />
                        <PanelCollection>
                            <dx:PanelContent ID="xpct_confplan" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0" height="124" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td valign="top" width="100%">
                                                <table border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" class="style16">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="images/finished-work.png" height="16" width="16" /><span id="spn_ConfirmPlan" runat='server' class="style16">สรุปรถยืนยันตามแผน </span>
                                                                        </td>
                                                                        <td style="width: 16px; white-space: nowrap;">
                                                                            <div class="reload" id="vendor_confplan" onclick="reload(this.id);">
                                                                                &nbsp;</div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Literal ID="ltr_confplan" runat="server">
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td width="56%">
                                                                รถตามแผน</td>
                                                            <td align="center" width="44%">
                                                                0 คัน </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ยืนยันแล้ว</td>
                                                            <td align="center">
                                                                0 คัน </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                ไม่ยืนยัน</td>
                                                            <td align="center">
                                                                0 คัน </td>
                                                        </tr>
                                                    </table>
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td align="right">
                                                                <a href="PlanTransport.aspx">ดูรายละเอียด</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </td>
                <td width="33%">
                    <dx:ASPxCallbackPanel ID="xcpn_accident" runat="server" HideContentOnCallback="False"
                        ClientInstanceName="xcpn_accident" OnCallback="xcpn_conftruck_Callback" >
                        <ClientSideEvents EndCallback="function(s,e){ setToolTip(); } " />
                        <PanelCollection>
                            <dx:PanelContent ID="xpct_accident" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td width="100%">
                                                <table border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="images/43.png" height="16" width="16" /><span id="spn_TruckIssue" runat='server' class="style16">สรุปรถเกิดปัญหา </span></td>
                                                                        <td style="width: 16px; white-space: nowrap;">
                                                                            <div class="reload" id="vendor_accident" onclick="reload(this.id);">
                                                                                &nbsp;</div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Literal ID="ltr_accident" runat="server">
                                                    <table border="0" width="100%">
                                            <tr>
                                                <td width="60%">
                                                    รถตกค้าง</td>
                                                <td align="center" width="40%">
                                                    0 คัน <a href="vendor_terminal_remain_lst.htm"><img src="images/Zoom.png" border="0"
                                                        height="16" width="16"></a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    รถไม่ผ่านการตรวจสภาพ</td>
                                                <td align="center">
                                                    0 คัน <a href="vendor_terminal_remain_lst.htm"><img src="images/Zoom.png" border="0"
                                                        height="16" width="16"></a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    รถเกิดอุบัติเหตุ</td>
                                                <td align="center">
                                                    0 คัน <a href="vendor_accident_lst.htm"><img src="images/Zoom.png" border="0" height="16"
                                                        width="16"></a></td>
                                            </tr>
                                                    </table>
                                                                </asp:Literal>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td align="right">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </td> 
                <%--<td width="21%">
                    <table cellpadding="0" cellspacing="0" border="0" height="122" width="100%">
                        <tbody>
                            <tr>
                                <td align="left" valign="top">
                                    <img src="images/im_tipnet011_i.gif"></td>
                                <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                    <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                <td align="right" valign="top">
                                    <img src="images/im_tipnet011_l.gif"></td>
                            </tr>
                            <tr>
                                <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                    <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                <td width="100%">
                                    <table border="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    ยินดีต้อนรับ </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    ผู้ใช้ :
                                                    <asp:Label ID="lbFulllName" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    หน่วยงาน :
                                                    <asp:Label ID="lblVendorName" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span style="display: inline;">เปลี่ยน Password </span>&nbsp;</td>
                                                <td>
                                                    <dx:ASPxHyperLink ID="lnkForget" runat="server" ImageUrl="images/15.PNG" Font-Bold="true"
                                                        Font-Underline="false" Cursor="pointer">
                                                        <ClientSideEvents Click="function(s, e) { pcForget.Show(); }"></ClientSideEvents>
                                                    </dx:ASPxHyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="66%">
                                                    <span style="display: none;">ปรับปรุงข้อมูลส่วนตัว</span>&nbsp;</td>
                                                <td width="34%">
                                                    <img src="images/ic_04.GIF" style="display: none;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                    <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                    <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                <td>
                                    <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>--%> 
            </tr>
                <td valign="top">
                    <dx:ASPxCallbackPanel ID="xcpn_approve" runat="server" HideContentOnCallback="False"
                        ClientInstanceName="xcpn_approve" OnCallback="xcpn_Callback">
                        <ClientSideEvents EndCallback="function(s,e){ setToolTip(); } " />
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent2" runat="server">
                                <table cellpadding="0" cellspacing="0" border="0" style="height: 123;" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img
                                                    src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img
                                                    src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td valign="top" width="100%">
                                                <table border="0" width="100%" class="vendor_conftruck_review">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="98%">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="images/mousex.png" height="16" width="16"><span runat="server" id="Span1"
                                                                                class="style16">ตรวจวัดน้ำ (มว.)</span>
                                                                            <dx:ASPxLabel runat="server" ID="lblMonth">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td style="width: 16px; white-space: nowrap;">
                                                                            <div class="reload" id="vendor_approve" onclick="reload(this.id);">
                                                                                &nbsp;
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%--   <asp:Literal ID="ltrloadorder" runat="server">--%>
                                                                <table width="100%" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="40%"></td>
                                                                            <td width="20%" align="right">รอดำเนินการ</td>
                                                                            <%--//<td width="15%" align="right">ปรับปรุง</td>--%>
                                                                            <td width="20%" align="right">ปิดงาน</td>
                                                                            <td width="20%" align="right">ยกเลิก</td>
                                                                        </tr>
                                                                        <%--<tr runat="server" id="trRk">
                                                                            <td><a href="approve.aspx">ตรวจสอบเอกสาร (รข.)</a></td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw1">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw2">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw3">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw4">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>--%>
                                                                        <tr runat="server" id="trMv">
                                                                            <td>ตรวจสอบวัดน้ำ</td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw21">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                            <%--<td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw22">
                                                                                </dx:ASPxLabel>
                                                                            </td>--%>
                                                                             <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw23">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                             <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw24">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr runat="server" id="trMv2">
                                                                            <td>ตรวจสอบรับรองความถูกต้อง และตีซีลใหม่ของรถน้ำมัน</td>
                                                                            <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw25">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                           <%-- <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw26">
                                                                                </dx:ASPxLabel>
                                                                            </td>--%>
                                                                             <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw27">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                             <td align="right">
                                                                                <dx:ASPxLabel runat="server" ID="lblSumw28">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <%--  </asp:Literal>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">&nbsp;
                                                                <!--<a href="vendor_confirm_contract.aspx">ดูรายละเอียด</a>-->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img
                                                    src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img
                                                    src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                </td>
            </tr>
        </tbody>
    </table>
    <dx:ASPxCallbackPanel ID="xcpn_plan2day" runat="server" HideContentOnCallback="False"
        ClientInstanceName="xcpn_plan2day" OnCallback="xcpn_conftruck_Callback">
        <PanelCollection>
            <dx:PanelContent ID="xpct_plan2day" runat="server">
                <table border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top" width="59%">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td width="100%">
                                                <table cellpadding="2" cellspacing="1" border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4" bgcolor="#FFFFFF">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="99%">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="images/order.png" height="16" width="16" /><span id="spn_Plan2Day" runat="server" class="style16"> แผนงาน </span> </td>
                                                                        <td style="width: 16px; white-space: nowrap;">
                                                                            <%--<div class="reload" id="vendor_plan2day" onclick="reload(this.id);">
                                                                                &nbsp;</div>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <div class="divTable" style="width: 100%; height: 100%">
                                                                    <asp:Literal ID="ltr_2dayplan" runat="server"> 
                                                                    </asp:Literal>
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                <span style="display: none;"><a href="vendor_confirm_fifo.htm">ดูรายละเอียด</a></span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                            </td>
                            <td valign="top" width="41%">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td width="100%">
                                                <table border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" class="style16">
                                                                <img src="images/em.gif" height="16" width="16" /><span id="spn_Alert" runat="server" class="style16">แจ้งเตือนในระบบ</span> </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <div class="divTable" style="width: 100%; height: 100%">
                                                                    <asp:Literal ID="ltrAlert" runat="server"></asp:Literal></div>
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="57%">
                                                                &nbsp;</td>
                                                            <td align="center" width="43%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <img src="images/im_tipnet011_i.gif"></td>
                                            <td align="right" background="images/im_tipnet011_k.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_k.gif" height="6" width="6"></td>
                                            <td align="right" valign="top">
                                                <img src="images/im_tipnet011_l.gif"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" background="images/im_tipnet011_d.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_d.gif" height="6" width="6"></td>
                                            <td width="100%">
                                                <table border="0" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" class="style16">
                                                                <img src="images/communication.png" height="16" width="16"> ดาวน์โหลดแบบฟอร์ม
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="57%"><a href="FileFormat/Vendor/ConfirmTruckGPSFormat.xls">แบบฟอร์มการยืนยันรถตามสัญญา</a></td>
                                                            <td align="center" width="43%">
                                                                <a href="faqsearch.aspx">ถามตอบเบื้องต้น FAQ</a></td>
                                                        </tr>
                                                        <tr runat="server" id="trVen">
                                                            <td colspan="2"><a href="vendor_homeAlert.aspx">งานสอบเทียบ</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td align="center">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;</td>
                                                            <td align="center">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td align="right" background="images/im_tipnet011_e.gif" valign="top" width="6">
                                                <img src="images/im_tipnet011_e.gif" height="6" width="6"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/im_tipnet011_f.gif" height="6" width="6"></td>
                                            <td align="left" background="images/im_tipnet011_g.gif" height="6" valign="top">
                                                <img src="images/im_tipnet011_g.gif" height="6" width="6"></td>
                                            <td>
                                                <img src="images/im_tipnet011_h.gif" height="6" width="6"></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>
                                    &nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="59%">
                                &nbsp;</td>
                            <td width="41%">
                                &nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s, e){ if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpPopup!=undefined){eval(s.cpPopup); s.cpPopup=null; } }">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <dx:ASPxPopupControl ID="pcForget" runat="server" CloseAction="CloseButton" Modal="True"
                    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="pcForget"
                    HeaderText="กรุณาระบุข้อมูลให้ครบถ้วน" AllowDragging="True" EnableAnimation="False"
                    EnableViewState="False">
                    <ContentCollection>
                        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                            <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btnPopOK">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <table cellpadding="2" cellspacing="2" width="420px">
                                            <tr>
                                                <td width="25%">
                                                    Username </td>
                                                <td width="75%">
                                                    <dx:ASPxTextBox ID="txtForgetUsername" runat="server" Width="170px">
                                                        <ValidationSettings EnableCustomValidation="True" ValidationGroup="forget" SetFocusOnError="True"
                                                            ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="กรุณาระบุ!" IsRequired="True" />
                                                            <ErrorFrameStyle Font-Size="10px">
                                                                <ErrorTextPaddings PaddingLeft="0px"></ErrorTextPaddings>
                                                            </ErrorFrameStyle>
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุ!"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Old Password</td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtoldpassword" runat="server" Width="170px" Password="True">
                                                        <ValidationSettings EnableCustomValidation="True" ValidationGroup="forget" SetFocusOnError="True"
                                                            ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="กรุณาระบุ!" IsRequired="True" />
                                                            <ErrorFrameStyle Font-Size="10px">
                                                                <ErrorTextPaddings PaddingLeft="0px"></ErrorTextPaddings>
                                                            </ErrorFrameStyle>
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุ!"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    New Password</td>
                                                <td>
                                                    <dx:ASPxTextBox ID="txtnewpassword" runat="server" Width="170px" Password="True">
                                                        <ValidationSettings EnableCustomValidation="True" ValidationGroup="forget" SetFocusOnError="True"
                                                            ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField ErrorText="กรุณาระบุ!" IsRequired="True" />
                                                            <ErrorFrameStyle Font-Size="10px">
                                                                <ErrorTextPaddings PaddingLeft="0px"></ErrorTextPaddings>
                                                            </ErrorFrameStyle>
                                                            <RequiredField IsRequired="True" ErrorText="กรุณาระบุ!"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td colspan="2">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <table class="mainTable" cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td align="left" style="font-size: 0;">
                                                                            <img id="refreshButton" src="Images/refreshButton.gif" alt="Show another code" title=""
                                                                                class="refreshButton" onclick="captcha.Refresh();" />
                                                                            <div class="captchaDiv">
                                                                                <dx:ASPxCaptcha ID="Captcha" runat="server" SkinID="dummy" ClientInstanceName="captcha"
                                                                                    CodeLength="6">
                                                                                    <ChallengeImage BackgroundColor="Transparent" ForegroundColor="#676767" BorderWidth="0"
                                                                                        Height="72" />
                                                                                    <ValidationSettings EnableValidation="False">
                                                                                    </ValidationSettings>
                                                                                    <TextBox Visible="False" />
                                                                                    <RefreshButton Visible="False">
                                                                                    </RefreshButton>
                                                                                    <LoadingPanel Enabled="False" />
                                                                                    <ClientSideEvents BeginCallback="OnCaptchaBeginCallback" EndCallback="OnCaptchaEndCallback" />
                                                                                </dx:ASPxCaptcha>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" class="labelCell">
                                                                            <span>
                                                                                <dx:ASPxLabel ID="lblCode" runat="server" AssociatedControlID="tbCode" Text="Type the code">
                                                                                </dx:ASPxLabel>
                                                                            </span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="textBoxCell">
                                                                            <dx:ASPxTextBox ID="tbCode" runat="server" Width="200px" AutoCompleteType="Disabled"
                                                                                BackColor="Transparent" ClientInstanceName="tbCode">
                                                                                <Border BorderWidth="0px" />
                                                                                <ValidationSettings EnableCustomValidation="True" SetFocusOnError="True"
                                                                                    ErrorDisplayMode="None" ErrorFrameStyle-Border-BorderColor="Red" >
                                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาระบุ!"></RequiredField>
                                                                                    <ErrorFrameStyle>
                                                                                        <Border BorderColor="Red"></Border>
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="padding-top: 10px;">
                                                                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" UseSubmitBehavior="true"
                                                                    AutoPostBack="true" Width="175px">
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" style="margin-top: 5px; height: 20px; vertical-align: middle;">
                                                                <dx:ASPxLabel ID="lblIncorrectCodeMessage" runat="server" ForeColor="Red" Visible="False"
                                                                    Text="The submitted code is incorrect" ClientInstanceName="lblIncorrectCodeMessage">
                                                                </dx:ASPxLabel>
                                                                <dx:ASPxLabel ID="lblCorrectCodeMessage" runat="server" ForeColor="Green" Visible="False"
                                                                    Text="The submitted code is correct" ClientInstanceName="lblCorrectCodeMessage">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxButton ID="btnPopOK" runat="server" SkinID="popupOK">
                                                                    <ClientSideEvents Click="function(s, e) {  if(ASPxClientEdit.ValidateGroup('forget')){xcpn.PerformCallback('popup'); pcForget.Hide();} }">
                                                                    </ClientSideEvents>
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                &nbsp; </td>
                                                            <td>
                                                                <dx:ASPxButton ID="btnPopCancel" runat="server" SkinID="popupCancel">
                                                                    <ClientSideEvents Click="function(s, e) { pcForget.Hide(); }"></ClientSideEvents>
                                                                </dx:ASPxButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                    <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('forget'); }">
                    </ClientSideEvents>
                    <ContentStyle>
                        <Paddings PaddingBottom="5px" />
                        <Paddings PaddingBottom="5px"></Paddings>
                    </ContentStyle>
                </dx:ASPxPopupControl>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
