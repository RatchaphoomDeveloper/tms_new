﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class report_kpi : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if(!IsPostBack)
        {
            SetinitalGridView();
        }
    }

    private void SetinitalGridView()
    {
        try
        {
            dgvreportkpi.DataSource = KPIBLL.Instance.GetFormReport();
            dgvreportkpi.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

   
    protected void ADD_Report_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddReportKpi.aspx");
    }
    protected void Edit_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string command = btn.CommandArgument;
        string url = "EditReportKpi.aspx?str=";
        string id = dgvreportkpi.DataKeys[int.Parse(command)].Value.ToString();
        url += ConfigValue.GetEncodeText(id);
        Response.Redirect(url);
    }
    protected void mpConfirmBack_ClickOK(object sender ,EventArgs e)
    {
        Response.Redirect("Kpi_lst.aspx");
    }
}