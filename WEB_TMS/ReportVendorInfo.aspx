﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ReportVendorInfo.aspx.cs"
    Inherits="ReportVendorInfo" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .Rotate
        {
            height: 50px;
        }
        
        
        .Rotate table
        {
            /* float your elements or inline-block them to display side by side */
            float: left; /* these are height and width dimensions of your header */
            height: 10em;
            width: 1.5em; /* set to hidden so when there's too much vertical text it will be clipped. */
            overflow: auto; /* these are not relevant and are here to better see the elements */
            background: #E2F0FF;
            margin-right: 1px;
        }
        .Rotate td
        {
            /* setting background may yield better results in IE text clear type rendering */
            background: #E2F0FF;
            display: block; /* this will prevent it from wrapping too much text */
            white-space: nowrap; /* so it stays off the edge */
            padding-left: 3px; /* IE specific rotation code */
            writing-mode: tb-rl;
            filter: flipv fliph; /* CSS3 specific totation code */ /* translate should have the same negative dimension as head height */
            transform: rotate(270deg) translate(-10em,0);
            transform-origin: 0 0;
            -moz-transform: rotate(270deg) translate(-10em,0);
            -moz-transform-origin: 0 0;
            -webkit-transform: rotate(270deg) translate(-10em,0);
            -webkit-transform-origin: 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            &nbsp; </td>
                        <td width="202px">
                            <dx:ASPxTextBox ID="txtKeyword" runat="Server" ClientInstanceName="txtKeyword" Width="200px" NullText="เลขทะเบียนหัว-ท้าย, ชื่อบริษัทผู้ข่นส่ง">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="82px">
                            <dx:ASPxButton ID="btnSearch" runat="Server" ClientInstanceName="btnSearch" SkinID="_search">
                                <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <td width="87%" height="35">
                            <dx:ASPxLabel ID="lblReport" runat="Server" ClientInstanceName="lblReport" EncodeHtml="false" EnableTheming="false"
                                EnableDefaultAppearance="false">
                            </dx:ASPxLabel>
                        </td>
                        <td width="13%">
                            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td width="13%">
                                        <dx:ASPxButton ID="btnPrint" runat="Server" ClientInstanceName="btnPrint" EncodeHtml="false" EnableTheming="false"
                                            SkinID="None" EnableDefaultAppearance="false" Image-Url="Images/ic_pdf2.gif" Image-Width="16" Image-Height="16"
                                            ToolTip="Print" Cursor="pointer" AutoPostBack="true" OnClick="btnPrint_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td width="37%">
                                        Print</td>
                                    <td width="13%">
                                        <dx:ASPxButton ID="btnExport" runat="Server" ClientInstanceName="btnExport" EncodeHtml="false" EnableTheming="false"
                                            SkinID="None" EnableDefaultAppearance="false" Image-Url="Images/ic_ms_excel.gif" Image-Width="16"
                                            ToolTip="Export" Cursor="pointer" Image-Height="16" AutoPostBack="true" OnClick="btnExport_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td width="37%">
                                        Export</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <dx:ASPxGridView ID="gvwVendor" runat="server" ClientInstanceName="gvwVendor" SkinID="_gvw" Width="100%"
                    AutoGenerateColumns="false" Style="margin: 0px;">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ลำดับที่" Width="55px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="ชื่อบริษัทผู้ขนส่ง" FieldName="SABBREVIATION" Width="115px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="ประเภทผู้ขนส่ง" FieldName="STRANS_TYPE_DESC" Width="115px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewBandColumn Caption="ทะเบียนรถขนส่ง">
                            <Columns>
                                <dx:GridViewDataColumn Caption="ทะเบียน<br/>หัวลาก" FieldName="HEAD_NO" Width="87px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="ทะเบียน<br/>หาง" FieldName="TRAIL_NO" Width="87px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewDataColumn Caption="ประเภท<br/>รถขนส่ง" FieldName="NWHEELS" Width="60px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewBandColumn Caption="กลุ่มงาน">
                            <Columns>
                                <dx:GridViewDataColumn Caption="ในสัญญา" FieldName="SCONTRACT" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="สำรอง" FieldName="CSTANDBY" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="จ้างพิเศษ" FieldName="CSPACIALCONTRAC" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="อื่นๆ(ระบุ)" FieldName="OJOB" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="ประเภทการขนส่ง">
                            <Columns>
                                <dx:GridViewDataColumn Caption="ลูกค้า" FieldName="CUST" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="โอนคลัง" FieldName="WARE" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="อินโดจีน" FieldName="INDO" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="อื่นๆ(ระบุ)" FieldName="OTRAN" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="ชนิดผลิตภัณฑ์">
                            <Columns>
                                <dx:GridViewDataColumn Caption="น้ำมันใส" FieldName="LOIL" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="LPG" FieldName="LPG" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="น้ำมันเตา" FieldName="FOIL" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="น้ำมันอากาศยาน" FieldName="FUEL" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="ยางมะตอย" FieldName="POL" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="อื่นๆ(ระบุ)" FieldName="OPROD" Width="50px">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom" CssClass="Rotate" />
                                    <CellStyle HorizontalAlign="Center" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewBandColumn>
                        <dx:GridViewDataColumn Caption="Fleetต้นทาง" FieldName="FLEET" Width="100px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataTextColumn Caption="ความจุ<br/>บรรทุก<br/>(ลิตร)" FieldName="NTOTALCAPACITY" Width="68px">
                            <PropertiesTextEdit DisplayFormatString="{0:N0}">
                            </PropertiesTextEdit>
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="ความสูง<br/>ของตัวรถ<br/>(วัดจากพื้น)<br/>หน่วยเป็น<br/>เมตร" FieldName="NTANK_HIGH"
                            Width="78px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataTextColumn Caption="จำนวน<br/>ช่อง<br/>บรรทุก" FieldName="NSLOT" Width="62px">
                            <PropertiesTextEdit DisplayFormatString="{0:N0}">
                            </PropertiesTextEdit>
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowHorizontalScrollBar="true" />
                    <SettingsPager Mode="ShowPager">
                    </SettingsPager>
                    <Styles Row-BackColor="#E3F7F8" AlternatingRow-BackColor="#B9EAEF" PagerBottomPanel-BackColor="#f0f0f0">
                    </Styles>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sdsVendor" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                    CacheKeyDependency="ckdVendor" SelectCommand=""></asp:SqlDataSource>
                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvwVendor" PaperKind="A4" RightMargin="0"
                    TopMargin="0" LeftMargin="0" Landscape="True" BottomMargin="0" MaxColumnWidth="270">
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
