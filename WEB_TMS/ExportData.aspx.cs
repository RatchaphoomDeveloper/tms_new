﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using DevExpress.XtraPrinting;

public partial class ExportData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = str.Split('|');//STCrypt.DecryptURL(str);
                //Response.Write(QueryString[0] + "");
                CONFTRCK.Visible = ("CONFTRCK" == QueryString[0] + "") ? true : false;
                PSCD.Visible = ("PSCD" == QueryString[0] + "") ? true : false;
                CONFPLNT.Visible = ("CONFPLNT" == QueryString[0] + "") ? true : false;
            }
        }
    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cboContractNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboContractNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsContract.SelectCommand = @"SELECT SCONTRACTID, SCONTRACTNO,TERMINAL FROM( 
    SELECT ROW_NUMBER() OVER(ORDER BY SCONTRACTNO) AS LINE_NO  ,SCONTRACTID , SCONTRACTNO,  NVL(STERMINALID,'COD') TERMINAL
    FROM  TCONTRACT
    WHERE NVL(CACTIVE,'N')='Y' AND SVENDORID = :SVENDORID   
)  
WHERE LINE_NO BETWEEN :startIndex AND :endIndex ";

        sdsContract.SelectParameters.Clear();
        sdsContract.SelectParameters.Add("SVENDORID", TypeCode.String, cbxVendor.Value + "");
        sdsContract.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsContract.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsContract;
        comboBox.DataBind();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {

        Cache.Remove(sdsContract.CacheKeyDependency);
        Cache[sdsContract.CacheKeyDependency] = new object();
        sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsContract.DataBind();

        Confirm_Truck.DataBind();

    }
    protected void btnXlsExport_Click(object sender, EventArgs e)
    {

        //int rownum = Confirm_Truck.EditingRowVisibleIndex;
        //dynamic data = Confirm_Truck.GetRowValues(rownum, "SVENDORNAME", "SCONTRACTNO", "NCOUNT", "SUMNPOINT");
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;

        //gridExport.PageHeader.Left = "ชื่อผู้ประกอบการ : " + data[0] + " เลขที่สัญญา : " + data[1] + " จำนวนครั้ง : " + data[2] + " คะแนนที่ถูกตัด : " + data[3] + " คะแนน";
        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("ConfirmTruck", options);
        //gridExport.WriteXlsToResponse();
    }
    protected void Confirm_Truck_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {

    }
    protected void gvw_PSCHD_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {

    }
    protected void btnXlsExport_PSCHD_Click(object sender, EventArgs e)
    {
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;
        grid_PSCHDExporter.WriteXlsToResponse("PLANSCHEDULE", options);

    }
    protected void btnSearch_PSCHD_Click(object sender, EventArgs e)
    {

        Cache.Remove(sds_PSCHD.CacheKeyDependency);
        Cache[sds_PSCHD.CacheKeyDependency] = new object();
        sds_PSCHD.Select(new System.Web.UI.DataSourceSelectArguments());
        sds_PSCHD.DataBind();

        gvw_PSCHD.DataBind();
    }
    protected void btnXlsExport_CONFPLNT_Click(object sender, EventArgs e)
    {

        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;
        grid_CONFPLNTExporter.WriteXlsToResponse("ConfirmPlan", options);
    }
    protected void btnSearch_CONFPLNT_Click(object sender, EventArgs e)
    {

        Cache.Remove(sds_CONFPLNT.CacheKeyDependency);
        Cache[sds_CONFPLNT.CacheKeyDependency] = new object();
        sds_CONFPLNT.Select(new System.Web.UI.DataSourceSelectArguments());
        sds_CONFPLNT.DataBind();

        gvw_CONFPLNT.DataBind();

    }
}