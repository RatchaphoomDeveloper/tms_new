﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="depo_terminal_remain_add.aspx.cs" Inherits="depo_terminal_remain_add"
    StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 15%">
                            หน่วยงาน<font color="#FF0000">*</font>
                        </td>
                        <td style="width: 35%">
                            <dx:ASPxTextBox ID="txtTerminal" runat="server" Width="170px" Border-BorderColor="#cccccc"
                                ClientEnabled="false">
                                <Border BorderColor="#CCCCCC"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 15%;">
                            วันที่แจ้ง
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 35%;">
                            <dx:ASPxLabel ID="lblDate" runat="server">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            วันที่รถตกค้าง<font color="#ff0000"> *</font>
                        </td>
                        <td align="left" class="style27">
                            <dx:ASPxDateEdit ID="dteBan" runat="server" SkinID="xdte">
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            Outbound No. <font color="#FF0000">*</font>
                        </td>
                        <td align="left" class="style18">
                            <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" 
                                ClientInstanceName="cboDelivery" EnableCallbackMode="True" MaxLength="10" 
                                OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL" 
                                OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL" 
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SDELIVERYNO" Width="180px">
                                <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){cboVendor.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID')) ; cboHeadRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')) ;txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) ;}else{s.SetValue('');};xcpn.PerformCallback('SelectChange');}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="Outbound No." FieldName="SDELIVERYNO" 
                                        Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" 
                                        Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO" 
                                        Width="100px" />
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" 
                                        Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสผู้ขนส่ง" FieldName="SVENDORID" Width="80px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsDelivery" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="style28">
                            ทะเบียนรถ (หัว) <font color="#ff0000">*</font>
                        </td>
                        <td align="left" class="style27">
                            <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="150px">
                                <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) ;}else{s.SetValue('');};xcpn.PerformCallback('SelectChange');}">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            ทะเบียนรถ (ท้าย)
                        </td>
                        <td align="left" class="style18">
                            <dx:ASPxTextBox ID="txtTrailerRegist" runat="server" ClientEnabled="False" ClientInstanceName="txtTrailerRegist"
                                Width="150px">
                                <Border BorderColor="#cccccc" />
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ชื่อผู้ขนส่ง <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                                 <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' == '-1')s.SetValue(''); xcpn.PerformCallback('SelectChange');}">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุชื่อผู้ขนส่ง" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            ชื่อพนักงานขับ
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientInstanceName="cmbPersonalNo"
                                EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SEMPLOYEEID" Width="150px">
                                 <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' == '-1')s.SetValue(''); }">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                    <dx:ListBoxColumn Caption="รหัสพนักงาน" FieldName="SEMPLOYEEID" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF" class="style24" valign="top">
                            สาเหตุรถตกค้าง <font color="#ff0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="3">
                            <dx:ASPxCheckBoxList ID="chkBanCause" runat="server" RepeatColumns="2" EnableTheming="True"
                                DataSourceID="sdsCarBanCause" TextField="SCARBANCAUSENAME" ValueField="NCARBANCAUSEID"
                                ValueType="System.Int32" ItemSpacing="10px" Width="80%">
                                <Border BorderStyle="None" />
                            </dx:ASPxCheckBoxList>
                            <asp:SqlDataSource ID="sdsCarBanCause" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT NCARBANCAUSEID,SCARBANCAUSENAME FROM TCARBANCAUSE Order BY NCARBANCAUSEID"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                      <td>สาเหตุอื่นๆ</td>
                      <td colspan="3"><dx:ASPxTextBox ID="txtOtherCause" runat="server" Width="500px"></dx:ASPxTextBox></td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            รายละเอียดเพิ่มเติม
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxMemo ID="txtRemark" runat="server" Height="150px" Width="500px">
                            </dx:ASPxMemo>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            สถานะ <font color="#ff0000">*</font>
                        </td>
                        <td align="left" colspan="3">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxCheckBox ID="chkUnBan" runat="server" CheckState="Unchecked" Text="ปล่อยรถ   "
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxCheckBox>
                                    </td>
                                    <td>
                                        <dx:ASPxDateEdit ID="dteUnBan" runat="server" CssClass="dxeLineBreakFix" SkinID="xdte">
                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                ValidationGroup="add">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxDateEdit>
                                    </td>
                                    <td>
                                        เวลา&nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxTimeEdit ID="teUnBan" runat="server" CssClass="dxeLineBreakFix" Width="60px">
                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                ValidationGroup="add">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                            </ValidationSettings>
                                        </dx:ASPxTimeEdit>
                                    </td>
                                    <td>
                                        น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'depo_terminal_remain_lst.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
