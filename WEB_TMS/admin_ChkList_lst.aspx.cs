﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_ChkList_lst : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            Session["oTypeCheckListID"] = null;

            LogUser("36", "R", "เปิดดูข้อมูล แบบฟอร์มตรวจสอบ", "");
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("STYPECHECKLISTID", "STYPECHECKLISTNAME")
                    .Cast<object[]>()
                    .Select(s => new { STYPECHECKLISTID = s[0].ToString(), STYPECHECKLISTNAME = s[1].ToString() });

                string delid = "";
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    foreach (var l in ld)
                    {
                        delid += l.STYPECHECKLISTID + ",";
                        string strsql2 = "DELETE FROM TCHECKLIST WHERE STYPECHECKLISTID = '" + l.STYPECHECKLISTID + "'";
                        using (OracleCommand com = new OracleCommand(strsql2, con))
                        {
                            com.ExecuteNonQuery();
                        }

                        string strsql = "DELETE FROM TGROUPOFCHECKLIST WHERE STYPECHECKLISTID = '" + l.STYPECHECKLISTID + "'";
                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {
                            com.ExecuteNonQuery();
                        }

                        string strsql1 = "DELETE FROM TTYPEOFCHECKLIST WHERE STYPECHECKLISTID = '" + l.STYPECHECKLISTID + "'";
                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {
                            com1.ExecuteNonQuery();
                        }
                    }
                }

                LogUser("36", "D", "ลบข้อมูลหน้า แบบฟอร์มตรวจสอบ รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();
                gvw.DataBind();

                break;

            case "SetPageSize":
                int num;
                int PageSize = int.TryParse(paras[1] + "", out num) ? num : 0;
                gvw.SettingsPager.PageSize = PageSize;

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STYPECHECKLISTID");
                string stmID = Convert.ToString(data);

                Session["oTypeCheckListID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_ChkList_add.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_ChkList_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        int VisibleIndex = e.VisibleIndex;
        ASPxButton imbedit = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbedit");

        if (!CanWrite)
        {
            imbedit.Enabled = false;
        }
    }

}
