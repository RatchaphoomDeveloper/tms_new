﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxClasses;
using System.Data.Common;

public partial class ContractTruckManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Session["UserID"] = "40"; Session["CGROUP"] = "1";
    }
    protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (!gvw.IsEditing || e.Column.FieldName != "STRAILERID") return;
        if (e.KeyValue == DBNull.Value || e.KeyValue == null) return;
        object val = gvw.GetRowValuesByKeyValue(e.KeyValue, "STRUCKID");
        if (val == DBNull.Value) return;
        string country = (string)val;

        ASPxComboBox combo = e.Editor as ASPxComboBox;
        //FillCityCombo(combo, country);

        combo.Callback += new CallbackEventHandlerBase(cmbCity_OnCallback);
    }
    void cmbCity_OnCallback(object source, CallbackEventArgsBase e)
    {
        //FillCityCombo(source as ASPxComboBox, e.Parameter);
    }
    protected void cboHeadRegistfifo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        Literal ltrSVENDORID = (Literal)gvw.FindEditFormTemplateControl("ltrSVENDORID");

        sdsVehicle.SelectCommand = @"SELECT TTRUCK.STRUCKID    ,TTRUCK.SHEADREGISTERNO  
,TTRUCK.STRANSPORTID ,TTRUCK.CACTIVE ,NVL(TTRUCK.CHOLD,'0') CHOLD
,TRUNC(TTRUCK.DWATEREXPIRE ) DWATEREXPIRE 
,TTRUCKTYPE.SCARTYPENAME 
,CASE WHEN NVL(TTRUCK.CHOLD,'0')='0' THEN
CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 'N/A' ELSE 'SAP' END 
ELSE 'TMS' END MS_HOLD
FROM TTRUCK  
LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
WHERE TTRUCK.CACTIVE='Y' AND TTRUCK.SCARTYPEID IN ('0','3' )   
AND TTRUCK.SHEADREGISTERNO LIKE :fillter  
AND rownum >= :startIndex AND rownum <= :endIndex ";

        sdsVehicle.SelectParameters.Clear();
        sdsVehicle.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVehicle.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVehicle.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVehicle;
        comboBox.DataBind();


    }
    protected void cboHeadRegistfifo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string CallbackName = e.CallbackName, modes = "", KEY_ID = "";
        switch (e.CallbackName)
        {
            case "STARTEDIT":
                KEY_ID = e.Args[0] + "";
                break;
            case "CANCELEDIT":
                break;
        }
    }
    protected void SearchData(string _mode)
    {


        Cache.Remove(sdsContractTruck.CacheKeyDependency);
        Cache[sdsContractTruck.CacheKeyDependency] = new object();
        sdsContractTruck.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsContractTruck.DataBind();
        gvw.DataBind();

        //DataView dv = (DataView)sdsContract.Select(DataSourceSelectArguments.Empty);
        //DataTable dt = dv.ToTable();
        //lblRecord.Text = dt.Select("CCONFIRM<>'1'").Length + "";
        //dt.Dispose();

    }
    private void On_Click(Object source, EventArgs e)
    {
        sdsContractTruck.Update();
    }
    protected void sdsContractTruck_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsContractTruck_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e); 
    } 
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool isProcessSucceeded = (e.Exception == null);
        Session["FLAGIUCOMMAND"] = isProcessSucceeded;

        if (isProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void gvw_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        sdsContractTruck.UpdateCommandType = SqlDataSourceCommandType.StoredProcedure;
        sdsContractTruck.UpdateCommand = "IUTCONTRACT_TRUCK";
        sdsContractTruck.UpdateParameters.Clear();

        sdsContractTruck.UpdateParameters.Add("INP_SCONTRACTID", "" + e.NewValues["INP_SCONTRACTID"]);
        sdsContractTruck.UpdateParameters.Add("INP_STRUCKID", "" + e.NewValues["INP_STRUCKID"] );
        sdsContractTruck.UpdateParameters.Add("INP_STRAILERID", "" + e.NewValues["INP_STRAILERID"] );
        sdsContractTruck.UpdateParameters.Add("INP_CSTANDBY", "" + e.NewValues["INP_CSTANDBY"] );
        sdsContractTruck.UpdateParameters.Add("INP_CREJECT", "" + e.NewValues["INP_CREJECT"] );
        sdsContractTruck.UpdateParameters.Add("INP_DSTART", "" + e.NewValues["INP_DSTART"] );
        sdsContractTruck.UpdateParameters.Add("INP_DEND", "" + e.NewValues["INP_DEND"] );
        sdsContractTruck.UpdateParameters.Add("INP_DCREATE", "" + e.NewValues["INP_DCREATE"] );
        sdsContractTruck.UpdateParameters.Add("INP_SCREATE", "" + e.NewValues["INP_SCREATE"] );
        sdsContractTruck.UpdateParameters.Add("INP_DUPDATE", "" + e.NewValues["INP_DUPDATE"] );
        sdsContractTruck.UpdateParameters.Add("INP_SUPDATE", "" + e.NewValues["INP_SUPDATE"] );
        sdsContractTruck.UpdateParameters.Add("KEY_ID", "" + e.NewValues["KEY_ID"] );
        sdsContractTruck.Update();

        gvw.CancelEdit();
        e.Cancel = true;
    }
}