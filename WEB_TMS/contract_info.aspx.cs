﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;
using TMS_BLL.Master;


public partial class contract_info : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {

        #region Event
        //gvwContract.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwContract_CustomColumnDisplayText);
        //gvwContract.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContract_AfterPerformCallback);
        //gvwContract.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwContract_HtmlRowPrepared);
        //gvwContract.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwContract_HtmlDataCellPrepared);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {

            //if (!Permissions("50"))
            //{
            //    //ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='admin_home.aspx';<script>"); return;
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='Default.aspx';", true); return;
            //}
            if (Session["chkurl"] + "" == "1")
                btnAddContract.ClientEnabled = false;
            DrowDownList();
            this.AssignAuthen();

            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
        }
        else
        {
            cmbVendor.DataSource = ViewState["TVendorSap"];
            cmbVendor.DataBind();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAddContract.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DrowDownList()
    {
        ViewState["TVendorSap"] = VendorBLL.Instance.TVendorSapSelect();
        cmbVendor.DataSource = ViewState["TVendorSap"];
        cmbVendor.DataBind();
    }
    //GridView gvwContract
    protected void gvwContract_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwContract_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = (e.CallbackName.Equals("CANCELEDIT") || e.CallbackName.Equals("SORT")) ? e.CallbackName : e.Args[0].Split(';')[0].ToUpper();//switch CallbackName 
        switch (CallbackName)// (e.CallbackName.Equals("CANCELEDIT") || e.CallbackName.Equals("SORT"))
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                string[] eArgs = e.Args[0].Split(';');
                if (eArgs.Length < 2)
                    break;
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                //gvwContract.CancelEdit();
                gvwContract.DataSource = Session["DataContract"];
                gvwContract.DataBind();
                //SearchData("CANCELEDIT");
                break;
            case "STARTEDIT":
                //dynamic dy_data = gvwContract.GetRowValues(visibleindex, "SCONTRACTID", "SVENDORID");
                //string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + dy_data[0] + "&" + dy_data[1]))
                //    , sUrl = "contract_add.aspx?str=";
                //gvwContract.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;//Response.Redirect(sUrl + sEncrypt);

                break;
            case "SEARCH":
                ViewState["DataContract"] = ContractBLL.Instance.ContractListSelect(txtContractNo.Text, cmbVendor.Value + string.Empty,radActive.SelectedValue + string.Empty);
                gvwContract.DataSource = ViewState["DataContract"];
                gvwContract.DataBind();
               
                break;
            case "NEWRECORD":
                string sUrl_New = "contract_add.aspx?str="
                    + Server.UrlEncode(STCrypt.Encrypt("NEW&&"));
                //gvwContract.JSProperties["cpRedirectTo"] = sUrl_New;
                break;

            case "VIEW_HISTORY":
                string sUrl_History = "contract_history.aspx?str="
                    + Server.UrlEncode(STCrypt.Encrypt("HISTORY&&"));
                //gvwContract.JSProperties["cpRedirectTo"] = sUrl_History;
                break;
            case "VIEW":
                //dynamic vdy_data = gvwContract.GetRowValues(visibleindex, "SCONTRACTID", "SVENDORID");
                //string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + vdy_data[0] + "&" + vdy_data[1]))
                    //, vsUrl = "contract_add.aspx?str=";
                //gvwContract.JSProperties["cpRedirectTo"] = vsUrl + vsEncrypt;
                break;
        }
    }

    private bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton img = (ImageButton)sender;
        string[] ids = img.CommandArgument.Split('_');
        string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + ids[0] + "&" + ids[1]));
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "open", "window.open('contract_add.aspx?str=" + vsEncrypt +"','_blank');", true);
    }
    protected void gvwContract_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dv = (DataRowView)e.Row.DataItem;
            HyperLink hl = (HyperLink)e.Row.FindControl("hlView");
            if (hl != null)
            {
                string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dv["SCONTRACTID"] + "&" + dv["SVENDORID"]));
                hl.NavigateUrl = "contract_add.aspx?str=" + vsEncrypt;
                hl.Text = dv["SCONTRACTNO"] + string.Empty;
            }
        }
    }
    protected void gvwContract_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvwContract.PageIndex = e.NewPageIndex;
        GridViewHelper.BindGridView(ref gvwContract, (DataTable)ViewState["DataContract"]);
    }
    protected void btnAddContract_Click(object sender, EventArgs e)
    {
        string sUrl_New = "contract_add.aspx?str="
                    + Server.UrlEncode(STCrypt.Encrypt("NEW&&"));
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "open", "window.open('" + sUrl_New + "','_blank');", true);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string SpotStatus = "%";
        if (radSpot.SelectedIndex > -1)
        {
            if (radSpot.SelectedIndex == 0)
                SpotStatus = "N";
            else if (radSpot.SelectedIndex == 1)
                SpotStatus = "Y";
        }

        ViewState["DataContract"] = ContractBLL.Instance.ContractListSelect2(txtContractNo.Text, cmbVendor.Value + string.Empty,radActive.SelectedValue + string.Empty, SpotStatus);
        gvwContract.DataSource = ViewState["DataContract"];
        gvwContract.DataBind();
    }
}