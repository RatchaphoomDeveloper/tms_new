﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;

public partial class fifo_User_regis_lst : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler

        #endregion
        if (!IsPostBack)
        {
            //bool chkurl = false;
            //if (Session["cPermission"] != null)
            //{
            //    string[] url = (Session["cPermission"] + "").Split('|');
            //    string[] chkpermision;
            //    bool sbreak = false;

            //    foreach (string inurl in url)
            //    {
            //        chkpermision = inurl.Split(';');
            //        if (chkpermision[0] == "18")
            //        {
            //            switch (chkpermision[1])
            //            {
            //                case "0":
            //                    chkurl = false;

            //                    break;
            //                case "1":
            //                    chkurl = true;

            //                    break;

            //                case "2":
            //                    chkurl = true;

            //                    break;
            //            }
            //            sbreak = true;
            //        }

            //        if (sbreak == true) break;
            //    }
            //}

            //if (chkurl == false)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}

            string str = Request.QueryString["str"];
            string uid = Request.QueryString["uid"];
            txtTerminal.Text = str + "";
            txtUserID.Text = uid + "";
            Session["SVDID"] = null;
            Session["UserID"] = null;


            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            //LogUser("18", "R", "เปิดดูข้อมูลหน้า ลงคิวเข้ารับงาน โดย พขร.", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

        sds.SelectCommand = @"SELECT F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,
CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(nvl(Tt.NTOTALCAPACITY,0), SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(nvl(T.NTOTALCAPACITY,0), SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
 
 CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT COUNT(ttc.NCOMPARTNO) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE COUNT(TC1.NCOMPARTNO) END AS NCOMPARTNO, 


VP.SVENDORNAME,F.SEMPLOYEENAME,F.STEL  
FROM ((TFIFO F LEFT JOIN TTRUCK T ON F.SHEADREGISTERNO = T.SHEADREGISTERNO ) LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) TC1 ON T.STRUCKID = TC1.STRUCKID ) 
LEFT JOIN (TVENDOR V INNER JOIN TVENDOR_SAP VP ON V.SVENDORID = VP.SVENDORID) ON F.SVENDORID = V.SVENDORID WHERE NVL(F.CACTIVE,'1') = '1' AND nvl(F.CPLAN,'0') != '1'  AND F.STERMINAL = :oTerminal AND to_char(DDATE,'dd/MM/yyyy') = to_char(SYSDATE,'dd/MM/yyyy') GROUP BY F.NID,F.NNO,F.DDATE,F.SHEADREGISTERNO,F.STRAILERREGISTERNO,VP.SVENDORNAME,F.SEMPLOYEENAME,F.STEL,t.NTOTALCAPACITY ORDER BY  F.NNO ,F.NID";

        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oTerminal", txtTerminal.Text);
        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;
            case "AddPage":
                string uid = Request.QueryString["UserID"];
                xcpn.JSProperties["cpRedirectTo"] = "fifo_User_regis_add.aspx?str=" + txtTerminal.Text + "&uid=" + txtUserID.Text;
                break;
        }
    }

    //private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    //{
    //    UserTrace trace = new UserTrace(this, sql);
    //    trace.SUID = Session["UserID"] + "";
    //    trace.SMENUID = MENUID;
    //    trace.SCREATE = Session["UserID"] + "";
    //    trace.STYPE = TYPE;
    //    trace.SDESCRIPTION = DESCEIPTION;
    //    trace.SREFERENTID = REFERENTID;
    //    trace.Insert();
    //}
}
