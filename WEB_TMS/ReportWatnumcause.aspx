﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportWatnumcause.aspx.cs" Inherits="ReportWatnumcause" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">
                                                    <dx:ASPxComboBox ID="cboVendor" runat="server" Width="80%" ClientInstanceName="cboVendor"
                                                        TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="True"
                                                        OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" ValueField="SVENDORID" AutoPostBack="false">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                    </asp:SqlDataSource>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="2%">
                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text=" - " CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท ระหว่างวันที่"
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text=""
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="13%"></td>
                                                <td width="37%">
                                                    <%--<dx:ASPxButton ID="btnDowload" runat="server" Text="Download" ClientInstanceName="btnDowload"
                                                        AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('PDF');}" />
                                                    </dx:ASPxButton>--%>
                                                    <dx:ASPxButton runat="server" ID="btnDowload" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer"  CssClass="dxeLineBreakFix" Text="PDF">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('PDF');}" />
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                </td>
                                                <td width="37%"> <dx:ASPxButton runat="server" ID="btnDowload2" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer"  CssClass="dxeLineBreakFix" Text="Excel">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('xls');}" />
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" KeyFieldName="REQUEST_ID">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="SNO">
                                                    <DataItemTemplate>
                                                        <%#Container.ItemIndex + 1 %>.
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนหัว" Width="9%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="VEH_NO">
                                                    <%--          <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblVehNO">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนท้าย" Width="9%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="TU_NO">
                                                    <%--   <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblTuNo">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ข่นส่ง" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    FieldName="SABBREVIATION" GroupIndex="0">
                                                    <%--   <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblVendor">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันตรวจ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="SERVICE_DATE">
                                                    <%-- <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblDateWatnum">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สาเหตุ" Width="35%" HeaderStyle-HorizontalAlign="Center"
                                                    FieldName="CAUSE_NAME">
                                                    <%--   <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblCause">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ความคิดเห็นผู้ตรวจ" Width="35%" HeaderStyle-HorizontalAlign="Center"
                                                    FieldName="DESCRIPTION">
                                                    <%-- <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblDescription">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>--%>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <Settings ShowGroupPanel="false" />
                                            <GroupSummary>
                                                <%--<dx:ASPxSummaryItem FieldName="SNO" SummaryType="None" />--%>
                                                <dx:ASPxSummaryItem FieldName="SABBREVIATION" SummaryType="Count" DisplayFormat="จำนวน {0} รายการ" />
                                            </GroupSummary>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="display: none">
                                        <dx:ReportViewer ID="rvw" runat="server" ClientInstanceName="rvw">
                                        </dx:ReportViewer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
