﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MPTMS.master" AutoEventWireup="true"
    CodeFile="bypassmail2system.aspx.cs" Inherits="bypassmail2system" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style>
        .divsplogin001
        {
            background-image: url("images/bypass/bglog.png");
            padding: 12px 0 0 210px;
        }
        .Hvtica14w
        {
            color: #FFFFFF;
            font-family: Verdana,Geneva,sans-serif;
            font-size: 14px;
            text-align: left;
        }
        .Hvtica14center
        {
            color: #FFFFFF;
            font-family: Verdana,Geneva,sans-serif;
            font-size: 16px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div id="dv_login" runat="server" style="text-align: center;">
        <table width="98%" cellspacing="2" cellpadding="0" border="0" align="center">
            <tbody>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table width="500" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                    <td valign="top" class="bgloginadmin001">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td class="divsplogin001">
                                                        <table width="92%" cellspacing="3" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="Hvtica14center">
                                                                        <b>เข้าสู่ระบบ</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Hvtica14w">
                                                                        User Name</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="Hvtica14w">
                                                                        Password</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" ></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="imbLogIn" runat="server" ImageUrl="~/images/bypass/login.png"
                                                                            OnClick="imbLogIn_Click" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:23px;">
                                                                        &nbsp;</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="dv_checkstep" runat="server" style="display: none; text-align: center;">
        <asp:Literal ID="ltrProgress" runat="server"></asp:Literal>
        <asp:ImageButton ID="imbTryAgain" runat="server" ImageUrl="~/Images/btnLoop.gif"
            OnClick="imbTryAgain_Click" />
    </div>
    <div id="dv_Progress">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
