﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ContractTied.aspx.cs" Inherits="ContractTied" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">สัญญา</label>
        <div class="col-md-4">
            <asp:TextBox Text="" ID="txtSCONTRACTNO" runat="server" CssClass="form-control" />
            <asp:HiddenField runat="server" ID="hidSCONTRACTID" />
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">กลุ่มงาน</label>
        <div class="col-md-4">
            <asp:DropDownList runat="server" ID="ddlWordGroup" OnSelectedIndexChanged="ddlWordGroup_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                    </asp:DropDownList>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">กลุ่มที่</label>
        <div class="col-md-4">
            <asp:DropDownList runat="server" ID="ddlGroups" CssClass="form-control">
                    </asp:DropDownList>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">Vendor</label>
        <div class="col-md-4"><dx:ASPxComboBox ID="cmbVendor" runat="server" CssClass="form-control" ClientInstanceName="cmbVendor" Width="100%"
                        SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID">
                        <Columns>
                            <dx:ListBoxColumn Width="20%" Caption="รหัสบริษัท" FieldName="SVENDORID" />
                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" />
                        </Columns>
                    </dx:ASPxComboBox></div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <center>
                        <input id="btnSave" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />&nbsp;&nbsp;&nbsp;
                <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-info" />
            </center>
        </div>
        <div class="col-md-4"></div>
    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmBack_ClickOK" TextTitle="ยืนยันการย้อนกลับ" TextDetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

