﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;

public partial class ConfirmTruckConclude : PageBase
{
    DataTable dt;
    DataSet ds;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        lblCSTANBY.Text = "จำนวนรถหมุนเวียน {0} คัน";
        lblCCONFIRM.Text = "จำนวนรถที่ยืนยัน {0} คัน";
        if (!IsPostBack)
        {
            //if (!(Permissions("97") || Permissions("7")))
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}
            if (Session["CGROUP"] + string.Empty == "0")
            {
                //Response.Redirect("~/default.aspx");
                //return;
            }
            SetDrowDownList();
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            lblCSTANBY.Text = string.Format(lblCSTANBY.Text, "0");
            lblCCONFIRM.Text = string.Format(lblCCONFIRM.Text, "0");
            //Session["SVDID"]
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //Response.Redirect("NotAuthorize.aspx");  
                 btnSearch.Enabled = false;
                 btnExport.Enabled = false;
                 btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                 btnExport.CssClass = "btn btn-md bth-hover btn-info";
                 //gvData.Columns[11].Visible = false;
            }
            if (!CanWrite)
            {
                // gvw.Columns[0].Visible = false;
                //gvData.Columns[10].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlGroup.DataTextField = "NAME";
        ddlGroup.DataValueField = "ID";
        ddlGroup.DataSource = WorkGroupBLL.Instance.GroupSelect(0, "%", string.Empty);
        ddlGroup.DataBind();
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlGroup.SelectedIndex = 0;
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlContract.SelectedIndex = 0;

        ddlTTERMINAL.DataTextField = "SABBREVIATIOn";
        ddlTTERMINAL.DataValueField = "STERMINALID";
        ddlTTERMINAL.DataSource = ContractBLL.Instance.TTERMINALSelect();
        ddlTTERMINAL.DataBind();
        ddlTTERMINAL.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = ""
        });
        ddlTTERMINAL.SelectedIndex = 0;
        if (Session["CGROUP"] + string.Empty == "2")
        {
            ddlTTERMINAL.SelectedValue = Session["SVDID"] + string.Empty;
        }
    }

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", false);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", false);
            }
            ddlContract.Items.Insert(0, new ListItem()
            {
                Text = "--เลือก--",
                Value = ""
            });
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtDate.Text.Trim()))
            {
                alertFail("กรุณาเลือกวันที่ยืนยันรถ !!!");
                return;
            }
            GetData();
            gvData.DataSource = ds.Tables[0];
            gvData.DataBind();
            gvTerminal.DataSource = ds.Tables[1];
            gvTerminal.DataBind();
            lblItem.Text = ds.Tables[0].Rows.Count + string.Empty;
            lblItemTerminal.Text = ds.Tables[1].Rows.Count + string.Empty;

            DataStatus();
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetData
    private void GetData()
    {
        ds = ConfirmTruckBLL.Instance.PTTConfirmTruckSelect(txtDate.Text.Trim(), ddlTTERMINAL.SelectedValue, ddlContract.SelectedValue, ddlVendor.SelectedValue, cbCarRecycling.Checked ? "N" : "Y", ddlGroup.SelectedValue, rblStatus.SelectedValue);

        long CSTANBY = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => long.Parse(it["CSTANBY"] + string.Empty));
        long CCONFIRM = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => long.Parse(it["CCONFIRM"] + string.Empty));
        lblCSTANBY.Text = string.Format(lblCSTANBY.Text, CSTANBY);
        lblCCONFIRM.Text = string.Format(lblCCONFIRM.Text, CCONFIRM);
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        rblStatus.ClearSelection();
        ddlGroup.SelectedIndex = 0;
        ddlTTERMINAL.SelectedIndex = 0;
        cbCarRecycling.Checked = false;
        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            DataTable dtStatus = DataStatus();
            dt = ds.Tables[0];
            dt.Columns.Remove("SCONTRACTID");
            dt.Columns.Remove("NCONFIRMID");
            dt.Columns.Remove("CSTANBY");
            dt.Columns.Remove("STATUS_ID");
            dt.Columns.Remove("CSTANBY_NOT");
            foreach (DataRow item in dt.Rows)
            {
                item["PTT_LOCK"] = item["PTT_LOCK"] + string.Empty == "1" ? "LOCK" : "";
            }
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/ConfirmTruckConcludeFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruckConclude"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            ds.Tables[1].Columns.Remove("STERMINALID");
            ds.Tables[1].Columns.Remove("CSTANBY_N");
            ExcelWorksheet worksheet2 = workbook.Worksheets["ConfirmTruckTerminal"];
            worksheet2.InsertDataTable(ds.Tables[1], new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            dtStatus.Columns.Remove("CSTANBY_N");

            ExcelWorksheet worksheet3 = workbook.Worksheets["ConfirmTruckStatus"];
            worksheet3.InsertDataTable(dtStatus, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });
            string Path = this.CheckPath();
            string FileName = "ConfirmTruckConclude_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);

            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion

    #region gvData_PageIndexChanging
    protected void gvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvData.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion

    #region gvData_RowDataBound
    protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (drv["STATUS_ID"] + string.Empty != "1")
            {
                e.Row.ForeColor = Color.Red;
                foreach (TableCell item in e.Row.Cells)
                {
                    item.ForeColor = Color.Red;
                }
            }

            HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
            //SCONTRACTID & กลุ่มงาน & บริษัท & เลขที่สัญญา & คลัง & รถหมุนเวียน & วันที่
            string strQuery = drv["SCONTRACTID"] + "&" + drv["GROUPNAME"] + "&" + drv["SABBREVIATION"] + "&" + drv["SCONTRACTNO"] + "&" + ddlTTERMINAL.SelectedValue + "&" + (cbCarRecycling.Checked ? "N" : "Y") + "&" + txtDate.Text.Trim() + "&" + drv["SVENDORID"];
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(strQuery);
            string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            aView.HRef = "~/ConfirmTruckDetail.aspx?str=" + encryptedValue;
            CheckBox cbLock = (CheckBox)e.Row.FindControl("cbLock");
            
            if (cbLock != null)
            {
                if (drv["PTT_LOCK"] + string.Empty == "1")
                {

                    cbLock.Checked = true;
                }
                if (Session["CGROUP"] + string.Empty == "2")
                {
                    ListItem listitem = ddlTTERMINAL.Items.FindByValue(Session["SVDID"] + string.Empty);
                    if (listitem != null)
                    {
                        if ((drv["TTERMINALNAME"] + string.Empty).IndexOf(listitem.Text) < 0)
                        {
                            cbLock.Enabled = false;
                        }
                    }

                }
                else
                {
                    cbLock.Enabled = true;
                }
            }

            if (!CanRead)
            {               
                aView.HRef = "javascript:void(0);";
                aView.Style.Add("cursor", "not-allowed");
            }
            if (!CanWrite)
            {
                cbLock.Enabled = false;  
            }
        }
    }
    #endregion

    #region cbLock_CheckedChanged
    protected void cbLock_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox chk = (CheckBox)sender;
            GridViewRow gvr = (GridViewRow)chk.NamingContainer;
            if (gvr != null)
            {
                object NCONFIRMID = gvData.DataKeys[gvr.RowIndex].Value;
                if (ConfirmTruckBLL.Instance.PTTConfirmTruckLockUpdate(NCONFIRMID + string.Empty, Session["UserID"] + string.Empty, chk.Checked ? "1" : "0"))
                {
                    if (chk.Checked)
                    {
                        alertSuccess("ไม่อนุญาต ผขส.แก้ไขข้อมูลยืนยันรถในสัญญา(สำเร็จ)");
                    }
                    else
                    {
                        alertSuccess("อนุญาต ผขส.แก้ไขข้อมูลยืนยันรถในสัญญา(สำเร็จ)");
                    }
                }
                else
                {
                    if (chk.Checked)
                    {
                        alertFail("ไม่อนุญาต ผขส.แก้ไขข้อมูลยืนยันรถในสัญญา(ไม่สำเร็จ)");
                    }
                    else
                    {
                        alertFail("อนุญาต ผขส.แก้ไขข้อมูลยืนยันรถในสัญญา(ไม่สำเร็จ)");
                    }

                }

            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion


    #region DataStatus
    private DataTable DataStatus()
    {
        DataTable dtData = new DataTable();
        dtData.Columns.Add("STATUS_NAME", typeof(string));
        dtData.Columns.Add("CCONFIRM", typeof(string));
        dtData.Columns.Add("CSTANBY_N", typeof(string));
        dtData.Columns.Add("CSTANBY", typeof(string));
        dtData.Columns.Add("DCREATE", typeof(string));
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            DataRow dr = dtData.NewRow();
            dr["STATUS_NAME"] = "ยืนยัน";
            Decimal CCONFIRM = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => it.Field<Decimal>("CCONFIRM"));
            Decimal CSTANBY = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => it.Field<Decimal>("CSTANBY"));
            string DCREATE = ds.Tables[0].Rows[0].IsNull("DUPDATE") ? "" : ds.Tables[0].Rows[0].Field<DateTime>("DUPDATE").ToString("dd/MM/yyyy HH:mm:ss");
            dr["CCONFIRM"] = (CCONFIRM - CSTANBY) + string.Empty;
            //dr["CSTANBY_N"] = CSTANBY_N + string.Empty;
            dr["CSTANBY"] = CSTANBY + string.Empty;
            dr["DCREATE"] = DCREATE + string.Empty;
            dtData.Rows.Add(dr);


            dr = dtData.NewRow();
            dr["STATUS_NAME"] = "ปฏิเสธ";
            CCONFIRM = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => it.Field<Decimal>("CCONFIRM_NOT"));
            CSTANBY = ds.Tables[0].AsEnumerable().Where(it => it["STATUS_ID"] + string.Empty == "1").Sum(it => it.Field<Decimal>("CSTANBY_NOT"));
            dr["CCONFIRM"] = (CCONFIRM - CSTANBY) + string.Empty;
            //dr["CSTANBY_N"] = CSTANBY_N + string.Empty;
            dr["CSTANBY"] = CSTANBY + string.Empty;
            dr["DCREATE"] = DCREATE + string.Empty;
            dtData.Rows.Add(dr);
            gvStatus.DataSource = dtData;
            gvStatus.DataBind();
            lblItemStatus.Text = dtData.Rows.Count + string.Empty;
        }
        return dtData;
    }
    #endregion
}