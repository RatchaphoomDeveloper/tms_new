﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="QuarterResult.aspx.cs" Inherits="QuarterResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <br />
    
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหา
                </div>
                <div class="panel-body">
                    <div class="row">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                        <div class="form-group">
                            <div class="col-md-4 text-right">
                                ปี :

                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">&nbsp;</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-md-4 text-right">
                                ไตรมาส :

                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList ID="ddlQuerter" runat="server" CssClass="form-control" >
                                    <asp:ListItem Text="ทั้งหมด" Value="0" />
                                    <asp:ListItem Text="ไตรมาส 1" Value="1" />
                                    <asp:ListItem Text="ไตรมาส 2" Value="2" />
                                    <asp:ListItem Text="ไตรมาส 3" Value="3" />
                                    <asp:ListItem Text="ไตรมาส 4" Value="4" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                    <div class="col-md-4 text-right">
                        ผู้ขนส่ง :

                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control">
                            
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6"></div>
                </div> 
            <div class="clearfix"></div>
                        <div class="form-group">
                    <div class="col-md-4 text-right">
                        สถานะเอกสาร :

                    </div>
                    <div class="col-md-4">
                        <asp:CheckBoxList ID="cblStatus" RepeatDirection="Horizontal" runat="server">
                            <asp:ListItem Text="แจ้งผู้ขนส่ง" Value="1" Selected="True" />
                            <asp:ListItem Text="ยกเลิกเอกสาร" Value="0" Selected="True" />
                        </asp:CheckBoxList>
                    </div>
                    <div class="col-md-6"></div>
                </div>
                        <div class="clearfix"></div>
                        <p>&nbsp;</p>
                        </ContentTemplate>
    </asp:UpdatePanel>
                        <div class="form-group">
                            <div class="col-md-4 text-right">
          

                    </div>
                    <div class="col-md-4">
                                <asp:Button Text="ค้นหา" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-default" runat="server" />
                                <asp:Button Text="Export Excel" ID="btnExportExcel" CssClass="btn btn-default" runat="server" OnClick="btnExportExcel_Click" />
                                <%--<asp:Button Text="ย้อนกลับ" CssClass="btn btn-default" PostBackUrl="~/Quartery_Report.aspx" ID="btnBack" runat="server" />--%>
                           
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        
    <p>&nbsp;</p>
    <dx:ASPxGridView ID="gvDownload" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" ClientInstanceName="gvDownload" KeyFieldName="ROWNUMS"
         OnAfterPerformCallback="gvDownload_AfterPerformCallback" >
        <ClientSideEvents RowClick="function (s,e) { gvDownload.StartEditRow(e.visibleIndex); } ">

        </ClientSideEvents>
        <Columns>
            <dx:GridViewDataTextColumn Caption="ที่" >
                <DataItemTemplate>
                    <%# Container.VisibleIndex + 1  %>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ชื่อเอกสาร" FieldName="DOCNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="เลขที่เอกสาร" FieldName="DOCNO">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ไตรมาส" FieldName="QUARTER">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="แจ้งผล ผขส.">
                <HeaderStyle HorizontalAlign="Center" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ผู้ดำเนินการ" FieldName="USERNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Date Time" FieldName="UPDATE_DATETIME">
                <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="VIEWRESULT">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataColumn Caption="Document">
                <HeaderStyle HorizontalAlign="Center" />
                <DataItemTemplate>
                    <asp:Button Text='Download' CssClass="btn btn-default" ID="btnDownload" runat="server" OnClick="btnDownload_Click" CommandArgument='<%# Container.VisibleIndex  %>' />
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="SVENDORID" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="YEARS" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="QUARTER" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="LOGVERSION" Visible="false">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Templates>
            <EditForm>
                <dx:ASPxGridView ID="gvUser" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" KeyFieldName="QUARTER">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ที่" >
                <DataItemTemplate>
                    <%# Container.VisibleIndex + 1  %>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ชื่อ" FieldName="SFIRSTNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="นามสกุล" FieldName="SLASTNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="USER" FieldName="SUSERNAME">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Date Time" FieldName="CREATE_DATETIME">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="" FieldName="SVENDORID" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="YEARS" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="quarter" Visible="false">
            </dx:GridViewDataTextColumn>
        </Columns>

    </dx:ASPxGridView>
            </EditForm>
        </Templates>
    </dx:ASPxGridView>

    <asp:HiddenField ID="hidID" runat="server" />
    <asp:HiddenField ID="hidSVENDORID" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

