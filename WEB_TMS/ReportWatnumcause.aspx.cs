﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraReports.UI;
using System.Globalization;

public partial class ReportWatnumcause : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);


        ListData();
        //CreateReport();
    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.ToString() == "ที่")
        {
            //int sss =   gvw.GroupSummarySortInfo.IndexOf;  
            //e.Cell.Text = e.VisibleIndex + "";

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');
        string SUSERID = Session["UserID"] + "";
        string VENID = CommonFunction.ReplaceInjection(cboVendor.Value + "");

        switch (paras[0])
        {
            case "search":
                ListData();
                break;
        }
    }

    void ListData()
    {
        string Codition = "";
        if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
        {
            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Codition += " AND (TRUNC(SDEC.EXAM_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;
        }

        if (!string.IsNullOrEmpty(cboVendor.Value + ""))
        {
            Codition += " AND TREQ.VENDOR_ID = '" + CommonFunction.ReplaceInjection(cboVendor.Value + "") + "'";
            lblsTail.Text = " - ";
        }

        string Query = @"SELECT  ROW_NUMBER() OVER (PARTITION BY TVEN.SABBREVIATION  ORDER BY TREQ.SERVICE_DATE)||'.' as sNO
, TREQ.REQUEST_ID
,TREQ.VENDOR_ID
,TVEN.SABBREVIATION
,TREQ.VEH_No
,TREQ.TU_No
,TRUNC(SDEC.EXAM_DATE) as SERVICE_DATE
,TREQ.Status_Flag
 ,TSTATUS.STATUSREQ_NAME
 ,TCAUSETYPE.CAUSE_NAME 
 ,TCAUSETYPE.CAUSE_ID
 ,NVL(SDEC.DESCRIPTION,'xxx') as DESCRIPTION
 ,TVEN.SABBREVIATION||' จำนวน '|| GC.NCOUNT||' รายการ' as STEXT
 ,SDEC.NVERSION
FROM TBL_Request TREQ
LEFT JOIN 
(
    SELECT SVENDORID,SABBREVIATION FROM  TVENDOR
) TVEN
ON TVEN.SVENDORID = TREQ.VENDOR_ID
LEFT JOIN TBL_CAUSE TCAUSETYPE
ON TCAUSETYPE.CAUSE_ID = TREQ.CAUSE_ID
LEFT JOIN TBL_STATUSREQ TSTATUS
ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
LEFT JOIN (SELECT VENDOR_ID , COUNT(VENDOR_ID) as NCOUNT FROM TBL_Request GROUP BY VENDOR_ID) GC
ON GC.VENDOR_ID = TREQ.VENDOR_ID 
LEFT JOIN
(
    SELECT  SCL.REQUEST_ID, SCL.NVERSION,  SCL.STATUS_COMFIRM, MAX(SCL.EXAM_DATE) as EXAM_DATE, SCL.DESCRIPTION
    FROM TBL_SCRUTINEERINGLIST  SCL
    WHERE STATUS_COMFIRM = 'N'
    GROUP BY SCL.REQUEST_ID, SCL.NVERSION,  SCL.STATUS_COMFIRM, SCL.DESCRIPTION
    UNION
    SELECT TIC.REQUEST_ID,  TIC.NVERSION, TIC.CCHECKING_WATER, MAX(TIC.EXAMDATE) as EXAM_DATE, TCC.SCOMMENT  as DESCRIPTION
    FROM TBL_TIME_INNER_CHECKINGS TIC
    LEFT JOIN TBL_CHECKING_COMMENT TCC
    ON TIC.REQUEST_ID = TCC.REQUEST_ID AND TIC.NVERSION = TCC.NVERSION
    WHERE TIC.CCHECKING_WATER = 'N'
    GROUP BY TIC.REQUEST_ID,  TIC.NVERSION, TIC.CCHECKING_WATER,TCC.SCOMMENT
)SDEC
ON TREQ.REQUEST_ID = SDEC.REQUEST_ID
WHERE NVL(SDEC.DESCRIPTION,'xxx') <> 'xxx' " + Codition + @"
ORDER BY TREQ.VENDOR_ID,TVEN.SABBREVIATION , TREQ.SERVICE_DATE, ROW_NUMBER() OVER (PARTITION BY TVEN.SABBREVIATION  ORDER BY TREQ.SERVICE_DATE) ";

        DataTable dt = CommonFunction.Get_Data(conn, Query);

        gvw.DataSource = dt;
        gvw.DataBind();
        CreateReport(dt);
        gvw.ExpandAll();
        //  gvw.GroupSummarySortInfo.Add(,
        //ColumnSortOrder.Ascending, firstGroupingColumn);

    }

    void CreateReport(DataTable dt)
    {

        //        string Query = @"SELECT  ROWNUM as sNO, TREQ.REQUEST_ID,TREQ.VENDOR_ID,TVEN.SABBREVIATION,TREQ.VEH_No,TREQ.TU_No,TRUNC(TREQ.SERVICE_DATE) as SERVICE_DATE
        //                        ,TREQ.Status_Flag ,TSTATUS.STATUSREQ_NAME,TCAUSETYPE.CAUSE_NAME ,TCAUSETYPE.CAUSE_ID, 'xxx' as DESCRIPTION,TVEN.SABBREVIATION||' จำนวน '|| GC.NCOUNT||' รายการ' as STEXT
        //                        FROM TBL_Request TREQ
        //                        LEFT JOIN 
        //                        (
        //                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR
        //                        ) TVEN
        //                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
        //                        LEFT JOIN TBL_CAUSE TCAUSETYPE
        //                        ON TCAUSETYPE.CAUSE_ID = TREQ.CAUSE_ID
        //                        LEFT JOIN TBL_STATUSREQ TSTATUS
        //                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
        //                        LEFT JOIN (SELECT VENDOR_ID , COUNT(VENDOR_ID) as NCOUNT FROM TBL_Request GROUP BY VENDOR_ID) GC
        //                        ON GC.VENDOR_ID = TREQ.VENDOR_ID";

        // DataTable dt = CommonFunction.Get_Data(conn, Query);

        //gvw.DataSource = dt;
        //gvw.DataBind();
        if (dt.Rows.Count > 0)
        {
            //1
            ReportWatnum report = new ReportWatnum();
            report.Name = "รายงานรถวัดน้ำไม่ผ่านแจ้งรข.(แยกบริษัท)";

            if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
            {
                ((XRLabel)report.FindControl("XRLabel3", true)).Visible = true;
                ((XRLabel)report.FindControl("XRLabel3", true)).Text = "รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท ระหว่างวันที่ " + edtStart.Text + " - " + edtEnd.Text;
            }
            else
            {
                ((XRLabel)report.FindControl("XRLabel3", true)).Text = "รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท ระหว่างวันที่ -";
            }

            report.DataSource = dt;
            rvw.Report = report;
            rvw.DataBind();
            //report.CreateDocument();
        }

    }

    #region SetAutoComplete

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    #endregion
}