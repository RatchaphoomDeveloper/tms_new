﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Text;
using System.Web.Security;

public partial class vendor_request : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        ListData();
        this.AssignAuthen();
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.ToString() == "")
        {

            ASPxButton btn = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnSaveother") as ASPxButton;
            dynamic data = gvw.GetRowValues(e.VisibleIndex, "STATUS");
            if (data == "0")
            {
                btn.Text = "ยื่นคำร้อง";
            }
            else if (data == "3")
            {
                btn.Text = "เพิ่มเติมข้อมูล";
            }
            else if (data == "2" || data == "4" || data == "5")
            {
                btn.Visible = false;
            }
            else
            {
                btn.Text = "ดูรายละเอียด";
            }

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {


    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            int Idx = 0;
            if (param.Length > 1)
            {
                Idx = int.Parse(param[1] + "");
            }

            switch (param[0])
            {
                case "approve":
                    dynamic data = gvw.GetRowValues(Idx, "TYPE", "REF_ID", "REQ_ID", "STATUS");
                    string TYPE = data[0] + "";
                    switch (TYPE)
                    {
                        //ผุ้ขนส่ง
                        case "V":
                            //Session["Backpage"] = VendorID;
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_edit_p4.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1])) + "&strID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[2])) + "";
                            break;
                        //พขร.
                        case "E":
                            byte[] plaintextBytes = Encoding.UTF8.GetBytes(data[1] + string.Empty);
                            string P4 = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                            plaintextBytes = Encoding.UTF8.GetBytes(data[2] + string.Empty);
                            string strID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                            plaintextBytes = Encoding.UTF8.GetBytes(data[3] + string.Empty);
                            string status = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                            xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?P4=" + P4 + "&strID=" + strID + "&status=" + status;
                            break;
                        //สัญญา
                        case "C":

                            //int inxT2 = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
                            dynamic dataT2 = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + dataT2[0] + "&" + dataT2[1])), vsUrl = "contract_add_p4.aspx?str=";

                            string SCONTRACTID = dataT2 + "";
                            xcpn.JSProperties["cpRedirectTo"] = vsUrl + vsEncrypt + "&strID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[2])) + "";
                            break;
                        //รถ
                        case "T":

                            dynamic dy_data = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            DataTable dt = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + dy_data[0] + "' AND STRANSPORTID LIKE '%" + dy_data[1] + "%'");
                            string STRUCKID = "";
                            string STRAILERID = "";
                            string SCARTYPEID = "";

                            if (dt.Rows.Count > 0)
                            {
                                STRUCKID = dt.Rows[0]["STRUCKID"] + "";
                                STRAILERID = dt.Rows[0]["STRAILERID"] + "";
                                SCARTYPEID = dt.Rows[0]["SCARTYPEID"] + "";
                            }
                            else
                            {
                                DataTable dt_TEMP = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK_TEMP WHERE STRUCKID = '" + dy_data[0] + "' AND STRANSPORTID LIKE '%" + dy_data[1] + "%'");
                                if (dt_TEMP.Rows.Count > 0)
                                {
                                    STRUCKID = dt_TEMP.Rows[0]["STRUCKID"] + "";
                                    STRAILERID = dt_TEMP.Rows[0]["STRAILERID"] + "";
                                    SCARTYPEID = dt_TEMP.Rows[0]["SCARTYPEID"] + "";
                                }
                            }
                            if (string.Equals(data[3], "1") || string.Equals(data[3], "2"))
                            {
                                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID)
                                    , sUrl = "Truck_History_View.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                            }
                            else
                            {
                                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID) + "&STRAILERID=" + ConfigValue.GetEncodeText(STRAILERID) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID)
                                    , sUrl = "truck_edit.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                            }
                            break;
                        //การผูกรถ
                        case "H":
                            dynamic dy_data_H = gvw.GetRowValues(Idx, "REF_ID", "SVENDORID");
                            DataTable dt_H = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + dy_data_H[0] + "' AND STRANSPORTID LIKE '%" + dy_data_H[1] + "%'");
                            string STRUCKID_H = "";
                            string STRAILERID_H = "";
                            string SCARTYPEID_H = "";

                            if (dt_H.Rows.Count > 0)
                            {
                                STRUCKID_H = dt_H.Rows[0]["STRUCKID"] + "";
                                STRAILERID_H = dt_H.Rows[0]["STRAILERID"] + "";
                                SCARTYPEID_H = dt_H.Rows[0]["SCARTYPEID"] + "";
                            }
                            else
                            {
                                DataTable dt_TEMP = CommonFunction.Get_Data(conn, "SELECT STRUCKID,STRAILERID,SCARTYPEID FROM TTRUCK_TEMP WHERE STRUCKID = '" + dy_data_H[0] + "' AND STRANSPORTID LIKE '%" + dy_data_H[1] + "%'");
                                if (dt_TEMP.Rows.Count > 0)
                                {
                                    STRUCKID_H = dt_TEMP.Rows[0]["STRUCKID"] + "";
                                    STRAILERID_H = dt_TEMP.Rows[0]["STRAILERID"] + "";
                                    SCARTYPEID_H = dt_TEMP.Rows[0]["SCARTYPEID"] + "";
                                }
                            }
                            if (string.Equals(data[3], "1") || string.Equals(data[3], "2"))
                            {
                                string sEncrypt_H = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID_H) +"&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID_H)
                                    , HUrl = "Vehicle_detail.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = HUrl + sEncrypt_H;
                            }
                            else
                            {
                                string sEncrypt_H = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STRUCKID_H) + "&CarTypeID=" + ConfigValue.GetEncodeText(SCARTYPEID_H)
                                    , HUrl = "Vehicle_Edit.aspx?str=";
                                xcpn.JSProperties["cpRedirectTo"] = HUrl + sEncrypt_H;
                            }
                            break;                            
                    }
                    break;
                case "request": xcpn.JSProperties["cpRedirectTo"] = "Vendor_Detail.aspx";
                    break;
            }

        }
    }

    void ListData()
    {
        string Condition = "";
        string VENID = SystemFunction.GET_VENDORID(Session["UserID"] + ""); ;
        if (!string.IsNullOrEmpty(VENID))
        {
            Condition += " AND VEN.SVENDORID = '" + VENID + "'";
        }

        if (!string.IsNullOrEmpty(rblStatus.Value + ""))
        {
            Condition += " AND TDC.STATUS = '" + rblStatus.Value + "'";
        }

        if (!string.IsNullOrEmpty(rblDatastatus.Value + ""))
        {
            Condition += " AND TDC.TYPE = '" + rblDatastatus.Value + "'";
        }
        if (!string.IsNullOrEmpty(txtSearch.Text.ToString().Trim()))
        {
            Condition += " AND TDC.DESCRIPTION LIKE '%" + txtSearch.Text.ToString().Trim() + "%'";
        }

        string QUERY = @"SELECT TDC.REQ_ID, TDC.REF_ID
                        ,CASE 
                        WHEN TDC.TYPE = 'V' THEN 'ข้อมูลผู้ขนส่ง' 
                        WHEN TDC.TYPE = 'E' THEN 'ข้อมูล พขร.' 
                        WHEN TDC.TYPE = 'C' THEN 'ข้อมูลสัญญา' 
                        WHEN TDC.TYPE = 'T' THEN 'Truck Component(TC)'
                        WHEN TDC.TYPE = 'H' THEN 'ข้อมูลรถ'
                        ELSE '' END as STYPE
                        ,TDC.DESCRIPTION, TDC.STATUS, TDC.DCREATE, TDC.SCREATE, TDC.DUPDATE, TDC.SUPDATE
                        ,VEN.SABBREVIATION,TDC.TYPE,TDC.SVENDORID,
                        CASE 
                        WHEN TDC.STATUS = '0' THEN 'ผขส. ขอเปลี่ยนข้อมูล' 
                        WHEN TDC.STATUS = '1' THEN 'รข. อนุมัติ'
                        WHEN TDC.STATUS = '2' THEN 'รข. ปฏิเสธ' 
                        WHEN TDC.STATUS = '3' THEN 'รข. ขอข้อมูลเพิ่มเติม' 
                        ELSE '' END as STATUSNAME
                        FROM TREQ_DATACHANGE TDC
                        LEFT JOIN TVENDOR VEN
                        ON TDC.SVENDORID = VEN.SVENDORID
                        WHERE 1=1 " + Condition + @"
                        ORDER BY NVL(TDC.DUPDATE,TDC.DCREATE) DESC";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        gvw.DataSource = dt;
        gvw.DataBind();
    }
}