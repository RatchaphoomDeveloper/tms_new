﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Admin_VisitForm_Group : PageBase
{
    #region " Prop "

    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }
    protected PAGE_ACTION PageAction
    {
        get { return (PAGE_ACTION)ViewState[this.ToString() + "PageAction"]; }
        set { ViewState[this.ToString() + "PageAction"] = value; }
    } 
    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }

    #endregion ""
    protected void Page_Load(object sender, EventArgs e)
    {
          this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            if (Session["EditNGROUPID"] != null)
            {
                PageAction = PAGE_ACTION.EDIT;
                QDocID = Session["EditNGROUPID"].ToString();
                Session["EditNGROUPID"] = null;
            }
            else if (Session["ViewNGROUPID"] != null)
            {
                PageAction = PAGE_ACTION.VIEW;
                QDocID = Session["ViewNGROUPID"].ToString();
                Session["ViewNGROUPID"] = null;
            }
            else
            {
                PageAction = PAGE_ACTION.ADD;
                QDocID = string.Empty;
            }
            LoadMain();
            InitForm();
        }
    }
    public void InitForm()
    {
        string _err = string.Empty;

    }

    public void LoadMain()
    {
        string _err = string.Empty;
        if (string.IsNullOrEmpty(QDocID))
        {

        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {

    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
}