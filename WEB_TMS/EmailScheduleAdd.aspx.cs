﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using TMS_BLL.Master;


public partial class EmailScheduleAdd : PageBase
{
    #region + View State +
    private int ScheduleID
    {
        get
        {
            if ((int)ViewState["ScheduleID"] != null)
                return (int)ViewState["ScheduleID"];
            else
                return 0;
        }
        set
        {
            ViewState["ScheduleID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            ScheduleID = 0;
            this.LoadEmailType();

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["SCHEDULE_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);

                ScheduleID = int.Parse(decryptedValue);
                this.LoadData();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadEmailType()
    {
        try
        {
            DataTable dtEmailType = EmailTemplateBLL.Instance.EmailTypeSelectBLL(" AND M_EMAIL_TYPE.ISACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlEmailType, dtEmailType, "EMAIL_TYPE_ID", "EMAIL_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadData()
    {
        try
        {
            DataTable dtSchedule = EmailTemplateBLL.Instance.EmailScheduleSelectAllBLL(" AND SCHEDULE_ID = " + ScheduleID.ToString());
            if (dtSchedule.Rows.Count > 0)
            {
                ddlEmailType.SelectedValue = dtSchedule.Rows[0]["EMAIL_TYPE_ID"].ToString();

                DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectAllBLL(" AND M_EMAIL_TYPE.EMAIL_TYPE_ID = " + ddlEmailType.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlTemplate, dtTemplate, "TEMPLATE_ID", "TEMPLATE_NAME", true);

                ddlTemplate.SelectedValue = dtSchedule.Rows[0]["TEMPLATE_ID"].ToString();
                txtNo.Text = dtSchedule.Rows[0]["SCHEDULE_NO"].ToString();
                txtDay.Text = dtSchedule.Rows[0]["SCHEDULE_DAY"].ToString();
                radStatus.SelectedValue = dtSchedule.Rows[0]["ISACTIVE_VALUE"].ToString();

                ddlEmailType.Enabled = false;
                ddlTemplate.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            EmailTemplateBLL.Instance.EmailScheduleUpdateBLL(ScheduleID, int.Parse(ddlTemplate.SelectedValue), int.Parse(txtNo.Text.Trim()), int.Parse(txtDay.Text.Trim()), int.Parse(radStatus.SelectedValue), int.Parse(Session["UserID"].ToString()));

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "EmailSchedule.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlTemplate.SelectedIndex == 0)
                throw new Exception("กรุณา เลือกข้อมูล Template Name");

            if (string.Equals(txtNo.Text.Trim(), string.Empty))
                throw new Exception("กรูณา ป้อนข้อมูล ครั้งที่");

            if (string.Equals(txtDay.Text.Trim(), string.Empty))
                throw new Exception("กรูณา ป้อนข้อมูล แจ้งเตือนล่วงหน้า (วัน)");

            int tmp;
            if (!int.TryParse(txtNo.Text.Trim(), out tmp))
                throw new Exception("กรูณา ป้อนข้อมูล ครั้งที่ เป็นตัวเลขเท่านั้น");

            if (!int.TryParse(txtDay.Text.Trim(), out tmp))
                throw new Exception("กรูณา ป้อนข้อมูล แจ้งเตือนล่วงหน้า (วัน) เป็นตัวเลขเท่านั้น");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void ddlEmailType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectAllBLL(" AND M_EMAIL_TYPE.EMAIL_TYPE_ID = " + ddlEmailType.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlTemplate, dtTemplate, "TEMPLATE_ID", "TEMPLATE_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}