﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.KM;
using System.Linq;
using System.Drawing;
using System.IO;

public partial class KMView : PageBase
{
    #region + View State +
    private string Type
    {
        get
        {
            if ((string)ViewState["Type"] != null)
                return (string)ViewState["Type"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Type"] = value;
        }
    }

    private string KM
    {
        get
        {
            if ((string)ViewState["KM"] != null)
                return (string)ViewState["KM"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["KM"] = value;
        }
    }
    private string KMComment
    {
        get
        {
            if ((string)ViewState["KMComment"] != null)
                return (string)ViewState["KMComment"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["KMComment"] = value;
        }
    }

    private string TopicID
    {
        get
        {
            if ((string)ViewState["TopicID"] != null)
                return (string)ViewState["TopicID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    private DataTable dtComment
    {
        get
        {
            if ((DataTable)ViewState["dtComment"] != null)
                return (DataTable)ViewState["dtComment"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtComment"] = value;
        }
    }

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtKMView
    {
        get
        {
            if ((DataTable)ViewState["dtKMView"] != null)
                return (DataTable)ViewState["dtKMView"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dtKMView"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.CheckQueryString();
            this.GetData();
            this.GetDataLikeViewKM();
            this.GetDataCommentKM();
            this.GetDataBookKM();
            this.GetDataLikeKM();
            this.Visible();
            this.AssignAuthen();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            //if (!CanRead)
            //{
            //    cmdSearch.Enabled = false;
            //    cmdSearchBook.Enabled = false;
            //    ImageButton2.Enabled = false;
            //    cmdSearchBook.Style.Add("cursor", "not-allowed");
            //    ImageButton2.Style.Add("cursor", "not-allowed");
            //}
            if (!CanWrite)
            {
                cmdPostComment.Enabled = false;
                cmdEdit.Enabled = false;
                cmdDelete.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetDataBookKM()
    {
        try
        {
            DataTable dtBook = KMBLL.Instance.SelectKMBookBLL(" AND KM_ID = " + KM + " AND USER_ID = " + int.Parse(Session["UserID"].ToString()));
            if (dtBook.Rows.Count > 0)
            {
                if (!string.Equals(dtBook.Rows[0]["KM_ID"].ToString(), KM))
                {
                    imgBookYes.Visible = false;
                    imgBookNo.Visible = true;
                }
                else
                {
                    imgBookNo.Visible = false;
                    imgBookYes.Visible = true;
                }
            }
            else
            {
                imgBookNo.Visible = true;
                imgBookYes.Visible = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void GetDataLikeKM()
    {
        try
        {
            DataTable dtlike = KMBLL.Instance.SelectKMViewBLL(" AND KM_ID = " + KM + " AND USER_ID = " + int.Parse(Session["UserID"].ToString()));
            if (dtlike.Rows.Count > 0)
            {
                if (!string.Equals(dtlike.Rows[0]["KM_ID"].ToString(), KM))
                {
                    imgLikeYes.Visible = false;
                    imgLikeNo.Visible = true;
                }
                else
                {
                    imgLikeNo.Visible = false;
                    imgLikeYes.Visible = true;
                }
            }
            else
            {
                imgLikeNo.Visible = true;
                imgLikeYes.Visible = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void GetDataLikeViewKM()
    {
        try
        {
            DataTable dtcount = KMBLL.Instance.SelectLikeViewKMBLL(" AND KM_ID = " + KM);
            if (dtcount.Rows.Count > 0)
                lblView.Text = dtcount.Rows[0]["VIEW_TOTAL"].ToString() + " views : " + dtcount.Rows[0]["LIKES"].ToString() + " Likes";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void GetDataCommentKM()
    {
        try
        {
            dtComment = KMBLL.Instance.SelectKMCommentBLL(" AND KM_ID = " + KM);
            dgvKMView.DataSource = dtComment;
            dgvKMView.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void Visible()
    {
        if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            tbComment.Visible = false;
        else
            tbComment.Visible = true;

        if (!string.Equals(Session["UserID"].ToString(), hidCreate.Value))
        {
            cmdEdit.Visible = false;
            cmdDelete.Visible = false;
        }
    }

    private void CheckQueryString()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                Type = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["type"], MachineKeyProtection.All));
                TopicID = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All));
            }
            else
                Response.Redirect("../Other/NotAuthorize.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetData()
    {
        if (string.Equals(Type, Mode.View.ToString()))
        {
            try
            {
                dt = KMBLL.Instance.SelectKMBLL(" AND TOPIC_ID = '" + TopicID + "' ");
                if (dt.Rows.Count > 0)
                {
                    KM = dt.Rows[0]["KM_ID"].ToString();
                    txtTopicName.Text = dt.Rows[0]["TOPIC_NAME"].ToString();
                    txtTopicID.Text = TopicID;
                    txtAuthorName.Text = dt.Rows[0]["FULLNAME"].ToString();
                    txtDepartment.Text = dt.Rows[0]["DEPARTMENT_NAME"].ToString();
                    txtDivisioin.Text = dt.Rows[0]["DIVISION_NAME"].ToString();
                    txtCoP.Text = dt.Rows[0]["COP"].ToString();
                    txtKnowledgeType.Text = dt.Rows[0]["KNOWLEDGE_TYPE_NAME"].ToString();
                    txtProductTag.Text = dt.Rows[0]["TAGS"].ToString();
                    txtKeyword.Text = dt.Rows[0]["OTHER_KEYWORDS"].ToString();
                    txtExecutive.Text = dt.Rows[0]["EXECUTIVE"].ToString();
                    txtCreatedDate.Text = dt.Rows[0]["CREATE_DATE"].ToString();
                    txtUpdatedDate.Text = dt.Rows[0]["UPDATE_DATE"].ToString();
                    hidCreate.Value = dt.Rows[0]["CREATE_BY"].ToString();
                    dtUpload = KMBLL.Instance.SelectKMUploadBLL(" AND REF_STR = '" + TopicID + "' ");
                    if (dtUpload.Rows.Count > 0)
                    {
                        dgvItem.DataSource = dtUpload;
                        dgvItem.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }

    protected void cmdEdit_Click(object sender, EventArgs e)
    {
        try
        {
            TopicID = MachineKey.Encode(Encoding.UTF8.GetBytes(TopicID), MachineKeyProtection.All);
            Response.Redirect("KMAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Edit.ToString()), MachineKeyProtection.All) + "&id=" + TopicID);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("KM.aspx");
    }
    protected void mpConfirmDelete_ClickOK(object sender, EventArgs e)
    {
        try
        {
            DataTable dtDel = KMBLL.Instance.DeleteKMHeaderBLL(int.Parse(KM), int.Parse(Session["UserID"].ToString()), 2);
            if (dtDel.Rows.Count == 0)
                throw new Exception("เกิดข้อผิดพลาด");
            else
                alertSuccess("ลบข้อมูลสำเร็จ", "KM.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvKMView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "EditData"))
            {
                int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                KMComment = dgvKMView.DataKeys[Index].Value.ToString();
                //ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);

                txtCommentReset.Text = dtComment.Rows[Index]["COMMENT_DETAIL"].ToString();
                cmdDel.Visible = false;
                cmdReset.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowEvent').modal();</script>", false);

                updShowEvent.Update();
                //Response.Redirect("KMView.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + ID);
            }
            else if (string.Equals(e.CommandName, "Delete"))
            {
                int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                KMComment = dgvKMView.DataKeys[Index].Value.ToString();
                //ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);

                txtCommentReset.Text = dtComment.Rows[Index]["COMMENT_DETAIL"].ToString();
                txtCommentReset.Enabled = false;
                cmdReset.Visible = false;
                cmdDel.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowEvent').modal();</script>", false);

                updShowEvent.Update();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvKMView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvKMView.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvKMView, dt);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdPostComment_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtComment = KMBLL.Instance.InsertKMCommentBLL(0, int.Parse(KM), int.Parse(Session["UserID"].ToString()), 1, txtComment.Text);
            if (dtComment.Rows.Count == 0)
                throw new Exception("เกิดข้อผิดพลาด");
            else
            {
                this.GetdataAll();
            }
            txtComment.Text = string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtComment = KMBLL.Instance.InsertKMCommentBLL(int.Parse(KMComment), int.Parse(KM), int.Parse(Session["UserID"].ToString()), 1, txtCommentReset.Text);
            if (dtComment.Rows.Count == 0)
                throw new Exception("เกิดข้อผิดพลาด");
            else
            {
                this.GetdataAll();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    //private void DeleteComment()
    //{
    //    try
    //    {
    //        DataTable dtComment = KMBLL.Instance.InsertKMCommentBLL(int.Parse(KMComment), int.Parse(KM), int.Parse(Session["UserID"].ToString()), 0, txtCommentReset.Text);
    //        if (dtComment.Rows.Count == 0)
    //            throw new Exception("เกิดข้อผิดพลาด");
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}


    protected void dgvItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //ImageButton imgDelete;
                //imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                //imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    protected void dgvItem_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvItem.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string FileName = dgvItem.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        string FileNameSystem = dgvItem.DataKeys[e.RowIndex]["FILENAME_USER"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FileName), Path.GetFileName(FileNameSystem));
    }

    private void DownloadFile(string Path, string FileName, string FileNameSystem)
    {
        try
        {
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileNameSystem);
            Response.End();
        }
        catch (Exception ex)
        {
            throw new Exception("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "Download"))
            {
                int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                //string ID = dgvKMView.DataKeys[Index].Value.ToString();
                //ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);
                string FullPath = dgvItem.DataKeys[Index]["FULLPATH"].ToString();
                string FileName = dgvItem.DataKeys[Index]["FILENAME_USER"].ToString();
                string FileNameSystem = dgvItem.DataKeys[Index]["FILENAME_SYSTEM"].ToString();
                this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FileName), Path.GetFileName(FileNameSystem));
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowEvent').modal();</script>", false);
                //UpdatePanel1.Update();
                //Response.Redirect("KMView.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + ID);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void imgBookYes_Click(object sender, ImageClickEventArgs e)
    {
        DataTable dtBookMark = KMBLL.Instance.InsertKMBookmarkBLL(int.Parse(KM), int.Parse(Session["UserID"].ToString()), 0);
        if (dtBookMark.Rows.Count == 0)
            throw new Exception("เกิดข้อผิดพลาด");
        else
        {
            this.GetdataAll();
        }
    }
    protected void imgBookNo_Click(object sender, ImageClickEventArgs e)
    {
        DataTable dtBookMark = KMBLL.Instance.InsertKMBookmarkBLL(int.Parse(KM), int.Parse(Session["UserID"].ToString()), 1);
        if (dtBookMark.Rows.Count == 0)
            throw new Exception("เกิดข้อผิดพลาด");
        else
        {
            this.GetdataAll();
        }
    }
    protected void imgLikeYes_Click(object sender, ImageClickEventArgs e)
    {
        DataTable dtLike = KMBLL.Instance.InsertKMLikeBLL(int.Parse(KM), int.Parse(Session["UserID"].ToString()), 0);
        if (dtLike.Rows.Count == 0)
            throw new Exception("เกิดข้อผิดพลาด");
        else
        {
            this.GetdataAll();
        }
    }
    protected void imgLikeNo_Click(object sender, ImageClickEventArgs e)
    {
        DataTable dtLike = KMBLL.Instance.InsertKMLikeBLL(int.Parse(KM), int.Parse(Session["UserID"].ToString()), 1);
        if (dtLike.Rows.Count == 0)
            throw new Exception("เกิดข้อผิดพลาด");
        else
        {
            this.GetdataAll();
        }
    }

    private void GetdataAll()
    {
        this.GetData();
        this.GetDataLikeViewKM();
        this.GetDataCommentKM();
        this.GetDataBookKM();
        this.GetDataLikeKM();
    }
    protected void dgvKMView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            txtCommentReset.Text = dtComment.Rows[e.RowIndex]["COMMENT_DETAIL"].ToString();
            dtComment.Rows.RemoveAt(e.RowIndex);
            KMComment = dgvKMView.DataKeys[e.RowIndex].Value.ToString();
            //this.DeleteComment();
            GridViewHelper.BindGridView(ref dgvKMView, dtComment);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvKMView_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int index = e.Row.RowIndex;
            //int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
            LinkButton Delete;
            Delete = (LinkButton)e.Row.Cells[0].FindControl("LinkDelete");
            //Delete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูล Comment นี้\\nใช่หรือไม่ ?')==false){return false;}");
            if (dtComment.Rows[index]["CREATE_BY"].ToString() != Session["UserID"].ToString())
            {
                LinkButton Edit;
                Edit = (LinkButton)e.Row.Cells[0].FindControl("LinkEdit");

                Edit.Visible = false;
                Delete.Visible = false;
            }
        }
    }
    protected void cmdDel_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtComment = KMBLL.Instance.InsertKMCommentBLL(int.Parse(KMComment), int.Parse(KM), int.Parse(Session["UserID"].ToString()), 0, txtCommentReset.Text);
            if (dtComment.Rows.Count == 0)
                throw new Exception("เกิดข้อผิดพลาด");
            else
            {
                this.GetdataAll();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}