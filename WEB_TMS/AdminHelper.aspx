﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    StylesheetTheme="Aqua" CodeFile="AdminHelper.aspx.cs" Inherits="AdminHelper" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="cpl" ClientInstanceName="cpl" runat="server" ShowLoadingPanel="true"
        ShowLoadingPanelImage="true">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%">
                    <tr style="display: none">
                        <td>
                            ประเภทข้อมูล:
                            <dx:ASPxComboBox ID="cbbData" ClientInstanceName="cbbData" runat="server" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Text="ระบุข้อมูล" Value="" />
                                    <dx:ListEditItem Text="ข้อมูลพนักงาน" Value="Employee" />
                                    <dx:ListEditItem Text="ข้อมูลผู้ขนส่ง" Value="Vendor" />
                                    <dx:ListEditItem Text="ข้อมูลสัญญา" Value="Contract" />
                                    <dx:ListEditItem Text="ข้อมูลรถ" Value="TRUCK" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            ผู้ขนส่ง
                            <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="50" ClientInstanceName="cboVendor"
                                EnableCallbackMode="true" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID" Width="180px">
                                <ClientSideEvents ValueChanged="function (s, e) {txtVendorID.SetValue(cboVendor.GetValue());}" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="80px" />
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="300px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%">
                                <tr>
                                    <td style="width: 170px; text-align: right;">
                                        ข้อมูลพนักงาน/พขร.
                                    </td>
                                    <td style="width: 70%">
                                        <dx:ASPxUploadControl ID="uclGPSxls" ClientInstanceName="uclGPSxls" runat="server"
                                            Width="70%" NullText="Click here to browse files..." OnFileUploadComplete="uclGPSxls_FileUploadComplete">
                                            <ClientSideEvents FileUploadComplete="function(s, e) { txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED');txtDataType.SetValue(cbbData.GetValue()); xgvw.PerformCallback(s.name.substring(s.name.split('btnGPSconfirm')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]+'$EMPLOYEE'); } " />
                                            <ValidationSettings AllowedFileExtensions=".xls,.xlsx" ShowErrors="true" NotAllowedFileExtensionErrorText="กรุณาระบถไฟล์ ที่มีนามสกุล .xls หรือ .xlsx เท่านั้น!"
                                                MaxFileSize="4194304">
                                            </ValidationSettings>
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td style="width: 15%;">
                                        <dx:ASPxButton ID="btnGPSconfirm" runat="server" Text=" Import Employee " class="dxeLineBreakFix"
                                            AutoPostBack="false" CausesValidation="true" Width="120px">
                                            <ClientSideEvents Click="function(s,e){var msg=''; if(uclGPSxls.GetText()==''){msg+='<br>แนบไฟล์(Excel)';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}{uclGPSxls.Upload(); } }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        ข้อมูลผู้ขนส่ง
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="uclVender" ClientInstanceName="uclVender" runat="server"
                                            Width="70%" NullText="Click here to browse files..." OnFileUploadComplete="uclVender_FileUploadComplete">
                                            <ClientSideEvents FileUploadComplete="function(s, e) { txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED');txtDataType.SetValue(cbbData.GetValue()); xgvw.PerformCallback(s.name.substring(s.name.split('btnGPSconfirm')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]+'$VENDOR'); } " />
                                            <ValidationSettings AllowedFileExtensions=".xls,.xlsx" ShowErrors="true" NotAllowedFileExtensionErrorText="กรุณาระบุไฟล์ ที่มีนามสกุล .xls หรือ .xlsx เท่านั้น!"
                                                MaxFileSize="4194304">
                                            </ValidationSettings>
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btn" runat="server" Text=" Import Vendor " class="dxeLineBreakFix"
                                            AutoPostBack="false" CausesValidation="true" Width="120px">
                                            <ClientSideEvents Click="function(s,e){var msg=''; if(uclVender.GetText()==''){msg+='<br>แนบไฟล์(Excel)';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}{uclVender.Upload(); } }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        ข้อมูลสัญญา
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="uclContract" runat="server" ClientInstanceName="uclContract"
                                            NullText="Click here to browse files..." OnFileUploadComplete="uclContract_FileUploadComplete"
                                            Width="70%">
                                            <ValidationSettings AllowedFileExtensions=".xls,.xlsx" ShowErrors="true" NotAllowedFileExtensionErrorText="กรุณาระบุไฟล์ ที่มีนามสกุล .xls หรือ .xlsx เท่านั้น!"
                                                MaxFileSize="4194304">
                                            </ValidationSettings>
                                            <ClientSideEvents FileUploadComplete="function(s, e) { txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED');txtDataType.SetValue(cbbData.GetValue()); xgvw.PerformCallback(s.name.substring(s.name.split('btnGPSconfirm')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]+'$CONTRACT'); } " />
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnContract" runat="server" Text=" Import Contract " class="dxeLineBreakFix"
                                            AutoPostBack="false" CausesValidation="true" Width="120px">
                                            <ClientSideEvents Click="function(s,e){var msg=''; if(uclContract.GetText()==''){msg+='<br>แนบไฟล์(Excel)';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}{uclContract.Upload(); } }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        ข้อมูลรถ
                                    </td>
                                    <td>
                                        <dx:ASPxUploadControl ID="uclTruck" runat="server" ClientInstanceName="uclTruck"
                                            NullText="Click here to browse files..." OnFileUploadComplete="uclTruck_FileUploadComplete"
                                            Width="70%">
                                            <ValidationSettings AllowedFileExtensions=".xls,.xlsx" ShowErrors="true" NotAllowedFileExtensionErrorText="กรุณาระบุไฟล์ ที่มีนามสกุล .xls หรือ .xlsx เท่านั้น!"
                                                MaxFileSize="4194304">
                                            </ValidationSettings>
                                            <ClientSideEvents FileUploadComplete="function(s, e) { txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED');txtDataType.SetValue(cbbData.GetValue()); xgvw.PerformCallback(s.name.substring(s.name.split('btnGPSconfirm')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]+'$TRUCK'); } " />
                                        </dx:ASPxUploadControl>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnTruck" runat="server" Text=" Import Truck " class="dxeLineBreakFix"
                                            AutoPostBack="false" CausesValidation="true" Width="120px">
                                            <ClientSideEvents Click="function(s,e){ LoadingPanel.Show(); var msg=''; if(uclTruck.GetText()==''){msg+='<br>แนบไฟล์(Excel)';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}{uclTruck.Upload(); } }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                            <dx:ASPxTextBox runat="server" ID="txtVendorID" ClientInstanceName="txtVendorID"
                                ClientVisible="false" Width="10px" />
                            <dx:ASPxTextBox runat="server" ID="txtSystemFileName" ClientInstanceName="txtSystemFileName"
                                ClientVisible="false" Width="10px" />
                            <dx:ASPxTextBox runat="server" ID="txtOriginalFileName" ClientInstanceName="txtOriginalFileName"
                                ClientVisible="false" Width="10px" />
                            <dx:ASPxTextBox runat="server" ID="txtGPSMode" ClientInstanceName="txtGPSMode" ClientVisible="false"
                                Width="10px" />
                            <dx:ASPxTextBox runat="server" ID="txtDataType" ClientInstanceName="txtDataType"
                                ClientVisible="false" Width="10px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                        </td>
                    </tr>
                </table>
                <table width="100%" style="display: none">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="xgvw" ClientInstanceName="xgvw" runat="server" Width="99%" OnAfterPerformCallback="xgvw_AfterPerformCallback">
                                <ClientSideEvents BeginCallback="function(s,e){ LoadingPanel.Show();}" EndCallback="function(s,e){ LoadingPanel.Hide(); eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
    </dx:ASPxLoadingPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
