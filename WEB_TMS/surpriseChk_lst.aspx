﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="surpriseChk_lst.aspx.cs" Inherits="surpriseChk_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>

    <style type="text/css">
        .search {
            background: url(../Images/ic_search.gif) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .time {
            background: url(../Images/ico_time.png) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .auto-style1 {
            height: 21px;
        }

        #file-input {
            cursor: pointer;
            outline: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 0;
            height: 0;
            overflow: hidden;
            filter: alpha(opacity=0); /* IE < 9 */
            opacity: 0;
        }

        .input-label {
            cursor: pointer;
            position: relative;
            display: inline-block;
        }
    </style>

    <style type="text/css">
        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--  ViewStateMode="Disabled" EnableViewState="False"--%>
    <div class="panel panel-default" style="width: 98%; padding: 10px; margin: 10px">
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#SurpriceCheck" id="aSurpriceCheck" aria-controls="SurpriceCheck" role="tab" data-toggle="tab">Surprise Check</a></li>
                <li><a href="#CheckHistory" aria-controls="CheckHistory" role="tab" data-toggle="tab">ประวัติพบปัญหาสภาพรถ</a></li>
                <li class="" id="TabYear" runat="server"><a href="#YearCheck" aria-controls="YearCheck" role="tab" data-toggle="tab">นัดหมายการตรวจประจำปี</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                <div role="tabpanel" class="tab-pane active" id="SurpriceCheck">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="ทะเบียนหัว"></asp:Label>
                                        <asp:Label CssClass="control-label" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <div class="col-md-12 PaddingLeftRight0">
                                            <div class="col-md-8 PaddingLeftRight0">
                                                <asp:TextBox runat="server" ID="txtTruck" CssClass="form-control" ReadOnly="true" />
                                                <asp:HiddenField runat="server" ID="hidSTRUCKID" />
                                                <asp:HiddenField runat="server" ID="hidSCONTRACTID" />
                                                <asp:HiddenField runat="server" ID="hidSVENDORID" />
                                            </div>
                                            <div class="col-md-1 PaddingLeftRight0">
                                                <asp:Button ID="btnTruck" runat="server" Text="..." Width="30px"
                                                    OnClick="btnTruck_Click" />
                                            </div>
                                            <div class="col-md-1 PaddingLeftRight0">
                                                <asp:Button ID="btnTruckClear" runat="server" Text="X" Width="30px" OnClick="btnTruckClear_Click" />
                                            </div>
                                        </div>
                                    </td>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="ทะเบียนท้าย"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTrailerNumber" Height="35" ReadOnly="true" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="200px" placeholder=""></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="ชื่อผู้ประกอบการ"></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox ID="txtVendorName" Height="35" ReadOnly="true" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="200px" placeholder=""></asp:TextBox>
                                    </td>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtContactNo" Height="35" ReadOnly="true" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="200px" placeholder=""></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label CssClass="control-label" runat="server" Text="วันที่ทำ Surprise Check"></asp:Label>
                                        <asp:Label CssClass="control-label" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="dteChk" Height="35" runat="server" ClientIDMode="Static" CssClass="datetimepicker" Width="200" placeholder="วันที่"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="สถานที่ทำ Surprise Check"></asp:Label>
                                        <asp:Label CssClass="control-label" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <%--<div class="input-group" style="width:200px;">--%>
                                        <asp:TextBox ID="txtCheckPlace" Height="35" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="200" placeholder=""></asp:TextBox>
                                        <%--</div>--%>
                                    </td>
                                    <td width="25%"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="ผู้ทำการตรวจสอบ"></asp:Label>
                                        <asp:Label CssClass="control-label" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <%--<div class="input-group" style="width:200px;">
                                        <asp:TextBox ID="txtChecker" ReadOnly="true" runat="server" AutoPostBack="false" CssClass="form-control input-lg search" Font-Size="Small" Width="50%" placeholder=""></asp:TextBox>
                                    </div>--%>
                                        <%--<div class="input-group" style="width:200px;">
                                            <asp:TextBox ID="txtChecker" ReadOnly="true" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                            <span class="input-group-btn">
                                                <asp:ImageButton ID="btn2" CssClass="btn btn-default" height="20px" OnClick="btnSearch2_Click" runat="server" ImageUrl="../Images/ic_search.gif" />
                                            </span>
                                    </div> --%>
                                        <asp:DropDownList ID="ddlChecker" runat="server" Width="200px" CssClass="form-control" Font-Size="Small" Height="35">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="25%"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <asp:Label CssClass="control-label" runat="server" Text="ผู้ทำการบันทึก"></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <%--<div style="width:200px;">--%>
                                        <asp:TextBox ID="txtRegisBy" Height="35" ReadOnly="true" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="200" placeholder=""></asp:TextBox>
                                        <%--</div>  --%>
                                    </td>
                                    <td width="25%"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="25%">
                                        <table width="100%">
                                            <tr>
                                                <td class="auto-style1">
                                                    <asp:CheckBox ID="chkCheck" Visible="false" runat="server" Width="10" Height="10" />
                                                </td>
                                                <td class="auto-style1">
                                                    <asp:Label CssClass="control-label" Visible="false" runat="server" Text="ไม่ได้ทำการตรวจ"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="25%"></td>
                                    <td width="25%"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="25%"></td>
                                    <td width="25%"></td>
                                    <td colspan="2" align="center">

                                        <asp:Button ID="btnConfirm" runat="server" Text="บันทึกข้อมูล" CssClass="btn btn-info" Width="180" Enabled="False" OnClick="btnConfirm_Click1" /><%--//UseSubmitBehavior="false" --%>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" Text="ปิด" CssClass="btn btn-info" Width="180" OnClick="Button2_Click" />

                                        <%--&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="DeleteRecord" UseSubmitBehavior="false" />--%>

                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnConfirm" />
                            <asp:PostBackTrigger ControlID="btnCancel" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div role="tabpanel" class="tab-pane" id="CheckHistory">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td width="30%"></td>
                                    <td>
                                        <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="false" CssClass="form-control input-lg" Font-Size="Small" Width="250px" placeholder="ทะเบียนรถ,เลขที่สัญญา,ชื่อผู้ประกอบการขนส่ง" Height="33px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="dteStart" runat="server" ClientIDMode="Static" CssClass="datepicker" Width="250px" placeholder="วันที่ ที่อยู่ในช่วงสัญญา"></asp:TextBox>
                                        <%--<asp:TextBox ID="dteStart" runat="server" ClientIDMode="Static" CssClass="m-wrap span12 date form_datetime datetimepick" width="250px" placeholder="วันที่ ที่อยู่ในช่วงสัญญา" ></asp:TextBox>--%>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList ID="cboChecker" runat="server" Width="200px" CssClass="form-control" Font-Size="Small">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTermminal" runat="server" Width="200px" CssClass="form-control" Font-Size="Small">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Button CssClass="btn btn-info" BackColor="#169bd5" Text="ค้นหา" runat="server" ID="btnSearch" Width="180px" OnClick="btnSearch2_Click" Font-Size="Small" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" colspan="8">จำนวนรายการ&nbsp;<asp:Label ID="lblCarCount" runat="server" Text="0"></asp:Label>
                                        &nbsp;รายการ
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                        <div class="”table-responsive”">

                                            <asp:GridView ID="gvw" runat="server" CssClass="table table-primary"
                                                AutoGenerateColumns="False" Width="100%" KeyFieldName="ID1" AllowPaging="true"
                                                ShowFooter="false"
                                                CellPadding="4" ForeColor="Black" GridLines="Both"
                                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px"
                                                OnRowCommand="gvw_RowCommand" OnRowDataBound="gvw_RowDataBound"
                                                OnPageIndexChanging="gvw_PageIndexChanging" OnRowCreated="gvw_RowCreated">
                                                <Columns>
                                                    <asp:BoundField DataField="ID1" HeaderText="ที่" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:BoundField DataField="SVENDORNAME" HeaderText="ชื่อผู้ประกอบการ" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:BoundField DataField="SCONTRACTNO" HeaderText="สัญญา" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:BoundField DataField="DSURPRISECHECK" HeaderText="วันที่ CHECK" HeaderStyle-BackColor="#c6defb" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                                    <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="หัว" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:BoundField DataField="STRAILERREGISTERNO" HeaderText="ท้าย" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:BoundField DataField="SSURPRISECHECKBY" HeaderText="ผู้ทำการบันทึก" HeaderStyle-BackColor="#c6defb" />
                                                    <asp:ButtonField Text="แก้ไข" CommandName="select" />
                                                    <asp:BoundField DataField="SCONTRACTID" HeaderText="ContactID" HeaderStyle-CssClass="HeaderStyle" Visible="false" />
                                                </Columns>
                                                <FooterStyle BackColor="#CCCC99" />
                                                <PagerStyle HorizontalAlign="Center" CssClass="GridPager" />
                                                <%--<SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />--%>
                                                <HeaderStyle CssClass="HeaderStyle" HorizontalAlign="Center" />
                                                <AlternatingRowStyle BackColor="White" />
                                            </asp:GridView>



                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div role="tabpanel" class="tab-pane" id="YearCheck">
                    <div class="container" style="width: 100%">
                        <div class="container" style="width: 100%;">
                            <div class="form-horizontal" style="background-color: white">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i>
                                                <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ข้อมูลทั่วไป&#8711;</a>
                                                <input type="hidden" id="hiddencollapseFindContract" value=" " />
                                            </div>
                                            <div class="panel-collapse collapse in" id="collapseFindContract">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="จำนวนรถที่นัดหมาย" runat="server" />
                                                                <asp:Label ID="lbl1" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:TextBox ID="TotalCar" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="ชื่อผู้ประกอบการ" runat="server" />
                                                                <asp:Label ID="lbl2" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="เลขที่สัญญา" runat="server" />
                                                                <asp:Label ID="lbl3" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="วันที่นัดหมายตรวจประจำปี" runat="server" />
                                                                <asp:Label ID="lbl4" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:TextBox ID="AppointDate" runat="server" CssClass="form-control datetimepicker"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="สถานที่ตรวจนัดหมายประจำปี" runat="server" />
                                                                <asp:Label ID="lbl5" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:TextBox ID="AppointLocate" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="ผู้ทำการตรวจ" runat="server" />
                                                                <asp:Label ID="lbl6" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:DropDownList ID="ddlUserCheck" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                                <asp:Label Text="รายชื่อทีมผู้ทำการตรวจ" runat="server" />
                                                                <asp:Label ID="lbl7" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                                            </label>
                                                            <div class="col-md-8">
                                                                <asp:DropDownList ID="ddlTeamCheck" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label col-md-4">
                                                               <asp:Button ID="btnAddTeam" runat="server" OnClick="btnAddTeam_Click" Text="เพิ่ม" CssClass="btn btn-info" />
                                                            </label>
                                                            <div class="col-md-8">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-footer text-right" runat="server" id="divButton">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button ID="btnSaveY" runat="server" Text="บันทึกผล" OnClick="btnSaveY_Click" CssClass="btn btn-info" />
                                                        <asp:Button ID="btnClearY" runat="server" Text="เคลียร์" OnClick="btnClearY_Click" CssClass="btn btn-danger" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract5">รายชื่อทีมผู้ทำการตรวจ&#8711;</a>
                                                <input type="hidden" id="hiddencollapseFindContract5" value=" " />
                                            </div>
                                            <div class="panel-collapse collapse in" id="collapseFindContract5">
                                                <div class="panel-body">
                                                    <asp:GridView runat="server" DataKeyNames="SUID" ID="dgvTeamCheck" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvTeamCheck_PageIndexChanging"
                                                        OnRowUpdating="dgvTeamCheck_RowUpdating" OnRowDeleting="dgvTeamCheck_RowDeleting">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDel" runat="server" ImageUrl="~/Images/bin1.png" Width="23"
                                                                        Height="23" Style="cursor: pointer" CommandName="delete" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="SUID" HeaderText="SUID" Visible="false">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FULLNAME" HeaderText="ชื่อทีมที่จะทำการตรวจสอบ">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <PagerStyle CssClass="pagination-ys" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="text-align: right">
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                                                <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                                            </div>
                                            <div class="panel-collapse collapse in" id="collapseFindContract2">
                                                <div class="panel-body">
                                                    <asp:GridView runat="server" DataKeyNames="ID,IS_ACTIVE" ID="dgvSurpriseCheckYear" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="true"
                                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvSurpriseCheckYear_PageIndexChanging"
                                                        OnRowUpdating="dgvSurpriseCheckYear_RowUpdating">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Action">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/64/blue-23.png" Width="23"
                                                                        Height="23" Style="cursor: pointer" CommandName="update" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="SVENDORNAME" HeaderText="ชื่อผู้ประกอบการ">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTALCAR" HeaderText="จำนวนรถที่นัดหมาย">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="APPOINTDATE" HeaderText="วันที่นัดหมายตรวจประจำปี" DataFormatString="{0:dd/MM/yyyy  HH:mm}">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="APPOINTLOCATE" HeaderText="สถานที่ตรวจนัดหมายประจำปี">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FULLNAME" HeaderText="ผู้ทำการตรวจ">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TEAMCHECK" HeaderText="ทีมผู้ทำการตรวจ" Visible="false">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="STATUS" HeaderText="สถานะ">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <PagerStyle CssClass="pagination-ys" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <br />
                                            <br />
                                            <div class="panel-footer" style="text-align: right">
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--Bootstrap Modal Dialog--%>
    <!-- Confirm Modal-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="align-content: center; height: 400px">
        <div class="modal-dialog" style="height: 100px">
        </div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>บันทึก</h4>
                </div>
                <div class="modal-body"><b>ต้องการบันทึกข้อมูล</b> ใช่หรือไม่?</div>
                <div class="modal-footer">
                    <asp:Button runat="server" CssClass="btn btn-info" ID="btnConfirmSave" Text="บันทึก" OnClick="btnConfirmSave_Click"></asp:Button>
                    <asp:Button runat="server" CssClass="btn btn-info" ID="btnConfirmCancel" Text="ยกเลิก" OnClick="btnConfirmCancel_Click"></asp:Button>
                </div>
            </div>
        </div>
    </div>

    <!-- Confirm2 Modal-->
    <div class="modal fade" id="myModalConfirm2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="align-content: center; height: 400px">
        <div class="modal-dialog" style="height: 100px">
        </div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>ยืนยันการทำงาน</h4>
                </div>
                <div class="modal-body"><b>บันทึกข้อมูลเรียบร้อยแล้ว ท่านต้องการทำรายการตรวจสภาพรถ</b> ต่อใช่หรือไม่?</div>
                <div class="modal-footer">
                    <asp:Button runat="server" CssClass="btn btn-info" ID="Button1" Text="ใช่" OnClick="btnConfirmCheckTruck_Click"></asp:Button>
                    <asp:Button runat="server" CssClass="btn btn-info" ID="Button2" Text="ยกเลิก" OnClick="btnCancelCheckTruck_Click"></asp:Button>
                </div>
            </div>
        </div>
    </div>
    <!-- Alert Modal-->
    <div class="modal fade" id="myModalAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="align-content: center; height: 400px">
        <div class="modal-dialog" style="height: 100px">
        </div>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>บันทึก</h4>
                </div>
                <div class="modal-body">
                    <%--กรุณากรอกข้อมูลให้ครบถ้วน--%>
                    <b>
                        <asp:Label runat="server" ID="lblAlert" CssClass="label-info" Text="" /></b>
                </div>
                <div class="modal-footer">
                    <asp:Button runat="server" CssClass="btn btn-info" ID="btnClose" Text="ตกลง"></asp:Button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ShowTruckSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updTruckSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label3" runat="server" Text="ค้นหาทะเบียนรถ"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ทะเบียนรถ</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtTruckSearch" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnTruckSearch" runat="server" Text="ค้นหา" OnClick="btnTruckSearch_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvTruckSearch"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvTruckSearch_PageIndexChanging"
                                            DataKeyNames="SHEADREGISTERNO,STRAILERREGISTERNO,SCONTRACTNO,SABBREVIATION,GROUPNAME 
  ,STRUCKID,SCONTRACTID,SVENDORID,GROUPID,WORKGROUPID"
                                            OnRowUpdating="gvTruckSearch_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ทะเบียน(หัว)" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ทะเบียน(ห่าง)" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="บริษัท" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="กลุ่มที่" DataField="GROUPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseTERMINAL" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $('#aSurpriceCheck').tab('show');
            }, 100);

        });
    </script>
</asp:Content>


