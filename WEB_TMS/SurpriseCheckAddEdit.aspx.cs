﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.SurpriseCheck;

public partial class SurpriseCheckAddEdit : PageBase
{
    #region ViewState
    private DataTable dtCheckTruck
    {
        get { return (DataTable)ViewState["dtCheckTruck"]; }
        set { ViewState["dtCheckTruck"] = value; }
    }
    private DataTable dtCheckList
    {
        get { return (DataTable)ViewState["dtCheckList"]; }
        set { ViewState["dtCheckList"] = value; }
    }
    private DataTable dtFileStep1
    {
        get { return (DataTable)ViewState["dtFileStep1"]; }
        set { ViewState["dtFileStep1"] = value; }
    }
    private DataTable dtFileStep2
    {
        get { return (DataTable)ViewState["dtFileStep2"]; }
        set { ViewState["dtFileStep2"] = value; }
    }
    private DataTable dtFileStep1History
    {
        get { return (DataTable)ViewState["dtFileStep1History"]; }
        set { ViewState["dtFileStep1History"] = value; }
    }
    private DataTable dtFileStep2History
    {
        get { return (DataTable)ViewState["dtFileStep2History"]; }
        set { ViewState["dtFileStep2History"] = value; }
    }
    private DataTable dtFileStep3
    {
        get { return (DataTable)ViewState["dtFileStep3"]; }
        set { ViewState["dtFileStep3"] = value; }
    }
    private DataTable dtFileOther
    {
        get { return (DataTable)ViewState["dtFileOther"]; }
        set { ViewState["dtFileOther"] = value; }
    }
    #endregion
    string TempDirectory = "UploadFile/checktruck/{0}/{2}/{1}/";
    string NPLANID = "3";
    string messSendEnail = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string id = "0";
            //SetDrowDownList();
            hidCactive.Value = "0";
            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                string[] strQuerys = strQuery.Split('&');

                SetData(strQuerys);

            }
            this.AssignAuthen();
        }
    }


    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region SetData
    private void SetData(string[] strQuerys)
    {

        hidSCONTRACTID.Value = strQuerys[0];
        hidSTRUCKID.Value = strQuerys[1];
        txtSHEADREGISTERNO.Text = strQuerys[2];
        txtSTRAILERREGISTERNO.Text = strQuerys[3];
        txtCheckerDate.Text = strQuerys[4];
        hidNSURPRISECHECKID.Value = strQuerys[5];
        txtSCONTRACTNO.Text = strQuerys[6];
        hidSVENDORNAME.Value = strQuerys[7];
        hidSVENDORID.Value = strQuerys[8];
        if (strQuerys.Count() > 9)
        {
            hidYear.Value = strQuerys[9];
            hidYearID.Value = strQuerys[10];
        }
        //CheckListSelect ต้องมี ข้อมูล
        dtCheckList = SurpriseCheckBLL.Instance.CheckListSelect();
        string modehis = "0";
        if (Request.QueryString["modehis"] != null)
        {
            string mode = Request.QueryString["modehis"];
            modehis = mode;
        }
        dtCheckTruck = SurpriseCheckBLL.Instance.CheckTruckSelect(hidSTRUCKID.Value, hidYear.Value, modehis);
        DataTable dtFileStep1Sum = SurpriseCheckBLL.Instance.CheckTruckFileSelect(hidSTRUCKID.Value, "1", hidYear.Value);
        DataTable dtFileStep2Sum = SurpriseCheckBLL.Instance.CheckTruckFileSelect(hidSTRUCKID.Value, "2", hidYear.Value);
        dtFileOther = SurpriseCheckBLL.Instance.CheckTruckFileOtherSelect(hidSTRUCKID.Value, hidSCONTRACTID.Value, hidYear.Value);
        if (dtCheckTruck.Rows.Count > 0)
        {
            dtCheckTruck.DefaultView.Sort = "DCHECK DESC";
            if (!string.IsNullOrEmpty(dtCheckTruck.Rows[0]["DCHECK"] + string.Empty))
            {
                txtCheckerDate.Text = Convert.ToDateTime(dtCheckTruck.Rows[0]["DCHECK"]).ToString("dd/MM/yyyy HH:mm");
            }
        }
        DataRow[] drs = dtFileStep1Sum.Select("CCHECKED <> 1");
        if (drs.Any())
        {
            dtFileStep1 = drs.CopyToDataTable();
        }
        else
        {
            dtFileStep1 = dtFileStep1Sum.Clone();
        }
        drs = dtFileStep1Sum.Select("CCHECKED = 1");
        if (drs.Any())
        {
            dtFileStep1History = drs.CopyToDataTable();
        }
        else
        {
            dtFileStep1History = dtFileStep1Sum.Clone();
        }

        drs = dtFileStep2Sum.Select("CCHECKED <> 1");
        if (drs.Any())
        {
            dtFileStep2 = drs.CopyToDataTable();
        }
        else
        {
            dtFileStep2 = dtFileStep2Sum.Clone();
        }
        drs = dtFileStep2Sum.Select("CCHECKED = 1");
        if (drs.Any())
        {
            dtFileStep2History = drs.CopyToDataTable();
        }
        else
        {
            dtFileStep2History = dtFileStep2Sum.Clone();
        }

        drs = dtFileStep1.Select("SCHECKID > -1");
        if (drs.Any())
        {
            hidSCHECKID.Value = drs[0]["SCHECKID"] + string.Empty;

        }
        drs = dtCheckTruck.Select("SCHECKLISTID = 0");
        if (drs.Any())
        {
            cbCOTHER.Checked = drs[0]["COTHER"] + string.Empty == "1" ? true : false;
            txtSOTHERREMARK.Text = drs[0]["SOTHERREMARK"] + string.Empty;
            txtSREMARK.Text = drs[0]["SREMARK"] + string.Empty;
        }
        gvOther.DataSource = dtFileOther;
        gvOther.DataBind();
        gvCheckList.DataSource = dtCheckList.Select("SGROUPID = 1").CopyToDataTable();
        gvCheckList.DataBind();
        gvCheckListEmp.DataSource = dtCheckList.Select("SGROUPID = 2").CopyToDataTable();
        gvCheckListEmp.DataBind();
        drs = dtCheckTruck.Select("SCHECKLISTID > 0 AND CCHECKED = 1");
        if (drs.Any())
        {
            gvHistory.DataSource = drs.CopyToDataTable();
        }
        else
        {
            gvHistory.DataSource = dtCheckTruck.Clone();
        }

        gvHistory.DataBind();

    }
    #endregion

    #region Step1
    protected void btnAddStep1_Click(object sender, EventArgs e)
    {
        try
        {

            if (fileUploadStep1.HasFile && fileUploadStep1.PostedFile.ContentType.Contains("image/") && !string.IsNullOrEmpty(txtDetailStep1.Text.Trim()))
            {
                string[] fileExt = fileUploadStep1.FileName.Split('.');
                int ncol = fileExt.Length - 1;
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                string SFILEPATH = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "");
                UploadFile2Server(fileUploadStep1, genName, SFILEPATH);

                DataRow dr = dtFileStep1.NewRow();
                //dr["SCHECKID"] = hidSCHECKID.Value;
                dr["NID"] = 0;
                dr["SCHECKLISTID"] = !string.IsNullOrEmpty(hidSCHECKLISTID.Value) ? int.Parse(hidSCHECKLISTID.Value) : (object)DBNull.Value;
                dr["SVERSIONLIST"] = !string.IsNullOrEmpty(hidSVERSIONLIST.Value) ? int.Parse(hidSVERSIONLIST.Value) : (object)DBNull.Value;
                dr["STYPECHECKLISTID"] = !string.IsNullOrEmpty(hidSTYPECHECKLISTID.Value) ? int.Parse(hidSTYPECHECKLISTID.Value) : (object)DBNull.Value;
                dr["SPROCESS"] = "1";
                dr["NNO"] = "1";
                dr["SEVIDENCENAME"] = txtDetailStep1.Text.Trim();
                dr["SFILENAME"] = fileUploadStep1.FileName;
                dr["SGENFILENAME"] = genName + "." + fileExt[ncol].Trim();
                dr["SFILEPATH"] = SFILEPATH + genName + "." + fileExt[ncol].Trim();
                dr["SCREATE"] = Session["UserID"] + "";
                dr["CCHECKED"] = "0";
                dtFileStep1.Rows.Add(dr);
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    gvFileStep1.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep1.DataSource = dtFileStep1.Clone();
                }
                gvFileStep1.DataBind();
                txtDetailStep1.Text = string.Empty;
            }
            else
            {
                throw new Exception("กรุณาเลือกไฟล์ที่เป็นรูปภาพแล้วระบุรายละเอียด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void btnCloseStep1_Click(object sender, EventArgs e)
    {
        plStep1.CssClass = "hide";
        plCheckList.Visible = true;
        liTab2.Visible = true;
        liTab3.Visible = true;
        if (!string.IsNullOrEmpty(hidRowIndex.Value))
        {
            GridViewRow gvr = null;
            if (hidEmp.Value == "0")//หาว่าคลิกจาก Grid สภาพรถ = 0 หรือ จาก พนักงานประจำรถ = 1
            {
                gvr = gvCheckList.Rows[int.Parse(hidRowIndex.Value)];
            }
            else
            {
                gvr = gvCheckListEmp.Rows[int.Parse(hidRowIndex.Value)];
            }
            if (hidStep.Value == "Step1")
            {

                Button btn = (Button)gvr.FindControl("btnStep1");
                Image img = (Image)this.Master.FindControl("cph_Main").FindControl("imgTruck_" + hidSCHECKLISTID.Value);
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    //btn.Text = "แจ้งพบปัญหา";
                    btn.CssClass = "btn btn-warning";
                    if (img != null)
                    {
                        img.Visible = true;
                    }

                }
                else
                {

                    //btn.Text = "แจ้งตรวจพบปัญหา";
                    btn.CssClass = "btn btn-info";
                    if (img != null)
                    {
                        img.Visible = false;
                    }
                }
            }
        }

    }
    protected void gvFileStep1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value + "");

        dtFileStep1.Rows.Remove(drs[e.RowIndex]);
        drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value + "");
        if (drs.Any())
        {
            gvFileStep1.DataSource = drs.CopyToDataTable();
        }
        else
        {
            gvFileStep1.DataSource = dtFileStep1.Clone();
        }
        gvFileStep1.DataBind();
    }
    #endregion

    #region Step2
    protected void gvFileStep2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Repeater rp = (Repeater)e.Row.FindControl("rpFileStep2Detail");
            if (rp != null)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                DataRow[] drs = dtFileStep2.Select("SCHECKLISTID = " + drv["SCHECKLISTID"] + " AND SMAINPICTURE = " + drv["NID"]);
                if (drs.Any())
                {
                    rp.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    rp.DataSource = dtFileStep2.Clone();
                }
                rp.DataBind();
            }
        }
    }

    protected void btnAddStep2_Click(object sender, EventArgs e)
    {
        try
        {
            if (fileUploadStep2.HasFile && fileUploadStep2.PostedFile.ContentType.Contains("image/") && !string.IsNullOrEmpty(txtDetailStep2.Text.Trim()))
            {
                string[] fileExt = fileUploadStep2.FileName.Split('.');
                int ncol = fileExt.Length - 1;
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                string SFILEPATH = string.Format(TempDirectory, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                UploadFile2Server(fileUploadStep2, genName, SFILEPATH);

                DataRow dr = dtFileStep2.NewRow();
                //dr["SCHECKID"] = hidSCHECKID.Value;
                dr["NID"] = 0;
                dr["SCHECKLISTID"] = !string.IsNullOrEmpty(hidSCHECKLISTID.Value) ? int.Parse(hidSCHECKLISTID.Value) : (object)DBNull.Value;
                dr["SVERSIONLIST"] = !string.IsNullOrEmpty(hidSVERSIONLIST.Value) ? int.Parse(hidSVERSIONLIST.Value) : (object)DBNull.Value;
                dr["STYPECHECKLISTID"] = !string.IsNullOrEmpty(hidSTYPECHECKLISTID.Value) ? int.Parse(hidSTYPECHECKLISTID.Value) : (object)DBNull.Value;
                dr["SPROCESS"] = "2";
                dr["NNO"] = "1";
                dr["SEVIDENCENAME"] = txtDetailStep2.Text.Trim();
                dr["SFILENAME"] = fileUploadStep2.FileName;
                dr["SGENFILENAME"] = genName + "." + fileExt[ncol].Trim();
                dr["SFILEPATH"] = SFILEPATH + genName + "." + fileExt[ncol].Trim();
                dr["SCREATE"] = Session["UserID"] + "";
                dr["SMAINPICTURE"] = ddlFileStep2.SelectedValue;
                dr["CCHECKED"] = "0";
                dtFileStep2.Rows.Add(dr);
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    gvFileStep2.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep2.DataSource = dtFileStep2.Clone();
                }
                gvFileStep2.DataBind();
                txtDetailStep2.Text = string.Empty;
            }
            else
            {
                throw new Exception("กรุณาเลือกไฟล์ที่เป็นรูปภาพแล้วระบุรายละเอียด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        DataRow[] drs = dtFileStep2.Select("SMAINPICTURE = " + btn.CommandArgument);
        dtFileStep2.Rows.Remove(drs[int.Parse(btn.CommandName)]);
        //GridViewRow gvr = (GridViewRow)(((Button)btn.CommandSource).NamingContainer);

        //int RowIndex = gvr.RowIndex;

        drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
        if (drs.Any())
        {
            gvFileStep2.DataSource = drs.CopyToDataTable();
        }
        else
        {
            gvFileStep2.DataSource = dtFileStep2.Clone();
        }
        gvFileStep2.DataBind();
    }

    protected void btnCloseStep2_Click(object sender, EventArgs e)
    {
        plStep2.CssClass = "hide";
        plCheckList.Visible = true;
        liTab2.Visible = true;
        liTab3.Visible = true;
        if (!string.IsNullOrEmpty(hidRowIndex.Value))
        {
            GridViewRow gvr = null;
            if (hidEmp.Value == "0")//หาว่าคลิกจาก Grid สภาพรถ = 0 หรือ จาก พนักงานประจำรถ = 1
            {
                gvr = gvCheckList.Rows[int.Parse(hidRowIndex.Value)];
            }
            else
            {
                gvr = gvCheckListEmp.Rows[int.Parse(hidRowIndex.Value)];
            }
            if (hidStep.Value == "Step2")
            {

                Button btn = (Button)gvr.FindControl("btnStep2");

                DataRow[] drs = dtFileStep2.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    //btn.Text = "แจ้งพบปัญหา";
                    btn.CssClass = "btn btn-warning";
                }
                else
                {

                    //btn.Text = "แจ้งตรวจพบปัญหา";
                    btn.CssClass = "btn btn-info";
                }
            }
        }
    }
    #endregion

    #region Step3

    protected void gvFileStep3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Repeater rp = (Repeater)e.Row.FindControl("rpFileStep3Detail");
            if (rp != null)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                DataRow[] drs = dtFileStep2.Select("SCHECKLISTID = " + drv["SCHECKLISTID"] + " AND SMAINPICTURE = " + drv["NID"]);
                if (drs.Any())
                {
                    rp.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    rp.DataSource = dtFileStep2.Clone();
                }
                rp.DataBind();
            }
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
        if (drs.Any())
        {
            foreach (DataRow item in drs)
            {
                item["CCHECKED"] = rblCCHECKED.SelectedValue;
                item["SREMARKCHECKED"] = txtSREMARKCHECKED.Text;

            }
        }
        btnCloseStep3_Click(null, null);
    }

    protected void btnCloseStep3_Click(object sender, EventArgs e)
    {
        plStep3.CssClass = "hide";
        plCheckList.Visible = true;

        liTab2.Visible = true;
        liTab3.Visible = true;
        if (!string.IsNullOrEmpty(hidRowIndex.Value))
        {
            GridViewRow gvr = null;
            if (hidEmp.Value == "0")//หาว่าคลิกจาก Grid สภาพรถ = 0 หรือ จาก พนักงานประจำรถ = 1
            {
                gvr = gvCheckList.Rows[int.Parse(hidRowIndex.Value)];
            }
            else
            {
                gvr = gvCheckListEmp.Rows[int.Parse(hidRowIndex.Value)];
            }
            if (hidStep.Value == "Step3")
            {
                Button btn = (Button)gvr.FindControl("btnStep3");

                DataRow[] drs = dtFileStep1.Select(" SCHECKLISTID = " + hidSCHECKLISTID.Value + " ");
                if (drs.Any())
                {
                    //btn.Text = "แจ้งพบปัญหา";
                    btn.CssClass = "btn btn-warning";
                }
                else
                {

                    //btn.Text = "แจ้งตรวจพบปัญหา";
                    btn.CssClass = "btn btn-info";
                }
            }
        }
    }
    #endregion

    #region gvCheckList
    protected void gvCheckList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            Label lblCHOLD = (Label)e.Row.FindControl("lblCHOLD");
            Label lblMAnDay = (Label)e.Row.FindControl("lblMAnDay");
            Label lblCheckBy = (Label)e.Row.FindControl("lblCheckBy");
            string SCHECKLISTID = drv["SCHECKLISTID"] + string.Empty;
            if (dtCheckTruck != null && dtFileStep1 != null && dtFileStep1.Select("SCHECKLISTID = " + SCHECKLISTID).Any())
            {
                string CBAN = "", CCUT = "", NDAY_MA = "", ReportBy = ""; int HasCheckList = 0;

                foreach (DataRow drCheckData in dtCheckTruck.Select("SCHECKLISTID='" + SCHECKLISTID + "' AND CCHECKED = '0'"))
                {
                    HasCheckList = 1;
                    //SCHECKID = drCheckData["SCHECKID"] + "";
                    ReportBy = drCheckData["SCREATENAMED"] + "";
                    break;
                }
                CBAN = dtCheckTruck.Select("SCHECKLISTID='" + SCHECKLISTID + "' AND ICMA='2'").Any() ? "1" : "0";
                CCUT = dtCheckTruck.Select("SCHECKLISTID='" + SCHECKLISTID + "' AND ICMA='1'").Any() ? "1" : "0";
                DataRow[] drBan = dtCheckTruck.Select("SCHECKLISTID='" + SCHECKLISTID + "' AND ICMA='2'", "DEND_LIST ,DBEGIN_LIST");
                DataRow[] drNDAYMA = dtCheckTruck.Select("SCHECKLISTID='" + SCHECKLISTID + "' AND ICMA='1'", "DEND_LIST ,DBEGIN_LIST");
                NDAY_MA = drNDAYMA.Length > 0 ? "" + drNDAYMA[0]["INDAY_MA"] : "0";
                if (HasCheckList > 0)// (dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "'").Length > 0)
                {
                    lblCHOLD.Text = "ห้ามวิ่ง" + (drBan.Length > 0 ? "(เมื่อ " + Convert.ToDateTime("" + drBan[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "");
                    if (NDAY_MA != "0")
                    {
                        lblMAnDay.Text = (NDAY_MA != "") ? "แก้ไขภายใน " + NDAY_MA + " วัน" + (drNDAYMA.Length > 0 ? "(ภายใน " + Convert.ToDateTime("" + drNDAYMA[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "") : "";
                    }
                    lblCheckBy.Text = "<img title='" + ReportBy + "' src='images/ic_owner.gif' />";

                }
            }
            if (dtFileStep1 != null)
            {
                Image img = (Image)this.Master.FindControl("cph_Main").FindControl("imgTruck_" + SCHECKLISTID);
                if (dtFileStep1.Select("SCHECKLISTID = " + SCHECKLISTID).Any())
                {
                    Button btn = (Button)e.Row.FindControl("btnStep1");
                    btn.CssClass = "btn btn-warning";
                    if (img != null)
                    {
                        img.Visible = true;
                    }
                    //btn.Text = "แจ้งพบปัญหา";
                    if (Session["CGROUP"] + string.Empty == "0")
                    {
                        Button btnStep2 = (Button)e.Row.FindControl("btnStep2");
                        btnStep2.Enabled = true;
                        if (dtFileStep2 != null)
                        {
                            if (dtFileStep2.Select("SCHECKLISTID = " + SCHECKLISTID).Any())
                            {
                                btnStep2.CssClass = "btn btn-warning";
                            }
                        }

                    }
                }
                else
                {
                    if (img != null)
                    {
                        img.Visible = false;
                    }
                }
            }
            if (Session["CGROUP"] + string.Empty == "0")
            {
                Button btn = (Button)e.Row.FindControl("btnStep1");
                btn.Enabled = false;
            }
            else
            {
                if (dtFileStep2.Select("SCHECKLISTID = " + SCHECKLISTID).Any())
                {
                    Button btn = (Button)e.Row.FindControl("btnStep3");
                    btn.Enabled = true;


                    btn = (Button)e.Row.FindControl("btnStep2");
                    btn.CssClass = "btn btn-warning";
                }
            }
        }


    }
    protected void gvCheckList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (gv != null)
        {
            DataKey dka = gv.DataKeys[int.Parse(e.CommandName)];
            hidRowIndex.Value = e.CommandName;
            hidStep.Value = e.CommandArgument + string.Empty;
            hidSCHECKLISTID.Value = dka["SCHECKLISTID"] + string.Empty;
            hidSVERSIONLIST.Value = dka["SVERSIONLIST"] + string.Empty;
            hidSTYPECHECKLISTID.Value = dka["STYPECHECKLISTID"] + string.Empty;
            hidEmp.Value = gv.ID == "gvCheckList" ? "0" : "1";//หาว่าคลิกจาก Grid สภาพรถ = 0 หรือ จาก พนักงานประจำรถ = 1
            if (hidStep.Value == "Step1")
            {

                lblSCHECKLISTNAME.Text = dka["SCHECKLISTNAME"] + string.Empty;
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    gvFileStep1.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep1.DataSource = dtFileStep1.Clone();
                }
                gvFileStep1.DataBind();
                plCheckList.Visible = false;
                plStep1.CssClass = "";
            }
            else if (hidStep.Value == "Step2")
            {
                lblSCHECKLISTNAMEStep2.Text = dka["SCHECKLISTNAME"] + string.Empty;
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    gvFileStep2.DataSource = drs.CopyToDataTable();
                    ddlFileStep2.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep2.DataSource = dtFileStep1.Clone();
                    ddlFileStep2.DataSource = dtFileStep1.Clone();
                }
                gvFileStep2.DataBind();
                ddlFileStep2.DataTextField = "SEVIDENCENAME";
                ddlFileStep2.DataValueField = "NID";
                ddlFileStep2.DataBind();
                plCheckList.Visible = false;
                plStep2.CssClass = "";
            }
            else if (hidStep.Value == "Step3")
            {

                lblSCHECKLISTNAMEStep3.Text = dka["SCHECKLISTNAME"] + string.Empty;
                DataRow[] drs = dtFileStep1.Select("SCHECKLISTID = " + hidSCHECKLISTID.Value);
                if (drs.Any())
                {
                    rblCCHECKED.SelectedValue = drs[0]["CCHECKED"] + string.Empty;
                    txtSREMARKCHECKED.Text = drs[0]["SREMARKCHECKED"] + string.Empty;
                    gvFileStep3.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep3.DataSource = dtFileStep1.Clone();
                }
                gvFileStep3.DataBind();

                plCheckList.Visible = false;
                plStep3.CssClass = "";
            }
            liTab2.Visible = false;
            liTab3.Visible = false;

        }
    }
    #endregion

    #region UploadFile2Server
    private bool UploadFile2Server(FileUpload ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    #endregion

    #region Save

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
            {
                SaveStep1();

                SaveStep3();
                //SaveLog(ConfigValue.SurpriseCheck3, "AAAAA", "พิจารณาแก้ไขสภาพรถ", "green", "");
                alertSuccess("บันทึกสำเร็จ" + "<br/>" + messSendEnail, "surprisechk_show.aspx");
            }
            else
            {
                SaveStep2();
                alertSuccess("บันทึกสำเร็จ" + "<br/>" + messSendEnail, "VendorConfirmChecktruck.aspx");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SaveStep3()
    {
        DataRow[] drsData = dtFileStep1.Select("CCHECKED > -1");
        if (drsData.Any() && dtFileStep2.Rows.Count > 0)
        {
            var drs = drsData.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
            if (drs.Any())
            {
                DataTable dtCHECKTRUCKITEM = drs.CopyToDataTable();
                dtCHECKTRUCKITEM.TableName = "DT";
                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dtCHECKTRUCKITEM.Copy());
                SurpriseCheckBLL.Instance.CheckTruckItemUpdate(hidYearID.Value, ds);

                string CheckItemRepair = @"SELECT TCT.STRUCKID,TCL.CBAN FROM TCHECKTRUCK TCT
LEFT JOIN TCHECKTRUCKITEM TCI
ON TCI.SCHECKID = TCT.SCHECKID
LEFT JOIN TCHECKLIST TCL
ON TCL.SCHECKLISTID = TCI.SCHECKLISTID
WHERE STRUCKID = '" + hidSTRUCKID.Value + "' AND TCI.CCHECKED ='0' AND TCL.CBAN ='1'";

                using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    DataTable dt_RepairTruck = CommonFunction.Get_Data(connection, CheckItemRepair);
                    if (dt_RepairTruck.Rows.Count == 0)
                    {
                        DataTable dt_Type = CommonFunction.Get_Data(connection, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(hidSTRUCKID.Value + "") + "'");
                        if (dt_Type.Rows.Count > 0)
                        {
                            string SCARTYPE = dt_Type.Rows[0]["SCARTYPEID"] + "";
                            AlertTruckOnHold AT = new AlertTruckOnHold(Page, connection);
                            AT.Lock_SAP(hidSTRUCKID.Value + "", SCARTYPE);
                        }

                    }
                }

                var dtGroup = drs;
                string TitleName = string.Empty, StatusName = string.Empty;
                if (dtGroup.Any())
                {
                    foreach (var item in dtGroup)
                    {
                        DataRow[] drGroups = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                        if (drGroups.Any())
                        {
                            TitleName += "," + drGroups[0]["SCHECKLISTNAME"];
                            if (item["CCHECKED"] + string.Empty == "1")
                            {
                                StatusName += ",อนุมัติผลการแก้ไข";
                            }
                            else
                            {
                                StatusName += ",ไม่อนุมัติ";
                            }
                        }

                    }
                    SaveLog(ConfigValue.SurpriseCheck3, TitleName, "พิจารณาแก้ไขสภาพรถ", StatusName == ",ไม่อนุมัติ" ? "red" : "green", StatusName);
                }
            }
        }


    }

    private void SaveStep2()
    {
        ///FileDetail แจ้งผลการแก้ไข
        DataRow[] drs = dtFileStep2.Select("NID = 0");
        if (drs.Any())
        {
            DataSet ds = new DataSet("DS");
            dtFileStep2.TableName = "DT";

            ds.Tables.Add(dtFileStep2.Copy());
            SurpriseCheckBLL.Instance.CheckTruckFileSave(hidSCHECKID.Value, "2", hidYearID.Value, ds);
            var dtGroup = drs.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
            string TitleName = string.Empty;
            if (dtGroup.Any())
            {
                foreach (var item in dtGroup)
                {
                    drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                    if (drs.Any())
                        TitleName += "," + drs[0]["SCHECKLISTNAME"];
                }
                SaveLog(ConfigValue.SurpriseCheck2, TitleName, "แก้ไขปัญหาสภาพรถ", "orange");
            }
        }


    }

    private void SaveStep1()
    {

        DataRow[] drs = null;
        DataRow dr = null;
        double NPOINT = 0;


        using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            connection.Open();
            OracleTransaction oTransaction = connection.BeginTransaction();
            try
            {

                TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection, oTransaction);
                TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(Page, connection, oTransaction);
                TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection, oTransaction);
                foreach (DataRow item in dtFileStep1.Select("NID = 0"))
                {
                    drs = dtFileStep1.Select("NID > 0 AND SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                    if (!drs.Any())
                    {
                        drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                        if (drs.Any())
                        {
                            dr = drs[0];
                            #region TCHECKTRUCK
                            ///TCHECKTRUCK
                            ctrck.SCHECKID = hidSCHECKID.Value;//send blank for gen new id
                            ctrck.NPLANID = NPLANID; /////NPLANID
                            ctrck.STERMINALID = "" + Session["SVDID"];
                            ctrck.SCONTRACTID = "" + hidSCONTRACTID.Value;         /////SCONTRACTID
                            ctrck.CGROUPCHECK = "3";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                            ctrck.CCLEAN = "0";
                            ctrck.STRUCKID = "" + hidSTRUCKID.Value; /////STRUCKID
                            ctrck.SHEADERREGISTERNO = "" + txtSHEADREGISTERNO.Text; /////SHEADERREGISTERNO
                            ctrck.STRAILERREGISTERNO = "" + txtSTRAILERREGISTERNO.Text; /////STRAILERREGISTERNO
                            ctrck.CMA = (("" + dr["CBAN"] == "1") ? "2" : (("" + dr["CCUT"] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                            ctrck.NDAY_MA = (("" + dr["NDAY_MA"] == "") ? "0" : "" + dr["NDAY_MA"]);
                            ctrck.SCREATE = "" + Session["UserID"];
                            ctrck.SUPDATE = "" + Session["UserID"];
                            ctrck.IS_YEAR = hidYear.Value;
                            ctrck.NSURPRISECHECKID = hidNSURPRISECHECKID.Value;
                            ctrck.Insert();
                            hidSCHECKID.Value = ctrck.SCHECKID;
                            #endregion
                            #region TCHECKTRUCKITEM
                            ctrckItem.SCHECKID = hidSCHECKID.Value;
                            ctrckItem.SCHECKLISTID = "" + dr["SCHECKLISTID"];
                            ctrckItem.SVERSIONLIST = "" + dr["SVERSIONLIST"];
                            ctrckItem.NDAY_MA = (("" + dr["NDAY_MA"] == "") ? "0" : "" + dr["NDAY_MA"]);
                            ctrckItem.NPOINT = ("" + dr["NPOINT"] != "") ? ("" + dr["NPOINT"]) : "0";
                            ctrckItem.CMA = (("" + dr["CCUT"] == "1") ? "2" : (("" + dr["CCUT"] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                            ctrckItem.COTHER = "0";
                            ctrckItem.SOTHERREMARK = "";
                            ctrckItem.STYPECHECKLISTID = "" + dr["STYPECHECKLISTID"];
                            ctrckItem.SCREATE = "" + Session["UserID"];
                            ctrckItem.SUPDATE = "" + Session["UserID"];
                            ctrckItem.IS_YEAR = hidYear.Value;
                            ctrckItem.Insert();
                            #endregion
                            #region บันทึกห้ามวิ่ง+ตัดแต้ม

                            if ("" + dr["CBAN"] == "1" && string.IsNullOrEmpty(hidYear.Value))
                            {//ถ้าเป้นเคส ห้ามวิ่ง 
                                #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                                using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                {
                                    if (con.State == ConnectionState.Closed) con.Open();
                                    OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_HEADERREGISTERNO", con);
                                    ora_cmd.Parameters.Add(":S_HEADERREGISTERNO", OracleType.VarChar).Value = ctrck.SHEADERREGISTERNO.Trim();
                                    ora_cmd.ExecuteNonQuery();

                                    OracleCommand ora_trail = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_TRAILERREGISTERNO", con);
                                    ora_trail.Parameters.Add(":S_TRAILERREGISTERNO", OracleType.VarChar).Value = ctrck.STRAILERREGISTERNO.Trim();
                                    ora_trail.ExecuteNonQuery();

                                    AlertTruckOnHold onHold = new AlertTruckOnHold(Page, con);
                                    onHold.VehNo = ctrck.SHEADERREGISTERNO;
                                    onHold.TUNo = ctrck.STRAILERREGISTERNO;
                                    onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                    onHold.VehID = ctrck.STRUCKID;
                                    onHold.SendTo();
                                }
                                #endregion
                            }
                            if ("" + dr["CCUT"] == "1")
                            {//ถ้าตัดคะแนนทันที
                                //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                                ReducePoint(connection, oTransaction, "01", "040", "" + dr["SCHECKLISTNAME"], "" + dr["SCHECKLISTID"], "" + dr["STOPICID"], "" + dr["SVERSIONLIST"], "" + (("" + dr["NPOINT"] != "") ? ("" + dr["NPOINT"]) : "0")
                            , hidSCHECKID.Value, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");

                            }
                            #endregion
                            NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;
                        }

                    }

                }
                #region Update คะแนนรวมที่ตัดที่ รายการหลัก
                ctrck.SCHECKID = hidSCHECKID.Value;
                ctrck.UpdateMA();
                #endregion
                oTransaction.Commit();
            }
            catch (Exception ex)
            {
                oTransaction.Rollback();
                throw new Exception(ex.Message);
            }
            finally
            {
                connection.Close();
            }


        }
        drs = dtFileStep1.Select("NID = 0");
        if (drs.Any())
        {
            DataSet ds = new DataSet("DS");
            dtFileStep1.TableName = "DT";
            ds.Tables.Add(dtFileStep1.Copy());

            SurpriseCheckBLL.Instance.CheckTruckFileSave(hidSCHECKID.Value, "1", hidYearID.Value, ds);
            var dtGroup = drs.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
            string TitleName = string.Empty;
            if (dtGroup.Any())
            {
                foreach (var item in dtGroup)
                {
                    drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                    if (drs.Any())
                        TitleName += "," + drs[0]["SCHECKLISTNAME"];
                }
                SaveLog(ConfigValue.SurpriseCheck1, TitleName, "แจ้งปัญหาสภาพรถ", "red");
            }

        }


    }

    private string SaveLog(int TemplateID, string TitleName, string HeadName, string CssColor, string StatusName = "")
    {
        messSendEnail = string.Empty;



        if (!string.IsNullOrEmpty(TitleName))
        {

            TitleName = (TitleName).Remove(0, 1);
            LOG.Instance.SaveLog(Session["vendoraccountname"] + " " + HeadName + " Surprise Check", txtSHEADREGISTERNO.Text, Session["UserID"].ToString(), "<span class=\"" + CssColor + "\">" + TitleName + "</span>", StatusName);
            #region บันทึกหางลาก Log
            if (!string.IsNullOrEmpty(txtSTRAILERREGISTERNO.Text))
            {
                LOG.Instance.SaveLog(Session["vendoraccountname"] + " " + HeadName + " Surprise Check", txtSTRAILERREGISTERNO.Text, Session["UserID"].ToString(), "<span class=\"" + CssColor + "\">" + TitleName + "</span>", StatusName);
            }
            #endregion

            if (SendEmail(TemplateID, TitleName, StatusName))
            {
                messSendEnail += MessEmailSuccess;
            }
            else
            {
                messSendEnail += MessEmailFail;
            }
        }
        return messSendEnail;
    }
    protected bool ReducePoint(OracleConnection connection, OracleTransaction oTransaction, string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {   //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO 
        // OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection, oTransaction);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        //repoint.SDELIVERYNO = "" ;
        repoint.Insert();
        return IsReduce;
    }
    #endregion

    #region btnClose_Click
    protected void btnClose_Click(object sender, EventArgs e)
    {
        if ("" + Session["CGROUP"] == "1" || "" + Session["CGROUP"] == "2")
        {
            Response.Redirect("surpriseChk_lst.aspx?s=1");
        }
        else
        {
            Response.Redirect("VendorConfirmChecktruck.aspx");
        }

    }

    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string TitleName, string StatusName)
    {
        try
        {


            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                string EmailList = string.Empty, Link = string.Empty, str = string.Empty, status = string.Empty;
                byte[] plaintextBytes;

                if (TemplateID == ConfigValue.SurpriseCheck1 || TemplateID == ConfigValue.SurpriseCheck2)
                {
                    str = hidSCONTRACTID.Value + "&" + hidSTRUCKID.Value + "&" + txtSHEADREGISTERNO.Text + "&" + txtSTRAILERREGISTERNO.Text + "&" + txtCheckerDate.Text + "&" + hidNSURPRISECHECKID.Value + "&" + txtSCONTRACTNO.Text + "&" + hidSVENDORNAME.Value + "&" + hidSVENDORID.Value;
                }
                else if (TemplateID == ConfigValue.SurpriseCheck3)
                {
                    str = hidSCONTRACTID.Value + "&" + hidSTRUCKID.Value + "&" + txtSHEADREGISTERNO.Text + "&" + txtSTRAILERREGISTERNO.Text + "&" + txtCheckerDate.Text + "&" + hidNSURPRISECHECKID.Value + "&" + txtSCONTRACTNO.Text + "&" + hidSVENDORNAME.Value + "&" + hidSVENDORID.Value;
                    status = StatusName;
                    if (!string.IsNullOrEmpty(status))
                    {
                        status = status.Remove(0, 1);
                        string[] statuss = status.Split(',');
                        if (!string.IsNullOrEmpty(TitleName))
                        {
                            string[] titles = TitleName.Split(',');
                            status = string.Empty;
                            for (int i = 0; i < titles.Count(); i++)
                            {
                                TitleName = titles[i];
                                status += TitleName + " สถานะ : " + statuss[i] + "<br/>";
                            }
                        }
                    }
                }
                plaintextBytes = Encoding.UTF8.GetBytes(str);
                str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                if (TemplateID == ConfigValue.SurpriseCheck3)
                    Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckAddEdit.aspx?str=" + str + "&modehis=1";
                else
                    Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckAddEdit.aspx?str=" + str;
                #region SurpriseCheck
                EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, false, false);
                //Body = Body.Replace("{Email}", "");

                DataTable dtemail = SurpriseCheckBLL.Instance.CheckTruckEmailSelectBLL(hidNSURPRISECHECKID.Value);

                if (dtCheckTruck.Rows.Count > 0)
                {
                    if (dtemail.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(EmailList) && !string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()) && !string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += "," + dtCheckTruck.Rows[0]["SEMAIL"].ToString() + "," + dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()) && !string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtCheckTruck.Rows[0]["SEMAIL"].ToString() + "," + dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(EmailList) && !string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += "," + dtCheckTruck.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtCheckTruck.Rows[0]["SEMAIL"].ToString();
                        }
                    }
                }

                //EmailList += "zsuntipab.k@pttdigital.com";
                //EmailList += "TMS_terapat.p@pttplc.com, TMS_atit.l@pttplc.com, TMS_wasupol.p@pttplc.com";
                Body = Body.Replace("{Title}", TitleName);
                Body = Body.Replace("{SCONTRACTID}", txtSCONTRACTNO.Text);
                Body = Body.Replace("{SHEADREGISTERNO}", txtSHEADREGISTERNO.Text + (!string.IsNullOrEmpty(txtSTRAILERREGISTERNO.Text) && txtSTRAILERREGISTERNO.Text != "-" ? " - " + txtSTRAILERREGISTERNO.Text : ""));
                Body = Body.Replace("{Vendor}", hidSVENDORNAME.Value);
                Body = Body.Replace("{Status}", status);
                Body = Body.Replace("{VendorAccountName}", Session["vendoraccountname"] + string.Empty);


                //byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                //string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                //plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                Body = Body.Replace("{LINK}", Link);
                MailService.SendMail(EmailList, Subject, Body, "", "SURPRISE_CHECK", ColumnEmailName);
                #endregion


                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region Other
    protected void btnSaveOther_Click(object sender, EventArgs e)
    {
        try
        {
            if (cbCOTHER.Checked || dtFileOther.Rows.Count > 0)
            {
                using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    connection.Open();
                    OracleTransaction oTransaction = connection.BeginTransaction();
                    try
                    {
                        if (!(dtFileStep1 != null && dtFileStep1.Rows.Count > 0))
                        {

                            #region TCHECKTRUCK
                            TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection, oTransaction);
                            ///TCHECKTRUCK
                            ctrck.SCHECKID = "";//send blank for gen new id
                            ctrck.NPLANID = "" + NPLANID;
                            ctrck.STERMINALID = "" + Session["SVDID"];
                            ctrck.SCONTRACTID = "" + hidSCONTRACTID.Value;
                            ctrck.CGROUPCHECK = "3";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                            ctrck.CCLEAN = "0";
                            ctrck.STRUCKID = "" + hidSTRUCKID.Value;
                            ctrck.SHEADERREGISTERNO = "" + txtSHEADREGISTERNO.Text;
                            ctrck.STRAILERREGISTERNO = "" + txtSTRAILERREGISTERNO.Text;
                            ctrck.CMA = "0";//2 hold ,1 mainternent ,0 ปกติ
                            ctrck.NDAY_MA = "0";
                            ctrck.SCREATE = "" + Session["UserID"];
                            ctrck.SUPDATE = "" + Session["UserID"];

                            ctrck.Insert();
                            hidSCHECKID.Value = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.

                            #endregion

                        }
                        TCHECKTRUCKITEM chkItem = new TCHECKTRUCKITEM(this.Page, connection, oTransaction);
                        #region TCHECKTRUCKITEM
                        chkItem.SCHECKID = hidSCHECKID.Value;
                        chkItem.SCHECKLISTID = "0";
                        chkItem.CMA = "0";
                        chkItem.NPOINT = "0";
                        chkItem.SCHECKLISTID = "0";
                        chkItem.SVERSIONLIST = "0";
                        chkItem.NDAY_MA = "0";
                        chkItem.COTHER = cbCOTHER.Checked ? "1" : "0";
                        chkItem.SOTHERREMARK = cbCOTHER.Checked ? txtSOTHERREMARK.Text : "";
                        chkItem.SREMARK = txtSREMARK.Text;
                        chkItem.SCREATE = "" + Session["UserID"];
                        chkItem.SUPDATE = "" + Session["UserID"];
                        chkItem.InsertTab2(hidSCONTRACTID.Value, hidSTRUCKID.Value, txtCheckerDate.Text);
                        #endregion

                        oTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }


                if (dtFileOther.Rows.Count > 0)
                {
                    DataSet ds = new DataSet("DS");
                    dtFileOther.TableName = "DT";
                    ds.Tables.Add(dtFileOther.Copy());

                    SurpriseCheckBLL.Instance.CheckTruckFileOtherSave(hidSCHECKID.Value, hidSTRUCKID.Value, hidSCONTRACTID.Value, ds);
                }
                alertSuccess("บันทึกสำเร็จ");
            }
            else
            {
                throw new Exception("กรุณาเลือก ปัญหาอื่นๆ ระบุปัญหา หรือ เพิ่มไฟล์");
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    protected void btnAddOther_Click(object sender, EventArgs e)
    {
        try
        {
            if (fileUploadOther.HasFile && fileUploadOther.PostedFile.ContentType.Contains("image/"))
            {
                string[] fileExt = fileUploadOther.FileName.Split('.');
                int ncol = fileExt.Length - 1;
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                string SFILEPATH = string.Format(TempDirectory, Session["SVDID"] + "", "uploaderOther", Session["UserID"] + "");
                UploadFile2Server(fileUploadOther, genName, SFILEPATH);

                DataRow dr = dtFileOther.NewRow();
                //dr["SCHECKID"] = hidSCHECKID.Value;
                dr["SEVIDENCENAME"] = txtDetailOther.Text.Trim();
                dr["SFILE"] = fileUploadOther.FileName;
                dr["SSYSFILE"] = genName + "." + fileExt[ncol].Trim();
                dr["SPATH"] = SFILEPATH + genName + "." + fileExt[ncol].Trim();
                dr["SCREATE"] = Session["UserID"] + "";
                dr["SUPDATE"] = Session["UserID"] + "";
                dtFileOther.Rows.Add(dr);
                gvOther.DataSource = dtFileOther;
                gvOther.DataBind();
                txtDetailOther.Text = string.Empty;
            }
            else
            {
                throw new Exception("กรุณาเลือกไฟล์ที่เป็นรูปภาพ");
            }
        }
        catch (Exception ex)
        {

            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }

    protected void gvOther_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        dtFileOther.Rows.RemoveAt(e.RowIndex);
        gvOther.DataSource = dtFileOther;
        gvOther.DataBind();
    }
    #endregion

    #region History

    protected void gvHistoryDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Repeater rp = (Repeater)e.Row.FindControl("rpFileStep3Detail");
            if (rp != null)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                DataRow[] drs = dtFileStep2History.Select("SCHECKLISTID = " + drv["SCHECKLISTID"] + " AND SMAINPICTURE = " + drv["NID"]);
                if (drs.Any())
                {
                    rp.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    rp.DataSource = dtFileStep2History.Clone();
                }
                rp.DataBind();
            }
        }
    }

    protected void gvHistory_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (gv != null)
        {
            DataKey dka = gv.DataKeys[int.Parse(e.CommandName)];
            hidRowIndexHistory.Value = e.CommandName;
            //hidEmp.Value = gv.ID == "gvCheckList" ? "0" : "1";//หาว่าคลิกจาก Grid สภาพรถ = 0 หรือ จาก พนักงานประจำรถ = 1
            if (e.CommandArgument + string.Empty == "History")
            {

                lblSCHECKLISTNAMEDetil.Text = dka["SCHECKLISTNAME"] + string.Empty;
                DataRow[] drs = dtFileStep1History.Select("SCHECKLISTID = " + dka["SCHECKLISTID"] + " AND SCHECKID = " + dka["SCHECKID"] + "");
                if (drs.Any())
                {
                    rblCCHECKED.SelectedValue = drs[0]["CCHECKED"] + string.Empty;
                    txtSREMARKCHECKEDHistory.Text = drs[0]["SREMARKCHECKED"] + string.Empty;
                    gvHistoryDetail.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvHistoryDetail.DataSource = dtFileStep1History.Clone();
                }
                gvHistoryDetail.DataBind();

                plHistory.Visible = false;
                plHistoryDetail.CssClass = "";
            }
        }
    }


    protected void btnCloseHistoryDetail_Click(object sender, EventArgs e)
    {
        plHistoryDetail.CssClass = "hide";
        plHistory.Visible = true;
    }
    #endregion

}
