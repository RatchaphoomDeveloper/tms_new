﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPanel;
using System.Text;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallback;
using DevExpress.Web.ASPxTabControl;
using TMS_BLL.Master;
using TMS_BLL.Transaction.FIFO;

public partial class bookcar : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/bookcar/{0}/{2}/{1}/";
    double def_Double;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw1.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw2.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw3.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion

        //lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        lblDate2.Text = DateTime.Today.ToString("dd/MM/yyyy", new CultureInfo("en-US"));

        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            Session["CheckPermission"] = null;
            Session["sDataUpload"] = null;
            Session["sShowPopup"] = null;

            Session["fifosDataUpload"] = null;
            Session["fifosShowPopup"] = null;

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Session["STERMINAL_DATA"] = Teminal_ID();


            if (CanWrite)
            {
                cboTerminal1.Value = CommonFunction.ReplaceInjection(Session["SVDID"] + "");
                //cboTerminal.Value = Session["SVDID"] + "";
                cboTerminal2.Value = CommonFunction.ReplaceInjection(Session["SVDID"] + "");
            }

            ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
            cboVendor.DataSource = ViewState["DataVendor"];
            cboVendor.DataBind();

            ViewState["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
            cboTerminal2.DataSource = ViewState["DataTTERMINAL"];
            cboTerminal2.DataBind();

            Cache.Remove(sds2.CacheKeyDependency);
            Cache[sds2.CacheKeyDependency] = new object();
            sds2.DataBind();
            gvw2.DataBind();

            Cache.Remove(sds3.CacheKeyDependency);
            Cache[sds3.CacheKeyDependency] = new object();
            sds3.DataBind();
            gvw3.DataBind();

            Cache.Remove(sds1.CacheKeyDependency);
            Cache[sds1.CacheKeyDependency] = new object();
            sds1.DataBind();
            gvw1.DataBind();

            dtePlan1.Value = DateTime.Now.ToString("dd/MM/yyyy");

            var dt = new List<BDT1>();
            Session["bdt1"] = dt;
            Session["tTerminalID"] = null;

            LogUser("11", "R", "เปิดดูข้อมูลหน้า จัดแผนการขนส่ง", "");

            if ((Request.QueryString["s"] + "") == "2")
            {
                ASPxPageControl1.ActiveTabIndex = 2;

            }
            else if ((Request.QueryString["s"] + "") == "1")
            {
                ASPxPageControl1.ActiveTabIndex = 1;
            }
            if (Session["CGROUP"] + string.Empty == "2")
            {
                cboTerminal2.Value = Session["SVDID"] + string.Empty;
            }
            ListData1();

            this.AssignAuthen();
        }
        else
        {
            cboVendor.DataSource = ViewState["DataVendor"];
            cboVendor.DataBind();
            cboTerminal2.DataSource = ViewState["DataTTERMINAL"];
            cboTerminal2.DataBind();

            //cboEmployee.DataSource 
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnSearchplan.Enabled = false;
            }
            if (!CanWrite)
            {
                btnDel1.Enabled = false;
                btnAdd1.Enabled = false;
                btnDel2.Enabled = false;
                btnImport1.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string Teminal_ID()
    {
        string Result = "";

        string UserID = Session["UserID"] + "";
        DataTable dt_TERMINAL = CommonFunction.Get_Data(sql, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '2' AND SUID = '" + UserID + "'");
        if (dt_TERMINAL.Rows.Count > 0)
        {
            Result = dt_TERMINAL.Rows[0]["SVENDORID"] + "";
        }
        else
        {
            dt_TERMINAL = CommonFunction.Get_Data(sql, "SELECT SUID,SVENDORID FROM TUSER WHERE CGROUP = '1' AND SUID = '" + UserID + "'");
            if (dt_TERMINAL.Rows.Count > 0)
            {
                Result = "";
            }
            else
            {
                Result = "XXX";
            }
        }

        return Result;
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    void ClearControl()
    {


    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {
            //gvw.DataBind();
        }
        else if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            //gvw2.DataBind();
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            gvw1.DataBind();

        }
        else
        {
            gvw3.DataBind();
        }

        listImportFileDetail();
        fifolistImportFileDetail();
        ShowCountCar();
    }

    #region xcpn_Callback
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        DateTime outdate;
        int index = 0;
        iTerminal itm = new iTerminal();

        string sMessageiterminal = "";

        switch (paras[0])
        {
            case "AddNewRow":

                if (ASPxPageControl1.ActiveTabIndex == 0)
                {
                    //gvw.AddNewRow();
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {

                    gvw1.AddNewRow();
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    cmbTerminal1.Value = CommonFunction.ReplaceInjection(cboTerminal1.Value + "");
                }


                SetVisibleControl();
                break;
            case "Save":
                #region save


                if (CanWrite)
                {
                    string para = (e.Parameter.Split(';')[1] == "btnSave") ? "-1" : e.Parameter.Split(';')[1];


                    // อ้างอิงคอนโทรลในเทมเพลดของ grid 
                    object NPLANID = null;
                    object NNO = null;
                    object NPLANLISTID = null;
                    object NID = null;

                    ASPxTextBox dtePlan = new ASPxTextBox();
                    TextBox txtPlan = new TextBox();
                    ASPxTextBox txtTime = new ASPxTextBox();
                    ASPxComboBox cmbTerminal = new ASPxComboBox();
                    ASPxTextBox txtTerminal = new ASPxTextBox();
                    ASPxTextBox dteDelivery = new ASPxTextBox();
                    ASPxComboBox cbxTimeWindow = new ASPxComboBox();
                    ASPxComboBox cboHeadRegist = new ASPxComboBox();
                    ASPxComboBox cboTrailerRegist = new ASPxComboBox();
                    ASPxTextBox txtEmployeeID = new ASPxTextBox();

                    ASPxTextBox txtDrop = new ASPxTextBox();
                    //  ASPxComboBox cboDelivery = new ASPxComboBox();
                    ASPxTextBox txtDelivery = new ASPxTextBox();
                    ASPxTextBox txtShipto = new ASPxTextBox();
                    ASPxTextBox txtValue = new ASPxTextBox();
                    ASPxGridView gvDONum = new ASPxGridView();

                    if (ASPxPageControl1.ActiveTabIndex == 0)
                    {

                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 1)
                    {
                        index = int.Parse(para);
                        NID = gvw2.GetRowValues(index, "NID");
                        NPLANID = gvw1.GetRowValues(index, "NPLANID");
                        NNO = gvw1.GetRowValues(index, "NNO");
                        NPLANLISTID = gvw1.GetRowValues(index, "SPLANLISTID");
                        string ValueTruck = gvw2.GetRowValues(index, "STRUCKID") + string.Empty;
                        string STRAILERID = gvw2.GetRowValues(index, "STRAILERID") + string.Empty;
                        string SHEADID = gvw2.GetRowValues(index, "SHEADID") + string.Empty;
                        string SHEADREGISTERNO = gvw2.GetRowValues(index, "SHEADREGISTERNO") + string.Empty;
                        string STRAILERREGISTERNO = gvw2.GetRowValues(index, "STRAILERREGISTERNO") + string.Empty;
                        string STERMINAL = gvw2.GetRowValues(index, "STERMINAL") + string.Empty;
                        txtPlan = (TextBox)gvw2.FindEditFormTemplateControl("dtePlan");
                        //txtTime = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtTime");
                        txtTerminal = (ASPxTextBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                        //dteDelivery = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");
                        cbxTimeWindow = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");
                        gvDONum = (ASPxGridView)gvw2.FindEditFormTemplateControl("gvDONum");
                        //cboHeadRegist = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                        //cboTrailerRegist = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboTrailerRegist");
                        //txtEmployeeID = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtEmployeeID");
                        int PID = Convert.ToInt32(NPLANID);

                        //string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPLANSCHEDULELIST pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + PID + "' AND PL.SPLANLISTID != '" + NPLANLISTID + "'");

                        //string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE T.STRUCKID = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                        int num = 0;
                        //int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                        //ความจุระ
                        string ValueCAPACITY = gvw2.GetRowValues(index, "NCAPACITY") + string.Empty;
                        int NCAPACITY = int.Parse(ValueCAPACITY);
                        int SUMCAPACITY = 0;
                        ASPxTextBox txtDelivery1 = new ASPxTextBox() ;
                        string pars = string.Empty;
                        for (int i = 0; i < gvDONum.VisibleRowCount; i++)
                        {
                            txtDelivery1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtDelivery");
                            pars += ",'" + txtDelivery1.Text.PadLeft(10, '0') + "'";
                        }
                        if (!string.IsNullOrEmpty(pars))
                        {
                            pars = pars.Remove(0, 1);
                            DataTable dtValue = GetNVALUE(pars);

                            if (dtValue != null && dtValue.Rows.Count > 0 && !dtValue.Rows[0].IsNull("NVALUE"))
                            {
                                //NVALUE
                                SUMCAPACITY = int.Parse(dtValue.Rows[0]["NVALUE"] + string.Empty);
                            }
                        }
                        

                        //int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;

                        //SUMCAPACITY = SUMCAPACITY + nValue1;

                        if ((NCAPACITY + 100) < SUMCAPACITY)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มแผนงานในระบบได้ : เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                            return;

                        }
                        string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                        string strsql = "INSERT INTO TPLANSCHEDULE(SCONTRACTNO,SCONTRACTID,CCONFIRM,SEMPLOYEEID,SVENDORID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SCONTRACTNO,:SCONTRACTID,'1',:SEMPLOYEEID,:SVENDORID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)  RETURNING nPlanID INTO :nPlanID";
                        string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:sDeliveryNo,'1',:nValue)";

                        //ข็อมูล

                        using (OracleConnection con1 = new OracleConnection(sql))
                        {
                            int nPlanID = 0;
                            con1.Open();
                            string tmpEmployeeID = "";
                            using (OracleCommand com = new OracleCommand(strsql, con1))
                            {
                                // venderid = CommonFunction.Get_Value(con1, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value.ToString() + "')");

                                string sVendorID = gvw2.GetRowValues(index, "SVENDORID") + string.Empty;
                                string sContractID = gvw2.GetRowValues(index, "SCONTRACTID") + string.Empty;
                                string sContractNO = gvw2.GetRowValues(index, "SCONTRACTNO") + string.Empty;
                                //DataTable DataTemp = CommonFunction.Get_Data(con1, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.STRUCKID = TRIM('" + ValueTruck + "') ORDER BY C.SCONTRACTNO ");
                                //if (DataTemp.Rows.Count > 0)
                                //{
                                //    sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                //    sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                //    sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                //}


                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                               
                                com.Parameters.Add(new OracleParameter(){
                                     Direction = ParameterDirection.Output,
                                    ParameterName = "nPlanID",
                                    OracleType = OracleType.Number,
                                    IsNullable = true,
                                });
                                //ถ้าpagecontrol เป็นหน้าที่จัดแผนแบบเร่งด่วนให้ CFIFO เท่ากับ 1

                                    com.Parameters.Add(":CFIFO", OracleType.Char).Value = '1';
                                    com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                                    tmpEmployeeID = gvw2.GetRowValues(index, "SEMPLOYEEID") + string.Empty;
                                

                                com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tmpEmployeeID;
                                com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = txtTerminal.Text;
                                com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value =
                                    ConvertToDateNull(txtPlan.Text).Value;
                                //com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = txtTime.Text;
                                //com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dteDelivery.Value);
                                com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = ConvertToDateNull(txtPlan.Text).Value;
                                com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = Convert.ToInt32(cbxTimeWindow.Value);
                                com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = SHEADREGISTERNO;
                                com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = STRAILERREGISTERNO;
                                com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.ExecuteNonQuery();
                                nPlanID = int.Parse(com.Parameters["nPlanID"].Value + string.Empty);
                            }
                            
                            string _DL = "";
                            for (int i = 0; i < gvDONum.VisibleRowCount; i++)
                            {
                                string genListID = CommonFunction.Gen_ID(con1, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                txtDelivery1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtDelivery");
                                using (OracleCommand com1 = new OracleCommand(strsql1, con1))
                                {

                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genListID;
                                    com1.Parameters.Add(":nPlanID", OracleType.Number).Value = nPlanID;
                                    //com1.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                    //com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                    com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = txtDelivery1.Text;
                                    com1.Parameters.Add(":nValue", OracleType.Number).Value = ValueCAPACITY;
                                    com1.ExecuteNonQuery();

                                    _DL += ";" + txtDelivery1.Text;
                                }
                            }
                            

                            #region Webservices iterminal
                            DateTime dateplan = ConvertToDateNull(txtPlan.Text).Value;
                            sMessageiterminal = itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), cbxTimeWindow.Value + "", SHEADREGISTERNO + "", STRAILERREGISTERNO + "", STERMINAL + "", tmpEmployeeID, nPlanID + string.Empty, Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                            #endregion
                            if (!string.IsNullOrEmpty(sMessageiterminal) && sMessageiterminal.StartsWith("S"))
                            {
                                string strsql2 = "UPDATE TFIFO SET CPLAN = '1' WHERE NID = :NID";
                                using (OracleCommand com2 = new OracleCommand(strsql2, con1))
                                {
                                    com2.Parameters.Clear();
                                    com2.Parameters.Add(":NID", OracleType.Number).Value = Convert.ToInt32(NID);
                                    com2.ExecuteNonQuery();
                                }
                            }
                                
                            
                        }

                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 2)
                    {
                        NPLANID = gvw1.GetRowValues(int.Parse(para), "NPLANID");
                        NNO = gvw1.GetRowValues(int.Parse(para), "NNO");
                        NPLANLISTID = gvw1.GetRowValues(int.Parse(para), "SPLANLISTID");

                        dtePlan = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dtePlan");
                        txtTime = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtTime");
                        cmbTerminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                        dteDelivery = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");
                        cbxTimeWindow = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                        cboHeadRegist = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                        cboTrailerRegist = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboTrailerRegist");

                        txtDrop = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDrop");
                        txtDelivery = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDelivery");
                        txtShipto = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShipto");
                        txtValue = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtValue");
                    }
                    string Mess = "เพิ่ม";
                    if (!(ASPxPageControl1.ActiveTabIndex == 3) && ASPxPageControl1.ActiveTabIndex != 1)//ถ้าไม่ใช่แท๊บสรุปการจัดแผนประจำวัน
                    {

                        #region ActiveTabIndex != 3

                        //แก้ไข
                        if (NPLANID + "" != "")
                        {
                            #region แก้ไข
                            int PID = Convert.ToInt32(NPLANID);

                            string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPLANSCHEDULELIST pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + PID + "' AND PL.SPLANLISTID != '" + NPLANLISTID + "'");

                            string SCAPACITY;

                            string ValueTruck;

                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");


                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;
                            int nValue1 = int.TryParse(txtValue.Text, out num) ? num : 0;

                            SUMCAPACITY = SUMCAPACITY + nValue1;

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TPlanSchedule SET SCONTRACTNO = :SCONTRACTNO,SCONTRACTID = :SCONTRACTID, SVENDORID = :SVENDORID,SPLANDATE = :SPLANDATE,SPLANTIME= :SPLANTIME, STERMINALID = :STERMINALID, DDELIVERY= :DDELIVERY, STIMEWINDOW = :STIMEWINDOW, SHEADREGISTERNO = :SHEADREGISTERNO, STRAILERREGISTERNO = :STRAILERREGISTERNO, DUPDATE = sysdate, SUPDATE = :SUPDATE WHERE NPLANID = :NPLANID ";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    string sVendorID = "";
                                    string sContractID = "";
                                    string sContractNO = "";
                                    DataTable DataTemp = CommonFunction.Get_Data(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ORDER BY C.SCONTRACTNO ");
                                    if (DataTemp.Rows.Count > 0)
                                    {
                                        sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                        sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                        sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                    }

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                                    //com.Parameters.Add(":SPLANDATE", OracleType.DateTime).Value = DateTime.TryParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "d/M/yyyy HH.mm", new CultureInfo("en-US"), DateTimeStyles.None, out outdate) ? outdate : DateTime.Now;
                                    com.Parameters.Add(":SPLANDATE", OracleType.DateTime).Value = DateTime.ParseExact(dteDelivery.Text.Trim() + " " + txtTime.Text.Trim(), "dd/MM/yyyy HH.mm", new CultureInfo("en-US"));
                                    com.Parameters.Add(":SPLANTIME", OracleType.VarChar).Value = txtTime.Text.Trim();
                                    com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cmbTerminal.Value.ToString();
                                    //com.Parameters.Add(":DDELIVERY", OracleType.DateTime).Value = Convert.ToDateTime(dteDelivery.Value);
                                    com.Parameters.Add(":DDELIVERY", OracleType.DateTime).Value = DateTime.ParseExact(dteDelivery.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
                                    com.Parameters.Add(":STIMEWINDOW", OracleType.Number).Value = Convert.ToInt32(cbxTimeWindow.Value);
                                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = PID;
                                    com.ExecuteNonQuery();

                                }

                                if (NPLANLISTID + "" != "")
                                {
                                    int PlanListID = Convert.ToInt32(NPLANLISTID);
                                    int num1;
                                    int num2;
                                    string strsql1 = "UPDATE TPLANSCHEDULELIST SET NDROP = :NDROP, SDELIVERYNO = :SDELIVERYNO, SSHIPTO = :SSHIPTO, NVALUE = :NVALUE WHERE SPLANLISTID = :SPLANLISTID";
                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NDROP", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = txtDelivery.Text.Trim();
                                        com1.Parameters.Add(":SSHIPTO", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.Parameters.Add(":SPLANLISTID", OracleType.Number).Value = PlanListID;
                                        com1.ExecuteNonQuery();


                                        itm.DeleteMAP(txtDelivery.Text.Trim(), cbxTimeWindow.Value + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", cmbTerminal.Value.ToString(), "", PID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);

                                    }
                                }
                                else
                                {
                                    //ถ้าไม่มี ID sPlanListID ให้ทำการ Insert ข้อมูล ลง TPLANSCHDULELIST
                                    int num1;
                                    int num2;

                                    string genidsPlanListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                    string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,nValue,cActive) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,:nValue,'1')";

                                    NPLANLISTID = genidsPlanListID;

                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genidsPlanListID;
                                        com1.Parameters.Add(":nPlanID", OracleType.Number).Value = PID;
                                        com1.Parameters.Add(":nDrop", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = txtDelivery.Text.Trim();
                                        com1.Parameters.Add(":nValue", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.ExecuteNonQuery();
                                    }
                                }

                                #region Webservices iterminal
                                DateTime dateplan = DateTime.ParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "dd/MM/yyyy HH.mm", new CultureInfo("en-US"));

                                sMessageiterminal = itm.UpdateMAP(txtDelivery.Text.Trim(), cbxTimeWindow.Value + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", cmbTerminal.Value.ToString(), "", PID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                                #endregion
                            }

                            LogUser("11", "E", "แก้ไขข้อมูลหน้า จัดแผนการขนส่งสินค้า", PID + "");
                            #endregion
                            Mess = "แก้ไข";
                        }//insert ข้อมูล
                        else
                        {

                            var bdt1 = (List<BDT1>)Session["bdt1"];

                            string SCAPACITY;

                            string ValueTruck;

                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            double SUMCAPACITY = bdt1.Select(o => o.dtValue).Sum();

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }


                            //กรณีเพิ่มหลาย Timewindows
                            var oTimeWindow = bdt1.Select(o => o.sTimeWindow).Distinct();

                            foreach (var TimeWindows in oTimeWindow)
                            {
                                string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                string strsql = "INSERT INTO TPLANSCHEDULE(SCONTRACTNO,SCONTRACTID,CCONFIRM,SEMPLOYEEID,SVENDORID,nPlanID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SCONTRACTNO,:SCONTRACTID,'1',:SEMPLOYEEID,:SVENDORID,:nPlanID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                                string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:nValue)";

                                //ข็อมูล

                                using (OracleConnection con1 = new OracleConnection(sql))
                                {
                                    con1.Open();
                                    string tmpEmployeeID = "";
                                    using (OracleCommand com = new OracleCommand(strsql, con1))
                                    {




                                        // venderid = CommonFunction.Get_Value(con1, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value.ToString() + "')");

                                        string sVendorID = "";
                                        string sContractID = "";
                                        string sContractNO = "";
                                        DataTable DataTemp = CommonFunction.Get_Data(con1, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value.ToString() + "') ORDER BY C.SCONTRACTNO ");
                                        if (DataTemp.Rows.Count > 0)
                                        {
                                            sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                            sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                            sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                        }


                                        com.Parameters.Clear();
                                        com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                        com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                                        com.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                        //ถ้าpagecontrol เป็นหน้าที่จัดแผนแบบเร่งด่วนให้ CFIFO เท่ากับ 1

                                        if (ASPxPageControl1.ActiveTabIndex == 1)
                                        {
                                            com.Parameters.Add(":CFIFO", OracleType.Char).Value = '1';
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                                            tmpEmployeeID = txtEmployeeID.Text;
                                        }
                                        else
                                        {
                                            com.Parameters.Add(":CFIFO", OracleType.Char).Value = '0';
                                            com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.ParseExact(dtePlan1.Text + " " + DateTime.Now.ToString("HH.mm"), "dd/MM/yyyy HH.mm", new CultureInfo("en-US"));
                                        }

                                        com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tmpEmployeeID;
                                        com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = cmbTerminal.Value.ToString();
                                        com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DateTime.ParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "dd/MM/yyyy HH.mm", new CultureInfo("en-US"));
                                        com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = txtTime.Text;
                                        //com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dteDelivery.Value);
                                        com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = DateTime.ParseExact(dteDelivery.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US"));
                                        com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = TimeWindows;
                                        com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = cboHeadRegist.Value.ToString();
                                        com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                        com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com.ExecuteNonQuery();
                                    }


                                    var sbdt1 = bdt1.Where(o => o.sTimeWindow == TimeWindows).ToList();

                                    string _DL = "";

                                    foreach (var dr in sbdt1)
                                    {
                                        string genListID = CommonFunction.Gen_ID(con1, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com1 = new OracleCommand(strsql1, con1))
                                        {

                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genListID;
                                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genidnPlanID;
                                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = dr.dtDeliveryNo;
                                            com1.Parameters.Add(":nValue", OracleType.Number).Value = dr.dtValue;
                                            com1.ExecuteNonQuery();

                                            _DL += ";" + dr.dtDeliveryNo;
                                        }
                                    }

                                    #region Webservices iterminal
                                    DateTime dateplan = (DateTime.ParseExact(dtePlan.Text + " " + txtTime.Text.Trim(), "dd/MM/yyyy HH.mm", new CultureInfo("en-US")));
                                    sMessageiterminal = itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), TimeWindows + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", cmbTerminal.Value + "", tmpEmployeeID, genidnPlanID, Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                                    #endregion



                                    if (ASPxPageControl1.ActiveTabIndex == 1)
                                    {
                                        string strsql2 = "UPDATE TFIFO SET CPLAN = '1' WHERE NID = :NID";
                                        using (OracleCommand com2 = new OracleCommand(strsql2, con1))
                                        {
                                            com2.Parameters.Clear();
                                            com2.Parameters.Add(":NID", OracleType.Number).Value = Convert.ToInt32(NID);
                                            com2.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }

                            LogUser("11", "I", "บันทึกข้อมูลหน้า จัดแผนการขนส่งสินค้า", "");
                        }

                        #endregion
                    }
                    else if (ASPxPageControl1.ActiveTabIndex != 1) //แท็บสรุปการจัดแผนประจำวัน
                    {
                        #region แท็บสรุปการจัดแผนประจำวัน

                        NPLANID = gvw3.GetRowValues(int.Parse(para), "NPLANID");
                        NPLANLISTID = gvw3.GetRowValues(int.Parse(para), "SPLANLISTID");
                        string STIMEWINDOW = gvw3.GetRowValues(int.Parse(para), "STIMEWINDOW") + "";
                        string STERMINALID = gvw3.GetRowValues(int.Parse(para), "STERMINALID") + "";
                        string DDELIVERY = gvw3.GetRowValues(int.Parse(paras[1]), "DDELIVERY") + "";
                        string CFIFO = gvw3.GetRowValues(int.Parse(paras[1]), "CFIFO") + "";


                        int NPlanID = Convert.ToInt32(NPLANID);
                        string PLAN_OLD = GetDOFROMPLAN(NPlanID + "");//ไปเก็ต DO มาก่อนที่จะอัพเดทข้อมูลเพื่อยิงไปลบจากIterminal(โจ้)

                        ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

                        cboHeadRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                        cboTrailerRegist = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");

                        ASPxComboBox cmbPersonalNo = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                        ASPxTextBox txtRemark = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtRemark");

                        txtDrop = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                        txtDelivery = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDelivery");
                        txtShipto = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                        txtValue = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");

                        if (rblCheck.SelectedIndex == 0)//Case แก้ไขแผน
                        {
                            string OUTBOUNDCAPACITY = CommonFunction.Get_Value(sql, "SELECT SUM(nvl(PL.NVALUE,0)) as SUMVALUE FROM TPLANSCHEDULELIST pl WHERE pl.CACTIVE = '1' AND PL.NPLANID = '" + NPLANID + "' AND PL.SPLANLISTID != '" + NPLANLISTID + "'");

                            string ValueTruck;
                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            int SUMCAPACITY = int.TryParse(OUTBOUNDCAPACITY, out num) ? num : 0;
                            int nValue1 = int.TryParse(txtValue.Text, out num) ? num : 0;

                            SUMCAPACITY = SUMCAPACITY + nValue1;

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }


                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();
                                string strsql = "UPDATE TPlanSchedule SET SCONTRACTNO = :SCONTRACTNO,SCONTRACTID = :SCONTRACTID, SVENDORID = :SVENDORID,SEMPLOYEEID = :SEMPLOYEEID,SREMARK = :SREMARK,DPLAN = sysdate +1,SHEADREGISTERNO = :SHEADREGISTERNO, STRAILERREGISTERNO = :STRAILERREGISTERNO, DUPDATE = sysdate, SUPDATE = :SUPDATE WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {

                                    // string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ");

                                    string sVendorID = "";
                                    string sContractID = "";
                                    string sContractNO = "";
                                    DataTable DataTemp = CommonFunction.Get_Data(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ORDER BY C.SCONTRACTNO ");
                                    if (DataTemp.Rows.Count > 0)
                                    {
                                        sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                        sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                        sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                    }


                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                                    com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();

                                }

                                if (NPLANLISTID + "" != "")
                                {
                                    int PlanListID = Convert.ToInt32(NPLANLISTID);
                                    int num1;
                                    int num2;
                                    string strsql1 = "UPDATE TPLANSCHEDULELIST SET NDROP = :NDROP, SDELIVERYNO = :SDELIVERYNO, SSHIPTO = :SSHIPTO, NVALUE = :NVALUE WHERE SPLANLISTID = :SPLANLISTID";
                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":NDROP", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = txtDelivery.Text.Trim() + "";
                                        com1.Parameters.Add(":SSHIPTO", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":NVALUE", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.Parameters.Add(":SPLANLISTID", OracleType.Number).Value = PlanListID;
                                        com1.ExecuteNonQuery();


                                        PLAN_OLD = PLAN_OLD.Length > 0 ? PLAN_OLD.Remove(0, 1) : "";
                                        itm.DeleteMAP(PLAN_OLD, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID, cmbPersonalNo.Value + "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);

                                    }
                                }
                                else
                                {
                                    //ถ้าไม่มี ID sPlanListID ให้ทำการ Insert ข้อมูล ลง TPLANSCHDULELIST
                                    int num1;
                                    int num2;

                                    string genidsPlanListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                                    string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,nValue,cActive) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,:nValue,'1')";

                                    NPLANLISTID = genidsPlanListID;

                                    using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                    {

                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genidsPlanListID);
                                        com1.Parameters.Add(":nPlanID", OracleType.Number).Value = NPlanID;
                                        com1.Parameters.Add(":nDrop", OracleType.Number).Value = Int32.TryParse(txtDrop.Text, out num1) ? num1 : 0;
                                        com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = txtShipto.Text.Trim();
                                        com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = txtDelivery.Text.Trim();
                                        com1.Parameters.Add(":nValue", OracleType.Number).Value = Int32.TryParse(txtValue.Text, out num2) ? num2 : 0;
                                        com1.ExecuteNonQuery();

                                    }
                                }

                                #region Webservices iterminal

                                DateTime dTemp;
                                DateTime dateplan = DateTime.TryParse(DDELIVERY, out dTemp) ? dTemp : DateTime.Now;

                                string PLAN_NEW = GetDOFROMPLAN(NPlanID + "");//ไปเก็ต DO มาหลังจากจะอัพเดทข้อมูลเพื่อยิงไปสร้าง DO ที่Iterminal(โจ้)
                                PLAN_NEW = PLAN_NEW.Length > 0 ? PLAN_NEW.Remove(0, 1) : "";
                                sMessageiterminal = itm.UpdateMAP(PLAN_NEW, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID, cmbPersonalNo.Value + "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                                #endregion
                            }
                            Mess = "แก้ไข";
                        }
                        else if (rblCheck.SelectedIndex == 1)//Case ยกเลิกแผน
                        {
                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();

                                //if (CFIFO == "1")
                                //{
                                string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPlanID + "'");

                                itm.DeleteMAP(sValueDelivery, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID + "", "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);
                                //}

                                string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();
                                }

                                string strsql1 = "UPDATE TPlanSchedulelist SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com1.ExecuteNonQuery();
                                }
                            }
                            Mess = "ยกเลิก";
                        }
                        else if (rblCheck.SelectedIndex == 2)//Case ยกเลิกและสร้างแผนใหม่
                        {
                            var bdt2 = (List<BDT1>)Session["bdt1"];

                            string ValueTruck;
                            if (!string.IsNullOrEmpty("" + cboTrailerRegist.Value))
                            {
                                ValueTruck = cboTrailerRegist.Value + "";
                            }
                            else
                            {
                                ValueTruck = cboHeadRegist.Value + "";
                            }

                            string SCAPACITY = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + ValueTruck + "' GROUP BY  T.NTOTALCAPACITY");

                            int num = 0;
                            int NCAPACITY = int.TryParse(SCAPACITY, out num) ? num : 0;
                            double SUMCAPACITY = bdt2.Select(o => o.dtValue).Sum();

                            if ((NCAPACITY + 100) < SUMCAPACITY)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจาก เกินปริมาณความจุของรถ! ความจุรถ " + NCAPACITY + " ปริมาณที่จัด " + SUMCAPACITY + "' );");
                                return;

                            }

                            using (OracleConnection con = new OracleConnection(sql))
                            {
                                con.Open();


                                //if (CFIFO == "1")
                                //{
                                //ยิงลบแผนเก่า
                                string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + NPlanID + "'");
                                itm.DeleteMAP(sValueDelivery, STIMEWINDOW + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", STERMINALID + "", "", NPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);
                                //}

                                string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com.ExecuteNonQuery();
                                }

                                string strsql1 = "UPDATE TPlanSchedulelist SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = NPlanID;
                                    com1.ExecuteNonQuery();
                                }

                                //ที่ตัด WHERE ออกเพราะต้องนำค้าตั้งตนจากข้อมูลเดมมาเซฟลง TABLE ไม่งั้นระบบจะไม่สร้างแผนใหม่ให้(โจ้)
                                DataTable dt = new DataTable();
                                dt = CommonFunction.Get_Data(con, "SELECT nvl(CFIFO,'0') AS CFIFO,STERMINALID,SPLANDATE,SPLANTIME,DDELIVERY,STIMEWINDOW from TPLANSCHEDULE WHERE /*CACTIVE = '1' AND*/ NPLANID = " + NPlanID);

                                string genidnPlanID = CommonFunction.Gen_ID(sql, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");

                                string strsql2 = "INSERT INTO TPLANSCHEDULE(SCONTRACTNO,SCONTRACTID,CCONFIRM,SEMPLOYEEID,SREMARK,SVENDORID,nPlanID,nNo,CFIFO,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:SCONTRACTNO,:SCONTRACTID,'1',:SEMPLOYEEID,:SREMARK,:SVENDORID,:nPlanID,1,:CFIFO,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate)";
                                string strsql3 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,nValue) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:nValue)";



                                if (dt.Rows.Count > 0 && bdt2.Count > 0)
                                {
                                    //ข็อมูล
                                    using (OracleCommand com2 = new OracleCommand(strsql2, con))
                                    {

                                        // venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID AND T.STRAILERID = CT.STRAILERID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ");

                                        string sVendorID = "";
                                        string sContractID = "";
                                        string sContractNO = "";
                                        DataTable DataTemp = CommonFunction.Get_Data(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID,c.SCONTRACTID, C.SCONTRACTNO FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE T.SHEADREGISTERNO = TRIM('" + cboHeadRegist.Value + "') ORDER BY C.SCONTRACTNO ");
                                        if (DataTemp.Rows.Count > 0)
                                        {
                                            sVendorID = DataTemp.Rows[0]["SVENDORID"] + "";
                                            sContractID = DataTemp.Rows[0]["SCONTRACTID"] + "";
                                            sContractNO = DataTemp.Rows[0]["SCONTRACTNO"] + "";
                                        }


                                        com2.Parameters.Clear();
                                        com2.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = sContractNO;
                                        com2.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = sContractID;
                                        com2.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = sVendorID;
                                        com2.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = cmbPersonalNo.Value;
                                        com2.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtRemark.Text.Trim();
                                        com2.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                        com2.Parameters.Add(":CFIFO", OracleType.Char).Value = Convert.ToChar(dt.Rows[0]["CFIFO"] + "");
                                        com2.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = dt.Rows[0]["STERMINALID"] + "";
                                        com2.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now.AddDays(1);
                                        com2.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["SPLANDATE"]);
                                        com2.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = dt.Rows[0]["SPLANTIME"] + "";
                                        com2.Parameters.Add(":dDelivery", OracleType.DateTime).Value = Convert.ToDateTime(dt.Rows[0]["DDELIVERY"]);
                                        com2.Parameters.Add(":sTimeWindow", OracleType.Number).Value = Convert.ToInt32(dt.Rows[0]["STIMEWINDOW"]);
                                        com2.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = cboHeadRegist.Value.ToString();
                                        com2.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = cboTrailerRegist.Value + "";
                                        com2.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com2.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                        com2.ExecuteNonQuery();
                                    }


                                    string _DL = "";

                                    foreach (var dr in bdt2)
                                    {
                                        string genListID = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");

                                        using (OracleCommand com3 = new OracleCommand(strsql3, con))
                                        {

                                            com3.Parameters.Clear();
                                            com3.Parameters.Add(":sPlanListID", OracleType.Number).Value = Convert.ToInt32(genListID);
                                            com3.Parameters.Add(":nPlanID", OracleType.Number).Value = Convert.ToInt32(genidnPlanID);
                                            com3.Parameters.Add(":nDrop", OracleType.Number).Value = dr.dtDrop;
                                            com3.Parameters.Add(":sShipTo", OracleType.VarChar).Value = dr.dtShipto;
                                            com3.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = dr.dtDeliveryNo;
                                            com3.Parameters.Add(":nValue", OracleType.Number).Value = dr.dtValue;
                                            com3.ExecuteNonQuery();
                                        }

                                        _DL += ";" + dr.dtDeliveryNo;

                                    }


                                    #region Webservices iterminal
                                    DateTime dTemp;
                                    DateTime dateplan = DateTime.TryParse(dt.Rows[0]["SPLANDATE"] + "", out dTemp) ? dTemp : DateTime.Now;

                                    sMessageiterminal = itm.UpdateMAP(((_DL.Length > 0) ? _DL.Remove(0, 1) : ""), dt.Rows[0]["STIMEWINDOW"] + "", cboHeadRegist.Value + "", cboTrailerRegist.Value + "", dt.Rows[0]["STERMINALID"] + "", cmbPersonalNo.Value + "", genidnPlanID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                                    #endregion

                                }
                                else
                                {

                                }

                            }
                            Mess = "ยกเลิก/สร้าง";
                        }
                        #endregion

                    }

                    string iMessage = "";
                    if (sMessageiterminal.StartsWith("E"))
                    {
                        iMessage = " iTerminal Error : " + sMessageiterminal.Split(';')[1];
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มแผนงานในระบบได้ : " + iMessage + "');");
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Mess + "แผนงานในระบบ i-Terminal/TMS ผ่าน');");
                    }
                    

                    var dtt = new List<BDT1>();
                    Session["bdt1"] = dtt;

                    //gvw.CancelEdit();
                    gvw1.CancelEdit();
                    gvw2.CancelEdit();
                    gvw3.CancelEdit();


                    if (ASPxPageControl1.ActiveTabIndex == 0)
                    {

                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 2)
                    {
                        Cache.Remove(sds1.CacheKeyDependency);
                        Cache[sds1.CacheKeyDependency] = new object();
                        sds1.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds1.DataBind();
                        gvw1.DataBind();
                    }
                    else if (ASPxPageControl1.ActiveTabIndex == 1)
                    {
                        ListData1();
                        //Cache.Remove(sds2.CacheKeyDependency);
                        //Cache[sds2.CacheKeyDependency] = new object();
                        //sds2.Select(new System.Web.UI.DataSourceSelectArguments());
                        //sds2.DataBind();
                        //gvw2.DataBind();
                    }
                    else
                    {
                        Cache.Remove(sds3.CacheKeyDependency);
                        Cache[sds3.CacheKeyDependency] = new object();
                        sds3.Select(new System.Web.UI.DataSourceSelectArguments());
                        sds3.DataBind();
                        gvw3.DataBind();
                    }

                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                ShowCountCar();
                #endregion
                break;

            case "InsertExceltoDatabase":



                break;

            case "EditClick":
                #region EditClick
                var ggg = new List<BDT1>();
                Session["bdt1"] = ggg;
                index = int.Parse(paras[1]);

                if (ASPxPageControl1.ActiveTabIndex == 0)
                {


                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {

                    gvw2.StartEdit(index);

                    object sterminal = gvw2.GetRowValues(index, "SABBREVIATION");
                    object sterminalID = gvw2.GetRowValues(index, "STERMINAL");
                    object sHeadRegist = gvw2.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw2.GetRowValues(index, "STRAILERREGISTERNO");
                    object SVENDORID = gvw2.GetRowValues(index, "SVENDORID");

                    ASPxTextBox cmbTerminal1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                    //ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                    //ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboTrailerRegist");
                    //ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");
                    //ASPxTextBox txtVendorID = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtVendorID");

                    //dtePlan1.Value = DateTime.Now.ToString("dd/MM/yyyy");
                    //cmbTerminal1.Text = sterminal + "";
                    cmbTerminal1.Text = sterminalID + "";

                    //cboHeadRegist1.Value = sHeadRegist + "";
                    //cboTrailerRegist1.Value = sTrailerRegist + "";
                    //txtVendorID.Text = SVENDORID + string.Empty;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    gvw1.StartEdit(index);

                    object sterminal = gvw1.GetRowValues(index, "STERMINALID");
                    object sHeadRegist = gvw1.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw1.GetRowValues(index, "STRAILERREGISTERNO");
                    object stimeWindow = gvw1.GetRowValues(index, "STIMEWINDOW");
                    object sDELIVERYNO = gvw1.GetRowValues(index, "SDELIVERYNO");

                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboTrailerRegist");
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxTextBox txtDelivery1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDelivery");

                    object SSHIPTO = gvw1.GetRowValues(index, "SSHIPTO");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShiptoName");
                    string sSSHIPTO = CommonFunction.Get_Value(sql, "SELECT C.CUST_NAME FROM TCUSTOMER_SAP c WHERE C.SHIP_TO = '" + SSHIPTO + "'");
                    txtShiptoName1.Text = sSSHIPTO;

                    cmbTerminal1.Value = sterminal + "";
                    cboHeadRegist1.Value = sHeadRegist + "";
                    cboTrailerRegist1.Value = sTrailerRegist + "";
                    cbxTimeWindow1.Value = stimeWindow + "";

                    //cboDelivery_OnItemRequestedByValueSQL(cboDelivery1, new ListEditItemRequestedByValueEventArgs(sDELIVERYNO + ""));
                    txtDelivery1.Text = sDELIVERYNO + "";

                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    gvw3.StartEdit(index);

                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxComboBox cboTrailerRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboTrailerRegist");
                    ASPxTextBox txtDelivery1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDelivery");

                    object sHeadRegist = gvw3.GetRowValues(index, "SHEADREGISTERNO");
                    object sTrailerRegist = gvw3.GetRowValues(index, "STRAILERREGISTERNO");
                    object cType = gvw3.GetRowValues(index, "CFIFO");
                    object sDELIVERYNO = gvw3.GetRowValues(index, "SDELIVERYNO");

                    object SSHIPTO = gvw3.GetRowValues(index, "SSHIPTO");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");
                    string sSSHIPTO = CommonFunction.Get_Value(sql, "SELECT C.CUST_NAME FROM TCUSTOMER_SAP c WHERE C.SHIP_TO = '" + SSHIPTO + "'");
                    txtShiptoName1.Text = sSSHIPTO;

                    txtCTYPE.Text = cType + "";
                    cboHeadRegist1.Value = sHeadRegist + "";
                    cboTrailerRegist1.Value = sTrailerRegist + "";


                    // cboDelivery_OnItemRequestedByValueSQL(cboDelivery1, new ListEditItemRequestedByValueEventArgs(sDELIVERYNO + ""));
                    txtDelivery1.Text = sDELIVERYNO + "";

                    ASPxComboBox cboPersonalNo1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cmbPersonalNo");
                    object SEMPLOYEE = gvw3.GetRowValues(index, "SEMPLOYEEID");

                    cmbPersonalNo_OnItemRequestedByValueSQL(cboPersonalNo1, new ListEditItemRequestedByValueEventArgs(SEMPLOYEE + ""));
                    cboPersonalNo1.Value = SEMPLOYEE + "";

                    if (cType + "" == "1")
                    {
                        cboHeadRegist1.ClientEnabled = false;
                        cboTrailerRegist1.ClientEnabled = false;
                        cboPersonalNo1.ClientEnabled = false;
                    }
                    else
                    {
                        cboHeadRegist1.ClientEnabled = true;
                        cboTrailerRegist1.ClientEnabled = true;
                        cboPersonalNo1.ClientEnabled = true;
                    }

                }

                SetVisibleControl();
                #endregion
                break;


            case "AddDataToList":
                #region AddDataToList
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxTextBox dteDelivery1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw2.FindEditFormTemplateControl("sgvw");
                    ASPxGridView gvDONum = (ASPxGridView)gvw2.FindEditFormTemplateControl("gvDONum");
                    var bdt2 = (List<BDT1>)Session["bdt1"];
                    for (int i = 0; i < gvDONum.VisibleRowCount; i++)
                    {
                        ASPxTextBox txtDrop1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtDrop");
                        ASPxTextBox txtDelivery1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtDelivery");
                        ASPxTextBox txtShipto1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtShipto");
                        ASPxTextBox txtValue1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtValue");

                        ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvDONum.FindRowCellTemplateControl(i, null, "txtShiptoName");



                        int num = 0;
                        int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;

                        if (InDrop > 10)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                            SetVisibleControlAdd();
                            return;
                        }

                        double InParse = 0;
                        string outbounds = txtDelivery1.Text + "";
                        var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                        if (ddd.dtDeliveryNo != null)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                            SetVisibleControlAdd();
                            return;
                        }


                        string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                        int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                        if (CountOutbound > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                            SetVisibleControlAdd();
                            return;
                        }

                        int tempParse = 0;
                        int nTimeWindow = Int32.TryParse("" + cbxTimeWindow1.Value, out tempParse) ? tempParse : 0;

                        var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);

                        if (CheckCarList.dtDeliveryNo != null)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                            SetVisibleControlAdd();
                            return;
                        }

                        int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Char(P.DDELIVERY,'dd/MM/yyyy') = '" + dteDelivery1.Text.Trim() + "' AND p.STERMINALID ='" + cmbTerminal1.Value + "' AND pl.NDROP = " + InDrop);

                        if (CheckCar > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + dteDelivery1.Text.Trim() + " คลังต้นทาง : " + cmbTerminal1.Value + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                            SetVisibleControlAdd();
                            return;
                        }


                        bdt2.Add(new BDT1
                        {
                            dtDrop = InDrop,
                            dtDeliveryNo = txtDelivery1.Text + "",
                            dtShipto = txtShipto1.Text,
                            dtValue = double.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                            sTimeWindow = nTimeWindow,
                            dtShiptoName = txtShiptoName1.Text
                        });
                    }





                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    ASPxComboBox cbxTimeWindow1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cbxTimeWindow");
                    ASPxComboBox cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cboHeadRegist");
                    ASPxTextBox dteDelivery1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");

                    ASPxTextBox txtDrop1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDrop");
                    ASPxTextBox txtDelivery1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtDelivery");
                    ASPxTextBox txtShipto1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtValue");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtShiptoName");

                    var bdt2 = (List<BDT1>)Session["bdt1"];


                    int num = 0;
                    int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;

                    if (InDrop > 10)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                        SetVisibleControlAdd();
                        return;
                    }

                    double InParse = 0;
                    string outbounds = txtDelivery1.Text + "";
                    var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                    if (ddd.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                    int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                    if (CountOutbound > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }
                    int tempParse = 0;
                    int nTimeWindow = Int32.TryParse("" + cbxTimeWindow1.Value, out tempParse) ? tempParse : 0;

                    var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);

                    if (CheckCarList.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Char(P.DDELIVERY,'dd/MM/yyyy') = '" + dteDelivery1.Text.Trim() + "' AND p.STERMINALID ='" + cmbTerminal1.Value + "' AND pl.NDROP = " + InDrop);

                    if (CheckCar > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + dteDelivery1.Text.Trim() + " คลังต้นทาง : " + cmbTerminal1.Value + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    bdt2.Add(new BDT1
                    {
                        dtDrop = InDrop,
                        dtDeliveryNo = txtDelivery1.Text + "",
                        dtShipto = txtShipto1.Text,
                        dtValue = double.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                        sTimeWindow = nTimeWindow,
                        dtShiptoName = txtShiptoName1.Text
                    });

                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    int indexx = gvw3.EditingRowVisibleIndex;
                    string sTimeWindow1 = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                    string sDDELIVERY = gvw3.GetRowValues(indexx, "DDELIVERY") + "";
                    string sSTIMEWINDOW = gvw3.GetRowValues(indexx, "STIMEWINDOW") + "";
                    ASPxComboBox cboHeadRegist1 = (ASPxComboBox)gvw3.FindEditFormTemplateControl("cboHeadRegist");

                    ASPxTextBox txtDrop1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDrop");
                    ASPxTextBox txtDelivery1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtDelivery");
                    ASPxTextBox txtShipto1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShipto");
                    ASPxTextBox txtValue1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtValue");
                    ASPxGridView sgvw1 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                    ASPxTextBox txtShiptoName1 = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtShiptoName");


                    var bdt2 = (List<BDT1>)Session["bdt1"];


                    int num = 0;
                    int InDrop = Int32.TryParse(txtDrop1.Text, out num) ? num : 0;
                    if (InDrop > 10)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่ม Drop No. เกินกว่า 10 Drop ได้');");
                        SetVisibleControlAdd();
                        return;
                    }

                    double InParse = 0;
                    string outbounds = txtDelivery1.Text + "";
                    var ddd = bdt2.FirstOrDefault(s => s.dtDeliveryNo == outbounds);
                    if (ddd.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในรายการแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }


                    string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO = '" + outbounds + "'");
                    int CountOutbound = Int32.TryParse(CountValue, out num) ? num : 0;

                    if (CountOutbound > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Outbound No. " + outbounds + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    int tempParse = 0;
                    int nTimeWindow = Int32.TryParse(sTimeWindow1, out tempParse) ? tempParse : 0;

                    var CheckCarList = bdt2.FirstOrDefault(s => s.sTimeWindow == nTimeWindow && s.dtDrop == InDrop);


                    if (CheckCarList.dtDeliveryNo != null)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี Drop No. " + InDrop + " เที่ยวที่ " + nTimeWindow + " นี้ ในรายการแล้ว ');");
                        SetVisibleControlAdd();
                        return;
                    }

                    DateTime Date1;
                    DateTime csDDELIVERY = DateTime.TryParse(sDDELIVERY, out Date1) ? Date1 : DateTime.Now;
                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '" + cboHeadRegist1.Value + "' AND P.STIMEWINDOW = '" + nTimeWindow + "' AND To_Char(P.DDELIVERY,'dd/MM/yyyy') = '" + csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND p.STERMINALID ='" + sSTIMEWINDOW + "' AND pl.NDROP = " + InDrop);

                    if (CheckCar > 0)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากมี รถทะเบียน : " + cboHeadRegist1.Value + " เที่ยวที่ :" + nTimeWindow + " วันที่จัดส่ง : " + csDDELIVERY.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + " คลังต้นทาง : " + sSTIMEWINDOW + " Drop No. : " + InDrop + " อยู่ในระบบแล้ว');");
                        SetVisibleControlAdd();
                        return;
                    }

                    bdt2.Add(new BDT1
                    {
                        dtDrop = InDrop,
                        dtDeliveryNo = txtDelivery1.Text + "",
                        dtShipto = txtShipto1.Text,
                        dtValue = double.TryParse(txtValue1.Text, out InParse) ? InParse : 0,
                        sTimeWindow = nTimeWindow,
                        dtShiptoName = txtShiptoName1.Text
                    });

                    sgvw1.DataSource = bdt2;
                    sgvw1.DataBind();

                    Session["bdt1"] = bdt2;
                }

                SetVisibleControlAdd();

                #endregion

                break;
            case "AddDONum":
                DataTable dtAdd = new DataTable();
                dtAdd.Columns.Add("DROP", typeof(string));
                dtAdd.Columns.Add("DELIVERY", typeof(string));
                dtAdd.Columns.Add("SHIPTO", typeof(string));
                dtAdd.Columns.Add("VALUE", typeof(string));
                int nums = 0;
                ASPxTextBox txtDONum = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtDONum");
                ASPxGridView gvDONums = (ASPxGridView)gvw2.FindEditFormTemplateControl("gvDONum");
                if (int.TryParse(txtDONum.Text, out nums))
                {
                    DataRow dr;
                    for (int i = 0; i < nums; i++)
                    {
                        dr = dtAdd.NewRow();
                        dtAdd.Rows.Add(dr);
                    }
                    gvDONums.DataSource = dtAdd;
                    gvDONums.DataBind();
                }


                break;
            case "DelClick":
                #region DelClick
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw2.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    ASPxGridView sgvw2 = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
                    string IndexValue = paras[1];
                    var bdt3 = (List<BDT1>)Session["bdt1"];

                    bdt3.RemoveAt(Convert.ToInt32(IndexValue));

                    sgvw2.DataSource = bdt3;
                    sgvw2.DataBind();

                    Session["bdt1"] = bdt3;
                }

                #endregion

                break;

            case "delete":
                #region delete
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {

                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    var ld = gvw2.GetSelectedFieldValues("ID1", "NID")
                  .Cast<object[]>()
                  .Select(s => new { ID1 = s[0].ToString(), NPLANID = s[1].ToString() });

                    string delid = "";
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();
                        foreach (var l in ld)
                        {
                            // Session["delPlanList"] = l.SPLANLISTID;

                            string strsql1 = "UPDATE TFIFO SET CACTIVE = '0' WHERE NID =" + l.NPLANID;
                            using (OracleCommand com = new OracleCommand(strsql1, con))
                            {
                                com.ExecuteNonQuery();
                            }

                            // sds2.Delete();
                            delid += l.NPLANID + ",";

                            //if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPLANSCHEDULEList WHERE CACTIVE = '1' AND nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            //{
                            //    string strsql = "UPDATE TPLANSCHEDULE SET CACTIVE = '0' WHERE nPlanID =" + l.NPLANID;
                            //    //  string strsql = "DELETE FROM TPLANSCHEDULE WHERE nPlanID =" + l.NPLANID;
                            //    using (OracleCommand com = new OracleCommand(strsql, con))
                            //    {
                            //        com.ExecuteNonQuery();
                            //    }
                            //}

                        }
                    }


                    LogUser("11", "D", "ลบข้อมูลหน้า จัดแผนการขนส่ง รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                    gvw2.Selection.UnselectAll();
                    gvw2.DataBind();
                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    var ld = gvw1.GetSelectedFieldValues("ID1", "NPLANID", "NNO", "SPLANDATE", "SPLANTIME", "STERMINALNAME", "STERMINALID", "DDELIVERY", "STIMEWINDOW", "NDROP", "SDELIVERYNO", "SSHIPTO", "SHEADREGISTERNO", "STRAILERREGISTERNO", "NVALUE", "SPLANLISTID")
                   .Cast<object[]>()
                   .Select(s => new { ID1 = s[0].ToString(), NPLANID = s[1].ToString(), NNO = s[2].ToString(), SPLANDATE = s[3].ToString(), SPLANTIME = s[4].ToString(), STERMINALNAME = s[5].ToString(), STERMINALID = s[6].ToString(), DDELIVERY = s[7].ToString(), STIMEWINDOW = s[8].ToString(), NDROP = s[9].ToString(), SDELIVERYNO = s[10].ToString(), SSHIPTO = s[11].ToString(), SHEADREGISTERNO = s[12].ToString(), STRAILERREGISTERNO = s[13].ToString(), NVALUE = s[14].ToString(), SPLANLISTID = s[15].ToString() });

                    string delid = "";

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        con.Open();
                        foreach (var l in ld)
                        {
                            string sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + l.NPLANID + "'");
                            Session["delPlanList"] = l.SPLANLISTID;
                            sds1.Delete();

                            itm.DeleteMAP(sValueDelivery, l.STIMEWINDOW + "", l.SHEADREGISTERNO + "", l.STRAILERREGISTERNO + "", l.STERMINALID + "", "", l.NPLANID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", DateTime.Now);

                            sValueDelivery = CommonFunction.Get_Value(con, "SELECT wm_concat(SDELIVERYNO) AS SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + l.NPLANID + "'");
                            if (sValueDelivery != "")
                            {
                                DateTime dTemp;
                                DateTime dateplan = DateTime.TryParse(l.SPLANDATE + "", out dTemp) ? dTemp : DateTime.Now;

                                #region Webservices iterminal
                                sMessageiterminal = itm.UpdateMAP(sValueDelivery, l.STIMEWINDOW + "", l.SHEADREGISTERNO + "", l.STRAILERREGISTERNO + "", l.STERMINALID + "", "", l.NPLANID + "", Session["UserName"] + "", "", Session["CGROUP"] + "", dateplan);
                                #endregion
                            }

                            delid += l.SPLANLISTID + ",";

                            if (CommonFunction.Count_Value(con, "SELECT sPlanListID FROM TPlanScheduleList WHERE nPlanID = " + Convert.ToInt32(l.NPLANID)) == 0)
                            {
                                string strsql = "DELETE FROM TPlanSchedule WHERE nPlanID =" + l.NPLANID;
                                using (OracleCommand com = new OracleCommand(strsql, con))
                                {
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    LogUser("11", "D", "ลบข้อมูลหน้า จัดแผนการขนส่ง รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                    gvw1.Selection.UnselectAll();
                    gvw1.DataBind();
                }

                #endregion
                break;

            case "Cancel":
                #region Cancel
                if (ASPxPageControl1.ActiveTabIndex == 0)
                {
                }
                else if (ASPxPageControl1.ActiveTabIndex == 1)
                {
                    gvw2.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;

                }
                else if (ASPxPageControl1.ActiveTabIndex == 2)
                {
                    gvw1.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;

                }
                else if (ASPxPageControl1.ActiveTabIndex == 3)
                {
                    gvw3.CancelEdit();
                    var dt = new List<BDT1>();
                    Session["bdt1"] = dt;
                }
                #endregion

                break;

            case "SearchPlan":
                gvw3.CancelEdit();
                gvw3.DataBind();
                break;
            case "Search":
                gvw2.DataSource = FIFOBLL.Instance.GetFIFO1(cboTerminal2.Value + string.Empty, txtEmployee.Text, cboVendor.Value + string.Empty);
                gvw2.DataBind();
                break;
            case "Claer":
                cboTerminal2.Value = null;
                cboVendor.Value = null;
                txtEmployee.Text = string.Empty;
                break;
        }

    }
    #endregion
    
    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "" && (_Filename[1].ToLower().ToString().Trim().Equals("xls") || _Filename[1].ToLower().ToString().Trim().Equals("xlsx")))
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            string genName = _Filename[0].Replace(" ", "") + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("en-US"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                //SaveImportDetail(_Filename[0], genName);
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[1];
                e.CallbackData = data;

                Session["sDataUpload"] = data;
                Session["sShowPopup"] = null;
            }
        }
        else
        {

            return;

        }

    }

    protected void fifouplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "" && (_Filename[1].ToLower().ToString().Trim().Equals("xls") || _Filename[1].ToLower().ToString().Trim().Equals("xlsx")))
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            string genName = _Filename[0].Replace(" ", "") + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("en-US"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                //SaveImportDetail(_Filename[0], genName);
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[1];
                e.CallbackData = data;

                Session["fifosDataUpload"] = data;
                Session["fifosShowPopup"] = null;
            }
        }
        else
        {

            return;

        }

    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    void SaveImportDetail(string filename, string genname)
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            string strsql = "INSERT INTO TIMPORTFILE(NIMPORTID,SFILENAME,SGENFILENAME,SUSERNAME,SCREATE,DCREATE) VALUES (:NIMPORTID,:SFILENAME,:SGENFILENAME,:SUSERNAME,:SCREATE,SYSDATE)";
            using (OracleCommand com = new OracleCommand(strsql, con))
            {
                string genImportID = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TIMPORTFILE ORDER BY NIMPORTID DESC) WHERE ROWNUM <= 1");
                com.Parameters.Clear();
                com.Parameters.Add(":NIMPORTID", OracleType.Number).Value = Convert.ToInt32(genImportID);
                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = filename;
                com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = genname;
                com.Parameters.Add(":SUSERNAME", OracleType.VarChar).Value = Session["UserName"] + "";
                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                com.ExecuteNonQuery();
            }
        }
    }
    protected void cmbTerminal_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        comboBox.DataSource = ContractBLL.Instance.TTERMINALSelect();
        comboBox.DataBind();
    }
    protected void cmbTerminal_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxTextBox dtePlan = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");


        if (dtePlan != null && Terminal != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRANSPORTID, SHEADREGISTERNO,STRAILERREGISTERNO, '' AS DWATEREXPIRE,SVENDORNAME,NCAPACITY 
FROM(SELECT T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,CASE WHEN Tc.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = Tc.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.SHEADREGISTERNO) AS RN 
FROM  ((((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON TC.SHEADID = T.STRUCKID) 
LEFT JOIN TVENDOR_SAP v ON T.STRANSPORTID = V.SVENDORID)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE('" + date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/mm/yyyy') - 1 /*AND nvl(ct.STERMINALID,'" + Terminal.Value + "') LIKE '%" + Terminal.Value + "%'*/  GROUP BY  T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboTrailerRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxTextBox dtePlan = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
        ASPxTextBox VendorID = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtVendorID");

        if (dtePlan != null && Terminal != null && VendorID != null)
        {
            DateTime date;
            DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY 
FROM(SELECT Tc.STRAILERREGISTERNO, nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.STRAILERREGISTERNO) AS RN 
FROM  (((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON T.SHEADREGISTERNO =  Tc.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE Tc.STRAILERREGISTERNO IS NOT NULL AND nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'
 AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE('" + date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/mm/yyyy') - 1 /*AND nvl(ct.STERMINALID,'" + Terminal.Value + "') LIKE '%" + Terminal.Value + "%'*/ AND ct.SVENDORID = '" + VendorID.Text + "' GROUP BY Tc.STRAILERREGISTERNO,T.NTOTALCAPACITY ) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cbxTimeWindow_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox cmbTerminal1 = null;
        ASPxTextBox txtTerminal1 = null;
        ASPxTextBox dteDelivery1 = null;

        if (ASPxPageControl1.ActiveTabIndex == 1)
        {

            txtTerminal1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
            dteDelivery1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {

            cmbTerminal1 = (ASPxComboBox)gvw1.FindEditFormTemplateControl("cmbTerminal");
            dteDelivery1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");
        }
        string value = string.Empty;
        if (cmbTerminal1 !=null)
        {
            value = cmbTerminal1.Value + string.Empty;
        }
        else if (txtTerminal1 != null)
        {
            value = txtTerminal1.Text;
        }
        if (dteDelivery1 != null)
        {
            if (value == "H102")
            {
                DateTime date1;
                DateTime date = DateTime.TryParse(dteDelivery1.Value + "", out date1) ? date1 : DateTime.Now;
                if ((int)date.DayOfWeek == 6 || (int)date.DayOfWeek == 0)
                {
                    txtCDAYTYPE.Text = "1";
                }
                else if (CommonFunction.Count_Value(sql, "SELECT * FROM LSTHOLIDAY WHERE TRUNC(DHOLIDAY) = TO_DATE('" + date.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')") > 0)
                {
                    txtCDAYTYPE.Text = "1";
                }
                else
                {
                    txtCDAYTYPE.Text = "0";
                }
            }
            else
            {
                txtCDAYTYPE.Text = "0";
            }
                ASPxComboBox comboBox = (ASPxComboBox)source;
                sdsTimeWindow.SelectCommand = @"SELECT        NLINE, 'เที่ยวที่ ' || NLINE || ' เวลา ' ||  TSTART || ' - ' || TEND AS WINDOWTIME
FROM            TWINDOWTIME
WHERE 'เที่ยวที่ ' || NLINE || ' เวลา ' ||  TSTART || ' - ' || TEND LIKE :fillter AND STERMINALID = :STERMINALID AND CDAYTYPE = :CDAYTYPE ORDER BY NLINE";

                sdsTimeWindow.SelectParameters.Clear();
                sdsTimeWindow.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
                sdsTimeWindow.SelectParameters.Add("STERMINALID", TypeCode.String, value);
                sdsTimeWindow.SelectParameters.Add("CDAYTYPE", TypeCode.String, txtCDAYTYPE.Text);

                comboBox.DataSource = sdsTimeWindow;
                comboBox.DataBind();
            
        }

    }
    protected void cbxTimeWindow_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboHeadRegisTFIFO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxTextBox dtePlan = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");

        if (dtePlan != null && Terminal != null)
        {
            DateTime date;
            //DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;
            DateTime date1 = (!string.Equals(dtePlan.Text.Trim(), string.Empty)) ? DateTime.ParseExact(dtePlan.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US")) : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;

            sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRANSPORTID,SVENDORNAME,'' AS DWATEREXPIRE,NCAPACITY FROM( 

SELECT f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID AS STRANSPORTID,V.SVENDORNAME,
  CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN
  
 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) END AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.SHEADREGISTERNO) AS RN 
FROM  ((TFIFO f  LEFT JOIN TVENDOR_SAP v ON F.SVENDORID  = V.SVENDORID) LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID
WHERE NVL(f.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yy') = TO_DATE(:DPLAN,'dd/MM/yy') AND f.STERMINAL LIKE :TERMINAL  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID ,V.SVENDORNAME,T.NTOTALCAPACITY  ) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy"));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal.Value + ""));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegisTFIFO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboTrailerRegisTFIFO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxTextBox dtePlan = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");
        ASPxComboBox Terminal = (ASPxComboBox)gvw2.FindEditFormTemplateControl("cmbTerminal");
        ASPxTextBox VendorID = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtVendorID");

        if (dtePlan != null && Terminal != null && VendorID != null)
        {
            DateTime date;
            //DateTime date1 = DateTime.TryParse(dtePlan.Value + "", out date) ? date : DateTime.Now;
            DateTime date1 = (!string.Equals(dtePlan.Text.Trim(), string.Empty)) ? DateTime.ParseExact(dtePlan.Text.Trim(), "dd/MM/yyyy", new CultureInfo("en-US")) : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;
            sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY FROM( 

SELECT f.STRAILERREGISTERNO , nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0)))  AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.STRAILERREGISTERNO) AS RN 
FROM  (TFIFO f   LEFT JOIN TTRUCK t ON t.SHEADREGISTERNO = f.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID 
WHERE NVL(f.CACTIVE,'1') = '1' AND  f.STRAILERREGISTERNO IS NOT NULL AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yy') = TO_DATE(:DPLAN,'dd/MM/yy') AND f.STERMINAL LIKE :TERMINAL AND SVENDORID = :SVENDORID  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO ,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy"));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal.Value + ""));
            sdsTruck.SelectParameters.Add("SVENDORID", TypeCode.String, VendorID.Value + "");
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegisTFIFO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SEMPLOYEEID,SPERSONELNO,FULLNAME,STEL,SPERSONELNO FROM (SELECT E.SEMPLOYEEID,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,E.SPERSONELNO, ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND E.INAME || ES.FNAME || ' ' || ES.LNAME || E.SPERSONELNO LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT E.SEMPLOYEEID,E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE  E.SEMPLOYEEID = :fillter ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, e.Value + "");

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();
    }
    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        if ((e.Filter.StartsWith("8") && e.Filter.Length == 8) || e.Filter.Length == 10)
        {

            ASPxComboBox comboBox = (ASPxComboBox)source;
            //            sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(
            //SELECT d.DELIVERY_NO,d.SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
            //,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
            //, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
            //FROM TDELIVERY d  
            //LEFT JOIN TCUSTOMER_SAP ct ON D.SHIP_TO = CT.SHIP_TO  
            //WHERE  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-7 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+7 AND d.DELIVERY_NO = :fillter 
            //) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsDelivery.SelectCommand = @"SELECT DELIVERY_NO,SHIP_TO,SCUSTOMER,NVALUE FROM(
SELECT d.DELIVERY_NO,(CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
FROM TDELIVERY d  
LEFT JOIN 
(
    SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
)TRN
ON TRN.DOC_NO = d.SALES_ORDER
LEFT JOIN 
(
    SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
)TBO
ON TBO.DOCNO = d.SALES_ORDER
LEFT JOIN TCUSTOMER_SAP ct ON CT.SHIP_TO  =  (CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END)
WHERE  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-7 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+7 AND d.DELIVERY_NO = :fillter 
) WHERE RN BETWEEN :startIndex AND :endIndex ";

            sdsDelivery.SelectParameters.Clear();
            sdsDelivery.SelectParameters.Add("fillter", TypeCode.String, String.Format("{0}", e.Filter.PadLeft(10, '0')));
            sdsDelivery.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsDelivery.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsDelivery;
            comboBox.DataBind();

            if (comboBox.Items.Count <= 0)
            {
                //comboBox.Value = "";
                //comboBox.JSProperties["cpPopup"] = "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่มี Outbound " + e.Filter.PadLeft(10, '0') + " ในระบบ');";

                if (GetOutBound(e.Filter))
                {
                    comboBox.JSProperties["cpPopup"] = "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่มี Outbound " + e.Filter + " ในระบบ SAP Master');";
                }
                else
                {
                    comboBox.JSProperties["cpPopup"] = "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','นำเข้าข้อมูล Outbound " + e.Filter + " เรียบร้อยแล้ว');";
                }

            }
        }

    }
    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    [Serializable]
    struct BDT1
    {
        public int dtDrop { get; set; }
        public string dtDeliveryNo { get; set; }
        public string dtShipto { get; set; }
        public double dtValue { get; set; }
        public int sTimeWindow { get; set; }
        public string dtShiptoName { get; set; }
    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    void SetVisibleControl()
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {

        }
        else if (ASPxPageControl1.ActiveTabIndex == 1)
        {
            TextBox dtePlan1 = (TextBox)gvw2.FindEditFormTemplateControl("dtePlan");
            ASPxTextBox txtTime1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("txtTime");
            ASPxTextBox dteDelivery1 = (ASPxTextBox)gvw2.FindEditFormTemplateControl("dteDelivery");

            dtePlan1.Text = DateTime.Now.ToString("dd/MM/yyyy");
            dteDelivery1.Value = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw1.FindEditFormTemplateControl("btnsAdd");
            ASPxLabel lblShow = (ASPxLabel)gvw1.FindEditFormTemplateControl("lblShow");
            ASPxTextBox dtePlan1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dtePlan");
            ASPxTextBox txtTime1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("txtTime");
            ASPxTextBox dteDelivery1 = (ASPxTextBox)gvw1.FindEditFormTemplateControl("dteDelivery");

            if (gvw1.IsNewRowEditing == true)
            {
                lblShow.Text = "เพิ่ม / Add";
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;
                dtePlan1.Value = DateTime.Now.ToString("dd/MM/yyyy");
                txtTime1.Text = DateTime.Now.ToString("HH.mm");
                dteDelivery1.Value = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else
            {
                lblShow.Text = "แก้ไข / Edit";
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }
        else if (ASPxPageControl1.ActiveTabIndex == 3)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw3.FindEditFormTemplateControl("btnsAdd");
            ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

            if (rblCheck.SelectedIndex == 2)
            {
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;

            }
            else
            {
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }

    }
    void SetVisibleControlAdd()
    {
        if (ASPxPageControl1.ActiveTabIndex == 0)
        {

        }
        else if (ASPxPageControl1.ActiveTabIndex == 2)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw1.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw1.FindEditFormTemplateControl("btnsAdd");
            ASPxLabel lblShow = (ASPxLabel)gvw1.FindEditFormTemplateControl("lblShow");

            if (gvw1.IsNewRowEditing == true)
            {
                lblShow.Text = "เพิ่ม / Add";
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;
            }
            else
            {
                lblShow.Text = "แก้ไข / Edit";
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }
        else if (ASPxPageControl1.ActiveTabIndex == 3)
        {
            ASPxGridView sgvw = (ASPxGridView)gvw3.FindEditFormTemplateControl("sgvw");
            ASPxButton btnsAdd = (ASPxButton)gvw3.FindEditFormTemplateControl("btnsAdd");
            ASPxRadioButtonList rblCheck = (ASPxRadioButtonList)gvw3.FindEditFormTemplateControl("rblCheck");

            if (rblCheck.SelectedIndex == 2)
            {
                sgvw.ClientVisible = true;
                btnsAdd.ClientVisible = true;

            }
            else
            {
                sgvw.ClientVisible = false;
                btnsAdd.ClientVisible = false;
            }
        }

    }
    void listImportFileDetail()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT ROW_NUMBER() OVER(ORDER BY NIMPORTID) AS ID,SUSERNAME,DCREATE FROM TIMPORTFILE WHERE nvl(CFIFO,'') <> '1' AND to_char(DCREATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy')");
        if (dt.Rows.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            string str = "<td><img src='images/ic_ms_excel.gif' width='16' height='16' /> ครั้งที่ {0} {1} {2} น.</td>";
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows.IndexOf(dr) % 2 == 0)
                {
                    sb.Append("<tr>");
                }

                sb.Append(string.Format(str, dr["ID"], dr["SUSERNAME"], Convert.ToDateTime(dr["DCREATE"]).ToShortTimeString()));

                if (dt.Rows.IndexOf(dr) % 2 != 0)
                {
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");

            //ltlHistory.Text = sb.ToString();
            ltlHistory1.Text = sb.ToString();

        }
    }
    void fifolistImportFileDetail()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT ROW_NUMBER() OVER(ORDER BY NIMPORTID) AS ID,SUSERNAME,DCREATE FROM TIMPORTFILE WHERE nvl(CFIFO,'') = '1' AND to_char(DCREATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy')");
        if (dt.Rows.Count > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            string str = "<td><img src='images/ic_ms_excel.gif' width='16' height='16' /> ครั้งที่ {0} {1} {2} น.</td>";
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows.IndexOf(dr) % 2 == 0)
                {
                    sb.Append("<tr>");
                }

                sb.Append(string.Format(str, dr["ID"], dr["SUSERNAME"], Convert.ToDateTime(dr["DCREATE"]).ToShortTimeString()));

                if (dt.Rows.IndexOf(dr) % 2 != 0)
                {
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");

            //ltlHistory.Text = sb.ToString();
            fifoltlHistory1.Text = sb.ToString();

        }
    }
    protected void gvw1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        
        switch (e.CallbackName)
        {
            case "SORT":
                //gvw.CancelEdit();
                gvw1.CancelEdit();
                gvw2.CancelEdit();
                gvw3.CancelEdit();
                ListData1();
                var dtt = new List<BDT1>();
                Session["bdt1"] = dtt;
                break;
            case "PAGERONCLICK": ListData1();
                break;
        }
    }
    private void ShowCountCar()
    {
        string sTerminal = CommonFunction.ReplaceInjection(cboTerminal2.Value + "");
        string sWhere = (sTerminal != "") ? " AND STERMINAL = '" + sTerminal + "' " : "";

        string nCountFIFO = CommonFunction.Get_Value(sql, "SELECT COUNT(*) AS nCountFIFO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' " + sWhere + " AND (nvl(CPLAN,'0') = '0') AND TO_DATE(DDATE,'dd/MM/yyyy') = TO_DATE(SYSDATE,'dd/MM/yyyy')");
        //รอจัด
        lblCarWait.Text = nCountFIFO;


        //Tab2 Plan


        string _ddate = (ASPxPageControl1.ActiveTabIndex == 2) ? dtePlan1.Text.Trim() : DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"))
            , _condition = (ASPxPageControl1.ActiveTabIndex == 3 ? "" : " AND nvl(STERMINALID,'" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "') LIKE '%" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "%'")
            , _condition1 = (ASPxPageControl1.ActiveTabIndex == 3 ? "" : " AND nvl(ct.STERMINALID,'" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "') LIKE '%" + CommonFunction.ReplaceInjection(cboTerminal1.Value + "") + "%'");

        string nCarConfirm = CommonFunction.Get_Value(sql, "SELECT COUNT(tc.sHeadRegisterNo) AS nCarCount FROM (TTRUCKCONFIRM t INNER JOIN TTruckConfirmList tc ON t.NCONFIRMID = tc.NCONFIRMID)LEFT JOIN TCONTRACT ct ON t.SCONTRACTID = ct.SCONTRACTID WHERE tc.cConfirm = '1'  AND TRUNC(t.DDATE) = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') - 1 " + _condition1);

        string nCarPlan = CommonFunction.Get_Value(sql, "SELECT COUNT(tc.sHeadRegisterNo) AS nCarCount FROM ((TTRUCKCONFIRM t INNER JOIN TTruckConfirmList tc ON t.NCONFIRMID = tc.NCONFIRMID) LEFT JOIN TCONTRACT ct ON t.SCONTRACTID = ct.SCONTRACTID) INNER JOIN (SELECT sHeadRegisterNo FROM TPlanSchedule WHERE CACTIVE = '1' AND TRUNC(SPLANDATE) = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') " + _condition + " GROUP BY sHeadRegisterNo) p ON tc.sHeadRegisterNo = p.sHeadRegisterNo   WHERE (tc.cConfirm = '1'  AND TRUNC(t.DDATE) = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') - 1)  " + _condition1);

        string nCarPlanFIFO = CommonFunction.Get_Value(sql, "SELECT COUNT(sHeadRegisterNo) AS nCarCount FROM TPlanSchedule WHERE CFIFO = '1' AND CACTIVE = '1' AND TRUNC(SPLANDATE) = TO_DATE('" + CommonFunction.ReplaceInjection(_ddate) + "','dd/MM/yyyy') " + _condition);

        int num;
        int nnCarConfirm = int.TryParse(nCarConfirm, out num) ? num : 0;
        int nnCarPlan = int.TryParse(nCarPlan, out num) ? num : 0;
        int nnCarPlanFifo = int.TryParse(nCarPlanFIFO, out num) ? num : 0;
        // int sumCarConfirm = nnCarConfirm - nnCarPlan;


        int sumCarPlan = nnCarPlan + nnCarPlanFifo;
        // คัน
        lblShowConfirm1.Text = nCarConfirm;
        // จัดแล้ว
        lblShowBookCar1.Text = nCarPlan + "";


        //Tab3 

        // ยืนยันรถตามสัญญา
        lblShowConfirm2.Text = nCarConfirm;
        //จัดแล้ว
        lblShowBookCar2.Text = sumCarPlan + "";
        lblDateNow.Text = DateTime.Today.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
    }
    protected void cboHeadRegistFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";
            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;


            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRANSPORTID, SHEADREGISTERNO,STRAILERREGISTERNO, '' AS DWATEREXPIRE,SVENDORNAME,NCAPACITY 
FROM(SELECT T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,CASE WHEN Tc.STRAILERREGISTERNO IS NOT NULL THEN

 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = Tc.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) END AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.SHEADREGISTERNO) AS RN 
FROM  ((((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON TC.SHEADID = T.STRUCKID) 
LEFT JOIN TVENDOR_SAP v ON T.STRANSPORTID = V.SVENDORID)
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID    WHERE nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/MM/yyyy') - 1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL  GROUP BY  T.STRANSPORTID, Tc.SHEADREGISTERNO,Tc.STRAILERREGISTERNO,T.DWATEREXPIRE,v.SVENDORNAME,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRANSPORTID,SVENDORNAME,'' AS DWATEREXPIRE,NCAPACITY FROM( 

SELECT f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID AS STRANSPORTID,V.SVENDORNAME,
  CASE WHEN f.STRAILERREGISTERNO IS NOT NULL THEN
  
 (SELECT nvl(Tt.NTOTALCAPACITY, SUM(nvl(ttc.NCAPACITY,0))) FROM TTRUCK tt LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) ttc ON Tt.STRUCKID = TtC.STRUCKID  WHERE tt.SHEADREGISTERNO = f.STRAILERREGISTERNO
 GROUP BY Tt.NTOTALCAPACITY)
 
  ELSE nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) END AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.SHEADREGISTERNO) AS RN 
FROM  ((TFIFO f  LEFT JOIN TVENDOR_SAP v ON F.SVENDORID  = V.SVENDORID) LEFT JOIN TTRUCK t ON F.SHEADREGISTERNO = T.SHEADREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID
WHERE f.CACTIVE = '1' AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO , F.SVENDORID ,V.SVENDORNAME,T.NTOTALCAPACITY  ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal + ""));
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboHeadRegistFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboTrailerRegistFIFOPLAN_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxTextBox VendorID = (ASPxTextBox)gvw3.FindEditFormTemplateControl("txtVendorID");

        if (VendorID != null)
        {
            string Terminal = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "STERMINALID") + "";
            string DPLAN = gvw3.GetRowValues(gvw3.EditingRowVisibleIndex, "SPLANDATE") + "";

            DateTime date;
            DateTime date1 = DateTime.TryParse(DPLAN + "", out date) ? date : DateTime.Now;

            ASPxComboBox comboBox = (ASPxComboBox)source;

            if (("" + txtCTYPE.Text).Equals("0"))
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY 
FROM(SELECT Tc.STRAILERREGISTERNO, nvl(T.NTOTALCAPACITY, SUM(nvl(tc1.NCAPACITY,0))) AS NCAPACITY, 
ROW_NUMBER()OVER(ORDER BY Tc.STRAILERREGISTERNO) AS RN 
FROM  (((TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
INNER JOIN TTRUCK t ON T.SHEADREGISTERNO =  Tc.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc1 ON T.STRUCKID = TC1.STRUCKID) 
LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  WHERE Tc.STRAILERREGISTERNO IS NOT NULL AND  nvl(T.DWATEREXPIRE,SYSDATE) >= SYSDATE AND TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE :fillter AND TF.DDATE = TO_DATE(:DPLAN,'dd/mm/yyyy') - 1 AND nvl(ct.STERMINALID,:TERMINAL) LIKE :TERMINAL AND ct.SVENDORID = :SVENDORID  GROUP BY Tc.STRAILERREGISTERNO,T.NTOTALCAPACITY ) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }
            else
            {
                sdsTruck.SelectCommand = @"SELECT STRAILERREGISTERNO,NCAPACITY FROM( 

SELECT f.STRAILERREGISTERNO , nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0)))  AS NCAPACITY, 
  ROW_NUMBER()OVER(ORDER BY f.STRAILERREGISTERNO) AS RN 
FROM  (TFIFO f   LEFT JOIN TTRUCK t ON t.SHEADREGISTERNO = f.STRAILERREGISTERNO) 
LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON T.STRUCKID = TC.STRUCKID 
WHERE f.CACTIVE = '1' AND  f.STRAILERREGISTERNO IS NOT NULL AND f.SHEADREGISTERNO LIKE :fillter AND TO_DATE(f.DDATE,'dd/MM/yyyy') = TO_DATE(:DPLAN,'dd/MM/yyyy') AND f.STERMINAL LIKE :TERMINAL AND SVENDORID = :SVENDORID  GROUP BY  f.SHEADREGISTERNO,f.STRAILERREGISTERNO,T.NTOTALCAPACITY) WHERE RN BETWEEN :startIndex AND :endIndex ";
            }

            sdsTruck.SelectParameters.Clear();
            sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
            sdsTruck.SelectParameters.Add("DPLAN", TypeCode.String, date1.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            sdsTruck.SelectParameters.Add("TERMINAL", TypeCode.String, String.Format("%{0}%", Terminal + ""));
            sdsTruck.SelectParameters.Add("SVENDORID", TypeCode.String, VendorID.Text + "");
            sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
            sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

            comboBox.DataSource = sdsTruck;
            comboBox.DataBind();
        }

    }
    protected void cboTrailerRegistFIFOPLAN_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void lkbconfirmtruck_Click(object sender, EventArgs e)
    {
        Response.Redirect("ConfirmTruck.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt(CommonFunction.ReplaceInjection(dtePlan1.Text.Trim()) + "&" + CommonFunction.ReplaceInjection(cboTerminal1.Value + ""))));
    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    protected void xcpnOutBound_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string paras = CommonFunction.ReplaceInjection(e.Parameter);
        ASPxCallback xcpnOutBound = (ASPxCallback)sender;
        if ((paras.StartsWith("8") && paras.Length == 8) || paras.Length == 10)
        {

            DataTable dtOutbount = GetCheckOutbound(paras.PadLeft(10, '0'));


            if (dtOutbount.Rows.Count > 0)
            {

                xcpnOutBound.JSProperties["cpPopup"] = "txtShipto.SetValue('" + dtOutbount.Rows[0]["SHIP_TO"] + "') ; txtShiptoName.SetValue('" + dtOutbount.Rows[0]["SCUSTOMER"] + "') ;txtValue.SetValue('" + dtOutbount.Rows[0]["NVALUE"] + "') ;";

            }
            else
            {

                if (GetOutBound(paras))
                {

                    dtOutbount = GetCheckOutbound(paras.PadLeft(10, '0')); ;


                    if (dtOutbount.Rows.Count > 0)
                    {

                        xcpnOutBound.JSProperties["cpPopup"] = "txtShipto.SetValue('" + dtOutbount.Rows[0]["SHIP_TO"] + "') ; txtShiptoName.SetValue('" + dtOutbount.Rows[0]["SCUSTOMER"] + "') ;txtValue.SetValue('" + dtOutbount.Rows[0]["NVALUE"] + "') ;";

                    }
                    else
                    {
                        xcpnOutBound.JSProperties["cpPopup"] = "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่มี Outbound " + paras + " ในระบบ');txtShipto.SetValue('') ; txtShiptoName.SetValue('') ;txtValue.SetValue('') ;";
                    }

                }
                else
                {
                    xcpnOutBound.JSProperties["cpPopup"] = "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่มี Outbound " + paras + " ในระบบ SAP Master');txtShipto.SetValue('') ; txtShiptoName.SetValue('') ;txtValue.SetValue('') ;";
                }

            }

        }

    }

    private DataTable GetCheckOutbound(string sPara)
    {
        //        string sqlString = @"SELECT d.DELIVERY_NO,d.SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
        //,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
        //, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
        //FROM TDELIVERY d  
        //LEFT JOIN TCUSTOMER_SAP ct ON D.SHIP_TO = CT.SHIP_TO  
        //WHERE  d.DELIVERY_NO = '" + sPara.PadLeft(10, '0') + "' AND  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-3 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+3";

        string sqlString = @"SELECT d.DELIVERY_NO,(CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END) SHIP_TO,d.ct.CUST_NAME AS SCUSTOMER
,nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0) AS NVALUE
, ROW_NUMBER()OVER(ORDER BY d.DELIVERY_NO DESC) AS RN 
FROM TDELIVERY d  
LEFT JOIN 
(
    SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
)TRN
ON TRN.DOC_NO = d.SALES_ORDER
LEFT JOIN 
(
    SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
)TBO
ON TBO.DOCNO = d.SALES_ORDER
LEFT JOIN TCUSTOMER_SAP ct ON CT.SHIP_TO  =  (CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END)
WHERE  d.DELIVERY_NO = '" + sPara.PadLeft(10, '0') + "' AND  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-7 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+7";

        DataTable dtOutbount = CommonFunction.Get_Data(sql, sqlString);

        return dtOutbount;
    }

    private DataTable GetNVALUE(string sPara)
    {
        string sqlString = @"SELECT sum(nvl(d.ULG,0) + nvl(d.ULR,0) + nvl(d.HSD,0) + nvl(d.LSD,0) + nvl(d.IK,0) + nvl(d.GH,0) + nvl(d.PL,0) + nvl(d.GH95,0) + nvl(d.GH95E20,0) + nvl(d.GH95E85,0) + nvl(d.HSDB5,0) + nvl(d.OTHER,0)) NVALUE
FROM TDELIVERY d  
LEFT JOIN 
(
    SELECT DISTINCT DOC_NO,SHIP_ID,REV_CODE FROM TRANS_ORDER   WHERE NVL(VALTYP,'-') !='REBRAND' 
)TRN
ON TRN.DOC_NO = d.SALES_ORDER
LEFT JOIN 
(
    SELECT DISTINCT DOCNO,RECIEVEPOINT FROM TBORDER WHERE RECIEVEPOINT IS NOT NULL
)TBO
ON TBO.DOCNO = d.SALES_ORDER
LEFT JOIN TCUSTOMER_SAP ct ON CT.SHIP_TO  =  (CASE WHEN d.DELIVERY_NO LIKE '008%' THEN (CASE WHEN LENGTH(TRN.REV_CODE)=4  THEN LPAD('9'||NVL(TRN.REV_CODE,TBO.RECIEVEPOINT),10,'0') ELSE NVL(TRN.REV_CODE,LPAD('9'||TBO.RECIEVEPOINT,10,'0')) END)  ELSE   NVL(d.SHIP_TO,TRN.SHIP_ID) END)
WHERE  d.DELIVERY_NO in (" + sPara + ") AND  trunc(d.DELIVERY_DATE) >= TRUNC(SYSDATE)-7 AND trunc(d.DELIVERY_DATE) <= TRUNC(SYSDATE)+7";

        DataTable dtOutbount = CommonFunction.Get_Data(sql, sqlString);

        return dtOutbount;
    }

    protected bool GetOutBound(string sOutBound)
    {
        string paras = sOutBound;
        string _outbound = "";
        using (OracleConnection con = new OracleConnection(sql))
        {
            if ("" + paras == "") return false;

            string para = sOutBound.PadLeft(10, '0');


            if (con.State == ConnectionState.Closed) con.Open();

            using (OracleCommand com = new OracleCommand("SCHD_DELIVERY_GET", con))
            {
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.Add("S_OUTBOUND", OracleType.VarChar).Value = para;

                com.ExecuteNonQuery();
            }

            string scount = CommonFunction.Get_Value(con, "SELECT COUNT(*) FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') =LPAD('" + CommonFunction.ReplaceInjection(paras) + "',10,'0')") + "";


            if (scount == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }

    private string GetDOFROMPLAN(string NPLANID)
    {
        string RESULT = "";

        DataTable DT_PLAN = CommonFunction.Get_Data(sql, "SELECT SDELIVERYNO FROM TPLANSCHEDULELIST WHERE NPLANID = '" + CommonFunction.ReplaceInjection(NPLANID) + "'  AND CACTIVE = '1'");
        if (DT_PLAN.Rows.Count > 0)
        {

            for (int i = 0; i < DT_PLAN.Rows.Count; i++)
            {
                RESULT += "," + DT_PLAN.Rows[i]["SDELIVERYNO"];
            }

        }
        return RESULT;
    }

   private void ListData1()
    {
        gvw2.DataSource = FIFOBLL.Instance.GetFIFO1(cboTerminal2.Value + string.Empty, txtEmployee.Text, cboVendor.Value + string.Empty);
        gvw2.DataBind();
    }
}

