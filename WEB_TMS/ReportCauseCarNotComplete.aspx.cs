﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraReports.UI;
using System.Globalization;

public partial class ReportCauseCarNotComplete : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);


        ListData();
        //CreateReport();
    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.ToString() == "ที่")
        {
            //int sss =   gvw.GroupSummarySortInfo.IndexOf;  
            //e.Cell.Text = e.VisibleIndex + "";

        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');
        string SUSERID = Session["UserID"] + "";
        string VENID = CommonFunction.ReplaceInjection(cboVendor.Value + "");

        switch (paras[0])
        {
            case "search":
                ListData();
                break;
        }
    }

    void ListData()
    {
        string Codition = "";
        if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
        {
            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Codition += " AND (TRUNC(NVL(LA.DUPDATE,LA.DCREATE)) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;
        }

        if (!string.IsNullOrEmpty(cboVendor.Value + ""))
        {
            Codition += " AND TVEN.SVENDORID = '" + CommonFunction.ReplaceInjection(cboVendor.Value + "") + "'";
            lblsTail.Text = " - ";
        }

        string Query = @"SELECT ROW_NUMBER() OVER (PARTITION BY TVEN.SABBREVIATION  ORDER BY MS.NORDER)||'.' as NO
, TVEN.SABBREVIATION  ,MS.CHECKLIST_ID,MS.CHECKLIST_NAME_REPORT,--COUNT(MS.CHECKLIST_ID) as SCOUNT,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '01' THEN 1  ELSE 0 END) as M1,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '02' THEN 1  ELSE 0 END) as M2,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '03' THEN 1  ELSE 0 END) as M3,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '04' THEN 1  ELSE 0 END) as M4,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '05' THEN 1  ELSE 0 END) as M5,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '06' THEN 1  ELSE 0 END) as M6,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '07' THEN 1  ELSE 0 END) as M7,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '08' THEN 1  ELSE 0 END) as M8,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '09' THEN 1  ELSE 0 END) as M9,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '10' THEN 1  ELSE 0 END) as M10,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '11' THEN 1  ELSE 0 END) as M11,
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '12' THEN 1  ELSE 0 END) as M12 
,MS.NORDER
,TVEN.SVENDORID
,SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '01' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '02' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '03' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '04' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '05' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '06' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '07' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '08' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '09' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '10' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '11' THEN 1  ELSE 0 END)+
SUM(CASE WHEN TO_CHAR(NVL(LA.DUPDATE,LA.DCREATE),'MM') = '12' THEN 1  ELSE 0 END) as TOTAL
,'รวมสาเหตุรถบรรทุกที่ตรวจสภาพไม่ผ่าน' as FAKETEXT
,TVEN.SABBREVIATION||' (จำนวน '||COUNT(TVEN.SABBREVIATION)||' รายการ)' as RPTSHOW
FROM TBL_CHECKLIST MS
LEFT JOIN TBL_SCRUTINEERINGLIST LAW
ON  MS.CHECKLIST_ID = LAW.CHECKLIST_ID
LEFT JOIN TBL_SCRUTINEERING LA
ON  LAW.REQUEST_ID = LA.REQUEST_ID
LEFT JOIN TBL_REQUEST REQ 
ON LAW.REQUEST_ID = REQ.REQUEST_ID
LEFT JOIN TVENDOR TVEN
ON TVEN.SVENDORID = REQ.VENDOR_ID
WHERE LAW.ISACTIVE_FLAG = 'Y' " + Codition + @" AND (LAW.ITEM1_VAL = 'N' OR LAW.ITEM2_VAL = 'N'  OR (LAW.ITEM3_VAL = 'N'  AND MS.RD_STATUSCT = 'R')) 
GROUP BY  TVEN.SABBREVIATION ,MS.CHECKLIST_ID,MS.CHECKLIST_NAME_REPORT, MS.NORDER,TVEN.SVENDORID,'รวมสาเหตุรถบรรทุกที่ตรวจสภาพไม่ผ่าน'
ORDER BY TVEN.SVENDORID, MS.NORDER ,ROW_NUMBER() OVER (PARTITION BY TVEN.SABBREVIATION  ORDER BY MS.NORDER)";

        DataTable dt = CommonFunction.Get_Data(conn, Query);

        gvw.DataSource = dt;
        gvw.DataBind();
        CreateReport(dt);
        gvw.ExpandAll();
        //  gvw.GroupSummarySortInfo.Add(,
        //ColumnSortOrder.Ascending, firstGroupingColumn);

    }

    void CreateReport(DataTable dt)
    {

        //        string Query = @"SELECT  ROWNUM as sNO, TREQ.REQUEST_ID,TREQ.VENDOR_ID,TVEN.SABBREVIATION,TREQ.VEH_No,TREQ.TU_No,TRUNC(TREQ.SERVICE_DATE) as SERVICE_DATE
        //                        ,TREQ.Status_Flag ,TSTATUS.STATUSREQ_NAME,TCAUSETYPE.CAUSE_NAME ,TCAUSETYPE.CAUSE_ID, 'xxx' as DESCRIPTION,TVEN.SABBREVIATION||' จำนวน '|| GC.NCOUNT||' รายการ' as STEXT
        //                        FROM TBL_Request TREQ
        //                        LEFT JOIN 
        //                        (
        //                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR
        //                        ) TVEN
        //                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
        //                        LEFT JOIN TBL_CAUSE TCAUSETYPE
        //                        ON TCAUSETYPE.CAUSE_ID = TREQ.CAUSE_ID
        //                        LEFT JOIN TBL_STATUSREQ TSTATUS
        //                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
        //                        LEFT JOIN (SELECT VENDOR_ID , COUNT(VENDOR_ID) as NCOUNT FROM TBL_Request GROUP BY VENDOR_ID) GC
        //                        ON GC.VENDOR_ID = TREQ.VENDOR_ID";

        // DataTable dt = CommonFunction.Get_Data(conn, Query);

        //gvw.DataSource = dt;
        //gvw.DataBind();
        if (dt.Rows.Count > 0)
        {
            //1
            rpt_CauseCarNotComplete report = new rpt_CauseCarNotComplete();
            report.Name = "rpt_CauseCarNotComplete";

            if (!string.IsNullOrEmpty(edtStart.Text) && !string.IsNullOrEmpty(edtEnd.Text))
            {
                ((XRLabel)report.FindControl("XRLabel3", true)).Visible = true;
                ((XRLabel)report.FindControl("XRLabel3", true)).Text = lblsHead.Text + edtStart.Text + " - " + edtEnd.Text;
            }
            else
            {
                ((XRLabel)report.FindControl("XRLabel3", true)).Text = lblsHead.Text+" -";
            }

            //report.DataSource = dt;
            report.DetailReport.DataSource  = dt;
            rvw.Report = report;
            rvw.DataBind();
            //report.CreateDocument();
        }

    }

    #region SetAutoComplete

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    #endregion
}