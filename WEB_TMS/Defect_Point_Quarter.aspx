﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="Defect_Point_Quarter.aspx.cs" Inherits="Defect_Point_Quarter" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .Rotate
        {
            height: 400px;
        }
        
        
        .Rotate table
        {
            /* float your elements or inline-block them to display side by side */
            float: left; /* these are height and width dimensions of your header */
            height: 10em;
            width: 1.5em; /* set to hidden so when there's too much vertical text it will be clipped. */
            overflow: auto; /* these are not relevant and are here to better see the elements */
            background: #E2F0FF;
            margin-right: 1px;
        }
        .Rotate td
        {
            /* setting background may yield better results in IE text clear type rendering */
            background: #E2F0FF;
            display: block; /* this will prevent it from wrapping too much text */
            white-space: nowrap; /* so it stays off the edge */
            padding-left: 3px; /* IE specific rotation code */
            writing-mode: tb-rl;
            filter: flipv fliph; /* CSS3 specific totation code */ /* translate should have the same negative dimension as head height */
            transform: rotate(270deg) translate(-10em,0);
            transform-origin: 0 0;
            -moz-transform: rotate(270deg) translate(-10em,0);
            -moz-transform-origin: 0 0;
            -webkit-transform: rotate(270deg) translate(-10em,0);
            -webkit-transform-origin: 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" CausesValidation="False"
        OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="35%" align="right">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" NullText="เลขที่สัญญา,ชื่อผู้ประกอบการขนส่ง"
                                Width="200px">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="8%" align="right">
                            ปี ที่ประเมิน
                        </td>
                        <td width="15%" align="center">
                            <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px">
                            </dx:ASPxComboBox>
                        </td>
                        <td width="20%">
                            <dx:ASPxRadioButtonList ID="rblCheck" runat="server" SkinID="rblStatus">
                                <ClientSideEvents SelectedIndexChanged="function(s,e){cmbMonth.PerformCallback();}" />
                                <Items>
                                    <dx:ListEditItem Text="รายไตรมาส" Value="1" Selected="true" />
                                    <dx:ListEditItem Text="รายเดือน" Value="2" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td width="12%">
                            <dx:ASPxComboBox ID="cmbMonth" ClientInstanceName="cmbMonth" runat="server" Width="100px"
                                OnCallback="cmbMonth_Callback">
                            </dx:ASPxComboBox>
                        </td>
                        <td width="10%">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search');   }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                                <tr>
                                    <td style="padding-right: 4px">
                                        <div style="float: left">
                                            <dx:ASPxButton ID="btnPdfExport" ToolTip="Export to PDF" runat="server" UseSubmitBehavior="False"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                OnClick="btnPdfExport_Click">
                                                <Image Width="16px" Height="16px" Url="Images/ic_pdf2.gif">
                                                </Image>
                                            </dx:ASPxButton>
                                        </div>
                                        <div style="float: left">
                                            PDF</div>
                                    </td>
                                    <td style="padding-right: 4px">
                                        <div style="float: left">
                                            <dx:ASPxButton ID="btnXlsExport" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                OnClick="btnXlsExport_Click">
                                                <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                                                </Image>
                                            </dx:ASPxButton>
                                        </div>
                                        <div style="float: left">
                                            Excel</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <%--    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnAfterPerformCallback="gvw_AfterPerformCallback" OnInit="gvw_Init">--%>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%" OnAfterPerformCallback="gvw_AfterPerformCallback"
                                OnInit="gvw_Init">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="30px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="บริษัท" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="2" Width="120px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <FooterCellStyle HorizontalAlign="Center" />
                                        <FooterTemplate>
                                            <dx:ASPxLabel ID="lblVendor" runat="server" Text="รวมทุกบริษัท">
                                            </dx:ASPxLabel>
                                        </FooterTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="3" Width="120px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <FooterCellStyle HorizontalAlign="Center" />
                                        <FooterTemplate>
                                            <dx:ASPxLabel ID="lblContract" runat="server" Text="รวมทุกสัญญา">
                                            </dx:ASPxLabel>
                                        </FooterTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings ShowFooter="True" ShowHorizontalScrollBar="true" />
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdDefectPoint"></asp:SqlDataSource>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvw" PageHeader-Font-Names="Tahoma"
                                PageHeader-Font-Bold="true">
                                <Styles>
                                    <Header Font-Names="Tahoma" Font-Size="10">
                                    </Header>
                                    <Cell Font-Names="Tahoma" Font-Size="8">
                                    </Cell>
                                </Styles>
                                <PageHeader>
                                    <Font Bold="True" Names="Tahoma"></Font>
                                </PageHeader>
                            </dx:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
