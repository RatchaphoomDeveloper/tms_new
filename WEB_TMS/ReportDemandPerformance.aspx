﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    StylesheetTheme="Aqua" CodeFile="ReportDemandPerformance.aspx.cs" Inherits="ReportDemandPerformance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<script type="text/javascript" src="Javascript/DevExpress/DevExpress.js" > </script>
    <table width="100%">
        <tr >
            <td style="width: 35%; text-align: right;">
                ปี/เดือน :
            </td>
            <td style="width: 10%;">
                <div style="float: left;">
                    <dx:ASPxComboBox ID="cboYear" ClientInstanceName="cboMonth" runat="server" Width="70px" DataSourceID="sdsYear"
                    TextField="stext" ValueField="svalue">
                        <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                            CausesValidation="true" ValidationGroup="search">
                            <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                    <asp:SqlDataSource ID="sdsYear" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" SelectCommand="SELECT distinct EXTRACT(year from DLVR.DELIVERY_DATE ) svalue,EXTRACT(year from DLVR.DELIVERY_DATE )+543 stext
FROM TDELIVERY DLVR
ORDER BY  EXTRACT(year from DLVR.DELIVERY_DATE ) DESC">
                </asp:SqlDataSource>
                </div>
                <div style="float: left;">
                    <dx:ASPxComboBox ID="cboMonth" ClientInstanceName="cboMonth" runat="server" Width="80px">
                        <Items>
                            <dx:ListEditItem Text="มกราคม" Value="01" />
                            <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                            <dx:ListEditItem Text="มีนาคม" Value="03" />
                            <dx:ListEditItem Text="เมษายน" Value="04" />
                            <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                            <dx:ListEditItem Text="มิถุนายน" Value="06" />
                            <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                            <dx:ListEditItem Text="สิงหาคม" Value="08" />
                            <dx:ListEditItem Text="กันยายน" Value="09" />
                            <dx:ListEditItem Text="ตุลาคม" Value="10" />
                            <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                            <dx:ListEditItem Text="ธันวาคม" Value="12" />
                        </Items>
                        <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                            CausesValidation="true" ValidationGroup="search">
                            <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </div>
            </td>
            <td style="width: 5%;">
            </td>
            <td style="width: 40%; ">
            </td>
        </tr>
        <tr style="width: 10%;">
            <td style="width: 10%; text-align: right;">
                คลัง :
            </td>
            <td style="width: 10%;">
                <dx:ASPxComboBox ID="cboTerminal" ClientInstanceName="cboTerminal" runat="server"
                    CallbackPageSize="30" EnableCallbackMode="True" OnItemRequestedByValue="cboTerminal_OnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="cboTerminal_OnItemsRequestedByFilterConditionSQL" nulltext="-- ทั้งหมด --"
                    SkinID="xcbbATC" TextFormatString="{0}-{1}" ValueField="STERMINALID" Width="180px">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="รหัส" FieldName="STERMINALID" Width="80px" />
                        <dx:ListBoxColumn Caption="คลัง" FieldName="STERMINALNAME" Width="100px" />
                    </Columns>
                    <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                        CausesValidation="true" ValidationGroup="search">
                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsTERMINAL" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td style="width: 10%;">
            </td>
            <td>
            </td>
        </tr>
        <tr style="width: 10%;">
            <td style="width: 10%; text-align: right; white-space: nowrap;">
                กลุ่มผลิตภัณฑ์ :
            </td>
            <td>
                <dx:ASPxComboBox ID="cboProdGrp" ClientInstanceName="cboProdGrp" runat="server" DataSourceID="sdsProdGrp"
                    TextField="SPRODUCTTYPENAME" ValueField="SPRODUCTTYPEID">
                    <ValidationSettings SetFocusOnError="true" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                        CausesValidation="true" ValidationGroup="search">
                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsProdGrp" runat="server" SelectCommand="SELECT CPRODUCT_GROUP
        ,RTRIM (
          XMLAGG (XMLELEMENT (e, SPRODUCTTYPENAME || ',')).EXTRACT ('//text()'),
          ',')
          SPRODUCTTYPENAME
          ,CPRODUCT_GROUP||':'||RTRIM (
          XMLAGG (XMLELEMENT (e, SPRODUCTTYPEID || ',')).EXTRACT ('//text()'),
          ',')
          SPRODUCTTYPEID
  FROM TPRODUCTTYPE
  GROUP BY CPRODUCT_GROUP" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td style="width: 10%;">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <dx:ASPxButton ID="btnSearch" runat="server" OnClick="btnSubmit_Click" SkinID="_search"
                    ValidationGroup="search">
                </dx:ASPxButton>
            </td>
            <td colspan="2" align="left">
                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                    <ClientSideEvents Click="function(s,e){ window.location = 'Report.aspx'}" />
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Literal ID="ltr" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
