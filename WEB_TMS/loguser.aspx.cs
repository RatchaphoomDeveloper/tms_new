﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Globalization;

public partial class loguser : System.Web.UI.Page
{
    string connection = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
      
        cboSearchNameAndOrgUser.ItemRequestedByValue += new ListEditItemRequestedByValueEventHandler(cboSearchNameAndOrgUser_ItemRequestedByValue);
        btnExport.Click += new EventHandler(btnExport_Click);

        if (!IsPostBack)
        {
            dateEditStart.Date = DateTime.Today;
            dateEditEnd.Date = DateTime.Today;
        }
    }

    protected void cboSearchNameAndOrgUser_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        listUserName.SelectCommand = @"SELECT USER_NAME ,ORG ,SUID FROM (SELECT TUSR.SUID,  TUSR.SFIRSTNAME||' '||TUSR.SLASTNAME AS USER_NAME,TUNITCODE.SABBREVIATION AS ORG , ROW_NUMBER()OVER(ORDER BY SUID) AS RN 
                                        FROM TUSER TUSR
                                            LEFT JOIN (
                                            SELECT SVENDORID , SABBREVIATION FROM TVENDOR
                                            UNION ALL
                                            SELECT STERMINALID , SABBREVIATION  FROM TTERMINAL
                                            UNION ALL
                                            SELECT UNITCODE ,UNITNAME FROM TUNIT
                                        ) TUNITCODE ON TUSR.SVENDORID = TUNITCODE.SVENDORID
                                        WHERE TUSR.SFIRSTNAME||' '||TUSR.SLASTNAME || TUNITCODE.SABBREVIATION LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex 
                                        ORDER BY USER_NAME";

        listUserName.SelectParameters.Clear();
        listUserName.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        listUserName.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        listUserName.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = listUserName;
        comboBox.DataBind();
    }

    void cboSearchNameAndOrgUser_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e) { }

   

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (e.Parameter == "Search")
        {
            bindData();
        }

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Page_Load ทำงานก่อน xcpn_Load
        bindData();
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        bindData();
        expGvwXls.WriteXlsToResponse();
    }

    #region bindDataExport
    public void bindDataExport()
    {
        //DataTable dt = new DataTable();
        //dt = CommonFunction.Get_Data(connection, sqlQueryLogUser);
        //gvwLogUser.DataSource = dt;
        //gvwLogUser.DataBind();
    }
    #endregion bindDataExport

    public void bindData()
    {
        string dateStart = "";
        string dateEnd = "";
        string where = "";
        string menu = "";
        string searchNameAndOrgUser = cboSearchNameAndOrgUser.Value + "";
        //string searchNameAndOrgUser = cboSearchNameAndOrgUser.Text.Trim();
        string searchMenu = cboSearchMenu.Value + "";
        string userGroup = cboUserGroup.Value + "";
        menu = searchMenu;

        if (dateEditStart.Value != null && dateEditEnd.Value != null)
        {
            dateStart = dateEditStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            dateEnd = dateEditEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        }

        if (dateEditStart.Value == null && dateEditEnd.Value == null)
        {
            if (userGroup == "1")
            {
                where = "WHERE u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 0 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else if (userGroup == "2")
            {
                where = "WHERE u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 1 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else if (userGroup == "3")
            {
                where = "WHERE u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 2 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else
            {
                where = "WHERE u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
        }
        else
        {
            if (userGroup == "1")
            {
                where = "WHERE TO_DATE(UT.DCREATE,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 0 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else if (userGroup == "2")
            {
                where = "WHERE TO_DATE(UT.DCREATE,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 1 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else if (userGroup == "3")
            {
                where = "WHERE TO_DATE(UT.DCREATE,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND u.CGROUP = 2 AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
            else
            {
                where = "WHERE TO_DATE(UT.DCREATE,'DD/MM/YYYY') BETWEEN NVL(TO_DATE('" + dateStart + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND NVL(TO_DATE('" + dateEnd + "','DD/MM/YYYY'),TO_DATE(SYSDATE,'DD/MM/YYYY')) AND u.SUID = NVL('" + searchNameAndOrgUser + "',u.SUID) AND UT.SMENUID = NVL('" + menu + "',UT.SMENUID)";
            }
        }

        #region sqlQueryLogUser
        string sqlQueryLogUser = @"SELECT ROWNUM ARRANGE,M.SMENUNAME AS MENU,UT.DCREATE AS DATES, CASE U.CGROUP WHEN '0' THEN 'ผู้ขนส่ง' WHEN '1' THEN 'แอดมิน' ELSE 'คลัง' END AS USER_GROUP,
U.SFIRSTNAME || ' ' || U.SLASTNAME AS USER_ACTIVE,TUNITCODE.SABBREVIATION ORG,UT.SDESCRIPTION AS WORKING
FROM ((TUSERTRACE ut LEFT JOIN TMENU m ON UT.SMENUID = M.SMENUID )LEFT JOIN TUSER u ON UT.SUID = U.SUID)LEFT JOIN (
                                        SELECT SVENDORID , SABBREVIATION FROM TVENDOR
                                        UNION ALL
                                        SELECT STERMINALID , SABBREVIATION  FROM TTERMINAL
                                        UNION ALL
                                        SELECT UNITCODE ,UNITNAME FROM TUNIT
                                    ) TUNITCODE ON U.SVENDORID = TUNITCODE.SVENDORID " + where;
                                      
        #endregion

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(connection, sqlQueryLogUser);
        Report_Log_User.DataSource = dt;
        Report_Log_User.DataBind();
    }


}
