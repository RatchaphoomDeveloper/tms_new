﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ConfirmTruckConclude.aspx.cs" Inherits="ConfirmTruckConclude" Culture="en-US" UICulture="en-US" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a class="accordion-toggle" data-toggle="collapse" href="#collapse1" id="acollapse1">ค้นหา</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row form-group">
                                <label class="col-md-3 control-label">
                                    วันที่ยืนยันรถ
                                    <asp:Label Text=" *" runat="server" ForeColor="Red" /></label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDate" />
                                </div>
                                <label class="col-md-2 control-label">กลุ่มงาน</label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                                <div class="col-md-3">

                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>

                                </div>
                                <label class="col-md-2 control-label">คลังต้นทางประสงค์เข้ารับงาน</label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTTERMINAL">
                                    </asp:DropDownList>

                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">เลขที่สัญญา</label>
                                <div class="col-md-3">

                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract">
                                    </asp:DropDownList>
                                </div>
                                <label class="col-md-2 control-label">สถานะ</label>
                                <div class="col-md-3">
                                    <asp:RadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="&nbsp;ทั้งหมด&nbsp;" Value="ALL" Selected="True" />
                                        <asp:ListItem Text="&nbsp;ส่งข้อมูลให้ ปตท.&nbsp;" Value="1" />
                                        <asp:ListItem Text="&nbsp;รอส่งข้อมูล&nbsp;" Value="0" />
                                    </asp:RadioButtonList>

                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">รถหมุนเวียน</label>
                                <div class="col-md-3">
                                    <asp:CheckBox Text="" ID="cbCarRecycling" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        <asp:Button Text="Export" runat="server" ID="btnExport" class="btn btn-md bth-hover btn-info" OnClick="btnExport_Click" />
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2"><i class="fa fa-table"></i>ผลการค้นหา จำนวน
                <asp:Label Text="0" runat="server" ID="lblItem" />
                            รายการ</a>

                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row text-right">
                                <asp:Label Text="จำนวนรถหมุนเวียน {0} คัน" runat="server" ID="lblCSTANBY" ForeColor="Red" />&nbsp;
                                <asp:Label Text="จำนวนรถที่ยืนยัน {0} คัน" runat="server" ID="lblCCONFIRM" ForeColor="Red" />
                            </div>
                            <div class="row">
                                <asp:GridView runat="server" ID="gvData"
                                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter GridColorHeaderFont11"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]" OnPageIndexChanging="gvData_PageIndexChanging" OnRowDataBound="gvData_RowDataBound" DataKeyNames="NCONFIRMID">
                                    <Columns>
                                        <asp:BoundField HeaderText="กลุ่มงาน" DataField="GROUPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="บริษัทผู้ขนส่ง" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="คลังต้นทางตามสัญญา" DataField="TTERMINALNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="รถในสัญญา(คัน)" DataField="NTRUCKCONTRACT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="สำรอง(คัน)" DataField="NTRUCKRESERVE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="ยืนยันการใช้งาน(คัน)" HtmlEncode="false" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="ไม่พร้อมใช้งาน(คัน)" HtmlEncode="false" DataField="CCONFIRM_NOT" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="วันที่อัพเดต" DataField="DUPDATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                        <asp:BoundField HeaderText="สถานะ<br/>ส่งข้อมูล" DataField="STATUSNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HtmlEncode="false" />
                                        <asp:TemplateField HeaderText="ห้ามแก้ไข" ItemStyle-Wrap="false">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:CheckBox Text="" ID="cbLock" runat="server" AutoPostBack="true" OnCheckedChanged="cbLock_CheckedChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Wrap="false">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div class="col-sm-1">
                                                    <a id="aView" runat="server" target="_blank">View</a>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3"><i class="fa fa-table"></i>ผลการค้นหาคลัง จำนวน
                <asp:Label Text="0" runat="server" ID="lblItemTerminal" />
                                    รายการ (เฉพาะรถที่ยืนยัน)</a>

                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvTerminal"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter " PageSize="100"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="คลังต้นทางที่รับงาน" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ยืนยันการใช้งาน(คัน)" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                <asp:BoundField HeaderText="รถปกติ(คัน)" DataField="CSTANBY_N" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถหมุนเวียน(คัน)" DataField="CSTANBY" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse6" id="acollapse6"><i class="fa fa-table"></i>สรุปการยืนยันรถ TMS
                <asp:Label Text="0" runat="server" ID="lblItemStatus" Visible="false" />
                                    </a>

                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvStatus"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter "
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="สถานะ" DataField="STATUS_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถปกติ(คัน)" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถหมุนเวียน(คัน)" DataField="CSTANBY" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(คัน)" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="DCREATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

