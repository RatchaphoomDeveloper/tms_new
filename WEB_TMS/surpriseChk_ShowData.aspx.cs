﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Security;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.SurpriseCheckYear;
using System.IO;

public partial class surpriseChk_ShowData : PageBase
{
    #region ViewState
    private string ID
    {
        get
        {
            if ((string)ViewState["ID"] != null)
                return (string)ViewState["ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    private DataTable dtSurpriseCheckYearList
    {
        get
        {
            if ((DataTable)ViewState["dtSurpriseCheckYearList"] != null)
                return (DataTable)ViewState["dtSurpriseCheckYearList"];
            else
                return null;
        }
        set
        {
            ViewState["dtSurpriseCheckYearList"] = value;
        }
    }
    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }
    private DataTable dtTruck
    {
        get
        {
            if ((DataTable)ViewState["dtTruck"] != null)
                return (DataTable)ViewState["dtTruck"];
            else
                return null;
        }
        set
        {
            ViewState["dtTruck"] = value;
        }
    }
    private DataTable dtSurpriseCheckYear
    {
        get
        {
            if ((DataTable)ViewState["dtSurpriseCheckYear"] != null)
                return (DataTable)ViewState["dtSurpriseCheckYear"];
            else
                return null;
        }
        set
        {
            ViewState["dtSurpriseCheckYear"] = value;
        }
    }
    private DataTable dtFileStep1
    {
        get { return (DataTable)ViewState["dtFileStep1"]; }
        set { ViewState["dtFileStep1"] = value; }
    }
    #endregion
    string TempDirectory = "UploadFile/SurpriseCheckYear/{0}/{2}/{1}/";

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string vendorID;
    string truckID;
    int contractID;
    string strChkTruck;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            this.InitialForm();
            AssignAuthen();

            this.LoadVendor();
            this.LoadData();
            this.LoadTruck();
            this.LoadTruckFile();
            this.LoadDataTruck();

            if (dtSurpriseCheckYearList.Rows.Count > 0)
                RealCheck.Text = dtSurpriseCheckYearList.Rows.Count.ToString();

            if (dtSurpriseCheckYear.Rows.Count > 0)
            {
                if (dtSurpriseCheckYear.Rows[0]["IS_ACTIVE"].ToString() == "2")
                {
                    ddlShead.Enabled = false;
                }
            }
        }
        if (Session["vendorID"] != null)
        {
            vendorID = Session["vendorID"].ToString();
        }
        if (Session["truckID"] != null)
        {
            truckID = Session["truckID"].ToString();
        }
        if (Session["contractID"] != null)
        {
            contractID = Convert.ToInt32(Session["contractID"]);
        }
        if (Session["strChkTruck"] != null)
        {
            strChkTruck = Session["strChkTruck"].ToString();
        }


    }

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {

                var decryptedBytes = MachineKey.Decode(Request.QueryString["DocID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                DocID = decryptedValue;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadVendor()
    {
        try
        {
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTruck()
    {
        try
        {
            dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSelectBLL(" AND DOC_ID = " + DocID);
            GridViewHelper.BindGridView(ref dgvSurpriseCheck, dtSurpriseCheckYearList, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadTruckFile()
    {
        try
        {
            hidID.Value = DocID;
            dtFileStep1 = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckFileSelectBLL(" AND DOC_ID = " + DocID);
            //GridViewHelper.BindGridView(ref dgvSurpriseCheck, dtSurpriseCheckYearList, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDataTruck()
    {
        try
        {
            dtTruck = SurpriseCheckYearBLL.Instance.DataTruckSelectBLL(" AND SVENDORID = '" + ddlVendor.SelectedValue + "' AND SCONTRACTNO = '" + ddlContract.SelectedItem + "'");
            DropDownListHelper.BindDropDownList(ref ddlShead, dtTruck, "STRUCKID", "SHEADREGISTERNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtcontract = ContractBLL.Instance.ConTractSelectByVendor(ddlVendor.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlContract, dtcontract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadData()
    {
        try
        {
            dtSurpriseCheckYear = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND ID = " + DocID);
            if (dtSurpriseCheckYear.Rows.Count > 0)
            {
                TotalCar.Text = dtSurpriseCheckYear.Rows[0]["TOTALCAR"].ToString();
                ddlVendor.SelectedValue = dtSurpriseCheckYear.Rows[0]["SVENDORID"].ToString();
                ddlVendor_SelectedIndexChanged(null, null);
                ddlContract.SelectedValue = dtSurpriseCheckYear.Rows[0]["SCONTRACTID"].ToString();
                AppointDate.Text = DateTime.Parse(dtSurpriseCheckYear.Rows[0]["APPOINTDATE"].ToString()).ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
                STATUS.Text = dtSurpriseCheckYear.Rows[0]["STATUS"].ToString();
                GROUPS_NAME.Text = dtSurpriseCheckYear.Rows[0]["GROUPS_NAME"].ToString();
                TYPE_NAME.Text = dtSurpriseCheckYear.Rows[0]["TYPE_NAME"].ToString();
                Remark.Text = dtSurpriseCheckYear.Rows[0]["REMARK"].ToString();
                this.ID = dtSurpriseCheckYear.Rows[0]["ID"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvSurpriseCheck_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseCheck.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseCheck, dtSurpriseCheckYearList);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheck_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string SCONTRACTID = dgvSurpriseCheck.DataKeys[e.RowIndex]["SCONTRACTID"] + string.Empty;
            string STRUCKID = dgvSurpriseCheck.DataKeys[e.RowIndex]["STRUCKID"] + string.Empty;
            string SHEADREGISTERNO = dgvSurpriseCheck.DataKeys[e.RowIndex]["SHEADREGISTERNO"] + string.Empty;
            string STRAILERREGISTERNO = dgvSurpriseCheck.DataKeys[e.RowIndex]["STRAILERREGISTERNO"] + string.Empty;
            string SCONTRACTNO = dgvSurpriseCheck.DataKeys[e.RowIndex]["SCONTRACTNO"] + string.Empty;
            string SVENDORNAME = dgvSurpriseCheck.DataKeys[e.RowIndex]["SVENDORNAME"] + string.Empty;
            string SVENDORID = dgvSurpriseCheck.DataKeys[e.RowIndex]["SVENDORID"] + string.Empty;
            string APPOINTDATE = dgvSurpriseCheck.DataKeys[e.RowIndex]["APPOINTDATE"] + string.Empty;

            string TSURPRISECHECKYEARTRUCK_ID = dgvSurpriseCheck.DataKeys[e.RowIndex]["ID"] + string.Empty;
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(SCONTRACTID + "&" + STRUCKID + "&" + SHEADREGISTERNO + "&" + STRAILERREGISTERNO + "&" + APPOINTDATE + "&" + "&" + SCONTRACTNO + "&" + SVENDORNAME + "&" + SVENDORID + "&1" + "&" + TSURPRISECHECKYEARTRUCK_ID);
            // ค่า 1 parameter ตัวที่ 10 คือค่าสำหรับรายปีที่ต้องส่งไป รายวันไม่ต้องส่ง
            string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("SurpriseCheckAddEdit.aspx?str=" + str, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlShead_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataRow[] row = dtTruck.Select("SHEADREGISTERNO = '" + ddlShead.SelectedValue + "'");
            txtStrailer.Text = row[0]["STRAILERREGISTERNO"].ToString();
            btnSave.Enabled = true;
            btnClear.Enabled = true;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            ddlShead.ClearSelection();
            txtStrailer.Text = string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //int count = TotalCar.Text == string.Empty ? 0 : int.Parse(TotalCar.Text);
            //if (dtSurpriseCheckYearList.Rows.Count < count)
            //{
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSaveBLL(DocID, ddlShead.SelectedValue, txtStrailer.Text, "0", Session["UserID"].ToString(), "1");
                var plaintextBytes = Encoding.UTF8.GetBytes(DocID);
                var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess("บันทึกสำเร็จ", "surpriseChk_ShowData.aspx?DocID=" + encryptedValue);
            //}
            //else
            //{
            //    throw new Exception("เกินจำนวนรถที่กำหนด");
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheck_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
        GridView gv = (GridView)sender;
        if (string.Equals(e.CommandName, "delete"))
        {
            try
            {
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSaveBLL(dgvSurpriseCheck.DataKeys[Index]["DOC_ID"].ToString(), dgvSurpriseCheck.DataKeys[Index]["SHEADREGISTERNO"].ToString(), string.Empty, string.Empty, Session["UserID"].ToString(), "2");
                var plaintextBytes = Encoding.UTF8.GetBytes(dgvSurpriseCheck.DataKeys[Index]["DOC_ID"].ToString());
                var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                Response.Redirect("surpriseChk_ShowData.aspx?DocID=" + encryptedValue);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        else if (string.Equals(e.CommandName, "file"))
        {
            DataKey dka = gv.DataKeys[Index];
            hidRowIndex.Value = Index.ToString();
            hidM_CHECK_CONTRACT_LIST_ID.Value = dka["ID"] + string.Empty;

            lblSCHECKLISTNAME.Text = dka["DETAIL"] + string.Empty;
            DataRow[] drs = dtFileStep1.Select("TRUCK_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
            if (drs.Any())
            {
                gvFileStep1.DataSource = drs.CopyToDataTable();
            }
            else
            {
                gvFileStep1.DataSource = dtFileStep1.Clone();
            }
            gvFileStep1.DataBind();
            plCheckList.Visible = false;
            plStep1.CssClass = "";

        }
    }
    protected void cbIS_ACTIVE_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox cbIS_ACTIVE = (CheckBox)sender;
            int Index = ((GridViewRow)cbIS_ACTIVE.NamingContainer).RowIndex;
            string ID = dgvSurpriseCheck.DataKeys[Index]["ID"] + string.Empty;
            SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckUpdateStatusBLL(ID, Session["UserID"] + string.Empty, cbIS_ACTIVE.Checked ? "1" : "0");
            Label lblSTATUS_NAME = (Label)dgvSurpriseCheck.Rows[Index].FindControl("lblSTATUS_NAME");
            if (lblSTATUS_NAME != null)
            {
                if (cbIS_ACTIVE.Checked)
                {
                    lblSTATUS_NAME.Text = "ผ่าน";
                }
                else
                {
                    lblSTATUS_NAME.Text = "รอตรวจ";
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }



    }
    protected void gvFileStep1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataRow[] drs = dtFileStep1.Select("TRUCK_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);

        dtFileStep1.Rows.Remove(drs[e.RowIndex]);
        drs = dtFileStep1.Select("TRUCK_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
        if (drs.Any())
        {
            gvFileStep1.DataSource = drs.CopyToDataTable();
        }
        else
        {
            gvFileStep1.DataSource = dtFileStep1.Clone();
        }
        gvFileStep1.DataBind();
    }
    protected void btnCloseStep1_Click(object sender, EventArgs e)
    {
        plStep1.CssClass = "hide";
        plCheckList.Visible = true;
        if (!string.IsNullOrEmpty(hidRowIndex.Value))
        {
            GridViewRow gvr = null;
            gvr = dgvSurpriseCheck.Rows[int.Parse(hidRowIndex.Value)];

            DataSet ds = new DataSet("DS");
            dtFileStep1.TableName = "FILE";
            ds.Tables.Add(dtFileStep1.Copy());

            SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckFileSaveBLL(DocID, ds);
        }
    }
    protected void btnAddStep1_Click(object sender, EventArgs e)
    {
        try
        {

            if (fileUploadStep1.HasFile && !string.IsNullOrEmpty(txtDetailStep1.Text.Trim()))
            {
                string[] fileExt = fileUploadStep1.FileName.Split('.');
                int ncol = fileExt.Length - 1;
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                string SFILEPATH = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "");
                UploadFile2Server(fileUploadStep1, genName, SFILEPATH);

                DataRow dr = dtFileStep1.NewRow();
                //dr["SCHECKID"] = hidSCHECKID.Value;
                dr["DOC_ID"] = hidID.Value;
                dr["TRUCK_ID"] = hidM_CHECK_CONTRACT_LIST_ID.Value;

                dr["REMARK"] = txtDetailStep1.Text.Trim();
                dr["FILENAME_USER"] = fileUploadStep1.FileName;
                dr["FILENAME_SYSTEM"] = genName + "." + fileExt[ncol].Trim();
                dr["FULLPATH"] = SFILEPATH + genName + "." + fileExt[ncol].Trim();
                dr["CREATE_BY"] = Session["UserID"] + "";
                dtFileStep1.Rows.Add(dr);
                DataRow[] drs = dtFileStep1.Select("TRUCK_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
                if (drs.Any())
                {
                    gvFileStep1.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep1.DataSource = dtFileStep1.Clone();
                }
                gvFileStep1.DataBind();
                txtDetailStep1.Text = string.Empty;
            }
            else
            {
                throw new Exception("กรุณาเลือกไฟล์ที่เป็นรูปภาพแล้วระบุรายละเอียด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #region UploadFile2Server
    private bool UploadFile2Server(FileUpload ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    #endregion
}