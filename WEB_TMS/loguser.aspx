﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="loguser.aspx.cs" Inherits="loguser" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
     

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" OnLoad="xcpn_Load"
        ClientInstanceName="xcpn">
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="40%">
                            <dx:ASPxImage Width="20px" Height="20px" ID="imgExport" runat="server" ImageUrl="images/ic_ms_excel.gif" Cursor="pointer">
                                <ClientSideEvents Click="function(s,e){btnExport.OnClick();}" />
                            </dx:ASPxImage>
                            <dx:ASPxButton ID="btnExport" ClientInstanceName="btnExport" runat="server" Text="Export Exel File" Width="80px" ClientVisible="false">
                                
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboSearchMenu" runat="server" Width="180px" SkinID="xcbbATC" TextField="SMENUNAME" ValueField="SMENUID" DataSourceID="SqlDataSource1">
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" SelectCommand="SELECT * FROM TMENU">

                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <%--<ClientSideEvents SelectedIndexChanged ="function (s, e) {cboTrailerRegist.PerformCallback(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));txtVenderID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANSPORTID'));txtVenderName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORNAME'));cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) }" />--%>
                            <dx:ASPxComboBox ID="cboSearchNameAndOrgUser" runat="server" CallbackPageSize="30"
                                SkinID="xcbbATC" Width="230px" TextFormatString="{0},{1}" ClientInstanceName="cboSearchNameAndOrgUser"
                                ValueField="SUID" EnableCallbackMode="True" OnItemsRequestedByFilterCondition="cboSearchNameAndOrgUser_ItemsRequestedByFilterCondition">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ผู้ใช้งาน" FieldName="USER_NAME" Width="250px" />
                                    <dx:ListBoxColumn Caption="หน่วยงาน" FieldName="ORG" Width="200px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="listUserName" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dateEditStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dateEditEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboUserGroup" runat="server" Width="100px">
                                <Items>
                                    <dx:ListEditItem Selected="true" Text="- กลุ่มผู้ใช้งาน -" />
                                    <dx:ListEditItem Text="ผู้ขนส่ง" Value="1" />
                                    <dx:ListEditItem Text="แอดมิน" Value="2" />
                                    <dx:ListEditItem Text="คลัง" Value="3" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" Text="ค้นหา" Width="75px" AutoPostBack="false">
                                <%--ClientSideEvents ทำให้ไม่มีการ Refresh หน้าจอ--%>
                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="Report_Log_User" runat="server" SkinID="_gvw">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" FieldName="ARRANGE" ShowInCustomizationForm="True"
                                        Width="3%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เมนู" FieldName="MENU" ShowInCustomizationForm="True"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่" FieldName="DATES" ShowInCustomizationForm="True"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="กลุ่มผู้ใช้งาน" FieldName="USER_GROUP" ShowInCustomizationForm="True"
                                        Width="3%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ใช้งาน" FieldName="USER_ACTIVE" ShowInCustomizationForm="True"
                                        Width="19%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หน่วยงาน" FieldName="ORG" ShowInCustomizationForm="True"
                                        Width="18%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="การทำงาน" FieldName="WORKING" ShowInCustomizationForm="True"
                                        Width="22%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <dx:ASPxGridViewExporter ID="expGvwXls" GridViewID="Report_Log_User" runat="server">
                            </dx:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
