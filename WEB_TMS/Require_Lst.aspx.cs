﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Require_Lst : PageBase
{
    #region Member
    private int Level
    {
        get
        {
            if ((int)ViewState["Level"] != null)
                return (int)ViewState["Level"];
            else
                return 0;
        }
        set
        {
            ViewState["Level"] = value;
        }
    }
    #endregion

    #region Event
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            SetDrowDownList();
            ddlMenu_SelectedIndexChanged(null, null);
        }
    }
    #endregion

    #region ddlMenu_SelectedIndexChanged
    protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["DataLevel"] = RequireBLL.Instance.RequireLevelSelect(ddlMenu.SelectedValue);
        SetLevel();
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion

    #region ddlLevel1_SelectedIndexChanged
    protected void ddlLevel1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDataDDL(sender);
    }
    #endregion

    #region ddlLevel2_SelectedIndexChanged
    protected void ddlLevel2_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDataDDL(sender);
    }
    #endregion

    #region ddlLevel3_SelectedIndexChanged
    protected void ddlLevel3_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDataDDL(sender);
    }
    #endregion

    #region ddlLevel4_SelectedIndexChanged
    protected void ddlLevel4_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    #endregion

    #region ddlREQUIRE_TYPE_NAME_SelectedIndexChanged
    protected void ddlREQUIRE_TYPE_NAME_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    #endregion

    #region gvRequire_RowDataBound
    protected void gvRequire_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[2].Text = lblLevel1.Text;
            e.Row.Cells[3].Text = lblLevel2.Text;
            e.Row.Cells[4].Text = lblLevel3.Text;
            e.Row.Cells[5].Text = lblLevel4.Text;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            DropDownList ddl = (DropDownList)e.Row.FindControl("ddlREQUIRE_TYPE_NAME");
            if (ddl != null)
            {
                ddl.SelectedValue = dr["REQUIRE_TYPE"] + string.Empty;

            }
        }

    }
    #endregion

    #region gvRequireFile_RowDataBound
    protected void gvRequireFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[5].Text = lblLevel1.Text;
            e.Row.Cells[6].Text = lblLevel2.Text;
            e.Row.Cells[7].Text = lblLevel3.Text;
            e.Row.Cells[8].Text = lblLevel4.Text;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblCactive");
            if (rbl != null)
            {
                rbl.SelectedValue = dr["ISACTIVE"] + string.Empty;

            }
        }

    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            Button btn = (Button)sender;

            bool isRes = RequireBLL.Instance.RequireSave(btn.CommandArgument, "1", hidRequireTypeName.Value);
            if (isRes)
            {
                alertSuccess("แก้ไขข้อมูลสำเร็จ");
            }
            else
            {
                alertFail("แก้ไขข้อมูลไม่สำเร็จ");
            }
        }
        catch (Exception ex)
        {
            alertFail("แก้ไขข้อมูลไม่สำเร็จ : " + ex.Message);
        }
    }
    #endregion

    protected void mpConfirmSaveFile_ClickOK(object sender, EventArgs e)
    {
        try
        {
            Button btn = (Button)sender;
            int index = int.Parse(btn.CommandArgument);
            GridViewRow gvr = gvRequireFile.Rows[index];
            string ID = "0", UploadID = "0", UploadName = string.Empty, Extention = string.Empty, MaxFileSize = string.Empty, CACTIVE = "1";
            TextBox txtUploadName = (TextBox)gvr.FindControl("txtUploadName");
            if (txtUploadName != null)
            {
                UploadName = txtUploadName.Text.Trim();
            }
            TextBox txtExtention = (TextBox)gvr.FindControl("txtExtention");
            if (txtExtention != null)
            {
                Extention = txtExtention.Text.Trim();
            }
            TextBox txtMaxFileSize = (TextBox)gvr.FindControl("txtMaxFileSize");
            if (txtMaxFileSize != null)
            {
                MaxFileSize = txtMaxFileSize.Text.Trim();
            }
            RadioButtonList rbl = (RadioButtonList)gvr.FindControl("rblCactive");
            if (rbl != null)
            {
                CACTIVE = rbl.SelectedValue;
            }
            HiddenField hid = (HiddenField)gvr.FindControl("hidID");
            if (hid != null)
            {
                ID = hid.Value;
            }
            HiddenField hidUploadID = (HiddenField)gvr.FindControl("hidUploadID");
            if (hidUploadID != null)
            {
                UploadID = hidUploadID.Value;
            }

            bool isRes = RequireBLL.Instance.RequireFileSave(ID, CACTIVE, UploadName, Extention, MaxFileSize, UploadID);
            if (isRes)
            {
                alertSuccess("แก้ไขข้อมูลสำเร็จ");
            }
            else
            {
                alertFail("แก้ไขข้อมูลไม่สำเร็จ");
            }
        }
        catch (Exception ex)
        {
            alertFail("แก้ไขข้อมูลไม่สำเร็จ : " + ex.Message);
        }
    }
    #endregion

    #region methed
    #region DrowDownList
    private void SetDrowDownList()
    {
        DropDownListHelper.BindDropDownList(ref ddlMenu, RequireBLL.Instance.MenuSelect(), "FIELD_TYPE", "SMENUNAME", false);
    }
    #endregion

    #region BindData
    private void BindData()
    {
        DataTable dt = RequireBLL.Instance.RequireSelect(ddlLevel1.SelectedValue, ddlLevel2.SelectedValue, ddlLevel3.SelectedValue, ddlLevel4.SelectedValue, hidMREQUIRELEVELIDLevel1.Value, hidMREQUIRELEVELIDLevel2.Value, hidMREQUIRELEVELIDLevel3.Value, hidMREQUIRELEVELIDLevel4.Value, ddlMenu.SelectedValue, Level);

        DataTable dtFile = RequireBLL.Instance.RequireFileSelect(ddlLevel1.SelectedValue, ddlLevel2.SelectedValue, ddlLevel3.SelectedValue, ddlLevel4.SelectedValue, hidMREQUIRELEVELIDLevel1.Value, hidMREQUIRELEVELIDLevel2.Value, hidMREQUIRELEVELIDLevel3.Value, hidMREQUIRELEVELIDLevel4.Value, ddlMenu.SelectedValue, Level);
        gvRequire.DataSource = dt;
        gvRequire.DataBind();

        gvRequireFile.DataSource = dtFile;
        gvRequireFile.DataBind();
    }
    #endregion

    #region SetDataDDL
    private void SetDataDDL(object sender)
    {
        DropDownList ddl = (DropDownList)sender;
        DropDownList ddlLevel = new DropDownList();
        string MREQUIRELEVELID = string.Empty;
        switch (ddl.ID)
        {
            case "ddlLevel1": ddlLevel = ddlLevel2;
                MREQUIRELEVELID = hidMREQUIRELEVELIDLevel2.Value;
                break;
            case "ddlLevel2": ddlLevel = ddlLevel3;
                MREQUIRELEVELID = hidMREQUIRELEVELIDLevel3.Value;
                break;
            case "ddlLevel3": ddlLevel = ddlLevel4;
                MREQUIRELEVELID = hidMREQUIRELEVELIDLevel4.Value;
                break;
            default:
                break;
        }
        ddlLevel.Items.Clear();
        DropDownListHelper.BindDropDownList(ref ddlLevel, RequireBLL.Instance.RequireLevelDataSelect(MREQUIRELEVELID, ddl.SelectedValue), "ID", "DETAIL", true);

    }
    #endregion

    #region SetLevel
    private void SetLevel()
    {
        if (ViewState["DataLevel"] != null)
        {
            plLevel1.Visible = false;
            plLevel2.Visible = false;
            plLevel3.Visible = false;
            plLevel4.Visible = false;
            hidMREQUIRELEVELIDLevel1.Value = string.Empty;
            hidMREQUIRELEVELIDLevel2.Value = string.Empty;
            hidMREQUIRELEVELIDLevel3.Value = string.Empty;
            hidMREQUIRELEVELIDLevel4.Value = string.Empty;
            DataTable dt = (DataTable)ViewState["DataLevel"];
            Level = dt.Rows.Count;
            for (int i = 0; i < Level; i++)
            {
                switch (i)
                {
                    case 0:
                        hidMREQUIRELEVELIDLevel1.Value = dt.Rows[i]["ID"] + string.Empty;
                        plLevel1.Visible = true;
                        lblLevel1.Text = dt.Rows[i]["DETAIL"] + string.Empty;
                        ddlLevel1.Items.Clear();
                        DropDownListHelper.BindDropDownList(ref ddlLevel1, RequireBLL.Instance.RequireLevelDataSelect(dt.Rows[i]["ID"] + string.Empty, string.Empty), "ID", "DETAIL", true);
                        ddlLevel1.AutoPostBack = true;
                        if (Level == 1)
                        {
                            ddlLevel1.AutoPostBack = false;
                            gvRequire.Columns[3].Visible = false;
                            gvRequire.Columns[4].Visible = false;
                            gvRequire.Columns[5].Visible = false;
                            gvRequireFile.Columns[6].Visible = false;
                            gvRequireFile.Columns[7].Visible = false;
                            gvRequireFile.Columns[8].Visible = false;
                        }

                        break;
                    case 1: hidMREQUIRELEVELIDLevel2.Value = dt.Rows[i]["ID"] + string.Empty;
                        plLevel2.Visible = true;
                        lblLevel2.Text = dt.Rows[i]["DETAIL"] + string.Empty;
                        ddlLevel2.AutoPostBack = true;
                        if (Level == 2)
                        {
                            ddlLevel2.AutoPostBack = false;
                            gvRequire.Columns[4].Visible = false;
                            gvRequire.Columns[5].Visible = false;
                            gvRequireFile.Columns[7].Visible = false;
                            gvRequireFile.Columns[8].Visible = false;
                        }
                        break;
                    case 2: hidMREQUIRELEVELIDLevel3.Value = dt.Rows[i]["ID"] + string.Empty;
                        plLevel3.Visible = true;
                        lblLevel3.Text = dt.Rows[i]["DETAIL"] + string.Empty;
                        ddlLevel3.AutoPostBack = true;
                        if (Level == 3)
                        {
                            ddlLevel3.AutoPostBack = false;
                            gvRequire.Columns[5].Visible = false;
                            gvRequireFile.Columns[8].Visible = false;
                        }
                        break;
                    case 3: hidMREQUIRELEVELIDLevel4.Value = dt.Rows[i]["ID"] + string.Empty;
                        plLevel4.Visible = true;
                        lblLevel4.Text = dt.Rows[i]["DETAIL"] + string.Empty;
                        ddlLevel4.AutoPostBack = true;
                        if (Level == 4)
                        {
                            ddlLevel4.AutoPostBack = false;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    #endregion
    #endregion

    #region mpConfirmSaveFile_ClickOK

    #endregion
    
}