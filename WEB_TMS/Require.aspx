﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Require.aspx.cs" Inherits="Require" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">หน้าจอ</label>
        <div class="col-md-4">
            <asp:DropDownList runat="server" ID="ddlFieldType" Enabled="false" CssClass="form-control">
                        <asp:ListItem Text="รับเรื่องร้องเรียน" Value="COMPLAIN" />
                        <asp:ListItem Text="ข้อมูลพนักงาน" Value="VENDOR" />
                    </asp:DropDownList>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">เรื่องที่ร้องเรียน</label>
        <div class="col-md-4">
            <asp:DropDownList runat="server" ID="ddlTopic" CssClass="form-control" Enabled="false">
                    </asp:DropDownList>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">ประเภทเรื่องร้องเรียน - หลัก</label>
        <div class="col-md-4">
            <asp:DropDownList runat="server" ID="ddlComplainType" CssClass="form-control" Enabled="false">
                    </asp:DropDownList>
        </div>
        <div class="col-md-4"></div>
    </div>
    
    <div class="row form-group">
        <label  class="col-md-4 control-label">Field</label>
        <div class="col-md-4"><asp:DropDownList runat="server" ID="ddlField" Enabled="false" CssClass="form-control" >
                    </asp:DropDownList>
            <asp:HiddenField ID="hidM_COMPLAIN_REQUIRE_FIELD" runat="server" />
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">สถานะ</label>
        <div class="col-md-4">
            <asp:RadioButtonList ID="rblStatus" runat="server">
                <asp:ListItem Text="REQUIRE" Value="1" />
                <asp:ListItem Text="OPTIONAL" Value="2" />
                <asp:ListItem Text="DISABLE" Value="3" />
            </asp:RadioButtonList>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row form-group">
        <label  class="col-md-4 control-label">การใช้งาน</label>
        <div class="col-md-4">
            <asp:RadioButtonList ID="rblActive" runat="server">
                <asp:ListItem Text="ใช้งาน" Value="1" />
                <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
            </asp:RadioButtonList>
        </div>
        <div class="col-md-4"></div>
    </div>
                
    <div class="row form-group">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <center>
                        <input id="btnSave" type="button" value="บันทึก" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info" />&nbsp;&nbsp;&nbsp;
                <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-info" />
            </center>
        </div>
        <div class="col-md-4"></div>
    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmBack_ClickOK" TextTitle="ยืนยันการย้อนกลับ" TextDetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

