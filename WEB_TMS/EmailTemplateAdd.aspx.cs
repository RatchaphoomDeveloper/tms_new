﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using TMS_BLL.Master;

public partial class EmailTemplateAdd : PageBase
{
    #region + View State +
    private int TemplateID
    {
        get
        {
            if ((int)ViewState["TemplateID"] != null)
                return (int)ViewState["TemplateID"];
            else
                return 0;
        }
        set
        {
            ViewState["TemplateID"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            txtBody.Settings.AllowHtmlView = false;
            txtBody.Settings.AllowPreview = false;

            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadEmailType();

            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["TEMPLATE_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);

                TemplateID = int.Parse(decryptedValue);
                this.LoadData();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadEmailType()
    {
        try
        {
            DataTable dtEmailType = EmailTemplateBLL.Instance.EmailTypeSelectBLL(" AND M_EMAIL_TYPE.ISACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlEmailType, dtEmailType, "EMAIL_TYPE_ID", "EMAIL_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadData()
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectAllBLL(" AND TEMPLATE_ID = " + TemplateID.ToString());
            if (dtTemplate.Rows.Count > 0)
            {
                ddlEmailType.SelectedValue = dtTemplate.Rows[0]["EMAIL_TYPE_ID"].ToString();

                txtTemplateName.Text = dtTemplate.Rows[0]["TEMPLATE_NAME"].ToString();
                txtSubject.Text = dtTemplate.Rows[0]["SUBJECT"].ToString();
                txtParameter.Text = dtTemplate.Rows[0]["MAIL_PARAMETER"].ToString();
                txtBody.Html = dtTemplate.Rows[0]["BODY"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            EmailTemplateBLL.Instance.EmailTemplateUpdateBLL(ddlEmailType.SelectedValue, TemplateID, txtTemplateName.Text.Trim(), txtSubject.Text.Trim(), txtBody.Html);
            alertSuccess("บันทึกข้อมูล เรียบร้อย", "EmailTemplate.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlEmailType.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก ประเภทอีเมล์");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}