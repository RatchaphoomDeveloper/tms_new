﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

public partial class vendor_confirm_contract : System.Web.UI.Page
{
    int defInt;
    // Connection 
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string UploadDirectory = "~/UploadFile/EvidenceOfWrongdoing/";
    const int ThumbnailSize = 100;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        xgvw.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(xgvw_CustomColumnDisplayText);
        xgvw.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(xgvw_AfterPerformCallback);
        xgvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(xgvw_HtmlRowPrepared);
        xgvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(xgvw_HtmlDataCellPrepared);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("1", "R", "เปิดดูข้อมูลหน้า ยืนยันรถตามสัญญา", "");


            Cache.Remove(sdsContract.CacheKeyDependency);
            Cache[sdsContract.CacheKeyDependency] = new object();
            sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsContract.DataBind();

            Cache.Remove(sdsTruck.CacheKeyDependency);
            Cache[sdsTruck.CacheKeyDependency] = new object();
            sdsTruck.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsTruck.DataBind();

            Session["ss_data"] = null; Session["IssueData"] = null; Session["DataAttechment"] = null;
            dteStart.Text = DateTime.Now.ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.ToString("dd/MM/yyyy");//.AddDays(1)
            SearchData("");
        }
        else
        {
            /*UploadControl.ShowProgressPanel = true;*/
        }
    }
    #region Function
    protected void SearchData(string _mode)
    {
        Session["ss_data"] = null;
        imp_TTRUCKCONFIRM();

        Cache.Remove(sdsContract.CacheKeyDependency);
        Cache[sdsContract.CacheKeyDependency] = new object();
        sdsContract.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsContract.DataBind();
        xgvw.DataBind();

        DataView dv = (DataView)sdsContract.Select(DataSourceSelectArguments.Empty);
        DataTable dt = dv.ToTable();
        lblRecord.Text = dt.Select("CCONFIRM<>'1'").Length + "";
        dt.Dispose();

        Cache.Remove(sdsTruck.CacheKeyDependency);
        Cache[sdsTruck.CacheKeyDependency] = new object();
        sdsTruck.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsTruck.DataBind();

        Cache.Remove(sdsCheckLists.CacheKeyDependency);
        Cache[sdsCheckLists.CacheKeyDependency] = new object();
        sdsCheckLists.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsCheckLists.DataBind();
    }
    protected void imp_TTRUCKCONFIRM()
    {
        #region Update กรณีสั่งให้แก้ แล้วไม่แก้ไขและเกินวันที่กำหนดแล้ว ให้ระบบตีเป็นห้ามวิ่ง
        using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            OracleCommand U_TTRUCK_Cmd = new OracleCommand();
            U_TTRUCK_Cmd.Connection = OraConnection;
            if (OraConnection.State == ConnectionState.Closed) OraConnection.Open();
            U_TTRUCK_Cmd.CommandType = CommandType.Text;
            U_TTRUCK_Cmd.CommandText = @" UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID IN(
        SELECT DISTINCT TCHECKTRUCK.STRUCKID
        FROM TCheckTruckItem 
        LEFT JOIN TCHECKTRUCK ON TCheckTruckItem.SCHECKID =TCheckTruck.SCHECKID 
        LEFT JOIN TTRUCK ON TCHECKTRUCK.STRUCKID=TTRUCK.STRUCKID 
        WHERE NVL(TCheckTruckItem.CEDITED,'0') !='1' AND TCheckTruckItem.CMA IN('2','1')
         AND TCheckTruckItem.DEND_LIST < SYSDATE AND NVL(TTRUCK.CHOLD,'0')='0')";
            U_TTRUCK_Cmd.ExecuteNonQuery();
        }
        #endregion
        if ((!String.IsNullOrEmpty(dteStart.Value + "")) && (!String.IsNullOrEmpty(dteEnd.Value + "")))
        {
            OracleCommand oraCmd = new OracleCommand();
            oraCmd.Connection = connection;
            if (connection.State == ConnectionState.Closed) connection.Open();

            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = "Imp_ContractConfirm";

            oraCmd.Parameters.Add("D_BEGIN", OracleType.NVarChar).Value = (object)Convert.ToDateTime(dteStart.Value).AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            oraCmd.Parameters.Add("D_END", OracleType.NVarChar).Value = (object)Convert.ToDateTime(dteEnd.Value).AddDays(2).ToString("dd/MM/yyyy", new CultureInfo("en-US"));//แอดล่วงหน้า 1 วัน
            oraCmd.Parameters.Add("usr_SUID", OracleType.VarChar).Value = Session["UserID"] + "";

            oraCmd.ExecuteNonQuery();
        }
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool isProcessSucceeded = (e.Exception == null);
        Session["FLAGIUCOMMAND"] = isProcessSucceeded;

        if (isProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected DataTable PrepareDataTable(string ss_name, string sdate, string contractid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] != null)
        {
            dtData = (DataTable)Session[ss_name];
            foreach (DataRow drDel in dtData.Select("DDATE='" + sdate + "' AND SCONTRACTID='" + contractid + "'"))
            {
                int idx = dtData.Rows.IndexOf(drDel);
                dtData.Rows[idx].Delete();
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }
    protected bool IsSelectLongLenght(ASPxGridView xgvwCar)
    {
        string cmb_headvalues = "", cmb_headcheck = "", cmb_tailvalues = "", cmb_tailvaluescheck = "", IsValid_Values = "";

        for (int record = 0; record < xgvwCar.VisibleRowCount; record++)
        {
            ASPxComboBox cmb_SHEADREGISTERNO = xgvwCar.FindRowCellTemplateControl(record, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(หัว) "], "cmbSHEADREGISTERNO") as ASPxComboBox;
            ASPxComboBox cmb_STRAILERREGISTERNO = xgvwCar.FindRowCellTemplateControl(record, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(ท้าย) "], "cmbSTRAILERREGISTERNO") as ASPxComboBox;
            cmb_headvalues = (String.IsNullOrEmpty("" + cmb_SHEADREGISTERNO.Value)) ? "" : cmb_SHEADREGISTERNO.Value.ToString();
            for (int chkrecord = 0; chkrecord < xgvwCar.VisibleRowCount; chkrecord++)
            {
                ASPxComboBox cmbSHEADREGISTERNO = xgvwCar.FindRowCellTemplateControl(chkrecord, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(หัว) "], "cmbSHEADREGISTERNO") as ASPxComboBox;
                ASPxComboBox cmbSTRAILERREGISTERNO = xgvwCar.FindRowCellTemplateControl(chkrecord, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(ท้าย) "], "cmbSTRAILERREGISTERNO") as ASPxComboBox;

                cmb_headcheck = (String.IsNullOrEmpty("" + cmbSHEADREGISTERNO.Value)) ? "" : cmbSHEADREGISTERNO.Value.ToString();

                if (record != chkrecord && (cmb_headvalues != "" && cmb_headcheck != "") && cmb_headvalues.Equals(cmb_headcheck))
                {
                    cmb_SHEADREGISTERNO.IsValid = false;
                    IsValid_Values += "," + cmbSHEADREGISTERNO.Text;
                    cmb_SHEADREGISTERNO.ErrorText = "ระบุซ้ำ!(ลำดับที่ " + (chkrecord + 1) + ")";
                    cmb_SHEADREGISTERNO.Validate();
                }
            }
        }
        IsValid_Values = (IsValid_Values.Length > 0) ? IsValid_Values.Remove(0, 1) : "";
        return (IsValid_Values.Length > 0) ? false : true;
    }
    protected void BackUpData(int visibleindex, ASPxGridView xgvwCar)
    {

        dynamic values = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
        DataTable dtData = PrepareDataTable("ss_data", "" + values[1], "" + values[0], "SCONTRACTID", "DDATE", "STRUCKID", "SHEADID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND");
        DataRow dr;
        int csend = 0;
        //return;dynamic dataCUSTOMCALLBACKs = xgvwCar.GetRowValues(0, "SCONTRACTID", "DDATE", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND");

        for (int rows = 0; rows < xgvwCar.VisibleRowCount; rows++)
        {
            ASPxButton imbconfirm = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["#"], "imbconfirm") as ASPxButton;
            ASPxButton imbcancel = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["#"], "imbcancel") as ASPxButton;
            ASPxTextBox txtconfirm = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["#"], "txtconfirm") as ASPxTextBox;
            ASPxTextBox txtPassed = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["ใบตรวจสภาพรถ"], "txtPassed") as ASPxTextBox;
            ASPxComboBox cmbSHEADREGISTERNO = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(หัว) "], "cmbSHEADREGISTERNO") as ASPxComboBox;
            ASPxComboBox cmbSTRAILERREGISTERNO = xgvwCar.FindRowCellTemplateControl(rows, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(ท้าย) "], "cmbSTRAILERREGISTERNO") as ASPxComboBox;
            string V_HEADREGISTERNO = (String.IsNullOrEmpty(cmbSHEADREGISTERNO.Value + "")) ? "" : cmbSHEADREGISTERNO.Value.ToString();
            string S_HEADREGISTERNO = (String.IsNullOrEmpty(cmbSHEADREGISTERNO.Text + "")) ? "" : cmbSHEADREGISTERNO.Text.ToString();
            string V_TRAILERREGISTERNO = (String.IsNullOrEmpty(cmbSTRAILERREGISTERNO.Value + "")) ? "" : cmbSTRAILERREGISTERNO.Value.ToString();
            string S_TRAILERREGISTERNO = (String.IsNullOrEmpty(cmbSTRAILERREGISTERNO.Text + "")) ? "" : cmbSTRAILERREGISTERNO.Text.ToString();
            string val = "" + txtconfirm.Value;
            dynamic dataCUSTOMCALLBACK = xgvwCar.GetRowValues(rows, "SCONTRACTID", "DDATE", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND");
            dr = dtData.NewRow();
            dr["SCONTRACTID"] = "" + dataCUSTOMCALLBACK[0];
            dr["DDATE"] = "" + dataCUSTOMCALLBACK[1];
            dr["STRUCKID"] = "" + dataCUSTOMCALLBACK[2];
            dr["SHEADID"] = (V_HEADREGISTERNO != "") ? V_HEADREGISTERNO : "" + dataCUSTOMCALLBACK[2];
            dr["SHEADREGISTERNO"] = (S_HEADREGISTERNO != "") ? S_HEADREGISTERNO : "" + dataCUSTOMCALLBACK[3];
            dr["STRAILERID"] = (V_TRAILERREGISTERNO != "") ? V_TRAILERREGISTERNO : "" + dataCUSTOMCALLBACK[4];
            dr["STRAILERREGISTERNO"] = (S_TRAILERREGISTERNO != "") ? S_TRAILERREGISTERNO : "" + dataCUSTOMCALLBACK[5];
            dr["CSTANDBY"] = "" + dataCUSTOMCALLBACK[6];
            dr["NTOTALCAPACITY"] = "" + dataCUSTOMCALLBACK[7];
            dr["CPASSED"] = (val == "1") ? "" + txtPassed.Value : "0";//ถ้าไม่สามารถยืนยันรถได้ จะตีเป็นตรวจสถาพไม่ผ่านโดยอัตโนมัติ
            dr["CSEND"] = "" + val;
            csend += ((val == "1") ? 1 : 0);
            dtData.Rows.Add(dr);
        }

        Session["ss_data"] = dtData;
    }
    protected void DataBindTab2()
    {


    }
    string SavePostedFiles(UploadedFile uploadedFile)
    {
        string PathUploadDirectory = UploadDirectory + "/" + "";
        dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
        string IssueData = "" + Session["IssueData"];
        /*
         *0     14$58
         *1     58
         *2     "TR4901473"
         *3     "กท.75-0887"
         *4     ""
         *5     "#"
        */
        string[] ArrayIssueData = IssueData.Split('^');
        PathUploadDirectory = ArrayIssueData[1] + "/" + ArrayIssueData[2] + "/" + Convert.ToDateTime("" + MasterData[1]).ToString("MMddyyyy", new CultureInfo("en-US")) + "/";

        if (!uploadedFile.IsValid)
            return string.Empty;

        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);
        if (!Directory.Exists(UploadDirectory + PathUploadDirectory)) Directory.CreateDirectory(MapPath(UploadDirectory + PathUploadDirectory));
        string resFileName = MapPath(UploadDirectory + PathUploadDirectory) + fileInfo.Name;

        if (File.Exists(resFileName))
        {
            string sPath = Path.GetDirectoryName(resFileName)
                , sFileName = Path.GetFileNameWithoutExtension(resFileName)
                , sFileType = Path.GetExtension(resFileName);
            int nExists = Directory.GetFiles(Path.GetDirectoryName(resFileName), "*" + Path.GetFileName(resFileName)).Length;
            sPath = sPath + "\\" + "(" + nExists + ")" + sFileName + sFileType;
            File.Copy(resFileName, sPath, true);
        }

        string fileLabel = fileInfo.Name;
        string fileLength = uploadedFile.ContentLength / 1024 + "K";
        #region temp Data Attechment
        {//temp Data Attechment 
            DataTable dtAttechment = new DataTable();
            #region prepaire temp data Attechment storage
            if (Session["DataAttechment"] == null)
            {
                dtAttechment.Columns.Add("SCHECKID", typeof(string));
                dtAttechment.Columns.Add("NATTACHMENT", typeof(string));
                dtAttechment.Columns.Add("STRUCKID", typeof(string));
                dtAttechment.Columns.Add("SCONTRACTID", typeof(string));
                dtAttechment.Columns.Add("SATTACHTYPEID", typeof(string));
                dtAttechment.Columns.Add("SPATH", typeof(string));
                dtAttechment.Columns.Add("SFILE", typeof(string));
                dtAttechment.Columns.Add("SSYSFILE", typeof(string));
                dtAttechment.Columns.Add("DCREATE", typeof(string));
                dtAttechment.Columns.Add("SCREATE", typeof(string));
                dtAttechment.Columns.Add("DUPDATE", typeof(string));
                dtAttechment.Columns.Add("SUPDATE", typeof(string));
                dtAttechment.Columns.Add("cRowFlag", typeof(string));
            }
            else
            {
                dtAttechment = (DataTable)Session["DataAttechment"];
                foreach (DataRow drRemove in dtAttechment.Select("cRowFlag='Page' AND SFILE='" + fileInfo.Name + "'")) dtAttechment.Rows[dtAttechment.Rows.IndexOf(drRemove)].Delete();

            }
            #endregion
            DataRow drAttechment;

            drAttechment = dtAttechment.NewRow();
            drAttechment["SCHECKID"] = "";
            drAttechment["NATTACHMENT"] = dtAttechment.Rows.Count + 1;
            drAttechment["SCONTRACTID"] = "" + ArrayIssueData[1];
            drAttechment["STRUCKID"] = "" + ArrayIssueData[2];
            drAttechment["SATTACHTYPEID"] = "";
            drAttechment["SPATH"] = "" + UploadDirectory + PathUploadDirectory;
            drAttechment["SFILE"] = "" + fileInfo.Name;
            drAttechment["SSYSFILE"] = "" + fileInfo.Name;
            drAttechment["DCREATE"] = "" + Convert.ToDateTime("" + MasterData[1]).ToString("MM/dd/yyyy", new CultureInfo("en-US"));
            drAttechment["SCREATE"] = "" + Session["SVDID"];
            drAttechment["DUPDATE"] = "";
            drAttechment["SUPDATE"] = "";
            drAttechment["cRowFlag"] = "Page";

            dtAttechment.Rows.Add(drAttechment);
            Session["DataAttechment"] = dtAttechment;

        }
        #endregion

        uploadedFile.SaveAs(resFileName);

        return string.Format("{0} ({1})|{2}#{3}", fileLabel, fileLength, fileInfo.Name, (UploadDirectory + PathUploadDirectory).Remove(0, 2));
    }
    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    { //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO 
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        //repoint.DREDUCE = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];

        repoint.Insert();
        return IsReduce;
    }
    protected void BindCheckListData(ASPxGridView gvw, string SCONTRACTID, string STRUCKID)
    {
        string qry = @"SELECT CHKTRCK.*, ICHKTRCK.*
,ICHKTRCK.CMA ICMA,ICHKTRCK.NDAY_MA INDAY_MA
FROM (
    SELECT distinct * FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + STRUCKID + @"'
)CHKTRCK
LEFT JOIN (
    SELECT TCHECKTRUCKITEM.*, TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SCREATENAME
    FROM TCHECKTRUCKITEM LEFT JOIN TUser ON TCHECKTRUCKITEM.SCREATE=TUSER.SUID
    WHERE 1=1 AND NVL(CEDITED,'0')='0'
     and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + STRUCKID + @"')
)ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
WHERE 1=1 AND NVL(CHKTRCK.CCLEAN,'0')='0'  AND NVL(ICHKTRCK.SCHECKID,'-9999999')!='-9999999'";
        DataTable dtCheckData = CommonFunction.Get_Data(connection, qry);

        for (int idxgvw = 0; idxgvw < gvw.VisibleRowCount; idxgvw++)
        {
            dynamic ArrayRowValues = gvw.GetRowValues(idxgvw, "SCHECKLISTID", "SCHECKLISTNAME", "STOPICID", "SVERSION", "SVERSIONLIST", "NLIST", "NDAY_MA", "NPOINT", "CCUT", "CBAN");
            ASPxTextBox txtSCHECKLISTID = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "txtSCHECKLISTID") as ASPxTextBox;
            ASPxTextBox txtIsChecked = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "txtIsChecked") as ASPxTextBox;
            ASPxCheckBox cbxSCHECKLISTID = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "cbxSCHECKLISTID") as ASPxCheckBox;
            ASPxLabel lblCHOLD = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblCHOLD") as ASPxLabel;
            ASPxLabel lblMAnDay = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblMAnDay") as ASPxLabel;
            Label lblCheckBy = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lblCheckBy") as Label;
            ASPxHyperLink lnkEdited = gvw.FindRowCellTemplateControl(idxgvw, (GridViewDataColumn)gvw.Columns[0], "lnkEdited") as ASPxHyperLink;
            string CBAN = "", CCUT = "", NDAY_MA = "", ReportBy = "", SCHECKID = ""; int HasCheckList = 0;

            foreach (DataRow drCheckData in dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "'"))
            {
                HasCheckList = 1;
                SCHECKID = drCheckData["SCHECKID"] + "";
                ReportBy = drCheckData["SCREATENAME"] + "";
                //CBAN = (drCheckData["CMA"] + "" == "2") ? "1" : "0";
                //CCUT = (drCheckData["CMA"] + "" == "1") ? "1" : "0";
                //NDAY_MA = drCheckData["NDAY_MA"] + "";
                break;
            }
            CBAN = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'").Length > 0 ? "1" : "0";
            CCUT = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'").Length > 0 ? "1" : "0";
            DataRow[] drBan = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='2'", "DEND_LIST ,DBEGIN_LIST");
            DataRow[] drNDAYMA = dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "' AND ICMA='1'", "DEND_LIST ,DBEGIN_LIST");
            NDAY_MA = drNDAYMA.Length > 0 ? "" + drNDAYMA[0]["INDAY_MA"] : "0";
            if (HasCheckList > 0)//(dtCheckData.Select("SCHECKLISTID='" + ArrayRowValues[0] + "'").Length > 0)
            {
                string SCHECKLISTID = ((CBAN == "1") ? "2" : ((CCUT == "1") ? "1" : "0"));
                txtSCHECKLISTID.Text = ((CBAN == "1") ? "2" : ((CCUT == "1") ? "1" : "0"));
                lblCHOLD.ClientVisible = (SCHECKLISTID == "2") ? true : false;
                lblCHOLD.Text = "ห้ามวิ่ง" + (drBan.Length > 0 ? "(เมื่อ " + Convert.ToDateTime("" + drBan[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "");
                lblMAnDay.ClientVisible = (SCHECKLISTID == "1") ? true : false;
                lblMAnDay.Text = (NDAY_MA != "") ? "แก้ไขภายใน " + NDAY_MA + " วัน" + (drNDAYMA.Length > 0 ? "(ภายใน " + Convert.ToDateTime("" + drNDAYMA[0]["DEND_LIST"]).ToString("dd MMM yyyy") + " )" : "") : "";
            }
            cbxSCHECKLISTID.Checked = (HasCheckList > 0) ? true : false;
            cbxSCHECKLISTID.ClientEnabled = !cbxSCHECKLISTID.Checked;
            txtIsChecked.Text = (HasCheckList > 0) ? "1" : "0";
            if (cbxSCHECKLISTID.Checked)
            {
                lblCheckBy.Text = "<img title='" + ReportBy + "' src='images/ic_owner.gif' />";
                lnkEdited.Text = "<img title='คลิ๊กเพื่อส่งตรวจ' src='images/btnCon1.gif' />";
                lnkEdited.ClientSideEvents.Click = "function(s,e){UpdateCheckListItem('" + lnkEdited.ClientID + "','" + SCONTRACTID + "','" + STRUCKID + "','" + ArrayRowValues[0] + "','" + SCHECKID + "');}";
            }
            lblCheckBy.Visible = cbxSCHECKLISTID.Checked;
            lnkEdited.Visible = cbxSCHECKLISTID.Checked;


        }
        dtCheckData.Dispose();
    }
    protected void BindTruckWithDataInGridEditing(ASPxGridView _gvw, params string[] sArrayParams)
    {
        int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
        dynamic data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
        string _Query = "";
        _Query = string.Format(@"SELECT '' SKEYID,CTRK.STRUCKID,TRCK.SHEADREGISTERNO,CTRK.STRAILERID,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,NVL(CTRK.CSTANDBY,'N') CSTANDBY
,CONT.SCONTRACTID ,'{1}' DDATE,'N' CSEND
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END CMA, MAX(CMA) MAXCMA
,NVL(MIN(chktrck.DFINAL_MA),'') DFINAL_MA
FROM 
( 
  SELECT * 
  FROM TCONTRACT
  WHERE 1=1 AND CACTIVE='Y' AND SCONTRACTID='{0}' 
) cont
LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID
LEFT JOIN 
(
   SELECT chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO , MAX(chktrk.CMA) CMA,MIN(DBEGIN_MA) DBEGIN_MA,MIN(DFINAL_MA) DFINAL_MA,MIN(chktrk.NDAY_MA) NDAY_MA,COUNT(STRUCKID) nClean
   FROM TCheckTruck  chktrk  LEFT JOIN TCheckTruckITEM ichktrk ON chktrk.SCHECKID=ichktrk.SCHECKID
   WHERE 1=1 AND cClean='0' AND NVL(ichktrk.COTHER,'0')!='1'
   GROUP BY chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO --,chktrk.CMA
) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE CACTIVE='Y'
) trck ON  CTRK.STRUCKID=trck.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
LEFT JOIN 
(
  SELECT tconf.NCONFIRMID ,tconf.SCONTRACTID  ,tconf.ddate,tconf.cconfirm tconf_cconfirm
  ,tconflst.nlistno ,tconflst.cconfirm tconflst_cconfirm ,tconflst.struckid ,tconflst.sheadid ,tconflst.sheadregisterno ,tconflst.strailerid ,tconflst.strailerregisterno ,tconflst.cstanby ,tconflst.cpassed
 FROM ttruckconfirm tconf 
  LEFT JOIN ttruckconfirmlist tconflst ON tconf.nconfirmid=tconflst.nconfirmid
  where tconf.scontractid='{0}' AND tconf.DDATE=TO_DATE('{2}','DD/MM/YYYY')
) conf ON CONT.SCONTRACTID=conf.SCONTRACTID and ctrk.struckid=conf.struckid
WHERE 1=1 AND TRCK.CACTIVE='Y' AND  NVL(CTRK.CSTANDBY,'N')='N'
AND cont.SCONTRACTID='{0}'  
GROUP BY CTRK.STRUCKID,TRCK.SHEADREGISTERNO,CTRK.STRAILERID,TRCK.STRAILERREGISTERNO,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,NVL(CTRK.CSTANDBY,'N'),CONT.SCONTRACTID
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO "
            , "" + data[0], "" + data[1], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));
        DataTable _dtTruck = CommonFunction.Get_Data(connection, _Query);
        DataView _dvTruck = new DataView(_dtTruck);
        _dvTruck.Sort = "SHEADREGISTERNO ASC";
        _gvw.DataSource = _dvTruck;
        _gvw.DataBind();

    }
    #endregion
    #region event
    protected void sdsContract_OnDeleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sdsContract_OnDeleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    //CallBlackPanel
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":
                SearchData("search");
                break;
            case "checkdata":
                string msg = "";
                for (int nRows = 0; nRows < xgvw.VisibleRowCount; nRows++)
                {//loop grid 's contract
                    if (xgvw.Selection.IsRowSelected(nRows))
                    {
                        dynamic griddata = xgvw.GetRowValues(nRows, "SCONTRACTID", "DDATE", "NCONFIRMID", "NTRUCK", "NCONFIRM", "CCONFIRM", "DEXPIRE");
                        ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(nRows, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;
                        if ((Convert.ToDateTime(griddata[6] + "") < DateTime.Now))
                        {//ถ้ายังลยระยะเวลาที่กำหนดแล้ว
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('ข้อความจากระบบ','ท่านมีบางรายการสัญญาที่ <b>ไม่ยืนยันรถ</b>ได้เนื่องจากเลยระยะเวลาที่กำหนด <br /> กรุณาตรวจสอบ อีกครั้ง!')");
                            break;
                        }
                        if ("" + griddata[4] != txtnTruckConfirm.Text)
                        {
                            msg += "1";
                        }
                    }
                }
                if (msg != "")
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','<b>ท่านมีบางรายการสัญญาที่ยืนยันรถไม่ครบตามจำนวน</b> <br /> ท่านต้องการทำการ <b>ยืนยันรถ</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('senddata');} ,function(s,e){ dxPopupConfirm.Hide(); })");
                }
                else { CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','ท่านต้องการทำการ <b>ยืนยันรถ</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('senddata');} ,function(s,e){ dxPopupConfirm.Hide(); })"); }
                break;
            case "senddata":

                string dStart = dteStart.Value.ToString();
                string dEnd = dteEnd.Value.ToString();
                string sVendorID = "" + Session["SVDID"];
                string exec = "";
                DataTable dtContract_Truck = CommonFunction.Get_Data(connection, @"SELECT rownum NLISTNO, TRCK.STRUCKID,TRCK.SHEADID ,TRCK.SHEADREGISTERNO ,TRCK.STRAILERID ,TRCK.STRAILERREGISTERNO ,NVL(TRCK.CSTATUS,'0') CSTATUS ,NVL(TRCK.CACCIDENT,'0') CACCIDENT 
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CONT_TRCK.SCONTRACTID ,CONT.SCONTRACTNO ,CONT_TRCK.CSTANDBY ,CONT_TRCK.CREJECT ,CONT_TRCK.DSTART ,CONT_TRCK.DEND
 FROm TCONTRACT CONT
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT_TRCK.SCONTRACTID=CONT.SCONTRACTID
 LEFT JOIN (
        SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y' 
)TRCK ON CONT_TRCK.STRUCKID=TRCK.STRUCKID 
 WHERE CONT.CACTIVE='Y' AND NVL(CONT.NTRUCK,'0')<>'0'  AND  CONT.SVENDORID='" + sVendorID + @"'  
 AND TRCK.CACTIVE='Y' AND NVL(CSTANDBY,'N') ='N' ");
                TTRUCKCONFIRMLIST conf_list = new TTRUCKCONFIRMLIST(connection);
                if (Session["ss_data"] == null)
                {
                    #region Send All Hot Event
                    for (int nGridRows = 0; nGridRows < xgvw.VisibleRowCount; nGridRows++)
                    {
                        if (xgvw.Selection.IsRowSelected(nGridRows))
                        {
                            dynamic griddata = xgvw.GetRowValues(nGridRows, "SCONTRACTID", "DDATE", "NCONFIRMID", "DEXPIRE");
                            if ((Convert.ToDateTime(griddata[3] + "") < DateTime.Now)) break;//ถ้ายังเลยระยะเวลาที่กำหนดแล้ว
                            conf_list.NCONFIRMID = "" + griddata[2];
                            foreach (DataRow drContract_Truck in dtContract_Truck.Select("SCONTRACTID='" + griddata[0] + "'"))
                            {
                                conf_list.NLISTNO = "" + drContract_Truck["NLISTNO"];
                                conf_list.STRUCKID = "" + drContract_Truck["STRUCKID"];
                                conf_list.SHEADID = "" + drContract_Truck["STRUCKID"];
                                conf_list.SHEADREGISTERNO = "" + drContract_Truck["SHEADREGISTERNO"];
                                conf_list.STRAILERID = "" + drContract_Truck["STRAILERID"];
                                conf_list.STRAILERREGISTERNO = "" + drContract_Truck["STRAILERREGISTERNO"];
                                conf_list.CCONFIRM = ("" + drContract_Truck["CHOLD"] == "1") ? "0" : "1";
                                conf_list.CPASSED = ("" + drContract_Truck["CHOLD"] == "1") ? "0" : "1";
                                conf_list.CSTANBY = "" + drContract_Truck["CSTANDBY"];
                                conf_list.SCREATE = "" + Session["UserID"]; ;
                                conf_list.SUPDATE = "" + Session["UserID"];

                                conf_list.Insert();
                            }
                            conf_list.CommitChild();
                        }
                    }
                    #endregion
                }
                else
                {
                    DataTable dt_data = (DataTable)Session["ss_data"];
                    #region Send
                    for (int nGridRows = 0; nGridRows < xgvw.VisibleRowCount; nGridRows++)
                    {//loop grid 's contract
                        if (xgvw.Selection.IsRowSelected(nGridRows))
                        {
                            dynamic griddata = xgvw.GetRowValues(nGridRows, "SCONTRACTID", "DDATE", "NCONFIRMID", "DEXPIRE");
                            if ((Convert.ToDateTime(griddata[3] + "") < DateTime.Now)) break;//ถ้าเลยระยะเวลาที่กำหนดแล้ว
                            conf_list.NCONFIRMID = "" + griddata[2];
                            DataRow[] drdata = dt_data.Select("SCONTRACTID='" + griddata[0] + "' AND DDATE='" + griddata[1] + "'");
                            if (drdata.Length > 0)
                            {
                                foreach (DataRow dr_data in drdata)
                                {// "SCONTRACTID", "DDATE", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO", "CSTANDBY", "NTOTALCAPACITY", "CPASSED", "CSEND"
                                    conf_list.NLISTNO = "";
                                    conf_list.STRUCKID = "" + dr_data["STRUCKID"];
                                    conf_list.SHEADID = "" + dr_data["STRUCKID"];
                                    conf_list.SHEADREGISTERNO = "" + dr_data["SHEADREGISTERNO"];
                                    conf_list.STRAILERID = "" + dr_data["STRAILERID"];
                                    conf_list.STRAILERREGISTERNO = "" + dr_data["STRAILERREGISTERNO"];
                                    conf_list.CCONFIRM = "" + dr_data["CSEND"];
                                    conf_list.CPASSED = "" + dr_data["CPASSED"];
                                    conf_list.CSTANBY = "" + dr_data["CSTANDBY"];
                                    conf_list.SCREATE = "" + Session["UserID"]; ;
                                    conf_list.SUPDATE = "" + Session["UserID"];
                                    conf_list.Insert();
                                }
                            }
                            else
                            {
                                foreach (DataRow drContract_Truck in dtContract_Truck.Select("SCONTRACTID='" + griddata[0] + "'"))
                                {
                                    conf_list.NLISTNO = "" + drContract_Truck["NLISTNO"];
                                    conf_list.STRUCKID = "" + drContract_Truck["STRUCKID"];
                                    conf_list.SHEADID = "" + drContract_Truck["STRUCKID"];
                                    conf_list.SHEADREGISTERNO = "" + drContract_Truck["SHEADREGISTERNO"];
                                    conf_list.STRAILERID = "" + drContract_Truck["STRAILERID"];
                                    conf_list.STRAILERREGISTERNO = "" + drContract_Truck["STRAILERREGISTERNO"];
                                    conf_list.CCONFIRM = "1";
                                    conf_list.CPASSED = "1";
                                    conf_list.CSTANBY = "" + drContract_Truck["CSTANDBY"];
                                    conf_list.SCREATE = "" + Session["UserID"]; ;
                                    conf_list.SUPDATE = "" + Session["UserID"];

                                    conf_list.Insert();
                                }
                            }
                            conf_list.CommitChild();
                        }
                    }
                    #endregion

                }
                SearchData("search");
                //ต้องเคลียร์รายการที่เลือกไว้ด้วย
                break;
        }
    }
    //CallBlackPanel
    protected void cpnCheckList_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

    }
    //GridViewMaster
    void xgvw_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "") e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void xgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        ASPxGridView xgvwCar = (ASPxGridView)xgvw.FindEditFormTemplateControl("xgvwCar");
        if (xgvwCar == null) CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('ข้อความจากระบบ','ระบบไม่สามารถแสดงรายการ รถ <br>ได้เนื่องจากการทำงานที่ผิดพลาดหรือนานเกินไป กรุณาตรวจสอบ อีกครั้ง!')");
        int visibleindex = 0;
        string CallbackName = (e.CallbackName.Equals("STARTEDIT") || e.CallbackName.Equals("CANCELEDIT") || e.CallbackName.Equals("SORT")) ? e.CallbackName : e.Args[0].ToUpper();//switch CallbackName 
        switch (e.CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                xgvw.CancelEdit();
                SearchData("");//xgvw.DataBind();
                break;
            case "STARTEDIT":
                #region Comments STARTEDIT
                BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                /*
                visibleindex = xgvw.FindVisibleIndexByKeyValue(e.Args.GetValue(0));
                dynamic data = xgvw.GetRowValues(visibleindex, "SCONTRACTID", "DDATE");
                string _Query = "";
                _Query = string.Format(@"SELECT '' SKEYID,CTRK.STRUCKID,TRCK.SHEADREGISTERNO,CTRK.STRAILERID,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,NVL(CTRK.CSTANDBY,'N') CSTANDBY
,CONT.SCONTRACTID ,'{1}' DDATE,'N' CSEND
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END CMA, MAX(CMA) MAXCMA
,NVL(MIN(chktrck.DFINAL_MA),'') DFINAL_MA
FROM 
( 
  SELECT * 
  FROM TCONTRACT
  WHERE 1=1 AND CACTIVE='Y' AND SCONTRACTID='{0}' 
) cont
LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID
LEFT JOIN 
(
   SELECT chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,chktrk.CMA,MIN(DBEGIN_MA) DBEGIN_MA,MIN(DFINAL_MA) DFINAL_MA,MIN(chktrk.NDAY_MA) NDAY_MA,COUNT(STRUCKID) nClean
   FROM TCheckTruck  chktrk  LEFT JOIN TCheckTruckITEM ichktrk ON chktrk.SCHECKID=ichktrk.SCHECKID
   WHERE 1=1 AND cClean='0' AND NVL(ichktrk.COTHER,'0')!='1'
   GROUP BY chktrk.SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,chktrk.CMA
) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE CACTIVE='Y'
) trck ON  CTRK.STRUCKID=trck.STRUCKID
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
LEFT JOIN 
(
  SELECT tconf.NCONFIRMID ,tconf.SCONTRACTID  ,tconf.ddate,tconf.cconfirm tconf_cconfirm
  ,tconflst.nlistno ,tconflst.cconfirm tconflst_cconfirm ,tconflst.struckid ,tconflst.sheadid ,tconflst.sheadregisterno ,tconflst.strailerid ,tconflst.strailerregisterno ,tconflst.cstanby ,tconflst.cpassed
 FROM ttruckconfirm tconf 
  LEFT JOIN ttruckconfirmlist tconflst ON tconf.nconfirmid=tconflst.nconfirmid
  where tconf.scontractid='{0}' AND tconf.DDATE=TO_DATE('{2}','DD/MM/YYYY')
) conf ON CONT.SCONTRACTID=conf.SCONTRACTID and ctrk.struckid=conf.struckid
WHERE 1=1 AND TRCK.CACTIVE='Y' AND  NVL(CTRK.CSTANDBY,'N')='N'
AND cont.SCONTRACTID='{0}'  
GROUP BY CTRK.STRUCKID,TRCK.SHEADREGISTERNO,CTRK.STRAILERID,TRCK.STRAILERREGISTERNO,NDAY_MA,CONF.cpassed,CONF.TCONFLST_CCONFIRM,TRCK.SCARTYPENAME,NVL(CTRK.CSTANDBY,'N'),CONT.SCONTRACTID
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '2' ELSE (CASE WHEN (CMA)='1' THEN '1' ELSE '0' END) END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO "
                    , "" + data[0], "" + data[1], Convert.ToDateTime("" + data[1]).ToString("dd/MM/yyyy", new CultureInfo("en-US")));
                DataTable _dtTruck = CommonFunction.Get_Data(connection, _Query);
                xgvwCar.DataSource = _dtTruck;
                xgvwCar.DataBind();
                */
                #endregion
                break;
            case "CUSTOMCALLBACK":
                #region CUSTOMCALLBACK
                //ถ้าไม่มีกริดรถ
                if (xgvwCar == null) break;
                //ถ้ามีการเลือกซ้ำ
                if (IsSelectLongLenght(xgvwCar))
                {

                    visibleindex = int.Parse(e.Args.GetValue(0).ToString());//xgvw.FindVisibleIndexByKeyValue(e.Args.GetValue(0));

                    BackUpData(visibleindex, xgvwCar);
                    xgvw.CancelEdit();
                }
                else
                {//ดักไม่ให้ไปEvent อื่นเพื่อไม่ไห้ ทำการทำงานซ้ำซ้อน เมื่อวาลิเดท ทำงาน
                    return;
                }
                #endregion
                break;
        }
    }
    protected void xgvw_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":

                if (("" + e.GetValue("CCONFIRM")).Equals("0") && (Convert.ToDateTime("" + e.GetValue("DEXPIRE")) < DateTime.Now))
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
                }
                else if (("" + e.GetValue("CCONFIRM")).Equals("1"))
                { //CEECF5
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#CEECF5");
                }
                break;
        }
    }
    protected void xgvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ยืนยันแล้ว")
        {
            ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;
            int NCONFIRM = 0;
            if (Session["ss_data"] != null)
            {
                NCONFIRM = ((DataTable)Session["ss_data"]).Select("CSEND='1'  AND DDATE='" + e.GetValue("DDATE") + "' AND SCONTRACTID='" + e.GetValue("SCONTRACTID") + "' ").Length;
                txtnTruckConfirm.Value = (NCONFIRM <= 0) ? "" + e.GetValue("NCONFIRM") : "" + NCONFIRM;
                txtnTruckConfirm.Attributes.Add("title", NCONFIRM + "/" + e.GetValue("NONHAND"));
            }
            else
            {
                txtnTruckConfirm.Value = "" + e.GetValue("NCONFIRM");
            }
        }
    }
    //GridViewParent
    void xgvwCar_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "") e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void xgvwCar_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxGridView xgvwCar = sender as ASPxGridView;
        bool IsExpire = (DateTime.Now > Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DEXPIRE").ToString())) ? true : false;
        bool IsNotEdit = (DateTime.Now > Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DEXPIRE").ToString())/*.AddDays(-1)*/) ? true : false;
        if (e.DataColumn.Caption.Equals("ลำดับที่"))
        {
            e.Cell.Text = string.Format("{0}.", e.VisibleIndex + 1);
        }
        else if (e.DataColumn.Caption == "#")
        {
            #region Set&event column #

            int VisibleIndex = e.VisibleIndex;
            ASPxButton imbconfirm = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbconfirm") as ASPxButton;
            ASPxButton imbcancel = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "imbcancel") as ASPxButton;
            ASPxTextBox txtconfirm = xgvwCar.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtconfirm") as ASPxTextBox;
            if (IsExpire || IsNotEdit)
            {//ถ้ายืนยันรถ+เกินเวลาที่กำหนด แล้ว
                #region display mode
                imbconfirm.ClientEnabled = false;
                imbcancel.ClientEnabled = false;
                ASPxButton btnsubmit = xgvwCar.FindFooterCellTemplateControl(e.DataColumn, "btnsubmit") as ASPxButton;
                btnsubmit.ClientVisible = false;
                //btnsubmit
                #endregion
            }
            else
            {
                #region comfirm mode
                if (e.GetValue("CMA").ToString().Equals("2"))
                {//ถ้าห้ามวิ่ง
                    imbconfirm.ClientEnabled = false;
                    imbcancel.ClientEnabled = false;
                    txtconfirm.Value = "0";
                }
                else if ((e.GetValue("CMA").ToString().Equals("1") && !e.GetValue("DFINAL_MA").ToString().Equals("")))
                {//ถ้าต้องแก้ไขภายใน nวัน
                    DateTime DFINAL_MA = Convert.ToDateTime(e.GetValue("DFINAL_MA").ToString());
                    if (DFINAL_MA < DateTime.Now)
                    {//ถ้าวันสิ้นสุดการแก้ไขเลยระยะเวลาๆปแล้ว
                        imbconfirm.ClientEnabled = false;
                        imbcancel.ClientEnabled = false;
                        txtconfirm.Value = "0";
                    }
                    else
                    {
                        imbconfirm.ClientEnabled = !true;
                        imbcancel.ClientEnabled = true;
                        txtconfirm.Value = "1";
                        #region AddEvent
                        {
                            ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(xgvw.EditingRowVisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;

                            string txtnTruckConfirmID = txtnTruckConfirm.ClientID + "_I";
                            string sPlus = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) -1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "',''); ";
                            string sDiff = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) +1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "','update');";
                            //config ClientInstanceName
                            txtconfirm.ClientInstanceName = txtconfirm.ClientInstanceName + "_" + VisibleIndex;
                            imbconfirm.ClientInstanceName = imbconfirm.ClientInstanceName + "_" + VisibleIndex;
                            imbcancel.ClientInstanceName = imbcancel.ClientInstanceName + "_" + VisibleIndex;

                            //Add Event
                            imbcancel.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + imbconfirm.ClientInstanceName + ".SetEnabled(true); " + sPlus + " }";
                            imbconfirm.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true); " + sDiff + " }";

                            bool IsEnabled = ("" + txtconfirm.Value == "1") ? true : false;
                            //BindingData
                            if (Session["ss_data"] != null)
                            {
                                DataTable dtss_data = ((DataTable)Session["ss_data"]);
                                DataRow[] drSect = dtss_data.Select("SCONTRACTID='" + e.GetValue("SCONTRACTID").ToString() + "' AND DDATE='" + e.GetValue("DDATE").ToString() + "' AND STRUCKID='" + e.GetValue("STRUCKID").ToString() + "'");
                                if (drSect.Length > 0)
                                {
                                    txtconfirm.Value = ("" + drSect[0]["CSEND"] == "0") ? "0" : "1";
                                    IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                                    //imbconfirm.ClientEnabled = !IsEnabled;
                                    //imbcancel.ClientEnabled = IsEnabled;
                                }

                            }
                            imbconfirm.ClientEnabled = !IsEnabled;
                            imbcancel.ClientEnabled = IsEnabled;


                        }
                        #endregion
                    }
                }
                else
                {
                    ASPxTextBox txtnTruckConfirm = xgvw.FindRowCellTemplateControl(xgvw.EditingRowVisibleIndex, (GridViewDataColumn)xgvw.Columns["ยืนยันแล้ว"], "txtnTruckConfirm") as ASPxTextBox;

                    string txtnTruckConfirmID = txtnTruckConfirm.ClientID + "_I";
                    string sPlus = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) -1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "',''); ";
                    string sDiff = "document.getElementById('" + txtnTruckConfirmID + "').value=parseInt(document.getElementById('" + txtnTruckConfirmID + "').value) +1; BackupConfirmTruckID('" + txtClientClickID.ClientID + "','," + e.GetValue("STRUCKID").ToString() + "','update');";
                    //config ClientInstanceName
                    txtconfirm.ClientInstanceName = txtconfirm.ClientInstanceName + "_" + VisibleIndex;
                    imbconfirm.ClientInstanceName = imbconfirm.ClientInstanceName + "_" + VisibleIndex;
                    imbcancel.ClientInstanceName = imbcancel.ClientInstanceName + "_" + VisibleIndex;

                    //Add Event
                    imbcancel.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('0');  s.SetEnabled(false); " + imbconfirm.ClientInstanceName + ".SetEnabled(true); " + sPlus + " }";
                    imbconfirm.ClientSideEvents.Click = "function (s, e) { " + txtconfirm.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true); " + sDiff + " }";

                    bool IsEnabled = ("" + txtconfirm.Value == "1") ? true : false;
                    //BindingData
                    if (Session["ss_data"] != null)
                    {
                        DataTable dtss_data = ((DataTable)Session["ss_data"]);
                        DataRow[] drSect = dtss_data.Select("SCONTRACTID='" + e.GetValue("SCONTRACTID").ToString() + "' AND DDATE='" + e.GetValue("DDATE").ToString() + "' AND STRUCKID='" + e.GetValue("STRUCKID").ToString() + "'");
                        if (drSect.Length > 0)
                        {
                            txtconfirm.Value = ("" + drSect[0]["CSEND"] == "0") ? "0" : "1";
                            IsEnabled = ("" + txtconfirm.Value.ToString() == "1") ? true : false;
                            //imbconfirm.ClientEnabled = !IsEnabled;
                            //imbcancel.ClientEnabled = IsEnabled;
                        }

                    }
                    imbconfirm.ClientEnabled = !IsEnabled;
                    imbcancel.ClientEnabled = IsEnabled;


                }
                #endregion
            }
            #endregion
        }
        else if (e.DataColumn.FieldName == "SHEADREGISTERNO" && e.DataColumn.Caption == "ทะเบียนรถ(หัว) ")
        {
            #region Set&Event Combo
            ASPxComboBox cmbSHEADREGISTERNO = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "cmbSHEADREGISTERNO") as ASPxComboBox;
            ASPxComboBox cmbSTRAILERREGISTERNO = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, (GridViewDataColumn)xgvwCar.Columns["ทะเบียนรถ(ท้าย) "], "cmbSTRAILERREGISTERNO") as ASPxComboBox;
            //config ClientInstanceName
            cmbSHEADREGISTERNO.ClientInstanceName = cmbSHEADREGISTERNO.ClientInstanceName + "_" + e.VisibleIndex;
            cmbSTRAILERREGISTERNO.ClientInstanceName = cmbSTRAILERREGISTERNO.ClientInstanceName + "_" + e.VisibleIndex;
            ////Add Event
            //cmbSHEADREGISTERNO.ClientSideEvents.SelectedIndexChanged = "function (s, e) { " + cmbSTRAILERREGISTERNO.ClientInstanceName + ".SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')); }";


            bool IsExpire_MA = false;
            if ((e.GetValue("CMA").ToString().Equals("1") && !e.GetValue("DFINAL_MA").ToString().Equals("")))
            {
                IsExpire_MA = (Convert.ToDateTime(e.GetValue("DFINAL_MA").ToString()) < DateTime.Now) ? true : false;
            }

            if (!e.GetValue("CMA").ToString().Equals("2") && !IsExpire_MA)
            {
                cmbSHEADREGISTERNO.Value = cmbSHEADREGISTERNO.Value == null ? "" + xgvwCar.GetRowValues(e.VisibleIndex, "STRUCKID") : "" + cmbSHEADREGISTERNO.Value;
                cmbSTRAILERREGISTERNO.Value = cmbSTRAILERREGISTERNO.Value == null ? "" + xgvwCar.GetRowValues(e.VisibleIndex, "STRAILERID") : "" + cmbSTRAILERREGISTERNO.Value;
                //xgvwCar.GetRowValues(e.VisibleIndex, "STRAILERID") + "";
            }
            #endregion
        }
        else if (e.DataColumn.Caption == "ใบตรวจสภาพรถ")
        {
            if (/*xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "CCONFIRM").ToString().Equals("1") ||*/ IsExpire)
            {
                ASPxButton imbIssue = xgvwCar.FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "imbIssue") as ASPxButton;
                imbIssue.ClientEnabled = false;
            }
        }
    }
    protected void xgvwCar_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        //BackUpData(xgvw.EditingRowVisibleIndex, ((ASPxGridView)sender));
        int idxMaster = xgvw.EditingRowVisibleIndex;
        int idxParent = 0;
        if (e.Args.Length > 0) idxParent = (int.TryParse("" + e.Args[0], out defInt)) ? int.Parse("" + e.Args[0]) : 0;
        switch (e.CallbackName.ToUpper())
        {
            case "HIDEALLDETAIL":
                BindTruckWithDataInGridEditing((ASPxGridView)sender, "HIDEALLDETAIL");
                break;
            case "HIDEDETAILROW":

                break;
            case "SHOWDETAILROW":
                #region SHOWDETAILROW
                DataView dvCheckLists = (DataView)sdsCheckLists.Select(DataSourceSelectArguments.Empty);
                DataTable dtCheckLists = dvCheckLists.ToTable();
                dynamic xgvwData = xgvw.GetRowValues(idxMaster, "SCONTRACTID", "COIL", "CGAS", "DDATE");
                dynamic xgvwCarData = ((ASPxGridView)sender).GetRowValues(idxParent, "SCONTRACTID", "COIL", "CGAS", "STRUCKID", "SHEADREGISTERNO", "STRAILERID", "STRAILERREGISTERNO");//SCONTRACTID^STRUCKID^SHEADREGISTERNO^STRAILERID^STRAILERREGISTERNO
                string scondition = ("" + xgvwData[1] == "1" ? " AND COIL='1'" : "") + ("" + xgvwData[2] == "1" ? " AND CGAS='1'" : "");
                Session["IssueData"] = null;
                Session["IssueData"] = "" + e.Args[0] + "$" + xgvwData[0] + "^" + xgvwCarData[0] + "^" + xgvwCarData[3] + "^" + xgvwCarData[4] + "^" + xgvwCarData[5] + "^" + xgvwCarData[6] + "#";
                Literal ltrTruckData = ((ASPxGridView)sender).FindDetailRowTemplateControl(idxParent, "ltrTruckData") as Literal;
                Literal ltrHiddenIndex = ((ASPxGridView)sender).FindDetailRowTemplateControl(idxParent, "ltrHiddenIndex") as Literal;

                ltrTruckData.Text = "" + xgvwCarData[0] + "^" + xgvwCarData[3] + "^" + xgvwCarData[4] + "^" + xgvwCarData[5] + "^" + xgvwCarData[6];
                ltrHiddenIndex.Text = "" + e.Args[0];
                ASPxPageControl gvwGroupCheckLists = ((ASPxGridView)sender).FindDetailRowTemplateControl(int.Parse("" + e.Args[0]), "pageControl") as ASPxPageControl;
                ASPxGridView gvwGroupCheckList = gvwGroupCheckLists.FindControl("gvwGroupCheckList") as ASPxGridView;
                DataTable dtGroupCheckLists = CommonFunction.GroupDatable("grpdtCheckList", dtCheckLists, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE", "GOCL_CACTIVE='1'" + scondition, "SGROUPID,SGROUPNAME,COIL,CGAS,GOCL_CACTIVE");
                //Session["ss_SCHECKID"] = null;
                gvwGroupCheckList.DataSource = dtGroupCheckLists;
                gvwGroupCheckList.DataBind();

                //Tab2
                #region Other Remark
                DataTable dtOtherRemark = CommonFunction.Get_Data(connection, @"SELECT ichktrck.* 
 FROm TCHECKTRUCK chktrck
 LEFT JOIN TCHECKTRUCKITEM ichktrck ON chktrck.SCHECKID=ichktrck.SCHECKID
WHERE 1=1 AND SCONTRACTID='" + xgvwCarData[0] + "' AND STRUCKID='" + xgvwCarData[3] + "' AND TO_DATE(DCHECKLIST,'DD/MM/YYYY')=TO_DATE('" + Convert.ToDateTime("" + xgvwData[3]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') AND ichktrck.COTHER='1'");
                if (dtOtherRemark.Rows.Count > 0)
                {
                    string sOtherRemark = "", sRemark = "";
                    ASPxCheckBox cbxOtherIssue = gvwGroupCheckLists.FindControl("cbxOtherIssue") as ASPxCheckBox;
                    ASPxMemo txtOtherIssue = gvwGroupCheckLists.FindControl("txtOtherIssue") as ASPxMemo;
                    ASPxMemo txtDetailIssue = gvwGroupCheckLists.FindControl("txtDetailIssue") as ASPxMemo;
                    ASPxTextBox txtNMA = gvwGroupCheckLists.FindControl("txtNMA") as ASPxTextBox;
                    foreach (DataRow drOtherRemark in dtOtherRemark.Rows)
                    {
                        sOtherRemark += "," + drOtherRemark["sOtherRemark"];
                        sRemark += "," + drOtherRemark["sRemark"];
                        break;
                    }//(Eval("CBAN")+""=="1")?"2":((Eval("NDAY_MA")+""!="")?"1":"0")
                    cbxOtherIssue.Checked = (true) ? true : false;
                    if (cbxOtherIssue.Checked)
                    {
                        txtOtherIssue.Text = sOtherRemark.Length > 0 ? sOtherRemark.Remove(0, 1) : "";
                        txtDetailIssue.Text = sRemark.Length > 0 ? sRemark.Remove(0, 1) : "";

                    }

                }
                dtOtherRemark.Dispose();

                #endregion
                #region AttachmentFile
                DataTable dtatt = CommonFunction.Get_Data(connection, @"SELECT * 
FROM TCHECKTRUCKATTACHMENT 
WHERE 1=1 AND SCONTRACTID='" + xgvwCarData[0] + @"'  AND NVL(SATTACHTYPEID,'1')='1'
AND SCHECKID IN(
    SELECT TCHECKTRUCKITEM.SCHECKID
    FROM TCHECKTRUCKITEM  
    WHERE 1=1 AND NVL(CEDITED,'0')='0' AND DEND_LIST>= SYSDATE
    and TCHECKTRUCKITEM.SCHECKID IN(SELECT distinct SCHECKID FROM TCHECKTRUCK WHERE 1=1 AND STRUCKID='" + xgvwCarData[3] + @"')
)  ");
                string satt = "<table>", attment = "", sfile = "<a href='{0}{1}?dx={2}' target='{3}'>{4}</a>";
                foreach (DataRow dratt in dtatt.Rows)
                {
                    string LinkFile = string.Format(sfile, ("" + dratt["SPATH"]).Replace("~/", ""), "" + dratt["SSYSFILE"], DateTime.Now.ToString("hhmmss"), "_blank", "" + dratt["SFILE"]);

                    attment += string.Format("<tr><td width='60%'>{0}</td><td width='20%'>{1}</td><td width='18%'>{2}</td></tr>", LinkFile
                        , ""
                        , "");

                }
                attment = attment.Length > 0 ? attment : "<tr><td width='60%'></td><td width='20%'></td><td width='18%'></td></tr>";
                satt += attment + "</table>";
                ((HtmlGenericControl)gvwGroupCheckLists.TabPages[1].FindControl("rplAttachmented").FindControl("pnctTab2").FindControl("AttachmentedListFiles")).InnerHtml = "" + satt;

                dtatt.Dispose();
                #endregion

                dtCheckLists.Dispose();
                dtGroupCheckLists.Dispose();
                #endregion
                break;
            case "CUSTOMCALLBACK":
                #region CUSTOMCALLBACK
                string[] ArrayArgs = e.Args[0].Split('$');
                if (ArrayArgs.Length <= 0) return;
                if ("" + ArrayArgs[1] == "1")
                {
                    #region TAB1
                    Literal ltrTruckData_CUSTOMCALLBACK = ((ASPxGridView)sender).FindDetailRowTemplateControl(idxParent, "ltrTruckData") as Literal;
                    Literal ltrHiddenIndex_CUSTOMCALLBACK = ((ASPxGridView)sender).FindDetailRowTemplateControl(idxParent, "ltrHiddenIndex") as Literal;
                    ASPxPageControl gvwGroupCheckLists_CUSTOMCALLBACK = ((ASPxGridView)sender).FindDetailRowTemplateControl(int.Parse("" + ArrayArgs[0]), "pageControl") as ASPxPageControl;
                    ASPxGridView gvwGroupCheckList_CUSTOMCALLBACK = gvwGroupCheckLists_CUSTOMCALLBACK.FindControl("gvwGroupCheckList") as ASPxGridView;
                    TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
                    TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(Page, connection);
                    TREDUCEPOINT reduce = new TREDUCEPOINT(Page, connection);

                    double NPOINT = 0;
                    //string[] strDataTruck = (ltrTruckData_CUSTOMCALLBACK.Text.Length > 0) ? ltrTruckData_CUSTOMCALLBACK.Text.Split('^') : ("").Split('^');
                    string sData = "", SCHECKID = "";
                    for (int idxGroupGrid = 0; idxGroupGrid < gvwGroupCheckList_CUSTOMCALLBACK.VisibleRowCount; idxGroupGrid++)
                    {
                        ASPxGridView gvwItemCheckList_CUSTOMCALLBACK = gvwGroupCheckList_CUSTOMCALLBACK.FindRowCellTemplateControl(idxGroupGrid, (GridViewDataColumn)gvwGroupCheckList_CUSTOMCALLBACK.Columns[0], "gvwItemCheckList") as ASPxGridView;
                        for (int idxItemGrid = 0; idxItemGrid < gvwItemCheckList_CUSTOMCALLBACK.VisibleRowCount; idxItemGrid++)
                        {
                            ASPxCheckBox cbxSCHECKLISTID = gvwItemCheckList_CUSTOMCALLBACK.FindRowCellTemplateControl(idxItemGrid, (GridViewDataColumn)gvwItemCheckList_CUSTOMCALLBACK.Columns[0], "cbxSCHECKLISTID") as ASPxCheckBox;
                            ASPxTextBox txtIsChecked = gvwItemCheckList_CUSTOMCALLBACK.FindRowCellTemplateControl(idxItemGrid, (GridViewDataColumn)gvwItemCheckList_CUSTOMCALLBACK.Columns[0], "txtIsChecked") as ASPxTextBox;
                            if (cbxSCHECKLISTID.Checked && txtIsChecked.Text.Equals("0"))
                            {
                                dynamic datagriditem = gvwItemCheckList_CUSTOMCALLBACK.GetRowValues(idxItemGrid, "SVERSIONLIST", "SVERSION", "STOPICID", "SCHECKLISTID", "NLIST", "NDAY_MA", "CCUT", "CBAN", "CL_CACTIVE", "NPOINT", "STYPECHECKLISTID");
                                string gvwCarIndex = "" + ("" + Session["IssueData"]).Split('$')[0];
                                string[] TruckData = ("" + Session["IssueData"]).Split('$')[1].Split('#')[0].Split('^');
                                ///1.Call StoreProcedure for insert into DB
                                #region TCHECKTRUCK
                                ///TCHECKTRUCK
                                ctrck.SCHECKID = SCHECKID != "" ? SCHECKID : "";//send blank for gen new id
                                ctrck.NPLANID = "";
                                ctrck.STERMINALID = "";
                                ctrck.SCONTRACTID = "" + TruckData[0];
                                ctrck.CGROUPCHECK = "4";//fix หน้านี้เป็นการทำงานของ ผปก
                                ctrck.CCLEAN = "0";
                                ctrck.STRUCKID = "" + TruckData[2];
                                ctrck.SHEADERREGISTERNO = "" + TruckData[3];
                                ctrck.STRAILERREGISTERNO = "" + TruckData[5];
                                ctrck.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                ctrck.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                ctrck.SCREATE = "" + Session["UserID"];
                                ctrck.SUPDATE = "" + Session["UserID"];

                                ctrck.Insert();
                                SCHECKID = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.
                                #endregion
                                #region///TCHECKTRUCKITEM
                                ctrckItem.SCHECKID = SCHECKID;
                                ctrckItem.SCHECKLISTID = "" + datagriditem[3];
                                ctrckItem.SVERSIONLIST = "" + datagriditem[0];
                                ctrckItem.NDAY_MA = (("" + datagriditem[5] == "") ? "0" : "" + datagriditem[5]);
                                ctrckItem.NPOINT = ("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0";
                                ctrckItem.CMA = (("" + datagriditem[7] == "1") ? "2" : (("" + datagriditem[6] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                ctrckItem.COTHER = "0";
                                ctrckItem.SOTHERREMARK = "";
                                ctrckItem.SCREATE = "" + Session["UserID"];
                                ctrckItem.SUPDATE = "" + Session["UserID"];
                                ctrckItem.STYPECHECKLISTID = "" + datagriditem[10];
                                ctrckItem.Insert();
                                #endregion
                                #region บันทึกห้ามวิ่ง+ตัดแต้ม
                                sData += "^$SCHECKID:" + "" + "$CMA:" + ctrckItem.CMA + "$SCHECKLISTID:" + datagriditem[3] + "$SVERSIONLIST:" + datagriditem[0] + "$NPOINT:" + ctrckItem.NPOINT + "$SCREATE:" + ctrckItem.SCREATE + "$CBAN:" + datagriditem[7] + "$CCUT:" + datagriditem[6] + "$NDAY_MA:" + ctrckItem.NDAY_MA;//+"$:"
                                string[] sArrayData = sData.Split('^');
                                if ("" + datagriditem[7] == "1")
                                {//ถ้าเป้นเคส ห้ามวิ่ง 
                                    #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                                    using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                    {
                                        if (con.State == ConnectionState.Closed) con.Open();
                                        OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1' WHERE STRUCKID=:S_TRUCKID", con);
                                        ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = ctrck.STRUCKID;
                                        ora_cmd.ExecuteNonQuery();
                                    }
                                    #endregion
                                }
                                if ("" + datagriditem[6] == "1")
                                {//ถ้าตัดคะแนนทันที
                                    //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                                    //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                                    ReducePoint("01", "011", "" + cbxSCHECKLISTID.Text, "" + datagriditem[3], "" + datagriditem[2], "" + datagriditem[0], "" + (("" + datagriditem[9] != "") ? ("" + datagriditem[9]) : "0")
                                        , SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");
                                }
                                #endregion
                                NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;
                                ///2.List Data for future

                            }//End Checklist Item
                        }//End loop grid item
                    }//EndLoop grid group checklist
                    sData = Session["IssueData"] + sData;
                    #region Update คะแนนรวมที่ตัดที่ รายการหลัก
                    ctrck.SCHECKID = SCHECKID;
                    ctrck.UpdateMA();
                    #endregion
                    #endregion
                }
                else if ("" + ArrayArgs[1] == "2")
                {
                    #region TAB2

                    string ss_IssueData = Session["IssueData"] + "";
                    string[] ArrayIssueData = ss_IssueData.Split('^');
                    if (ArrayIssueData.Length <= 0) return;
                    int idxEditMaster = xgvw.EditingRowVisibleIndex;
                    dynamic xgvwDataMaster = xgvw.GetRowValues(idxEditMaster, "SCONTRACTID", "COIL", "CGAS", "DDATE");
                    TCHECKTRUCK ctrck = new TCHECKTRUCK(Page, connection);
                    TCHECKTRUCKITEM chkItem = new TCHECKTRUCKITEM(this.Page, connection);
                    ASPxPageControl PageControlTab2 = ((ASPxGridView)sender).FindDetailRowTemplateControl(int.Parse("" + ArrayArgs[0]), "pageControl") as ASPxPageControl;
                    ASPxCheckBox cbxOtherIssue = PageControlTab2.FindControl("cbxOtherIssue") as ASPxCheckBox;
                    ASPxMemo txtOtherIssue = PageControlTab2.FindControl("txtOtherIssue") as ASPxMemo;
                    ASPxTextBox txtNMA = PageControlTab2.FindControl("txtNMA") as ASPxTextBox;
                    ASPxMemo txtDetailIssue = PageControlTab2.FindControl("txtDetailIssue") as ASPxMemo;
                    string SCHECKID_TAB2 = "";

                    SCHECKID_TAB2 = CommonFunction.Get_Value(connection, @"SELECT TCHECKTRUCK.SCHECKID FROM TCHECKTRUCK  WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection("" + xgvwDataMaster[0]) + @"' 
AND STRUCKID='" + CommonFunction.ReplaceInjection("" + ArrayIssueData[2]) + @"'  AND TO_DATE(DCHECK,'DD/MM/YYYY')=TO_DATE('" + CommonFunction.ReplaceInjection("" + Convert.ToDateTime("" + xgvwDataMaster[3]).ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') ");
                    if (SCHECKID_TAB2 == "")
                    {
                        #region TCHECKTRUCK
                        ///TCHECKTRUCK
                        ctrck.SCHECKID = SCHECKID_TAB2 != "" ? SCHECKID_TAB2 : "";//send blank for gen new id
                        ctrck.NPLANID = "";
                        ctrck.STERMINALID = "";
                        ctrck.SCONTRACTID = "" + xgvwDataMaster[0];
                        ctrck.CGROUPCHECK = "4";//fix หน้านี้เป็นการทำงานของ ผปก
                        ctrck.CCLEAN = "0";
                        ctrck.STRUCKID = "" + ArrayIssueData[2];
                        ctrck.SHEADERREGISTERNO = "" + ArrayIssueData[3];
                        ctrck.STRAILERREGISTERNO = "" + ArrayIssueData[5].Replace("#", "");
                        ctrck.CMA = "0";//2 hold ,1 mainternent ,0 ปกติ
                        ctrck.NDAY_MA = "0";
                        ctrck.SCREATE = "" + Session["UserID"];
                        ctrck.SUPDATE = "" + Session["UserID"];

                        ctrck.Insert();
                        SCHECKID_TAB2 = ctrck.SCHECKID;//BackUp SCHECKID for Child rows.

                        #endregion
                    }
                    #region TCHECKTRUCKITEM
                    chkItem.SCHECKID = SCHECKID_TAB2;
                    chkItem.SCHECKLISTID = "0";
                    chkItem.CMA = "0";
                    chkItem.NPOINT = "0";
                    chkItem.SCHECKLISTID = "0";
                    chkItem.SVERSIONLIST = "0";
                    chkItem.NDAY_MA = txtNMA.Text == "" ? "0" : txtNMA.Text;
                    chkItem.COTHER = cbxOtherIssue.Checked ? "1" : "0";
                    chkItem.SOTHERREMARK = cbxOtherIssue.Checked ? txtOtherIssue.Text : "";
                    chkItem.SREMARK = txtDetailIssue.Text;
                    chkItem.SCREATE = "" + Session["UserID"];
                    chkItem.SUPDATE = "" + Session["UserID"];
                    chkItem.InsertTab2(xgvwDataMaster[0] + "", ArrayIssueData[2] + "", Convert.ToDateTime("" + xgvwDataMaster[3]).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "");
                    #endregion
                    #region Attchment File
                    if (Session["DataAttechment"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();
                            DataTable dtDataAttechment = ((DataTable)Session["DataAttechment"]);
                            foreach (DataRow drDataAttechment in dtDataAttechment.Select("cRowFlag='Page'"))
                            {
                                OracleCommand ora_cmd = new OracleCommand(@"INSERT INTO TCHECKTRUCKATTACHMENT ( SCHECKID, NATTACHMENT, STRUCKID,SCONTRACTID, SATTACHTYPEID, SPATH,SFILE, SSYSFILE, DCREATE,SCREATE, DUPDATE, SUPDATE) 
 VALUES ( :S_CHECKID,FC_GENID_TCHECKTRUCKATTACHMENT(:S_CONTRACTID, :S_TRUCKID ,SYSDATE),:S_TRUCKID, :S_CONTRACTID,:S_ATTACHTYPEID,:S_PATH, :S_FILE,:S_SYSFILE, SYSDATE, :S_CREATE, SYSDATE,:S_UPDATE ) ", con);
                                ora_cmd.Parameters.Add(":S_CHECKID", OracleType.VarChar).Value = SCHECKID_TAB2;
                                //ora_cmd.Parameters.Add(":N_ATTACHMENT", OracleType.VarChar).Value = "" + drDataAttechment["NATTACHMENT"];
                                ora_cmd.Parameters.Add(":S_TRUCKID", OracleType.VarChar).Value = "" + drDataAttechment["STRUCKID"];
                                ora_cmd.Parameters.Add(":S_CONTRACTID", OracleType.VarChar).Value = "" + drDataAttechment["SCONTRACTID"];
                                ora_cmd.Parameters.Add(":S_ATTACHTYPEID", OracleType.VarChar).Value = "" + drDataAttechment["SATTACHTYPEID"];
                                ora_cmd.Parameters.Add(":S_PATH", OracleType.VarChar).Value = "" + drDataAttechment["SPATH"];
                                ora_cmd.Parameters.Add(":S_FILE", OracleType.VarChar).Value = "" + drDataAttechment["SFILE"];
                                ora_cmd.Parameters.Add(":S_SYSFILE", OracleType.VarChar).Value = "" + drDataAttechment["SSYSFILE"];
                                // ora_cmd.Parameters.Add(":D_CREATE", OracleType.VarChar).Value = "" + drDataAttechment["NATTACHMENT"];
                                ora_cmd.Parameters.Add(":S_CREATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                //ora_cmd.Parameters.Add(":D_UPDATE", OracleType.VarChar).Value = "" + drDataAttechment["NATTACHMENT"];
                                ora_cmd.Parameters.Add(":S_UPDATE", OracleType.VarChar).Value = "" + Session["SVDID"];
                                ora_cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    #endregion
                    ((ASPxGridView)sender).DetailRows.CollapseAllRows();
                    #endregion
                }
                #endregion
                break;
        }
    }
    //GridViewChild
    protected void gvwGroupCheckList_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        dynamic xgvwData = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "SGROUPID", "GOCL_CACTIVE");
        ASPxGridView gvwItemCheckList = ((ASPxGridView)sender).FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "gvwItemCheckList") as ASPxGridView;
        if (gvwItemCheckList != null)
        {
            DataView dvCheckLists = (DataView)sdsCheckLists.Select(DataSourceSelectArguments.Empty);
            DataRow[] drCheckLists = dvCheckLists.ToTable().Select("SGROUPID='" + xgvwData[0] + "'");
            DataTable dtCheckLists = drCheckLists.CopyToDataTable<DataRow>();

            gvwItemCheckList.DataSource = dtCheckLists;
            gvwItemCheckList.DataBind();
            if (Session["IssueData"] != null)
            {
                string[] ArrayIssueData = Session["IssueData"].ToString().Length > 0 ? Session["IssueData"].ToString().Split('$') : ("").Split('$');
                string[] ArrayTruckData = ArrayIssueData.Length > 0 ? ArrayIssueData[1].Split('^') : ("").Split('^');
                if (ArrayTruckData.Length > 0)
                    BindCheckListData(gvwItemCheckList, "" + ArrayTruckData[0], "" + ArrayTruckData[2]);
            }
            dvCheckLists.Dispose();
            dtCheckLists.Dispose();
        }
    }
    //Combobox 's GridViewChild
    protected void cmbSHEADREGISTERNO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SSTANDBY,CSTANDBY
,NVL(chktrck.nClean,0) CCLEAN,NVL(chktrck.CMA,0) CMA/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END SHOLD
,NVL(TRCK.CACCIDENT,'0') CACCIDENT,NVL(TRCK.CSTATUS,'0') CSTATUS 
,CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confHead
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confTRAIL
FROM ( 
         SELECT * FROM TCONTRACT WHERE 1=1 AND CACTIVE='Y' AND NVL(NTRUCK,0) !=0 AND SVENDORID=:SVDID 
 ) cont LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID 
 LEFT JOIN (
        SELECT SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA,COUNT(STRUCKID) nClean 
        FROM TCheckTruck 
        WHERE 1=1 AND cClean='0'
        GROUP BY SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA
 ) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID 
 LEFT JOIN (
         SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y'
 ) trck ON  CTRK.STRUCKID=trck.STRUCKID 
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,CONF_LST.STRAILERID ,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confHead ON CTRK.STRUCKID=confHead.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,NVL(CONF_LST.STRAILERID,'TRxxxxxxx') STRAILERID,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confTRAIL ON CTRK.STRAILERID=confTRAIL.STRAILERID
 WHERE 1=1 AND rownum>=:startIndex and rownum<=:endIndex 
AND NVL(chktrck.CMA,'0') !='2'
--AND TRCK.CACTIVE='Y' AND NVL(CTRK.CREJECT,'N')  ='Y'
AND TRCK.SHEADREGISTERNO LIKE :fillter 
GROUP BY CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,TRCK.STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END ,CSTANDBY
,NVL(chktrck.nClean,0) ,NVL(chktrck.CMA,0)/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END 
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END 
,NVL(TRCK.CACCIDENT,'0') ,NVL(TRCK.CSTATUS,'0'),CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END 
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO ";


        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("SVDID", TypeCode.String, "" + Session["SVDID"]);
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("SDATE", TypeCode.String, String.Format("{0}", Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DDATE") + "").ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }
    protected void cmbSHEADREGISTERNO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SSTANDBY,CSTANDBY
,NVL(chktrck.nClean,0) CCLEAN,NVL(chktrck.CMA,0) CMA/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END SHOLD
,NVL(TRCK.CACCIDENT,'0') CACCIDENT,NVL(TRCK.CSTATUS,'0') CSTATUS
,CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confHead
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confTRAIL
FROM ( 
         SELECT * FROM TCONTRACT WHERE 1=1 AND CACTIVE='Y' AND NVL(NTRUCK,0) !=0 AND SVENDORID=:SVDID
 ) cont LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID 
 LEFT JOIN (
        SELECT SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA,COUNT(STRUCKID) nClean 
        FROM TCheckTruck 
        WHERE 1=1 AND cClean='0'
        GROUP BY SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA
 ) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID 
 LEFT JOIN (
         SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y'
 ) trck ON  CTRK.STRUCKID=trck.STRUCKID 
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,CONF_LST.STRAILERID ,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confHead ON CTRK.STRUCKID=confHead.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,NVL(CONF_LST.STRAILERID,'TRxxxxxxx') STRAILERID,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confTRAIL ON CTRK.STRAILERID=confTRAIL.STRAILERID
 WHERE 1=1 
AND NVL(chktrck.CMA,'0') !='2'
--AND TRCK.CACTIVE='Y' AND NVL(CTRK.CREJECT,'N') ='Y' 
AND NVL(TRCK.SHEADREGISTERNO,'')  LIKE :SHEADREGISTERNO 
GROUP BY CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,TRCK.STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END ,CSTANDBY
,NVL(chktrck.nClean,0) ,NVL(chktrck.CMA,0)/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END 
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END 
,NVL(TRCK.CACCIDENT,'0') ,NVL(TRCK.CSTATUS,'0'),CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END 
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO  ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("SVDID", TypeCode.String, "" + Session["SVDID"]);
        sdsTruck.SelectParameters.Add("SHEADREGISTERNO", TypeCode.String, string.Format("%{0}%", e.Value));
        sdsTruck.SelectParameters.Add("SDATE", TypeCode.String, String.Format("{0}", Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DDATE") + "").ToString("dd/MM/yyyy", new CultureInfo("en-US"))));

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }
    protected void cmbSTRAILERREGISTERNO_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SSTANDBY,CSTANDBY
,NVL(chktrck.nClean,0) CCLEAN,NVL(chktrck.CMA,0) CMA/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END SHOLD
,NVL(TRCK.CACCIDENT,'0') CACCIDENT,NVL(TRCK.CSTATUS,'0') CSTATUS
,CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confHead
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confTRAIL
FROM ( 
         SELECT * FROM TCONTRACT WHERE 1=1 AND CACTIVE='Y' AND NVL(NTRUCK,0) !=0 AND SVENDORID=:SVDID 
 ) cont LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID 
 LEFT JOIN (
        SELECT SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA,COUNT(STRUCKID) nClean 
        FROM TCheckTruck 
        WHERE 1=1 AND cClean='0'
        GROUP BY SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA
 ) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID 
 LEFT JOIN (
         SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y'
 ) trck ON  CTRK.STRUCKID=trck.STRUCKID 
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,CONF_LST.STRAILERID ,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confHead ON CTRK.STRUCKID=confHead.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,NVL(CONF_LST.STRAILERID,'TRxxxxxxx') STRAILERID,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confTRAIL ON CTRK.STRAILERID=confTRAIL.STRAILERID
 WHERE 1=1 AND rownum>=:startIndex and rownum<=:endIndex 
AND NVL(chktrck.CMA,'0') !='2'
--AND TRCK.CACTIVE='Y' AND NVL(CTRK.CREJECT,'N')  ='Y'
AND NVL(TRCK.STRAILERREGISTERNO,'')  LIKE :fillter 
GROUP BY CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,TRCK.STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END ,CSTANDBY
,NVL(chktrck.nClean,0) ,NVL(chktrck.CMA,0)/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END 
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END 
,NVL(TRCK.CACCIDENT,'0') ,NVL(TRCK.CSTATUS,'0'),CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END 
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO   ";


        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("SVDID", TypeCode.String, "" + Session["SVDID"]);
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("SDATE", TypeCode.String, String.Format("{0}", Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DDATE") + "").ToString("dd/MM/yyyy", new CultureInfo("en-US"))));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }
    protected void cmbSTRAILERREGISTERNO_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,CASE WHEN NVL(CTRK.STRAILERID,'TRxxxxxxx') !='TRxxxxxxx' THEN (CASE WHEN NVL(TRCK.STRAILERREGISTERNO,'xx.nn-nnnn')!='xx.nn-nnnn' THEN TRCK.STRAILERREGISTERNO ELSE trcktail.SHEADREGISTERNO END ) ELSE '' END STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SSTANDBY,CSTANDBY
,NVL(chktrck.nClean,0) CCLEAN,NVL(chktrck.CMA,0) CMA/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA
,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END CHOLD
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END SHOLD
,NVL(TRCK.CACCIDENT,'0') CACCIDENT,NVL(TRCK.CSTATUS,'0') CSTATUS
,CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confHead
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END confTRAIL
FROM ( 
         SELECT * FROM TCONTRACT WHERE 1=1 AND CACTIVE='Y' AND NVL(NTRUCK,0) !=0 AND SVENDORID=:SVDID 
 ) cont LEFT JOIN TCONTRACT_TRUCK ctrk ON cont.SCONTRACTID= ctrk.SCONTRACTID 
 LEFT JOIN (
        SELECT SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA,COUNT(STRUCKID) nClean 
        FROM TCheckTruck 
        WHERE 1=1 AND cClean='0'
        GROUP BY SCONTRACTID,STRUCKID,SHEADERREGISTERNO ,STRAILERREGISTERNO ,CMA,DBEGIN_MA,DFINAL_MA,NDAY_MA
 ) chktrck ON  cont.SCONTRACTID= chktrck.SCONTRACTID AND chktrck.STRUCKID=ctrk.STRUCKID 
 LEFT JOIN (
         SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME 
        ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
        FROM TTRUCK 
        LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
        LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
        WHERE CACTIVE='Y'
 ) trck ON  CTRK.STRUCKID=trck.STRUCKID 
LEFT JOIN 
(
  SELECT TTRUCK.* ,TTRUCKTYPE.SCARTYPENAME ,CASE WHEN TVEHICLE.VEH_NO IS NULL THEN '0' ELSE '1' END MS_HOLD
  FROM TTRUCK LEFT JOIN TTRUCKTYPE ON TTRUCK.SCARTYPEID=TTRUCKTYPE.SCARTYPEID 
  LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TTRUCK.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','')
  WHERE 1=1 --ANDCACTIVE='Y'
) trcktail ON  CTRK.STRAILERID=trcktail.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,CONF_LST.STRAILERID ,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confHead ON CTRK.STRUCKID=confHead.STRUCKID
 LEFT JOIN (
    SELECt conf_lst.STRUCKID,conf_lst.SHEADID, CONF_LST.SHEADREGISTERNO ,NVL(CONF_LST.STRAILERID,'TRxxxxxxx') STRAILERID,conf_lst.strailerregisterno
    FROM TTRUCKCONFIRM conf
    LEFT JOIN TTRUCKCONFIRMLIST conf_lst ON CONF.NCONFIRMID=conf_lst.NCONFIRMID
    WHERE NVL(conf.CCONFIRM,'0')='1' AND  NVL(conf_LST.CCONFIRM,'0')='1'
    AND TO_DATE(DDATE,'DD/MM/YYYY')=TO_DATE(:SDATE,'DD/MM/YYYY')
 ) confTRAIL ON CTRK.STRAILERID=confTRAIL.STRAILERID
 WHERE 1=1 
AND NVL(chktrck.CMA,'0') !='2'
--AND TRCK.CACTIVE='Y' AND NVL(CTRK.CREJECT,'N')  ='Y'
AND  NVL(TRCK.STRAILERREGISTERNO,'')  LIKE :STRAILERREGISTERNO 
GROUP BY CONT.SCONTRACTID ,CONT.SCONTRACTNO ,CTRK.STRUCKID ,CTRK.STRAILERID,TRCK.SHEADREGISTERNO,TRCK.STRAILERREGISTERNO
,CASE WHEN NVL(CTRK.CSTANDBY,'N')='N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END ,CSTANDBY
,NVL(chktrck.nClean,0) ,NVL(chktrck.CMA,0)/*,DBEGIN_MA,DFINAL_MA*/,NDAY_MA,CASE WHEN NVL(TRCK.CHOLD,'0')='1' OR NVL(TRCK.MS_HOLD,'0') ='1' THEN '1' ELSE '0' END 
,CASE NVL(TRCK.CHOLD,'0')||NVL(TRCK.MS_HOLD,'0') 
WHEN  '10' THEN 'TMS' 
WHEN  '01' THEN 'SAP' 
WHEN  '11' THEN 'SAP,TMS' 
 ELSE '' END 
,NVL(TRCK.CACCIDENT,'0') ,NVL(TRCK.CSTATUS,'0'),CASE WHEN NVL(confHead.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END 
,CASE WHEN NVL(confTRAIL.STRUCKID,'N')='N' THEN 'N' ELSE 'Y' END,trcktail.STRAILERID ,trcktail.SHEADREGISTERNO ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("SVDID", TypeCode.String, "" + Session["SVDID"]);
        sdsTruck.SelectParameters.Add("STRAILERREGISTERNO", TypeCode.String, string.Format("%{0}%", "" + e.Value));
        sdsTruck.SelectParameters.Add("SDATE", TypeCode.String, String.Format("{0}", Convert.ToDateTime(xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "DDATE") + "").ToString("dd/MM/yyyy", new CultureInfo("en-US"))));

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();
    }
    //Upload
    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFiles(e.UploadedFile);
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    protected void pageControl_TabClick(object source, DevExpress.Web.ASPxTabControl.TabControlCancelEventArgs e)
    {
        ASPxTabControl TCT = source as ASPxTabControl;
        int tabIndex = e.Tab.VisibleIndex;
        if (tabIndex == 1)
        {
            string[] ArrayIssueData = Session["IssueData"] != null ? Session["IssueData"].ToString().Split('^') : ("").Split('^');
            if (ArrayIssueData.Length <= 0) { return; }
            else
            {
                string sQry = @"SELECT NVL(CHKTRCK.COTHER,'0') COTHER,CHKTRCK.SOTHERREMARK ,CHKTRCK.SREMARK,CHKTRCK.STRUCKID, ICHKTRCK.* 
,((DEND_LIST)-(DBEGIN_LIST))+1 NDATEDIFF 
FROM (
    SELECT * FROM TCHECKTRUCK WHERE 1=1 AND NVL(COK,'0')='0' AND STRUCKID='" + ArrayIssueData[3] + @"'
)CHKTRCK
LEFT JOIN (
    SELECT TCHECKTRUCKITEM.* 
    FROM TCHECKTRUCKITEM 
    WHERE 1=1 AND NVL(CEDITED,'0')='0'

)ICHKTRCK ON CHKTRCK.SCHECKID=ICHKTRCK.SCHECKID
WHERE 1=1 AND ICHKTRCK.SCHECKID IS NOT NULL
ORDER BY ICHKTRCK.CMA DESC,MIN(DBEGIN_LIST),MIN(DEND_LIST)
 ";
                DataTable dtMAData = CommonFunction.Get_Data(connection, sQry);
                if (dtMAData.Rows.Count > 0)
                {
                    ASPxTextBox txtNMA = TCT.Tabs[tabIndex].FindControl("txtNMA") as ASPxTextBox;
                    ASPxCheckBox cbxOtherIssue = TCT.Tabs[tabIndex].FindControl("cbxOtherIssue") as ASPxCheckBox;
                    ASPxCheckBox txtDetailIssue = TCT.Tabs[tabIndex].FindControl("txtDetailIssue") as ASPxCheckBox;
                    txtDetailIssue.Text = dtMAData.Rows[0]["SREMARK"] + "";
                    txtNMA.Text = dtMAData.Rows[0]["NDATEDIFF"] + "";
                    cbxOtherIssue.Checked = (dtMAData.Rows[0]["COTHER"] + "" == "1") ? true : false;
                    if (cbxOtherIssue.Checked)
                    {
                        ASPxTextBox txtOtherIssue = TCT.Tabs[tabIndex].FindControl("txtOtherIssue") as ASPxTextBox;
                        txtOtherIssue.Text = dtMAData.Rows[0]["SOTHERREMARK"] + "";
                    }
                    Session["ss_SCHECKID"] = dtMAData.Rows[0]["SCHECKID"] + "";
                }

            }
        }
        else
        {
            //0
        }

    }
    #endregion
    protected string T(object s)
    {
        string ss = (string)s;
        return "2";

        //function(s, e){   var msg ='';  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')!=''){ msg='\n ท่านไม่สามารถเลือกรถที่ติดสถานะ ห้ามวิ่ง จากระบบ '+s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')+'ได้!'; }  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('CONFHEAD')!='N'){ msg='\n ท่านไม่สามารถเลือกรถที่ ถุูกยืนยัน ไว้ในสัญญาอื่นแล้ว ได้!'; }  if(msg!=''){ alert('แจ้งเตือนจากระบบ'+msg);return false; } }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}//End Class