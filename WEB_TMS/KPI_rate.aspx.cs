﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;

public partial class KPI_rate : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<Tdata> lstData = new List<Tdata>();
    private static List<Tdata> lstData_KPI = new List<Tdata>();
    private static List<Tdata> lstData_KPI_Rate = new List<Tdata>();
    private static string SYEAR = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        gvwT1.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvwT1_HtmlRowPrepared);
        gvwT1.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvwT1_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            Session["SUM"] = "";
            Session["VENID"] = QueryStr_ReturnValue("ven");
            Session["CONID"] = QueryStr_ReturnValue("con");
            SYEAR = QueryStr_ReturnValue("year");
            ListData();
            //SetTotal();
        }
    }

    void gvwT1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        #region GetControl
        ASPxTextBox txtM1 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM1") as ASPxTextBox;
        ASPxTextBox txtM2 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM2") as ASPxTextBox;
        ASPxTextBox txtM3 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM3") as ASPxTextBox;
        ASPxTextBox txtM4 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM4") as ASPxTextBox;
        ASPxTextBox txtM5 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM5") as ASPxTextBox;
        ASPxTextBox txtM6 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM6") as ASPxTextBox;
        ASPxTextBox txtM7 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM7") as ASPxTextBox;
        ASPxTextBox txtM8 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM8") as ASPxTextBox;
        ASPxTextBox txtM9 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM9") as ASPxTextBox;
        ASPxTextBox txtM10 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM10") as ASPxTextBox;
        ASPxTextBox txtM11 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM11") as ASPxTextBox;
        ASPxTextBox txtM12 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtM12") as ASPxTextBox;
        ASPxTextBox lblSum = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "lblSum") as ASPxTextBox;
        ASPxTextBox lblSumWeight = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "lblSumWeight") as ASPxTextBox;

        #endregion

        #region AddClientInstanceName
        txtM1.ClientInstanceName = "txtM1_" + e.VisibleIndex;
        txtM2.ClientInstanceName = "txtM2_" + e.VisibleIndex;
        txtM3.ClientInstanceName = "txtM3_" + e.VisibleIndex;
        txtM4.ClientInstanceName = "txtM4_" + e.VisibleIndex;
        txtM5.ClientInstanceName = "txtM5_" + e.VisibleIndex;
        txtM6.ClientInstanceName = "txtM6_" + e.VisibleIndex;
        txtM7.ClientInstanceName = "txtM7_" + e.VisibleIndex;
        txtM8.ClientInstanceName = "txtM8_" + e.VisibleIndex;
        txtM9.ClientInstanceName = "txtM9_" + e.VisibleIndex;
        txtM10.ClientInstanceName = "txtM10_" + e.VisibleIndex;
        txtM11.ClientInstanceName = "txtM11_" + e.VisibleIndex;
        txtM12.ClientInstanceName = "txtM12_" + e.VisibleIndex;
        lblSum.ClientInstanceName = "lblSum_" + e.VisibleIndex;
        lblSumWeight.ClientInstanceName = "lblSumWeight_" + e.VisibleIndex;
        #endregion

        #region AddJava
        dynamic data = gvwT1.GetRowValues(e.VisibleIndex, "KPI_SUM", "PHASE1", "PHASE2", "PHASE3", "PHASE4", "PHASE5", "KPI_SUMWEIGHT");
        txtM1.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM2.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM3.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM4.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM5.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM6.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM7.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM8.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM9.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM10.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM11.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        txtM12.ClientSideEvents.ValueChanged = "function(s,e) { CalculateRow(" + e.VisibleIndex + ",'" + data[0] + "','" + data[1] + "','" + data[2] + "','" + data[3] + "','" + data[4] + "','" + data[5] + "','" + data[6] + "') }";
        #endregion

        #region SetData
        //เช็คข้อมูล

        if (lstData_KPI.Count > 0 && lstData_KPI_Rate.Count > 0)
        {
            dynamic data2 = gvwT1.GetRowValues(e.VisibleIndex, "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12", "SUMKPI", "SUMKPI_WEIGHT", "KPI_ID");

            string CC = data2[14] + "";
            var KPI = lstData_KPI.Where(w => w.KPI_ID == data2[14] + "").FirstOrDefault();
            var KPI_RATE = lstData_KPI_Rate.Where(w => w.KPI_ID == data2[14] + "").FirstOrDefault();

            //หาก มีการเปลี่ยนแปลง ความถี่ ผู้ใช้จะต้องกรอกค่า คะแนนประเมินผล KPI ใหม่ทั้งหมด
            if (KPI.KPI_Frequency != KPI_RATE.KPI_Frequency)
            {

            }
            //หาก มีการเปลี่ยนแปลง วิธีคำนวณผลรวม ระบบจะทำการrecalculateให้ใหม่ 
            else if (KPI.KPI_Sum != KPI_RATE.KPI_Sum)
            {
                string SVAL = "";
                decimal SUM = 0;
                decimal? SUM2 = null;
                //หาค่านำไปเทียบ
                switch (KPI.KPI_Sum)
                {
                    //ค่าเฉลี่ย
                    case "0":
                        int C = 0;

                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                SUM = SUM + decimal.Parse(data2[i] + "");
                                C++;
                            }
                        }

                        SVAL = string.Format("{0:00}", (SUM / C));


                        break;
                    case "1":

                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                decimal SVALUE = decimal.Parse(data2[i] + "");

                                if (SUM2 == null)
                                {
                                    SUM2 = SVALUE;
                                }

                                if (SUM2 > SVALUE)
                                {
                                    SUM2 = SVALUE;
                                }
                            }
                        }
                        SVAL = string.Format("{0:n0}", SUM2);
                        break;
                    case "2":
                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                decimal SVALUE = decimal.Parse(data2[i] + "");

                                if (SUM2 == null)
                                {
                                    SUM2 = SVALUE;
                                }

                                if (SUM2 < SVALUE)
                                {
                                    SUM2 = SVALUE;
                                }
                            }
                        }
                        SVAL = string.Format("{0:n0}", SUM2);
                        break;
                }

                string SUMKPI = "";

                if (CheckRank(SVAL + "", data[1] + ""))
                {
                    SUMKPI = "1";
                }
                else if (CheckRank(SVAL + "", data[2] + ""))
                {
                    SUMKPI = "2";
                }
                else if (CheckRank(SVAL + "", data[3] + ""))
                {
                    SUMKPI = "3";
                }
                else if (CheckRank(SVAL + "", data[4] + ""))
                {
                    SUMKPI = "4";
                }
                else if (CheckRank(SVAL + "", data[5] + ""))
                {
                    SUMKPI = "5";
                }
                else
                {
                    SUMKPI = "ไม่มีค่าที่ตรงกับ Rank";
                }


                txtM1.Text = data2[0] + "";
                txtM2.Text = data2[1] + "";
                txtM3.Text = data2[2] + "";
                txtM4.Text = data2[3] + "";
                txtM5.Text = data2[4] + "";
                txtM6.Text = data2[5] + "";
                txtM7.Text = data2[6] + "";
                txtM8.Text = data2[7] + "";
                txtM9.Text = data2[8] + "";
                txtM10.Text = data2[9] + "";
                txtM11.Text = data2[10] + "";
                txtM12.Text = data2[11] + "";
                lblSum.Text = SUMKPI;

                decimal CAL = SUMKPI != "ไม่มีค่าที่ตรงกับ Rank" ? ((decimal.Parse(SUMKPI) / 5) * decimal.Parse(data[6] + "")) : null;
                if (CAL != null)
                {
                    string SUM_Format = string.Format("{0:n0}", CAL);
                    lblSumWeight.Text = SUM_Format;
                }
            }
            //กรณีถ้ามีการแก้ไข Rank ระบบจะทำการrecalculateให้ดูใหม่
            else if ((KPI.Phase1 + KPI.Phase2 + KPI.Phase3 + KPI.Phase4 + KPI.Phase5) != (KPI_RATE.Phase1 + KPI_RATE.Phase2 + KPI_RATE.Phase3 + KPI_RATE.Phase4 + KPI_RATE.Phase5))
            {
                string SVAL = "";
                decimal SUM = 0;
                decimal? SUM2 = null;
                //หาค่านำไปเทียบ
                switch (KPI.KPI_Sum)
                {
                    //ค่าเฉลี่ย
                    case "0":
                        int C = 0;

                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                SUM = SUM + decimal.Parse(data2[i] + "");
                                C++;
                            }
                        }

                        SVAL = string.Format("{0:00}", (SUM / C));


                        break;
                    case "1":

                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                decimal SVALUE = decimal.Parse(data2[i] + "");

                                if (SUM2 == null)
                                {
                                    SUM2 = SVALUE;
                                }

                                if (SUM2 > SVALUE)
                                {
                                    SUM2 = SVALUE;
                                }
                            }
                        }


                        SVAL = string.Format("{0:n0}", SUM2);
                        break;
                    case "2":
                        for (int i = 0; i < 12; i++)
                        {
                            if (!string.IsNullOrEmpty(data2[i] + ""))
                            {
                                decimal SVALUE = decimal.Parse(data2[i] + "");

                                if (SUM2 == null)
                                {
                                    SUM2 = SVALUE;
                                }

                                if (SUM2 < SVALUE)
                                {
                                    SUM2 = SVALUE;
                                }
                            }
                        }
                        SVAL = string.Format("{0:n0}", SUM2);
                        break;
                }


                string sMaxPhase = MaxPhase(data[1] + "", data[2] + "", data[3] + "", data[4] + "", data[5] + "");
                string vMaxPhase = ValueMaxPhase(data[1] + "", data[2] + "", data[3] + "", data[4] + "", data[5] + "");
                string SUMKPI = "";

                if (CheckRankMaxValue(SVAL + "", data[1] + "", "1", sMaxPhase, vMaxPhase))
                {
                    SUMKPI = "1";
                }
                else if (CheckRankMaxValue(SVAL + "", data[2] + "", "2", sMaxPhase, vMaxPhase))
                {
                    SUMKPI = "2";
                }
                else if (CheckRankMaxValue(SVAL + "", data[3] + "", "3", sMaxPhase, vMaxPhase))
                {
                    SUMKPI = "3";
                }
                else if (CheckRankMaxValue(SVAL + "", data[4] + "", "4", sMaxPhase, vMaxPhase))
                {
                    SUMKPI = "4";
                }
                else if (CheckRankMaxValue(SVAL + "", data[5] + "", "5", sMaxPhase, vMaxPhase))
                {
                    SUMKPI = "5";
                }
                else
                {
                    SUMKPI = "ไม่มีค่าที่ตรงกับ Rank";


                }


                txtM1.Text = Value_MaxPhase(data2[0] + "", vMaxPhase);
                txtM2.Text = Value_MaxPhase(data2[1] + "", vMaxPhase);
                txtM3.Text = Value_MaxPhase(data2[2] + "", vMaxPhase);
                txtM4.Text = Value_MaxPhase(data2[3] + "", vMaxPhase);
                txtM5.Text = Value_MaxPhase(data2[4] + "", vMaxPhase);
                txtM6.Text = Value_MaxPhase(data2[5] + "", vMaxPhase);
                txtM7.Text = Value_MaxPhase(data2[6] + "", vMaxPhase);
                txtM8.Text = Value_MaxPhase(data2[7] + "", vMaxPhase);
                txtM9.Text = Value_MaxPhase(data2[8] + "", vMaxPhase);
                txtM10.Text = Value_MaxPhase(data2[9] + "", vMaxPhase);
                txtM11.Text = Value_MaxPhase(data2[10] + "", vMaxPhase);
                txtM12.Text = Value_MaxPhase(data2[11] + "", vMaxPhase);
                lblSum.Text = SUMKPI;

                decimal? CAL = SUMKPI != "ไม่มีค่าที่ตรงกับ Rank" ? ((decimal.Parse(SUMKPI) / 5) * decimal.Parse(data[6] + "")) : null;
                if (CAL != null)
                {
                    string SUM_Format = string.Format("{0:n0}", CAL);
                    lblSumWeight.Text = SUM_Format;
                }
            }
            else
            {

                txtM1.Text = data2[0] + "";
                txtM2.Text = data2[1] + "";
                txtM3.Text = data2[2] + "";
                txtM4.Text = data2[3] + "";
                txtM5.Text = data2[4] + "";
                txtM6.Text = data2[5] + "";
                txtM7.Text = data2[6] + "";
                txtM8.Text = data2[7] + "";
                txtM9.Text = data2[8] + "";
                txtM10.Text = data2[9] + "";
                txtM11.Text = data2[10] + "";
                txtM12.Text = data2[11] + "";
                lblSum.Text = (data2[12] + "" == "9999999" ? "ไม่มีค่าที่ตรงกับ Rank" : data2[12] + "");
                lblSumWeight.Text = data2[13] + "";
            }

        }
        else
        {
            dynamic data2 = gvwT1.GetRowValues(e.VisibleIndex, "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M10", "M11", "M12", "SUMKPI", "SUMKPI_WEIGHT");
            txtM1.Text = data2[0] + "";
            txtM2.Text = data2[1] + "";
            txtM3.Text = data2[2] + "";
            txtM4.Text = data2[3] + "";
            txtM5.Text = data2[4] + "";
            txtM6.Text = data2[5] + "";
            txtM7.Text = data2[6] + "";
            txtM8.Text = data2[7] + "";
            txtM9.Text = data2[8] + "";
            txtM10.Text = data2[9] + "";
            txtM11.Text = data2[10] + "";
            txtM12.Text = data2[11] + "";
            lblSum.Text = (data2[12] + "" == "9999999" ? "ไม่มีค่าที่ตรงกับ Rank" : data2[12] + "");
            lblSumWeight.Text = data2[13] + "";
        }
        #endregion

        #region SetTotal
        if (e.DataColumn.ToString() == "สรุปผลคะแนน<br>หลัง  Weight")
        {
            if (string.IsNullOrEmpty(Session["SUM"] + "") && !string.IsNullOrEmpty(lblSumWeight.Text))
            {
                Session["SUM"] = lblSumWeight.Text;
            }
            else if (!string.IsNullOrEmpty(Session["SUM"] + "") && !string.IsNullOrEmpty(lblSumWeight.Text))
            {
                Session["SUM"] = int.Parse(lblSumWeight.Text) + int.Parse(Session["SUM"] + "");
            }

            ASPxLabel lblTotal = gvwT1.FindFooterRowTemplateControl("lblTotal") as ASPxLabel;
            lblTotal.Text = Session["SUM"] + "";
        }
        #endregion
    }

    void gvwT1_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.KeyValue != null)
        {
            string KPI_FREQUENCY = e.GetValue("KPI_FREQUENCY").ToString();
            if (e.Row.Cells.Count > 1)
            {
                switch (KPI_FREQUENCY)
                {
                    case "0":
                        break;
                    case "1":
                        /*คอลัมน์ M1 = 9,M12 = 20*/
                        e.Row.Cells[11].ColumnSpan = 3;
                        e.Row.Cells[14].ColumnSpan = 3;
                        e.Row.Cells[17].ColumnSpan = 3;
                        e.Row.Cells[20].ColumnSpan = 3;

                        e.Row.Cells[9].Visible = false;
                        e.Row.Cells[10].Visible = false;
                        e.Row.Cells[12].Visible = false;
                        e.Row.Cells[13].Visible = false;
                        e.Row.Cells[15].Visible = false;
                        e.Row.Cells[16].Visible = false;
                        e.Row.Cells[18].Visible = false;
                        e.Row.Cells[19].Visible = false;

                        e.Row.Cells[11].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[14].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[17].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[20].HorizontalAlign = HorizontalAlign.Center;

                        break;
                    case "2":
                        e.Row.Cells[14].ColumnSpan = 6;
                        e.Row.Cells[20].ColumnSpan = 6;

                        e.Row.Cells[9].Visible = false;
                        e.Row.Cells[10].Visible = false;
                        e.Row.Cells[11].Visible = false;
                        e.Row.Cells[12].Visible = false;
                        e.Row.Cells[13].Visible = false;
                        e.Row.Cells[15].Visible = false;
                        e.Row.Cells[16].Visible = false;
                        e.Row.Cells[17].Visible = false;
                        e.Row.Cells[18].Visible = false;
                        e.Row.Cells[19].Visible = false;

                        e.Row.Cells[14].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[20].HorizontalAlign = HorizontalAlign.Center;

                        break;
                    case "3":
                        e.Row.Cells[20].ColumnSpan = 12;

                        e.Row.Cells[9].Visible = false;
                        e.Row.Cells[10].Visible = false;
                        e.Row.Cells[11].Visible = false;
                        e.Row.Cells[12].Visible = false;
                        e.Row.Cells[13].Visible = false;
                        e.Row.Cells[14].Visible = false;
                        e.Row.Cells[15].Visible = false;
                        e.Row.Cells[16].Visible = false;
                        e.Row.Cells[17].Visible = false;
                        e.Row.Cells[18].Visible = false;
                        e.Row.Cells[19].Visible = false;

                        e.Row.Cells[20].HorizontalAlign = HorizontalAlign.Center;

                        break;
                }

            }



        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');

        int Index = 0;
        if (param.Length > 1)
        {
            Index = int.Parse(param[1] + "");
        }

        switch (param[0])
        {
            case "Save":
                Session["SUM"] = "";
                AddDataKPI_RATE();
                ListData();
                break;
            case "Cancel":
                xcpn.JSProperties["cpRedirectTo"] = "KPI.aspx";
                break;

        }


    }

    void ListData()
    {
        string QUERY = @"SELECT KP.KPI_ID, KP.KPI_NAME, CASE 
WHEN KP.KPI_FREQUENCY = '0' THEN 'รายเดือน' 
WHEN KP.KPI_FREQUENCY = '1' THEN 'รายไตรมาส' 
WHEN KP.KPI_FREQUENCY = '2' THEN 'รายครึ่งปี' 
WHEN KP.KPI_FREQUENCY = '3' THEN 'รายปี' 
ELSE '' END as  KPI_FREQUENCY_NAME, 
CASE 
WHEN KP.KPI_SUM = '0' THEN 'ค่าเฉลี่ย' 
WHEN KP.KPI_SUM = '1' THEN 'ค่าต่ำสุด' 
WHEN KP.KPI_SUM = '2' THEN 'ค่าสูงสุด' 
ELSE '' END as  KPI_SUM_NAME,KP.KPI_FREQUENCY,KP.KPI_SUM,  KP.PHASE1,KP.PHASE2, KP.PHASE3, KP.PHASE4, KP.PHASE5, KP.KPI_SUMWEIGHT, KP.CACTIVE ,
KR.SCONTRACTID, KR.SVENDORID,
KR.M1, KR.M2, KR.M3, KR.M4,  KR.M5, KR.M6, KR.M7,  KR.M8, KR.M9, KR.M10,  KR.M11, KR.M12, KR.SUMKPI,  KR.SUMKPI_WEIGHT
FROM TBL_KPI KP
LEFT JOIN TBL_KPI_RATE KR
ON KP.KPI_ID = KR.KPI_ID WHERE  KR.SCONTRACTID  = " + CommonFunction.ReplaceInjection(Session["CONID"] + "") + " AND KR.SVENDORID = '" + CommonFunction.ReplaceInjection(Session["VENID"] + "") + "' AND KR.SYEAR = '" + CommonFunction.ReplaceInjection(SYEAR) + "' ORDER BY KP.KPI_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
           
        }
        else
        {
            string _QUERY = @"SELECT KP.KPI_ID, KP.KPI_NAME, CASE 
WHEN KP.KPI_FREQUENCY = '0' THEN 'รายเดือน' 
WHEN KP.KPI_FREQUENCY = '1' THEN 'รายไตรมาส' 
WHEN KP.KPI_FREQUENCY = '2' THEN 'รายครึ่งปี' 
WHEN KP.KPI_FREQUENCY = '3' THEN 'รายปี' 
ELSE '' END as  KPI_FREQUENCY_NAME, 
CASE 
WHEN KP.KPI_SUM = '0' THEN 'ค่าเฉลี่ย' 
WHEN KP.KPI_SUM = '1' THEN 'ค่าต่ำสุด' 
WHEN KP.KPI_SUM = '2' THEN 'ค่าสูงสุด' 
ELSE '' END as  KPI_SUM_NAME,KP.KPI_FREQUENCY,KP.KPI_SUM,  KP.PHASE1,KP.PHASE2, KP.PHASE3, KP.PHASE4, KP.PHASE5, KP.KPI_SUMWEIGHT, KP.CACTIVE ,
'' as SCONTRACTID, '' as SVENDORID,
'' as M1, '' as M2, '' as M3, '' as M4,  '' as M5, '' as M6, '' as M7,  '' as M8, '' as M9, '' as M10,  '' as M11, '' as M12, '' as SUMKPI,  '' as SUMKPI_WEIGHT
FROM TBL_KPI KP WHERE KP.CACTIVE = 'Y'";
            dt = new DataTable();
            dt = CommonFunction.Get_Data(conn, _QUERY);
        }

        gvwT1.DataSource = dt;
        gvwT1.DataBind();



        //เก็บค่าเพื่อไปเช็คว่ามีการเปลี่ยนแปลงข้อมูลไหม
        AddList_KPI();

    }

    private string QueryStr_ReturnValue(string QS_Name)
    {
        string Result = "";

        string str = Request.QueryString[QS_Name];
        string[] strArray;
        if (!string.IsNullOrEmpty(str))
        {
            strArray = STCrypt.DecryptURL(str);
            Result = strArray[0] + "";
        }

        return Result;
    }

    private void GridToList()
    {
        lstData.Clear();

        for (int i = 0; i < gvwT1.VisibleRowCount; i++)
        {
            dynamic data = gvwT1.GetRowValues(i, "KPI_ID", "CACTIVE", "KPI_NAME", "KPI_FREQUENCY", "KPI_SUM", "PHASE1", "PHASE2", "PHASE3", "PHASE4", "PHASE5", "KPI_SUMWEIGHT");

            ASPxTextBox txtM1 = gvwT1.FindRowCellTemplateControl(i, null, "txtM1") as ASPxTextBox;
            ASPxTextBox txtM2 = gvwT1.FindRowCellTemplateControl(i, null, "txtM2") as ASPxTextBox;
            ASPxTextBox txtM3 = gvwT1.FindRowCellTemplateControl(i, null, "txtM3") as ASPxTextBox;
            ASPxTextBox txtM4 = gvwT1.FindRowCellTemplateControl(i, null, "txtM4") as ASPxTextBox;
            ASPxTextBox txtM5 = gvwT1.FindRowCellTemplateControl(i, null, "txtM5") as ASPxTextBox;
            ASPxTextBox txtM6 = gvwT1.FindRowCellTemplateControl(i, null, "txtM6") as ASPxTextBox;
            ASPxTextBox txtM7 = gvwT1.FindRowCellTemplateControl(i, null, "txtM7") as ASPxTextBox;
            ASPxTextBox txtM8 = gvwT1.FindRowCellTemplateControl(i, null, "txtM8") as ASPxTextBox;
            ASPxTextBox txtM9 = gvwT1.FindRowCellTemplateControl(i, null, "txtM9") as ASPxTextBox;
            ASPxTextBox txtM10 = gvwT1.FindRowCellTemplateControl(i, null, "txtM10") as ASPxTextBox;
            ASPxTextBox txtM11 = gvwT1.FindRowCellTemplateControl(i, null, "txtM11") as ASPxTextBox;
            ASPxTextBox txtM12 = gvwT1.FindRowCellTemplateControl(i, null, "txtM12") as ASPxTextBox;
            ASPxTextBox lblSum = gvwT1.FindRowCellTemplateControl(i, null, "lblSum") as ASPxTextBox;
            ASPxTextBox lblSumWeight = gvwT1.FindRowCellTemplateControl(i, null, "lblSumWeight") as ASPxTextBox;

            lstData.Add(new Tdata
            {
                KPI_ID = data[0] + "",
                KPI_NAME = data[2] + "",
                KPI_Frequency = data[3] + "",
                KPI_Sum = data[4] + "",
                Phase1 = data[5] + "",
                Phase2 = data[6] + "",
                Phase3 = data[7] + "",
                Phase4 = data[8] + "",
                Phase5 = data[9] + "",
                KPI_SumWeight = data[10] + "",
                CACTIVE = data[1],
                SCONTRACTID = Session["CONID"] + "",
                SVENDORID = Session["VENID"] + "",
                M1 = txtM1.Text,
                M2 = txtM2.Text,
                M3 = txtM3.Text,
                M4 = txtM4.Text,
                M5 = txtM5.Text,
                M6 = txtM6.Text,
                M7 = txtM7.Text,
                M8 = txtM8.Text,
                M9 = txtM9.Text,
                M10 = txtM10.Text,
                M11 = txtM11.Text,
                M12 = txtM12.Text,
                SUMKPI = lblSum.Text,
                SUMKPI_WEIGHT = lblSumWeight.Text
            });
        }
    }

    private void AddDataKPI_RATE()
    {
        GridToList();

        foreach (var item in lstData)
        {
            DataTable dt = CommonFunction.Get_Data(conn, "SELECT KPI_ID,SCONTRACTID,SVENDORID FROM TBL_KPI_RATE WHERE  KPI_ID  = " + CommonFunction.ReplaceInjection(item.KPI_ID) + " AND SCONTRACTID  = " + CommonFunction.ReplaceInjection(item.SCONTRACTID) + " AND SVENDORID = '" + CommonFunction.ReplaceInjection(item.SVENDORID) + "' AND SYEAR = '" + CommonFunction.ReplaceInjection(SYEAR) + "'");

            if (dt.Rows.Count > 0)
            {

                ToHistTory(item.KPI_ID, item.SCONTRACTID, item.SVENDORID);

                string QUERY_UPT = @"UPDATE TBL_KPI_RATE
                                    SET    KPI_NAME      = " + CheckNull(item.KPI_NAME) + @",
                                           KPI_FREQUENCY = " + CheckNull(item.KPI_Frequency) + @",
                                           KPI_SUM       = " + CheckNull(item.KPI_Sum) + @",
                                           PHASE1        = " + CheckNull(item.Phase1) + @",
                                           PHASE2        = " + CheckNull(item.Phase2) + @",
                                           PHASE3        = " + CheckNull(item.Phase3) + @",
                                           PHASE4        = " + CheckNull(item.Phase4) + @",
                                           PHASE5        = " + CheckNull(item.Phase5) + @",
                                           KPI_SUMWEIGHT = " + CheckNullInt(item.KPI_SumWeight) + @",
                                           CACTIVE       = " + CheckNull(item.CACTIVE) + @",
                                           DUPDATE       = SYSDATE,
                                           SUPDATE       = " + CheckNull(Session["UserID"] + "") + @",
                                           M1            = " + CheckNullInt(item.M1) + @",
                                           M2            = " + CheckNullInt(item.M2) + @",
                                           M3            = " + CheckNullInt(item.M3) + @",
                                           M4            = " + CheckNullInt(item.M4) + @",
                                           M5            = " + CheckNullInt(item.M5) + @",
                                           M6            = " + CheckNullInt(item.M6) + @",
                                           M7            = " + CheckNullInt(item.M7) + @",
                                           M8            = " + CheckNullInt(item.M8) + @",
                                           M9            = " + CheckNullInt(item.M9) + @",
                                           M10           = " + CheckNullInt(item.M10) + @",
                                           M11           = " + CheckNullInt(item.M11) + @",
                                           M12           = " + CheckNullInt(item.M12) + @",
                                           SUMKPI        = " + CheckNullInt(item.SUMKPI) + @",
                                           SUMKPI_WEIGHT = " + CheckNullInt(item.SUMKPI_WEIGHT) + @"
                                    WHERE  KPI_ID        = " + CommonFunction.ReplaceInjection(item.KPI_ID) + " AND SCONTRACTID  = " + CommonFunction.ReplaceInjection(item.SCONTRACTID) + " AND SVENDORID = '" + CommonFunction.ReplaceInjection(item.SVENDORID) + "' AND SYEAR = '" + CommonFunction.ReplaceInjection(SYEAR) + "'";
                AddTODB(QUERY_UPT);
            }
            else
            {
                string QUERY_INS = @"INSERT INTO TBL_KPI_RATE (KPI_ID, KPI_NAME, KPI_FREQUENCY, KPI_SUM, PHASE1, PHASE2, PHASE3, PHASE4, PHASE5, KPI_SUMWEIGHT, CACTIVE, DCREATE, SCREATE, 
   SCONTRACTID, SVENDORID, M1, M2, M3, M4, M5, M6, M7, M8, M9, M10, M11, M12, SUMKPI, SUMKPI_WEIGHT,SYEAR) 
                                     VALUES(" + item.KPI_ID + @",
                                            " + CheckNull(item.KPI_NAME) + @", 
                                            " + CheckNull(item.KPI_Frequency) + @",
                                            " + CheckNull(item.KPI_Sum) + @",
                                            " + CheckNull(item.Phase1) + @",
                                            " + CheckNull(item.Phase2) + @",
                                            " + CheckNull(item.Phase3) + @",
                                            " + CheckNull(item.Phase4) + @",
                                            " + CheckNull(item.Phase5) + @",
                                            " + CheckNullInt(item.KPI_SumWeight) + @",
                                            " + CheckNull(item.CACTIVE) + @",
                                            SYSDATE,
                                            " + CheckNull(Session["UserID"] + "") + @",
                                            " + CheckNullInt(item.SCONTRACTID) + @",
                                            " + CheckNull(item.SVENDORID) + @",
                                            " + CheckNullInt(item.M1) + @",
                                            " + CheckNullInt(item.M2) + @",
                                            " + CheckNullInt(item.M3) + @",
                                            " + CheckNullInt(item.M4) + @",
                                            " + CheckNullInt(item.M5) + @",
                                            " + CheckNullInt(item.M6) + @",
                                            " + CheckNullInt(item.M7) + @",
                                            " + CheckNullInt(item.M8) + @",
                                            " + CheckNullInt(item.M9) + @",
                                            " + CheckNullInt(item.M10) + @",
                                            " + CheckNullInt(item.M11) + @",
                                            " + CheckNullInt(item.M12) + @",
                                            " + CheckNullInt(item.SUMKPI) + @",
                                            " + CheckNullInt(item.SUMKPI_WEIGHT) + @",
                                            '" + CommonFunction.ReplaceInjection(SYEAR) + "')";

                AddTODB(QUERY_INS);
            }
        }
    }

    void ToHistTory(string KPI_ID, string SCONTRACTID, string SVENDORID)
    {
        string NVERSION = CommonFunction.Gen_ID(conn, "SELECT NVERSION FROM TBL_KPI_RATE_HISTORY WHERE  KPI_ID  = " + CommonFunction.ReplaceInjection(KPI_ID) + " AND SCONTRACTID  = " + CommonFunction.ReplaceInjection(SCONTRACTID) + " AND SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "' ORDER BY NVERSION DESC");

        string QUERY = @"INSERT INTO TBL_KPI_RATE_HISTORY (
   KPI_ID, KPI_NAME, KPI_FREQUENCY, 
   KPI_SUM, PHASE1, PHASE2, 
   PHASE3, PHASE4, PHASE5, 
   KPI_SUMWEIGHT, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   SCONTRACTID, SVENDORID, M1, 
   M2, M3, M4, 
   M5, M6, M7, 
   M8, M9, M10, 
   M11, M12, SUMKPI, 
   SUMKPI_WEIGHT,NVERSION,SYEAR) 
SELECT  KPI_ID ,
  KPI_NAME ,
  KPI_FREQUENCY ,
  KPI_SUM ,
  PHASE1 ,
  PHASE2 ,
  PHASE3 ,
  PHASE4 ,
  PHASE5 ,
  KPI_SUMWEIGHT ,
  CACTIVE ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  SCONTRACTID ,
  SVENDORID ,
  M1 ,
  M2 ,
  M3 ,
  M4 ,
  M5 ,
  M6 ,
  M7 ,
  M8 ,
  M9 ,
  M10 ,
  M11 ,
  M12 ,
  SUMKPI ,
  SUMKPI_WEIGHT," + NVERSION + " as NVERSION,SYEAR   FROM TBL_KPI_RATE WHERE  KPI_ID  = " + CommonFunction.ReplaceInjection(KPI_ID) + " AND SCONTRACTID  = " + CommonFunction.ReplaceInjection(SCONTRACTID) + " AND SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";
        AddTODB(QUERY);
    }

    private string CheckNull(string VALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(VALUE))
        {
            Result = "'" + CommonFunction.ReplaceInjection(VALUE) + "'";
        }
        else
        {
            Result = "null";
        }

        return Result;
    }

    private string CheckNullInt(string VALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(VALUE))
        {
            Result = VALUE;
        }
        else
        {
            Result = "null";
        }

        if (VALUE == "ไม่มีค่าที่ตรงกับ Rank")
        {
            Result = "9999999";
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private void AddList_KPI()
    {
        lstData_KPI.Clear();
        lstData_KPI_Rate.Clear();

        string QUERY = @"SELECT KPI_ID, KPI_FREQUENCY,KPI_SUM,  PHASE1, PHASE2, PHASE3, PHASE4, PHASE5 FROM TBL_KPI ORDER BY KPI_ID ASC";
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            lstData_KPI.Add(new Tdata
            {
                KPI_ID = dt.Rows[i]["KPI_ID"] + "",
                KPI_Frequency = dt.Rows[i]["KPI_FREQUENCY"] + "",
                KPI_Sum = dt.Rows[i]["KPI_SUM"] + "",
                Phase1 = dt.Rows[i]["PHASE1"] + "",
                Phase2 = dt.Rows[i]["PHASE2"] + "",
                Phase3 = dt.Rows[i]["PHASE3"] + "",
                Phase4 = dt.Rows[i]["PHASE4"] + "",
                Phase5 = dt.Rows[i]["PHASE5"] + "",
            });
        }

        string QUERY_RATE = @"SELECT KPI_ID, KPI_FREQUENCY,KPI_SUM,  PHASE1, PHASE2, PHASE3, PHASE4, PHASE5 FROM TBL_KPI_RATE WHERE CACTIVE = 'Y' AND  SCONTRACTID  = " + CommonFunction.ReplaceInjection(Session["CONID"] + "") + " AND SVENDORID = '" + CommonFunction.ReplaceInjection(Session["VENID"] + "") + "' ORDER BY KPI_ID ASC";
        DataTable dt_rate = CommonFunction.Get_Data(conn, QUERY_RATE);

        for (int i = 0; i < dt_rate.Rows.Count; i++)
        {
            lstData_KPI_Rate.Add(new Tdata
            {
                KPI_ID = dt_rate.Rows[i]["KPI_ID"] + "",
                KPI_Frequency = dt_rate.Rows[i]["KPI_FREQUENCY"] + "",
                KPI_Sum = dt_rate.Rows[i]["KPI_SUM"] + "",
                Phase1 = dt_rate.Rows[i]["PHASE1"] + "",
                Phase2 = dt_rate.Rows[i]["PHASE2"] + "",
                Phase3 = dt_rate.Rows[i]["PHASE3"] + "",
                Phase4 = dt_rate.Rows[i]["PHASE4"] + "",
                Phase5 = dt_rate.Rows[i]["PHASE5"] + "",
            });
        }
    }

    private bool CheckRank(string SVAL, string PAHSE)
    {
        bool Restult = false;

        if (!string.IsNullOrEmpty(PAHSE))
        {
            string VAL1 = null;
            string VAL2 = null;
            /*เช็คว่าค่าที่ส่งมาเป็นแบบช่วง หรือแบบเดี่ยว*/
            string Rank = PAHSE + "";
            if (Rank.Contains("-"))
            {

                string[] res = Rank.Split('-');

                VAL1 = res[0];
                VAL2 = res[1];


                if (decimal.Parse(VAL1) <= decimal.Parse(SVAL) && decimal.Parse(VAL2) >= decimal.Parse(SVAL))
                {
                    Restult = true;
                }
                else
                {

                    Restult = false;

                }
            }
            else
            {
                if (decimal.Parse(SVAL) == decimal.Parse(PAHSE))
                {
                    Restult = true;
                }
                else
                {
                    Restult = false;
                }
            }
        }
        else
        {
            Restult = false;
        }

        return Restult;
    }

    private bool CheckRankMaxValue(string SVAL, string PAHSE, string ISPHASE, string MAXPHASE, string VALUEMAX)
    {
        bool Restult = false;

        if (!string.IsNullOrEmpty(PAHSE))
        {
            string VAL1 = null;
            string VAL2 = null;
            /*เช็คว่าค่าที่ส่งมาเป็นแบบช่วง หรือแบบเดี่ยว*/
            string Rank = PAHSE + "";
            if (Rank.Contains("-"))
            {

                string[] res = Rank.Split('-');

                VAL1 = res[0];
                VAL2 = res[1];


                if (decimal.Parse(VAL1) <= decimal.Parse(SVAL) && decimal.Parse(VAL2) >= decimal.Parse(SVAL))
                {
                    Restult = true;
                }
                else
                {
                    if (ISPHASE == MAXPHASE && int.Parse(SVAL) > int.Parse(VALUEMAX))
                    {
                        Restult = true;
                    }
                    else
                    {
                        Restult = false;
                    }
                }
            }
            else
            {
                if (decimal.Parse(SVAL) == decimal.Parse(PAHSE))
                {
                    Restult = true;
                }
                else
                {
                    if (ISPHASE == MAXPHASE && int.Parse(SVAL) > int.Parse(VALUEMAX))
                    {
                        Restult = true;
                    }
                    else
                    {
                        Restult = false;
                    }
                }
            }
        }
        else
        {
            Restult = false;
        }

        return Restult;
    }

    private string MaxPhase(string PHASE1, string PHASE2, string PHASE3, string PHASE4, string PHASE5)
    {
        string Result = "";

        int MAX_P1 = Max_SelfPhase(PHASE1);
        int MAX_P2 = Max_SelfPhase(PHASE2);
        int MAX_P3 = Max_SelfPhase(PHASE3);
        int MAX_P4 = Max_SelfPhase(PHASE4);
        int MAX_P5 = Max_SelfPhase(PHASE5);

        int[] array1 = new int[] { MAX_P1, MAX_P2, MAX_P3, MAX_P4, MAX_P5 };


        int MAX = 0;
        int SUM = 0;
        for (int i = 0; i < array1.Count(); i++)
        {
            if (array1[i] > SUM)
            {
                SUM = array1[i];
                MAX++;
            }
        }

        Result = MAX + "";

        return Result;
    }

    private string ValueMaxPhase(string PHASE1, string PHASE2, string PHASE3, string PHASE4, string PHASE5)
    {
        string Result = "";

        int MAX_P1 = Max_SelfPhase(PHASE1);
        int MAX_P2 = Max_SelfPhase(PHASE2);
        int MAX_P3 = Max_SelfPhase(PHASE3);
        int MAX_P4 = Max_SelfPhase(PHASE4);
        int MAX_P5 = Max_SelfPhase(PHASE5);

        int[] array1 = new int[] { MAX_P1, MAX_P2, MAX_P3, MAX_P4, MAX_P5 };

        Result = array1.Max() + "";

        return Result;
    }

    private int Max_SelfPhase(string PHASE)
    {
        int Result = 0;

        if (!string.IsNullOrEmpty(PHASE))
        {
            if (PHASE.Contains("-"))
            {
                string[] StrArr = PHASE.Split('-');
                if (int.Parse(StrArr[0] + "") > int.Parse(StrArr[1] + ""))
                {
                    Result = int.Parse(StrArr[0] + "");
                }
                else
                {
                    Result = int.Parse(StrArr[1] + "");
                }
            }
            else
            {
                Result = int.Parse(PHASE);
            }
        }
        else
        {
            Result = 0;
        }

        return Result;
    }

    private string Value_MaxPhase(string SVAL, string SMAXVALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(SVAL))
        {
            if (decimal.Parse(SVAL) > decimal.Parse(SMAXVALUE))
            {
                Result = SMAXVALUE;
            }
            else
            {
                Result = SVAL;
            }
        }

        return Result;
    }

    #region class
    [Serializable]
    class Tdata
    {
        public string KPI_ID { get; set; }
        public string KPI_NAME { get; set; }
        public string KPI_Frequency { get; set; }
        public string KPI_Sum { get; set; }
        public string Phase1 { get; set; }
        public string Phase2 { get; set; }
        public string Phase3 { get; set; }
        public string Phase4 { get; set; }
        public string Phase5 { get; set; }
        public string KPI_SumWeight { get; set; }
        public string CACTIVE { get; set; }
        public string SCONTRACTID { get; set; }
        public string SVENDORID { get; set; }
        public string M1 { get; set; }
        public string M2 { get; set; }
        public string M3 { get; set; }
        public string M4 { get; set; }
        public string M5 { get; set; }
        public string M6 { get; set; }
        public string M7 { get; set; }
        public string M8 { get; set; }
        public string M9 { get; set; }
        public string M10 { get; set; }
        public string M11 { get; set; }
        public string M12 { get; set; }
        public string SUMKPI { get; set; }
        public string SUMKPI_WEIGHT { get; set; }
        public string _Flag { get; set; }
    }
    #endregion

}