﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using TMS_BLL.Transaction.AutoStatus;
using System.Globalization;
using System.Web.UI;

public partial class AutoChangeStatusLog : PageBase
{
    #region + View State +
    private DataTable dtDriverLog
    {
        get
        {
            if ((DataTable)ViewState["dtDriverLog"] != null)
                return (DataTable)ViewState["dtDriverLog"];
            else
                return null;
        }
        set
        {
            ViewState["dtDriverLog"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.CheckPermission();
            this.InitialForm();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void CheckPermission()
    {
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
    }

    private void InitialForm()
    {
        try
        {
            dteStart.Text = DateTime.Now.Date.AddDays(-30).ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            this.SearchData(string.Empty);

            //this.LoadEmailType();
            //this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadEmailType()
    {
        //try
        //{
        //    DataTable dtEmailType = EmailTemplateBLL.Instance.EmailTypeSelectBLL(" AND M_EMAIL_TYPE.ISACTIVE = 1");
        //    DropDownListHelper.BindDropDownList(ref ddlEmailType, dtEmailType, "EMAIL_TYPE_ID", "EMAIL_TYPE_NAME", true);
        //}
        //catch (Exception ex)
        //{
        //    alertFail(ex.Message);
        //}
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtDriverLog = AutoStatusBLL.Instance.DriverLogSelectBLL(Condition, DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd"), DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd"));
            GridViewHelper.BindGridView(ref dgvDriverLog, dtDriverLog);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvDriverLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvDriverLog.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvDriverLog, dtDriverLog);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //try
        //{
        //    var plaintextBytes = Encoding.UTF8.GetBytes(dgvTemplate.DataKeys[e.RowIndex]["SCHEDULE_ID"].ToString());
        //    var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        //    MsgAlert.OpenForm("EmailScheduleAdd.aspx?SCHEDULE_ID=" + encryptedValue, Page);
        //}
        //catch (Exception ex)
        //{
        //    alertFail(ex.Message);
        //}
    }
    //protected void cmdAdd_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("EmailScheduleAdd.aspx");
    //}
}