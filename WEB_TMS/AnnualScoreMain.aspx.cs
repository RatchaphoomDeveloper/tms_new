﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using OfficeOpenXml;
using System.Globalization;
using Utility;
using System.IO;

public partial class AnnualScoreMain : PageBase
{
    string GroupPermission = "01";

    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    protected DataTable dtContract
    {
        get { return (DataTable)ViewState[this.ToString() + "dtContract"]; }
        set { ViewState[this.ToString() + "dtContract"] = value; }
    }

    protected DataTable dtGroup
    {
        get { return (DataTable)ViewState[this.ToString() + "dtGroup"]; }
        set { ViewState[this.ToString() + "dtGroup"] = value; }
    }
    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected DataTable dtSPROCESS
    {
        get { return (DataTable)ViewState[this.ToString() + "dtSPROCESS"]; }
        set { ViewState[this.ToString() + "dtSPROCESS"] = value; }
    }
    protected string SPROCESSID
    {
        get { return ViewState[this.ToString() + "SPROCESSID"].ToString(); }
        set { ViewState[this.ToString() + "SPROCESSID"] = value; }
    }

    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnExport.Enabled = false;
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitForm()
    {
        try
        {
            int StartYear = 2015;
            int cYear = DateTime.Today.Year;
            int endYear = cYear + 4;
            int index = 0;
            for (int i = StartYear; i <= endYear; i++)
            {
                ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
            }

            // เลือกปีปัจจุบัน
            ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
            ddlYearSearch.ClearSelection();
            if (sel != null) sel.Selected = true;
            else ddlYearSearch.SelectedIndex = 0;

            ddlVendorSearch.SelectedIndex = 0;
            LoadVendor();

            DataTable dt = QuarterReportBLL.Instance.GetSPROCESSID();
            if (dt != null)
            {
                List<string> lstStirng = new List<string>();
                foreach (DataRow itm in dt.Rows) lstStirng.Add(itm["SPROCESSID"].ToString());

                SPROCESSID = string.Join(",", lstStirng.ToArray());
            }
            else
                SPROCESSID = "''";

            string _err = string.Empty;
            dtGroup = new FinalGradeBLL().GetGroup(ref _err, true);
            DataRow drG = dtGroup.NewRow();
            drG["NAME"] = "";
            dtGroup.Rows.InsertAt(drG, 0);
            ddlGroup.DataSource = dtGroup;
            ddlGroup.DataBind();

            dtContract = ComplainBLL.Instance.ContractSelectBLL("");
            DataRow dr = dtContract.NewRow();
            dr["SCONTRACTNO"] = "";
            dtContract.Rows.InsertAt(dr, 0);

            ddlContract.DataValueField = "SCONTRACTID";
            ddlContract.DataTextField = "SCONTRACTNO";
            ddlContract.DataSource = dtContract;
            ddlContract.DataBind();

            dtSPROCESS = QuarterReportBLL.Instance.GetSPROCESSID();
            Chkaccident.DataSource = dt;
            Chkaccident.DataTextField = "SPROCESSNAME";
            Chkaccident.DataValueField = "SPROCESSID";

            Chkaccident.DataBind();

            rdoDate.Checked = false;
            rdoMonth.Checked = false;
            rdoQuarter.Checked = false;
            txtCARREGISTERNO.Text = string.Empty;
            txtEMPLOYEE.Text = string.Empty;
            txtDateStart.Text = string.Empty;
            txtDateEnd.Text = string.Empty;

            foreach (ListItem itm in Chkaccident.Items) itm.Selected = true;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();

            ddlVendorSearch.ClearSelection();
            ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
            if (lst != null) lst.Selected = true;

            if (user_profile.CGROUP == GROUP_PERMISSION.VENDOR)// admin = 0 พขร lock --> ที่เหลือ เปิด
            {                
                ddlVendorSearch.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    private void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearchCriteria = new FinalGade() { YEAR = string.Empty, VENDORID = string.Empty };
            if (ddlYearSearch.SelectedItem != null) SearchCriteria.YEAR = ddlYearSearch.SelectedItem.Value;
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.VENDORID = ddlVendorSearch.SelectedItem.Value;
            if (ddlGroup.SelectedItem != null)
            {
                SearchCriteria.GROUPID = ddlGroup.SelectedItem.Value;
                SearchCriteria.GROUPNAME = ddlGroup.SelectedItem.Text;
            }

            if (ddlContract.SelectedItem != null)
            {
                SearchCriteria.SCONTRACTNO = ddlContract.SelectedItem.Text;
                SearchCriteria.SCONTRACTID = ddlContract.SelectedItem.Value;
            }

            SearchCriteria.CARREGISTERNO = txtCARREGISTERNO.Text.Trim();
            SearchCriteria.EMPLOYEE = txtEMPLOYEE.Text.Trim();

            if (rdoDate.Checked)
            {
                bool IsSelStart = (!string.IsNullOrEmpty(txtDateStart.Text.Trim()));
                bool IsSelEnd = (!string.IsNullOrEmpty(txtDateEnd.Text.Trim()));

                if (IsSelStart)
                {
                    SearchCriteria.DateStart = txtDateStart.Text.Trim();
                    DateTime? dStart = DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim());
                    if (dStart > DateTime.Now.AddDays(1))
                    {
                        alertFail("วันที่เริ่มต้นมากกว่าวันที่ปัจจุบัน");
                        return;
                    }
                }

                if (IsSelStart != IsSelEnd)
                {
                    alertFail("เลือกวันที่เริ่มต้น/วันที่สิ้นสุด ไม่ถูกต้อง");
                    return;
                }

                if (IsSelEnd)
                {
                    SearchCriteria.DateEnd = txtDateEnd.Text.Trim();
                    DateTime? dEnd = DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim());
                    if (DateTimeHelpers.ParseDateTime(txtDateStart.Text.Trim()) > dEnd)
                    {
                        alertFail("วันที่เริ่มต้นมากกว่าวันที่สิ้นสุด");
                        return;
                    }
                }
            }
            else if (rdoMonth.Checked)
            {
                if (ddlMonth.SelectedItem != null)
                    SearchCriteria.Month = ddlMonth.SelectedItem.Value;
            }
            else if (rdoQuarter.Checked)
            {
                if (ddlMonth.SelectedItem != null)
                    SearchCriteria.Quarter = ddlQuarter.SelectedItem.Value;
            }


            var selected = Chkaccident.Items.Cast<ListItem>().Where(li => li.Selected).Select(x => x.Value).ToList();
            if (selected != null) SearchCriteria.LstPROCESSID = selected;
            SearhData = new FinalGradeBLL().GetReduceScore(ref _err, SearchCriteria, true);
            foreach (DataRow r in SearhData.Rows)
            {
                List<string> lst = r["SCONTRACTNO"].ToString().Split('/').ToList();
                for (int i = 1; (i <= lst.Count && i <= 6); i++)
                {
                    int val = 0;
                    bool result = int.TryParse(lst[i - 1].ToString(), out val);
                    r[string.Format("col_{0}", i.ToString())] = result ? val.ToString("0000") : lst[i - 1].ToString();
                }
            }
            SearhData.DefaultView.Sort = " col_1 ASC, col_2 ASC, col_3 ASC, col_5 ASC, col_4 ASC";
            grvMain.DataSource = SearhData;
            grvMain.DataBind();

            if (SearhData != null) lblHeader.Text = string.Format("ข้อมูลการตัดคะแนน จำนวน {0} สัญญา", SearhData.Rows.Count);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
            //GotoDefault();
            //return;
        //}

        if (user_profile.CGROUP == GROUP_PERMISSION.VENDOR)
        {
            Chkaccident.Visible = false;
            dvChkaccident2.Visible = false;
        }
    }

    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
            string _keySCONTRACTNO = grvMain.DataKeys[rowIndex.Value]["SCONTRACTNO"].ToString();
            if (_keySCONTRACTNO != null) Session["AASM_SCONTRACTNO"] = _keySCONTRACTNO;
            SearchCriteria.SCONTRACTNO = _keySCONTRACTNO;

            string _keySABBREVIATION = grvMain.DataKeys[rowIndex.Value]["SABBREVIATION"].ToString();
            if (_keySABBREVIATION != null) Session["AASM_SABBREVIATION"] = _keySABBREVIATION;

            string _keyGROUPNAME = grvMain.DataKeys[rowIndex.Value]["GROUPNAME"].ToString();
            if (_keyGROUPNAME != null) Session["AASM_GROUPNAME"] = _keyGROUPNAME;

            Session["AASM_SearchCriteria"] = SearchCriteria;

            Response.Redirect("AnnualScoreDetails.aspx");

        }
    }

    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var SCONTRACTNO = DataBinder.Eval(e.Row.DataItem, "SCONTRACTNO").ToString();
                var SABBREVIATION = DataBinder.Eval(e.Row.DataItem, "SABBREVIATION").ToString();
                var GROUPNAME = DataBinder.Eval(e.Row.DataItem, "GROUPNAME").ToString();

                var ImageView = e.Row.Cells[1].FindControl("imgView") as ImageButton;
                if (ImageView != null)
                {
                    ImageView.Attributes.Add("onclick", "gotoView(\'" + SCONTRACTNO + "|" + SABBREVIATION + "|" + GROUPNAME + "\');");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnViewHistory_Click(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {

        InitForm();
        LoadMain();

    }
    protected void btnExport_Click(object sender, EventArgs e)
    { 
        using (ExcelPackage pck = new ExcelPackage(new MemoryStream(), new MemoryStream(File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/FileFormat/Admin/AnnualScoreMainFormat.xlsx")))))
        {
            ExcelWorksheet ws = pck.Workbook.Worksheets[1];
            int StartRow = 2;
            int index = 1;
            foreach (DataRow dr in SearhData.Rows)
            {
                ws.Cells["A" + StartRow.ToString()].Value = "" + index.ToString();
                ws.Cells["B" + StartRow.ToString()].Value = dr["SABBREVIATION"].ToString();
                ws.Cells["C" + StartRow.ToString()].Value = dr["GROUPNAME"].ToString();
                ws.Cells["D" + StartRow.ToString()].Value = dr["SCONTRACTNO"].ToString();
                ws.Cells["E" + StartRow.ToString()].Value = dr["DISABLE_DRIVER"].ToString();
                ws.Cells["F" + StartRow.ToString()].Value = decimal.Parse(dr["SUMNPOINT"].ToString()).ToString("#,0.00");
                ws.Cells["G" + StartRow.ToString()].Value = decimal.Parse(dr["COST"].ToString()).ToString("#,0.00"); 
                StartRow++;
                index++;
            } 

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=AnnualScoreMain.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    private string GetConditionVendor()
    {
        string result = string.Empty;
        try
        {

            if (ddlVendorSearch.SelectedIndex > 0)
                result = " AND TCONTRACT.SVENDORID = '" + ddlVendorSearch.SelectedValue + "'";

            return result;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void ddlVendorSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        string _err = string.Empty;
        string vendorId = ddlVendorSearch.SelectedValue != null ? ddlVendorSearch.SelectedItem.Value : "";
        dtGroup = new FinalGradeBLL().GetGroup(ref _err, true, vendorId);
        DataRow drG = dtGroup.NewRow();
        drG["NAME"] = "";
        dtGroup.Rows.InsertAt(drG, 0);
        ddlGroup.DataSource = dtGroup;
        ddlGroup.DataBind();

        dtContract = ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor());
        DataRow dr = dtContract.NewRow();
        dr["SCONTRACTNO"] = "";
        dtContract.Rows.InsertAt(dr, 0);

        ddlContract.DataValueField = "SCONTRACTID";
        ddlContract.DataTextField = "SCONTRACTNO";
        ddlContract.DataSource = dtContract;
        ddlContract.DataBind();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        string Id = hdnNum.Value;//SCONTRACTNO,SABBREVIATION,GROUPNAME
        if (Id == "0") return;
        Button btn = sender as Button;
        if (btn.ID == "btnView")
        {
            Session["AASM_SCONTRACTNO"] = Id.Split('|')[0];
            SearchCriteria.SCONTRACTNO = Id.Split('|')[0];
            Session["AASM_SABBREVIATION"] = Id.Split('|')[1];
            Session["AASM_GROUPNAME"] = Id.Split('|')[2];
            Session["AASM_SearchCriteria"] = SearchCriteria;
            SearhData.DefaultView.Sort = " col_1 ASC, col_2 ASC, col_3 ASC, col_5 ASC, col_4 ASC";
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
    }
}