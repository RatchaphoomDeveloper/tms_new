﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    StylesheetTheme="Aqua" CodeFile="Change2stat.aspx.cs" Inherits="Change2stat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" CausesValidation="False">
        <ClientSideEvents EndCallback="function(s, e){ eval(s.cpPopup);  s.cpPopup='';  }">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtSearch" ClientInstanceName="txtSearch" runat="server" NullText="รหัสคลัง ,ชื่อคลัง"
                                            Width="200px" CssClass="dxeLineBreakFix">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxButton ID="btnSearch" runat="server" ClientInstanceName="btnSearch" SkinID="_search">
                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search;-1'); }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" ClientInstanceName="gvw" runat="server" AutoGenerateColumns="false"
                                nableRowsCache="false" DataSourceID="sdsCHG2ST" KeyFieldName="SKEYID" SkinID="_gvw">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่." Width="1%">
                                        <EditItemTemplate>
                                            &nbsp;
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                        <CellStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="STERMINALID" Caption="STERMINALID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="TERMINAL_NAME" Caption="คลัง">
                                        <EditItemTemplate>
                                            <dx:ASPxLabel ID="lblTERMINAL_NAME" ClientInstanceName="lblTERMINAL_NAME" runat="server"
                                                Text='<%# DataBinder.Eval(Container.DataItem,"TERMINAL_NAME") %>'>
                                            </dx:ASPxLabel>
                                        </EditItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="Change to stat">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn FieldName="GRP1" Caption="น้ำมันใส ,อากาศยาน ,<br>Ethenal ,B100"
                                                Width="15%">
                                                <EditItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP1_MIN2STAT" ClientInstanceName="txtGRP1_MIN2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP1_MIN2STAT") %>'
                                                                    NullText="Warnning" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP1_MAX2STAT" ClientInstanceName="txtGRP1_MAX2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP1_MAX2STAT") %>'
                                                                    NullText="Normal" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP1_MIN2STAT" Caption="GRP1_MIN2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP1_MAX2STAT" Caption="GRP1_MAX2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP2" Caption="น้ำมันเตา" Width="15%">
                                                <EditItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP2_MIN2STAT" ClientInstanceName="txtGRP2_MIN2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP2_MIN2STAT") %>'
                                                                    NullText="Warnning" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP2_MAX2STAT" ClientInstanceName="txtGRP2_MAX2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP2_MAX2STAT") %>'
                                                                    NullText="Normal" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP2_MIN2STAT" Caption="GRP2_MIN2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP2_MAX2STAT" Caption="GRP2_MAX2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP3" Caption="LPG" Width="15%">
                                                <EditItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP3_MIN2STAT" ClientInstanceName="txtGRP3_MIN2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP3_MIN2STAT") %>'
                                                                    NullText="Warnning" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP3_MAX2STAT" ClientInstanceName="txtGRP3_MAX2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP3_MAX2STAT") %>'
                                                                    NullText="Normal" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP3_MIN2STAT" Caption="GRP3_MIN2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP3_MAX2STAT" Caption="GRP3_MAX2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP4" Caption="อินโดจีน" Width="15%">
                                                <EditItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP4_MIN2STAT" ClientInstanceName="txtGRP4_MIN2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP4_MIN2STAT") %>'
                                                                    NullText="Warnning" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP4_MAX2STAT" ClientInstanceName="txtGRP4_MAX2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP4_MAX2STAT") %>'
                                                                    NullText="Normal" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP4_MIN2STAT" Caption="GRP4_MIN2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP4_MAX2STAT" Caption="GRP4_MAX2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP5" Caption="ยางมะตอย" Width="15%">
                                                <EditItemTemplate>
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP5_MIN2STAT" ClientInstanceName="txtGRP5_MIN2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP5_MIN2STAT") %>'
                                                                    NullText="Warnning" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td>
                                                                -
                                                            </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtGRP5_MAX2STAT" ClientInstanceName="txtGRP5_MAX2STAT" runat="server"
                                                                    Width="50px" Text='<%# DataBinder.Eval(Container.DataItem,"GRP5_MAX2STAT") %>'
                                                                    NullText="Normal" CssClass="dxeLineBreakFix" MaxLength="10">
                                                                    <ValidationSettings ValidationGroup="add" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip"
                                                                        Display="Dynamic">
                                                                        <RegularExpression ValidationExpression="\d{0,}" ErrorText="ตัวเลขเท่านั้น" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP5_MIN2STAT" Caption="GRP5_MIN2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="GRP5_MAX2STAT" Caption="GRP5_MAX2STAT" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataColumn Width="5%" Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center"
                                                CellStyle-HorizontalAlign="Center">
                                                <DataItemTemplate>
                                                    <dx:ASPxButton runat="server" ID="btnEdit" SkinID="_edit">
                                                        <ClientSideEvents Click="function(s,e){ gvw.PerformCallback('startedit;'+s.name.split('btnEdit')[1].replace('_','')); }" />
                                                    </dx:ASPxButton>
                                                </DataItemTemplate>
                                                <EditItemTemplate>
                                                    <dx:ASPxButton runat="server" ID="btnUpdate" SkinID="_update" AutoPostBack="false"
                                                        CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false;  gvw.PerformCallback('updateedit;'+s.name.split('btnUpdate')[1].replace('_','')); }" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton runat="server" ID="btnCancel" SkinID="_cancelth" AutoPostBack="false"
                                                        CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e){gvw.CancelEdit(); }" />
                                                    </dx:ASPxButton>
                                                </EditItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                </Columns>
                                <SettingsPager PageSize="30">
                                </SettingsPager>
                                <SettingsEditing Mode="Inline" />
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sdsCHG2ST" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                CancelSelectOnNullParameter="False" CacheKeyDependency="ckdContract" SelectCommand="SELECT TBL.STERMINALID SKEYID, TBL.STERMINALID||' - '||TBL.STERMINALNAME TERMINAL_NAME, TBL.STERMINALID , TBL.STERMINALNAME 
,GRP1_MIN2STAT||' - '||GRP1_MAX2STAT GRP1  ,GRP1_MIN2STAT ,GRP1_MAX2STAT
,GRP2_MIN2STAT||' - '||GRP2_MAX2STAT GRP2  ,GRP2_MIN2STAT ,GRP2_MAX2STAT
,GRP3_MIN2STAT||' - '||GRP3_MAX2STAT GRP3  ,GRP3_MIN2STAT ,GRP3_MAX2STAT
,GRP4_MIN2STAT||' - '||GRP4_MAX2STAT GRP4  ,GRP4_MIN2STAT ,GRP4_MAX2STAT
,GRP5_MIN2STAT||' - '||GRP5_MAX2STAT GRP5  ,GRP5_MIN2STAT ,GRP5_MAX2STAT 
FROM (
    SELECT TERM.STERMINALID,TERM.SABBREVIATION STERMINALNAME   
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='1' THEN (CNG2ST.MIN2STAT) ELSE null END) GRP1_MIN2STAT
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='1' THEN (CNG2ST.MAX2STAT) ELSE null END) GRP1_MAX2STAT 
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='2' THEN (CNG2ST.MIN2STAT) ELSE null END) GRP2_MIN2STAT
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='2' THEN (CNG2ST.MAX2STAT) ELSE null END) GRP2_MAX2STAT 
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='3' THEN (CNG2ST.MIN2STAT) ELSE null END) GRP3_MIN2STAT
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='3' THEN (CNG2ST.MAX2STAT) ELSE null END) GRP3_MAX2STAT 
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='4' THEN (CNG2ST.MIN2STAT) ELSE null END) GRP4_MIN2STAT
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='4' THEN (CNG2ST.MAX2STAT) ELSE null END) GRP4_MAX2STAT 
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='5' THEN (CNG2ST.MIN2STAT) ELSE null END) GRP5_MIN2STAT
    ,MAX(CASE WHEN CNG2ST.CPRODUCT_GROUP='5' THEN (CNG2ST.MAX2STAT) ELSE null END) GRP5_MAX2STAT 
    FROM TTERMINAL TERM
    LEFT JOIN TChange2STAT CNG2ST ON TERM.STERMINALID=CNG2ST.STERMINALID
    WHERE SUBSTR(TERM.STERMINALID,0,1)  IN ('5','8' )  AND NVL(TERM.CACTIVE,'1')='1' AND NVL(CNG2ST.CACTIVE,'1')='1'
    GROUP BY TERM.STERMINALID,TERM.SABBREVIATION  
) TBL
WHERE TBL.STERMINALID||' '||TBL.STERMINALNAME LIKE '%'|| NVL( :keyword ,TBL.STERMINALID||' '||TBL.STERMINALNAME )||'%' ">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtSearch" Name="keyword" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
