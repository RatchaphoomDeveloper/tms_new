﻿<%@ Page Title="" MasterPageFile="~/Mp1.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_fifo_User_regis_add.aspx.cs" Inherits="fifo_User_regis_add"
    StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td width="18%" bgcolor="#FFFFFF">
                            ลงคิวเข้ารับงานประจำวันที่
                        </td>
                        <td width="30%">
                            <dx:ASPxComboBox ID="cboDate" runat="server" Width="150px">
                                <ClientSideEvents SelectedIndexChanged="function (s, e) {xcpn.PerformCallback('listgrid');}" />
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณาเลือกวันที่"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <dx:ASPxTextBox ID="txtVendorID" runat="server" ClientVisible="true" Width="50px">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtUserID" runat="server" ClientVisible="False" Width="50px">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="16%" align="left" bgcolor="#FFFFFF" class="style21">
                            &nbsp;
                        </td>
                        <td width="36%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="text-align: left">ทะเบียนรถ <font color="#ff0000">*</font></span>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboHeadRegist" runat="server" ClientInstanceName="cboHeadRegist"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="150px">
                                <ClientSideEvents SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVenderName.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORNAME'));cboTrailerRegist.PerformCallback();cboTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));cmbPersonalNo.PerformCallback();
                                txtContractNO.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTNO'));txtContractID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID'));cbxOrganiz.PerformCallback();cbxOrganiz.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STERMINALID'))}else{s.SetValue('');} }" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสผู้ประกอบการ" FieldName="STRANSPORTID" Width="100px" />
                                    <dx:ListBoxColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="100px" />
                                    <dx:ListBoxColumn Caption="ชื่อสัญญา" FieldName="SCONTRACTNO" Width="100px" />
                                    <dx:ListBoxColumn Caption="คลังต้นทาง" FieldName="STERMINALID" Width="100px" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            ทะเบียนท้าย
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboTrailerRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboTrailerRegist"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboTrailerRegist_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboTrailerRegist_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="STRAILERREGISTERNO" Width="150px">
                                <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' == '-1')s.SetValue(''); }">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                </Columns>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            เลขที่สัญญา <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtContractNO" runat="server" ClientEnabled="False" 
                                ClientInstanceName="txtContractNO" Width="200px">
                                <Border BorderColor="#CCCCCC" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtContractID" runat="server" 
                                ClientInstanceName="txtContractID" ClientVisible="False" Width="10px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            ชื่อคลังต้นทาง
                        <font color="#ff0000">*</font></td>
                        <td>
                            <dx:ASPxComboBox ID="cbxOrganiz" runat="server" CallbackPageSize="10" ClientInstanceName="cbxOrganiz"
                                EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="150px">
                                 <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' == '-1')s.SetValue(''); }">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" Width="80px" />
                                    <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" Width="150px" />
                                </Columns>
                                  <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุคลังต้นทาง" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            เลขบัตรประชาชน พขร. <font color="#ff0000">*</font>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientInstanceName="cmbPersonalNo"
                                EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="SPERSONELNO" Width="220px">
                                <ClientSideEvents SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){hideEmployeeID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SEMPLOYEEID')) ;txtFullname.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('FULLNAME')) ;txtTel.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STEL'));txtDRIVERNO.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SDRIVERNO'));}else{s.SetValue('');hideEmployeeID.SetValue('') ;txtFullname.SetValue('') ;txtTel.SetValue('');txtDRIVERNO.SetValue('');} }" />
                                <Columns>
                                    <dx:ListBoxColumn Caption="เลขที่บัตรประชาชน" FieldName="SPERSONELNO" />
                                    <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                    <dx:ListBoxColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL" />
                                    <dx:ListBoxColumn Caption="หมายเลขใบขับขี่" FieldName="SDRIVERNO" />
                                    <dx:ListBoxColumn Caption="เลขที่ประจำตัวพนักงาน" FieldName="SEMPLOYEEID" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณาระบุคลัง" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            ชื่อพนักงานขับรถ
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtFullname" ClientInstanceName="txtFullname" runat="server"
                                Width="150px">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="hideEmployeeID" ClientInstanceName="hideEmployeeID" runat="server"
                                Width="10px" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            หมายเลขโทรศัพท์
                        </td>
                        <td class="style15">
                            <dx:ASPxTextBox ID="txtTel" ClientInstanceName="txtTel" runat="server" Width="150px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            ชื่อบริษัท
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtVenderName" runat="server" ClientInstanceName="txtVenderName"
                                ClientEnabled="false" Width="200px" Border-BorderColor="#cccccc">
                                <Border BorderColor="#CCCCCC"></Border>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            หมายเลขใบขับขี่
                        </td>
                        <td class="style15">
                            <dx:ASPxTextBox ID="txtDRIVERNO" runat="server" ClientEnabled="False" ClientInstanceName="txtDRIVERNO"
                                Width="150px" Border-BorderColor="#cccccc">
                                <Border BorderColor="#CCCCCC"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                Text="ลงคิวเข้ารับงาน" Width="100px">
                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                Text="ปิด" Width="100px">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); xcpn.PerformCallback('ListPage'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwTopicQ"
                                KeyFieldName="NID" SkinID="_gvw" Style="margin-top: 0px" Width="100%" DataSourceID="sds">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ลำดับ" FieldName="NNO" VisibleIndex="1" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คลังต้นทาง" FieldName="PLANT" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว-ท้าย)" FieldName="dd" VisibleIndex="2"
                                        Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblHead" runat="server" Text='<%# Eval("SHEADREGISTERNO") %>'>
                                            </dx:ASPxLabel>
                                            -
                                            <dx:ASPxLabel ID="lblTrailer" runat="server" Text='<%# Eval("STRAILERREGISTERNO") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                         <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขบัตรประชาชน" FieldName="SPERSONALNO" VisibleIndex="3"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อพนักงานขับรถ" FieldName="SEMPLOYEENAME" VisibleIndex="4"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                         <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL" VisibleIndex="5"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เวลาลงคิว" FieldName="dd" VisibleIndex="6" Width="18%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblDate1" runat="server" Text='<%# Eval("DDATE", "{0:dd/MM/yyyy เวลา HH:mm น.}") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsBehavior AllowSort="False" />
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT FIFO.NID,FIFO. NNO,FIFO. SHEADREGISTERNO,FIFO. STRAILERREGISTERNO,FIFO. SPERSONALNO,FIFO. SEMPLOYEENAME,FIFO. STEL,FIFO. DDATE ,FIFO.STERMINAL||'-'||PLANT.SABBREVIATION PLANT FROM TFIFO FIFO LEFT JOIN TTERMINAL PLANT ON FIFO.STERMINAL= PLANT.STERMINALID WHERE NVL(FIFO.CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = to_char(sysdate,'dd/MM/yyyy') AND SVENDORID = :SVENDORID ORDER BY NNO DESC">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="txtVendorID" Name="SVENDORID" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
