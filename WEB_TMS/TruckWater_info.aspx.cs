﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;

public partial class TruckWater_info : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        gvwTruck.DataBinding += new EventHandler(gvwTruck_DataBinding);
        gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        gvwTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        gvwTruck.PageIndexChanged += new EventHandler(gvwTruck_PageIndexChanged);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        if (!IsPostBack)
        {
            bool checkUrl = false;
            if (Permissions("53"))
                checkUrl = true;
            if (!checkUrl) { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        gvwTruck.DataBind();
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SEARCH":
                gvwTruck.DataBind();
                break;
            case "SEARCH_CANCEL":
                txtsHeadRegisterNo.Text = "";
                txtsTrailerRegisterNo.Text = "";
                txtsCHASIS.Text = "";
                cmbsCarTypeID.SelectedIndex = 0;
                gvwTruck.DataBind();
                break;
        }
    }
    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "STARTEDIT":
                break;
            case "VIEW":
                dynamic dy_data = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "RTRUCKID", "SCARTYPEID", "SCHASIS", "RCHASIS");
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("View&" + dy_data[0] + "&" + dy_data[1] + "&" + dy_data[2]))
                    , sUrl = "TruckWater.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
            case "EDIT":
                dynamic edy_data = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "RTRUCKID", "SCARTYPEID", "SCHASIS", "RCHASIS");
                string eEncrypt = Server.UrlEncode(STCrypt.Encrypt("Edit&" + edy_data[0] + "&" + edy_data[1] + "&" + edy_data[2]))
                    , eUrl = "TruckWater.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = eUrl + eEncrypt;
                break;
            case "VIEWCONTRACT":
                dynamic dydata = gvwTruck.GetRowValues(visibleindex, "SCONTRACTID", "SVENDORID");
                string vEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dydata[0] + "&" + dydata[1]))
                    , vUrl = "contract_add.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = vUrl + vEncrypt;
                break;
        }
    }
    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                if (Convert.ToInt32(e.GetValue("LDUPDATE")) == 0)
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
                e.Row.BackColor.GetBrightness();
                break;
        }
    }
    protected void gvwTruck_PageIndexChanged(object sender, EventArgs e)
    {
        gvwTruck.DataBind();
    }
    protected void gvwTruck_DataBinding(object sender, EventArgs e)
    {
        DataView dv = (DataView)sdsTruck.Select(new DataSourceSelectArguments());
        gvwTruck.DataSource = dv.ToTable();
    }
    private bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            chkurl = true;

                            break;

                        case "2":
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
}