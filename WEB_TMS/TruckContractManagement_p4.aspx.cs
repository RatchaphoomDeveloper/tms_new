﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class TruckContractManagement_p4 : System.Web.UI.Page
{
    const string UploadContractDirectory = @"UploadFile\Contract\Truck\";
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        gvwTruck.DataBinding += new EventHandler(gvwTruck_DataBinding);
        gvwTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        gvwContractTruck.DataBinding += new EventHandler(gvwContractTruck_DataBinding);
        gvwContractTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContractTruck_AfterPerformCallback);
        gvwContractTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwContractTruck_HtmlRowPrepared);
        gvwSignOff.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwSignOff_AfterPerformCallback);
        gvwEntrance.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwEntrance_AfterPerformCallback);
        #endregion
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        { ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }

        if (!IsPostBack)
        {
            ClearSession();
            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                Session["CMODE"] = "" + QueryString[0];
                Session["CCONTRACTID"] = "" + QueryString[1];
                Session["CVENDORID"] = "" + QueryString[2];
            }
            PreparedData();
        }
    }

    protected void xcpnMain_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SAVECHANGE":
                SAVECHANGED();
                break;
            case "CANCEL":
                ClearSession();
                PreparedData();
                break;
        }
    }

    #region GridView
    protected void gvwTruck_DataBinding(object sender, EventArgs e)
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        gvwTruck.DataSource = CommonFunction.ArrayDataRowToDataTable(dtTruck, dtTruck.Select("CHANGED<>'OUT'", "TRID ASC"));
    }
    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "MOVEBACK":
                MOVEBACK(visibleindex);
                break;
            case "BINDGRID":
                if (Session["dtTRUCK"] != null)
                    gvwTruck.DataBind();
                break;
        }
    }
    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                if (e.GetValue("CHANGED") + "" == "IN" || e.GetValue("CHANGED") + "" == "BACK")
                {
                    if (e.GetValue("CHANGED") + "" == "BACK")
                    {
                        ASPxButton btnMoveBack = (ASPxButton)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnMoveBack");
                        btnMoveBack.ClientEnabled = false;
                    }
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
                    e.Row.BackColor.GetBrightness();
                }
                break;
        }
    }

    protected void gvwContractTruck_DataBinding(object sender, EventArgs e)
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        gvwContractTruck.DataSource = CommonFunction.ArrayDataRowToDataTable(dtTruck, dtTruck.Select("CSTANDBY='Y' AND CHANGED<>'IN'", "TRID ASC"));
    }
    protected void gvwContractTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        dynamic dydata = gvwContractTruck.GetRowValues(visibleindex, "TRID", "SCONTRACTID", "REFCONT");
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "VIEWDOC":
                string sSysFileName = dtTruck.Select("TRID='" + dydata[0] + "'")[0]["SSYSFILENAME"] + "";
                string CONID = (dydata[2] + "" != "" ? dydata[2] + "" : dydata[1] + "");
                gvwContractTruck.JSProperties["cpRedirectOpen"] = @"openFile.aspx?str=" + UploadContractDirectory + CONID + @"\" + sSysFileName;
                break;
            case "DELDOC":
                foreach (DataRow dr in dtTruck.Select("TRID='" + dydata[0] + "'"))
                {
                    dr.BeginEdit();
                    dr["SFILENAME"] = "";
                    dr["SSYSFILENAME"] = "";
                    dr.EndEdit();
                }
                Session["dtTRUCK"] = dtTruck;
                break;
            case "MOVEOUT":
                MOVEOUT(visibleindex);
                break;
            case "MOVEIN":
                MOVEIN(visibleindex);
                break;
            case "UPDATE":
                UPDATE(visibleindex);
                break;
        }
    }
    protected void gvwContractTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType.ToString().ToLower())
        {
            case "data":
                ASPxComboBox cmbContract = (ASPxComboBox)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "cmbContract");
                ASPxDateEdit dteDBEGIN = (ASPxDateEdit)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "dteDBEGIN");
                ASPxDateEdit dteDEND = (ASPxDateEdit)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "dteDEND");
                ASPxUploadControl ulcContractDoc = (ASPxUploadControl)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "ulcContractDoc");
                ASPxButton btnUpload = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnUpload");
                ASPxButton btnView = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnView");
                ASPxButton btnDel = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDel");
                ASPxTextBox txtFlag = (ASPxTextBox)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "txtFlag");
                ASPxMemo memRemark = (ASPxMemo)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "memRemark");
                ASPxButton btnMoveOut = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnMoveOut");
                ASPxButton btnMoveIn = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnMoveIn");
                ASPxButton btnUpdate = (ASPxButton)gvwContractTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnUpdate");

                cmbContract.ClientInstanceName = "cmbContract_" + e.VisibleIndex;
                dteDBEGIN.ClientInstanceName = "dteDBEGIN_" + e.VisibleIndex;
                dteDEND.ClientInstanceName = "dteDEND_" + e.VisibleIndex;
                ulcContractDoc.ClientInstanceName = "ulcContractDoc_" + e.VisibleIndex;
                btnUpload.ClientInstanceName = "btnUpload_" + e.VisibleIndex;
                btnView.ClientInstanceName = "btnView_" + e.VisibleIndex;
                btnDel.ClientInstanceName = "btnDel_" + e.VisibleIndex;
                txtFlag.ClientInstanceName = "txtFlag_" + e.VisibleIndex;
                btnUpdate.ClientInstanceName = "btnUpdate_" + e.VisibleIndex;

                #region SET ClientSideEvents

                ulcContractDoc.ClientSideEvents.FileUploadComplete = @"function(s, e) { if(e.callbackData =='')
                 {dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{ btnView_" + e.VisibleIndex + ".SetVisible(true); btnDel_" + e.VisibleIndex + ".SetVisible(true); txtFlag_" + e.VisibleIndex + ".SetText(''); btnUpload_" + e.VisibleIndex + ".SetVisible(false); s.SetVisible(false); } } ";

                btnUpload.ClientSideEvents.Click = @"function(s,e){ 
                var msg=''; if(ulcContractDoc_" + e.VisibleIndex + ".GetText()==''){msg+='<br>แนบไฟล์เอกสารย้ายรถ';}  if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}else{ulcContractDoc_" + e.VisibleIndex + ".Upload(); } }";

                btnView.ClientSideEvents.Click = @"function (s, e) { gvwContractTruck.PerformCallback('VIEWDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                if(btnDel_" + e.VisibleIndex + ".GetVisible()==true){setTimeout(function(){ ulcContractDoc_" + e.VisibleIndex + ".SetVisible(false); btnUpload_" + e.VisibleIndex + ".SetVisible(false); btnDel_" + e.VisibleIndex + ".SetVisible(true); btnView_" + e.VisibleIndex + ".SetVisible(true); },3000);}}";

                btnDel.ClientSideEvents.Click = @"function (s, e) { ulcContractDoc_" + e.VisibleIndex + ".SetVisible(true); btnUpload_" + e.VisibleIndex + ".SetVisible(true); btnView_" + e.VisibleIndex + ".SetVisible(false); s.SetVisible(false); " + (e.GetValue("CHANGED") + "" == "OUT" ? " txtFlag_" + e.VisibleIndex + ".SetText('1');" : " gvwContractTruck.PerformCallback('DELDOC;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));") + " }";

                btnMoveOut.ClientSideEvents.Click = @"function(s ,e){ 
                                                        if(cmbContract_" + e.VisibleIndex + @".GetValue() == null) 
                                                        { dxWarningRedirect('แจ้งเตือน','กรุณาระบุเลขที่สัญญา',function(s,e){ cmbContract_" + e.VisibleIndex + @".Focus(); });                                                                return; } 
                                                        if(dteDBEGIN_" + e.VisibleIndex + @".GetValue() == null)
                                                        { dxWarningRedirect('แจ้งเตือน','กรุณาระบุวันที่เริ่มต้นใช้งาน',function(s,e){ dteDBEGIN_" + e.VisibleIndex + @".Focus(); }); return; }
                                                        else if(dteDEND_" + e.VisibleIndex + @".GetValue() == null)
                                                        { dxWarningRedirect('แจ้งเตือน','กรุณาระบุวันที่ใช้งานสิ้นสุด',function(s,e){ dteDEND_" + e.VisibleIndex + @".Focus(); }); return; }
                                                        var vis = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); 
                                                        dxConfirm('แจ้งเตือน','ท่านต้องการย้ายรถไปยังสัญญา '+cmbContract_" + e.VisibleIndex + @".GetText()+' ใช่หรือไม่ ?'
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); gvwContractTruck.PerformCallback('MOVEOUT;'+vis); } 
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); }) 
                                                    }";

                btnMoveIn.ClientSideEvents.Click = @"function(s,e){ var vis = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); 
                                                                    dxConfirm('แจ้งเตือน','ท่านต้องการย้ายรถกลับมายังสัญญาเดิม ใช่หรือไม่ ?'
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); gvwContractTruck.PerformCallback('MOVEIN;'+vis); } 
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); })  }";

                btnUpdate.ClientSideEvents.Click = @"function(s,e){ var vis = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); 
                                                                    dxConfirm('แจ้งเตือน','ท่านต้องการเปลี่ยนแปลงข้อมูล ใช่หรือไม่ ใช่หรือไม่ ?'
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); gvwContractTruck.PerformCallback('UPDATE;'+vis); } 
                                                                   ,function(s,e){ dxPopupConfirm.Hide(); })  }";

                #endregion

                #region CHANGED
                /*CHANGED=NULL เป็นรถในสัญญาและมีการใช้งานในสัญญานี้อยู่
                  CHANGED=IN   เป็นรถที่ถูกโยกย้ายมาจากสัญญาอื่น
                  CHANGED=OUT  เป็นรถในสัญญาและถูกย้ายไปยังสัญญาอื่น*/
                #endregion
                if (e.GetValue("CHANGED") + "" == "OUT")
                {
                    cmbContract.Value = e.GetValue("TOCONTRACTID") + "";
                    cmbContract.ClientEnabled = false;
                    if (e.GetValue("DSTART") + "" != "")
                        dteDBEGIN.Value = Convert.ToDateTime(e.GetValue("DSTART") + "");
                    dteDBEGIN.ClientEnabled = false;
                    if (e.GetValue("DEND") + "" != "")
                        dteDEND.Value = Convert.ToDateTime(e.GetValue("DEND") + "");
                    //dteDEND.ClientEnabled = false;
                    if (e.GetValue("SSYSFILENAME") + "" != "")
                    {
                        ulcContractDoc.ClientVisible = false;
                        btnUpload.ClientVisible = false;
                        btnView.ClientVisible = true;
                        btnDel.ClientVisible = true;
                    }
                    //else
                    //{
                    //    ulcContractDoc.Enabled = false;
                    //    btnUpload.ClientEnabled = false;
                    //}
                    memRemark.Text = e.GetValue("SREMARK") + "";
                    //memRemark.ClientEnabled = false;
                    btnMoveOut.ClientVisible = false;
                    btnUpdate.ClientVisible = true;
                    if (e.GetValue("CSTATUS") + "" != "1")
                        btnMoveIn.ClientVisible = true;

                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
                    e.Row.BackColor.GetBrightness();
                }
                else if (e.GetValue("CHANGED") + "" == "NULL" && e.GetValue("CSTATUS") + "" == "1")
                {
                    cmbContract.ClientEnabled = false;
                    dteDBEGIN.ClientEnabled = false;
                    dteDEND.ClientEnabled = false;
                    ulcContractDoc.Enabled = false;
                    btnUpload.ClientEnabled = false;
                    memRemark.ClientEnabled = false;
                    btnMoveOut.ClientEnabled = false;

                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
                    e.Row.BackColor.GetBrightness();
                }
                break;
        }
    }

    protected void gvwSignOff_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "VIEWDOC":
                dynamic dydata = gvwSignOff.GetRowValues(visibleindex, "SCONTRACTID", "SSYSFILENAME", "SFILENAME", "FLAG");
                gvwSignOff.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + UploadContractDirectory + dydata[3] + @"\" + dydata[1];
                break;
        }
    }

    protected void gvwEntrance_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        if (eArgs.Length < 2) return;
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "VIEWDOC":
                dynamic dydata = gvwEntrance.GetRowValues(visibleindex, "SCONTRACTID", "SSYSFILENAME", "SFILENAME", "FLAG");
                gvwEntrance.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + UploadContractDirectory + dydata[3] + @"\" + dydata[1];
                break;
        }
    }
    #endregion

    #region Upload
    protected void ulcContractDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        ASPxUploadControl ulcContractDoc = (ASPxUploadControl)sender;
        string[] CIName = ulcContractDoc.ClientID.ToString().Split('_');

        int visibleIndex = int.Parse(CIName[CIName.Length - 1]);
        dynamic dydata = gvwContractTruck.GetRowValues(visibleIndex, "TRID", "STRUCKID", "STRAILERID");
        e.CallbackData = UploadFile2Server(e.UploadedFile
            , "TruckContract_" + dydata[1] + (dydata[2] + "" != "" ? "_" + dydata[2] : "") + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
            , UploadContractDirectory + Session["CCONTRACTID"] + "/");

        string[] sFileArray = e.CallbackData.Split('$');
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        foreach (DataRow dr in dtTruck.Select("TRID='" + dydata[0] + "'"))
        {
            dr.BeginEdit();
            dr["SFILENAME"] = sFileArray[1];
            dr["SSYSFILENAME"] = sFileArray[0];
            dr.EndEdit();
        }
        Session["dtTRUCK"] = dtTruck;
    }
    #endregion

    private void MOVEOUT(int vis)
    {
        DataTable dtCon = (DataTable)Session["dtTRUCK"];
        dynamic dydata = gvwContractTruck.GetRowValues(vis, "TRID", "SCONTRACTID", "STRUCKID", "STRAILERID", "CSTANDBY", "REFCONT", "PREVCONT", "NTRUCK", "CHANGED");
        ASPxComboBox cmbContract = (ASPxComboBox)gvwContractTruck.FindRowCellTemplateControl(vis, null, "cmbContract");
        ASPxDateEdit dteDBEGIN = (ASPxDateEdit)gvwContractTruck.FindRowCellTemplateControl(vis, null, "dteDBEGIN");
        ASPxDateEdit dteDEND = (ASPxDateEdit)gvwContractTruck.FindRowCellTemplateControl(vis, null, "dteDEND");
        ASPxMemo memRemark = (ASPxMemo)gvwContractTruck.FindRowCellTemplateControl(vis, null, "memRemark");

        int nTruck = dydata[7] + "" != "" ? int.Parse(dydata[7] + "") : 0;

        #region Policy
        string msg = "";
        if ("" + dydata[1] == "" + cmbContract.Value)//กรณีที่ย้ายไปสัญญาเดิม
            msg = "กรุณาเลือกสัญญาอื่นที่ไม่ใช่สัญญาเดิมหรือสัญญาปัจจุบันของรถคันนี้";
        else
        {
            if (nTruck >= dtCon.Select("SCONTRACTID='" + dydata[1] + "' AND REFCONT IS NULL").Length)//กรณีที่จำนวนรถไม่เพียงพอหากมีการโยกย้ายเกิดขึ้น
                msg += "- ไม่สามารถทำการย้ายรถคันนี้ไปยังสัญญา " + cmbContract.Text + " ได้ เนื่องจากจำนวนรถในสัญญาปัจจุบันไม่เพียงพอ<br/>";
        }

        if (msg != "")
        {
            //CommonFunction.SetPopupOnLoad(gvwContractTruck, "dxWarning('แจ้งเตือน','" + msg + "');");
            //return;
        }
        #endregion

        foreach (DataRow dr in dtCon.Select("TRID='" + dydata[0] + "'"))
        {
            dr.BeginEdit();
            dr["TOCONTRACTID"] = cmbContract.Value + "";
            dr["TOCONTRACTNO"] = cmbContract.Text + "";
            dr["DSTART"] = dteDBEGIN.Value + "";
            dr["DEND"] = dteDEND.Value + "";
            dr["SREMARK"] = memRemark.Text + "";
            dr["CREJECT"] = "1";
            dr["CHANGED"] = "OUT";
            dr["CSTATUS"] = "1";
            dr.EndEdit();
        }
        Session["dtTRUCK"] = dtCon;
        gvwContractTruck.DataBind();
        gvwContractTruck.JSProperties["cpBindGrid"] = "BindGrid";
    }
    private void MOVEIN(int vis)
    {
        DataTable dtCon = (DataTable)Session["dtTRUCK"];
        dynamic dydata = gvwContractTruck.GetRowValues(vis, "TRID", "SCONTRACTID", "STRUCKID", "STRAILERID", "CSTANDBY", "REFCONT", "PREVCONT", "NTRUCK", "CHANGED");
        foreach (DataRow dr in dtCon.Select("TRID='" + dydata[0] + "'"))
        {
            dr.BeginEdit();
            dr["CHANGED"] = "NULL";
            dr["CSTATUS"] = "1";
            dr.EndEdit();
        }
        Session["dtTRUCK"] = dtCon;
        gvwContractTruck.DataBind();
        gvwContractTruck.JSProperties["cpBindGrid"] = "BindGrid";
    }
    private void UPDATE(int vis)
    {
        DataTable dtCon = (DataTable)Session["dtTRUCK"];
        dynamic dydata = gvwContractTruck.GetRowValues(vis, "TRID", "SCONTRACTID", "STRUCKID", "STRAILERID", "CSTANDBY", "REFCONT", "PREVCONT", "NTRUCK", "CHANGED");
        ASPxDateEdit dteDEND = (ASPxDateEdit)gvwContractTruck.FindRowCellTemplateControl(vis, null, "dteDEND");
        ASPxMemo memRemark = (ASPxMemo)gvwContractTruck.FindRowCellTemplateControl(vis, null, "memRemark");
        ASPxTextBox txtFlag = (ASPxTextBox)gvwContractTruck.FindRowCellTemplateControl(vis, null, "txtFlag");
        foreach (DataRow dr in dtCon.Select("TRID='" + dydata[0] + "'"))
        {
            dr.BeginEdit();
            dr["DEND"] = dteDEND.Value + "";
            dr["SREMARK"] = memRemark.Text + "";
            if (txtFlag.Text + "" == "1")
            {
                dr["SFILENAME"] = "";
                dr["SSYSFILENAME"] = "";
            }
            dr["CSTATUS"] = "1";
            dr.EndEdit();
        }
        Session["dtTRUCK"] = dtCon;
        gvwContractTruck.DataBind();
    }
    private void MOVEBACK(int vis)
    {
        DataTable dtCon = (DataTable)Session["dtTRUCK"];
        dynamic dydata = gvwTruck.GetRowValues(vis, "TRID", "SCONTRACTID", "REFCONT", "PREVCONT");
        foreach (DataRow dr in dtCon.Select("TRID = '" + dydata[0] + "'"))
        {
            dr.BeginEdit();
            dr["CHANGED"] = "BACK";
            dr["CSTATUS"] = "1";
            dr.EndEdit();
        }
        Session["dtTRUCK"] = dtCon;
        gvwTruck.DataBind();
    }
    private void SAVECHANGED()
    {
        DataTable dtTruck = (DataTable)Session["dtTRUCK"];
        int sChnaged = dtTruck.Select("CSTATUS='1'").Length;
        if (sChnaged < 1)
            return;

        string SCONTRACTID = "" + Session["CCONTRACTID"];
        string SVENDORID = "" + Session["CVENDORID"];

        foreach (DataRow drTruck in dtTruck.Select("CSTATUS='1'"))
        {
            using (OracleConnection con = new OracleConnection(conn))
            {
                con.Open();
                string GenID = "", strSql = "";

                //กรณีที่รถถูกโยกไปช่วยสัญญาอื่นและมีการอัพเดทเพื่อเปลี่ยนแปลงวันที่สิ้นสุด
                if (drTruck["CHANGED"] + "" == "OUT")
                {
                    string sChk = "";
                    sChk = CommonFunction.Get_Value(conn, string.Format("SELECT SCONTRACTID FROM TCONTRACT_TRUCK_TEMP WHERE SCONTRACTID='{0}' AND STRUCKID='{1}'"
                                                                , "" + drTruck["TOCONTRACTID"], "" + drTruck["STRUCKID"]));

                    if (!string.IsNullOrEmpty(sChk))
                    {
                        strSql = "UPDATE TCONTRACT_TRUCK_TEMP SET DEND=:DEND,SFILENAME=:SFILENAME,SSYSFILENAME=:SSYSFILENAME,SREMARK=:SREMARK WHERE SCONTRACTID=:SCONTRACTID AND STRUCKID=:STRUCKID";
                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + drTruck["TOCONTRACTID"];//รหัสสัญญาที่จะทำการโยกรถไป
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + drTruck["STRUCKID"];
                            com.Parameters.Add(":DEND", OracleType.DateTime).Value = Convert.ToDateTime(drTruck["DEND"] + "");
                            com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + drTruck["SFILENAME"];
                            com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + drTruck["SSYSFILENAME"];
                            com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + drTruck["SREMARK"];

                            com.ExecuteNonQuery();
                        }
                        continue;
                    }
                }

                //Back Up ข้อมูลรถในสัญญาปัจจุบัน
                GenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY_TEMP ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

                strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY_TEMP                   (HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,CREJECT,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT,REF_SCONTRACTID,REF_PREVIOUSCONTRACT,DSTART,DEND,SFILENAME,SSYSFILENAME,SREMARK) 
(SELECT :HISTORY_ID,SYSDATE,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,'1',DCREATE,SCREATE,DUPDATE,SUPDATE,SYSDATE,:SREJECT,REF_SCONTRACTID,REF_PREVIOUSCONTRACT,DSTART,DEND,SFILENAME,SSYSFILENAME,SREMARK FROM TCONTRACT_TRUCK_TEMP WHERE STRUCKID=:STRUCKID)";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":HISTORY_ID", OracleType.VarChar).Value = GenID;
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + drTruck["STRUCKID"];
                    //com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = ("" + drTruck["STRAILERID"] != "" ? "" + drTruck["STRAILERID"] : null);
                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = "" + Session["UserID"];
                    com.ExecuteNonQuery();
                }

                if (drTruck["CHANGED"] + "" == "OUT")
                {
                    //ย้ายรถจากสัญญาปัจจุบันไปยังสัญญาอื่น กรณีที่รถคันนี้(สัญญาปัจจุบัน)ไม่ได้ถูกโยกมาจากสัญญาอื่นมาก่อน
                    strSql = @"UPDATE TCONTRACT_TRUCK_TEMP SET SCONTRACTID=:NCONTRACTID,DSTART=:DSTART,DEND=:DEND,DUPDATE=SYSDATE,SUPDATE=:SUPDATE,REF_SCONTRACTID=:SCONTRACTID,REF_PREVIOUSCONTRACT=:SCONTRACTID,SFILENAME=:SFILENAME,SSYSFILENAME=:SSYSFILENAME,SREMARK=:SREMARK WHERE SCONTRACTID=:SCONTRACTID AND STRUCKID=:STRUCKID";


                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = SCONTRACTID;//รหัสสัญญาปัจจุบัน
                        com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + drTruck["TOCONTRACTID"];//รหัสสัญญาที่จะทำการโยกรถไป
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + drTruck["STRUCKID"];
                        //com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = ("" + drTruck["STRAILERID"] != "" ? "" + drTruck["STRAILERID"] : null);
                        com.Parameters.Add(":DSTART", OracleType.DateTime).Value = Convert.ToDateTime(drTruck["DSTART"] + "");
                        com.Parameters.Add(":DEND", OracleType.DateTime).Value = Convert.ToDateTime(drTruck["DEND"] + "");
                        com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + drTruck["SFILENAME"];
                        com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + drTruck["SSYSFILENAME"];
                        com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + drTruck["SREMARK"];
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();

                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ย้ายรถจากสัญญาอื่นกลับมายังต้นสัญญา(สัญญาเดิม)
                    strSql = @"UPDATE TCONTRACT_TRUCK_TEMP SET SCONTRACTID=:NCONTRACTID,REF_SCONTRACTID=null,REF_PREVIOUSCONTRACT=null,DSTART=null,DEND=null,CREJECT=null,SFILENAME=null,SSYSFILENAME=null,SREMARK=null,DUPDATE=SYSDATE,SUPDATE=:SUPDATE WHERE SCONTRACTID=:SCONTRACTID AND STRUCKID=:STRUCKID";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + drTruck["SCONTRACTID"];
                        com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + drTruck["REFCONT"];
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + drTruck["STRUCKID"];
                        //com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = ("" + drTruck["STRAILERID"] != "" ? "" + drTruck["STRAILERID"] : null);
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();

                        com.ExecuteNonQuery();
                    }
                }

                //เก็บประวัติข้อมูลรถก่อนการเปลี่ยนแปลงรหัสสัญญา
                strSql = @"INSERT INTO TTRUCK_HIS
(STRUCKID,NITEM,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE) 

(SELECT STRUCKID,NVL((SELECT MAX(NITEM)+1 FROM TTRUCK_HIS WHERE STRUCKID=T.STRUCKID),1) NITEM
,SHEADREGISTERNO,SCARTYPEID,NSLOT,SHEADID,STRAILERID,STRAILERREGISTERNO,STRANSPORTID,STRANSPORTTYPE,SCAR_NUM,SENGINE,SCHASIS,SOWNER_NAME,SCONTRACTID,SCONTRACTTYPE
,STERMINALID,SREGION,SBRAND,SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NCALC_WEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,NTANK_HIGH_HEAD,NTANK_HIGH_TAIL
,SLOADING_METHOD,DREGISTER,SSTICKERNO,SPROD_GRP,SPERSONID,SDRIVERNAME,SBLACKLISTTYPE,DBLACKLIST,CBLACKLIST,CACTIVE,STATUS_REMARK,SREMARK,DPREV_SERV,DNEXT_SERV,SPREV_REQ_ID
,SLAST_REQ_ID,DLAST_SERV,DLAST_STAT,DMCMP_DATE,DCERTIFICATE,DCLOSED,DAPPROVER,DCREATE,SCREATE,DUPDATE,SUPDATE,SCOMPARTMENT,DBLACKLIST2,NTANK_THICK_HEAD,NTANK_THICK_BODY
,NTANK_THICK_TAIL,STANK_ATTRIBUTE,DCHECK,DNEXT_CHECK_DATE,CRESULT,SCERT_NO,SCHECK_DETAIL,CSTATUS,CACCIDENT,CHOLD,DWATEREXPIRE,CMIG,DSIGNIN,POWERMOVER,NSHAFTDRIVEN,PUMPPOWER
,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,BLACKLIST_CAUSE
FROM TTRUCK T WHERE STRUCKID IN (:STRUCK_ID,:STRAILER_ID))";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCK_ID", OracleType.VarChar).Value = drTruck["STRUCKID"] + "";
                    com.Parameters.Add(":STRAILER_ID", OracleType.VarChar).Value = drTruck["STRAILERID"] + "";
                    com.ExecuteNonQuery();
                }

                //อัพเดทรหัสสัญญาในข้อมูลรถ TTRUCK
                strSql = @"UPDATE TTRUCK SET SCONTRACTID=:SCONTRACTID,DUPDATE=SYSDATE,SUPDATE=:SUPDATE WHERE STRUCKID IN (:STRUCKID,:STRAILERID)";

                using (OracleCommand com = new OracleCommand(strSql, con))
                {
                    com.Parameters.Clear();
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = drTruck["STRUCKID"] + "";
                    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = drTruck["STRAILERID"] + "";
                    if (drTruck["CHANGED"] + "" == "OUT")
                        com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = drTruck["TOCONTRACTID"] + "";
                    else if (drTruck["CHANGED"] + "" == "NULL")
                        com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = SCONTRACTID;
                    else
                        com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = drTruck["REFCONT"] + "";
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();

                    com.ExecuteNonQuery();
                }
            }
        }
        ClearSession();
        string cEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + SCONTRACTID + "&" + SVENDORID))
          , cUrl = "TruckContractManagement_p4.aspx?str=";
        xcpnMain.JSProperties["cpRedirectTo"] = cUrl + cEncrypt;
    }
    private void PreparedData()
    {
        Cache.Remove(sdsContractTRUCK.CacheKeyDependency);
        Cache[sdsContractTRUCK.CacheKeyDependency] = new object();
        DataView dv = (DataView)sdsContractTRUCK.Select(new DataSourceSelectArguments());
        Session["dtTRUCK"] = dv.ToTable();
        gvwTruck.DataBind();
        gvwContractTruck.DataBind();

        Cache.Remove(sdsSignOff.CacheKeyDependency);
        Cache[sdsSignOff.CacheKeyDependency] = new object();
        sdsSignOff.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsSignOff.DataBind();

        Cache.Remove(sdsEntrance.CacheKeyDependency);
        Cache[sdsEntrance.CacheKeyDependency] = new object();
        sdsEntrance.Select(new System.Web.UI.DataSourceSelectArguments());
        sdsEntrance.DataBind();
    }
    private void ClearSession()
    {
        string[] Sesarr = new string[] { "dtTRUCK" };
        foreach (string arr in Sesarr)
            Session.Remove(arr);
    }
    private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
        FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
        string ResultFileName = ServerMapPath + File.Name;
        string sPath = Path.GetDirectoryName(ResultFileName)
                , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                , sFileType = Path.GetExtension(ResultFileName);

        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(ServerMapPath))
            {
                Directory.CreateDirectory(ServerMapPath);
            }
            #endregion
            string fileName = (GenFileName + "" + sFileType.Trim());
            ful.SaveAs(ServerMapPath + fileName);
            //LogUser("1", "I", "อัพโหลดไฟล์(" + uploadmode + ") หน้ายืนยันรถตามสัญญา", "");///แก้ไขให้ระบบเก็บLoglว่าอัพโหลดExcel||Picture

            return fileName + "$" + sFileName.Replace("$", "") + sFileType;
        }
        else
            return "$";
    }
}