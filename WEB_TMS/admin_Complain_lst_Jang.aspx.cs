﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;

public partial class admin_Complain_lst_Jang : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            bool chkurl = false;
            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "21")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;

                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Session["oSSERVICEID"] = null;

            LogUser("21", "R", "เปิดดูข้อมูลหน้า รับเรื่องร้องเรียน", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    BindFirstData();
        //}
        //else
        //{
        BindData();
        //}
        lblCarCount.Text = ((DataView)sds.Select(DataSourceSelectArguments.Empty)).ToTable().Rows.Count + "";

    }

    //กด แสดงเลข Record
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();

                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "SSERVICEID")
                    .Cast<object[]>()
                    .Select(s => new { ID1 = s[0].ToString(), SSERVICEID = s[1].ToString() });

                //foreach (var l in ld)
                //{
                //    Session["delSSERVICEID"] = l.SSERVICEID;
                //    sds.Delete();

                //}
                string delid = "";
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    foreach (var l in ld)
                    {
                        Session["delSSERVICEID"] = l.SSERVICEID;
                        sds.Delete();

                        delid += l.SSERVICEID + ",";

                        string strsql = "SELECT * FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = " + l.SSERVICEID;
                        DataTable dt = new DataTable();
                        dt = CommonFunction.Get_Data(con, strsql);

                        if (dt.Rows.Count > 0)
                        {

                            string strdel = "DELETE FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = " + l.SSERVICEID;
                            using (OracleCommand com = new OracleCommand(strdel, con))
                            {
                                com.ExecuteNonQuery();
                            }

                            foreach (DataRow dr in dt.Rows)
                            {

                                string FilePath = dr["SFILEPATH"] + "";

                                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                                {
                                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                                }
                            }
                        }
                    }
                }

                LogUser("21", "D", "ลบข้อมูลหน้า รับเรื่องร้องเรียน รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SSERVICEID");
                string stmID = Convert.ToString(data);

                Session["oSSERVICEID"] = stmID;
                //xcpn.JSProperties["cpRedirectTo"] = "admin_Complain_add_jang.aspx";
                xcpn.JSProperties["cpRedirectOpen"] = "admin_Complain_add_jang.aspx";

                break;

        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_Complain_add.aspx");
    }

    private void BindData()
    {
        string condition = "";

        if ("" + cboStatus.Value == "1")
        {
            condition = "AND ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) > 0";
        }
        else if ("" + cboStatus.Value == "2")
        {
            condition = "AND (ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5')";
        }
        else if ("" + cboStatus.Value == "3")
        {
            condition = "AND ((ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) <= 0) OR ap.CSTATUS = '6' OR ap.CSTATUS = '3')";
        }
        condition = ("" + cboStatus.Value != "") ? " AND (CASE WHEN ap.SAPPEALID IS NULL THEN  nvl(c.CSTATUS,'1')  ELSE '2' END)='" + CommonFunction.ReplaceInjection("" + cboStatus.Value) + "'" : "";
        string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(c.SSERVICEID AS INT) DESC) AS ID1,c.SSERVICEID,c.DDATECOMPLAIN,V.SVENDORNAME,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,T.STOPICNAME,
CASE WHEN c.CCUT = '1' THEN 'ตัดคะแนนแล้ว' ELSE 'ไม่ตัดคะแนน' END AS CUTSCORE,CASE WHEN CAST((c.DCLOSE - SYSDATE) AS INT) <= 0 THEN 0 ELSE CAST((c.DCLOSE - SYSDATE) AS INT) END AS DCLOSE, 

CASE WHEN ap.SAPPEALID IS NULL THEN 
    --(CASE WHEN TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 THEN 
    (
    CASE nvl(c.CSTATUS,'1') 
    WHEN '1' THEN 'ดำเนินการ'
    WHEN '2' THEN 'อุทธรณ์'
    WHEN '3' THEN 'ปิดเรื่อง' 
    END)
     --ELSE 'กำลังดำเนินการ*' END)  
ELSE
    (CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'กำลังอุทธรณ์' 
    ELSE
        (CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'รอปิดเรื่อง'  END) 
    END) 
END  As STATUS
--,THEADCOMPLAIN.SDETAIL SHEADCOMLIAINNAME
,comp.SCOMPLAINTYPENAME SHEADCOMLIAINNAME
 FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID)
 LEFT JOIN (SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ) THEADCOMPLAIN ON NVL(c.SHEADCOMLIAIN,-1)=THEADCOMPLAIN.NID
 LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION )
 LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE='080' 
LEFT JOIN TCOMPLAINTYPE comp ON comp.SCOMPLAINTYPEID =c.SHEADCOMLIAIN
WHERE (c.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR 
c.SEMPLOYEENAME LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' ) AND To_Date(c.DDATECOMPLAIN,'dd/MM/yyyy') BETWEEN To_Date(:dStart,'dd/MM/yyyy') AND To_Date(:dEnd,'dd/MM/yyyy')  " + condition;

        sds.SelectCommand = sql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oSearch", txtSearch.Text);
        sds.SelectParameters.Add("dStart", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
        sds.SelectParameters.Add("dEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));


        sds.DataBind();
        gvw.DataBind();

    }

    //    private void BindFirstData()
    //    {
    //        string condition = "";

    //        condition = "AND ap.SAPPEALID IS NULL AND TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 OR (ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5')";

    //        string sql = @"SELECT ROW_NUMBER () OVER (ORDER BY CAST(c.SSERVICEID AS INT) DESC) AS ID1,c.SSERVICEID,c.DDATECOMPLAIN,V.SVENDORNAME,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,T.STOPICNAME,
    //CASE WHEN c.CCUT = '1' THEN 'ตัดคะแนนแล้ว' ELSE 'ไม่ตัดคะแนน' END AS CUTSCORE,CASE WHEN CAST((c.DCLOSE - SYSDATE) AS INT) <= 0 THEN 0 ELSE CAST((c.DCLOSE - SYSDATE) AS INT) END AS DCLOSE, 
    //
    //CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - c.DCREATE)) > 0 THEN 'ดำเนินการ' ELSE 'ปิดเรื่อง' END  ELSE
    //CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN 'อุทธรณ์' ELSE
    //CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN 'ปิดเรื่อง'  END END END  As STATUS
    //
    // FROM ((TCOMPLAIN c LEFT JOIN TVENDOR_SAP v ON c.SVENDORID = V.SVENDORID)
    // LEFT JOIN TTOPIC t ON c.STOPICID = T.STOPICID AND C.SVERSION = T.SVERSION )
    // LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE='080' WHERE (c.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR 
    //c.SEMPLOYEENAME LIKE '%' || :oSearch || '%' OR V.SVENDORNAME LIKE '%' || :oSearch || '%' ) " + condition;

    //        sds.SelectCommand = sql;
    //        sds.SelectParameters.Clear();
    //        sds.SelectParameters.Add("oSearch", txtSearch.Text);

    //        sds.DataBind();
    //        gvw.DataBind();

    //    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}