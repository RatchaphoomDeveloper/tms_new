﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="service_cost.aspx.cs" Inherits="service_cost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function IsValidNumber() {
            //ตรวจเช็ค keyCode
            if ((event.keyCode >= 48) && (event.keyCode <= 57))
            { }
            else
            { event.returnValue = false; } //หรือ { event.keyCode = 0 ; }
        }


        function Enable() {
            var gvwrow = txtgvwrowcount.GetValue();
            for (var i = 0; i < gvwrow; i++) {
                window["txtStartCapacity_" + i + ""].SetEnabled(true);
                window["txtEndCapacity_" + i + ""].SetEnabled(true);
                window["txtcarcustomer_" + i + ""].SetEnabled(true);
                window["txtcarservice_" + i + ""].SetEnabled(true);
                window["txtcargov_" + i + ""].SetEnabled(true);
                window["txtcarother_" + i + ""].SetEnabled(true);
                window["txtcarcustomerO_" + i + ""].SetEnabled(true);
                window["txtcarserviceO_" + i + ""].SetEnabled(true);
                window["txtcargovO_" + i + ""].SetEnabled(true);
                window["txtcarotherO_" + i + ""].SetEnabled(true);

            }


            //            txtPanValue.SetEnabled(true);
            //            txtSeal.SetEnabled(true);
            //            txtPaintsara.SetEnabled(true);
            //            txtDocument.SetEnabled(true);

            var gvwMs = txtgvwMscount.GetValue();
            for (var i = 0; i < gvwMs; i++) {
                window["txtService_" + i + ""].SetEnabled(true);
            }


            var gvwrow2 = txtgvwohterrowcount.GetValue();
            for (var i = 0; i < gvwrow2; i++) {
                window["txtMenu_" + i + ""].SetEnabled(true);
                window["txtOhterservice_" + i + ""].SetEnabled(true);
                window["txtDescription_" + i + ""].SetEnabled(true);
                window["CheckFrom_" + i + ""].SetEnabled(true);
            }

            txtPan2Capacity_CIN.SetEnabled(true);
        }

        function EnableGvw(GVWROW) {

            var gvwrow = GVWROW;

            for (var i = 0; i < gvwrow; i++) {
                window["txtStartCapacity_" + i + ""].SetEnabled(true);
                window["txtEndCapacity_" + i + ""].SetEnabled(true);
                window["txtcarcustomer_" + i + ""].SetEnabled(true);
                window["txtcarservice_" + i + ""].SetEnabled(true);
                window["txtcargov_" + i + ""].SetEnabled(true);
                window["txtcarother_" + i + ""].SetEnabled(true);
                window["txtcarcustomerO_" + i + ""].SetEnabled(true);
                window["txtcarserviceO_" + i + ""].SetEnabled(true);
                window["txtcargovO_" + i + ""].SetEnabled(true);
                window["txtcarotherO_" + i + ""].SetEnabled(true);
            }
        }

        //        function EnableMs(GVWROW) {

        //            var gvwrow = GVWROW;

        //            for (var i = 0; i < gvwrow; i++) {
        //                window["txtService_" + i + ""].SetEnabled(true);
        //             
        //            }
        //        }

        function EnableOther(GVWROW) {

            var gvwrow = GVWROW;

            for (var i = 0; i < gvwrow; i++) {
                window["txtMenu_" + i + ""].SetEnabled(true);
                window["txtOhterservice_" + i + ""].SetEnabled(true);
                window["txtDescription_" + i + ""].SetEnabled(true);
                window["CheckFrom_" + i + ""].SetEnabled(true);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e)
        {      
            eval(s.cpPopup); 
            s.cpPopup = '';
            if(s.cpRedirectTo != undefined) 
            window.location = s.cpRedirectTo;
            if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen);
             s.cpRedirectOpen = undefined ;
         
             }"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" cellpadding="3" cellspacing="1">
                    <tr>
                        <td align="right">
                            <dx:ASPxButton runat="server" ID="btnEdit" AutoPostBack="false" Text="แก้ไขรายการ">
                                <ClientSideEvents Click="function(s,e){ Enable(); }" />
                            </dx:ASPxButton>
                            <dx:ASPxTextBox runat="server" ID="txtgvwrowcount" ClientInstanceName="txtgvwrowcount"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtgvwohterrowcount" ClientInstanceName="txtgvwohterrowcount"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtgvwMscount" ClientInstanceName="txtgvwMscount"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtIndexgvw" ClientInstanceName="txtIndexgvw"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtIndexgvwService" ClientInstanceName="txtIndexgvwService"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <%-- <dx:ASPxTextBox runat="server" ID="txtChkAddrow" ClientInstanceName="txtChkAddrow"
                                Width="30%">
                            </dx:ASPxTextBox>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%" OnAfterPerformCallback="gvw_AfterPerformCallback"
                                AutoGenerateColumns="false" ClientInstanceName="gvw" KeyFieldName="SERVICE_ID"
                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText">
                                <ClientSideEvents EndCallback="function(s,e){ if(s.cpGvwCount != undefined){  EnableGvw(s.cpGvwCount);}}">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:GridViewBandColumn Caption="ขนาดความจุ (ลิตร)">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="No" Width="7%">
                                                <DataItemTemplate>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxButton runat="server" ID="btnDelete" CssClass="dxeLineBreakFix" EnableDefaultAppearance="False"
                                                                    EnableTheming="False" Cursor="pointer" AutoPostBack="false">
                                                                    <ClientSideEvents Click="function (s, e)
                                                                    {
                                                                        txtIndexgvw.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                                                        dxConfirm('คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่','คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่', function (s, e) { xcpn.PerformCallback('delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));  dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                                                    }" />
                                                                    <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                                    </Image>
                                                                </dx:ASPxButton>
                                                            </td>
                                                            <td>
                                                                <dx:ASPxLabel runat="server" ID="lblNo" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="เริ่ม" Width="11%">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtStartCapacity" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                        ClientInstanceName="txtStartCapacity" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                        Text='<%# DataBinder.Eval(Container.DataItem,"START_CAPACITY") %>' HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="สิ้นสุด" Width="11%">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtEndCapacity" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                        ClientInstanceName="txtEndCapacity" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                        Text='<%# DataBinder.Eval(Container.DataItem,"END_CAPACITY") %>' HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ค่าบริการ (บาท)">
                                        <Columns>
                                            <dx:GridViewBandColumn Caption="รถใหม่">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="รถลูกค้า" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarcustomer" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarcustomer" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carcustomer") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถรับจ้าง" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarservice" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarservice" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carservice") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถราชการ" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcargov" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcargov" DisplayFormatString="#,#;-#,#;0" Width="96%" Text='<%# DataBinder.Eval(Container.DataItem,"cargov") %>'
                                                                HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถอื่นๆ" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarother" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarother" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carother") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <dx:GridViewBandColumn Caption="รถเก่า">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="รถลูกค้า" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarcustomerO" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarcustomerO" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carcustomerO") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถรับจ้าง" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarserviceO" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarserviceO" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carserviceO") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถราชการ" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcargovO" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcargovO" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"cargovO") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="รถอื่นๆ" Width="9%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox runat="server" ID="txtcarotherO" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                ClientInstanceName="txtcarotherO" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                Text='<%# DataBinder.Eval(Container.DataItem,"carotherO") %>' HorizontalAlign="Right">
                                                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            </dx:ASPxTextBox>
                                                        </DataItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="SERVICE_ID" Visible="false">
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="NROWADD" Visible="false">
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn FieldName="NO" Visible="false">
                                                    </dx:GridViewDataColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton runat="server" ID="btnAdd" AutoPostBack="false" SkinID="_addnew">
                                <ClientSideEvents Click="function (s, e) { gvw.PerformCallback('add');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <%--  <tr>
                        <td width="30%" align="center" bgcolor="#B9EAEF">เพิ่มแป้น</td>
                        <td width="70%" align="left">
                            <dx:ASPxTextBox runat="server" ID="txtPanValue" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtPanValue" ClientEnabled="false" DisplayFormatString="#,#;-#,#;0">
                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtchkpan" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtchkpan" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ค่ารับรอง/ตีซีล</td>
                        <td align="left">
                            <dx:ASPxTextBox runat="server" ID="txtSeal" Width="10%" HorizontalAlign="Right" ClientEnabled="false"
                                ClientInstanceName="txtSeal" DisplayFormatString="#,#;-#,#;0">
                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtchkseal" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtchkpan" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ค่าบริการพ่นสาระสำคัญ</td>
                        <td align="left">
                            <dx:ASPxTextBox runat="server" ID="txtPaintsara" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtPaintsara" ClientEnabled="false" DisplayFormatString="#,#;-#,#;0">
                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtchkPaintsara" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtchkPaintsara" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ค่าสำเนาใบรับรองการตรวจ</td>
                        <td align="left">
                            <dx:ASPxTextBox runat="server" ID="txtDocument" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtDocument" ClientEnabled="false" DisplayFormatString="#,#;-#,#;0">
                                <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtchkDocument" Width="10%" HorizontalAlign="Right"
                                ClientInstanceName="txtchkDocument" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="2">
                            <dx:ASPxGridView runat="server" ID="gvwMs" Width="100%" KeyFieldName="SERVICE_ID"
                                Border-BorderColor="White" Paddings-Padding="0">
                                <Columns>
                                    <dx:GridViewDataColumn CellStyle-BackColor="#B9EAEF" FieldName="SERVICE_NAME" Width="30%">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Width="70%">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox runat="server" ID="txtService" CssClass="dxeLineBreakFix" Width="15%"
                                                ClientEnabled="false" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICECHARGE") %>'
                                                DisplayFormatString="#,#;-#,#;0" HorizontalAlign="Right">
                                            </dx:ASPxTextBox>&nbsp;
                                            <dx:ASPxLabel runat="server" ID="lblUnit" CssClass="dxeLineBreakFix" Text='<%# DataBinder.Eval(Container.DataItem,"UNIT") %>'></dx:ASPxLabel>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="SERVICE_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Settings ShowColumnHeaders="false" />
                                <Styles Cell-Border-BorderColor="White" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF" width="30%">งานบริการแก้ไขเพิ่มเติม</td>
                        <td align="left">
                            <table width="100%" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td>
                                        <dx:ASPxGridView runat="server" ID="gvwOhter" Width="100%" KeyFieldName="SERVICE_ID"
                                            OnAfterPerformCallback="gvwOhter_AfterPerformCallback" ClientInstanceName="gvwOhter">
                                            <ClientSideEvents EndCallback="function(s,e){ if(s.cpGvwOhterCount != undefined){  EnableOther(s.cpGvwOhterCount);}}">
                                            </ClientSideEvents>
                                            <Columns>
                                                <dx:GridViewDataColumn Visible="false" FieldName="SERVICE_ID">
                                    
                                                </dx:GridViewDataColumn>
                                                 <dx:GridViewDataColumn FieldName="NROWADD"  Visible="false"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="No" Width="5%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton runat="server" ID="btnDeleteOther" CssClass="dxeLineBreakFix" EnableDefaultAppearance="False"
                                                            EnableTheming="False" Cursor="pointer" AutoPostBack="false">
                                                            <ClientSideEvents Click="function (s, e)
                                                                    {
                                                                        
                                                                        txtIndexgvwService.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                                                        dxConfirm('คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่','คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่', function (s, e) { xcpn.PerformCallback('deleteService;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));  dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} );
                                                                    }" />
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                        <dx:ASPxLabel runat="server" ID="lblNoOther" CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                        
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รายการ" Width="40%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="txtMenu" CssClass="dxeLineBreakFix" Width="96%"
                                                            ClientEnabled="false" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICE_NAME") %>'>
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุชื่อรายการ"
                                                                RequiredField-IsRequired="true" ValidationGroup="add">
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ค่าบริการ (บาท)" Width="15%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="txtOhterservice" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                            DisplayFormatString="#,#;-#,#;0" Width="96%" Text='<%# DataBinder.Eval(Container.DataItem,"SERVICECHARGE") %>'
                                                            HorizontalAlign="Right">
                                                            <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุค่าบริการ"
                                                                RequiredField-IsRequired="true" ValidationGroup="add">
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="หมายเหตุ" Width="30%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="txtDescription" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                            Width="96%" Text='<%# DataBinder.Eval(Container.DataItem,"DESCRIPTION") %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รูปแบบการแสดง" Width="10%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxCheckBox runat="server" ID="CheckFrom" ClientEnabled="false">
                                                        </dx:ASPxCheckBox>
                                                    </DataItemTemplate>
                                                    <%--<propertiescheckedit valuetype="System.String" valuechecked="1" valueunchecked="0" />--%>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxButton runat="server" ID="btnAddother" AutoPostBack="false" SkinID="_addnew">
                                            <ClientSideEvents Click="function (s, e) {if(ASPxClientEdit.ValidateGroup('add')) gvwOhter.PerformCallback('addother');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF">ปริมาตรของแป้นที่ 2 ต่อปริมาตรของแป้นที่ 1</td>
                        <td>
                            <dx:ASPxTextBox runat="server" ID="txtPan2Capacity" ClientEnabled="false" ClientInstanceName="txtPan2Capacity_CIN">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#B9EAEF"></td>
                        <td>
                            <dx:ASPxButton runat="server" ID="btnSubmit" AutoPostBack="false" ClientInstanceName="btnSubmit"
                                SkinID="_submit" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) { if(ASPxClientEdit.ValidateGroup('add')) xcpn.PerformCallback('save'); }" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnCancel" AutoPostBack="false" ClientInstanceName="btnCancel"
                                SkinID="_cancel" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('cancel'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
