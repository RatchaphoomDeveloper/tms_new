﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="KPI_Index.aspx.cs" Inherits="KPI_Index" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="form-horizontal">
            <div class="tab-pane fade active in" id="TabGeneral">
                <br />
                <br />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>
                                <asp:Label ID="lblHeaderTab1" runat="server" Text="ดัชนีชี้วัดประสิทธิภาพการทำงาน (KPI)"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <asp:Table runat="server" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="15%">
                                                    <asp:Button ID="cmdFormula" runat="server" CssClass="btn btn-primary" Visible="false" Text="สูตรการคำนวณ" Width="150px" UseSubmitBehavior="false" OnClick="cmdFormula_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="15%">
                                                    <asp:Button ID="cmdForm" runat="server" CssClass="btn btn-success" Text="ชุดประเมิน KPI" Width="150px" UseSubmitBehavior="false" OnClick="cmdForm_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="15%">
                                                    <asp:Button ID="cmdMappingForm" runat="server" CssClass="btn btn-warning" Text="ตั้งค่าชุดประเมิน KPI" Width="150px" UseSubmitBehavior="false" OnClick="cmdMappingForm_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="15%">
                                                    <asp:Button ID="cmdKPISave" runat="server" CssClass="btn btn-info" Text="บันทึกผลประเมิน KPI" Width="150px" UseSubmitBehavior="false" OnClick="cmdKPISave_Click" />
                                                </asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="15%">
                                                    <asp:Button ID="cmdKPIView" runat="server" CssClass="btn btn-danger" Text="ดูผลการประเมิน KPI" Visible="false" Width="150px" UseSubmitBehavior="false" />
                                                </asp:TableCell>
                                                <asp:TableCell Style="text-align: center" Width="5%">&nbsp;</asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>

                                        <br />


                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer" style="text-align: right">
                            </div>
                        </div>
                        </ContentTemplate>
                </asp:UpdatePanel>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>
                                <asp:Label ID="Label1" runat="server" Text="สรุป"></asp:Label>
                            </div>
                            
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label">ปี</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3" style="padding-top:6px">
                                                <%--<asp:ImageButton ID="imgView2" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer" OnClick="imgView2_Click" />--%>
                                                <asp:LinkButton ID="lblViewYear" runat="server" Text="ผลประเมิน KPI รายปี" OnClick="lblViewYear_Click"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><asp:Label Text="0" runat="server" ID="lblItem" />
                                            </div>
                                            <div class="panel-body">
                                                <div class="row form-group">
                                                    <asp:GridView runat="server" ID="gvKPI"
                                                        Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                        GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false" ShowFooter="true"
                                                        HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                        AllowPaging="false" DataKeyNames="STATUS_VALUE" >
                                                        <Columns>
                                                            <%--<asp:BoundField DataField="ROW_COLOR" Visible="true"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="ROW_COLOR" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRowColor" runat="server" Text='<%# Eval("ROW_COLOR") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatusName" runat="server" Text='<%# Eval("STATUS_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    สรุปสถานะการแจ้งผล KPI
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ม.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth1" runat="server" Text='<%# Eval("MONTH1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView1" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  CommandArgument="1" OnClick="imgView_Click" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ก.พ." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth2" runat="server" Text='<%# Eval("MONTH2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView2" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="2" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="มี.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth3" runat="server" Text='<%# Eval("MONTH3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView3" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="3" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="เม.ย." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth4" runat="server" Text='<%# Eval("MONTH4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView4" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="4" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="พ.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth5" runat="server" Text='<%# Eval("MONTH5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView5" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="5" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="มิ.ย." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth6" runat="server" Text='<%# Eval("MONTH6") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView6" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="6" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ก.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth7" runat="server" Text='<%# Eval("MONTH7") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView7" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="7" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ส.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth8" runat="server" Text='<%# Eval("MONTH8") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView8" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="8" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ก.ย." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth9" runat="server" Text='<%# Eval("MONTH9") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView9" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="9" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ต.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth10" runat="server" Text='<%# Eval("MONTH10") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView10" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="10" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="พ.ย." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth11" runat="server" Text='<%# Eval("MONTH11") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView11" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="11" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ธ.ค." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMonth12" runat="server" Text='<%# Eval("MONTH12") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:ImageButton ID="imgView12" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                        Height="25px" Style="cursor: pointer"  OnClick="imgView_Click" CommandArgument="12" />
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>

                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="300" />
                                                        <PagerStyle CssClass="pagination-ys" />
                                                    </asp:GridView>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
