﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using TMS_BLL.Master;

public partial class KPI_Formula_AddEdit : PageBase
{
    #region + View State +
    private int FormulaID
    {
        get
        {
            if ((int)ViewState["FormulaID"] != null)
                return (int)ViewState["FormulaID"];
            else
                return 0;
        }
        set
        {
            ViewState["FormulaID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            if (Session["chkurl"] != null && Session["chkurl"] == "1")
            {
                cmdSave.Visible = false;
            }
        }
    }

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["FORMULA_ID"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                FormulaID = int.Parse(decryptedValue);

                DataTable dt = new DataTable();
                dt = KPIBLL.Instance.KPISelectForEditBLL(FormulaID);

                if (dt.Rows.Count > 0)
                {
                    txtFormulaName.Text = dt.Rows[0]["FORMULA_NAME"].ToString();
                    txtFormulaDetail.Text = dt.Rows[0]["FORMULA_DETAIL"].ToString();
                    txtVariableA.Text = dt.Rows[0]["VARIABLE_NAME"].ToString();
                    txtValueA.Text = dt.Rows[0]["DEFAULT_VALUE"].ToString();

                    txtVariableB.Text = dt.Rows[1]["VARIABLE_NAME"].ToString();
                    txtValueB.Text = dt.Rows[1]["DEFAULT_VALUE"].ToString();

                    txtFrequency.Text = dt.Rows[1]["FREQUENCY"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();

            KPIBLL.Instance.KPISaveBLL(FormulaID, txtVariableA.Text.Trim(), txtValueA.Text.Trim(), txtVariableB.Text.Trim(), txtValueB.Text.Trim(), int.Parse(Session["UserID"].ToString()));

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "KPI_Formula.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave()
    {
        try
        {
            int tmp;

            if ((!string.Equals(txtValueA.Text.Trim(), string.Empty) && (!int.TryParse(txtValueA.Text.Trim(), out tmp))))
                throw new Exception("ค่าเริ่มต้น A ต้องเป็นตัวเลขเท่านั้น");

            if ((!string.Equals(txtValueB.Text.Trim(), string.Empty) && (!int.TryParse(txtValueB.Text.Trim(), out tmp))))
                throw new Exception("ค่าเริ่มต้น B ต้องเป็นตัวเลขเท่านั้น");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Formula.aspx");
    }
}