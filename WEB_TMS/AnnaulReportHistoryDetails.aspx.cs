﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;

public partial class AnnaulReportHistoryDetails : PageBase
{
    string GroupPermission = "01";
    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }
    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }

    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();

            if ((Session["AARN_HID"] == null) || (Session["AARN_YEAR"] == null) || (Session["AARN_SABBREVIATION"] == null))
            {
                btnClose_Click(null, null);
            }
            else
            {
                lblYear.Text = Session["AARN_YEAR"].ToString();
                lblVendorName.Text = Session["AARN_SABBREVIATION"].ToString();
                Session["AARN_YEAR"] = null;
                Session["AARN_SABBREVIATION"] = null;
                QDocID = Session["AARN_HID"].ToString();
                LoadMain();
            }



        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    private void LoadMain()
    {
        try
        {
            string _err = string.Empty;
            SearchCriteria = new FinalGade() { YEAR = string.Empty, VENDORID = string.Empty };
            SearhData = new FinalGradeBLL().GetHistoryDetails(ref _err, QDocID);
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}
    }

    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {

        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("AnnualReportHistory.aspx");
    }
    protected void grvMain_DataBound(object sender, EventArgs e)
    {

    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void btnVendorDownload_Click(object sender, EventArgs e)
    {

    }
    protected void btnExportPdf_Click(object sender, EventArgs e)
    {

    }
}