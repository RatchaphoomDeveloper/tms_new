﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="CarImageAdd.aspx.cs" Inherits="CarImageAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <link href="Javascript/Jquery/plupload/scripts/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css"
        rel="stylesheet" type="text/css" />
    <script src="Javascript/Jquery/plupload/scripts/plupload/plupload.full.js" type="text/javascript"></script>
    <script src="Javascript/Jquery/plupload/scripts/plupload/jquery.plupload.queue/jquery.plupload.queue.js"
        type="text/javascript"></script>
    <script src="Javascript/Jquery/plupload/scripts/ww.jquery.min.js" type="text/javascript"></script>
    <script src="Javascript/Jquery/plupload/scripts/UploadImages.js" type="text/javascript"></script>
    <%--<script src="Javascript/Jquery/jquery.ui.core.js" type="text/javascript"></script>--%>
    <style type="text/css">
        .hide
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" HideContentOnCallback="false"
        CausesValidation="False" OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr id="trtab">
                        <td>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="15%" align="center" bgcolor="#EDEDED">
                                        <dx:ASPxHyperLink ID="hplT1" runat="server" Text="ประวัติการตรวจ" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                            <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT1;')}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="15%" align="center" bgcolor="#EDEDED">
                                        <dx:ASPxHyperLink ID="hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                            <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT2;')}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="15%" align="center" bgcolor="#EDEDED">
                                        <dx:ASPxHyperLink ID="hplT3" runat="server" Text="ผลตรวจลงน้ำ" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                            <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT3;')}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="15%" align="center" bgcolor="#EDEDED">
                                        <dx:ASPxHyperLink ID="hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                            <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="15%" align="center" bgcolor="#00CC33">
                                        <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="15%" align="center" bgcolor="#EDEDED">
                                        <dx:ASPxHyperLink ID="hplT5" runat="server" Text="บันทึกผลปิดงาน" Style="text-decoration: none;
                                            cursor: pointer; color: Blue;">
                                            <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT5;')}" />
                                        </dx:ASPxHyperLink>
                                    </td>
                                    <td width="10%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="containercontent">
                                <div id="Uploader">
                                </div>
                                <div id="ImageContainer" style="display: none">
                                </div>
                                <div id="ImagePreview" style="display: none">
                                    <img id="ImageView" />
                                </div>
                            </div>
                            <asp:Button runat="server" ID="btnClickComplete" OnClick="btnClickComplete_Click"
                                CssClass="hide" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxGridView runat="server" ID="gvwWaiting" Width="80%" ClientInstanceName="gvwWaiting"
                                AutoGenerateColumns="false" KeyFieldName="REQUEST_ID">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="รูป" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"
                                        Width="65%">
                                        <DataItemTemplate>
                                            <img src='<%# Eval("IMG").ToString() %>' alt='' width="200px" height="200px" />
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่อัพโหลด" FieldName="CREATE_DATE" HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption=" " Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" ID="btnEditwaiting" AutoPostBack="false" EnableTheming="false"
                                                EnableDefaultAppearance="False" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) 
                                            { 
                                           
                                               txtIdx.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                             dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการลบรูปใช่หรือไม่', function (s, e) {  gvwWaiting.PerformCallback('delete');gvwWaiting.Refresh();dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                            }"></ClientSideEvents>
                                                <%--<ClientSideEvents Click="function (s, e) { dxConfirm('คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่','คุณต้องการยืนยันการอนุมัติคำใช่หรือไม่',  xcpn.PerformCallback('delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));, function(s, e) {  dxPopupConfirm.Hide();} ) }" />--%>
                                                <Image Url="Images/bin1.png" Height="20px" Width="20px">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="REQUEST_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="CARIMAGE_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="NVERSION" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <dx:ASPxTextBox runat="server" ID="txtIdx" ClientInstanceName="txtIdx" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
