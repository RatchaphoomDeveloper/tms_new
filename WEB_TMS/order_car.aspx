﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="order_car.aspx.cs" Inherits="order_car" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">

        function IsValidNumber() {
            //ตรวจเช็ค keyCode
            if ((event.keyCode >= 48) && (event.keyCode <= 57))
            { }
            else
            { event.returnValue = false; } //หรือ { event.keyCode = 0 ; }
        }

        function Enable() {

            var gvwrow = txtgvwrowcount.GetValue();

            window["txtCapacitycell1"].SetEnabled(true);
            window["txtCapacitycell2"].SetEnabled(true);
            window["txtCapacitycell3"].SetEnabled(true);
            window["txtCapacitycell4"].SetEnabled(true);
            for (var i = 0; i < gvwrow; i++) {

                window["txtcaramonth_" + i + ""].SetEnabled(true);
                window["txtcaramonth2_" + i + ""].SetEnabled(true);
                window["txtsealhit_" + i + ""].SetEnabled(true);
            }
        }

        function EnableGvw(GVWROW) {

            var gvwrow = GVWROW;

            for (var i = 0; i < gvwrow; i++) {
                window["txtCapacitycell1"].SetEnabled(true);
                window["txtCapacitycell2"].SetEnabled(true);
                window["txtCapacitycell3"].SetEnabled(true);
                window["txtCapacitycell4"].SetEnabled(true);
                window["txtcaramonth_" + i + ""].SetEnabled(true);
                window["txtcaramonth2_" + i + ""].SetEnabled(true);
                window["txtsealhit_" + i + ""].SetEnabled(true);
            }
        }

        function CheckDecrease(Value, Order, e) {

            if (Order == 'txt1-2') {
                var data = window["txtCapacitycell1"].GetValue();
                if (data != null && data !== undefined) {

                }
                else {

                }
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e) {  eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined ;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" cellpadding="3" cellspacing="1">
                    <tr>
                        <td align="right">
                            <dx:ASPxButton runat="server" ID="btnEdit" AutoPostBack="false" Text="แก้ไขรายการ">
                                <ClientSideEvents Click="function(s,e){ Enable(); }" />
                            </dx:ASPxButton>
                            <dx:ASPxTextBox runat="server" ID="txtgvwrowcount" ClientInstanceName="txtgvwrowcount"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtIndexgvw" ClientInstanceName="txtIndexgvw"
                                Width="30%" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <%-- <dx:ASPxTextBox runat="server" ID="txtChkAddrow" ClientInstanceName="txtChkAddrow"
                                Width="30%">
                            </dx:ASPxTextBox>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%" OnAfterPerformCallback="gvw_AfterPerformCallback"
                                AutoGenerateColumns="false" ClientInstanceName="gvw" KeyFieldName="SERVICE_ID" SettingsPager-Mode="ShowAllRecords">
                                <ClientSideEvents EndCallback="function(s,e){ if(s.cpGvwCount != undefined){EnableGvw(s.cpGvwCount);}
                                if(s.cpBEGIN_CAP1 != undefined){ txtCapacitycell1.SetText(s.cpBEGIN_CAP1);}
                                if(s.cpEND_CAP1 != undefined){ txtCapacitycell2.SetText(s.cpEND_CAP1);}
                                if(s.cpBEGIN_CAP2 != undefined){ txtCapacitycell3.SetText(s.cpBEGIN_CAP2);}
                                if(s.cpEND_CAP2 != undefined){ txtCapacitycell4.SetText(s.cpEND_CAP2);}
                                }"></ClientSideEvents>
                                <Columns>
                                    <dx:GridViewDataColumn Caption="เงื่อนไขที่" Width="8%">
                                        <DataItemTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <dx:ASPxButton runat="server" ID="btnDelete" CssClass="dxeLineBreakFix" EnableDefaultAppearance="False"
                                                            EnableTheming="False" Cursor="pointer" AutoPostBack="false">
                                                            <ClientSideEvents Click="function (s, e) 
                                                            {
                                                                        txtIndexgvw.SetText(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                                                        dxConfirm('คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่','คุณต้องการยืนยันการลบข้อมูลใช่หรือไม่', function (s, e) { xcpn.PerformCallback('delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));  dxPopupConfirm.Hide(); }, function(s, e) {  dxPopupConfirm.Hide();} )
                                                                
                                                            }" />
                                                            <Image Height="25px" Url="Images/bin1.png" Width="25px">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxLabel runat="server" ID="lblNo" CssClass="dxeLineBreakFix">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewBandColumn Caption="จำนวนรถที่รับได้">
                                        <Columns>
                                            <dx:GridViewDataColumn Width="38%" Caption="data1" CellStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <table cellpadding="0" cellspacing="0" width="96%">
                                                        <tr>
                                                            <td align="center" colspan="4">ช่วงความจุรถบรรทุก / รถสิบล้อ</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="24%">
                                                                <dx:ASPxLabel runat="server" ID="lblCapacity1" Text="ความจุไม่เกิน" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td width="33%">
                                                                <dx:ASPxTextBox runat="server" ID="txtCapacitycell1" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                    ClientInstanceName="txtCapacitycell1" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                    Text="" HorizontalAlign="Right">
                                                                    <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                                        RequiredField-ErrorText="ระบุจำนวน" ValidationGroup="add">
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td width="6%">
                                                                <dx:ASPxLabel runat="server" ID="lblunder" Text=" - " CssClass="dxeLineBreakFix">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td width="33%">
                                                                <dx:ASPxTextBox runat="server" ID="txtCapacitycell2" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                    ClientInstanceName="txtCapacitycell2" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                    Text="" HorizontalAlign="Right">
                                                                    <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" Validation="function(s,e){CheckDecrease(s.GetValue(),'txt1-2',e)}" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                                        RequiredField-ErrorText="ระบุจำนวน" ValidationGroup="add">
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtcaramonth" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                        DisplayFormatString="#,#;-#,#;0" Width="50%" Text='<%# DataBinder.Eval(Container.DataItem,"CAR_AMOUNT1") %>'
                                                        HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Width="38%" Caption="data2" CellStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <table cellpadding="0" cellspacing="0" width="96%">
                                                        <tr>
                                                            <td align="center"  colspan="4">รถกึ่งพ่วง</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="24%">
                                                                <dx:ASPxLabel runat="server" ID="lblCapacity3" Text="ความจุไม่เกิน" CssClass="dxeLineBreakFix">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td width="33%">
                                                                <dx:ASPxTextBox runat="server" ID="txtCapacitycell3" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                    ClientInstanceName="txtCapacitycell3" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                    Text="" HorizontalAlign="Right">
                                                                    <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" Validation="function(s,e){CheckDecrease(s.GetValue(),'txt2-1',e)}" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                                        RequiredField-ErrorText="ระบุจำนวน" ValidationGroup="add">
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td width="6%">
                                                                <dx:ASPxLabel runat="server" ID="lblunder" Text=" - " CssClass="dxeLineBreakFix">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td width="33%">
                                                                <dx:ASPxTextBox runat="server" ID="txtCapacitycell4" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                                    ClientInstanceName="txtCapacitycell4" DisplayFormatString="#,#;-#,#;0" Width="96%"
                                                                    Text="" HorizontalAlign="Right">
                                                                    <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" Validation="function(s,e){CheckDecrease(s.GetValue(),'txt2-2',e)}" />
                                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                                        RequiredField-ErrorText="ระบุจำนวน" ValidationGroup="add">
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtcaramonth2" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                        DisplayFormatString="#,#;-#,#;0" Width="50%" Text='<%# DataBinder.Eval(Container.DataItem,"CAR_AMOUNT2") %>'
                                                        HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="เพิ่มเติมตีซีล" Width="15%">
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox runat="server" ID="txtsealhit" CssClass="dxeLineBreakFix" ClientEnabled="false"
                                                        DisplayFormatString="#,#;-#,#;0" Width="96%" Text='<%# DataBinder.Eval(Container.DataItem,"SEAL_HIT") %>'
                                                        HorizontalAlign="Right">
                                                        <ClientSideEvents KeyPress="function(s,e){IsValidNumber();}" />
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn FieldName="NROWADD" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="NO" Visible="false">
                                    </dx:GridViewDataColumn>
                                    <%-- <dx:GridViewDataColumn FieldName="STYPECAP">
                                    </dx:GridViewDataColumn>--%>
                                </Columns>
                                <SettingsBehavior AllowDragDrop="false" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton runat="server" ID="btnAdd" AutoPostBack="false" SkinID="_addnew">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('add'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <dx:ASPxButton runat="server" ID="btnSubmit" AutoPostBack="false" ClientInstanceName="btnSubmit"
                                SkinID="_submit" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('save'); }" />
                            </dx:ASPxButton>
                            <dx:ASPxButton runat="server" ID="btnCancel" AutoPostBack="false" ClientInstanceName="btnCancel"
                                SkinID="_cancel" CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('cancel'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
