﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Truck_Moving.aspx.cs"
    Inherits="Truck_Moving" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphHeader">
</asp:Content>
<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="cph_Main">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <dx:ASPxCallbackPanel ID="xcpnMain" runat="server" CausesValidation="False" HideContentOnCallback="False"
        ClientInstanceName="xcpnMain" OnCallback="xcpnMain_Callback">
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <%--<td width="9%" align="right">
                            ประเภทรถ : </td>
                        <td width="11%" align="right">
                            <dx:ASPxComboBox ID="cmbTruckType" runat="Server" ClientInstanceName="cmbTruckType" Width="150px">
                                <Items>
                                    <dx:ListEditItem Value="3" Text="SEMI TRAILER" Selected="true" />
                                    <dx:ListEditItem Value="1" Text="FULL TRAILER" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>--%>
                        <td width="80px" align="right">
                            ทะเบียนหัว : </td>
                        <td width="128px" align="right">
                            <dx:ASPxTextBox ID="txtTruckHNo" runat="Server" ClientInstanceName="txtTruckHNo" Width="125px" NullText="กรุณาระบุทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="80px" align="right">
                            ทะเบียนหาง : </td>
                        <td width="128px" align="right">
                            <dx:ASPxTextBox ID="txtTruckTNo" runat="Server" ClientInstanceName="txtTruckTNo" Width="125px" NullText="กรุณาระบุทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="81px" align="right">
                            <dx:ASPxButton ID="btnSearch" runat="Server" ClientInstanceName="btnSearch" SkinID="_search">
                                <ClientSideEvents Click="function(s,e){ xcpnMain.PerformCallback('SEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td width="105px" align="right">
                            <dx:ASPxButton ID="btnClearSearch" ClientInstanceName="btnClearSearch" runat="server" SkinID="_clearsearch">
                                <ClientSideEvents Click="function(s,e){ xcpnMain.PerformCallback('SEARCH_CANCEL;'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <hr />
                <dx:ASPxGridView ID="gvwTruckMove" runat="Server" ClientInstanceName="gvwTruckMove" SkinID="_gvw" AutoGenerateColumns="False"
                    EnableCallBacks="true" KeyFieldName="TRID" OnPageIndexChanged="gvwTruckMove_PageIndexChanged" OnDataBinding="gvwTruckMove_DataBinding">
                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup=''; if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="TRID" Visible="false">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Width="6%">
                            <DataItemTemplate>
                                <dx:ASPxButton ID="btnLock" runat="Server" ClientInstanceName="btnLock" Image-Width="30px" CausesValidation="false"
                                    AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" CssClass="dxeLineBreakFix">
                                </dx:ASPxButton>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="รหัสรถส่วนหัว" FieldName="H_STRUCK"  Width="16%">
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถส่วนหัว" FieldName="SHEADREGISTERNO"  Width="21%">
                            <DataItemTemplate>
                                <dx:ASPxLabel ID="lblHTRUCK" runat="Server" ClientInstanceName="lblHTRUCK" EncodeHtml="false" Text='<%# ""+Eval("H_STRUCK") %>'>
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbHTRUCK" runat="server" CallbackPageSize="30" ClientInstanceName="cmbHTRUCK" EnableCallbackMode="True"
                                    OnItemsRequestedByFilterCondition="cmbHTRUCK_OnItemsRequestedByFilterConditionSQL" SkinID="xcbbATC"
                                    TextFormatString="{1}" ValueField="STRUCKID" Width="190px" DataSourceID="sdsHTRUCK">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="รหัสรถ" FieldName="STRUCKID" Width="65px" />
                                        <dx:ListBoxColumn Caption="ทะเบียนรถ" FieldName="SHEADREGISTERNO" Width="85px" />
                                    </Columns>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="sdsHTRUCK" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdHTRUCK" SelectCommand="SELECT * FROM TTRUCK WHERE SCARTYPEID ='3'"></asp:SqlDataSource>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ทะเบียนรถส่วนหาง" FieldName="STRAILERREGISTERNO"  Width="21%">
                            <DataItemTemplate>
                                <dx:ASPxLabel ID="lblTTRUCK" runat="Server" ClientInstanceName="lblTTRUCK" EncodeHtml="false" Text='<%# ""+Eval("T_STRUCK") %>'>
                                </dx:ASPxLabel>
                                <dx:ASPxComboBox ID="cmbTTRUCK" runat="server" CallbackPageSize="30" ClientInstanceName="cmbTTRUCK" EnableCallbackMode="True"
                                    OnItemsRequestedByFilterCondition="cmbTTRUCK_OnItemsRequestedByFilterConditionSQL" SkinID="xcbbATC"
                                    TextFormatString="{1}" ValueField="STRUCKID" Width="190px" DataSourceID="sdsTTRUCK">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="รหัสรถ" FieldName="STRUCKID" Width="65px" />
                                        <dx:ListBoxColumn Caption="ทะเบียนรถ" FieldName="SHEADREGISTERNO" Width="85px" />
                                    </Columns>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="sdsTTRUCK" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                    CacheKeyDependency="ckdTTRUCK" SelectCommand="SELECT * FROM TTRUCK WHERE SCARTYPEID = '4'"></asp:SqlDataSource>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="รหัสรถส่วนหาง" FieldName="T_STRUCK"  Width="16%">
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="การทำสัญญารถ" FieldName="CONTRACT" Width="12%">
                            <DataItemTemplate>
                                <dx:ASPxLabel ID="lblContract" runat="Server" ClientInstanceName="lblContract" EncodeHtml="false" Text='<%# ""+Eval("CONTRACT")!=""?"ทำสัญญาแล้ว":"ไม่ทำสัญญา" %>'>
                                </dx:ASPxLabel>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="การจัดการ" Width="8%">
                            <DataItemTemplate>
                                <dx:ASPxButton ID="btnUpdate" runat="Server" ClientInstanceName="btnUpdate" CausesValidation="false"
                                    AutoPostBack="false" Text="อัพเดท">
                                </dx:ASPxButton>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SCARTYPEID" Visible="false">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CHANGED" Visible="false">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sdsTRUCK" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                    CacheKeyDependency="ckdTRUCK" SelectCommand="SELECT ROWNUM-1 TRID,TBL.* FROM
(         
    SELECT SCARTYPEID,STRUCKID H_STRUCK,SHEADREGISTERNO,STRAILERID T_STRUCK,STRAILERREGISTERNO,'NULL' CHANGED,CASE WHEN SCARTYPEID='1' THEN 'FULL TRAILER' ELSE 'SEMI TRAILER' END CARTYPE,(SELECT SCONTRACTID FROM TCONTRACT_TRUCK WHERE ROWNUM =1 AND STRUCKID=TR.STRUCKID AND STRAILERID=TR.STRAILERID) CONTRACT FROM TTRUCK TR WHERE SCARTYPEID='3'
    UNION
    SELECT SCARTYPEID-1,SHEADID H_STRUCK,STRAILERREGISTERNO,STRUCKID T_STRUCK,SHEADREGISTERNO,'NULL' CHANGED,CASE WHEN SCARTYPEID='2' THEN 'FULL TRAILER' ELSE 'SEMI TRAILER' END CARTYPE,(SELECT SCONTRACTID FROM TCONTRACT_TRUCK WHERE ROWNUM =1 AND STRUCKID=TR.SHEADID AND STRAILERID=TR.STRUCKID) CONTRACT FROM TTRUCK TR WHERE SCARTYPEID='4'
) TBL
"></asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
