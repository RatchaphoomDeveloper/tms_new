﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Data;

public partial class KPI : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<Tdata> lstData = new List<Tdata>();
    private static List<Tdata> lstDataDelete = new List<Tdata>();
    private static List<syear> lstYear = new List<syear>();

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetNoStore();
        //Session["UserID"] = "441";
        //Session["CGROUP"] = "2";
        gvwT1.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvwT1_HtmlDataCellPrepared);
        gvwT2.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvwT2_HtmlRowPrepared);
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        if (!IsPostBack)
        {
            lstDataDelete.Clear();
            ListData();
            WeightAll();

            SetCboYear();
            cboYear.Value = DateTime.Now.Year + "";
            ListDataT2();

        }

    }

    #region Tab1

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');

        int Index = 0;
        if (param.Length > 1)
        {
            Index = int.Parse(param[1] + "");
        }

        switch (param[0])
        {
            case "Save":

                AddDataKPI();
                DeleteDataKPI();
                ListData();
                WeightAll();
                break;

            case "Delete":

                dynamic data = gvwT1.GetRowValues(Index, "KPI_ID");

                //Set control Enable เพราะตอนกดลบข้อมูลถ้ากรณีที่เป็น Newrow จะติด Validate ทำให้ลบไม่ออก
                ASPxTextBox txtHeadKPI = gvwT1.FindRowCellTemplateControl(Index, null, "txtHeadKPI") as ASPxTextBox;
                //ASPxComboBox cboFrequency = gvwT1.FindRowCellTemplateControl(Index, null, "cboFrequency") as ASPxComboBox;
                //ASPxComboBox cboSum = gvwT1.FindRowCellTemplateControl(Index, null, "cboSum") as ASPxComboBox;
                ASPxTextBox txtT1 = gvwT1.FindRowCellTemplateControl(Index, null, "txtT1") as ASPxTextBox;
                ASPxTextBox txtT2 = gvwT1.FindRowCellTemplateControl(Index, null, "txtT2") as ASPxTextBox;
                ASPxTextBox txtT3 = gvwT1.FindRowCellTemplateControl(Index, null, "txtT3") as ASPxTextBox;
                ASPxTextBox txtT4 = gvwT1.FindRowCellTemplateControl(Index, null, "txtT4") as ASPxTextBox;
                ASPxTextBox txtT5 = gvwT1.FindRowCellTemplateControl(Index, null, "txtT5") as ASPxTextBox;
                ASPxTextBox txtWeight = gvwT1.FindRowCellTemplateControl(Index, null, "txtWeight") as ASPxTextBox;

                txtHeadKPI.IsValid = false;
                txtT1.IsValid = false;
                txtT2.IsValid = false;
                txtT3.IsValid = false;
                txtT4.IsValid = false;
                txtT5.IsValid = false;
                txtWeight.IsValid = false;
                if (!string.IsNullOrEmpty(data))
                {
                    lstDataDelete.Add(new Tdata
                    {
                        KPI_ID = data,
                        CACTIVE = "N"
                    });
                }

                if (lstData.Count > 0)
                {
                    lstData[Index].CACTIVE = "N";
                    gvwT1.DataSource = lstData.Where(w => w.CACTIVE == "Y");
                    gvwT1.DataBind();
                    WeightAll();
                    txtgvwrowcount.Text = (lstData.Count + "");
                }
                break;

            case "Add":

                GridToList();

                lstData.Add(new Tdata { CACTIVE = "Y" });

                gvwT1.DataSource = lstData;
                gvwT1.DataBind();
                txtgvwrowcount.Text = (lstData.Count + "");

                break;

            case "Cancel":
                break;
        }


    }

    void gvwT1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxComboBox cboSum = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "cboSum") as ASPxComboBox;
        ASPxTextBox txtT1 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtT1") as ASPxTextBox;
        ASPxTextBox txtT2 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtT2") as ASPxTextBox;
        ASPxTextBox txtT3 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtT3") as ASPxTextBox;
        ASPxTextBox txtT4 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtT4") as ASPxTextBox;
        ASPxTextBox txtT5 = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtT5") as ASPxTextBox;
        ASPxTextBox txtWeight = gvwT1.FindRowCellTemplateControl(e.VisibleIndex, null, "txtWeight") as ASPxTextBox;


        string cboSumClientID = "cboSum_" + e.VisibleIndex;
        cboSum.ClientInstanceName = cboSumClientID;

        string T1ClientID = "txtT1_" + e.VisibleIndex;
        txtT1.ClientInstanceName = T1ClientID;

        string T2ClientID = "txtT2_" + e.VisibleIndex;
        txtT2.ClientInstanceName = T2ClientID;

        string T3ClientID = "txtT3_" + e.VisibleIndex;
        txtT3.ClientInstanceName = T3ClientID;

        string T4ClientID = "txtT4_" + e.VisibleIndex;
        txtT4.ClientInstanceName = T4ClientID;

        string T5ClientID = "txtT5_" + e.VisibleIndex;
        txtT5.ClientInstanceName = T5ClientID;

        string txtWeightClientID = "txtWeight_" + e.VisibleIndex;
        txtWeight.ClientInstanceName = txtWeightClientID;

        //" + cboSumClientID + ",
        txtT1.ClientSideEvents.TextChanged = "function(s,e){CheckPhaseVal(" + e.VisibleIndex + ",1); }";
        txtT2.ClientSideEvents.TextChanged = "function(s,e){CheckPhaseVal(" + e.VisibleIndex + ",2); }";
        txtT3.ClientSideEvents.TextChanged = "function(s,e){CheckPhaseVal(" + e.VisibleIndex + ",3); }";
        txtT4.ClientSideEvents.TextChanged = "function(s,e){CheckPhaseVal(" + e.VisibleIndex + ",4); }";
        txtT5.ClientSideEvents.TextChanged = "function(s,e){CheckPhaseVal(" + e.VisibleIndex + ",5); }";
    }

    private void AddDataKPI()
    {


        GridToList();


        foreach (var item in lstData)
        {
            if (!string.IsNullOrEmpty(item.KPI_ID))
            {
                string QUERY_UPT = @"UPDATE TBL_KPI
                                    SET    KPI_NAME      = " + CheckNull(item.KPI_NAME) + @",
                                           KPI_FREQUENCY = " + CheckNull(item.KPI_Frequency) + @",
                                           KPI_SUM       = " + CheckNull(item.KPI_Sum) + @",
                                           PHASE1        = " + CheckNull(item.Phase1) + @",
                                           PHASE2        = " + CheckNull(item.Phase2) + @",
                                           PHASE3        = " + CheckNull(item.Phase3) + @",
                                           PHASE4        = " + CheckNull(item.Phase4) + @",
                                           PHASE5        = " + CheckNull(item.Phase5) + @",
                                           KPI_SUMWEIGHT = " + CheckNullInt(item.KPI_SumWeight) + @",
                                           CACTIVE       = " + CheckNull(item.CACTIVE) + @",
                                           DUPDATE       = SYSDATE,
                                           SUPDATE       = " + CheckNull(Session["UserID"] + "") + @"
                                    WHERE  KPI_ID        = " + CommonFunction.ReplaceInjection(item.KPI_ID) + "";
                AddTODB(QUERY_UPT);
            }
            else
            {
                string KPI_ID = CommonFunction.Gen_ID(conn, "SELECT KPI_ID FROM TBL_KPI  ORDER BY  KPI_ID DESC");
                string QUERY_INS = @"INSERT INTO TBL_KPI (KPI_ID, KPI_NAME, KPI_FREQUENCY, KPI_SUM, PHASE1, PHASE2, PHASE3, PHASE4, PHASE5, KPI_SUMWEIGHT, CACTIVE, DCREATE, SCREATE) 
                                     VALUES(" + KPI_ID + @",
                                            " + CheckNull(item.KPI_NAME) + @", 
                                            " + CheckNull(item.KPI_Frequency) + @",
                                            " + CheckNull(item.KPI_Sum) + @",
                                            " + CheckNull(item.Phase1) + @",
                                            " + CheckNull(item.Phase2) + @",
                                            " + CheckNull(item.Phase3) + @",
                                            " + CheckNull(item.Phase4) + @",
                                            " + CheckNull(item.Phase5) + @",
                                            " + CheckNullInt(item.KPI_SumWeight) + @",
                                            " + CheckNull(item.CACTIVE) + @",
                                            SYSDATE,
                                            " + CheckNull(Session["UserID"] + "") + ")";

                AddTODB(QUERY_INS);
            }
        }
    }

    private void DeleteDataKPI()
    {
        foreach (var item in lstDataDelete)
        {
            string QUERY_UPT = @"UPDATE TBL_KPI
                                    SET    CACTIVE       = " + CheckNull(item.CACTIVE) + @",
                                           DUPDATE       = SYSDATE,
                                           SUPDATE       = " + CheckNull(Session["UserID"] + "") + @"
                                    WHERE  KPI_ID        = " + CommonFunction.ReplaceInjection(item.KPI_ID) + "";
            AddTODB(QUERY_UPT);
        }
        lstDataDelete.Clear();
    }

    private void GridToList()
    {
        lstData.Clear();

        for (int i = 0; i < gvwT1.VisibleRowCount; i++)
        {
            dynamic data = gvwT1.GetRowValues(i, "KPI_ID", "CACTIVE");
            ASPxTextBox txtHeadKPI = gvwT1.FindRowCellTemplateControl(i, null, "txtHeadKPI") as ASPxTextBox;
            ASPxComboBox cboFrequency = gvwT1.FindRowCellTemplateControl(i, null, "cboFrequency") as ASPxComboBox;
            ASPxComboBox cboSum = gvwT1.FindRowCellTemplateControl(i, null, "cboSum") as ASPxComboBox;
            ASPxTextBox txtT1 = gvwT1.FindRowCellTemplateControl(i, null, "txtT1") as ASPxTextBox;
            ASPxTextBox txtT2 = gvwT1.FindRowCellTemplateControl(i, null, "txtT2") as ASPxTextBox;
            ASPxTextBox txtT3 = gvwT1.FindRowCellTemplateControl(i, null, "txtT3") as ASPxTextBox;
            ASPxTextBox txtT4 = gvwT1.FindRowCellTemplateControl(i, null, "txtT4") as ASPxTextBox;
            ASPxTextBox txtT5 = gvwT1.FindRowCellTemplateControl(i, null, "txtT5") as ASPxTextBox;
            ASPxTextBox txtWeight = gvwT1.FindRowCellTemplateControl(i, null, "txtWeight") as ASPxTextBox;

            lstData.Add(new Tdata
            {
                KPI_ID = data[0],
                KPI_NAME = txtHeadKPI.Text,
                KPI_Frequency = cboFrequency.Value + "",
                KPI_Sum = cboSum.Value + "",
                Phase1 = txtT1.Text,
                Phase2 = txtT2.Text,
                Phase3 = txtT3.Text,
                Phase4 = txtT4.Text,
                Phase5 = txtT5.Text,
                KPI_SumWeight = txtWeight.Text,
                CACTIVE = data[1]
            });
        }
    }

    private string CheckNull(string VALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(VALUE))
        {
            Result = "'" + CommonFunction.ReplaceInjection(VALUE) + "'";
        }
        else
        {
            Result = "null";
        }

        return Result;
    }

    private string CheckNullInt(string VALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(VALUE))
        {
            Result = VALUE;
        }
        else
        {
            Result = "null";
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    void ListData()
    {
        lstData.Clear();
        string QUERY = "SELECT KPI_ID, KPI_NAME, KPI_FREQUENCY, KPI_SUM, PHASE1, PHASE2, PHASE3, PHASE4, PHASE5, KPI_SUMWEIGHT, CACTIVE FROM TBL_KPI WHERE CACTIVE = 'Y' ORDER BY KPI_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstData.Add(new Tdata
                {
                    KPI_ID = dt.Rows[i]["KPI_ID"] + "",
                    KPI_NAME = dt.Rows[i]["KPI_NAME"] + "",
                    KPI_Frequency = dt.Rows[i]["KPI_FREQUENCY"] + "",
                    KPI_Sum = dt.Rows[i]["KPI_SUM"] + "",
                    Phase1 = dt.Rows[i]["PHASE1"] + "",
                    Phase2 = dt.Rows[i]["PHASE2"] + "",
                    Phase3 = dt.Rows[i]["PHASE3"] + "",
                    Phase4 = dt.Rows[i]["PHASE4"] + "",
                    Phase5 = dt.Rows[i]["PHASE5"] + "",
                    KPI_SumWeight = dt.Rows[i]["KPI_SUMWEIGHT"] + "",
                    CACTIVE = dt.Rows[i]["CACTIVE"] + "",
                });

            }
        }

        if (lstData.Count > 0)
        {
            gvwT1.DataSource = lstData;
            gvwT1.DataBind();
            txtgvwrowcount.Text = gvwT1.VisibleRowCount + "";
        }
        else
        {
            txtgvwrowcount.Text = "0";
        }
        gvwT1.DataBind();
    }

    void WeightAll()
    {
        string QUERY_SUM = @"SELECT  SUM(KPI_SUMWEIGHT) as SUMWEIGHT, CACTIVE FROM TBL_KPI WHERE CACTIVE = 'Y' GROUP BY CACTIVE";
        DataTable dt_SUM = CommonFunction.Get_Data(conn, QUERY_SUM);
        if (dt_SUM.Rows.Count > 0)
        {
            txtWeightAll.Text = !string.IsNullOrEmpty(dt_SUM.Rows[0]["SUMWEIGHT"] + "") ? dt_SUM.Rows[0]["SUMWEIGHT"] + "" : "0";
        }
        else
        {
            txtWeightAll.Text = "0";
        }
    }

    #endregion

    #region Tab2

    void gvwT2_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        ASPxButton btnEdit = gvwT2.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit") as ASPxButton;
        if (!string.IsNullOrEmpty(e.GetValue("SABBREVIATION") + ""))
        {
            e.Row.BackColor = System.Drawing.Color.FromName("#B2DFEE");
            btnEdit.ClientVisible = false;
        }
    }

    protected void xcpnT2_Load(object sender, EventArgs e)
    {

    }

    protected void xcpnT2_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');

        int Index = 0;
        if (param.Length > 1)
        {
            Index = int.Parse(param[1] + "");
        }

        switch (param[0])
        {
            case "Search":
                ListDataT2();
                break;
            case "KPI":
                dynamic data = gvwT2.GetRowValues(Index, "SVENDORID", "SCONTRACTID");
                xcpnT2.JSProperties["cpRedirectTo"] = "KPI_rate.aspx?ven=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&con=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1])) + "&year=" + Server.UrlEncode(STCrypt.Encrypt("" + cboYear.Value));
                break;
        }


    }

    private void ListDataT2()
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(cboSCONTRACT.Value + ""))
        {
            Condition += " AND CON.SCONTRACTID = '" + CommonFunction.ReplaceInjection(cboSCONTRACT.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(cboVendor.Value + ""))
        {
            Condition += " AND CON.SVENDORID = '" + CommonFunction.ReplaceInjection(cboVendor.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(cboYear.Value + ""))
        {
            Condition += " AND (extract(year from CON.DBEGIN) <= " + CommonFunction.ReplaceInjection(cboYear.Value + "") + "  AND extract(year from CON.DEND) >= " + CommonFunction.ReplaceInjection(cboYear.Value + "") + ")";
        }

        string QUERY = @"SELECT CON.SCONTRACTID,CON.SCONTRACTTYPEID,CON.SCONTRACTNO,VEN.SABBREVIATION,CON.SVENDORID FROM TCONTRACT CON
INNER JOIN TVENDOR VEN
ON VEN.SVENDORID = CON.SVENDORID
WHERE CON.CACTIVE = 'Y' " + Condition + @" 
ORDER BY CON.SVENDORID";

        DataTable dt = MergeSubGroup(QUERY);
        if (dt.Rows.Count > 0)
        {
            gvwT2.DataSource = dt;

        }
        gvwT2.DataBind();


    }

    private DataTable MergeSubGroup(string QUERY)
    {

        DataTable dt = new DataTable();
        dt.Rows.Clear();

        dt.Columns.Add("SABBREVIATION", typeof(string));
        dt.Columns.Add("SCONTRACTNO", typeof(string));
        dt.Columns.Add("SVENDORID", typeof(string));
        dt.Columns.Add("SCONTRACTID", typeof(int));
        dt.Columns.Add("SCONTRACTTYPEID", typeof(string));

        DataTable dt_DATA = CommonFunction.Get_Data(conn, QUERY);
        DataRow rows;
        int nInsertemp = 0;
        int nNo = 0;
        for (int i = 0; i < dt_DATA.Rows.Count; i++)
        {
            string HEADER = dt_DATA.Rows[i]["SABBREVIATION"] + "";
            string DETAIL = dt_DATA.Rows[i]["SCONTRACTNO"] + "";
            string SVENDORNAME = "";
            string SVENDORID = "";
            string SCONTRACTID = "";
            string SCONTRACTTYPEID = "";

            if (("" + ViewState["HEADER"]) != HEADER)
            {
                ViewState["HEADER"] = HEADER;
                SVENDORNAME = dt_DATA.Rows[i]["SABBREVIATION"] + "";



                dt.Rows.Add(SVENDORNAME, null, null, null, null);

                if (("" + ViewState["DETAIL"]) != DETAIL)
                {
                    SVENDORID = dt_DATA.Rows[i]["SVENDORID"] + "";
                    SCONTRACTID = dt_DATA.Rows[i]["SCONTRACTID"] + "";
                    SCONTRACTTYPEID = dt_DATA.Rows[i]["SCONTRACTTYPEID"] + "";
                    ViewState["DETAIL"] = DETAIL;
                    SVENDORNAME = "";

                    dt.Rows.Add(null, DETAIL, SVENDORID, SCONTRACTID, SCONTRACTTYPEID);
                }
            }
            else
            {
                if (("" + ViewState["DETAIL"]) != DETAIL)
                {
                    SVENDORID = dt_DATA.Rows[i]["SVENDORID"] + "";
                    SCONTRACTID = dt_DATA.Rows[i]["SCONTRACTID"] + "";
                    SCONTRACTTYPEID = dt_DATA.Rows[i]["SCONTRACTTYPEID"] + "";

                    ViewState["DETAIL"] = DETAIL;
                    SVENDORNAME = "";
                    dt.Rows.Add(null, DETAIL, SVENDORID, SCONTRACTID, SCONTRACTTYPEID);
                }
            }



        }
        ViewState["HEADER"] = "";
        ViewState["DETAIL"] = "";
        return dt;
    }

    #region AutoComplete

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboVendor_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        SqlDataSource2.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY TVENDOR.SVENDORID) AS RN , TVENDOR.SVENDORID , TVENDOR.SABBREVIATION
          FROM TVENDOR 
          INNER JOIN TCONTRACT 
          ON TCONTRACT.SVENDORID = TVENDOR.SVENDORID
          WHERE TVENDOR.SVENDORID||TVENDOR.SABBREVIATION LIKE :fillter GROUP BY  TVENDOR.SVENDORID,TVENDOR.SABBREVIATION) WHERE RN BETWEEN :startIndex AND :endIndex ";

        SqlDataSource2.SelectParameters.Clear();
        SqlDataSource2.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        SqlDataSource2.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        SqlDataSource2.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));

        comboBox.DataSource = SqlDataSource2;
        comboBox.DataBind();


    }

    protected void cboSCONTRACT_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboSCONTRACT_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        SqlDataSource1.SelectCommand = @"SELECT SCONTRACTID , SCONTRACTNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY SCONTRACTID) AS RN , SCONTRACTID , SCONTRACTNO
          FROM TCONTRACT WHERE SCONTRACTID||SCONTRACTNO LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        SqlDataSource1.SelectParameters.Clear();
        SqlDataSource1.SelectParameters.Add(":startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        SqlDataSource1.SelectParameters.Add(":endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        SqlDataSource1.SelectParameters.Add(":fillter", TypeCode.String, String.Format("%{0}%", e.Filter));


        comboBox.DataSource = SqlDataSource1;
        comboBox.DataBind();
    }

    #endregion

    private void SetCboYear()
    {
        lstYear.Clear();
        string QUERY = @"SELECT SYEAR FROM TBL_KPI_RATE 
                        WHERE NVL(SYEAR,'xxx') <> 'xxx'
                        GROUP BY SYEAR
                        ORDER BY to_number(SYEAR) ASC";
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        int nstartYear = 0;
        if (dt.Rows.Count > 0)
        {
            nstartYear = int.Parse(dt.Rows[0]["SYEAR"] + "");
        }
        else
        {
            nstartYear = DateTime.Now.Year;
        }



        for (int i = nstartYear; i < (nstartYear + 11); i++)
        {
            lstYear.Add(new syear
            {
                NYEAR = i,
                SYEAR = i + 543
            });
        }

        cboYear.DataSource = lstYear.OrderByDescending(w => w.NYEAR);
        cboYear.ValueField = "NYEAR";
        cboYear.TextField = "SYEAR";
        cboYear.DataBind();

    }

    #endregion

    #region class
    [Serializable]
    class Tdata
    {
        public string KPI_ID { get; set; }
        public string KPI_NAME { get; set; }
        public string KPI_Frequency { get; set; }
        public string KPI_Sum { get; set; }
        public string Phase1 { get; set; }
        public string Phase2 { get; set; }
        public string Phase3 { get; set; }
        public string Phase4 { get; set; }
        public string Phase5 { get; set; }
        public string KPI_SumWeight { get; set; }
        public string CACTIVE { get; set; }
    }

    class syear
    {
        public int SYEAR { get; set; }
        public int NYEAR { get; set; }

    }
    #endregion

    protected void ASPxPageControl_ActiveTabChanged(object source, DevExpress.Web.ASPxTabControl.TabControlEventArgs e)
    {
        if (e.Tab.VisibleIndex == 0)
        {
            ListData();
        }
        else
        {
            ListDataT2();
        }
    }
}