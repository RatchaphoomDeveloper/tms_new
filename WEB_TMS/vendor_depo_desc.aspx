﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_depo_desc.aspx.cs" Inherits="vendor_depo_desc" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
<ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = &#39;&#39;;if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                 <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td width="15%" bgcolor="#FFFFFF">
                            คลัง</td>
                        <td width="25%">
                            <dx:ASPxLabel ID="lblTerminal" runat="server">
                            </dx:ASPxLabel>
                        </td>
                        <td width="15%" align="left" bgcolor="#FFFFFF" class="style21">
                            ประเภทผลิตภัณฑ์</td>
                        <td width="25%">
                            <dx:ASPxLabel ID="lblProductType" runat="server" >
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ที่ตั้ง</td>
                        <td>
                            <dx:ASPxLabel ID="lblAddress" runat="server" >
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            ภาค</td>
                        <td>
                            <dx:ASPxLabel ID="lblRegion" runat="server" >
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            โทรศัพท์</td>
                        <td>
                            <dx:ASPxLabel ID="lblTel" runat="server" >
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            โทรสาร</td>
                        <td>
                            <dx:ASPxLabel ID="lblFax" runat="server" >
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            ไฟล์เอกสาร</td>
                        <td class="style15">
                            ชื่อเอกสาร</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            </td>
                        <td class="style15">
                            1.
                            <dx:ASPxLabel ID="lblEvidence" runat="server">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnView" runat="server" CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer" Width="20px">
                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue());}" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png" ></Image>
                                            </dx:ASPxButton></td>
                        <td>
                            <dx:ASPxTextBox ID="txtFilePath" runat="server" 
                                ClientInstanceName="txtFilePath" ClientVisible="False" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            </td>
                        <td class="style15">
                            2.
                            <dx:ASPxLabel ID="lblEvidence1" runat="server">
                            </dx:ASPxLabel>
                        </td>
                        <td>
                             <dx:ASPxButton ID="btnView1" runat="server" CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer" Width="20px">
                                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png" ></Image>
                                            </dx:ASPxButton></td>
                        <td>
                            <dx:ASPxTextBox ID="txtFilePath1" runat="server" 
                                ClientInstanceName="txtFilePath1" ClientVisible="False" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <dx:ASPxButton ID="btnSave" runat="server" 
                                Text="ตกลง" Width="100px" OnClick="btnSave_Click">
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
