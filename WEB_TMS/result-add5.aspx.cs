﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using DevExpress.Web.ASPxGridView;

public partial class result_add5 : System.Web.UI.Page
{
    string cSAP_SYNC = WebConfigurationManager.AppSettings["SAP_SYNCOUT"].ToString();
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private string sFomateDate = ConfigurationManager.AppSettings["FormatDate"] + "";
    private string sCheckWaterID = "01";

    //private static string sREQUESTID = "";
    //private static string STATUSID = "";
    //private static string STRUCKID = "";
    //private static string USER_ID = "";
    //private static string STATUSFLAG_ID = "";
    //private static string VENDOR_ID = "";
    //private static string CARCATE_ID = "";
    //private static DataTable dtMainData = new DataTable();

    private string sREQUESTID { get { return Session["sREQUESTID"] + ""; } set { Session["sREQUESTID"] = value; } }
    private string STATUSID { get { return Session["STATUSID"] + ""; } set { Session["STATUSID"] = value; } }
    private string STRUCKID { get { return Session["STRUCKID"] + ""; } set { Session["STRUCKID"] = value; } }
    private string USER_ID { get { return Session["UserID"] + ""; } set { Session["UserID"] = value; } }
    private string STATUSFLAG_ID { get { return Session["STATUSFLAG_ID"] + ""; } set { Session["STATUSFLAG_ID"] = value; } }
    private string VENDOR_ID { get { return Session["VENDOR_ID"] + ""; } set { Session["VENDOR_ID"] = value; } }
    private string CARCATE_ID { get { return Session["CARCATE_ID"] + ""; } set { Session["CARCATE_ID"] = value; } }
    private DataTable dtMainData { get { return SystemFunction.ConvertObject(Session["dtMainData"]); } set { Session["dtMainData"] = value; } }
    string SMENUID = "57";
    string STATUS_TAP2 = "";
    string STATUS_TAP3 = "";
    private void ClearSessionStatic()
    {
        Session["sREQUESTID"] = "";
        Session["STATUSID"] = "";
        Session["STRUCKID"] = "";
        Session["STATUSFLAG_ID"] = "";
        Session["VENDOR_ID"] = "";
        Session["CARCATE_ID"] = "";
        Session["dtMainData"] = "";
    }

    private void NewLstStatic()
    {
        sREQUESTID = "";
        STATUSID = "";
        STRUCKID = "";
        STATUSFLAG_ID = "";
        VENDOR_ID = "";
        CARCATE_ID = "";
        dtMainData = new DataTable();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
                ClearSessionStatic();
                NewLstStatic();
                string strREQID = Request.QueryString["strRQID"];
                if (!string.IsNullOrEmpty(strREQID + ""))
                {
                    USER_ID = Session["UserID"] + "";

                    string[] arrREQID = STCrypt.DecryptURL(strREQID);
                    sREQUESTID = arrREQID[0];
                    LogUser(SMENUID, "R", "เปิดดูข้อมูลหน้า บันทึกผลปิดงาน(มว.)", sREQUESTID);
                    if (!string.IsNullOrEmpty(sREQUESTID))
                    {
                        //bool ccc = SendMailToUser(arrREQID[0]);
                        ListDataToPage(arrREQID[0]);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                    }

                }
            }
        }
    }

    private void ListDataToPage(string sREQID)
    {
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,
                        TCAT.CARCATE_NAME,TRQ.CARCATE_ID,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,TRK.DPREV_SERV,TRK.DLAST_SERV,TRK.SCAR_NUM,TRQ.WATERON_DATE
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        SetDataToPage();
    }

    private void CheckStatusTbl(string sREQID)
    {
        string sql1 = @"SELECT STATUS_TAP2,STATUS_TAP3 FROM TBL_REQUEST WHERE REQUEST_ID = '" + sREQID + "'";
        DataTable dt1 = CommonFunction.Get_Data(strConn, sql1);
        STATUS_TAP2 = dt1.Rows[0]["STATUS_TAP2"].ToString().Trim();
        STATUS_TAP3 = dt1.Rows[0]["STATUS_TAP3"].ToString().Trim();
    }

    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
            decimal nTemp = 0;

            //if (dr["STATUS_FLAG"] + "" == "10")
            //{
            //    btnSaveT5.ClientVisible = true;

            //}
            //else
            //{
            //    btnSaveT5.ClientVisible = true;
            //}
            STATUSFLAG_ID = dr["STATUS_FLAG"] + "";
            VENDOR_ID = dr["SVENDORID"] + "";
            CARCATE_ID = dr["CARCATE_ID"] + "";

            lblREQUEST_DATE.Text = !string.IsNullOrEmpty(dr["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr["REQUEST_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblDWATEREXPIRE.Text = !string.IsNullOrEmpty(dr["DWATEREXPIRE"] + "") ? Convert.ToDateTime(dr["DWATEREXPIRE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblAPPOINTMENT_BY_DATE.Text = (!string.IsNullOrEmpty(dr["APPROVE_DATE"] + "") ? Convert.ToDateTime(dr["APPROVE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "") +
                                          (!string.IsNullOrEmpty(dr["SFIRSTNAME"] + "") && !string.IsNullOrEmpty(dr["SLASTNAME"] + "") ? " - คุณ" + dr["SFIRSTNAME"] + " " + dr["SLASTNAME"] : "-");


            lblREQTYPE_NAME.Text = dr["REQTYPE_NAME"] + "";
            lblCAUSE_NAME.Text = dr["CAUSE_NAME"] + "" + (!string.IsNullOrEmpty(dr["REMARK_CAUSE"] + "") ? (" (" + dr["REMARK_CAUSE"] + ")") : "");
            lblVendorName.Text = dr["SABBREVIATION"] + "";
            lblTypeCar.Text = dr["CARCATE_NAME"] + "";
            lblTotalCap.Text = (decimal.TryParse(dr["TOTLE_CAP"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(0));
            lblREGISTERNO.Text = dr["VEH_NO"] + "" + (!string.IsNullOrEmpty(dr["TU_NO"] + "") ? "/" + dr["TU_NO"] + "" : "");


            //เช็คว่าถ้าปิดงานแล้วจะไม่สามารถบันทึกได้
            if (dr["STATUS_FLAG"] + "" == "10" || dr["STATUS_FLAG"] + "" == "11")
            {
                btnSaveT5.Visible = false;
                rblStatusChecking.ReadOnly = true;
            }
            else
            {
                btnSaveT5.Visible = true;
            }

            //if (dr["REQTYPE_ID"] + "" == sCheckWaterID)
            //{
            // set data to grid
            string sTruckID = "";
            switch (dr["SCARTYPEID"] + "")
            {
                case "0": STRUCKID = dr["VEH_ID"] + ""; sTruckID = dr["VEH_ID"] + ""; break; // 10 ล้อ
                case "3": STRUCKID = dr["TU_ID"] + ""; sTruckID = dr["TU_ID"] + ""; break; // หัวลาก
                case "4": STRUCKID = dr["TU_ID"] + ""; sTruckID = dr["TU_ID"] + ""; break; // หัวลาก
            }
            //}


            STATUSID = dr["REQTYPE_ID"] + "";

            /*****/
            ListDataDoc();
            ListDataDateProcess();

        }
    }

    /*ส่วนข้างล่าง*/

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});"); return;
        }
        string Message_SHOW = "";
        string[] spara = e.Parameter.Split(';');
        switch (spara[0])
        {
            case "SAVET5":
                LogUser(SMENUID, "I", "บันทึกผลปิดงาน(มว.)", sREQUESTID);
                CheckStatusTbl(sREQUESTID);
                if (!string.IsNullOrEmpty(rblStatusChecking.Value + "") || STATUSID == "04" || STATUS_TAP2 != "" || STATUS_TAP3 != "")
                {
                    string Error = UpdateData();
                    if (string.IsNullOrEmpty(Error))
                    {
                        Message_SHOW += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_check.gif />";
                        Message_SHOW += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_check.gif />";
                        //ส่งเมล์
                        if (SendMailToUser(sREQUESTID))
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Message_SHOW + "',function(){window.location='approve_mv.aspx';});");
                        }
                        else // Error send mail
                        {
                            //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้');");
                            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้','" + Message_SHOW + "',function(){window.location='approve_mv.aspx';});");
                        }
                    }
                    else
                    {
                        Message_SHOW += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_check.gif />";
                        Message_SHOW += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif /><br/>" + Error;
                        //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งให้ทราบ','" + Error + "')");
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งให้ทราบ','" + CommonFunction.ReplaceInjection(Message_SHOW) + "')");
                    }
                }
                else
                {
                    //Message_SHOW += "<br /> บันทึกข้อมูล(TMS) : <img src=images/stat_check.gif />";
                    //Message_SHOW += "<br /> บันทึกข้อมูล(SAP) : <img src=images/stat_unchecked.gif />";
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งให้ทราบ','ไม่สามารถบันทึกได้เนื่องจากยังไม่ได้แนบเอกสาร และยังไม่ได้ระบุผลการตรวจสอบ')");
                }
                //CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='approve_mv.aspx';});");
                break;
            case "RedirectT1": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectImg": xcpn.JSProperties["cpRedirectTo"] = "CarImageAdd.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
        };
    }

    private void ListDataDoc()
    {
        List<TData_File> lstFile = new List<TData_File>(ListAttacFile(sREQUESTID, ""));
        List<TData_File> lstFileCloseJob = new List<TData_File>();
        bool cHasDataInvoiceMore = CheckData_ItemMore(sREQUESTID);

        // เอกสารเพิ่มเติม ***  0005 = ใบเสร็จค่าธรรมเนียมเพิ่มเติม,เอกสารเทียบแป้น
        lstFileCloseJob = lstFile.Where(w => cHasDataInvoiceMore == true ? (w.sFileType == "0005" || w.sFileType == "0006") : w.sFileType == "0006").Select(s => new TData_File
        {
            nFileID = s.nFileID,
            sNameShow = s.sNameShow,
            sSysFileName = s.sSysFileName,
            sPath = s.sPath,
            sFileType = s.sFileType,
            sNewFile = s.sNewFile,
            nOrder = s.nOrder,

        }).OrderBy(o => o.nOrder).ToList();
        //กรณีรถลูกค้าไม่ต้องเทียบแป้น
        if (CARCATE_ID == "01")
        {
            lstFileCloseJob.RemoveAll(w => w.sFileType == "0006");
            trPan.Visible = false;

        }
        else
        {
            string sMark = "";
            if (STATUSFLAG_ID == "07")
            {
                sMark = "<span class='active'>ไม่ต้องแนบเอกสารเทียบแป้น</span>";
            }
            else
            {
                sMark = "<span class='active'>ยังไม่แนบเอกสาร</span>";
                lblOpenInvoiceMore.Text = cHasDataInvoiceMore == true ? sMark : "<span class='active'>ไม่มีค่าธรรมเนียมเพิ่มเติม</span>";
            }
            lblOpenDocPan.Text = sMark;
        }
        int CheckService = SystemFunction.GetServceAdd(sREQUESTID);
        if (CheckService > 0)
        {
            trService.Visible = true;
        }
        else
        {
            trService.Visible = false;
        }

        foreach (var item in lstFileCloseJob)
        {
            switch (item.sFileType)
            {
                case "0005":
                    lblOpenInvoiceMore.Text = LinkOpenFile(item.sPath, item.sSysFileName, item.sNameShow);
                    break;
                case "0006":
                    lblOpenDocPan.Text = LinkOpenFile(item.sPath, item.sSysFileName, item.sNameShow);
                    break;
            }
        }
    }

    private void ListDataDateProcess()
    {
        string sql1 = @"SELECT  TRQ.REQUEST_ID,TRQ.REQUEST_DATE ,TRQ.APPROVE_DATE as DMVCONFIRM_DATE,TRQ.SERVICE_DATE,TRQ.RESULT_CHECKING_DATE,TRQ.CLOSEJOB_DATE,TRQ.RESULT_TRY FROM TBL_REQUEST TRQ WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'";
        string sql2 = @"SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "'";
        DataTable dtRQ = new DataTable();
        DataTable dtREDoc = new DataTable();

        //ถ้าเป็นกรณีพ่นสาระไม่ต้องแนบเอกสรเทียบแป้น
        if (STATUSID == "04")
        {
            trTeabpan.Visible = false;
            trPan.Visible = false;
            trPass.Visible = false;
            rblStatusChecking.Enabled = false;
        }
        else
        {
            //เช็คว่ามีบริการเพิ่มเติมไหม
            int CheckService = SystemFunction.GetServceAdd(sREQUESTID);
            if (CheckService > 0)
            {
                //เช็คว่ามีเอกสารไหม
                int COUNTDOC = CommonFunction.Count_Value(strConn, "SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "' AND DOC_TYPE = '0005'");
                //เอกสารเทียบแป้น
                int COUNTDOC2 = CommonFunction.Count_Value(strConn, "SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "' AND DOC_TYPE = '0006'");
                //กรณีรถลูกค้าไม่ต้องเทียบแป้น
                if (CARCATE_ID == "01")
                {
                    COUNTDOC2 = 1;
                }


                if (COUNTDOC + COUNTDOC2 == 2)
                {
                    trPass.Visible = true;
                }
                else
                {
                    trPass.Visible = false;
                }
            }
            else
            {
                ////เช็คว่ามีเอกสารไหม
                //int COUNTDOC = CommonFunction.Count_Value(strConn, "SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "' AND DOC_TYPE = '0005'");
                //เอกสารเทียบแป้น
                int COUNTDOC2 = CommonFunction.Count_Value(strConn, "SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(CommonFunction.ReplaceInjection(sREQUESTID)) + "' AND DOC_TYPE = '0006'");

                if (COUNTDOC2 > 0)
                {
                    trPass.Visible = true;
                }
                else
                {
                    trPass.Visible = false;
                }
            }
        }
        dtRQ = CommonFunction.Get_Data(strConn, sql1);
        dtREDoc = CommonFunction.Get_Data(strConn, sql2);

        DataRow dr1 = dtRQ.Rows[0];
        if (dr1 != null)
        {
            lblREQUEST_DATE2.Text = !string.IsNullOrEmpty(dr1["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr1["REQUEST_DATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-";
            lblDMVCONFIRM_DATE.Text = !string.IsNullOrEmpty(dr1["DMVCONFIRM_DATE"] + "") ? Convert.ToDateTime(dr1["DMVCONFIRM_DATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE2.Text = !string.IsNullOrEmpty(dr1["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr1["SERVICE_DATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-";
            lblRESULT_CHECKING_DATE.Text = !string.IsNullOrEmpty(dr1["RESULT_CHECKING_DATE"] + "") ? Convert.ToDateTime(dr1["RESULT_CHECKING_DATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-";
            lblCLOSEJOB_DATE.Text = !string.IsNullOrEmpty(dr1["CLOSEJOB_DATE"] + "") ? Convert.ToDateTime(dr1["CLOSEJOB_DATE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-";



            if (!string.IsNullOrEmpty(dr1["RESULT_TRY"] + ""))
            {
                //ใบเสร็จค่าธรรมเนียมเพิ่มเติม


                rblStatusChecking.Value = dr1["RESULT_TRY"] + "";
            }
        }

        /*0004	การเงิน
        0001	ใบเสร็จรับเงิน
        0002	เอกสารอื่นๆ
        0003	เอกสารทะเบียนรถ
        0005	ใบเสร็จค่าธรรมเนียมเพิ่มเติม
        0006	เอกสารเทียบแป้น*/

        lblDoc0001.Text = "-";
        lblDoc0005.Text = "-";
        lblDoc0006.Text = "-";
        foreach (DataRow dr in dtREDoc.Rows)
        {
            switch (dr["DOC_TYPE"] + "")
            {
                case "0001": lblDoc0001.Text = !string.IsNullOrEmpty(dr["DATTACH_FILE"] + "") ? Convert.ToDateTime(dr["DATTACH_FILE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-"; break;
                case "0005": lblDoc0005.Text = !string.IsNullOrEmpty(dr["DATTACH_FILE"] + "") ? Convert.ToDateTime(dr["DATTACH_FILE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-"; break;
                case "0006": lblDoc0006.Text = !string.IsNullOrEmpty(dr["DATTACH_FILE"] + "") ? Convert.ToDateTime(dr["DATTACH_FILE"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "-"; break;
            }

        }
    }

    private List<TData_File> ListAttacFile(string sREQID, string sFileTypeID)
    {
        List<TData_File> lstTemp = new List<TData_File>();
        string scondition1 = "AND DOC_TYPE  = '" + CommonFunction.ReplaceInjection(sFileTypeID) + "'";
        string sql = @"SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '{0}'" + (!string.IsNullOrEmpty(sFileTypeID) ? scondition1 : "");
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        if (dt.Rows.Count > 0)
        {
            lstTemp = dt.AsEnumerable().Select(s => new TData_File
            {
                nFileID = s.Field<decimal>("DOC_ID"),
                sSysFileName = s.Field<string>("FILE_SYSNAME"),
                sNameShow = s.Field<string>("FILE_NAME"),
                sPath = s.Field<string>("FILE_PATH"),
                nOrder = s.Field<decimal>("DOC_ITEM"),
                sFileType = s.Field<string>("DOC_TYPE"),
                sNewFile = "N",
                sConsider = s.Field<string>("CONSIDER") == null ? "N" : s.Field<string>("CONSIDER")
            }).ToList();
        }

        return lstTemp;
    }

    private string LinkOpenFile(string pathFile, string _Filename, string NameFileShow)
    {
        string strEncrypt = "";
        string sReturn = "";
        #region แสดงไฟล์
        /*
        if (ConfigurationManager.AppSettings["Encrypt"].ToString().Equals("1"))
        {
            strEncrypt = Server.UrlEncode(STCrypt.Encrypt(pathFile + _Filename));
        }
        else
        {
            strEncrypt = pathFile + _Filename;
        }*/

        strEncrypt = pathFile + _Filename;

        //sReturn = "<a href=openFile.aspx?str=" + strEncrypt + " target=_blank style=color:#03527c;text-decoration:none> <img src='images/ic_pdf2.gif' width='16' height='16' border='0'/> " + NameFileShow + " <img border='0' height='25' src='images/view1.png' width='25'/></a>";//" + _Filename + "
        sReturn = "<a href=openFile.aspx?str=" + strEncrypt + " target=_blank style=color:#03527c;text-decoration:none>" + NameFileShow + " <img border='0' height='25' src='images/view1.png' width='25'/></a>";//" + _Filename + "
        return sReturn;
        #endregion
    }

    private bool CheckData_ItemMore(string sReqID)
    {
        string sql = "SELECT * FROM TBL_REQUEST_ITEM2 WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' AND CTYPE_ITEM = '1'";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);

        return dt.Rows.Count > 0 ? true : false;
    }

    private string UpdateData()
    {
        string Message_error = "";

        if (STATUSID == "04")
        {
            Message_error = UpdateStatus_TBL_REQUEST(sREQUESTID, "10", "Y");
            SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, "N", SystemFunction.GetRemark_WorkFlowRequest(8), "06", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(15), "", "M");
        }
        else
        {
            if (!string.IsNullOrEmpty(rblStatusChecking.SelectedItem + ""))
            {

                if (rblStatusChecking.SelectedItem.Value + "" == "Y")
                {
                    Message_error = UpdateStatus_TBL_REQUEST(sREQUESTID, "10", "Y");
                    SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, "N", SystemFunction.GetRemark_WorkFlowRequest(8), "06", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(15), "", "M");
                }
                else
                {
                    Message_error = UpdateStatus_TBL_REQUEST(sREQUESTID, "07", "N");
                    SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, "N", SystemFunction.GetRemark_WorkFlowRequest(7), "06", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(15), "", "M");
                }

                if (string.IsNullOrEmpty(Message_error))
                {
                    Update_DOC(sREQUESTID, rblStatusChecking.SelectedItem.Value + "");
                }
                ListDataDateProcess();

                // CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
            }
            else if (string.IsNullOrEmpty(rblStatusChecking.SelectedItem + "") && STATUS_TAP2 != "" || STATUS_TAP3 != "")
            {
                rblStatusChecking.Value = "N";
                Message_error = UpdateStatus_TBL_REQUEST(sREQUESTID, "10", "N");
                SystemFunction.Add_To_TBL_REQREMARK(sREQUESTID, "N", SystemFunction.GetRemark_WorkFlowRequest(7), "06", "A", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(15), "", "M");

                if (string.IsNullOrEmpty(Message_error))
                {
                    Update_DOC(sREQUESTID, rblStatusChecking.SelectedItem.Value + "");
                }
                ListDataDateProcess();
            }
            else
            {
                Message_error = "กรุณาระบุผลการตรวจสอบ";
                //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งให้ทราบ','กรุณาระบุผลการตรวจสอบ')");
            }
        }

        return Message_error;
    }

    private string UpdateStatus_TBL_REQUEST(string sReqID, string _sStatusID, string _sTry)
    {
        string Message_error = "";
        string sql = "";

        if (_sTry == "Y")
        {
            sql = @"UPDATE TBL_REQUEST SET STATUS_FLAG = '" + CommonFunction.ReplaceInjection(_sStatusID) + "', RESULT_TRY = '" + CommonFunction.ReplaceInjection(_sTry) + "', CLOSEJOB_DATE = SYSDATE  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

            //ในกรณีที่ปิดงานแล้วจะอัพเดท SCAENUM รหัสวัดน้ำ และ SLAST_REQ 
            if (STATUSID == "01")
            {
                string SQLPREV = "SELECT DPREV_SERV,SLAST_REQ_ID,NVL(DNEXT_SERV,DWATEREXPIRE) as DNEXT_SERV FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
                DataTable dtPrev = CommonFunction.Get_Data(strConn, SQLPREV);
                //ถ้า Request ไอดีเป็นตัวปัจจุบันไม่ต้องอัพเดทข้อมูล
                //if (dtPrev.Select("SLAST_REQ_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'").Count() > 0)
                //{

                //}
                //else
                //{
                string ConditionDPrev = "";
                if (dtPrev.Rows.Count > 0)
                {
                    #region Update TMS
                    //เช็คว่าวันหมดอายุวัดน้ำแล้วหรือยัง
                    DateTime dTemp;
                    DateTime WaterExpire;
                    DateTime WaterOn;
                    if (!string.IsNullOrEmpty(dtMainData.Rows[0]["DWATEREXPIRE"] + ""))
                    {
                        //ถ้าไม่สามารถแปลงวันที่ได้จะต้องนำวันที่ปัจจุบัน - 1 เพื่อให้ไปเข้าเคสของ  if (WaterExpire > DateTime.Now)
                        WaterExpire = DateTime.Parse(dtMainData.Rows[0]["DWATEREXPIRE"] + "");
                        //เช็คว่ามาลงน้ำก่อนหมดหรือไม่ ถ้ายังไม่หมดเอาวันที่วัดน้ำเดิมไปใช้ แต่ถ้าหมดแล้วเอาวันที่วัดน้ำใหม่ไปใช้
                        if (WaterExpire > DateTime.Now)
                        {
                            WaterOn = DateTime.Parse(dtMainData.Rows[0]["DWATEREXPIRE"] + "");
                        }
                        else
                        {
                            WaterOn = DateTime.Parse(dtMainData.Rows[0]["WATERON_DATE"] + "");
                        }
                        //ถ้ามีวันที่หมดอายุวัดน้ำเดิมจะเอาไปใส่ใน DPREV_SERV แทน
                        ConditionDPrev = ", DPREV_SERV = TO_DATE('" + CommonFunction.ReplaceInjection(WaterExpire.AddYears(-3).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd') , DLAST_SERV = TO_DATE('" + CommonFunction.ReplaceInjection(WaterExpire.AddYears(-3).ToString("yyyy/MM/dd", new CultureInfo("en-US"))) + "','yyyy-MM-dd')";

                    }
                    else
                    {
                        //แต่ถ้าไม่มีวันที่หมดอายุวัดน้ำ ให้นำวันที่หมดอายุวัดน้ำใหม่ไปใส่ใหม่ไปใช้
                        WaterOn = DateTime.Parse(dtMainData.Rows[0]["WATERON_DATE"] + "");
                        //ไม่มีวันหมดอายุวัดน้ำเก่าไม่ต้องใส่ DPREV_SERV
                    }

                    string sTempDateNEW = WaterOn.ToString("yyyy/MM/dd", new CultureInfo("en-US"));
                    string QUERY = "UPDATE TTRUCK SET DWATEREXPIRE = TO_DATE('" + CommonFunction.ReplaceInjection(sTempDateNEW) + "','yyyy-MM-dd'), DNEXT_SERV = TO_DATE('" + CommonFunction.ReplaceInjection(sTempDateNEW) + "','yyyy-MM-dd') , SPREV_REQ_ID = '" + CommonFunction.ReplaceInjection(dtPrev.Rows[0]["SLAST_REQ_ID"] + "") + "' , SLAST_REQ_ID = '" + CommonFunction.ReplaceInjection(!string.IsNullOrEmpty(sReqID) ? sReqID : sREQUESTID) + "' " + ConditionDPrev + ",CACTIVE = 'Y' WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
                    SystemFunction.SQLExecuteNonQuery(strConn, QUERY);
                    #endregion

                    //#region Update SAP
                    //string STRUCK = dtMainData.Rows[0]["STRUCKID"] + "";
                    //string VEH_NO = CommonFunction.Get_Value(new OracleConnection(strConn), "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");
                    //string ScarType = dtMainData.Rows[0]["SCARTYPEID"] + "";
                    //string sMsg = "";
                    //if (VEH_NO.Trim() != "" && cSAP_SYNC == "1") // cSAP_SYNC = 1 ให้มีการ Syncข้อมูลไปยัง SAP
                    //{
                    //    try
                    //    {
                    //        SAP_SYNCOUT_SI veh_syncout = new SAP_SYNCOUT_SI();
                    //        veh_syncout.VEH_NO = VEH_NO + "";
                    //        sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    //        if (sMsg != "")
                    //        {
                    //            //กรณีที่มีการอัพเดทข้อมูลรถ แต่รถยังไม่มีใน SAP
                    //            if (sMsg.Contains("does not exist"))
                    //            {
                    //                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                    //                //กรณีที่หัวไม่มีใน SAP
                    //                if (sMsg.Contains("Entry " + VEH_NO + " does not exist"))
                    //                {
                    //                    //รถบรรทุก
                    //                    if (ScarType == "0")
                    //                    {
                    //                        veh_create.sVehicle = VEH_NO + "";
                    //                        sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                    //                        if (sMsg.Contains("Transport unit " + VEH_NO + " already exists"))
                    //                        {
                    //                            veh_create.sVehicle = VEH_NO + "";
                    //                            //sMsg = veh_create.CRT_Vehicle_SOS();
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        veh_create.sVehicle = VEH_NO + "";
                    //                        //sMsg = veh_create.CRT_Vehicle_SOS();
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    veh_create.sVehicle = VEH_NO + "";
                    //                    sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                    //                }
                    //            }
                    //        }

                    //        if (sMsg == "")
                    //        {

                    //        }
                    //        else
                    //        {

                    //            Message_error = sMsg;


                    //        }
                    //        SystemFunction.SynSapError("RESULT-ADD5", sMsg);
                    //    }
                    //    catch (Exception ect)
                    //    {

                    //        Message_error = "ไม่สามารถส่งข้อมูลไปยัง SAP ได้";

                    //        SystemFunction.SynSapError("RESULT-ADD5", ect.ToString());
                    //    }
                    //}
                    //#endregion
                }
                // }
            }


        }
        else
        {
            sql = @"UPDATE TBL_REQUEST SET STATUS_FLAG = '" + CommonFunction.ReplaceInjection(_sStatusID) + "', RESULT_TRY = '" + CommonFunction.ReplaceInjection(_sTry) + "', CLOSEJOB_DATE = SYSDATE  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
        }

        //if (string.IsNullOrEmpty(Message_error))
        //{
        SystemFunction.SQLExecuteNonQuery(strConn, sql);
        //}
        return Message_error;
    }

    private void Update_DOC(string sReqID, string _sStatus)
    {
        string UPDATE = @"UPDATE TBL_REQDOC
SET    
       CONSIDER     = '" + _sStatus + @"'
WHERE  REQUEST_ID   = '" + sReqID + @"'
AND    DOC_TYPE  in ('0005','0006')";
        SystemFunction.SQLExecuteNonQuery(strConn, UPDATE);

    }

    //ส่งเมลล์
    private bool SendMailToUser(string sREQID)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "แจ้งผลการตรวจสอบสภาพรถและตรวจรับรองความถูกต้อง";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = SystemFunction.GetUserMailForSend(USER_ID, STATUSFLAG_ID, VENDOR_ID, "M");
        }

        #region html

        string CarType = "";
        string RegisID = "";
        string CHASSIS = "";
        string NWHEALL = "";
        string TOTLE_SLOT = "";
        string TOTALCAP = "";
        string DPREV_SERV = "";
        string DLAST_SERV = "";
        string SCAR_NUM = "";
        if (dtMainData.Rows.Count > 0)
        {
            CarType = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? "หัว" : "หาง");
            RegisID = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            CHASSIS = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_CHASSIS"] + "" : dtMainData.Rows[0]["TU_CHASSIS"] + "");
            NWHEALL = dtMainData.Rows[0]["NWHEELS"] + "";
            TOTLE_SLOT = dtMainData.Rows[0]["TOTLE_SLOT"] + "";
            TOTALCAP = dtMainData.Rows[0]["TOTLE_CAP"] + "";
            DPREV_SERV = dtMainData.Rows[0]["DPREV_SERV"] + "";
            DLAST_SERV = dtMainData.Rows[0]["DLAST_SERV"] + "";
            SCAR_NUM = dtMainData.Rows[0]["SCAR_NUM"] + "";
        }

        if (rblStatusChecking.SelectedIndex == 0)
        {
            //{0} รีเควสไอดี{1}บริษัท{2}รหัสรถ{3}ทะเบียนหัวหรือหาง{4}ทะเบียนรถ{5}เลขแชชซี{6}ชนิดรถ(เช่น 18ล้อ){7}จำนอนช่อง{8}ความจุรวม{9}เข้ามาวัดน้ำครั้งสุดท้าบ{10}ตรวจสอบวัดน้ำล่าสุด
            sHTML = string.Format(SystemFunction.Form_Email("5", USER_ID), sREQID, lblVendorName.Text, SCAR_NUM, CarType, RegisID, CHASSIS, NWHEALL, TOTLE_SLOT, TOTALCAP, DPREV_SERV, DLAST_SERV);
        }
        else
        {
            #region ตรวจภายนอกภายใน


            DataTable dtCheckTruck = CommonFunction.Get_Data(strConn, @"SELECT MS.CHECKLIST_ID,MS.CHECKLIST_NAME,LAW.ITEM1_VAL,LAW.ITEM2_VAL,LAW.ITEM3_VAL,MS.RD_STATUSCT,REQUEST_ID,
CASE 
WHEN LAW.ITEM1_VAL = 'N' THEN MS.CHECKLIST_NAME 
WHEN LAW.ITEM1_VAL <> 'N' AND  LAW.ITEM2_VAL = 'N'   THEN REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL) 
WHEN LAW.ITEM1_VAL <> 'N' AND LAW.ITEM2_VAL <> 'N' THEN  REPLACE((REPLACE(MS.CHECKLIST_NAME,'{0}',LAW.ITEM1_VAL)),'{1}',LAW.ITEM2_VAL )
ELSE ''
END
AS DETAIL FROM TBL_CHECKLIST MS
LEFT JOIN TBL_SCRUTINEERINGLIST LAW
ON  MS.CHECKLIST_ID = LAW.CHECKLIST_ID
WHERE LAW.ITEM1_VAL = 'N' OR LAW.ITEM2_VAL = 'N'  OR (LAW.ITEM3_VAL = 'N'  AND MS.RD_STATUSCT = 'R' ) AND  REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

            string HTML11 = "";
            if (dtCheckTruck.Rows.Count > 0)
            {
                HTML11 = "1. ตรวจสภาพภายใน/ภายนอก พบว่า<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                for (int i = 0; i < dtCheckTruck.Rows.Count; i++)
                {
                    HTML11 += "-" + dtCheckTruck.Rows[i]["DETAIL"] + " ไม่ผ่าน <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }

            }

            #endregion

            #region ตรวจสอบวัดน้ำล่าสุด
            string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
,CASE WHEN CCP.SEAL_ERR = '1' THEN '/' ELSE 'X' END  as  SEAL_ERR
,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
ELSE '' END AS MANHOLD
,CASE WHEN INC.RESULT = '1' THEN '/' ELSE 'X' END  as  RESULT
,CASE WHEN INC.PAN_ERR = '1' THEN '/' ELSE 'X' END  as  PAN_ERR
,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' ELSE 'X' END  as  PASSSTANDARD
FROM TBL_CHECKING_COMPART  CCP
LEFT JOIN 
(
    SELECT REQ_ID,COMPART_NO
    ,MAX(PAN_LEVEL) as PAN_LEVEL
    ,PAN_ERR
    ,PASSSTANDARD
    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
    FROM TBL_INNER_CHECKINGS
    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
)INC
 ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + @"'
ORDER BY CCP.COMPART_NO ASC";

            DataTable dtNopass = CommonFunction.Get_Data(strConn, SQL_NOPASS);
            string HTML12 = "";
            if (dtNopass.Rows.Count > 0)
            {
                HTML12 = "2.ตรวจสภาพลงน้ำเพื่อติดตั้งแป้นระดับ พบว่า<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                HTML12 = "<table width='100%' border='1' cellpadding='3' cellspacing='1'>";

                HTML12 += @"<tr><td width='100px' align='center'>ตรวจแป้นช่องที่</td><td width='100px' align='center'>ตีซีล แป้น</td><td width='100px' align='center'>ตีซีล ขอบฝา</td><td width='100px' align='center'>วัดระยะแผ่นเพลท</td><td width='100px' align='center'>สภาพแป้น</td><td width='100px' align='center'>เทียบแป้นอยู่ในเกณฑ์</td></tr>";
                for (int i = 0; i < dtNopass.Rows.Count; i++)
                {
                    HTML12 += @"<tr><td align='center'>" + dtNopass.Rows[i]["COMPART_NO"] + "</td><td align='center'>" + dtNopass.Rows[i]["SEAL_ERR"] + "</td><td align='center'>" + dtNopass.Rows[i]["MANHOLD"] + "</td><td align='center'>" + dtNopass.Rows[i]["RESULT"] + "</td><td align='center'>" + dtNopass.Rows[i]["PAN_ERR"] + "</td><td align='center'>" + dtNopass.Rows[i]["PASSSTANDARD"] + "</td></tr>";
                }

                HTML12 += "</table>";

            }
            #endregion

            //{0} รีเควสไอดี{1}บริษัท{2}รหัสรถ{3}ทะเบียนหัวหรือหาง{4}ทะเบียนรถ{5}เลขแชชซี{6}ชนิดรถ(เช่น 18ล้อ){7}จำนอนช่อง{8}ความจุรวม{9}เข้ามาวัดน้ำครั้งสุดท้าบ{10}ตรวจสอบวัดน้ำล่าสุด{11}ตรวจสภาพภายใน/ภายนอก{12}ตรวจสภาพลงน้ำเพื่อติดตั้งแป้นระดับ{13}ความเห็นผู้ตรวจสอบ
            sHTML = string.Format(SystemFunction.Form_Email("5", USER_ID), sREQID, lblVendorName.Text, SCAR_NUM, CarType, RegisID, CHASSIS, NWHEALL, TOTLE_SLOT, TOTALCAP, DPREV_SERV, DLAST_SERV, HTML11, HTML12, GetComment(sREQID));
        }
        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(strConn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
        //return false;
    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(strConn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, strConn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    #region class
    [Serializable]
    class TData_File
    {
        public decimal nFileID { get; set; }
        public string sSysFileName { get; set; }
        public string sNameShow { get; set; }
        public string sPath { get; set; }
        public string sOpenFile { get; set; }
        public decimal nOrder { get; set; }
        public string sFileType { get; set; }
        public string sNewFile { get; set; } // ไฟล์ใหม่หรือไฟล์เดิม
        public string sConsider { get; set; } // สถานะเอกสาร // Y = ตรวจสอบผ่านแล้ว, N = ไม่ผ่าน/ยังไม่ตรวจาสอบ

        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string sAllowEdit { get; set; }
    }
    #endregion

}