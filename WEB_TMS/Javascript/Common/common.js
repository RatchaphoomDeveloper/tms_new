/*
##########################################
############## History info ##############
##########################################

Create by       : Dy
Create date     : 15/02/2012
Version         : 1.0.0
Description     : Common function for use on all page

Modify by       : Pandy 
Modify data     : 02/07/2012 
Description     : Common function for use on all page (DevExpress)

Modify by       : - (for modify)
Modify data     : - (for modify)
Description     : - (for modify)
*/

function GetCurrentPageName() {
    var fullPath = window.location.pathname;
    var filePage = fullPath.substring(fullPath.lastIndexOf('/') + 1);
    return filePage.toLowerCase()
}

function checkBeforeDeleteRowxPopupImg(xGridView, referenceFunctionPressYes, referenceFunctionPressNo) {
    if (xGridView.GetSelectedRowCount() > 0) {
        dxConfirm('ยืนยันการดำเนินการ', 'ต้องการดำเนินการลบรายการหรือไม่', referenceFunctionPressYes, referenceFunctionPressNo);
    }
    else {
        dxWarning('แจ้งให้ทราบ', 'กรุณาเลือกรายการที่ต้องการลบ');
    }
}

function checkBeforeDelete(referenceFunctionPressYes, referenceFunctionPressNo) {

    dxConfirm('ยืนยันการดำเนินการ', 'ต้องการดำเนินการลบรายการหรือไม่', referenceFunctionPressYes, referenceFunctionPressNo);
}
function checkBeforeReSaveRequest(referenceFunctionPressYes, referenceFunctionPressNo) {

    dxConfirm('ยืนยันการดำเนินการ', 'การแก้ไขครั้งนี้คุณต้องดำเนินการแนบไฟล์ใหม่ คุณต้องการดำเนินการต่อหรือไม่', referenceFunctionPressYes, referenceFunctionPressNo);
}
function dxInfo(shead, smsg) {
    dxPopupInfo.SetHeaderText(shead);
    xlblpopupMsgInfo.SetText(smsg);
    ximginfo.SetImageUrl("Images/dialog-information.png");
    dxPopupInfo.Show()
}
function dxInfoRedirect(shead, smsg, referenceFunctionPressYes) {
    xbtnPopupOKInfo.Click.ClearHandlers();
    dxPopupInfo.SetHeaderText(shead);
    xlblpopupMsgInfo.SetText(smsg);
    ximginfo.SetImageUrl("Images/dialog-information.png");
    dxPopupInfo.Show()
    xbtnPopupOKInfo.Click.AddHandler(referenceFunctionPressYes);
   
}
function dxError(shead, smsg) {
    dxPopupError.SetHeaderText(shead);
    xlblpopupMsgError.SetText(smsg);
    ximgerror.SetImageUrl("Images/dialog-error.png");
    dxPopupError.Show()
}

function dxWarning(shead, smsg) {

    dxPopupWarning.SetHeaderText(shead);
    xlblpopupMsgWarning.SetText(smsg);
    ximgwarning.SetImageUrl("Images/dialog-warning.png");
    dxPopupWarning.Show()
   
}

function dxWarningRedirect(shead, smsg, referenceFunctionPressYes) {
    xbtnPopupOKWarning.Click.ClearHandlers();
    dxPopupWarning.SetHeaderText(shead);
    xlblpopupMsgWarning.SetText(smsg);
    ximgwarning.SetImageUrl("Images/dialog-warning.png");
    dxPopupWarning.Show()
    xbtnPopupOKWarning.Click.AddHandler(referenceFunctionPressYes);

}

function dxConfirm(shead, smsg, referenceFunctionPressYes, referenceFunctionPressNo) {

    xbtnPopupSubmit.Click.ClearHandlers();
    xbtnPopupCancel.Click.ClearHandlers();
    dxPopupConfirm.SetHeaderText(shead);
    xlblpopupMsgConfirm.SetText(smsg);
    ximgconfirm.SetImageUrl("Images/dialog-help.png");
    dxPopupConfirm.Show();
    xbtnPopupSubmit.Click.AddHandler(referenceFunctionPressYes);
    xbtnPopupCancel.Click.AddHandler(referenceFunctionPressNo);
}

function GetImbPopup(smode) {
    var urlimg = "";
    var _smode = smode;
    switch (_smode.toLowerCase()) {
        case "info": urlimg = "Images/dialog-information.png"; break;
        case "error": urlimg = "Images/dialog-error.png"; break;
        case "warning": urlimg = "Images/dialog-warning.png"; break;
        case "confirm": urlimg = "Images/dialog-help.png"; break;
        default: urlimg = "Images/dialog-information.png"; break;
    }
    return urlimg;
}
function dxConfirmPrint(shead, smsg, referenceFunctionPressYes, referenceFunctionPressNo) {
    xbtnPopupPrintSubmit.Click.ClearHandlers();
    xbtnPopupPrintCancel.Click.ClearHandlers();
    dxpopupPrint.SetHeaderText(shead);
    /*xlblpopupMsgConfirm.SetText(smsg);*/
    ximgconfirm.SetImageUrl("Images/dialog-help.png");
    dxpopupPrint.Show();
    xbtnPopupPrintSubmit.Click.AddHandler(referenceFunctionPressYes);
    xbtnPopupPrintCancel.Click.AddHandler(referenceFunctionPressNo);

}


/**
* This method will give open the popup without a warning.
*/
function performSyncronousRequest(url) {
    window.open(url, '_blank');

}