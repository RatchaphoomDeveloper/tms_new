function boxOverlayCSS() {
    $('#BoxOverlay').css({
        height: $(document).height(),
        width: $(document).width()
    }).hide();
}

function jAlertBox(headerMessage, bodyMessage) {
    boxOverlayCSS();
    Sexy.alert("<br/><h1>&nbsp;&nbsp;" + headerMessage + "</h1><br/><p>&nbsp;&nbsp;&nbsp;" + bodyMessage + "</p>");
}

function jInfoBox(headerMessage, bodyMessage) {
    boxOverlayCSS();
    Sexy.info("<br/><h1>&nbsp;&nbsp;" + headerMessage + "</h1><br/><p>&nbsp;&nbsp;&nbsp;" + bodyMessage + "</p>");
}

function jErrorBox(headerMessage, bodyMessage) {
    boxOverlayCSS();
    Sexy.error("<br/><h1>&nbsp;&nbsp;" + headerMessage + "</h1><br/><p>&nbsp;&nbsp;&nbsp;" + bodyMessage + "</p>");
}

function jInfoSuccessBox(headerMessage, bodyMessage, referenceFunction) {
    boxOverlayCSS();
    Sexy.info("<br/><h1>&nbsp;&nbsp;" + headerMessage + "</h1><br/><p>&nbsp;&nbsp;&nbsp;" + bodyMessage + "</p>",
    { onComplete:
			function (returnvalue) {
			    if (returnvalue) {
			        referenceFunction();
			    }
			}
    });
}

function jConfirmBox(headerMessage, bodyMessage, referenceFunctionPressYes, referenceFunctionPressNo) {
    boxOverlayCSS();
    Sexy.confirm("<br/><h1>&nbsp;&nbsp;" + headerMessage + "</h1><br/><p>&nbsp;&nbsp;&nbsp;" + bodyMessage + "</p>",
		{ onComplete:
			function (returnvalue) {
			    if (returnvalue) {
			        referenceFunctionPressYes();
			    }
			    else {
			        referenceFunctionPressNo();
			    }
			}
		});
}