﻿/*
function สำหรับช่วยในการ ใช้งาน ZebraDialog 
*/
function ShowZAlertWithRedirect(message,url) {
    $.Zebra_Dialog(message, {
        'type': 'information',
        'title': 'Redirect',
        'buttons': [{ caption: 'Yes', callback: function () { window.location=url } }]
    });
}

function ShowZAlertSuccessWithRedirect(message, url) {
    $.Zebra_Dialog(message, {
        'type': 'information',
        'title': 'Information',
        'buttons': [{ caption: 'Yes', callback: function () { window.location = url } }]
    });
}

function ShowZConfirmDialog(btn, message,title) {
    $.Zebra_Dialog(message, {
        'type': 'question',
        'title': title,
        'buttons': [{ caption: 'No' },
                            { caption: 'Yes', callback: function () { __doPostBack('' + btn, '') } }]
    });
}
function ShowZErrorDialog(message,title) {
    $.Zebra_Dialog(message, {
        'type': 'error',
        'title': title,
        'buttons': [{ caption: 'Close'}]
    });
}
function ShowZWarningDialog(message,title) {
    $.Zebra_Dialog(message, {
        'type': 'warning',
        'title': title,
        'buttons': [{ caption: 'Close'}]
    });
}
function ShowZInformDialog(message,title) {
    $.Zebra_Dialog(message, {
        'type': 'information',
        'title': title,
        'width': 1000,
        'height':900,
        'buttons': [{ caption: 'Close'}]
    });
}
function ShowZAlert(message) {
    $.Zebra_Dialog(message);
}
