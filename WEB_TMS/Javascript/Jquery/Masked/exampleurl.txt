http://digitalbush.com/projects/masked-input-plugin/

//First, include the jQuery and masked input javascript files.
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.maskedinput.js" type="text/javascript"></script>

jQuery(function($){
   $("#date").mask("99/99/9999");
   $("#phone").mask("(999) 999-9999");
   $("#tin").mask("99-9999999");
   $("#ssn").mask("999-99-9999");
});

//Optionally, if you are not satisfied with the underscore ('_') character as a placeholder, you may pass an optional argument to the maskedinput method.
jQuery(function($){
   $("#product").mask("a*-999-a999",{placeholder:" "});
});

//Optionally, if you would like to execute a function once the mask has been completed, you can specify that function as an optional argument to the maskedinput method.
jQuery(function($){
   $("#product").mask("a*-999-a999",{completed:function(){alert("You typed the following: "+this.val());}});
});

//You can now supply your own mask definitions.
jQuery(function($){
   $.mask.definitions['~']='[+-]';
   $("#eyescript").mask("~9.99 ~9.99 999");
});

//You can have part of your mask be optional. Anything listed after '?' within the mask is considered optional user input. The common example for this is phone number + optional extension.
jQuery(function($){
   $("#phone").mask("(999) 999-9999? x99999");
});