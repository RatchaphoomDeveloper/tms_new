﻿/* Add nullText on Combo Autocomplete*/
var nullText = "-- ทั้งหมด --";

function OnLostFocus(s, e) {
    if (s.GetValue() != "" && s.GetValue() != null)
        return;
    var input = s.GetInputElement();
    input.style.color = "gray";
    input.value = $('#' + input.id.replace('_I', '')).attr("nulltext");
}

function OnGotFocus(s, e) {
    var input = s.GetInputElement();
    if (input.value == $('#' + input.id.replace('_I', '')).attr("nulltext")) {
        input.value = "";
        input.style.color = "black";
    }
}

function OnInit(s, e) {
    OnLostFocus(s, e);
}