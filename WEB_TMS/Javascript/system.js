﻿//--23/3/2555 Softhai Intranet
function isNumber(e, ctrID) {
    var charCode = (e.which) ? e.which : window.event.keyCode;
    if (charCode == 8) return true;
    var keyChar = String.fromCharCode(charCode);
    var re = /^\d+$/
    return re.test(ctrID.value + keyChar);
}
function isDecimal(e, ctrID) {
    var charCode = (e.which) ? e.which : window.event.keyCode;
    //alert(charCode)
    if (charCode == 8) return true;
    var keyChar = String.fromCharCode(charCode);
    var re = /^\d+$|[.]/
    return re.test(ctrID.value + keyChar);
}
function alertDelete(referenceFunctionPressYes, referenceFunctionPressNo) {
    jConfirmBox('ยืนยันการดำเนินการ', 'ต้องการดำเนินการลบรายการหรือไม่', referenceFunctionPressYes, referenceFunctionPressNo);
    
}