﻿function CalculateRow(Index, SUMTYPE, PAHSE1, PAHSE2, PAHSE3, PAHSE4, PAHSE5) {

    var SVAL = null;

    if (SUMTYPE == "0") {
        SVAL = SumAVG(Index);
    }
    else if (SUMTYPE == "1") {
        SVAL = SumLow(Index);
    }
    else if (SUMTYPE == "2") {
        SVAL = SumHeight(Index);
    }

    alert(SVAL);

    if (CheckRank(SVAL, PAHSE1, "", SUMTYPE)) {
        alert('1');
    }
    else if (CheckRank(SVAL, PAHSE2, PAHSE1, SUMTYPE)) {
        alert('2');
    }
    else if (CheckRank(SVAL, PAHSE3, PAHSE2, SUMTYPE)) {
        alert('3');
    }
    else if (CheckRank(SVAL, PAHSE4, PAHSE3, SUMTYPE)) {
        alert('4');
    }
    else if (CheckRank(SVAL, PAHSE5, PAHSE4, SUMTYPE)) {
        alert('5');
        alert('NOELSE');
    }
    else {
        alert('5');
    }
}
/*หาค่าเฉลี่ย*/
function SumAVG(Index) {
    var divide = 0;
    var SUM = 0;

    for (var i = 1; i < 13; i++) {


        if (parseFloat(window["txtM" + i + "_" + Index + ""] + "") != undefined && window["txtM" + i + "_" + Index + ""].GetValue() != null) {
            divide = parseFloat(divide) + 1;
            SUM = parseFloat(window["txtM" + i + "_" + Index + ""].GetValue()) + parseFloat(SUM);
            /* SUM = window["txtM" + i + "_" + Index + ""].GetValue() + SUM;*/


        }

    }

    return (SUM / divide).toFixed(0);
}

/*หาค่าต่ำ*/
function SumLow(Index) {

    var SUM = null;

    for (var i = 1; i < 13; i++) {



        if (window["txtM" + i + "_" + Index + ""] != undefined && window["txtM" + i + "_" + Index + ""].GetValue() != null) {

            /*เช็คว่าถ้าค่า SUM เป็น null ให้ใส่ค่าครั้งแรกไปเพื่อใช้เทียบ*/
            if (SUM == null) {
                SUM = window["txtM" + i + "_" + Index + ""].GetValue();
            }

            if (parseFloat(SUM) > parseFloat(window["txtM" + i + "_" + Index + ""].GetValue())) {
                SUM = window["txtM" + i + "_" + Index + ""].GetValue();
            }
            else {

            }

        }

    }

    return parseFloat(SUM).toFixed(0);
}

/*หาค่าต่ำ*/
function SumHeight(Index) {

    var SUM = null;

    for (var i = 1; i < 13; i++) {



        if (window["txtM" + i + "_" + Index + ""] != undefined && window["txtM" + i + "_" + Index + ""].GetValue() != null) {

            /*เช็คว่าถ้าค่า SUM เป็น null ให้ใส่ค่าครั้งแรกไปเพื่อใช้เทียบ*/
            if (SUM == null) {
                SUM = window["txtM" + i + "_" + Index + ""].GetValue();
            }

            if (parseFloat(SUM) < parseFloat(window["txtM" + i + "_" + Index + ""].GetValue())) {
                SUM = window["txtM" + i + "_" + Index + ""].GetValue();
            }
            else {

            }

        }

    }

    return parseFloat(SUM).toFixed(0);
}

/*เช็ค Rank ถ้าใช่ return true ไม่ใช่ return false*/
function CheckRank(SVAL, PAHSE, PAHSEOLD, SUMTYPE) {

    var VAL1 = null;
    var VAL2 = null;
    /*เช็คว่าค่าที่ส่งมาเป็นแบบช่วง หรือแบบเดี่ยว*/
    var Rank = PAHSE + "";
    if (Rank.indexOf("-") > -1) {

        var res = Rank.split("-");

        VAL1 = res[0];
        VAL2 = res[1];


        if (parseFloat(VAL1) <= parseFloat(SVAL) && parseFloat(VAL2) >= parseFloat(SVAL)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        /*เช็คว่า PAHSEOLD มีค่าไหมไม่มีแสดงว่าเป็นค่าแรก*/
        if (PAHSEOLD == "") {
            if (parseFloat(PAHSE) >= parseFloat(SVAL)) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            /*1 หาจากค่าต่ำสุด 0 หาจากค่าเฉลี่ย 2 หาจากค่าสูงสุด*/
            var RankOLD = PAHSEOLD + "";
            /*เช็คว่าตัวเก่ามาเดี่ยวหรือมาแบบช่วง*/
            if (RankOLD.indexOf("-") > -1) {

            }
            else {
                /**/
                if (SUMTYPE == 1) {
                    if (parseFloat(SVAL) >= parseFloat(PAHSE) && parseFloat(PAHSEOLD) > parseFloat(SVAL)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (parseFloat(SVAL) <= parseFloat(PAHSE) && parseFloat(PAHSEOLD) < parseFloat(SVAL)) {
                        return true;
                    }
                    else {
                        return false;
                    }

                }
            }
        }
    }
}
