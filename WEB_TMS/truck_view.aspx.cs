﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class truck_view : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                ViewState["STRUCKID"] = QueryString[1];
                ViewState["RTRUCKID"] = QueryString[2];
                ViewState["SCARTYPEID"] = QueryString[3];
                ViewState["SCHASIS"] = QueryString[4];
                ViewState["RCHASIS"] = QueryString[5];
                SetData();
            }
        }
    }
    protected void xbnChangeMode_Click(object sender, EventArgs e)
    {
        string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("Edit&" + ViewState["STRUCKID"].ToString() + "&" + ViewState["RTRUCKID"].ToString() + "&" + ViewState["SCARTYPEID"].ToString() + "&" + ViewState["SCHASIS"].ToString() + "&" + ViewState["RCHASIS"].ToString()))
                , sUrl = "truck_edit.aspx?str=";
        Response.Redirect(sUrl + sEncrypt);
    }
    private void SetData()
    {
        string sTRUCKID = ViewState["STRUCKID"].ToString(), sCHASIS = ViewState["SCHASIS"].ToString();
        string rTRUCKID = ViewState["RTRUCKID"].ToString(), rCHASIS = ViewState["RCHASIS"].ToString();

        #region Get Data
        string _sql = "";

        DataTable dt_STRUCK = new DataTable(); DataTable dt_STRUCK_Capacity = new DataTable(); DataTable dt_STRUCK_Doc = new DataTable();
        DataTable dt_STRUCK_Insure = new DataTable(); DataTable dt_STRUCK_InsureDoc = new DataTable();
        DataTable dt_STRUCK_MWaterDoc = new DataTable();
        DataTable dt_STRUCK_BLACKLIST = new DataTable();
        DataTable dt_RTRUCK = new DataTable(); DataTable dt_RTRUCK_Capacity = new DataTable(); DataTable dt_RTRUCK_Doc = new DataTable();
        DataTable dt_RTRUCK_Insure = new DataTable(); DataTable dt_RTRUCK_InsureDoc = new DataTable();

        if (Session["DT_TRUCK"] != null)
        {
            DataTable[] DT_TRUCK = (DataTable[])Session["DT_TRUCK"];

            dt_STRUCK = DT_TRUCK[0];
            dt_STRUCK_Capacity = DT_TRUCK[1];
            dt_STRUCK_Doc = DT_TRUCK[2];
            dt_STRUCK_Insure = DT_TRUCK[3];
            dt_STRUCK_InsureDoc = DT_TRUCK[4];
            dt_STRUCK_MWaterDoc = DT_TRUCK[5];
            dt_STRUCK_BLACKLIST = DT_TRUCK[6];

            dt_RTRUCK = DT_TRUCK[7];
            dt_RTRUCK_Capacity = DT_TRUCK[8];
            dt_RTRUCK_Doc = DT_TRUCK[9];
            dt_RTRUCK_Insure = DT_TRUCK[10];
            dt_RTRUCK_InsureDoc = DT_TRUCK[11];
        }

        if (dt_STRUCK == null || dt_STRUCK.Rows.Count <= 0)
        {
            DataTable[] DT_TRUCK = new DataTable[12];
            //ข้อมูลส่วนหัว
            _sql = @"SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,PT.SPRODUCTTYPENAME ,CT.SCONTRACTNO ,V.SABBREVIATION FROM TTRUCK T 
                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID 
                    LEFT JOIN TPRODUCTTYPE PT ON T.SPROD_GRP=PT.SPRODUCTTYPEID 
                    LEFT JOIN TCONTRACT CT ON T.SCONTRACTID=CT.SCONTRACTID
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID=V.SVENDORID
                    WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[0] = dt_STRUCK;
            _sql = "SELECT * FROM TTRUCK_COMPART WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_Capacity = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[1] = dt_STRUCK_Capacity;
            _sql = "SELECT * FROM TTRUCK_DOC WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"' AND CACTIVE='1'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_Doc = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[2] = dt_STRUCK_Doc;
            _sql = "SELECT * FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_Insure = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[3] = dt_STRUCK_Insure;
            _sql = "SELECT * FROM TTRUCK_INSUREDOC WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"' AND CACTIVE='1'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_InsureDoc = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[4] = dt_STRUCK_InsureDoc;

            //ข้อมูลเอกสารการวัดน้ำ
            _sql = "SELECT * FROM TTRUCK_MWATERDOC WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"' AND CACTIVE='1'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_MWaterDoc = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[5] = dt_STRUCK_MWaterDoc;

            //ข้อมูลประวัติการระงับใช้งาน
            _sql = "SELECT * FROM TTRUCK_BLACKLIST WHERE STRUCKID='" + CommonFunction.ReplaceInjection(sTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(sCHASIS) + @"'";
            dt_STRUCK_BLACKLIST = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[6] = dt_STRUCK_BLACKLIST;

            //ข้อมูลส่วนหาง
            _sql = @"SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,PT.SPRODUCTTYPENAME FROM TTRUCK T 
                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID 
                    LEFT JOIN TPRODUCTTYPE PT ON T.SPROD_GRP=PT.SPRODUCTTYPEID 
                    WHERE STRUCKID='" + CommonFunction.ReplaceInjection(rTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(rCHASIS) + @"'";
            dt_RTRUCK = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[7] = dt_RTRUCK;
            _sql = "SELECT * FROM TTRUCK_COMPART WHERE STRUCKID='" + CommonFunction.ReplaceInjection(rTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(rCHASIS) + @"'";
            dt_RTRUCK_Capacity = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[8] = dt_RTRUCK_Capacity;
            _sql = "SELECT * FROM TTRUCK_DOC WHERE STRUCKID='" + CommonFunction.ReplaceInjection(rTRUCKID) + @"' AND CACTIVE='1'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(rCHASIS) + @"'";
            dt_RTRUCK_Doc = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[9] = dt_RTRUCK_Doc;
            _sql = "SELECT * FROM TTRUCK_INSURANCE WHERE STRUCKID='" + CommonFunction.ReplaceInjection(rTRUCKID) + @"'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(rCHASIS) + @"'";
            dt_RTRUCK_Insure = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[10] = dt_RTRUCK_Insure;
            _sql = "SELECT * FROM TTRUCK_INSUREDOC WHERE STRUCKID='" + CommonFunction.ReplaceInjection(rTRUCKID) + @"' AND CACTIVE='1'";// AND SCHASIS='" + CommonFunction.ReplaceInjection(rCHASIS) + @"'";
            dt_RTRUCK_InsureDoc = CommonFunction.Get_Data(conn, _sql); DT_TRUCK[11] = dt_RTRUCK_InsureDoc;

            Session["DT_TRUCK"] = DT_TRUCK;
        }
        #endregion

        #region HeaderText RoundPanel
        string sCarTypeID = dt_STRUCK.Rows[0]["SCARTYPEID"].ToString();
        if (sCarTypeID == "3" || sCarTypeID == "5")
        {
            dvHead.Visible = true;
            rpnHInfomation.HeaderText = "ข้อมูล" + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnHStatute.HeaderText = "พรบ." + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnHInsurance.HeaderText = "การประกันภัย" + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            dvTrail.Visible = true;
            rpnTInfomation.HeaderText = "ข้อมูล" + dt_RTRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnTStatute.HeaderText = "พรบ." + dt_RTRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnTInsurance.HeaderText = "การประกันภัย" + dt_RTRUCK.Rows[0]["SCARTYPENAME"].ToString();
        }
        else
        {
            dvHead.Visible = true;
            rpnHInfomation.HeaderText = "ข้อมูล" + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnHStatute.HeaderText = "พรบ." + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            rpnHInsurance.HeaderText = "การประกันภัย" + dt_STRUCK.Rows[0]["SCARTYPENAME"].ToString();
            dvTrail.Visible = false;
        }
        #endregion

        #region ข้อมูลรถส่วนหัว

        if (dt_STRUCK.Rows.Count > 0)
        {
            xlbHRegNo.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SHEADREGISTERNO"].ToString()) ? dt_STRUCK.Rows[0]["SHEADREGISTERNO"].ToString() : "-";
            xlbdHRegNo.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["DREGISTER"].ToString()) ? Convert.ToDateTime(dt_STRUCK.Rows[0]["DREGISTER"].ToString()).ToString("dd/MM/yyyy", new CultureInfo("th-th")) : "-";
            DateTime dcurdate, dSignin;
            if (!string.IsNullOrEmpty(dt_STRUCK.Rows[0]["DSIGNIN"].ToString()))
            {
                dSignin = Convert.ToDateTime(dt_STRUCK.Rows[0]["DSIGNIN"].ToString());
                dcurdate = DateTime.Now.Date;
                double nPeroid = (dcurdate - dSignin).TotalDays;
                if (nPeroid / 365 >= 1)
                    xlbHnPeriod.Text = ((int)(nPeroid / 365)).ToString() + " ปี " + (nPeroid % 365 != 0 ? ((int)(nPeroid % 365) / 30).ToString() + " เดือน " : "");
                else
                {
                    if (nPeroid / 30 >= 1)
                        xlbHnPeriod.Text = ((int)(nPeroid / 30)).ToString() + " เดือน";
                    else
                        xlbHnPeriod.Text = ((int)(nPeroid)).ToString() + " วัน";
                }
            }
            else
                xlbHnPeriod.Text = "-";
            xlbHsChasis.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SCHASIS"].ToString()) ? dt_STRUCK.Rows[0]["SCHASIS"].ToString() : "-";
            xlbHsEngine.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SENGINE"].ToString()) ? dt_STRUCK.Rows[0]["SENGINE"].ToString() : "-";
            xlbHsBrand.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SBRAND"].ToString()) ? dt_STRUCK.Rows[0]["SBRAND"].ToString() : "-";
            xlbHsModel.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SMODEL"].ToString()) ? dt_STRUCK.Rows[0]["SMODEL"].ToString() : "-";
            xlbHnWheel.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NWHEELS"].ToString()) ? dt_STRUCK.Rows[0]["NWHEELS"].ToString() : "-";
            xlbHPowermover.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["POWERMOVER"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_STRUCK.Rows[0]["POWERMOVER"].ToString())) : "-";
            xlbHnShaftDriven.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NSHAFTDRIVEN"].ToString()) ? dt_STRUCK.Rows[0]["NSHAFTDRIVEN"].ToString() : "-";
            xlbHsVibration.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SVIBRATION"].ToString()) ? dt_STRUCK.Rows[0]["SVIBRATION"].ToString() : "-";
            xlbHPumpPower.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["PUMPPOWER"].ToString()) ? dt_STRUCK.Rows[0]["PUMPPOWER"].ToString() : "-";
            xlbHPumpPower_type.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["PUMPPOWER_TYPE"].ToString()) ? dt_STRUCK.Rows[0]["PUMPPOWER_TYPE"].ToString() : "-";
            xlbHMaterialOfPressure.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["MATERIALOFPRESSURE"].ToString()) ? dt_STRUCK.Rows[0]["MATERIALOFPRESSURE"].ToString() : "-";
            xlbHValveType.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["VALVETYPE"].ToString()) ? dt_STRUCK.Rows[0]["VALVETYPE"].ToString() : "-";
            xlbHFuelType.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["FUELTYPE"].ToString()) ? dt_STRUCK.Rows[0]["FUELTYPE"].ToString() : "-";
            xlbHGPSProvider.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["GPS_SERVICE_PROVIDER"].ToString()) ? dt_STRUCK.Rows[0]["GPS_SERVICE_PROVIDER"].ToString() : "-";
            xlbHnWeight.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NWEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_STRUCK.Rows[0]["NWEIGHT"].ToString())) : "-";
            if (sCarTypeID == "3") { xlbHnCap.Text = "-"; gvwHnCap.Visible = false; }
            else
            {
                xlbHnCap.Text = "";
                DataTable dtCompart = new DataTable();
                dtCompart.Columns.Add("Order", typeof(int));
                dtCompart.Columns.Add("nPanL1", typeof(int));
                dtCompart.Columns.Add("nPanL2", typeof(int));
                dtCompart.Columns.Add("nPanL3", typeof(int));
                if (dt_STRUCK_Capacity.Rows.Count > 0)
                {
                    for (int i = 1; i <= int.Parse(dt_STRUCK_Capacity.Compute("MAX(NCOMPARTNO)", string.Empty).ToString()); i++)
                    {
                        DataRow dr = dtCompart.NewRow();
                        dr["Order"] = i;
                        foreach (DataRow r in dt_STRUCK_Capacity.Select("NCOMPARTNO='" + i + "'", "NPANLEVEL"))
                        {
                            switch (r["NPANLEVEL"].ToString())
                            {
                                case "1":
                                    dr["nPanL1"] = r["NCAPACITY"].ToString();
                                    break;
                                case "2":
                                    dr["nPanL2"] = r["NCAPACITY"].ToString();
                                    break;
                                case "3":
                                    dr["nPanL3"] = r["NCAPACITY"].ToString();
                                    break;
                            }
                        }
                        dtCompart.Rows.Add(dr);
                    }
                    gvwHnCap.DataSource = dtCompart;
                    gvwHnCap.DataBind();
                }
            }
            xlbHnSlot.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NSLOT"].ToString()) ? dt_STRUCK.Rows[0]["NSLOT"].ToString() : "-";
            xlbHnTatolCapacity.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NTOTALCAPACITY"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_STRUCK.Rows[0]["NTOTALCAPACITY"].ToString())) : "-";
            xlbHTankMaterial.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["STANK_MATERAIL"].ToString()) ? dt_STRUCK.Rows[0]["STANK_MATERAIL"].ToString() : "-";
            xlbHLoadMethod.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SLOADING_METHOD"].ToString()) ? dt_STRUCK.Rows[0]["SLOADING_METHOD"].ToString() : "-";
            xlbHTank_Maker.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["STANK_MAKER"].ToString()) ? dt_STRUCK.Rows[0]["STANK_MAKER"].ToString() : "-";
            xlbHProdGRP.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SPRODUCTTYPENAME"].ToString()) ? dt_STRUCK.Rows[0]["SPRODUCTTYPENAME"].ToString() : "-";
            xlbHnLoadWeight.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NLOAD_WEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_STRUCK.Rows[0]["NLOAD_WEIGHT"].ToString())) : "-";
            xlbHCalcWeight.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["NCALC_WEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_STRUCK.Rows[0]["NCALC_WEIGHT"].ToString())) : "-";

            string hDoc01 = "", hDoc02 = "", hDoc03 = "", hDoc04 = "";
            foreach (DataRow dr in dt_STRUCK_Doc.Rows)
            {
                switch (dr["DOC_TYPE"].ToString())
                {
                    case "1":
                        hDoc01 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Reg/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "2":
                        hDoc02 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Tax_Evidence/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "3":
                        hDoc03 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Compartment/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "4":
                        hDoc04 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/First_Reg/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                }
            }
            xlbHDoc01.Text = !string.IsNullOrEmpty(hDoc01) ? hDoc01 : "-";
            xlbHDoc02.Text = !string.IsNullOrEmpty(hDoc02) ? hDoc02 : "-";
            xlbHDoc03.Text = !string.IsNullOrEmpty(hDoc03) ? hDoc03 : "-";
            xlbHDoc04.Text = !string.IsNullOrEmpty(hDoc04) ? hDoc04 : "-";

            //ข้อมูล พรบ.และการประกันภัย
            string hSDOC = "", hIDOC = "";
            foreach (DataRow dr in dt_STRUCK_Insure.Rows)
            {
                if (dr["CTYPE"].ToString() == "STATUTE")
                {
                    xlbHSCompany.Text = dr["SCOMPANY"].ToString();
                    xlbHSDuration.Text = dr["START_DURATION"].ToString() + " - " + dr["END_DURATION"].ToString();
                    xlbHSDetail.Text = dr["SDETAIL"].ToString();
                    foreach (DataRow r in dt_RTRUCK_InsureDoc.Select("CTYPE='STATUTE'"))
                        hSDOC += "<p>" + r["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Statute/" + r["DOC_NAME"].ToString() + "');'/></p>";
                    xlbHSDoc.Text = !string.IsNullOrEmpty(hSDOC) ? hSDOC : "-";
                }
                else
                {
                    xlbHICompany.Text = dr["SCOMPANY"].ToString();
                    xlbHIBudget.Text = string.Format("{0:#,###0}", double.Parse(dr["INSURE_BUDGET"].ToString()));
                    xlbHIType.Text = dr["INSURE_TYPE"].ToString();
                    xlbHIHolding.Text = dr["HOLDING"].ToString();
                    xlbHInInsure.Text = string.Format("{0:#,###0}", double.Parse(dr["NINSURE"].ToString()));
                    xlbHIDetail.Text = dr["SDETAIL"].ToString();
                    foreach (DataRow r in dt_RTRUCK_InsureDoc.Select("CTYPE='INSURANCE'"))
                        hIDOC += "<p>" + r["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Insurance/" + r["DOC_NAME"].ToString() + "');'/></p>";
                    xlbHIDoc.Text = !string.IsNullOrEmpty(hIDOC) ? hIDOC : "-";
                }
            }
        }
        #endregion

        #region ข้อมูลรถส่วนหาง

        if (dt_RTRUCK.Rows.Count > 0)
        {
            xlbRRegNo.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SHEADREGISTERNO"].ToString()) ? dt_RTRUCK.Rows[0]["SHEADREGISTERNO"].ToString() : "-";
            xlbdRRegNo.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["DREGISTER"].ToString()) ? Convert.ToDateTime(dt_RTRUCK.Rows[0]["DREGISTER"].ToString()).ToString("dd/MM/yyyy", new CultureInfo("th-th")) : "-";
            DateTime dcurdate, dSignin;
            if (!string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["DSIGNIN"].ToString()))
            {
                dSignin = Convert.ToDateTime(dt_RTRUCK.Rows[0]["DSIGNIN"].ToString());
                dcurdate = DateTime.Now.Date;
                double nPeroid = (dcurdate - dSignin).TotalDays;
                if (nPeroid / 365 >= 1)
                    xlbRnPeriod.Text = ((int)(nPeroid / 365)).ToString() + " ปี " + (nPeroid % 365 != 0 ? ((int)(nPeroid % 365) / 30).ToString() + " เดือน " : "");
                else
                {
                    if (nPeroid / 30 >= 1)
                        xlbRnPeriod.Text = ((int)(nPeroid / 30)).ToString() + " เดือน";
                    else
                        xlbRnPeriod.Text = ((int)(nPeroid)).ToString() + " วัน";
                }
            }
            else
                xlbRnPeriod.Text = "-";
            xlbRsChasis.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SCHASIS"].ToString()) ? dt_RTRUCK.Rows[0]["SCHASIS"].ToString() : "-";
            xlbRsEngine.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SENGINE"].ToString()) ? dt_RTRUCK.Rows[0]["SENGINE"].ToString() : "-";
            xlbRsBrand.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SBRAND"].ToString()) ? dt_RTRUCK.Rows[0]["SBRAND"].ToString() : "-";
            xlbRsModel.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SMODEL"].ToString()) ? dt_RTRUCK.Rows[0]["SMODEL"].ToString() : "-";
            xlbRnWheel.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NWHEELS"].ToString()) ? dt_RTRUCK.Rows[0]["NWHEELS"].ToString() : "-";
            xlbRPowermover.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["POWERMOVER"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_RTRUCK.Rows[0]["POWERMOVER"].ToString())) : "-";
            xlbRnShaftDriven.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NSHAFTDRIVEN"].ToString()) ? dt_RTRUCK.Rows[0]["NSHAFTDRIVEN"].ToString() : "-";
            xlbRsVibration.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SVIBRATION"].ToString()) ? dt_RTRUCK.Rows[0]["SVIBRATION"].ToString() : "-";
            xlbRPumpPower.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["PUMPPOWER"].ToString()) ? dt_RTRUCK.Rows[0]["PUMPPOWER"].ToString() : "-";
            xlbRPumpPower_type.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["PUMPPOWER_TYPE"].ToString()) ? dt_RTRUCK.Rows[0]["PUMPPOWER_TYPE"].ToString() : "-";
            xlbRMaterialOfPressure.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["MATERIALOFPRESSURE"].ToString()) ? dt_RTRUCK.Rows[0]["MATERIALOFPRESSURE"].ToString() : "-";
            xlbRValveType.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["VALVETYPE"].ToString()) ? dt_RTRUCK.Rows[0]["VALVETYPE"].ToString() : "-";
            xlbRFuelType.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["FUELTYPE"].ToString()) ? dt_RTRUCK.Rows[0]["FUELTYPE"].ToString() : "-";
            xlbRGPSProvider.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["GPS_SERVICE_PROVIDER"].ToString()) ? dt_RTRUCK.Rows[0]["GPS_SERVICE_PROVIDER"].ToString() : "-";
            xlbRnWeight.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NWEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_RTRUCK.Rows[0]["NWEIGHT"].ToString())) : "-";

            xlbRnCap.Text = "";
            DataTable dtCompart = new DataTable();
            dtCompart.Columns.Add("Order", typeof(int));
            dtCompart.Columns.Add("nPanL1", typeof(int));
            dtCompart.Columns.Add("nPanL2", typeof(int));
            dtCompart.Columns.Add("nPanL3", typeof(int));
            if (dt_RTRUCK_Capacity.Rows.Count > 0)
            {
                for (int i = 1; i <= int.Parse(dt_RTRUCK_Capacity.Compute("MAX(NCOMPARTNO)", string.Empty).ToString()); i++)
                {
                    DataRow dr = dtCompart.NewRow();
                    dr["Order"] = i;
                    foreach (DataRow r in dt_RTRUCK_Capacity.Select("NCOMPARTNO='" + i + "'", "NPANLEVEL"))
                    {
                        switch (r["NPANLEVEL"].ToString())
                        {
                            case "1":
                                dr["nPanL1"] = r["NCAPACITY"].ToString();
                                break;
                            case "2":
                                dr["nPanL2"] = r["NCAPACITY"].ToString();
                                break;
                            case "3":
                                dr["nPanL3"] = r["NCAPACITY"].ToString();
                                break;
                        }
                    }
                    dtCompart.Rows.Add(dr);
                }
                gvwRnCap.DataSource = dtCompart;
                gvwRnCap.DataBind();

            }
            xlbRnSlot.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NSLOT"].ToString()) ? dt_RTRUCK.Rows[0]["NSLOT"].ToString() : "-";
            xlbRnTatolCapacity.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NTOTALCAPACITY"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_RTRUCK.Rows[0]["NTOTALCAPACITY"].ToString())) : "-";
            xlbRTankMaterial.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["STANK_MATERAIL"].ToString()) ? dt_RTRUCK.Rows[0]["STANK_MATERAIL"].ToString() : "-";
            xlbRLoadMethod.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SLOADING_METHOD"].ToString()) ? dt_RTRUCK.Rows[0]["SLOADING_METHOD"].ToString() : "-";
            xlbRTank_Maker.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["STANK_MAKER"].ToString()) ? dt_RTRUCK.Rows[0]["STANK_MAKER"].ToString() : "-";
            xlbRProdGRP.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["SPRODUCTTYPENAME"].ToString()) ? dt_RTRUCK.Rows[0]["SPRODUCTTYPENAME"].ToString() : "-";
            xlbRnLoadWeight.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NLOAD_WEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_RTRUCK.Rows[0]["NLOAD_WEIGHT"].ToString())) : "-";
            xlbRCalcWeight.Text = !string.IsNullOrEmpty(dt_RTRUCK.Rows[0]["NCALC_WEIGHT"].ToString()) ? string.Format("{0:#,###0}", double.Parse(dt_RTRUCK.Rows[0]["NCALC_WEIGHT"].ToString())) : "-";

            string rDoc01 = "", rDoc02 = "", rDoc03 = "", rDoc04 = "";
            foreach (DataRow dr in dt_RTRUCK_Doc.Rows)
            {
                switch (dr["DOC_TYPE"].ToString())
                {
                    case "1":
                        rDoc01 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Reg/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "2":
                        rDoc02 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Tax_Evidence/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "3":
                        rDoc03 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Compartment/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                    case "4":
                        rDoc04 += "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/First_Reg/" + dr["DOC_NAME"].ToString() + "');'/></p>";
                        break;
                }
            }
            xlbRDoc01.Text = !string.IsNullOrEmpty(rDoc01) ? rDoc01 : "-";
            xlbRDoc02.Text = !string.IsNullOrEmpty(rDoc02) ? rDoc02 : "-";
            xlbRDoc03.Text = !string.IsNullOrEmpty(rDoc03) ? rDoc03 : "-";
            xlbRDoc04.Text = !string.IsNullOrEmpty(rDoc04) ? rDoc04 : "-";

            //ข้อมูล พรบ.และการประกันภัย
            string rSDOC = "", rIDOC = "";
            foreach (DataRow dr in dt_RTRUCK_Insure.Rows)
            {
                if (dr["CTYPE"].ToString() == "STATUTE")
                {
                    xlbRSCompany.Text = dr["SCOMPANY"].ToString();
                    xlbRSDuration.Text = dr["START_DURATION"].ToString() + " - " + dr["END_DURATION"].ToString();
                    xlbRSDetail.Text = dr["SDETAIL"].ToString();
                    foreach (DataRow r in dt_RTRUCK_InsureDoc.Select("CTYPE='STATUTE'"))
                        rSDOC += "<p>" + r["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Statute/" + r["DOC_NAME"].ToString() + "');'/></p>";
                    xlbRSDoc.Text = !string.IsNullOrEmpty(rSDOC) ? rSDOC : "-";
                }
                else
                {
                    xlbRICompany.Text = dr["SCOMPANY"].ToString();
                    xlbRIBudget.Text = string.Format("{0:#,###0}", double.Parse(dr["INSURE_BUDGET"].ToString()));
                    xlbRIType.Text = dr["INSURE_TYPE"].ToString();
                    xlbRIHolding.Text = dr["HOLDING"].ToString();
                    xlbRInInsure.Text = string.Format("{0:#,###0}", double.Parse(dr["NINSURE"].ToString()));
                    xlbRIDetail.Text = dr["SDETAIL"].ToString();
                    foreach (DataRow r in dt_RTRUCK_InsureDoc.Select("CTYPE='INSURANCE'"))
                        rIDOC += "<p>" + r["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Insurance/" + r["DOC_NAME"].ToString() + "');'/></p>";
                    xlbRIDoc.Text = !string.IsNullOrEmpty(rIDOC) ? rIDOC : "-";
                }
            }

        }
        #endregion

        #region ข้อมูลวัดน้ำ
        DataTable _dtWater;
        if (sCarTypeID == "3")
            _dtWater = dt_RTRUCK;
        else
            _dtWater = dt_STRUCK;
        if (_dtWater.Rows.Count > 0)
        {
            if (_dtWater.Rows[0]["PLACE_WATER_MEASURE"].ToString() == "INTERNAL")
                xlbPlaceMWater.Text = "วัดน้ำภายใน ปตท.";
            else if (_dtWater.Rows[0]["PLACE_WATER_MEASURE"].ToString() == "EXTERNAL")
                xlbPlaceMWater.Text = "วัดน้ำภายนอก ปตท.";
            else
                xlbPlaceMWater.Text = "-";
            xlbsCar_Num.Text = !string.IsNullOrEmpty(_dtWater.Rows[0]["SCAR_NUM"].ToString()) ? _dtWater.Rows[0]["SCAR_NUM"].ToString() : "-";
            xlbdWaterExpire.Text = !string.IsNullOrEmpty(_dtWater.Rows[0]["DWATEREXPIRE"].ToString()) ? Convert.ToDateTime(_dtWater.Rows[0]["DWATEREXPIRE"].ToString()).ToString("dd/MM/yyyy", new CultureInfo("th-th")) : "-"; ;
            xlbMWaterDoc.Text = "-";
            foreach (DataRow dr in dt_STRUCK_MWaterDoc.Rows)
                xlbMWaterDoc.Text = "<p>" + dr["DOC_NAME"].ToString() + "&nbsp;<img src='images/search.png' onclick='javascript:window.open('openFile.aspx?str=truck/Mwater/" + dr["DOC_NAME"].ToString() + "');'/></p>";
        }
        #endregion

        #region ข้อมูลการใช้งานและถือครอง
        xlbsContractNo.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SCONTRACTNO"].ToString()) ? "<a href='javascript:void(0);'>" + dt_STRUCK.Rows[0]["SCONTRACTNO"].ToString() + "</a>" : "-";
        xlbsTransport.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["SABBREVIATION"].ToString()) ? dt_STRUCK.Rows[0]["SABBREVIATION"].ToString() : "-";
        #endregion

        #region การอนุญาตรับงาน
        string cActive = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["CACTIVE"].ToString()) ? dt_STRUCK.Rows[0]["CACTIVE"].ToString() : "-";
        switch (cActive)
        {
            case "Y":
                xlbPermit.Text = "อนุญาต";
                xlbdBlackListExp.Text = "-";
                xlbBlackListCause.Text = "-";
                break;
            case "N":
                xlbPermit.Text = "ระงับ";
                xlbdBlackListExp.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["DBLACKLIST2"].ToString()) ? Convert.ToDateTime(dt_STRUCK.Rows[0]["DBLACKLIST2"].ToString()).ToString("dd/MM/yyyy", new CultureInfo("th-th")) : "-";
                xlbBlackListCause.Text = !string.IsNullOrEmpty(dt_STRUCK.Rows[0]["BLACKLIST_CAUSE"].ToString()) ? dt_STRUCK.Rows[0]["BLACKLIST_CAUSE"].ToString() : "-";
                break;
            case "D":
                xlbPermit.Text = "ยกเลิก";
                xlbdBlackListExp.Text = "-";
                xlbBlackListCause.Text = "-";
                break;
            default:
                xlbPermit.Text = "-";
                xlbdBlackListExp.Text = "-";
                xlbBlackListCause.Text = "-";
                break;
        }
        DataTable dtBlackList = new DataTable();
        dtBlackList.Columns.Add("REMARK", typeof(string));
        dtBlackList.Columns.Add("SDATE", typeof(string));
        dtBlackList.Columns.Add("SBLACKLIST", typeof(string));
        foreach (DataRow dr in dt_STRUCK_BLACKLIST.Rows)
        {
            DataRow drBlackList = dtBlackList.NewRow();
            drBlackList["REMARK"] = dr["REMARK"].ToString();
            drBlackList["SDATE"] = Convert.ToDateTime(dr["DBLACKLIST_START"].ToString()).ToString("dd/MM/yyyy", new CultureInfo("th-th")) + " - " + Convert.ToDateTime(dr["DBLACKLIST_START"].ToString()).ToString("HH:mm", new CultureInfo("th-th")) + " น.";
            drBlackList["SBLACKLIST"] = CommonFunction.Get_Value(conn, "SELECT SFIRSTNAME||'  '||SLASTNAME FROM TUSER WHERE SUID='" + dr["REMARK"].ToString() + "'");
            dtBlackList.Rows.Add(drBlackList);
        }
        gvwBlacklist.DataSource = dtBlackList;
        gvwBlacklist.DataBind();
        #endregion
    }
}