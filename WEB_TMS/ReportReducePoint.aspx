﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportReducePoint.aspx.cs" Inherits="ReportReducePoint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table width="100%" cellspacing="2px" cellpadding="2px">
        <tr>
            <td width="20%">
             &nbsp;
            </td>
            <td width="30%">
            </td>
            <td width="50%"></td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                ช่วงเวลาที่ต้องการค้นหา :
            </td>
            <td colspan="2">
                <div style="float: left">
                <dx:ASPxComboBox ID="cmbStartMonth" runat="server" Width="100px" SelectedIndex="0">
                </dx:ASPxComboBox>
                </div>
                <div style="float: left">
                    &nbsp;&nbsp;</div>
                <div style="float: left">
                    <dx:ASPxComboBox ID="cmbEndMonth" runat="server" Width="100px" SelectedIndex="0">
                    </dx:ASPxComboBox>
                </div>
                <div style="float: left">
                    &nbsp;&nbsp;</div>
                <div style="float: left">
                    <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px" SelectedIndex="0">
                    </dx:ASPxComboBox>
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
              &nbsp;
            </td>
            <td><div style="float: left">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_search" OnClick="btnSubmit_Click">
                    </dx:ASPxButton>
                </div>
                <div style="float:left">&nbsp;</div>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close">
                        <ClientSideEvents Click="function(s,e){ window.location = 'Report.aspx'}" />
                    </dx:ASPxButton>
                </div></td>
            <td>
                
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <dx:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewerID="ReportViewer1"
                    ShowDefaultButtons="False" Width="80%">
                    <Items>
                        <dx:ReportToolbarButton ItemKind="Search" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton ItemKind="PrintReport" />
                        <dx:ReportToolbarButton ItemKind="PrintPage" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                        <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                        <dx:ReportToolbarLabel ItemKind="PageLabel" />
                        <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                        </dx:ReportToolbarComboBox>
                        <dx:ReportToolbarLabel ItemKind="OfLabel" />
                        <dx:ReportToolbarTextBox ItemKind="PageCount" />
                        <dx:ReportToolbarButton ItemKind="NextPage" />
                        <dx:ReportToolbarButton ItemKind="LastPage" />
                        <dx:ReportToolbarSeparator />
                        <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                        <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                        <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                            <Elements>
                                <dx:ListElement Value="pdf" />
                                <dx:ListElement Value="xls" />
                                <dx:ListElement Value="xlsx" />
                                <dx:ListElement Value="rtf" />
                                <dx:ListElement Value="mht" />
                                <dx:ListElement Value="html" />
                                <dx:ListElement Value="txt" />
                                <dx:ListElement Value="csv" />
                                <dx:ListElement Value="png" />
                            </Elements>
                        </dx:ReportToolbarComboBox>
                    </Items>
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft="3px" MarginRight="3px" />
                        </LabelStyle>
                    </Styles>
                </dx:ReportToolbar>
            </td>
        </tr>
        <tr align="center">
            <td colspan="3">
                <dx:ReportViewer ID="ReportViewer1" runat="server">
                </dx:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
