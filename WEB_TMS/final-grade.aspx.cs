﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_Entity;
using OfficeOpenXml;
using System.Drawing;
using System.IO;
public partial class final_grade : PageBase
{
    string GroupPermission = "01";

    #region " Prop "
    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }

    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }

    protected DataTable VendorDDL
    {
        get { return (DataTable)ViewState[this.ToString() + "VendorDDL"]; }
        set { ViewState[this.ToString() + "VendorDDL"] = value; }
    }

    protected DataTable dtContract
    {
        get { return (DataTable)ViewState[this.ToString() + "dtContract"]; }
        set { ViewState[this.ToString() + "dtContract"] = value; }
    }

    protected DataTable dtGroup
    {
        get { return (DataTable)ViewState[this.ToString() + "dtGroup"]; }
        set { ViewState[this.ToString() + "dtGroup"] = value; }
    }

    protected DataTable dtGrade
    {
        get { return (DataTable)ViewState[this.ToString() + "dtGrade"]; }
        set { ViewState[this.ToString() + "dtGrade"] = value; }
    }
    protected DataTable dtStatus
    {
        get { return (DataTable)ViewState[this.ToString() + "dtStatus"]; }
        set { ViewState[this.ToString() + "dtStatus"] = value; }
    }
    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }
    protected string SPROCESSID
    {
        get { return ViewState[this.ToString() + "SPROCESSID"].ToString(); }
        set { ViewState[this.ToString() + "SPROCESSID"] = value; }
    }
    protected string AVG_ALL_QUARTER
    {
        get { return ViewState[this.ToString() + "AVG_ALL_QUARTER"].ToString(); }
        set { ViewState[this.ToString() + "AVG_ALL_QUARTER"] = value; }
    }
    protected string AVG_AUDIT_SCORE
    {
        get { return ViewState[this.ToString() + "AVG_AUDIT_SCORE"].ToString(); }
        set { ViewState[this.ToString() + "AVG_AUDIT_SCORE"] = value; }
    }
    protected string AVG_KPI_SCORE_BY_YEAR
    {
        get { return ViewState[this.ToString() + "AVG_KPI_SCORE_BY_YEAR"].ToString(); }
        set { ViewState[this.ToString() + "AVG_KPI_SCORE_BY_YEAR"] = value; }
    }
    protected string AVG_SUM_ALL_SCORE
    {
        get { return ViewState[this.ToString() + "AVG_SUM_ALL_SCORE"].ToString(); }
        set { ViewState[this.ToString() + "AVG_SUM_ALL_SCORE"] = value; }
    }
    #endregion " Prop "

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();
            InitForm();
            LoadMain();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnNoticeHistory.Enabled = false;
                btnSearch.Enabled = false;
                btnExport.Enabled = false;    
               
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetConditionVendor()
    {
        string result = string.Empty;
        try
        {

            if (ddlVendorSearch.SelectedIndex > 0)
                result = " AND TCONTRACT.SVENDORID = '" + ddlVendorSearch.SelectedValue + "'";

            return result;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private DataTable GetConfigData()
    {
        return QuarterReportBLL.Instance.ConfigQuarterSelect(ddlYearSearch.SelectedValue, "0");
    }

    private void InitForm()
    {
        try
        {
            int StartYear = 2015;
            int cYear = DateTime.Today.Year;
            int endYear = cYear + 4;
            int index = 0;
            for (int i = StartYear; i <= endYear; i++)
            {
                ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
            }

            // เลือกปีปัจจุบัน
            ListItem sel = ddlYearSearch.Items.FindByValue(cYear.ToString());
            ddlYearSearch.ClearSelection();
            if (sel != null) sel.Selected = true;
            else ddlYearSearch.SelectedIndex = 0;

            ddlVendorSearch.SelectedIndex = 0;
            LoadVendor();

            DataTable dt = QuarterReportBLL.Instance.GetSPROCESSID();
            if (dt != null)
            {
                List<string> lstStirng = new List<string>();
                foreach (DataRow itm in dt.Rows) lstStirng.Add(itm["SPROCESSID"].ToString());

                SPROCESSID = string.Join(",", lstStirng.ToArray());
            }
            else
                SPROCESSID = "''";

            ddlVendorSearch_SelectedIndexChanged(null, null);

            dtContract = ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor());
            DataRow dr = dtContract.NewRow();
            dr["SCONTRACTNO"] = "";
            dtContract.Rows.InsertAt(dr, 0);

            ddlContract.DataValueField = "SCONTRACTID";
            ddlContract.DataTextField = "SCONTRACTNO";
            ddlContract.DataSource = dtContract;
            ddlContract.DataBind();

            LoadStatus();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void LoadStatus()
    {
        try
        {
            string _err = string.Empty;
            dtStatus = new FinalGradeBLL().GetStatus(ref _err);
            chkStatus.DataTextField = "CONFIG_NAME";
            chkStatus.DataValueField = "CONFIG_VALUE";
            chkStatus.DataSource = dtStatus;
            chkStatus.DataBind();

            if (user_profile.CGROUP == GROUP_PERMISSION.VENDOR)
            {
                divStatus.Visible = false;
                chkStatus.ClearSelection();
                ListItem sel = chkStatus.Items.FindByValue("3");
                if (sel != null) sel.Selected = true;
            }
            else
                foreach (ListItem itm in chkStatus.Items) itm.Selected = true;
        }
        catch (Exception ex)
        {

            alertFail(ex.Message);
        }
    }
    public void LoadVendor()
    {
        try
        {
            VendorDDL = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = VendorDDL.NewRow();
            dr["SABBREVIATION"] = "";
            VendorDDL.Rows.InsertAt(dr, 0);
            ddlVendorSearch.DataSource = VendorDDL;
            ddlVendorSearch.DataBind();

            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                ddlVendorSearch.ClearSelection();
                ListItem lst = ddlVendorSearch.Items.FindByValue(user_profile.SVDID);
                if (lst != null) lst.Selected = true;
                ddlVendorSearch.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }
    private void LoadMain()
    {
        try
        {
            DataTable dt = GetConfigData();
            if (dt != null)
            {
                List<string> lstStirng = new List<string>();
                foreach (DataRow itm in dt.Rows) lstStirng.Add(itm["SPROCESSID"].ToString());

                SPROCESSID = string.Join(",", lstStirng.ToArray());
            }
            else
                SPROCESSID = "''";

            string _err = string.Empty;
            SearchCriteria = new FinalGade() { YEAR = string.Empty, VENDORID = string.Empty };
            if (ddlYearSearch.SelectedItem != null) SearchCriteria.YEAR = ddlYearSearch.SelectedItem.Value;
            if (ddlVendorSearch.SelectedItem != null) SearchCriteria.VENDORID = ddlVendorSearch.SelectedItem.Value;
            if (ddlGroup.SelectedItem != null)
            {
                SearchCriteria.GROUPID = ddlGroup.SelectedItem.Value;
                SearchCriteria.GROUPNAME = ddlGroup.SelectedItem.Text;
            }

            if (ddlContract.SelectedItem != null)
            {
                SearchCriteria.SCONTRACTNO = ddlContract.SelectedItem.Text;
                SearchCriteria.SCONTRACTID = ddlContract.SelectedItem.Value;
            }
            var selected = chkStatus.Items.Cast<ListItem>().Where(li => li.Selected).Select(x => x.Value).ToList();

            SearhData = new FinalGradeBLL().GetFinalScore(ref _err, SearchCriteria, SPROCESSID, false);

            dtGrade = new FinalGradeBLL().GetGrade(ref _err);
            if (SearhData != null)
            {
                SearhData.Columns.Add("col_1", typeof(string));
                SearhData.Columns.Add("col_2", typeof(string));
                SearhData.Columns.Add("col_3", typeof(string));
                SearhData.Columns.Add("col_4", typeof(string));
                SearhData.Columns.Add("col_5", typeof(string));
                SearhData.Columns.Add("col_6", typeof(string));
                SearhData.Columns.Add("DOCSTATUSNAME", typeof(string));
                for (int j = SearhData.Rows.Count - 1; j >= 0; j--)
                {
                    DataRow dr = SearhData.Rows[j];
                    List<string> lst = dr["SCONTRACTNO"].ToString().Split('/').ToList();

                    // Last Record 
                    int valLast = 0; // Ex ปตท./ธกน./พ./4/16/2549 ต.6 
                    bool resultLast = int.TryParse(lst[lst.Count - 1].Split(' ').First().ToString(), out valLast);
                    dr["col_1"] = resultLast ? valLast.ToString("0000") : lst[lst.Count - 1].ToString();

                    for (int i = 1; (i <= lst.Count && i <= 5); i++)
                    {
                        int val = 0;
                        bool result = int.TryParse(lst[i - 1].ToString(), out val);
                        dr[string.Format("col_{0}", (i + 1).ToString())] = result ? val.ToString("0000") : lst[i - 1].ToString();
                    }

                    var CAN_ANNUAL_NOTICE = dr["CAN_ANNUAL_NOTICE"].ToString();
                    if (CAN_ANNUAL_NOTICE == "1") dr["DOC_STATUS"] = "1";
                    var DOC_STATUS = dr["DOC_STATUS"].ToString();
                    var DOCSTATUSNAME = string.Empty;
                    if (CAN_ANNUAL_NOTICE == "2" && DOC_STATUS == "1")
                    {
                        DOCSTATUSNAME = "รอแจ้งผลประเมินประจำปี"; 
                    }
                    else if (DOC_STATUS == "3")
                    {
                        DOCSTATUSNAME = "แจ้งผลประเมินแล้ว";
                    }
                    else if (DOC_STATUS == "4")
                    {
                        DOCSTATUSNAME = "ยกเลิกเอกสาร";
                    }
                    else if (CAN_ANNUAL_NOTICE == "1")
                    {
                        DOCSTATUSNAME = "รอแจ้งผลประเมินย่อย";
                    }
                    dr["DOCSTATUSNAME"] = DOCSTATUSNAME;
                    var match = selected.FirstOrDefault(stringToCheck => stringToCheck.Contains(dr["DOC_STATUS"].ToString()));
                    if (match == null) { dr.Delete(); continue; }

                    var vendorID = dr["SVENDORID"].ToString();
                    var ContractID = int.Parse(dr["SCONTRACTID"].ToString());
                    bool ISPTT = user_profile.CGROUP == GROUP_PERMISSION.UNIT ? true : false;
                    var KPIPoint = new KPI_Helper().GetKPIYear(ddlYearSearch.SelectedValue, vendorID, ContractID, ISPTT);
                    dr["KPI_SCORE_BY_YEAR"] = KPIPoint.ToString("0");
                    dr["AUDIT_SCORE"] = decimal.Parse(dr["AUDIT_SCORE"].ToString()).ToString("0");
                    dr["ALL_QUARTER"] = decimal.Parse(dr["ALL_QUARTER"].ToString()).ToString("0");

                    dr["SUM_ALL_SCORE"] = (decimal.Parse(dr["AUDIT_SCORE"].ToString()) + decimal.Parse(dr["KPI_SCORE_BY_YEAR"].ToString()) + decimal.Parse(dr["ALL_QUARTER"].ToString())).ToString("0");


                    //GET GRADE 
                    string FinalPoint = dr["SUM_ALL_SCORE"].ToString();
                    string expression = string.Format(" '{0}' >= NSTARTPOINT and '{1}' <= NENDPOINT ", FinalPoint, FinalPoint);
                    DataRow[] foundRows = null;
                    foundRows = dtGrade.Select(expression);
                    if (foundRows != null) dr["GRADE"] = foundRows[0]["SGRADENAME"];
                }

                SearhData.AcceptChanges();
            }

            SearhData.DefaultView.Sort = " col_1 ASC, col_5 ASC, col_2 ASC, col_3 ASC, col_4 ASC";
            
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
            //if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            //{
            //    grvMain.Columns[4].Visible = false;
            //    grvMain.Columns[5].Visible = false;
            //    grvMain.Columns[6].Visible = false;
            //    grvMain.Columns[7].Visible = false;
            //}

            lblHeaderGrid.Text = string.Format("สรุปผลประเมินการปฏิบัติงานในรอบ 12 เดือน จำนวน {0} สัญญา", SearhData.Rows.Count);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT) btnNoticeHistory.Visible = false;

    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if ((e.CommandName == "SendEmail") || (e.CommandName == "ViewResult"))
        {
            int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString()) + 1;
            string _keySABBREVIATION = grvMain.DataKeys[rowIndex.Value]["SABBREVIATION"].ToString();
            if (_keySABBREVIATION != null) Session["AARN_SABBREVIATION"] = _keySABBREVIATION;
            string _keySVENDORID = grvMain.DataKeys[rowIndex.Value]["SVENDORID"].ToString();
            if (_keySVENDORID != null) Session["AARN_SVENDORID"] = _keySVENDORID;
            Session["AARN_YEAR"] = SearchCriteria.YEAR;
            Response.Redirect("AnnualReportNotice.aspx");
        }
    }

    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                var lblHeaderBtn = e.Row.Cells[1].FindControl("lblHeaderBtn") as Label;
                if (lblHeaderBtn != null)
                {
                    lblHeaderBtn.Text = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? "ประเมินประจำปี" : "ประเมินประจำปี";
                }

                var lnkKPI = e.Row.Cells[1].FindControl("lnkKPI") as HyperLink;
                if (lnkKPI != null)
                {
                    string urlKPI = user_profile.CGROUP == GROUP_PERMISSION.UNIT ? "KPI_Index.aspx" : "KPI_Summary.aspx";
                    lnkKPI.Attributes.Add("onclick", "openInNewTab(\'" + urlKPI + "\');");
                }

                var lnkQuarteryReport = e.Row.Cells[1].FindControl("lnkQuarteryReport") as HyperLink;
                if (lnkQuarteryReport != null)
                {
                    string urlQuarteryReport = user_profile.CGROUP == GROUP_PERMISSION.UNIT ? "Quartery_Report.aspx" : "Config_QuarterReport.aspx";
                    lnkQuarteryReport.Attributes.Add("onclick", "openInNewTab(\'" + urlQuarteryReport + "\');");
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var CAN_ANNUAL_NOTICE = DataBinder.Eval(e.Row.DataItem, "CAN_ANNUAL_NOTICE").ToString();
                var SVENDORID = DataBinder.Eval(e.Row.DataItem, "SVENDORID").ToString();
                var DOC_STATUS = DataBinder.Eval(e.Row.DataItem, "DOC_STATUS").ToString();
                var DOCSTATUSNAME = DataBinder.Eval(e.Row.DataItem, "DOCSTATUSNAME").ToString();
                var lblDocStatus = e.Row.Cells[1].FindControl("lblDocStatus") as Label;
                var btnSaveMail = e.Row.Cells[1].FindControl("btnSaveMail") as Button;
                if (btnSaveMail != null)
                {
                    btnSaveMail.Visible = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? false : true;
                    if (CAN_ANNUAL_NOTICE == "2" && DOC_STATUS == "1")
                    {
                        btnSaveMail.Enabled = true; btnSaveMail.Attributes.Add("onclick", "gotoView(\'" + SVENDORID + "\');");
                    }
                    else if (int.Parse(DOC_STATUS) < 2) btnSaveMail.Enabled = false;
                    else btnSaveMail.Attributes.Add("onclick", "gotoView(\'" + SVENDORID + "\');");
                }

                var txtALL_QUARTER = e.Row.Cells[1].FindControl("txtALL_QUARTER") as Label;
                if (txtALL_QUARTER != null)
                {
                    string ALL_QUARTER = DataBinder.Eval(e.Row.DataItem, "ALL_QUARTER").ToString();
                    txtALL_QUARTER.Text = ALL_QUARTER;
                }

                /// รอแจ้งข้อมูลหัวข้อย่อย	1
                /// รอแจ้งผล (ข้อมูลครบ)	2
                /// แจ้งผลแล้ว	3
                /// ยกเลิกเอกสาร	4 

                var btnViewResult = e.Row.Cells[1].FindControl("btnViewResult") as Button;
                if (btnViewResult != null)
                {
                    btnViewResult.Visible = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
                    btnViewResult.Attributes.Add("onclick", "gotoView(\'" + SVENDORID + "\');");
                    if (CAN_ANNUAL_NOTICE == "0")
                    {
                        btnViewResult.Enabled = false;
                    }
                    else if (CAN_ANNUAL_NOTICE == "1" && DOC_STATUS == "1")
                    {
                        btnViewResult.Text = "รอแจ้งผล";
                        btnViewResult.Enabled = false;
                    }
                    else if (DOC_STATUS == "3")
                    {
                        btnViewResult.Text = "รายละเอียด";
                    }
                    else if (DOC_STATUS == "4")
                    {
                        btnViewResult.Enabled = false;
                    }

                }
                if (btnSaveMail != null)
                {
                    if (CAN_ANNUAL_NOTICE == "1" && DOC_STATUS == "3")
                        btnSaveMail.Text = "รายละเอียด";
                    else if (CAN_ANNUAL_NOTICE == "0")
                        btnSaveMail.Enabled = false;
                }

            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void grvMain_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvMain.PageIndex = e.NewPageIndex;
        LoadMain();
    }
    protected void btnViewHistory_Click(object sender, EventArgs e)
    {
        grvMain.DataSource = SearhData;
        grvMain.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LoadMain();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        InitForm();
        LoadMain();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        using (ExcelPackage pck = new ExcelPackage(new MemoryStream(), new MemoryStream(File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath("~/FileFormat/Admin/FinalScoreFormat.xlsx")))))
        {
            ExcelWorksheet ws = pck.Workbook.Worksheets[1];
            int StartRow = 3;
            int index = 1;
            foreach (DataRow dr in SearhData.Rows)
            {
                ws.Cells["A" + StartRow.ToString()].Value = "" + index.ToString();
                ws.Cells["B" + StartRow.ToString()].Value = dr["SABBREVIATION"].ToString();
                ws.Cells["C" + StartRow.ToString()].Value = dr["GROUPNAME"].ToString();
                ws.Cells["D" + StartRow.ToString()].Value = dr["SCONTRACTNO"].ToString();
                ws.Cells["E" + StartRow.ToString()].Value = dr["ALL_QUARTER"].ToString();
                ws.Cells["F" + StartRow.ToString()].Value = dr["AUDIT_SCORE"].ToString();
                ws.Cells["G" + StartRow.ToString()].Value = dr["KPI_SCORE_BY_YEAR"].ToString();
                ws.Cells["H" + StartRow.ToString()].Value = dr["SUM_ALL_SCORE"].ToString();
                ws.Cells["I" + StartRow.ToString()].Value = dr["GRADE"].ToString();
                ws.Cells["J" + StartRow.ToString()].Value = dr["DOCSTATUSNAME"].ToString();
                ws.Cells["K" + StartRow.ToString()].Value = dr["ACTION_DATE"].ToString();
                StartRow++;
                index++;
            }

            ws.Cells["E1"].Value = AVG_ALL_QUARTER;
            ws.Cells["F1"].Value = AVG_AUDIT_SCORE;
            ws.Cells["G1"].Value = AVG_KPI_SCORE_BY_YEAR;
            ws.Cells["H1"].Value = AVG_SUM_ALL_SCORE;

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=final-score.xlsx");
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void grvMain_DataBound(object sender, EventArgs e)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
        if (user_profile.CGROUP == GROUP_PERMISSION.UNIT)
        {

            for (var i = 0; i < 4; i++)
            {
                TableHeaderCell cell = new TableHeaderCell();
                cell.Text = "";
                cell.ColumnSpan = 1;
                row.Controls.Add(cell);
            }
        }
        if (SearhData.Rows.Count > 0)
        {
            var ALL_QUARTER = (decimal)(SearhData.AsEnumerable().Sum(x => x.Field<decimal>("ALL_QUARTER"))) / SearhData.Rows.Count;
            var AUDIT_SCORE = (decimal)(SearhData.AsEnumerable().Sum(x => x.Field<decimal>("AUDIT_SCORE"))) / SearhData.Rows.Count;
            var KPI_SCORE_BY_YEAR = (decimal)(SearhData.AsEnumerable().Sum(x => x.Field<decimal>("KPI_SCORE_BY_YEAR"))) / SearhData.Rows.Count;
            var SUM_ALL_SCORE = (decimal)(SearhData.AsEnumerable().Sum(x => x.Field<decimal>("SUM_ALL_SCORE"))) / SearhData.Rows.Count;
            AVG_ALL_QUARTER = string.Format("เฉลี่ย: {0}", ALL_QUARTER.ToString("0"));
            AVG_AUDIT_SCORE = string.Format("เฉลี่ย: {0}", AUDIT_SCORE.ToString("0"));
            AVG_KPI_SCORE_BY_YEAR = string.Format("เฉลี่ย: {0}", KPI_SCORE_BY_YEAR.ToString("0"));
            AVG_SUM_ALL_SCORE = string.Format("เฉลี่ย: {0}", SUM_ALL_SCORE.ToString("0"));

            if (user_profile.CGROUP == GROUP_PERMISSION.UNIT)
            {
                TableHeaderCell cell1 = new TableHeaderCell();
                cell1.Text = AVG_ALL_QUARTER;
                cell1.ColumnSpan = 1;
                row.Controls.Add(cell1);

                TableHeaderCell cell2 = new TableHeaderCell();
                cell2.Text = AVG_AUDIT_SCORE;
                cell2.ColumnSpan = 1;
                row.Controls.Add(cell2);

                TableHeaderCell cell3 = new TableHeaderCell();
                cell3.Text = AVG_KPI_SCORE_BY_YEAR;
                cell3.ColumnSpan = 1;
                row.Controls.Add(cell3);

                TableHeaderCell cell4 = new TableHeaderCell();
                cell4.Text = AVG_SUM_ALL_SCORE;
                cell4.ColumnSpan = 1;
                row.Controls.Add(cell4);
            }
        }
        else
        {
            if (user_profile.CGROUP == GROUP_PERMISSION.UNIT)
            {
                for (var i = 0; i < 4; i++)
                {
                    TableHeaderCell cell = new TableHeaderCell();
                    cell.Text = string.Format("เฉลี่ย: 0");
                    cell.ColumnSpan = 1;
                    row.Controls.Add(cell);
                }

            }
            AVG_ALL_QUARTER = "เฉลี่ย: 0";
            AVG_AUDIT_SCORE = "เฉลี่ย: 0";
            AVG_KPI_SCORE_BY_YEAR = "เฉลี่ย: 0";
            AVG_SUM_ALL_SCORE = "เฉลี่ย: 0";
        }
        if (user_profile.CGROUP == GROUP_PERMISSION.UNIT)
        {
            TableHeaderCell cell12 = new TableHeaderCell();
            cell12.Text = "ประเมินผลการปฏิบัติงาน<br/>ในรอบ 12 เดือน";
            cell12.ColumnSpan = 2;
            row.Controls.Add(cell12);

            for (var i = 0; i < 2; i++)
            {
                TableHeaderCell cell = new TableHeaderCell();
                cell.Text = "";
                cell.ColumnSpan = 1;
                row.Controls.Add(cell);
            }
            grvMain.HeaderRow.Parent.Controls.AddAt(0, row);
        }
    }

    protected void grvMain_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string _keySABBREVIATION = grvMain.DataKeys[e.RowIndex]["SABBREVIATION"].ToString();
        if (_keySABBREVIATION != null) Session["AARN_SABBREVIATION"] = _keySABBREVIATION;
        string _keySVENDORID = grvMain.DataKeys[e.RowIndex]["SVENDORID"].ToString();
        if (_keySVENDORID != null) Session["AARN_SVENDORID"] = _keySVENDORID;
        Session["AARN_YEAR"] = SearchCriteria.YEAR;
        Response.Redirect("AnnualReportNotice.aspx");
    }


    protected void ddlVendorSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        string _err = string.Empty;
        string vendorId = ddlVendorSearch.SelectedValue != null ? ddlVendorSearch.SelectedItem.Value : "";
        dtGroup = new FinalGradeBLL().GetGroup(ref _err, true, vendorId);
        DataRow drG = dtGroup.NewRow();
        drG["NAME"] = "";
        dtGroup.Rows.InsertAt(drG, 0);
        ddlGroup.DataSource = dtGroup;
        ddlGroup.DataBind();

        dtContract = ComplainBLL.Instance.ContractSelectBLL(this.GetConditionVendor());
        DataRow dr = dtContract.NewRow();
        dr["SCONTRACTNO"] = "";
        dtContract.Rows.InsertAt(dr, 0);

        ddlContract.DataValueField = "SCONTRACTID";
        ddlContract.DataTextField = "SCONTRACTNO";
        ddlContract.DataSource = dtContract;
        ddlContract.DataBind();

        if (SearhData != null)
        {
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        string Id = hdnNum.Value;
        if (Id == "0") return;
        Button btn = sender as Button;
        if (btn.ID == "btnView")
        {
            string expression = string.Format(" SVENDORID='{0}' ", Id);
            DataRow[] foundRows = null;
            foundRows = SearhData.Select(expression);
            if (foundRows != null) Session["AARN_SABBREVIATION"] = foundRows[0]["SABBREVIATION"];
            Session["AARN_SVENDORID"] = Id;
            Session["AARN_YEAR"] = SearchCriteria.YEAR;

            SearhData.DefaultView.Sort = " col_1 ASC, col_5 ASC, col_2 ASC, col_3 ASC, col_4 ASC";
            grvMain.DataSource = SearhData;
            grvMain.DataBind();
        }
    }
}