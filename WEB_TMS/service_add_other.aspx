﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="service_add_other.aspx.cs" Inherits="service_add_other" %>

<%@ Register TagPrefix="uc" TagName="ucCalendar" Src="~/ucCalendar.ascx" %>
<%@ Register Src="DocumentCharges.ascx" TagName="UserControl" TagPrefix="myUsr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            //            if (cmbRequestType.GetValue() != '02' && cmbRequestType.GetValue() != '01') {
            //                // $('#trCause').hide();
            //            }

            //            if (txtReqID.GetValue() != null && txtReqID.GetValue() != "") {
            //                $("#trdoc").show();

            //            }
            //            else {
            //                $("#trdoc").hide();
            //            }
        });


        function Showdoc() {
            //            if (txtReqID.GetValue() != null && txtReqID.GetValue() != "") {
            //                $("#trdoc").show();

            //            }
            //            else {
            //                $("#trdoc").hide();
            //            }
        }

        function hidcontrol(sValue) {

            if (sValue == $("input[id$=txtFixCause]").val()) {
                $("#trAdd1").show();
            }
            else {
                $("#trAdd1").hide();

            }



            if (cboCause.GetValue() == '00010') {

                $("#trOhtercause").show();
                txtRemarkCause.SetEnabled(true);
            }
            else {
                $("#trOhtercause").hide();
                txtRemarkCause.SetText('');
                txtRemarkCause.SetEnabled(false);

            }

        }



        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function OnStartUpload(e, s) {

            var filename = e.GetText();
            if (filename != "") {
                var arrfilename = filename.split('.');
                var indx = e.name.substring(e.name.lastIndexOf('_') + 1, e.name.length);

                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    e.Upload(indx);
                }
                else {
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }

        function UploadFile1(e, s) {

            var filename = e.GetText();
            if (filename != "") {
                var arrfilename = filename.split('.');
                var indx = e.name.substring(e.name.lastIndexOf('_') + 1, e.name.length);
                if (arrfilename[arrfilename.length - 1].toLowerCase() == 'pdf' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'jpeg' || arrfilename[arrfilename.length - 1].toLowerCase() == 'bmp' || arrfilename[arrfilename.length - 1].toLowerCase() == 'gif' || arrfilename[arrfilename.length - 1].toLowerCase() == 'png' || arrfilename[arrfilename.length - 1].toLowerCase() == 'doc' || arrfilename[arrfilename.length - 1].toLowerCase() == 'docx' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xls' || arrfilename[arrfilename.length - 1].toLowerCase() == 'xlsx') {
                    gvwBill.PerformCallback('UPLOAD;' + indx);
                }
                else {
                    dxWarning('แจ้งเตือน', 'เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น');
                }
            }
        }

        function openFile(str) {

            window.open("" + str + "");


        }

        function OpenPage(sUrl) {

            window.open(sUrl);
        }
       
    </script>
    <style type="text/css">
        .cHideControl
        {
            display: none;
        }
        
        .cShowControl
        {
            display: block;
        }
        
        .cInline
        {
            display: inline;
        }
        .dxeHLC
        {
            display: none;
        }
        .dxeLTM
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="false" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents Init="function(e,s) {hidcontrol(cboCause.GetValue());}" EndCallback="function(s,e){ Showdoc();  hidcontrol(cboCause.GetValue());  eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td align="right" colspan="2">
                            <myUsr:UserControl ID="UserControl1" runat="server"></myUsr:UserControl>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td class="td1" width="25%" bgcolor="#B9EAEF" align="center">ประเภทคำขอ </td>
                        <td width="50%">
                            <dx:ASPxComboBox ID="cboRequestType" runat="server" Width="75%" ClientInstanceName="cboRequestType"
                                DataSourceID="sdsReqType" TextField="REQTYPE_NAME" ValueField="REQTYPE_ID">
                                <ClientSideEvents SelectedIndexChanged="function(e,s) {xcpn.PerformCallback('ChangeReq');}">
                                </ClientSideEvents>
                                <ValidationSettings RequiredField-ErrorText="ระบุประเภทคำขอ" RequiredField-IsRequired="true"
                                    ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุประเภทคำขอ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsReqType" runat="server" SelectCommand="
                          SELECT REQTYPE_ID, REQTYPE_NAME ,NORDER
                          FROM TBL_REQTYPE
                          Where ISACTIVE_FLAG = 'Y' AND REQTYPE_ID IN ('00','01','02')
                          ORDER BY NORDER ASC " ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td rowspan="6" width="25%" valign="top">
                            <table width="100%" border="0" align="center" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="center" bgcolor="#B9EAEF">ค่าใช้จ่าย </td>
                                </tr>
                                <tr valign="top">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC"><strong class="active">
                                        <b>
                                            <dx:ASPxLabel ID="lblTotal" runat="server" Text="" CssClass="active">
                                            </dx:ASPxLabel>
                                        </b></strong>บาท </td>
                                </tr>
                                <tr valign="top">
                                    <td align="center" valign="middle" bgcolor="#FFFFCC">*หมายเหตุ เป็นค่าบริการประมาณการ
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td height="35" align="center" valign="middle" bgcolor="#FFFFCC"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trCause" runat="server">
                        <td class="td1" align="center" bgcolor="#B9EAEF">สาเหตุ </td>
                        <td>
                            <dx:ASPxComboBox ID="cboCause" runat="server" Width="100%" ClientInstanceName="cboCause">
                                <ClientSideEvents ValueChanged="function(){hidcontrol();}" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="- ระบุสาเหตุ -"
                                    RequiredField-IsRequired="true" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="- ระบุสาเหตุ -"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr id="trOhtercause" style="display: none;">
                        <td align="center" bgcolor="#B9EAEF">หมายเหตุกรณีอื่นๆ </td>
                        <td>
                            <dx:ASPxTextBox ID="txtRemarkCause" runat="server" ClientInstanceName="txtRemarkCause"
                                Width="90%" ToolTip="ระบุสาเหตุ">
                                <ValidationSettings RequiredField-ErrorText="ระบุหมายเหตุกรณีสาเหตุอื่นๆ" RequiredField-IsRequired="true"
                                    ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุหมายเหตุกรณีสาเหตุอื่นๆ"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                        <td>
                            <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="80%">
                                <ClientSideEvents ValueChanged="function(){ cboCarregis.SetText(' '); xcpn.PerformCallback('List');  }">
                                </ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="1px" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">เลือกรถ </td>
                        <td>
                            <dx:ASPxComboBox ID="cboCarregis" runat="server" CallbackPageSize="30" ClientInstanceName="cboCarregis"
                                ItemStyle-Wrap="True" EnableCallbackMode="True" OnItemRequestedByValue="cboCarregis_OnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="cboCarregis_ItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} - {1}{3}" ValueField="STRUCKID" Width="400px"
                                CssClass="dxeLineBreakFix" Height="25">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="STRUCKID" Width="100px" Caption="รหัส"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="head" Width="150px" Caption="ทะเบียนหัว"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="tail" Width="150px" Caption="ทะเบียนหาง"></dx:ListBoxColumn>
                                    <dx:ListBoxColumn FieldName="tail1" Width="0px"></dx:ListBoxColumn>
                                </Columns>
                                <%--<ItemStyle Wrap="True"></ItemStyle>--%>
                                <ClientSideEvents ValueChanged="function(s,e){ xcpn.PerformCallback('SetDetail');  }" />
                                <ItemStyle Wrap="True"></ItemStyle>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic" RequiredField-IsRequired="true"
                                    RequiredField-ErrorText="ระบุทะเบียรถ" ValidationGroup="add">
                                    <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCarregis" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CacheKeyDependency="CacheCarregis" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">ประเภทรถ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblTruckType" Text="">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="td1" align="center" bgcolor="#B9EAEF">วันหมดอายุวัดน้ำ </td>
                        <td>
                            <dx:ASPxLabel runat="server" ID="lblWaterExpire" Text="">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <dx:ASPxButton ID="btnSubmit" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                SkinID="_submitandprint" ValidationGroup="add">
                                <ClientSideEvents Click="function(e,s){if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('save');}">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnBack" runat="server" AutoPostBack="False" CssClass="dxeLineBreakFix"
                                SkinID="_back">
                                <ClientSideEvents Click="function(e,s){ xcpn.PerformCallback('back');}"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
