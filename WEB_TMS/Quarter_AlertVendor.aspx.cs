﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Data;
using System.Web.Security;
using System.Text;
using SelectPdf;
using System.IO;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
public partial class Quarter_AlertVendor : PageBase
{
    DataTable dt, dtSave;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
            string strQuery = Encoding.UTF8.GetString(decryptedBytes);
            string[] listStr = strQuery.Split('&');
            string Quarter = listStr[0], SVENDORID = listStr[1], SABBREVIATION = listStr[2], YEAR = listStr[3], ID = listStr[4];
            SetData(Quarter, SVENDORID, SABBREVIATION, YEAR, ID);
            if (!string.IsNullOrEmpty(ID))
            {
                Preview_Click(null, null);
                btnConfirm.Disabled = true;
            }
            else
            {
                btnCancel.Disabled = true;
                btnConfirm.Disabled = true;
                btnPDF.Enabled = false;
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnPreview.Enabled = false;
                btnPDF.Enabled = false;
                btnWord.Enabled = false;
                btnConfirm.Disabled = true;

            }
            if (!CanWrite)
            {
                btnCancel.Disabled = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region Preview_Click
    protected void Preview_Click(object sender, EventArgs e)
    {
        SetDataToSave();
        string strFont = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Font.txt"));
        string font = strFont.Split('|')[0];
        string size = strFont.Split('|')[1];
        string detail = "<table style='width:100%;font-family:\"" + font + "\";'><tbody>";
        DataRow dr;
        for (int i = 0; i < dtSave.Rows.Count; i++)
        {
            if (dtSave.Rows[i]["SCORE1"].ToString() != "" || dtSave.Rows[i]["SCORE2"].ToString() != "" || dtSave.Rows[i]["SCORE3"].ToString() != "")
            {
                dr = dtSave.Rows[i];
                detail += "<tr>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;width:5%;'>";
                detail += "&nbsp;";
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;width:20px'>";
                detail += (i + 1) + ".";
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
                detail += "เลขที่สัญญา";
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
                detail += dr["SCONTRACTNO"];
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
                detail += "ผลการประเมินได้";
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:24pt;'>";
                detail += dr["QUARTER"];
                detail += "</td>";
                detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
                detail += "คะแนน";
                detail += "</td>";
                detail += "</tr>";
            }
        }
        detail += "</tbody></table>";

        string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty, HEDERTITLE = string.Empty, TITLE = "บริษัท";
        if (lblSABBREVIATION.Text.Trim().Contains("บริษัท"))
        {
            SVENDORNAME = lblSABBREVIATION.Text.Trim().Replace("บริษัท", "");
            HEDERTITLE = "กรรมการผู้จัดการ";
        }
        else
        {
            SVENDORNAME = lblSABBREVIATION.Text.Trim().Replace("ห้างหุ้นส่วนจำกัด", "");
            SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
            TITLE = "ห้าง";
            HEDERTITLE = "หุ้นส่วนผู้จัดการ";
        }
        string text = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor.txt"));
        text = text.Replace("{DOCNO}", txtNo.Text.Trim());
        text = text.Replace("{DATESTR}", txtDate.Text.Trim());
        text = text.Replace("{QUARTER}", lblQuarter.Text.Trim());
        text = text.Replace("{YEAR}", YearAdd543(lblYEAR.Text.Trim()));
        text = text.Replace("{SVENDORTITLE}", SVENDORTITLE);
        text = text.Replace("{TITLE}", TITLE);
        text = text.Replace("{HEDERTITLE}", HEDERTITLE);
        text = text.Replace("{SVENDORNAME}", SVENDORNAME);
        text = text.Replace("{DETAIL}", detail);
        divBody.InnerHtml = text;
        #region ReportViewer
        //ReportViewer1.ProcessingMode = ProcessingMode.Local;
        ////---เอาไว้ลงข้อมูลจริง
        //ReportParameter ReportDate = new ReportParameter("ReportDate", txtDate.Text);
        //ReportParameter ReportNo = new ReportParameter("ReportNo", txtNo.Text);
        //ReportParameter ReportQuarter = new ReportParameter("ReportQuarter", lblQuarter.Text);
        //ReportParameter ReportVendorName = new ReportParameter("ReportVendorName", lblSABBREVIATION.Text);
        ////---เอาไว้ลงข้อมูลจริง
        //DataSet ds = new DataSet("Ds_QuarterVendor");
        //DataTable dt = GetData();
        //dt.TableName = "DataTable1";
        //switch (lblQuarter.Text)
        //{
        //    case "1":
        //        //dt.Columns.Remove("SABBREVIATION");
        //        //dt.Columns.Remove("SCONTRACTNO");
        //        dt.Columns.Remove("JAN");
        //        dt.Columns.Remove("feb");
        //        dt.Columns.Remove("mar");
        //        //dt.Columns.Remove("QUARTER1");
        //        dt.Columns.Remove("VIEW1");
        //        dt.Columns.Remove("APR");
        //        dt.Columns.Remove("MAY");
        //        dt.Columns.Remove("JUN");
        //        dt.Columns.Remove("QUARTER2");
        //        dt.Columns.Remove("VIEW2");
        //        dt.Columns.Remove("JUL");
        //        dt.Columns.Remove("AUG");
        //        dt.Columns.Remove("SEP");
        //        dt.Columns.Remove("QUARTER3");
        //        dt.Columns.Remove("VIEW3");
        //        dt.Columns.Remove("OCT");
        //        dt.Columns.Remove("NOV");
        //        dt.Columns.Remove("DEC");
        //        dt.Columns.Remove("QUARTER4");
        //        dt.Columns.Remove("VIEW4");
        //        dt.Columns.Remove("SVENDORID");
        //        dt.Columns.Remove("SCONTRACTID");
        //        dt.Columns["QUARTER1"].ColumnName = "QUARTER";
        //        break;
        //    case "2":
        //        //dt.Columns.Remove("SABBREVIATION");
        //        //dt.Columns.Remove("SCONTRACTNO");
        //        dt.Columns.Remove("JAN");
        //        dt.Columns.Remove("FEB");
        //        dt.Columns.Remove("MAR");
        //        dt.Columns.Remove("QUARTER1");
        //        dt.Columns.Remove("VIEW1");
        //        dt.Columns.Remove("APR");
        //        dt.Columns.Remove("MAY");
        //        dt.Columns.Remove("JUN");
        //        //dt.Columns.Remove("QUARTER2");
        //        dt.Columns.Remove("VIEW2");
        //        dt.Columns.Remove("JUL");
        //        dt.Columns.Remove("AUG");
        //        dt.Columns.Remove("SEP");
        //        dt.Columns.Remove("QUARTER3");
        //        dt.Columns.Remove("VIEW3");
        //        dt.Columns.Remove("OCT");
        //        dt.Columns.Remove("NOV");
        //        dt.Columns.Remove("DEC");
        //        dt.Columns.Remove("QUARTER4");
        //        dt.Columns.Remove("VIEW4");
        //        dt.Columns.Remove("SVENDORID");
        //        dt.Columns.Remove("SCONTRACTID");
        //        dt.Columns["QUARTER2"].ColumnName = "QUARTER";
        //        break;
        //    case "3":
        //        //dt.Columns.Remove("SABBREVIATION");
        //        //dt.Columns.Remove("SCONTRACTNO");
        //        dt.Columns.Remove("JAN");
        //        dt.Columns.Remove("FEB");
        //        dt.Columns.Remove("MAR");
        //        dt.Columns.Remove("QUARTER1");
        //        dt.Columns.Remove("VIEW1");
        //        dt.Columns.Remove("APR");
        //        dt.Columns.Remove("MAY");
        //        dt.Columns.Remove("JUN");
        //        dt.Columns.Remove("QUARTER2");
        //        dt.Columns.Remove("VIEW2");
        //        dt.Columns.Remove("JUL");
        //        dt.Columns.Remove("AUG");
        //        dt.Columns.Remove("SEP");
        //        //dt.Columns.Remove("QUARTER3");
        //        dt.Columns.Remove("VIEW3");
        //        dt.Columns.Remove("OCT");
        //        dt.Columns.Remove("NOV");
        //        dt.Columns.Remove("DEC");
        //        dt.Columns.Remove("QUARTER4");
        //        dt.Columns.Remove("VIEW4");
        //        dt.Columns.Remove("SVENDORID");
        //        dt.Columns.Remove("SCONTRACTID");
        //        dt.Columns["QUARTER3"].ColumnName = "QUARTER";
        //        break;
        //    case "4":
        //        //dt.Columns.Remove("SABBREVIATION");
        //        //dt.Columns.Remove("SCONTRACTNO");
        //        dt.Columns.Remove("JAN");
        //        dt.Columns.Remove("FEB");
        //        dt.Columns.Remove("MAR");
        //        dt.Columns.Remove("QUARTER1");
        //        dt.Columns.Remove("VIEW1");
        //        dt.Columns.Remove("APR");
        //        dt.Columns.Remove("MAY");
        //        dt.Columns.Remove("JUN");
        //        dt.Columns.Remove("QUARTER2");
        //        dt.Columns.Remove("VIEW2");
        //        dt.Columns.Remove("JUL");
        //        dt.Columns.Remove("AUG");
        //        dt.Columns.Remove("SEP");
        //        dt.Columns.Remove("QUARTER3");
        //        dt.Columns.Remove("VIEW3");
        //        dt.Columns.Remove("OCT");
        //        dt.Columns.Remove("NOV");
        //        dt.Columns.Remove("DEC");
        //        //dt.Columns.Remove("QUARTER4");
        //        dt.Columns.Remove("VIEW4");
        //        dt.Columns.Remove("SVENDORID");
        //        dt.Columns.Remove("SCONTRACTID");
        //        dt.Columns["QUARTER4"].ColumnName = "QUARTER";
        //        break;
        //    default:
        //        break;
        //}
        //ds.Tables.Add(dt.Copy());
        ////ReportParameter ReportDate = new ReportParameter("ReportDate", "เมษายน 2559");
        ////ReportParameter ReportNo = new ReportParameter("ReportNo", "800000368/");
        ////ReportParameter ReportQuarter = new ReportParameter("ReportQuarter", "1 ปี 2559");
        //ReportViewer1.LocalReport.DataSources.Clear();
        //ReportViewer1.LocalReport.ReportPath = "App_Code\\quarteralert.rdlc";
        //ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { ReportDate, ReportNo, ReportQuarter, ReportVendorName });
        //ReportDataSource _rsource = new ReportDataSource("DataSet1", ds.Tables[0]);
        //ReportViewer1.LocalReport.DataSources.Add(_rsource);
        //ReportViewer1.LocalReport.Refresh();
        #endregion
        if (!string.IsNullOrEmpty(hidID.Value))
        {
            btnCancel.Disabled = false;
        }
        else
        {
            btnCancel.Disabled = true;
        }
        btnConfirm.Disabled = false;
        btnPDF.Enabled = true;
    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        //string
        try
        {
            string Mess = string.Empty;
            SetDataToSave();
            if (QuarterReportBLL.Instance.Save(hidID.Value, lblYEAR.Text, lblQuarter.Text, hidSVENDORID.Value, txtNo.Text.Trim(), txtDate.Text.Trim(), Session["UserID"] + string.Empty, dtSave))
            {
                Mess = "แจ้งผู้ขนส่งสำเร็จ";
                if (SendEmail())
                {
                    Mess += "<br/>ส่งอีเมลล์สำเร็จ";

                }
                else
                {
                    Mess += "<br/>ไม่สามารถส่งอีเมลล์ได้";
                }
                alertSuccess(Mess, "Quartery_Report.aspx");
            }
        }
        catch (Exception ex)
        {
            alertFail("แจ้งผู้ขนส่งไม่สำเร็จเนื่องจาก : " + RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region SetDataToSave
    private void SetDataToSave()
    {
        dt = GetData();
        dtSave = new DataTable();
        dtSave.Columns.Add("SCONTRACTID", typeof(string));
        dtSave.Columns.Add("SCONTRACTNO", typeof(string));
        dtSave.Columns.Add("SCORE1", typeof(string));
        dtSave.Columns.Add("SCORE2", typeof(string));
        dtSave.Columns.Add("SCORE3", typeof(string));
        dtSave.Columns.Add("QUARTER", typeof(string));
        switch (lblQuarter.Text.Trim())
        {
            case "1":
                AddData(ref dtSave, dt, "JAN", "FEB", "MAR", "QUARTER1");
                break;
            case "2":
                AddData(ref dtSave, dt, "APR", "MAY", "JUN", "QUARTER2");
                break;
            case "3":
                AddData(ref dtSave, dt, "JUL", "AUG", "SEP", "QUARTER3");
                break;
            default:
                AddData(ref dtSave, dt, "OCT", "NOV", "DEC", "QUARTER4");
                break;
        }
    }
    #endregion

    #region AddData
    private void AddData(ref DataTable dtSave, DataTable dt, string Column1, string Column2, string Column3, string Column4)
    {
        foreach (DataRow item in dt.Rows)
        {
            dtSave.Rows.Add(item["SCONTRACTID"], item["SCONTRACTNO"], item[Column1], item[Column2], item[Column3], item[Column4]);
        }
    }
    #endregion

    #region mpConfirmCancel_ClickOK
    protected void mpConfirmCancel_ClickOK(object sender, EventArgs e)
    {
        try
        {
            SetDataToSave();
            if (QuarterReportBLL.Instance.Save(hidID.Value, lblYEAR.Text, lblQuarter.Text, hidSVENDORID.Value, txtNo.Text.Trim(), txtDate.Text.Trim(), Session["UserID"] + string.Empty, dtSave))
            {
                alertSuccess("ยกเลิกเอกสารสำเร็จ", "Quartery_Report.aspx");
            }
        }
        catch (Exception ex)
        {
            alertFail("ยกเลิกเอกสารไม่สำเร็จเนื่องจาก : " + ex.Message);
        }
    }
    #endregion

    #region SetData
    private void SetData(string Quarter, string SVENDORID, string SABBREVIATION, string YEAR, string ID)
    {
        hidID.Value = ID;
        hidSVENDORID.Value = SVENDORID;
        lblYEAR.Text = YEAR;
        lblSABBREVIATION.Text = SABBREVIATION;
        lblQuarter.Text = Quarter;
    }
    #endregion

    #region GetData
    private DataTable GetData()
    {
        dt = QuarterReportBLL.Instance.ConfigQuarterSelect(lblYEAR.Text.Trim(), lblQuarter.Text.Trim());
        string listSPROCESSID = string.Empty;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataRow dr = dt.Rows[i];
            listSPROCESSID += ",'" + dr["SPROCESSID"] + "'";

        }
        if (!string.IsNullOrEmpty(listSPROCESSID))
        {
            listSPROCESSID = listSPROCESSID.Substring(1);
        }
        else
        {
            listSPROCESSID = "''";
        }
        dt = QuarterReportBLL.Instance.GetData(lblYEAR.Text, hidSVENDORID.Value, listSPROCESSID, 1);
        foreach (DataRow item in dt.Rows)
        {
            if (!IsPostBack)
            {
                switch (lblQuarter.Text.Trim())
                {
                    case "1":
                        txtNo.Text = item["DOCNO1"] + string.Empty;
                        txtDate.Text = item["DATESTR1"] + string.Empty;
                        break;
                    case "2":
                        txtNo.Text = item["DOCNO2"] + string.Empty;
                        txtDate.Text = item["DATESTR2"] + string.Empty;
                        break;
                    case "3":
                        txtNo.Text = item["DOCNO3"] + string.Empty;
                        txtDate.Text = item["DATESTR3"] + string.Empty;
                        break;
                    case "4":
                        txtNo.Text = item["DOCNO4"] + string.Empty;
                        txtDate.Text = item["DATESTR4"] + string.Empty;
                        break;
                    default:
                        break;
                }

            }
            decimal total1 = 0, total2 = 0, total3 = 0, total4 = 0;
            decimal value1 = 0, value2 = 0, value3 = 0, value4 = 0, value5 = 0, value6 = 0, value7 = 0, value8 = 0, value9 = 0, value10 = 0, value11 = 0, value12 = 0;

            if (decimal.Parse(item["JAN"].ToString()) > -1)
                total1 = 1 + total1;
            if (decimal.Parse(item["FEB"].ToString()) > -1)
                total1 = 1 + total1;
            if (decimal.Parse(item["MAR"].ToString()) > -1)
                total1 = 1 + total1;
            if (decimal.Parse(item["JAN"].ToString()) == -1)
                value1 = 0;
            else
                value1 = decimal.Parse(item["JAN"].ToString());
            if (decimal.Parse(item["FEB"].ToString()) == -1)
                value2 = 0;
            else
                value2 = decimal.Parse(item["FEB"].ToString());
            if (decimal.Parse(item["MAR"].ToString()) == -1)
                value3 = 0;
            else
                value3 = decimal.Parse(item["MAR"].ToString());
            if (total1 > 0)
                item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3) / total1)), 0, MidpointRounding.AwayFromZero);
            else
                item["QUARTER1"] = Math.Round(((decimal)((value1 + value2 + value3))), 0, MidpointRounding.AwayFromZero);
            if (decimal.Parse(item["APR"].ToString()) > -1)
                total2 = 1 + total2;
            if (decimal.Parse(item["MAY"].ToString()) > -1)
                total2 = 1 + total2;
            if (decimal.Parse(item["JUN"].ToString()) > -1)
                total2 = 1 + total2;
            if (decimal.Parse(item["APR"].ToString()) == -1)
                value4 = 0;
            else
                value4 = decimal.Parse(item["APR"].ToString());
            if (decimal.Parse(item["MAY"].ToString()) == -1)
                value5 = 0;
            else
                value5 = decimal.Parse(item["MAY"].ToString());
            if (decimal.Parse(item["JUN"].ToString()) == -1)
                value6 = 0;
            else
                value6 = decimal.Parse(item["JUN"].ToString());
            if (total2 > 0)
                item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6) / total2)), 0, MidpointRounding.AwayFromZero);
            else
                item["QUARTER2"] = Math.Round(((decimal)((value4 + value5 + value6))), 0, MidpointRounding.AwayFromZero);
            if (decimal.Parse(item["JUL"].ToString()) > -1)
                total3 = 1 + total3;
            if (decimal.Parse(item["AUG"].ToString()) > -1)
                total3 = 1 + total3;
            if (decimal.Parse(item["SEP"].ToString()) > -1)
                total3 = 1 + total3;
            if (decimal.Parse(item["JUL"].ToString()) == -1)
                value7 = 0;
            else
                value7 = decimal.Parse(item["JUL"].ToString());
            if (decimal.Parse(item["AUG"].ToString()) == -1)
                value8 = 0;
            else
                value8 = decimal.Parse(item["AUG"].ToString());
            if (decimal.Parse(item["SEP"].ToString()) == -1)
                value9 = 0;
            else
                value9 = decimal.Parse(item["SEP"].ToString());
            if (total3 > 0)
                item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9) / total3)), 0, MidpointRounding.AwayFromZero);
            else
                item["QUARTER3"] = Math.Round(((decimal)((value7 + value8 + value9))), 0, MidpointRounding.AwayFromZero);
            if (decimal.Parse(item["OCT"].ToString()) > -1)
                total4 = 1 + total4;
            if (decimal.Parse(item["NOV"].ToString()) > -1)
                total4 = 1 + total4;
            if (decimal.Parse(item["DEC"].ToString()) > -1)
                total4 = 1 + total4;
            if (decimal.Parse(item["OCT"].ToString()) == -1)
                value10 = 0;
            else
                value10 = decimal.Parse(item["OCT"].ToString());
            if (decimal.Parse(item["NOV"].ToString()) == -1)
                value11 = 0;
            else
                value11 = decimal.Parse(item["NOV"].ToString());
            if (decimal.Parse(item["DEC"].ToString()) == -1)
                value12 = 0;
            else
                value12 = decimal.Parse(item["DEC"].ToString());
            if (total4 > 0)
                item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12) / total4)), 0, MidpointRounding.AwayFromZero);
            else
                item["QUARTER4"] = Math.Round(((decimal)((value10 + value11 + value12))), 0, MidpointRounding.AwayFromZero);

            if (decimal.Parse(item["JAN"].ToString()) == -1 && decimal.Parse(item["FEB"].ToString()) == -1 && decimal.Parse(item["MAR"].ToString()) == -1)
                item["QUARTER1"] = DBNull.Value;
            if (decimal.Parse(item["APR"].ToString()) == -1 && decimal.Parse(item["MAY"].ToString()) == -1 && decimal.Parse(item["JUN"].ToString()) == -1)
                item["QUARTER2"] = DBNull.Value;
            if (decimal.Parse(item["JUL"].ToString()) == -1 && decimal.Parse(item["AUG"].ToString()) == -1 && decimal.Parse(item["SEP"].ToString()) == -1)
                item["QUARTER3"] = DBNull.Value;
            if (decimal.Parse(item["OCT"].ToString()) == -1 && decimal.Parse(item["NOV"].ToString()) == -1 && decimal.Parse(item["DEC"].ToString()) == -1)
                item["QUARTER4"] = DBNull.Value;

            if (decimal.Parse(item["JAN"].ToString()) < 0)
                item["JAN"] = DBNull.Value;
            if (decimal.Parse(item["FEB"].ToString()) < 0)
                item["FEB"] = DBNull.Value;
            if (decimal.Parse(item["MAR"].ToString()) < 0)
                item["MAR"] = DBNull.Value;

            if (decimal.Parse(item["APR"].ToString()) < 0)
                item["APR"] = DBNull.Value;
            if (decimal.Parse(item["MAY"].ToString()) < 0)
                item["MAY"] = DBNull.Value;
            if (decimal.Parse(item["JUN"].ToString()) < 0)
                item["JUN"] = DBNull.Value;

            if (decimal.Parse(item["JUL"].ToString()) < 0)
                item["JUL"] = DBNull.Value;
            if (decimal.Parse(item["AUG"].ToString()) < 0)
                item["AUG"] = DBNull.Value;
            if (decimal.Parse(item["SEP"].ToString()) < 0)
                item["SEP"] = DBNull.Value;

            if (decimal.Parse(item["OCT"].ToString()) < 0)
                item["OCT"] = DBNull.Value;
            if (decimal.Parse(item["NOV"].ToString()) < 0)
                item["NOV"] = DBNull.Value;
            if (decimal.Parse(item["DEC"].ToString()) < 0)
                item["DEC"] = DBNull.Value;
        }
        return dt;
    }
    #endregion

    #region DownloadFile
    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    private void DownloadFileWord(string Path, string FileName, string doc)
    {

        //Response.Clear();
        //Response.ContentType = "application/msword";
        //Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.ClearContent();
        Response.Charset = "";
        Response.AppendHeader("content-disposition", "attachment;  filename=" + FileName);
        Response.ContentType = "application/vnd.ms-xpsdocument";
        Response.Write(doc);
        Response.End();
    }
    #endregion

    #region CheckPath
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region brnPDF_Click
    protected void brnPDF_Click(object sender, EventArgs e)
    {
        int MarginSize = 45;
        HtmlToPdf converter = new HtmlToPdf();
        converter.Options.MarginTop = -10;
        converter.Options.MarginLeft = MarginSize;
        converter.Options.MarginBottom = -10;
        converter.Options.MarginRight = MarginSize;
        var sb = new StringBuilder();
        divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
        ///exgap1ch.jpeg
        string s = sb.ToString();
        // convert the url to pdf 
        PdfDocument doc = converter.ConvertHtmlString(sb.ToString());

        string Path = this.CheckPath();
        string FileName = "Quartery_Alert_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";

        doc.Save(Response, false, FileName);
        //this.DownloadFile(Path, FileName);
        doc.Close();
    }
    #endregion

    #region btnWord_Click
    protected void btnWord_Click(object sender, EventArgs e)
    {
        var sb = new StringBuilder();
        divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
        string Path = this.CheckPath();
        string FileName = "Quartery_Alert_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".doc";

        string wordHeader = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" ";
        wordHeader += "xmlns:w=\"urn:schemas-microsoft-com:office:word\" ";
        wordHeader += "xmlns=\"http://www.w3.org/TR/REC-html40\"> ";
        wordHeader += "<head><title>Document Title</title>";
        wordHeader += "<!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:Zoom>100</w:Zoom>";
        wordHeader += "<w:DoNotOptimizeForBrowser/></w:WordDocument></xml><![endif]-->";
        wordHeader += "<style> @page Section1 {";
        wordHeader += "";
        wordHeader += "margin:auto ; mso-header-margin:auto; ";
        wordHeader += "mso-footer-margin:-5; mso-paper-source:0;} ";
        wordHeader += "div.Section1 {page:Section1;} p.MsoFooter, li.MsoFooter, ";
        wordHeader += "div.MsoFooter{margin:0in; margin-bottom:.0001pt; ";
        wordHeader += "mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; ";
        wordHeader += "} ";
        wordHeader += "p.MsoHeader, li.MsoHeader, div.MsoHeader {margin:0in; ";
        wordHeader += "margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center ";
        wordHeader += "3.0in right 6.0in;}--></style></head> ";

        sb.Insert(0, wordHeader + "<body><div class=Section1>");
        sb.AppendLine("</div></body></html>");
        this.DownloadFileWord(Path, FileName, sb.ToString());
    }
    #endregion

    #region SendEmail
    private bool SendEmail()
    {
        try
        {


            bool isREs = false;
            DataTable dtComplainEmail = new DataTable();
            string Subject = "แจ้งผลการประเมินการทำงานขนส่ง ประจำไตรมาสที่ {QUARTER} ปี {YEAR}";
            Subject = Subject.Replace("{QUARTER}", lblQuarter.Text.Trim());
            Subject = Subject.Replace("{YEAR}", YearAdd543(lblYEAR.Text.Trim()));
            string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty;
            if (lblSABBREVIATION.Text.Trim().Contains("บริษัท"))
            {
                SVENDORNAME = lblSABBREVIATION.Text.Trim().Replace("บริษัท", "");
            }
            else
            {
                SVENDORNAME = lblSABBREVIATION.Text.Trim().Replace("ห้างหุ้นส่วนจำกัด", "");
                SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
            }
            string Body = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Email.txt"));
            Body = Body.Replace("{YEAR}", YearAdd543(lblYEAR.Text.Trim()));
            Body = Body.Replace("{QUARTER}", lblQuarter.Text.Trim());
            Body = Body.Replace("{SVENDORTITLE}", SVENDORTITLE);
            Body = Body.Replace("{SVENDORNAME}", SVENDORNAME);

            #region VendorEmployeeAdd
            string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), hidSVENDORID.Value, true, false);
            //EmailList += ",zsuntipab.k@pttict.com,zchanaphon.p@pttict.com";

            string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Config_QuarterReport.aspx";
            Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
            MailService.SendMail(EmailList, Subject, Body, "", "VendorEmployeeAdd", ColumnEmailName);
            #endregion
            isREs = true;


            return isREs;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
            //return false;
        }
    }

    #endregion

    private string YearAdd543(string Year)
    {
        return (!string.IsNullOrEmpty(Year) ? int.Parse(Year) + 543 + string.Empty : string.Empty);
    }
}