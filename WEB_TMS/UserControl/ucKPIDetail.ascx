﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucKPIDetail.ascx.cs" Inherits="UserControl_ucKPIDetail" ClassName="UserControl_ucKPIDetail" %>
<asp:Label ID="lblHeaderName" runat="server"></asp:Label>
<br />
<asp:Label ID="lblKPI" runat="server"></asp:Label>
<asp:GridView ID="dgv" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
    CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
    <Columns>
        <asp:TemplateField HeaderText = "ข้อที่" ItemStyle-Width="80">
            <ItemTemplate>
                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="TOPIC_HEADER_ID" Visible="false">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="TOPIC_DETAIL_ID" Visible="false">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="FORMULA_ID" Visible="false">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="METHOD_ID" Visible="false">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:TemplateField HeaderText = "METHOD_ID" Visible="false">
            <ItemTemplate>
                <asp:Label ID="lblMethodID" runat="server" Text='<%# Eval("METHOD_ID") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="METHOD_NAME" HeaderText="วิธีคำนวณผลรวม">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="TOPIC_NAME" HeaderText="ชื่อหัวข้อประเมิน">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="FORMULA_NAME" HeaderText="สูตรการคำนวณ">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="FREQUENCY" HeaderText="ความถี่">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="WEIGHT" HeaderText="Weight">
            <HeaderStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_1" HeaderText="ม.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_2" HeaderText="ก.พ.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_3" HeaderText="มี.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_4" HeaderText="เม.ย.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_5" HeaderText="พ.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_6" HeaderText="มิ.ย.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_7" HeaderText="ก.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_8" HeaderText="ส.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_9" HeaderText="ก.ย.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_10" HeaderText="ต.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_11" HeaderText="พ.ย.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="MONTH_12" HeaderText="ธ.ค.">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
        <asp:BoundField DataField="POINT" HeaderText="ผลลัพธ์">
            <HeaderStyle HorizontalAlign="Center" />
            <ItemStyle HorizontalAlign="Center" />
        </asp:BoundField>
    </Columns>
    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
    <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
    <PagerStyle CssClass="pagination-ys" />
    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
</asp:GridView>
<br />
<br />