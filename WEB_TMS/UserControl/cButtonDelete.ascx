﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="cButtonDelete.ascx.cs" Inherits="UserControl_cButtonDelete" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>

<div class="ImageButtonDelete16 center col-sm-1" style="margin-left:5px;" data-toggle="modal" data-target='<%# "#ModalConfirmDelete" + IDDelete %>'></div>
<uc1:ModelPopup runat="server" id="mpDelete" IsButtonTypeSubmit="false"  IsOnClick="true" IsShowClose="true" OnClickOK="btnDelete_Click"   TextTitle="ยืนยันการลบ" TextDetail="ท่านต้องการที่จะลบใช่หรือไม่ ?"  CommandArgument='<%# IDDelete %>' IDModel='<%# "ModalConfirmDelete" + IDDelete %>' />
<asp:HiddenField runat="server" ID="hidIDDelete" />