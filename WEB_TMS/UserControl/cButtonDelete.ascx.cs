﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_cButtonDelete : System.Web.UI.UserControl
{
        #region Member
        /// <summary>
        /// MemoNo or ContractNo or ID
        /// </summary>
        public string IDDelete
        {
            get { return hidIDDelete.Value; }
            set { hidIDDelete.Value = value; }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            //mpDelete.IDModel = "ModalConfirmDelete" + IDDelete;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ClickDelete != null)
            {
                ClickDelete(sender, e);
            }
        }

        public event EventHandler ClickDelete;
}