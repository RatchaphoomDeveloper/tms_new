﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModelPopup.ascx.cs" Inherits="UserControl_ModelPopup" %>
<div class="modal fade" style="z-index:1060" id='<%= IDModel %>' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" data-backdrop="static" >
        <div class="modal-dialog">
            <div class="modal-content" style="text-align:left;">
                <div class="modal-header">
                    <h4 class="modal-title"><%= TextTitle %></h4>
                </div>
                <div class="modal-body" style="word-wrap:break-word;">
                    <table style="width:100%">
                        <tr>
                            <td valign="top" style="width:50px;"><asp:Image ImageUrl="../Images/Question_Mark.png" ID="imgPopup" runat="server" /></td>
                            <td valign="top" align="left" style="font-size:16px;" class="TextDetail"> <%= TextDetail %></td>
                        </tr>
                    </table>
                    
                   

                </div>
                <div class="modal-footer">
<asp:Button runat="server" ID="btnSave" CommandArgument='<%# CommandArgument %>' OnClientClick="$($(this).closest('.modal').get(0)).find('button').click()" Text="" CssClass="btn btn-md bth-hover btn-info" />  
                   
<button id="btnClose" runat="server" type="button" class="btn btn-md bth-hover btn-info" data-dismiss="modal" ><%= TextClose %></button>     
                   
                </div>
            </div>
        </div>
    </div>
<div class="url" style="display:none;">
<asp:HiddenField ID="hidUrl" runat="server" />
    </div>
<asp:HiddenField ID="hidIDModel" runat="server" />