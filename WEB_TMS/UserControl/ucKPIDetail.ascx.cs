﻿using System;

public partial class UserControl_ucKPIDetail : System.Web.UI.UserControl
{
    #region + Properties +
    public string HeaderName
    {
        get
        {
            return this.lblHeaderName.Text;
        }
        set
        {
            this.lblHeaderName.Text = value;
        }
    }

    public object DataSource
    {
        get
        {
            return this.dgv.DataSource;
        }
        set
        {
            this.dgv.DataSource = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
            {
                dgv.Columns[GridViewHelper.GetColumnIndexByName(dgv, "METHOD_NAME")].Visible = false;
                dgv.Columns[GridViewHelper.GetColumnIndexByName(dgv, "FORMULA_NAME")].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}