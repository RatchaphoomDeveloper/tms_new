﻿using System;
using System.Web.UI;
public partial class UserControl_successModel : System.Web.UI.UserControl
{
    #region Member
    public string IDModel
    {
        set { hidIDModel.Value = value; }
        get { return hidIDModel.Value; }
    }

    string texttitle = string.Empty;
    public string TextTitle
    {
        set { texttitle = value; }
        get { return texttitle; }
    }
    bool isshowok = true;
    public bool IsShowOk
    {
        set { isshowok = value; }
        get { return isshowok; }
    }

    bool isshowclose = true;
    public bool IsShowClose
    {
        set { isshowclose = value; }
        get { return isshowclose; }
    }
    string textdetail = string.Empty;
    public string TextDetail
    {
        set { textdetail = value; }
        get { return textdetail; }
    }
    bool isbuttontypesubmit = false;
    public bool IsButtonTypeSubmit
    {
        set { isbuttontypesubmit = value; }
        get { return isbuttontypesubmit; }
    }
    bool isonclick = false;
    public bool IsOnClick
    {
        set { isonclick = value; }
        get { return isonclick; }
    }
    string _btnclose = string.Empty;
    public string btnclose
    {
        set { _btnclose = value; }
        get { return _btnclose; }
    }
    string commandargument = string.Empty;
    public string CommandArgument
    {
        set { commandargument = value; }
        get { return commandargument; }
    }
    string imagename = "imgInformationGreen.png";
    public string ImageName
    {
        set { imagename = value; }
        get { return imagename; }
    }

    string textOK = "ตกลง";
    public string TextOK
    {
        set { textOK = value; }
        get { return textOK; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        cmdModalSave.UseSubmitBehavior = IsButtonTypeSubmit ? true : false;
        if (IsOnClick)
        {
            cmdModalSave.Click += btnSave_Click;
        }
        else
        {
            cmdModalSave.Click -= btnSave_Click;
        }
        if (!IsShowClose)
        {
            cmdModalClose.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        else
        {
            cmdModalClose.Style.Remove(HtmlTextWriterStyle.Display);

        }
        if (!IsShowOk)
        {
            cmdModalSave.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        else
        {
            cmdModalSave.Style.Remove(HtmlTextWriterStyle.Display);
        }
        imgPopup.ImageUrl = "../Images/" + ImageName;
        cmdModalSave.Text = textOK;
    }
    public event EventHandler ClickOK;
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ClickOK != null)
        {
            ClickOK(sender, e);
        }
        else
        {
            if (!string.IsNullOrEmpty(hidUrl.Value))
            {
                Response.Redirect(hidUrl.Value);
            }
        }

    }

}