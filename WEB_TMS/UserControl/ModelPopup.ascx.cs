﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControl_ModelPopup : System.Web.UI.UserControl
{
    #region Member
    public string IDModel
    {
        set { hidIDModel.Value = value; }
        get { return hidIDModel.Value; }
    }

    string texttitle = string.Empty;
    public string TextTitle
    {
        set { texttitle = value; }
        get { return texttitle; }
    }

    string textdetail = string.Empty;
    public string TextDetail
    {
        set { textdetail = value; }
        get { return textdetail; }
    }

    bool isbuttontypesubmit = false;
    public bool IsButtonTypeSubmit
    {
        set { isbuttontypesubmit = value; }
        get { return isbuttontypesubmit; }
    }

    bool isonclick = false;
    public bool IsOnClick
    {
        set { isonclick = value; }
        get { return isonclick; }
    }

    bool isshowok = true;
    public bool IsShowOk
    {
        set { isshowok = value; }
        get { return isshowok; }
    }

    bool isshowclose = true;
    public bool IsShowClose
    {
        set { isshowclose = value; }
        get { return isshowclose; }
    }

    string textok = "OK";
    public string TextOk
    {
        set { textok = value; }
        get { return textok; }
    }

    string textclose = "CANCEL";
    public string TextClose
    {
        set { textclose = value; }
        get { return textclose; }
    }

    string imagename = "Icon_Lamp.png";
    public string ImageName
    {
        set { imagename = value; }
        get { return imagename; }
    }

    string commandargument = string.Empty;
    public string CommandArgument
    {
        set { commandargument = value; }
        get { return commandargument; }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }

        if (!string.IsNullOrEmpty(CommandArgument))
        {
            btnSave.CommandArgument = CommandArgument;
        }
        //
        btnSave.UseSubmitBehavior = IsButtonTypeSubmit ? true : false;
        if (IsOnClick)
        {
            btnSave.Click += btnSave_Click;
        }
        else
        {
            btnSave.Click -= btnSave_Click;
        }
        if (!IsShowClose)
        {
            btnClose.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        else
        {
            btnClose.Style.Remove(HtmlTextWriterStyle.Display);

        }
        if (!IsShowOk)
        {
            btnSave.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
        else
        {
            btnSave.Style.Remove(HtmlTextWriterStyle.Display);
        }
        imgPopup.ImageUrl = "../Images/" + ImageName;
        btnSave.Text = TextOk;
    }

    public event EventHandler ClickOK;
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ClickOK != null)
        {
            ClickOK(sender, e);
        }
        else
        {
            if (!string.IsNullOrEmpty(hidUrl.Value))
            {
                Response.Redirect(hidUrl.Value);
            }
        }

    }
}