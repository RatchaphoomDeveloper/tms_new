﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxEditors;


public partial class ReportGrade : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            checkPermitionReport();

            string cYear = DateTime.Today.Year.ToString();
            int nYear = Convert.ToInt16(cYear);
            for (int i = 0; i <= 20; i++)
            {
                int Year = nYear - i;
                cmbYear.Items.Add(Year + 543 + "", Year);
            }
            cmbYear.SelectedIndex = 0;

        }
        else
        {
            CreateReport();
        }
    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v 
WHERE  V.SVENDORNAME  like :fillter 
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void checkPermitionReport()
    {
        // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

    }

    protected void CreateReport()
    {

        //(SELECT    nvl(r.SCONTRACTID,C.SCONTRACTID) AS SCONTRACTID, (((1200 - (SUM(nvl(R.NPOINT, 0)))) / 1200) * 100) * 0.7 AS SUMPOINT
        //                                                 FROM            ((TREDUCEPOINT r LEFT JOIN
        //                                                                           TTRUCK t ON R.SHEADREGISTERNO = T .SHEADREGISTERNO) LEFT JOIN
        //                                                                           TCONTRACT_TRUCK ct ON T .STRUCKID = CT.STRUCKID) LEFT JOIN
        //                                                                           TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID
        //                                                 WHERE        To_Char(R.DREDUCE, 'YYYY') = '{0}'
        //                                                 GROUP BY nvl(r.SCONTRACTID,C.SCONTRACTID))

        string ratio_grade_lab = WebConfigurationManager.AppSettings["ratio_grade_lab"] + "";
        string ratio_grade_home = WebConfigurationManager.AppSettings["ratio_grade_home"] + "";
        string ratio_grade_KPI = WebConfigurationManager.AppSettings["ratio_grade_KPI"] + "";

        double temp;
        double ngrad_lab = double.TryParse(ratio_grade_lab,out temp) ?temp : 0.0;
        double ngrad_home = double.TryParse(ratio_grade_home, out temp) ? temp : 0.0;
        double ngrad_KPI = double.TryParse(ratio_grade_KPI, out temp) ? temp : 0.0;

        string sngrad_lab = (ngrad_lab / 100.0).ToString("0.0");
        string sgrad_home = (ngrad_home / 100.0).ToString("0.0");
        string sgrad_KPI = (ngrad_KPI / 100.0).ToString("0.0");

        string sqlquery = string.Format(@"
SELECT  t.SVENDORID,t.SCONTRACTNO, t.SUMPOINT, t.QUESTIONNAIRESCORE, t.KPISCORE, t.NGRADE, g.SGRADENAME
FROM            (SELECT  cc.SVENDORID,  cc.SCONTRACTID,CC.SCONTRACTNO, ROUND(SUM(nvl(RP.SUMPOINT, {2})), 0) AS SUMPOINT, ROUND(SUM(nvl(QN.QUESTIONNAIRESCORE, 0)), 0) 
                                                    AS QUESTIONNAIRESCORE, ROUND(SUM(nvl(kpi.KPISCORE, 0)), 0) AS KPISCORE ,ROUND(SUM(nvl(RP.SUMPOINT, {2})), 0) + ROUND(SUM(nvl(QN.QUESTIONNAIRESCORE, 0)), 0) + ROUND(SUM(nvl(kpi.KPISCORE, 0)), 0) AS NGRADE
                          FROM            (TCONTRACT cc LEFT JOIN
                                                       (

SELECT r_t.SCONTRACTID,  (((1200 - (SUM(nvl(r_t.NPOINT, 0)))) / 1200) * 100) * {3} AS SUMPOINT From
(
SELECT EXTRACT(month FROM rr.DREDUCE) as nMonth,    nvl(rr.SCONTRACTID,C.SCONTRACTID) AS SCONTRACTID, case when (SUM(nvl(Rr.NPOINT, 0))) <= 100 then SUM(nvl(Rr.NPOINT, 0)) else 100 End  AS NPOINT
                                                          FROM            ((TREDUCEPOINT rr LEFT JOIN
                                                                                    TTRUCK t ON Rr.SHEADREGISTERNO = T .SHEADREGISTERNO) LEFT JOIN
                                                                                    TCONTRACT_TRUCK ct ON T .STRUCKID = CT.STRUCKID) LEFT JOIN
                                                                                    TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID
                                                          WHERE        To_Char(rr.DREDUCE, 'YYYY') = '{0}'  AND NVL(C.CACTIVE,'Y') = 'Y' 

                                                          GROUP BY nvl(rr.SCONTRACTID,C.SCONTRACTID), EXTRACT(month FROM rr.DREDUCE)
   ) r_t  Group By r_t. SCONTRACTID
 )  RP ON cc.SCONTRACTID = RP.SCONTRACTID) LEFT JOIN
                                                        (SELECT        q.SVENDORID, ((AVG(Q.NVALUE) /
                                                                                        (SELECT        SUM(MAXWEIGHT) AS MAXWEIGHT
                                                                                          FROM            (SELECT        nvl(NWEIGHT, 0) *
                                                                                                                                                  (SELECT        MAX(NTYPEVISITLISTSCORE) AS MAXSCORE
                                                                                                                                                    FROM            TTYPEOFVISITFORMLIST) AS MAXWEIGHT
                                                                                                                    FROM            TVISITFORM
                                                                                                                    WHERE        CACTIVE = '1'))) * 100) * {4} AS QUESTIONNAIRESCORE
                                                          FROM            TQUESTIONNAIRE q
                                                          WHERE        To_Char(Q.DCHECK, 'YYYY') = '{0}'
                                                          GROUP BY q.SVENDORID) QN ON QN.SVENDORID = cc.SVENDORID LEFT JOIN
  ( SELECT        k.SVENDORID, (case when (sum(nvl(k.SUMKPI_WEIGHT,0))) <= 100 then (sum(nvl(k.SUMKPI_WEIGHT,0))) else 100 End) * {5} AS KPISCORE
                                                          FROM            TBL_KPI_RATE k
                                                         INNER JOIN TCONTRACT TC
ON TC.SCONTRACTID = K.SCONTRACTID
WHERE        k.SYEAR =  '{0}' AND NVL(TC.CACTIVE,'Y') = 'Y'
                                                          GROUP BY k.SVENDORID) kpi ON kpi.SVENDORID = cc.SVENDORID
WHERE NVL(cc.CACTIVE,'Y') = 'Y'
                          GROUP BY cc.SCONTRACTID,cc.SVENDORID,CC.SCONTRACTNO) t 
              LEFT JOIN
                         TGRADE g ON t.NGRADE BETWEEN G.NSTARTPOINT AND G.NENDPOINT
                       WHERE t.SVENDORID = '{1}'
", CommonFunction.ReplaceInjection(cmbYear.Value + ""), CommonFunction.ReplaceInjection(cboVendor.Value + ""), ratio_grade_lab, sngrad_lab, sgrad_home, sgrad_KPI);

        DataTable dt = CommonFunction.Get_Data(sql, sqlquery);
        string contract = "";
        int c = 0;
        foreach (DataRow dr in dt.Rows)
        {
            contract += dr["SCONTRACTNO"] + Environment.NewLine;
            c += 1;
        }


        GradeReport report = new GradeReport();
        report.DataSource = dt;
        report.Parameters["CONTRACT"].Value = CommonFunction.ReplaceInjection(contract);
        report.Parameters["COUNTCONTRACT"].Value = c;
        report.Parameters["VENDORNAME"].Value = CommonFunction.ReplaceInjection(hideVendor.Text);
        report.Parameters["YEAR"].Value = CommonFunction.ReplaceInjection(cmbYear.Text);

        report.Name = "หนังสือแจ้งบริษัท";
        this.ReportViewer1.Report = report;
    }
}