﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;
using System.Drawing;

public partial class ReportCarRequest : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            SetCboYear();

        }
        CreateReport();
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": CreateReport();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    void CreateReport()
    {
        string Condition = "";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {
            Condition = " AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'mm') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "' AND to_char(NVL(REQUEST_DATE,CREATE_DATE),'yyyy') = '" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
            lblsTail.Text = "";
        }

        #region Data
        string Query = @"SELECT RQT.REQTYPE_ID, RQT.REQTYPE_NAME,LD.STATUS,LD.NITEm
FROM TBL_REQTYPE RQT
LEFT JOIN 
(
    SELECT '01' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    UNION
     SELECT '01' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
        UNION
     SELECT '01' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
     UNION
         SELECT '02' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    UNION
     SELECT '02' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
        UNION
     SELECT '02' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
          UNION
         SELECT '03' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    UNION
     SELECT '03' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
        UNION
     SELECT '03' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
          UNION
         SELECT '04' as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    UNION
     SELECT '04' as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
        UNION
     SELECT '04' as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
)FD
ON FD.REQTYPE_ID = RQT.REQTYPE_ID
LEFT JOIN
(
    SELECT REQTYPE_ID,
    CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ' WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ' WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ' ELSE  '' END as STATUS,
    COUNT(CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ' WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ' WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ' ELSE  '' END) as NITEm
    FROM TBL_REQUEST
    WHERE 1=1 " + Condition + @"
    GROUP BY REQTYPE_ID,CASE WHEN STATUS_FLAG = '10' THEN 'สำเร็จ'WHEN STATUS_FLAG IN ('02','12','04','07') THEN 'รอดำเนินการ'WHEN STATUS_FLAG IN ('09','01','03','06') THEN 'หน่วยอื่นดำเนินการ'ELSE  '' END
)LD
ON LD.REQTYPE_ID = RQT.REQTYPE_ID AND  FD.STATUS = LD.STATUS

";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        #endregion

        string Query1 = @"SELECT '1' as NO FROM DUAL";

        DataTable dsFill = CommonFunction.Get_Data(conn, Query1);

        string Query2 = @"   SELECT 1 as REQTYPE_ID,'สำเร็จ' as STATUS  FROM DUAL
    UNION
     SELECT 2 as REQTYPE_ID,'รอดำเนินการ' as STATUS  FROM DUAL
        UNION
     SELECT 3 as REQTYPE_ID,'หน่วยอื่นดำเนินการ' as STATUS  FROM DUAL
    ORDER BY REQTYPE_ID";
        DataTable dsSerie = CommonFunction.Get_Data(conn, Query2);
        string Query3 = @"SELECT  REQTYPE_ID, REQTYPE_NAME, ISACTIVE_FLAG, 
   NORDER
FROM TBL_REQTYPE WHERE ISACTIVE_FLAG = 'Y' ORDER BY NORDER";
        DataTable dsArugument = CommonFunction.Get_Data(conn, Query3);




        //gvw.DataSource = dt;
        //gvw.DataBind();SELECT  MS.STATUSREQ_ID,MS.STATUSREQ_NAME,LDS.CSTATUS_FLAG,LDS.SMONTH,LDS.SYEAR

        if (dt.Rows.Count > 0)
        {
            //1
            rpt_CarRequest report = new rpt_CarRequest();

        
            XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
            xrChart2.Series.Clear();
            //add Title
            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "จำนวนธุรกรรมให้บริการ (คำขอ)";
            chartTitle1.Font = new Font("Tahoma", 15); 
            xrChart2.Titles.Add(chartTitle1);

            if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
            {
                ChartTitle chartTitle2 = new ChartTitle();
                chartTitle2.Text = "เดือน " + cboMonth.Text + " " + cboYear.Text;
                chartTitle2.Font = new Font("Tahoma", 13); 
                xrChart2.Titles.Add(chartTitle2);
            }
            if (dsSerie.Rows.Count > 0)
            {
                for (int i = 0; i < dsSerie.Rows.Count; i++)
                {
                    Series series = new Series(dsSerie.Rows[i]["STATUS"] + "", ViewType.Bar);

                    //series.Points.Add(new SeriesPoint("A", new double[] { 10 }));
                    //series.Points.Add(new SeriesPoint("B", new double[] { 20 }));
                    //series.Points.Add(new SeriesPoint("C", new double[] { 30 }));
                    //series.Points.Add(new SeriesPoint("D", new double[] { 40 }));

                    if (dt.Rows.Count > 0)
                    {
                        if (dsArugument.Rows.Count > 0)
                        {
                            for (int k = 0; k < dsArugument.Rows.Count; k++)
                            {
                                DataRow[] dr = dt.Select("STATUS = '" + dsSerie.Rows[i]["STATUS"] + "" + "' AND REQTYPE_ID = '" + dsArugument.Rows[k]["REQTYPE_ID"] + "" + "'", "REQTYPE_ID");

                                if (dr.Count() > 0)
                                {
                                    foreach (DataRow row in dr)
                                    {
                                        double Value = !string.IsNullOrEmpty(row["NITEM"] + "") ? double.Parse(row["NITEM"] + "") : 0;
                                        switch (row["REQTYPE_ID"] + "")
                                        {
                                            case "01": series.Points.Add(new SeriesPoint("ตรวจสอบวัดน้ำ", new double[] { Value }));
                                                break;
                                            case "02": series.Points.Add(new SeriesPoint("ตรวจสอบรับรองความถูกต้อง และตีซีล", new double[] { Value }));
                                                break;
                                            case "03": series.Points.Add(new SeriesPoint("ขอสำเนาเอกสาร", new double[] { Value }));
                                                break;
                                            case "04": series.Points.Add(new SeriesPoint("ขอพ่นสาระสำคัญ", new double[] { Value }));
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    double Value = 0;
                                    switch (dsArugument.Rows[k]["REQTYPE_ID"] + "")
                                    {
                                        case "01": series.Points.Add(new SeriesPoint("ตรวจสอบวัดน้ำ", new double[] { Value }));
                                            break;
                                        case "02": series.Points.Add(new SeriesPoint("ตรวจสอบรับรองความถูกต้อง และตีซีล", new double[] { Value }));
                                            break;
                                        case "03": series.Points.Add(new SeriesPoint("ขอสำเนาเอกสาร", new double[] { Value }));
                                            break;
                                        case "04": series.Points.Add(new SeriesPoint("ขอพ่นสาระสำคัญ", new double[] { Value }));
                                            break;
                                    }
                                }
                            }

                        }
                    }

                    xrChart2.Series.Add(series);
                }
            }

            // xrChart2.DataSource = dt;
            //xrChart2.SeriesDataMember = "STATUS";

            //for (int i = 0; i < dt.Rows.Count; i++)
            //{

            //    xrChart2.Series[0].ArgumentScaleType = ScaleType.Qualitative;
            //    xrChart2.Series[0].ArgumentDataMember = "REQTYPE_NAME";
            //    xrChart2.Series[0].ValueDataMembers[0] = "NITEM";

            //}
            report.Name = "RequestCar";
            string DateShow = "วันที่จัดทำรายงาน " + DateTime.Now.ToString("dd MMMM yyyy");

            ((XRLabel)report.FindControl("XRLabel1", true)).Text = DateShow;


            report.DataSource = dsFill;
            rvw.Report = report;
            rvw.DataBind();
            //report.CreateDocument();
        }

    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}