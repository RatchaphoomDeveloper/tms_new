﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Security;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.SurpriseCheckYear;
using TMS_BLL.Transaction.Accident;
using EmailHelper;
using TMS_BLL.Transaction.Complain;

public partial class surpriseChk_lst : PageBase
{
    private string ID
    {
        get
        {
            if ((string)ViewState["ID"] != null)
                return (string)ViewState["ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    private DataTable dtSurpriseCheckYearList
    {
        get
        {
            if ((DataTable)ViewState["dtSurpriseCheckYearList"] != null)
                return (DataTable)ViewState["dtSurpriseCheckYearList"];
            else
                return null;
        }
        set
        {
            ViewState["dtSurpriseCheckYearList"] = value;
        }
    }
    private DataTable dtTeamCheckData
    {
        get
        {
            if ((DataTable)ViewState["dtTeamCheckData"] != null)
                return (DataTable)ViewState["dtTeamCheckData"];
            else
                return null;
        }
        set
        {
            ViewState["dtTeamCheckData"] = value;
        }
    }

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string vendorID;
    string truckID;
    int contractID;
    string strChkTruck;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            dtTeamCheckData = new DataTable();
            dtTeamCheckData.Columns.Add("SUID");
            dtTeamCheckData.Columns.Add("FULLNAME");
            ID = "0";
            dteChk.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
            dteStart.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            AppointDate.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
            AssignAuthen();
            this.LoadVendor();
            this.LoadUser();
            this.LoadTeamCheck();
            LoadTerminal();
            BindChecker();
            BindData2();

            txtRegisBy.Text = CommonFunction.Get_Value(sql, "SELECT SFIRSTNAME || ' ' || SLASTNAME FROM TUSER WHERE SUID = '" + Session["UserID"] + "'");
            this.LoadData();
            this.InitialForm();
            if (Session["CGROUP"].ToString() == "2")
                TabYear.Visible = false;
        }
        if (Session["vendorID"] != null)
        {
            vendorID = Session["vendorID"].ToString();
        }
        if (Session["truckID"] != null)
        {
            truckID = Session["truckID"].ToString();
        }
        if (Session["contractID"] != null)
        {
            contractID = Convert.ToInt32(Session["contractID"]);
        }
        if (Session["strChkTruck"] != null)
        {
            strChkTruck = Session["strChkTruck"].ToString();
        }


    }
    private void InitialForm()
    {
        try
        {
            if (Request.QueryString["str"] != null)
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                var decryptedBytes2 = MachineKey.Decode(Request.QueryString["locate"], MachineKeyProtection.All);
                var decryptedValue2 = Encoding.UTF8.GetString(decryptedBytes2);
                var decryptedBytes3 = MachineKey.Decode(Request.QueryString["user"], MachineKeyProtection.All);
                var decryptedValue3 = Encoding.UTF8.GetString(decryptedBytes3);
                txtTruck.Text = decryptedValue;
                txtCheckPlace.Text = decryptedValue2;
                ddlChecker.SelectedValue = decryptedValue3;
                //btnSearch_Click(null, null);
                btnConfirm_Click1(null, null);
                btnConfirmSave_Click(null, null);
                btnConfirmCheckTruck_Click(null, null);
            }
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ClickTab1", "$('#aSurpriceCheck').tab('show');", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadData()
    {
        try
        {
            dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(string.Empty);
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    private void LoadUser()
    {
        try
        {
            String Group_id = ConfigurationManager.AppSettings["Checker_Group_ID"];
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND USERGROUP_ID IN (" + Group_id + ")");
            DropDownListHelper.BindDropDownList(ref ddlUserCheck, dtUser, "SUID", "FULLNAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTeamCheck()
    {
        try
        {
            String Group_id = ConfigurationManager.AppSettings["Checker_Group_ID"];
            DataTable dtTeam = UserBLL.Instance.UserSelectBLL(" AND USERGROUP_ID IN (" + Group_id + ")");
            DropDownListHelper.BindDropDownList(ref ddlTeamCheck, dtTeam, "SUID", "FULLNAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnConfirm.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("surpriseChk_show.aspx");
    }

    protected void btnSearch_Click(Object sender, EventArgs e)
    {
        //GetAutoCompletHeadereData(txtHeadNumber.Text);
    }

    protected void btnSearch2_Click(Object sender, EventArgs e)
    {
        BindData();
    }

    public bool GetAutoCompletHeadereData(string HeadNumber)
    {
        string message = string.Empty;
        string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
        bool result = false;
        try
        {
            string query = @"SELECT SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID,SCONTRACTID,SCONTRACTNO,SVENDORID,SVENDORNAME
                FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
                c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME FROM ((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
                INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID
                WHERE NVL(C.CACTIVE,'Y') = 'Y' AND t.SHEADREGISTERNO LIKE '%" + HeadNumber + "%' )";
            DataTable dt = CommonFunction.Get_Data(sql, query);
            if (dt != null)
            {
                if (dt.Rows.Count == 1)
                {
                    txtTrailerNumber.Text = dt.Rows[0]["STRAILERREGISTERNO"].ToString();
                    txtContactNo.Text = dt.Rows[0]["SCONTRACTNO"].ToString();
                    txtVendorName.Text = dt.Rows[0]["SVENDORNAME"].ToString();
                    vendorID = dt.Rows[0]["SVENDORID"].ToString();
                    truckID = dt.Rows[0]["STRUCKID"].ToString();
                    contractID = Convert.ToInt32(dt.Rows[0]["SCONTRACTID"]);
                    Session["vendorID"] = vendorID;
                    Session["truckID"] = truckID;
                    Session["contractID"] = contractID;
                    txtCheckPlace.ReadOnly = false;
                    dteChk.ReadOnly = false;
                    //txtTime.ReadOnly = false;
                    //txtChecker.ReadOnly = false;
                    ddlChecker.Enabled = true;
                    BindChecker();
                    btnConfirm.Enabled = true;
                }
                else if (dt.Rows.Count > 1)
                {
                    string msg = "กรุณาระบุข้อมูลให้ถูกต้อง";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + msg + "')", true);
                }
                else
                {
                    string msg = "กรุณาระบุข้อมูลให้ถูกต้อง";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + msg + "')", true);
                }
            }
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {

        }
        return result;
    }

    protected void txtHeaderNumber_TextChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "SearchText()", true);
    }

    protected void BindData()
    {
        try
        {
            string query = "SELECT ROW_NUMBER() OVER (ORDER BY ts.NSURPRISECHECKID) AS ID1,ts.NSURPRISECHECKID,TS.SHEADREGISTERNO,TS.STRAILERREGISTERNO,to_char(ts.DSURPRISECHECK,'dd/mm/yyyy') || ' ' ||TTIME DSURPRISECHECK,ts.TTIME,vs.SVENDORNAME,ts.STRUCKID,C.SCONTRACTNO,C.SCONTRACTID,c.SVENDORID,ts.SSURPRISECHECKBY"
                           + " FROM TSURPRISECHECKHISTORY ts INNER JOIN TTRUCK t on TS.STRUCKID = t.STRUCKID "
                           + " INNER JOIN TCONTRACT c ON TS.SCONTRACTID = C.SCONTRACTID "
                           + " INNER JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID";
            if (txtSearch.Text != "" || dteStart.Text != "" || cboTermminal.SelectedItem.Value != "" || cboChecker.SelectedItem.Value != "")
            {
                query += " WHERE 1=1 ";
                if (txtSearch.Text != "")
                {
                    query += "  AND (C.SCONTRACTNO LIKE '%' || '" + txtSearch.Text + "' || '%' OR VS.SVENDORNAME LIKE '%' || '" + txtSearch.Text + "' || '%'  OR TS.SHEADREGISTERNO LIKE '%' || '" + txtSearch.Text + "' || '%'  OR TS.STRAILERREGISTERNO LIKE '%' || '" + txtSearch.Text + "' || '%' ) ";
                }

                if (dteStart.Text != "")
                {


                    query += " AND  (TO_DATE('" + dteStart.Text + "','dd/MM/yyyy') = to_date(DSURPRISECHECK)) ";

                }
                if (cboTermminal.SelectedItem.Value != "")
                {
                    query += " AND nvl(c.STERMINALID,1) LIKE '%' || '" + cboTermminal.SelectedItem.Value + "' || '%'";
                }

                if (cboChecker.SelectedItem.Value != "" && cboChecker.SelectedIndex > 0)
                {
                    query += " AND nvl(SSURPRISECHECKBY,1) LIKE '%' || '" + cboChecker.SelectedItem.Value + "' || '%'";
                }
            }

            DataTable dt = CommonFunction.Get_Data(sql, query);
            gvw.DataSource = dt;
            gvw.DataBind();

            Session["mySearch"] = dt;

            lblCarCount.Text = dt.Rows.Count.ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void BindData2()
    {
        try
        {
            string query = "SELECT ROW_NUMBER() OVER (ORDER BY NSURPRISECHECKID) AS ID1,NSURPRISECHECKID,TS.SHEADREGISTERNO,TS.STRAILERREGISTERNO,DSURPRISECHECK,TTIME,SVENDORNAME,SVENDORID,STRUCKID,SCONTRACTNO,C.SCONTRACTID,SSURPRISECHECKBY"
                           + " FROM TSURPRISECHECKHISTORY ts INNER JOIN TTRUCK t on TS.STRUCKID = t.STRUCKID "
                           + " LEFT JOIN TCONTRACT_TRUCK ct ON CT.STRUCKID = T.STRUCKID LEFT JOIN TCONTRACT c ON C.SCONTRACTID = CT.SCONTRACTID "
                           + " LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID";

            DataTable dt = CommonFunction.Get_Data(sql, query);
            gvw.DataSource = dt;
            gvw.DataBind();

            Session["mySearch"] = dt;

            lblCarCount.Text = dt.Rows.Count.ToString();
        }
        catch (Exception ex)
        {

        }
    }

    protected void BindChecker()
    {
        try
        {
            String Group_id = ConfigurationManager.AppSettings["Checker_Group_ID"];
            string query = @"select (SFIRSTNAME || ' ' || SLASTNAME) as fullname from tuser where USERGROUP_ID in ("
                            + Group_id + ")";
            DataTable dt = CommonFunction.Get_Data(sql, query);
            DataTable data = new DataTable();
            data.Columns.Add("fullname", typeof(string));
            data.Rows.Add("==== กรุณาเลือก ====");
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    data.Rows.Add(dt.Rows[i][0]);
                }
            }
            ddlChecker.DataTextField = "fullname";
            ddlChecker.DataValueField = "fullname";
            ddlChecker.DataSource = data;
            ddlChecker.DataBind();

            cboChecker.DataTextField = "fullname";
            cboChecker.DataValueField = "fullname";
            cboChecker.DataSource = data;
            cboChecker.DataBind();
        }
        catch (Exception ex)
        {
            //message = ex.Message;
        }
    }
    //protected void btnConfirm_Click(object sender, EventArgs e)
    //{
    //    if (CanWrite)
    //    {
    //        string GenID;
    //        using (OracleConnection con = new OracleConnection(sql))
    //        {
    //            con.Open();

    //            GenID = CommonFunction.Gen_ID(con, "SELECT NSURPRISECHECKID FROM (SELECT NSURPRISECHECKID FROM TSURPRISECHECKHISTORY ORDER BY NSURPRISECHECKID DESC)  WHERE ROWNUM <= 1");
    //            Session["GENSURPRISECHECKID"] = GenID;
    //            string strsqlSave = @"INSERT INTO TSURPRISECHECKHISTORY (NSURPRISECHECKID, SCONTRACTID, DCHECK, STRUCKID,  SHEADREGISTERNO, STRAILERREGISTERNO, SCREATE,  DCREATE, COK, DSURPRISECHECK,  TTIME, SSURPRISECHECKPLACE, SSURPRISECHECKBY, 
    //            CNOCHECK,  SUPDATE, DUPDATE, CUPDATE) VALUES (:NSURPRISECHECKID, :SCONTRACTID, SYSDATE, :STRUCKID,  :SHEADREGISTERNO, :STRAILERREGISTERNO, :SCREATE,  SYSDATE, '1', :DSURPRISECHECK,  :TTIME, :SSURPRISECHECKPLACE, :SSURPRISECHECKBY, 
    //            :CNOCHECK,  :SUPDATE, SYSDATE, '1')";

    //            using (OracleCommand com = new OracleCommand(strsqlSave, con))
    //            {
    //                DateTime DateTime;
    //                com.Parameters.Clear();
    //                com.Parameters.Add(":NSURPRISECHECKID", OracleType.Number).Value = GenID;
    //                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = txtContactNo.Text + "";
    //                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtHeadNumber.Text;
    //                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = txtHeadNumber.Text + "";
    //                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerNumber.Text + "";
    //                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
    //                com.Parameters.Add(":DSURPRISECHECK", OracleType.DateTime).Value = Convert.ToDateTime(dteChk.Text).ToString("YYYY-mm-dd HH:MM:ss"); //DateTime.TryParse(dteCheck.Value + "", out DateTime) ? DateTime : DateTime.Now;
    //                com.Parameters.Add(":TTIME", OracleType.VarChar).Value = txtTime.Text;
    //                com.Parameters.Add(":SSURPRISECHECKPLACE", OracleType.VarChar).Value = txtCheckPlace.Text;
    //                com.Parameters.Add(":SSURPRISECHECKBY", OracleType.VarChar).Value = ddlChecker.Text;
    //                com.Parameters.Add(":CNOCHECK", OracleType.Char).Value = (chkCheck.Checked) ? '1' : '0';
    //                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"];
    //                com.ExecuteNonQuery();
    //            }
    //        }

    //        //Server.UrlEncode(STCrypt.Encrypt(txtContractID.Text + "&" + txtTruckID.Text + "&" + cboHeadRegist.Value + "&" + cboTrailerRegist.Value + "&&&3&040")) + "');});");

    //        LogUser("27", "I", "บันทึกข้อมูลหน้า Surprise check", GenID + "");
    //        byte[] plaintextBytes = Encoding.UTF8.GetBytes(txtContactNo.Text + "&" + txtHeadNumber.Text + "&" + txtHeadNumber.Text + "&" + txtTrailerNumber.Text + "&&" + Session["GENSURPRISECHECKID"] + "&3&040");
    //        string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
    //        //CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','<b>บันทึกข้อมูลเรียบร้อยแล้ว ท่านต้องการทำรายการตรวจสภาพรถ</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide();" + "performSyncronousRequest('" + "checktruck.aspx?str=" + str + "');" + "} ,function(s,e){ dxPopupConfirm.Hide();xcpn.PerformCallback('ClearData'); })");

    //        //ClearData();

    //    }
    //    else
    //    {
    //        //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
    //        lblAlert.Text = "คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!";
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "myModalAlert", "$('#myModalAlert').modal('show');", true);
    //    }
    //}

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void DeleteRecord(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Record Deleted.')", true);
    }

    protected void btnConfirm_Click1(object sender, EventArgs e)
    {
        bool showModal = true;
        if (ValidateRequirefiled())
        {
            //if (showModal)
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModal", "$('#myModal').modal('show');", true);
        }
        else
        {
            lblAlert.Text = "กรุณากรอกข้อมูลให้ครบถ้วน";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModalAlert", "$('#myModalAlert').modal('show');", true);
        }
    }

    public bool validateControl(Control ctrl)
    {
        bool result = false;
        try
        {
            string ctrlNameType = ctrl.GetType().Name.ToString();
            switch (ctrlNameType.ToLower())
            {
                case "textbox":
                    if (ctrl != null)
                    {
                        TextBox txt = (TextBox)ctrl;
                        if (txt.Text != "")
                        {
                            result = true;
                            txt.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            txt.BackColor = System.Drawing.Color.Coral;
                        }
                    }
                    break;
                case "dropdownlist":
                    if (ctrl != null)
                    {
                        DropDownList ddl = (DropDownList)ctrl;
                        if (ddl.SelectedIndex > 0)
                        {
                            result = true;
                            ddl.BackColor = System.Drawing.Color.White;
                        }
                        else
                        {
                            ddl.BackColor = System.Drawing.Color.Coral;
                        }
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return result;
    }

    protected bool ValidateRequirefiled()
    {
        bool result = false;
        try
        {
            if (!validateControl(txtTruck))
            {
                return false;
            }
            else
            {
                result = true;
            }
            if (!validateControl(txtVendorName))
            {
                return false;
            }
            else
            {
                result = true;
            }
            if (!validateControl(dteChk))
            {
                return false;
            }
            else
            {
                result = true;
            }
            //if (!validateControl(txtTime))
            //{
            //    return false;
            //}
            //else
            //{
            //    result = true;
            //}
            if (!validateControl(txtCheckPlace))
            {
                return false;
            }
            else
            {
                result = true;
            }
            if (!validateControl(ddlChecker))
            {
                return false;
            }
            else
            {
                result = true;
            }
            if (!validateControl(txtRegisBy))
            {
                return false;
            }
            else
            {
                result = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return result;
    }

    private void LoadTerminal()
    {
        string message = string.Empty;
        try
        {
            //cboStatus.SelectedValue
            string query = "SELECT STERMINALID, STERMINALNAME FROM TTERMINAL_SAP WHERE STERMINALID LIKE 'H%' OR STERMINALID LIKE 'J%' OR STERMINALID LIKE 'K%'";

            DataTable dt = CommonFunction.Get_Data(sql, query);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_result = new DataTable();
                dt_result.Columns.Add("STERMINALNAME", typeof(string));
                dt_result.Columns.Add("STERMINALID", typeof(string));
                dt_result.Rows.Add("ทั้งหมด", "");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt_result.Rows.Add(dt.Rows[i]["STERMINALNAME"], dt.Rows[i]["STERMINALID"]);
                }
                cboTermminal.DataSource = dt_result;
                cboTermminal.DataTextField = "STERMINALNAME";
                cboTermminal.DataValueField = "STERMINALID";
                cboTermminal.DataBind();
            }

        }

        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            //GlobalClass.CloseConnection(out message);

        }
    }

    protected void Decision_Command(object sender, CommandEventArgs e)
    {
        //lblOutput.Text = "User clicked - " + e.CommandArgument;
    }

    protected void btnConfirmSave_Click(object sender, EventArgs e)
    {
        if (CanWrite)
        {
            string GenID;
            using (OracleConnection con = new OracleConnection(sql))
            {
                con.Open();

                GenID = CommonFunction.Gen_ID(con, "SELECT NSURPRISECHECKID FROM (SELECT NSURPRISECHECKID FROM TSURPRISECHECKHISTORY ORDER BY NSURPRISECHECKID DESC)  WHERE ROWNUM <= 1");
                Session["GENSURPRISECHECKID"] = GenID;
                string strsqlSave = @"INSERT INTO TSURPRISECHECKHISTORY (NSURPRISECHECKID, SCONTRACTID, DCHECK, STRUCKID,  SHEADREGISTERNO, STRAILERREGISTERNO, SCREATE,  DCREATE, COK, DSURPRISECHECK,  TTIME, SSURPRISECHECKPLACE, SSURPRISECHECKBY, 
                CNOCHECK,  SUPDATE, DUPDATE, CUPDATE) VALUES (:NSURPRISECHECKID, :SCONTRACTID, SYSDATE, :STRUCKID,  :SHEADREGISTERNO, :STRAILERREGISTERNO, :SCREATE,  SYSDATE, '1', TO_DATE(:DSURPRISECHECK, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH'),  :TTIME, :SSURPRISECHECKPLACE, :SSURPRISECHECKBY, 
                :CNOCHECK,  :SUPDATE, SYSDATE, '1')";

                using (OracleCommand com = new OracleCommand(strsqlSave, con))
                {
                    DateTime DateTime;
                    com.Parameters.Clear();
                    com.Parameters.Add(":NSURPRISECHECKID", OracleType.Number).Value = GenID;
                    com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = hidSCONTRACTID.Value;
                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = hidSTRUCKID.Value;
                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = txtTruck.Text + "";
                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerNumber.Text + "";
                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                    com.Parameters.Add(":DSURPRISECHECK", OracleType.VarChar).Value = dteChk.Text.Split(' ')[0]; //.ToString("YYYY-mm-dd HH:MM:ss");DateTime.TryParse(dteCheck.Value + "", out DateTime) ? DateTime : DateTime.Now;
                    com.Parameters.Add(":TTIME", OracleType.VarChar).Value = dteChk.Text.Split(' ')[1];
                    com.Parameters.Add(":SSURPRISECHECKPLACE", OracleType.VarChar).Value = txtCheckPlace.Text;
                    com.Parameters.Add(":SSURPRISECHECKBY", OracleType.VarChar).Value = ddlChecker.Text;
                    com.Parameters.Add(":CNOCHECK", OracleType.Char).Value = (chkCheck.Checked) ? '1' : '0';
                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"];
                    com.ExecuteNonQuery();
                }
            }

            //Server.UrlEncode(STCrypt.Encrypt(txtContractID.Text + "&" + txtTruckID.Text + "&" + cboHeadRegist.Value + "&" + cboTrailerRegist.Value + "&&&3&040")) + "');});");

            LogUser("27", "I", "บันทึกข้อมูลหน้า Surprise check", GenID + "");
            //byte[] plaintextBytes = Encoding.UTF8.GetBytes(contractID + "&" + truckID.ToString() + "&" + txtHeadNumber.Text + "&" + txtTrailerNumber.Text + "&&" + Session["GENSURPRISECHECKID"] + "&3&040");

            byte[] plaintextBytes = Encoding.UTF8.GetBytes(hidSCONTRACTID.Value + "&" + hidSTRUCKID.Value + "&" + txtTruck.Text + "&" + txtTrailerNumber.Text + "&" + dteChk.Text + "&" + GenID + "&" + txtContactNo.Text + "&" + txtVendorName.Text + "&" + hidSVENDORID.Value);
            strChkTruck = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            Session["strChkTruck"] = strChkTruck;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModalConfirm2", "$('#myModalConfirm2').modal('show');", true);
            //CommonFunction.SetPopupOnLoad(xcpn, "dxConfirm('ยืนยันการทำงาน','<b>บันทึกข้อมูลเรียบร้อยแล้ว ท่านต้องการทำรายการตรวจสภาพรถ</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide();" + "performSyncronousRequest('" + "checktruck.aspx?str=" + str + "');" + "} ,function(s,e){ dxPopupConfirm.Hide();xcpn.PerformCallback('ClearData'); })");

            //ClearData();

        }
        else
        {
            //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
            lblAlert.Text = "คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "myModalAlert", "$('#myModalAlert').modal('show');", true);
        }
    }

    protected void btnConfirmCancel_Click(object sender, EventArgs e)
    {

    }

    protected void btnConfirmCheckTruck_Click(object sender, EventArgs e)
    {
        Response.Redirect("SurpriseCheckAddEdit.aspx?str=" + strChkTruck);
    }

    protected void btnCancelCheckTruck_Click(object sender, EventArgs e)
    {
        ClearData();
    }

    protected void ClearData()
    {
        //txtHeadNumber.Text = "";
        txtTrailerNumber.Text = "";
        txtVendorName.Text = "";
        txtContactNo.Text = "";
        dteChk.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
        //txtTime.Text = DateTime.Now.ToShortTimeString();
        txtCheckPlace.Text = "";
        ddlChecker.SelectedIndex = 0;
        vendorID = "";
        truckID = "";
        contractID = 0;
        Session["vendorID"] = null;
        Session["truckID"] = null;
        Session["contractID"] = null;
        this.hidSTRUCKID.Value = string.Empty;
        this.hidSCONTRACTID.Value = string.Empty;
        this.hidSVENDORID.Value = string.Empty;
        txtTruck.Text = string.Empty;
        txtTrailerNumber.Text = string.Empty;
        txtContactNo.Text = string.Empty;
        txtVendorName.Text = string.Empty;
    }

    [WebMethod]
    public static List<string> GetTruck(string prefix)
    {
        List<string> empResult = new List<string>();
        string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
        try
        {
            string query = @"SELECT DISTINCT(SHEADREGISTERNO)
                FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,
                c.SCONTRACTNO,c.SVENDORID,vs.SVENDORNAME FROM ((TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) 
                INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TVENDOR_SAP vs ON c.SVENDORID = vs.SVENDORID
                WHERE NVL(C.CACTIVE,'Y') = 'Y' AND t.SHEADREGISTERNO LIKE '%" + prefix + "%' )";
            DataTable dt = CommonFunction.Get_Data(sql, query);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    empResult.Add(dt.Rows[i]["SHEADREGISTERNO"].ToString());
                }
            }
        }
        catch (Exception ex)
        {

        }
        //using (SqlConnection con = new SqlConnection(@"Data Source=SARSHA\SqlServer2k8;Integrated Security=true;Initial Catalog=Test"))
        //{
        //    using (SqlCommand cmd = new SqlCommand())
        //    {
        //        cmd.CommandText = "select Top 10 EmployeeName from Employee where EmployeeName LIKE ''+@SearchEmpName+'%'";
        //        cmd.Connection = con;
        //        con.Open();
        //        cmd.Parameters.AddWithValue("@SearchEmpName", empName);
        //        SqlDataReader dr = cmd.ExecuteReader();
        //        while (dr.Read())
        //        {
        //            empResult.Add(dr["EmployeeName"].ToString());
        //        }
        //        con.Close();
        //        return empResult;
        //    }
        //}
        return empResult;
    }

    protected void gvw_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView ProductGrid = (GridView)sender;

            // Creating a Row
            GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell HeaderCell = new TableCell();
            HeaderCell.Text = "ที่";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);


            HeaderCell = new TableCell();
            HeaderCell.Text = "ชื่อผู้ประกอบการ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "สัญญา";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "วันที่ CHECK";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "ทะเบียนรถ";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.ColumnSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "ผู้ทำการบันทึก";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.VerticalAlign = VerticalAlign.Middle;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "";
            HeaderCell.HorizontalAlign = HorizontalAlign.Center;
            HeaderCell.RowSpan = 2;
            HeaderCell.CssClass = "HeaderStyle";
            HeaderCell.BackColor = System.Drawing.ColorTranslator.FromHtml("#c6defb");
            HeaderRow.Cells.Add(HeaderCell);

            ProductGrid.Controls[0].Controls.AddAt(0, HeaderRow);
        }
    }

    protected void gvw_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;
            e.Row.Cells[6].Visible = false;
            e.Row.Cells[7].Visible = false;
        }
    }

    protected void gvw_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        BindData();
        gvw.PageIndex = e.NewPageIndex;
        gvw.DataBind();
    }
    protected void gvw_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        DataTable dt = (DataTable)Session["mySearch"];
        if (e.CommandName == "select")
        {
            if (dt.Rows[rowIndex]["SCONTRACTID"].ToString() != "")
            {
                contractID = Convert.ToInt32(dt.Rows[rowIndex]["SCONTRACTID"]);
                truckID = dt.Rows[rowIndex]["STRUCKID"].ToString();
                string txtHeadNumber = dt.Rows[rowIndex]["SHEADREGISTERNO"].ToString();
                string txtTrailerNumber = dt.Rows[rowIndex]["STRAILERREGISTERNO"].ToString();
                string SURPRISECHECKID = dt.Rows[rowIndex]["NSURPRISECHECKID"].ToString();
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(contractID + "&" + truckID.ToString() + "&" + txtHeadNumber + "&" + txtTrailerNumber + "&" + dt.Rows[rowIndex]["DSURPRISECHECK"] + "&" + SURPRISECHECKID + "&" + dt.Rows[rowIndex]["SCONTRACTNO"] + "&" + dt.Rows[rowIndex]["SVENDORNAME"] + "&" + dt.Rows[rowIndex]["SVENDORID"]);

                strChkTruck = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

                Response.Redirect("SurpriseCheckAddEdit.aspx?str=" + strChkTruck + "&modehis=1");
            }
        }
    }
    protected void btnClearY_Click(object sender, EventArgs e)
    {
        try
        {
            TotalCar.Text = string.Empty;
            ddlVendor.ClearSelection();
            ddlVendor_SelectedIndexChanged(null, null);
            ddlContract.ClearSelection();
            AppointDate.Text = string.Empty;
            AppointLocate.Text = string.Empty;
            ddlUserCheck.ClearSelection();
            ddlTeamCheck.ClearSelection();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnSaveY_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            DataSet ds = new DataSet("DS");
            dtTeamCheckData.TableName = "DT";
            ds.Tables.Add(dtTeamCheckData.Copy());
            DataTable dtData = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSaveBLL(ID, TotalCar.Text, ddlVendor.SelectedValue, ddlContract.SelectedValue, AppointDate.Text, AppointLocate.Text, ddlUserCheck.SelectedValue, ddlTeamCheck.SelectedValue, Session["UserID"].ToString(), "0", string.Empty, ds);
            if (dtData.Rows.Count > 0)
                this.SendEmail(128, dtData.Rows[0]["doc_id"].ToString());
            alertSuccess("บันทึกสำเร็จ", "surprisechk_show.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtcontract = ContractBLL.Instance.ConTractSelectByVendor(ddlVendor.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlContract, dtcontract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseCheckYear.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvSurpriseCheckYear.DataKeys[e.RowIndex]["ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            string data = dgvSurpriseCheckYear.DataKeys[e.RowIndex]["IS_ACTIVE"].ToString();
            if (data == "1")
                MsgAlert.OpenForm("surpriseChk_ShowData.aspx?DocID=" + encryptedValue, Page);
            else if (data == "0")
                this.LoadDataEdit(dgvSurpriseCheckYear.DataKeys[e.RowIndex]["ID"].ToString());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void LoadDataEdit(string DATA_ID)
    {
        try
        {
            DataTable dtSurpriseCheckYear = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND ID = " + DATA_ID);
            if (dtSurpriseCheckYear.Rows.Count > 0)
            {
                TotalCar.Text = dtSurpriseCheckYear.Rows[0]["TOTALCAR"].ToString();
                ddlVendor.SelectedValue = dtSurpriseCheckYear.Rows[0]["SVENDORID"].ToString();
                ddlVendor_SelectedIndexChanged(null, null);
                ddlContract.SelectedValue = dtSurpriseCheckYear.Rows[0]["SCONTRACTID"].ToString();
                AppointDate.Text = DateTime.Parse(dtSurpriseCheckYear.Rows[0]["APPOINTDATE"].ToString()).ToString("dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
                AppointLocate.Text = dtSurpriseCheckYear.Rows[0]["APPOINTLOCATE"].ToString();
                ddlUserCheck.SelectedValue = dtSurpriseCheckYear.Rows[0]["USERCHECK"].ToString();
                //ddlTeamCheck.SelectedValue = dtSurpriseCheckYear.Rows[0]["TEAMCHECK"].ToString();
                this.ID = dtSurpriseCheckYear.Rows[0]["ID"].ToString();

                dtTeamCheckData = SurpriseCheckYearBLL.Instance.SurpriseCheckTeamSelect(" AND DOC_ID = " + DATA_ID);
                GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region Truck
    #region btnTruckSearch_Click
    protected void btnTruckSearch_Click(object sender, EventArgs e)
    {
        DataTable dt = AccidentBLL.Instance.TruckSelect(txtTruckSearch.Text.Trim());
        gvTruckSearch.DataSource = dt;
        gvTruckSearch.DataBind();
        updTruckSearch.Update();
    }
    #endregion

    #region btnTruck_Click
    protected void btnTruck_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowTruckSearch", "$('#ShowTruckSearch').modal();", true);
        updTruckSearch.Update();
    }
    #endregion

    #region gvTruckSearch_PageIndexChanging
    protected void gvTruckSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTruckSearch.PageIndex = e.NewPageIndex;
        btnTruckSearch_Click(null, null);
    }
    #endregion

    #region gvTruckSearch_RowUpdating
    protected void gvTruckSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        this.hidSTRUCKID.Value = gvTruckSearch.DataKeys[e.RowIndex]["STRUCKID"] + string.Empty;
        this.hidSCONTRACTID.Value = gvTruckSearch.DataKeys[e.RowIndex]["SCONTRACTID"] + string.Empty;
        this.hidSVENDORID.Value = gvTruckSearch.DataKeys[e.RowIndex]["SVENDORID"] + string.Empty;
        txtTrailerNumber.Text = gvTruckSearch.DataKeys[e.RowIndex]["STRAILERREGISTERNO"] + string.Empty;
        txtContactNo.Text = gvTruckSearch.DataKeys[e.RowIndex]["SCONTRACTNO"] + string.Empty;
        txtVendorName.Text = gvTruckSearch.DataKeys[e.RowIndex]["SABBREVIATION"] + string.Empty;
        txtTruck.Text = gvTruckSearch.DataKeys[e.RowIndex]["SHEADREGISTERNO"] + string.Empty;

        btnConfirm.Enabled = true;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowTruckSearch').modal('hide');</script>", false);
    }
    #endregion

    #region btnTruckClear_Click
    protected void btnTruckClear_Click(object sender, EventArgs e)
    {
        this.hidSTRUCKID.Value = string.Empty;
        this.hidSCONTRACTID.Value = string.Empty;
        this.hidSVENDORID.Value = string.Empty;
        txtTruck.Text = string.Empty;
        txtTrailerNumber.Text = string.Empty;
        txtContactNo.Text = string.Empty;
        txtVendorName.Text = string.Empty;


    }
    #endregion
    #endregion


    private void ValidateSave()
    {
        try
        {
            if (string.Equals(TotalCar.Text, string.Empty))
                throw new Exception("กรุณากรอก จำนวนรถที่นัดหมาย");
            if (ddlVendor.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ชื่อผู้ประกอบการ");
            if (ddlContract.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก วันที่นัดหมายตรวจประจำปี");
            if (string.Equals(AppointDate.Text, string.Empty))
                throw new Exception("กรุณากรอก วันที่นัดหมายตรวจประจำปี");
            if (string.Equals(AppointLocate.Text, string.Empty))
                throw new Exception("กรุณากรอก สถานที่ตรวจนัดหมายประจำปี");
            if (ddlUserCheck.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ผู้ทำการตรวจ");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnAddTeam_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlTeamCheck.SelectedIndex > 0)
            {
                dtTeamCheckData.Rows.Add(ddlTeamCheck.SelectedValue, ddlTeamCheck.SelectedItem);
                GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
            }
            else
                throw new Exception("กรุณเลือกทีมผู้ทำการตรวจก่อน");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTeamCheck_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTeamCheck.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTeamCheck_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void dgvTeamCheck_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtTeamCheckData.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvTeamCheck, dtTeamCheckData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void SendEmail(int TemplateID, string Doc_id)
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                DataTable dt = VendorBLL.Instance.VendorSelectBLL(" AND SVENDORID = '" + ddlVendor.SelectedValue + "'");
                if (TemplateID == 128)
                {
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem.ToString());
                    Body = Body.Replace("{TOTALCAR}", TotalCar.Text.Trim());
                    Body = Body.Replace("{APPOINTDATE}", AppointDate.Text.Trim());
                    Body = Body.Replace("{APPOINTLOCATE}", AppointLocate.Text.Trim());
                    Body = Body.Replace("{USERCHECK}", ddlUserCheck.SelectedItem.ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/surpriseChk_Vendor.aspx?str=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Doc_id), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dt.Rows[0]["EMAIL"].ToString(), false, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dt.Rows[0]["EMAIL"].ToString(), false, true), Subject, Body, "", "SurpriseCheck3", ColumnEmailName);
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }
}