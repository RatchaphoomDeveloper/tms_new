﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHtmlEditor;


public partial class FAQINFORMATION : System.Web.UI.Page
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/FAQTEMP/{0}/{2}/{1}/";
    const int MaxLength = 4000;
    string CustomErrorText = string.Format("Custom validation fails because the HTML content&prime;s length exceeds {0} characters.", MaxLength);
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
   

    protected void Page_Load(object sender, EventArgs e)
    {
        //xcpn.Callback += new DevExpress.Web.ASPxClasses.CallbackEventHandlerBase(xcpn_Callback);
        //xcpn.Load += new EventHandler(xcpn_Load);

        string html = HtmlEditorUtils.GetASPxHtmlEditorHtml(Page, "faqinformation.aspx");

            txtAnswer.Validation += new EventHandler<HtmlEditorValidationEventArgs>(ASPxHtmlEditor1_Validation);

        if (!IsPostBack)
        {
            Cache.Remove(sdsSelectdata.CacheKeyDependency);
            Cache[sdsSelectdata.CacheKeyDependency] = new object();
            sdsSelectdata.Select(new System.Web.UI.DataSourceSelectArguments());
            sdsSelectdata.DataBind();
            cboSelect.DataBind();

            
            txtAnswer.ClientSideEvents.Validation = "ValidationHandler";
            lblContentLength.Text = txtAnswer.Html.Length.ToString();
            //txtAnswer.Html = html;
            //txtAnswer.Html = html.Length.ToString();
        }
        //else
        //{
        //    if (chbValidateOnServer.Checked)
        //        txtAnswer.Validation += new EventHandler<HtmlEditorValidationEventArgs>(ASPxHtmlEditor1_Validation);
        //    else
        //        txtAnswer.Validation -= new EventHandler<HtmlEditorValidationEventArgs>(ASPxHtmlEditor1_Validation);
        //    txtAnswer.ClientSideEvents.Validation = chbValidateOnClient.Checked ? "ValidationHandler" : "";
        //    txtAnswer.SettingsValidation.RequiredField.IsRequired = chbIsRequired.Checked;
        //    lblContentLength.Text = txtAnswer.Html.Length.ToString();
        //}

        

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] param = e.Parameter.Split(';');


        switch (param[0])
        {
            case "Save":

                if (CommonFunction.Count_Value(sql, "Select FAQ_QUEST from TFAQ Where FAQ_QUEST = '" + txtQuest.Text.Trim() + "'") > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มหัวข้อได้เนื่องจากมีในฐานข้อมูลแล้ว!');");
                    return;
                }



                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();


                    string strsql = @"INSERT INTO TFAQ(FAQ_ID, FAQ_QUEST, FAQ_ANSWER, DATE_CREATED ,USER_CREATED
                                       , FLAG_ACTIVE, FILE_ORIGINALNAME, FILE_SYSTEMNAME ,FLAG_HITRATE) 
                                       VALUES (FC_GENID_TFAQ(), :FAQ_QUEST, :FAQ_ANSWER , SYSDATE ,:USER_CREATED
                                       , :FLAG_ACTIVE, :FILE_ORIGINALNAME, :FILE_SYSTEMNAME, :FLAG_HITRATE)";
                    using (OracleCommand com1 = new OracleCommand(strsql, con))
                    {
                        com1.Parameters.Clear();
                        com1.Parameters.Add(":FAQ_QUEST", OracleType.VarChar).Value = txtQuest.Text;
                        com1.Parameters.Add(":FAQ_ANSWER", OracleType.VarChar).Value = (txtAnswer.Html != "") ? txtAnswer.Html : "-";
                        com1.Parameters.Add(":USER_CREATED", OracleType.VarChar).Value = Session["UserID"] + "";
                        //com1.Parameters.Add(":USER_UPDATED", OracleType.VarChar).Value = ;
                        com1.Parameters.Add(":FLAG_ACTIVE", OracleType.Char).Value = "1";
                        com1.Parameters.Add(":FILE_ORIGINALNAME", OracleType.VarChar).Value = txtFileName.Text;
                        com1.Parameters.Add(":FILE_SYSTEMNAME", OracleType.VarChar).Value = txtFilePath.Text;
                        com1.Parameters.Add(":FLAG_HITRATE", OracleType.Number).Value = "1";
                        com1.ExecuteNonQuery();

                    }
                    ClearControl();
                    break;
                }


            case "Update":

                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();


                    string strsql = @"UPDATE TFAQ SET FAQ_QUEST =:FAQ_QUEST, FAQ_ANSWER = :FAQ_ANSWER, DATE_UPDATED=SYSDATE,
                                                      USER_UPDATED=:USER_UPDATED, FLAG_ACTIVE=:FLAG_ACTIVE, FILE_ORIGINALNAME=:FILE_ORIGINALNAME ,
                                                      FILE_SYSTEMNAME=:FILE_SYSTEMNAME,FLAG_HITRATE =:FLAG_HITRATE WHERE FAQ_ID = '" + CommonFunction.ReplaceInjection(cboSelect.Value + "") + "' ";



                    using (OracleCommand com1 = new OracleCommand(strsql, con))
                    {
                        com1.Parameters.Clear();
                        com1.Parameters.Add(":FAQ_QUEST", OracleType.VarChar).Value = txtQuest.Text;
                        com1.Parameters.Add(":FAQ_ANSWER", OracleType.VarChar).Value = txtAnswer.Html;
                        com1.Parameters.Add(":USER_UPDATED", OracleType.VarChar).Value = Session["UserID"] + "";
                        //com1.Parameters.Add(":USER_UPDATED", OracleType.VarChar).Value = ;
                        com1.Parameters.Add(":FLAG_ACTIVE", OracleType.Char).Value = "1";
                        com1.Parameters.Add(":FILE_ORIGINALNAME", OracleType.VarChar).Value = txtFileName.Text;
                        com1.Parameters.Add(":FILE_SYSTEMNAME", OracleType.VarChar).Value = txtFilePath.Text;
                        com1.Parameters.Add(":FLAG_HITRATE", OracleType.Number).Value = "1";
                        com1.ExecuteNonQuery();

                    }

                    ClearControl();
                }


                cboSelect_ItemsRequestedByFilterConditionSQL(cboSelect, new ListEditItemsRequestedByFilterConditionEventArgs(0, 9, ""));

                break;

            case "deleteFile":

                string FilePath = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                }

                string cNo = param[2];
                if (cNo == "1")
                {
                    txtFileName.Text = "";
                    txtFilePath.Text = "";
                }


                VisibleControlUpload();
                break;
            case "binddata":
                BindData();
                break;

            //case "Detail":
            //int VisibleIndex = int.TryParse(param[1] + "", out defInt) ? int.Parse(param[1] + "") : 0;
            //dynamic data = txtQuest.GetClientSideEventHandler("FAQ_ID");

            ////Session["FID"] = data;

            //xcpn.JSProperties["cpRedirectTo"] = "faqinformation.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data));
            //break;
        }

    }


    protected void upload_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "FAQ" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void VisibleControlUpload()
    {
        bool visible = string.IsNullOrEmpty(txtFilePath.Text);
        upload.ClientVisible = visible;
        txtFileName.ClientVisible = !visible;
        btnView.ClientEnabled = !visible;
        btnDelFile.ClientEnabled = !visible;

        if (!(string.IsNullOrEmpty(txtFilePath.Text)))
        {
            chkUpload1.Value = "1";
        }
        else
        {
            chkUpload1.Value = "";
        }


    }

    protected void cboSelect_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsSelectdata.SelectCommand = @"SELECT FAQ_ID , FAQ_QUEST FROM (SELECT ROW_NUMBER()OVER(ORDER BY FAQ_ID) AS RN , FAQ_ID , FAQ_QUEST
          FROM TFAQ WHERE FAQ_QUEST LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsSelectdata.SelectParameters.Clear();
        sdsSelectdata.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsSelectdata.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsSelectdata.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsSelectdata;
        comboBox.DataBind();

    }

    protected void cboSelect_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    void BindData()
    {
        string sqlstr1 = @"SELECT FAQ_ID, FAQ_QUEST, FAQ_ANSWER, FILE_ORIGINALNAME ,FILE_SYSTEMNAME  FROM TFAQ WHERE FAQ_ID = '" + CommonFunction.ReplaceInjection(cboSelect.Value + "") + "'";
        DataTable dt1 = CommonFunction.Get_Data(sql, sqlstr1);

        if (dt1.Rows.Count > 0)
        {
            txtQuest.Text = dt1.Rows[0]["FAQ_QUEST"] + "";
            txtAnswer.Html = dt1.Rows[0]["FAQ_ANSWER"] + "";
            txtFileName.Text = dt1.Rows[0]["FILE_ORIGINALNAME"] + "";
            txtFilePath.Text = dt1.Rows[0]["FILE_SYSTEMNAME"] + "";
            VisibleControlUpload();
        }

    }

    void ClearControl()
    {
        cboSelect.Value = "";
        txtQuest.Text = "";
        txtAnswer.Html = "";

    }


    protected void ASPxHtmlEditor1_Validation(object sender, HtmlEditorValidationEventArgs e)
    {
        if (e.Html.Length > MaxLength)
        {
            e.IsValid = false;
            e.ErrorText = CustomErrorText;
        }
    }

    //public static class HtmlEditorUtils
    //{

    //    const string htmlLocation = "~/App_Data/Html";
    //    static readonly ICollection<string> darkThemeNames = new string[] { "BlackGlass", "PlasticBlue", "RedWine" };

    //    public static void SetASPxHtmlEditorHtml(Page page, ASPxHtmlEditor htmlEditor, string htmlFileName)
    //    {
    //        string resolvedPath = page.MapPath(Path.Combine(htmlLocation, htmlFileName));
    //        htmlEditor.Html = File.ReadAllText(resolvedPath);
    //    }
    //    public static string GetASPxHtmlEditorHtml(Page page, string htmlFileName)
    //    {
    //        string resolvedPath = page.MapPath(Path.Combine(htmlLocation, htmlFileName));
    //        return File.ReadAllText(resolvedPath);
    //    }

    //    public static bool IsDarkTheme(string themeName)
    //    {
    //        return darkThemeNames.Contains(themeName);
    //    }

    //}

}

