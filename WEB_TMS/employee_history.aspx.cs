﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using TMS_BLL.Master;
using System.Web.Security;
using System.Text;

public partial class employee_history : PageBase
{
    string SEMPLOYEEID = string.Empty;
    string PERS_CODE = string.Empty;
    DataTable dt = new DataTable();
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            string str = Request.QueryString["str"];
            //string strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                SEMPLOYEEID = Encoding.UTF8.GetString(decryptedBytes);
                //SEMPLOYEEID = strQuery[0];
                setdata();
                Listgrid();
            }
            
        }
        
    }

    #endregion
    
    #region DrowDownList
    private void SetDrowDownList()
    {
        ddlPosition.DataTextField = "PERSON_TYPE_DESC";
        ddlPosition.DataValueField = "PERSON_TYPE";
        ddlPosition.DataSource = VendorBLL.Instance.PositionSelect();
        ddlPosition.DataBind();
        ddlPosition.Items.Insert(0, new ListItem("-เลือกตำแหน่ง-", null));
        ddlPosition.SelectedIndex = 0;

        ddlTitle.DataTextField = "TITLE_NAME";
        ddlTitle.DataValueField = "TITLE_ID";
        ddlTitle.DataSource = VendorBLL.Instance.TitleSelect();
        ddlTitle.DataBind();
        ddlTitle.Items.Insert(0, new ListItem("-เลือกคำนำหน้าชื่อ-", null));
        ddlTitle.SelectedIndex = 0;
    }
    #endregion

    #region setdata
    void setdata()
    {        
        dt = VendorBLL.Instance.VendorSelect(SEMPLOYEEID);
        if (dt.Rows.Count > 0)
        {
            PERS_CODE = dt.Rows[0]["PERS_CODE"] + "";
            txtEmpID.Text = PERS_CODE;
            if (!string.IsNullOrEmpty(PERS_CODE) && PERS_CODE.Length == 13)
            {
                
                txtEmpID.Text = txtEmpID.Text.Insert(12, "-").Insert(10, "-").Insert(5, "-").Insert(1, "-");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["SEMPTPYE"] + ""))
            {
                // cboPosition.SelectedItem.Value = dt.Rows[0]["SEMPTPYE"] + "";
                ddlPosition.SelectedValue = dt.Rows[0]["SEMPTPYE"] + "";

            }
            else
            {
                ddlPosition.SelectedIndex = 0;
            }
            ddlTitle.SelectedValue = dt.Rows[0]["TNAME"] + "";
            txtName.Text = dt.Rows[0]["FNAME"] + "";
            txtSurName.Text = dt.Rows[0]["LNAME"] + "";

            if (!string.IsNullOrEmpty(dt.Rows[0]["SFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SSYSFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SPATH"] + ""))
            {
                imgEmp.ImageUrl = dt.Rows[0]["SPATH"] + "" + dt.Rows[0]["SSYSFILENAME"] + "";
            }
            else
            {
                imgEmp.ImageUrl = "images/Avatar.png";
            }

        }
    }
    #endregion

    #region Listgrid
    void Listgrid()
    {

        List<string[]> sds = new List<string[]>();

        DataTable dtEmployeeHistory = new DataTable();
        dtEmployeeHistory = VendorBLL.Instance.EmployeeHistorySelect(PERS_CODE);

        //string DateUpdate = "";
        //string SUPDATE = "";
        //string datachange = "";
        //string dataold = "";
        //string datanew = "";
        
        //if (dtEmployeeHistory.Rows.Count > 0)
        //{
        //    DataRow dr,drNew;

        //    for (int i = 0; i <= dtEmployeeHistory.Rows.Count - 1; i++)
        //    {
        //        dr = dtEmployeeHistory.Rows[i];
                
        //        if (i + 1 != dtEmployeeHistory.Rows.Count)
        //        {
        //            drNew = dtEmployeeHistory.Rows[i + 1];
        //            DateUpdate = dr["DUPDATE"] + "";
        //            SUPDATE = dr["SUPDATE"] + "";
        //            SetDataGrid(dr, drNew, ref datachange, ref dataold, ref datanew);
        //        }
        //        else
        //        {
        //            DateUpdate = dt.Rows[0]["DUPDATE"] + "";
        //            SUPDATE = dt.Rows[0]["SUPDATENAME"] + "";
        //            SetDataGrid(dr, dt.Rows[0], ref datachange, ref dataold, ref datanew);
        //        }

        //        if (string.IsNullOrEmpty(DateUpdate))
        //        {
        //            DateUpdate = DateTime.Now.ToString();
        //        }
        //        sds.Add(new string[] { DateUpdate, SUPDATE, datachange, dataold, datanew });

        //        DateUpdate = "";
        //        SUPDATE = "";
        //        datachange = "";
        //        dataold = "";
        //        datanew = "";

        //    }
        //}
        //if (sds.Any())
        //{
        //    gvw.DataSource = sds.Select(s => new { DateUpdate = s[0], SUPDATE = s[1], datachange = s[2], dataold = s[3], datanew = s[4] }).OrderByDescending(o => Convert.ToDateTime(o.DateUpdate));
        //    gvw.DataBind();
        //}
        gvw.DataSource = dtEmployeeHistory;
        gvw.DataBind();
        gvw.Visible = true;
    }
    #endregion

    #region SetDataGrid
    private void SetDataGrid(DataRow dr, DataRow drNew, ref string datachange, ref string dataold, ref string datanew)
    {
        if (dr["SEMPTPYE"] + "" != drNew["SEMPTPYE"] + "")
        {
            datachange += "ตำแหน่ง" + "<br>";

            datanew += drNew["PERSON_TYPE_DESC"] + "<br>";

            dataold += dr["PERSON_TYPE_DESC"] + "<br>";
        }

        if (dr["FNAME"] + "" != drNew["FNAME"] + "")
        {
            datachange += "ชื่อ" + "<br>";
            dataold += dr["FNAME"] + "" + "<br>";
            datanew += drNew["FNAME"] + "" + "<br>";
        }

        if (dr["LNAME"] + "" != drNew["LNAME"] + "")
        {
            datachange += "นามสกุล" + "<br>";
            dataold += dr["LNAME"] + "" + "<br>";
            datanew += drNew["LNAME"] + "" + "<br>";
        }

        if (dr["DBIRTHDATE"] + "" != drNew["DBIRTHDATE"] + "")
        {
            datachange += "วันเกิด" + "<br>";
            dataold += dr["DBIRTHDATE"] + "<br>";
            datanew += drNew["DBIRTHDATE"] + "<br>";
        }

        if (dr["STEL"] + "" != drNew["STEL"] + "")
        {
            datachange += "หมายเลขโทรศัพท์หลัก" + "<br>";
            dataold += dr["STEL"] + "" + "<br>";
            datanew += drNew["STEL"] + "" + "<br>";
        }

        if (dr["STEL2"] + "" != drNew["STEL2"] + "")
        {
            datachange += "หมายเลขโทรศัพท์สำรอง" + "<br>";
            dataold += dr["STEL2"] + "" + "<br>";
            datanew += drNew["STEL2"] + "" + "<br>";
        }

        if (dr["SMAIL"] + "" != drNew["SMAIL"] + "")
        {
            datachange += "อีเมล์หลัก" + "<br>";
            dataold += dr["SMAIL"] + "" + "<br>";
            datanew += drNew["SMAIL"] + "" + "<br>";
        }

        if (dr["SMAIL2"] + "" != drNew["SMAIL2"] + "")
        {
            datachange += "อีเมล์สำรอง" + "<br>";
            dataold += dr["SMAIL2"] + "" + "<br>";
            datanew += drNew["SMAIL2"] + "" + "<br>";
        }

        if (dr["SABBREVIATION"] + "" != drNew["SABBREVIATION"] + "")
        {
            datachange += "บริษัทผู้ขนส่ง" + "<br>";
            dataold += dr["SABBREVIATION"] + "" + "<br>";
            datanew += drNew["SABBREVIATION"] + "" + "<br>";
        }

        if (dr["PERS_CODE"] + "" != drNew["PERS_CODE"] + "")
        {
            datachange += "หมายเลขบัตรประชาชน" + "<br>";
            dataold += dr["PERS_CODE"] + "" + "<br>";
            datanew += drNew["PERS_CODE"] + "" + "<br>";
        }

        if (dr["PERSONEL_BEGIN"] + "" != drNew["PERSONEL_BEGIN"] + "")
        {
            datachange += "วันที่อนุญาตบัตรประชาชน" + "<br>";
            dataold += dr["PERSONEL_BEGIN"] + "<br>";
            datanew += drNew["PERSONEL_BEGIN"] + "<br>";
        }

        if (dr["PERSONEL_EXPIRE"] + "" != drNew["PERSONEL_EXPIRE"] + "")
        {
            datachange += "วันที่หมดอายุบัตรประชาชน" + "<br>";
            dataold += dr["PERSONEL_EXPIRE"] + "<br>";
            datanew += drNew["PERSONEL_EXPIRE"] + "<br>";
        }

        if (dr["SDRIVERNO"] + "" != drNew["SDRIVERNO"] + "")
        {
            datachange += "หมายเลขใบขับขี่ประเภท 4" + "<br>";
            dataold += dr["SDRIVERNO"] + "" + "<br>";
            datanew += drNew["SDRIVERNO"] + "" + "<br>";
        }

        if (dr["DDRIVEBEGIN"] + "" != drNew["DDRIVEBEGIN"] + "")
        {
            datachange += "วันที่อนุญาตใบขับขี่ประเภท 4" + "<br>";
            dataold += dr["DDRIVEBEGIN"] + "<br>";
            datanew += drNew["DDRIVEBEGIN"] + "<br>";
        }

        if (dr["DDRIVEEXPIRE"] + "" != drNew["DDRIVEEXPIRE"] + "")
        {
            datachange += "วันที่ยกเลิกใบขับขี่ประเภท 4" + "<br>";
            dataold += dr["DDRIVEEXPIRE"] + "<br>";
            datanew += drNew["DDRIVEEXPIRE"] + "<br>";
        }
        if (dr["CAUSESAPCOMMIT"] + "" != drNew["CAUSESAPCOMMIT"] + "")
        {
            datachange += "สาเหตุที่อนุญาติ" + "<br>";
            dataold += dr["CAUSESAPCOMMIT"] + "" + "<br>";
            datanew += drNew["CAUSESAPCOMMIT"] + "" + "<br>";
        }

        //if (dr["CAUSESAPCOMMIT"] + "" != drNew["CAUSESAPCOMMIT"] + "")
        //{
        //    datachange += "สาเหตุที่อนุญาติ" + "<br>";
        //    dataold += dr["CAUSESAPCOMMIT"] + "" + "<br>";
        //    datanew += PresentData.CAUSESAPCOMMIT + "" + "<br>";
        //}

        if (dr["BANSTATUS"] + "" != drNew["BANSTATUS"] + "")
        {
            datachange += "ประเภทการระงับ " + "<br>";
            switch (dr["BANSTATUS"] + "")
            {
                case "0": dataold += "ผู้ขนส่งแจ้งยกเลิกข้อมูล " + "<br>";
                    break;
                case "1": dataold += "ผู้ขนส่งแจ้งระงับการใช้งานชั่วคราว " + "<br>";
                    break;
                case "2": dataold += "ผู้ขนส่งดำเนินการแจ้งการสอบสวนความผิด " + "<br>";
                    break;
                case "3": dataold += "คลังระงับการใช้งานชั่วคราว " + "<br>";
                    break;
                default: dataold += " " + "<br>";
                    break;
            }

            switch (drNew["BANSTATUS"] + "")
            {
                case "0": datanew += "ผู้ขนส่งแจ้งยกเลิกข้อมูล " + "<br>";
                    break;
                case "1": datanew += "ผู้ขนส่งแจ้งระงับการใช้งานชั่วคราว " + "<br>";
                    break;
                case "2": datanew += "ผู้ขนส่งดำเนินการแจ้งการสอบสวนความผิด " + "<br>";
                    break;
                case "3": datanew += "คลังระงับการใช้งานชั่วคราว " + "<br>";
                    break;
                default: datanew += " " + "<br>";
                    break;
            }
        }

        if (dr["CAUSESAP"] + "" != drNew["CAUSESAP"] + "")
        {
            datachange += "สาเหตุที่ระงับจากระบบ SAP" + "<br>";
            dataold += dr["CAUSESAP"] + "" + "<br>";
            datanew += drNew["CAUSESAP"] + "" + "<br>";
        }

        if (dr["CANCELSTATUS"] + "" != drNew["CANCELSTATUS"] + "")
        {
            datachange += "ประเภทการยกเลิกใช้งาน" + "<br>";
            switch (dr["CANCELSTATUS"] + "")
            {
                case "0": dataold += "รข. ดำเนินการBlacklist" + "<br>";
                    break;
                case "1": dataold += "รข. แจ้งระงับชั่วคราว" + "<br>";
                    break;
                case "2": dataold += "รข. อยู่ระหว่างการตัดสินความผิด" + "<br>";
                    break;
                default: dataold += " " + "<br>";
                    break;
            }

            switch (drNew["CANCELSTATUS"] + "")
            {
                case "0": datanew += "รข. ดำเนินการBlacklist" + "<br>";
                    break;
                case "1": datanew += "รข. แจ้งระงับชั่วคราว" + "<br>";
                    break;
                case "2": datanew += "รข. อยู่ระหว่างการตัดสินความผิด" + "<br>";
                    break;
                default: datanew += " " + "<br>";
                    break;
            }
        }

        if (dr["CAUSESAPCANCEL"] + "" != drNew["CAUSESAPCANCEL"] + "")
        {
            datachange += "สาเหตุที่ยกเลิกใช้งานจากระบบ SAP" + "<br>";
            dataold += dr["CAUSESAPCANCEL"] + "" + "<br>";
            datanew += drNew["CAUSESAPCANCEL"] + "" + "<br>";
        }

        if (dr["NOTFILL"] + "" != drNew["NOTFILL"] + "")
        {
            datachange += "การเติมเต็มข้อมูล " + "<br>";
            switch (dr["CANCELSTATUS"] + "")
            {
                case "0": dataold += "ไม่เติมเต็มข้อมูล" + "<br>";
                    break;
                case "1": dataold += "เติมเต็มข้อมูล" + "<br>";
                    break;
                default: dataold += " " + "<br>";
                    break;

            }

            switch (drNew["NOTFILL"] + "")
            {
                case "0": datanew += "ไม่เติมเต็มข้อมูล" + "<br>";
                    break;
                case "1": datanew += "เติมเต็มข้อมูล" + "<br>";
                    break;
                default: datanew += " " + "<br>";
                    break;
            }
        }

        if (dr["SFILENAME"] + "" != drNew["SFILENAME"] + "")
        {
            datachange += "เปลี่ยนรูปประจำตัว" + "<br>";
            dataold += dr["SFILENAME"] + "" + "<br>";
            datanew += drNew["SFILENAME"] + "" + "<br>";
        }

        if (dr["TNAME"] + "" != drNew["TNAME"] + "")
        {
            datachange += "คำนำหน้าชื่อ" + "<br>";
            dataold += dr["TITLE_NAME"] + "" + "<br>";
            datanew += drNew["TITLE_NAME"] + "" + "<br>";
        }
    }
    #endregion
    
}