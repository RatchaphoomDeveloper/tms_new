﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.XtraPrinting;
using System.Drawing.Printing;
using System.Globalization;

public partial class ReportTruckWaterMPY : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Events
        gvwVehHis.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwVehHis_CustomColumnDisplayText);
        gvwVehHis.DataBinding += new EventHandler(gvwVehHis_DataBinding);
        #endregion
        if (!IsPostBack)
        {
            lblReport.Text = GetMessage();
            SetControl();
            BindGrid();
        }
    }
    protected void xcpn_Callback(object sender, CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch ("" + paras[0])
        {
            case "SEARCH":
                lblReport.Text = GetMessage();
                BindGrid();
                break;
        }
    }
    protected void gvwVehHis_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void gvwVehHis_DataBinding(object sender, EventArgs e)
    {
        if (ViewState["vsVehicle"] != null)
            gvwVehHis.DataSource = (DataTable)ViewState["vsVehicle"];
    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v 
WHERE  V.SVENDORNAME  like :fillter 
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WritePdfToResponse();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WriteXlsToResponse();
    }
    #region Other Function
    void SetControl()
    {
        cmbsYear.DataSource = CommonFunction.Get_Data(conn, "SELECT CASE WHEN cYear IS NULL THEN 0 ELSE ROWNUM END As cOrder,T.* FROM (SELECT NULL As cYear,'- ปี -' As sYear FROM DUAL UNION SELECT TO_CHAR(SERVICE_DATE,'YYYY') As cYear,TO_CHAR(TO_CHAR(SERVICE_DATE,'YYYY')+543) As sYear FROM TBL_REQUEST WHERE SERVICE_DATE IS NOT NULL GROUP BY TO_CHAR(SERVICE_DATE,'YYYY') ) T ORDER BY cOrder");
        cmbsYear.DataBind();
    }
    private void BindGrid()
    {
        string _sql = "", _condi = "";

        if (cboVendor.Text.Trim() != "")
            _condi += " AND VEN.SABBREVIATION LIKE '" + cboVendor.Text + "'";
        if ("" + cmbsYear.Value != "")
            _condi += " AND TO_CHAR(REQ.SERVICE_DATE ,'YYYY')='" + cmbsYear.Value + "'";

        _sql = @"SELECT RI.REQUEST_ID,REQ.VEH_NO,REQ.TU_NO,VEN.SVENDORID,VEN.SABBREVIATION,T.SCAR_NUM,RI.NITEM,TYP.REQTYPE_NAME,CAU.CAUSE_NAME,REQ.SERVICE_DATE  
FROM TBL_REQUEST_ITEM RI
INNER JOIN TBL_REQUEST REQ ON RI.REQUEST_ID=REQ.REQUEST_ID
LEFT JOIN TTRUCK T ON NVL(REQ.TU_ID,REQ.VEH_ID)=T.STRUCKID
LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID 
LEFT JOIN TBL_REQTYPE TYP ON REQ.REQTYPE_ID=TYP.REQTYPE_ID 
LEFT JOIN TBL_CAUSE CAU ON REQ.CAUSE_ID=CAU.CAUSE_ID
WHERE RI.SERVICE_ID='00001' AND REQ.REQTYPE_ID='01'  " + _condi;

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        ViewState["vsVehicle"] = dt;
        gvwVehHis.DataBind();
    }
    private void PreparePrinting()
    {
        PrivateFontCollection fontColl = new PrivateFontCollection();
        fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

        BindGrid();
        gridExport.GridView.Columns[7].Visible = false;
        gridExport.Landscape = true;
        gridExport.PaperKind = PaperKind.A4;
        gridExport.PageHeader.Center = GetMessage();
        gridExport.PageHeader.Font.Name = fontColl.Families[0].Name;
        gridExport.PageHeader.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.PageHeader.Font.Bold = true;
        gridExport.TopMargin = 1;
        gridExport.RightMargin = 1;
        gridExport.BottomMargin = 1;
        gridExport.LeftMargin = 1;
        gridExport.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.True;
        gridExport.Styles.Default.Font.Name = fontColl.Families[0].Name;
        gridExport.Styles.Default.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.Styles.Header.Font.Bold = true;

        gridExport.FileName = "รายงานผลตรวจสอบรายคันประวัติเก่า_" + DateTime.Now.ToString("MMddyyyyHHmmss");
    }
    private string GetMessage()
    {
        string sMessage = "", sCondi = "";

        if (cboVendor.Text.Trim() != "" && "" + cmbsYear.Value != "")
            sCondi = string.Format(@"ผู้ขนส่ง :{0} ,ปี : {1}", cboVendor.Text, cmbsYear.Text);
        else if (cboVendor.Text.Trim() != "")
            sCondi = string.Format(@"ผู้ขนส่ง :{0} ", cboVendor.Text);
        else if ("" + cmbsYear.Value != "")
            sCondi = string.Format(@"ปี : {0}", cmbsYear.Text);

        sMessage = string.Format(@"รายงานรถวัดน้ำเกิน 1 ครั้ง/ปี {1} (จัดทำรายงานวันที่ {0:d/M/yyyy})", DateTime.Now.Date, (sCondi == "" ? @"""ทั้งหมด""" : sCondi));

        return sMessage;
    }
    #endregion
}