﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;

public partial class AccidentTab3_Vendor : PageBase
{
    DataTable dt;
    DataSet ds;
    DataRow dr;
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["CGROUP"] + string.Empty != "0")
            {
                Response.Redirect("AccidentTab3.aspx?str=" + Request.QueryString["str"]);
                return;
            }
            InitialUpload();
            InitialRequireFile();
            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            //SetDrowDownList();

            if (Request.QueryString["str"] != null && !string.IsNullOrEmpty(Request.QueryString["str"]))
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);
                liTab1.Visible = true;
                GeneralTab1.HRef = "AccidentTab1.aspx?str=" + str;
                liTab2.Visible = true;
                GeneralTab2.HRef = "AccidentTab2.aspx?str=" + str;
                if (int.Parse(hidCactive.Value) >= 8)
                {
                    liTab4.Visible = true;
                    GeneralTab4.HRef = "AccidentTab4.aspx?str=" + str;
                }
            }
        }
        SetEnabledControlByID(hidCactive.Value);
    }
    #endregion

    #region SetData
    private void SetData(string ACCIDENT_ID)
    {
        ds = AccidentBLL.Instance.AccidentTab3VendorSelect(ACCIDENT_ID);
        if (ds.Tables.Count > 0)
        {
            #region จาก Tab1
            dt = ds.Tables["ACCIDENT"];
            if (dt != null && dt.Rows.Count > 0)
            {

                DataRow dr = dt.Rows[0];
                txtAccidentID.Text = dr["ACCIDENT_ID"] + string.Empty;
                if (!dr.IsNull("ACCIDENT_DATE"))
                {
                    txtAccidentDate.Text = DateTime.Parse(dr["ACCIDENT_DATE"] + string.Empty).ToString(DateTimeFormat);
                }
                if (!dr.IsNull("SYS_TIME"))
                    txtAccidentDateSystem.Text = DateTime.Parse(dr["SYS_TIME"] + string.Empty).ToString(DateTimeFormat);
                rblREPORTER.SelectedValue = dr["REPORTER"] + string.Empty;
                if (!dr.IsNull("REPORT_PTT"))
                    txtAccidentDatePtt.Text = DateTime.Parse(dr["REPORT_PTT"] + string.Empty).ToString(DateTimeFormat);
                txtAccidentName.Text = dr["INFORMER_NAME"] + string.Empty;
                if (!dr.IsNull("TIMECREATE"))
                    txtDatePrimary.Text = DateTime.Parse(dr["TIMECREATE"] + string.Empty).ToString(DateTimeFormat);
                if (!dr.IsNull("APPROVE_DATE"))
                    txtApproveDate.Text = DateTime.Parse(dr["APPROVE_DATE"] + string.Empty).ToString(DateTimeFormat);
                hidID.Value = txtAccidentID.Text;
                hidCactive.Value = dr["CACTIVE"] + string.Empty;
                if (dr["SERIOUS"] + string.Empty == "0")
                {
                    lblType.Text = "เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Red;
                }
                else if (dr["SERIOUS"] + string.Empty == "1")
                {
                    lblType.Text = "ไม่เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด";
                    lblType.ForeColor = Color.Green;
                }
            }
            #endregion

            #region DAMAGE
            dt = ds.Tables["DAMAGE"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];
                txtDAMAGE.Text = dr["DAMAGE"] + string.Empty;
                if (!dr.IsNull("CREATE_DATE"))
                    txtDateAnalysis.Text = DateTime.Parse(dr["CREATE_DATE"] + string.Empty).ToString(DateTimeFormat);
            }
            #endregion

            #region IMAGE
            dt = ds.Tables["IMAGE"];
            if (dt != null && dt.Rows.Count > 0)
            {
                dtUpload = dt;
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            #endregion

        }

    }
    #endregion

    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, "", DBNull.Value, "");
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
        }
        catch (Exception ex)
        {
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "ACCIDENTTAB3VENDOR" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                DataRowView dv = (DataRowView)e.Row.DataItem;
                if (!dv.Row.IsNull("STATUS"))
                {
                    imgDelete.Visible = false;
                    if (dv["STATUS"] + string.Empty == "1")
                    {
                        e.Row.ForeColor = Color.Red;
                        foreach (TableCell item in e.Row.Cells)
                        {
                            item.ForeColor = Color.Red;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("ACCIDENTTAB3VENDOR");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
        dtUpload.Columns.Add("STATUSNAME");
        dtUpload.Columns.Add("STATUS");
        dtUpload.Columns.Add("REMARK");

        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
    }

    private void InitialRequireFile()
    {
        dtRequestFile = AccidentBLL.Instance.AccidentRequestFile("ACCIDENTTAB3VENDOR");
        GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);
    }

    #endregion

    #region btn
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string accID = SaveData(4);
            if (!string.IsNullOrEmpty(accID))
            {
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess("บันทึกสำเร็จ", "AccidentTab3_Vendor.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region mpSaveSend_ClickOK
    protected void mpSaveSend_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = ValidateSaveUpLoadFile();
            if (!string.IsNullOrEmpty(mess))
            {
                alertFail(RemoveSpecialCharacters(mess));
            }
            else
            {
                string accID = SaveData(6);
                mess += "บันทึกสำเร็จและส่งข้อมูลสำเร็จ";
                if (!string.IsNullOrEmpty(accID))
                {
                    if (SendEmail(ConfigValue.EmailAccident4, accID))
                    {
                        mess += "<br/>ส่ง E-mail สำเร็จ";
                    }
                    else
                    {
                        mess += "<br/><span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                    }
                    //send email
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    alertSuccess(mess, "AccidentTab3_Vendor.aspx?str=" + encryptedValue);
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #endregion

    #region SaveData
    private string SaveData(int CACTIVE)
    {
        DataSet dsFile = new DataSet("DS");


        dtUpload.TableName = "DT";
        dsFile.Tables.Add(dtUpload.Copy());
        dt = AccidentBLL.Instance.SaveTab3Vendor(hidID.Value, CACTIVE, Session["UserID"] + string.Empty, txtDAMAGE.Text.Trim(), dsFile);
        return dt.Rows.Count > 0 ? dt.Rows[0][0] + string.Empty : "";
    }
    #endregion

    #region ValidateSaveUpLoadFile
    private string ValidateSaveUpLoadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string accID)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            dt = AccidentBLL.Instance.AccidentTab1Select(accID);

            if (dtTemplate.Rows.Count > 0 && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty;
                if (TemplateID == ConfigValue.EmailAccident4)
                {
                    #region EmailAccident4
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, true, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com
                    //                    ,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com
                    //                    ,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com
                    //                    ,TMS_nut.t@pttor.com";

                    //EmailList += "raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com";
                    
                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    //Body = Body.Replace("{CREATE_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab3.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident4", ColumnEmailName);
                    #endregion

                }

                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region SetEnabledControlByID
    private void SetEnabledControlByID(string ID)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "SetEnabledControlByID('" + ID + "');", true);
    }
    #endregion

    #region dgvRequestFile_RowDataBound
    protected void dgvRequestFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            ImageButton btnDownload = (ImageButton)e.Row.FindControl("btnDownload");
            //ฟิก ตัวอักษรไว้ก่อน
            if (drv["UPLOAD_NAME"] + string.Empty == "แบบฟอร์มสอบสวนอุบัติเหตุ FM-รข.034")
            {
                btnDownload.Visible = true;
            }
        }

    }
    #endregion

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        string Path = this.CheckPathDownload();
        DownloadFilePDF(Path, "Incident_report_and_analysis_(FM_034).pdf");
    }

    private void DownloadFilePDF(string Path, string FileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    private string CheckPathDownload()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\FileFormat\\Vendor";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}