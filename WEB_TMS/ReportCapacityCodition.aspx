﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportCapacityCodition.aspx.cs" Inherits="ReportCapacityCodition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="72%" align="right">
                                                    <%-- <dx:ASPxComboBox ID="cboVendor" runat="server" Width="40%" ClientInstanceName="cboVendor"
                                                        TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="True"
                                                        OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" ValueField="SVENDORID" AutoPostBack="false">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                    </asp:SqlDataSource>--%>
                                                    <%--<dx:ASPxRadioButtonList runat="server" ID="rblCodition" SkinID="rblStatus">
                                                        <Items>
                                                            <dx:ListEditItem Text="ทั้งหมด" Value="A" />
                                                            <dx:ListEditItem Text="รายเดือน" Value="M" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>--%>
                                                </td>
                                                <td width="10%" align="right">
                                                    <dx:ASPxComboBox runat="server" ID="cboMonth" Width="100%">
                                                        <Items>
                                                            <dx:ListEditItem Text="มกราคม" Value="01" />
                                                            <dx:ListEditItem Text="กุมภาพันธ์" Value="02" />
                                                            <dx:ListEditItem Text="มีนาคม" Value="03" />
                                                            <dx:ListEditItem Text="เมษายน" Value="04" />
                                                            <dx:ListEditItem Text="พฤษภาคม" Value="05" />
                                                            <dx:ListEditItem Text="มิถุนายน" Value="06" />
                                                            <dx:ListEditItem Text="กรกฎาคม" Value="07" />
                                                            <dx:ListEditItem Text="สิงหาคม" Value="08" />
                                                            <dx:ListEditItem Text="กันยายน" Value="09" />
                                                            <dx:ListEditItem Text="ตุลาคม" Value="10" />
                                                            <dx:ListEditItem Text="พฤศจิกายน" Value="11" />
                                                            <dx:ListEditItem Text="ธันวาคม" Value="12" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="2%">-</td>
                                                <td width="8%">
                                                    <dx:ASPxComboBox runat="server" ID="cboYear" Width="100%">
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td width="8%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function(){ xcpn.PerformCallback('search');}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" AutoGenerateColumns="false"
                                            ClientInstanceName="gvw">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วัน" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NAMEOFDATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="วันที่" Width="18%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="SDATE">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="MaxCapacity" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center"  >
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="txtMaxCapacity" Text='<%# Eval("MAXCAPACITY").ToString() %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="OutSource" Width="8%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" >
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox runat="server" ID="txtOUTSOURCE" Text='<%# Eval("OUTSOURCE").ToString() %>'>
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager PageSize="31">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <dx:ASPxButton runat="server" ID="ASPxButton1" SkinID="_submit" AutoPostBack="false"
                                            CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(){ xcpn.PerformCallback('save');}" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" ID="ASPxButton2" SkinID="_back" AutoPostBack="false"
                                            CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function(){ xcpn.PerformCallback('back');}" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
