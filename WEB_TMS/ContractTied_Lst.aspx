﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ContractTied_Lst.aspx.cs" Inherits="ContractTied_Lst" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Src="~/UserControl/cButtonDelete.ascx" TagPrefix="uc1" TagName="cButtonDelete" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                 <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">กลุ่มงาน</label>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="ddlWordGroup" OnSelectedIndexChanged="ddlWordGroup_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <label  class="col-md-1 control-label">กลุ่มที่</label>
                <div class="col-md-5">
                    <asp:DropDownList runat="server" ID="ddlGroups" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row form-group">
                <label  class="col-md-1 control-label">สัญญา</label>
                <div class="col-md-5">
                    <dx:ASPxComboBox ID="cmbConTract" runat="server" CallbackPageSize="30" ClientInstanceName="cmbConTract" EnableCallbackMode="True" CssClass="form-control" OnItemRequestedByValue="cmbConTract_ItemRequestedByValue" Width="100%" OnItemsRequestedByFilterCondition="cmbConTract_ItemsRequestedByFilterCondition"
                        SkinID="xcbbATC" ValueField="ID">
                        <Columns>
                            <dx:ListBoxColumn FieldName="ID" Visible="false" />
                            <dx:ListBoxColumn FieldName="NAME" />
                        </Columns>
                    </dx:ASPxComboBox>
                </div>
                <label  class="col-md-1 control-label">Vendor</label>
                <div class="col-md-5 ">
                    <dx:ASPxComboBox ID="cmbVendor" runat="server" CssClass="form-control" ClientInstanceName="cmbVendor"  Width="100%"
                        SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID">
                        <Columns>
                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" />
                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" />
                        </Columns>
                    </dx:ASPxComboBox>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-11">
                    <center>
                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                </div>
                <div class="col-md-1">
                    <center>
                        <asp:Button Text="เพิ่ม" runat="server" ID="btnAdd" OnClick="btnAdd_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <asp:GridView runat="server" ID="gvContract" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="ID">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false" />
                            <asp:BoundField DataField="WORKGROUPNAME" HeaderText="กลุ่มงาน" />
                            <asp:BoundField DataField="GROUPNAME" HeaderText="กลุ่มที่" />
                            <asp:BoundField DataField="NAME" HeaderText="สัญญา" />
                            <asp:BoundField DataField="SABBREVIATION" HeaderText="บริษัท" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>  
                                    <div class="col-sm-1">
                                    <asp:ImageButton CssClass=""  ID="imgEdit" runat="server"  ImageUrl="~/Images/edit.png" Style="cursor: pointer" CommandArgument='<%# Eval("ID") %>' OnClick="imgEdit_Click" /></div>
                                    <uc1:cButtonDelete runat="server" ID="cButtonDelete" OnClickDelete="cButtonDelete_ClickDelete" IDDelete='<%# "gvContract_" + Eval("ID") %>'  />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

