﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="EmployeeExpire.aspx.cs" Inherits="EmployeeExpire" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .table> tbody> tr.GridColorHeader > th {
          text-align:center;
          font-weight:normal;
        }
        .table> tbody> tr > td {
          text-align:center;
          font-weight:normal;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <script language="javascript" type="text/javascript">
                    function CheckNumeric(e) {

                        if (window.event) // IE 
                        {
                            if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                                event.returnValue = false;
                                return false;

                            }
                        }
                        else { // Fire Fox
                            if ((e.which < 48 || e.which > 57) & e.which != 8) {
                                e.preventDefault();
                                return false;

                            }
                        }
                    }

             </script>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">เงื่อนไขการค้นหา&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract">
                    <div>
                        <br />
                        <div runat="server" Width="100%" CellPadding="5" CellSpacing="5" >                           
                            <div class="row" style="padding-bottom:5px;">
                                <div class="col-sm-1">

                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                    <asp:Label ID="Label3" runat="server" Text="บริษัทผู้ขนส่ง :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-md-3  ">
                                  <dx:ASPxComboBox ID="cboVendor" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{1}" ValueField="SVENDORID" IncrementalFilteringMode="Contains" >
                                      <Columns>
                                          <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                          <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="200px" />
                                      </Columns>
                                  </dx:ASPxComboBox>
                                  </div>

                                <div class="col-sm-2" style="text-align: right;">
                                    <asp:Label ID="lblFirstNameTab1" runat="server" Text="ค้นหาข้อมูลพนักงาน :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3" >
                                     <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"  placeholder="รหัสพนักงาน,ชื่อ-นามสกุล,หมายเลขบัตรประชาชน" />
                                </div>
                                                           
                            </div>                 
                            <div class="row" style="padding-bottom:5px;">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2" style="text-align: right;">
                                   <asp:Label ID="Label4" runat="server" Text="ประเภทเอกสาร :&nbsp;"></asp:Label>                                    
                                </div>
                                <div class="col-sm-3">
                                     <asp:DropDownList ID="ddlDocType" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>

                                <div class="col-sm-2" style="text-align: right;" >
                                    <asp:Label ID="lblLastNameTab1" runat="server" Text="สถานะ :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                   <asp:DropDownList ID="ddlStatusTab1" runat="server" CssClass="form-control">
                                       <asp:ListItem Selected="True" Text="- เลือกข้อมูล -" Value=""></asp:ListItem>
                                        <asp:ListItem Text="อนุญาต" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="ระงับการใช้งาน" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Black List" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>    

                                <%--<div class="col-sm-2" style="text-align: right;">
                                   <asp:Label ID="Label3" runat="server" Text="จำนวนวัน :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                     <asp:TextBox  ID="txtDayCount" runat="server" CssClass="form-control"placeholder="" />                                   
                                </div>--%>
                                
                            </div>
                            <div class="row" style="padding-bottom:5px;">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2" style="text-align: right;">
                                    <asp:Label ID="Label1" runat="server" Text="ช่วงเวลาเอกสารหมดอายุ :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                   <asp:TextBox runat="server" ID="txtDateStart" CssClass="datepicker" />
                                </div>
                                <div class="col-sm-2" style="text-align: right;">
                                     <asp:Label ID="Label2" runat="server" Text="ถึง :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3">
                                      <asp:TextBox runat="server" ID="txtDateEnd" CssClass="datepicker" />
                                </div>   
                            </div>

                            <div class="row" style="padding-bottom:5px;">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2" style="text-align: right;">
                                    <asp:Label ID="Label5" runat="server" Text="หมดอายุในอีก(วัน) :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                   <asp:TextBox runat="server" ID="txtDocExpireInDay" CssClass="form-control" placeholder="รับเฉพาะตัวเลข" onkeypress="CheckNumeric(event);"  />                                  
                                </div>
                                <div class="col-sm-3" style="text-align: right;">
                                    <asp:Label ID="Label6" runat="server" Text="จำนวนค้นหา :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-3" style="text-align: right;">
                                    <asp:DropDownList ID="ddlMaxNumber" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="100" Selected="True" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="200" Value="200"></asp:ListItem>                                       
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="1000" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="ทั้งหมด" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                             </div>
                            <div class="row" style="padding-bottom:5px;">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-2" style="text-align: right;">
                                    <asp:Label ID="Label7" runat="server" Text="การจ้างงาน :&nbsp;"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlInUse" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="ทั้งหมด" Value=""></asp:ListItem>
                                        <asp:ListItem Text="จ้างงาน" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="เลิกจ้างงาน" Value="0"></asp:ListItem>
                                    </asp:DropDownList>                                                                 
                                </div>
                             </div>
                        </div>
                        <br />
                        <div class="panel-footer" style="text-align: right">
                         <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                            <ContentTemplate>
                            <asp:Button ID="cmdSearchTab1" runat="server" Text="Search" OnClick="cmdSearchTab1_Click" CssClass="btn btn-success" Width="100px" />
                            <asp:Button ID="cmdRefresh" runat="server" Text="Clear" OnClick="cmdRefresh_ServerClick" CssClass="btn btn-success" Width="100px" />
                            <asp:Button ID="cmdExport" runat="server" Text="Export Excel" OnClick="cmdExport_ServerClick" CssClass="btn btn-success" Width="100px" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="cmdExport" />
                            </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                </div>
                <div class="panel-body" align="center">
                    <br />
                    <div class="">
                        <div class="panel-collapse collapse in" id="collapseFindContract2">
                            <div class="table-responsive" style="font-weight:normal; text-align:center;">
                                <asp:GridView ID="dgvData0" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" AutoGenerateColumns="false"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" 
                                    HeaderStyle-Font-Bold="false" HorizontalAlign="Center" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    OnPageIndexChanging="dgvData_PageIndexChanging" OnRowCommand="dgvData_RowCommand"  OnRowDataBound="dgvData_RowDataBound" DataKeyNames="SEMPLOYEEID"> 
                                    <Columns>
                                        <asp:BoundField DataField="SEMPLOYEEID" HeaderText="รหัส">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>                                       
                                        <asp:BoundField DataField="FULLNAME" HeaderText="ชื่อ - นามสกุล">
                                            <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                        </asp:BoundField>                                       
                                         <asp:BoundField DataField="VENDOR_FULLNAME" HeaderText="บริษัทผู้ขนส่ง">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ENDDATE_TYPE1" HeaderText="วันที่ใบขับขี่ประเภท 4 หมดอายุ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>   
                                         <asp:BoundField DataField="COUNT_LEFTDATE1" HeaderText="จำนวนวันที่คงเหลือ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>  
                                         <asp:BoundField DataField="ENDDATE_TYPE2" HeaderText="ใบขับขี่เชิงป้องกันอุบัติเหตุ หมดอายุ" DataFormatString="{0:dd/MM/yyyy}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField> 
                                         <asp:BoundField DataField="COUNT_LEFTDATE2" HeaderText="จำนวนวันที่คงเหลือ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>  
                                         <asp:BoundField DataField="ENDDATE_TYPE3" HeaderText="ใบอนุญาตผู้ปฏิบัติงาน หมดอายุ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField> 
                                         <asp:BoundField DataField="COUNT_LEFTDATE3" HeaderText="จำนวนวันที่คงเหลือ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>  
                                         <asp:BoundField DataField="ENDDATE_TYPE4" HeaderText="ใบผ่านการอบรม SHE หมดอายุ" DataFormatString="{0:dd/MM/yyyy}">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>    
                                         <asp:BoundField DataField="COUNT_LEFTDATE4" HeaderText="จำนวนวันที่คงเหลือ" DataFormatString="{0:dd/MM/yyyy}" >
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>                                    
                                        <asp:BoundField DataField="CACTIVE" HeaderText="สถานะ">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>                              
                                        <%--<asp:BoundField DataField="DBIRTHDATE" HeaderText="อายุ พขร.">
                                            <ItemStyle Wrap="False" HorizontalAlign="Center" />
                                        </asp:BoundField>--%>
                                        <asp:TemplateField HeaderText="View">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aView" runat="server" ImageUrl="~/Images/blue-37.png" CommandName="view" Width="24px" Height="24px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Edit">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aEdit" runat="server" ImageUrl="~/Images/blue-23.png" CommandName="editData" Width="26px" Height="26px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>

                             <div class="col-md-12">
                                 <asp:GridView runat="server" ID="dgvData"
                                        Width="100%" HeaderStyle-HorizontalAlign="Center" BorderWidth="1"
                                        GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-BorderWidth="1"
                                        HorizontalAlign="Center" AutoGenerateColumns="true" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AllowPaging="false" RowStyle-Width="1" Font-Names="Cordia New" Font-Size="16" 
                                      OnPageIndexChanging="dgvData_PageIndexChanging" OnRowCommand="dgvData_RowCommand" OnRowCreated="dgvData_RowCreated"  OnRowDataBound="dgvData_RowDataBound" DataKeyNames="รหัส">
                                        <Columns> 
                                         <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="View" >
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aView" runat="server" ImageUrl="~/Images/blue-37.png" CommandName="view" Width="24px" Height="24px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Edit">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="aEdit" runat="server" ImageUrl="~/Images/blue-23.png" CommandName="editData" Width="26px" Height="26px" Style="cursor: pointer" />
                                            </ItemTemplate>
                                        </asp:TemplateField>       --%>                                   
                                        </Columns>
                                    </asp:GridView>
                             </div>
                        </div>
                        <br />
                        <br />
                        <div class="panel-footer" style="text-align: right;display:none;">
                            <%--<a href="#" id="cmdAdd" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdAdd_ServerClick"><i class="fa fa-edit"></i>&nbsp;เพิ่มข้อมูล</a>--%>
                            <asp:Button ID="cmdAdd" runat="server" Text="&nbsp;เพิ่มข้อมูล" OnClick="cmdAdd_ServerClick" CssClass="btn btn-success" Width="100px" Visible="false" />
                            <%--<a href="#" id="cmdRefresh" runat="server" class="btn btn-md btn-hover btn-success" style="width: 100px" onserverclick="cmdRefresh_ServerClick"><i class="fa fa-refresh"></i>&nbsp;รีเฟรช</a>--%>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

