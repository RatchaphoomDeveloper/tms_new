﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="questionnaire_edit.aspx.cs" Inherits="questionnaire_edit" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13 {
            width: 50%;
            height: 31px;
        }

        .padding {
            padding: 0px 0px 0px 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td bgcolor="#0E4999">
                <img src="images/spacer.GIF" width="250px" height="1px"></td>
        </tr>
    </table>
    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
        <tr>
            <td style="width: 20%">
                <%-- <span style="color: Red; display:none;">กรุณากรอกข้อมูลให้ครบถ้วน</span>--%>
            </td>
            <td style="width: 35%">&nbsp;
            </td>
            <td style="width: 15%"></td>
            <td style="width: 30%"></td>
        </tr>
        <tr>
            <td class="style28">ครั้งที่ตรวจประเมิน</td>
            <td align="left" class="style27">
                <asp:Label runat="server" ID="lbltmp">1</asp:Label>
                <dx:ASPxRadioButtonList ID="rblNO" runat="server" SkinID="rblStatus">
                    <ClientSideEvents ValueChanged="function(s,e){xcpn.PerformCallback('NOChange');}"></ClientSideEvents>
                </dx:ASPxRadioButtonList>
            </td>
            <td>บริษัทผู้ประกอบการขนส่ง
            </td>
            <td>
                 <dx:ASPxComboBox ID="cmbVendor" runat="server" TextField="SABBREVIATION" ValueField="SVENDORID" Width="200"></dx:ASPxComboBox>

                <dx:ASPxTextBox ID="txtVendor" runat="server" ClientEnabled="False"
                    ClientInstanceName="txtVendor" CssClass="dxeLineBreakFix" ForeColor="Black"
                    Text="" Width="200px" Visible="false">
                    <Border BorderColor="White" />
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="txtVendorID" runat="server" Width="10px" ClientVisible="false">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <%--OnPreRender="txtPassword_PreRender"--%>
            <td bgcolor="#FFFFFF" class="style24">วันที่ตรวจประเมิน
            </td>
            <td align="left">
                <dx:ASPxDateEdit ID="dteDateStart1" runat="server" SkinID="xdte" ClientInstanceName="dteDateCheck">
                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                        ValidationGroup="add">
                        <ErrorFrameStyle ForeColor="Red">
                        </ErrorFrameStyle>
                        <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                    </ValidationSettings>
                </dx:ASPxDateEdit>
            </td>
            <td>สถานที่ตรวจประเมิน
            </td>
            <td>
                <dx:ASPxTextBox ID="txtAddress" runat="server" Width="260px">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">รายชื่อคณะผู้ตรวจ
            </td>
            <td>
                <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                    Border-BorderStyle="None" Settings-ShowColumnHeaders="false" Style="margin-top: 0px"
                    ClientInstanceName="sgvw" Width="100%" KeyFieldName="dtsID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                    SkinID="_gvw" OnAfterPerformCallback="sgvw_AfterPerformCallback">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                            VisibleIndex="0">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="รหัส" FieldName="dtsID" Visible="false">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="3" Width="20%">
                            <DataItemTemplate>
                                <dx:ASPxTextBox ID="txtName" runat="server" Width="150px" Text='<%# Eval("dtsName") %>'>
                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                        ValidationGroup="add">
                                        <ErrorFrameStyle ForeColor="Red">
                                        </ErrorFrameStyle>
                                        <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="30%">
                            <DataItemTemplate>
                                <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" SkinID="_delete"
                                    Width="10px">
                                    <ClientSideEvents Click="function (s, e) {sgvw.PerformCallback('deleteList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                </dx:ASPxButton>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Settings ShowColumnHeaders="False" GridLines="None"></Settings>
                    <Border BorderStyle="None"></Border>
                </dx:ASPxGridView>
                <dx:ASPxButton ID="btnAddd" runat="server" Text="More..." AutoPostBack="false" CssClass="dxeLineBreakFix"
                    Width="70px">
                    <ClientSideEvents Click="function (s, e) {sgvw.PerformCallback('AddClick'); }"></ClientSideEvents>
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td valign="top">รายชื่อผู้รับการตรวจ
            </td>
            <td>&nbsp;
                            <dx:ASPxGridView ID="sgvw1" runat="server" AutoGenerateColumns="False" ClientInstanceName="sgvw1"
                                KeyFieldName="dtsID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnAfterPerformCallback="sgvw1_AfterPerformCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtsID" ShowInCustomizationForm="True"
                                        Visible="False">
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn ShowInCustomizationForm="True" VisibleIndex="3" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxTextBox ID="txtName1" runat="server" Text='<%# Eval("dtsName") %>' Width="150px">
                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                    ValidationGroup="add">
                                                    <ErrorFrameStyle ForeColor="Red">
                                                    </ErrorFrameStyle>
                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" ShowInCustomizationForm="True" VisibleIndex="5"
                                        Width="30%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbDel1" runat="server" CausesValidation="false" SkinID="_delete"
                                                Width="10px">
                                                <ClientSideEvents Click="function (s, e) {sgvw1.PerformCallback('deleteList1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <Settings GridLines="None" ShowColumnHeaders="False" />
                                <Border BorderStyle="None" />
                            </dx:ASPxGridView>
                <dx:ASPxButton ID="btnAddd1" runat="server" Text="More..." AutoPostBack="false" CssClass="dxeLineBreakFix"
                    Width="70px">
                    <ClientSideEvents Click="function (s, e) {sgvw1.PerformCallback('AddClick1'); }"></ClientSideEvents>
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td valign="top">เอกสารแนบ
            </td>
            <td colspan="3" valign="top">
                <div class="panel-body" style="padding: 0px;">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table3" runat="server">
                                <asp:TableRow>
                                    <%--<asp:TableCell>
                                        <asp:Label ID="lblUploadTypeTab3" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                    </asp:TableCell>--%>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell>

                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:FileUpload ID="fileUpload" runat="server" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                    <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                    <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                    <asp:BoundField DataField="FULLPATH" Visible="false" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <table style="display: none;">
                    <tr>
                        <td>
                            <span style="text-align: left">ชื่อหลักฐาน</span>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <span style="text-align: left">ไฟล์แนบ </span>
                        </td>
                        <td>
                            <dx:ASPxUploadControl ID="uploader" runat="server" ClientInstanceName="uploader"
                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="UploadControl_FileUploadComplete">
                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>" AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                </ValidationSettings>
                                <BrowseButton Text="แนบไฟล์">
                                </BrowseButton>
                            </dx:ASPxUploadControl>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add">
                                <ClientSideEvents Click="function(s,e){uploader.Upload();sgvwFile.PerformCallback('Upload');}"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td>แบบฟอร์ม </td>
            <td colspan="3">
                <dx:ASPxComboBox class="form-control" ID="cmbForm" runat="server" Width="300" SelectedIndex="0" OnSelectedIndexChanged="cmbForm_SelectedIndexChanged" AutoPostBack="true">
                </dx:ASPxComboBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td colspan="3">
                <dx:ASPxGridView ID="sgvwFile" runat="server" AutoGenerateColumns="False" Settings-ShowColumnHeaders="false"
                    Style="margin-top: 0px" ClientInstanceName="sgvwFile" Width="100%" KeyFieldName="dtID"
                    OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SkinID="_gvw"
                    OnAfterPerformCallback="sgvwFile_AfterPerformCallback" Visible="false">
                    <ClientSideEvents EndCallback="function(s,e){if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                            VisibleIndex="0">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="รหัส" FieldName="dtID" Visible="false">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="FilePath" FieldName="dtFilePath" Visible="false">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                            <DataItemTemplate>
                                <dx:ASPxLabel ID="lblScore" runat="server" Text="ชื่อหลักฐาน">
                                </dx:ASPxLabel>
                            </DataItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <CellStyle ForeColor="#0066FF" HorizontalAlign="Center">
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="85%">
                            <DataItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="lblEvidence" runat="server" Text='<%# Eval("dtEvidenceName") %>'
                                                Width="250">
                                            </dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                Text="Delete" Width="15px">
                                                <ClientSideEvents Click="function (s, e) {sgvwFile.PerformCallback('DeleteFileList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="imbView0" runat="server" CausesValidation="false" AutoPostBack="false"
                                                Text="View" Width="15px">
                                                <ClientSideEvents Click="function (s, e) {sgvwFile.PerformCallback('ViewFileList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Settings GridLines="None" ShowColumnHeaders="False"></Settings>
                    <Border BorderStyle="None" />
                </dx:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td colspan="4">ผลการประเมินแบบสอบถาม
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:UpdatePanel runat="server" ID="uplMain" UpdateMode="Conditional" ViewStateMode="Enabled" ValidateRequestMode="Enabled">
                    <ContentTemplate>
                        <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" SettingsBehavior-AllowSort="false"
                            KeyFieldName="NGROUPID" Width="100%" DataSourceID="sdsMaster">
                            <Columns>
                                <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="หัวข้อแบบประเมิน" CellStyle-HorizontalAlign="Left" VisibleIndex="0" Width="400">
                                    <DataItemTemplate>
                                        <asp:Label ID="lblSGROUPNAME" runat="server" Text='<%#Eval("SGROUPNAME")%>'></asp:Label>
                                    </DataItemTemplate>
                                </dx:GridViewDataColumn>

                                <dx:GridViewDataColumn Caption="ระดับคะแนน" VisibleIndex="1" Width="300" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="เหตุผลเพิ่มเติม" VisibleIndex="2">
                                    <DataItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="คะแนนรวม "></asp:Label><asp:Label ID="Label2" runat="server">0</asp:Label>
                                    </DataItemTemplate>
                                    <CellStyle HorizontalAlign="Right" />
                                </dx:GridViewDataColumn>
                            </Columns>
                            <SettingsBehavior AllowSort="False"></SettingsBehavior>
                            <SettingsDetail ShowDetailButtons="False" ShowDetailRow="True" />
                            <Templates>
                                <DetailRow>
                                    <dx:ASPxGridView ID="detailGrid" runat="server" DataSourceID="sdsDetail" KeyFieldName="NTYPEVISITFORMID" OnRowCommand="detailGrid_RowCommand"
                                        Width="100%" OnBeforePerformDataSelect="detailGrid_DataSelect" Settings-ShowColumnHeaders="false" OnHtmlDataCellPrepared="detailGrid_HtmlDataCellPrepared" SettingsBehavior-AllowSort="false" SettingsPager-PageSize="100">
                                        <Settings GridLines="None" ShowVerticalScrollBar="true" VerticalScrollableHeight="300"></Settings>
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="SVISITFORMNAME" CellStyle-HorizontalAlign="Left" Width="400">
                                                <DataItemTemplate>
                                                    <asp:Label ID="lblSVISITFORMNAME" runat="server"></asp:Label>
                                                </DataItemTemplate>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Width="300" CellStyle-HorizontalAlign="Center" Caption="#">
                                                <DataItemTemplate>
                                                    <dx:ASPxRadioButtonList ID="rblWEIGHT" runat="server" ClientInstanceName="rblWEIGHT" TextField="STYPEVISITLISTNAME" ValueField="NTYPEVISITLISTSCORE" DataSourceID="sdsRedio"  CommandName="ChangeScore" SkinID="rblStatus" OnSelectedIndexChanged="rblWEIGHT_SelectedIndexChanged" AutoPostBack="true">
                                                    </dx:ASPxRadioButtonList>
                                                    <dx:ASPxTextBox ID="txtWEIGHT" runat="server" ClientInstanceName="txtWEIGHT" Text='<%#Eval("NWEIGHT")%>' ClientVisible="false" ClientEnabled="false">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="txtOldSum" runat="server" ClientInstanceName="txtOldSum" Text="0" ClientVisible="false" ClientEnabled="false">
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <CellStyle HorizontalAlign="Center" VerticalAlign="Top">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn>
                                                <DataItemTemplate>
                                                    <dx:ASPxTextBox ID="txtRemark" runat="server" Width="250">
                                                    </dx:ASPxTextBox>
                                                </DataItemTemplate>
                                                <CellStyle HorizontalAlign="Left" VerticalAlign="Top">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <%--                                                <dx:GridViewDataColumn FieldName="NWEIGHT" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NGROUPID" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NTYPEVISITFORMID" Visible="false">
                                                </dx:GridViewDataColumn>--%>
                                        </Columns>
                                        <SettingsBehavior AllowSort="False" />
                                        <SettingsPager Visible="False">
                                        </SettingsPager>
                                        <Settings ShowFooter="False" />
                                        <Border BorderStyle="None" />
                                    </dx:ASPxGridView>
                                </DetailRow>
                            </Templates>
                            <Styles Cell-BackColor="#ccffff">
                                <Cell BackColor="#CCFFFF"></Cell>
                            </Styles>
                            <Settings GridLines="None"></Settings>
                        </dx:ASPxGridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:SqlDataSource ID="sdsMaster" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    SelectCommand="SELECT GV.NGROUPID ,GV.NNO || '. ' || GV.SGROUPNAME AS SGROUPNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TGROUPOFVISITFORM gv ON TV.NTYPEVISITFORMID = GV.NTYPEVISITFORMID where TV.NTYPEVISITFORMID = :NTYPEVISITFORMID ORDER BY GV.NNO">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sNTYPEVISITFORMID" Name="NTYPEVISITFORMID" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="sdsDetail" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    SelectCommand="SELECT NVISITFORMID,'&nbsp;&nbsp;&nbsp;' || rownum || ') ' || SVISITFORMNAME AS SVISITFORMNAME, nvl(NWEIGHT,0) AS NWEIGHT, NVISITFORMID ,NGROUPID,NTYPEVISITFORMID
 FROM TVISITFORM WHERE (NGROUPID = :oNGROUPID) AND (CACTIVE = '1')">
                    <SelectParameters>
                        <asp:SessionParameter Name="oNGROUPID" SessionField="ONGROUPID" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsRedio" runat="server" EnableCaching="true" CacheKeyDependency="chkRedio"
                    ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    SelectCommand="SELECT TVL.NTYPEVISITLISTSCORE , TVL.STYPEVISITLISTNAME  FROM TTYPEOFVISITFORM tv INNER JOIN TTYPEOFVISITFORMLIST tvl ON TV.NTYPEVISITFORMID = TVL.NTYPEVISITFORMID WHERE TV.NTYPEVISITFORMID = :NTYPEVISITFORMID ORDER BY TVL.NTYPEVISITLISTSCORE">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="sNTYPEVISITFORMID" Name="NTYPEVISITFORMID" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>รายละเอียดเพิ่มเติม
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <dx:ASPxMemo ID="txtDetail" runat="server" Height="100px" Width="400px">
                </dx:ASPxMemo>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                <table>
                    <tr>
                        <td>คะแนนรวม
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtTotal" ClientInstanceName="txtTotal" ClientEnabled="false"
                                Border-BorderColor="White" runat="server" Width="80px" Text="0" CssClass="dxeLineBreakFix" ForeColor="Black"
                                RightToLeft="True">
                                <Border BorderColor="White"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td>คะแนน
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                <asp:Button ID="btnSave" runat="server" Enabled="false" Text="บันทึก"
                    UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" OnClick="btnSave_Click"
                    Style="width: 120px" />
                <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" Visible="false" Style="width: 100px">
                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                </dx:ASPxButton>
            </td>
            <td colspan="2" class="style13" style="padding-left:5px;">
                <asp:Button ID="btnClose" runat="server" Enabled="false" Text="ปิด"
                    UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click"
                    Style="width: 100px" />
                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" Visible="false">
                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'questionnaire.aspx'; }" />
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td align="left" bgcolor="#FFFFFF" colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>

            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
