﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_TimeLine_add.aspx.cs" Inherits="admin_TimeLine_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 27px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);

            var bool1 = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
            uploader1.SetClientVisible(bool1);
            txtFileName1.SetClientVisible(!bool1);
            btnView1.SetEnabled(!bool1);
            btnDelFile1.SetEnabled(!bool1);

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px">
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 25%">
                            หน่วยงาน<font color="#FF0000">*</font>
                        </td>
                        <td style="width: 60%" colspan="2">
                            <dx:ASPxComboBox ID="cbxOrganiz" runat="server" CallbackPageSize="10" ClientInstanceName="cbxOrganiz"
                                EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="200px">
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="100px" />
                                    <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" Width="100px" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="<%$ Resources:CommonResource, Msg_Unit %>" IsRequired="True" />
                                    <RequiredField IsRequired="True" ErrorText="<%$ Resources:CommonResource, Msg_Unit %>">
                                    </RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 10%;">
                            &nbsp;
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 40%;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            TimeLine การยืนยันรถตามสัญญา <font color="#ff0000">*</font>
                        </td>
                        <td align="left" class="style27" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtCarConfirm" runat="server" ClientInstanceName="txtCarConfirm"
                                            Width="100px" MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลายืนยันรถตามสัญญา" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            <span style="text-align: left">TimeLine การยืนยันรถตามแผน <font color="#ff0000">*</font></span>
                        </td>
                        <td align="left" class="style27" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtPlanConfirm" runat="server" ClientInstanceName="txtPlanConfirm"
                                            Width="100px" MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="ChkAdd"
                                                ErrorFrameStyle-ForeColor="Red" SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลายืนยันรถตามสัญญา" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.&nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxCheckBox ID="ckbStatus" ClientInstanceName="ckbStatus" runat="server" CheckState="Unchecked"
                                            Text="ภายใน 1 ชม.หลังจากปตท.จัดแผน ">
                                        </dx:ASPxCheckBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            เวลาปิดรับ Order ของคลัง
                        </td>
                        <td align="left" class="style27" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtCloseOrder" runat="server" ClientInstanceName="txtCloseOrder"
                                            Width="100px" MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลาปิดรับ Order" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            Change To Stat
                        </td>
                        <td align="left" class="style27" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtMinCHG2STAT" runat="server" ClientInstanceName="txtMinCHG2STAT"
                                            Width="100px" NullText="Min Stat" >
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่ค่า Min Stat" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Require %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        <dx:ASPxTextBox ID="txtMaxCHG2STAT" runat="server" ClientInstanceName="txtMaxCHG2STAT"
                                            Width="100px" NullText="Max Stat">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่ค่า Max Stat" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Require %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF" class="style24">
                            สถานะ <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="4">
                            <dx:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                SelectedIndex="0" EnableDefaultAppearance="False" ClientInstanceName="rblStatus"
                                SkinID="rblStatus" ID="rblStatus">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Active" Value="1"></dx:ListEditItem>
                                    <dx:ListEditItem Text="InActive" Value="0"></dx:ListEditItem>
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ไฟล์เอกสารเพิ่มเติม
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ชื่อหลักฐาน
                        </td>
                        <td colspan="2">
                            Allowed file types: ฯลฯ<br>
                            Max file size:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxUploadControl ID="uplExcel" runat="server" ClientInstanceName="uploader"
                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                </ValidationSettings>
                                <ClientSideEvents TextChanged="function(s,e){uploader.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xls หรือ .xlsx หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath.SetValue((e.callbackData+'').split('|')[0]);txtFileName.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                </ClientSideEvents>
                                <BrowseButton Text="แนบไฟล์">
                                </BrowseButton>
                            </dx:ASPxUploadControl>
                            <dx:ASPxTextBox ID="txtFileName" runat="server" Width="300px" ClientInstanceName="txtFileName"
                                ClientEnabled="false" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtFilePath" runat="server" Width="220px" ClientInstanceName="txtFilePath"
                                ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnView" runat="server" Text="View" AutoPostBack="false" ClientInstanceName="btnView"
                                CssClass="dxeLineBreakFix" Width="60px" ClientEnabled="false">
                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue());}" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnDelFile" runat="server" Text="Delete" AutoPostBack="false"
                                ClientInstanceName="btnDelFile" CssClass="dxeLineBreakFix" Width="60px" ClientEnabled="false">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath.GetValue() +';1');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">
                            <dx:ASPxTextBox ID="txtEvidence1" runat="server" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td class="style13">
                            <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                    AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                </ValidationSettings>
                                <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath1.SetValue((e.callbackData+'').split('|')[0]);txtFileName1.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                </ClientSideEvents>
                                <BrowseButton Text="แนบไฟล์">
                                </BrowseButton>
                            </dx:ASPxUploadControl>
                            <dx:ASPxTextBox ID="txtFileName1" runat="server" ClientInstanceName="txtFileName1"
                                ClientEnabled="False" ClientVisible="False" Width="300px">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtFilePath1" runat="server" ClientInstanceName="txtFilePath1"
                                ClientVisible="False" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td class="style13">
                            <dx:ASPxButton ID="btnView1" runat="server" AutoPostBack="false" ClientInstanceName="btnView1"
                                CssClass="dxeLineBreakFix" Text="View" Width="60px" ClientEnabled="false">
                                <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}" />
                            </dx:ASPxButton>
                            <dx:ASPxButton ID="btnDelFile1" runat="server" AutoPostBack="false" ClientInstanceName="btnDelFile1"
                                CssClass="dxeLineBreakFix" Text="Delete" Width="60px" ClientEnabled="false">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath1.GetValue() +';2');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            ผู้บันทึก
                        </td>
                        <td align="left" colspan="4">
                            <dx:ASPxLabel ID="lblUser" runat="server" Text="" ClientInstanceName="lblUser">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" align="right" class="style14">
                            &nbsp;
                        </td>
                        <td align="right" bgcolor="#FFFFFF" style="width: 25%">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')&& (ckbStatus.GetChecked() ? true: !ASPxClientEdit.ValidateGroup('ChkAdd'))) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td align="right" bgcolor="#FFFFFF" style="text-align: left">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_TimeLine_lst.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="3" class="style14">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" UpdateCommand="UPDATE TTERMINAL SET SEVIDENCE = :oSEVIDENCE,SFILENAME = :oSFILENAME,SFILEPATH = :oSFILEPATH,SEVIDENCE1 = :oSEVIDENCE1,SFILENAME1 = :oSFILENAME1,SFILEPATH1 = :oSFILEPATH1, TCARCONFIRM = :TCARCONFIRM, TPLANCONFIRM =:TPLANCONFIRM,CSTATUS =:CSTATUS, TCLOSEORDER =:TCLOSEORDER, CACTIVE =:CACTIVE, DUPDATE =SYSDATE, SUPDATE = :sUpdate ,MIN2STAT= :MIN2STAT ,MAX2STAT= :MAX2STAT WHERE  STERMINALID = :sterminalID"
                    OnInserted="sds_Inserted" OnInserting="sds_Inserting" OnUpdated="sds_Updated"
                    OnUpdating="sds_Updating" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                    <UpdateParameters>
                        <asp:ControlParameter Name="oSEVIDENCE" ControlID="txtEvidence" PropertyName="Text" />
                        <asp:ControlParameter Name="oSFILENAME" ControlID="txtFileName" PropertyName="Text" />
                        <asp:ControlParameter Name="oSFILEPATH" ControlID="txtFilePath" PropertyName="Text" />
                        <asp:ControlParameter Name="oSEVIDENCE1" ControlID="txtEvidence1" PropertyName="Text" />
                        <asp:ControlParameter Name="oSFILENAME1" ControlID="txtFileName1" PropertyName="Text" />
                        <asp:ControlParameter Name="oSFILEPATH1" ControlID="txtFilePath1" PropertyName="Text" />
                        <asp:ControlParameter Name="TCARCONFIRM" ControlID="txtCarConfirm" PropertyName="Text" />
                        <asp:ControlParameter Name="TPLANCONFIRM" ControlID="txtPlanConfirm" PropertyName="Text" />
                        <asp:ControlParameter Name="CSTATUS" ControlID="ckbStatus" PropertyName="Value" />
                        <asp:ControlParameter Name="TCLOSEORDER" ControlID="txtCloseOrder" PropertyName="Text" />
                        <asp:ControlParameter Name="MAX2STAT" ControlID="txtMaxCHG2STAT" PropertyName="Text" />
                        <asp:ControlParameter Name="MIN2STAT" ControlID="txtMinCHG2STAT" PropertyName="Text" />
                        <asp:ControlParameter Name="cActive" ControlID="rblStatus" PropertyName="Value" />
                        <asp:ControlParameter Name="sterminalID" ControlID="cbxOrganiz" PropertyName="Value" />
                        <asp:SessionParameter Name="sUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
