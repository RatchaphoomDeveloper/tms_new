﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Data;
using System.Web.Configuration;

public partial class bypassmail2system : System.Web.UI.Page
{
    string connectionstring = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string[] parammode = { "mode", "refid", "page", "parameter", "userlst" };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["UserID"] = "SYS_TMS";

            Session["CGROUP"] = "1";
            string str = Request.QueryString["proc"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);

                int idx4vs = 0;
                foreach (string QryString in strQuery)
                {

                    string vs_name = QryString, vs_value = QryString;
                    if ((QryString.LastIndexOf('$') > -1))
                    {
                        vs_name = QryString.Split('$')[0];
                        vs_value = QryString.Split('$')[1];
                    }
                    ViewState[vs_name] = vs_value;
                    //Response.Write("<br>" + QryString);
                    idx4vs++;
                }

            }
        }
    }
    protected void imbLogIn_Click(object sender, ImageClickEventArgs e)
    {
        ///เช็คผู้ใช้ลีอกอิน
        // ViewState["usr_acc"] = ",sysadmin,rkadmin-4,rkadmin-9,";
        string dv_logindisp = "", dv_checkstepdisp = "none";
        if (("," + ViewState["usr_acc"].ToString() + ",").IndexOf("," + txtUserName.Text + ",") > -1)
        {
            //Session["UserID"] = txtUserName.Text;
            #region เมื่อกดปุ่ม login
            if (!String.IsNullOrEmpty(txtUserName.Text + "") || !String.IsNullOrEmpty(txtPassword.Text + ""))
            {
                var EncodePass = STCrypt.encryptMD5(txtPassword.Text);
                DataTable dt = new DataTable();
                dt = CommonFunction.Get_Data(connectionstring, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE SUSERNAME = '" + CommonFunction.ReplaceInjection(txtUserName.Text.Trim()) + "' AND SPASSWORD = '" + EncodePass + "' AND CACTIVE = '1'");
                if (dt.Rows.Count > 0)
                {
                    Session["UserID"] = dt.Rows[0][0] + "";
                    Session["SVDID"] = dt.Rows[0][1] + "";
                    Session["CGROUP"] = dt.Rows[0][2] + "";
                    Session["UserName"] = dt.Rows[0][4] + "";
                    Session["LoginID"] = dt.Rows[0][3] + "";
                    Session["CHKCHANGEPASSWORD"] = dt.Rows[0]["CHKDATE"] + "";
                }
                else { Page.ClientScript.RegisterStartupScript(this.GetType(), "UnUser", "<script>alert('กรุณาตรวจสอบ การกรอกUserNameและpassword!');</script>"); return; }
            }
            else { Page.ClientScript.RegisterStartupScript(this.GetType(), "Loginfail", "<script>alert('กรุณาระบุ usernameและpassword เพื่อยืนยันตัวตนของท่าน!');</script>"); return; }
            #endregion

            dv_logindisp = "none"; dv_checkstepdisp = "";
            ltrProgress.Text = "กำลังเช็คข้อมูลของรายการ..";
        }
        else { dv_logindisp = ""; dv_checkstepdisp = "none"; Page.ClientScript.RegisterStartupScript(this.GetType(), "Loginfail", "<script>alert('ท่านไม่ได้รับมอบหมายใด้ดูแลและดำเนินการผ่านระบบEmail!');</script>"); return; }
        ///display div
        dv_login.Style.Add("display", dv_logindisp);
        dv_checkstep.Style.Add("display", dv_checkstepdisp);
        

        ///เช็คสถานะของรายการว่ายังอยู่ในสถานะ
        using (OracleConnection oraconect = new OracleConnection(connectionstring))
        {
            if (oraconect.State == ConnectionState.Closed) oraconect.Open();

            string get_val = CommonFunction.Get_Value(oraconect, "SELECT CSTATUS FROm TAPPEAL WHERE SAPPEALID='" + ViewState["SAPPEALID"] + "'");
            if (get_val != ViewState["CSTATUS"] + "")
            {
                imbTryAgain.Visible = true;
                ltrProgress.Text = "<br>ไม่สามารถทำรายการได้เนื่องจากรายการดังกล่าวไม่ได้อยู่ในสถานะที่ท่านจะทำรายการ<br>หรืออาจจะมีผู้อื่นที่ได้รับสิทธิ์มอบหมายงานเข้าไปทำรายการดังกล่าวไปแล้ว";
            }
            else
            {
                imbTryAgain.Visible = false;
                ltrProgress.Text = "กำลังนำส่งท่านสู่ URL ปลายทาง..";
                string sParamByMode = "&";
                switch (ViewState["mode"].ToString().ToLower()) 
                {
                    case "NEEDDOC2VD4ATTHDOC":
                        sParamByMode = "&" + ViewState["SPROCESSID"];
                        break; 
                }
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt(ViewState["SAPPEALID"] + "&" + ViewState["SHEADREGISTERNO"] + "&" + ViewState["STRAILREGISTERNO"] + "&" + ViewState["DINCIDENT"] + sParamByMode));

                Response.Redirect("" + ViewState["URL"] + ".aspx?str=" + sEncrypt);
            }
        }
    }
    protected void imbTryAgain_Click(object sender, ImageClickEventArgs e)
    {
        dv_checkstep.Style.Add("display", "none");
        dv_login.Style.Add("display", "");
    }

}