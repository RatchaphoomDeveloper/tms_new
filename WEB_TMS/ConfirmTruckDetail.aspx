﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ConfirmTruckDetail.aspx.cs" Inherits="ConfirmTruckDetail" Culture="en-US" UICulture="en-US" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="form-horizontal">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a class="accordion-toggle" data-toggle="collapse" href="#collapse1" id="acollapse1">ค้นหา</a>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row form-group">
                                <label class="col-md-3 control-label">
                                    วันที่ยืนยันรถ
                                    <asp:Label Text=" *" runat="server" ForeColor="Red" /></label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDate" Enabled="false" />
                                </div>
                                <label class="col-md-2 control-label">กลุ่มงาน</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtGroup" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtVendor" CssClass="form-control" Enabled="false" />
                                    <asp:HiddenField runat="server" ID="hidVendorID" />
                                </div>
                                <label class="col-md-2 control-label">เลขที่สัญญา</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtContract" CssClass="form-control" Enabled="false" />
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">คลังต้นทางประสงค์เข้ารับงาน</label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTTERMINAL">
                                    </asp:DropDownList>

                                </div>
                                <label class="col-md-2 control-label">รถหมุนเวียน</label>
                                <div class="col-md-3">
                                    <asp:CheckBox Text="" ID="cbCarRecycling" runat="server" />
                                </div>

                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">สถานะรถขนส่ง</label>
                                <div class="col-md-3">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlCONFIRM_STATUS">
                                    </asp:DropDownList>

                                </div>
                                <label class="col-md-2 control-label">สรุปสถานะ</label>
                                <div class="col-md-3">
                                    <asp:RadioButtonList runat="server" ID="rblStatus" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="&nbsp;ทั้งหมด&nbsp;" Value="" Selected="True" />
                                        <asp:ListItem Text="&nbsp;ยืนยัน&nbsp;" Value="1" />
                                        <asp:ListItem Text="&nbsp;ปฏิเสธ&nbsp;" Value="0" />
                                    </asp:RadioButtonList>

                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 control-label">ค้นหาทะเบียนรถ</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtTEXTSEARCH" CssClass="form-control" />

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                        <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                        <asp:Button Text="Export" runat="server" ID="btnExport" class="btn btn-md bth-hover btn-info" OnClick="btnExport_Click" />
                        <asp:Button Text="ตรวจสอบ IVMS" runat="server" ID="btnCheckIVMS" class="btn btn-md bth-hover btn-info" OnClick="btnCheckIVMS_Click" style="background-color:yellow;border-color:antiquewhite;color:black;" />
                        <a id="aConfirmCheckIVMS" runat="server" href="ConfirmTruckDetailCheckIVMS.aspx" target="_blank" class="btn btn-md bth-hover btn-info" style="color:white;">
                            ตรวจสอบ IVMS รายทะเบียน
                        </a>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2"><i class="fa fa-table"></i>ผลการค้นหา จำนวน
                <asp:Label Text="0" runat="server" ID="lblItem" />
                            รายการ <font color="red"> (*สามารถตรวจสอบสถานะรถขนส่งปัจจุบัน โดยกดปุ่ม ตรวจสอบ IVMS ระบบจะแสดงข้อมูลในคอลัมน์สีเหลืองท้ายตาราง)</font></a>
<font c></font>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6 text-right">
                                    <asp:Label Text="จำนวนรถหมุนเวียน {0} คัน" runat="server" ID="lblCSTANBY" ForeColor="Red" />&nbsp;
                                <asp:Label Text="จำนวนรถที่ยืนยัน {0} คัน" runat="server" ID="lblCCONFIRM" ForeColor="Red" />
                                </div>
                            </div>
                            <div class="row">
                                <asp:GridView runat="server" ID="gvData"
                                    Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter GridColorHeaderFont11"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                    HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]" OnPageIndexChanging="gvData_PageIndexChanging" OnRowDataBound="gvData_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ที่">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="ประเภทรถ" DataField="STANBY_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="รอบการขนส่ง" DataField="DELIVERY_PART" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField HeaderText="ทะเบียนหัว">
                                            <ItemTemplate>
                                                <a href="#" runat="server" id="aSHEADREGISTERNO" target="_blank"><%# Eval("SHEADREGISTERNO") %></a> 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="ทะเบียนหาง" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="ประเภท" DataField="TTRUCK_TYPE_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="รถขนส่ง" DataField="CAR_STATUS_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="คลังต้นทาง" HtmlEncode="false" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="เที่ยวที่" HtmlEncode="false" DataField="DELIVERY_NUM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="หมายเหตุ" DataField="REMARK" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="GPS" DataField="IVMS_GPS_STAT_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="กล้อง" DataField="IVMS_MDVR_STAT_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="สถานะ Tracking" DataField="IVMS_STATUS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="การกระจัด(กม.)" HtmlEncode="false" DataField="IVMS_RADIUS_KM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="วันที่อัพเดต" DataField="IVMS_CHECKDATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                        <asp:BoundField HeaderText="การอนุญาต" HtmlEncode="false" DataField="CACTIVE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="ยืนยัน" HtmlEncode="false" DataField="CCONFIRM_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="GPS" DataField="IVMS_GPS_STAT_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                        <asp:BoundField HeaderText="กล้อง" DataField="IVMS_MDVR_STAT_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                        <asp:BoundField HeaderText="สถานะ Tracking" DataField="IVMS_STATUS_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                        <asp:BoundField HeaderText="การกระจัด(กม.)" HtmlEncode="false" DataField="IVMS_RADIUS_KM_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                        <asp:BoundField HeaderText="วันที่อัพเดต" DataField="IVMS_CHECKDATE_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse3" id="acollapse3"><i class="fa fa-table"></i>ผลการค้นหาสรุปสถานะ IVMS จำนวน
                <asp:Label Text="0" runat="server" ID="lblItemIVMSStatus" />
                                    รายการ(เฉพาะรถที่ยืนยัน)</a>

                            </div>
                            <div id="collapse3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvIVMSStatus"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="สถานะ Tracking" DataField="IVMS_STATUS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="IVMS_CHECKDATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(Yes)" DataField="CCONFIRM_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="IVMS_CHECKDATE_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse7" id="acollapse7"><i class="fa fa-table"></i>ผลการค้นหาสรุปสถานะ IVMS ปัจจุบัน จำนวน
                <asp:Label Text="0" runat="server" ID="lblItemIVMSStatusNow" />
                                    รายการ</a>

                            </div>
                            <div id="collapse7" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvIVMSStatusNow"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="สถานะ Tracking" DataField="IVMS_STATUS_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" />
                                                <asp:BoundField HeaderText="จำนวน" DataField="CCONFIRM_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="IVMS_CHECKDATE_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4"><i class="fa fa-table"></i>ผลการค้นหาสรุปสถานะกล้อง & GPS จำนวน
                <asp:Label Text="0" runat="server" ID="lblItemGPS" />
                                    รายการ(เฉพาะรถที่ยืนยัน)</a>

                            </div>
                            <div id="collapse4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvGPS"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="IVMS" DataField="GPS_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(Yes)" DataField="CCONFIRM_YES" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(No)" DataField="CCONFIRM_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="IVMS_CHECKDATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(Yes)" DataField="CCONFIRM_YES_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                                <asp:BoundField HeaderText="จำนวน(No)" DataField="CCONFIRM_NO_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="IVMS_CHECKDATE_NOW" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-BackColor="Yellow" Visible="false" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="row form-group">
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse5" id="acollapse5"><i class="fa fa-table"></i>ผลการค้นหาคลัง จำนวน
                <asp:Label Text="0" runat="server" ID="lblItemTerminal" />
                                    รายการ(เฉพาะรถที่ยืนยัน)</a>

                            </div>
                            <div id="collapse5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvTerminal"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="คลังต้นทางที่รับงาน" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถปกติ(คัน)" DataField="CSTANBY_N" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถหมุนเวียน(คัน)" DataField="CSTANBY" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ยืนยันการใช้งาน(คัน)" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse6" id="acollapse6"><i class="fa fa-table"></i>สรุปการยืนยันรถ TMS
                <asp:Label Text="0" runat="server" ID="lblItemStatus" Visible="false" />
                                    </a>

                            </div>
                            <div id="collapse6" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="row col-md-12">
                                        <asp:GridView runat="server" ID="gvStatus"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeaderCenter "
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                            HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]">
                                            <Columns>
                                                <asp:BoundField HeaderText="สถานะ" DataField="STATUS_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถปกติ(คัน)" DataField="CSTANBY_N" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="รถหมุนเวียน(คัน)" DataField="CSTANBY" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="จำนวน(คัน)" DataField="CCONFIRM" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" Visible="false" />
                                                <asp:BoundField HeaderText="วันที่อัพเดตล่าสุด" DataField="DCREATE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:HiddenField runat="server" ID="hidSCONTRACTID" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

