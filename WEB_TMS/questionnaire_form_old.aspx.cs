﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;


public partial class questionnaire_form : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string PageFileUpload = "AUDIT_FILE";
    #region " Prop "


    string PagePermission = "questionnaire.aspx";
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }

    public List<RadioValueList> DTRdo1
    {
        get { return (List<RadioValueList>)ViewState[this.ToString() + "DTRdo1"]; }
        set { ViewState[this.ToString() + "DTRdo1"] = value; }
    }
    public Questionnaire QuestionnaireData
    {
        get { return (Questionnaire)ViewState[this.ToString() + "QuestionnaireData"]; }
        set { ViewState[this.ToString() + "QuestionnaireData"] = value; }
    }
    public List<HeaderQuestionnaire> Header1
    {
        get { return (List<HeaderQuestionnaire>)ViewState[this.ToString() + "DTHeader1"]; }
        set { ViewState[this.ToString() + "DTHeader1"] = value; }
    }
    /// <summary>
    /// Form ID สำหรับแก้ไข
    /// </summary>
    public string sNTYPEVISITFORMID1
    {
        get { return ViewState[this.ToString() + "sNTYPEVISITFORMID1"].ToString(); }
        set { ViewState[this.ToString() + "sNTYPEVISITFORMID1"] = value; }
    }
    public string sNTYPEVISITFORM_Y1
    {
        get { return ViewState[this.ToString() + "sNTYPEVISITFORM_Y1"].ToString(); }
        set { ViewState[this.ToString() + "sNTYPEVISITFORM_Y1"] = value; }
    }
    public string VendorID
    {
        get { return ViewState[this.ToString() + "VendorID"].ToString(); }
        set { ViewState[this.ToString() + "VendorID"] = value; }
    }
    private int? QDocID
    {
        get { return int.Parse(ViewState["QDocID"].ToString()); }
        set { ViewState["QDocID"] = value; }
    }

    #endregion " Prop "
    public void GotoDefault()
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>");

    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }
        string _err = string.Empty;
        Page_Status = new QuestionnaireBLL().CheckPermission(ref _err, Session["UserID"].ToString(), PagePermission);
        if (Page_Status == USER_PERMISSION.DISABLE)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;
        }
        else if (Page_Status == USER_PERMISSION.VIEW)
        {
            btnSave.Enabled = false;
            btnSaveTop.Enabled = false;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            CheckPermission();
            LoadFormName();
            BindVendor();
            sNTYPEVISITFORMID1 = Session["sNTYPEVISITFORMID"] == null ? "" : Session["sNTYPEVISITFORMID"].ToString();
            QDocID = Session["NQUESTIONNAIREID"] == null ? 0 : int.Parse(Session["NQUESTIONNAIREID"].ToString());
            QuestionnaireData = new Questionnaire();
            if (QDocID > 0) //แก้ไข
            {
                Session["NQUESTIONNAIREID"] = 0;
                string _err = string.Empty;
                QuestionnaireData = new QuestionnaireBLL().GetQuestionnaire(ref _err, QDocID);
                GetAs();
                GetBy();
                BindEdit();
            }
            else
            {
                GetAs();
                GetBy();
                HideAllTblForm();
            }
            InitialUpload();
        }

        RegisterPostDownload();
    }

    public void BindEdit()
    {
        dteDcheck.Text = QuestionnaireData.SDCHECK;
        ListItem selectedListItem = cmbForm.Items.FindByValue(QuestionnaireData.YEAR + " : " + QuestionnaireData.NTYPEVISITFORMID.ToString());
        if (selectedListItem != null)
        {
            selectedListItem.Selected = true;
            cmbForm_SelectedIndexChanged(null, null);
        }
        else
        {
            ContentPlaceHolder mainContent = (ContentPlaceHolder)this.Master.FindControl("cph_Main");
            foreach (var c in mainContent.Controls)
            {
                if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                {
                    Button btn = (Button)c;
                    btn.Enabled = false;
                }
                else if (c.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                {
                    TextBox txt = (TextBox)c;
                    txt.Enabled = false;
                }
                else if (c.GetType().ToString().Equals("System.Web.UI.UpdatePanel"))
                {
                    UpdatePanel upl = (UpdatePanel)c;
                    foreach (var u in upl.Controls)
                    {
                        if (c.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                        {
                            Button btn = (Button)c;
                            btn.Enabled = false;
                        }
                        else if (c.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                        {
                            TextBox txt = (TextBox)c;
                            txt.Enabled = false;
                        }
                    }
                }
            }
            btnSaveTop.Enabled = false;
            //Form Not Active
            string _err = string.Empty;
            string formName = new QuestionnaireBLL().GetFormNameNOTAcitveForView(ref _err, QuestionnaireData.NTYPEVISITFORMID.ToString());
            if (string.IsNullOrEmpty(formName))
            {
                cmbForm.Items.Insert(1, new ListItem() { Text = "ไม่พบข้อมูลแบบทดสอบในระบบ", Value = QuestionnaireData.YEAR + " : " + QuestionnaireData.NTYPEVISITFORMID, Selected = true });
                cmbForm.Enabled = false;
                alertFail("ไม่พบข้อูลแบบทดสอบใระบบ");
            }
            else
            {
                cmbForm.Items.Insert(1, new ListItem() { Text = QuestionnaireData.YEAR + " : " + formName, Value = QuestionnaireData.YEAR + " : " + QuestionnaireData.NTYPEVISITFORMID, Selected = true });
                cmbForm.Enabled = false;
                cmbForm_SelectedIndexChanged(null, null);
            }
        }

        if (!string.IsNullOrEmpty(QuestionnaireData.SVENDORID))
        {
            ListItem selectedListItemV = cmbVendor.Items.FindByValue(QuestionnaireData.SVENDORID);
            if (selectedListItemV != null)
            {
                selectedListItemV.Selected = true;
            }
        }

        txtAddress.Text = QuestionnaireData.SADDRESS;
        txtDetail.Text = QuestionnaireData.SREMARK;
    }

    public void HideAllTblForm()
    {
        for (var i = 1; i <= 10; i++)
        {
            ContentPlaceHolder mainContent = (ContentPlaceHolder)this.Master.FindControl("cph_Main");
            mainContent.FindControl("tbl" + i.ToString()).Visible = false;
        }
    }
    public void LoadFormName()
    {
        string _err = string.Empty;
        DropDownListHelper.BindDropDownList(ref cmbForm, new QuestionnaireBLL().GetFormNameForInput(ref _err), "VAL", "FORMNAME", true);
    }
    public void GetRadioList(int NTYPEVISITFORMID)
    {
        string _err = string.Empty;
        DTRdo1 = new QuestionnaireBLL().GetRadio(ref _err, int.Parse(NTYPEVISITFORMID.ToString()));
        if (DTRdo1.Count > 0)
        {
            CheckAllRdo.Items.Clear();
            foreach (var itm in DTRdo1)
            {
                CheckAllRdo.Items.Add(new ListItem("&nbsp;" + itm.STYPEVISITLISTNAME.ToString() + "&nbsp;&nbsp;", itm.NTYPEVISITLISTSCORE.ToString()));
            }
        }
    }
    public void LoadHeader(int NTYPEVISITFORMID, int HeaderID)
    {
        try
        {
            string _err = string.Empty;
            if (HeaderID == 1)
            {
                Header1 = new QuestionnaireBLL().GetHeaderQ(ref _err, NTYPEVISITFORMID);
                if (Header1.Count > 0)
                {
                    tblButtonHead.Visible = true;
                }
            }
            InitHeaderForm(HeaderID);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    /// <summary>
    /// Get All GridView ( Max 10)
    /// </summary>
    /// <param name="Tab"></param>
    public void InitHeaderForm(int Tab)
    {
        ContentPlaceHolder mainContent = (ContentPlaceHolder)this.Master.FindControl("cph_Main");

        string _err = string.Empty;
        if (Tab == 1)
        {
            /// Get All GridView ( Max 10)
            int indexHead = 1;
            foreach (HeaderQuestionnaire itm in Header1)
            {
                Label lblHd = mainContent.FindControl("lblHeaderTab" + indexHead.ToString()) as Label;
                if (lblHd != null) lblHd.Text = itm.SGROUPNAME;
                GridView grv = mainContent.FindControl("GridView" + indexHead.ToString()) as GridView;
                if (grv != null)
                {
                    grv.DataSource = new QuestionnaireBLL().GetDetailFormQ(ref _err, itm.NGROUPID);
                    grv.DataBind();
                }

                Table Table = mainContent.FindControl("tbl" + indexHead.ToString()) as Table;
                Table.Visible = true;
                indexHead++;
            }

            for (int i = 10; i > Header1.Count; i--)
            {
                Table Table = mainContent.FindControl("tbl" + i.ToString()) as Table;
                Table.Visible = false;
            }
        }
    }
    public void BindGrv(int index)
    {

    }
    public void BindVendor()
    {
        DropDownListHelper.BindDropDownList(ref cmbVendor, ComplainBLL.Instance.VendorSelectBLL(), "SVENDORID", "SABBREVIATION", true);
    }
    public void GetBy()
    {
        string _err = string.Empty;
        if (QDocID == 0) QuestionnaireData.LST_TQUESTIONNAIRE_BY = new QuestionnaireBLL().GetQBY(ref _err, QDocID);

        grvBY.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_BY;
        grvBY.DataBind();
    }
    public void GetAs()
    {
        string _err = string.Empty;
        if (QDocID == 0) QuestionnaireData.LST_TQUESTIONNAIRE_AS = new QuestionnaireBLL().GetQAS(ref _err, QDocID);

        grvAS.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_AS;
        grvAS.DataBind();
    }

    public void GetEditData()
    {
        try
        {
            // Pedding Dup row
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    /// <summary>
    /// ยังไม่มี Condition
    /// </summary>
    public void SetAsView()
    {

    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void cmbForm_SelectedIndexChanged(object sender, EventArgs e)
    {

        sNTYPEVISITFORMID1 = cmbForm.SelectedItem.Value.ToString().Split(':').Last().Trim().ToString();
        sNTYPEVISITFORM_Y1 = cmbForm.SelectedItem.Value.ToString().Split(':').First().Trim().ToString();
        if (!string.IsNullOrEmpty(sNTYPEVISITFORMID1))
        {
            QuestionnaireData.YEAR = sNTYPEVISITFORM_Y1;
            QuestionnaireData.NTYPEVISITFORMID = int.Parse(sNTYPEVISITFORMID1);
            GetRadioList(int.Parse(sNTYPEVISITFORMID1));
            LoadHeader(int.Parse(sNTYPEVISITFORMID1), 1);

        }
        else
        { // Reset
            GetRadioList(0);
            LoadHeader(0, 1);
        }

    }
    protected void btnClose_Click(object sender, EventArgs erblWEIGHT_SelectedIndexChanged)
    {
        GotoMain();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            int? refID = 0;
            if (!ValidateSave()) return;
            double? TotalPoint = 0;
            if (QDocID > 0)
            {
                #region " Edit "

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    using (OracleTransaction tran = con.BeginTransaction())
                    {

                        try
                        {
                            string _strDel = "DELETE FROM TQUESTIONNAIRE_BY WHERE NQUESTIONNAIREID = " + QDocID;
                            using (OracleCommand comDel = new OracleCommand(_strDel, con)) { comDel.Transaction = tran; comDel.ExecuteNonQuery(); }

                            string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, SNAME,  DCREATE, SCREATE, NINDEX) VALUES ( :NQUESTIONNAIREID  ,:SNAME , SYSDATE , :SCREATE,:NINDEX  )";

                            QuestionnaireData.LST_TQUESTIONNAIRE_BY.OrderBy(x => x.NINDEX);
                            int? index = 0;
                            foreach (var itm in QuestionnaireData.LST_TQUESTIONNAIRE_BY)
                            {

                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            _strDel = "DELETE FROM TQUESTIONNAIRE_AS WHERE NQUESTIONNAIREID = " + QDocID;
                            using (OracleCommand comDel = new OracleCommand(_strDel, con)) { comDel.Transaction = tran; comDel.ExecuteNonQuery(); }

                            string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS (NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE, NINDEX) VALUES (:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE,:NINDEX)";

                            QuestionnaireData.LST_TQUESTIONNAIRE_AS.OrderBy(x => x.NINDEX);
                            index = 0;
                            foreach (var itm in QuestionnaireData.LST_TQUESTIONNAIRE_AS)
                            {

                                using (OracleCommand com1 = new OracleCommand(strsql2, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            #region " File "

                            var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);
                            if (delFile != null)
                            {
                                string _strCondition = string.Join("','", DTFile.Select(r => r.ID));
                                string _whereCondition = string.Empty;
                                if (!string.IsNullOrEmpty(_strCondition))
                                {
                                    _whereCondition = string.Format(" ID NOT IN ('{0}') and ", _strCondition);
                                }
                                using (OracleCommand comDel1 = new OracleCommand(string.Format(@"UPDATE F_UPLOAD SET ISACTIVE = 0 WHERE {0} ref_int = {1} and ref_str = '{2}'", _whereCondition, QDocID, PageFileUpload), con))
                                {
                                    comDel1.Transaction = tran;
                                    comDel1.Parameters.Clear();
                                    comDel1.ExecuteNonQuery();
                                }
                            }

                            foreach (var item in DTFile)
                            {
                                if (item.ID == 0)
                                {
                                    using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                    {
                                        com1.Transaction = tran;
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                        com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                        com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                        com1.Parameters.Add(":REF_INT", OracleType.Number).Value = QDocID;
                                        com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                        com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                        com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = Session["UserID"];
                                        com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }

                            #endregion " File "

                            using (OracleCommand comDel1 = new OracleCommand("DELETE FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = " + QDocID, con)) { comDel1.Transaction = tran; comDel1.ExecuteNonQuery(); }

                            string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,  NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";
                            for (var i = 1; i <= 10; i++)
                            {
                                string ControlName = String.Format("GridView{0}", i.ToString());
                                ContentPlaceHolder mainContent = (ContentPlaceHolder)this.Master.FindControl("cph_Main");
                                GridView grv = mainContent.FindControl(ControlName) as GridView;
                                if (grv == null) continue;
                                foreach (GridViewRow row in grv.Rows)
                                {
                                    if (row.RowType != DataControlRowType.DataRow) continue;
                                    double? NVALUE = 0.0;
                                    double? NVALUEITEM = 0.0;
                                    string Remark = string.Empty;
                                    int? NVISITFORMID = int.Parse(grv.DataKeys[row.RowIndex]["NVISITFORMID"].ToString());
                                    int? NGROUPID = int.Parse(grv.DataKeys[row.RowIndex]["NGROUPID"].ToString());
                                    int? NTYPEVISITFORMID = int.Parse(grv.DataKeys[row.RowIndex]["NTYPEVISITFORMID"].ToString());
                                    RadioButtonList RdoListPoint = row.Cells[1].FindControl("RdoListPoint") as RadioButtonList;
                                    if (RdoListPoint != null) { NVALUEITEM = double.Parse(RdoListPoint.SelectedValue); }
                                    HiddenField hdd = row.Cells[1].FindControl("hdWEIGHT") as HiddenField;
                                    if (hdd != null) { NVALUE = double.Parse(hdd.Value); }
                                    TextBox txtRemark = row.Cells[2].FindControl("txtRemark") as TextBox;
                                    if (txtRemark != null) { Remark = txtRemark.Text; }
                                    using (OracleCommand com3 = new OracleCommand(strsql4, con))
                                    {
                                        com3.Transaction = tran;
                                        TotalPoint += (NVALUE * NVALUEITEM); // Recalculate ; 
                                        com3.Parameters.Clear();
                                        com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                        com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = NVISITFORMID.ToString();
                                        com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = NGROUPID.ToString();
                                        com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = NTYPEVISITFORMID.ToString();
                                        com3.Parameters.Add(":NVALUE", OracleType.Number).Value = NVALUE;
                                        com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = NVALUEITEM;
                                        com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = Remark;
                                        com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                        com3.ExecuteNonQuery();
                                    }
                                }
                            }
                            string strsql = @"UPDATE TQUESTIONNAIRE SET SVENDORID =:SVENDORID, DCHECK=to_date(:DCHECK,'DD/MM/YYYY'),SADDRESS =:SADDRESS,SREMARK = :SREMARK,DUPDATE= SYSDATE,SUPDATE  =:SUPDATE,NVALUE   =:NVALUE WHERE  NQUESTIONNAIREID =:NQUESTIONNAIREID and NTYPEVISITFORMID=:NTYPEVISITFORMID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cmbVendor.SelectedValue;
                                com.Parameters.Add(":DCHECK", OracleType.VarChar).Value = dteDcheck.Text + "";
                                com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NVALUE", OracleType.Number).Value = TotalPoint;
                                com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = QDocID;
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = QuestionnaireData.NTYPEVISITFORMID;
                                com.ExecuteNonQuery();
                            }
                            tran.Commit();
                            alertSuccess(" บันทึกข้อมูลเรียนร้อย ", "questionnaire.aspx");
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            alertFail(ex.Message);
                        }
                    }

                }
                #endregion " Edit "
            }
            else
            {
                #region " Add "

                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    using (OracleTransaction tran = con.BeginTransaction())
                    {
                        try
                        {
                            string _err = string.Empty;
                            refID = new QuestionnaireBLL().GetDocID(ref _err);
                            string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, SNAME,  DCREATE, SCREATE) VALUES ( :NQUESTIONNAIREID  ,:SNAME , SYSDATE , :SCREATE ,:NINDEX )";

                            QuestionnaireData.LST_TQUESTIONNAIRE_BY.OrderBy(x => x.NINDEX);
                            foreach (var itm in QuestionnaireData.LST_TQUESTIONNAIRE_BY)
                            {
                                int index = 0;
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS (NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE) VALUES (:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE,:NINDEX)";

                            QuestionnaireData.LST_TQUESTIONNAIRE_AS.OrderBy(x => x.NINDEX);
                            foreach (var itm in QuestionnaireData.LST_TQUESTIONNAIRE_AS)
                            {
                                int index = 0;
                                using (OracleCommand com1 = new OracleCommand(strsql2, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = itm.SNAME;
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                    com1.Parameters.Add(":NINDEX", OracleType.Number).Value = index;
                                    com1.ExecuteNonQuery();
                                }
                                index++;
                            }

                            #region " File "

                            var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);

                            foreach (var item in DTFile)
                            {
                                if (item.ID == 0)
                                {
                                    using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                    {
                                        com1.Transaction = tran;
                                        com1.Parameters.Clear();
                                        com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                        com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                        com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                        com1.Parameters.Add(":REF_INT", OracleType.Number).Value = refID;
                                        com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                        com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                        com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = Session["UserID"];
                                        com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                        com1.ExecuteNonQuery();
                                    }
                                }
                            }

                            #endregion " File "

                            string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,  NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";
                            for (var i = 1; i <= 10; i++)
                            {
                                string ControlName = String.Format("GridView{0}", i.ToString());
                                ContentPlaceHolder mainContent = (ContentPlaceHolder)this.Master.FindControl("cph_Main");
                                GridView grv = mainContent.FindControl(ControlName) as GridView;
                                if (grv == null) continue;
                                foreach (GridViewRow row in grv.Rows)
                                {
                                    if (row.RowType != DataControlRowType.DataRow) continue;
                                    double? NVALUE = 0.0;
                                    double? NVALUEITEM = 0.0;
                                    string Remark = string.Empty;
                                    int? NVISITFORMID = int.Parse(grv.DataKeys[row.RowIndex]["NVISITFORMID"].ToString());
                                    int? NGROUPID = int.Parse(grv.DataKeys[row.RowIndex]["NGROUPID"].ToString());
                                    int? NTYPEVISITFORMID = int.Parse(grv.DataKeys[row.RowIndex]["NTYPEVISITFORMID"].ToString());
                                    RadioButtonList RdoListPoint = row.Cells[1].FindControl("RdoListPoint") as RadioButtonList;
                                    if (RdoListPoint != null) { NVALUEITEM = double.Parse(RdoListPoint.SelectedValue); }
                                    HiddenField hdd = row.Cells[1].FindControl("hdWEIGHT") as HiddenField;
                                    if (hdd != null) { NVALUE = double.Parse(hdd.Value); }
                                    TextBox txtRemark = row.Cells[2].FindControl("txtRemark") as TextBox;
                                    if (txtRemark != null) { Remark = txtRemark.Text; }
                                    using (OracleCommand com3 = new OracleCommand(strsql4, con))
                                    {
                                        com3.Transaction = tran;
                                        TotalPoint += (NVALUE * NVALUEITEM); // Recalculate ; 
                                        com3.Parameters.Clear();
                                        com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                        com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = NVISITFORMID.ToString();
                                        com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = NGROUPID.ToString();
                                        com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = NTYPEVISITFORMID.ToString();
                                        com3.Parameters.Add(":NVALUE", OracleType.Number).Value = NVALUE;
                                        com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = NVALUEITEM;
                                        com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = Remark;

                                        com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                        com3.ExecuteNonQuery();
                                    }
                                }
                            }

                            string strsql = @"INSERT INTO TQUESTIONNAIRE ( SVENDORID, DCHECK, SADDRESS,SREMARK,NVALUE,DUPDATE,SUPDATE,NQUESTIONNAIREID,NYEAR,DCREATE,NNO,NTYPEVISITFORMID ) VALUES( :SVENDORID , to_date(:DCHECK,'DD/MM/YYYY'), :SADDRESS, :SREMARK,:NVALUE , SYSDATE, :SUPDATE,  :NQUESTIONNAIREID ,:YEAR,SYSDATE,1,:NTYPEVISITFORMID )";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Transaction = tran;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cmbVendor.SelectedValue;
                                com.Parameters.Add(":DCHECK", OracleType.VarChar).Value = dteDcheck.Text;
                                com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                                com.Parameters.Add(":NVALUE", OracleType.VarChar).Value = TotalPoint.ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = refID;
                                com.Parameters.Add(":YEAR", OracleType.VarChar).Value = QuestionnaireData.YEAR.ToString();
                                com.Parameters.Add(":NTYPEVISITFORMID", OracleType.VarChar).Value = QuestionnaireData.NTYPEVISITFORMID;
                                com.ExecuteNonQuery();
                            }

                            tran.Commit();
                            alertSuccess(" บันทึกข้อมูลเรียนร้อย ", "questionnaire.aspx");
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            alertFail(ex.Message);
                        }
                    }

                }
                #endregion " Add "
            } 
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }

    }

    public void GotoMain()
    {
        Response.Redirect("questionnaire.aspx");
    }
    public void GrvGet()
    {

    }
    public bool ValidateSave()
    {
        bool result = true;
        try
        {
            if (string.IsNullOrEmpty(dteDcheck.Text))
            {
                alertFail("กรุณาระบุวันที่ตรวจประเมิน");
                return false;
            }

            if (cmbVendor.SelectedIndex == 0)
            {
                alertFail("กรุณาระบุบริษัทผู้ประกอบการขนส่ง");
                return false;
            }
            if (cmbForm.SelectedIndex == 0)
            {
                alertFail("กรุณาระบุแบบฟอร์ม");
                return false;
            }

            string _err = string.Empty;
            if (new QuestionnaireBLL().IsDuplicateQuestion(ref _err, QuestionnaireData.YEAR, cmbVendor.SelectedValue.ToString(), QDocID))
            {
                alertFail("ผู้บริษัทผู้ประกอบการขนส่งที่ระบุ ในปีที่เลือกมีการทำแบบสอบถามเรียบร้อยแล้ว ไม่สามารถรอกซ้ำได้");
                return false;
            }
        }
        catch (Exception ex)
        {
            result = false;
            alertFail(ex.Message);
        }
        return result;
    }

    #region " Upload File New "

    public List<F_UPLOAD> DTFile
    {
        get
        {
            if ((List<F_UPLOAD>)ViewState["dtUpload"] != null)
                return (List<F_UPLOAD>)ViewState["dtUpload"];
            else
                return null;
        }
        set { ViewState["dtUpload"] = value; }
    }
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DTFile.RemoveAt(e.RowIndex);
            dgvUploadFile.DataSource = DTFile;
            dgvUploadFile.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gv = (GridView)sender;
        string FullPath = gv.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string path = Path.GetDirectoryName(FullPath);
        string FileName = Path.GetFileName(FullPath);

        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(path + "\\" + FileName);
        Response.End();
    }

    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");

                ImageButton imgView;
                imgView = (ImageButton)e.Row.Cells[0].FindControl("imgView");

                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(imgView);


            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    public void RegisterPostDownload()
    {
        foreach (GridViewRow row in dgvUploadFile.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow) continue;

            ImageButton imgView = row.Cells[0].FindControl("imgView") as ImageButton;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(imgView);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Quesrionnaire" + "\\" + QDocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("AUDIT_FILE");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
        DTFile = new List<F_UPLOAD>();
        if (QDocID > 0)
        {
            DTFile = QuestionnaireData.LST_FILE_UPLOAD;
            dgvUploadFile.DataSource = DTFile;
            dgvUploadFile.DataBind();
        }
    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                DTFile.Add(new F_UPLOAD()
                {
                    FILENAME_SYSTEM = System.IO.Path.GetFileName(Path + "\\" + FileNameSystem),
                    FULLPATH = Path + "\\" + FileNameSystem,
                    ID = 0,
                    FILENAME_USER = FileNameUser,
                    UPLOAD_ID = int.Parse(cboUploadType.SelectedValue),
                    UPLOAD_NAME = cboUploadType.SelectedItem.Text
                });

                dgvUploadFile.DataSource = DTFile;
                dgvUploadFile.DataBind();
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            foreach (var item in DTFile)
            {
                data.Add(decimal.Parse(item.UPLOAD_ID.ToString()));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion " Upload File New "

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int? _keyNGROUPID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NGROUPID").ToString());
            int? _keyNTYPEVISITFORMID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NTYPEVISITFORMID").ToString());
            int? _keyNVISITFORMID = int.Parse(DataBinder.Eval(e.Row.DataItem, "NVISITFORMID").ToString());
            string expression = string.Format(" NGROUPID={0} and NTYPEVISITFORMID={1} and NVISITFORMID={2}", _keyNGROUPID, _keyNTYPEVISITFORMID, _keyNVISITFORMID);
            DataRow[] foundRows = null;
            if (QuestionnaireData.DT_QUESTIONNAIRELIST != null)
            {
                foundRows = QuestionnaireData.DT_QUESTIONNAIRELIST.Select(expression);
            }


            RadioButtonList RdoListPoint = e.Row.Cells[1].FindControl("RdoListPoint") as RadioButtonList;
            if (RdoListPoint != null)
            {
                if (Page_Status == USER_PERMISSION.VIEW) RdoListPoint.Enabled = false;
                foreach (RadioValueList itm in DTRdo1)
                {
                    RdoListPoint.Items.Add(new ListItem("&nbsp;" + itm.STYPEVISITLISTNAME, int.Parse(itm.NTYPEVISITLISTSCORE.ToString()).ToString()));
                }

                if (QDocID > 0)
                {
                    if (foundRows.Length > 0)
                    {
                        string radioVal = foundRows[0]["NVALUEITEM"].ToString();
                        if (!String.IsNullOrEmpty(radioVal))
                        {
                            ListItem chk = RdoListPoint.Items.FindByValue(radioVal);
                            if (chk != null) chk.Selected = true;
                        }

                        string remark = foundRows[0]["SREMARK"].ToString();
                    }
                }
                else
                {
                    RdoListPoint.ClearSelection();
                }
            }

            Label lblSVISITFORMNAME = e.Row.Cells[1].FindControl("lblSVISITFORMNAME") as Label;
            if (lblSVISITFORMNAME != null)
            {
                string _str = DataBinder.Eval(e.Row.DataItem, "SVISITFORMNAME").ToString().Replace("\n", "<br />");
                lblSVISITFORMNAME.Text = _str;
            }

            TextBox txtRemark = e.Row.Cells[1].FindControl("txtRemark") as TextBox;
            if ((txtRemark != null) && (foundRows != null) && (QDocID > 0))
            {
                txtRemark.Text = foundRows[0]["SREMARK"].ToString();
                if (Page_Status == USER_PERMISSION.VIEW) txtRemark.Enabled = false;
            }
        }
    }
    protected void grvBY_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {

    }
    protected void grvAS_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {

    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void btnAddd_Click(object sender, EventArgs e)
    {
        int? index = 0;
        if (string.IsNullOrEmpty(txtBy.Text.Trim()))
        {
            alertFail("กรุณาระบุรายชื่อคณะผู้ตรวจ ");
            return;
        }

        if (QuestionnaireData.LST_TQUESTIONNAIRE_BY.Count > 0)
        {
            index = QuestionnaireData.LST_TQUESTIONNAIRE_BY.Max(x => x.NINDEX) + 1;
        }

        QuestionnaireData.LST_TQUESTIONNAIRE_BY.Add(new TQUESTIONNAIRE_BY()
        {
            NQUESTIONNAIREID = QDocID,
            NID = index,
            SNAME = txtBy.Text
        });
        txtBy.Text = string.Empty;
        grvBY.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_BY;
        grvBY.DataBind();
    }
    protected void btnAddAS_Click(object sender, EventArgs e)
    {
        int? index = 0;
        if (string.IsNullOrEmpty(txtAs.Text.Trim()))
        {
            alertFail("กรุณาระบุรายชื่อผู้รับการตรวจ ");
            return;
        }
        if (QuestionnaireData.LST_TQUESTIONNAIRE_AS.Count > 0)
        {
            index = QuestionnaireData.LST_TQUESTIONNAIRE_AS.Max(x => x.NINDEX) + 1;
        }

        QuestionnaireData.LST_TQUESTIONNAIRE_AS.Add(new TQUESTIONNAIRE_AS()
        {
            NQUESTIONNAIREID = QDocID,
            NID = index,
            SNAME = txtAs.Text
        });

        txtAs.Text = string.Empty;
        grvAS.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_AS;
        grvAS.DataBind();
    }
    protected void grvBY_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            if (QuestionnaireData.LST_TQUESTIONNAIRE_BY == null) return;
            if (QuestionnaireData.LST_TQUESTIONNAIRE_BY.Count > 0)
            {
                int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
                QuestionnaireData.LST_TQUESTIONNAIRE_BY.RemoveAt(Convert.ToInt32(rowIndex));
                grvBY.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_BY;
                grvBY.DataBind();
            }
        }
    }
    protected void grvAS_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            if (QuestionnaireData.LST_TQUESTIONNAIRE_AS == null) return;
            if (QuestionnaireData.LST_TQUESTIONNAIRE_AS.Count > 0)
            {
                int? rowIndex = Utility.ApplicationHelpers.ConvertToInt(e.CommandArgument.ToString());
                QuestionnaireData.LST_TQUESTIONNAIRE_AS.RemoveAt(Convert.ToInt32(rowIndex));
                grvAS.DataSource = QuestionnaireData.LST_TQUESTIONNAIRE_AS;
                grvAS.DataBind();
            }
        }
    }
    protected void ScriptManager1_Load(object sender, EventArgs e)
    {

    }
}