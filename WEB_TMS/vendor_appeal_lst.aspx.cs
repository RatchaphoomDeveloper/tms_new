﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using System.IO;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTabControl;
using System.Configuration;
using TMS_BLL.Transaction.Appeal;
using EmailHelper;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Master;
using System.Web.UI.HtmlControls;
using TMS_BLL.Transaction.Accident;
using System.Web.Security;

public partial class vendor_appeal_lst : PageBase
{
    #region + View State +
    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }
    #endregion

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/Appeal/Temp/{0}/{2}/{1}/";
    const int ThumbnailSize = 100;
    int defInt;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            #region Check Permission
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //getOldAuthenticateCode();  //ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน             
            #endregion

            if (Session["CGROUP"] + string.Empty != ConfigValue.UserGroup1.ToString())
            {
                Response.Redirect("~/admin_suppliant_lst.aspx"); //หน้านี้สำหรับ Cgroup ที่ไม่ใช่ 0
                return;
            }

            //dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

            dteStart.Text = DateTime.Now.Date.AddMonths(-1).ToString("dd/MM/yyyy");
            dteEnd.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");


            string str = Request.QueryString["str"], cType = "1";
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);

                foreach (string QryString in strQuery)
                {
                    //Response.Write(QryString + "<br />");
                }
                hideAppealID.Text = "" + strQuery[0];
                txtSearch.Text = "" + strQuery[1];
                dteStart.Text = dteEnd.Text = Convert.ToDateTime("" + strQuery[3]).ToString("dd/MM/yyyy");
                switch ("" + strQuery[4])
                {
                    case "011":
                    case "030":
                    case "040":
                    case "050":
                    case "060":
                        cType = "1";
                        break;
                    case "090":
                        cType = "2";
                        break;
                    case "080":
                        cType = "3";
                        break;
                    case "010":
                        cType = "4";
                        break;
                    case "020":
                        cType = "5";
                        break;
                }
                cboType.Value = cType;

            }

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            var dt = new List<dt>();
            Session["dt"] = dt;

            LogUser("4", "R", "เปิดดูข้อมูลหน้า ยื่นอุทธรณ์", "");

        }

    }

    protected void getOldAuthenticateCode()
    {
        bool chkurl = false;
        Session["CheckPermission"] = null;
        string AddEdit = "";

        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == "4")
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            chkurl = true;

                            break;

                        case "2":
                            chkurl = true;
                            AddEdit = "1";
                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        if (chkurl == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        Session["CheckPermission"] = AddEdit;
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (Session["CGROUP"] + string.Empty == "0")
        {
            BindData();

            if (Session["AppealDoc"] != null)
            {
                DocID = Session["AppealDoc"].ToString();
                Session.Remove("AppealDoc");

                txtSearch.Visible = false;
                dteStart.Visible = false;
                dteEnd.Visible = false;
                cboStatus.Visible = false;
                cboType.Visible = false;
                btnSearch.Visible = false;
                txtSearch.Text = DocID;
            }

            if (!string.Equals(DocID, string.Empty))
            {
                this.GetDataComplain();
            }

            if (Session["AppealAccID"] != null)
            {
                DocID = Session["AppealAccID"].ToString();
                Session.Remove("AppealAccID");

                txtSearch.Visible = false;
                dteStart.Visible = false;
                dteEnd.Visible = false;
                cboStatus.Visible = false;
                cboType.Visible = false;
                btnSearch.Visible = false;
                txtSearch.Text = DocID;
            }

            if (!string.Equals(DocID, string.Empty))
            {
                this.GetDataAccident();
            }
        }
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "Search":
                //Cache.Remove(sds.CacheKeyDependency);
                //Cache[sds.CacheKeyDependency] = new object();

                break;
            case "download":
                int DownloadIndex = int.Parse(e.Parameter.Split(';')[1]);
                ASPxPageControl Page = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                ASPxGridView dgvUpload = (ASPxGridView)Page.FindControl("dgvUpload");

                dynamic rowData = dgvUpload.GetRowValues(DownloadIndex, "FULLPATH");
                Session["Appeal_Download"] = rowData[0];

                xcpn.JSProperties["cpRedirectOpen"] = "DownloadFile.aspx";
                break;
            case "edit":

                int Index = int.Parse(e.Parameter.Split(';')[1]);

                gvw.StartEdit(Index);
                ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                ASPxMemo txtAppealDetailE = (ASPxMemo)PageControl1.FindControl("txtAppealDetail");
                ASPxButton btnConfirm = (ASPxButton)PageControl1.FindControl("btnConfirm");

                ASPxRadioButtonList rblStatus = (ASPxRadioButtonList)PageControl1.FindControl("rblStatus");
                ASPxMemo txtData = (ASPxMemo)PageControl1.FindControl("txtData");
                ASPxMemo txtWaitDocument = (ASPxMemo)PageControl1.FindControl("txtWaitDocument");
                HtmlControl div = (HtmlControl)PageControl1.FindControl("divAttach");

                string id = gvw.GetRowValues(Index, "SAPPEALID") + "";

                string CHKSTATUS = gvw.GetRowValues(Index, "CHKSTATUS") + "";

                txtType.Text = gvw.GetRowValues(Index, "SPROCESS") + "";
                txtDocumentOther.Text = gvw.GetRowValues(Index, "CHKSTATUS") + "";

                DataTable dtDetail = CommonFunction.Get_Data(sql, "SELECT SSENTENCER,CSENTENCE,SSENTENCETEXT,SSENTENCERTEXT,SAPPEALTEXT,SWAITDOCUMENT FROM TAPPEAL ap WHERE SAPPEALID = '" + id + "'");

                if (CHKSTATUS == "F")
                {
                    btnConfirm.Visible = false;
                    if (dtDetail.Rows.Count > 0)
                    {
                        txtAppealDetailE.Text = dtDetail.Rows[0]["SAPPEALTEXT"] + "";
                        txtAppealDetailE.ClientEnabled = false;
                    }

                    listFileName(id, false);
                    visibleTabPage();

                }
                else if (CHKSTATUS == "I")
                {
                    btnConfirm.Visible = false;

                    if (dtDetail.Rows.Count > 0)
                    {
                        txtAppealDetailE.Text = dtDetail.Rows[0]["SAPPEALTEXT"] + "";
                        txtAppealDetailE.ClientEnabled = false;
                        txtWaitDocument.Text = dtDetail.Rows[0]["SWAITDOCUMENT"] + "";
                    }

                    listFileName(id, false);
                    visibleTabPage();
                }
                else
                {
                    VisibleControlUpload();
                }

                this.GetFile(id);
                break;

            case "SaveFileMore":
                if (CanWrite)
                //if ("" + Session["CheckPermission"] == "1") //ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน     
                {
                    int IndexM = int.Parse(e.Parameter.Split(';')[1]);
                    ASPxPageControl PageControlM = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                    dynamic DATAM = gvw.GetRowValues(IndexM, "CHKSTATUS", "SAPPEALID");
                    //dynamic DATA = gvw.GetRowValues(IndexM, "SPROCESS", "STOPICID", "SHEADREGISTERNO", "STRAILERREGISTERNO", "SUMPOINT", "DCHECK", "ISSUETEXT", "OUTBOUNDNO", "AREAACCIDENT", "NREDUCEID", "SCONTRACTID", "SCHECKID", "CHKSTATUS", "SAPPEALID", "DCREATE", "SCUSTOMERID", "SVERSION", "SCHECKLISTID", "SVERSIONLIST", "STYPECHECKLISTID");

                    dynamic GridData = gvw.GetRowValues(IndexM, "SAPPEALID", "DINCIDENT", "SVENDORID", "SVENDORID", "SPROCESSNAME", "STOPICNAME", "SHEADREGISTERNO", "STRAILERREGISTERNO", "NPOINT", "STATUS", "SAPPEALNO", "SPROCESSID", "ATTACH_NO");
                    string sSubject = "", sStep = "", sStatus = "", cStat = "", sHeaderLink = "", sDetail = "", sMode = "";
                    int ATTCHEDNO = int.TryParse("" + GridData[12], out defInt) ? int.Parse("" + GridData[12]) + 1 : 1;
                    using (OracleConnection con1 = new OracleConnection(sql))
                    {
                        if (con1.State == ConnectionState.Closed) con1.Open();
                        if ("" + DATAM[0] == "I") //เพิ่มเติม
                        {
                            string strsql111 = "UPDATE TAPPEAL SET CSTATUS = :CSTATUS WHERE  SAPPEALID = :SAPPEALID";
                            using (OracleCommand com = new OracleCommand(strsql111, con1))
                            {
                                com.Parameters.Clear();

                                com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "4";
                                com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = DATAM[1] + "";
                                com.ExecuteNonQuery();

                                #region //Send Mail
                                /*
                             * 0    "SAPPEALID"
                             * 1    "DINCIDENT"
                             * 2    "SVENDORID"
                             * 3    "SVENDORNAME"   SVENDORID
                             * 4    "SPROCESSNAME"
                             * 5    "STOPICNAME"
                             * 6    "SHEADREGISTERNO"
                             * 7    "STRAILREGISTERNO"
                             * 8    "NPOINT"
                             * 9    "STATUS"
                             * 10   "SAPPEALNO"
                             * 11    "SPROCESSID"
                             * 12   "ATTACH_NO"
                             */

                                sSubject = "[TMS]:แจ้งส่งเอกสารเพิ่มเติม เลขที่ " + GridData[10] + " ครั้งที่" + ATTCHEDNO;
                                sStep = "";
                                sStatus = "ส่งเอกสารเพิ่มเติมครั้งที่ " + ATTCHEDNO + " แล้ว";
                                sMode = "VD2RK4MOREDOC";
                                sHeaderLink = "คลิีกเพื่อเข้าสู่ระบบและตรวจสอบเอกสาร คลิ๊กที่นี้ :"; sDetail = "";
                                cStat = "4";

                                string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
                                    , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
                                    , _toid = "55,40", _refid = "" + GridData[0], _status = "" + GridData[9], _usr_acc = "anucha.p,sysadmin";
                                string[] _Url = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
                                if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1")
                                {
                                    DataTable dt_usr = CommonFunction.Get_Data(con1, "SELECT wm_concat(TUSR.SUID) usr_recv ,wm_concat(TUSR.SUSERNAME ) usr_acc,wm_concat(TUSR.SEMAIL) email_recv FROM TPERMISSION PRMS  LEFT JOIN TMENU MENU ON PRMS.SMENUID = MENU.SMENUID  LEFT JOIN TUSER TUSR ON TUSR.SUID=PRMS.SUID WHERE 1=1 AND MENU.SMENUID='24' AND PRMS.MAIL2ME ='1'");
                                    if (dt_usr.Rows.Count > 0)
                                    {
                                        _to = "" + dt_usr.Rows[0]["email_recv"];
                                        _toid = "" + dt_usr.Rows[0]["usr_recv"];
                                        _usr_acc = "" + dt_usr.Rows[0]["usr_acc"];
                                    }
                                }
                                string sEncrypt = "mode$" + sMode + "&RefID$" + _refid + "&URL$admin_suppliant_lst&SAPPEALID$" + _refid + "&SHEADREGISTERNO$" + GridData[6] + "&STRAILREGISTERNO$" + GridData[7] + "&DINCIDENT$" + GridData[1] + "&CSTATUS$" + cStat + "&CAPPEAL$x&SPROCESSID$" + GridData[11] + "&usr_recv$" + _toid + "&usr_acc$" + _usr_acc + ""
                                    , link = ((sHeaderLink == "") ? "" : "" + sHeaderLink + " :<a target=_blank href='http://" + HttpContext.Current.Request.Url.Host + "/" + _Url[_Url.Length - 2] + "/" + "bypassmail2system.aspx?proc=" +
                                    Server.UrlEncode(STCrypt.Encrypt(sEncrypt)) + "' >ตกลง</a>");

                                #region massage
                                string messageMail = @"<table>
<tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>เลขที่การอุทธรณ์ : </td>
            <td style=' width:30%;'>{10}</td>
            <td style=' width:10%; '></td>
            <td style=' width:30%'></td>
        </tr>
        <tr>
            <td style='width:10%; white-space:nowrap;background-color:#F1F1F1;'>ประเภทการอุทธรณ์ : </td>
            <td style=' width:30%;'>{0}</td>
            <td style=' width:10%; white-space:nowrap;background-color:#F1F1F1;'>วันที่เกิดเหตุ : </td>
            <td style=' width:30%'>{1}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>หัวข้อปัญหา : </td>
            <td>{2}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>คะแนนที่ถูกตัด : </td>
            <td>{3}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(หัว) : </td>
            <td>{4}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>ทะเบียน(ท้าย) : </td>
            <td>{5}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะการอุทธรณ์ : </td>
            <td>{6}</td>
            <td style=' white-space:nowrap;background-color:#F1F1F1;'>สถานะหลังการอุทธรณ์ : </td>
            <td>{7}</td>
        </tr>
        <tr>
            <td style=' white-space:nowrap;' colspan='2'>{11} </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan='4'>{8}</td>
        </tr>
        <tr>
            <td colspan='4'>{9}</td>
        </tr>
    </table>";
                                #endregion

                                CommonFunction.SendNetMail(_from, _to, sSubject
                                    , string.Format(messageMail, GridData[4] + "", GridData[1] + "", GridData[5] + "", GridData[8] + "", GridData[6] + "", GridData[7] + ""
                                    , sStatus, "-", sDetail, "" + link, "" + GridData[10], "" + sStep)
                                    , con1, ""
                                    , "", "", "" + _toid, "" + _refid, "0");/**/
                                #endregion
                            }
                            // return;
                            using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TAPPEALATTACHMENT WHERE SAPPEALID = '" + DATAM[1] + "' AND nvl(CFILEMORE,'0') = '1'", con1))
                            {
                                comDelfile.ExecuteNonQuery();
                            }

                            string strImport = @"INSERT INTO TAPPEALATTACHMENT (SAPPEALID, NITEM, SDOCFROM,SDOCTYPE, SDOCUMENT, SPATH, SFILE, SSYSFILE, DUPLOAD, DEDIT, SCREATE, SUPDATE,CFILEMORE) 
                        VALUES (:SAPPEALID, :NITEM, :SDOCFROM,:SDOCTYPE, :SDOCUMENT, :SPATH, :SFILE, :SSYSFILE, SYSDATE, SYSDATE, :SCREATE, :SUPDATE,'1')";

                            var listImport = (List<dt>)Session["dt"];
                            int nCount = 5;
                            foreach (var list in listImport)
                            {
                                nCount++;
                                using (OracleCommand comImport = new OracleCommand(strImport, con1))
                                {
                                    comImport.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = DATAM[1] + "";
                                    comImport.Parameters.Add(":NITEM", OracleType.Number).Value = nCount;
                                    comImport.Parameters.Add(":SDOCFROM", OracleType.Char).Value = "";
                                    comImport.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "";
                                    comImport.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = list.dtEvidenceName;
                                    comImport.Parameters.Add(":SPATH", OracleType.VarChar).Value = list.dtFilePath;
                                    comImport.Parameters.Add(":SFILE", OracleType.VarChar).Value = list.dtFileName;
                                    comImport.Parameters.Add(":SSYSFILE", OracleType.VarChar).Value = list.dtGenFileName;
                                    comImport.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    comImport.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    comImport.ExecuteNonQuery();

                                }
                            }
                            var dt = new List<dt>();
                            Session["dt"] = dt;
                        }
                    }
                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
                    gvw.CancelEdit();
                    Cache.Remove(sds.CacheKeyDependency);
                    Cache[sds.CacheKeyDependency] = new object();
                    sds.Select(new System.Web.UI.DataSourceSelectArguments());
                    sds.DataBind();
                    gvw.DataBind();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;
            case "Save":
                if (CanWrite)
                //if ("" + Session["CheckPermission"] == "1")//ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน     
                {
                    int Index1 = int.Parse(e.Parameter.Split(';')[1]);
                    ASPxPageControl PageControl = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                    ASPxMemo txtAppealDetail = (ASPxMemo)PageControl.FindControl("txtAppealDetail");
                    if (string.Equals(txtAppealDetail.Text.Trim(), string.Empty))
                    {
                        //alertFail("กรุณาระบุ รายละเอียดการขออุทธรณ์");
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาระบุรายละเอียดการขออุทธรณ์');");
                        return;
                    }

                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();

                        dynamic DATA = gvw.GetRowValues(Index1, "SPROCESS", "STOPICID", "SHEADREGISTERNO", "STRAILERREGISTERNO", "SUMPOINT", "DCHECK", "ISSUETEXT", "OUTBOUNDNO", "AREAACCIDENT", "NREDUCEID", "SCONTRACTID", "SCHECKID", "CHKSTATUS", "SAPPEALID", "DCREATE", "SCUSTOMERID", "SVERSION", "SCHECKLISTID", "SVERSIONLIST", "STYPECHECKLISTID", "COST", "DISABLE_DRIVER", "BLACKLIST");
                        dynamic GridData = gvw.GetRowValues(Index1, "SAPPEALID", "DINCIDENT", "SVENDORID", "SVENDORID", "SPROCESSNAME", "STOPICNAME", "SHEADREGISTERNO", "STRAILERREGISTERNO", "NPOINT", "STATUS", "SAPPEALNO", "SPROCESSID", "ATTACH_NO");

                        if ("" + DATA[12] == "W")//ยื่นอุทธรณ์
                        {
                            string strsql = @"INSERT INTO TAPPEAL(SAPPEALID,SAPPEALNO,DAPPEAL,SAPPEALTYPE,STOPICAPPEAL,SVENDORID,SDRIVERNO,SHEADREGISTERNO,STRAILREGISTERNO,NPOINT,DEXPIRE,CSTATUS,DINCIDENT,SISSUETEXT,SAPPEALTEXT,DCREATE,SCREATE,DUPDATE,SUPDATE,CAPPEAL,SDELIVERYNO,SAREAACCIDENT,NREDUCEID,SCONTRACTID,SREFERENCEID,SCUSTOMERID,SVERSION,SCHECKLISTID,SVERSIONLIST,STYPECHECKLISTID, COST, DISABLE_DRIVER, BLACKLIST)
                                      Values (:SAPPEALID,:SAPPEALNO,sysdate,:SAPPEALTYPE,:STOPICAPPEAL,:SVENDORID,:SDRIVERNO,:SHEADREGISTERNO,:STRAILREGISTERNO,:NPOINT,:DEXPIRE,:CSTATUS,:DINCIDENT,:SISSUETEXT,:SAPPEALTEXT,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CAPPEAL,:SDELIVERYNO,:SAREAACCIDENT,:NREDUCEID,:SCONTRACTID,:SREFERENCEID,:SCUSTOMERID,:SVERSION,:SCHECKLISTID,:SVERSIONLIST,:STYPECHECKLISTID, :COST, :DISABLE_DRIVER, :BLACKLIST)";
                            string GenID = CommonFunction.Gen_ID(con, "SELECT SAPPEALID FROM (SELECT SAPPEALID FROM TAPPEAL ORDER BY CAST(SAPPEALID AS INT) DESC) WHERE ROWNUM <= 1");
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                int num = 0;
                                double ndouble = 0.0;
                                com.Parameters.Clear();
                                com.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = GenID;
                                com.Parameters.Add(":SAPPEALNO", OracleType.VarChar).Value = "PB" + DateTime.Now.ToString("yyMdHHmm_ss");
                                com.Parameters.Add(":SAPPEALTYPE", OracleType.VarChar).Value = DATA[0] + "";
                                com.Parameters.Add(":STOPICAPPEAL", OracleType.Number).Value = int.TryParse(DATA[1] + "", out num) ? num : 0;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = Session["SVDID"] + "";
                                com.Parameters.Add(":SDRIVERNO", OracleType.VarChar).Value = "";
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = DATA[2] + "";
                                com.Parameters.Add(":STRAILREGISTERNO", OracleType.VarChar).Value = DATA[3] + "";
                                com.Parameters.Add(":NPOINT", OracleType.Number).Value = double.TryParse((DATA[4] + "").Replace("-", ""), out ndouble) ? ndouble : 0.0;
                                com.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = ((DATA[14] + "" == "") ? DateTime.Today.AddDays(7) : DATA[14].AddDays(7));
                                com.Parameters.Add(":CSTATUS", OracleType.Char).Value = "0";
                                if (string.Equals(cboType.Value.ToString(), "3") || string.Equals(cboType.Value.ToString(), "6") || !cboType.Visible)
                                    com.Parameters.Add(":DINCIDENT", OracleType.DateTime).Value = DateTime.ParseExact(DATA[5].ToString(), "dd/MM/yyyy", null);
                                else
                                    com.Parameters.Add(":DINCIDENT", OracleType.DateTime).Value = DateTime.ParseExact(DATA[5].ToString(), "dd/MM/yyyy", null);
                                //com.Parameters.Add(":DINCIDENT", OracleType.DateTime).Value = DATA[5] + "";
                                com.Parameters.Add(":SISSUETEXT", OracleType.VarChar).Value = DATA[6] + "";
                                com.Parameters.Add(":SAPPEALTEXT", OracleType.VarChar).Value = txtAppealDetail.Text + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":CAPPEAL", OracleType.Char).Value = '0';
                                if (string.Equals(cboType.Value.ToString(), "3") || string.Equals(cboType.Value.ToString(), "6") || !cboType.Visible)
                                    com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = string.Empty;
                                else
                                    com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = DATA[7] + "";
                                com.Parameters.Add(":SAREAACCIDENT", OracleType.VarChar).Value = DATA[8] + "";
                                com.Parameters.Add(":NREDUCEID", OracleType.Number).Value = int.TryParse(DATA[9] + "", out num) ? num : 0;
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = DATA[10] + "";
                                com.Parameters.Add(":SREFERENCEID", OracleType.VarChar).Value = DATA[11] + "";
                                com.Parameters.Add(":SCUSTOMERID", OracleType.VarChar).Value = DATA[15] + "";
                                com.Parameters.Add(":SVERSION", OracleType.Number).Value = int.TryParse(DATA[16] + "", out num) ? num : 0;
                                com.Parameters.Add(":SCHECKLISTID", OracleType.Number).Value = int.TryParse(DATA[17] + "", out num) ? num : 0;
                                com.Parameters.Add(":SVERSIONLIST", OracleType.Number).Value = int.TryParse(DATA[18] + "", out num) ? num : 0;
                                com.Parameters.Add(":STYPECHECKLISTID", OracleType.VarChar).Value = DATA[19] + "";
                                com.Parameters.Add(":COST", OracleType.Number).Value = string.Equals(DATA[20] + "", string.Empty) ? 0 : DATA[20] + "";
                                com.Parameters.Add(":DISABLE_DRIVER", OracleType.Number).Value = string.Equals(DATA[21] + "", string.Empty) || string.Equals(DATA[21] + "", "H") ? 0 : DATA[21] + "";
                                com.Parameters.Add(":BLACKLIST", OracleType.Number).Value = string.Equals(DATA[22] + "", string.Empty) ? 0 : 1;
                                com.ExecuteNonQuery();

                                if (DATA[0] + "" == "090")//เฉพาะอุบัติเหตุ
                                {
                                    //CACTIVE = 10 เป็น ผขส. ยื่นอุทธรณ์ ดูใน table acc_status
                                    AccidentBLL.Instance.UpdateStatus(DATA[11] + "", 10, Session["UserID"] + string.Empty);
                                    SendEmail(ConfigValue.EmailAccident7, DATA[11] + "");
                                }
                                //AppealBLL.Instance.UpdateStatusComplainBLL(DATA[11] + "", ConfigValue.DocStatus6, DATA[5] + "");

                                //Send Email แจ้งการอุทธรณ์
                                this.SendEmail(ConfigValue.EmailComplainAppeal, DATA[11] + "");

                                //                                #region//Send Mail

                                //                                string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
                                //                                    , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
                                //                                    , _toid = "55,40", _refid = "", _status = "", _usr_acc = "anucha.p,sysadmin";
                                //                                string[] _Url = HttpContext.Current.Request.Url.AbsolutePath.Split('/');
                                //                                if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1")
                                //                                {
                                //                                    DataTable dt = CommonFunction.Get_Data(con, "SELECT wm_concat(TUSR.SUID) usr_recv ,wm_concat(TUSR.SUSERNAME ) usr_acc,wm_concat(TUSR.SEMAIL) email_recv FROM TPERMISSION PRMS  LEFT JOIN TMENU MENU ON PRMS.SMENUID = MENU.SMENUID  LEFT JOIN TUSER TUSR ON TUSR.SUID=PRMS.SUID WHERE 1=1 AND MENU.SMENUID='24' AND PRMS.MAIL2ME ='1'");
                                //                                    if (dt.Rows.Count > 0)
                                //                                    {
                                //                                        _to = "" + dt.Rows[0]["email_recv"];
                                //                                        _toid = "" + dt.Rows[0]["usr_recv"];
                                //                                        _usr_acc = "" + dt.Rows[0]["usr_acc"];
                                //                                    }
                                //                                }
                                //                                _refid = GenID; _status = "0";
                                //                                /*
                                //                                 * {1}:work's mode
                                //                                 * {2}:work's id
                                //                                 * {3}:url for forword
                                //                                 * {4}:params for forword format:{[],[],[],[][],[],[],[]}
                                //                                 * {5}:user recieve
                                //                                 */
                                //                                string sEncrypt = "mode$APPEAL2RK4APPROVE&RefID$" + _refid + "&URL$admin_suppliant_lst&SAPPEALID$" + _refid + "&SHEADREGISTERNO$" + DATA[2] + "&STRAILREGISTERNO$" + DATA[3] + "&DINCIDENT$" + DATA[5] + "&CSTATUS$0&CAPPEAL$0&usr_recv$" + _toid + "&usr_acc$" + _usr_acc + ""
                                //                                    , link = "คลิีกเพื่อเข้าสู่ระบบและพิจารณารายการ คลิ๊กที่นี้ :<a target=_blank href='http://" + HttpContext.Current.Request.Url.Host + "/" + _Url[_Url.Length - 2] + "/" + "bypassmail2system.aspx?proc=" +
                                //                                    Server.UrlEncode(STCrypt.Encrypt(sEncrypt)) + "' >ตกลง</a>";

                                //                                #region massage
                                //                                string messageMail = @"<table>
                                //        <tr>
                                //            <td style='width='10%'; white-space:nowrap;'>ประเภทการอุทธรณ์ : </td>
                                //            <td style=' width:30%'>{0}</td>
                                //            <td style=' width='10%'; white-space:nowrap;'>วันที่เกิดเหตุ : </td>
                                //            <td style=' width:30%'>{1}</td>
                                //        </tr>
                                //        <tr>
                                //            <td style=' white-space:nowrap;'>หัวข้อปัญหา : </td>
                                //            <td>{2}</td>
                                //            <td style=' white-space:nowrap;'>คะแนนที่ถูกตัด : </td>
                                //            <td>{3}</td>
                                //        </tr>
                                //        <tr>
                                //            <td style=' white-space:nowrap;'>ทะเบียน(หัว) : </td>
                                //            <td>{4}</td>
                                //            <td style=' white-space:nowrap;'>ทะเบียน(ท้าย) : </td>
                                //            <td>{5}</td>
                                //        </tr>
                                //        <tr>
                                //            <td style=' white-space:nowrap;'>สถานะการอุทธรณ์ : </td>
                                //            <td>{6}</td>
                                //            <td style=' white-space:nowrap;'>สถานะหลังการอุทธรณ์ : </td>
                                //            <td>{7}</td>
                                //        </tr>
                                //        <tr>
                                //            <td style=' white-space:nowrap;'>รายละเอียดการขออุทธรณ์ </td>
                                //            <td></td>
                                //            <td></td>
                                //            <td></td>
                                //        </tr>
                                //        <tr>
                                //            <td colspan='4'>{8}</td>
                                //        </tr>
                                //        <tr>
                                //            <td colspan='4'>{9}</td>
                                //        </tr>
                                //    </table>";
                                //                                #endregion
                                //                                CommonFunction.SendNetMail(_from, _to, "TMS:แจ้งผู้ประกอบการขอยื่นอุทธรณ์"
                                //                                    , string.Format(messageMail, GridData[4] + "", DATA[5] + "", GridData[5] + "", (double.TryParse((DATA[4] + "").Replace("-", ""), out ndouble) ? ndouble : 0.0) + "", DATA[2], DATA[3] + "", "ยื่นอุทธรณ์", "-", txtAppealDetail.Text + "", "" + link), con, ""
                                //                                    , "", "", "" + _toid, "" + GenID, "0");
                                //                                #endregion
                            }
                            //return;
                            string strImport = @"INSERT INTO TAPPEALATTACHMENT (SAPPEALID, NITEM, SDOCFROM,SDOCTYPE, SDOCUMENT, SPATH, SFILE, SSYSFILE, DUPLOAD, DEDIT, SCREATE, SUPDATE,CFILEMORE) 
                        VALUES (:SAPPEALID, :NITEM, :SDOCFROM,:SDOCTYPE, :SDOCUMENT, :SPATH, :SFILE, :SSYSFILE, SYSDATE, SYSDATE, :SCREATE, :SUPDATE,'0')";

                            int MAX = 0;
                            string[] sDucumentName = new string[5];
                            switch (txtType.Text)
                            {
                                case "011":
                                    MAX = 2;
                                    sDucumentName[0] = "รูปภาพอุปกรณ์ที่มีปัญหา";
                                    sDucumentName[1] = "เอกสารตรวจสอบรถขนส่งประจำวัน";
                                    break;
                                case "090":
                                    MAX = 5;
                                    sDucumentName[0] = "ดำเนินการตามรายงานอุบัติเหตุ";
                                    sDucumentName[1] = "ข้อมูลระบบ IVMS";
                                    sDucumentName[2] = "ภาพถ่าย";
                                    sDucumentName[3] = "เอกสารประกอบอื่น ๆ";
                                    sDucumentName[4] = "เอกสารประกอบอื่น ๆ";
                                    break;
                                case "010":
                                    MAX = 2;
                                    sDucumentName[0] = "แผนงานขนส่ง,ใบแนะนำการเติม";
                                    sDucumentName[1] = "ใบกำกับการขนส่ง";
                                    break;
                                case "020":
                                    MAX = 1;
                                    sDucumentName[0] = "ข้อมูล GPS";
                                    break;
                                case "080":
                                    MAX = 5;
                                    sDucumentName[0] = "เอกสารชี้แจง";
                                    sDucumentName[1] = "ข้อมูลระบบ IVMS";
                                    sDucumentName[2] = "ภาพถ่าย";
                                    sDucumentName[3] = "เอกสารประกอบ อื่น ๆ";
                                    sDucumentName[4] = "เอกสารประกอบ อื่น ๆ";
                                    break;
                                case "100":
                                    MAX = 5;
                                    sDucumentName[0] = "เอกสารอุทธรณ์";
                                    sDucumentName[1] = "เอกสารอุทธรณ์";
                                    sDucumentName[2] = "เอกสารอุทธรณ์";
                                    sDucumentName[3] = "เอกสารอุทธรณ์";
                                    sDucumentName[4] = "เอกสารอุทธรณ์";
                                    break;
                            }

                            for (int i = 0; i < MAX; i++)
                            {
                                using (OracleCommand comImport = new OracleCommand(strImport, con))
                                {


                                    ASPxTextBox txtFileName = (ASPxTextBox)PageControl.FindControl("txtFileName" + i);
                                    ASPxTextBox txtFilePath = (ASPxTextBox)PageControl.FindControl("txtFilePath" + i);

                                    if (txtFileName != null && txtFilePath != null)
                                    {
                                        if ("" + txtFilePath.Text != "")
                                        {
                                            comImport.Parameters.Add(":SAPPEALID", OracleType.VarChar).Value = GenID;
                                            comImport.Parameters.Add(":NITEM", OracleType.Number).Value = i + 1;
                                            comImport.Parameters.Add(":SDOCFROM", OracleType.Char).Value = "";
                                            comImport.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "";
                                            comImport.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[i];
                                            comImport.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                            comImport.Parameters.Add(":SFILE", OracleType.VarChar).Value = txtFileName.Text;
                                            comImport.Parameters.Add(":SSYSFILE", OracleType.VarChar).Value = "";
                                            comImport.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            comImport.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            comImport.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }

                            VisibleControlUpload();
                        }
                    }

                    LogUser("4", "I", "บันทึกข้อมูลหน้า ยื่นอุทธรณ์", "");

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "');");
                    gvw.CancelEdit();
                    Cache.Remove(sds.CacheKeyDependency);
                    Cache[sds.CacheKeyDependency] = new object();
                    sds.Select(new System.Web.UI.DataSourceSelectArguments());
                    sds.DataBind();
                    gvw.DataBind();
                    if (string.Equals(cboType.Value.ToString(), "2"))
                        this.GetDataAccident();
                    else if (string.Equals(cboType.Value.ToString(), "3"))
                        this.GetDataComplain();
                    else if (string.Equals(cboType.Value.ToString(), "6"))
                        this.GetDataMonthlyReport();
                    else
                        this.BindData();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

            case "deleteFile":

                if (CanWrite)
                //if ("" + Session["CheckPermission"] == "1")//ถ้าเอาขึ้นโปร ต้องเปิดใช้งาน     
                {
                    ASPxPageControl PageControl11 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;

                    string FilePath = paras[1];

                    if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                    }

                    string cNo = paras[2];
                    if (cNo == "1")
                    {

                        ASPxTextBox txtFileName0 = (ASPxTextBox)PageControl11.FindControl("txtFileName0");
                        ASPxTextBox txtFilePath0 = (ASPxTextBox)PageControl11.FindControl("txtFilePath0");

                        txtFileName0.Text = "";
                        txtFilePath0.Text = "";
                    }
                    else if (cNo == "2")
                    {
                        ASPxTextBox txtFileName1 = (ASPxTextBox)PageControl11.FindControl("txtFileName1");
                        ASPxTextBox txtFilePath1 = (ASPxTextBox)PageControl11.FindControl("txtFilePath1");

                        txtFileName1.Text = "";
                        txtFilePath1.Text = "";
                    }
                    else if (cNo == "3")
                    {
                        ASPxTextBox txtFileName2 = (ASPxTextBox)PageControl11.FindControl("txtFileName2");
                        ASPxTextBox txtFilePath2 = (ASPxTextBox)PageControl11.FindControl("txtFilePath2");

                        txtFileName2.Text = "";
                        txtFilePath2.Text = "";
                    }
                    else if (cNo == "4")
                    {
                        ASPxTextBox txtFileName3 = (ASPxTextBox)PageControl11.FindControl("txtFileName3");
                        ASPxTextBox txtFilePath3 = (ASPxTextBox)PageControl11.FindControl("txtFilePath3");

                        txtFileName3.Text = "";
                        txtFilePath3.Text = "";
                    }
                    else if (cNo == "5")
                    {
                        ASPxTextBox txtFileName4 = (ASPxTextBox)PageControl11.FindControl("txtFileName4");
                        ASPxTextBox txtFilePath4 = (ASPxTextBox)PageControl11.FindControl("txtFilePath4");

                        txtFileName4.Text = "";
                        txtFilePath4.Text = "";
                    }
                    else if (cNo == "6")
                    {
                        ASPxTextBox txtFileName5 = (ASPxTextBox)PageControl11.FindControl("txtFileName5");
                        ASPxTextBox txtFilePath5 = (ASPxTextBox)PageControl11.FindControl("txtFilePath5");

                        txtFileName5.Text = "";
                        txtFilePath5.Text = "";
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }

                VisibleControlUpload();
                break;

            case "DeleteList":

                ASPxPageControl PageControlListD = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                ASPxGridView sgvwD = PageControlListD.FindControl("sgvw") as ASPxGridView;
                var deldt = (List<dt>)Session["dt"];
                int index1 = Convert.ToInt32(paras[1]);

                string FilePathD = sgvwD.GetRowValues(index1, "dtFilePath") + "";

                if (File.Exists(Server.MapPath("./") + FilePathD.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePathD.Replace("/", "\\"));
                }


                deldt.RemoveAt(index1);

                sgvwD.DataSource = deldt;
                sgvwD.DataBind();
                Session["dt"] = deldt;
                visibleTabPage();

                break;

            case "ViewList":

                ASPxPageControl PageControlListV = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                ASPxGridView sgvwV = PageControlListV.FindControl("sgvw") as ASPxGridView;
                int index = Convert.ToInt32(paras[1]);
                string FilePathV = sgvwV.GetRowValues(index, "dtFilePath") + "";

                xcpn.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + FilePathV;

                visibleTabPage();
                break;

            case "Upload":
                ASPxPageControl PageControlListU = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
                ASPxTextBox txtEvidence = PageControlListU.FindControl("txtEvidence") as ASPxTextBox;
                ASPxGridView sgvwU = PageControlListU.FindControl("sgvw") as ASPxGridView;

                string dtr = txtEvidence.Text;
                var addData = (List<dt>)Session["dt"];
                int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                var fileupload = (Session["FileUpload"] + "").Split(';');

                if (fileupload.Count() == 3)
                {
                    addData.Add(new dt
                    {
                        dtID = max,
                        dtEvidenceName = txtEvidence.Text,
                        dtFileName = fileupload[0],
                        dtGenFileName = fileupload[1],
                        dtFilePath = fileupload[2]
                    });

                    Session["dt"] = addData;
                    sgvwU.DataSource = addData;
                    sgvwU.DataBind();
                }

                visibleTabPage();
                break;

        }
    }
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    void BindData()
    {
        gvw.Columns["OUTBOUNDNO"].Caption = "Outbound No.";

        string strsql = "";
        string strstatus = "";
        string Type = cboType.Value + "";
        string Status = cboStatus.Value + "";

        if (Status == "1")
        {
            strstatus = "AND (ap.CSTATUS = '0' or  ap.CSTATUS = '1' or ap.CSTATUS = '2')";
        }
        else if (Status == "2")
        {
            strstatus = "AND ap.CSTATUS = '3'";
        }
        else if (Status == "3")
        {
            strstatus = "AND ap.SAPPEALID IS NULL";
        }

        string Condition = string.Empty;

        if (hideAppealID.Text != "") { strstatus = " AND AP.SAPPEALID='" + hideAppealID.Text + "'"; }
        if (Type == "1")
        {
            Condition = " AND TO_CHAR(C.DCHECK,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(C.DCHECK,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'";

            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY c.DCHECK DESC) AS ID1, c.SCHECKID AS SCHECKID , TO_CHAR(c.DCHECK, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK,'' AS OUTBOUNDNO, cc.SCONTRACTNO,'ตรวจสภาพรถ' AS STYPE,'011' AS SPROCESS,cl.SCHECKLISTNAME as STOPICNAME,T.STOPICID,t.SVERSION,C.SHEADERREGISTERNO AS SHEADREGISTERNO,C.STRAILERREGISTERNO,- nvl(ct.NPOINT,0) AS SUMPOINT, ap.SAPPEALID,'' AS ISSUETEXT,'' AS SDRIVERNO,'' AS AREAACCIDENT,C.SCONTRACTID,C.DCREATE,'' AS SCUSTOMERID,
 (SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE ct.SCHECKID = REPLACE( RP.SREFERENCEID,'-','') AND CT.SCHECKLISTID = RP.SCHECKLISTID AND   CT.SVERSIONLIST = rp.SVERSIONLIST AND ct.STYPECHECKLISTID  = rp.STYPECHECKLISTID /*AND RP.SPROCESSID = '011'*/ AND rownum <=1)  as NREDUCEID,

CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(C.DCREATE, 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(C.DCREATE, 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(C.DCREATE, 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS,
CL.SCHECKLISTID,CL.SVERSIONLIST,CL.STYPECHECKLISTID,
CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'011' SPROCESSID ,'ตรวจสภาพรถ' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM ((((TCHECKTRUCK c LEFT JOIN TCHECKTRUCKITEM ct  ON C.SCHECKID = ct.SCHECKID)  LEFT JOIN TCHECKLIST  cl ON CT.SCHECKLISTID = CL.SCHECKLISTID AND CT.SVERSIONLIST = CL.SVERSIONLIST AND ct.STYPECHECKLISTID  = CL.STYPECHECKLISTID
) LEFT JOIN (SELECT STOPICID,STOPICNAME,SVERSION FROM TTOPIC) t ON cl.STOPICID = T.STOPICID AND CL.SVERSION = t.SVERSION ) LEFT JOIN TAPPEAL ap ON c.SCHECKID = REPLACE( AP.SREFERENCEID,'-','') AND AP.SAPPEALTYPE = '011' AND CT.SCHECKLISTID = AP.SCHECKLISTID AND CT.STYPECHECKLISTID = AP.STYPECHECKLISTID ) 
LEFT JOIN TCONTRACT cc ON C.SCONTRACTID = CC.SCONTRACTID 
WHERE  ct.SCHECKLISTID != '0' AND cc.CACTIVE = 'Y' AND CC.SVENDORID = :SVENDORID AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR c.SHEADERREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus + Condition;
            //WHERE  ct.SCHECKLISTID != '0' AND CC.SVENDORID = :SVENDORID AND TO_DATE(C.DCHECK,'DD/MM/YY') BETWEEN TO_DATE(:dStart,'DD/MM/YY') AND TO_DATE(:dEnd,'DD/MM/YY') AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR c.SHEADERREGISTERNO LIKE '%' || :oSearch || '%' OR c.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus;

        }
        else if (Type == "2")
        {
            Condition = " AND TO_CHAR(a.DACCIDENT,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(a.DACCIDENT,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'";

            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY a.DACCIDENT DESC) AS ID1, a.SACCIDENTID as SCHECKID,TO_CHAR(a.DACCIDENT, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK,A.SDELIVERYNO AS OUTBOUNDNO, cc.SCONTRACTNO,'อุบัติเหตุ' AS STYPE,'090' AS SPROCESS,A.SCAUSE AS STOPICID,0 AS SVERSION,CASE WHEN A.SCAUSE = '0' THEN A.SCAUSEOTHER ELSE AC.SACCIDENTCAUSENAME END AS STOPICNAME,A.SHEADREGISTERNO,A.STRAILERREGISTERNO,- REPLACE(nvl(A.NSCORESUBTRACT,0),'-','') as SUMPOINT,ap.SAPPEALID,CASE WHEN A.SCAUSE = '0' THEN A.SCAUSEOTHER ELSE AC.SACCIDENTCAUSENAME END AS ISSUETEXT, a.SCITIZENID AS SDRIVERNO,a.SADDRESS AS AREAACCIDENT,a.SCONTRACTID,A.DCREATE,'' AS SCUSTOMERID,
(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE( RP.SREFERENCEID ,'-','') = a.SACCIDENTID AND RP.SPROCESSID = '090' AND rownum <=1)  as NREDUCEID,

CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(A.DCREATE, 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(A.DCREATE, 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(A.DCREATE, 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS,
'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID,
CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'090' SPROCESSID ,'อุบัติเหตุ' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM ((TACCIDENT a LEFT JOIN LSTACCIDENTCAUSE ac ON A.SCAUSE = AC.NACCIDENTCAUSEID) LEFT JOIN TAPPEAL ap ON A.SACCIDENTID = REPLACE(AP.SREFERENCEID  ,'-','') AND AP.SAPPEALTYPE='090' ) LEFT JOIN TCONTRACT cc ON a.SCONTRACTID = CC.SCONTRACTID 
WHERE A.CUSE = '1' AND A.CFAULT != '0' AND A.SVENDORID = :SVENDORID AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR a.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR a.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus + Condition;
            //WHERE A.CUSE = '1' AND A.CFAULT != '0' AND A.SVENDORID = :SVENDORID AND TO_DATE(a.DACCIDENT,'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR a.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR a.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus;
        }
        else if (Type == "3")
        {
            Condition = " AND TO_CHAR(cp.DSERVICE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(cp.DSERVICE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'";

            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY cp.DDATECOMPLAIN DESC) AS ID1, CP.SSERVICEID AS SCHECKID,TO_CHAR(cp.DSERVICE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK,cp.SDELIVERYNO AS OUTBOUNDNO, cc.SCONTRACTNO,'เรื่องร้องเรียน' AS STYPE,'080' AS SPROCESS,TP.STOPICNAME,TP.STOPICID,CP.SVERSION,cp.SHEADREGISTERNO,cp.STRAILERREGISTERNO,- REPLACE(nvl(CP.NPOINT,0),'-','') AS SUMPOINT, ap.SAPPEALID,cp.SDETAIL AS ISSUETEXT,CP.SDRIVERNO,cp.SCOMPLAINADDRESS AS AREAACCIDENT,cp.SCONTRACTID,cp.DCREATE,'' AS SCUSTOMERID,
(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE( RP.SREFERENCEID ,'-','') = CP.SSERVICEID AND RP.SPROCESSID = '080' AND rownum <=1)  as NREDUCEID,
CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(cp.DSERVICE, 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(cp.DSERVICE, 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(cp.DSERVICE, 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS,
'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID,
CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'080' SPROCESSID,'เรื่องร้องเรียน' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM ((TCOMPLAIN cp LEFT JOIN TTOPIC  tp ON cp.STOPICID = TP.STOPICID AND CP.SVERSION = TP.SVERSION )LEFT JOIN TAPPEAL ap ON CP.SSERVICEID = REPLACE( AP.SREFERENCEID ,'-','') AND AP.SAPPEALTYPE='080')
 LEFT JOIN TCONTRACT cc ON cp.SCONTRACTID = CC.SCONTRACTID 
WHERE cp.CCUT = '1'
 AND CP.SVENDORID = :SVENDORID AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR cp.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR cp.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus + Condition;
            //AND CP.SVENDORID = :SVENDORID AND TO_DATE(cp.DSERVICE,'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR cp.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR cp.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus;
        }
        else if (Type == "4")
        {
            Condition = " AND TO_CHAR(tc.DDATE+1,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(tc.DDATE+1,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'";

            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY tc.DDATE DESC) AS ID1
,NVL(tl.NLISTNO,TC.NCONFIRMID) AS SCHECKID,TO_CHAR(tc.DDATE+1, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK, '' AS OUTBOUNDNO, cc.SCONTRACTNO,'ไม่ยืนยันรถตามสัญญา' AS STYPE,'010' AS SPROCESS,TP.STOPICNAME,tp.STOPICID,tp.SVERSION
,NVL(RDP.SHEADREGISTERNO,TL.SHEADREGISTERNO) SHEADREGISTERNO
,NVL(RDP.STRAILERREGISTERNO,TL.STRAILERREGISTERNO) STRAILERREGISTERNO
,- REPLACE(nvl(TP.NPOINT,0),'-','') AS SUMPOINT, ap.SAPPEALID,TP.STOPICNAME AS ISSUETEXT,'' AS SDRIVERNO,'' AS AREAACCIDENT, tc.SCONTRACTID ,tc.DDATE+1 DCREATE,'' AS SCUSTOMERID
,(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE( RP.SREFERENCEID ,'-','') = TC.NCONFIRMID AND RP.SPROCESSID = '010' AND rownum <=1 AND rp.SHEADREGISTERNO=NVL(RDP.SHEADREGISTERNO,TL.SHEADREGISTERNO))  as NREDUCEID

,CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(tc.DDATE+1, 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(tc.DDATE+1, 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(tc.DDATE+1, 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS
,'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID
,CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'010' SPROCESSID ,'ไม่ยืนยันรถตามสัญญา' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM TREDUCEPOINT RDP
LEFT JOIN TTRUCKCONFIRM tc ON REPLACE( RDP.SREFERENCEID  ,'-','')= TC.NCONFIRMID
LEFT JOIN TTRUCKCONFIRMLIST tl ON TL.NCONFIRMID = TC.NCONFIRMID AND NVL(RDP.SHEADREGISTERNO,'TRxxxxxx')=NVL(TL.SHEADREGISTERNO,'TRxxxxxx')
LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS LIKE '%,010%' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 
LEFT JOIN TAPPEAL ap ON NVL(tl.NLISTNO,TC.NCONFIRMID) = REPLACE( AP.SREFERENCEID ,'-','') AND NVL(AP.SHEADREGISTERNO,'TRxxxxxx')=NVL(RDP.SHEADREGISTERNO,'TRxxxxxx') AND NVL(AP.STRAILREGISTERNO,'TRxxxxxx')=NVL(RDP.STRAILERREGISTERNO,'TRxxxxxx') AND AP.SAPPEALTYPE='010'
--LEFT JOIN TAPPEAL ap ON tl.NLISTNO = REPLACE( AP.SREFERENCEID ,'-','') AND AP.SAPPEALTYPE='010'
LEFT JOIN TCONTRACT cc ON tc.SCONTRACTID = CC.SCONTRACTID 
WHERE 1=1 AND RDP.SPROCESSID='010'
AND NVL(TL.CCONFIRM,'0') = '0' 
AND CC.SVENDORID = :SVENDORID 
AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR NVL(RDP.SHEADREGISTERNO,TL.SHEADREGISTERNO) LIKE '%' || :oSearch || '%' OR NVL(RDP.STRAILERREGISTERNO,TL.STRAILERREGISTERNO) LIKE '%' || :oSearch || '%')
" + strstatus + Condition + " order by tc.DDATE DESC";
            // AND TO_DATE(tc.DDATE+1,'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') 
            #region back qry
            /*
            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY tl.DCREATE DESC) AS ID1, tl.NLISTNO AS SCHECKID,tc.DDATE+1 AS DCHECK, '' AS OUTBOUNDNO,'ไม่ยืนยันรถตามสัญญา' AS STYPE,'010' AS SPROCESS,TP.STOPICNAME,tp.STOPICID,tp.SVERSION
,VEH_NO.SHEADREGISTERNO,TU_NO.SHEADREGISTERNO STRAILERREGISTERNO ,- REPLACE(nvl(TP.NPOINT,0),'-','') AS SUMPOINT, ap.SAPPEALID,TP.STOPICNAME AS ISSUETEXT,tl.PERSONAL_CODE AS SDRIVERNO,'' AS AREAACCIDENT
, tc.SCONTRACTID ,NVL(tl.DCREATE,tc.DDATE) DCREATE,'' AS SCUSTOMERID
,(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE( RP.SREFERENCEID ,'-','') = tl.NLISTNO AND RP.SPROCESSID = '010' AND rownum <=1)  as NREDUCEID
,CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN TRUNC(7 - (SYSDATE - (tc.DDATE+1) )) > 0 THEN 'เหลือเวลา ' || TRUNC(7 - (SYSDATE - (tc.DDATE+1) )) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN TRUNC(7 - (SYSDATE - (tc.DDATE+1))) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS,
'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID,
CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'010' SPROCESSID ,'ไม่ยืนยันรถตามสัญญา' SPROCESSNAME--,SVENDORNAME,STOPICNAME
FROM  TCONTRACT cc 
LEFT JOIN TCONTRACT_TRUCK cctk on cc.SCONTRACTID=cctk.SCONTRACTID
LEFT JOIN TTRUCK VEH_NO ON CCTK.STRUCKID=VEH_NO.STRUCKID
LEFT JOIN TTRUCK TU_NO ON CCTK.STRAILERID =VEH_NO.STRUCKID
LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS LIKE '%,010%' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 
LEFT JOIN TTRUCKCONFIRM tc  ON tc.SCONTRACTID = CC.SCONTRACTID 
LEFT JOIN TTRUCKCONFIRMLIST tl  ON TL.NCONFIRMID = TC.NCONFIRMID
LEFT JOIN TAPPEAL ap ON tl.NLISTNO = REPLACE( AP.SREFERENCEID ,'-','') AND AP.SAPPEALTYPE='010'
WHERE 1=1
AND NVL(TL.CCONFIRM,'0')= '0' 
AND CC.SVENDORID = :SVENDORID 
AND TO_DATE(tc.DDATE,'DD/MM/YYYY')+1 BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') 
AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR tl.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR tl.STRAILERREGISTERNO LIKE '%' || :oSearch || '%')  " + strstatus;
            */
            #endregion
        }
        else if (Type == "5")
        {
            Condition = " AND TO_CHAR(CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'";

            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY P.DDELIVERY DESC) AS ID1, PL.SPLANLISTID AS SCHECKID,TO_CHAR(P.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') AS DCHECK,PL.SDELIVERYNO AS OUTBOUNDNO, cc.SCONTRACTNO,'ไม่ยืนยันรถตามแผน' AS STYPE ,'020' AS SPROCESS,tp.STOPICID,tp.SVERSION,tp.STOPICNAME,T.SHEADREGISTERNO,T.STRAILERREGISTERNO,- REPLACE(nvl(tp.NPOINT,0),'-','') AS SUMPOINT,ap.SAPPEALID,tp.STOPICNAME AS ISSUETEXT,p.SEMPLOYEEID AS SDRIVERNO,'' AS AREAACCIDENT,t.SCONTRACTID,P.DCREATE,PL.SSHIPTO AS SCUSTOMERID,
(SELECT rp.NREDUCEID FROM TREDUCEPOINT rp WHERE REPLACE( RP.SREFERENCEID ,'-','') = PL.SPLANLISTID AND RP.SPROCESSID = '020' AND rownum <=1)  as NREDUCEID,
CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(P.DCREATE, 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(P.DCREATE, 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END  ELSE
CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  END  END  END END END END END END END As STATUS,
CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE(P.DCREATE, 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  END END END END As CHKSTATUS,
'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID,
CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT  END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'020' SPROCESSID ,'ไม่ยืนยันรถตามแผน' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM ((((TPLANSCHEDULE p LEFT JOIN TPlanScheduleList pl ON p.NPLANID = PL.NPLANID) LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS LIKE '%,020%' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp ON 1 = 1 )
LEFT JOIN TTRUCK t ON REPLACE(REPLACE(p.SHEADREGISTERNO,'-',''),' ','') = REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),' ','') )LEFT JOIN TAPPEAL ap ON PL.SPLANLISTID = REPLACE( AP.SREFERENCEID ,'-','') AND AP.SAPPEALTYPE='020') 
LEFT JOIN TCONTRACT cc ON cc.SCONTRACTID = t.SCONTRACTID
WHERE p.CACTIVE = '1' AND (nvl(P.CCONFIRM,'0') = '0' ) AND PL.CACTIVE = '1' AND PL.CREDUCE = '1' AND P.SVENDORID = :SVENDORID 
AND TO_DATE(p.SPLANDATE,'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') AND (Cc.SCONTRACTNO LIKE '%' || :oSearch || '%' OR t.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR t.STRAILERREGISTERNO LIKE '%' || :oSearch || '%') " + strstatus;
            strsql = @"SELECT ROW_NUMBER () OVER (ORDER BY ODP.DATE_CREATE DESC) AS ID1,ODP.ORDERID AS SCHECKID , CASE WHEN ORDERTYPE='1' THEN TO_CHAR(ODP.DATE_CREATE, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') ELSE TO_CHAR(ODP.DDELIVERY, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH') END DCHECK ,ODP.SDELIVERYNO OUTBOUNDNO, ODP.SCONTRACTNO,'ไม่ยืนยันรถตามแผน' AS STYPE,'020' AS SPROCESS
 ,tp.STOPICID,tp.SVERSION,tp.STOPICNAME ,'' SHEADREGISTERNO , '' STRAILERREGISTERNO,- REPLACE(nvl(tp.NPOINT,0),'-','') AS SUMPOINT ,ap.SAPPEALID,tp.STOPICNAME AS ISSUETEXT
 ,'' AS SDRIVERNO,'' AS AREAACCIDENT,ODP.SCONTRACTID,ODP.DATE_CREATE DCREATE ,ODP.SHIP_TO AS SCUSTOMERID
,RDP.NREDUCEID
,CASE WHEN ap.SAPPEALID IS NULL THEN CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE((CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END), 'dd/MM/yy')) > 0 THEN 'เหลือเวลา ' || TO_CHAR(7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE((CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END), 'dd/MM/yy'))) || ' วัน' ELSE 'หมดเวลาอุทธรณ์' END   ELSE
    CASE WHEN ap.CSTATUS = '0' THEN 'รอพิจารณา' ELSE
        CASE WHEN ap.CSTATUS = '1' THEN 'กำลังดำเนินการ'  ELSE
            CASE WHEN ap.CSTATUS = '2' THEN 'ขอเอกสารเพิ่มเติม' ELSE
                CASE WHEN ap.CSTATUS = '4' THEN 'ส่งเอกสารเพิ่มเติมแล้ว' ELSE
                    CASE WHEN ap.CSTATUS = '5' THEN 'รอคณะกรรมการพิจารณา' ELSE
                        CASE WHEN ap.CSTATUS = '6' THEN 'ไม่รับอุทธรณ์' ELSE
                            CASE WHEN ap.CSTATUS = '3' THEN  
                                CASE WHEN ap.SSENTENCER = '1' THEN 'ตัดสิน (ถูก)' ELSE 
                                    CASE WHEN ap.SSENTENCER = '2' THEN 'ตัดสิน (ผิด)' ELSE 'ตัดสิน' END  
                                END  
                            END 
                         END 
                     END 
                 END 
             END 
         END 
     END 
 END As STATUS
,CASE WHEN ap.SAPPEALID IS NULL THEN  CASE WHEN 7 - (TO_DATE(SYSDATE, 'dd/MM/yy') - TO_DATE((CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END), 'dd/MM/yy')) > 0 THEN 'W' ELSE 'E' END  ELSE 
    CASE WHEN ap.CSTATUS = '0' OR  ap.CSTATUS = '1' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5'  THEN 'F' ELSE
        CASE WHEN ap.CSTATUS = '2' THEN 'I' ELSE
            CASE WHEN ap.CSTATUS = '6' THEN 'N' ELSE
                CASE WHEN ap.CSTATUS = '3' THEN  'F'  END  
            END 
        END 
    END 
END As CHKSTATUS
,'0' as SCHECKLISTID,'0' as SVERSIONLIST,'0' as STYPECHECKLISTID
,CASE WHEN ap.CSTATUS = '3' THEN CASE WHEN ap.SSENTENCER = '1' THEN '0' ELSE '-' || ap.NPOINT  END ELSE '' END AS FINALPOINT
,NVL(AP.ATTACH_NO,1) ATTACH_NO,AP.DINCIDENT ,AP.SVENDORID ,AP.NPOINT,AP.SAPPEALNO,'020' SPROCESSID ,'ไม่ยืนยันรถตามแผน' SPROCESSNAME--,SVENDORNAME,STOPICNAME 
, '' AS COST, '' AS DISABLE_DRIVER, '' AS TOTAL_POINT, '' AS TOTAL_COST, '' AS TOTAL_DISABLE_DRIVER, '' AS BLACKLIST, '' AS BLACKLIST_A
FROM TREDUCEPOINT RDP 
LEFT JOIN TBL_ORDERPLAN ODP ON RDP.SREFERENCEID=ODP.ORDERID
LEFT JOIN TPLANSCHEDULELIST PLNLST ON  PLNLST.SDELIVERYNO=ODP.SDELIVERYNO
LEFT JOIN (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM (SELECT STOPICID,STOPICNAME,NPOINT,SVERSION FROM TTOPIC WHERE CACTIVE = '1' AND CPROCESS LIKE '%,020%' ORDER BY SVERSION DESC ,DUPDATE DESC) WHERE ROWNUM <= 1) tp on 1=1
LEFT JOIN TAPPEAL AP 
ON  NVL(PLNLST.SPLANLISTID,REPLACE(ODP.ORDERID,'-','') ) = REPLACE( AP.SREFERENCEID ,'-','') 
--ON  PLNLST.SPLANLISTID = REPLACE( AP.SREFERENCEID ,'-','') 
AND AP.SAPPEALTYPE='020'
WHERE 1=1 AND RDP.sProcessID ='020' AND PLNLST.SDELIVERYNO is null 
--AND NVL(RDP.CACTIVE,'1') !='0' 
AND NVL(ODP.CREDUCE,'0') = '1' 
AND NVL(ODP.CACTIVE,'Y') != 'N' AND NVL(PLNLST.CACTIVE,'1') != '0' 
AND ODP.SVENDORID = :SVENDORID  
AND (ODP.SCONTRACTNO LIKE '%' || :oSearch || '%' OR ODP.SDELIVERYNO LIKE '%' || :oSearch || '%' )  " + Condition;
            //AND TO_DATE((CASE WHEN ORDERTYPE='1' THEN ODP.DATE_CREATE ELSE ODP.DDELIVERY END),'DD/MM/YYYY') BETWEEN TO_DATE(:dStart,'DD/MM/YYYY') AND TO_DATE(:dEnd,'DD/MM/YYYY') 
        }

        if (Type == "3")                            //ข้อร้องเรียน
            this.GetDataComplain();
        else if (Type == "2")                            //รายงานประจำเดือน
            this.GetDataAccident();
        else if (Type == "6")                            //รายงานประจำเดือน
            this.GetDataMonthlyReport();
        else
        {
            sds.SelectCommand = strsql;
            sds.SelectParameters.Clear();
            sds.SelectParameters.Add(":SVENDORID", Session["SVDID"] + "");
            sds.SelectParameters.Add(":oSearch", txtSearch.Text.Trim());
            //sds.SelectParameters.Add(":dStart", dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            //sds.SelectParameters.Add(":dEnd", dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")));
            //sds.SelectParameters.Add("dStart", DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy"));
            //sds.SelectParameters.Add("dEnd", DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy"));
            //sds.SelectParameters.Add("dStart", DbType.DateTime, DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString());
            //sds.SelectParameters.Add("dEnd", DbType.DateTime, DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("dd/MM/yyyy").ToString());

            sds.DataBind();
            gvw.DataSource = sds;
            gvw.DataBind();
        }

    }
    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        if (_Filename[0] != "")
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            //string genName = _Filename[0].Replace(" ", "") + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            int count = _Filename.Count() - 1;

            string genName = "appeal" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }
    }
    protected void MultipleUploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            string[] _Filename = e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;

                string genName = "otherappeal" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
                {
                    string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                    e.CallbackData = data;

                    Session["FileUpload"] = e.UploadedFile.FileName + ";" + genName + "." + _Filename[1] + ";" + data;

                }
            }
            else
            {

                return;

            }
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Contains(" "))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxButton imbIssue = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbedit");

            if (("" + e.GetValue("CHKSTATUS")).Equals("I"))
            {
                imbIssue.Text = "ส่งหลักฐานเพิ่มเติม";
            }
            else if (("" + e.GetValue("CHKSTATUS")).Equals("F"))
            {
                imbIssue.Text = "ดูรายละเอียดเพิ่มเติม";
            }
            else if (("" + e.GetValue("CHKSTATUS")).Equals("E"))
            {
                imbIssue.Text = "หมดเวลาอุทธรณ์";
                imbIssue.ClientEnabled = false;
            }
            else if (("" + e.GetValue("CHKSTATUS")).Equals("N"))
            {
                imbIssue.Text = "ไม่รับอุทธรณ์";
                imbIssue.ClientEnabled = false;
            }
        }
    }
    protected void sgvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Contains("คะแนนที่ได้"))
        {
            int VisibleIndex = e.VisibleIndex;

            if ("" + txtDocumentOther.Text == "F")
            {
                ASPxButton imbDel0 = (ASPxButton)((ASPxGridView)sender).FindRowCellTemplateControl(VisibleIndex, null, "imbDel0");
                imbDel0.ClientVisible = false;

            }
        }
    }
    private void VisibleControlUpload()
    {
        ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;

        ASPxMemo txtAppealDetail = (ASPxMemo)PageControl1.FindControl("txtAppealDetail");
        ASPxTextBox txtFilePath0 = (ASPxTextBox)PageControl1.FindControl("txtFilePath0");
        ASPxUploadControl uplExcel0 = (ASPxUploadControl)PageControl1.FindControl("uplExcel0");
        ASPxTextBox txtFileName0 = (ASPxTextBox)PageControl1.FindControl("txtFileName0");
        ASPxButton btnView0 = (ASPxButton)PageControl1.FindControl("btnView0");
        ASPxButton btnDelFile0 = (ASPxButton)PageControl1.FindControl("btnDelFile0");
        ASPxLabel lblName0 = (ASPxLabel)PageControl1.FindControl("lblName0");

        ASPxTextBox txtFilePath1 = (ASPxTextBox)PageControl1.FindControl("txtFilePath1");
        ASPxUploadControl uplExcel1 = (ASPxUploadControl)PageControl1.FindControl("uplExcel1");
        ASPxTextBox txtFileName1 = (ASPxTextBox)PageControl1.FindControl("txtFileName1");
        ASPxButton btnView1 = (ASPxButton)PageControl1.FindControl("btnView1");
        ASPxButton btnDelFile1 = (ASPxButton)PageControl1.FindControl("btnDelFile1");
        ASPxLabel lblName1 = (ASPxLabel)PageControl1.FindControl("lblName1");

        ASPxTextBox txtFilePath2 = (ASPxTextBox)PageControl1.FindControl("txtFilePath2");
        ASPxUploadControl uplExcel2 = (ASPxUploadControl)PageControl1.FindControl("uplExcel2");
        ASPxTextBox txtFileName2 = (ASPxTextBox)PageControl1.FindControl("txtFileName2");
        ASPxButton btnView2 = (ASPxButton)PageControl1.FindControl("btnView2");
        ASPxButton btnDelFile2 = (ASPxButton)PageControl1.FindControl("btnDelFile2");
        ASPxLabel lblName2 = (ASPxLabel)PageControl1.FindControl("lblName2");

        ASPxTextBox txtFilePath3 = (ASPxTextBox)PageControl1.FindControl("txtFilePath3");
        ASPxUploadControl uplExcel3 = (ASPxUploadControl)PageControl1.FindControl("uplExcel3");
        ASPxTextBox txtFileName3 = (ASPxTextBox)PageControl1.FindControl("txtFileName3");
        ASPxButton btnView3 = (ASPxButton)PageControl1.FindControl("btnView3");
        ASPxButton btnDelFile3 = (ASPxButton)PageControl1.FindControl("btnDelFile3");
        ASPxLabel lblName3 = (ASPxLabel)PageControl1.FindControl("lblName3");

        ASPxTextBox txtFilePath4 = (ASPxTextBox)PageControl1.FindControl("txtFilePath4");
        ASPxUploadControl uplExcel4 = (ASPxUploadControl)PageControl1.FindControl("uplExcel4");
        ASPxTextBox txtFileName4 = (ASPxTextBox)PageControl1.FindControl("txtFileName4");
        ASPxButton btnView4 = (ASPxButton)PageControl1.FindControl("btnView4");
        ASPxButton btnDelFile4 = (ASPxButton)PageControl1.FindControl("btnDelFile4");
        ASPxLabel lblName4 = (ASPxLabel)PageControl1.FindControl("lblName4");

        ASPxTextBox txtFilePath5 = (ASPxTextBox)PageControl1.FindControl("txtFilePath5");
        ASPxUploadControl uplExcel5 = (ASPxUploadControl)PageControl1.FindControl("uplExcel5");
        ASPxTextBox txtFileName5 = (ASPxTextBox)PageControl1.FindControl("txtFileName5");
        ASPxButton btnView5 = (ASPxButton)PageControl1.FindControl("btnView5");
        ASPxButton btnDelFile5 = (ASPxButton)PageControl1.FindControl("btnDelFile5");
        ASPxLabel lblName5 = (ASPxLabel)PageControl1.FindControl("lblName5");

        bool visible;
        if (txtDocumentOther.Text != "I" && txtDocumentOther.Text != "F")
        {
            switch (txtType.Text)
            {
                case "011":

                    lblName0.Text = "1) รูปภาพอุปกรณ์ที่มีปัญหา";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    lblName1.Text = "2) เอกสารตรวจสอบรถขนส่งประจำวัน";
                    visible = string.IsNullOrEmpty(txtFilePath1.Text);
                    uplExcel1.ClientVisible = visible;
                    txtFileName1.ClientVisible = !visible;
                    btnView1.ClientEnabled = !visible;
                    btnDelFile1.ClientEnabled = !visible;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "090":
                    lblName0.Text = "1) ดำเนินการตามรายงานอุบัติเหตุ";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    lblName1.Text = "2) ข้อมูลระบบ IVMS";
                    visible = string.IsNullOrEmpty(txtFilePath1.Text);
                    uplExcel1.ClientVisible = visible;
                    txtFileName1.ClientVisible = !visible;
                    btnView1.ClientEnabled = !visible;
                    btnDelFile1.ClientEnabled = !visible;

                    lblName2.Text = "3) ภาพถ่าย";
                    visible = string.IsNullOrEmpty(txtFilePath2.Text);
                    uplExcel2.ClientVisible = visible;
                    txtFileName2.ClientVisible = !visible;
                    btnView2.ClientEnabled = !visible;
                    btnDelFile2.ClientEnabled = !visible;

                    lblName3.Text = "4) เอกสารประกอบอื่น ๆ";
                    visible = string.IsNullOrEmpty(txtFilePath3.Text);
                    uplExcel3.ClientVisible = visible;
                    txtFileName3.ClientVisible = !visible;
                    btnView3.ClientEnabled = !visible;
                    btnDelFile3.ClientEnabled = !visible;

                    lblName4.Text = "5) เอกสารประกอบอื่น ๆ";
                    visible = string.IsNullOrEmpty(txtFilePath4.Text);
                    uplExcel4.ClientVisible = visible;
                    txtFileName4.ClientVisible = !visible;
                    btnView4.ClientEnabled = !visible;
                    btnDelFile4.ClientEnabled = !visible;
                    break;
                case "010":
                    lblName0.Text = "1) แผนงานขนส่ง, ใบแนะนำการเติม";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    lblName1.Text = "2) ใบกำกับการขนส่ง";
                    visible = string.IsNullOrEmpty(txtFilePath1.Text);
                    uplExcel1.ClientVisible = visible;
                    txtFileName1.ClientVisible = !visible;
                    btnView1.ClientEnabled = !visible;
                    btnDelFile1.ClientEnabled = !visible;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "020":
                    lblName0.Text = "1) ข้อมูล GPS";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = false;
                    btnView1.ClientVisible = false;
                    btnDelFile1.ClientVisible = false;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "080":
                    lblName0.Text = "1) เอกสารชี้แจง";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    lblName1.Text = "2) ข้อมูลระบบ IVMS";
                    visible = string.IsNullOrEmpty(txtFilePath1.Text);
                    uplExcel1.ClientVisible = visible;
                    txtFileName1.ClientVisible = !visible;
                    btnView1.ClientEnabled = !visible;
                    btnDelFile1.ClientEnabled = !visible;

                    lblName2.Text = "3) ภาพถ่าย";
                    visible = string.IsNullOrEmpty(txtFilePath2.Text);
                    uplExcel2.ClientVisible = visible;
                    txtFileName2.ClientVisible = !visible;
                    btnView2.ClientEnabled = !visible;
                    btnDelFile2.ClientEnabled = !visible;

                    lblName3.Text = "4) เอกสารประกอบ อื่น ๆ";
                    visible = string.IsNullOrEmpty(txtFilePath3.Text);
                    uplExcel3.ClientVisible = visible;
                    txtFileName3.ClientVisible = !visible;
                    btnView3.ClientEnabled = !visible;
                    btnDelFile3.ClientEnabled = !visible;

                    lblName4.Text = "5) เอกสารประกอบ อื่น ๆ";
                    visible = string.IsNullOrEmpty(txtFilePath4.Text);
                    uplExcel4.ClientVisible = visible;
                    txtFileName4.ClientVisible = !visible;
                    btnView4.ClientEnabled = !visible;
                    btnDelFile4.ClientEnabled = !visible;
                    break;
                case "100":
                    lblName0.Text = "1) เอกสารอุทธรณ์";
                    visible = string.IsNullOrEmpty(txtFilePath0.Text);
                    uplExcel0.ClientVisible = visible;
                    txtFileName0.ClientVisible = !visible;
                    btnView0.ClientEnabled = !visible;
                    btnDelFile0.ClientEnabled = !visible;

                    lblName1.Text = "2) เอกสารอุทธรณ์";
                    visible = string.IsNullOrEmpty(txtFilePath1.Text);
                    uplExcel1.ClientVisible = visible;
                    txtFileName1.ClientVisible = !visible;
                    btnView1.ClientEnabled = !visible;
                    btnDelFile1.ClientEnabled = !visible;

                    lblName2.Text = "3) เอกสารอุทธรณ์";
                    visible = string.IsNullOrEmpty(txtFilePath2.Text);
                    uplExcel2.ClientVisible = visible;
                    txtFileName2.ClientVisible = !visible;
                    btnView2.ClientEnabled = !visible;
                    btnDelFile2.ClientEnabled = !visible;

                    lblName3.Text = "4) เอกสารอุทธรณ์";
                    visible = string.IsNullOrEmpty(txtFilePath3.Text);
                    uplExcel3.ClientVisible = visible;
                    txtFileName3.ClientVisible = !visible;
                    btnView3.ClientEnabled = !visible;
                    btnDelFile3.ClientEnabled = !visible;

                    lblName4.Text = "5) เอกสารอุทธรณ์";
                    visible = string.IsNullOrEmpty(txtFilePath4.Text);
                    uplExcel4.ClientVisible = visible;
                    txtFileName4.ClientVisible = !visible;
                    btnView4.ClientEnabled = !visible;
                    btnDelFile4.ClientEnabled = !visible;
                    break;

            }

        }
        else
        {
            txtAppealDetail.ClientEnabled = false;
            switch (txtType.Text)
            {
                case "011":
                    lblName0.Text = "1) รูปภาพอุปกรณ์ที่มีปัญหา";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    lblName1.Text = "2) เอกสารตรวจสอบรถขนส่งประจำวัน";
                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = true;
                    txtFileName1.ClientEnabled = false;
                    btnView1.ClientEnabled = true;
                    btnDelFile1.ClientVisible = false;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "090":
                    lblName0.Text = "1) ดำเนินการตามรายงานอุบัติเหตุ";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    lblName1.Text = "2) ข้อมูลระบบ IVMS";
                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = true;
                    txtFileName1.ClientEnabled = false;
                    btnView1.ClientEnabled = true;
                    btnDelFile1.ClientVisible = false;

                    lblName2.Text = "3) ภาพถ่าย";
                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = true;
                    txtFileName2.ClientEnabled = false;
                    btnView2.ClientEnabled = true;
                    btnDelFile2.ClientVisible = false;

                    lblName3.Text = "4) เอกสารประกอบ อื่น ๆ";
                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = true;
                    txtFileName3.ClientEnabled = false;
                    btnView3.ClientEnabled = true;
                    btnDelFile3.ClientVisible = false;

                    lblName4.Text = "5) เอกสารประกอบ อื่น ๆ";
                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = true;
                    txtFileName4.ClientEnabled = false;
                    btnView4.ClientEnabled = true;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "010":
                    lblName0.Text = "1) แผนงานขนส่ง, ใบแนะนำการเติม";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    lblName1.Text = "2) ใบกำกับการขนส่ง";
                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = true;
                    txtFileName1.ClientEnabled = false;
                    btnView1.ClientEnabled = true;
                    btnDelFile1.ClientVisible = false;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "020":
                    lblName0.Text = "1) ข้อมูล GPS";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = false;
                    btnView1.ClientVisible = false;
                    btnDelFile1.ClientVisible = false;

                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = false;
                    btnView2.ClientVisible = false;
                    btnDelFile2.ClientVisible = false;

                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = false;
                    btnView3.ClientVisible = false;
                    btnDelFile3.ClientVisible = false;

                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = false;
                    btnView4.ClientVisible = false;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "080":
                    lblName0.Text = "1) เอกสารชี้แจง";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    lblName1.Text = "2) ข้อมูลระบบ IVMS";
                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = true;
                    txtFileName1.ClientEnabled = false;
                    btnView1.ClientEnabled = true;
                    btnDelFile1.ClientVisible = false;

                    lblName2.Text = "3) ภาพถ่าย";
                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = true;
                    txtFileName2.ClientEnabled = false;
                    btnView2.ClientEnabled = true;
                    btnDelFile2.ClientVisible = false;

                    lblName3.Text = "4) เอกสารประกอบ อื่น ๆ";
                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = true;
                    txtFileName3.ClientEnabled = false;
                    btnView3.ClientEnabled = true;
                    btnDelFile3.ClientVisible = false;

                    lblName4.Text = "5) เอกสารประกอบ อื่น ๆ";
                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = true;
                    txtFileName4.ClientEnabled = false;
                    btnView4.ClientEnabled = true;
                    btnDelFile4.ClientVisible = false;
                    break;
                case "100":
                    lblName0.Text = "1) เอกสารอุทธรณ์";
                    uplExcel0.ClientVisible = false;
                    txtFileName0.ClientVisible = true;
                    txtFileName0.ClientEnabled = false;
                    btnView0.ClientEnabled = true;
                    btnDelFile0.ClientVisible = false;

                    lblName1.Text = "2) เอกสารอุทธรณ์";
                    uplExcel1.ClientVisible = false;
                    txtFileName1.ClientVisible = true;
                    txtFileName1.ClientEnabled = false;
                    btnView1.ClientEnabled = true;
                    btnDelFile1.ClientVisible = false;

                    lblName2.Text = "3) เอกสารอุทธรณ์";
                    uplExcel2.ClientVisible = false;
                    txtFileName2.ClientVisible = true;
                    txtFileName2.ClientEnabled = false;
                    btnView2.ClientEnabled = true;
                    btnDelFile2.ClientVisible = false;

                    lblName3.Text = "4) เอกสารอุทธรณ์";
                    uplExcel3.ClientVisible = false;
                    txtFileName3.ClientVisible = true;
                    txtFileName3.ClientEnabled = false;
                    btnView3.ClientEnabled = true;
                    btnDelFile3.ClientVisible = false;

                    lblName4.Text = "5) เอกสารอุทธรณ์";
                    uplExcel4.ClientVisible = false;
                    txtFileName4.ClientVisible = true;
                    txtFileName4.ClientEnabled = false;
                    btnView4.ClientEnabled = true;
                    btnDelFile4.ClientVisible = false;
                    break;

            }


        }


    }
    private void listFileName(string id, bool visibleControl)
    {
        ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;

        ASPxTextBox txtFilePath0 = (ASPxTextBox)PageControl1.FindControl("txtFilePath0");
        ASPxTextBox txtFileName0 = (ASPxTextBox)PageControl1.FindControl("txtFileName0");

        ASPxTextBox txtFilePath1 = (ASPxTextBox)PageControl1.FindControl("txtFilePath1");
        ASPxTextBox txtFileName1 = (ASPxTextBox)PageControl1.FindControl("txtFileName1");

        ASPxTextBox txtFilePath2 = (ASPxTextBox)PageControl1.FindControl("txtFilePath2");
        ASPxTextBox txtFileName2 = (ASPxTextBox)PageControl1.FindControl("txtFileName2");

        ASPxTextBox txtFilePath3 = (ASPxTextBox)PageControl1.FindControl("txtFilePath3");
        ASPxTextBox txtFileName3 = (ASPxTextBox)PageControl1.FindControl("txtFileName3");

        ASPxTextBox txtFilePath4 = (ASPxTextBox)PageControl1.FindControl("txtFilePath4");
        ASPxTextBox txtFileName4 = (ASPxTextBox)PageControl1.FindControl("txtFileName4");
        ASPxGridView sgvw = PageControl1.FindControl("sgvw") as ASPxGridView;

        DataTable dtFile = CommonFunction.Get_Data(sql, "SELECT NITEM, SPATH, SFILE, SSYSFILE,CFILEMORE,SDOCUMENT FROM TAPPEALATTACHMENT WHERE SAPPEALID ='" + id + "'");
        foreach (DataRow dr in dtFile.Rows)
        {
            switch ("" + dr["NITEM"])
            {
                case "1":
                    txtFileName0.Text = dr["SFILE"] + "";
                    txtFilePath0.Text = dr["SPATH"] + "";
                    break;
                case "2":
                    txtFileName1.Text = dr["SFILE"] + "";
                    txtFilePath1.Text = dr["SPATH"] + "";
                    break;
                case "3":
                    txtFileName2.Text = dr["SFILE"] + "";
                    txtFilePath2.Text = dr["SPATH"] + "";
                    break;
                case "4":
                    txtFileName3.Text = dr["SFILE"] + "";
                    txtFilePath3.Text = dr["SPATH"] + "";
                    break;
                case "5":
                    txtFileName4.Text = dr["SFILE"] + "";
                    txtFilePath4.Text = dr["SPATH"] + "";
                    break;
            }

        }


        DataRow[] ddt = dtFile.Select("CFILEMORE = 1");

        if (ddt.Count() > 0)
        {
            var Data = new List<dt>();

            foreach (DataRow dr1 in ddt)
            {
                Data.Add(new dt
                {
                    dtID = Convert.ToInt32(dr1["NITEM"]),
                    dtEvidenceName = dr1["SDOCUMENT"] + "",
                    dtFileName = dr1["SFILE"] + "",
                    dtGenFileName = dr1["SSYSFILE"] + "",
                    dtFilePath = dr1["SPATH"] + ""
                });
            }
            sgvw.DataSource = Data;
            sgvw.DataBind();
            Session["dt"] = Data;
        }

        VisibleControlUpload();

    }
    private void visibleTabPage()
    {
        ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;

        if (txtDocumentOther.Text == "I")
        {
            PageControl1.ActiveTabIndex = 2;
            PageControl1.TabPages[2].ClientEnabled = true;

        }
        else if (txtDocumentOther.Text == "F")
        {
            ASPxGridView sgvw = (ASPxGridView)PageControl1.FindControl("sgvw");
            if (sgvw.VisibleRowCount > 0)
            {
                ASPxTextBox txtEvidence = (ASPxTextBox)PageControl1.FindControl("txtEvidence");
                ASPxUploadControl MultipleUploader = (ASPxUploadControl)PageControl1.FindControl("MultipleUploader");
                ASPxButton btnAdd = (ASPxButton)PageControl1.FindControl("btnAdd");

                PageControl1.TabPages[2].ClientEnabled = true;
                txtEvidence.ClientEnabled = false;
                MultipleUploader.ClientVisible = false;
                btnAdd.Visible = false;
            }
        }
    }
    [Serializable]
    struct dt
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.Search();
    }

    private void Search()
    {
        if (string.Equals(cboType.Value.ToString(), "2"))
            this.GetDataAccident();
        else if (string.Equals(cboType.Value.ToString(), "3"))
            this.GetDataComplain();
        else if (string.Equals(cboType.Value.ToString(), "6"))
            this.GetDataMonthlyReport();
        else
            this.BindData();
    }

    private void GetDataAccident()
    {
        try
        {
            string VendorID = Session["SVDID"].ToString();
            DataTable dtAppeal = AppealBLL.Instance.AppealAccidentSelectBLL(VendorID, this.GetConditionSearchAccident());
            gvw.DataSource = dtAppeal;
            gvw.DataBind();

            //gvw.Columns["OUTBOUNDNO"].Caption = "เลขที่สัญญา";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetDataComplain()
    {
        try
        {
            string VendorID = Session["SVDID"].ToString();
            DataTable dtAppeal = AppealBLL.Instance.AppealComplainSelectBLL(VendorID, this.GetConditionSearchComplain());
            gvw.DataSource = dtAppeal;
            gvw.DataBind();

            //gvw.Columns["OUTBOUNDNO"].Caption = "เลขที่สัญญา";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetDataMonthlyReport()
    {
        try
        {
            string VendorID = Session["SVDID"].ToString();
            DataTable dtAppeal = AppealBLL.Instance.AppealMonthlyReportSelectBLL(VendorID, this.GetConditionSearchMonthlyReport());
            gvw.DataSource = dtAppeal;
            gvw.DataBind();

            //gvw.Columns["OUTBOUNDNO"].Caption = "เลขที่สัญญา";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionSearchAccident()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (Session["AppealAccID"] != null)
                return sb.Append(" AND TAPPEAL.SREFERENCEID ='" + Session["AppealAccID"].ToString() + "'").ToString();
            else
                sb.Append(" AND TO_CHAR(ACC_ACCIDENT.ACCIDENT_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(ACC_ACCIDENT.ACCIDENT_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'");

            if (hideAppealID.Text != "")
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hideAppealID.Text + "'");
            else if (cboStatus.Value != null)
            {
                if (string.Equals(cboStatus.Value.ToString(), "1"))
                    sb.Append(" AND (TAPPEAL.CSTATUS IN ('0','1','2'))");
                else if (string.Equals(cboStatus.Value.ToString(), "2"))
                    sb.Append(" AND TAPPEAL.CSTATUS = '3'");
                else if (string.Equals(cboStatus.Value.ToString(), "3"))
                    sb.Append(" AND TAPPEAL.SAPPEALID IS NULL");
            }

            if (!string.Equals(DocID, string.Empty))
                sb.Append(" AND ACC_ACCIDENT.ACCIDENT_ID = '" + txtSearch.Text.Trim() + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionSearchComplain()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (Session["AppealDoc"] != null)
                return sb.Append(" AND TAPPEAL.SREFERENCEID ='" + Session["AppealDoc"].ToString() + "'").ToString();
            else
                sb.Append(" AND TO_CHAR(TCOMPLAIN.COMPLAIN_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(TCOMPLAIN.COMPLAIN_DATE,'yyyyMMdd', 'NLS_DATE_LANGUAGE = ENGLISH') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'");
            //DDELIVERY

            if (hideAppealID.Text != "")
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hideAppealID.Text + "'");
            else if (cboStatus.Value != null)
            {
                if (string.Equals(cboStatus.Value.ToString(), "1"))
                    sb.Append(" AND (TAPPEAL.CSTATUS IN ('0','1','2'))");
                else if (string.Equals(cboStatus.Value.ToString(), "2"))
                    sb.Append(" AND TAPPEAL.CSTATUS = '3'");
                else if (string.Equals(cboStatus.Value.ToString(), "3"))
                    sb.Append(" AND TAPPEAL.SAPPEALID IS NULL");
            }

            if (!string.Equals(DocID, string.Empty))
                sb.Append(" AND TCOMPLAIN.DOC_ID = '" + txtSearch.Text.Trim() + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionSearchMonthlyReport()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            if (hideAppealID.Text != "")
                sb.Append(" AND TAPPEAL.SAPPEALID = '" + hideAppealID.Text + "'");
            else if (cboStatus.Value != null)
            {
                if (string.Equals(cboStatus.Value.ToString(), "1"))
                    sb.Append(" AND (TAPPEAL.CSTATUS IN ('0','1','2'))");
                else if (string.Equals(cboStatus.Value.ToString(), "2"))
                    sb.Append(" AND TAPPEAL.CSTATUS = '3'");
                else if (string.Equals(cboStatus.Value.ToString(), "3"))
                    sb.Append(" AND TAPPEAL.SAPPEALID IS NULL");
            }

            sb.Append(" AND TO_CHAR(TMONTHLY_HEADER.FINAL_DATETIME,'yyyyMMdd') >= '" + DateTime.ParseExact(dteStart.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "' AND TO_CHAR(TMONTHLY_HEADER.FINAL_DATETIME,'yyyyMMdd') <= '" + DateTime.ParseExact(dteEnd.Text.Trim(), "dd/MM/yyyy", CultureInfo.CurrentCulture).ToString("yyyyMMdd") + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SendEmail(int TemplateID, string DocID)
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailComplainAppeal)
                {
                    dtComplainEmail = ComplainBLL.Instance.ComplainEmailRequestDocBLL(DocID);

                    Subject = Subject.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());

                    Body = Body.Replace("{CAR}", dtComplainEmail.Rows[0]["CAR"].ToString());
                    Body = Body.Replace("{DRIVER}", dtComplainEmail.Rows[0]["DRIVER"].ToString());
                    Body = Body.Replace("{VENDOR}", dtComplainEmail.Rows[0]["VENDOR"].ToString());
                    Body = Body.Replace("{CONTRACT}", dtComplainEmail.Rows[0]["CONTRACT"].ToString());
                    Body = Body.Replace("{SOURCE}", dtComplainEmail.Rows[0]["WAREHOUSE"].ToString());
                    Body = Body.Replace("{COMPLAIN}", dtComplainEmail.Rows[0]["COMPLAIN"].ToString());
                    Body = Body.Replace("{DOCID}", dtComplainEmail.Rows[0]["DOCID"].ToString());
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_suppliant_lst.aspx";
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //string CreateMail = (!string.Equals(dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString(), string.Empty)) ? "," + dtComplainEmail.Rows[0]["EMAIL_CREATE"].ToString() : string.Empty;
                    string CreateMail = string.Empty;

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtComplainEmail.Rows[0]["VENDORID"].ToString(), false, false) + CreateMail, Subject, Body, "", "EmailComplainAppeal", ColumnEmailName);
                }
                else if (TemplateID == ConfigValue.EmailAccident7)
                {
                    #region EmailAccident7
                    string accID = DocID;
                    DataTable dt = AccidentBLL.Instance.AccidentTab1Select(accID);
                    DataRow dr = dt.Rows[0];
                    string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dr["SVENDORID"] + string.Empty, false, false);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}

                    //EmailList += "TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com,TMS_nut.t@pttor.com,TMS_terapat.p@pttor.com,TMS_sarun.c@pttor.com";
                    //EmailList += "yutasak.c@pttor.com,kittipong.l@pttor.com,suphakit.k@pttor.com, apipat.k@pttor.com, nut.t@pttor.com, terapat.p@pttor.com, nataporn.c@pttor.com";
                    //EmailList += "raviwan.t@pttor.com,nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com";
                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{DATE}", dr["ACCIDENT_DATE_PREVIEW"] + string.Empty);
                    Body = Body.Replace("{TIME}", dr["ACCIDENT_TIME_PREVIEW"] + string.Empty);
                    Body = Body.Replace("{CAR}", dr["SHEADREGISTERNO"] + string.Empty);
                    Body = Body.Replace("{VENDOR}", dr["VENDORNAME"] + string.Empty);
                    Body = Body.Replace("{CONTRACT}", dr["SCONTRACTNO"] + string.Empty);
                    Body = Body.Replace("{ACCSTATUS}", dr["ACCIDENTTYPENAME"] + string.Empty);
                    Body = Body.Replace("{SOURCE}", dr["SOURCE"] + string.Empty);
                    Body = Body.Replace("{DRIVER}", dr["EMPNAME"] + string.Empty);
                    Body = Body.Replace("{ACCPOINT}", dr["LOCATIONS"] + string.Empty);
                    //Body = Body.Replace("{REPORT_CHECK}", IsApprove);
                    Body = Body.Replace("{REMARK}", "");
                    Body = Body.Replace("{GPS}", dr["GPSL"] + string.Empty + "," + dr["GPSR"] + string.Empty);
                    Body = Body.Replace("{USER_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);

                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "admin_suppliant_lst.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident7", ColumnEmailName);
                    #endregion
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    //protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();

    //    Session["Appeal_Download"] = FullPath;
    //    FormHelper.OpenForm("DownloadFile.aspx", Page);
    //}

    //protected void cmdAttach_Click(object sender, EventArgs e)
    //{
    //    DataTable dtUpload = AppealBLL.Instance.ImportFileSelectBLL("8611", "COMPLAIN_APPEAL");
    //    GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

    //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowAttach", "$('#ShowAttach').modal();", true);
    //    udpAttach.Update();
    //}
    //protected void cmdClose_Click(object sender, EventArgs e)
    //{
    //    lblAttachTotal.Text = string.Format(lblAttachTotal.Text, dgvUploadFile.Rows.Count.ToString());
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "closeModalAttach();", true);

    //    this.GetAppealFile();
    //    this.BindData();
    //}

    //private void GetAppealFile()
    //{
    //    ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
    //    DataTable dtUpload = AppealBLL.Instance.ImportFileSelectBLL("8611", "COMPLAIN_APPEAL");

    //    if (PageControl1 != null)
    //    {
    //        UpdatePanel updFile = (UpdatePanel)PageControl1.FindControl("udpFile");
    //        if (updFile != null && dtUpload.Rows.Count > 0)
    //        {
    //            if (lblAttachTotal != null)
    //            {
    //                if (dtUpload.Rows.Count == 0)
    //                {
    //                    cmdAttach.Visible = false;
    //                    lblAttachTotal.Visible = false;
    //                }
    //                else
    //                {
    //                    cmdAttach.Visible = true;
    //                    lblAttachTotal.Visible = true;

    //                    lblAttachTotal.Text = string.Format(lblAttachTotal.Text, dtUpload.Rows.Count.ToString());
    //                }
    //            }
    //        }
    //    }
    //}

    private void GetFile(string id)
    {
        DataTable dtUpload = AppealBLL.Instance.ImportFileSelectBLL(id, "COMPLAIN_APPEAL");
        ASPxPageControl PageControl1 = gvw.FindEditFormTemplateControl("pageControl") as ASPxPageControl;
        ASPxGridView dgvUpload = (ASPxGridView)PageControl1.FindControl("dgvUpload");
        Label lblAppeal = (Label)PageControl1.FindControl("lblAppeal");

        if (dtUpload.Rows.Count > 0)
        {
            dgvUpload.DataSource = dtUpload;
            dgvUpload.DataBind();

            Session["dtAppealComplain"] = dtUpload;
            lblAppeal.Visible = true;
            dgvUpload.Visible = true;
        }
        else
        {
            lblAppeal.Visible = false;
            dgvUpload.Visible = false;
        }
    }
}