﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_employee.aspx.cs" Inherits="vendor_employee" StylesheetTheme="Aqua" %>

<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">

        $(document).ready(function () {
            //Settxtdateover();
        });


    </script>
    <style type="text/css">
        .dxrpcontent
        {
            padding: 0px !important;
        }
        .dxeRadioButtonList_Aqua
        {
            border-color: White !important;
            border-width: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined ; inEndRequestHandler();}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="100%"
                    CssClass="nopading" HeaderText="จัดการข้อมูลพนักงาน">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="PanelContent1" CssClass="nopading">
                            <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                            <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
   
                                    <div class="form-horizontal">
                                        <div class="row form-group">
                                            </div>
                                        <div class="row form-group">
                                            </div>
                                         <div class="row form-group">
                                                <label class="col-md-2 control-label">บริษัทผู้ขนส่ง :</label>
                                                <div class="col-md-4  ">
                                                   <dx:ASPxComboBox ID="cboVendor" runat="server" Width="100%" ClientInstanceName="cboVendor" CssClass="form-control" TextFormatString="{1}" ValueField="SVENDORID" IncrementalFilteringMode="Contains" >

                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="200px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                </div>
                                             <label class="col-md-2 control-label">การจ้างงาน :</label>
                                                <div class="col-md-4  ">
                                                   <%--<dx:ASPxRadioButtonList runat="server" ID="cblstatus" RepeatDirection="Horizontal" ClientInstanceName="cblstatus">
                                                                    <Items>
                                                                        <dx:ListEditItem Text="ทั้งหมด" Value="2" Selected="true" />
                                                                        <dx:ListEditItem Text="จ้างงาน" Value="1" />
                                                                        <dx:ListEditItem Text="เลิกจ้างงาน" Value="0" />
                                                                    </Items>
                                                                    
                                                                </dx:ASPxRadioButtonList>--%>
                                                    <asp:RadioButtonList ID="radStatusDriver" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="ทั้งหมด" Value="2" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="จ้างงาน" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="เลิกจ้างงาน" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                        <div class="row form-group">
                                                <label class="col-md-2 control-label">ช่วงเวลาอัพเดทข้อมูล :</label>
                                                <div class="col-md-4  ">
                                                    <asp:TextBox runat="server" ID="dedtStart" CssClass="datepicker" />
                                                </div>
                                             <label class="col-md-2 control-label">ถึง :</label>
                                                <div class="col-md-4  ">
                                                   <asp:TextBox runat="server" ID="dedtEnd" CssClass="datepicker" />
                                                </div>
                                        </div>
                                        <div class="row form-group">
                                                <label class="col-md-2 control-label">การอนุญาตใช้งาน :</label>
                                                <div class="col-md-4  ">
                                                   <dx:ASPxCheckBox runat="server" ID="chkAllow" Text="อนุญาต" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxCheckBox runat="server" ID="chkDisallow" Text="ระงับการใช้งาน" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxCheckBox runat="server" ID="chkDisabled" Text="Black List" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxCheckBox>
                                                </div>

                                                <label class="col-md-2 control-label">ประเภทสัญญาจ้าง :</label>
                                                <div class="col-md-4  ">
                                                   <asp:RadioButtonList ID="radSpot" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                                </div>
                                            </div>
                                        <div class="row form-group">
                                                <label class="col-md-2 control-label">ค้นหาข้อมูลพนักงาน :</label>
                                                <div class="col-md-4  ">
                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtSearch"  placeholder="รหัสพนักงาน,ชื่อ-นามสกุล,หมายเลขบัตรประชาชน" />
                                                </div>
                                             <%--<label class="col-md-2 control-label"> </label>--%>
                                                <div class="col-md-6  ">
                                                   <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function (s, e) 
                                                        {if(!ASPxClientEdit.ValidateGroup('add')) return false;
                                                        xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnClaer" runat="server"  CssClass="dxeLineBreakFix" Text="เคลียร์" AutoPostBack="false">
                                                        <ClientSideEvents Click="function (s, e) 
                                                        {xcpn.PerformCallback('Claer');}"></ClientSideEvents>
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="imbadd" runat="server" SkinID="_add"  CssClass="dxeLineBreakFix">
                                                      <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('add')}" />
                                                    </dx:ASPxButton>

                                                </div>
                                            </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 text-right">
                                                <asp:Label Text="" ID="lblCount" ForeColor="Red" runat="server" /></div>
                                            </div>
                                        <div class="">
                                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" SkinID="_gvw" Width="100%" KeyFieldName="SEMPLOYEEID">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="รหัสพนักงาน" FieldName="SEMPLOYEEID" CellStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" Width="7%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ชื่อ - นามสกุล" Width="13%" HeaderStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHyperLink runat="server" ID="hplSNAME" Cursor="pointer" Text='<%# Eval("SNAME") %>'>
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('view;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxHyperLink>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="เลขบัตรประชาชน" FieldName="PERS_CODE" CellStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" Width="7%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ชื่อบริษัทผู้ขนส่ง" FieldName="SABBREVIATION" CellStyle-HorizontalAlign="Left"
                                                    HeaderStyle-HorizontalAlign="Center" Width="13%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ตำแหน่ง" FieldName="SEMPTPYE" CellStyle-HorizontalAlign="Left"
                                                    HeaderStyle-HorizontalAlign="Center" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn> 
                                      <dx:GridViewDataDateColumn Caption="วันที่อัพเดท<br>ล่าสุดใน SAP" FieldName="DUPDATESAP"
                                                    HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy" CellStyle-HorizontalAlign="Center" Width="10%">
                                                    
                                                </dx:GridViewDataDateColumn>          <dx:GridViewDataColumn Caption="ผู้เปลี่ยนแปลง<br>ข้อมูลใน SAP" FieldName="SUPDATESAP"
                                                    Visible="false" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"
                                                    Width="9%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                               
                                                <dx:GridViewDataDateColumn Caption="วันที่อัพเดท<br>ล่าสุดใน TMS" FieldName="DUPDATEEMP"
                                                    HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy" CellStyle-HorizontalAlign="Center" Width="10%">
                                                    
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataColumn Caption="ผู้เปลี่ยนแปลง<br>ข้อมูลใน TMS" FieldName="SUPDATEEMP"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="8%" 
                                                    Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทสัญญาจ้าง" FieldName="SPOT_STATUS_NAME"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="8%" 
                                                    Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="เลขที่สัญญา (จ้างพิเศษ)" FieldName="SCONTRACTNO"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="10%" 
                                                    Visible="true">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="การจ้างงาน" FieldName="" HeaderStyle-HorizontalAlign="Center" 
                                                    Width="5%" CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="btn" runat="server" AutoPostBack="false" CausesValidation="False" ClientEnabled="false"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                            CssClass="dxeLineBreakFix" Width="16px" Height="16px">
                                                            <%--<ClientSideEvents Click="function (s, e) {

                                                                dxConfirm('แจ้งเตือน', 'ท่านต้องการเปลี่ยนงานใช้งานใช่หรอไม่' ,function(ss,ee){ dxPopupConfirm.Hide(); 
                                                                if(!xcpn.InCallback()) 
                                                                xcpn.PerformCallback('ChkInUse;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                                                                } ,function(ss,ee){ dxPopupConfirm.Hide(); });
                              
                                                                }" />--%>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                               
                                                <dx:GridViewBandColumn  Caption="การอนุญาตใช้งาน" HeaderStyle-HorizontalAlign="Center">
                                            <Columns>
                                                
                                                <dx:GridViewDataColumn  FieldName="CACTIVE" CellStyle-HorizontalAlign="Center" Settings-AllowHeaderFilter="False" Caption=" "  
                                                    HeaderStyle-HorizontalAlign="Center" Width="10%">
                                                    <Settings />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="7%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"  Caption=" "   
                                                    CellStyle-HorizontalAlign="Center" >
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="imbeditStatus" runat="server" Text="เปลี่ยน" CausesValidation="False" OnClick="imbeditStatus_Click" CommandArgument='<%# Container.ItemIndex %>' AutoPostBack="true">
                                                            <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('editStatus;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                </Columns>
                                                </dx:GridViewBandColumn>
                                                
                                                <dx:GridViewDataColumn Width="7%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="ข้อมูล">

                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>

                                                <dx:GridViewDataColumn Width="7%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="เอกสาร">

                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="imbSearchDoc" runat="server" SkinID="_search" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('searchDoc;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="CHECKIN" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NOTFILL" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="INUSE" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager PageSize="50">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                        </div>
                                        </div>


                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
