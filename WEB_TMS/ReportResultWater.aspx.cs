﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;

public partial class ReportResultWater : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static int nNextYeatCheckWater = 3;
    private static DataTable dtMainData = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            ListData();

        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบรับรอง")
        {
            ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
            txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
            ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
            txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
        }
       

    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void ListData()
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = " AND REQ.VEH_NO||REQ.TU_NO||VEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%'";
        }

        if (edtStart.Value != null && edtEnd.Value != null)
        {
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;

            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Condition += " AND (TRUNC(TIC.EXAMDATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
        }
        else
        {
            lblsTail.Text = " - ";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
, TO_CHAR(add_months(TIC.EXAMDATE,6516),'dd/MM/yyyy') as APPOINTMENT_DATE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
,NVL(REQ.CCHECKING_WATER,'x') as CCHECKING_WATER
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

//    private void ListReport(string Type)
//    {

//        string Condition = "";
//        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
//        //{
//        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
//        //}
//        //else
//        //{

//        //}



//        string QUERY = @"SELECT ROWNUM||'.' as NO,REQ.REQUEST_ID
//,REQ.VEH_NO
//,REQ.TU_NO
//,TCK.SCAR_NUM,RQT.REQTYPE_NAME,CAS.CAUSE_NAME
//, TO_CHAR(add_months(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),6516),'dd/MM/yyyy') as APPOINTMENT_DATE
//,STU.STATUSREQ_NAME
//FROM TBL_REQUEST REQ
//LEFT JOIN TTRUCK TCK
//ON TCK.STRUCKID =  NVL(REQ.TU_ID,REQ.VEH_ID)
//LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
//LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
//LEFT JOIN TBL_STATUSREQ STU ON STU.STATUSREQ_ID = REQ.STATUS_FLAG
//" + Condition + "";

//        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
//        //if (dt.Rows.Count > 0)
//        //{
//        //    gvw.DataSource = dt;
//        //    gvw.DataBind();
//        //}


//        ReportOperationMonth report = new ReportOperationMonth();

//        #region function report

//        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
//        //{
//        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
//        //    DateTime DMONTH = DateTime.Parse(Date);
//        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
//        //}
//        //else
//        //{
//        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
//        //}

//        report.Name = "รายงานผลการดำเนินงานประจำเดือน";
//        report.DataSource = dt;
//        string fileName = "รายงานผลการดำเนินงานประจำเดือน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

//        MemoryStream stream = new MemoryStream();


//        string sType = "";
//        if (Type == "P")
//        {
//            report.ExportToPdf(stream);
//            Response.ContentType = "application/pdf";
//            sType = ".pdf";
//        }
//        else
//        {
//            report.ExportToXls(stream);
//            Response.ContentType = "application/xls";
//            sType = ".xls";
//        }


//        Response.AddHeader("Accept-Header", stream.Length.ToString());
//        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
//        Response.AddHeader("Content-Length", stream.Length.ToString());
//        Response.ContentEncoding = System.Text.Encoding.ASCII;
//        Response.BinaryWrite(stream.ToArray());
//        Response.End();
//        #endregion
//    }

//    protected void btnPDF_Click(object sender, EventArgs e)
//    {
//        ListReport("P");
//    }

//    protected void btnExcel_Click(object sender, EventArgs e)
//    {
//        ListReport("E");
//    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string REQID = txtReqID.Text;
        if(!string.IsNullOrEmpty(txtReqID.Text))
        {
            ListDataToPage(REQID);

            if (txtStatus.Text == "Y")
            {
                ListReport(REQID);
            }
            else
            {
                ListReportNoPass(REQID);
            }
        }
       
    }

    private void ListDataToPage(string sREQID)
    {
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRK.DWATEREXPIRE,TRK.SCARTYPEID,TRQ.RESULT_CHECKING_DATE,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,TRQ.CHECKWATER_NVERSION,
                        TVD.SVENDORID,TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME,TRK.NWHEELS,TRQ.TOTLE_SLOT,TRK.DPREV_SERV,TRK.DLAST_SERV,TRKN.SCAR_NUM,INC.DATE_CREATED
                        FROM TBL_REQUEST TRQ 
                        LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = TRQ.STRUCKID
                        LEFT JOIN TTRUCK TRKN ON TRKN.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        LEFT JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        LEFT JOIN (SELECT REQ_ID, MAX(NVL( DATE_UPDATED, DATE_CREATED)) as DATE_CREATED FROM TBL_INNER_CHECKINGS GROUP BY REQ_ID)INC
                        ON TRQ.REQUEST_ID = INC.REQ_ID
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));

    }

    private void ListReport(string sReqID)
    {
        string sTruckID_CheckWater = "";
        switch (dtMainData.Rows[0]["SCARTYPEID"] + "")
        {
            case "0": sTruckID_CheckWater = dtMainData.Rows[0]["VEH_ID"] + ""; break; // 10 ล้อ
            case "3": sTruckID_CheckWater = dtMainData.Rows[0]["TU_ID"] + ""; break; // หัวลาก
        }


        #region data table //

//        string sql1 = @"SELECT TCC.REQUEST_ID,TCC.COMPART_NO, TIC.PAN_LEVEL,NVL(TRC.NCAPACITY,REQ.CAPACITY) as NCAPACITY,TIC.HEIGHT1,TIC.HEIGHT2,
//                                CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
//                                TCC.SEAL_NO,
//                                CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
//                                CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
//                                TCC.NEW_SEAL_NO
//                                 FROM TBL_CHECKING_COMPART TCC
//                                 INNER JOIN TBL_INNER_CHECKINGS TIC ON TCC.REQUEST_ID = TIC.REQ_ID AND TCC.COMPART_NO = TIC.COMPART_NO
//                                 LEFT JOIN TTRUCK_COMPART TRC ON  TCC.STRUCKID  = TRC.STRUCKID AND  TIC.COMPART_NO = TRC.NCOMPARTNO AND TIC.PAN_LEVEL = TRC.NPANLEVEL
//                                 LEFT JOIN TBL_REQSLOT REQ ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO   AND TIC.PAN_LEVEL = REQ.LEVEL_NO
//                                 WHERE REQUEST_ID = '{0}'
//                                 ORDER BY TCC.COMPART_NO,TIC.PAN_LEVEL ASC";

        string sql1 = @"SELECT REQ.REQUEST_ID,REQ.SLOT_NO as COMPART_NO, REQ.LEVEL_NO as PAN_LEVEL,REQ.CAPACITY as NCAPACITY  ,TIC.HEIGHT1,TIC.HEIGHT2,
CASE WHEN TCC.EMER_VALUE = '0' THEN '' WHEN TCC.EMER_VALUE = '1' THEN '*' ELSE '-' END AS EMER_VALUE,
TCC.SEAL_NO,
CASE WHEN TCC.SEAL_ERR = '0' THEN 'ไม่ชำรุด'  WHEN TCC.SEAL_ERR = '1' THEN 'ชำรุด' ELSE '-' END AS SEAL_ERR ,
CASE WHEN 1 = 1 THEN 'มว.1' END AS LEAD_SEAL,
TCC.NEW_SEAL_NO
FROM TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART TCC  ON TCC.REQUEST_ID =  REQ.REQUEST_ID AND TCC.COMPART_NO = REQ.SLOT_NO  -- AND TCC. = REQ.LEVEL_NO
LEFT JOIN TBL_INNER_CHECKINGS TIC ON REQ.REQUEST_ID = TIC.REQ_ID AND REQ.SLOT_NO = TIC.COMPART_NO AND TIC.PAN_LEVEL = REQ.LEVEL_NO
 WHERE REQ.REQUEST_ID = '{0}'
ORDER BY REQ.SLOT_NO,REQ.LEVEL_NO  ASC";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, string.Format(sql1, CommonFunction.ReplaceInjection(sReqID)));



        var query = dt.AsEnumerable().Select(s => new { 
            COMPART_NO = s.Field<decimal>("COMPART_NO"), 
            PAN_LEVEL = s.Field<decimal>("PAN_LEVEL"), 
            NCAPACITY = s["NCAPACITY"] != DBNull.Value ? s.Field<decimal>("NCAPACITY") : 0 
        }).ToList();
        decimal nSum_Capacity = 0;
        if (query.Count > 0)
        {
            var query2 = query.GroupBy(g => new { g.COMPART_NO }).Select(s => new { s.Key.COMPART_NO, NCAPACITY = s.Max(x => x.NCAPACITY) }).ToList();
            nSum_Capacity = query2.Sum(s => s.NCAPACITY);
        }

     
        #endregion

        rpt_InnerChecking_FM001 report = new rpt_InnerChecking_FM001();

        // parameter
        //SENGINE หมายเลขเคื่อง


        // set control
        ((XRLabel)report.FindControl("xrlblsDay", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsMonth", true)).Text = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("rxlblsYear", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));

        string[] arrNo1 = sReqID.Split('-');
        ((XRLabel)report.FindControl("xrlblNo1", true)).Text = arrNo1[arrNo1.Length - 1];
        ((XRLabel)report.FindControl("xrlblNo2", true)).Text = DateTime.Now.ToString("yy", new CultureInfo("th-TH"));

        //
        string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
 WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtPara1 = new DataTable();
        dtPara1 = CommonFunction.Get_Data(conn, string.Format(sql2, sReqID));

        //sTruckID_CheckWater เก็บ id รถที่ใช้วัดน้ำ
        string sql3 = @"SELECT TRK.*,TRKT.SCARTYPENAME FROM TTRUCK  TRK LEFT JOIN TTRUCKTYPE TRKT ON  TRK.CARCATE_ID = TRKT.SCARTYPEID WHERE  TRK.STRUCKID = '" + CommonFunction.ReplaceInjection(sTruckID_CheckWater) + "'";
        DataTable dtTruck = new DataTable();
        dtTruck = CommonFunction.Get_Data(conn, sql3);

        //เวลา
        string sql4 = @"SELECT * FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NID";
        DataTable dtTime = new DataTable();
        dtTime = CommonFunction.Get_Data(conn, sql4);
        decimal nTemp = 0;

        string vStartH = "", vStartM = "", vEndH = "", vEndM = "";


        //ตรวจสอบจากคำขอวัดน้ำ
        string sqlslot = @"SELECT * FROM TBL_REQSLOT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY SLOT_NO DESC";
        DataTable dtNslot = CommonFunction.Get_Data(conn, sqlslot);


        if (dtPara1.Rows.Count == 1 && dtTruck.Rows.Count == 1)
        {
            DataRow dr1 = null, dr2 = null, dr3 = null;

            dr1 = dtPara1.Rows[0];
            dr2 = dtTruck.Rows[0];
            dr3 = dtMainData.Rows[0];

            report.Parameters["sCompName"].Value = dr1["SABBREVIATION"] + "";
            report.Parameters["sTU_NO"].Value = dr1["TU_NO"] + "" != "" ? dr1["TU_NO"] + "" : "-";
            report.Parameters["sTruckID"].Value = sTruckID_CheckWater;
            report.Parameters["sSerialNumber"].Value = dr2["SENGINE"] + "" != "" ? dr2["SENGINE"] + "" : "-";
            report.Parameters["sTU_CHASSIS"].Value = dr1["TU_CHASSIS"] + "" != "" ? dr1["TU_CHASSIS"] + "" : "-";
            report.Parameters["sCarType"].Value = dr2["SCARTYPENAME"] + "";
            report.Parameters["sVEH_NO"].Value = dr1["VEH_NO"] + "" != "" ? dr1["VEH_NO"] + "" : "-";
            report.Parameters["sBRAND"].Value = dr2["SBRAND"] + "" != "" ? dr2["SBRAND"] + "" : "-";
            report.Parameters["sVEH_CHASSIS"].Value = dr1["VEH_CHASSIS"] + "" != "" ? dr1["VEH_CHASSIS"] + "" : "-";
            report.Parameters["nNumSlot"].Value = dtNslot.Rows[0]["SLOT_NO"] + "";
            nTemp = decimal.TryParse(dr2["NTOTALCAPACITY"] + "", out nTemp) ? nTemp : 0;
            report.Parameters["nSum_Capacity"].Value = dr2["NTOTALCAPACITY"] + "" != "" ? (nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(0)) : "0") : "-";
            report.Parameters["nWeight_Sum"].Value = dr2["NWEIGHT"] + "" != "" ? dr2["NWEIGHT"] + "" : "-";
            report.Parameters["sNB"].Value = dr2["NLOAD_WEIGHT"] + "" != "" ? dr2["NLOAD_WEIGHT"] + "" : "-";
            report.Parameters["dLastCheckWater"].Value = dr2["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dr2["DLAST_SERV"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";   // dr1["TU_NO"] + "";
            report.Parameters["sHeight_H2T"].Value = dr2["NTANK_HIGH_HEAD"] + "";
            report.Parameters["sHeight_EndT"].Value = dr2["NTANK_HIGH_TAIL"] + "";
            report.Parameters["sTypeMaterailTank"].Value = dr2["STANK_MATERAIL"] + "" != "" ? dr2["STANK_MATERAIL"] + "" : "-";
            report.Parameters["sAddress"].Value = dr1["SNO"] + "  " + dr1["SDISTRICT"] + "  " + dr1["SREGION"] + "  จังหวัด " + dr1["SPROVINCE"] + "  " + dr1["SPROVINCECODE"] + "";
            report.Parameters["dService"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";


            string sShow = "";
            decimal nTempH = 0, nTempM = 0;
            foreach (DataRow row in dtTime.Rows)
            {
                nTempH = 0; nTempM = 0;
                nTempH = decimal.TryParse(row["NSTART_HOUR"] + "", out nTempH) ? nTempH : 0;
                nTempM = decimal.TryParse(row["NSTART_MINUTE"] + "", out nTempM) ? nTempM : 0;
                if (nTempH < 10)
                {
                    sShow = "0" + nTempH + ":";
                }
                else
                {
                    sShow = nTempH + ":";
                }

                if (nTempM < 10)
                {
                    sShow = sShow + "0" + nTempM;
                }
                else
                {
                    sShow = sShow + nTempM;
                }


                switch (row["NID"] + "")
                {
                    case "1": //เวลานำรถเข้าตรวจ
                        report.Parameters["sTime_dService"].Value = sShow;
                        break;
                    case "2"://เวลานำรถเข้าตรวจสภาพภายนอก/ใน
                        break;
                    case "3": //เวลาเริ่มต้นลงน้ำ
                        report.Parameters["sTime_DownWater"].Value = sShow;
                        vStartH = row["NSTART_HOUR"] + "";
                        vStartM = row["NSTART_MINUTE"] + "";
                        break;
                    case "4": //หยุดน้ำ/วัดแป้น
                        report.Parameters["sTime_StopCheckWater"].Value = sShow;
                        break;
                    case "5": //สูบน้ำคืนกลับ
                        report.Parameters["sTime_ReturnWater"].Value = sShow;
                        vEndH = row["NSTART_HOUR"] + "";
                        vEndM = row["NSTART_MINUTE"] + "";
                        break;
                }
            }

            string st1 = vStartH + ":" + vStartM;
            string st2 = vEndH + ":" + vEndM;
            int hours = 0, minutes = 0;

            if (!string.IsNullOrEmpty(vStartH) && !string.IsNullOrEmpty(vStartM) && !string.IsNullOrEmpty(vEndH) && !string.IsNullOrEmpty(vEndM))
            {
                TimeSpan timeStart = TimeSpan.Parse(st1);
                TimeSpan timeEnd = TimeSpan.Parse(st2);

                TimeSpan difference = timeEnd - timeStart;

                hours = difference.Hours;
                minutes = difference.Minutes;
            }

            report.Parameters["nSumTimeChecking"].Value = hours + "." + minutes;

            string sDateNextCheckWater = "";
            DateTime dNextCheckWater;
            if (!string.IsNullOrEmpty(dr2["DNEXT_SERV"] + ""))
            {
                dNextCheckWater = Convert.ToDateTime(dr2["DNEXT_SERV"] + "", new CultureInfo("th-TH"));
                if (dNextCheckWater.Year == DateTime.Now.Year) // ยังไม่ได้อัพเดทข้อมูลวัดน้ำใน TTRUCK
                {
                    sDateNextCheckWater = dNextCheckWater.AddYears(nNextYeatCheckWater).ToString("dd MMM yyyy");
                }
                else
                {
                    sDateNextCheckWater = dNextCheckWater.ToString("dd MMM yyyy");
                }
            }
            else
            {
                sDateNextCheckWater = DateTime.Now.ToString("dd MMM yyyy");
            }

            report.Parameters["dNextCheckWater"].Value = sDateNextCheckWater;
            report.Parameters["dCompletCheckWater"].Value = dr3["SERVICE_DATE"] + "" != "" ? Convert.ToDateTime(dr3["SERVICE_DATE"] + "", new CultureInfo("th-Th")).ToString("dd MMM yyyy") : "-";
            report.Parameters["nSumCapacity"].Value = nSum_Capacity.ToString(SystemFunction.CheckFormatNuberic(0));

        }


        string sUSER = @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME,SUID.SPOSITION FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'  AND ISACTIVE_FLAG = 'Y'";
        DataTable dtUSER = CommonFunction.Get_Data(conn, sUSER);
        if (dtUSER.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrlblFullNameControl", true)).Text = dtUSER.Rows[0]["SNAME"] + "";
            ((XRLabel)report.FindControl("xrblbPositionControl", true)).Text = dtUSER.Rows[0]["SPOSITION"] + "";
        }


        ((XRLabel)report.FindControl("xrlblsDay2", true)).Text = DateTime.Now.Day + "";
        ((XRLabel)report.FindControl("xrlblsMonth2", true)).Text = DateTime.Now.ToString("MMM", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblsYear2", true)).Text = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
        ((XRLabel)report.FindControl("xrlblComment", true)).Text = GetComment(sReqID);


        report.Name = "รายงานวัดน้ำ";
        report.DataSource = dt;
        string fileName = "รายงานวัดน้ำ_FM-มว.-001_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private void ListReportNoPass(string sReqID)
    {
        string CarType = "";
        string RegisID = "";
        string CHASSIS = "";
        string NWHEALL = "";
        string TOTLE_SLOT = "";
        string TOTALCAP = "";
        string DPREV_SERV = "";
        string DLAST_SERV = "";
        string SABBREVIATION = "";
        string SCAR_NUM = "";
        string RESULT_CHECKING_DATE = "";
        string DATE_CREATED = "";

        if (dtMainData.Rows.Count > 0)
        {
            //CarType = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? "หัว" : "หาง");
            RegisID = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_NO"] + "" : dtMainData.Rows[0]["TU_NO"] + "");
            CHASSIS = (dtMainData.Rows[0]["SCARTYPEID"] + "" == "0" ? dtMainData.Rows[0]["VEH_CHASSIS"] + "" : dtMainData.Rows[0]["TU_CHASSIS"] + "");
            NWHEALL = dtMainData.Rows[0]["NWHEELS"] + "";
            TOTLE_SLOT = dtMainData.Rows[0]["TOTLE_SLOT"] + "";
            TOTALCAP = dtMainData.Rows[0]["TOTLE_CAP"] + "";
            DPREV_SERV = dtMainData.Rows[0]["DPREV_SERV"] + "";
            DLAST_SERV = dtMainData.Rows[0]["DLAST_SERV"] + "";
            SABBREVIATION = dtMainData.Rows[0]["SABBREVIATION"] + "";
            SCAR_NUM = dtMainData.Rows[0]["SCAR_NUM"] + "";
            RESULT_CHECKING_DATE = dtMainData.Rows[0]["RESULT_CHECKING_DATE"] + "";
            DATE_CREATED = dtMainData.Rows[0]["DATE_CREATED"] + "";
        }

        rpt_nopasswatercheck report = new rpt_nopasswatercheck();

        // set control
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = sReqID;
        ((XRLabel)report.FindControl("xrLabel4", true)).Text = "วันที่ " + DateTime.Now.Day + " เดือน " + DateTime.Now.ToString("MMMM", new CultureInfo("th-TH")) + " พ.ศ. " + DateTime.Now.ToString("yyyy", new CultureInfo("th-TH")) + "";
        ((XRLabel)report.FindControl("xrLabel10", true)).WidthF = 364;
        ((XRLabel)report.FindControl("xrLabel10", true)).Text = SABBREVIATION;
        ((XRLabel)report.FindControl("xrLabel12", true)).Text = SCAR_NUM;
        ((XRLabel)report.FindControl("xrLabel14", true)).Text = RegisID;
        ((XRLabel)report.FindControl("xrLabel16", true)).Text = CHASSIS;
        ((XRLabel)report.FindControl("xrLabel18", true)).Text = NWHEALL;
        ((XRLabel)report.FindControl("xrLabel20", true)).Text = TOTLE_SLOT;
        ((XRLabel)report.FindControl("xrLabel22", true)).Text = TOTALCAP;
        ((XRLabel)report.FindControl("xrLabel24", true)).Text = !string.IsNullOrEmpty(DPREV_SERV) ? DateTime.Parse(DPREV_SERV).Day + " " + DateTime.Parse(DPREV_SERV).ToString("MMMM", new CultureInfo("th-TH")) + " " + DateTime.Parse(DPREV_SERV).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel26", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel28", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel30", true)).Text = !string.IsNullOrEmpty(RESULT_CHECKING_DATE) ? DateTime.Parse(RESULT_CHECKING_DATE).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel33", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).Day + " " : "";
        ((XRLabel)report.FindControl("xrLabel35", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("MMM", new CultureInfo("th-TH")) + "" : "";
        ((XRLabel)report.FindControl("xrLabel37", true)).Text = !string.IsNullOrEmpty(DATE_CREATED) ? DateTime.Parse(DATE_CREATED).ToString("yyyy", new CultureInfo("th-TH")) + "" : "";


        DataTable dtName = CommonFunction.Get_Data(conn, @"SELECT TIC.REQUEST_ID , TIC.USER_EXAMINER,SUID.SFIRSTNAME||' '||SUID.SLASTNAME as SNAME FROM TBL_TIME_INNER_CHECKINGS TIC
LEFT JOIN TUSER SUID
ON TIC.USER_EXAMINER = SUID.SUID WHERE TIC.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' AND ISACTIVE_FLAG = 'Y'");
        if (dtName.Rows.Count > 0)
        {
            ((XRLabel)report.FindControl("xrLabel44", true)).Text = dtName.Rows[0]["SNAME"] + "";
        }


        string SQL_NOPASS = @"SELECT CCP.REQUEST_ID,CCP.COMPART_NO 
,CASE WHEN CCP.SEAL_ERR = '1' THEN '/'  WHEN CCP.SEAL_ERR = '0' THEN 'X' ELSE '' END  as  SEAL_ERR
,CASE WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '1' AND CCP.MANHOLD_ERR2 = '0' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '1' THEN '/'
WHEN CCP.MANHOLD_ERR1 = '0' AND CCP.MANHOLD_ERR2 = '0' THEN 'X'
ELSE '' END AS MANHOLD
,CASE WHEN INC.RESULT = '1' THEN '/' WHEN INC.RESULT = '0' THEN 'X'    ELSE '' END  as  RESULT
,CASE WHEN INC.PAN_ERR = '1' THEN '/' WHEN INC.PAN_ERR = '0' THEN 'X' ELSE '' END  as  PAN_ERR
,CASE WHEN INC.PASSSTANDARD = '1' THEN '/' WHEN INC.PASSSTANDARD = '0' THEN 'X' ELSE '' END  as  PASSSTANDARD
FROM 
(
SELECT REQ.REQUEST_ID ,REQ.SLOT_NO as COMPART_NO,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2 FROM  TBL_REQSLOT REQ
LEFT JOIN  TBL_CHECKING_COMPART ds  ON REQ.REQUEST_ID = DS.REQUEST_ID AND REQ.SLOT_NO = DS.COMPART_NO
WHERE REQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
GROUP BY  REQ.REQUEST_ID ,REQ.SLOT_NO ,DS.SEAL_ERR,DS.MANHOLD_ERR1,DS.MANHOLD_ERR2 
)
CCP
LEFT JOIN 
(
    SELECT REQ_ID,COMPART_NO
    ,MAX(PAN_LEVEL) as PAN_LEVEL
    ,PAN_ERR
    ,PASSSTANDARD
    ,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END AS RESULT
    --,(HEIGHT1 - OLD_HEIGHT1)  AS CAL1
    --,(HEIGHT2 - OLD_HEIGHT2)  AS CAL2
    FROM TBL_INNER_CHECKINGS
    GROUP BY REQ_ID,COMPART_NO,PAN_ERR,PASSSTANDARD,CASE WHEN (HEIGHT1 - OLD_HEIGHT1) > 3 OR (HEIGHT2 - OLD_HEIGHT2) > 3 THEN '/' ELSE 'X' END
)INC
 ON INC.REQ_ID = CCP.REQUEST_ID AND INC.COMPART_NO = CCP.COMPART_NO
WHERE CCP.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
ORDER BY CCP.COMPART_NO ASC";

        DataTable dtNopass = CommonFunction.Get_Data(conn, SQL_NOPASS);
        DataRow dr = null;
        for (int i = dtNopass.Rows.Count; i <= 9; i++)
        {
            dr = dtNopass.NewRow();
            dr[0] = ""; // or you could generate some random string.
            dtNopass.Rows.Add(dr);
        }
        report.Name = "แจ้งสาเหตุการตรวจสอบสภาพรถ/การตวงวัดน้าและตรวจวัดระดับแป้นที่ไม่ผ่าน";
        report.DataSource = dtNopass;
        string fileName = "รายงานวัดน้ำ_FM-มว.-006_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(conn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }
}