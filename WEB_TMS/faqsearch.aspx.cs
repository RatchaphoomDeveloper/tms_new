﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web;


public partial class faqsearch : System.Web.UI.Page
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    int defInt = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        xcpn.Callback += new DevExpress.Web.ASPxClasses.CallbackEventHandlerBase(xcpn_Callback);
        xcpn.Load += new EventHandler(xcpn_Load);


        Cache.Remove(sds.CacheKeyDependency);
        Cache[sds.CacheKeyDependency] = new object();
        sds.Select(new System.Web.UI.DataSourceSelectArguments());


        sds.DataBind();
        gvw.DataBind();
    }


    void xcpn_Load(object sender, EventArgs e)
    {

    }

    void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');
      
        switch (param[0] + "")
        {
            case "Detail":
                int VisibleIndex = int.TryParse(param[1] + "", out defInt) ? int.Parse(param[1] + "") : 0;
                dynamic data = gvw.GetRowValues(VisibleIndex, "FAQ_ID");
                //Session["FID"] = data;

                xcpn.JSProperties["cpRedirectTo"] = "faqdescription.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data));
                break;
            default: break;
        }
    }


    protected void cboQuest_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        FAQ.SelectCommand = @"SELECT FAQ_ID , FAQ_QUEST FROM (SELECT ROW_NUMBER()OVER(ORDER BY FAQ_ID) AS RN , FAQ_ID , FAQ_QUEST
          FROM TFAQ WHERE FAQ_QUEST LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        FAQ.SelectParameters.Clear();
        FAQ.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        FAQ.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        FAQ.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = FAQ;
        comboBox.DataBind();

    }

    protected void cboQuest_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
}