﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Require_Lst.aspx.cs" Inherits="Require_Lst" %>

<%@ Register Src="~/UserControl/cButtonDelete.ascx" TagPrefix="uc1" TagName="cButtonDelete" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                    <label class="col-md-4 control-label">หน้าจอ</label>
                    <div class="col-md-4">
                        <asp:DropDownList runat="server" ID="ddlMenu" OnSelectedIndexChanged="ddlMenu_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:Panel ID="plLevel1" runat="server">
                    <div class="row form-group">
                        <label class="col-md-4 control-label">
                            <asp:Label Text="" ID="lblLevel1" runat="server" /></label>
                        <asp:HiddenField ID="hidMREQUIRELEVELIDLevel1" runat="server" />
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="ddlLevel1" CssClass="form-control " OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="plLevel2">
                    <div class="row form-group">
                        <label class="col-md-4 control-label">
                            <asp:Label Text="" ID="lblLevel2" runat="server" /></label>
                        <asp:HiddenField ID="hidMREQUIRELEVELIDLevel2" runat="server" />
                        <div class="col-md-4">
                            <asp:DropDownList runat="server" ID="ddlLevel2" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" CssClass="form-control" >
                            </asp:DropDownList>
                        </div>

                    </div>
                </asp:Panel>

                <asp:Panel runat="server" ID="plLevel3">
                    <div class="row form-group">
                        <label class="col-md-4 control-label">
                            <asp:Label Text="Field" ID="lblLevel3" runat="server" /></label></label>
                        <asp:HiddenField ID="hidMREQUIRELEVELIDLevel3" runat="server" />
                <div class="col-md-4 ">
                    <asp:DropDownList runat="server" ID="ddlLevel3" OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" CssClass="form-control" >
                    </asp:DropDownList>
                </div>
                    </div>
                </asp:Panel>

                <asp:Panel runat="server" ID="plLevel4">
                    <div class="row form-group">
                        <label class="col-md-4 control-label">
                            <asp:Label Text="Field" ID="lblLevel4" runat="server" /></label></label>
                        <asp:HiddenField ID="hidMREQUIRELEVELIDLevel4" runat="server" />
                <div class="col-md-4 ">
                    <asp:DropDownList runat="server" ID="ddlLevel4" OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" CssClass="form-control" >
                    </asp:DropDownList>
                </div>
                    </div>
                </asp:Panel>


                <div class="row form-group">
                    <div class="col-md-12">
                        <center>
                            <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" runat="server" id="tabtest">
        <li class="active" id="Tab1" runat="server"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab">Require Field</a></li>
        <li class="" id="Tab2" runat="server"><a href="#TabProcess" data-toggle="tab" aria-expanded="false"
            runat="server" id="ProcessTab">Require File</a></li>
    </ul>
                        <div    class="tab-content"  >
                            <div class="tab-pane fade active in" id="TabGeneral">
                            <asp:GridView runat="server" ID="gvRequire" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="M_COMPLAIN_REQUIRE_FIELD" OnRowDataBound="gvRequire_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="M_COMPLAIN_REQUIRE_FIELD" Visible="false" />
                                <asp:BoundField DataField="DESCRIPTION" HeaderText="ชื่อ Field" />
                                <asp:BoundField DataField="DETAIL1" HeaderText="" />
                                <asp:BoundField DataField="DETAIL2" HeaderText="" />
                                <asp:BoundField DataField="DETAIL3" HeaderText="" />
                                <asp:BoundField DataField="DETAIL4" HeaderText="" />
                                <asp:TemplateField HeaderText="ประเภท">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlREQUIRE_TYPE_NAME" CssClass="form-control"  onchange='<%# "IndexChanged(" + Eval("M_COMPLAIN_REQUIRE_FIELD") + ",this);"%>'>
                                            <asp:ListItem Text="REQUIRE" Value="1" />
                                            <asp:ListItem Text="OPTIONAL" Value="2" />
                                            <asp:ListItem Text="DISABLE" Value="3" />
                                        </asp:DropDownList>
                                         <div style="display:none">
                                            <input id='<%# "btnSave" + Eval("M_COMPLAIN_REQUIRE_FIELD") %>' type="button" value="บันทึก" data-toggle="modal"  class="btn btn-md bth-hover btn-info" data-target='<%# "#ModalConfirmBeforeSave" + Eval("M_COMPLAIN_REQUIRE_FIELD") %>'/>
                                            </div>
                                        <uc1:ModelPopup runat="server" ID="mpConfirmSave"  IDModel='<%# "ModalConfirmBeforeSave" + Eval("M_COMPLAIN_REQUIRE_FIELD") %>' IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการเปลี่ยน ประเภทการทำงาน" TextDetail="คุณต้องการเปลี่ยน ประเภทการทำงานใช่หรือไม่ ?"  CommandArgument='<%#Eval("M_COMPLAIN_REQUIRE_FIELD") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID1" Visible="false" />
                                <asp:BoundField DataField="ID2" Visible="false" />
                                <asp:BoundField DataField="ID3" Visible="false" />
                                <asp:BoundField DataField="ID4" Visible="false" />
                            </Columns>
                        </asp:GridView>
                                </div>
                            <div class="tab-pane " id="TabProcess">
                            <asp:GridView runat="server" ID="gvRequireFile" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="REQUEST_ID" OnRowDataBound="gvRequireFile_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="REQUEST_ID" Visible="false" />
                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                <asp:TemplateField HeaderText="ชื่อ File">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtUploadName" CssClass="form-control" Text='<%# Eval("UPLOAD_NAME") %>' />
                                        <asp:HiddenField runat="server" ID="hidUploadID" Value='<%# Eval("UPLOAD_ID") %>' />
                                        <asp:HiddenField runat="server" ID="hidID" Value='<%# Eval("REQUEST_ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EXTENTION">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtExtention" CssClass="form-control" Text='<%# Eval("EXTENTION") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MAX_FILE_SIZE">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtMaxFileSize" CssClass="form-control" Text='<%# Eval("MAX_FILE_SIZE") %>' MaxLength="2" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DETAIL1" HeaderText="" />
                                <asp:BoundField DataField="DETAIL2" HeaderText="" />
                                <asp:BoundField DataField="DETAIL3" HeaderText="" />
                                <asp:BoundField DataField="DETAIL4" HeaderText="" />
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="rblCactive" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="ใช้งาน" Value="1" />
                                            <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="กระบวนการ">
                                    <ItemTemplate>
                                        <input id='<%# "btnSave" + Eval("REQUEST_ID") %>' type="button" value="บันทึก" data-toggle="modal"  class="btn btn-md bth-hover btn-info" data-target='<%# "#ModalConfirmBeforeSaveRequireFile" + Eval("REQUEST_ID") %>'/>
                                        <uc1:ModelPopup runat="server" ID="mpConfirmSaveFile"  IDModel='<%# "ModalConfirmBeforeSaveRequireFile" + Eval("REQUEST_ID") %>' IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSaveFile_ClickOK" TextTitle="ยืนยันการแก้ไข" TextDetail="คุณต้องการแก้ไขข้อมูลใช้หรือไม่ ?"  CommandArgument='<%# Container.DataItemIndex %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID1" Visible="false" />
                                <asp:BoundField DataField="ID2" Visible="false" />
                                <asp:BoundField DataField="ID3" Visible="false" />
                                <asp:BoundField DataField="ID4" Visible="false" />
                            </Columns>
                        </asp:GridView>
                            </div>
                            </div>
                        
                        
                    </div>
                </div>
            </div>
           <asp:HiddenField ID="hidRequireTypeName" ClientIDMode="Static" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function IndexChanged(e,s) {
            $('#hidRequireTypeName').val(s.value);
            $('#btnSave' + e).click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

