﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Data;
using System.Text;
using DevExpress.Web.ASPxPanel;
using System.IO;
using DevExpress.Web.ASPxGridView.Export;
using DevExpress.XtraPrinting;

public partial class Defect_Point_Quarter : System.Web.UI.Page
{
    string _con = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    static string _Year;
    static string _Month;

    protected void Page_Load(object sender, EventArgs e)
    {

        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //bool chkurl = false;
            //if (Session["cPermission"] != null)
            //{
            //    string[] url = (Session["cPermission"] + "").Split('|');
            //    string[] chkpermision;
            //    bool sbreak = false;

            //    foreach (string inurl in url)
            //    {
            //        chkpermision = inurl.Split(';');
            //        if (chkpermision[0] == "26")
            //        {
            //            switch (chkpermision[1])
            //            {
            //                case "0":
            //                    chkurl = false;

            //                    break;
            //                case "1":
            //                    chkurl = true;

            //                    break;

            //                case "2":
            //                    chkurl = true;

            //                    break;
            //            }
            //            sbreak = true;
            //        }

            //        if (sbreak == true) break;
            //    }
            //}

            //if (chkurl == false)
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //}
            Session["dtDefectPointQuarter"] = null;


            DataTable dt = CommonFunction.Get_Data(_con, "SELECT TO_CHAR(DREDUCE,'YYYY') AS SYEAR FROM TREDUCEPOINT GROUP BY TO_CHAR(DREDUCE,'YYYY') ORDER BY TO_CHAR(DREDUCE,'YYYY') DESC");

            if (dt.Rows.Count > 0)
            {


                bool checkYear = false;
                string cYear = DateTime.Today.Year.ToString();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int Year = Convert.ToInt32(dt.Rows[i]["SYEAR"]);
                    cmbYear.Items.Add(Year + 543 + "", Year);
                    if ("" + dt.Rows[i]["SYEAR"] == cYear)
                    {
                        checkYear = true;
                    }
                }
                if (checkYear == false)
                {

                    cmbYear.Items.Insert(0, new ListEditItem(int.Parse(cYear) + 543 + "", cYear));
                }
                cmbYear.SelectedIndex = 0;
            }


            listTrimas();


            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            LogUser("26", "R", "เปิดดูข้อมูลหน้า สรุปผลการตัดคะแนน", "");
        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }


    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }


    protected void gvw_Init(object sender, EventArgs e)
    {
        string sPSSting = "";

        //if (("" + Session["ssType"]) != "1")
        //{

        //foreach (ListEditItem item in chkSType.SelectedItems)
        //{
        //    sPSSting += ",'" + item.Value + "'";
        //}


        //sPSSting = sPSSting.Length > 0 ? "and FieldName in (" + sPSSting.Remove(0, 1) + ")" : "and  1 = 2";

        //DataTable dt = CommonFunction.Get_Data(scon, "select FieldName from userdatatable where TableName = 'datatbl' " + sPSSting);

        DataTable dt;
        if (Session["dtDefectPointQuarter"] == null)
        {
            string sYear = cmbYear.Value+"" != "" ? cmbYear.Value + "" : DateTime.Now.Year+"";


            dt = CommonFunction.Get_Data(_con, @" SELECT SREDUCENAME,CASE WHEN SREDUCENAME = 'อุบัติเหตุ' OR SREDUCENAME = 'ข้อร้องเรียน' 
THEN SREDUCENAME ELSE SREDUCENAME ||  ' (' || MAX(nvl(NPOINT,0)) || ' คะแนน)' END  as SUMPOINT FROM TREDUCEPOINT 
WHERE TO_CHAR(DREDUCE,'yyyy')  = '" + sYear + @"'
GROUP BY SREDUCENAME ORDER BY SREDUCENAME");

            Session["dtDefectPointQuarter"] = dt;
        }
        else
        {
            dt = Session["dtDefectPointQuarter"] as DataTable;
        }



        int i1 = 0;
        int i2 = 0;


        GridViewBandColumn sGridViewBandBefore = new GridViewBandColumn("คะแนนก่อนยื่นอุทธรณ์");
        sGridViewBandBefore.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;


        GridViewBandColumn sGridViewBandAfter = new GridViewBandColumn("คะแนนหลังยื่นอุทธรณ์");
        sGridViewBandAfter.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

        foreach (DataRow dr in dt.Rows)
        {
            GridViewDataColumn sGridViewDataColumnSBEFORE = new GridViewDataColumn("B" + i1, dr["SUMPOINT"] + "");
            sGridViewDataColumnSBEFORE.Width = Unit.Pixel(40);
            sGridViewDataColumnSBEFORE.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
            sGridViewDataColumnSBEFORE.HeaderStyle.VerticalAlign = VerticalAlign.Bottom;
            sGridViewDataColumnSBEFORE.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            sGridViewDataColumnSBEFORE.HeaderStyle.CssClass = "Rotate";

            sGridViewBandBefore.Columns.Insert(i1, sGridViewDataColumnSBEFORE);

            i1++;

            ASPxSummaryItem siSB = new ASPxSummaryItem();
            siSB.FieldName = "B" + i1;
            siSB.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            siSB.DisplayFormat = "{0}";
            gvw.TotalSummary.Add(siSB);


            GridViewDataColumn sGridViewDataColumnSAFTER = new GridViewDataColumn("A" + i2, dr["SUMPOINT"] + "");
            sGridViewDataColumnSAFTER.Width = Unit.Pixel(40);
            sGridViewDataColumnSAFTER.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;
            sGridViewDataColumnSAFTER.HeaderStyle.VerticalAlign = VerticalAlign.Bottom;
            sGridViewDataColumnSAFTER.CellStyle.HorizontalAlign = HorizontalAlign.Center;
            sGridViewDataColumnSAFTER.HeaderStyle.CssClass = "Rotate";

            sGridViewBandAfter.Columns.Insert(i2, sGridViewDataColumnSAFTER);
            i2++;

            ASPxSummaryItem siS = new ASPxSummaryItem();
            siS.FieldName = "A" + i2;
            siS.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            siS.DisplayFormat = "{0}";
            gvw.TotalSummary.Add(siS);
        }


        #region คะแนนก่อนยื่นอุทธรณ์


        GridViewDataColumn sGridViewDataColumnBEFORE = new GridViewDataColumn("NBEFORE", "(1)คะแนนที่โดนหัก ก่อนอุทธรณ์");
        sGridViewDataColumnBEFORE.Width = Unit.Pixel(120);
        sGridViewDataColumnBEFORE.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFORE.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFORE.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;

        sGridViewBandBefore.Columns.Insert(i1, sGridViewDataColumnBEFORE);
        i1++;

        ASPxSummaryItem siB = new ASPxSummaryItem();
        siB.FieldName = "NBEFORE";
        siB.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        siB.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(siB);



        GridViewDataColumn sGridViewDataColumnBEFOREAVG = new GridViewDataColumn("NBEFOREAVG", "(2)คะแนนที่โดนหัก หลังอุทธรณ์");
        sGridViewDataColumnBEFOREAVG.Width = Unit.Pixel(120);
        sGridViewDataColumnBEFOREAVG.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFOREAVG.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFOREAVG.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;


        sGridViewBandBefore.Columns.Insert(i1, sGridViewDataColumnBEFOREAVG);
        i1++;

        ASPxSummaryItem siBA = new ASPxSummaryItem();
        siBA.FieldName = "NBEFOREAVG";
        siBA.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        siBA.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(siBA);



        GridViewDataColumn sGridViewDataColumnBEFOREFINAL = new GridViewDataColumn("NBEFOREFINAL", "(3)คะแนนที่ได้");
        sGridViewDataColumnBEFOREFINAL.Width = Unit.Pixel(120);
        sGridViewDataColumnBEFOREFINAL.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFOREFINAL.CellStyle.HorizontalAlign = HorizontalAlign.Center;


        sGridViewBandBefore.Columns.Insert(i1, sGridViewDataColumnBEFOREFINAL);

        ASPxSummaryItem siBF = new ASPxSummaryItem();
        siBF.FieldName = "NBEFOREFINAL";
        siBF.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        siBF.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(siBF);

        #endregion

        #region คะแนนหลังยื่นอุทธรณ์


        GridViewDataColumn sGridViewDataColumnAFTER = new GridViewDataColumn("NAFTER", "(1)คะแนนที่โดนหัก ก่อนอุทธรณ์");
        sGridViewDataColumnAFTER.Width = Unit.Pixel(120);
        sGridViewDataColumnAFTER.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnAFTER.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnAFTER.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;

        sGridViewBandAfter.Columns.Insert(i2, sGridViewDataColumnAFTER);
        i2++;

        ASPxSummaryItem si = new ASPxSummaryItem();
        si.FieldName = "NAFTER";
        si.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        si.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(si);


        GridViewDataColumn sGridViewDataColumnAFTERAVG = new GridViewDataColumn("NAFTERAVG", "(2)คะแนนที่โดนหัก หลังอุทธรณ์");
        sGridViewDataColumnAFTERAVG.Width = Unit.Pixel(120);
        sGridViewDataColumnAFTERAVG.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnAFTERAVG.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnAFTERAVG.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;

        sGridViewBandAfter.Columns.Insert(i2, sGridViewDataColumnAFTERAVG);
        i2++;

        ASPxSummaryItem siA = new ASPxSummaryItem();
        siA.FieldName = "NAFTERAVG";
        siA.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        siA.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(siA);






        GridViewDataColumn sGridViewDataColumnAFTERFINAL = new GridViewDataColumn("NAFTERFINAL", "(3)คะแนนที่ได้");
        sGridViewDataColumnBEFOREFINAL.Width = Unit.Pixel(120);
        sGridViewDataColumnBEFOREFINAL.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFOREFINAL.CellStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewDataColumnBEFOREFINAL.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;

        sGridViewBandAfter.Columns.Insert(i2, sGridViewDataColumnAFTERFINAL);


        ASPxSummaryItem siF = new ASPxSummaryItem();
        siF.FieldName = "NAFTERFINAL";
        siF.SummaryType = DevExpress.Data.SummaryItemType.Sum;
        siF.DisplayFormat = "{0}";
        gvw.TotalSummary.Add(siF);


        #endregion



        GridViewBandColumn sGridViewBandColumn = new GridViewBandColumn("รายงานประเมินผลการทำงาน");
        sGridViewBandColumn.Name = "BandColumnFinal";
        sGridViewBandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        sGridViewBandColumn.VisibleIndex = 4;

        sGridViewBandColumn.Columns.Insert(0, sGridViewBandBefore);
        sGridViewBandColumn.Columns.Insert(1, sGridViewBandAfter);

        gvw.Columns.Insert(3, sGridViewBandColumn);






    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void btnPdfExport_Click(object sender, EventArgs e)
    {
        BindGrid();
        PdfExportOptions options = new PdfExportOptions();

        gridExport.PageHeader.Center = string.Format("รายงานประเมินผลการทำงาน {0} ปี {1}", _Month, _Year);
        gridExport.Landscape = true;
        gridExport.MaxColumnWidth = 180;
        gridExport.WritePdfToResponse("รายงานประเมินผลการทำงาน", options);
    }
    protected void btnXlsExport_Click(object sender, EventArgs e)
    {
        BindGrid();
        XlsExportOptions options = new XlsExportOptions();
        options.TextExportMode = TextExportMode.Text;

        gridExport.PageHeader.Center = string.Format("รายงานประเมินผลการทำงาน {0} ปี {1}", _Month, _Year);
        //gridExport.ExportToXls("File.xls", options);
        gridExport.WriteXlsToResponse("รายงานประเมินผลการทำงาน", options);
        //gridExport.WriteXlsToResponse();
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, _con);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void cmbMonth_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if ("" + rblCheck.Value == "1")
        {
            listTrimas();
        }
        else
        {
            cmbMonth.Items.Clear();
            cmbMonth.Items.Add(new ListEditItem(" -ทั้งหมด- "));
            cmbMonth.SelectedIndex = 0;
            for (int i = 1; i <= 12; i++)
            {
                string Month = DateTimeFormatInfo.GetInstance(new CultureInfo("th-TH")).GetMonthName(i);
                cmbMonth.Items.Add(new ListEditItem("เดือน " + Month, i));
            }
        }
    }

    protected void listTrimas()
    {
        cmbMonth.Items.Clear();
        cmbMonth.Items.Add(new ListEditItem(" -ทั้งหมด- "));
        cmbMonth.SelectedIndex = 0;
        int Year1 = Convert.ToInt32(cmbYear.Value);

        string MaxDate = "";
        string DisplayT = "";
        string ValueT = "";

        int tYear = Year1 + 543;

        for (int i = 1; i <= 4; i++)
        {
            switch (i)
            {
                case 1:
                    MaxDate = DateTime.DaysInMonth(Year1, 3) + "";
                    DisplayT = "ไตรมาสที่ 1 " + "1/1/" + (tYear) + " ถึง " + MaxDate + "/3/" + tYear;
                    ValueT = "1";
                    break;

                case 2:
                    MaxDate = DateTime.DaysInMonth(Year1, 6) + "";
                    DisplayT = "ไตรมาสที่ 2 " + "1/4/" + tYear + " ถึง " + MaxDate + "/6/" + tYear;
                    ValueT = "2";
                    break;

                case 3:
                    MaxDate = DateTime.DaysInMonth(Year1, 9) + "";
                    DisplayT = "ไตรมาสที่ 3 " + "1/7/" + tYear + " ถึง " + MaxDate + "/9/" + tYear;
                    ValueT = "3";

                    break;
                case 4:
                    MaxDate = DateTime.DaysInMonth(Year1, 12) + "";
                    DisplayT = "ไตรมาสที่ 4 " + "1/10/" + tYear + " ถึง " + MaxDate + "/12/" + tYear;
                    ValueT = "4";
                    break;

            }

            cmbMonth.Items.Add(new ListEditItem(DisplayT, ValueT));

        }
    }
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
            case "PAGERONCLICK":
                string[] Para = e.Args[0].Split(';');
                string mode = (e.CallbackName.Equals("PAGERONCLICK")) ? "Search" : "" + Para[0];
                switch (mode)
                {
                    case "Search":

                        BindGrid();

                        // "BandColumnFinal"

                        break;

                }
                break;
        }
    }

    private void BindGrid()
    {

        #region condition

        _Year = cmbYear.Text;

        string MaxDate = "";
        int Year = Convert.ToInt32(cmbYear.Value);

        string sStart = "";
        string sEnd = "";

        if ("" + rblCheck.Value == "1")
        {
            _Month = cmbMonth.Text;

            switch ("" + cmbMonth.Value)
            {
                case "1":
                    MaxDate = DateTime.DaysInMonth(Year, 3) + "";
                    sStart = "1/1/" + (Year);
                    sEnd = MaxDate + "/3/" + Year;

                    break;
                case "2":
                    MaxDate = DateTime.DaysInMonth(Year, 6) + "";
                    sStart = "1/4/" + Year;
                    sEnd = MaxDate + "/6/" + Year;

                    break;
                case "3":
                    MaxDate = DateTime.DaysInMonth(Year, 9) + "";
                    sStart = "1/7/" + Year;
                    sEnd = MaxDate + "/9/" + Year;

                    break;
                case "4":
                    MaxDate = DateTime.DaysInMonth(Year, 12) + "";
                    sStart = "1/10/" + Year;
                    sEnd = MaxDate + "/12/" + Year;
                    break;
                default:
                    MaxDate = DateTime.DaysInMonth(Year, 12) + "";
                    sStart = "1/1/" + (Year);
                    sEnd = MaxDate + "/12/" + Year;
                    break;

            }

        }
        else
        {

            _Month = cmbMonth.Text;

            switch ("" + cmbMonth.Value)
            {
                case "1":
                    MaxDate = DateTime.DaysInMonth(Year, 1) + "";
                    sStart = "1/1/" + Year;
                    sEnd = MaxDate + "/1/" + Year;
                    break;
                case "2":
                    MaxDate = DateTime.DaysInMonth(Year, 2) + "";
                    sStart = "1/2/" + Year;
                    sEnd = MaxDate + "/2/" + Year;
                    break;
                case "3":
                    MaxDate = DateTime.DaysInMonth(Year, 3) + "";
                    sStart = "1/3/" + Year;
                    sEnd = MaxDate + "/3/" + Year;
                    break;
                case "4":
                    MaxDate = DateTime.DaysInMonth(Year, 4) + "";
                    sStart = "1/4/" + Year;
                    sEnd = MaxDate + "/4/" + Year;
                    break;
                case "5":
                    MaxDate = DateTime.DaysInMonth(Year, 5) + "";
                    sStart = "1/5/" + Year;
                    sEnd = MaxDate + "/5/" + Year;
                    break;
                case "6":
                    MaxDate = DateTime.DaysInMonth(Year, 6) + "";
                    sStart = "1/6/" + Year;
                    sEnd = MaxDate + "/6/" + Year;
                    break;
                case "7":
                    MaxDate = DateTime.DaysInMonth(Year, 7) + "";
                    sStart = "1/7/" + Year;
                    sEnd = MaxDate + "/7/" + Year;
                    break;
                case "8":
                    MaxDate = DateTime.DaysInMonth(Year, 8) + "";
                    sStart = "1/8/" + Year;
                    sEnd = MaxDate + "/8/" + Year;
                    break;
                case "9":
                    MaxDate = DateTime.DaysInMonth(Year, 9) + "";
                    sStart = "1/9/" + Year;
                    sEnd = MaxDate + "/9/" + Year;
                    break;
                case "10":
                    MaxDate = DateTime.DaysInMonth(Year, 10) + "";
                    sStart = "1/10/" + Year;
                    sEnd = MaxDate + "/10/" + Year;
                    break;
                case "11":
                    MaxDate = DateTime.DaysInMonth(Year, 11) + "";
                    sStart = "1/11/" + Year;
                    sEnd = MaxDate + "/11/" + Year;
                    break;
                case "12":
                    MaxDate = DateTime.DaysInMonth(Year, 12) + "";
                    sStart = "1/12/" + Year;
                    sEnd = MaxDate + "/12/" + Year;
                    break;
                default:
                    string MaxDate1 = DateTime.DaysInMonth(Year, 12) + "";
                    sStart = "1/1/" + (Year);
                    sEnd = MaxDate1 + "/12/" + Year;
                    break;
            }
        }

        #endregion


        DataTable dt;
        if (Session["dtDefectPointQuarter"] == null)
        {
            string sYear = cmbYear.Value + "" != "" ? cmbYear.Value + "" : DateTime.Now.Year + "";
            dt = CommonFunction.Get_Data(_con, @" SELECT SREDUCENAME,CASE WHEN SREDUCENAME = 'อุบัติเหตุ' OR SREDUCENAME = 'ข้อร้องเรียน' 
THEN SREDUCENAME ELSE SREDUCENAME ||  ' (' || MAX(nvl(NPOINT,0)) || ' คะแนน)' END  as SUMPOINT FROM TREDUCEPOINT 
WHERE TO_CHAR(DREDUCE,'yyyy')  = '" + sYear + @"'
GROUP BY SREDUCENAME ORDER BY SREDUCENAME");

            Session["dtDefectPointQuarter"] = dt;
        }
        else
        {
            dt = Session["dtDefectPointQuarter"] as DataTable;
        }

        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();

        int i = 0;

        foreach (DataRow dr in dt.Rows)
        {
            sb.Append(", SUM(CASE WHEN o.SREDUCENAME = '" + dr["SREDUCENAME"] + "' THEN 1 ELSE 0 END) AS B" + i + " , SUM(CASE WHEN o.SREDUCENAME = '" + dr["SREDUCENAME"] + "' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A" + i);
            sb1.Append(", SUM(B" + i + ") AS B" + i + ", SUM(A" + i + ") AS A" + i);
            i++;
        }



        sds.SelectCommand = string.Format(@"SELECT SVENDORNAME ,SCONTRACTNO, ROUND(SUM(nBefore), 0) as nBefore,  ROUND(AVG(nBeforeAVG), 0) as nBeforeAVG ,100 - ROUND(AVG(nBeforeAVG), 0) as nBeforeFinal, 
ROUND(SUM( nAfter), 0) as nAfter, ROUND(AVG(nAfterAVG), 0) as nAfterAVG,100 - ROUND(AVG(nBeforeAVG), 0) as nAfterFinal 
{1}
FROM
(
SELECT  vs.SVENDORNAME ,CT.SCONTRACTNO ,EXTRACT(month FROM o.DREDUCE) as nMonth , SUM( NVL(o.SUMNPOINT,0)) as nBefore , 
case when  (SUM(nvl(o.SUMNPOINT, 0))) <= 100 then SUM(nvl(o.SUMNPOINT, 0)) else 100 End  as nBeforeAVG, 
SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END ) as nAfter,  
case when  (SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )) <= 100 then SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )  else 100 End  as  nAfterAVG   
{0}
FROM 
TCONTRACT ct LEFT JOIN TVENDOR_SAP vs ON Ct.SVENDORID = VS.SVENDORID
LEFT JOIN 
(
SELECT  R.SREDUCENAME , nvl(r.NPOINT,0) AS SUMNPOINT,
/*ตัดสินถูก 1 ผิด 2 */
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN '1' ELSE CASE WHEN ap.SSENTENCER = '2' THEN '2' ELSE '2' END  END  END   As STATUS ,
r.DREDUCE,

CASE WHEN R.SPROCESSID = '020' THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN r.SCONTRACTID ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))) LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID)
LEFT JOIN TEMPLOYEE_SAP es ON R.SDRIVERNO = ES.SEMPLOYEEID ))
WHERE nvl(r.CACTIVE,'0') = '1'
AND TRUNC(r.DREDUCE) Between To_Date(:dStart,'dd/MM/yyyy') And To_Date(:dEnd,'dd/MM/yyyy')

) o  ON CT.SCONTRACTID = O.SCONTRACTID   
AND nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(VS.SVENDORNAME,'') LIKE '%' || :oSearch || '%'
WHERE NVL(ct.CACTIVE,'Y') = 'Y'
Group By  vs.SVENDORNAME ,CT.SCONTRACTNO , EXTRACT(month FROM o.DREDUCE)
) GROUP BY SVENDORNAME ,SCONTRACTNO", sb.ToString(), sb1.ToString());

        string CHK = string.Format(@"SELECT SVENDORNAME ,SCONTRACTNO, ROUND(SUM(nBefore), 0) as nBefore,  ROUND(AVG(nBeforeAVG), 0) as nBeforeAVG ,100 - ROUND(AVG(nBeforeAVG), 0) as nBeforeFinal, 
ROUND(SUM( nAfter), 0) as nAfter, ROUND(AVG(nAfterAVG), 0) as nAfterAVG,100 - ROUND(AVG(nBeforeAVG), 0) as nAfterFinal 
{1}
FROM
(
SELECT  vs.SVENDORNAME ,CT.SCONTRACTNO ,EXTRACT(month FROM o.DREDUCE) as nMonth , SUM( NVL(o.SUMNPOINT,0)) as nBefore , 
case when  (SUM(nvl(o.SUMNPOINT, 0))) <= 100 then SUM(nvl(o.SUMNPOINT, 0)) else 100 End  as nBeforeAVG, 
SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END ) as nAfter,  
case when  (SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )) <= 100 then SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )  else 100 End  as  nAfterAVG   
{0}
FROM 
TCONTRACT ct LEFT JOIN TVENDOR_SAP vs ON Ct.SVENDORID = VS.SVENDORID
LEFT JOIN 
(
SELECT  R.SREDUCENAME , nvl(r.NPOINT,0) AS SUMNPOINT,
/*ตัดสินถูก 1 ผิด 2 */
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN '1' ELSE CASE WHEN ap.SSENTENCER = '2' THEN '2' ELSE '2' END  END  END   As STATUS ,
r.DREDUCE,

CASE WHEN R.SPROCESSID = '020' THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))) LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID)
LEFT JOIN TEMPLOYEE_SAP es ON R.SDRIVERNO = ES.SEMPLOYEEID ))
WHERE --nvl(r.CACTIVE,'0') = '1'AND 
TRUNC(r.DREDUCE) Between To_Date(:dStart,'dd/MM/yyyy') And To_Date(:dEnd,'dd/MM/yyyy')

) o  ON CT.SCONTRACTID = O.SCONTRACTID   
AND nvl(CT.SCONTRACTNO,'') LIKE '%' || :oSearch || '%' OR nvl(VS.SVENDORNAME,'') LIKE '%' || :oSearch || '%'
WHERE NVL(ct.CACTIVE,'Y') = 'Y'
Group By  vs.SVENDORNAME ,CT.SCONTRACTNO , EXTRACT(month FROM o.DREDUCE)
) GROUP BY SVENDORNAME ,SCONTRACTNO", sb.ToString(), sb1.ToString());

        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oSearch", CommonFunction.ReplaceInjection(txtSearch.Text.Trim()));
        sds.SelectParameters.Add("dStart", CommonFunction.ReplaceInjection(sStart));
        sds.SelectParameters.Add("dEnd", CommonFunction.ReplaceInjection(sEnd));

        sds.DataBind();

        string QUERY = @"SELECT SVENDORNAME ,SCONTRACTNO, ROUND(SUM(nBefore), 0) as nBefore,  ROUND(AVG(nBeforeAVG), 0) as nBeforeAVG ,100 - ROUND(AVG(nBeforeAVG), 0) as nBeforeFinal, 
ROUND(SUM( nAfter), 0) as nAfter, ROUND(AVG(nAfterAVG), 0) as nAfterAVG,100 - ROUND(AVG(nBeforeAVG), 0) as nAfterFinal 
, SUM(B0) AS B0, SUM(A0) AS A0, SUM(B1) AS B1, SUM(A1) AS A1, SUM(B2) AS B2, SUM(A2) AS A2, SUM(B3) AS B3, SUM(A3) AS A3, SUM(B4) AS B4, SUM(A4) AS A4, SUM(B5) AS B5, SUM(A5) AS A5, SUM(B6) AS B6, SUM(A6) AS A6, SUM(B7) AS B7, SUM(A7) AS A7, SUM(B8) AS B8, SUM(A8) AS A8, SUM(B9) AS B9, SUM(A9) AS A9, SUM(B10) AS B10, SUM(A10) AS A10, SUM(B11) AS B11, SUM(A11) AS A11, SUM(B12) AS B12, SUM(A12) AS A12, SUM(B13) AS B13, SUM(A13) AS A13, SUM(B14) AS B14, SUM(A14) AS A14, SUM(B15) AS B15, SUM(A15) AS A15, SUM(B16) AS B16, SUM(A16) AS A16, SUM(B17) AS B17, SUM(A17) AS A17, SUM(B18) AS B18, SUM(A18) AS A18, SUM(B19) AS B19, SUM(A19) AS A19, SUM(B20) AS B20, SUM(A20) AS A20, SUM(B21) AS B21, SUM(A21) AS A21, SUM(B22) AS B22, SUM(A22) AS A22, SUM(B23) AS B23, SUM(A23) AS A23, SUM(B24) AS B24, SUM(A24) AS A24, SUM(B25) AS B25, SUM(A25) AS A25, SUM(B26) AS B26, SUM(A26) AS A26, SUM(B27) AS B27, SUM(A27) AS A27, SUM(B28) AS B28, SUM(A28) AS A28, SUM(B29) AS B29, SUM(A29) AS A29, SUM(B30) AS B30, SUM(A30) AS A30, SUM(B31) AS B31, SUM(A31) AS A31, SUM(B32) AS B32, SUM(A32) AS A32, SUM(B33) AS B33, SUM(A33) AS A33, SUM(B34) AS B34, SUM(A34) AS A34, SUM(B35) AS B35, SUM(A35) AS A35, SUM(B36) AS B36, SUM(A36) AS A36, SUM(B37) AS B37, SUM(A37) AS A37, SUM(B38) AS B38, SUM(A38) AS A38, SUM(B39) AS B39, SUM(A39) AS A39, SUM(B40) AS B40, SUM(A40) AS A40, SUM(B41) AS B41, SUM(A41) AS A41, SUM(B42) AS B42, SUM(A42) AS A42, SUM(B43) AS B43, SUM(A43) AS A43, SUM(B44) AS B44, SUM(A44) AS A44, SUM(B45) AS B45, SUM(A45) AS A45, SUM(B46) AS B46, SUM(A46) AS A46, SUM(B47) AS B47, SUM(A47) AS A47, SUM(B48) AS B48, SUM(A48) AS A48, SUM(B49) AS B49, SUM(A49) AS A49, SUM(B50) AS B50, SUM(A50) AS A50, SUM(B51) AS B51, SUM(A51) AS A51, SUM(B52) AS B52, SUM(A52) AS A52, SUM(B53) AS B53, SUM(A53) AS A53, SUM(B54) AS B54, SUM(A54) AS A54, SUM(B55) AS B55, SUM(A55) AS A55, SUM(B56) AS B56, SUM(A56) AS A56, SUM(B57) AS B57, SUM(A57) AS A57, SUM(B58) AS B58, SUM(A58) AS A58, SUM(B59) AS B59, SUM(A59) AS A59, SUM(B60) AS B60, SUM(A60) AS A60
FROM
(
SELECT  vs.SVENDORNAME ,CT.SCONTRACTNO ,EXTRACT(month FROM o.DREDUCE) as nMonth , SUM( NVL(o.SUMNPOINT,0)) as nBefore , 
case when  (SUM(nvl(o.SUMNPOINT, 0))) <= 100 then SUM(nvl(o.SUMNPOINT, 0)) else 100 End  as nBeforeAVG, 
SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END ) as nAfter,  
case when  (SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )) <= 100 then SUM( CASE WHEN STATUS = '1' THEN 0  ELSE NVL(o.SUMNPOINT,0) END )  else 100 End  as  nAfterAVG   
, SUM(CASE WHEN o.SREDUCENAME = '1.5 1) รถบรรทุกและพนักงานขับรถไม่พร้อมปฏิบัติงานตั่งแต่วันเริ่มต้นสัญญาหรือและระหว่างอายุสัญญา' THEN 1 ELSE 0 END) AS B0 , SUM(CASE WHEN o.SREDUCENAME = '1.5 1) รถบรรทุกและพนักงานขับรถไม่พร้อมปฏิบัติงานตั่งแต่วันเริ่มต้นสัญญาหรือและระหว่างอายุสัญญา' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A0, SUM(CASE WHEN o.SREDUCENAME = 'GPSใช้งานได้/เปิดดูได้บนเว็บ' THEN 1 ELSE 0 END) AS B1 , SUM(CASE WHEN o.SREDUCENAME = 'GPSใช้งานได้/เปิดดูได้บนเว็บ' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A1, SUM(CASE WHEN o.SREDUCENAME = 'Sight Glass ที่ยึดแล้วถอดไม่ได้ (Bottom load)' THEN 1 ELSE 0 END) AS B2 , SUM(CASE WHEN o.SREDUCENAME = 'Sight Glass ที่ยึดแล้วถอดไม่ได้ (Bottom load)' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A2, SUM(CASE WHEN o.SREDUCENAME = 'กรณีรถ Bottom Load มี Sight Glass ที่ยึดแล้วถอดไม่ได้และไม่รั่วซึม [แก้ไขภายใน7วัน]' THEN 1 ELSE 0 END) AS B3 , SUM(CASE WHEN o.SREDUCENAME = 'กรณีรถ Bottom Load มี Sight Glass ที่ยึดแล้วถอดไม่ได้และไม่รั่วซึม [แก้ไขภายใน7วัน]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A3, SUM(CASE WHEN o.SREDUCENAME = 'ข้อร้องเรียน' THEN 1 ELSE 0 END) AS B4 , SUM(CASE WHEN o.SREDUCENAME = 'ข้อร้องเรียน' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A4, SUM(CASE WHEN o.SREDUCENAME = 'จัดทำจุดร้อยลวดซีลกับอุปกรณ์ทุกตัวที่เปิดปิดได้ [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B5 , SUM(CASE WHEN o.SREDUCENAME = 'จัดทำจุดร้อยลวดซีลกับอุปกรณ์ทุกตัวที่เปิดปิดได้ [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A5, SUM(CASE WHEN o.SREDUCENAME = 'ชุดอุปกรณ์ฝา Man hole จะต้องอยู่ในสภาพพร้อมใช้งาน ยึดติดแน่นถอดออกไม่ได้ และซีลฝาแมนโฮลอยู่ในสภาพปกติ [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B6 , SUM(CASE WHEN o.SREDUCENAME = 'ชุดอุปกรณ์ฝา Man hole จะต้องอยู่ในสภาพพร้อมใช้งาน ยึดติดแน่นถอดออกไม่ได้ และซีลฝาแมนโฮลอยู่ในสภาพปกติ [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A6, SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมน๊อตยึดOver Fill (รถBottom Load)' THEN 1 ELSE 0 END) AS B7 , SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมน๊อตยึดOver Fill (รถBottom Load)' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A7, SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมน๊อตสลักฝาMan hole' THEN 1 ELSE 0 END) AS B8 , SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมน๊อตสลักฝาMan hole' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A8, SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมสลักด้ามแกนวาล์ว/หัวน๊อตยึดแกนวาล์ว' THEN 1 ELSE 0 END) AS B9 , SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมสลักด้ามแกนวาล์ว/หัวน๊อตยึดแกนวาล์ว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A9, SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมหัวน๊อตหน้าแปลนวาล์วอย่างน้อย 50% ของจำนวนน๊อตทั้งหมด' THEN 1 ELSE 0 END) AS B10 , SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมหัวน๊อตหน้าแปลนวาล์วอย่างน้อย 50% ของจำนวนน๊อตทั้งหมด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A10, SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมหัวน๊อตหน้าแปลนวาล์วอย่างน้อย 50% ของจำนวนน๊อตทั้งหมด/เชื่อมสลักด้ามแกนวาล์ว/หัวน๊อตยึดแกนวาล์ว [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B11 , SUM(CASE WHEN o.SREDUCENAME = 'เชื่อมหัวน๊อตหน้าแปลนวาล์วอย่างน้อย 50% ของจำนวนน๊อตทั้งหมด/เชื่อมสลักด้ามแกนวาล์ว/หัวน๊อตยึดแกนวาล์ว [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A11, SUM(CASE WHEN o.SREDUCENAME = 'ซีลฝาMan hole ปกติ/ไม่ชำรุด' THEN 1 ELSE 0 END) AS B12 , SUM(CASE WHEN o.SREDUCENAME = 'ซีลฝาMan hole ปกติ/ไม่ชำรุด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A12, SUM(CASE WHEN o.SREDUCENAME = 'ซีลฝาหน้าท่อปกติไม่รั่ว' THEN 1 ELSE 0 END) AS B13 , SUM(CASE WHEN o.SREDUCENAME = 'ซีลฝาหน้าท่อปกติไม่รั่ว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A13, SUM(CASE WHEN o.SREDUCENAME = 'ซีลและลวดร้อยซีลทุกตัวมีสภาพปกติ/ตรงตามเอกสารการขนส่ง [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B14 , SUM(CASE WHEN o.SREDUCENAME = 'ซีลและลวดร้อยซีลทุกตัวมีสภาพปกติ/ตรงตามเอกสารการขนส่ง [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A14, SUM(CASE WHEN o.SREDUCENAME = 'ต่อสายป้องกันไฟฟ้าสถิตในถังถึงก้นถัง' THEN 1 ELSE 0 END) AS B15 , SUM(CASE WHEN o.SREDUCENAME = 'ต่อสายป้องกันไฟฟ้าสถิตในถังถึงก้นถัง' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A15, SUM(CASE WHEN o.SREDUCENAME = 'แต่งกายถูกต้อง/มีบัตรประจำตัว' THEN 1 ELSE 0 END) AS B16 , SUM(CASE WHEN o.SREDUCENAME = 'แต่งกายถูกต้อง/มีบัตรประจำตัว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A16, SUM(CASE WHEN o.SREDUCENAME = 'ถังดับเพลิงแบบ6A,20-B ขนาด 5ปอนด์ 1ถัง ในห้องคนขับ + ถังดับเพลิงแบบ6A,20-B ขนาด 20 ปอนด์ 2 ถัง [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B17 , SUM(CASE WHEN o.SREDUCENAME = 'ถังดับเพลิงแบบ6A,20-B ขนาด 5ปอนด์ 1ถัง ในห้องคนขับ + ถังดับเพลิงแบบ6A,20-B ขนาด 20 ปอนด์ 2 ถัง [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A17, SUM(CASE WHEN o.SREDUCENAME = 'ถังดับเพลิงแบบ6A,20-B5ปอนด์1ถัง' THEN 1 ELSE 0 END) AS B18 , SUM(CASE WHEN o.SREDUCENAME = 'ถังดับเพลิงแบบ6A,20-B5ปอนด์1ถัง' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A18, SUM(CASE WHEN o.SREDUCENAME = 'ถังน้ำมัน, ท่อทางน้ำมัน, วาล์ว รวมถึงซีลยางและปะเก็นไม่รั่วซึม ไม่เป็นสนิม ไม่ลอกล่อนและอยู่ในสภาพดี [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B19 , SUM(CASE WHEN o.SREDUCENAME = 'ถังน้ำมัน, ท่อทางน้ำมัน, วาล์ว รวมถึงซีลยางและปะเก็นไม่รั่วซึม ไม่เป็นสนิม ไม่ลอกล่อนและอยู่ในสภาพดี [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A19, SUM(CASE WHEN o.SREDUCENAME = 'ถังน้ำมัน/ก๊าซสภาพดี/ไม่รั่วซึม' THEN 1 ELSE 0 END) AS B20 , SUM(CASE WHEN o.SREDUCENAME = 'ถังน้ำมัน/ก๊าซสภาพดี/ไม่รั่วซึม' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A20, SUM(CASE WHEN o.SREDUCENAME = 'ท่อยางระบายไอไม่หลวมและเชื่อมหัวน๊อตรัดเข็มขัด' THEN 1 ELSE 0 END) AS B21 , SUM(CASE WHEN o.SREDUCENAME = 'ท่อยางระบายไอไม่หลวมและเชื่อมหัวน๊อตรัดเข็มขัด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A21, SUM(CASE WHEN o.SREDUCENAME = 'ท่อไอเสียมีหม้อพักกันลูกไฟ ขณะอยู่ในคลัง/โรงกลั่น ต้องใส่กระแกรงกันลูกไฟ [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B22 , SUM(CASE WHEN o.SREDUCENAME = 'ท่อไอเสียมีหม้อพักกันลูกไฟ ขณะอยู่ในคลัง/โรงกลั่น ต้องใส่กระแกรงกันลูกไฟ [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A22, SUM(CASE WHEN o.SREDUCENAME = 'ท่อไอเสียมีหม้อพักกันลูกไฟขณะอยู่ในคลังต้องใส่กระแกรงกันลูกไฟ' THEN 1 ELSE 0 END) AS B23 , SUM(CASE WHEN o.SREDUCENAME = 'ท่อไอเสียมีหม้อพักกันลูกไฟขณะอยู่ในคลังต้องใส่กระแกรงกันลูกไฟ' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A23, SUM(CASE WHEN o.SREDUCENAME = 'ทางเดินหลังถังมีพื้นผิวแบบกันลื่น' THEN 1 ELSE 0 END) AS B24 , SUM(CASE WHEN o.SREDUCENAME = 'ทางเดินหลังถังมีพื้นผิวแบบกันลื่น' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A24, SUM(CASE WHEN o.SREDUCENAME = 'แบตเตอรีมีฝาครอบเป็นฉนวน' THEN 1 ELSE 0 END) AS B25 , SUM(CASE WHEN o.SREDUCENAME = 'แบตเตอรีมีฝาครอบเป็นฉนวน' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A25, SUM(CASE WHEN o.SREDUCENAME = 'ปฏิบัติงานถูกต้องตามขั้นตอนในคลัง/โรงกลั่น' THEN 1 ELSE 0 END) AS B26 , SUM(CASE WHEN o.SREDUCENAME = 'ปฏิบัติงานถูกต้องตามขั้นตอนในคลัง/โรงกลั่น' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A26, SUM(CASE WHEN o.SREDUCENAME = 'ปฏิบัติงานที่ลูกค้าด้วยความถูกต้อง/ปลอดภัย' THEN 1 ELSE 0 END) AS B27 , SUM(CASE WHEN o.SREDUCENAME = 'ปฏิบัติงานที่ลูกค้าด้วยความถูกต้อง/ปลอดภัย' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A27, SUM(CASE WHEN o.SREDUCENAME = 'แป้นระดับน้ำมันยึดแน่น ขยับไม่ได้ รวมถึงลวดและซีลของแป้นต้องตึง [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B28 , SUM(CASE WHEN o.SREDUCENAME = 'แป้นระดับน้ำมันยึดแน่น ขยับไม่ได้ รวมถึงลวดและซีลของแป้นต้องตึง [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A28, SUM(CASE WHEN o.SREDUCENAME = 'แป้นสภาพดี/ลวดไม่หย่อน/ตีซีล ปตท.' THEN 1 ELSE 0 END) AS B29 , SUM(CASE WHEN o.SREDUCENAME = 'แป้นสภาพดี/ลวดไม่หย่อน/ตีซีล ปตท.' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A29, SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งกระทำการใดๆ ที่ ปตท. เชื่อได้ว่าไม่ปฏิบัติตามสัญญา รวมทั้งกฎ ระเบียบ ข้อกำหนด ข้อบังคับ คำสั่ง เงื่อนไข หลักเกณฑ์และมาตรฐานใดๆ ที่กฎหมายหรือ ปตท. กำหนด' THEN 1 ELSE 0 END) AS B30 , SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งกระทำการใดๆ ที่ ปตท. เชื่อได้ว่าไม่ปฏิบัติตามสัญญา รวมทั้งกฎ ระเบียบ ข้อกำหนด ข้อบังคับ คำสั่ง เงื่อนไข หลักเกณฑ์และมาตรฐานใดๆ ที่กฎหมายหรือ ปตท. กำหนด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A30, SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งไม่ดำเนินการในระบบ TMS ตามขั้นตอนการปฏิบัติงานที่ ปตท.กำหนด' THEN 1 ELSE 0 END) AS B31 , SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งไม่ดำเนินการในระบบ TMS ตามขั้นตอนการปฏิบัติงานที่ ปตท.กำหนด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A31, SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งไม่ดำเนินการในระบบ TMS ตามขั้นตอนการปฏิบัติงานที่ ปตท.กำหนด (จัดแผน)' THEN 1 ELSE 0 END) AS B32 , SUM(CASE WHEN o.SREDUCENAME = 'ผู้ขนส่งไม่ดำเนินการในระบบ TMS ตามขั้นตอนการปฏิบัติงานที่ ปตท.กำหนด (จัดแผน)' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A32, SUM(CASE WHEN o.SREDUCENAME = 'แผ่นกันCompartmentชั้นเดียว' THEN 1 ELSE 0 END) AS B33 , SUM(CASE WHEN o.SREDUCENAME = 'แผ่นกันCompartmentชั้นเดียว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A33, SUM(CASE WHEN o.SREDUCENAME = 'พขร. ต้องไม่มีปริมาณแอลกอฮอล์ในเลือด (ตรวจวัดแล้วมีค่า 0 mg%BAC) [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B34 , SUM(CASE WHEN o.SREDUCENAME = 'พขร. ต้องไม่มีปริมาณแอลกอฮอล์ในเลือด (ตรวจวัดแล้วมีค่า 0 mg%BAC) [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A34, SUM(CASE WHEN o.SREDUCENAME = 'พขร. แต่งเครื่องแบบ/ปฏิบัติงาน ตามกฎระเบียบคลัง/โรงกลั่น/โรงงานผู้ผลิต/ลูกค้า ด้วยความถูกต้องปลอดภัย [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B35 , SUM(CASE WHEN o.SREDUCENAME = 'พขร. แต่งเครื่องแบบ/ปฏิบัติงาน ตามกฎระเบียบคลัง/โรงกลั่น/โรงงานผู้ผลิต/ลูกค้า ด้วยความถูกต้องปลอดภัย [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A35, SUM(CASE WHEN o.SREDUCENAME = 'ไฟสัญญาณใช้งานได้/ไม่แตกหัก' THEN 1 ELSE 0 END) AS B36 , SUM(CASE WHEN o.SREDUCENAME = 'ไฟสัญญาณใช้งานได้/ไม่แตกหัก' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A36, SUM(CASE WHEN o.SREDUCENAME = 'มีการ์ดบาร์/และตรงหน้าท่อจ่ายเป็นแบบ Brake Interlock ใช้งานได้และทำจุดร้อยลวดซีล' THEN 1 ELSE 0 END) AS B37 , SUM(CASE WHEN o.SREDUCENAME = 'มีการ์ดบาร์/และตรงหน้าท่อจ่ายเป็นแบบ Brake Interlock ใช้งานได้และทำจุดร้อยลวดซีล' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A37, SUM(CASE WHEN o.SREDUCENAME = 'มีชุดสายไฟคีบกราวด์ที่ใช้งานได้' THEN 1 ELSE 0 END) AS B38 , SUM(CASE WHEN o.SREDUCENAME = 'มีชุดสายไฟคีบกราวด์ที่ใช้งานได้' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A38, SUM(CASE WHEN o.SREDUCENAME = 'มีราวกันตกทั้ง 2 ข้าง, ขนาดความสูงไม่น้อยกว่า 1 เมตร [แก้ไขภายใน7วัน]' THEN 1 ELSE 0 END) AS B39 , SUM(CASE WHEN o.SREDUCENAME = 'มีราวกันตกทั้ง 2 ข้าง, ขนาดความสูงไม่น้อยกว่า 1 เมตร [แก้ไขภายใน7วัน]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A39, SUM(CASE WHEN o.SREDUCENAME = 'มีสวิตช์ตัดไฟฟ้าออกจากแบตเตอรีทั้ง2ขั้ว' THEN 1 ELSE 0 END) AS B40 , SUM(CASE WHEN o.SREDUCENAME = 'มีสวิตช์ตัดไฟฟ้าออกจากแบตเตอรีทั้ง2ขั้ว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A40, SUM(CASE WHEN o.SREDUCENAME = 'มีสวิตช์เปลี่ยนเชื้อเพลิง(รถใช้NGV)' THEN 1 ELSE 0 END) AS B41 , SUM(CASE WHEN o.SREDUCENAME = 'มีสวิตช์เปลี่ยนเชื้อเพลิง(รถใช้NGV)' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A41, SUM(CASE WHEN o.SREDUCENAME = 'ไม่มีอุปกรณ์ต้องห้าม' THEN 1 ELSE 0 END) AS B42 , SUM(CASE WHEN o.SREDUCENAME = 'ไม่มีอุปกรณ์ต้องห้าม' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A42, SUM(CASE WHEN o.SREDUCENAME = 'ไม่มีอุปกรณ์ต้องห้าม เช่น คีมดัดแปลง ซีลสำรอง ซีลที่ผ่านการใช้งาน ลวดร้อยซีล กาว วัสดุปลายแหลมที่ใช้แกะซีลได้ [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B43 , SUM(CASE WHEN o.SREDUCENAME = 'ไม่มีอุปกรณ์ต้องห้าม เช่น คีมดัดแปลง ซีลสำรอง ซีลที่ผ่านการใช้งาน ลวดร้อยซีล กาว วัสดุปลายแหลมที่ใช้แกะซีลได้ [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A43, SUM(CASE WHEN o.SREDUCENAME = 'ไม่ส่งรถเข้ามารับงาน (โหลดน้ำมัน) ที่ต้นทาง ตามแผนที่ผู้ขนส่งยืนยันแล้ว' THEN 1 ELSE 0 END) AS B44 , SUM(CASE WHEN o.SREDUCENAME = 'ไม่ส่งรถเข้ามารับงาน (โหลดน้ำมัน) ที่ต้นทาง ตามแผนที่ผู้ขนส่งยืนยันแล้ว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A44, SUM(CASE WHEN o.SREDUCENAME = 'ยางหลังสภาพดีความลึกไม่ต่ำกว่า2mm./แก้มยางไม่ฉีกขาดหรือเห็นโครงเหล็ก ' THEN 1 ELSE 0 END) AS B45 , SUM(CASE WHEN o.SREDUCENAME = 'ยางหลังสภาพดีความลึกไม่ต่ำกว่า2mm./แก้มยางไม่ฉีกขาดหรือเห็นโครงเหล็ก ' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A45, SUM(CASE WHEN o.SREDUCENAME = 'ยางอะไหล่พร้อมใช้งาน' THEN 1 ELSE 0 END) AS B46 , SUM(CASE WHEN o.SREDUCENAME = 'ยางอะไหล่พร้อมใช้งาน' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A46, SUM(CASE WHEN o.SREDUCENAME = 'ล้อหน้าสภาพดี/ดอกลึก3mm.แก้มยางไม่ฉีกขาดหรือเห็นโครงเหล็ก' THEN 1 ELSE 0 END) AS B47 , SUM(CASE WHEN o.SREDUCENAME = 'ล้อหน้าสภาพดี/ดอกลึก3mm.แก้มยางไม่ฉีกขาดหรือเห็นโครงเหล็ก' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A47, SUM(CASE WHEN o.SREDUCENAME = 'วงแหวนกันงัดไม่เสียรูป/ร้อยลวดตีซีลหรือเชื่อมน๊อตยึด(ถ้ามี)' THEN 1 ELSE 0 END) AS B48 , SUM(CASE WHEN o.SREDUCENAME = 'วงแหวนกันงัดไม่เสียรูป/ร้อยลวดตีซีลหรือเชื่อมน๊อตยึด(ถ้ามี)' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A48, SUM(CASE WHEN o.SREDUCENAME = 'วาล์วฉุกเฉินใช้งานได้ดี' THEN 1 ELSE 0 END) AS B49 , SUM(CASE WHEN o.SREDUCENAME = 'วาล์วฉุกเฉินใช้งานได้ดี' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A49, SUM(CASE WHEN o.SREDUCENAME = 'วาล์วฉุกเฉินสามารถปิดวาล์วใต้ท้องถังได้ [แก้ไขภายใน7วัน]' THEN 1 ELSE 0 END) AS B50 , SUM(CASE WHEN o.SREDUCENAME = 'วาล์วฉุกเฉินสามารถปิดวาล์วใต้ท้องถังได้ [แก้ไขภายใน7วัน]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A50, SUM(CASE WHEN o.SREDUCENAME = 'วาล์วท่อจ่ายมีฝาปิดและซีลกันรั่ว' THEN 1 ELSE 0 END) AS B51 , SUM(CASE WHEN o.SREDUCENAME = 'วาล์วท่อจ่ายมีฝาปิดและซีลกันรั่ว' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A51, SUM(CASE WHEN o.SREDUCENAME = 'วาล์วทุกตัวไม่รั่วซึม' THEN 1 ELSE 0 END) AS B52 , SUM(CASE WHEN o.SREDUCENAME = 'วาล์วทุกตัวไม่รั่วซึม' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A52, SUM(CASE WHEN o.SREDUCENAME = 'วาล์วระบายไอตีซีล ปตท.' THEN 1 ELSE 0 END) AS B53 , SUM(CASE WHEN o.SREDUCENAME = 'วาล์วระบายไอตีซีล ปตท.' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A53, SUM(CASE WHEN o.SREDUCENAME = 'ส่งรถไม่ครบตามจำนวนที่ระบุไว้ในสัญญา' THEN 1 ELSE 0 END) AS B54 , SUM(CASE WHEN o.SREDUCENAME = 'ส่งรถไม่ครบตามจำนวนที่ระบุไว้ในสัญญา' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A54, SUM(CASE WHEN o.SREDUCENAME = 'สภาพซีลปกติ/ตรงตามเอกสารการขนส่ง' THEN 1 ELSE 0 END) AS B55 , SUM(CASE WHEN o.SREDUCENAME = 'สภาพซีลปกติ/ตรงตามเอกสารการขนส่ง' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A55, SUM(CASE WHEN o.SREDUCENAME = 'สลิงดึงวาล์วใต้ถังไม่หย่อน/วาล์วลมใช้งานได้' THEN 1 ELSE 0 END) AS B56 , SUM(CASE WHEN o.SREDUCENAME = 'สลิงดึงวาล์วใต้ถังไม่หย่อน/วาล์วลมใช้งานได้' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A56, SUM(CASE WHEN o.SREDUCENAME = 'สลิงดึงวาล์วใต้ถังไม่หย่อน/วาล์วลมใช้งานได้ [แก้ไขภายใน7วัน]' THEN 1 ELSE 0 END) AS B57 , SUM(CASE WHEN o.SREDUCENAME = 'สลิงดึงวาล์วใต้ถังไม่หย่อน/วาล์วลมใช้งานได้ [แก้ไขภายใน7วัน]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A57, SUM(CASE WHEN o.SREDUCENAME = 'สายไฟบริเวณถังมีฉนวนหุ้ม/ร้อยท่อเรียบร้อย' THEN 1 ELSE 0 END) AS B58 , SUM(CASE WHEN o.SREDUCENAME = 'สายไฟบริเวณถังมีฉนวนหุ้ม/ร้อยท่อเรียบร้อย' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A58, SUM(CASE WHEN o.SREDUCENAME = 'หลีกเลี่ยงการรับงานโดยไม่ยืนยันรถขนส่งตามแผนงานที่ ปตท. กำหนด' THEN 1 ELSE 0 END) AS B59 , SUM(CASE WHEN o.SREDUCENAME = 'หลีกเลี่ยงการรับงานโดยไม่ยืนยันรถขนส่งตามแผนงานที่ ปตท. กำหนด' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A59, SUM(CASE WHEN o.SREDUCENAME = 'อุปกรณ์ระบบ IVMS อยู่ในสภาพดี [หยุดและแก้ไขทันที]' THEN 1 ELSE 0 END) AS B60 , SUM(CASE WHEN o.SREDUCENAME = 'อุปกรณ์ระบบ IVMS อยู่ในสภาพดี [หยุดและแก้ไขทันที]' THEN CASE WHEN STATUS = '1' THEN 0 ELSE 1 END ELSE 0 END) AS A60
FROM 
TCONTRACT ct LEFT JOIN TVENDOR_SAP vs ON Ct.SVENDORID = VS.SVENDORID
LEFT JOIN 
(
SELECT  R.SREDUCENAME , nvl(r.NPOINT,0) AS SUMNPOINT,
/*ตัดสินถูก 1 ผิด 2 */
CASE WHEN ap.CSTATUS = '3' THEN  CASE WHEN ap.SSENTENCER = '1' THEN '1' ELSE CASE WHEN ap.SSENTENCER = '2' THEN '2' ELSE '2' END  END  END   As STATUS ,
r.DREDUCE,

CASE WHEN R.SPROCESSID = '020' THEN CASE WHEN T.SCONTRACTID is null THEN CASE WHEN CT2.SCONTRACTID is null THEN 0 ELSE CT2.SCONTRACTID END ELSE  T.SCONTRACTID END ELSE r.SCONTRACTID END as SCONTRACTID
FROM ((((((TREDUCEPOINT r LEFT JOIN TTRUCK t ON R.SHEADREGISTERNO = T.SHEADREGISTERNO )
LEFT JOIN TCONTRACT_TRUCK ct2 ON nvl(T.STRUCKID,1) = nvl(CT2.STRUCKID,1))) LEFT JOIN TAPPEAL ap on R.NREDUCEID = ap.NREDUCEID)
LEFT JOIN TEMPLOYEE_SAP es ON R.SDRIVERNO = ES.SEMPLOYEEID ))
WHERE nvl(r.CACTIVE,'0') = '1' AND 
TRUNC(r.DREDUCE) Between To_Date('" + CommonFunction.ReplaceInjection(sStart) + @"','dd/MM/yyyy') And To_Date('" + CommonFunction.ReplaceInjection(sEnd) + @"','dd/MM/yyyy')

) o  ON CT.SCONTRACTID = O.SCONTRACTID   
WHERE NVL(ct.CACTIVE,'Y') = 'Y' AND (nvl(CT.SCONTRACTNO,'') LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + @"' || '%' OR nvl(VS.SVENDORNAME,'') LIKE '%' || '" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + @"' || '%')

Group By  vs.SVENDORNAME ,CT.SCONTRACTNO , EXTRACT(month FROM o.DREDUCE)
) GROUP BY SVENDORNAME ,SCONTRACTNO";

        DataTable dt_CC = CommonFunction.Get_Data(_con, QUERY);

        gvw.DataSource = dt_CC;
        gvw.DataBind();
    }
}
