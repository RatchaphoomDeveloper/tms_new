﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="questionnaire.aspx.cs" Inherits="questionnaire" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnFormList" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnFormConfig" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCompare" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlVendorSearch.ClientID %>" class="col-md-2 control-label">ผู้ประกอบการขนส่ง</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlYearSearch.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <div style="text-align: right;">
                                    <div class="col-md-3">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <div style="text-align: right;">
                                    <div class="col-md-11">
                                        <asp:Button ID="btnFormList" runat="server" Text="จัดการแบบฟอร์มประเมินผลการทำงาน" CssClass="btn btn-hover btn-info" OnClientClick="openInNewTab('admin_VisitForm_lst.aspx')" OnClick="btnFormConfig_Click" />
                                        <asp:Button ID="btnFormConfig" runat="server" Text="ตั้งค่าการใช้งานแบบฟอร์ม" CssClass="btn btn-hover btn-info" OnClientClick="openInNewTab('Admin_VisitForm_Config.aspx')" OnClick="btnFormConfig_Click" />
                                        <asp:Button ID="btnAdd" runat="server" Text="บันทึกผลประเมิน" CssClass="btn btn-hover btn-info" OnClientClick="openInNewTab('questionnaire_form.aspx')" OnClick="btnFormConfig_Click" />
                                        <asp:Button ID="btnCompare" runat="server" Text="เปรียบเทียบผลประเมิน" CssClass="btn btn-hover btn-info" OnClientClick="openInNewTab('questionnaire_compare.aspx')" OnClick="btnFormConfig_Click" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>คะแนนประเมนผลการบริหาร
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="NQUESTIONNAIREID" OnRowCreated="grvMain_RowCreated"
                        EmptyDataText="[ ไม่มีข้อมูล ]" ShowFooter="true">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="บริษัทผู้ขนส่ง" ItemStyle-Width="250px"
                                HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่ตรวจสอบ" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="แสดงให้ผู้ขนส่งเห็น" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เรียกดูข้อมูล" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แก้ไขข้อมูล" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" />
        <asp:Button ID="btnEdit" runat="server" OnClick="btnView_Click" />
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
    </div>

    <script type="text/javascript">
       
        function gotoView(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnView.ClientID%>").click();
        }

        function gotoEdit(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnEdit.ClientID%>").click();
        }

        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

