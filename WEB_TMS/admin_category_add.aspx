﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_category_add.aspx.cs" Inherits="admin_category_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" 
        ClientInstanceName="xcpn" oncallback="xcpn_Callback" onload="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" /><PanelCollection>
        <dx:PanelContent>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
             <td bgcolor="#0E4999">
                   <img src="images/spacer.GIF" width="250px" height="1px"></td>
         </tr>
     </table>
    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
        <tr>
            <td valign="top" bgcolor="#FFFFFF" style=" width:20%;padding-top:10px">
                มุมมองที่ใช้ประเมินความรุนแรง<font color="#ff0000">*</font></td>
            <td colspan="2">
                <dx:ASPxRadioButtonList ID="rblCategory" runat="server" 
                    DataSourceID="sdsCategory" RepeatColumns="2" RepeatDirection="Horizontal" 
                    TextField="SCATEGORYTYPENAME" ValueField="SCATEGORYTYPEID" SelectedIndex="0">
                <ClientSideEvents  SelectedIndexChanged="function (s, e) { xcpn.PerformCallback('ShowSubCategory');  }" />
                    <Border BorderStyle="None" />
                </dx:ASPxRadioButtonList>
                <asp:SqlDataSource ID="sdsCategory" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                    SelectCommand="SELECT SCATEGORYTYPEID, SCATEGORYTYPENAME,SHEADTYPEID FROM TCATEGORYTYPE WHERE SHEADTYPEID IS NULL ORDER BY SCATEGORYTYPENAME"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" style=" width:20%">
                &nbsp;</td>
            <td colspan="2">
                <dx:ASPxRadioButtonList ID="rblSubCategory" runat="server"  ClientInstanceName="rblSubCategory"
                    DataSourceID="sdsSubCategory" RepeatColumns="4" RepeatDirection="Horizontal" 
                    TextField="SCATEGORYTYPENAME" ValueField="SCATEGORYTYPEID" 
                    SelectedIndex="0">
                <ClientSideEvents  SelectedIndexChanged="function (s, e) { xcpn.PerformCallback('ShowSubCategory');  }" />
                    <Border BorderStyle="None" />
                </dx:ASPxRadioButtonList>
                <asp:SqlDataSource ID="sdsSubCategory" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                    SelectCommand="SELECT SCATEGORYTYPEID, SCATEGORYTYPENAME FROM TCATEGORYTYPE WHERE (SHEADTYPEID = :SHEADTYPEID) ORDER BY SCATEGORYTYPENAME">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="rblCategory" Name="SHEADTYPEID" 
                            PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" class="style24">
                Category<font color="#ff0000">*</font></td>
            <td align="left" bgcolor="#FFFFFF" style=" width:28%">
                <dx:ASPxComboBox ID="cbxCategoryname" runat="server" Width="60px" 
                    SelectedIndex="0">
                    <Items>
                        <dx:ListEditItem Selected="True" Text="A" Value="A" />
                        <dx:ListEditItem Text="B" Value="B" />
                        <dx:ListEditItem Text="C" Value="C" />
                        <dx:ListEditItem Text="D" Value="D" />
                        <dx:ListEditItem Text="E" Value="E" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td align="left" bgcolor="#FFFFFF" class="style21">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style24" bgcolor="#FFFFFF">
                ค่าตัวคูณความรุนแรง <font color="#ff0000">*</font></td>
            <td align="left" class="style21" bgcolor="#FFFFFF" colspan="2">
                <dx:ASPxTextBox ID="txtNImpact" runat="server" Width="140px">
                     <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                    <RegularExpression ErrorText="กรุณาระบุตัวเลขจำนวนเต็ม"
                                                    ValidationExpression="<%$ Resources:ValidationResource, Valid_NumberUnLimit %>" />
                   </ValidationSettings>
                </dx:ASPxTextBox>
            </tr>
        <tr>
            <%--OnPreRender="txtPassword_PreRender"--%>
            <td bgcolor="#FFFFFF" class="style24">
                <span style="text-align: left;">ความรุนแรงของผลกระทบ<font color="#ff0000">*</font></span></td>
            <td align="left" bgcolor="#FFFFFF" class="style21" colspan="2">
                <dx:ASPxTextBox ID="txtSImpact" runat="server" Width="140px" MaxLength="200">
                   <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                     </ValidationSettings>
                </dx:ASPxTextBox>
                </td>
            
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" class="style24">
                นิยาม <font color="#ff0000">*</font> </td>
            <td align="left" bgcolor="#FFFFFF" class="style21" colspan="2">
                <dx:ASPxTextBox ID="txtDefine" runat="server" Width="270px" MaxLength="200">
                   <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                     </ValidationSettings>
                </dx:ASPxTextBox>
            </td>
        </tr>
            <tr>
            <td bgcolor="#FFFFFF" class="style24">
                คำอธิบายเพิ่มเติม</td>
            <td align="left" bgcolor="#FFFFFF" class="style21" colspan="2">
                <dx:ASPxTextBox ID="txtDescription" runat="server" Width="270px" MaxLength="200">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" class="style25" valign="top">
                สถานะ<font color="#ff0000">*</font></td>
            <td align="left" colspan="2">
                <dx:ASPxRadioButtonList ID="rblStatus" runat="server" 
                    ClientInstanceName="rblStatus" EnableDefaultAppearance="False" 
                    RepeatDirection="Horizontal" RepeatLayout="Flow" SelectedIndex="0" 
                    SkinID="rblStatus">
                    <Items>
                        <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                        <dx:ListEditItem Text="InActive" Value="0" />
                    </Items>
                </dx:ASPxRadioButtonList>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFFFFF" style="width:50%" align="right" colspan="2">
                <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save;'+ rblSubCategory.GetValue()); }" />
                </dx:ASPxButton>
            </td>
            <td style="width:50%">
                <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_category_lst.aspx'; }" />
                </dx:ASPxButton>
            </td>
        </tr>
        <tr>
            <td align="left" bgcolor="#FFFFFF" colspan="2" >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
        CancelSelectOnNullParameter="False"
        InsertCommand="INSERT INTO TCATEGORY(SCATEGORYID, SCATEGORYTYPEID, SCATEGORYNAME, SIMPACTLEVEL, NIMPACTLEVEL, SDEFINE, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE,SREMARK) VALUES (:SCATEGORYID, :SCATEGORYTYPEID, :SCATEGORYNAME, :SIMPACTLEVEL, :NIMPACTLEVEL, :SDEFINE, :CACTIVE, SYSDATE, :SCREATE, SYSDATE, :SUPDATE,:SREMARK)" 
        UpdateCommand="UPDATE TCATEGORY SET SCATEGORYTYPEID =:SCATEGORYTYPEID, SCATEGORYNAME =:SCATEGORYNAME, SIMPACTLEVEL =:SIMPACTLEVEL, NIMPACTLEVEL =:NIMPACTLEVEL, SDEFINE =:SDEFINE, CACTIVE =:CACTIVE, DUPDATE =SYSDATE, SUPDATE =:SUPDATE, SREMARK = :SREMARK WHERE SCATEGORYID = :SCATEGORYID" 
        oninserted="sds_Inserted" oninserting="sds_Inserting" onupdated="sds_Updated" 
        onupdating="sds_Updating" 
         ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
        <InsertParameters>
            <asp:SessionParameter Name="SCATEGORYID" SessionField="oSCATEGORYID" />
            <asp:ControlParameter Name="SCATEGORYTYPEID" ControlID="rblCategory" PropertyName="Value"/>
            <asp:ControlParameter Name="SCATEGORYNAME" ControlID="cbxCategoryname" PropertyName="Value" />
            <asp:ControlParameter Name="SIMPACTLEVEL" ControlID="txtSImpact" PropertyName="Text" />
            <asp:ControlParameter Name="NIMPACTLEVEL" ControlID="txtNImpact" PropertyName="Text" />
            <asp:ControlParameter Name="SDEFINE" ControlID="txtDefine" PropertyName="Text" />
            <asp:ControlParameter Name="cActive" ControlID="rblStatus" 
                PropertyName="Value"/>
            <asp:ControlParameter Name="SREMARK" ControlID="txtDescription" PropertyName="Text" />
            <asp:SessionParameter Name="sCreate" SessionField="UserID" DefaultValue="ยุทธนา" />
            <asp:SessionParameter Name="sUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter Name="SCATEGORYTYPEID" ControlID="rblCategory" PropertyName="Value"/>
            <asp:ControlParameter Name="SCATEGORYNAME" ControlID="cbxCategoryname" PropertyName="Value" />
            <asp:ControlParameter Name="SIMPACTLEVEL" ControlID="txtSImpact" PropertyName="Text" />
            <asp:ControlParameter Name="NIMPACTLEVEL" ControlID="txtNImpact" PropertyName="Text" />
            <asp:ControlParameter Name="SDEFINE" ControlID="txtDefine" PropertyName="Text" />
            <asp:ControlParameter Name="cActive" ControlID="rblStatus" 
                PropertyName="Value"/>
            <asp:ControlParameter Name="SREMARK" ControlID="txtDescription" PropertyName="Text" />
            <asp:SessionParameter Name="sUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
            <asp:SessionParameter Name="SCATEGORYID" SessionField="oSCATEGORYID" />
        </UpdateParameters>
    </asp:SqlDataSource>
        </dx:PanelContent>
      </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
