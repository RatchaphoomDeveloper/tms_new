﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraEditors;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Popup;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class ucCalendar : System.Web.UI.UserControl
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //  dteStart.Value = DateTime.Now.Date;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            string sMonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "-" + (year + 543);
           // this.ddeCalendar.Text = sMonthYear;
            Session["sCalendarDate"] = null;
            //รหัส;typeหน้า;ความจุ;SEAL_HIT
            //00001;01;170000;1
           // Session["sCalendar"] = "000002;02;17000;0";

            Calendar.GenerateJavaScriptFullMonthCalendar(this.ltrmonthcalendar, month, year);
        }



    }


    //protected void btnCallbackCalendar_Click(object sender, EventArgs e)
    //{
    //   showCalendar();
    //}

    //public void showCalendar()
    //{

    //    if ("" + Session["sCalendar"] != "")
    //    {


    //        //DateTime temp;
    //        //DateTime date = DateTime.TryParse("1-" + ddeCalendar.Text, out temp) ? temp : DateTime.Now;

    //        //int month = date.Month;
    //        //int year = date.Year;

    //        //รหัส;typeหน้า;ความจุ;SEAL_HIT
    //        //00001;01;170000;1
    //        string[] Para = (Session["sCalendar"] + "").Split(';');
    //        if (Para.Length == 4)
    //        {
    //            dvCalendar.Visible = true;

    //            Calendar.GenerateJavaScriptFullMonthCalendar(this.ltrmonthcalendar, month, year);

    //            string sMonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "-" + (year + 543);
    //            this.ddeCalendar.Text = sMonthYear;
    //        }
    //        else
    //        {
    //            dvCalendar.Visible = false;
    //            Session["sCalendar"] = null;
    //        }

    //    }
    //    else
    //    {
    //        dvCalendar.Visible = false;
    //    }
    //}


    protected void btnAddDataCalendar_Click(object sender, EventArgs e)
    {
        string sValue = CommonFunction.ReplaceInjection(txtValueCalendar.Text);

        //รหัส;typeหน้า;ความจุ;SEAL_HIT
        //00001;01;170000;1
        //(Session["sCalendar"] + "")

       // dvCalendar.Visible = false;
        Session["sCalendarDate"] = sValue;
        ASPxLabel lblCalendarDate = (ASPxLabel)Parent.FindControl("lblCalendarDate");

        if (lblCalendarDate != null)
        {
            lblCalendarDate.Text = Convert.ToDateTime(sValue).ToString("d MMMM yyyy") + "";
        }
    }


    //#region Year and Month only

    //protected void ddeCalendar_Init(object sender, EventArgs e)
    //{
    //    ddeCalendar.DropDownWindowTemplate = new DateTemplate();
    //}

    //public class DateTemplate : ITemplate
    //{
    //    public void InstantiateIn(Control container)
    //    {

    //        container.Controls.Add(new LiteralControl("<table border='0' cellpadding='0' cellspacing='0'"));
    //        container.Controls.Add(new LiteralControl("<tr>"));

    //        container.Controls.Add(new LiteralControl("<td  align='left'>"));
    //        ASPxButton btPrev = new ASPxButton();
    //        btPrev.ID = "btPrev";
    //        container.Controls.Add(btPrev);
    //        btPrev.EnableDefaultAppearance = false;
    //        btPrev.CssClass = "buttonMonth";
    //        btPrev.Width = 10;
    //        btPrev.ClientSideEvents.Click = "OnPrevClick";
    //        btPrev.AutoPostBack = false;
    //        btPrev.Text = "<";
    //        container.Controls.Add(new LiteralControl("</td>"));

    //        container.Controls.Add(new LiteralControl("<td style='text-align: center' width='100%'>"));
    //        ASPxButton label = new ASPxButton();
    //        label.ID = "YearLabel";
    //        container.Controls.Add(label);
    //        label.Text = (DateTime.Now.Year + 543).ToString();
    //        label.ClientInstanceName = "lblYear";
    //        label.Width = Unit.Percentage(100);
    //        container.Controls.Add(new LiteralControl("</td>"));

    //        container.Controls.Add(new LiteralControl("<td  align='right'>"));
    //        ASPxButton btNext = new ASPxButton();
    //        btNext.ID = "btNext";
    //        container.Controls.Add(btNext);
    //        btNext.AutoPostBack = false;
    //        btNext.EnableDefaultAppearance = false;
    //        btNext.CssClass = "buttonMonth";
    //        btNext.Width = 10;
    //        btNext.Text = ">";
    //        btNext.ClientSideEvents.Click = "OnNextClick";
    //        container.Controls.Add(new LiteralControl("</td>"));

    //        container.Controls.Add(new LiteralControl("</tr>"));
    //        container.Controls.Add(new LiteralControl("</table>"));

    //        container.Controls.Add(new LiteralControl("<table  border='0' cellpadding='0' cellspacing='0'>"));
    //        int k = 1;
    //        for (int i = 0; i < 3; i++)
    //        {
    //            container.Controls.Add(new LiteralControl("<tr>"));
    //            for (int j = 0; j < 4; j++)
    //            {
    //                container.Controls.Add(new LiteralControl("<td >"));
    //                ASPxButton button = new ASPxButton();
    //                button.ID = "btn#" + k;
    //                button.ClientInstanceName = k.ToString().PadLeft(2, '0');
    //                container.Controls.Add(button);
    //                button.AutoPostBack = false;
    //                button.Width = 50;
    //                button.EnableDefaultAppearance = false;
    //                button.CssClass = "buttonMonth";
    //                button.FocusRectBorder.BorderWidth = 1;
    //                button.ClientSideEvents.Click = "OnClick";
    //                button.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(k);
    //                container.Controls.Add(new LiteralControl("</td>"));
    //                k++;
    //            }
    //            container.Controls.Add(new LiteralControl("</tr>"));
    //        }
    //        container.Controls.Add(new LiteralControl("</table>"));


    //        container.Controls.Add(new LiteralControl("<center  >"));
    //        ASPxButton btOk = new ASPxButton();
    //        btOk.ID = "BtOk";
    //        container.Controls.Add(btOk);
    //        btOk.AutoPostBack = false;
    //        btOk.Text = "OK";
    //        btOk.Width = Unit.Percentage(100);
    //        btOk.ClientSideEvents.Click = "OnOkClick";
    //        container.Controls.Add(new LiteralControl("</center>"));
    //    }
    //}

    //#endregion

}