﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class admin_grade_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblUser.Text = Session["UserName"] + "";

        if (!IsPostBack)
        {            
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;      

            if (Session["oNGRADEID"] != null)
            {
                listData();
            }
            this.AssignAuthen();
        }
    }

    void ClearControl()
    {

        Session["oNGRADEID"] = null;
        txtGradeName.Text = "";
        txtStartPoint.Text = "";
        txtEndPoint.Text = "";
        rblStatus.Value = 1;
        lblUser.Text = "";


    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                
            }
            if (!CanWrite)
            {
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {


            case "Save":
                if (CanWrite)
                {
                    if (Session["oNGRADEID"] == null)
                    {
                        //ตรวจสอบว่ามีข้อมูลอยู่หรือไม่ถ้ามีให้ทำการอัพเดทข้อมูล
                        if (CommonFunction.Count_Value(sql, "Select * from TGRADE WHERE SGRADENAME = '" + txtGradeName.Text.Trim() + "' AND CACTIVE = '1'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ชื่อเกรดมีอยู่ในระบบแล้ว!');");
                            return;
                        }

                        string GenID;

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"INSERT INTO TGRADE (NGRADEID, SGRADENAME, NSTARTPOINT, NENDPOINT , CACTIVE,  SCREATE, SUPDATE, DCREATE, DUPDATE) 
VALUES (:NGRADEID,:SGRADENAME,:NSTARTPOINT,:NENDPOINT,:CACTIVE,:SCREATE,:SUPDATE,SYSDATE,SYSDATE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                GenID = CommonFunction.Gen_ID(con, "SELECT NGRADEID FROM (SELECT NGRADEID FROM TGRADE ORDER BY NGRADEID DESC) WHERE ROWNUM <= 1");
                                com.Parameters.Clear();
                                com.Parameters.Add(":NGRADEID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":SGRADENAME", OracleType.VarChar).Value = txtGradeName.Text;
                                com.Parameters.Add(":NSTARTPOINT", OracleType.Number).Value = txtStartPoint.Text;
                                com.Parameters.Add(":NENDPOINT", OracleType.Number).Value = txtEndPoint.Text;
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus.Value;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("35", "I", "บันทึกข้อมูลหน้า เกณฑ์การตัดเกรด", GenID + "");
                    }
                    else
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"UPDATE TGRADE SET  SGRADENAME  = :SGRADENAME,NSTARTPOINT = :NSTARTPOINT,NENDPOINT   = :NENDPOINT,
                                     CACTIVE = :CACTIVE,SUPDATE  = :SUPDATE, DUPDATE = SYSDATE WHERE  NGRADEID = :NGRADEID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NGRADEID", OracleType.Int32).Value = Session["oNGRADEID"] + "";
                                com.Parameters.Add(":SGRADENAME", OracleType.VarChar).Value = txtGradeName.Text;
                                com.Parameters.Add(":NSTARTPOINT", OracleType.Number).Value = txtStartPoint.Text;
                                com.Parameters.Add(":NENDPOINT", OracleType.Number).Value = txtEndPoint.Text;
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblStatus.Value;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("35", "E", "แก้ไขข้อมูลหน้า เกณฑ์การตัดเกรด", Session["oNGRADEID"] + "");

                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_grade_lst.aspx';});");

                    ClearControl();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

        }

    }


    void listData()
    {

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT SGRADENAME, NSTARTPOINT,  NENDPOINT, CACTIVE FROM TGRADE WHERE NGRADEID = '" + Session["oNGRADEID"] + "'");
        if (dt.Rows.Count > 0)
        {
            txtGradeName.Text = dt.Rows[0]["SGRADENAME"] + "";
            txtStartPoint.Text = dt.Rows[0]["NSTARTPOINT"] + "";
            txtEndPoint.Text = dt.Rows[0]["NENDPOINT"] + "";
            rblStatus.Value = dt.Rows[0]["CACTIVE"] + "";
            lblUser.Text = Session["UserName"] + "";
        }
        
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}