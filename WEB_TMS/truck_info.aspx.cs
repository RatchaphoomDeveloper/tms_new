﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;
using TMS_BLL.Master;
using DevExpress.XtraEditors;
using GemBox.Spreadsheet;
using TMS_BLL.Transaction.ExportExcel;
using System.Drawing;

public partial class truck_info : PageBase
{
    #region + ViewState +

    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        #region Event
        gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        gvwTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        gvwTruck.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwTruck_HtmlDataCellPrepared);
        VehicleTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(VehicleTruck_AfterPerformCallback);
        VehicleTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwVehicleTruck_HtmlRowPrepared);
        #endregion
        //if (!Permissions("99"))
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        //}

        CGROUP = Session["CGROUP"].ToString();
        SVDID = Session["SVDID"].ToString();
        if (!IsPostBack)
        {
            this.LoadVendor();
            this.LoadVendorVehicle();
            this.AssignAuthen();

            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
            InitialStatus(radSpotTruck, " AND STATUS_TYPE = 'SPOT_STATUS'");
            //ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;            

            if (string.Equals(CGROUP, "0"))
            {
                chkAllow.Checked = true;
                chkDisallow.Checked = true;
                //CheckBox Vehicle
                checkAllowVehicle.Checked = true;
                chkDisallowVehicle.Checked = true;
                gvwTruck.AllColumns[10].SetColVisible(false);
                VehicleTruck.AllColumns[12].SetColVisible(false);
                cboVendor.Enabled = false;
                //ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
                //cboVendor.DataSource = ViewState["DataVendor"];
                //cboVendor.DataBind();
                cboVendor.SelectedValue = SVDID;
                //cboVendorVehicle.DataSource = ViewState["DataVendor"];
                cboVendorVehicle.Enabled = false;
                //cboVendorVehicle.DataBind();
                cboVendorVehicle.SelectedValue = SVDID;

                //cblstatus.SelectedIndex = 1;
                //cblstatus.Enabled = false;

                radStatusTC.SelectedIndex = 1;
                radStatusTC.Enabled = false;

                BindGridSearch(gvwTruck);
                BindGridSearchVehicle(VehicleTruck);

            }
            else
            {
                ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
                //cboVendor.DataSource = ViewState["DataVendor"];
                //cboVendor.DataBind();
                //cboVendorVehicle.DataSource = ViewState["DataVendor"];
                //cboVendorVehicle.DataBind();

                chkAllow.Checked = true;
                chkDisallow.Checked = true;
                chkBacklist.Checked = true;
                //CheckBox Vehicle
                checkAllowVehicle.Checked = true;
                chkDisallowVehicle.Checked = true;
                chkBacklistVehicle.Checked = true;
                BindGridSearch(gvwTruck);
                BindGridSearchVehicle(VehicleTruck);

            }

        }
        else
        {
            if (string.Equals(CGROUP, "0"))
            {
                //chkAllow.Checked = true;
                //chkDisallow.Checked = true;
                ////CheckBox Vehicle
                //checkAllowVehicle.Checked = true;
                //chkDisallowVehicle.Checked = true;
                //cboVendor.DataSource = ViewState["DataVendor"];
                //cboVendor.DataBind();
                //cboVendorVehicle.DataSource = ViewState["DataVendor"];
                //cboVendorVehicle.DataBind();
                cboVendor.SelectedValue = SVDID;
                cboVendorVehicle.SelectedValue = SVDID;

                //cblstatus.SelectedIndex = 1;
                //cblstatus.Enabled = false;

                radStatusTC.SelectedIndex = 1;
                radStatusTC.Enabled = false;
                gvwTruck.AllColumns[10].SetColVisible(false);
                VehicleTruck.AllColumns[12].SetColVisible(false);
            }
            else
            {
                //cboVendor.DataSource = ViewState["DataVendor"];
                //cboVendor.DataBind();
                //cboVendorVehicle.DataSource = ViewState["DataVendor"];
                //cboVendorVehicle.DataBind();
            }
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                btnSearchVehicle.Enabled = false;
                btnExportVehicle.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SEARCH":
                BindGridSearch(gvwTruck);
                break;
            case "SEARCH_CANCEL":
                ClearTextTruck();
                BindGridSearch(gvwTruck);
                break;
            case "SEARCHVEHICLE":
                BindGridSearchVehicle(VehicleTruck);
                break;
            case "SEARCH_CANCELVEHICLE":
                ClearTextVehicle();
                BindGridSearchVehicle(VehicleTruck);
                break;
        }
    }

    private void ClearTextTruck()
    {

        txtsHeadRegisterNo.Text = "";
        chkAllow.Checked = true;
        chkDisallow.Checked = true;
        if (string.Equals(CGROUP.ToString(), "1")) cboVendor.Text = "";
        //cblstatus.Value = "";
        radStatusTC.SelectedValue = "";
        dedtStart.Text = "";
        dedtEnd.Text = "";
        this.LoadVendor();
    }
    private void ClearTextVehicle()
    {
        if (string.Equals(CGROUP.ToString(), "1")) cboVendorVehicle.Text = "";
        txtRegisVehicle.Text = "";
        checkAllowVehicle.Checked = true;
        chkDisallowVehicle.Checked = true;
        StartDateVehicle.Text = "";
        EndDateVehicle.Text = "";
    }

    private void LoadVendor()
    {
        try
        {
            DataTable dtVendor = VendorBLL.Instance.TVendorSapSelect();
            DropDownListHelper.BindDropDownList(ref cboVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadVendorVehicle()
    {
        try
        {
            DataTable dtVendorVehicle = VendorBLL.Instance.TVendorSapSelect();
            DropDownListHelper.BindDropDownList(ref cboVendorVehicle, dtVendorVehicle, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            case "PAGERONCLICK":
                BindGridSearch(gvwTruck);
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                break;
            case "STARTEDIT":
                break;
            case "VIEW":

                dynamic dy_data = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID", "SCHASIS");
                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(dy_data[0] + "") + "&CarTypeID=" + ConfigValue.GetEncodeText(dy_data[1] + "") + "&mode=view"
                    , sUrl = "truck_edit.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = sUrl + sEncrypt;
                break;
            case "ADD":

                string aEncrypt = Server.UrlEncode(STCrypt.Encrypt("Add&"))
                    , aUrl = "truck_add.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = aUrl + aEncrypt;
                break;
            case "EDIT":
                dynamic edy_data = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID", "SCHASIS", "SHEADREGISTERNO");
                string eEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(edy_data[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(edy_data[1] + "") + "&mode=edit"
                    , eUrl = "truck_edit.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = eUrl + eEncrypt;
                // BindGridSearch(gvwTruck, "SHEADREGISTERNO",edy_data[3]);                
                break;
            case "AGE":
                dynamic rowAge = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "SHEADREGISTERNO");
                string EncRowAge = ConfigValue.GetEncodeText("str") + "&License=" + ConfigValue.GetEncodeText(rowAge[1] + "")
                    , URL_Age = "truck_age.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = URL_Age + EncRowAge;
                break;
            case "VIEWCONTRACT":
                dynamic dydata = gvwTruck.GetRowValues(visibleindex, "SCONTRACTID", "SVENDORID");
                string vEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dydata[0] + "&" + dydata[1]))
                    , vUrl = "contract_add.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = vUrl + vEncrypt;
                break;
            case "ADDVEHICLE":
                string EncryptVehicle = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + string.Empty
                    , Vehicle = "Vehicle.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = Vehicle + EncryptVehicle;
                break;
            case "EDITVEHICLE":
                dynamic edy_datavehicle = VehicleTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID");
                string eEncryptvehicle = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(edy_datavehicle[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(edy_datavehicle[1] + "")
                        , eUrlvehicle = "Vehicle_Edit.aspx?str=";
                gvwTruck.JSProperties["cpRedirectTo"] = eUrlvehicle + eEncryptvehicle;
                break;
        }
    }

    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType)
        {
            case GridViewRowType.Header:
                ASPxButton btnAdd = (ASPxButton)e.Row.FindControl("btnAdd");
                ASPxLabel lblAdd = (ASPxLabel)e.Row.FindControl("lblAdd");
                string cPermission = Session["chkurl"] + "";
                switch (cPermission)
                {
                    case "1":
                        btnAdd.ClientVisible = false;
                        lblAdd.ClientVisible = true;
                        break;
                    case "2":
                        btnAdd.ClientVisible = true;
                        lblAdd.ClientVisible = false;
                        break;
                }

                break;
            //case GridViewRowType.Data:
            //    if (Convert.ToInt32(e.GetValue("LDUPDATE")) == 0)
            //        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            //    e.Row.BackColor.GetBrightness();
            //    break;
        }
    }

    void gvwTruck_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //ASPxButton btnEdit = (ASPxButton)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit");
        ASPxButton btnEdit = gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit") as ASPxButton;
        ASPxButton btnAdd = gvwTruck.FindHeaderTemplateControl((GridViewColumn)gvwTruck.Columns["การจัดการ"], "btnAdd") as ASPxButton;

        //if (!CanWrite)
        //{
        //    btnEdit.Enabled = false;
        //    btnAdd.Enabled = false;
        //}
    }

    protected void VehicleTruck_AfterPerformCallback(object s, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            case "PAGERONCLICK":
                BindGridSearchVehicle(VehicleTruck);
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
    }
    protected void gvwVehicleTruck_HtmlRowPrepared(object s, ASPxGridViewTableRowEventArgs e)
    {
        ASPxGridView gv = (ASPxGridView)s;
        try
        {
            switch (e.RowType)
            {
                case GridViewRowType.Data:

                    ASPxButton btnEdit = (ASPxButton)gv.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit");
                    if (string.Equals(CGROUP, "0"))
                    {
                        btnEdit.Text = "แยกหัวหาง";
                    }
                    if (!CanWrite)
                    {
                        btnEdit.Enabled = false;
                    }
                    break;
                default:
                    break;
            }



        }
        catch (Exception ex)
        {

            throw;
        }
    }
    private void BindGridSearch(ASPxGridView __gvw)
    {
        sdsTruck.SelectCommand = this.QueryConditionTC();
        //เก็บQuery เพื่อ โชว์Excel
        string sdsID = __gvw.DataSourceID;
        __gvw.DataBind();


    }
    private void BindGridSearchVehicle(ASPxGridView VehicleTruck)
    {

        sdsTruckVehicle.SelectCommand = this.QueryConditionViehcle();
        string sdsID = VehicleTruck.DataSourceID;
        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
        //Cache.Remove(__sds.CacheKeyDependency);
        //Cache[__sds.CacheKeyDependency] = new object();
        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //__sds.DataBind();
        VehicleTruck.DataBind();
    }
    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;
                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;
                            break;

                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    #region PrepareRow
    protected void vehiclegrid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (!string.Equals(CGROUP, "1"))
        {
            if (e.VisibleIndex == -1) return;
            int row = int.Parse(VehicleTruck.VisibleRowCount.ToString());
            string SemiTraller = e.GetValue("SCARTYPEID") + "";
            string STRUCKID = e.GetValue("STRUCKID") + "";
            ASPxButton btn = VehicleTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit") as ASPxButton;
            if (string.Equals(SemiTraller, "0"))
            {
                btn.Visible = false;
            }
            else
            {
                btn.Visible = true;
            }
        }

    }
    #endregion
    #region QueryTruckTC
    private string QueryConditionTC()
    {
        try
        {
            string Qeury = "";
            string Condition = "";
            if (!string.IsNullOrEmpty(cboVendor.SelectedValue))
            {
                Condition += " AND V.SVENDORID = '" + cboVendor.SelectedValue + "'";
            }
            //if (!string.IsNullOrEmpty(cblstatus.Value + ""))
            //{
            //    Condition += " AND TH.ISUSE = '" + cblstatus.Value + "'";
            //}
            if (!string.IsNullOrEmpty(radStatusTC.SelectedValue + ""))
            {
                Condition += " AND TH.ISUSE = '" + radStatusTC.SelectedValue + "'";
            }
            if (!string.IsNullOrEmpty(dedtStart.Text.ToString()) && !string.IsNullOrEmpty(dedtEnd.Text.Trim().ToString()))
            {
                Condition += " AND TH.DUPDATE >= TO_DATE( '" + dedtStart.Text + "','DD/MM/YYYY') AND TH.DUPDATE <= TO_DATE( '" + dedtEnd.Text + "','DD/MM/YYYY')";
            }
            if (radSpotTruck.SelectedIndex > -1)
            {
                Condition += " AND NVL(TH.SPOT_STATUS, 0) = " + radSpotTruck.SelectedValue;
            }
            if (chkAllow.Checked == true || chkDisallow.Checked == true || chkBacklist.Checked == true)
            {
                string checkAllow = "";
                string checkDisallow = "";
                string checkDisabled = "", checkCactive9 = string.Empty;
                if (chkAllow.Checked == true)
                {
                    checkAllow = "Y";
                }
                if (chkDisallow.Checked == true)
                {
                    checkDisallow = "N";
                }
                if (chkBacklist.Checked == true)
                {
                    checkDisabled = "D";
                }
                Condition += " AND TH.CACTIVE IN('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "','" + CommonFunction.ReplaceInjection(checkCactive9) + "')";
            }
            if (!string.IsNullOrEmpty(txtsHeadRegisterNo.Text.Trim().ToString()))
            {
                Condition += " AND ROWNUM='1' AND TH.SHEADREGISTERNO LIKE '%" + txtsHeadRegisterNo.Text.Trim().ToString() + "%'";
            }
            Qeury = @" SELECT  V.SABBREVIATION,C.SCONTRACTNO,TH.STRUCKID,SHEADREGISTERNO,  
                        TH.SCHASIS,
                        TH.SCARTYPEID ,
                        TT.CONFIG_NAME AS SCARCATEGORY,
                        (CASE SCARTYPEID 
                          WHEN 3 THEN
                          0
                          ELSE
                          TH.NSLOT
                          END)  NSLOT,
                        (CASE SCARTYPEID 
                          WHEN 3 THEN
                          0
                          ELSE
                          TH.NTOTALCAPACITY
                          END) NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,                                                                           
                            CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')
                                WHEN TH.CACTIVE ='D' THEN 
                                GET_C_CONFIG('02','TTRUCK_STATUS','1')                               
                            END STATUS,(CASE TH.ISUSE
                              WHEN 1 THEN
                              'รถในสังกัด'
                              ELSE
                              'รถไม่มีสังกัด'
                            END
                            ) ISUSE,CLASSGRP
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.SCARTYPEID=TT.CONFIG_VALUE 
                    AND TT.CONFIG_TYPE ='VICHICLE'  
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID  AND C.CACTIVE='Y'
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                    WHERE TH.LOGDATA='1' AND TH.SCARTYPEID IN ('0','3','4') " + Condition + " ORDER BY TH.STRUCKID DESC";
            return Qeury;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region QueryViehicleCondition
    private string QueryConditionViehcle()
    {
        try
        {
            string Query = "";
            string Condition = "";
            if (!string.IsNullOrEmpty(cboVendorVehicle.SelectedValue))
            {
                Condition += " AND V.SVENDORID = '" + cboVendorVehicle.SelectedValue + "'";
            }
            if (!string.IsNullOrEmpty(StartDateVehicle.Text.ToString()) && !string.IsNullOrEmpty(EndDateVehicle.Text.Trim().ToString()))
            {
                Condition += " AND TH.DUPDATE >= TO_DATE( '" + StartDateVehicle.Text + "','DD/MM/YYYY')  AND TH.DUPDATE <= TO_DATE( '" + EndDateVehicle.Text + "','DD/MM/YYYY')";
            }
            if (radSpot.SelectedIndex > -1)
            {
                Condition += " AND NVL(TH.SPOT_STATUS, 0) = " + radSpot.SelectedValue;
            }
            if (checkAllowVehicle.Checked == true || chkDisallowVehicle.Checked == true || chkBacklistVehicle.Checked == true)
            {
                string checkAllow_Vehicle = "";
                string checkDisallow_Vehicle = "";
                string checkDisabled_Vehicle = "", checkCactive9 = string.Empty;
                if (checkAllowVehicle.Checked == true)
                {
                    checkAllow_Vehicle = "Y";
                }
                if (chkDisallowVehicle.Checked == true)
                {
                    checkDisallow_Vehicle = "N";
                }
                if (chkBacklistVehicle.Checked == true)
                {
                    checkDisabled_Vehicle = "D";
                }

                Condition += " AND TH.CACTIVE IN('" + CommonFunction.ReplaceInjection(checkAllow_Vehicle) + "','" + CommonFunction.ReplaceInjection(checkDisallow_Vehicle) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled_Vehicle) + "','" + CommonFunction.ReplaceInjection(checkCactive9) + "')";
            }
            if (!string.IsNullOrEmpty(txtRegisVehicle.Text.Trim().ToString()))
            {
                Condition += " AND TH.SCARTYPEID NOT IN ('4') AND (TH.SHEADREGISTERNO LIKE'%" + txtRegisVehicle.Text.Trim().ToString() + "%' OR TH.STRAILERREGISTERNO LIKE '%" + txtRegisVehicle.Text.Trim().ToString() + "%')";
            }
            Query = @"SELECT  TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        C.SCONTRACTNO,
                        TH.SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,DECODE(TH.SCARTYPEID,3,ST.NSLOT, NVL (TH.NSLOT, ST.NSLOT)) NSLOT,
                        NVL(TH.NTOTALCAPACITY,ST.NTOTALCAPACITY) NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        TH.ISUSE,TH.CLASSGRP,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')
                                WHEN TH.CACTIVE ='D' THEN 
                                GET_C_CONFIG('02','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                    LEFT JOIN TTRUCK ST
                    ON TH.STRAILERREGISTERNO=ST.SHEADREGISTERNO
                    LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID AND C.CACTIVE='Y'
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE ='1' AND TH.SCARTYPEID IN ('0','3') AND (TH.VIEHICLE_TYPE IS NULL OR TH.VIEHICLE_TYPE IN ('00','34')) AND C.CACTIVE    ='Y' " + Condition + " ORDER BY TH.STRUCKID DESC";
            return Query;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion
    #region Drownload Excel
    #region Export TC
    protected void btnExportTC_Click(object sender, EventArgs e)
    {
        string QueryForExcelTC = this.QueryConditionTC();
        DataTable DataTC = ExportTCBLL.Instance.ExportTC(QueryForExcelTC);
        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
        ExcelFile workbook = new ExcelFile();
        workbook.Worksheets.Add("Export");
        ExcelWorksheet worksheet = workbook.Worksheets["Export"];
        DataTC.Columns["SHEADREGISTERNO"].ColumnName = "ทะเบียนรถ";
        DataTC.Columns["SABBREVIATION"].ColumnName = "ผู้ขนส่ง";
        DataTC.Columns["SCHASIS"].ColumnName = "SCHASIS";
        DataTC.Columns["SCARCATEGORY"].ColumnName = "ประเภทรถTC";
        DataTC.Columns["NTOTALCAPACITY"].ColumnName = "ปริมาตร";
        DataTC.Columns["DUPDATE"].ColumnName = "วันที่อัพเดตข้อมูล";
        DataTC.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";
        DataTC.Columns["NSLOT"].ColumnName = "จำนวนช่อง";
        DataTC.Columns["STATUS"].ColumnName = "สถานะ";
        DataTC.Columns["ISUSE"].ColumnName = "สังกัดรถ";
        DataTC.Columns.Remove("STRUCKID");
        DataTC.Columns.Remove("SCHASIS");
        DataTC.Columns.Remove("SCARTYPEID");
        DataTC.Columns.Remove("CACTIVE");
        DataTC.Columns.Remove("SCONTRACTID");
        DataTC.Columns.Remove("SVENDORID");
        DataTC.Columns.Remove("CLASSGRP");
        worksheet.InsertDataTable(DataTC, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
        for (int j = 0; j < DataTC.Columns.Count; j++)
        {//Export Detail
            this.SetFormatCell(worksheet.Cells[0, j], DataTC.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
            worksheet.Columns[j].AutoFit();
        }
        string Path = this.CheckPath();
        string FileName = "Truck_TC_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";
        workbook.Save(Path + "\\" + FileName);
        this.DownloadFile(Path, FileName);
    }
    #endregion
    #region Export Vehicle
    protected void btnExportVehicle_Click(object sender, EventArgs e)
    {
        string QueryForExcelVehicle = this.QueryConditionViehcle();
        DataTable DataTC = ExportTCBLL.Instance.ExportTC(QueryForExcelVehicle);
        GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
        ExcelFile workbook = new ExcelFile();
        workbook.Worksheets.Add("Export");
        ExcelWorksheet worksheet = workbook.Worksheets["Export"];
        DataTC.Columns["SABBREVIATION"].ColumnName = "ผู้ขนส่ง";
        DataTC.Columns["SCARTYPE"].ColumnName = "รูปแบบ";
        DataTC.Columns["SHEADREGISTERNO"].ColumnName = "ทะเบียนหัวลาก";
        DataTC.Columns["STRAILERREGISTERNO"].ColumnName = "ทะเบียนหางลาก";
        DataTC.Columns["SCARCATEGORY"].ColumnName = "ประเภทรถ";
        DataTC.Columns["NSLOT"].ColumnName = "จำนวนช่อง";
        DataTC.Columns["NTOTALCAPACITY"].ColumnName = "ปริมาตร";
        DataTC.Columns["DUPDATE"].ColumnName = "วันที่อัพเดตข้อมูล";
        DataTC.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";
        DataTC.Columns["CLASSGRP"].ColumnName = "CLASS GROUP";
        DataTC.Columns["STATUS"].ColumnName = "สถานะ";
        DataTC.Columns.Remove("STRUCKID");
        DataTC.Columns.Remove("SCARTYPEID");
        DataTC.Columns.Remove("CACTIVE");
        DataTC.Columns.Remove("SCONTRACTID");
        DataTC.Columns.Remove("SVENDORID");
        DataTC.Columns.Remove("ISUSE");
        worksheet.InsertDataTable(DataTC, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
        for (int j = 0; j < DataTC.Columns.Count; j++)
        {//Export Detail
            this.SetFormatCell(worksheet.Cells[0, j], DataTC.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
            worksheet.Columns[j].AutoFit();
        }
        string Path = this.CheckPath();
        string FileName = "Truck_Vehicle_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";
        workbook.Save(Path + "\\" + FileName);
        this.DownloadFile(Path, FileName);
    }
    #endregion
    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    #endregion
}