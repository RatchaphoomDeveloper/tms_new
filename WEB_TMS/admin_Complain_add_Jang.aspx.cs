﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Text;
using System.Configuration;

public partial class admin_Complain_add_Jang : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string TempDirectory = "UploadFile/Complain/{0}/{2}/{1}/";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;           
            bool boolchk = false;
            string username = Session["UserName"] + "";
            lblComplainBy.Text = username;
            lblUserCreate.Text = username;

            dteDateTrans.Value = DateTime.Now;
            dteDateClose.Value = DateTime.Now;
            dteDateComplain.Value = DateTime.Now;

            Session["addSSERVICEID"] = null;
            if (Session["oSSERVICEID"] != null)
            {
                Session["addSSERVICEID"] = Session["oSSERVICEID"];
                BindData();
                boolchk = true;
                txtChkEdit.Text = "1";
            }
            else
            {

                SetDetailFromQuaryString();

            }

            ASPxPageControl1.TabPages[1].ClientEnabled = boolchk;
            ASPxPageControl1.TabPages[2].ClientEnabled = boolchk;

            if (Session["CGROUP"] != null)
            {
                if (CanWrite)
                {
                    txtChkTerminal.Text = "1";
                    ASPxPageControl1.TabPages.FindByName("t3").ClientEnabled = false;
                }
            }


        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    void ClearControl()
    {

    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if (CanWrite)
        {
            switch (paras[0])
            {

                case "Save":

                    if (Session["addSSERVICEID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TCOMPLAIN SET SCONTRACTID = :SCONTRACTID, SHEADCOMLIAIN = :SHEADCOMLIAIN,SSUBJECT = :SSUBJECT,SDETAIL = :SDETAIL,SDELIVERYNO = :SDELIVERYNO,SHEADREGISTERNO = :SHEADREGISTERNO,STRAILERREGISTERNO = :STRAILERREGISTERNO,STRUCKID = :STRUCKID,SVENDORID = :SVENDORID,SPERSONALNO = :SPERSONALNO,SEMPLOYEENAME = :SEMPLOYEENAME,DDELIVERY = :DDELIVERY,TDELIVERY = :TDELIVERY,SCOMPLAINADDRESS = :SCOMPLAINADDRESS,STERMINALID = :STERMINALID,
                                       SCOMPLAINFNAME = :SCOMPLAINFNAME,SCOMPLAINLNAME = :SCOMPLAINLNAME,DDATECOMPLAIN = :DDATECOMPLAIN,TTIMECOMPLAIN = :TTIMECOMPLAIN,CTYPEPERSON = :CTYPEPERSON,SCOMPLAINBY = :SCOMPLAINBY,SUPDATE = :SUPDATE,DUPDATE = SYSDATE,SPRODUCTTYPEID = :SPRODUCTTYPEID,DSERVICE = :DSERVICE WHERE SSERVICEID = :SSERVICEID";

                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SSERVICEID", OracleType.VarChar).Value = Session["addSSERVICEID"] + "";
                                com.Parameters.Add(":SHEADCOMLIAIN", OracleType.VarChar).Value = cboComplainType.Value + "";
                                com.Parameters.Add(":SSUBJECT", OracleType.VarChar).Value = cmbTopic.Value + "";
                                com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = txtDetail.Text;
                                com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = txtEmployee.Text;
                                com.Parameters.Add(":DDELIVERY", OracleType.DateTime).Value = dteDateTrans.Value + "";
                                com.Parameters.Add(":TDELIVERY", OracleType.VarChar).Value = teTimeTrans.Text;
                                com.Parameters.Add(":SCOMPLAINADDRESS", OracleType.VarChar).Value = txtComplainAddress.Text;
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cbxOrganiz.Value + "";
                                com.Parameters.Add(":SCOMPLAINFNAME", OracleType.VarChar).Value = txtFName.Text;
                                com.Parameters.Add(":SCOMPLAINLNAME", OracleType.VarChar).Value = txtLName.Text;
                                com.Parameters.Add(":DDATECOMPLAIN", OracleType.DateTime).Value = dteDateComplain.Value;
                                com.Parameters.Add(":TTIMECOMPLAIN", OracleType.VarChar).Value = teTimeComplain.Text;
                                com.Parameters.Add(":CTYPEPERSON", OracleType.Char).Value = "1";
                                com.Parameters.Add(":SCOMPLAINBY", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = rblTypeProduct.Value + "";
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = cboContract.Value + "";
                                com.Parameters.Add(":DSERVICE", OracleType.DateTime).Value = dteDateTrans.Value + "";
                                
                                com.ExecuteNonQuery();

                            }

                        }

                        LogUser("21", "E", "แก้ไขข้อมูลหน้า รับเรื่องร้องเรียน", Session["addSSERVICEID"] + "");
                    }
                    else
                    {

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string GenID = CommonFunction.Gen_ID(con, "SELECT SSERVICEID FROM (SELECT SSERVICEID FROM TCOMPLAIN ORDER BY CAST(SSERVICEID AS INT) DESC)  WHERE ROWNUM <= 1");

                            string strsql = @"INSERT INTO TCOMPLAIN(SSERVICEID,SHEADCOMLIAIN,SSUBJECT,SDETAIL,SDELIVERYNO,SHEADREGISTERNO,STRAILERREGISTERNO,STRUCKID,SVENDORID,SPERSONALNO,SEMPLOYEENAME,DDELIVERY,TDELIVERY,SCOMPLAINADDRESS,STERMINALID,SCOMPLAINFNAME,SCOMPLAINLNAME,DDATECOMPLAIN,TTIMECOMPLAIN,CTYPEPERSON,SCOMPLAINBY,SCREATE,DCREATE,SUPDATE,DUPDATE,SPRODUCTTYPEID,SCONTRACTID,DSERVICE)
                                          VALUES(:SSERVICEID,:SHEADCOMLIAIN,:SSUBJECT,:SDETAIL,:SDELIVERYNO,:SHEADREGISTERNO,:STRAILERREGISTERNO,:STRUCKID,:SVENDORID,:SPERSONALNO,:SEMPLOYEENAME,:DDELIVERY,:TDELIVERY,:SCOMPLAINADDRESS,:STERMINALID,:SCOMPLAINFNAME,:SCOMPLAINLNAME,:DDATECOMPLAIN,:TTIMECOMPLAIN,:CTYPEPERSON,:SCOMPLAINBY,:SCREATE,SYSDATE,:SUPDATE,SYSDATE,:SPRODUCTTYPEID,:SCONTRACTID,:DSERVICE)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SSERVICEID", OracleType.VarChar).Value = GenID;
                                com.Parameters.Add(":SHEADCOMLIAIN", OracleType.VarChar).Value = cboComplainType.Value + "";
                                com.Parameters.Add(":SSUBJECT", OracleType.VarChar).Value = cmbTopic.Value + "";
                                com.Parameters.Add(":SDETAIL", OracleType.VarChar).Value = txtDetail.Text;
                                com.Parameters.Add(":SDELIVERYNO", OracleType.VarChar).Value = cboDelivery.Value + "";
                                com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = cboHeadRegist.Value + "";
                                com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = txtTrailerRegist.Text;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = txtTruckID.Text;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                                com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = cmbPersonalNo.Value + "";
                                com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = txtEmployee.Text;
                                com.Parameters.Add(":DDELIVERY", OracleType.DateTime).Value = dteDateTrans.Value + "";
                                com.Parameters.Add(":TDELIVERY", OracleType.VarChar).Value = teTimeTrans.Text;
                                com.Parameters.Add(":SCOMPLAINADDRESS", OracleType.VarChar).Value = txtComplainAddress.Text;
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cbxOrganiz.Value + "";
                                com.Parameters.Add(":SCOMPLAINFNAME", OracleType.VarChar).Value = txtFName.Text;
                                com.Parameters.Add(":SCOMPLAINLNAME", OracleType.VarChar).Value = txtLName.Text;
                                com.Parameters.Add(":DDATECOMPLAIN", OracleType.DateTime).Value = dteDateComplain.Value;
                                com.Parameters.Add(":TTIMECOMPLAIN", OracleType.VarChar).Value = teTimeComplain.Text;
                                com.Parameters.Add(":CTYPEPERSON", OracleType.Char).Value = "1";
                                com.Parameters.Add(":SCOMPLAINBY", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = rblTypeProduct.Value + "";
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = cboContract.Value + "";
                                com.Parameters.Add(":DSERVICE", OracleType.DateTime).Value = dteDateTrans.Value + "";
                                com.ExecuteNonQuery();

                                Session["addSSERVICEID"] = GenID;

                            }

                        }

                        LogUser("21", "I", "บันทึกข้อมูลหน้า รับเรื่องร้องเรียน", Session["addSSERVICEID"] + "");
                    }



                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                    ClearControl();
                    VisibleControlUpload();


                    break;

                case "Save1":

                    if (Session["addSSERVICEID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TCOMPLAIN SET DFINISH = :DFINISH, SSOLUTION = :SSOLUTION,SCOMPLAINBY = :SCOMPLAINBY,SPROTECT = :SPROTECT,SRESPONSIBLE = :SRESPONSIBLE,SUPDATE = :SUPDATE,DUPDATE = SYSDATE WHERE SSERVICEID = :SSERVICEID";

                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SSERVICEID", OracleType.VarChar).Value = Session["addSSERVICEID"] + "";
                                com.Parameters.Add(":SSOLUTION", OracleType.VarChar).Value = txtOperate.Text;
                                com.Parameters.Add(":SCOMPLAINBY", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SPROTECT", OracleType.VarChar).Value = txtProtect.Text;
                                com.Parameters.Add(":SRESPONSIBLE", OracleType.VarChar).Value = txtsEmployee.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":DFINISH", OracleType.DateTime).Value = (dteFinish.Value == null) ? DateTime.Now : dteFinish.Value;
                                com.ExecuteNonQuery();

                            }

                            using (OracleCommand comDelfile = new OracleCommand("DELETE FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID = '" + Session["addSSERVICEID"] + "'", con))
                            {
                                comDelfile.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TCOMPLAINTIMPORTFILE(NID,SSERVICEID,SDOCUMENT,SFILENAME,SGENFILENAME,SFILEPATH,SCREATE,DCREATE) VALUES (:NID,:SSERVICEID,:SDOCUMENT,:SFILENAME,'',:SFILEPATH,:SCREATE,SYSDATE)";

                            string[] sDucumentName = new string[8];

                            sDucumentName[0] = "บันทึกการสอบสวน/คาให้การโดย ปง. หรือ คลัง";
                            sDucumentName[1] = "บันทึกยอมรับผิด โดย พขร.";
                            sDucumentName[2] = "ใบกากับการขนส่ง";
                            sDucumentName[3] = "สรุปข้อมูล GPS";
                            sDucumentName[4] = "รูปถ่าย";
                            sDucumentName[5] = "ใบตรวจสอบสภาพรถขนส่ง";
                            sDucumentName[6] = "เอกสารอื่นๆ (ถ้ามี)";
                            sDucumentName[7] = "รายงานบันทึกการแก้ไขป้องกัน";


                            for (int i = 0; i < 8; i++)
                            {
                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {

                                    ASPxTextBox txtFileName = (ASPxTextBox)ASPxPageControl1.FindControl("txtFileName" + i);
                                    ASPxTextBox txtFilePath = (ASPxTextBox)ASPxPageControl1.FindControl("txtFilePath" + i);

                                    if (txtFileName != null && txtFilePath != null)
                                    {
                                        if ("" + txtFilePath.Text != "")
                                        {
                                            com1.Parameters.Clear();
                                            com1.Parameters.Add(":NID", OracleType.Number).Value = i + 1;
                                            com1.Parameters.Add(":SSERVICEID", OracleType.Number).Value = Session["addSSERVICEID"] + "";
                                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                            com1.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = txtFilePath.Text;
                                            com1.Parameters.Add(":SDOCUMENT", OracleType.VarChar).Value = sDucumentName[i];
                                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                            com1.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }

                            if (txtChkTerminal.Text == "1")
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_Complain_lst_Jang.aspx';});");
                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_Complete + "');");
                            }
                            VisibleControlUpload();
                        }
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลพื้นฐานก่อน');");
                    }


                    break;

                case "Save2":

                    if (Session["addSSERVICEID"] != null)
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = @"UPDATE TCOMPLAIN SET STOPICID = :STOPICID,CCUT = :CCUT,CSTATUS = :CSTATUS,DCLOSE = :DCLOSE,TCLOSE = :TCLOSE,SUPDATE = :SUPDATE,DUPDATE = SYSDATE, NPOINT = :NPOINT,SVERSION = :SVERSION WHERE SSERVICEID = :SSERVICEID";

                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SSERVICEID", OracleType.VarChar).Value = Session["addSSERVICEID"] + "";
                                com.Parameters.Add(":STOPICID", OracleType.VarChar).Value = cboTopicName.Value + "";
                                com.Parameters.Add(":CCUT", OracleType.VarChar).Value = rblCut.Value + "";
                                com.Parameters.Add(":CSTATUS", OracleType.VarChar).Value = rblStatus.Value + "";
                                com.Parameters.Add(":DCLOSE", OracleType.DateTime).Value = dteDateClose.Value;
                                com.Parameters.Add(":TCLOSE", OracleType.VarChar).Value = teTimeClose.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NPOINT", OracleType.Number).Value = (("" + txtPoint.Text.Replace("-", "") != "") ? ("" + txtPoint.Text.Replace("-", "")) : "0");
                                com.Parameters.Add(":SVERSION", OracleType.Number).Value = txtVersion.Text;
                                com.ExecuteNonQuery();

                            }


                        }

                        int count = CommonFunction.Count_Value(sql, "SELECT * FROM TREDUCEPOINT WHERE SREDUCETYPE = '07' AND SPROCESSID = '080' AND SREFERENCEID = '" + Session["addSSERVICEID"] + "'");
                        if (count > 0)
                        {
                            using (OracleConnection con11 = new OracleConnection(sql))
                            {
                                if (con11.State == ConnectionState.Closed) con11.Open();
                                if ("" + rblCut.Value == "1")
                                {
                                    using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '1', NPOINT = " + (("" + txtPoint.Text.Replace("-", "") != "") ? ("" + txtPoint.Text.Replace("-", "")) : "0") + " WHERE SREDUCETYPE = '07' AND SPROCESSID = '080' AND SREFERENCEID = '" + Session["addSSERVICEID"] + "'", con11))
                                    {
                                        comUpdate.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    using (OracleCommand comUpdate = new OracleCommand("UPDATE TREDUCEPOINT SET CACTIVE = '0' WHERE SREDUCETYPE = '07' AND SPROCESSID = '080' AND SREFERENCEID = '" + Session["addSSERVICEID"] + "'", con11))
                                    {
                                        comUpdate.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                        else
                        {
                            //DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"))
                            //string REDUCEDATE = dteDateTrans.Date.ToString("dd/MM/yyyy",new CultureInfo("en-US"));
                            string REDUCEDATE = dteDateTrans.Date.ToString("dd/MM/yyyy");

                            if ("" + rblCut.Value == "1")
                                //SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT ,SCHECKID    ,SCONTRACTID ,SHEADID ,SHEADERREGISTERNO ,STRAILERID ,STRAILERREGISTERNO ,SDRIVERNO
                                ReducePoint("07", "080", "ข้อร้องเรียน", "", cboTopicName.Value + "", txtVersion.Text, "" + (("" + txtPoint.Text.Replace("-", "") != "") ? ("" + txtPoint.Text.Replace("-", "")) : "0"), Session["addSSERVICEID"] + ""
                                    , cboContract.Value + "", txtTruckID.Text + "", cboHeadRegist.Value + "", "", txtTrailerRegist.Text + "", hideDriverID.Text + "", REDUCEDATE);

                        }

                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_Complain_lst_Jang.aspx';});");
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาบันทึกข้อมูลพื้นฐานก่อน');");
                    }

                    break;

                case "DeliverySelect":
                    cboHeadRegist.Value = "";
                    break;

                case "deleteFile":

                    string FilePath = paras[1];

                    if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                    }

                    string cNo = paras[2];
                    if (cNo == "1")
                    {
                        txtFileName0.Text = "";
                        txtFilePath0.Text = "";
                    }
                    else if (cNo == "2")
                    {
                        txtFileName1.Text = "";
                        txtFilePath1.Text = "";
                    }
                    else if (cNo == "3")
                    {
                        txtFileName2.Text = "";
                        txtFilePath2.Text = "";
                    }
                    else if (cNo == "4")
                    {
                        txtFileName3.Text = "";
                        txtFilePath3.Text = "";
                    }
                    else if (cNo == "5")
                    {
                        txtFileName4.Text = "";
                        txtFilePath4.Text = "";
                    }
                    else if (cNo == "6")
                    {
                        txtFileName5.Text = "";
                        txtFilePath5.Text = "";
                    }
                    else if (cNo == "7")
                    {
                        txtFileName6.Text = "";
                        txtFilePath6.Text = "";
                    }
                    else if (cNo == "8")
                    {
                        txtFileName7.Text = "";
                        txtFilePath7.Text = "";
                    }

                    VisibleControlUpload();
                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }

    }
    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID,SCONTRACTID,SVENDORID FROM (SELECT ROW_NUMBER()OVER(ORDER BY T.SHEADREGISTERNO) AS RN , T.SHEADREGISTERNO,T.STRAILERREGISTERNO,t.STRUCKID,c.SCONTRACTID,c.SVENDORID FROM (TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID) INNER JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID 
WHERE  NVL(c.CACTIVE,'Y') = 'Y'  AND  t.SHEADREGISTERNO LIKE :fillter AND c.SVENDORID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cboDelivery_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsTruck.SelectCommand = @"SELECT SDELIVERYNO,SVENDORID,SHEADREGISTERNO, STRAILERREGISTERNO,STRUCKID,SCONTRACTID FROM(SELECT pl.SDELIVERYNO, p.STRAILERREGISTERNO, tk.STRUCKID,p.SVENDORID,p.SHEADREGISTERNO,CT.SCONTRACTID ,ROW_NUMBER()OVER(ORDER BY pl.SDELIVERYNO) AS RN 
FROM ((TPlanScheduleList pl LEFT JOIN TPLANSCHEDULE p ON PL.NPLANID = P.NPLANID  ) 
LEFT JOIN TTRUCK tk ON p.SHEADREGISTERNO = TK.SHEADREGISTERNO ) LEFT JOIN TCONTRACT_TRUCK ct ON TK.STRUCKID = CT.STRUCKID
WHERE pl.CACTIVE = '1' AND p.CACTIVE = '1' AND pl.SDELIVERYNO LIKE :fillter AND p.SVENDORID LIKE :fillter1 AND p.SHEADREGISTERNO LIKE :fillter2 ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsTruck.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();

    }
    protected void cboDelivery_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID) LEFT JOIN TEMPLOYEE ep ON V.SVENDORID = EP.STRANS_ID
WHERE V.SVENDORID like :fillter4 AND V.SVENDORNAME  like :fillter AND T.SHEADREGISTERNO  LIKE :fillter2 AND ep.SPERSONELNO LIKE :fillter3
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", cboHeadRegist.Value + ""));
        sdsVendor.SelectParameters.Add("fillter3", TypeCode.String, String.Format("%{0}%", cmbPersonalNo.Value + ""));
        sdsVendor.SelectParameters.Add("fillter4", TypeCode.String, String.Format("%{0}%", hideVendor.Text));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void cmbPersonalNo_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsPersonal.SelectCommand = @"SELECT SPERSONELNO,FULLNAME,STRANS_ID,SEMPLOYEEID FROM (SELECT E.SPERSONELNO,E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,E.STEL ,E.STRANS_ID, E.SEMPLOYEEID , ROW_NUMBER()OVER(ORDER BY E.SEMPLOYEEID) AS RN  FROM TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es  ON E.SEMPLOYEEID = ES.SEMPLOYEEID WHERE nvl(E.CACTIVE,'1') = '1' AND  E.SPERSONELNO LIKE :fillter AND E.STRANS_ID LIKE :fillter1) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsPersonal.SelectParameters.Clear();
        sdsPersonal.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsPersonal.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsPersonal.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsPersonal.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsPersonal;
        comboBox.DataBind();

    }
    protected void cmbPersonalNo_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = "SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE T.STERMINALID || TS.STERMINALNAME LIKE :fillter AND (T.STERMINALID LIKE :Plant5 OR T.STERMINALID LIKE :Plant8))  WHERE RN BETWEEN :startIndex AND :endIndex";
        sdsOrganiz.SelectParameters.Clear();
        sdsOrganiz.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("Plant5", PlantHelper.Plant5);
        sdsOrganiz.SelectParameters.Add("Plant8", PlantHelper.Plant8);

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

    }
    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboContract_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsContract.SelectCommand = @"SELECT SCONTRACTID ,SCONTRACTNO FROM (SELECT ROW_NUMBER()OVER(ORDER BY C.SCONTRACTID) AS RN , C.SCONTRACTID,C.SCONTRACTNO FROM TCONTRACT c  LEFT JOIN TEMPLOYEE es ON c.SVENDORID = es.STRANS_ID 
WHERE c.SCONTRACTID like :fillter3 AND C.SCONTRACTNO like :fillter AND C.SVENDORID LIKE :fillter1 AND es.SEMPLOYEEID  LIKE :fillter2 
 GROUP BY C.SCONTRACTID,C.SCONTRACTNO ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsContract.SelectParameters.Clear();
        sdsContract.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsContract.SelectParameters.Add("fillter1", TypeCode.String, String.Format("%{0}%", cboVendor.Value + ""));
        sdsContract.SelectParameters.Add("fillter2", TypeCode.String, String.Format("%{0}%", hideDriverID.Text + ""));
        sdsContract.SelectParameters.Add("fillter3", TypeCode.String, String.Format("%{0}%", hideContract.Text));
        sdsContract.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsContract.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsContract;
        comboBox.DataBind();

    }
    protected void cboContract_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    private void BindData()
    {
        DataTable dt = new DataTable();
        string strsql = @"SELECT c.DFINISH, c.SVERSION, c.SCONTRACTID, c.SHEADCOMLIAIN,c.SSUBJECT,c.SDETAIL,c.SSOLUTION,c.DCLOSE,c.SDELIVERYNO,c.SHEADREGISTERNO,c.STRAILERREGISTERNO,c.STRUCKID,c.SVENDORID,c.SPERSONALNO,c.SEMPLOYEENAME,c.DDELIVERY,c.TDELIVERY,
c.SCOMPLAINADDRESS,c.STERMINALID,c.SCOMPLAINFNAME,c.SCOMPLAINLNAME,c.DDATECOMPLAIN,c.TTIMECOMPLAIN,c.CTYPEPERSON,c.SCOMPLAINBY,c.SPROTECT,c.SRESPONSIBLE,c.STOPICID,c.CCUT,c.CSTATUS,c.TCLOSE,c.SPRODUCTTYPEID,c.NPOINT ,
CASE WHEN ap.SAPPEALID IS NULL THEN   
nvl(c.CSTATUS,'1') 
ELSE
    (CASE WHEN ap.CSTATUS = '0' OR ap.CSTATUS = '1' OR ap.CSTATUS = '2' OR ap.CSTATUS = '4' OR ap.CSTATUS = '5' THEN '2'  ELSE  (CASE WHEN ap.CSTATUS = '6' OR ap.CSTATUS = '3'THEN '2' END)  END) 
END  As STATUS
FROM TCOMPLAIN c  LEFT JOIN TAPPEAL ap ON C.SSERVICEID = AP.SREFERENCEID AND AP.SAPPEALTYPE='080' WHERE c.SSERVICEID = '" + Session["oSSERVICEID"] + "'";

        dt = CommonFunction.Get_Data(sql, strsql);

        if (dt.Rows.Count > 0)
        {
            cboContract_OnItemRequestedByValueSQL(cboContract, new ListEditItemRequestedByValueEventArgs(dt.Rows[0]["SCONTRACTID"] + ""));
            cboVendor_OnItemRequestedByValueSQL(cboVendor, new ListEditItemRequestedByValueEventArgs(dt.Rows[0]["SVENDORID"] + ""));

            DateTime date;
            cboComplainType.Value = dt.Rows[0]["SHEADCOMLIAIN"] + "";
            cmbTopic.DataBind();
            cmbTopic.Value = dt.Rows[0]["SSUBJECT"] + "";
            txtDetail.Text = dt.Rows[0]["SDETAIL"] + "";
            txtOperate.Text = dt.Rows[0]["SSOLUTION"] + "";
            dteDateClose.Value = DateTime.TryParse(dt.Rows[0]["DCLOSE"] + "", out date) ? date : DateTime.Now;
            cboDelivery.Value = dt.Rows[0]["SDELIVERYNO"] + "";
            cboHeadRegist.Value = dt.Rows[0]["SHEADREGISTERNO"] + "";
            txtTrailerRegist.Text = dt.Rows[0]["STRAILERREGISTERNO"] + "";
            txtTruckID.Text = dt.Rows[0]["STRUCKID"] + "";
            cboVendor.Value = dt.Rows[0]["SVENDORID"] + "";
            cmbPersonalNo.Value = dt.Rows[0]["SPERSONALNO"] + "";
            txtEmployee.Text = dt.Rows[0]["SEMPLOYEENAME"] + "";
            dteDateTrans.Value = DateTime.TryParse(dt.Rows[0]["DDELIVERY"] + "", out date) ? date : DateTime.Now;
            teTimeTrans.Text = dt.Rows[0]["TDELIVERY"] + "";
            txtComplainAddress.Text = dt.Rows[0]["SCOMPLAINADDRESS"] + "";
            cbxOrganiz.Value = dt.Rows[0]["STERMINALID"] + "";
            txtFName.Text = dt.Rows[0]["SCOMPLAINFNAME"] + "";
            txtLName.Text = dt.Rows[0]["SCOMPLAINLNAME"] + "";
            dteDateComplain.Value = DateTime.TryParse(dt.Rows[0]["DDATECOMPLAIN"] + "", out date) ? date : DateTime.Now;
            teTimeComplain.Text = dt.Rows[0]["TTIMECOMPLAIN"] + "";
            txtProtect.Text = dt.Rows[0]["SPROTECT"] + "";
            txtsEmployee.Text = dt.Rows[0]["SRESPONSIBLE"] + "";
            cboTopicName.Value = dt.Rows[0]["STOPICID"] + "";
            rblCut.Value = dt.Rows[0]["CCUT"] + "";
            rblStatus.Value = dt.Rows[0]["STATUS"] + "";
            dteDateClose.Value = DateTime.TryParse(dt.Rows[0]["DCLOSE"] + "", out date) ? date : DateTime.Now;
            teTimeClose.Text = dt.Rows[0]["TCLOSE"] + "";
            rblTypeProduct.Value = dt.Rows[0]["SPRODUCTTYPEID"] + "";
            cboContract.Value = dt.Rows[0]["SCONTRACTID"] + "";
            txtVersion.Text = dt.Rows[0]["SVERSION"] + "";
            dteFinish.Value = DateTime.TryParse(dt.Rows[0]["DFINISH"] + "", out date) ? date : DateTime.Now;

            DataTable dt1 = new DataTable();
            dt1 = CommonFunction.Get_Data(sql, "SELECT SCOL02,SCOL03,SCOL04,SCOL06,NMULTIPLEIMPACT,NPOINT FROM TTOPIC WHERE STOPICID = '" + dt.Rows[0]["STOPICID"] + "" + "'");
            if (dt1.Rows.Count > 0)
            {
                txtCal2.Text = dt1.Rows[0]["SCOL02"] + "";
                txtCal3.Text = dt1.Rows[0]["SCOL03"] + "";
                txtCal4.Text = dt1.Rows[0]["SCOL04"] + "";
                txtCal6.Text = dt1.Rows[0]["SCOL06"] + "";
                txtMultipleImpact.Text = dt1.Rows[0]["NMULTIPLEIMPACT"] + "";
                txtPoint.Text = dt.Rows[0]["NPOINT"] + "";
            }

            DataTable dtFile = CommonFunction.Get_Data(sql, "SELECT NID, SFILENAME,SFILEPATH FROM TCOMPLAINTIMPORTFILE WHERE SSERVICEID ='" + Session["oSSERVICEID"] + "'");
            foreach (DataRow dr in dtFile.Rows)
            {
                switch ("" + dr["NID"])
                {
                    case "1":
                        txtFileName0.Text = dr["SFILENAME"] + "";
                        txtFilePath0.Text = dr["SFILEPATH"] + "";
                        break;
                    case "2":
                        txtFileName1.Text = dr["SFILENAME"] + "";
                        txtFilePath1.Text = dr["SFILEPATH"] + "";
                        break;
                    case "3":
                        txtFileName2.Text = dr["SFILENAME"] + "";
                        txtFilePath2.Text = dr["SFILEPATH"] + "";
                        break;
                    case "4":
                        txtFileName3.Text = dr["SFILENAME"] + "";
                        txtFilePath3.Text = dr["SFILEPATH"] + "";
                        break;
                    case "5":
                        txtFileName4.Text = dr["SFILENAME"] + "";
                        txtFilePath4.Text = dr["SFILEPATH"] + "";
                        break;
                    case "6":
                        txtFileName5.Text = dr["SFILENAME"] + "";
                        txtFilePath5.Text = dr["SFILEPATH"] + "";
                        break;
                    case "7":
                        txtFileName6.Text = dr["SFILENAME"] + "";
                        txtFilePath6.Text = dr["SFILEPATH"] + "";
                        break;
                    case "8":
                        txtFileName7.Text = dr["SFILENAME"] + "";
                        txtFilePath7.Text = dr["SFILEPATH"] + "";
                        break;
                }


            }

            VisibleControlUpload();

        }
    }
    protected void uplExcel_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            ASPxUploadControl upl = (ASPxUploadControl)sender;
            int count = _Filename.Count() - 1;

            string genName = "complain" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }
    }
    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    private void VisibleControlUpload()
    {
        bool visible = string.IsNullOrEmpty(txtFilePath0.Text);
        uplExcel0.ClientVisible = visible;
        txtFileName0.ClientVisible = !visible;
        btnView0.ClientEnabled = !visible;
        btnDelFile0.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath1.Text);
        uplExcel1.ClientVisible = visible;
        txtFileName1.ClientVisible = !visible;
        btnView1.ClientEnabled = !visible;
        btnDelFile1.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath2.Text);
        uplExcel2.ClientVisible = visible;
        txtFileName2.ClientVisible = !visible;
        btnView2.ClientEnabled = !visible;
        btnDelFile2.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath3.Text);
        uplExcel3.ClientVisible = visible;
        txtFileName3.ClientVisible = !visible;
        btnView3.ClientEnabled = !visible;
        btnDelFile3.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath4.Text);
        uplExcel4.ClientVisible = visible;
        txtFileName4.ClientVisible = !visible;
        btnView4.ClientEnabled = !visible;
        btnDelFile4.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath5.Text);
        uplExcel5.ClientVisible = visible;
        txtFileName5.ClientVisible = !visible;
        btnView5.ClientEnabled = !visible;
        btnDelFile5.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath6.Text);
        uplExcel6.ClientVisible = visible;
        txtFileName6.ClientVisible = !visible;
        btnView6.ClientEnabled = !visible;
        btnDelFile6.ClientEnabled = !visible;

        visible = string.IsNullOrEmpty(txtFilePath7.Text);
        uplExcel7.ClientVisible = visible;
        txtFileName7.ClientVisible = !visible;
        btnView7.ClientEnabled = !visible;
        btnDelFile7.ClientEnabled = !visible;

    }
    protected bool ReducePoint(string REDUCETYPE, string PROCESSID, params string[] sArrayParams)
    {//SREDUCENAME ,SCHECKLISTID ,STOPICID ,SVERSIONLIST ,NPOINT
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(this.Page, connection);
        repoint.NREDUCEID = "";
        repoint.DREDUCE = "" + sArrayParams[12];
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = "" + Session["UserID"];
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        repoint.SDELIVERYNO = "" + sArrayParams[11];

        //repoint.Insert("IUTREDUCEPOINT_Complain", dteDateTrans.Date);
        return IsReduce;
    }
    private void SetDetailFromQuaryString()
    {
        string str = Request.QueryString["str"];
        string[] strQuery;
        if (!string.IsNullOrEmpty(str))
        {
            DateTime date;
            strQuery = STCrypt.DecryptURL(str);
            cboDelivery.Value = strQuery[1] + "";
            cboHeadRegist.Value = strQuery[3] + "";
            txtTrailerRegist.Text = strQuery[4] + "";
            txtTruckID.Text = strQuery[5] + "";
            cboVendor.Value = strQuery[10] + "";
            cboContract.Value = strQuery[7] + "";
            dteDateTrans.Value = DateTime.TryParse(strQuery[2] + "", out date) ? date : DateTime.Now;
            teTimeTrans.Value = DateTime.TryParse(strQuery[2] + "", out date) ? date : DateTime.Now;
            cbxOrganiz.Value = strQuery[11] + "";
            cmbPersonalNo.Value = strQuery[9] + "";
            hideDriverID.Text = strQuery[8] + "";
        }
    }
    protected void ASPxPageControl1_ActiveTabChanged(object source, DevExpress.Web.ASPxTabControl.TabControlEventArgs e)
    {

    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}