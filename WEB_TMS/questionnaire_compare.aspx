﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="questionnaire_compare.aspx.cs" Inherits="questionnaire_compare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
             <asp:PostBackTrigger ControlID="btnExport"/>
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlVendorSearch.ClientID %>" class="col-md-2 control-label">ผู้ประกอบการขนส่ง</label>
                                <div class="col-md-3">
                                    <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlYearSearchStart.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearchStart" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlYearSearchEnd.ClientID %>" class="col-md-1 control-label">ถึง</label>
                                <div class="col-md-2">
                                    <div style="text-align: right;">
                                        <asp:DropDownList ID="ddlYearSearchEnd" runat="server" CssClass="form-control marginTp">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group form-horizontal row">
                                <div style="text-align: right;">
                                    <div class="col-md-11">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>คะแนนประเมนผลการบริหาร
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="true" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" RowStyle-HorizontalAlign="Center"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
