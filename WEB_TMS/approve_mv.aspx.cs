﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using System.Globalization;
using DevExpress.XtraEditors.Controls;
using System.Windows.Forms;
using DevExpress.Web.ASPxEditors;
using System.IO;
using DevExpress.Web.ASPxPopupControl;
using System.Net;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraReports.UI;
using System.Data.OracleClient;

public partial class approve_mv : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string sCheckWaterID = "01";
    private static decimal nSlotAll = 0;
    private static int nNextYeatCheckWater = 3;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null || Session["UserID"] + "" == "") { ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return; }

        if (!IsPostBack)
        {

            string SUID = Session["UserID"] + "";
            string strsql = "SELECT SUID, SVENDORID FROM TUSER WHERE SUID ='" + CommonFunction.ReplaceInjection(SUID) + "'";
            DataTable dt = CommonFunction.Get_Data(conn, strsql);
            string SVENDORID = "";
            SVENDORID = dt.Rows[0]["SVENDORID"] + "";
            Session["SVENDORID"] = SVENDORID;
            //cboReatype.DataBind();
            //cboReatype.Items.Insert(0, new ListEditItem("- ระบุประเภท -", ""));
            //cboReatype.SelectedIndex = 0;
            //cboReatypeT2.DataBind();
            //cboReatypeT2.Items.Insert(0, new ListEditItem("- ระบุประเภท -", ""));
            //cboReatypeT2.SelectedIndex = 0;

            //DateTime sDate = DateTime.Now.AddMonths(-1);
            //DateTime eDate = DateTime.Now;

            //dedtStart.Value = sDate;
            //dedtEnd.Value = eDate;
            //dedtStartT2.Value = sDate;
            //dedtEndT2.Value = eDate;

            Lisdata(SVENDORID, "N");
            //rblDateType.SelectedIndex = 0;
            //rblReqType.SelectedIndex = 0;
            //LisdataT2(SVENDORID);

            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                // .Columns[7].Visible = false;
               // gvwtab2.Columns[8].Visible = false;
                btnSearch.Enabled = false;                
                ASPxButton2.Enabled = false; //ตารางคิวงานที่ตอบรับ
                ASPxButton3.Enabled = false; //ค้นหาทั้งหมด
                btnPrintPlot.Enabled = false; //พิมพ์แผนงานประจำวัน
                btnReportbankForm.Enabled = false;   
            }

            if (!CanWrite)
            {   ASPxButton1.Enabled = false; //เพื่มคำขอร้อง
                gvw.Columns[9].Visible = false;
                gvw.Columns[10].Visible = false;
                gvw.Columns[11].Visible = false;
                gvwOther.Columns[6].Visible = false;
               // btnReq.Enabled = false;               
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

        Lisdata(Session["SVENDORID"] + "", "Y");
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string SVENDORID = Session["SVENDORID"] + "";

            string[] param = e.Parameter.Split(';');

            dynamic data = "";
            switch (param[0])
            {
                case "search":
                    Lisdata(Session["SVENDORID"] + "", "Y");

                    break;
                case "Unsearch":
                    Lisdata(Session["SVENDORID"] + "", "N");

                    break;
                case "edit":
                    data = gvw.GetRowValues(int.Parse(param[1]), "REQUEST_ID", "REQUEST_OTHER_FLAG", "REQTYPE_ID", "STATUS_FLAG", "RK_FLAG");
                    //if (data[4] == "M" && (data[3] == "09" || data[3] == "06"))
                    //{
                    //    //ถ้าแนบเอกสารใบเทียบแป้นแล้ว ให้ไปหน้าจัดคิว
                    //    DataTable dtdoc = CommonFunction.Get_Data(conn, "SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(data[0]) + "' AND DOC_TYPE = '0006'");
                    //    if (dtdoc.Rows.Count > 0)
                    //    {
                    //        xcpn.JSProperties["cpRedirectTo"] = "approve_add_mv.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&type=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1]));

                    //    }
                    //    else
                    //    {
                    //        xcpn.JSProperties["cpRedirectTo"] = "vendor_attachfile_mv.aspx?strReqID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&&statusID=" + Server.UrlEncode(STCrypt.Encrypt(data[3]));
                    //    }
                    //}
                    //else
                    //{
                    xcpn.JSProperties["cpRedirectTo"] = "approve_add_mv.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&type=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1]));
                    //}
                    break;
                case "request":
                    data = gvwOther.GetRowValues(int.Parse(param[1]), "REQUEST_ID", "REQUEST_OTHER_FLAG", "REQTYPE_ID");
                    xcpn.JSProperties["cpRedirectTo"] = "Requesting.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0])) + "&type=" + Server.UrlEncode(STCrypt.Encrypt("" + data[1]));
                    break;
                case "print":
                    string sPathDownload = @"UploadFile/ReportBankForm/";
                    string sFileName = "";
                    ASPxPopupControl PopUpPrint = (ASPxPopupControl)Master.FindControl("dxpopupPrint");
                    ASPxRadioButtonList rblReport = PopUpPrint.FindControl("rblReport") as ASPxRadioButtonList;
                    switch (rblReport.Value + "")
                    {
                        case "0": sFileName = "FM-001.pdf";
                            break;
                        case "1": sFileName = "FM-005.pdf";
                            break;
                        case "2": sFileName = "Check-list.pdf";
                            break;
                        case "3": sFileName = "FM-007.pdf";
                            break;
                        case "4": sFileName = "FM-012.pdf";
                            break;
                    }
                    string dirpath = sPathDownload + sFileName;
                    if (!string.IsNullOrEmpty(sFileName))
                    {
                        xcpn.JSProperties["cpExport"] = "";
                        xcpn.JSProperties["cpNewTab"] = "openFile.aspx?str=" + Server.UrlDecode(dirpath);
                    }
                    else
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถดาว์โหลดเอกสารได้กรุณาลองอีกครั้ง')");
                    }

                    break;
                case "printReport":

                    ASPxPopupControl sPopUpPrint = (ASPxPopupControl)Master.FindControl("dxpopupPrint");
                    ASPxRadioButtonList srblReport = sPopUpPrint.FindControl("rblReport") as ASPxRadioButtonList;

                    xcpn.JSProperties["cpExport"] = srblReport.Value + "";
                    xcpn.JSProperties["cpNewTab"] = "";

                    break;
                case "newbill":

                    xcpn.JSProperties["cpRedirectTo"] = "service_add_mv.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("01")) + "&vehID=" + Server.UrlEncode(STCrypt.Encrypt("" + SVENDORID));
                    break;
                case "edit3": // บันทึกผล
                    data = gvw.GetRowValues(int.Parse(param[1]), "REQUEST_ID", "REQUEST_OTHER_FLAG", "REQTYPE_ID");
                    xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt("" + data[0]));
                    break;
                case "QWORK": // บันทึกผล
                    xcpn.JSProperties["cpRedirectTo"] = "CalendarView.aspx";


                    break;


            }
        }
    }

    protected void btnPrintPlot_Click(object sender, EventArgs e)
    {
        CreateReportPlan("" + edtStart1.Text.Trim(), "" + edtEnd1.Text.Trim());
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        //ASPxButton __btn = (ASPxButton)sender;
        //string REQ_ID = __btn.CommandArgument;
        //if (!string.IsNullOrEmpty(REQ_ID))
        //{
        //    CreateReportJob(REQ_ID);
        //}
        string sPathDownload = @"UploadFile/ReportBankForm/";
        string sFileName = "";
        ASPxPopupControl PopUpPrint = (ASPxPopupControl)Master.FindControl("dxpopupPrint");
        ASPxRadioButtonList chkReport1 = PopUpPrint.FindControl("rblReport") as ASPxRadioButtonList;
        //ASPxCheckBox chkReport2 = PopUpPrint.FindControl("chkReport2") as ASPxCheckBox;
        //ASPxCheckBox chkReport3 = PopUpPrint.FindControl("chkReport3") as ASPxCheckBox;
        //ASPxCheckBox chkReport4 = PopUpPrint.FindControl("chkReport4") as ASPxCheckBox;
        //ASPxCheckBox chkReport5 = PopUpPrint.FindControl("chkReport5") as ASPxCheckBox;

        switch (chkReport1.Value + "")
        {
            case "0": sFileName = "FM-มว.-001แบบใบรายงานวัดน้ำ.pdf";
                break;
            case "1": sFileName = "FM-มว.-005ตรวจสภาพก่อนวัดน้ำ.pdf";
                break;
            case "2": sFileName = "Check list ระยะหน้าท่อ,จรวด.pdf";
                break;
            case "3": sFileName = "FM-มว.-007(ใบวัดแห้ง).pdf";
                break;
            case "4": sFileName = "FM-มว.-012แบบบันทึกข้อมูลการลงน้ำ.pdf";
                break;
        }


        try
        {

            //Response.Clear();
            //Response.ContentType = "Application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(sFileName) + "");
            //Response.TransmitFile(sPathDownload + sFileName);
            //Response.End();
        }
        catch
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถดาว์โหลดเอกสารได้กรุณาลองอีกครั้ง')");
        }

    }

    private void Lisdata(string VENDOR_ID, string ACTIVE)
    {
        string CoditionAll = "";

        if (!string.IsNullOrEmpty(txtSearch.Text) && ACTIVE == "Y")
        {
            CoditionAll += " AND (TVEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%' OR TRUCK.SHEADREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%' OR TRUCK.STRAILERREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%')";
        }
        else
        {
            txtSearch.Text = "";
        }

        string ReqType = CommonFunction.ReplaceInjection(rblReqType.Value + "");
        if (!string.IsNullOrEmpty(ReqType) && ACTIVE == "Y")
        {
            CoditionAll += " AND TREQTYPE.REQTYPE_ID = '" + ReqType + "'";
        }
        else
        {
            rblReqType.Value = "";
        }

        string DateType = CommonFunction.ReplaceInjection(rblDateType.Value + "");
        if (!string.IsNullOrEmpty(DateType) && ACTIVE == "Y")
        {
            if (!string.IsNullOrEmpty(edtStart.Text.Trim() + "") && !string.IsNullOrEmpty(edtEnd.Text.Trim() + ""))
            {
                //DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
                //DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());
                switch (DateType)
                {
                    case "0":
                        CoditionAll += " AND (TRUNC(TREQ.REQUEST_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(edtStart.Text.Trim()) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(edtEnd.Text.Trim()) + "','dd/MM/yyyy')) ";
                        break;
                    case "1":
                        CoditionAll += " AND (TRUNC(TREQ.WATER_EXPIRE_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(edtStart.Text.Trim()) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(edtEnd.Text.Trim()) + "','dd/MM/yyyy')) ";
                        break;
                    case "2":
                        CoditionAll += " AND (TRUNC(TREQ.APPOINTMENT_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(edtStart.Text.Trim()) + "','dd/MM/yyyy') AND  TO_DATE('" + CommonFunction.ReplaceInjection(edtEnd.Text.Trim()) + "','dd/MM/yyyy')) ";
                        break;
                }
            }
        }
        else
        {
            //edtStart.Text = "";
            //edtEnd.Text = "";
            rblDateType.Value = "";
        }


        if (cblStatus.SelectedItems.Count > 0 && ACTIVE == "Y")
        {
            string SetSelect = "";
            foreach (ListEditItem item in cblStatus.Items)
            {
                if (item.Selected == true)
                {
                    SetSelect += ",'" + CommonFunction.ReplaceInjection(item.Value + "") + "'";
                }

            }
            SetSelect = SetSelect.Remove(0, 1);
            CoditionAll += " AND TSTATUS.STATUSREQ_ID IN (" + SetSelect + ")";
        }
        else
        {
            foreach (ListEditItem item in cblStatus.Items)
            {
                item.Selected = false;
            }
        }

        //if (dedtStart.Value != null && dedtEnd.Value != null)
        //{
        //    DateTime datestart = DateTime.Parse(dedtStart.Value.ToString());
        //    DateTime dateend = DateTime.Parse(dedtEnd.Value.ToString());

        //    CoditionAll += " AND (TRUNC(TREQ.REQUEST_DATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
        //}

        //if (!string.IsNullOrEmpty(cboReatype.Value + ""))
        //{
        //    CoditionAll += " AND TREQTYPE.REQTYPE_ID = '" + CommonFunction.ReplaceInjection(cboReatype.Value + "") + "'";
        //}

        string CoditionVendor = CommonFunction.ReplaceInjection(VENDOR_ID);

        string strsql = @"SELECT  DISTINCT TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE
                        ,TRUNC(TREQ.APPOINTMENT_DATE) as ACCEPT_DATE
                        ,TRUNC(TREQ.SERVICE_DATE) as SERVICE_DATE
                        ,NVL(TVEN.SABBREVIATION,TRUCK.SOWNER_NAME) as SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_ID,TREQTYPE.REQTYPE_NAME ,TREQ.REQUEST_OTHER_FLAG,TSTATUS.NORDER,TREQ.RK_FLAG
                        ,TREQ.CREATE_DATE 
                        FROM TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 --AND  SVENDORID = '" + CoditionVendor + @"'
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,SOWNER_NAME,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID 
                        WHERE 1=1  " + CoditionAll + @" AND (TSTATUS.NORDER > 2  {1} ) {0}
                        ORDER BY 
                         CASE WHEN TREQ.Status_Flag IN ('10','11') THEN null ELSE TREQ.CREATE_DATE END ASC NULLS LAST
                       ";
        //ป่าน 11/11/2015
        string AddCondition = string.Empty;
        foreach (ListEditItem item in cblStatus.Items)
        {
            string SetSelect = string.Empty;
            if (item.Selected == false)
            {
                if (string.Equals(item.Text, "ปิดงานแล้ว"))
                    AddCondition = "  AND TSTATUS.STATUSREQ_ID <> '10' ";
                SetSelect += ",'" + CommonFunction.ReplaceInjection(item.Value + "") + "'";
            }

        }
        //27 10 2558 ป่าน 

        //string strsql = SELECT  TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
        //                ,TREQ.Status_Flag,TRUNC(TREQ.WATER_EXPIRE_DATE) as DWATEREXPIRE
        //                ,TRUNC(TREQ.APPOINTMENT_DATE) as ACCEPT_DATE
        //                ,TRUNC(TREQ.SERVICE_DATE) as SERVICE_DATE
        //                ,NVL(TVEN.SABBREVIATION,TRUCK.SOWNER_NAME) as SABBREVIATION
        //                ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_ID,TREQTYPE.REQTYPE_NAME ,TREQ.REQUEST_OTHER_FLAG,TSTATUS.NORDER,TREQ.RK_FLAG
        //                FROM TBL_Request TREQ
        //                LEFT JOIN 
        //                (
        //                    SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 --AND  SVENDORID = '" + CoditionVendor + @"'
        //                ) TVEN
        //                ON TVEN.SVENDORID = TREQ.VENDOR_ID
        //                LEFT JOIN TBL_REQTYPE TREQTYPE
        //                ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
        //                LEFT JOIN TBL_STATUSREQ TSTATUS
        //                ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
        //                --ใช้หัวของ TBL_REQUEST จอย
        //                LEFT JOIN  
        //                (
        //                    SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,SOWNER_NAME,
        //                    CASE  SCARTYPEID
        //                    WHEN 0 THEN SHEADREGISTERNO
        //                    WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
        //                    ELSE ''
        //                    END REGISTERNO
        //                    ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
        //                    WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
        //                    FROM
        //                    (
        //                        SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
        //                     ) TRUCK
        //                     LEFT JOIN 
        //                     (
        //                        SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
        //                     )WATER
        //                    ON  WATER.SHEADID = TRUCK.STRUCKID
        //                )TRUCK
        //                ON TRUCK.STRUCKID = TREQ.VEH_ID 
        //                WHERE 1=1  " + CoditionAll + @" AND (TSTATUS.NORDER > 2  {1} ) {0}  
        //                ORDER BY 
        //////                 CASE WHEN TREQ.Status_Flag IN ('10','11') THEN null ELSE TREQ.CREATE_DATE END ASC NULLS LAST
        //               ";

        //ป่าน 10/11/2558
        //DataTable dt = CommonFunction.Get_Data(conn, string.Format(strsql, "AND TREQTYPE.REQTYPE_ID IN ('01','02','04') AND TREQTYPE.REQTYPE_ID <> '03'" + AddCondition, "OR RK_FLAG = 'M'"));
        DataTable dt = CommonFunction.Get_Data(conn, string.Format(strsql, "AND TREQTYPE.REQTYPE_ID IN ('01','02','04') AND TREQTYPE.REQTYPE_ID <> '03'" + AddCondition, "OR RK_FLAG = 'M'"));
        DataTable dt2 = CommonFunction.Get_Data(conn, string.Format(strsql, "AND TREQTYPE.REQTYPE_ID IN ('03')", "OR RK_FLAG = 'M'"));
        // lblsss.Text = strsql;

        if (dt.Rows.Count > 0)
        {
            //lblsss.Text = dt.Rows.Count + "";
            gvw.DataSource = dt;

        }
        else
        {

        }
        if (dt2.Rows.Count > 0)
        {
            //lblsss.Text = dt.Rows.Count + "";
            gvwOther.DataSource = dt2;

        }
        else
        {

        }
        gvw.DataBind();
        gvwOther.DataBind();
    }

    #region ฟังก์ชั่นต่างๆ
    private void CreateReportPlan(string DStart, string DEnd)
    {
        //DStart = DateTime.ParseExact(DStart, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None).ToString("dd/MM/yyyy");
        //DEnd = DateTime.ParseExact(DEnd, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None).ToString("dd/MM/yyyy");

        #region Prepare SOURCE

        string _sql = "", _condi = "", sDuration = "";

        DateTime sDT;
        if (DateTime.TryParse(DStart, out sDT))
            //DStart = sDT.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            DStart = DateTime.ParseExact(DStart, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None).ToString("dd/MM/yyyy");
        else
            DStart = "";
        if (DateTime.TryParse(DEnd, out sDT))
            //DEnd = sDT.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            DEnd = DateTime.ParseExact(DEnd, "dd/MM/yyyy", new CultureInfo("en-US"), DateTimeStyles.None).ToString("dd/MM/yyyy");
        else
            DEnd = "";

        //if (!string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        //{
        //    _condi = " AND REQ.REQUEST_DATE BETWEEN To_DATE('" + Convert.ToDateTime(DStart).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM') AND To_DATE('" + Convert.ToDateTime(DEnd).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
        //    sDuration = "ช่วงวันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        //}
        //else if (!string.IsNullOrEmpty(DStart) && string.IsNullOrEmpty(DEnd))
        //{
        //    _condi = " AND REQ.REQUEST_DATE >= To_DATE('" + Convert.ToDateTime(DStart).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
        //    sDuration = "ตั้งแต่วันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy");
        //}
        //else if (string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        //{
        //    _condi = " AND REQ.REQUEST_DATE <= To_DATE('" + Convert.ToDateTime(DEnd).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
        //    sDuration = "ก่อนวันที่ " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        //}

        if (!string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        {
            //AND REQ.REQUEST_DATE  = วันที่ยื่นคำขอ  เป็น REQ.SERVICE_DATE = วันที่นัดหมาย  ป่าน 29/10/2558//
            _condi = @" AND TRUNC(REQ.SERVICE_DATE) BETWEEN To_DATE('" + DateTime.Parse(DStart).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') AND To_DATE('" + DateTime.Parse(DEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')";
            sDuration = "ช่วงวันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        }
        else if (!string.IsNullOrEmpty(DStart) && string.IsNullOrEmpty(DEnd))
        {
            //AND REQ.REQUEST_DATE  = วันที่ยื่นคำขอ  เป็น REQ.SERVICE_DATE = วันที่นัดหมาย  ป่าน 29/10/2558//
            _condi = " AND TRUNC(REQ.SERVICE_DATE) >= To_DATE('" + DateTime.Parse(DStart).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')";
            sDuration = "ตั้งแต่วันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy");
        }
        else if (string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        {
            //AND REQ.REQUEST_DATE  = วันที่ยื่นคำขอ  เป็น REQ.SERVICE_DATE = วันที่นัดหมาย  ป่าน 29/10/2558//
            _condi = " AND TRUNC(REQ.SERVICE_DATE) <= To_DATE('" + DateTime.Parse(DEnd).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy')";
            sDuration = "ก่อนวันที่ " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        }


        _sql = @"SELECT REQ.REQUEST_ID,TRU.SCERT_NO As SCERT_NO,TU_NO,VEH_NO,TRT.SCARCATEGORY As SCARTYPE,REQ.TOTLE_CAP,VEN.SABBREVIATION,TYP.REQTYPE_NAME,CAU.CAUSE_NAME
        ,REQ.REQUEST_DATE,CASE WHEN REQ.ACCEPT_FLAG='Y' THEN REQ.ACCEPT_DATE END As ACCEPT_DATE,REQ.SERVICE_DATE,STA.STATUSREQ_NAME
        FROM TBL_REQUEST REQ
        LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
        LEFT JOIN TTRUCKTYPE TRT ON TRU.SCARTYPEID=TRT.SCARTYPEID
        LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
        LEFT JOIN TBL_REQTYPE TYP ON REQ.REQTYPE_ID=TYP.REQTYPE_ID
        LEFT JOIN TBL_CAUSE CAU ON REQ.CAUSE_ID=CAU.CAUSE_ID
        LEFT JOIN TBL_STATUSREQ STA ON REQ.Status_Flag=STA.STATUSREQ_ID
        WHERE 1=1 " + _condi;

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        if (dt.Rows.Count > 0)
        {
            dt.TableName = "dtReport";

            DataTable dtSum = new DataTable("dtSummary");
            dtSum.Columns.Add("STATUSREQ_NAME");
            dtSum.Columns.Add("NREQ");
            dtSum.Columns.Add("SREQ");

            if (dt.Rows.Count > 0)
            {
                string STATUSREQ_NAME = "", NREQ = "", SREQ = "";
                foreach (DataRow dr in CommonFunction.GroupDatable("", dt, "STATUSREQ_NAME", "", "STATUSREQ_NAME").Rows)
                {
                    STATUSREQ_NAME += (string.IsNullOrEmpty(STATUSREQ_NAME) ? "" : "\r\n") + dr["STATUSREQ_NAME"];
                    NREQ += (string.IsNullOrEmpty(NREQ) ? "" : "\r\n") + dt.Compute("COUNT(REQUEST_ID)", "STATUSREQ_NAME LIKE '" + dr["STATUSREQ_NAME"] + "'");
                    SREQ += (string.IsNullOrEmpty(SREQ) ? "" : "\r\n") + "คำขอ";
                }
                dtSum.Rows.Add(new object[] { STATUSREQ_NAME, NREQ, SREQ });
            }

            dsReportRequest ds = new dsReportRequest();
            ds.dtSummary.Merge(dtSum);
            ds.dtReport.Merge(dt);

        #endregion

            xrtReportRequest report = new xrtReportRequest();
            report.DataSource = ds;
            report.Parameters["Duration"].Value = sDuration;

            //report.Name = "แผนงานประจำวัน_" + DateTime.Now.ToString("MMddyyyyHHmmss");
            string fileName = "แผนงานประจำวัน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

            MemoryStream stream = new MemoryStream();
            report.ExportToXls(stream);

            Response.ContentType = "application/xls";
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".xls");
            Response.AddHeader("Content-Length", stream.Length.ToString());
            Response.ContentEncoding = System.Text.Encoding.ASCII;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }
        else
        {
            //CommonFunction.SetPopupOnLoad(xcpn, "dxError('แจ้งเตือน','ไม่พบข้อมูลแผนประจำวัน " + sDuration + "')");
            // Response.Write("dxError('แจ้งเตือน','ไม่พบข้อมูลแผนประจำวัน " + sDuration + "')");
            ClientScript.RegisterStartupScript(GetType(), "Javascript", "dxError('แจ้งเตือน','ไม่พบข้อมูลแผนประจำวัน <br>" + sDuration + "')", true);
        }
    }

    private void CreateReportJob(string REQ_ID)
    {
        #region Prepare SOURCE

        string _sql = "";

        _sql = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,CASE WHEN TRU.SCARTYPEID = '0' THEN TRU.SHEADREGISTERNO ELSE TRU.STRAILERREGISTERNO END  As REG_NO,TRU.SCERT_NO As SCERT_NO,TRU.NWHEELS,CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN '1' END As TRAILER
,CASE WHEN TRU.SCARTYPEID = '0' THEN '' ELSE  REQ.VEH_NO END  As HTRAILER,(CASE WHEN TRU.SCARTYPEID IN ('3','4') THEN (SELECT SBRAND FROM TTRUCK WHERE STRUCKID=REQ.STRUCKID AND ROWNUM=1) ELSE TRU.SBRAND END) As SBRAND,REQ.TOTLE_CAP,REQ.TOTLE_SLOT
,(SELECT RTRIM (XMLAGG (XMLELEMENT (e, NCAPACITY || ',')).EXTRACT ('//text()'), ',') FROM 
    (SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) As NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO ORDER BY STRUCKID,NCOMPARTNO)
    WHERE STRUCKID=TRU.STRUCKID GROUP BY STRUCKID) As NSLOT,NULL As FPRESSURE,NULL As BPRESSURE,'' As COMPAN,REQ.SERVICE_DATE
FROM TBL_REQUEST REQ LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'";

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        dt.TableName = "dtReport";

        _sql = @"SELECT REQ.REQUEST_ID,COMP.NCOMPARTNO,COMP.NPANLEVEL,COMP.NCAPACITY FROM TBL_REQUEST REQ
                 LEFT JOIN TTRUCK_COMPART COMP ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=COMP.STRUCKID
                 WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                 ORDER BY COMP.NCOMPARTNO,COMP.NPANLEVEL";
        DataTable dt2 = CommonFunction.Get_Data(conn, _sql);
        dt2.TableName = "dtCompart";


        dsReportJob ds = new dsReportJob();
        ds.dtReport.Merge(dt);
        ds.dtCompart.Merge(dt2);

        #endregion

        xrtReportJob report = new xrtReportJob();
        report.DataSource = ds;
        report.Parameters["sDate"].Value = DateTime.Now.Date;
        report.Parameters["PlaceVerify"].Value = "หอวัดน้ำ คน.ลก.";
        report.Parameters["PlaceLoad"].Value = "คน.ลก.";
        report.Parameters["Auditor"].Value = "";

        string fileName = "FM-มว.-009_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    #endregion

    #region มว.FM-001

    protected void btnReport1_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtIndexgvw.Text))
        {
            int index = int.Parse(txtIndexgvw.Text);

            dynamic data = gvw.GetRowValues(index, "REQUEST_ID");

            string REQ_ID = data;


            ListReport(REQ_ID);
        }
    }

    private void ListReport(string sReqID)
    {
        #region data table

        DataTable dt_REQ = CommonFunction.Get_Data(conn, "SELECT * FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'");

        string VEH_ID = "";
        string TU_ID = "";
        string SVENDORID = "";
        string REQTYPE_ID = "";
        string CAUSE_ID = "";
        if (dt_REQ.Rows.Count > 0)
        {
            SVENDORID = dt_REQ.Rows[0]["VENDOR_ID"] + "";
            VEH_ID = dt_REQ.Rows[0]["VEH_ID"] + "";
            TU_ID = dt_REQ.Rows[0]["TU_ID"] + "";
            REQTYPE_ID = dt_REQ.Rows[0]["REQTYPE_ID"] + "";
            CAUSE_ID = dt_REQ.Rows[0]["CAUSE_ID"] + "";
        }


        string SQL_H = @"SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,C.SCONTRACTID CONTRACKID,C.SVENDORID,C.SCONTRACTNO,TCT.SCONTRACTTYPENAME,CAT.CARCATE_NAME  FROM TTRUCK T 
                                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID  
                                    LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRUCKID AND T.STRAILERID=CT.STRAILERID
                                    LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
                                    LEFT JOIN TCONTRACTTYPE TCT ON TCT.SCONTRACTTYPEID=C.SCONTRACTTYPEID 
                                    LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID=T.CARCATE_ID 
                                    WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(VEH_ID) + "'";


        string SQL_T = @"SELECT  T.nwheels + coalesce((SELECT tt.nwheels + 0 FROM ttruck tt WHERE T.sheadid = tt.struckid AND ROWNUM <= 1),0) NWHEELS1
                                    ,T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,C.SCONTRACTID CONTRACKID,C.SVENDORID,C.SCONTRACTNO,TCT.SCONTRACTTYPENAME,CAT.CARCATE_NAME  FROM TTRUCK T 
                                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID  
                                    LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRUCKID AND T.STRAILERID=CT.STRAILERID
                                    LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
                                    LEFT JOIN TCONTRACTTYPE TCT ON TCT.SCONTRACTTYPEID=C.SCONTRACTTYPEID 
                                    LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID=T.CARCATE_ID 
                                    WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(TU_ID) + "'";

        // ป่าน 06/11/58
        //        string SQL_T = @"SELECT T.* ,TY.SCARTYPENAME ,TY.SCARCATEGORY ,C.SCONTRACTID CONTRACKID,C.SVENDORID,C.SCONTRACTNO,TCT.SCONTRACTTYPENAME,CAT.CARCATE_NAME  FROM TTRUCK T 
        //                                    LEFT JOIN TTRUCKTYPE TY ON T.SCARTYPEID=TY.SCARTYPEID  
        //                                    LEFT JOIN TCONTRACT_TRUCK CT ON T.STRUCKID=CT.STRUCKID AND T.STRAILERID=CT.STRAILERID
        //                                    LEFT JOIN TCONTRACT C ON CT.SCONTRACTID=C.SCONTRACTID 
        //                                    LEFT JOIN TCONTRACTTYPE TCT ON TCT.SCONTRACTTYPEID=C.SCONTRACTTYPEID 
        //                                    LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID=T.CARCATE_ID 
        //                                    WHERE T.STRUCKID ='" + CommonFunction.ReplaceInjection(TU_ID) + "'";



        string SQL_VENDOR = @"SELECT V.SVENDORID,V.SABBREVIATION,V.STEL,TVS.SNO FROM TVENDOR V
                              LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID=V.SVENDORID
                              WHERE TVS.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";

        string SQL_REQ_TYPE = @"SELECT * FROM TBL_REQTYPE
                              WHERE REQTYPE_ID = '" + CommonFunction.ReplaceInjection(REQTYPE_ID) + "'";

        string SQL_REQ_CAUSE = @"SELECT * FROM TBL_CAUSE
                              WHERE CAUSE_ID = '" + CommonFunction.ReplaceInjection(CAUSE_ID) + "' AND REQTYPE_ID = '" + CommonFunction.ReplaceInjection(REQTYPE_ID) + "'";


        string SQL_REQ_SLOT = @"SELECT SLOT_NO,LEVEL_NO,CAPACITY FROM TBL_REQSLOT
                              WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

        DataTable dt_H = CommonFunction.Get_Data(conn, SQL_H);
        DataTable dt_T = CommonFunction.Get_Data(conn, SQL_T);
        DataTable dt_VEN = CommonFunction.Get_Data(conn, SQL_VENDOR);
        DataTable dt_REQ_TYPE = CommonFunction.Get_Data(conn, SQL_REQ_TYPE);
        DataTable dt_REQ_CAUSE = CommonFunction.Get_Data(conn, SQL_REQ_CAUSE);
        DataTable dt_CAPACITY = CommonFunction.Get_Data(conn, SQL_REQ_SLOT);
        #endregion

        rpt_WorkSheet report = new rpt_WorkSheet();

        //ถ้ามีข้อมูลหางแสดงว่าเป็น เทรลเลอร์
        if (dt_T.Rows.Count > 0)
        {
            #region ข้อมูลรถ
            ((XRLabel)report.FindControl("xrLabel4", true)).Text = dt_T.Rows[0]["SCAR_NUM"] + "";
            ((XRLabel)report.FindControl("xrLabel6", true)).Text = dt_T.Rows[0]["SHEADREGISTERNO"] + "";
            ((XRLabel)report.FindControl("xrLabel8", true)).Text = dt_T.Rows[0]["SENGINE"] + "";
            ((XRLabel)report.FindControl("xrLabel10", true)).Text = dt_T.Rows[0]["SCHASIS"] + "";
            ((XRLabel)report.FindControl("xrLabel12", true)).Text = dt_T.Rows[0]["SCARTYPENAME"] + "";
            ((XRLabel)report.FindControl("xrLabel14", true)).Text = dt_T.Rows[0]["NWHEELS1"] + "";
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = dt_T.Rows[0]["SBRAND"] + "";
            ((XRLabel)report.FindControl("xrLabel18", true)).Text = dt_H.Rows[0]["SCHASIS"] + "";
            ((XRLabel)report.FindControl("xrLabel21", true)).Text = dt_H.Rows[0]["SHEADREGISTERNO"] + "";
            ((XRLabel)report.FindControl("xrLabel23", true)).Text = !string.IsNullOrEmpty(dt_H.Rows[0]["DSIGNIN"] + "") ? Convert.ToDateTime(dt_H.Rows[0]["DSIGNIN"] + "", new CultureInfo("th-TH")).ToString("dd/MM/yyyy") : "";
            ((XRLabel)report.FindControl("xrLabel25", true)).Text = dt_T.Rows[0]["STANK_MATERAIL"] + "";
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = dt_T.Rows[0]["STANK_MAKER"] + "";
            ((XRLabel)report.FindControl("xrLabel30", true)).Text = dt_T.Rows[0]["SMODEL"] + "";
            ((XRLabel)report.FindControl("xrLabel31", true)).Text = !string.IsNullOrEmpty(dt_T.Rows[0]["DSIGNIN"] + "") ? Convert.ToDateTime(dt_T.Rows[0]["DSIGNIN"] + "", new CultureInfo("th-TH")).ToString("dd/MM/yyyy") : "";
            ((XRLabel)report.FindControl("xrLabel33", true)).Text = dt_T.Rows[0]["SVIBRATION"] + "";
            ((XRLabel)report.FindControl("xrLabel35", true)).Text = "";
            ((XRLabel)report.FindControl("xrLabel38", true)).Text = dt_T.Rows[0]["NTANK_HIGH_HEAD"] + "";
            ((XRLabel)report.FindControl("xrLabel41", true)).Text = dt_T.Rows[0]["NTANK_HIGH_TAIL"] + "";
            ((XRLabel)report.FindControl("xrLabel44", true)).Text = dt_T.Rows[0]["FUELTYPE"] + "";
            ((XRLabel)report.FindControl("xrLabel46", true)).Text = dt_H.Rows[0]["SCONTRACTTYPENAME"] + "";
            ((XRLabel)report.FindControl("xrLabel48", true)).Text = dt_H.Rows[0]["SLOADING_METHOD"] + "";
            ((XRLabel)report.FindControl("xrLabel80", true)).Text = !string.IsNullOrEmpty(dt_T.Rows[0]["NWEIGHT"] + "") ? string.Format("{0:n0}", int.Parse(dt_T.Rows[0]["NWEIGHT"] + "")) : "";
            ((XRLabel)report.FindControl("xrLabel82", true)).Text = !string.IsNullOrEmpty(dt_H.Rows[0]["NWEIGHT"] + "") ? string.Format("{0:n0}", int.Parse(dt_H.Rows[0]["NWEIGHT"] + "")) : "";
            ((XRLabel)report.FindControl("xrLabel84", true)).Text = dt_H.Rows[0]["NLOAD_WEIGHT"] + "";
            #endregion

            #region ข้อมูลบริษัท
            if (dt_VEN.Rows.Count > 0)
            {
                ((XRLabel)report.FindControl("xrLabel51", true)).Text = dt_VEN.Rows[0]["SVENDORID"] + "";
                ((XRLabel)report.FindControl("xrLabel53", true)).Text = dt_T.Rows[0]["CARCATE_NAME"] + "";
                ((XRLabel)report.FindControl("xrLabel55", true)).Text = dt_T.Rows[0]["SOWNER_NAME"] + "";
                ((XRLabel)report.FindControl("xrLabel57", true)).Text = dt_VEN.Rows[0]["SABBREVIATION"] + "";
                ((XRLabel)report.FindControl("xrLabel59", true)).Text = dt_VEN.Rows[0]["SNO"] + "";
                ((XRLabel)report.FindControl("xrLabel61", true)).Text = dt_VEN.Rows[0]["STEL"] + "";
            }
            #endregion

            #region สาเหตุ
            string date = !string.IsNullOrEmpty(dt_REQ.Rows[0]["SERVICE_DATE"] + "") ? dt_REQ.Rows[0]["SERVICE_DATE"] + "" : dt_REQ.Rows[0]["APPOINTMENT_DATE"] + "";
            string SERVICEDATE = !string.IsNullOrEmpty(date) ? " (วันนัดหมาย " + Convert.ToDateTime(date, new CultureInfo("th-TH")).ToString("dd/MM/yyyy") + ")" : "";

            string REQ_TYPE = "";
            if (dt_REQ_TYPE.Rows.Count > 0)
            {
                REQ_TYPE = dt_REQ_TYPE.Rows[0]["REQTYPE_NAME"] + "";
            }

            string CAUSE = "";
            if (dt_REQ_CAUSE.Rows.Count > 0)
            {
                CAUSE = " เนื่องจาก" + dt_REQ_CAUSE.Rows[0]["CAUSE_NAME"] + "";
            }

            ((XRLabel)report.FindControl("xrLabel63", true)).Text = REQ_TYPE + CAUSE + SERVICEDATE;
            #endregion

            #region ความจุ
            string SQL_MAX_CAP = @"SELECT SUM(CAPACITY) as CAPACITY FROM
                                (
                                SELECT SLOT_NO,MAX(CAPACITY) as CAPACITY FROM TBL_REQSLOT
                                WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
                                GROUP BY SLOT_NO
                                )";
            DataTable dt_MAX_CAPACITY = CommonFunction.Get_Data(conn, SQL_MAX_CAP);
            if (dt_MAX_CAPACITY.Rows.Count > 0)
            {
                ((XRTableCell)report.FindControl("xrTableCell8", true)).Text = !string.IsNullOrEmpty(dt_MAX_CAPACITY.Rows[0]["CAPACITY"] + "") ? string.Format("{0:n0}", int.Parse(dt_MAX_CAPACITY.Rows[0]["CAPACITY"] + "")) : "";
            }

            ((XRLabel)report.FindControl("xrLabel74", true)).Text = DateTime.Now.ToString("dd");
            ((XRLabel)report.FindControl("xrLabel76", true)).Text = DateTime.Now.ToString("MMM");
            ((XRLabel)report.FindControl("xrLabel78", true)).Text = DateTime.Now.ToString("yyyy");

            ((XRLabel)report.FindControl("xrLabel85", true)).Text = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ((XRLabel)report.FindControl("xrLabel86", true)).Text = "หน้าที่1";

            #endregion
        }
        //เป็น 10ล้อ
        else
        {
            #region ข้อมูลรถ
            ((XRLabel)report.FindControl("xrLabel4", true)).Text = dt_H.Rows[0]["SCAR_NUM"] + "";
            ((XRLabel)report.FindControl("xrLabel6", true)).Text = dt_H.Rows[0]["SHEADREGISTERNO"] + "";
            ((XRLabel)report.FindControl("xrLabel8", true)).Text = dt_H.Rows[0]["SENGINE"] + "";
            ((XRLabel)report.FindControl("xrLabel10", true)).Text = dt_H.Rows[0]["SCHASIS"] + "";
            ((XRLabel)report.FindControl("xrLabel12", true)).Text = dt_H.Rows[0]["SCARTYPENAME"] + "";
            ((XRLabel)report.FindControl("xrLabel14", true)).Text = dt_H.Rows[0]["NWHEELS"] + "";
            ((XRLabel)report.FindControl("xrLabel19", true)).Text = dt_H.Rows[0]["SBRAND"] + "";
            //((XRLabel)report.FindControl("xrLabel18", true)).Text = dt_H.Rows[0]["SCHASIS"] + "";
            //((XRLabel)report.FindControl("xrLabel21", true)).Text = dt_H.Rows[0]["SHEADREGISTERNO"] + "";
            ((XRLabel)report.FindControl("xrLabel23", true)).Text = !string.IsNullOrEmpty(dt_H.Rows[0]["DSIGNIN"] + "") ? Convert.ToDateTime(dt_H.Rows[0]["DSIGNIN"] + "", new CultureInfo("th-TH")).ToString("dd/MM/yyyy") : "";
            ((XRLabel)report.FindControl("xrLabel25", true)).Text = dt_H.Rows[0]["STANK_MATERAIL"] + "";
            ((XRLabel)report.FindControl("xrLabel27", true)).Text = dt_H.Rows[0]["STANK_MAKER"] + "";
            ((XRLabel)report.FindControl("xrLabel30", true)).Text = dt_H.Rows[0]["SMODEL"] + "";
            //((XRLabel)report.FindControl("xrLabel31", true)).Text = !string.IsNullOrEmpty(dt_T.Rows[0]["DSIGNIN"] + "") ? Convert.ToDateTime(dt_T.Rows[0]["DSIGNIN"] + "", new CultureInfo("th-TH")).ToString("dd/MM/yyyy") : "";
            ((XRLabel)report.FindControl("xrLabel33", true)).Text = dt_H.Rows[0]["SVIBRATION"] + "";
            ((XRLabel)report.FindControl("xrLabel35", true)).Text = "";
            ((XRLabel)report.FindControl("xrLabel38", true)).Text = dt_H.Rows[0]["NTANK_HIGH_HEAD"] + "";
            ((XRLabel)report.FindControl("xrLabel41", true)).Text = dt_H.Rows[0]["NTANK_HIGH_TAIL"] + "";
            ((XRLabel)report.FindControl("xrLabel44", true)).Text = dt_H.Rows[0]["FUELTYPE"] + "";
            ((XRLabel)report.FindControl("xrLabel46", true)).Text = dt_H.Rows[0]["SCONTRACTTYPENAME"] + "";
            ((XRLabel)report.FindControl("xrLabel48", true)).Text = dt_H.Rows[0]["SLOADING_METHOD"] + "";
            //((XRLabel)report.FindControl("xrLabel80", true)).Text = !string.IsNullOrEmpty(dt_T.Rows[0]["NWEIGHT"] + "") ? string.Format("{0:n0}", int.Parse(dt_T.Rows[0]["NWEIGHT"] + "")) : "";
            ((XRLabel)report.FindControl("xrLabel82", true)).Text = !string.IsNullOrEmpty(dt_H.Rows[0]["NWEIGHT"] + "") ? string.Format("{0:n0}", int.Parse(dt_H.Rows[0]["NWEIGHT"] + "")) : "";
            ((XRLabel)report.FindControl("xrLabel84", true)).Text = dt_H.Rows[0]["NLOAD_WEIGHT"] + "";
            #endregion

            #region ข้อมูลบริษัท
            if (dt_VEN.Rows.Count > 0)
            {
                ((XRLabel)report.FindControl("xrLabel51", true)).Text = dt_VEN.Rows[0]["SVENDORID"] + "";
                ((XRLabel)report.FindControl("xrLabel53", true)).Text = dt_H.Rows[0]["CARCATE_NAME"] + "";
                ((XRLabel)report.FindControl("xrLabel55", true)).Text = dt_H.Rows[0]["SOWNER_NAME"] + "";
                ((XRLabel)report.FindControl("xrLabel57", true)).Text = dt_VEN.Rows[0]["SABBREVIATION"] + "";
                ((XRLabel)report.FindControl("xrLabel59", true)).Text = dt_VEN.Rows[0]["SNO"] + "";
                ((XRLabel)report.FindControl("xrLabel61", true)).Text = dt_VEN.Rows[0]["STEL"] + "";
            }
            #endregion

            #region สาเหตุ
            string date = !string.IsNullOrEmpty(dt_REQ.Rows[0]["SERVICE_DATE"] + "") ? dt_REQ.Rows[0]["SERVICE_DATE"] + "" : dt_REQ.Rows[0]["APPOINTMENT_DATE"] + "";
            string SERVICEDATE = !string.IsNullOrEmpty(date) ? " (วันนัดหมาย " + Convert.ToDateTime(date, new CultureInfo("th-TH")).ToString("dd/MM/yyyy") + ")" : "";

            string REQ_TYPE = "";
            if (dt_REQ_TYPE.Rows.Count > 0)
            {
                REQ_TYPE = dt_REQ_TYPE.Rows[0]["REQTYPE_NAME"] + "";
            }

            string CAUSE = "";
            if (dt_REQ_CAUSE.Rows.Count > 0)
            {
                CAUSE = " เนื่องจาก" + dt_REQ_CAUSE.Rows[0]["CAUSE_NAME"] + "";
            }

            ((XRLabel)report.FindControl("xrLabel63", true)).Text = REQ_TYPE + CAUSE + SERVICEDATE;
            #endregion

            #region ความจุ
            string SQL_MAX_CAP = @"SELECT SUM(CAPACITY) as CAPACITY FROM
                                (
                                SELECT SLOT_NO,MAX(CAPACITY) as CAPACITY FROM TBL_REQSLOT
                                WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + @"'
                                GROUP BY SLOT_NO
                                )";
            DataTable dt_MAX_CAPACITY = CommonFunction.Get_Data(conn, SQL_MAX_CAP);
            if (dt_MAX_CAPACITY.Rows.Count > 0)
            {
                ((XRTableCell)report.FindControl("xrTableCell8", true)).Text = !string.IsNullOrEmpty(dt_MAX_CAPACITY.Rows[0]["CAPACITY"] + "") ? string.Format("{0:n0}", int.Parse(dt_MAX_CAPACITY.Rows[0]["CAPACITY"] + "")) : "";
            }

            ((XRLabel)report.FindControl("xrLabel74", true)).Text = DateTime.Now.ToString("dd");
            ((XRLabel)report.FindControl("xrLabel76", true)).Text = DateTime.Now.ToString("MMM");
            ((XRLabel)report.FindControl("xrLabel78", true)).Text = DateTime.Now.ToString("yyyy");

            ((XRLabel)report.FindControl("xrLabel85", true)).Text = "วันที่พิมพ์รายงาน : " + DateTime.Now.ToString("dd/MM/yyyy");
            ((XRLabel)report.FindControl("xrLabel86", true)).Text = "หน้าที่1";

            #endregion
        }


        report.Name = "Work_Sheet_FM";
        report.DataSource = dt_CAPACITY;
        string fileName = "ใบงานมว._" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private string GetComment(string sReqID)
    {
        string sResult = "";
        //คอมเม้น/บันทึก
        string sql5 = @"SELECT * FROM TBL_CHECKING_COMMENT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' ORDER BY NVERSION DESC";
        DataTable dtComment = new DataTable();
        dtComment = CommonFunction.Get_Data(conn, sql5);

        DataRow dr4 = dtComment.Rows.Count > 0 ? dtComment.Rows[0] : null;

        sResult = dr4 != null ? dr4["SCOMMENT"] + "" : "";

        return sResult;
    }
    #endregion

    #region มว.FM-005
    protected void btnReport2_Click(object sender, EventArgs e)
    {
        string REQ_ID = "1407/01-0001";
        ListReport005(REQ_ID);
    }

    private void ListReport005(string sReqID)
    {
        rpt_InspectionCar report = new rpt_InspectionCar();

        // 
        OracleDataAdapter adapter = new OracleDataAdapter();
        dsInspectionCar ds = new dsInspectionCar();

        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,
                        TRK.DWATEREXPIRE,TRK.SCARTYPEID,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
                        TVD.SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME
                        FROM TBL_REQUEST TRQ 
                        INNER JOIN TTRUCK TRK ON TRQ.STRUCKID = TRK.STRUCKID
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        INNER JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        INNER JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        INNER JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        WHERE TRQ.REQUEST_ID = '{0}'";

        DataTable dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sReqID)));

        //SQl ประเภทสัญญา
        DataRow dr1 = dtMainData.Rows.Count > 0 ? dtMainData.Rows[0] : null;
        if (dr1 != null)
        {
            string sTruckSearchID = "";
            if (dr1["SCARTYPEID"] + "" == "0") // 10 ล้อ
            {
                sTruckSearchID = dr1["VEH_ID"] + "";
            }
            else if (dr1["SCARTYPEID"] + "" == "3") // หัวลาก
            {
                sTruckSearchID = dr1["TU_ID"] + "";
            }

            #region Q1
            string sql1 = @"select * from ttruck where struckid = '" + CommonFunction.ReplaceInjection(sTruckSearchID) + "'";
            DataTable dt1 = new DataTable();
            dt1 = CommonFunction.Get_Data(conn, sql1);
            if (dt1.Rows.Count > 0)
            {
                //วันที่ รายงาน
                ((XRLabel)report.FindControl("lblDate", true)).Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

                // วันที่วัดน้ำเดิม
                report.Parameters["sOldDateCheckWater"].Value = dt1.Rows[0]["DLAST_SERV"] + "" != "" ? Convert.ToDateTime(dt1.Rows[0]["DLAST_SERV"] + "").ToString("dd/MM/yyyy", new CultureInfo("th-TH")) : "";

                //ยี่ห้อรถ (ใช้หัวหา)
                DataTable dtCheckBand = new DataTable();
                dtCheckBand = CommonFunction.Get_Data(conn, "select * from ttruck where struckid = '" + CommonFunction.ReplaceInjection(dr1["VEH_ID"] + "") + "'");
                if (dtCheckBand.Rows.Count > 0)
                {
                    report.Parameters["sBandCar"].Value = dtCheckBand.Rows[0]["SBRAND"] + "";
                }
                else
                {
                    report.Parameters["sBandCar"].Value = "";
                }

                //คลังที่เข้ารับน้ำมัน
                report.Parameters["sSendOilStart"].Value = "";
                report.Parameters["sSendOilEnd"].Value = "";

                if (dt1.Rows[0]["CARCATE_ID"] + "" == "01") //รถลูกค้า
                {
                    ((XRCheckBox)report.FindControl("ckbContractType3", true)).Checked = true;
                }
                else if (dt1.Rows[0]["CARCATE_ID"] + "" == "02")// รถในสัญญา หรือ รถรับจ้าง
                {
                    // เป็นรถในสัญญาประเภทไหน
                    string sql1_1 = @"SELECT  TRT.*
                                    FROM TCONTRACT_TRUCK TCT 
                                    INNER JOIN TCONTRACT TCR ON TCT.SCONTRACTID = TCR.SCONTRACTID
                                    INNER JOIN TCONTRACTTYPE TRT ON TRT.SCONTRACTTYPEID = TCR.SCONTRACTTYPEID
                                    WHERE  NVL(TCT.STRAILERID,TCT.STRUCKID) = '" + sTruckSearchID + "'";
                    DataTable dt1_1 = new DataTable();
                    dt1_1 = CommonFunction.Get_Data(conn, sql1_1);
                    if (dt1_1.Rows.Count > 0)
                    {
                        DataRow dr1_1 = dt1_1.Rows[0];
                        switch (dr1_1["SCONTRACTTYPEID"] + "")
                        {
                            case "2": ((XRCheckBox)report.FindControl("ckbContractType1", true)).Checked = true; break;
                        }
                    }
                }



            }
            #endregion

            #region Q2
            string sql2 = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,TRQ.VEH_ID,TRQ.TU_ID,
                            TVD.SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
                            TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
                            CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
                            TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
                            TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
                             FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
                             LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
                             LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
                             WHERE TRQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";

            OracleConnection con = new OracleConnection(conn);
            con.Open();
            adapter.SelectCommand = new OracleCommand(sql2, con);
            adapter.Fill(ds, "dt1");
            adapter.Dispose();
            #endregion

            //Q3
            // 1  TBL_SCRUTINEERING
            string sql3_1 = @"select * from TBL_SCRUTINEERING where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "'";
            string sql3_2 = @"select * from TBL_SCRUTINEERINGLIST where REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReqID) + "' and NVERSION = {0} and ISACTIVE_FLAG = 'Y' order by checklist_item";
            DataTable dt3_1 = new DataTable();
            DataTable dt3_2 = new DataTable();
            dt3_1 = CommonFunction.Get_Data(conn, sql3_1);
            if (dt1.Rows.Count == 1)
            {
                DataRow dr3_1 = null;
                dr3_1 = dt3_1.Rows[0];
                dt3_2 = CommonFunction.Get_Data(conn, string.Format(sql3_2, dr3_1["NVERSION"] + ""));

                #region Deflut Seting
                //สรุปผลการตรวจสอบ
                //rblG9.Value = dr1["CHECKING_TRY"] + "";

                //ตรวจอุปกรณ์จรวดหรือไม่ G2
                if (dr3_1["ROCKETS_FLAG"] + "" == "Y")
                {
                    ((XRCheckBox)report.FindControl("ckbG2_1", true)).Checked = true;
                }
                else if (dr3_1["ROCKETS_FLAG"] + "" == "N")
                {
                    ((XRCheckBox)report.FindControl("ckbG2_2", true)).Checked = true;
                }

                //ตรวจยางรถตัวเทเลอร์  G4
                ((XRCheckBox)report.FindControl("ckbG4_1", true)).Checked = true;
                if (dr3_1["TUTIRE_FLAG"] + "" == "1")
                {
                    ((XRCheckBox)report.FindControl("ckbG4_1", true)).Text = "ยาง 8 ล้อหลังเทเลอร์";
                }
                else if (dr3_1["TUTIRE_FLAG"] + "" == "2")
                {
                    ((XRCheckBox)report.FindControl("ckbG4_1", true)).Text = "ยาง 12 ล้อหลังเทเลอร์";
                }

                //ชนิดวาล์ว G7
                ((XRLabel)report.FindControl("lblG7_1", true)).Text = GetVALVEName(dr3_1["VALVE_TYPE"] + "");

                // ความคิดเห็นผู้ตรวจ
                report.Parameters["sOpinion"].Value = dr3_1["CHECKED_OPINION"] + "";

                #endregion

                #region //Set Data
                foreach (DataRow dr in dt3_2.Rows)
                {
                    if (dr["CHECK_GROUPID"] + "" == "00001")
                    {
                        #region G1
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "1": ((XRCheckBox)report.FindControl("ckbG1_1", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "2": ((XRCheckBox)report.FindControl("ckbG1_2", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "3": ((XRCheckBox)report.FindControl("ckbG1_3", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "4": ((XRCheckBox)report.FindControl("ckbG1_4", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "5": ((XRCheckBox)report.FindControl("ckbG1_5", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                            case "6": ((XRCheckBox)report.FindControl("ckbG1_6", true)).Checked = dr["ITEM1_VAL"] + "" == "Y" ? true : false; break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00002")
                    {
                        #region G2
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "7":
                                XRCheckBox ckbG2_7_1 = (XRCheckBox)report.FindControl("ckbG2_7_1", true);
                                ckbG2_7_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_7_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_7_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_7_1.Text = "ไม่มี";
                                }
                                break;
                            case "8":
                                XRCheckBox ckbG2_8_1 = (XRCheckBox)report.FindControl("ckbG2_8_1", true);
                                ckbG2_8_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_8_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_8_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_8_1.Text = "ไม่มี";
                                }
                                break;
                            case "9":
                                XRCheckBox ckbG2_9_1 = (XRCheckBox)report.FindControl("ckbG2_9_1", true);
                                ckbG2_9_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_9_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_9_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_9_1.Text = "ไม่มี";
                                }
                                break;
                            case "10":
                                ((XRLabel)report.FindControl("lblG2_10_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_10_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_10_3", true)).Text = dr["ITEM3_UNIT"] + "";
                                break;
                            case "11":
                                ((XRLabel)report.FindControl("lblG2_11_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_11_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG2_11_3", true)).Text = dr["ITEM3_UNIT"] + "";
                                break;
                            case "12":
                                XRCheckBox ckbG2_12_1 = (XRCheckBox)report.FindControl("ckbG2_12_1", true);
                                ckbG2_12_1.Checked = true;
                                if (dr["ITEM1_VAL"] + "" == "Y")
                                {
                                    ckbG2_12_1.Text = "ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "N")
                                {
                                    ckbG2_12_1.Text = "ไม่ผ่าน";
                                }
                                else if (dr["ITEM1_VAL"] + "" == "W")
                                {
                                    ckbG2_12_1.Text = "ไม่มี";
                                }
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00003")
                    {
                        #region G3
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "13":
                                ((XRLabel)report.FindControl("lblG3_13_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_13_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "14":
                                ((XRLabel)report.FindControl("lblG3_14_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_14_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG3_14_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "1414":
                                ((XRLabel)report.FindControl("lblG3_15_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_15_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "15":
                                ((XRLabel)report.FindControl("lblG3_16_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG3_16_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG3_16_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00004")
                    {
                        #region G4
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "16":
                                ((XRLabel)report.FindControl("lblG4_17_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_17_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "17":
                                ((XRLabel)report.FindControl("lblG4_18_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_18_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_18_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "18":
                                ((XRLabel)report.FindControl("lblG4_19_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_19_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_19_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "19":
                                ((XRLabel)report.FindControl("lblG4_20_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG4_20_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG4_20_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00005")
                    {
                        #region G5
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "20":
                                ((XRLabel)report.FindControl("lblG5_21_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_21_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "21":
                                ((XRLabel)report.FindControl("lblG5_22_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_22_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG5_22_3", true)).Checked = (dr["ITEM3_VAL"] + "" == "Y");
                                break;
                            case "22":
                                ((XRLabel)report.FindControl("lblG5_23_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_23_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "23":
                                ((XRLabel)report.FindControl("lblG5_24_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG5_24_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00006")
                    {
                        #region G6
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "52":
                                ((XRLabel)report.FindControl("lblG6_25_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG6_25_2", true)).Checked = (dr["ITEM2_VAL"] + "" == "Y");
                                ((XRCheckBox)report.FindControl("ckbG6_25_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "53":
                                ((XRLabel)report.FindControl("lblG6_26_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG6_26_2", true)).Checked = (dr["ITEM2_VAL"] + "" == "Y");
                                ((XRCheckBox)report.FindControl("ckbG6_26_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00007")
                    {
                        #region G7
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "24":
                                ((XRLabel)report.FindControl("lblG7_27_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_27_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_27_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_27_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "25":
                                ((XRLabel)report.FindControl("lblG7_28_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_28_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_28_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_28_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "26":
                                ((XRLabel)report.FindControl("lblG7_29_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG7_29_2", true)).Text = dr["ITEM2_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG7_29_3", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_29_3", true)).Text = dr["ITEM3_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM3_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "27":
                                ((XRCheckBox)report.FindControl("ckbG7_30_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_30_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "28":
                                ((XRCheckBox)report.FindControl("ckbG7_31_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_31_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "29":
                                ((XRCheckBox)report.FindControl("ckbG7_32_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_32_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "30":
                                ((XRCheckBox)report.FindControl("ckbG7_33_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_33_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "31":
                                ((XRCheckBox)report.FindControl("ckbG7_34_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_34_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "32":
                                ((XRCheckBox)report.FindControl("ckbG7_35_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG7_35_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00010")
                    {
                        #region G10
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "33":
                                ((XRCheckBox)report.FindControl("ckbG10_36_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_36_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "34":
                                ((XRCheckBox)report.FindControl("ckbG10_37_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_37_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "35":
                                ((XRCheckBox)report.FindControl("ckbG10_38_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_38_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "36":
                                ((XRCheckBox)report.FindControl("ckbG10_39_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_39_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "37":
                                ((XRCheckBox)report.FindControl("ckbG10_40_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_40_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "38":
                                ((XRCheckBox)report.FindControl("ckbG10_41_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_41_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "39":
                                ((XRCheckBox)report.FindControl("ckbG10_42_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_42_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "40":
                                ((XRCheckBox)report.FindControl("ckbG10_43_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_43_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "41":
                                ((XRCheckBox)report.FindControl("ckbG10_44_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_44_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "42":
                                ((XRLabel)report.FindControl("lblG10_45_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRCheckBox)report.FindControl("ckbG10_45_2", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_45_2", true)).Text = dr["ITEM2_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM2_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "43":
                                ((XRCheckBox)report.FindControl("ckbG10_46_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_46_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "44":
                                ((XRCheckBox)report.FindControl("ckbG10_47_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_47_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "45":
                                ((XRCheckBox)report.FindControl("ckbG10_48_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_48_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "46":
                                ((XRCheckBox)report.FindControl("ckbG10_49_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_49_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : dr["ITEM1_VAL"] + "" == "N" ? "ไม่ผ่าน" : "ไม่มี";
                                break;
                            case "47":
                                ((XRCheckBox)report.FindControl("ckbG10_50_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_50_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                            case "48": ((XRLabel)report.FindControl("lblG10_51_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "50":
                                ((XRLabel)report.FindControl("lblG10_52_1", true)).Text = dr["ITEM1_VAL"] + "" == "1" ? "น๊อตยึดใน" : dr["ITEM1_VAL"] + "" == "2" ? "น๊อตรอบ" : dr["ITEM1_VAL"] + "" == "3" ? "แค้มรัดรอบฝา" : "";
                                break;
                            case "51":
                                ((XRCheckBox)report.FindControl("ckbG10_53_1", true)).Checked = true;
                                ((XRCheckBox)report.FindControl("ckbG10_53_1", true)).Text = dr["ITEM1_VAL"] + "" == "Y" ? "ผ่าน" : "ไม่ผ่าน";
                                break;
                        }
                        #endregion
                    }
                    else if (dr["CHECK_GROUPID"] + "" == "00011")
                    {
                        #region G11
                        switch (dr["CHECKLIST_ID"] + "")
                        {
                            case "55": ((XRLabel)report.FindControl("lblG11_54_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "56": ((XRLabel)report.FindControl("lblG11_55_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "57": ((XRLabel)report.FindControl("lblG11_56_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "58": ((XRLabel)report.FindControl("lblG11_57_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "59": ((XRLabel)report.FindControl("lblG11_58_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "60": ((XRLabel)report.FindControl("lblG11_59_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "61": ((XRLabel)report.FindControl("lblG11_60_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "62": ((XRLabel)report.FindControl("lblG11_61_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "63": ((XRLabel)report.FindControl("lblG11_62_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "64": ((XRLabel)report.FindControl("lblG11_63_1", true)).Text = dr["ITEM1_VAL"] + ""; break;
                            case "65":
                                ((XRLabel)report.FindControl("lblG11_64_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG11_64_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                            case "66":
                                ((XRLabel)report.FindControl("lblG11_65_1", true)).Text = dr["ITEM1_VAL"] + "";
                                ((XRLabel)report.FindControl("lblG11_65_2", true)).Text = dr["ITEM2_VAL"] + "";
                                break;
                        }
                        #endregion
                    }
                }
                #endregion

            }

        }

        #region function report

        report.Name = "รายงานการตรวจสภาพรถบรรทุกน้ำมัน";
        report.DataSource = ds;
        report.DataAdapter = adapter;
        report.DataMember = "Table";

        string fileName = "รายงานการตรวจสภาพรถบรรทุกน้ำมัน_FM-005_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    private string GetVALVEName(string sID)
    {
        string sql = @" SELECT VALVE_TYPEID,VALVE_TYPENAME FROM TBL_VALVETYPE WHERE CACTIVE = 'Y' AND VALVE_TYPEID = '" + CommonFunction.ReplaceInjection(sID) + "'";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            return dt.Rows[0]["VALVE_TYPENAME"] + "";
        }
        else
        {
            return "";
        }
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Lisdata(Session["SVENDORID"] + "", "Y");
    }
}