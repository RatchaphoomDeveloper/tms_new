﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Position_Lst : PageBase
{
    DataTable dt = new DataTable();
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AddDataTable();
        }
    }
    #endregion
    
    #region gvPosition_RowDataBound
    protected void gvPosition_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            TextBox txt = (TextBox)e.Row.FindControl("txtPersonTypeDesc");
            if (txt != null)
            {
                txt.Text = dr["PERSON_TYPE_DESC"] + string.Empty;
            }
            TextBox txtAgeMin = (TextBox)e.Row.FindControl("txtAgeMin");
            if (txtAgeMin != null)
            {
                txtAgeMin.Text = dr["AGEMIN"] + string.Empty;
            }
            TextBox txtAgeMax = (TextBox)e.Row.FindControl("txtAgeMax");
            if (txtAgeMax != null)
            {
                txtAgeMax.Text = dr["AGEMAX"] + string.Empty;
            }
            RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblCactive");
            if (rbl != null)
            {
                rbl.SelectedValue = dr["CACTIVE"] + string.Empty;
            }
            HiddenField hid = (HiddenField)e.Row.FindControl("hidID");
            if (hid != null)
            {
                hid.Value = dr["PERSON_TYPE"] + string.Empty;
            }
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        dt = PositionBLL.Instance.PositionSelect(txtPosition.Text, rblCactive.SelectedValue);
        DataRow dr = dt.NewRow();
        dr["PERSON_TYPE"] = 0;
        dr["PERSON_TYPE_DESC"] = "";
        dr["AGEMIN"] = DBNull.Value;
        dr["AGEMAX"] = DBNull.Value;
        dr["CACTIVE"] = "1";
        dt.Rows.InsertAt(dr, 0);

        gvPosition.DataSource = dt;
        gvPosition.DataBind();
    }
    #endregion

    #region AddDataTable
    private void AddDataTable()
    {
        
        dt.Columns.Add("PERSON_TYPE", typeof(int));
        dt.Columns.Add("PERSON_TYPE_DESC", typeof(string));
        dt.Columns.Add("AGEMIN", typeof(int));
        dt.Columns.Add("AGEMAX", typeof(int));
        dt.Columns.Add("CACTIVE", typeof(string));
        dt.Rows.Add(0, "", null, null, "1");

        gvPosition.DataSource = dt;
        gvPosition.DataBind();
    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int index = int.Parse(btn.CommandArgument);
        GridViewRow gvr = gvPosition.Rows[index];
        int? PERSON_TYPE = 0, AgeMin = 0, AgeMax = 0;
        string PERSON_TYPE_DESC = string.Empty, CACTIVE = "1";
        TextBox txt = (TextBox)gvr.FindControl("txtPersonTypeDesc");
        if (txt != null)
        {
            PERSON_TYPE_DESC = txt.Text.Trim();
        }
        TextBox txtAgeMin = (TextBox)gvr.FindControl("txtAgeMin");
        if (txtAgeMin != null && !string.IsNullOrEmpty(txtAgeMin.Text))
        {
            AgeMin = int.Parse(txtAgeMin.Text);
        }
        TextBox txtAgeMax = (TextBox)gvr.FindControl("txtAgeMax");
        if (txtAgeMax != null && !string.IsNullOrEmpty(txtAgeMin.Text))
        {
            AgeMax = int.Parse(txtAgeMax.Text);
        }
        RadioButtonList rbl = (RadioButtonList)gvr.FindControl("rblCactive");
        if (rbl != null)
        {
            CACTIVE = rbl.SelectedValue;
        }
        HiddenField hid = (HiddenField)gvr.FindControl("hidID");
        if (hid != null)
        {
            PERSON_TYPE = int.Parse(hid.Value);
        }
        bool isRes = false;
        try
        {
            if (PositionBLL.Instance.PositionCheckValidate(PERSON_TYPE.Value, PERSON_TYPE_DESC))
            {
                alertFail("ตำแหน่ง " + PERSON_TYPE_DESC + " มีอยู่แล้วในระบบ");

            }
            else
            {
                if (PERSON_TYPE == 0)
                {
                    isRes = PositionBLL.Instance.PositionInsert(PERSON_TYPE_DESC, CACTIVE,AgeMin,AgeMax);
                }
                else
                {
                    isRes = PositionBLL.Instance.PositionUpdate(PERSON_TYPE.Value, PERSON_TYPE_DESC, CACTIVE, AgeMin, AgeMax);
                }
                if (isRes)
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                    btnSearch_Click(null, null);
                }
                else
                {
                    alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>");
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>" + ex.Message));
        }

    }
    #endregion
    
}