﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Configuration;

public partial class contract_add_p4 : System.Web.UI.Page
{
    const string UploadContractDirectory = "UploadFile/Contract/";
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string REQ_ID = "";
    private static string TCONTRACT_GUARANTEES = "";
    private static string TCONTRACT_DOC = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        #region Event
        //gvwGuarantee.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwGuarantee_CustomColumnDisplayText);
        gvwGuarantee.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwGuarantee_AfterPerformCallback);

        //gvwContractDoc.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwContractDoc_CustomColumnDisplayText);
        gvwContractDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContractDoc_AfterPerformCallback);

        //gvwContractFile.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwContractFile_CustomColumnDisplayText);
        gvwContractFile.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContractFile_AfterPerformCallback);

        //gvwTerminal.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwTerminal_CustomColumnDisplayText);
        gvwTerminal.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTerminal_AfterPerformCallback);

        //gvwGProduct.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwGProduct_CustomColumnDisplayText);
        gvwGProduct.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwGProduct_AfterPerformCallback);

        //gvwTruck.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwTruck_CustomColumnDisplayText);
        gvwTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        #endregion

        //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='admin_home.aspx';", true); return;
        if (!IsPostBack)
        {
            //QUERY GENID
            TCONTRACT_GUARANTEES = SystemFunction.QUERY_TCONTRACT_GUARANTEES();
            TCONTRACT_DOC = SystemFunction.QUERY_TCONTRACT_DOC();
            ClearSession();

            string str = Request.QueryString["str"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                Session["SMODE"] = "" + QueryString[0];
                Session["SCONTRACTID"] = "" + QueryString[1];
                Session["SVENDORID"] = "" + QueryString[2];

                //AddToTemp("" + QueryString[1]);
                string SCONTRACTID = Session["SCONTRACTID"] + "";
                if ("" + QueryString[0] == "EDIT" || "" + QueryString[0] == "VIEW")
                {
                    Session["SMODE"] = "EDIT";

                    string strReq = Request.QueryString["strID"];
                    string[] strQueryReq;
                    REQ_ID = "";
                    string Condition = "";
                    if (!string.IsNullOrEmpty(strReq))
                    {
                        strQueryReq = STCrypt.DecryptURL(strReq);
                        REQ_ID = strQueryReq[0];
                        //เช็คว่ามี REQ_ID ไหม ถ้ามีให้ใช้ REQ หา แต่ถ้าไม่มีให้ใช้ STATUS 0 หา
                        if (!string.IsNullOrEmpty(REQ_ID))
                        {
                            Condition = " AND REQ.REQ_ID = '" + REQ_ID + "'";
                        }
                        else
                        {
                            Condition = " AND REQ.STATUS = '0'";
                        }
                    }
                    else
                    {
                        Condition = " AND REQ.STATUS = '0'";
                    }

                    //เช็คว่ามีข้อมูลใน Temp หรือไม่ ถ้ามีให้เปลี่ยน SELECTCOMMAND ของ datasource ใหม่
                    string CheckTEMP = @"SELECT CON.SCONTRACTID,REQ.CACTIVE,REQ.STATUS,REQ.REQ_ID FROM TCONTRACT_TEMP CON
INNER JOIN TREQ_DATACHANGE REQ
ON TO_CHAR(CON.SCONTRACTID) = REQ.REF_ID AND CON.REQ_ID = REQ.REQ_ID
WHERE REQ.CACTIVE = 'Y' AND CON.SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + "' " + Condition + "";
                    DataTable dt_TEMP = CommonFunction.Get_Data(conn, CheckTEMP);
                    REQ_ID = "";
                    if (dt_TEMP.Rows.Count > 0)
                    {
                        if (dt_TEMP.Rows[0]["STATUS"] + "" != "0" && dt_TEMP.Rows[0]["STATUS"] + "" != "3")
                        {
                            btnSubmit.ClientVisible = false;
                        }

                        REQ_ID = CommonFunction.ReplaceInjection(dt_TEMP.Rows[0]["REQ_ID"] + "");

                        sdsContract.SelectParameters.Clear();
                        sdsContract.SelectCommand = "SELECT * FROM TCONTRACT_TEMP WHERE SCONTRACTID = NVL(" + SCONTRACTID + " , SCONTRACTID) AND REQ_ID = '" + REQ_ID + "'";
                        sdsContract.DataBind();

                        sdsDocument.SelectParameters.Clear();
                        sdsDocument.SelectCommand = "SELECT SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION ,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_DOC_TEMP WHERE SCONTRACTID LIKE  NVL(" + SCONTRACTID + ", SCONTRACTID) AND REQ_ID = '" + REQ_ID + "'";
                        sdsDocument.DataBind();

                        sdsGUARANTEES.SelectParameters.Clear();
                        sdsGUARANTEES.SelectCommand = "SELECT NCGID, NCONTRACTID, SBOOKNO, SGUARANTEETYPE, NAMOUNT, DCREATE, SCREATE, DUPDATE, SUPDATE,SREMARK,GUARANTEESTYPE_ID ,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_GUARANTEES_TEMP WHERE NCONTRACTID = NVL(" + SCONTRACTID + ", NCONTRACTID)  AND REQ_ID = '" + REQ_ID + "'";
                        sdsGUARANTEES.DataBind();

                        sdsPlant.SelectParameters.Clear();
                        sdsPlant.SelectCommand = @"SELECT ROW_NUMBER()OVER(ORDER BY CONT_PLNT.STERMINALID) AS KEYID 
                                                                ,CONT_PLNT.SCONTRACTID NCONTRACTID,CONT_PLNT.STERMINALID
                                                                ,CONT_PLNT.STERMINALID||' :  '||PLNT.SABBREVIATION STERMINALNAME,CONT_PLNT.STERMINALID CONT_PLNT ,NVL(CONT_PLNT.CMAIN_PLANT,'0') CMAIN_PLANT
                                                                ,CONT_PLNT.CACTIVE ,'0' CNEW ,'0' CCHANGE,'0' CDEL 
                                                                FROM TCONTRACT_PLANT_TEMP CONT_PLNT 
                                                                LEFT JOIN TTERMINAL PLNT ON CONT_PLNT.STERMINALID =PLNT.STERMINALID 
                                                                WHERE SCONTRACTID LIKE  NVL(" + SCONTRACTID + ", SCONTRACTID)  AND REQ_ID = '" + REQ_ID + "'";
                        sdsPlant.DataBind();

                        sdsGPRODUCT.SelectParameters.Clear();
                        sdsGPRODUCT.SelectCommand = "SELECT ROWNUM-1 GID,SCONTRACTID AS NCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,CACTIVE,'0' CNEW ,'0' CCHANGE,'0' CDEL FROM TCONTRACT_GPRODUCT_TEMP WHERE SCONTRACTID=NVL(" + SCONTRACTID + ",SCONTRACTID)  AND REQ_ID = '" + REQ_ID + "'";
                        sdsGPRODUCT.DataBind();

                        sdsContractTruck.SelectParameters.Clear();
                        sdsContractTruck.SelectCommand = @"SELECT ROW_NUMBER()OVER(ORDER BY CONT_TRCK.STRUCKID) AS KEYID
                        ,CASE WHEN REF_SCONTRACTID IS NULL THEN CONT_TRCK.SCONTRACTID||'' ELSE " + SCONTRACTID + @"||'' END NCONTRACTID,CONT_TRCK.STRUCKID , VEH.SHEADREGISTERNO VEH_NO
                        ,CONT_TRCK.STRAILERID , TU.SHEADREGISTERNO TU_NO
                        ,CONT_TRCK.CSTANDBY,CONT_TRCK.CREJECT,CONT_TRCK.DSTART,CONT_TRCK.DEND
                        ,CONT_TRCK.DCREATE,CONT_TRCK.SCREATE,CONT_TRCK.DUPDATE,CONT_TRCK.SUPDATE ,VEH.SCARTYPEID, NVL(VEH.NTOTALCAPACITY,0) NTOTALCAPACITY
                        , CASE WHEN  VEH.SCARTYPEID=0 AND NVL(VEH.NTOTALCAPACITY,0) <= 10000 THEN '6Wheel'
                        ELSE
                             CASE WHEN  VEH.SCARTYPEID=3 THEN 'Semi-Trailer' ELSE '10Wheel' END
                         END TRUCK_TYPE,CASE WHEN REF_SCONTRACTID IS NULL THEN '0' ELSE '1' END MOVED,'0' CNEW ,'0' CCHANGE,'0' CDEL 
                        FROM TCONTRACT_TRUCK_TEMP CONT_TRCK
                        LEFT JOIN TTRUCK VEH ON CONT_TRCK.STRUCKID = VEH.STRUCKID
                        LEFT JOIN TTRUCK TU ON CONT_TRCK.STRAILERID = TU.STRUCKID
                        WHERE (CONT_TRCK.SCONTRACTID = " + SCONTRACTID + " AND CONT_TRCK.REF_SCONTRACTID IS NULL AND REQ_ID = '" + REQ_ID + "') OR CONT_TRCK.REF_SCONTRACTID = " + SCONTRACTID + " ORDER BY TRUCK_TYPE";
                        sdsContractTruck.DataBind();
                    }
                    else
                    {

                    }

                    BindData("BIND_CONTRACT_INFORMATION");
                    BindData("BIND_CONTRACT_GUARANTEE");
                    BindData("BIND_CONTRACT_PLANT");
                    BindData("BIND_CONTRACT_TRUCK");
                    BindData("BIND_CONTRACT_DOC");
                    BindData("BIND_CONTRACT_FILE");
                    BindData("BIND_CONTRACT_GPRODUCT");
                    ChangeMode("" + QueryString[0]);
                }
                else
                {
                    btnEditMode.Visible = false;
                    btnViewMode.Visible = false;
                    trModify_type.Visible = false;
                    btnTruckManagment.Enabled = false;
                    ChangeMode("EDIT");
                }
            }
        }
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        string SMODE, SCONTRACTID, SVENDORID;
        SMODE = "" + Session["SMODE"];
        SCONTRACTID = "" + Session["SCONTRACTID"];
        SVENDORID = "" + Session["SVENDORID"];

        switch (paras[0])
        {
            case "SAVE":

                DataTable dtContractTruck = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractTruck"], ((DataTable)Session["ssContractTruck"]).Select("CSTANDBY='N' AND CDEL = '0'"));
                int Check = dtContractTruck.Rows.Count;
                int Car_Contract = SystemFunction.TRUCK_CONTRAC_CHECK(SVENDORID, SCONTRACTID);
                if (Check == Car_Contract)
                {
                    SaveData(SMODE);
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาใส่รถในสัญญาให้เท่ากับ " + Car_Contract + " คัน ตอนนี้ท่านมี " + Check + " คัน ');");
                }
                break;
            case "TManagement":
                //ClearSession();
                string cEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + SCONTRACTID + "&" + SVENDORID))
                        , cUrl = "TruckContractManagement_p4.aspx?str=";
                xcpn.JSProperties["cpRedirectOpen"] = cUrl + cEncrypt;
                break;

            case "BACK":
                string str = Request.QueryString["strID"];
                if (!string.IsNullOrEmpty(str))
                {

                    xcpn.JSProperties["cpRedirectTo"] = "vendor_request.aspx";
                }
                else
                {

                    xcpn.JSProperties["cpRedirectTo"] = "Vendor_Detail.aspx";
                }
                break;
        }
    }

    protected void btnEditMode_Click(object sender, EventArgs e)
    {
        ChangeMode("EDIT");
    }
    protected void btnViewMode_Click(object sender, EventArgs e)
    {
        ChangeMode("VIEW");
    }

    #region GridView

    protected void gvwContractDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwContractDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "SCONTRACTID", "SDOCID", "SDOCVERSION", "SDOCTYPE", "SFILENAME"
            , "SSYSFILENAME", "SDESCRIPTION", "SPATH", "NVERSION", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwContractDoc.CancelEdit();
                break;
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[7] + "" + dyData[5];
                gvwContractDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DEL_CONTRACT_DOC":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
                DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractDoc", "", "" + dyData[0], "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("SDOCID='" + dyData[1] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_CONTRACT_DOC");
                break;
            case "BIND_CONTRACT_DOC":
                BindData("BIND_CONTRACT_DOC");
                break;

        }
    }

    protected void gvwContractFile_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwContractFile_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "SCONTRACTID", "SDOCID", "SDOCVERSION", "SDOCTYPE", "SFILENAME"
            , "SSYSFILENAME", "SDESCRIPTION", "SPATH", "NVERSION", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwContractFile.CancelEdit();
                break;
            case "VIEWFILE":
                string sUrl_View = "openFile.aspx?str=" + dyData[7] + "" + dyData[5];
                gvwContractFile.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DEL_CONTRACT_FILE":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
                DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractDoc", "", "" + dyData[0], "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("SDOCID='" + dyData[1] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_CONTRACT_FILE");
                break;
            case "BIND_CONTRACT_FILE":
                BindData("BIND_CONTRACT_FILE");
                break;

        }
    }

    protected void gvwGuarantee_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwGuarantee_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        string SCONTRACTID = "" + Session["SCONTRACTID"];

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwGuarantee.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwGuarantee.StartEdit(visibleindex);
                dynamic dyData = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");
                ASPxComboBox cmbSGUARANTEETYPE = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbSGUARANTEETYPE");
                ASPxTextBox txtSGUARANTEETYPE = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSGUARANTEETYPE");
                ASPxTextBox txtNCGID = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtNCGID");
                ASPxTextBox txtSBOOKNO = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSBOOKNO");
                ASPxTextBox txtGuaranteeValue = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtGuaranteeValue");
                ASPxComboBox cmbRemark = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbRemark");
                cmbSGUARANTEETYPE.Value = "" + dyData[3];
                txtSGUARANTEETYPE.Text = "" + dyData[3];
                txtNCGID.Text = "" + dyData[0];
                txtSBOOKNO.Text = "" + dyData[2];
                txtGuaranteeValue.Text = "" + dyData[5];
                foreach (ListEditItem item in cmbRemark.Items)
                {
                    if (item.Text == "" + dyData[6])
                    {
                        item.Selected = true;
                        break;
                    }
                }
                #endregion
                break;
            case "NEWGUARANTEE":
                gvwGuarantee.AddNewRow();
                break;
            case "DELGUARANTEE":
                dynamic dyDataDel = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");

                DataTable dtGuarantee4Delete = PrepareDataTable("ssGuarantee", "", "" + dyDataDel[1], "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtGuarantee4Delete.Select("NCGID='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtGuarantee4Delete.Rows.IndexOf(_drDataDel);
                        if (dtGuarantee4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtGuarantee4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtGuarantee4Delete.Rows[idx_drTemp].BeginEdit();
                            dtGuarantee4Delete.Rows[idx_drTemp]["CDEL"] = (dtGuarantee4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGuarantee4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssGuarantee"] = dtGuarantee4Delete;
                gvwGuarantee.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGuarantee4Delete, dtGuarantee4Delete.Select("CDEL='0'"));
                gvwGuarantee.DataBind();
                break;
            case "SAVEGUARANTEE":
                dynamic dyDataSubmit = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE",
                    "NAMOUNT", "SREMARK");

                string guarantee_mode = (visibleindex == -1) ? "Add" : "Edit";
                /*NCGID NCONTRACTID SBOOKNO GUARANTEESTYPE_ID   SGUARANTEETYPE  NAMOUNT SREMARK CCHANGE CDEL*/
                DataTable dtGuarantee = PrepareDataTable("ssGuarantee", "", "" + dyDataSubmit[1], "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");
                /*FindControl*/
                ASPxComboBox Save_cmbSGUARANTEETYPE = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbSGUARANTEETYPE");
                ASPxTextBox Save_txtSGUARANTEETYPE = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSGUARANTEETYPE");
                ASPxTextBox Save_txtNCGID = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtNCGID");
                ASPxTextBox Save_txtSBOOKNO = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSBOOKNO");
                ASPxTextBox Save_txtGuaranteeValue = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtGuaranteeValue");
                ASPxComboBox Save_cmbRemark = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbRemark");
                switch (guarantee_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */

                        string NCGID = "" + dtGuarantee.Compute("MIN(NCGID)", "");
                        NCGID = NCGID == "" ? "-1" : "" + (int.Parse(NCGID) - 1);
                        DataRow drNewRow = dtGuarantee.NewRow();
                        drNewRow["NCGID"] = "" + NCGID;
                        drNewRow["NCONTRACTID"] = SCONTRACTID;
                        drNewRow["SBOOKNO"] = "" + Save_txtSBOOKNO.Text;
                        drNewRow["GUARANTEESTYPE_ID"] = "" + Save_cmbSGUARANTEETYPE.Value;
                        drNewRow["SGUARANTEETYPE"] = "" + Save_cmbSGUARANTEETYPE.Text;
                        drNewRow["NAMOUNT"] = "" + Save_txtGuaranteeValue.Text;
                        drNewRow["SREMARK"] = "" + Save_cmbRemark.Text;
                        drNewRow["CNEW"] = "1";
                        drNewRow["CCHANGE"] = "0";
                        drNewRow["CDEL"] = "0";
                        dtGuarantee.Rows.Add(drNewRow);
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtGuarantee.Select("NCGID='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtGuarantee.Rows.IndexOf(_drData[0]);
                            dtGuarantee.Rows[idx_drTemp].BeginEdit();

                            //dtGuarantee.Rows[idx_drTemp]["NCGID"] = "" + NCGID;
                            dtGuarantee.Rows[idx_drTemp]["NCONTRACTID"] = SCONTRACTID;
                            dtGuarantee.Rows[idx_drTemp]["SBOOKNO"] = "" + Save_txtSBOOKNO.Text;
                            dtGuarantee.Rows[idx_drTemp]["GUARANTEESTYPE_ID"] = "" + Save_cmbSGUARANTEETYPE.Value;
                            dtGuarantee.Rows[idx_drTemp]["SGUARANTEETYPE"] = "" + Save_cmbSGUARANTEETYPE.Text;
                            dtGuarantee.Rows[idx_drTemp]["NAMOUNT"] = "" + Save_txtGuaranteeValue.Text;
                            dtGuarantee.Rows[idx_drTemp]["SREMARK"] = "" + Save_cmbRemark.Text;
                            dtGuarantee.Rows[idx_drTemp]["CNEW"] = dtGuarantee.Rows[idx_drTemp]["CNEW"] + "";
                            dtGuarantee.Rows[idx_drTemp]["CCHANGE"] = (dtGuarantee.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGuarantee.Rows[idx_drTemp]["CDEL"] = "0";

                            dtGuarantee.Rows[idx_drTemp].EndEdit();

                        }
                        #endregion
                        break;

                }
                Session["ssGuarantee"] = dtGuarantee;
                gvwGuarantee.CancelEdit();
                gvwGuarantee.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGuarantee, dtGuarantee.Select("CDEL='0'"));
                gvwGuarantee.DataBind();
                break;
        }
    }

    protected void gvwTerminal_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwTerminal_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        string SCONTRACTID = "" + Session["SCONTRACTID"];

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwTerminal.CancelEdit();
                break;
            case "CHECKPLANT":
                if (Session["ssContractPlant"] != null)
                {
                    if (((DataTable)Session["ssContractPlant"]).Select("STERMINALID='" + cboMainPlant.Value + "' AND CDEL='0' AND CMAIN_PLANT='0'").Length > 0)
                    {
                        CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลัง " + cboMainPlant.Text + "เป็นคลังต้นทางอื่นๆแล้ว! กรุณาตรวจสอบ');");

                    }
                    BindData("BIND_CONTRACT_PLANT");
                }
                break;
            case "NEWCONTRACTPLANT":
                //gvwTerminal.AddNewRow();
                DataTable dtCONTRACT_Plant = PrepareDataTable("ssContractPlant", "", "" + dyData[2], "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");
                if (cboMainPlant.Value + "" == cboPLANTOTHER.Value + "")
                    CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลัง " + cboPLANTOTHER.Text + "เป็นคลังต้นทางหลักแล้ว! กรุณาตรวจสอบ');");
                else if (dtCONTRACT_Plant.Select("STERMINALID='" + cboPLANTOTHER.Value + "' AND CDEL='0'").Length > 0)
                    CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลังต้นทางอื่นๆเป็น " + cboPLANTOTHER.Text + "แล้ว! กรุณาตรวจสอบ');");
                else
                {
                    DataRow drCONT_PLNT = dtCONTRACT_Plant.NewRow();

                    string KeyID = "" + dtCONTRACT_Plant.Compute("MIN(KEYID)", "");
                    KeyID = KeyID == "" ? "-1" : "" + (int.Parse(KeyID) - 1);

                    drCONT_PLNT["KEYID"] = "" + KeyID;
                    drCONT_PLNT["STERMINALID"] = "" + cboPLANTOTHER.Value;
                    drCONT_PLNT["STERMINALNAME"] = "" + cboPLANTOTHER.Text;
                    drCONT_PLNT["NCONTRACTID"] = SCONTRACTID;
                    drCONT_PLNT["CMAIN_PLANT"] = "0";
                    drCONT_PLNT["CACTIVE"] = "1";
                    drCONT_PLNT["CNEW"] = "1";
                    drCONT_PLNT["CCHANGE"] = "0";
                    drCONT_PLNT["CDEL"] = "0";
                    dtCONTRACT_Plant.Rows.Add(drCONT_PLNT);

                    Session["ssContractPlant"] = dtCONTRACT_Plant;

                }
                BindData("BIND_CONTRACT_PLANT");
                break;

            case "DEL_CONTRACT_PLANT":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
                DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractPlant", "", "" + dyData[2], "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");

                DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("STERMINALID='" + dyData[0] + "' AND NCONTRACTID='" + dyData[2] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_CONTRACT_PLANT");
                break;
            case "BIND_CONTRACT_PLANT":
                BindData("BIND_CONTRACT_PLANT");
                break;

        }
    }

    protected void gvwGProduct_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwGProduct_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        string SCONTRACTID = "" + Session["SCONTRACTID"];

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwGProduct.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwGProduct.StartEdit(visibleindex);
                dynamic dyData = gvwGProduct.GetRowValues(visibleindex, "GID", "SCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                ASPxComboBox cmbProductType = (ASPxComboBox)gvwGProduct.FindEditFormTemplateControl("cmbProductType");
                cmbProductType.Value = "" + dyData[2];
                #endregion
                break;
            case "NEWGPRODUCT":
                gvwGProduct.AddNewRow();
                break;
            case "DELGPRODUCT":
                dynamic dyDataDel = gvwGProduct.GetRowValues(visibleindex, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                DataTable dtGProduct4Delete = PrepareDataTable("ssGProduct", "", "" + dyDataDel[1], "GID", "SCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtGProduct4Delete.Select("GID='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtGProduct4Delete.Rows.IndexOf(_drDataDel);
                        if (dtGProduct4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtGProduct4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtGProduct4Delete.Rows[idx_drTemp].BeginEdit();
                            dtGProduct4Delete.Rows[idx_drTemp]["CDEL"] = (dtGProduct4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGProduct4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssGProduct"] = dtGProduct4Delete;
                gvwGProduct.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGProduct4Delete, dtGProduct4Delete.Select("CDEL='0'"));
                gvwGProduct.DataBind();
                break;
            case "SAVEGPRODUCT":
                dynamic dyDataSubmit = gvwGProduct.GetRowValues(visibleindex, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

                string GProduct_mode = (visibleindex == -1) ? "Add" : "Edit";
                DataTable dtGProdcut = PrepareDataTable("ssGProduct", "", "" + dyDataSubmit[1], "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                /*FindControl*/
                ASPxComboBox Save_cmbProductType = (ASPxComboBox)gvwGProduct.FindEditFormTemplateControl("cmbProductType");

                switch (GProduct_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */

                        if (dtGProdcut.Select("CDEL='0' AND SPRODUCTTYPEID='" + Save_cmbProductType.Value + "'").Length > 0)
                        {
                            CommonFunction.SetPopupOnLoad(gvwGProduct, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ กลุ่มผลิตภัณฑ์นี้อยู่ในรายการแล้ว');");
                            BindData("BIND_CONTRACT_GPRODUCT");
                            return;
                        }

                        string GID = "" + dtGProdcut.Compute("MIN(GID)", "");
                        GID = GID == "" ? "-1" : "" + (int.Parse(GID) - 1);
                        DataRow drNewRow = dtGProdcut.NewRow();
                        drNewRow["GID"] = "" + GID;
                        drNewRow["NCONTRACTID"] = SCONTRACTID;
                        drNewRow["SPRODUCTTYPEID"] = "" + Save_cmbProductType.Value;
                        drNewRow["SPRODUCTTYPENAME"] = "" + Save_cmbProductType.Text;
                        drNewRow["CACTIVE"] = "1";
                        drNewRow["CNEW"] = "1";
                        drNewRow["CCHANGE"] = "0";
                        drNewRow["CDEL"] = "0";
                        dtGProdcut.Rows.Add(drNewRow);
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        if (dtGProdcut.Select("GID<>'" + dyDataSubmit[0] + "' AND CDEL='0' AND SPRODUCTTYPEID='" + Save_cmbProductType.Value + "'").Length > 0)
                        {
                            CommonFunction.SetPopupOnLoad(gvwGProduct, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ กลุ่มผลิตภัณฑ์นี้อยู่ในรายการแล้ว');");
                            BindData("BIND_CONTRACT_GPRODUCT");
                            return;
                        }

                        DataRow[] _drData = dtGProdcut.Select("GID='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtGProdcut.Rows.IndexOf(_drData[0]);
                            dtGProdcut.Rows[idx_drTemp].BeginEdit();

                            dtGProdcut.Rows[idx_drTemp]["NCONTRACTID"] = SCONTRACTID;
                            dtGProdcut.Rows[idx_drTemp]["SPRODUCTTYPEID"] = "" + Save_cmbProductType.Value;
                            dtGProdcut.Rows[idx_drTemp]["SPRODUCTTYPENAME"] = "" + Save_cmbProductType.Text;
                            dtGProdcut.Rows[idx_drTemp]["CACTIVE"] = "1";
                            dtGProdcut.Rows[idx_drTemp]["CNEW"] = dtGProdcut.Rows[idx_drTemp]["CNEW"] + "";
                            dtGProdcut.Rows[idx_drTemp]["CCHANGE"] = (dtGProdcut.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGProdcut.Rows[idx_drTemp]["CDEL"] = "0";

                            dtGProdcut.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["ssGProduct"] = dtGProdcut;
                gvwGProduct.CancelEdit();
                gvwGProduct.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGProdcut, dtGProdcut.Select("CDEL='0'"));
                gvwGProduct.DataBind();
                break;
        }
    }

    protected void gvwTruck_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        string SCONTRACTID = "" + Session["SCONTRACTID"];

        /*  ,"NCONTRACTID","STRUCKID","STRAILERID", "CSTANDBY",,"CREJECT","DSTART","DEND","KEYID","CNEW","CCHANGE","CDEL","Truck_Type","VEH_NO","TU_NO"  */
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex
            , "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
            case "BIND_CONTRACT_TRUCK":
                BindData("BIND_CONTRACT_TRUCK");
                break;
            case "VIEWTRUCK":
                string scartype = CommonFunction.Get_Value(conn, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID='" + dyData[1] + "'");
                string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("View&" + dyData[1] + "&" + dyData[2] + "&" + scartype))
                    , sUrl = "truck_edit.aspx?str=";
                gvwTruck.JSProperties["cpRedirectOpen"] = sUrl + sEncrypt;
                break;
            case "ADD_CONTRACT_TRUCK":
                string msg = "";
                #region policy
                DataTable dtCONTRACT_Truck = PrepareDataTable("ssContractTruck", "", "" + dyData[0], "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL", "MOVED");
                if (VEH_TYPE.Text != "" && cboVEH.Value + "" != "")
                {
                    switch (VEH_TYPE.Text)
                    {
                        case "สิบล้อ":
                            //สิบล้อ ต้องไม่มีหาง
                            if (cboTU.Value + "" != "")
                            {
                                msg += "<br /> - รถประเภทสิบล้อ ไม่สามารถระบุหางได้ ";
                            }
                            break;
                        case "Semi-Trailer":
                            //Semi-Trailer ต้องมีหาง
                            if (cboTU.Value + "" == "")
                            {
                                msg += "<br /> - รถประเภท semi-trailer จำเป็นต้องระบุทะเบียนหาง ";
                            }
                            break;

                    }
                }
                else
                {
                    msg += "<br /> - รถที่ท่านระบุไม่ได้อยู่ในประเภทที่ปตท.อนุญาติ ";
                }
                //เช็คหัวซ้ำในสัญญาเดียวกัน
                if (dtCONTRACT_Truck.Select("STRUCKID='" + cboVEH.Value + "'").Length > 0)
                {
                    msg += "<br />- ทะเบียนหัว " + cboVEH.Text + " เคยถูกระบุในสัญญานี้แล้ว";
                }
                //เช็คหางซ้ำในสัญญาเดียวกัน
                if (cboTU.Value + "" != "" && dtCONTRACT_Truck.Select("STRAILERID='" + cboTU.Value + "'").Length > 0)
                {
                    msg += "<br />- ทะเบียนหาง " + cboTU.Text + " เคยถูกระบุในสัญญานี้แล้ว";
                }

                string sqlCmd;
                if (SCONTRACTID == "")
                    sqlCmd = "SELECT c.SCONTRACTID,c.SVENDORID,c.SCONTRACTNO,ct.STRUCKID,ct.STRAILERID FROM TCONTRACT_TEMP c LEFT JOIN TCONTRACT_TRUCK_TEMP ct ON c.SCONTRACTID=ct.SCONTRACTID WHERE  (ct.STRUCKID='" + "" + cboVEH.Value + "' OR ct.STRAILERID='" + "" + cboTU.Value + "') AND c.CACTIVE='Y'";
                else
                    sqlCmd = "SELECT c.SCONTRACTID,c.SVENDORID,c.SCONTRACTNO,ct.STRUCKID,ct.STRAILERID FROM TCONTRACT_TEMP c LEFT JOIN TCONTRACT_TRUCK_TEMP ct ON c.SCONTRACTID=ct.SCONTRACTID WHERE c.SCONTRACTID!='" + SCONTRACTID + "" + "' AND (ct.STRUCKID='" + "" + cboVEH.Value + "' OR ct.STRAILERID='" + "" + cboTU.Value + "') AND c.CACTIVE='Y'";

                DataTable dtCheck = CommonFunction.Get_Data(conn, sqlCmd);
                //เช็คหัวซ้ำในสัญญาอื่น
                if (dtCheck.Select("STRUCKID='" + "" + cboVEH.Value + "'").Length > 0)
                {
                    DataRow[] dr = dtCheck.Select("STRUCKID='" + "" + cboVEH.Value + "'");
                    string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dr[0]["SCONTRACTID"] + "&" + dr[0]["SVENDORID"]))
                            , vsUrl = "contract_add.aspx?str=" + vsEncrypt;
                    string link = @"<a href=\""javascript:void(0);\"" onclick=\""javascript:window.open(\'" + vsUrl + @"\');\"">" + dr[0]["SCONTRACTNO"] + "</a>";
                    msg += @"<br />- ทะเบียนหัว " + cboVEH.Text + @" ได้ถูกระบุในสัญญาเลขที่<br/> " + link + " แล้ว";
                }
                //เช็คหางซ้ำในสัญญาอื่น
                if (dtCheck.Select("STRAILERID='" + "" + cboTU.Value + "'").Length > 0)
                {
                    DataRow[] dr = dtCheck.Select("STRAILERID='" + "" + cboTU.Value + "'");
                    string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dr[0]["SCONTRACTID"] + "&" + dr[0]["SVENDORID"]))
                            , vsUrl = "contract_add.aspx?str=" + vsEncrypt;
                    string link = @"<a href=\""javascript:void(0);\"" onclick=\""javascript:window.open(\'" + vsUrl + @"\');\"">" + dr[0]["SCONTRACTNO"] + "</a>";
                    msg += @"<br />- ทะเบียนหาง " + cboTU.Text + @" ได้ถูกระบุในสัญญาเลขที่<br/> " + link + " แล้ว";
                }

                if (msg != "")
                {
                    CommonFunction.SetPopupOnLoad(gvwTruck, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ " + msg + "');");
                    BindData("BIND_CONTRACT_TRUCK");
                    return;
                }
                #endregion
                DataRow drCONT_PLNT = dtCONTRACT_Truck.NewRow();

                string KeyID = "" + dtCONTRACT_Truck.Compute("MIN(KEYID)", "");
                KeyID = KeyID == "" ? "-1" : "" + (int.Parse(KeyID) - 1);

                drCONT_PLNT["KEYID"] = "" + KeyID;
                drCONT_PLNT["NCONTRACTID"] = SCONTRACTID;
                drCONT_PLNT["STRUCKID"] = "" + cboVEH.Value;
                drCONT_PLNT["STRAILERID"] = "" + cboTU.Value;
                drCONT_PLNT["VEH_NO"] = "" + cboVEH.Text;
                drCONT_PLNT["TU_NO"] = "" + ((cboTU.Value + "" == "") ? "-" : cboTU.Text);
                drCONT_PLNT["Truck_Type"] = ("" + cboTU.Value == "") ? "10Wheel" : "Semi-Trailer";
                drCONT_PLNT["CSTANDBY"] = ckbIsStandBy.Checked ? "Y" : "N";
                //drCONT_PLNT["CREJECT"] = null;
                //drCONT_PLNT["DSTART"] = null;
                //drCONT_PLNT["DEND"] = null;

                drCONT_PLNT["CNEW"] = "1";
                drCONT_PLNT["CCHANGE"] = "0";
                drCONT_PLNT["CDEL"] = "0";
                drCONT_PLNT["MOVED"] = "0";
                dtCONTRACT_Truck.Rows.Add(drCONT_PLNT);

                Session["ssContractTruck"] = dtCONTRACT_Truck;
                cboVEH.Text = VEH_TYPE.Text = TU_TYPE.Text = cboTU.Text = "";
                BindData("BIND_CONTRACT_TRUCK");
                break;
            case "EDIT_CONTRACT_TRUCK":
                TRUCK_KEYID.Text = "" + dyData[9];
                cboVEH.Value = "" + dyData[1];
                cboTU.Value = "" + dyData[2];
                break;
            case "DEL_CONTRACT_TRUCK":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK"); 
                DataTable dtCONTRACT_TRCK4Delete = PrepareDataTable("ssContractTruck", "", "" + dyData[0], "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL", "MOVED");
                DataRow[] _drDataDels = dtCONTRACT_TRCK4Delete.Select(" NCONTRACTID='" + dyData[0] + "' AND STRUCKID='" + dyData[1] + "' AND isnull(STRAILERID,'')='" + dyData[2] + "' ");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        if (_drDataDel["MOVED"].ToString().Trim() == "1")
                        {
                            CommonFunction.SetPopupOnLoad(gvwTruck, "dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากรถคันนี้ถูกโยกไปช่วยสัญญาอื่น');"); break;
                        }
                        int idx_drTemp = dtCONTRACT_TRCK4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssContractTruck"] = dtCONTRACT_TRCK4Delete;
                BindData("BIND_CONTRACT_TRUCK");
                break;


        }
    }
    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType)
        {
            case GridViewRowType.Data:
                if (e.GetValue("TU_NO") + "" == "")
                {
                    ASPxLabel lblTU_No = (ASPxLabel)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "lblTU_No");
                    lblTU_No.Text = "-";
                }
                ASPxButton btnDelTruck = (ASPxButton)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDelTruck");
                if (btnDelTruck != null)
                {
                    if (e.GetValue("MOVED").ToString().Trim() == "1")
                        btnDelTruck.ClientSideEvents.Click = "function(s,e){ dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากรถคันนี้ถูกโยกไปช่วยสัญญาอื่น'); }";
                    else
                        btnDelTruck.ClientSideEvents.Click = "function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('DEL_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }";
                }
                break;
        }
    }

    #endregion

    #region Combo box

    protected void cboVendor_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE  v.SVENDORID||v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }

    protected void cboMainPlant_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboMainPlant_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT STERMINALID,SABBREVIATION STERMINALNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY STERMINALID) AS RN ,STERMINALID ,SABBREVIATION  FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1' AND STERMINALID||''||SABBREVIATION LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }

    protected void cboPLANTOTHER_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboPLANTOTHER_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT STERMINALID,SABBREVIATION STERMINALNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY STERMINALID) AS RN ,STERMINALID ,SABBREVIATION  FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1' AND STERMINALID||''||SABBREVIATION LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }

    protected void cboVEH_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVEH_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVEH.SelectCommand = @"SELECT STRUCKID ,VEH_NO ,STRAILERID ,TU_NO ,SCARTYPEID ,VEH_TYPE FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY VEH.STRUCKID) AS RN 
    ,VEH.STRUCKID ,VEH.SHEADREGISTERNO VEH_NO ,VEH.STRAILERID , TU.SHEADREGISTERNO TU_NO
    ,VEH.SCARTYPEID 
    ,CASE WHEN VEH.SCARTYPEID='0' THEN 'สิบล้อ' ELSE 'Semi-Trailer' END VEH_TYPE
    FROM TTRUCK VEH  
    LEFT JOIN TTRUCK TU ON VEH.STRAILERID = TU.STRUCKID
    LEFT JOIN TTRUCKTYPE VEH_TYPE ON VEH.SCARTYPEID =VEH_TYPE.SCARTYPEID
    WHERE VEH.SCARTYPEID in('0','3') AND VEH.STRUCKID||''||VEH.SHEADREGISTERNO LIKE :fillter 
) TBL
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVEH.SelectParameters.Clear();
        sdsVEH.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVEH.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVEH.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVEH;
        comboBox.DataBind();
    }

    protected void cboTU_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        cboTU.DataBind();
    }

    #endregion

    #region Uploadfile

    protected void ulcContractDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string SCONTRACTID = "" + Session["SCONTRACTID"];

        e.CallbackData = UploadFile2Server(e.UploadedFile
            , "ContractDoc_" + SCONTRACTID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
            , UploadContractDirectory + "Temp/ContractDoc/" + SCONTRACTID + "/");

        DataTable dtContractDoc = PrepareDataTable("ssContractDoc", "", "" + SCONTRACTID
            , "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");
        string[] sFileArray = e.CallbackData.Split('$');

        string DOCID = "" + dtContractDoc.Compute("MIN(SDOCID)", "");
        DOCID = (DOCID == "") ? "-1" : "" + (int.Parse(DOCID) - 1);
        DataRow drNewRow = dtContractDoc.NewRow();
        drNewRow["SDOCID"] = "" + DOCID;
        drNewRow["SCONTRACTID"] = "" + SCONTRACTID;
        drNewRow["SDOCTYPE"] = "DOC";
        drNewRow["SFILENAME"] = "" + sFileArray[1];
        drNewRow["SSYSFILENAME"] = "" + sFileArray[0];
        drNewRow["SDESCRIPTION"] = "เอกสารสัญญา";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = UploadContractDirectory + "/Temp/ContractDoc/" + SCONTRACTID + "/";
        drNewRow["CNEW"] = "1";
        drNewRow["CCHANGE"] = "0";
        drNewRow["CDEL"] = "0";
        dtContractDoc.Rows.Add(drNewRow);

        Session["ssContractDoc"] = dtContractDoc;
    }

    protected void ulcContractFile_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string SCONTRACTID = "" + Session["SCONTRACTID"];

        e.CallbackData = UploadFile2Server(e.UploadedFile
            , "ContractDoc_" + SCONTRACTID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
            , UploadContractDirectory + "Temp/ContractFile/" + SCONTRACTID + "/");

        DataTable dtContractDoc = PrepareDataTable("ssContractDoc", "", "" + SCONTRACTID
            , "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");
        string[] sFileArray = e.CallbackData.Split('$');

        string DOCID = "" + dtContractDoc.Compute("MIN(SDOCID)", "");
        DOCID = (DOCID == "") ? "-1" : "" + (int.Parse(DOCID) - 1);
        DataRow drNewRow = dtContractDoc.NewRow();
        drNewRow["SDOCID"] = "" + DOCID;
        drNewRow["SCONTRACTID"] = "" + SCONTRACTID;
        drNewRow["SDOCTYPE"] = "FILE";
        drNewRow["SFILENAME"] = "" + sFileArray[1];
        drNewRow["SSYSFILENAME"] = "" + sFileArray[0];
        drNewRow["SDESCRIPTION"] = "เอกสารอื่นๆ";
        drNewRow["CACTIVE"] = "1";
        drNewRow["SPATH"] = UploadContractDirectory + "/Temp/ContractFile/" + SCONTRACTID + "/";
        drNewRow["CNEW"] = "1";
        drNewRow["CCHANGE"] = "0";
        drNewRow["CDEL"] = "0";
        dtContractDoc.Rows.Add(drNewRow);

        Session["ssContractDoc"] = dtContractDoc;
    }

    #endregion

    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    void BindData(string mode)
    {

        string SCONTRACTID = "" + Session["SCONTRACTID"];

        DataTable dtContractInfo;
        DataView dvContractInfo;
        DataSourceSelectArguments args;
        switch (mode.ToUpper())
        {
            case "BIND_CONTRACT_GUARANTEE":
                #region หลักประกันสัญญา

                if (Session["ssGuarantee"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsGUARANTEES.Select(args);
                    Session["ssGuarantee"] = dvContractInfo.ToTable();
                }

                gvwGuarantee.DataSource = PrepareDataTable("ssGuarantee", "", SCONTRACTID, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL"); ;
                gvwGuarantee.DataBind();
                #endregion
                break;
            case "BIND_CONTRACT_INFORMATION":
                #region รายละเอียดสัญญา"


                args = new DataSourceSelectArguments();
                dvContractInfo = (DataView)sdsContract.Select(args);
                dtContractInfo = dvContractInfo.ToTable();
                if (dtContractInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtContractInfo.Rows)
                    {
                        if ("" + drInfo["RENEWFROM"] != "")
                            txtRenewFrom.Text = "" + drInfo["RENEWFROM"] + "-" + drInfo["SCONTRACTID"];
                        else
                            txtRenewFrom.Text = "" + drInfo["SCONTRACTID"];
                        cblProcurement.Value = "" + drInfo["CSPACIALCONTRAC"];
                        txtContractNo.Text = "" + drInfo["SCONTRACTNO"];
                        cmbVendor.DataSource = CommonFunction.Get_Data(conn, "SELECT v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID='" + drInfo["SVENDORID"] + "'");
                        cmbVendor.DataBind();
                        cmbVendor.Value = "" + drInfo["SVENDORID"];
                        cmbTransType.Value = "" + drInfo["CGROUPCONTRACT"];
                        cmbContractType.Value = "" + drInfo["SCONTRACTTYPEID"];
                        dteStart.Text = Convert.ToDateTime("" + drInfo["DBEGIN"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        if (drInfo["DEND"] + "" != "")
                            dteEnd.Text = Convert.ToDateTime("" + drInfo["DEND"], new CultureInfo("th-TH")).ToString("dd/MM/yyyy");
                        mmoRemark.Text = "" + drInfo["SREMARK"];
                        rblCACTIVE.Value = "" + drInfo["CACTIVE"];
                        break;
                    }
                }
                #endregion
                break;
            case "BIND_CONTRACT_DOC":
                #region แนบเอกสารสัญญา
                if (Session["ssContractDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsDocument.Select(args);
                    Session["ssContractDoc"] = dvContractInfo.ToTable();
                }

                DataTable dtContractDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractDoc"], ((DataTable)Session["ssContractDoc"]).Select("SDOCTYPE='DOC' AND CDEL='0'"));
                gvwContractDoc.DataSource = dtContractDoc;
                gvwContractDoc.DataBind();
                dtContractDoc.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_FILE":
                #region แนบเอกสารอื่นๆ
                if (Session["ssContractDoc"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsDocument.Select(args);
                    Session["ssContractDoc"] = dvContractInfo.ToTable();
                }

                DataTable dtContractFile = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractDoc"], ((DataTable)Session["ssContractDoc"]).Select("SDOCTYPE='FILE' AND CDEL='0'"));
                gvwContractFile.DataSource = dtContractFile;
                gvwContractFile.DataBind();
                dtContractFile.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_PLANT":
                #region คลังต้นทางอื่นๆ
                if (Session["ssContractPlant"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsPlant.Select(args);
                    Session["ssContractPlant"] = dvContractInfo.ToTable();

                }
                foreach (DataRow dr in ((DataTable)Session["ssContractPlant"]).Select("CDEL='0' AND CMAIN_PLANT='1'"))
                {
                    cboMainPlant.Text = "" + dr["STERMINALNAME"].ToString().Split(':')[1].Trim();
                    cboMainPlant.Value = "" + dr["STERMINALID"];
                }
                DataTable dtContractPlant = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractPlant"], ((DataTable)Session["ssContractPlant"]).Select("CDEL='0' AND CMAIN_PLANT='0' "));
                gvwTerminal.DataSource = dtContractPlant;
                gvwTerminal.DataBind();
                dtContractPlant.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_GPRODUCT":
                #region กลุ่มผลิตภัณฑ์
                if (Session["ssGProduct"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsGPRODUCT.Select(args);
                    Session["ssGProduct"] = dvContractInfo.ToTable();
                }

                gvwGProduct.DataSource = PrepareDataTable("ssGProduct", "", SCONTRACTID, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL"); ;
                gvwGProduct.DataBind();
                #endregion
                break;
            case "BIND_CONTRACT_TRUCK":
                #region รายชื่อรถในสัญญา
                if (Session["ssContractTruck"] == null)
                {
                    args = new DataSourceSelectArguments();
                    dvContractInfo = (DataView)sdsContractTruck.Select(args);
                    Session["ssContractTruck"] = dvContractInfo.ToTable();
                }

                DataTable dtContractTruck = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractTruck"], ((DataTable)Session["ssContractTruck"]).Select("CDEL='0'"));
                gvwTruck.DataSource = dtContractTruck;
                gvwTruck.DataBind();
                dtContractTruck.Dispose();
                #endregion
                break;
        }
    }
    void ChangeMode(string mode)
    {
        if (mode == "EDIT" && Session["chkurl"] + "" == "1")
        {
            mode = "VIEW";
            btnEditMode.ClientEnabled = false;
            btnTruckManagment.ClientEnabled = false;
        }
        bool V;
        string sColor;
        if (mode == "VIEW")
        {
            V = true;
            sColor = "#EEEEEE";
            btnEditMode.Visible = V;
            btnViewMode.Visible = !V;
        }
        else
        {
            V = false;
            sColor = "#FFFFFF";
            btnEditMode.Visible = V;
            btnViewMode.Visible = !V;
        }

        rpnInformation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
        cboModify_type.ReadOnly = true;
        txtContractNo.ReadOnly = true;
        cmbContractType.ReadOnly = true;
        dteStart.ReadOnly = true;
        dteEnd.ReadOnly = true;
        cmbTransType.ReadOnly = true;
        cblProcurement.ReadOnly = true;
        cmbVendor.ReadOnly = true;
        mmoRemark.ReadOnly = true;
        rblCACTIVE.ReadOnly = true;


        rpnDoc.Content.BackColor = System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
        ulcContractDoc.Enabled = false;
        btnUploadDoc.Enabled = false;
        gvwContractDoc.Columns[2].Visible = false;
        gvwContractDoc.Columns[3].Visible = true;
        ulcContractFile.Enabled = false;
        btnUploadFile.Enabled = false;
        gvwContractFile.Columns[2].Visible = false;
        gvwContractFile.Columns[3].Visible = true;

        rpnGuarantee.Content.BackColor = System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
        gvwGuarantee.Columns[4].Visible = false;
        tbladdgrt.Visible = false;

        rpnplant.Content.BackColor = System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
        cboMainPlant.ReadOnly = true;
        cboPLANTOTHER.ReadOnly = true;
        gvwTerminal.Columns[2].Visible = false;
        tbladdplant.Visible = false;

        rpnGProduct.Content.BackColor = System.Drawing.ColorTranslator.FromHtml("#EEEEEE");
        gvwGProduct.Columns[3].Visible = false;
        tbladdGProduct.Visible = false;

        rpnCTruck.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cboVEH.ReadOnly = V;
        cboTU.ReadOnly = V;
        ckbIsStandBy.ReadOnly = V;
        tblctruck.Visible = !V;
        gvwTruck.Columns[5].Visible = V;
        gvwTruck.Columns[6].Visible = !V;

        btnSubmit.Visible = !V;
        btnCancel.Visible = !V;


    }
    void SaveData(string mode)
    {
        DataTable ssContractDoc = (Session["ssContractDoc"] == null ? new DataTable() : (DataTable)Session["ssContractDoc"]);
        DataTable ssGuarantee = (Session["ssGuarantee"] == null ? new DataTable() : (DataTable)Session["ssGuarantee"]);
        DataTable ssContractPlant = (Session["ssContractPlant"] == null ? new DataTable() : (DataTable)Session["ssContractPlant"]);
        DataTable ssGProduct = (Session["ssGProduct"] == null ? new DataTable() : (DataTable)Session["ssGProduct"]);
        DataTable ssContractTruck = (Session["ssContractTruck"] == null ? new DataTable() : (DataTable)Session["ssContractTruck"]);

        string CONTRACTID = "", sGenID = "", strSql = "", PLNT = "";

        if (mode == "EDIT" & cboModify_type.Value + "" == "1")
        {
            mode = "NEW";
            if (ssContractTruck.Rows.Count > 0)
            {
                if (ssContractTruck.Select("MOVED='0'").Length > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากมีรถในสัญญาถูกโยกไปช่วยสัญญาอื่น');"); return;
                }
            }
        }

        switch (mode)
        {
            case "NEW":
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();

                    // AddToTemp(Session["SVENDORID"]+"");

                    #region กรณีที่มีการต่ออายุสัญญา
                    if (cboModify_type.Value + "" == "1")
                    {
                        //ให้ปรับสถานะสัญญาเก่าเป็น 'N'
                        strSql = @"UPDATE TCONTRACT_TEMP SET CACTIVE=:CACTIVE WHERE SCONTRACTID=:SCONTRACTID";

                        SystemFunction.SQLExecuteNonQuery(conn, strSql);

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "N";
                            com.ExecuteNonQuery();
                        }

                        //Back Up ข้อมูลรถของสัญญาเก่าเก็บไว้ แล้วลบออกจาก Table TCONTRACT_TRUCK เพื่อให้เป็นไปตามเงื่อนไขที่ว่า รถหนี่งคันอยู่ในแค่หนึ่งสัญญา
                        DataView dvContractInfo = (DataView)sdsContractTruck.Select(new DataSourceSelectArguments());
                        foreach (DataRow dr in dvContractInfo.ToTable().Rows)
                        {
                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY_TEMP ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

                            strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY_TEMP(HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT, REF_SCONTRACTID,REF_PREVIOUSCONTRACT) (SELECT :HISTORY_ID,SYSDATE,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,SYSDATE,:SREJECT,REF_SCONTRACTID,REF_PREVIOUSCONTRACT FROM TCONTRACT_TRUCK_TEMP WHERE
                            SCONTRACTID=:NCONTRACTID AND STRUCKID=:STRUCKID AND STRAILERID=:STRAILERID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":HISTORY_ID", OracleType.Number).Value = "" + sGenID;
                                com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"] != "" ? "" + dr["STRAILERID"] : null;
                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.ExecuteNonQuery();
                            }
                        }
                        //ลบข้อมูลรถของสัญญาเก่าทิ้ง
                        strSql = @"DELETE FROM TCONTRACT_TRUCK_TEMP WHERE SCONTRACTID=:NCONTRACTID";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion

                    #region รายละเอียดสัญญา

                    sGenID = CommonFunction.Gen_ID(con, "SELECT SCONTRACTID FROM (SELECT SCONTRACTID+0 As SCONTRACTID FROM TCONTRACT_TEMP ORDER BY SCONTRACTID DESC)  WHERE ROWNUM <= 1");

                    CONTRACTID = sGenID;
                    Session["SCONTRACTID"] = sGenID;
                    Session["SVENDORID"] = "" + cmbVendor.Value;

                    string CGROUPCONTRACT = CommonFunction.Get_Value(conn, @"SELECT CGROUP FROM TCONTRACTTYPE WHERE SCONTRACTTYPEID='" + cmbContractType.Value + "'");
                    foreach (DataRow dr in ssContractPlant.Rows) PLNT += "," + dr["STERMINALID"];

                    strSql = string.Format(@"INSERT INTO TCONTRACT_TEMP(SCONTRACTID,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,SCONT_PLNT,RENEWFROM) Values ('{0}','{1}','{2}','{3}',{4},{5},'{6}','{7}','{8}',SYSDATE,'{9}',SYSDATE,'{9}','{10}','{11}','{12}','{13}',{14})"
                        , sGenID
                        , cmbContractType.Value + ""
                        , txtContractNo.Text
                        , cmbVendor.Value + ""
                        , "to_date('" + Convert.ToDateTime(dteStart.Value).ToString("MM/dd/yyyy", new CultureInfo("en-US")) + "','mm/dd/yyyy')"
                        , (dteEnd.Value == null ? "null" : "to_date('" + Convert.ToDateTime(dteEnd.Value).ToString("MM/dd/yyyy", new CultureInfo("en-US")) + "','mm/dd/yyyy')")
                        , ssContractTruck.Select("CSTANDBY='N'").Length
                        , mmoRemark.Text
                        , rblCACTIVE.Value
                        , Session["UserID"] + ""
                        , CGROUPCONTRACT
                        , cblProcurement.Value + ""
                        , cboMainPlant.Value + ""
                        , PLNT
                        , (cboModify_type.Value + "" == "1" ? "'" + txtRenewFrom.Text + "'" : "null")
                        );

                    SystemFunction.SQLExecuteNonQuery(conn, strSql);

                    #endregion

                    #region หลักประกันสัญญา

                    foreach (DataRow dr in ssGuarantee.Rows)
                    {
                        sGenID = CommonFunction.Gen_ID(con, TCONTRACT_GUARANTEES);

                        strSql = @"INSERT INTO TCONTRACT_GUARANTEES_TEMP(NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) Values
                                (:NCGID,:NCONTRACTID,:SBOOKNO,:SGUARANTEETYPE,:NAMOUNT,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SREMARK,:GUARANTEESTYPE_ID)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":NCGID", OracleType.Number).Value = sGenID;
                            com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = CONTRACTID;
                            com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
                            com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
                            com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
                            com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion

                    #region เอกสารสำคัญของสัญญา

                    foreach (DataRow dr in ssContractDoc.Rows)
                    {
                        sGenID = CommonFunction.Gen_ID(con, TCONTRACT_DOC);

                        strSql = @"INSERT INTO TCONTRACT_DOC_TEMP(SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH) Values
                                (:SCONTRACTID,:SDOCID,:SDOCVERSION,:SDOCTYPE,:SFILENAME,:SSYSFILENAME,:SDESCRIPTION,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SPATH)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = sGenID;
                            com.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = "1";
                            com.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "" + dr["SDOCTYPE"];
                            com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + dr["SFILENAME"];
                            com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + dr["SSYSFILENAME"];
                            com.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "" + dr["SDESCRIPTION"];
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SPATH", OracleType.VarChar).Value = "" + dr["SPATH"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion

                    #region ขอบเขตการขนส่ง

                    if (cboMainPlant.Value + "" != "")//คลังต้นทางหลัก
                    {
                        strSql = @"INSERT INTO TCONTRACT_PLANT_TEMP(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + cboMainPlant.Value;
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "1";
                            com.ExecuteNonQuery();
                        }
                    }

                    foreach (DataRow dr in ssContractPlant.Rows)//คลังต้นทางอื่นๆ
                    {
                        strSql = @"INSERT INTO TCONTRACT_PLANT_TEMP(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                            com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion

                    #region กลุ่มผลิตภัณฑ์

                    foreach (DataRow dr in ssGProduct.Rows)
                    {

                        strSql = @"INSERT INTO TCONTRACT_GPRODUCT_TEMP(SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE) Values
                                (:SCONTRACTID,:SPRODUCTTYPEID,:SPRODUCTTYPENAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                            com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion

                    #region รายชื่อรถในสัญญา

                    foreach (DataRow dr in ssContractTruck.Rows)
                    {

                        strSql = @"INSERT INTO TCONTRACT_TRUCK_TEMP(SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
                                    (:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                            com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
                            com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + dr["CSTANDBY"];
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                            com.ExecuteNonQuery();
                        }

                        //บันทึกเปลี่ยนแปลงข้อมูลสัญญาใน TTRUCK
                        strSql = @"UPDATE TTRUCK SET SCONTRACTID=:SCONTRACTID,SCONTRACTTYPE=:SCONTRACTTYPE WHERE STRUCKID IN (:STRUCKID,:STRAILERID)";

                        using (OracleCommand com = new OracleCommand(strSql, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                            com.Parameters.Add(":SCONTRACTTYPE", OracleType.VarChar).Value = cmbContractType.Value;
                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                            com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
                            com.ExecuteNonQuery();
                        }
                    }
                    #endregion


                }
                break;
            case "EDIT":
                using (OracleConnection con = new OracleConnection(conn))
                {
                    con.Open();

                    CONTRACTID = "" + Session["SCONTRACTID"];
                    string SVENDORID = "" + Session["SVENDORID"];
                    string SREQ_ID = "";
                    if (!string.IsNullOrEmpty(REQ_ID))
                    {
                        SREQ_ID = REQ_ID;
                    }
                    else
                    {
                        SREQ_ID = SystemFunction.AddToTREQ_DATACHANGE(CONTRACTID, "C", Session["UserID"] + "", "0", txtContractNo.Text, SVENDORID, "Y");
                    }

                    AddToTemp(CONTRACTID, SREQ_ID);

                    #region รายละเอียดสัญญา
                    //เก็บประวัติ
                    //                    strSql = @"INSERT INTO TCONTRACT_HIS(SCONTRACTID,NITEM,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,SDETAIL,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,COIL,CGAS,SCONT_PLNT,RENEWFROM) 
                    //                        (SELECT SCONTRACTID,(SELECT NVL(MAX(NITEM),0)+1 FROM TCONTRACT_HIS WHERE SCONTRACTID=:SCONTRACTID) NITEM,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,SDETAIL,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,COIL,CGAS,SCONT_PLNT,RENEWFROM FROM TCONTRACT_TEMP WHERE SCONTRACTID=:SCONTRACTID)";

                    //                    using (OracleCommand com = new OracleCommand(strSql, con))
                    //                    {
                    //                        com.Parameters.Clear();
                    //                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                    //                        com.ExecuteNonQuery();
                    //                    }

                    strSql = @"UPDATE TCONTRACT_TEMP SET SCONTRACTTYPEID=:SCONTRACTTYPEID,SCONTRACTNO=:SCONTRACTNO,SVENDORID=:SVENDORID,DBEGIN=:DBEGIN,DEND=:DEND,NTRUCK=:NTRUCK,SREMARK=:SREMARK,CACTIVE=:CACTIVE,DUPDATE=SYSDATE,SUPDATE=:SUPDATE,CGROUPCONTRACT=:CGROUPCONTRACT,CSPACIALCONTRAC=:CSPACIALCONTRAC,STERMINALID=:STERMINALID,SCONT_PLNT=:SCONT_PLNT WHERE SCONTRACTID=:SCONTRACTID AND  REQ_ID = '" + SREQ_ID + @"'";

                    using (OracleCommand com = new OracleCommand(strSql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                        com.Parameters.Add(":SCONTRACTTYPEID", OracleType.VarChar).Value = cmbContractType.Value;
                        com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = txtContractNo.Text;
                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cmbVendor.Value;
                        com.Parameters.Add(":DBEGIN", OracleType.DateTime).Value = Convert.ToDateTime(dteStart.Value);
                        if (dteEnd.Value == null) com.Parameters.Add(":DEND", OracleType.DateTime).Value = OracleDateTime.Null;
                        else com.Parameters.Add(":DEND", OracleType.DateTime).Value = Convert.ToDateTime(dteEnd.Value);
                        com.Parameters.Add(":NTRUCK", OracleType.Number).Value = ssContractTruck.Select("CSTANDBY='N'").Length;
                        com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = mmoRemark.Text + "";
                        com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblCACTIVE.Value + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
                        com.Parameters.Add(":CGROUPCONTRACT", OracleType.Char).Value = CommonFunction.Get_Value(conn, @"SELECt CGROUP FROM TCONTRACTTYPE WHERE SCONTRACTTYPEID='" + cmbContractType.Value + "'") + "";
                        com.Parameters.Add(":CSPACIALCONTRAC", OracleType.Char).Value = cblProcurement.Value + "";
                        com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cboMainPlant.Value + "";
                        foreach (DataRow dr in ssContractPlant.Rows) { PLNT += "," + dr["STERMINALID"]; }
                        com.Parameters.Add(":SCONT_PLNT", OracleType.VarChar).Value = PLNT + "";
                        com.ExecuteNonQuery();
                    }
                    #endregion

                    #region หลักประกันสัญญา

                    foreach (DataRow dr in ssGuarantee.Rows)
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            sGenID = CommonFunction.Gen_ID(con, TCONTRACT_GUARANTEES);

                            strSql = @"INSERT INTO TCONTRACT_GUARANTEES_TEMP(NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID,REQ_ID) Values
                                (:NCGID,:NCONTRACTID,:SBOOKNO,:SGUARANTEETYPE,:NAMOUNT,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SREMARK,:GUARANTEESTYPE_ID,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NCGID", OracleType.Number).Value = sGenID;
                                com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = CONTRACTID;
                                com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
                                com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
                                com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
                                com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                com.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            //กรณีที่มีการเปลี่ยนแปลงให้เก็บประวัติไว้ใน TCONTRACT_GUARANTEES_HIS ก่อน แล้วค่อยอัพเดทข้อมูลที่มีการเปลี่ยนแปลง
                            if (dr["CCHANGE"] + "" == "1" && dr["CDEL"] + "" == "0")
                            {
                                //Step 1
                                //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GUARANTEES_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

                                //                                strSql = @"INSERT INTO TCONTRACT_GUARANTEES_HIS(HISTORYID,DREJECT,SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) 
                                //                                            (SELECT :HISTORYID,SYSDATE,:SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID FROM TCONTRACT_GUARANTEES_TEMP WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID)";

                                //                                using (OracleCommand com = new OracleCommand(strSql, con))
                                //                                {
                                //                                    com.Parameters.Clear();
                                //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                                //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
                                //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
                                //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                //                                    com.ExecuteNonQuery();
                                //                                }

                                //Step 2
                                strSql = @"UPDATE TCONTRACT_GUARANTEES_TEMP SET SBOOKNO=:SBOOKNO,SGUARANTEETYPE=:SGUARANTEETYPE,NAMOUNT=:NAMOUNT,DUPDATE=SYSDATE,SUPDATE=:SUPDATE,SREMARK=:SREMARK,GUARANTEESTYPE_ID=:GUARANTEESTYPE_ID WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID AND REQ_ID = '" + SREQ_ID + "'";

                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
                                    com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
                                    com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
                                    com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
                                    com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
                                    com.ExecuteNonQuery();
                                }
                            }

                            //กรณีที่มีการลบข้อมูลทิ้งให้เก็บประวัติไว้ใน TCONTRACT_GUARANTEES_HIS ก่อน แล้วลบรายการใน TCONTRACT_GUARANTEES ทิ้ง
                            if (dr["CDEL"] + "" == "1")
                            {
                                //Step 1
                                //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GUARANTEES_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

                                //                                strSql = @"INSERT INTO TCONTRACT_GUARANTEES_HIS(HISTORYID,DREJECT,SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) 
                                //                                            (SELECT :HISTORYID,SYSDATE,:SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID FROM TCONTRACT_GUARANTEES_TEMP WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID)";

                                //                                using (OracleCommand com = new OracleCommand(strSql, con))
                                //                                {
                                //                                    com.Parameters.Clear();
                                //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                                //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
                                //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
                                //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                //                                    com.ExecuteNonQuery();
                                //                                }

                                //Step 2
                                strSql = @"DELETE FROM TCONTRACT_GUARANTEES_TEMP WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID AND REQ_ID = '" + SREQ_ID + "'";

                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
                                    com.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                    #endregion

                    #region เอกสารสำคัญของสัญญา

                    foreach (DataRow dr in ssContractDoc.Rows)
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            sGenID = CommonFunction.Gen_ID(con, TCONTRACT_DOC);

                            strSql = @"INSERT INTO TCONTRACT_DOC_TEMP(SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH,REQ_ID) Values
                                        (:SCONTRACTID,:SDOCID,:SDOCVERSION,:SDOCTYPE,:SFILENAME,:SSYSFILENAME,:SDESCRIPTION,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SPATH,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = sGenID;
                                com.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = "1";
                                com.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "" + dr["SDOCTYPE"];
                                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + dr["SFILENAME"];
                                com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + dr["SSYSFILENAME"];
                                com.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "" + dr["SDESCRIPTION"];
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SPATH", OracleType.VarChar).Value = "" + dr["SPATH"];
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;

                                com.ExecuteNonQuery();
                            }
                        }
                        else if (dr["CDEL"] + "" == "1")//กรณีที่มีการลบรายการทิ้งให้เก็บประวัติก่อน แล้วลบรายการเก่าทิ้ง
                        {
                            //Step 1
                            //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_DOC_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

                            //                            strSql = @"INSERT INTO TCONTRACT_DOC_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH)
                            //                                        (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH FROM TCONTRACT_DOC_TEMP WHERE SCONTRACTID=:SCONTRACTID AND SDOCID=:SDOCID)";

                            //                            using (OracleCommand com = new OracleCommand(strSql, con))
                            //                            {
                            //                                com.Parameters.Clear();
                            //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                            //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["SCONTRACTID"];
                            //                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = "" + dr["SDOCID"];
                            //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                            //                                com.ExecuteNonQuery();
                            //                            }

                            //Step 2
                            strSql = @"DELETE FROM TCONTRACT_DOC_TEMP WHERE SCONTRACTID=:SCONTRACTID AND SDOCID=:SDOCID AND REQ_ID = '" + SREQ_ID + "'";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["SCONTRACTID"];
                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = "" + dr["SDOCID"];
                                com.ExecuteNonQuery();
                            }

                        }
                    }
                    #endregion

                    #region ขอบเขตการขนส่ง

                    //คลังต้นทางหลัก
                    string mainplant = "";
                    mainplant = CommonFunction.Get_Value(conn, "SELECT STERMINALID FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID='" + CONTRACTID + "' AND CMAIN_PLANT='1'") + "";
                    if ((cboMainPlant.Value + "" != "" || mainplant + "" != "") && cboMainPlant.Value + "" != mainplant + "")
                    {
                        if (mainplant + "" != "")
                        {
                            //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_PLANT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

                            //                            strSql = @"INSERT INTO TCONTRACT_PLANT_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) 
                            //                                    (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND CMAIN_PLANT='1')";

                            //                            using (OracleCommand com = new OracleCommand(strSql, con))
                            //                            {
                            //                                com.Parameters.Clear();
                            //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                            //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                            //                                com.ExecuteNonQuery();
                            //                            }

                            strSql = @"DELETE FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND CMAIN_PLANT='1' AND REQ_ID = '" + SREQ_ID + "'";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.ExecuteNonQuery();
                            }
                        }

                        if (cboMainPlant.Value + "" != "")
                        {
                            strSql = @"INSERT INTO TCONTRACT_PLANT_TEMP(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT,REQ_ID) Values
                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cboMainPlant.Value + "";
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "1";
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                com.ExecuteNonQuery();
                            }
                        }
                    }

                    foreach (DataRow dr in ssContractPlant.Rows)//คลังต้นทางอื่นๆ
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            strSql = @"INSERT INTO TCONTRACT_PLANT_TEMP(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT,REQ_ID) Values
                                        (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;

                                com.ExecuteNonQuery();
                            }
                        }
                        else if (dr["CDEL"] + "" == "1")
                        {
                            //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_PLANT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

                            //                            strSql = @"INSERT INTO TCONTRACT_PLANT_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) 
                            //                                        (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND STERMINALID=:STERMINALID AND CMAIN_PLANT=:CMAIN_PLANT)";

                            //                            using (OracleCommand com = new OracleCommand(strSql, con))
                            //                            {
                            //                                com.Parameters.Clear();
                            //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                            //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                            //                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
                            //                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
                            //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                            //                                com.ExecuteNonQuery();
                            //                            }

                            strSql = @"DELETE FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND STERMINALID=:STERMINALID AND CMAIN_PLANT=:CMAIN_PLANT AND REQ_ID = '" + SREQ_ID + "'";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
                                com.ExecuteNonQuery();
                            }
                        }
                    }
                    #endregion

                    #region กลุ่มผลิตภัณฑ์

                    foreach (DataRow dr in ssGProduct.Rows)
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            strSql = @"INSERT INTO TCONTRACT_GPRODUCT_TEMP(SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,REQ_ID) Values
                                (:SCONTRACTID,:SPRODUCTTYPEID,:SPRODUCTTYPENAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                                com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                                com.Parameters.Add(":REQ_ID", OracleType.Char).Value = SREQ_ID;

                                com.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            //กรณีที่มีการเปลี่ยนแปลงให้เก็บประวัติไว้ใน TCONTRACT_GPRODUCT_HIS ก่อน แล้วค่อยอัพเดทข้อมูลที่มีการเปลี่ยนแปลง
                            if (dr["CCHANGE"] + "" == "1" && dr["CDEL"] + "" == "0")
                            {
                                //Step 1
                                //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GPRODUCT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");
                                //                                strSql = @"INSERT INTO TCONTRACT_GPRODUCT_HIS(HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DREJECT,SREJECT) 
                                //                                            (SELECT :HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,SYSDATE,:SREJECT FROM TCONTRACT_GPRODUCT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID)";

                                //                                using (OracleCommand com = new OracleCommand(strSql, con))
                                //                                {
                                //                                    com.Parameters.Clear();
                                //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                                //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
                                //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                                //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                //                                    com.ExecuteNonQuery();
                                //                                }

                                //Step 2
                                strSql = @"UPDATE TCONTRACT_GPRODUCT_TEMP SET SPRODUCTTYPEID=:SPRODUCTTYPEID,SPRODUCTTYPENAME=:SPRODUCTTYPENAME,DUPDATE=SYSDATE,SUPDATE=:SUPDATE
                                ,CACTIVE=:CACTIVE WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID AND REQ_ID = '" + SREQ_ID + "'";

                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                                    com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
                                    com.ExecuteNonQuery();
                                }
                            }

                            //กรณีที่มีการลบข้อมูลทิ้งให้เก็บประวัติไว้ใน TCONTRACT_GPRODUCT_HIS ก่อน แล้วลบรายการใน TCONTRACT_GUARANTEES ทิ้ง
                            if (dr["CDEL"] + "" == "1")
                            {
                                //Step 1
                                //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GPRODUCT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");
                                //                                strSql = @"INSERT INTO TCONTRACT_GPRODUCT_HIS(HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DREJECT,SREJECT) 
                                //                                            (SELECT :HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,SYSDATE,:SREJECT FROM TCONTRACT_GPRODUCT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID)";

                                //                                using (OracleCommand com = new OracleCommand(strSql, con))
                                //                                {
                                //                                    com.Parameters.Clear();
                                //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
                                //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
                                //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                                //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                //                                    com.ExecuteNonQuery();
                                //                                }

                                //Step 2
                                strSql = @"DELETE FROM TCONTRACT_GPRODUCT_TEMP WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID AND REQ_ID = '" + SREQ_ID + "'";

                                using (OracleCommand com = new OracleCommand(strSql, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
                                    com.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                    #endregion

                    #region รายชื่อรถในสัญญา

                    foreach (DataRow dr in ssContractTruck.Rows)
                    {
                        if (dr["CNEW"] + "" == "1")
                        {
                            strSql = @"INSERT INTO TCONTRACT_TRUCK_TEMP(SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,REQ_ID) Values
                                        (:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:REQ_ID)";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
                                com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + dr["CSTANDBY"];
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;
                                com.ExecuteNonQuery();
                            }
                        }
                        else if (dr["CDEL"] + "" == "1")//กรณีที่มีการลบรายการทิ้งให้เก็บประวัติก่อน แล้วลบรายการเก่าทิ้ง
                        {
                            //Step 1
                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY_TEMP ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

                            strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY_TEMP(HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT) Values (:HISTORY_ID,SYSDATE,:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,:DCREATE,:SCREATE,:DUPDATE,:SUPDATE,SYSDATE,:SREJECT,:REQ_ID)";

                            DataTable dtCTRUCK = CommonFunction.Get_Data(conn, @"SELECT * FROM TCONTRACT_TRUCK_TEMP WHERE SCONTRACTID='" + CONTRACTID + "' AND STRUCKID='" + "" + dr["STRUCKID"] + "' AND '.'||STRAILERID='" + "." + dr["STRAILERID"] + "'");

                            foreach (DataRow r in dtCTRUCK.Rows)
                            {
                                //using (OracleCommand com = new OracleCommand(strSql, con))
                                //{
                                //    com.Parameters.Clear();
                                //    com.Parameters.Add(":HISTORY_ID", OracleType.VarChar).Value = sGenID;
                                //    com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + r["SCONTRACTID"];
                                //    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + r["STRUCKID"];
                                //    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + r["STRAILERID"];
                                //    com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + r["CSTANDBY"];
                                //    com.Parameters.Add(":DCREATE", OracleType.DateTime).Value = Convert.ToDateTime("" + r["DCREATE"]);
                                //    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + r["SCREATE"];
                                //    com.Parameters.Add(":DUPDATE", OracleType.DateTime).Value = Convert.ToDateTime("" + r["DUPDATE"]);
                                //    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + r["SUPDATE"];
                                //    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
                                //    com.Parameters.Add(":REQ_ID", OracleType.VarChar).Value = SREQ_ID;

                                //    com.ExecuteNonQuery();
                                //}
                                break;
                            }

                            //Step 2
                            strSql = @"DELETE FROM TCONTRACT_TRUCK_TEMP WHERE SCONTRACTID=:SCONTRACTID AND STRUCKID=:STRUCKID AND '.'||STRAILERID='.'||:STRAILERID AND REQ_ID = '" + SREQ_ID + "'";

                            using (OracleCommand com = new OracleCommand(strSql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
                                com.ExecuteNonQuery();
                            }

                            //บันทึกเปลี่ยนแปลงข้อมูลสัญญาใน TTRUCK
                            //strSql = @"UPDATE TTRUCK SET SCONTRACTID=:SCONTRACTID,SCONTRACTTYPE=:SCONTRACTTYPE WHERE STRUCKID IN (:STRUCKID,:STRAILERID)";

                            //using (OracleCommand com = new OracleCommand(strSql, con))
                            //{
                            //    com.Parameters.Clear();
                            //    com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
                            //    com.Parameters.Add(":SCONTRACTTYPE", OracleType.VarChar).Value = "" + cmbContractType.Value;
                            //    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
                            //    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
                            //    com.ExecuteNonQuery();
                            //}
                        }
                    }
                    #endregion

                    string Edit = @"UPDATE TREQ_DATACHANGE
                        SET    STATUS     = '0'
                        WHERE  REQ_ID      = '" + SREQ_ID + @"'";
                    AddTODB(Edit);
                }
                break;
        }
        ClearSession();

       

        if (SendMailToVendorRk())
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){ window.location='Vendor_Detail.aspx'; });");
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้',function(){window.location='Vendor_Detail.aspx';});");
        }
    }
    void ClearSession()
    {
        string[] sSession = new string[] { "SMODE", "SCONTRACTID", "SVENDORID", "ssContractDoc", "ssGuarantee", "ssContractPlant", "ssGProduct", "ssContractTruck" };
        foreach (string arr in sSession)
            Session.Remove(arr);
    }

    protected DataTable PrepareDataTable(string ss_name, string IsDel, string contractid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] != null)
        {
            dtData = (DataTable)Session[ss_name];
            if (IsDel == "0")
            {
                foreach (DataRow drDel in dtData.Select("NCONTRACTID='" + contractid + "'"))
                {
                    int idx = dtData.Rows.IndexOf(drDel);
                    dtData.Rows[idx].Delete();
                }
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }
    private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
        FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
        string ResultFileName = ServerMapPath + File.Name;
        string sPath = Path.GetDirectoryName(ResultFileName)
                , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                , sFileType = Path.GetExtension(ResultFileName);

        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(ServerMapPath))
            {
                Directory.CreateDirectory(ServerMapPath);
            }
            #endregion
            string fileName = (GenFileName + "" + sFileType.Trim());
            ful.SaveAs(ServerMapPath + fileName);
            //LogUser("1", "I", "อัพโหลดไฟล์(" + uploadmode + ") หน้ายืนยันรถตามสัญญา", "");///แก้ไขให้ระบบเก็บLoglว่าอัพโหลดExcel||Picture

            return fileName + "$" + sFileName.Replace("$", "") + sFileType;
        }
        else
            return "$";
    }

    void AddToTemp(string SCONTRACTID, string REQ_ID)
    {
        string QUERY_TCONTRACT = @"INSERT INTO TCONTRACT_TEMP (
   SCONTRACTID, SCONTRACTTYPEID, SCONTRACTNO, 
   SVENDORID, DBEGIN, DEND, 
   SDETAIL, NTRUCK, SREMARK, 
   CACTIVE, DCREATE, SCREATE, 
   DUPDATE, SUPDATE, CGROUPCONTRACT, 
   CSPACIALCONTRAC, STERMINALID, COIL, 
   CGAS, SCONT_PLNT, RENEWFROM,REQ_ID) 
SELECT SCONTRACTID ,
  SCONTRACTTYPEID ,
  SCONTRACTNO ,
  SVENDORID ,
  DBEGIN ,
  DEND ,
  SDETAIL ,
  NTRUCK ,
  SREMARK ,
  CACTIVE ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  CGROUPCONTRACT ,
  CSPACIALCONTRAC ,
  STERMINALID ,
  COIL ,
  CGAS ,
  SCONT_PLNT ,
  RENEWFROM , '" + REQ_ID + @"' as REQ_ID FROM TCONTRACT
  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"'
  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_TCONTRACT);

        string QUERY_DOC = @"INSERT INTO TCONTRACT_DOC_TEMP (
   SCONTRACTID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH, NVERSION ,REQ_ID) 
SELECT SCONTRACTID ,
  SDOCID ,
  SDOCVERSION ,
  SDOCTYPE ,
  SFILENAME ,
  SSYSFILENAME ,
  SDESCRIPTION ,
  CACTIVE ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  DEXPIRE ,
  SPATH ,
  NVERSION , '" + REQ_ID + @"' as REQ_ID FROM TCONTRACT_DOC
  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_DOC_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND SDOCID = TCONTRACT_DOC.SDOCID AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_DOC);

        string QUERY_PRODUCT = @"INSERT INTO TCONTRACT_GPRODUCT_TEMP (
   SCONTRACTID, SPRODUCTTYPEID, SPRODUCTTYPENAME, 
   DCREATE, SCREATE, DUPDATE, 
   SUPDATE, CACTIVE,REQ_ID) 
SELECT  SCONTRACTID ,
  SPRODUCTTYPEID ,
  SPRODUCTTYPENAME ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  CACTIVE , '" + REQ_ID + @"' as REQ_ID FROM TCONTRACT_GPRODUCT
  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_GPRODUCT_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND SPRODUCTTYPEID = TCONTRACT_GPRODUCT.SPRODUCTTYPEID AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_PRODUCT);

        string QUERY_GARUNTEE = @"INSERT INTO TCONTRACT_GUARANTEES_TEMP (
   NCGID, NCONTRACTID, SBOOKNO, 
   SGUARANTEETYPE, NAMOUNT, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   SREMARK, GUARANTEESTYPE_ID,REQ_ID) 
SELECT  NCGID ,
  NCONTRACTID ,
  SBOOKNO ,
  SGUARANTEETYPE ,
  NAMOUNT ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  SREMARK ,
  GUARANTEESTYPE_ID , '" + REQ_ID + @"' as REQ_ID  FROM TCONTRACT_GUARANTEES
  WHERE NCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
  AND NOT EXISTS (SELECT NCONTRACTID FROM TCONTRACT_GUARANTEES_TEMP WHERE NCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND NCGID = TCONTRACT_GUARANTEES.NCGID AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_GARUNTEE);

        string QUERY_PLANT = @"INSERT INTO TCONTRACT_PLANT_TEMP (
   SCONTRACTID, STERMINALID, CACTIVE, 
   DCREATE, SCREATE, DUPDATE, 
   SUPDATE, CMAIN_PLANT,REQ_ID) 
SELECT SCONTRACTID ,
  STERMINALID ,
  CACTIVE ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  CMAIN_PLANT , '" + REQ_ID + @"' as REQ_ID  FROM TCONTRACT_PLANT
  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_PLANT_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND STERMINALID = TCONTRACT_PLANT.STERMINALID AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_PLANT);

        //        string QUERY_TERMINAL = @"INSERT INTO TCONTRACT_TERMINAL_TEMP (
        //   SPLACEID, SCONTRACTID, STERMINALID, 
        //   SREGION, DCREATE, SCREATE, 
        //   DUPDATE, SUPDATE,REQ_ID) 
        //SELECT  SPLACEID ,
        //  SCONTRACTID ,
        //  STERMINALID ,
        //  SREGION ,
        //  DCREATE ,
        //  SCREATE ,
        //  DUPDATE ,
        //  SUPDATE , '" + REQ_ID + @"' as REQ_ID  FROM TCONTRACT_TERMINAL
        //  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
        //  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_TERMINAL_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND STERMINALID = TCONTRACT_TERMINAL.STERMINALID AND REQ_ID = '" + REQ_ID + "')";

        //        AddTODB(QUERY_TERMINAL);

        string QUERY_TRUCK = @"INSERT INTO TCONTRACT_TRUCK_TEMP (
   SCONTRACTID, STRUCKID, STRAILERID, 
   CSTANDBY, CREJECT, DSTART, 
   DEND, DCREATE, SCREATE, 
   DUPDATE, SUPDATE, REF_SCONTRACTID, 
   REF_PREVIOUSCONTRACT, SFILENAME, SSYSFILENAME, 
   SREMARK,REQ_ID) 
SELECT  SCONTRACTID ,
  STRUCKID ,
  STRAILERID ,
  CSTANDBY ,
  CREJECT ,
  DSTART ,
  DEND ,
  DCREATE ,
  SCREATE ,
  DUPDATE ,
  SUPDATE ,
  REF_SCONTRACTID ,
  REF_PREVIOUSCONTRACT ,
  SFILENAME ,
  SSYSFILENAME ,
  SREMARK, '" + REQ_ID + @"' as REQ_ID  FROM TCONTRACT_TRUCK
  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_TRUCK_TEMP WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND STRUCKID = TCONTRACT_TRUCK.STRUCKID AND REQ_ID = '" + REQ_ID + "')";

        AddTODB(QUERY_TRUCK);

        //        string QUERY_HISTORY = @"INSERT INTO TCONTRACT_TRUCK_HISTORY_TEMP (
        //   HISTORY_ID, DHISTORY, SCONTRACTID, 
        //   STRUCKID, STRAILERID, CSTANDBY, 
        //   CREJECT, DSTART, DEND, 
        //   DCREATE, SCREATE, DUPDATE, 
        //   SUPDATE, DREJECT, SREJECT, 
        //   REF_SCONTRACTID, REF_PREVIOUSCONTRACT, SFILENAME, 
        //   SSYSFILENAME, SREMARK) 
        //SELECT  HISTORY_ID ,
        //  DHISTORY ,
        //  SCONTRACTID ,
        //  STRUCKID ,
        //  STRAILERID ,
        //  CSTANDBY ,
        //  CREJECT ,
        //  DSTART ,
        //  DEND ,
        //  DCREATE ,
        //  SCREATE ,
        //  DUPDATE ,
        //  SUPDATE ,
        //  DREJECT ,
        //  SREJECT ,
        //  REF_SCONTRACTID ,
        //  REF_PREVIOUSCONTRACT ,
        //  SFILENAME ,
        //  SSYSFILENAME ,
        //  SREMARK   FROM TCONTRACT_TRUCK_HISTORY
        //  WHERE SCONTRACTID = '" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' 
        //  AND NOT EXISTS (SELECT SCONTRACTID FROM TCONTRACT_TRUCK_HISTORY_TEMP 
        //  WHERE SCONTRACTID='" + CommonFunction.ReplaceInjection(SCONTRACTID) + @"' AND HISTORY_ID = TCONTRACT_TRUCK_HISTORY.HISTORY_ID AND STRUCKID = TCONTRACT_TRUCK_HISTORY.STRUCKID)";

        //        AddTODB(QUERY_HISTORY);
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private bool SendMailToVendorRk()
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ขอเปลี่ยนแปลงข้อมูลสัญญา " + txtContractNo.Text;
        string VENDOR_NAME = "";
        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {

            DataTable dt_MAIL = CommonFunction.Get_Data(conn, @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('61')");
            for (int i = 0; i < dt_MAIL.Rows.Count; i++)
            {
                _to += ";" + dt_MAIL.Rows[i]["SEMAIL"] + "";
            }

            _to = _to.Remove(0, 1);
        }
        string SVENDOR_ID = SystemFunction.GET_VENDORID(Session["UserID"] + "");
        //หาชื่อบริษัท
        DataTable dt_NAME = CommonFunction.Get_Data(conn, "SELECT SABBREVIATION FROM TVENDOR WHERE SVENDORID = '" + SVENDOR_ID + "' ");
        if (dt_NAME.Rows.Count > 0)
        {
            VENDOR_NAME = dt_NAME.Rows[0]["SABBREVIATION"] + "";
        }

        #region html

        sHTML = @" <table width='600px' cellpadding='3' cellspacing='1' border='0' >
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ขอเปลี่ยนแปลงข้อมูลสัญญา
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน รข.</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   ทางบริษัท " + VENDOR_NAME + @" ได้ขอเปลี่ยนแปลงข้อมูลสัญญา """ + txtContractNo.Text + @""" เพื่อให้ข้อมูลเป็นปัจจุบันและถูกต้องยิ่งขึ้น</td>
            </tr>
              <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>จึงเรียนมาเพื่อโปรดทราบ
                    และดำเนินการต่อไป</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>" + VENDOR_NAME + @"
                </td>
            </tr>
            </table>";

        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(conn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }
}