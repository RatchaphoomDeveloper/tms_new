﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="contract_add.aspx.cs" 
    Inherits="contract_add" StylesheetTheme="Aqua" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPanel" Assembly="DevExpress.Web.v11.2" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style>
         .dxeTextBoxSys.form-control {
    display: table!important;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <script type="text/javascript">
        function ModifyTypeChanged(s, e) {
            if (s.GetValue() == '0')
            {
                //txtContractNo.SetValue(txtCNO.GetValue());
                $('#divContractNoNew').addClass("hidden");
                txtContract.SetEnabled(true);
                txtContractNew.SetEnabled(false);
            }
            else
            {
                $('#divContractNoNew').removeClass("hidden");
                txtContract.SetEnabled(false);
                txtContractNew.SetEnabled(true);
                //txtCNO.SetValue(txtContractNo.GetValue());
                //txtContractNo.SetValue('');
                //txtContractNo.Focus();
            }
        }
    </script>
	<script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            </ContentTemplate>
                                        </asp:UpdatePanel>
    <dx:ASPxCallbackPanel ID="xcpn"  runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn" CausesValidation="False"
        HideContentOnCallback="False">
        
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;inEndRequestHandler();}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server">
                <dx:ASPxPageControl ID="pageControl" ClientInstanceName="pageControl" runat="server"
                    EnableCallBacks="false" Width="100%" ActiveTabIndex="0">
                    <TabPages>
                        <dx:TabPage Text="เลขที่สัญญา" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ctctrl1" runat="server">
                                    <div id="ContractAdd">
                    <dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="รายละเอียดสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="pctInformation" runat="server">
                                <div class="form-horizontal">
                                <div class="row form-group">
                                            <div class="col-md-4 text-right padding-top9">

                                            </div>
                                            <div class="col-md-5 ">
                                                
                                            </div>
                                    <div class="col-md-3 text-right">
                                        <dx:ASPxButton ID="btnEditMode" runat="server" OnClick="btnEditMode_Click" Text="เปลี่ยนเป็นโหมดแก้ไข">
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnViewMode" runat="server" OnClick="btnViewMode_Click" Text="เปลี่ยนเป็นโหมดเรียกดู">
                                                </dx:ASPxButton>
                                        </div>
                                        </div>
                                <div id="trModify_type" runat="Server" class="row form-group">
                                    <label  class="col-md-4 control-label">ประเภทเปลี่ยนแปลงข้อมูลสัญญา:</label>
                                  
                                    <div class="col-md-6 ">
                                              <dx:ASPxComboBox ID="cboModify_type" runat="Server" ClientInstanceName="cboModify_type" CssClass="form-control" Width="100%">
                                                    <ClientSideEvents SelectedIndexChanged="ModifyTypeChanged" />
                                                    <Items>
                                                        <dx:ListEditItem Value="0" Text="แก้ไข/เพิ่มเติมสัญญา" Selected="true" />
                                                        <dx:ListEditItem Value="1" Text="ต่ออายุสัญญา" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการเปลี่ยนแปลงข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>  
                                        <table style="display: none;">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtRenewFrom" runat="Server" ClientInstanceName="txtRenewFrom">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtCNO" runat="Server" ClientInstanceName="txtCNO">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">ประเภทการจัดจ้าง:</label>
                                    
                                    <div class="col-md-6 ">
                                              <dx:ASPxRadioButtonList ID="cblProcurement" runat="server" ClientInstanceName="cblProcurement" RepeatColumns="2" AutoPostBack="true" OnSelectedIndexChanged="cblProcurement_SelectedIndexChanged">
                                                    <Items>
                                                        <dx:ListEditItem Text="จัดจ้างโดยสัญญา" Value="N" Selected="true" />
                                                        <dx:ListEditItem Text="จัดจ้างโดยวิธีพิเศษ" Value="Y" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการจัดจ้าง" />
                                                    </ValidationSettings>
                                                    <Border BorderWidth="0px" />
                                                </dx:ASPxRadioButtonList>  
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">เลขที่สัญญา:</label>
                                   
                                    <div class="col-md-6 ">
                                        <dx:ASPxTextBox runat="server" ID="txtContract" ClientInstanceName="txtContract" Width="100%" CssClass="form-control">
                                            <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุเลขที่สัญญา" />
                                                    </ValidationSettings>
                                        </dx:ASPxTextBox>
                                          
                                    </div>
                                </div>
                                <div id="divContractNoNew" class="row form-group hidden">
                                    <label  class="col-md-4 control-label">เลขที่สัญญา(ใหม่):</label>
                                    <div class="col-md-6 ">
                                        <dx:ASPxTextBox runat="server" ID="txtContractNew" ClientEnabled="false" ClientInstanceName="txtContractNew" Width="100%" CssClass="form-control">
                                            <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุเลขที่สัญญา(ใหม่)" />
                                                    </ValidationSettings>
                                        </dx:ASPxTextBox>
                                            
                                    </div>
                                </div>
                                    
                                <div class="row form-group">
        <label  class="col-md-4 control-label">กลุ่มงาน:</label>
        <div class="col-md-6">
            <dx:ASPxComboBox runat="server" ID="ddlWordGroup" EnableCallbackMode="true" CssClass="form-control" Width="100%" >
                <ClientSideEvents SelectedIndexChanged="function(s,e){ xcpn.PerformCallback('ddlWordGroupSelectedIndexChanged;'); }" />
                <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุกลุ่มงาน" />
                                                    </ValidationSettings>
            </dx:ASPxComboBox>
            
        </div>
        <div class="col-md-4"></div>
    </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">กลุ่มที่:</label>
                                    <div class="col-md-6">
                                        <dx:ASPxComboBox runat="server" ID="ddlGroups"  CssClass="form-control" Width="100%" >
                                            <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุกลุ่มที่" />
                                                    </ValidationSettings>
            </dx:ASPxComboBox>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
            
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">บริษัทผู้ขนส่ง:</label>
                                    <div class="col-md-6"><dx:ASPxComboBox ID="cmbVendor" runat="server" CssClass="form-control" ClientInstanceName="cmbVendor" Width="100%"
                                                    SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID">
                                                    <Columns>
                                                        <dx:ListBoxColumn Width="20%" Caption="รหัสบริษัท" FieldName="SVENDORID" />
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" />
                                                    </Columns>
                                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุบริษัทผู้ขนส่ง" />
                                                    </ValidationSettings>
                                        <ClientSideEvents SelectedIndexChanged="function(s,e){  xcpn.PerformCallback('VendorChange'); }" />

                                                </dx:ASPxComboBox></div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">Mode การขนส่ง:</label>
                                    <div class="col-md-6 ">
                                            <dx:ASPxComboBox ID="cmbTransType" runat="server"  ClientInstanceName="cmbTransType" 
                                                    TextFormatString="{1}" ValueField="STRANTYPEID"
                                                     Width="100%" CssClass="form-control">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัส" FieldName="STRANTYPEID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อประเภทการขนส่ง" FieldName="STRANTYPENAME" Width="360px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทการขนส่ง" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                    
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">ประเภทสัญญา:</label>
                                    <div class="col-md-6 ">
                                                                                                <dx:ASPxComboBox ID="cmbContractType" runat="server" ClientInstanceName="cmbContractType" CssClass="form-control" TextFormatString="{1}" ValueField="SCONTRACTTYPEID"
                                                     Width="100%">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัส" FieldName="SCONTRACTTYPEID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อประเภทสัญญา" FieldName="SCONTRACTTYPENAME" Width="360px" />
                                                        <dx:ListBoxColumn Caption="กลุ่มสัญญา" FieldName="CGROUP" Width="100px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ ประเภทสัญญา" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
  
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">ระยะเวลาสัญญา:</label>
                                    <div class="col-md-6 ">

                                               <asp:TextBox runat="server" ID="dteStart" CssClass="datepicker" ></asp:TextBox> 
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">ถึง:</label>
                                    <div class="col-md-6 ">
                                                <asp:TextBox runat="server" ID="dteEnd" CssClass="datepicker" />
                                    </div>
                                </div>
                                    <div class="row form-group">
                                    <label class="col-md-4 control-label">จำนวนรถในสัญญา:</label>
                                    <div class="col-md-6 ">
                                        <dx:ASPxTextBox runat="server" ID="txtNtruck"  ClientInstanceName="txtNtruck" Width="100%" CssClass="form-control number" MaxLength="2">
                                            <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุจำนวนรถในสัญญา" />
                                                    </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                    <div class="row form-group">
                                    <label class="col-md-4 control-label">จำนวนรถในสำรอง:</label>
                                    <div class="col-md-6 ">
                                        <dx:ASPxTextBox runat="server" ID="txtReserve"  ClientInstanceName="txtReserve" Width="100%" CssClass="form-control number" MaxLength="2">
                                            <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุจำนวนรถในสำรอง" />
                                                    </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </div>
                                </div>
                                    
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">หมายเหตุ:</label>
                                    <div class="col-md-6 ">
                                                <dx:ASPxMemo ID="mmoRemark" runat="Server" ClientInstanceName="mmoRemark"  Width="100%" Rows="3">
                                                </dx:ASPxMemo>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label  class="col-md-4 control-label">สถานะสัญญา:</label>
                                    <div class="col-md-6 ">
                                                <dx:ASPxRadioButtonList ID="rblCACTIVE" runat="server" ClientInstanceName="rblCACTIVE" RepeatColumns="2">
                                                    <Items>
                                                        <dx:ListEditItem Text="ใช้งาน" Value="Y" Selected="true" />
                                                        <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="N" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ สถานะ" />
                                                    </ValidationSettings>
                                                    <Border BorderWidth="0px" />
                                                </dx:ASPxRadioButtonList>
                                    </div>
                                </div>
                                </div>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="เอกสารสำคัญของสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent5" runat="server">
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:FileUpload ID="fileUpload" runat="server" />

                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-4 text-right padding-top9">
                                    </div>
                                    <div class="col-md-4  ">
                                        <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12  ">
                                        <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-hover" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                                            HorizontalAlign="Center" AutoGenerateColumns="false"
                                            CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH" OnRowDeleting="dgvUploadFile_RowDeleting"
                                            OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound">
                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                                        </asp:GridView>
                                    </div>
                                </div>
                                </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnGuarantee" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="หลักประกันสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="pctGuarantee" runat="server">
                                <table width="100%">
                                    <tbody id="tbdGuarantee" runat="server">
                                        <tr>
                                            <td style="">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <dx:ASPxGridView ID="gvwGuarantee" runat="server" ClientInstanceName="gvwGuarantee" AutoGenerateColumns="False"
                                                    KeyFieldName="NCGID" SkinID="_gvw">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SGUARANTEETYPE" Caption="ประเภท" Width="18%" VisibleIndex="1">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SBOOKNO" Caption="เลขที่เอกสาร" ShowInCustomizationForm="false"
                                                            VisibleIndex="2">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                            <FooterTemplate>
                                                                <dx:ASPxLabel ID="lblTotal" runat="Server" ClientInstanceName="lblTotal" Text="รวมมูลค่าหลักค้ำประกัน(บาท)">
                                                                </dx:ASPxLabel>
                                                            </FooterTemplate>
                                                            <FooterCellStyle HorizontalAlign="Center">
                                                            </FooterCellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NAMOUNT" Caption="มูลค่าหลักค้ำประกัน(บาท)" VisibleIndex="3" Width="9%">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Right" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditGuarantee" ClientInstanceName="btnEditGuarantee" runat="server" ToolTip="ปรับปรุง/แก้ไขรายการ"
                                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelGuarantee" ClientInstanceName="btnDelGuarantee" runat="server" ToolTip="ลบรายการ"
                                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('DELGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NCGID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SBOOKNO" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="GUARANTEESTYPE_ID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SGUARANTEETYPE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NAMOUNT" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                    </Columns>
                                                    <TotalSummary>
                                                        <dx:ASPxSummaryItem ShowInColumn="NAMOUNT" SummaryType="Sum" FieldName="NAMOUNT" DisplayFormat="N2" />
                                                    </TotalSummary>
                                                    <Templates>
                                                        <EditForm>
                                                            <div style="text-align: center;">
                                                                <table width="90%">
                                                                    <tr>
                                                                        <td style="width: 15%;" align="right">
                                                                            ประเภท : </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbSGUARANTEETYPE" ClientInstanceName="cmbSGUARANTEETYPE" runat="server" DataSourceID="sdsGUARANTEETYPE"
                                                                                TextField="GUARANTEESTYPE_NAME" ValueField="GUARANTEESTYPE_ID">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField ErrorText="กรุณาระบุประเภท" IsRequired="True" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsGUARANTEETYPE" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                                                CacheKeyDependency="ckdGUARANTEETYPE" SelectCommand="SELECT * FROM TGUARANTEESTYPE WHERE NVL( CACTIVE,'1')='1' ">
                                                                            </asp:SqlDataSource>
                                                                            <dx:ASPxTextBox ID="txtSGUARANTEETYPE" runat="server" ClientInstanceName="txtSGUARANTEETYPE" ClientVisible="false">
                                                                            </dx:ASPxTextBox>
                                                                            <dx:ASPxTextBox ID="txtNCGID" runat="server" ClientInstanceName="txtNCGID" ClientVisible="false">
                                                                            </dx:ASPxTextBox>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%;" align="right">
                                                                            เลขที่เอกสาร : </td>
                                                                        <td style="width: 25%" align="left">
                                                                            <dx:ASPxTextBox ID="txtSBOOKNO" runat="server" ClientInstanceName="txtSBOOKNO" Width="250px">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                        <td style="width: 20%; text-align: right;">
                                                                            มูลค่าหลักค้ำประกัน(บาท) : </td>
                                                                        <td style="width: 20%" align="left">
                                                                            <dx:ASPxTextBox ID="txtGuaranteeValue" runat="server" ClientInstanceName="txtGuaranteeValue" Width="150px"
                                                                                DisplayFormatString="#,##0.00" NullText="0.00">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addGuarantee">
                                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField ErrorText="กรุณาระบุ" IsRequired="true" />
                                                                                    <RegularExpression ErrorText="กรุณาระบุเป็นตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 15%; text-align: right; vertical-align: top;">
                                                                            ธนาคาร/ผู้ออก : </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbRemark" runat="Server" ClientInstanceName="cmbRemark" DataSourceID="sdsBank"
                                                                                TextField="BANKNAME" ValueField="BANKNAME">
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsBank" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                                                CacheKeyDependency="ckdBank" SelectCommand="SELECT * FROM TBANK WHERE NVL( CACTIVE,'1')='1' "></asp:SqlDataSource>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnSaveGuarantee" ClientInstanceName="btnSaveGuarantee" runat="server" CausesValidation="true"
                                                                                ValidationGroup="addGuarantee" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(ASPxClientEdit.ValidateGroup('addGuarantee')){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('SAVEGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }else{ return false;} }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server" SkinID="_cancel"
                                                                                CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowFooter="true" ShowStatusBar="Auto" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table id="tbladdgrt" runat="Server">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnNewGuarantee" ClientInstanceName="btnNewGuarantee" runat="server" ToolTip="เพิ่มหลักประกันอื่นๆ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGuarantee.InCallback()) gvwGuarantee.PerformCallback('NEWGUARANTEE;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มหลักประกันอื่นๆ </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnplant" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="ขอบเขตการขนส่ง">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent2" runat="server">
                                <table width="100%">
                                    <tbody id="Tbody3" runat="server">
                                        <tr>
                                            <td style="width: 15%;">
                                                คลังต้นทาง : </td>
                                            <td>
                                                <dx:ASPxComboBox ID="cboMainPlant" ClientInstanceName="cboMainPlant" runat="server" CallbackPageSize="30"
                                                    
                                                    SkinID="xcbbATC" TextFormatString="{0} : {1}" ValueField="STERMINALID" Width="400px">
                                                    <ClientSideEvents SelectedIndexChanged="function(s,e){ gvwTerminal.PerformCallback('CHECKPLANT;'); }" KeyDown="function(s, e) {
     //Delete = 46, Backspace = 8

if (e.htmlEvent.keyCode == 46 || e.htmlEvent.keyCode == 8)
                                                        {
s.SetSelectedIndex(-1);
                                                        gvwTerminal.PerformCallback('CHECKPLANT;');
                                                        }

}" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" Width="350px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="txtMainPlant" ClientInstanceName="txtMainPlant" runat="server" SkinID="txtValidate">
                                                    </dx:ASPxTextBox>
                                                </span></td>
                                            <td>
                                                <table style="display: none;">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnMainPlant" ClientInstanceName="btnMainPlant" runat="server" ToolTip="เพิ่มคลังต้นทางหลัก"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('MAINPLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มคลังต้นทางหลัก </td>
                                                    </tr>
                                                </table>
                                                <asp:SqlDataSource ID="sdsMainPlant" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                    CacheKeyDependency="ckdMainPlant" SelectCommand="SELECT * FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">
                                                คลังต้นทางอื่นๆ : </td>
                                            <td colspan="1">
                                                <dx:ASPxComboBox ID="cboPLANTOTHER" ClientInstanceName="cboPLANTOTHER" CallbackPageSize="30" runat="server" 
                                                    SkinID="xcbbATC" TextFormatString="{0} : {1}" ValueField="STERMINALID" Width="400px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" Width="50px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" Width="350px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ValidationGroup="plant" ErrorDisplayMode="ImageWithTooltip" RequiredField-IsRequired="true"
                                                        SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="กรุณาระบุ" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </td>
                                            <td>
                                                <table id="tbladdplant" runat="Server">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="btnNewTerminal" ClientInstanceName="btnNewTerminal" runat="server" ToolTip="คลังต้นทางอื่นๆ"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('plant')) return false; else{ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('NEWCONTRACTPLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); } }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle">
                                                            เพิ่มคลังต้นทาง </td>
                                                    </tr>
                                                </table>
                                                <asp:SqlDataSource ID="sdsOtherPlant" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                    CacheKeyDependency="ckdOtherPlant" SelectCommand="SELECT * FROM TTERMINAL WHERE NVL(CACTIVE,'1')='1'">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="">
                                                <dx:ASPxGridView ID="gvwTerminal" ClientInstanceName="gvwTerminal" runat="server" KeyFieldName="KEYID"
                                                    SkinID="_gvw" EnableTheming="False" Border-BorderStyle="None">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; }" />
                                                    <Columns>
                                                        <dx:GridViewDataComboBoxColumn Width="10%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblSMAIN_PLANT" ClientInstanceName="lblSMAIN_PLANT" runat="server" Text='<%# ""+ Eval("CMAIN_PLANT")=="1"?"คลังต้นทางหลัก:":"คลังต้นทางอื่นๆ:" %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn FieldName="STERMINALNAME" Width="68%">
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnDelContractPlant" ClientInstanceName="btnDelContractPlant" runat="server" ToolTip="ลบรายการ"
                                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTerminal.InCallback()) gvwTerminal.PerformCallback('DEL_CONTRACT_PLANT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="STERMINALID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CMAIN_PLANT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CONT_PLNT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="KEYID" Visible="false" />
                                                    </Columns>
                                                    <Border BorderStyle="None" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                    <Settings GridLines="None" ShowColumnHeaders="false" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="txtSCONT_PLNT" ClientInstanceName="txtSCONT_PLNT" runat="server">
                                                    </dx:ASPxTextBox>
                                                </span>
                                                
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                    <hr />
                    <dx:ASPxRoundPanel ID="rpnGProduct" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="กลุ่มผลิตภัณฑ์">
                        <PanelCollection>
                            <dx:PanelContent ID="pctGProduct" runat="server">
                                <table width="100%">
                                    <tbody id="tblGProduct" runat="server">
                                        <tr>
                                            <td style="">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <dx:ASPxGridView ID="gvwGProduct" runat="server" ClientInstanceName="gvwGProduct" AutoGenerateColumns="False"
                                                    EnableTheming="False" Width="60%" KeyFieldName="GID" SkinID="_gvw">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; } " />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="7%" ReadOnly="false" ShowInCustomizationForm="false">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SPRODUCTTYPEID" Caption="รหัสกลุ่มผลิตภัณฑ์" Width="18%" VisibleIndex="1">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SPRODUCTTYPENAME" Caption="ชื่อกลุ่มผลิตภัณฑ์" ShowInCustomizationForm="false"
                                                            VisibleIndex="2">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การกระทำ" ReadOnly="True" Width="10%">
                                                            <DataItemTemplate>
                                                                <table border="0" cellspacing="1" cellpadding="1">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnEditProduct" ClientInstanceName="btnEditProduct" runat="server" ToolTip="ปรับปรุง/แก้ไขรายการ"
                                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('STARTEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxButton ID="btnDelProduct" ClientInstanceName="btnDelProduct" runat="server" ToolTip="ลบรายการ"
                                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('DELGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="GID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CACTIVE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" ShowInCustomizationForm="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" ShowInCustomizationForm="false" />
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <div style="text-align: center;">
                                                                <table width="90%">
                                                                    <tr>
                                                                        <td style="width: 30%;" align="right">
                                                                            เลือกกลุ่มผลิตภัณฑ์ : </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxComboBox ID="cmbProductType" ClientInstanceName="cmbProductType" runat="server" DataSourceID="sdsProductType"
                                                                                TextField="SPRODUCTTYPENAME" ValueField="SPRODUCTTYPEID">
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True" ValidationGroup="addProduct">
                                                                                    <ErrorFrameStyle ForeColor="Red" />
                                                                                    <RequiredField ErrorText="กรุณาระบุกลุ่มผลิตภัณฑ์" IsRequired="True" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxComboBox>
                                                                            <asp:SqlDataSource ID="sdsProductType" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" CancelSelectOnNullParameter="False"
                                                                                CacheKeyDependency="ckdProductType" SelectCommand="SELECT SPRODUCTTYPEID,SPRODUCTTYPENAME FROM TPRODUCTTYPE WHERE ACTIVE = 1">
                                                                            </asp:SqlDataSource>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <dx:ASPxButton ID="btnSaveProduct" ClientInstanceName="btnSaveProduct" runat="server" CausesValidation="true"
                                                                                ValidationGroup="addProduct" SkinID="_confirm" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(ASPxClientEdit.ValidateGroup('addProduct')){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('SAVEGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }else{ return false; } }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxButton ID="btnCancelSave" ClientInstanceName="btnCancelSave" runat="server" SkinID="_cancel"
                                                                                CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('CANCELEDIT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowStatusBar="Auto" />
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <SettingsLoadingPanel Mode="ShowAsPopup" Text="Please Wait..." />
                                                    <SettingsEditing Mode="EditFormAndDisplayRow" />
                                                </dx:ASPxGridView>
                                               
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <table id="tbladdGProduct" runat="Server" width="60%">
                                                    <tr>
                                                        <td align="left" width="28px">
                                                            <dx:ASPxButton ID="btnNewGProduct" ClientInstanceName="btnNewGProduct" runat="server" ToolTip="เพิ่มกลุ่มผลิตภัณฑ์"
                                                                CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                <ClientSideEvents Click="function(s,e){ if(!gvwGProduct.InCallback()) gvwGProduct.PerformCallback('NEWGPRODUCT;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                        <td valign="middle" align="left">
                                                            เพิ่มกลุ่มผลิตภัณฑ์อื่นๆ </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                                        
                </div>
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>
                        <dx:TabPage Text="ทะเบียนรถในสัญญา" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server">
                                    <dx:ASPxRoundPanel ID="rpnCTruck" ClientInstanceName="rpn" runat="server" Width="100%" HeaderText="รายชื่อรถในสัญญา">
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent3" runat="server">
                                <table width="100%">
                                    <tbody id="Tbody4" runat="server">
                                        <tr>
                                            <td colspan="2">
                                                <span style="display: none;">
                                                    <dx:ASPxTextBox ID="TRUCK_KEYID" ClientInstanceName="TRUCK_KEYID" runat="server" Width="50px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="VEH_TYPE" ClientInstanceName="VEH_TYPE" runat="server" Width="150px">
                                                    </dx:ASPxTextBox>
                                                    <dx:ASPxTextBox ID="TU_TYPE" ClientInstanceName="TU_TYPE" runat="server" Width="150px">
                                                    </dx:ASPxTextBox>
                                                </span></td>
                                            <td align="right" style="padding-right: 10px;">
                                                <dx:ASPxButton ID="btnTruckManagment" ClientInstanceName="btnTruckManagment" runat="server" Text="จัดการธุรกรรมการใช้งานรถ"
                                                    AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('TManagement;'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            ทะบียน(หัว): </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboVEH" ClientInstanceName="cboVEH" CallbackPageSize="30" runat="server" EnableCallbackMode="True"
                                                                OnItemRequestedByValue="cboVEH_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVEH_OnItemsRequestedByFilterConditionSQL"
                                                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="STRUCKID" Width="250px">
                                                                <ClientSideEvents SelectedIndexChanged="function(s,e){ if(s.GetSelectedIndex() + '' != '-1'){ VEH_TYPE.SetText(s.GetItem(s.GetSelectedIndex()).GetColumnText('VEH_TYPE')); if(!cboTU.InCallback()) cboTU.PerformCallback(s.GetValue());} }"
                                                                    ValueChanged="function(s,e){ if(!cboTU.InCallback()) cboTU.PerformCallback(s.GetValue()); }" />
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัส" FieldName="STRUCKID" Width="80px" />
                                                                    <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ทะเบียนหาง" FieldName="TU_NO" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ประเภท" FieldName="VEH_TYPE" Width="70px" />
                                                                </Columns>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            ทะบียน(หาง): </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboTU" runat="server" ClientInstanceName="cboTU" Width="250px" EnableCallbackMode="True"
                                                                TextField="TU_NO" ValueField="STRAILERID"  OnCallback="cboTU_Callback" DropDownButton-Enabled="false"
                                                                SelectedIndex="0">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxCheckBox ID="ckbIsStandBy" ClientInstanceName="ckbIsStandBy" runat="server" Text="รถสำรอง">
                                                            </dx:ASPxCheckBox>
                                                        </td>
                                                        <td>
                                                            <table id="tblctruck" runat="Server">
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnAddNewTerminal" ClientInstanceName="btnNewTerminal" runat="server" ToolTip="เพิ่มรถในสัญญา"
                                                                            CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                            SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                            
                                                                            <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('ADD_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                            <Image Width="25px" Height="25px" Url="Images/26434_Increase.png" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td valign="middle">
                                                                        เพิ่มรถในสัญญา </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxGridView ID="gvwTruck" ClientInstanceName="gvwTruck" runat="server" KeyFieldName="STRUCKID" SkinID="_gvw"
                                                    EnableTheming="False" Width="80%">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined; VEH_TYPE.SetText(''); TU_TYPE.SetText(''); TRUCK_KEYID.SetText(''); cboVEH.SetValue(''); cboTU.SetValue(''); } " />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblNo" runat="Server">
                                                                    <ClientSideEvents Init="function(s,e){ s.SetText((parseInt(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))+1)+'.'); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="ทะเบียน(หัว)" FieldName="VEH_NO" Width="10%">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="ทะเบียน(หาง)" FieldName="TU_NO" Width="10%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblTU_No" runat="Server" Text='<%# ""+Eval("TU_NO") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Width="6%" Caption="ชนิด" FieldName="CSTANDBY">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblSTANDBY" ClientInstanceName="lblSTANDBY" runat="server" Text='<%# ""+Eval("CSTANDBY")=="Y"?"รถสำรอง":"รถในสัญญา" %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataComboBoxColumn Width="10%" Caption="ประเภท">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblTruck_Type" ClientInstanceName="lblTruck_Type" runat="server" Text='<%# ""+Eval("Truck_Type") %>'>
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataTextColumn Caption="การจัดการ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton ID="btnViewTruck" ClientInstanceName="btnViewTruck" runat="server" ToolTip="ดูข้อมูลรถ"
                                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('VIEWTRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="การจัดการ" ReadOnly="True" Width="2%">
                                                            <DataItemTemplate>
                                                                <%--<dx:ASPxButton ID="btnEditTruck" ClientInstanceName="btnEditTruck" runat="server"
                                                        ToolTip="แก้ไขรายการ" CausesValidation="False" AutoPostBack="false" EnableTheming="False"
                                                        EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix"
                                                        Width="25px">
                                                        <ClientSideEvents Click="function (s, e) { gvwTruck.PerformCallback('EDIT_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                        <Image Width="25px" Height="25px" Url="Images/26466_Settings.png" />
                                                    </dx:ASPxButton>--%>
                                                                <dx:ASPxButton ID="btnViewTruck" ClientInstanceName="btnViewTruck" runat="server" ToolTip="ดูข้อมูลรถ"
                                                                    CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                    SkinID="NoSkind" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px">
                                                                    <ClientSideEvents Click="function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('VIEWTRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                    <Image Width="25px" Height="25px" Url="Images/view1.png" />
                                                                </dx:ASPxButton>
                                                                <dx:ASPxButton ID="btnDelTruck" ClientInstanceName="btnDelTruck" runat="server" ToolTip="ลบรายการ" CausesValidation="False"
                                                                    AutoPostBack="False" EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                    CssClass="dxeLineBreakFix" Width="25px">
                                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="STRUCKID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="STRAILERID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CSTANDBY" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CREJECT" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DSTART" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="DEND" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="KEYID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CNEW" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CCHANGE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CDEL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="MOVED" Visible="false" />
                                                    </Columns>
                                                    <SettingsPager Mode="ShowAllRecords" />
                                                    <Settings ShowColumnHeaders="true" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxRoundPanel>
                          
                                       
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                        </TabPages>
                    </dx:ASPxPageControl>
                <center>                                                 <dx:ASPxButton ID="btnSubmit" ClientInstanceName="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix"
                                                    CausesValidation="true">
                                                    <ClientSideEvents Click="function(s,e){ if(ASPxClientEdit.ValidateGroup('search') && ASPxClientEdit.ValidateGroup('addGuarantee')){ var msg=''; if(window['cboModify_type'] != undefined){ if(cboModify_type.GetValue()=='0') msg='ท่านต้องการบันทึกเพื่อแก้ไข/เพิ่มเติมสัญญานี้ ใช่หรือไม่ ?'; else msg='ท่านต้องการบันทึกเพื่อต่ออายุสัญญานี้ ใช่หรือไม่ ?'; }else{ msg='ท่านต้องการบันทึกเพื่อเพิ่มสัญญานี้ ใช่หรือไม่ ?'; } dxConfirm('แจ้งเตือน', msg ,function(s,e){ dxPopupConfirm.Hide(); if(!xcpn.InCallback()) xcpn.PerformCallback('SAVE;'); } ,function(s,e){ dxPopupConfirm.Hide(); }); } else{ return false; } }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnCancel" ClientInstanceName="btnCancel" runat="server" SkinID="_back" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ window.location = 'contract_info.aspx'; }" />
                                                </dx:ASPxButton>
                                        </center>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <div>
        <table>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
