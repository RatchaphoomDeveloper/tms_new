﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Group.aspx.cs" Inherits="Group" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="row form-group">
                </div>
                <div class="row form-group">
                    <center>
                            <label class="control-label">จัดการข้อมูลกลุ่มที่</label>
                        </center>
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                    <label class="col-md-2 control-label">กลุ่มงาน</label>
                    <div class="col-md-3">
                        <asp:DropDownList runat="server" ID="ddlWorkGroup" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <label class="col-md-1 control-label">กลุ่มที่</label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtGroup" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-2 control-label">สถานะ</label>
                    <div class="col-md-3">
                        <asp:RadioButtonList runat="server" ID="rblCactive" RepeatDirection="Horizontal">
                            <asp:ListItem Text="ทั้งหมด"  Value="" />
                            <asp:ListItem Text="ใช้งาน" Value="1" Selected="True" />
                            <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <center>
                            <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                    </div>
                </div>
                <div class="row form-group" style="color:red;">
                    
                    ***Row แรกมีไว้สำหรับเพิ่มข้อมูล
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <asp:GridView runat="server" ID="gvGroup" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"  OnRowDataBound="gvWorkGroup_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="ชื่อกลุ่มงาน">
                                    <ItemTemplate>
                                        <asp:DropDownList runat="server" ID="ddlWorkGroups" CssClass="form-control">
                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อกลุ่มที่">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                                        <asp:HiddenField runat="server" ID="hidID" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="rblCactive" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="ใช้งาน" Value="1" />
                                            <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="กระบวนการ">
                                    <ItemTemplate>
                                        <input id='<%# "btnSave" + Eval("ID") %>' type="button" value="บันทึก" data-toggle="modal"  class="btn btn-md bth-hover btn-info" data-target='<%# "#ModalConfirmBeforeSave" + Eval("ID") %>'/>
                                                                                
                                        <uc1:ModelPopup runat="server" ID="mpConfirmSave"  IDModel='<%# "ModalConfirmBeforeSave" + Eval("ID") %>' IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการแก้ไข" TextDetail="คุณต้องการแก้ไขข้อมูลใช้หรือไม่ ?"  CommandArgument='<%# Container.DataItemIndex %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

