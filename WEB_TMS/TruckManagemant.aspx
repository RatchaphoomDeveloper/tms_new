﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="TruckManagemant.aspx.cs" Inherits="TruckManagemant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style14
        {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table class="OptionsTable BottomMargin" width="100%">
        <tr>
            <td align="right" style="width: 90%">
                <dx:ASPxTextBox ID="txtKeyword" runat="server" ClientInstanceName="txtKeyword" Width="50%">
                </dx:ASPxTextBox>
            </td>
            <td>
                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search">
                </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <dx:ASPxGridView ID="gvw" ClientInstanceName="gvw" runat="server" AutoGenerateColumns="false"
        DataSourceID="DemoDataSource1" KeyFieldName="STRUCKID" Width="100%" 
        EnableRowsCache="False" Settings-ShowStatusBar="Hidden">
        <Columns>
            <dx:GridViewCommandColumn VisibleIndex="0">
                <DeleteButton Visible="true">
                    <Image Url="Images/26468_Decrease.png" />
                </DeleteButton>
                <NewButton Visible="true" Text="+ Add" />
                <EditButton Visible="true" Text="/ Edit" />
                <CancelButton Visible="true" Text="Cancel" />
            </dx:GridViewCommandColumn>
            <dx:GridViewDataColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" VisibleIndex="1" />
            <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" VisibleIndex="2" ReadOnly="true" />
            <dx:GridViewDataColumn Caption="รหัสหัว" FieldName="STRUCKID" VisibleIndex="3" />
            <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="SVEHNO" VisibleIndex="4" ReadOnly="true" />
            <dx:GridViewDataColumn Caption="รหัสหาง" FieldName="STUID" VisibleIndex="5" />
            <dx:GridViewDataColumn Caption="ทะเบียนหาง" FieldName="STUNO" VisibleIndex="6" ReadOnly="true" />
            <dx:GridViewDataColumn Caption="สำรอง" FieldName="CSTANDBY" VisibleIndex="7" />
            <dx:GridViewDataColumn Caption="SVENDORID" FieldName="SVENDORID" VisibleIndex="8" Visible="false" />
            <dx:GridViewDataColumn FieldName="SUPDATE" Visible="false" />
        </Columns>
        <Templates>
            <EditForm>
                <table width="85%" border="0" cellpadding="1" cellspacing="1">
                    <tbody>
                        <tr>
                            <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">
                                รายละเอียด<asp:Literal ID="ltrSVENDORID" runat="server" Text='<%# Eval("SVENDORID") %>'></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" align="left" bgcolor="#FFEBD7">
                                <span class="style24">รหัสสัญญา</span>
                            </td>
                            <td width="19%" align="left">
                                <span class="style24">
                                    <asp:Literal ID="ltrSCONTRACTID" runat="server" Text='<%# Eval("SCONTRACTID") %>'></asp:Literal></span>
                            </td>
                            <td width="15%" align="left" bgcolor="#FFEBD7">
                                <span class="style24">เลขที่สัญญา</span>
                            </td>
                            <td width="32%" align="left">
                                <span class="style24">
                                    <asp:Literal ID="ltrSCONTRACTNO" runat="server" Text='<%# Eval("SCONTRACTNO") %>'></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#FFEBD7">
                                <span class="style24">รหัสหัว-ทะเบียนรถ (หัว)</span>
                            </td>
                            <td align="left">
                                <span class="style24">
                                    <dx:ASPxTextBox ID="txtSTRUCKID" ClientInstanceName="txtSTRUCKID" runat="server" Text='<%# Eval("STRUCKID") %>'>
                                    </dx:ASPxTextBox>
                                    <dx:ASPxComboBox ID="cboVihicleNo" runat="server" ClientInstanceName="cbxVihicleNo"
                                        EnableCallbackMode="True" SkinID="xcbbATC" Width="150px" 
                                        OnItemsRequestedByFilterCondition="cboHeadRegistfifo_OnItemsRequestedByFilterConditionSQL"
                                        TextFormatString="{1}" ValueField="SHEADREGISTERNO">
                                      <ClientSideEvents BeginCallback="function(s,e){LoadingPanel.Show();}" EndCallback="function(s,e){LoadingPanel.Hide();}"
                                            SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtSTRUCKID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));}else{ txtSTRUCKID.SetValue('');   } }" />
                                        <Columns>
                                            <dx:ListBoxColumn Caption="รหัสหัว" FieldName="STRUCKID" Width="60px" />
                                            <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="70px" />
                                            <dx:ListBoxColumn Caption="สถานะ" FieldName="CACTIVE" Width="30px" Visible="true" />
                                            <dx:ListBoxColumn Caption="ห้ามวิ่ง" FieldName="MS_HOLD" Width="40px" Visible="true" /> 
                                            <dx:ListBoxColumn Caption="อายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="80px"  Visible="true"/> 
                                        </Columns>
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                            ValidationGroup="add">
                                            <ErrorFrameStyle ForeColor="Red">
                                            </ErrorFrameStyle>
                                            <RequiredField ErrorText="กรุณาระบุทะเบียนหัว" IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                </span>
                            </td>
                            <td align="left" bgcolor="#FFEBD7">
                                <span class="style24">รหัสหาง-ทะเบียนรถ(ท้าย)</span>
                            </td>
                            <td align="left">
                                <span class="style24">
                                <dx:ASPxTextBox ID="txtSTRAILERID" ClientInstanceName="txtSTRAILERID" runat="server" Text='<%# Eval("STUID") %>' ></dx:ASPxTextBox> 
                                    <dx:ASPxComboBox ID="cboSTUNO" runat="server" ClientInstanceName="cboSTUNO"
                                        EnableCallbackMode="True" SkinID="xcbbATC" Width="150px"
                                        OnItemsRequestedByFilterCondition="cboTrailer_OnItemsRequestedByFilterConditionSQL"
                                        OnItemRequestedByValue="cboTrailer_OnItemRequestedByValueSQL"
                                        TextFormatString="{1}" ValueField="SHEADREGISTERNO">
                                       <ClientSideEvents BeginCallback="function(s,e){LoadingPanel.Show();}" EndCallback="function(s,e){LoadingPanel.Hide();}"
                                           SelectedIndexChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtSTRAILERID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));}else{ txtSTRAILERID.SetValue('');   } }" />
                                        <Columns>
                                            <dx:ListBoxColumn Caption="รหัสท้าย" FieldName="STRUCKID" Width="60px" />
                                            <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="SHEADREGISTERNO" Width="70px" />
                                            <dx:ListBoxColumn Caption="สถานะ" FieldName="CACTIVE" Width="30px" Visible="true" />
                                            <dx:ListBoxColumn Caption="ห้ามวิ่ง" FieldName="MS_HOLD" Width="40px" Visible="true" /> 
                                            <dx:ListBoxColumn Caption="อายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="80px"  Visible="true"/> 
                                        </Columns> 
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                            ValidationGroup="add">
                                            <ErrorFrameStyle ForeColor="Red">
                                            </ErrorFrameStyle>
                                            <RequiredField ErrorText="กรุณาระบุทะเบียนหาง" IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                </span>
                            </td>
                        </tr> 
                        <tr>
                            <td align="left" bgcolor="#FFEBD7">
                                <span class="style24">สำรอง</span>
                            </td>
                            <td align="left">
                                <span class="style24">
                                    <dx:ASPxTextBox ID="txtCSTANDBY" runat="server" ClientInstanceName="txtCSTANDBY"
                                        Text='<%# Eval("CSTANDBY") %>'>
                                    </dx:ASPxTextBox>
                                    <dx:ASPxRadioButtonList ID="rblCSTANDBY" ClientInstanceName="rblCSTANDBY" runat="server"
                                        Border-BorderWidth="0" RepeatDirection="Horizontal" ValueField='<%# Eval("CSTANDBY") %>'>
                                        <Items>
                                            <dx:ListEditItem Text="สำรอง" Value="Y" />
                                            <dx:ListEditItem Text="ในสัญญา" Value="N" />
                                        </Items>
                                    </dx:ASPxRadioButtonList>
                                </span>
                            </td>
                            <td align="left" bgcolor="#FFEBD7">
                                <span class="style24"></span>
                            </td>
                            <td align="left">
                                <span class="style24"></span>
                            </td>
                        </tr> 
                </table>
                <div style="text-align: right; padding: 2px 2px 2px 2px">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton"  ReplacementType="EditFormUpdateButton"  runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton" runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                </div>
            </EditForm>
        </Templates>
        <Settings ShowFilterRow="true" ShowFilterRowMenu="true" />
        <SettingsBehavior AllowSelectByRowClick="True" AllowSelectSingleRowOnly="True"  ConfirmDelete="True" />
        <SettingsPager PageSize="50" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="DemoDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
        SelectCommand="SELECT CONT_TRCK.SCONTRACTID,SCONTRACTNO ,CONT.SVENDORID
,STRUCKID ,VEH_TRCK.SHEADREGISTERNO SVEHNO
,STRAILERID STUID ,TU_TRCK.SHEADREGISTERNO STUNO 
,CONT_TRCK.CSTANDBY,CONT_TRCK.CREJECT,CONT_TRCK.DSTART,CONT_TRCK.DEND
,CONT_TRCK.DCREATE,CONT_TRCK.SCREATE,CONT_TRCK.DUPDATE,CONT_TRCK.SUPDATE
FROM TCONTRACT_TRUCK CONT_TRCK
LEFT JOIN TCONTRACT CONT ON CONT.SCONTRACTID=CONT_TRCK.SCONTRACTID
LEFT JOIN TTRUCK VEH_TRCK ON CONT_TRCK.STRUCKID=VEH_TRCK.STRUCKID
LEFT JOIN TTRUCK TU_TRCK ON CONT_TRCK.STRAILERID=TU_TRCK.STRUCKID
WHERE CONT_TRCK.SCONTRACTID is not null AND STRUCKID is not null" UpdateCommand="UPDATE TCONTRACT_TRUCK
SET    SCONTRACTID = :SCONTRACTID,
       STRUCKID    = :STRUCKID,
       STRAILERID  = :STRAILERID,
       CSTANDBY    = :CSTANDBY,
       DUPDATE     = SYSDATE,
       SUPDATE     = :SUPDATE
WHERE  CAST(SCONTRACTID as Varchar(10))||''||STRUCKID = :SCONTRACTID||''||:STRUCKID "
        DeleteCommand="DELETE TCONTRACT_TRUCK WHERE SCONTRACTID||''||STRUCKID = :SCONTRACTID||''||:STRUCKID ">
        <UpdateParameters>
            <asp:Parameter Name="SCONTRACTID" Type="String" />
            <asp:Parameter Name="STRUCKID" Type="String" />
            <asp:Parameter Name="STRAILERID" Type="String" />
            <asp:Parameter Name="CSTANDBY" Type="String" />
            <asp:Parameter Name="SUPDATE" Type="String" />
        </UpdateParameters>
        <DeleteParameters>
            <asp:Parameter Name="SCONTRACTID" Type="String" />
            <asp:Parameter Name="STRUCKID" Type="String" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsVEHICLE" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" >
        <SelectParameters> 
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
