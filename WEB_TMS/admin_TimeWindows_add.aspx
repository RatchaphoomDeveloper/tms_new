﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_TimeWindows_add.aspx.cs" Inherits="admin_TimeWindows_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            width: 50%;
            height: 31px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                   <tr>
                     <td>วันทำการ</td>
                     <td colspan="2">
                         <dx:ASPxRadioButtonList ID="rblDayType" runat="server" SkinID="rblStatus">
                           <Items>
                             <dx:ListEditItem Selected="true" Value="0" Text="วันปกติ" />
                             <dx:ListEditItem Value="1" Text="วันเสาร์ - อาทิตย์ และวันหยุดนักขัตฤกษ์" />
                           </Items>
                         </dx:ASPxRadioButtonList>
                         </td>
                   </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" style="width: 10%">
                            คลังต้นทาง<font color="#FF0000">*</font>
                        </td>
                        <td style="width: 40%">
                            <dx:ASPxComboBox ID="cbxOrganiz" runat="server" CallbackPageSize="10" ClientInstanceName="cbxOrganiz"
                                EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                                OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                                SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="200px">
                                <Columns>
                                    <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                    <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="STERMINALNAME" />
                                </Columns>
                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                    ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="<%$ Resources:CommonResource, Msg_Unit %>" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 10%;">
                            เที่ยวที่<font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 40%;">
                            <dx:ASPxTextBox ID="txtNLine" runat="server" Width="100px">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกเลขที่เที่ยว" />
                                    <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_DecimalNumber %>"
                                        ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            เวลาเริ่มต้น<font color="#FF0000">*</font>
                        </td>
                        <td align="left" class="style27">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtDStart" runat="server" ClientInstanceName="txtDStart" Width="100px"
                                            MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลาเริ่มต้น" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            เวลาสิ้นสุด<font color="#FF0000">*</font>
                        </td>
                        <td align="left" class="style18">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtDEnd" runat="server" Width="100px" MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลาสิ้นสุด" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style28">
                            วันที่ถึงปลายทาง<font color="#FF0000">*</font>
                        </td>
                        <td align="left" class="style27">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtLateDay" runat="server" Width="100px" Text="0">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกวันที่ถึงปลายทาง" />
                                    <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_DecimalNumber %>"
                                        ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>" />
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        * ใส่ตัวเลขวันที่จะถึงนับจากวันปัจจุบัน เช่นถึงปลายทางพรุ่งนี้ให้ใส่ 1
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style18">
                            เวลาที่ถึงปลายทาง<font color=" #FF0000">*</font>
                        </td>
                        <td align="left" class="style18">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <dx:ASPxTextBox ID="txtLateTime" runat="server" Width="100px" MaxLength="5">
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                SetFocusOnError="True" Display="Dynamic">
                                                <ErrorFrameStyle ForeColor="Red">
                                                </ErrorFrameStyle>
                                                <RequiredField IsRequired="True" ErrorText="กรุณาใส่เวลาที่ถึงปลายทาง" />
                                                <RegularExpression ErrorText="<%$ Resources:CommonResource, Msg_Time %>" ValidationExpression="<%$ Resources:ValidationResource, Valid_Time %>" />
                                            </ValidationSettings>
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td>
                                        &nbsp; น.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF" class="style24">
                            สถานะ <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" class="style21" colspan="3">
                            <dx:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                SelectedIndex="0" EnableDefaultAppearance="False" ClientInstanceName="rblStatus"
                                SkinID="rblStatus" ID="rblStatus">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Active" Value="1"></dx:ListEditItem>
                                    <dx:ListEditItem Text="InActive" Value="0"></dx:ListEditItem>
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" valign="top" class="style25">
                            ผู้บันทึก
                        </td>
                        <td align="left" colspan="3">
                            <dx:ASPxLabel ID="lblUser" runat="server" Text="" ClientInstanceName="lblUser">
                            </dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#FFFFFF" align="right" class="style13">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td colspan="2" class="style13">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_TimeWindows_lst.aspx'; }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="4">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="1" FieldName="STERMINALID"
                                        Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คลังต้นทาง" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        Width="25%" FieldName="STERMINALNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ช่วงเวลา" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                        Width="15%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("TSTART") %>' runat="server" ID="xlbldLastLogIn" />
                                            น. -
                                            <dx:ASPxLabel Text='<%# Eval("TEND") %>' runat="server" ID="ASPxLabel1" />
                                            น.
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เที่ยวที่" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="NLINE" Width="5%">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภท" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DAYTYPENAME" Width="8%">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="วันที่ถึง<br/>ปลายทาง" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center" PropertiesTextEdit-EncodeHtml="false"
                                        FieldName="LATEDAY" Width="8%">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เวลาที่ถึง<br/>ปลายทาง" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center" PropertiesTextEdit-EncodeHtml="false"
                                        FieldName="LATETIME" Width="8%">
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ปรับปรุงข้อมูลล่าสุด" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DUPDATE" Width="15%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("DUPDATE", "{0:dd/MM/yyyy เวลา HH:mm น. - โดย }") %>'
                                                runat="server" ID="xlbldLastLogIn" />
                                            <dx:ASPxLabel Text='<%# Eval("SUPDATE") %>' runat="server" ID="xlbPosition" />
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="6">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="TSTART" FieldName="TSTART" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="TEND" FieldName="TEND" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="CACTIVE" FieldName="CACTIVE" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  FieldName="CDAYTYPE" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdTimeWindow"
                    SelectCommand="SELECT  ROW_NUMBER () OVER (ORDER BY W.STERMINALID,W.NLINE) AS ID1, W.STERMINALID, TS.STERMINALNAME, W.TSTART, W.TEND,W.NLINE,w.CACTIVE,W.DUPDATE,U.SFIRSTNAME || ' ' || U.SLASTNAME AS  SUPDATE,CASE WHEN nvl(W.CDAYTYPE,'0') = '1' THEN 'วันหยุด' ELSE 'วันธรรมดา' END AS DAYTYPENAME,W.CDAYTYPE
                    ,W.LATEDAY,W.LATETIME FROM ((TWINDOWTIME W INNER JOIN TTERMINAL T ON W.STERMINALID =  T.STERMINALID) INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID) LEFT JOIN TUSER u ON W.SUPDATE = U.SUID WHERE TS.STERMINALID = :oSTERMINALID"
                    InsertCommand="INSERT INTO TWINDOWTIME(STERMINALID, NLINE, TSTART, TEND, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, CDAYTYPE,LATEDAY,LATETIME) VALUES (:sTerminalID, :nLine, :tStart, :tEnd, :cActive, SYSDATE, :sCreate, SYSDATE, :sUpdate, :CDAYTYPE,:tLateDay,:tLateTime)"
                    UpdateCommand="UPDATE TWINDOWTIME SET TSTART = :tStart, TEND = :tEnd, CACTIVE = :cActive, DUPDATE = SYSDATE, SUPDATE = :sUpdate, CDAYTYPE = :CDAYTYPE, LATEDAY = :tLateDay, LATETIME = :tLateTime WHERE STERMINALID = :sTerminalID AND NLINE = :nLine AND CDAYTYPE = :CDAYTYPE"
                    DeleteCommand="DELETE FROM TWINDOWTIME WHERE STERMINALID = :TerminalID AND NLINE = :NLine AND CDAYTYPE = :CDAYTYPE"
                    OnInserted="sds_Inserted" OnInserting="sds_Inserting" OnUpdated="sds_Updated"
                    OnUpdating="sds_Updating" OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                    <InsertParameters>
                        <asp:ControlParameter Name="CDAYTYPE" ControlID="rblDayType" PropertyName="Value" />
                        <asp:ControlParameter Name="sTerminalID" ControlID="cbxOrganiz" PropertyName="Value" />
                        <asp:ControlParameter Name="nLine" ControlID="txtNLine" PropertyName="Text" />
                        <asp:ControlParameter Name="tStart" ControlID="txtDStart" PropertyName="Text" />
                        <asp:ControlParameter Name="tEnd" ControlID="txtDEnd" PropertyName="Text" />
                        <asp:ControlParameter Name="cActive" ControlID="rblStatus" PropertyName="Value" />
                        <asp:SessionParameter Name="sCreate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:SessionParameter Name="sUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:ControlParameter Name="tLateDay" ControlID="txtLateDay" PropertyName="Text" />
                        <asp:ControlParameter Name="tLateTime" ControlID="txtLateTime" PropertyName="Text" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter Name="sTerminalID" ControlID="cbxOrganiz" PropertyName="Value" />
                        <asp:ControlParameter Name="nLine" ControlID="txtNLine" PropertyName="Text" />
                        <asp:ControlParameter Name="tStart" ControlID="txtDStart" PropertyName="Text" />
                        <asp:ControlParameter Name="tEnd" ControlID="txtDEnd" PropertyName="Text" />
                        <asp:ControlParameter Name="cActive" ControlID="rblStatus" PropertyName="Value" />
                        <asp:SessionParameter Name="sUpdate" SessionField="UserID" DefaultValue="ยุทธนา" />
                        <asp:ControlParameter Name="CDAYTYPE" ControlID="rblDayType" PropertyName="Value" />
                        <asp:ControlParameter Name="tLateDay" ControlID="txtLateDay" PropertyName="Text" />
                        <asp:ControlParameter Name="tLateTime" ControlID="txtLateTime" PropertyName="Text" />
                    </UpdateParameters>
                    <DeleteParameters>
                        <asp:SessionParameter Name="TerminalID" SessionField="oTerminalID" Type="String" />
                        <asp:SessionParameter Name="NLine" SessionField="oNLine" Type="Int16" />
                        <asp:SessionParameter Name="CDAYTYPE" SessionField="oCDAYTYPE" Type="String" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="oSTERMINALID" SessionField="stmID" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
