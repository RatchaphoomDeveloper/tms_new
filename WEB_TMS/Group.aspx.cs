﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Group : PageBase
{
    DataTable dt = new DataTable();

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            if (Request.QueryString[0] != null)
            {

                var decryptedBytes = MachineKey.Decode(Request.QueryString[0], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                if (decryptedValue == "0")
                {
                    AddDataTable();
                }
                else
                {
                    ddlWorkGroup.SelectedValue = decryptedValue;
                    btnSearch_Click(null, null);
                }
                
            }
            else
            {
                Response.Redirect("~");
            }

            //AddDataTable();
        }
    }
    #endregion

    #region SetDrowDownList
    private void SetDrowDownList()
    {
        DropDownListHelper.BindDropDownList(ref ddlWorkGroup, WorkGroupBLL.Instance.WorkGroupSelect(string.Empty,"1"), "ID", "NAME", false);
        ddlWorkGroup.Items.Insert(0, new ListItem() { Text="เลือกกลุ่มงาน",Value="0" });
    }
    #endregion

    #region gvWorkGroup_RowDataBound
    protected void gvWorkGroup_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            TextBox txt = (TextBox)e.Row.FindControl("txtName");
            if (txt != null)
            {
                txt.Text = dr["NAME"] + string.Empty;
            }
            RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblCactive");
            if (rbl != null)
            {
                rbl.SelectedValue = dr["CACTIVE"] + string.Empty;
            }
            HiddenField hid = (HiddenField)e.Row.FindControl("hidID");
            if (hid != null)
            {
                hid.Value = dr["ID"] + string.Empty;
            }

            DropDownList ddl = (DropDownList)e.Row.FindControl("ddlWorkGroups");
            if (ddl != null)
            {
                ddl.DataSource = ddlWorkGroup.DataSource;
                ddl.DataBind();
                DropDownListHelper.BindDropDownList(ref ddl, (DataTable)ddlWorkGroup.DataSource, "ID", "NAME", false);
                ddl.Items.Insert(0, new ListItem() { Text = "เลือกกลุ่มงาน", Value = "0" });
                ddl.SelectedValue = dr["WORKGROUPID"] + string.Empty;
            }
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        dt = WorkGroupBLL.Instance.GroupSelect(int.Parse(ddlWorkGroup.SelectedValue),txtGroup.Text, rblCactive.SelectedValue);
        DataRow dr = dt.NewRow();
        dr["ID"] = 0;
        dr["WORKGROUPID"] = 0;
        dr["NAME"] = "";
        dr["CACTIVE"] = "1";
        dt.Rows.InsertAt(dr, 0);

        gvGroup.DataSource = dt;
        gvGroup.DataBind();
    }
    #endregion

    #region AddDataTable
    private void AddDataTable()
    {

        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("WORKGROUPID", typeof(int));
        dt.Columns.Add("NAME", typeof(string));
        dt.Columns.Add("CACTIVE", typeof(string));
        dt.Rows.Add(0,0, "", "1");

        gvGroup.DataSource = dt;
        gvGroup.DataBind();
    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int index = int.Parse(btn.CommandArgument);
        GridViewRow gvr = gvGroup.Rows[index];
        int ID = 0,WorkGroupID = 0;
        string NAME = string.Empty, CACTIVE = "1";
        TextBox txt = (TextBox)gvr.FindControl("txtName");
        if (txt != null)
        {
            NAME = txt.Text.Trim();
        }
        RadioButtonList rbl = (RadioButtonList)gvr.FindControl("rblCactive");
        if (rbl != null)
        {
            CACTIVE = rbl.SelectedValue;
        }
        HiddenField hid = (HiddenField)gvr.FindControl("hidID");
        if (hid != null)
        {
            ID = int.Parse(hid.Value);
        }
        DropDownList ddl = (DropDownList)gvr.FindControl("ddlWorkGroups");
        if (ddl != null)
        {
            WorkGroupID = int.Parse(ddl.SelectedValue);
        }
        bool isRes = false;
        try
        {
            if (WorkGroupID == 0)
            {
                alertFail("กรุณาเลือก กลุ่มงาน !!!!!");
                return;
            }
            if (WorkGroupBLL.Instance.GroupCheckValidate(ID, NAME))
            {
                alertFail("กลุ่มที่ " + NAME + " มีอยู่แล้วในระบบ");

            }
            else
            {
                if (ID == 0)
                {
                    isRes = WorkGroupBLL.Instance.GroupInsert(WorkGroupID,NAME, CACTIVE);
                }
                else
                {
                    isRes = WorkGroupBLL.Instance.GroupUpdate(ID,WorkGroupID, NAME, CACTIVE);
                }
                if (isRes)
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                    btnSearch_Click(null, null);
                }
                else
                {
                    alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>");
                }
            }

        }
        catch (Exception ex)
        {
            alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>" + ex.Message);
        }

    }
    #endregion
}