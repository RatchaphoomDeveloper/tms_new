﻿<%@ Page Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ReportCheckVeh_His.aspx.cs"
    Inherits="ReportCheckVeh_His" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                    <tr>
                        <td>
                            &nbsp; </td>
                        <td width="202px">
                            <dx:ASPxTextBox ID="txtKeyword" runat="Server" ClientInstanceName="txtKeyword" Width="200px" NullText="เลขทะเบียนหัว-ท้าย, ชื่อบริษัทผู้ข่นส่ง">
                            </dx:ASPxTextBox>
                        </td>
                        <td width="92px">
                            <dx:ASPxDateEdit ID="dteStart" runat="Server" ClientInstanceName="dteStart" SkinID="xdte" Width="90px"
                                NullText="วันที่นัดหมาย" AllowNull="true">
                            </dx:ASPxDateEdit>
                        </td>
                        <td width="2px">
                            - </td>
                        <td width="92px">
                            <dx:ASPxDateEdit ID="dteEnd" runat="Server" ClientInstanceName="dteEnd" SkinID="xdte" Width="90px" NullText="ถึงวันที่"
                                AllowNull="true">
                            </dx:ASPxDateEdit>
                        </td>
                        <td width="82px">
                            <dx:ASPxButton ID="btnSearch" runat="Server" ClientInstanceName="btnSearch" SkinID="_search">
                                <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('SEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                        <td width="110px">
                            <dx:ASPxButton ID="btnClearSearch" runat="Server" ClientInstanceName="btnClearSearch" SkinID="_clearsearch">
                                <ClientSideEvents Click="function(s,e){ if(xcpn.InCallback()) return; else xcpn.PerformCallback('CLEARSEARCH;'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="1">
                    <tr>
                        <td width="87%" height="35">
                            <dx:ASPxLabel ID="lblReport" runat="Server" ClientInstanceName="lblReport" EncodeHtml="false" EnableTheming="false"
                                EnableDefaultAppearance="false">
                            </dx:ASPxLabel>
                        </td>
                        <td width="13%">
                            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td width="13%">
                                        <dx:ASPxButton ID="btnPrint" runat="Server" ClientInstanceName="btnPrint" EncodeHtml="false" EnableTheming="false"
                                            SkinID="None" EnableDefaultAppearance="false" Image-Url="Images/ic_pdf2.gif" Image-Width="16" Image-Height="16"
                                            ToolTip="Print" Cursor="pointer" AutoPostBack="true" OnClick="btnPrint_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td width="37%">
                                        Print</td>
                                    <td width="13%">
                                        <dx:ASPxButton ID="btnExport" runat="Server" ClientInstanceName="btnExport" EncodeHtml="false" EnableTheming="false"
                                            SkinID="None" EnableDefaultAppearance="false" Image-Url="Images/ic_ms_excel.gif" Image-Width="16"
                                            ToolTip="Export" Cursor="pointer" Image-Height="16" AutoPostBack="true" OnClick="btnExport_Click">
                                        </dx:ASPxButton>
                                    </td>
                                    <td width="37%">
                                        Export</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <dx:ASPxGridView ID="gvwVehHis" runat="server" ClientInstanceName="gvwVehHis" SkinID="_gvw" Width="100%"
                    AutoGenerateColumns="false" Style="margin: 0px;" KeyFieldName="REQUEST_ID">
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="ที่" Width="3%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" Width="8%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" Width="8%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="บริษัทผู้ข่นส่ง" FieldName="SABBREVIATION" Width="17%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" Width="29%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="สาเหตุ" FieldName="CAUSE_NAME" Width="17%">
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Left" />
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataTextColumn Caption="วันที่นัดหมาย" FieldName="SERVICE_DATE" Width="8%">
                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                            </PropertiesTextEdit>
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataColumn Caption="ผลตรวจ" Width="5%">
                            <DataItemTemplate>
                                <dx:ASPxButton ID="btnResult" runat="Server" EncodeHtml="false" EnableTheming="false" SkinID="None" EnableDefaultAppearance="false"
                                    Image-Url="Images/ic_pdf2.gif" Image-Width="16" Image-Height="16" Cursor="pointer" AutoPostBack="true"
                                    CommandArgument='<%#Bind("REQUEST_ID") %>' OnClick="btnResult_Click">
                                </dx:ASPxButton>
                            </DataItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <CellStyle HorizontalAlign="Center" />
                        </dx:GridViewDataColumn>
                    </Columns>
                    <SettingsPager AlwaysShowPager="true">
                    </SettingsPager>
                    <Styles Row-BackColor="#E3F7F8" AlternatingRow-BackColor="#B9EAEF" PagerBottomPanel-BackColor="#f0f0f0">
                    </Styles>
                </dx:ASPxGridView>
                <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvwVehHis" PaperKind="A4" RightMargin="0"
                    TopMargin="0" LeftMargin="0" Landscape="True" BottomMargin="0" MaxColumnWidth="270">
                </dx:ASPxGridViewExporter>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
