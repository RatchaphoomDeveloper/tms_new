﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class vendor_list : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        gvw.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);

        if (!IsPostBack)
        {
        #region เช็ค Permission
         // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
         LogUser("46", "R", "เปิดดูข้อมูลหน้า ข้อมูลผู้ขนส่ง", "");
         this.AssignAuthen();
        #endregion

                  }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnCancelSearch.Enabled=false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    
    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
        //ASPxButton imbeditStatus = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbeditStatus") as ASPxButton;
        if (!CanWrite)
        {
            imbedit.Enabled = false;
            //imbeditStatus.Enabled = false;
        }
    }

    void gvw_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        //string Chkin = e.GetValue("CHECKIN") + "";
        //string NOTFILL = e.GetValue("NOTFILL") + "";
        string INUSE = e.GetValue("INUSE") + "";
        ASPxButton btn = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btn") as ASPxButton;
        if (INUSE == "1")
        {
            e.Row.BackColor = System.Drawing.Color.White;
            // chk.Checked = true;
            try
            {
                btn.Image.Url = "Images/k2.png";
            }
            catch
            {

            }
        }
        else
        {
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            try
            {
                btn.Image.Url = "Images/x2.png";
            }
            catch
            {

            }
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
       
        Listdata();

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession2 + "',function(){window.location='admin_user_lst.aspx';});");

        }
        else
        {
            string[] param = e.Parameter.Split(';');
            switch (param[0])
            {
                case "Search": Listdata();
                    break;

                case "CancelSearch":
                    txtSearch.Text = "";
                    cblstatus.SelectedIndex = 0;
                    cblconfirm.SelectedIndex = 0;
                    Listdata();
                    break;

                case "edit":

                    dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID");
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_edit.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data));



                    break;
                case "view":

                    dynamic view = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID");
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_view.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + view));
                    break;

                case "ChkInUse":

                    dynamic VenId = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID");


                    string strCheckdata = @"SELECT SVENDORID, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS,DESCRIPTION
                                            FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VenId) + @"' AND INUSE ='1' ";
                    string strCompany = "";
                    DataTable dt = CommonFunction.Get_Data(conn, strCheckdata);
                    if (dt.Rows.Count > 0)
                    {
                        strCompany = @"UPDATE TVendor
                                         SET INUSE='0'
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VenId) + "'";
                    }
                    else
                    {
                        strCompany = @"UPDATE TVendor
                                         SET INUSE='1'
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VenId) + "'";
                    }

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }

                        using (OracleCommand com1 = new OracleCommand(strCompany, con))
                        {
                            com1.ExecuteNonQuery();
                        }

                    }


                    Listdata();
                    break;
            }

        }
    }

    void Listdata()
    {

        string Condition ="";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = "AND ven.SVENDORID LIKE  '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%' OR ven.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + @"%' ";
        }


        string Codition2 = "";
        switch (cblstatus.Value+"")
        {
            case "0": Codition2 += " AND NVL(ven.INUSE,'Y') != '1'";
                break;

            case "1": Codition2 += " AND ven.INUSE = '1'";
                break;
        }

        switch (cblconfirm.Value + "")
        {
            case "N": Codition2 += " AND ven.CACTIVE = '0'";
                break;

            case "1": Codition2 += " AND NVL(ven.CACTIVE,'Y') != '0'";
                break;
        }

        string strsql = @"SELECT ven.DUPDATE, ven.SVENDORID,ven.SABBREVIATION,vensap.SNO ||' '|| vensap.SDISTRICT||' '|| vensap.SREGION ||' '|| vensap.SPROVINCE ||' '|| vensap.SPROVINCECODE as Address
, ven.STEL ,CASE ven.CACTIVE
  WHEN '0' THEN 'ไม่อนุญาตให้ใช้งาน' 
  ELSE ' ' 
END AS CACTIVE
,ven.CHECKIN ,ven.NOTFILL,ven.INUSE
FROM TVendor ven
LEFT  JOIN TVENDOR_SAP vensap
ON ven.SVENDORID = vensap.SVENDORID
WHERE  1=1 "+ Condition +" " + Codition2 + @"
ORDER BY ven.INUSE DESC NULLS LAST,ven.DUPDATE DESC NULLS LAST";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);

        gvw.DataSource = dt;
        gvw.DataBind();




    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}