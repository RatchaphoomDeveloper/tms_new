﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Text;
using DevExpress.Web.ASPxCallbackPanel;
using System.Web.Configuration;
using TMS_BLL.Master;
using DevExpress.XtraEditors;

public partial class truck_status : PageBase
{
    #region + ViewState +
    private string CGROUP
    {
        get
        {
            if ((string)ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    private string SVDID
    {
        get
        {
            if ((string)ViewState["SVDID"] != null)
                return (string)ViewState["SVDID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVDID"] = value;
        }
    }
    #endregion
    //protected void Page_Init(object sender, EventArgs e)
    //{
    //    if(!string.IsNullOrEmpty(Session["CGROUP"].ToString()))
    //    {
    //        CGROUP = Session["CGROUP"].ToString();
    //        SVDID = Session["SVDID"].ToString();
    //    }
    //    if (string.Equals(CGROUP, "0"))
    //    {
    //        gvwTruck.AllColumns[10].SetColVisible(false);
    //        VehicleTruck.AllColumns[12].SetColVisible(false);
    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        #region Event
        //gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        //VehicleTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        VehicleTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(VehicleTruck_AfterPerformCallback);
        #endregion
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        CGROUP = Session["CGROUP"].ToString();
        SVDID = Session["SVDID"].ToString();
        if (!IsPostBack)
        {
            //if (!Permissions("51"))
            //{ ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return; }
            if(string.Equals(CGROUP,"1"))
            {
                ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
                cboVendorVehicle.DataSource = ViewState["DataVendor"];
                cboVendorVehicle.DataBind();            
                BindGridVehicle(VehicleTruck);
                
                //CheckBox Vehicle
                checkAllowVehicle.Checked = true;
                chkDisallowVehicle.Checked = true;
            }
            else
            {
                
                //CheckBox Vehicle
                checkAllowVehicle.Checked = true;
                chkDisallowVehicle.Checked = true;
                
                VehicleTruck.AllColumns[12].SetColVisible(false);
                
                
                cboVendorVehicle.DataSource = ViewState["DataVendor"];
                cboVendorVehicle.Enabled = false;
                cboVendorVehicle.DataBind();
                cboVendorVehicle.Value = SVDID;                
                BindGridSearchVehicle(VehicleTruck);
                
            }

            this.AssignAuthen();
        }
        else
        {
            if (string.Equals(CGROUP, "1"))
            {
                
                cboVendorVehicle.DataSource = ViewState["DataVendor"];
                cboVendorVehicle.DataBind();
            }
            else
            {
                
                //CheckBox Vehicle
                checkAllowVehicle.Checked = true;
                chkDisallowVehicle.Checked = true;                
                cboVendorVehicle.DataSource = ViewState["DataVendor"];
                cboVendorVehicle.DataBind();                
                cboVendorVehicle.Value = SVDID;                
                VehicleTruck.AllColumns[12].SetColVisible(false);
            }
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
               
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SEARCH":
                
                break;
             case "SEARCH_CANCEL":
                //ClearTextTruck();
                
            break;
            case "SEARCHVEHICLE":
                BindGridSearchVehicle(VehicleTruck);
            break;
            case "SEARCH_CANCELVEHICLE":
                ClearTextVehicle();
                BindGridVehicle(VehicleTruck);
            break;
        }
    }

    
    private void ClearTextVehicle()
    {
        if (string.Equals(CGROUP.ToString(), "1")) cboVendorVehicle.Text = "";
        txtRegisVehicle.Text = "";
        checkAllowVehicle.Checked = true;
        chkDisallowVehicle.Checked = true;
        StartDateVehicle.Text = "";
        EndDateVehicle.Text = "";
        
    }


    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            default: break;
            //case "SORT":
            //case "CANCELEDIT":
            //    break;
            //case "STARTEDIT":
            //    break;

            //case "EDIT":
            //    dynamic edy_data = gvwTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID", "SCHASIS", "SHEADREGISTERNO");
            //    string eEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(edy_data[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(edy_data[1] + "")
            //        , eUrl = "truck_edit.aspx?str=";
            //    gvwTruck.JSProperties["cpRedirectTo"] = eUrl + eEncrypt;
            //    // BindGridSearch(gvwTruck, "SHEADREGISTERNO",edy_data[3]);                
            //    break;
            //case "VIEWCONTRACT":
            //    dynamic dydata = gvwTruck.GetRowValues(visibleindex, "SCONTRACTID", "SVENDORID");
            //    string vEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dydata[0] + "&" + dydata[1]))
            //        , vUrl = "contract_add.aspx?str=";
            //    gvwTruck.JSProperties["cpRedirectTo"] = vUrl + vEncrypt;
            //    break;
            //case "ADDVEHICLE":
            //    string EncryptVehicle = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + string.Empty
            //        , Vehicle = "Vehicle.aspx?str=";
            //    gvwTruck.JSProperties["cpRedirectTo"] = Vehicle + EncryptVehicle;
            //    break;
            case "EDITVEHICLE":
                dynamic edy_datavehicle = VehicleTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID");
                string eEncryptvehicle = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(edy_datavehicle[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(edy_datavehicle[1] + "")
                        , eUrlvehicle = "Vehicle_Edit.aspx?str=";
                VehicleTruck.JSProperties["cpRedirectTo"] = eUrlvehicle + eEncryptvehicle;
                break;
        }
    }
    
    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType)
        {
            case GridViewRowType.Header:
                ASPxButton btnAdd = (ASPxButton)e.Row.FindControl("btnAdd");
                ASPxLabel lblAdd = (ASPxLabel)e.Row.FindControl("lblAdd");
                string cPermission = Session["chkurl"] + "";
                switch (cPermission)
                {
                    case "1":
                        btnAdd.ClientVisible = false;
                        lblAdd.ClientVisible = true;
                        break;
                    case "2":
                        btnAdd.ClientVisible = true;
                        lblAdd.ClientVisible = false;
                        break;
                }

                break;
            //case GridViewRowType.Data:
            //    if (Convert.ToInt32(e.GetValue("LDUPDATE")) == 0)
            //        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            //    e.Row.BackColor.GetBrightness();
            //    break;
        }
    }
    protected void VehicleTruck_AfterPerformCallback(object s, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            case "PAGERONCLICK":
                BindGridSearchVehicle(VehicleTruck);
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }
        switch (CallbackName.ToUpper())
        {
            case "EDITVEHICLE":
                dynamic edy_datavehicle = VehicleTruck.GetRowValues(visibleindex, "STRUCKID", "SCARTYPEID");
                string eEncryptvehicle = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(edy_datavehicle[0]) + "&CarTypeID=" + ConfigValue.GetEncodeText(edy_datavehicle[1] + "")
                        , eUrlvehicle = "Vehicle_Edit.aspx?str=";
                VehicleTruck.JSProperties["cpRedirectTo"] = eUrlvehicle + eEncryptvehicle;
                break;
            default: break;
        }
       
    }

    protected void VehicleTruck_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        int VisibleIndex = e.VisibleIndex;
        ASPxButton btnEdit = (ASPxButton)VehicleTruck.FindRowCellTemplateControl(VisibleIndex, null, "btnEdit");

        if (!CanWrite)
        {
            btnEdit.Enabled = false;
        }
    }
//    private void BindGridSearch(ASPxGridView __gvw)
//    {
//        string Condition = "";
//        if (!string.IsNullOrEmpty(cboVendor.Text))
//        {
//            Condition += " AND V.SVENDORID = '" + cboVendor.Value + "'";
//        }
//        if (!string.IsNullOrEmpty(cblstatus.Value+""))
//        {
//            Condition += " AND TH.ISUSE = '" + cblstatus.Value + "'";
//        }
//        if (!string.IsNullOrEmpty(dedtStart.Text.ToString()) && !string.IsNullOrEmpty(dedtEnd.Text.Trim().ToString()))
//        {
//            Condition += " AND TH.DCREATE >= TO_DATE( '" + dedtStart.Text + "','DD/MM/YYYY') AND TH.DCREATE <= TO_DATE( '" + dedtEnd.Text + "','DD/MM/YYYY')";
//        }
//        if (chkAllow.Checked == true || chkDisallow.Checked == true)
//        {
//            string checkAllow = "";
//            string checkDisallow = "";
//            string checkDisabled = "", checkCactive9 = string.Empty;
//            if (chkAllow.Checked == true)
//            {
//                checkAllow = "Y";
//            }
//            if (chkDisallow.Checked == true)
//            {
//                checkDisallow = "N";
//            }
//            Condition += " AND TH.CACTIVE IN('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "','" + CommonFunction.ReplaceInjection(checkCactive9) + "')";
//        }
//        if (!string.IsNullOrEmpty(txtsHeadRegisterNo.Text.Trim().ToString()))
//        {
//            Condition += " AND TH.SHEADREGISTERNO ='" + txtsHeadRegisterNo.Text.Trim().ToString() + "'"; 
//        }
//        sdsTruck.SelectCommand = @" SELECT   TH.STRUCKID,SHEADREGISTERNO,V.SABBREVIATION,  
//                        TH.SCHASIS,
//                        TH.SCARTYPEID ,
//                        TT.CONFIG_NAME AS SCARCATEGORY,
//                        (CASE SCARTYPEID 
//                          WHEN 3 THEN
//                          0
//                          ELSE
//                          TH.NTOTALCAPACITY
//                          END) NTOTALCAPACITY ,
//                        TH.CACTIVE ,
//                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
//                        C.SCONTRACTID,
//                        C.SVENDORID ,
//                        C.SCONTRACTNO,
//                        (CASE SCARTYPEID 
//                          WHEN 3 THEN
//                          0
//                          ELSE
//                          TH.NSLOT
//                          END)  NSLOT,                           
//                            CASE 
//                                WHEN TH.ISUSE='1' AND TH.CACTIVE ='N' THEN
//                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
//                                WHEN TH.ISUSE='1' AND TH.CACTIVE ='Y' THEN
//                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                
//                            END STATUS,TH.ISUSE
//                    FROM
//                        TTRUCK TH
//                        LEFT JOIN C_CONFIG TT
//                    ON TH.SCARTYPEID=TT.CONFIG_VALUE 
//                    AND TT.CONFIG_TYPE ='VICHICLE'  
//                    LEFT JOIN TCONTRACT_TRUCK CT
//                    ON TH.STRUCKID=CT.STRUCKID
//                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
//                    LEFT JOIN TCONTRACT C
//                    ON CT.SCONTRACTID=C.SCONTRACTID
//                    LEFT JOIN TVENDOR V 
//                    ON V.SVENDORID = TH.STRANSPORTID
//                    WHERE TH.LOGDATA='1' AND TH.SCARTYPEID IN ('0','3','4') " + Condition + "ORDER BY TH.STRUCKID DESC";           
        
//        string sdsID = __gvw.DataSourceID;
//        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
//        //Cache.Remove(__sds.CacheKeyDependency);
//        //Cache[__sds.CacheKeyDependency] = new object();
//        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
//        //__sds.DataBind();
//        __gvw.DataBind();
//    }
//    void BindGridTC(ASPxGridView __gvw)
//    {

//        sdsTruck.SelectCommand = @"SELECT TH.STRUCKID, SHEADREGISTERNO,V.SABBREVIATION,
//                                      TH.SCHASIS,
//                                      TH.SCARTYPEID ,
//                                      TT.CONFIG_NAME  AS SCARCATEGORY,
//                                      (CASE SCARTYPEID 
//                                        WHEN 3 THEN
//                                        0
//                                        ELSE
//                                        TH.NTOTALCAPACITY
//                                        END) NTOTALCAPACITY ,
//                                    (CASE SCARTYPEID 
//                                      WHEN 3 THEN
//                                      0
//                                      ELSE
//                                      TH.NSLOT
//                                      END)  NSLOT,
//                                      TH.CACTIVE ,
//                                      NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
//                                      C.SCONTRACTID,
//                                      C.SVENDORID ,
//                                      C.SCONTRACTNO,
//                                      CASE 
//                                            WHEN TH.CACTIVE ='N' THEN
//                                            GET_C_CONFIG('10','TTRUCK_STATUS','1')    
//                                            WHEN TH.CACTIVE ='Y' THEN
//                                            GET_C_CONFIG('11','TTRUCK_STATUS','1')                                
//                                        END STATUS,TH.ISUSE
//                                    FROM TTRUCK TH
//                                    LEFT JOIN C_CONFIG TT
//                                    ON TH.SCARTYPEID      =TT.CONFIG_VALUE
//                                    AND TT.CONFIG_TYPE    ='VICHICLE'
//                                    LEFT JOIN TCONTRACT_TRUCK CT
//                                    ON TH.STRUCKID              =CT.STRUCKID
//                                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
//                                    LEFT JOIN TCONTRACT C
//                                    ON CT.SCONTRACTID=C.SCONTRACTID
//                                    LEFT JOIN TVENDOR V 
//                                    ON V.SVENDORID = TH.STRANSPORTID
//                                    WHERE TH.LOGDATA='1'
//                                    ORDER BY TH.STRUCKID DESC";       
//        string sdsID = __gvw.DataSourceID;
//        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
//        //Cache.Remove(__sds.CacheKeyDependency);
//        //Cache[__sds.CacheKeyDependency] = new object();
//        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
//        //__sds.DataBind();
//        __gvw.DataBind();
//    }
    private void BindGridSearchVehicle(ASPxGridView VehicleTruck)
    {
        string Condition = "";
        if (!string.IsNullOrEmpty(cboVendorVehicle.Text))
        {
            Condition += " AND V.SVENDORID = '" + cboVendorVehicle.Value + "'";
        }
        if (!string.IsNullOrEmpty(StartDateVehicle.Text.ToString()) && !string.IsNullOrEmpty(EndDateVehicle.Text.Trim().ToString()))
        {
            Condition += " AND TH.DUPDATE >= TO_DATE( '" + StartDateVehicle.Text + "','DD/MM/YYYY')  AND TH.DUPDATE <= TO_DATE( '" + EndDateVehicle.Text + "','DD/MM/YYYY')";
        }
        if (checkAllowVehicle.Checked == true || chkDisallowVehicle.Checked == true)
        {
            string checkAllow_Vehicle = "";
            string checkDisallow_Vehicle = "";
            string checkDisabled_Vehicle = "", checkCactive9 = string.Empty;
            if (checkAllowVehicle.Checked == true)
            {
                checkAllow_Vehicle = "Y";
            }
            if (chkDisallowVehicle.Checked == true)
            {
                checkDisallow_Vehicle = "N";
            }
            Condition += " AND TH.CACTIVE IN('" + CommonFunction.ReplaceInjection(checkAllow_Vehicle) + "','" + CommonFunction.ReplaceInjection(checkDisallow_Vehicle) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled_Vehicle) + "','" + CommonFunction.ReplaceInjection(checkCactive9) + "')";
        }
        if (!string.IsNullOrEmpty(txtRegisVehicle.Text.Trim().ToString()))
        {
            Condition += " AND TH.SCARTYPEID NOT IN ('4') AND (TH.SHEADREGISTERNO LIKE '%" + txtRegisVehicle.Text.Trim().ToString() + "%' OR TH.STRAILERREGISTERNO LIKE '%" + txtRegisVehicle.Text.Trim().ToString() + "%')";
        }
        sdsTruckVehicle.SelectCommand = @"SELECT   TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,TH.NSLOT,
                        TH.NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        C.SCONTRACTNO,ISUSE,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                    
                            END STATUS
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE ='1' AND TH.SCARTYPEID IN ('0','3') AND (VIEHICLE_TYPE IS NULL OR VIEHICLE_TYPE IN ('00','34'))" + Condition + "ORDER BY TH.STRUCKID DESC";
       string sdsID = VehicleTruck.DataSourceID;
       //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
       //Cache.Remove(__sds.CacheKeyDependency);
       //Cache[__sds.CacheKeyDependency] = new object();
       //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
       //__sds.DataBind();
       VehicleTruck.DataBind();
    }
    void BindGridVehicle(ASPxGridView __gvw)
    {

        sdsTruckVehicle.SelectCommand = @"SELECT   TH.STRUCKID,V.SABBREVIATION,
                        CASE CT.CSTANDBY
                            WHEN 'N'
                            THEN 'รถในสัญญา'
                            ELSE 'รถสำรอง'
                        END SCARTYPE,
                        SHEADREGISTERNO,  
                        TH.STRAILERREGISTERNO,
                        TH.SCARTYPEID ,
                        CASE 
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '0' THEN
                          GET_C_CONFIG('00','VICHICLE_TYPE','1')
                          WHEN TH.VIEHICLE_TYPE IS NULL AND TH.SCARTYPEID = '3' THEN
                          GET_C_CONFIG('34','VICHICLE_TYPE','1')
                          ELSE
                          TT.CONFIG_NAME
                          END  AS SCARCATEGORY,TH.NSLOT,
                        TH.NTOTALCAPACITY ,
                        TH.CACTIVE ,
                        NVL(TH.DUPDATE ,TH.DCREATE) AS DUPDATE,
                        C.SCONTRACTID,
                        C.SVENDORID ,
                        C.SCONTRACTNO,ISUSE,
                          CASE 
                                WHEN TH.CACTIVE ='N' THEN
                                GET_C_CONFIG('10','TTRUCK_STATUS','1')    
                                WHEN TH.CACTIVE ='Y' THEN
                                GET_C_CONFIG('11','TTRUCK_STATUS','1')                                   
                            END STATUS
                    FROM
                        TTRUCK TH
                        LEFT JOIN C_CONFIG TT
                    ON TH.VIEHICLE_TYPE=TT.CONFIG_VALUE
                    AND (TH.VIEHICLE_TYPE !='99'
                    AND TT.CONFIG_TYPE    ='VICHICLE_TYPE' )   
                    LEFT JOIN TCONTRACT_TRUCK CT
                    ON TH.STRUCKID=CT.STRUCKID
                    AND NVL(TH.STRAILERID,'xxx')= NVL(CT.STRAILERID,'xxx')
                    LEFT JOIN TCONTRACT C
                    ON CT.SCONTRACTID=C.SCONTRACTID
                    LEFT JOIN TVENDOR V 
                    ON V.SVENDORID = TH.STRANSPORTID
                WHERE TH.LOGDATA='1' AND TH.ISUSE ='1' AND TH.SCARTYPEID IN ('0','3') AND (VIEHICLE_TYPE IS NULL OR VIEHICLE_TYPE IN ('00','34')) ORDER BY TH.STRUCKID DESC";       
        string sdsID = __gvw.DataSourceID;
        //SqlDataSource __sds = (SqlDataSource)__gvw.NamingContainer.FindControl(sdsID);
        //Cache.Remove(__sds.CacheKeyDependency);
        //Cache[__sds.CacheKeyDependency] = new object();
        //__sds.Select(new System.Web.UI.DataSourceSelectArguments());
        //__sds.DataBind();
        __gvw.DataBind();
    }
    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;
                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;
                            break;

                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
}