﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_multiplier_lst.aspx.cs" Inherits="admin_multiplier_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="90%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" NullText="ค้นหาจากคะแนน"
                                Style="margin-left: 0px" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds" OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" >
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="1" FieldName="NID"
                                        Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ช่วงคะแนน" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                        Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("NSTART") %>' runat="server" ID="xlbldLastLogIn" />
                                            -
                                            <dx:ASPxLabel Text='<%# Eval("NEND") %>' runat="server" ID="ASPxLabel1" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนน" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        Width="18%" FieldName="NSCORE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ปรับปรุงข้อมูลล่าสุด" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DUPDATE" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("DUPDATE", "{0:dd/MM/yyyy เวลา HH:mm น. - โดย }") %>'
                                                runat="server" ID="xlbldLastLogIn" />
                                            <dx:ASPxLabel Text='<%# Eval("FULLNAME") %>' runat="server" ID="xlbPosition" />&nbsp;
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="5">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                         <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY m.NID) AS ID1,m.NID,m.NSTART,m.NEND,m.NSCORE,m.DUPDATE,U.SFIRSTNAME || ' ' || U.SLASTNAME AS FULLNAME FROM LSTMULTIPLEIMPACT m LEFT JOIN TUSER u ON m.SUPDATE = U.SUID  WHERE M.NSCORE LIKE '%' || :oSearch || '%'"
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" DeleteCommand="DELETE FROM LSTMULTIPLEIMPACT WHERE (NID = :NID) "
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="NID" SessionField="dNID" Type="String" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
