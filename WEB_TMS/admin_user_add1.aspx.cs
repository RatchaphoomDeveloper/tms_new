﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class admin_user_add1 : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string CGROUP_USER = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            ListPermission();
            SetPermissionBySelectGroup();
            setRdoUserGroup();
            if (Session["suid"] != null)
            {
                listData();               
                SetPermission(Session["suid"].ToString());
            }

        }
    }
    protected void sds_Updated(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    protected void sds_Inserted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }
    void ClearControl()
    {
        Session["suid"] = null;
        txtUsername.Text = "";
        txtLastName.Text = "";
        txtPassword.Text = "";
        txtMD5Password.Text = "";
        txtMD5OldPassword.Text = "";
        btnReset.Visible = false;
        txtName.Text = "";
        txtPosition.Text = "";
        txtPhone.Text = "";
        cbxOrganiz.Value = "";
        rblgroup.Value = "0";
        rblStatus.Value = "1";
        cbxOrganiz.SelectedIndex = 0;
        txtEmail.Text = "";

        ListPermission();

    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "ResetPassword":
                string resetPassword = "p@ssw0rd";
                //xtxtMD5Password.Text = STCrypt.encryptMD5(resetPassword);

                txtPassword.ClientSideEvents.Init = string.Format("function(s, e){{ s.SetText('{0}'); }}", resetPassword);


                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','Reset Password เป็น p@ssw0rd แล้ว กรุณากดปุ่มบันทึกเพื่อบันทึกข้อมูล');");

                break;


            case "Save":
                if (CanWrite)
                {
                    if ("" + txtUsername.Value == "" + txtPassword.Value)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','UserName และ Password ต้องไม่เหมือนกัน!');");
                        return;
                    }

                    if (Session["suid"] == null)
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from TUSER Where SUSERNAME = '" + txtUsername.Text.Trim() + "'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากพบชื่อผู้ใช้ในฐานข้อมูลแล้ว!');");
                            return;
                        }

                        if ("" + cbxOrganiz.Value == "" + cbxOrganiz.Text)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาคลิกเลือกหน่วยงาน!');");
                            cbxOrganiz.SelectedIndex = -1;
                            return;
                        }

                        Session["suid"] = CommonFunction.Gen_ID(sql, "SELECT SUID FROM (SELECT SUID FROM TUSER ORDER BY CAST(SUID AS INT) DESC)  WHERE ROWNUM <= 1");
                        txtMD5Password.Text = STCrypt.encryptMD5(txtPassword.Text);
                        sds.Insert();

                        AddPermission(Session["suid"].ToString());

                        LogUser("30", "I", "บันทึกข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ", Session["suid"] + "");

                    }
                    else
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from TUSER Where SUSERNAME = '" + txtUsername.Text.Trim() + "' AND SUID <> '" + Session["suid"] + "'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถแก้ไขข้อมูลได้เนื่องจากพบชื่อผู้ใช้ในฐานข้อมูลแล้ว!');");
                            return;
                        }

                        if ("" + cbxOrganiz.Value == "" + cbxOrganiz.Text)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาคลิกเลือกหน่วยงาน!');");
                            cbxOrganiz.SelectedIndex = -1;
                            return;
                        }

                        if (!txtPassword.Value.Equals("*****t@1"))
                        {

                            txtMD5Password.Text = STCrypt.encryptMD5(txtPassword.Text);
                            UpdateChangeDate(Session["suid"] + "");

                        }
                        sds.Update();

                        AddPermission(Session["suid"].ToString());

                        LogUser("30", "E", "แก้ไขข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ", Session["suid"] + "");

                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_user_lst.aspx';});");

                }
                else
                {
                    //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                    #region + Make +
                    if ("" + txtUsername.Value == "" + txtPassword.Value)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','UserName และ Password ต้องไม่เหมือนกัน!');");
                        return;
                    }

                    if (Session["suid"] == null)
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from TUSER Where SUSERNAME = '" + txtUsername.Text.Trim() + "'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องจากพบชื่อผู้ใช้ในฐานข้อมูลแล้ว!');");
                            return;
                        }

                        if ("" + cbxOrganiz.Value == "" + cbxOrganiz.Text)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาคลิกเลือกหน่วยงาน!');");
                            cbxOrganiz.SelectedIndex = -1;
                            return;
                        }

                        Session["suid"] = CommonFunction.Gen_ID(sql, "SELECT SUID FROM (SELECT SUID FROM TUSER ORDER BY CAST(SUID AS INT) DESC)  WHERE ROWNUM <= 1");
                        txtMD5Password.Text = STCrypt.encryptMD5(txtPassword.Text);
                        sds.Insert();

                        AddPermission(Session["suid"].ToString());

                        LogUser("30", "I", "บันทึกข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ", Session["suid"] + "");

                    }
                    else
                    {
                        if (CommonFunction.Count_Value(sql, "Select * from TUSER Where SUSERNAME = '" + txtUsername.Text.Trim() + "' AND SUID <> '" + Session["suid"] + "'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถแก้ไขข้อมูลได้เนื่องจากพบชื่อผู้ใช้ในฐานข้อมูลแล้ว!');");
                            return;
                        }

                        if ("" + cbxOrganiz.Value == "" + cbxOrganiz.Text)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาคลิกเลือกหน่วยงาน!');");
                            cbxOrganiz.SelectedIndex = -1;
                            return;
                        }

                        if (!txtPassword.Value.Equals("*****t@1"))
                        {

                            txtMD5Password.Text = STCrypt.encryptMD5(txtPassword.Text);
                            UpdateChangeDate(Session["suid"] + "");

                        }
                        sds.Update();

                        AddPermission(Session["suid"].ToString());

                        LogUser("30", "E", "แก้ไขข้อมูลหน้า ผู้มีสิทธิใช้งานระบบ", Session["suid"] + "");

                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_user_lst.aspx';});");
                    #endregion
                }

                ClearControl();

                break;

            case "SelectGroup":
                ListDetailPermission();
                SetPermissionBySelectGroup();
                cbxOrganiz.Value = "";
                SetText();//toey Edit                
                break;

        }

    }
    private void SetText()//toey Edit
    {
        if (rblgroup.Value == "1")
        txtPassword.Visible = false;
    }

    private void setRdoUserGroup()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT USERGROUP_ID, USERGROUP_NAME FROM M_USERGROUP WHERE IS_ACTIVE = 1 ");
        if (dt.Rows.Count > 0)
        {
            //rdoUserGroup.Items.Clear();
            rblgroup.Items.Clear();
            foreach (DataRow item in dt.Rows)
            {                
                //rdoUserGroup.Items.Add(new ListItem()            
                //rdoUserGroup.Items.Add(new ListItem(item["USERGROUP_NAME"] + string.Empty, item["USERGROUP_ID"] + string.Empty));
                //rblgroup.Items.Add(new ListItem(item["USERGROUP_NAME"] + string.Empty, item["USERGROUP_ID"] + string.Empty));
                rblgroup.Items.Add(new ListEditItem(item["USERGROUP_NAME"] + string.Empty, item["USERGROUP_ID"] + string.Empty));
            }
        }
    }
    void listData()
    {
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT SVENDORID, CGROUP, SPOSITION, STEL, SFIRSTNAME, SLASTNAME, SUSERNAME, SPASSWORD, SOLDPASSWORD, CACTIVE, SEMAIL, IS_CONTRACT FROM TUSER WHERE SUID = '" + Session["suid"] + "'");
        if (dt.Rows.Count > 0)
        {
            rblgroup.Value = dt.Rows[0][1] + "";

            TP01RouteOnItemsRequestedByFilterConditionSQL(cbxOrganiz, new ListEditItemsRequestedByFilterConditionEventArgs(0, 9, dt.Rows[0][0] + ""));

            cbxOrganiz.SelectedItem.Value = dt.Rows[0][0] + "";
            txtPosition.Text = dt.Rows[0][2] + "";
            txtPhone.Text = dt.Rows[0][3] + "";
            txtName.Text = dt.Rows[0][4] + "";
            txtLastName.Text = dt.Rows[0][5] + "";
            txtUsername.Text = dt.Rows[0][6] + "";
            txtPassword.ClientSideEvents.Init = "function(s, e){ s.SetText('*****t@1');}";
            txtMD5Password.Text = dt.Rows[0][7] + "";
            txtMD5OldPassword.Text = dt.Rows[0][7] + "";
            rblStatus.Value = dt.Rows[0][9] + "";
            txtEmail.Text = dt.Rows[0][10] + "";
            radTeam.Value = dt.Rows[0]["IS_CONTRACT"] + "";
            btnReset.Visible = true;
            rdoUserGroup.SelectedValue = dt.Rows[0]["CGROUP"] + "";
        }
    }
    private void ListPermission()
    {
        DataTable dtCGROUP_USER = CommonFunction.Get_Data(sql, "SELECT SVENDORID, CGROUP, SPOSITION, STEL, SFIRSTNAME, SLASTNAME, SUSERNAME, SPASSWORD, SOLDPASSWORD, CACTIVE, SEMAIL FROM TUSER WHERE SUID = '" + Session["suid"] + "'");
        if (dtCGROUP_USER.Rows.Count > 0)
        {
            CGROUP_USER = dtCGROUP_USER.Rows[0]["CGROUP"] + "";
        }


        string Condition = "";



        //กรณีที่เป็น 1 คือ พนักงานปตท.เท่านั้น ที่จะให้เป็น รข. มว.
        switch (CGROUP_USER)
        {

            case "0": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%0%' OR NVL(PRMSEDIT,'xxx') LIKE '%0%'";
                break;
            case "1": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%1%' OR NVL(PRMSEDIT,'xxx') LIKE '%1%'";
                break;
            case "2": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%2%' OR NVL(PRMSEDIT,'xxx') LIKE '%2%'";
                break;
        }

        string QUERY = string.Format(@"SELECT SMENUID,SMENUNAME,CTYPE,NLEVEL,SMENUHEAD,CASE 
WHEN PRMSDISABLE LIKE '%" + CGROUP_USER + @"%' THEN '0'
WHEN PRMSVIEW LIKE '%" + CGROUP_USER + @"%' THEN '1'
WHEN PRMSEDIT LIKE '%" + CGROUP_USER + @"%' THEN '2'
ELSE '0' END PRMS  FROM TMENU {0} ORDER BY SMENUORDER,SMENUID", Condition);
        DataTable dt = CommonFunction.Get_Data(sql, QUERY);
        if (dt.Rows.Count > 0)
        {
            dgdPermission.Visible = true;
            dgdPermission.DataSource = dt;
            dgdPermission.DataBind();
            GridDetail(dgdPermission);
        }
        else
        {
            dgdPermission.Visible = false;
        }
    }
    private void ListDetailPermission()
    {
        string PRMS = rblgroup.Value + "";
        string Condition = "";
        //กรณีที่เป็น 1 คือ พนักงานปตท.เท่านั้น ที่จะให้เป็น รข. มว.
        switch (PRMS)
        {
            case "0": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%0%' OR NVL(PRMSEDIT,'xxx') LIKE '%0%'";
                break;
            case "1": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%1%' OR NVL(PRMSEDIT,'xxx') LIKE '%1%'";
                break;
            case "2": Condition = "WHERE NVL(PRMSVIEW,'xxx') LIKE '%2%' OR NVL(PRMSEDIT,'xxx') LIKE '%2%'";
                break;
        }

        string QUERY = string.Format(@"SELECT SMENUID,SMENUNAME,CTYPE,NLEVEL,SMENUHEAD,CASE 
WHEN PRMSDISABLE LIKE '%" + PRMS + @"%' THEN '0'
WHEN PRMSVIEW LIKE '%" + PRMS + @"%' THEN '1'
WHEN PRMSEDIT LIKE '%" + PRMS + @"%' THEN '2'
ELSE '0' END PRMS  FROM TMENU {0} ORDER BY SMENUORDER,SMENUID", Condition);
        DataTable dt = CommonFunction.Get_Data(sql, QUERY);
        if (dt.Rows.Count > 0)
        {
            dgdPermission.Visible = true;
            dgdPermission.DataSource = dt;
            dgdPermission.DataBind();
            GridDetail(dgdPermission);
        }
        else
        {
            dgdPermission.Visible = false;
        }
    }

    private void GridDetail(DataGrid _dgv)
    {
        sbyte _idxName = CommonFunction.FindIndexColumnOfDataFieldInGrid(_dgv, "SMENUNAME");
        sbyte _idxLevel = CommonFunction.FindIndexColumnOfDataFieldInGrid(_dgv, "NLEVEL");
        sbyte _idxType = CommonFunction.FindIndexColumnOfDataFieldInGrid(_dgv, "CTYPE");
        sbyte _idxSMENUID = CommonFunction.FindIndexColumnOfDataFieldInGrid(_dgv, "SMENUID");
        sbyte _idxSMENUHEAD = CommonFunction.FindIndexColumnOfDataFieldInGrid(_dgv, "SMENUHEAD");
        foreach (DataGridItem dgi in _dgv.Items)
        {
            if (dgi.Cells[_idxSMENUHEAD].Text == "1")//ไม่มี add/edit
            {
                ((ASPxCheckBox)dgi.FindControl("cbxMail2Me")).Visible = false;
            }

            if (dgi.Cells[_idxType].Text == "1")//ไม่มี add/edit
            {
                ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.RemoveAt(2);
            }
            else if (dgi.Cells[_idxType].Text == "0")// ไม่มีอะไรเลย
            {
                ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.Clear();
            }
            else if (dgi.Cells[_idxType].Text == "2" && dgi.Cells[_idxLevel].Text == "2")
            {
                ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.Clear();
            }
            if (dgi.Cells[_idxLevel].Text == "0")
                ((Literal)dgi.FindControl("ltrName")).Text = dgi.Cells[_idxName].Text;
            else
                ((Literal)dgi.FindControl("ltrName")).Text = "&nbsp;&nbsp;<img src='images/arr.gif'/>&nbsp;" + dgi.Cells[_idxName].Text;


        }
    }
    private void AddPermission(string _UserID)
    {
        sbyte _idxID = CommonFunction.FindIndexColumnOfDataFieldInGrid(dgdPermission, "SMENUID");
        sbyte _idxType = CommonFunction.FindIndexColumnOfDataFieldInGrid(dgdPermission, "CTYPE");

        using (OracleConnection con = new OracleConnection(sql))
        {
            con.Open();

            //delete item
            string sqldel = "Delete From TPERMISSION Where SUID = '" + _UserID + "'";
            OracleCommand com = new OracleCommand(sqldel, con);
            com.ExecuteNonQuery();

            string sqlpermiss = "Insert into TPERMISSION(SUID,SMENUID,CPERMISSION,MAIL2ME) Values(:sUID,:sMenuID,:cPermission,:MAIL2ME)";
            OracleCommand com1;
            foreach (DataGridItem dgi in dgdPermission.Items)
            {
                if (dgi.Cells[_idxType].Text != "0")
                {
                    if (dgi.Cells[_idxID].Text == "45")
                    {
                        com1 = new OracleCommand(sqlpermiss, con);
                        com1.Parameters.Add(":sUID", OracleType.VarChar).Value = _UserID;
                        com1.Parameters.Add(":sMenuID", OracleType.Int32).Value = int.Parse(dgi.Cells[_idxID].Text);
                        com1.Parameters.Add(":cPermission", OracleType.Char).Value = "" + ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Value;
                        com1.Parameters.Add(":MAIL2ME", OracleType.Char).Value = "" + (((ASPxCheckBox)dgi.FindControl("cbxMail2Me")).Checked ? "1" : "0");
                    }
                    else
                    {
                        com1 = new OracleCommand(sqlpermiss, con);
                        com1.Parameters.Add(":sUID", OracleType.VarChar).Value = _UserID;
                        com1.Parameters.Add(":sMenuID", OracleType.Int32).Value = int.Parse(dgi.Cells[_idxID].Text);
                        com1.Parameters.Add(":cPermission", OracleType.Char).Value = "" + ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Value;
                        com1.Parameters.Add(":MAIL2ME", OracleType.Char).Value = "" + (((ASPxCheckBox)dgi.FindControl("cbxMail2Me")).Checked ? "1" : "0");
                    }






                    com1.ExecuteNonQuery();

                }
            }

            com1 = null;

        }

    }
    private void SetPermission(string _UserID)
    {
        sbyte _idxID = CommonFunction.FindIndexColumnOfDataFieldInGrid(dgdPermission, "SMENUID");
        sbyte _idxType = CommonFunction.FindIndexColumnOfDataFieldInGrid(dgdPermission, "CTYPE");

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "Select * from TPERMISSION p Inner Join TMENU m On p.SMENUID = m.SMENUID Where p.SUID = '" + _UserID + "' Order by m.SMENUORDER");

        if (dt.Rows.Count > 0)
        {
            int index = 0;
            foreach (DataGridItem dgi in dgdPermission.Items)
            {
                DataRow[] dr = dt.Select("SMENUID='" + dgi.Cells[_idxID].Text + "'");

                if (dr.Length > 0)
                {
                    if (((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.Count > 0)
                    {
                        int _permisID = int.Parse(dgi.Cells[_idxID].Text);
                        //try
                        //{
                        ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Value = "" + dr[0]["CPERMISSION"];
                        //}catch { }

                        if (index < dt.Rows.Count - 1)
                            index++;

                    }
                    ((ASPxCheckBox)dgi.FindControl("cbxMail2Me")).Checked = ("" + dr[0]["MAIL2ME"] == "1") ? true : false;
                }
            }

        }

    }
    private void SetPermissionBySelectGroup()
    {
        int[] listItems = new int[32];
        string Condition = "";
        string PRMS = rblgroup.Value + "";

        switch (PRMS)
        {
            case "0": Condition = "WHERE PRMSDISABLE LIKE '%0%' OR PRMSVIEW LIKE '%0%' OR PRMSEDIT LIKE '%0%'";
                break;
            case "1": Condition = "WHERE PRMSDISABLE LIKE '%1%' OR PRMSVIEW LIKE '%1%' OR PRMSEDIT LIKE '%1%'";
                break;
            //case "2": Condition = "WHERE PRMSDISABLE LIKE '%2%' OR PRMSVIEW LIKE '%2%' OR PRMSEDIT LIKE '%2%'";
            //    break;
        }

        DataTable dt = CommonFunction.Get_Data(sql, @"SELECT SMENUID,SMENUNAME,CTYPE,NLEVEL,
                                                        CASE 
                                                        WHEN PRMSDISABLE LIKE '%" + PRMS + @"%' THEN '0'
                                                        WHEN PRMSVIEW LIKE '%" + PRMS + @"%' THEN '1'
                                                        WHEN PRMSEDIT LIKE '%" + PRMS + @"%' THEN '2'
                                                        ELSE '0' END PRMS 
                                                        FROM TMENU " + Condition + "  ORDER BY SMENUORDER,SMENUID");
        if (dt.Rows.Count > 0)
        {         

            sbyte _idxnMenuID = SystemFunction.FindIndexColumnOfDataFieldInGrid(dgdPermission, "SMENUID");
            foreach (DataGridItem dgi in dgdPermission.Items)
            {
                string SMEUID = dgi.Cells[_idxnMenuID].Text;

                DataView dv = new DataView(dt);
                dv.RowFilter = "SMENUID = '" + SMEUID + "'";
                
                if (dv.Count > 0)
                {
                    if (((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.Count > 0)
                    {
                        ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Value = dv[0]["PRMS"] + "";
                    }
                }
                else
                {
                    if (((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Items.Count > 0)
                    {
                        ((ASPxRadioButtonList)dgi.FindControl("rblPermission")).Value = "0";
                    }
                }
                              
            }
        }


    }
    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        String Selectgrp = rblgroup.Value.ToString();
        switch (Selectgrp)
        {

            case "0":
                sdsOrganiz.SelectCommand = "SELECT SVENDORID AS VENDOR_CODE, SVENDORNAME AS VENDOR_ABBR FROM (SELECT v.SVENDORID, VS.SVENDORNAME, ROW_NUMBER()OVER(ORDER BY VS.SVENDORNAME ) AS RN FROM  TVENDOR v INNER JOIN TVENDOR_SAP vs ON V.SVENDORID = VS.SVENDORID WHERE VS.SVENDORNAME LIKE '%" + e.Filter + "%' OR V.SVENDORID = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

                break;

            case "1":
                sdsOrganiz.SelectCommand = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT WHERE UNITNAME LIKE '%" + e.Filter + "%' OR UNITCODE = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";

                break;

            case "2":
                //sdsOrganiz.SelectCommand = "SELECT STERMINALID AS VENDOR_CODE,STERMINALNAME AS VENDOR_ABBR FROM (SELECT T.STERMINALID , TS.STERMINALNAME , ROW_NUMBER()OVER(ORDER BY TS.STERMINALNAME ) AS RN FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID WHERE (TS.STERMINALID LIKE '5%' OR TS.STERMINALID LIKE '8%') AND TS.STERMINALNAME LIKE '%" + e.Filter + "%' OR T.STERMINALID = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";
                sdsOrganiz.SelectCommand = "SELECT STERMINALID AS VENDOR_CODE,STERMINALNAME AS VENDOR_ABBR FROM (SELECT T.STERMINALID , TS.STERMINALNAME , ROW_NUMBER()OVER(ORDER BY TS.STERMINALNAME ) AS RN FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON T.STERMINALID = TS.STERMINALID WHERE (TS.STERMINALID LIKE 'H%' OR TS.STERMINALID LIKE 'K%') AND TS.STERMINALNAME LIKE '%" + e.Filter + "%' OR T.STERMINALID = '" + e.Filter + "')  WHERE RN BETWEEN '" + (e.BeginIndex + 1).ToString() + "' AND '" + (e.EndIndex + 1).ToString() + "'";
                break;
        }
        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();
        SetText();

    }
    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    private void UpdateChangeDate(string id)
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();
            using (OracleCommand com = new OracleCommand("UPDATE TUSER SET DCHANGEPASSWORD = SYSDATE WHERE SUID = :SUID", con))
            {
                com.Parameters.Clear();
                com.Parameters.Add(":SUID", OracleType.VarChar).Value = id;
                com.ExecuteNonQuery();
            }
        }
    }
}