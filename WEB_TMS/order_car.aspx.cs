﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;

public partial class order_car : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<TAddrow> lstAddnewrowgvw = new List<TAddrow>();


    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);

        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;


            lstAddnewrowgvw.Clear();
            Listdata();

            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //Response.Redirect("NotAuthorize.aspx");  
            }
            if (!CanWrite)
            {
                btnEdit.Enabled = false;
                btnAdd.Enabled = false;
                btnSubmit.Enabled = false;
                // gvw.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "เงื่อนไขที่")
        {
            ASPxLabel lblNo = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "lblNo") as ASPxLabel;
            lblNo.Text = "" + (e.VisibleIndex + 1) + ".";
        }
        
        ASPxTextBox txtcaramonth = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcaramonth") as ASPxTextBox;
        ASPxTextBox txtcaramonth2 = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcaramonth2") as ASPxTextBox;

        ASPxTextBox txtsealhit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtsealhit") as ASPxTextBox;
        ASPxButton btnDelete = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDelete") as ASPxButton;

        //config ClientInstanceName
        txtcaramonth.ClientInstanceName = txtcaramonth.ID + "_" + e.VisibleIndex;
        txtcaramonth2.ClientInstanceName = txtcaramonth2.ID + "_" + e.VisibleIndex;
        txtsealhit.ClientInstanceName = txtsealhit.ID + "_" + e.VisibleIndex;
        if (!CanWrite)
        {
            btnDelete.Visible = false;
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        //เก็บข้อมูลลงลิสใหม่เพื่อนำไปใช้ใรการเก็บ
        GetValueTOListGvw(gvw);

        switch (param[0])
        {

            case "save":

                string sqlInsert = @"INSERT INTO TBL_CONDITIONS (CONDITIONS_ID,BEGIN_CAP,END_CAP,CAR_AMOUNT,SEAL_HIT,NROWADD,ACTIVE_FLAG,STYPECAP) 
                                     VALUES ( {0},{1},{2},{3},{4},{5},'{6}','{7}')";

                foreach (var item in lstAddnewrowgvw)
                {
                    string Cap1 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.BEGIN_CAP1 + "") ? item.BEGIN_CAP1 + "" : "null") + "");
                    string Cap2 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.END_CAP1 + "") ? item.END_CAP1 + "" : "null") + "");
                    string Cap3 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.BEGIN_CAP2 + "") ? item.BEGIN_CAP2 + "" : "null") + "");
                    string Cap4 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.END_CAP2 + "") ? item.END_CAP2 + "" : "null") + "");
                    string NROWADD = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.NROWADD + "") ? item.NROWADD + "" : "null") + "");
                    string sealhit = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.SEAL_HIT + "") ? item.SEAL_HIT + "" : "null") + "");
                    string CAR_AMOUNT1 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.CAR_AMOUNT1 + "") ? item.CAR_AMOUNT1 + "" : "null") + "");
                    string CAR_AMOUNT2 = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.CAR_AMOUNT2 + "") ? item.CAR_AMOUNT2 + "" : "null") + "");

                    #region ดึง NROWADD เพื่อเก็บว่า row ที่เท่าไหร่
                    string QuerynRowadd = "SELECT NROWADD FROM TBL_CONDITIONS WHERE ACTIVE_FLAG = 'Y' GROUP BY NROWADD ORDER BY NROWADD DESC";
                    DataTable dt = CommonFunction.Get_Data(conn, QuerynRowadd);

                    string nROw = "";
                    if (dt.Rows.Count > 0)
                    {
                        nROw = CommonFunction.ReplaceInjection((int.Parse(dt.Rows[0]["NROWADD"] + "") + 1) + "");
                    }
                    else
                    {
                        nROw = "0";
                    }
                    #endregion

                    #region NVERSION
                    string QueryNVERSION = "SELECT NVL(NVERSION,0) as NVERSION FROM TBL_CONDITIONS WHERE  NVERSION <> 0 GROUP BY NVERSION ORDER BY NVERSION DESC";
                    DataTable dtNVERSION = CommonFunction.Get_Data(conn, QueryNVERSION);

                    string NVERSION = "";
                    if (dtNVERSION.Rows.Count > 0)
                    {
                        NVERSION = CommonFunction.ReplaceInjection((int.Parse(dtNVERSION.Rows[0]["NVERSION"] + "") + 1) + "");
                    }
                    else
                    {
                        NVERSION = "1";
                    }
                    #endregion

                    //ถ้าไม่มี NROWADD คือข้อมูลใหม่
                    if (!string.IsNullOrEmpty(item.NROWADD + ""))
                    {
                        string chkHead = @" SELECT MAX(BEGIN_CAP1) AS BEGIN_CAP1,MAX(END_CAP1) AS END_CAP1,MAX(BEGIN_CAP2) AS BEGIN_CAP2,MAX(END_CAP2) AS END_CAP2
                                                   FROM 
                                                    (
                                                        SELECT 
                                                        CASE WHEN STYPECAP = '1' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP1, 
                                                        CASE WHEN STYPECAP = '2' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP2,
                                                        CASE WHEN STYPECAP = '1' THEN END_CAP ELSE 0 END AS END_CAP1,
                                                        CASE WHEN STYPECAP = '2' THEN END_CAP ELSE 0 END AS END_CAP2,
                                                        STYPECAP,
                                                        NROWADD
                                                        FROM TBL_CONDITIONS
                                                        WHERE  ACTIVE_FLAG = 'Y'
                                                   ) o
                                            GROUP BY NROWADD
                                            UNION 
                                            SELECT {0},{1},{2},{3} FROM DUAL";

                        int chkHeadchange = CommonFunction.Count_Value(conn, string.Format(chkHead, Cap1, Cap2, Cap3, Cap4));
                        //ถ้ามากกว่า 1 แสดงว่ามีการเปลี่ยนค่าหัว ให้ทำการ ตีแฟลก เป็น N แล้ว Insert ข้อมูลใหม่ยกเว้นข้อมูลใหม่
                        if (chkHeadchange > 1)
                        {
                            string sqlchangeflage = @"UPDATE TBL_CONDITIONS SET ACTIVE_FLAG  = 'N', NVERSION = {1} WHERE  NROWADD ={0}";


                            AddTODB(string.Format(sqlchangeflage, NROWADD, NVERSION));

                            if (!string.IsNullOrEmpty(item.CAR_AMOUNT1 + ""))
                            {
                                AddTODB(string.Format(sqlInsert, CommonFunction.ReplaceInjection(Gen_ID()), Cap1, Cap2, CAR_AMOUNT1, sealhit, nROw, "Y", "1"));
                            }

                            if (!string.IsNullOrEmpty(item.CAR_AMOUNT2 + ""))
                            {
                                AddTODB(string.Format(sqlInsert, CommonFunction.ReplaceInjection(Gen_ID()), Cap3, Cap4, CAR_AMOUNT2, sealhit, nROw, "Y", "2"));
                            }

                        }
                        else
                        {
                            string chkdatachabge = @" SELECT MAX(COL{2}) AS CAR_AMOUNT  FROM 
                                                        (
                                                            SELECT 
                                                            CASE WHEN STYPECAP = '1' THEN CAR_AMOUNT ELSE 0 END AS COL1 , 
                                                            CASE WHEN STYPECAP = '2' THEN CAR_AMOUNT ELSE 0 END AS COL2,
                                                            STYPECAP,
                                                            NROWADD
                                                            FROM TBL_CONDITIONS
                                                            WHERE  ACTIVE_FLAG = 'Y' AND NROWADD = {0} AND STYPECAP ='{2}'
                                                       ) o
                                                    GROUP BY NROWADD 
                                                    UNION
                                                    SELECT {1} FROM DUAL";

                            string Updatesql = @"UPDATE TBL_CONDITIONS
                                                SET
                                                  CAR_AMOUNT    = {2}
                                            WHERE  NROWADD = {0} AND STYPECAP='{1}'";

                            int nCAR_AMOUNT1 = CommonFunction.Count_Value(conn, string.Format(chkdatachabge, NROWADD, CAR_AMOUNT1, "1"));
                            if (nCAR_AMOUNT1 > 1)
                            {
                                AddTODB(string.Format(Updatesql, NROWADD, "1", CAR_AMOUNT1));
                            }

                            int nCAR_AMOUNT2 = CommonFunction.Count_Value(conn, string.Format(chkdatachabge, NROWADD, CAR_AMOUNT2, "2"));
                            if (nCAR_AMOUNT2 > 1)
                            {
                                AddTODB(string.Format(Updatesql, NROWADD, "2", CAR_AMOUNT2));
                            }

                            string chksealchabge = @"  SELECT SEAL_HIT  FROM 
                                                         (
                                                            SELECT SEAL_HIT  FROM TBL_CONDITIONS  WHERE NROWADD = {0}
                                                         ) GROUP BY SEAL_HIT
                                                      UNION 
                                                      SELECT {1} FROM DUAL";

                            int chkseal = CommonFunction.Count_Value(conn, string.Format(chksealchabge, NROWADD, sealhit));

                            string Updatesealsql = @"UPDATE TBL_CONDITIONS
                                                     SET
                                                     SEAL_HIT    = {1}
                                                     WHERE  NROWADD = {0}";
                            if (chkseal > 1)
                            {
                                AddTODB(string.Format(Updatesealsql, NROWADD, sealhit));
                            }


                        }




                    }
                    else
                    {


                        if (!string.IsNullOrEmpty(item.CAR_AMOUNT1 + ""))
                        {
                            AddTODB(string.Format(sqlInsert, CommonFunction.ReplaceInjection(Gen_ID()), Cap1, Cap2, CAR_AMOUNT1, sealhit, nROw, "Y", "1"));
                        }

                        if (!string.IsNullOrEmpty(item.CAR_AMOUNT2 + ""))
                        {
                            AddTODB(string.Format(sqlInsert, CommonFunction.ReplaceInjection(Gen_ID()), Cap3, Cap4, CAR_AMOUNT2, sealhit, nROw, "Y", "2"));
                        }
                    }
                }
                lstAddnewrowgvw.Clear();
                ListdataNew();
                ListHeaderGvw();
               // Listdata();
                break;

            case "delete":

                if (!string.IsNullOrEmpty(txtIndexgvw.Text))
                {
                    int row = int.Parse(txtIndexgvw.Text.Trim());
                    dynamic data = gvw.GetRowValues(row, "NROWADD");

                    if (data != null)
                    {
                        string sqldelete = @"UPDATE TBL_CONDITIONS
                                            SET    
                                               ACTIVE_FLAG  = 'N'
                                            WHERE  NROWADD ={0}";
                        //string ss =   string.Format(sqldelete, data[0], data[1], UID);
                        AddTODB(string.Format(sqldelete, data));
                        ListdataNew();
                    }
                    else
                    {
                        if (lstAddnewrowgvw.Count > 0)
                        {
                            dynamic drow = gvw.GetRowValues(row, "NO");

                            lstAddnewrowgvw.RemoveAll(w => w.NO == drow);
                        }
                    }
                   // Listdata();
                    gvw.DataSource = lstAddnewrowgvw;
                    gvw.DataBind();
                    ListHeaderGvw();
                }
                break;
            case "cancel": xcpn.JSProperties["cpRedirectTo"] = "menustandard.aspx";
                break;
        }


    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {
                    case "add":

                        int? capset1 = null;
                        int? capset2 = null;
                        int? capset3 = null;
                        int? capset4 = null;

                        lstAddnewrowgvw.Add(new TAddrow
                        {
                            NROWADD = null,
                            BEGIN_CAP1 = null,
                            END_CAP1 = null,
                            BEGIN_CAP2 = null,
                            END_CAP2 = null,
                            CAR_AMOUNT1 = null,
                            CAR_AMOUNT2 = null,
                            SEAL_HIT = null,
                            ACTIVE_FLAG = "",
                            CONDITIONS_ID = null,
                            NO = lstAddnewrowgvw.Count > 0 ? (lstAddnewrowgvw.Select(a => a.NO).OrderByDescending(o => o).FirstOrDefault() + 1) : 1,
                        });


                        ASPxTextBox txtCapacitycell1 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell1") as ASPxTextBox;
                        ASPxTextBox txtCapacitycell2 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell2") as ASPxTextBox;
                        ASPxTextBox txtCapacitycell3 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell3") as ASPxTextBox;
                        ASPxTextBox txtCapacitycell4 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell4") as ASPxTextBox;

                        int s = lstAddnewrowgvw.Count;
                        if (s > 0)
                        {
                            for (int i = 0; i < s; i++)
                            {
                                lstAddnewrowgvw.RemoveAt(0);

                                ASPxTextBox txtcaramonth = gvw.FindRowCellTemplateControl(i, null, "txtcaramonth") as ASPxTextBox;
                                ASPxTextBox txtcaramonth2 = gvw.FindRowCellTemplateControl(i, null, "txtcaramonth2") as ASPxTextBox;
                                ASPxTextBox txtsealhit = gvw.FindRowCellTemplateControl(i, null, "txtsealhit") as ASPxTextBox;


                                int? NROWADD = PraseIntbehind(gvw.GetRowValues(i, "NROWADD") + "");
                                //string STYPECAP = gvw.GetRowValues(i, "STYPECAP") + "";

                                int? cap1 = PraseIntbehind(txtCapacitycell1 != null ? txtCapacitycell1.Text : null);
                                int? cap2 = PraseIntbehind(txtCapacitycell2 != null ? txtCapacitycell2.Text : null);
                                int? cap3 = PraseIntbehind(txtCapacitycell3 != null ? txtCapacitycell3.Text : null);
                                int? cap4 = PraseIntbehind(txtCapacitycell4 != null ? txtCapacitycell4.Text : null);
                                int? CAR_AMOUNT1 = PraseIntbehind(txtcaramonth != null ? txtcaramonth.Text : null);
                                int? CAR_AMOUNT2 = PraseIntbehind(txtcaramonth2 != null ? txtcaramonth2.Text : null);
                                int? Sealhit = PraseIntbehind(txtsealhit != null ? txtsealhit.Text : null);

                                capset1 = cap1;
                                capset2 = cap2;
                                capset3 = cap3;
                                capset4 = cap4;


                                lstAddnewrowgvw.Add(new TAddrow
                                {
                                    NO = i + 1,
                                    NROWADD = NROWADD,
                                    BEGIN_CAP1 = cap1,
                                    END_CAP1 = cap2,
                                    BEGIN_CAP2 = cap3,
                                    END_CAP2 = cap4,
                                    CAR_AMOUNT1 = CAR_AMOUNT1,
                                    CAR_AMOUNT2 = CAR_AMOUNT2,
                                    SEAL_HIT = Sealhit,
                                    ACTIVE_FLAG = "",
                                    CONDITIONS_ID = null,
                                    STYPECAP = ""
                                });
                            }
                        }
                        gvw.DataSource = lstAddnewrowgvw;
                        gvw.DataBind();
                        txtgvwrowcount.Text = gvw.VisibleRowCount + "";
                        gvw.JSProperties["cpGvwCount"] = gvw.VisibleRowCount;
                        gvw.JSProperties["cpBEGIN_CAP1"] = capset1;
                        gvw.JSProperties["cpEND_CAP1"] = capset2;
                        gvw.JSProperties["cpBEGIN_CAP2"] = capset3;
                        gvw.JSProperties["cpEND_CAP2"] = capset4;
                        //txtCapacitycell1.Text = capset1 + "";
                        //txtCapacitycell1.Text = capset2 + "";
                        //txtCapacitycell1.Text = capset3 + "";
                        //txtCapacitycell1.Text = capset4 + "";
                        break;



                }
                break;
        }

    }

    private void GetValueTOListGvw(ASPxGridView gvw)
    {
        ASPxTextBox txtCapacitycell1 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell1") as ASPxTextBox;
        ASPxTextBox txtCapacitycell2 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell2") as ASPxTextBox;
        ASPxTextBox txtCapacitycell3 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell3") as ASPxTextBox;
        ASPxTextBox txtCapacitycell4 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell4") as ASPxTextBox;

        int s = lstAddnewrowgvw.Count;
        if (s > 0)
        {
            for (int i = 0; i < s; i++)
            {
                //lstAddnewrowgvw.RemoveAll(w => w.NO == i + 1);
                dynamic drow = gvw.GetRowValues(i, "NO");

                lstAddnewrowgvw.RemoveAll(w => w.NO == drow);

                ASPxTextBox txtcaramonth = gvw.FindRowCellTemplateControl(i, null, "txtcaramonth") as ASPxTextBox;
                ASPxTextBox txtcaramonth2 = gvw.FindRowCellTemplateControl(i, null, "txtcaramonth2") as ASPxTextBox;
                ASPxTextBox txtsealhit = gvw.FindRowCellTemplateControl(i, null, "txtsealhit") as ASPxTextBox;


                int? NROWADD = PraseIntbehind(gvw.GetRowValues(i, "NROWADD") + "");
                string STYPECAP = gvw.GetRowValues(i, "STYPECAP") + "";

                int? cap1 = PraseIntbehind(txtCapacitycell1 != null ? txtCapacitycell1.Text : null);
                int? cap2 = PraseIntbehind(txtCapacitycell2 != null ? txtCapacitycell2.Text : null);
                int? cap3 = PraseIntbehind(txtCapacitycell3 != null ? txtCapacitycell3.Text : null);
                int? cap4 = PraseIntbehind(txtCapacitycell4 != null ? txtCapacitycell4.Text : null);
                int? CAR_AMOUNT1 = PraseIntbehind(txtcaramonth != null ? txtcaramonth.Text : null);
                int? CAR_AMOUNT2 = PraseIntbehind(txtcaramonth2 != null ? txtcaramonth2.Text : null);
                int? Sealhit = PraseIntbehind(txtsealhit != null ? txtsealhit.Text : null);

                lstAddnewrowgvw.Add(new TAddrow
                {
                    NO = int.Parse(drow + ""),
                    NROWADD = NROWADD,
                    BEGIN_CAP1 = cap1,
                    END_CAP1 = cap2,
                    BEGIN_CAP2 = cap3,
                    END_CAP2 = cap4,
                    CAR_AMOUNT1 = CAR_AMOUNT1,
                    CAR_AMOUNT2 = CAR_AMOUNT2,
                    SEAL_HIT = Sealhit,
                    ACTIVE_FLAG = "",
                    CONDITIONS_ID = null,
                    STYPECAP = STYPECAP
                });
            }
        }
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    void Listdata()
    {
        string sql = @"

SELECT ROWNUM as NO, sdata.BEGIN_CAP1, sdata.END_CAP1, sdata.BEGIN_CAP2, sdata.END_CAP2,  sdata.SEAL_HIT,  sdata.NROWADD,  sdata.ACTIVE_FLAG,sdata.CAR_AMOUNT1,sdata.CAR_AMOUNT2 FROM
(
        SELECT    gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        FROM TBL_CONDITIONS cdt
        LEFT JOIN 
        (
            SELECT MAX(BEGIN_CAP1) AS BEGIN_CAP1,MAX(END_CAP1) AS END_CAP1,MAX(BEGIN_CAP2) AS BEGIN_CAP2,MAX(END_CAP2) AS END_CAP2, 
                MAX(COL1) AS CAR_AMOUNT1,MAX(COL2) AS CAR_AMOUNT2,NROWADD FROM 
                (
                    SELECT 
                    CASE WHEN STYPECAP = '1' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP1, 
                    CASE WHEN STYPECAP = '2' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP2,
                    CASE WHEN STYPECAP = '1' THEN END_CAP ELSE 0 END AS END_CAP1,
                    CASE WHEN STYPECAP = '2' THEN END_CAP ELSE 0 END AS END_CAP2,
                    CASE WHEN STYPECAP = '1' THEN CAR_AMOUNT ELSE 0 END AS COL1 , 
                    CASE WHEN STYPECAP = '2' THEN CAR_AMOUNT ELSE 0 END AS COL2,
                    STYPECAP,
                    NROWADD
                    FROM TBL_CONDITIONS
                    WHERE  ACTIVE_FLAG = 'Y'
               ) o
            GROUP BY NROWADD
        )gdata
        ON cdt.NROWADD = gdata.NROWADD
        WHERE cdt.ACTIVE_FLAG = 'Y' 
        GROUP BY  gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        Order BY  cdt.NROWADD ASC
)sdata

";

        DataTable dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
            gvw.DataBind();
        }
        else
        {
            gvw.DataSource = lstAddnewrowgvw;
            gvw.DataBind();
        }

        txtgvwrowcount.Text = gvw.VisibleRowCount + "";

        ASPxTextBox txtCapacitycell1 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell1") as ASPxTextBox;
        ASPxTextBox txtCapacitycell2 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell2") as ASPxTextBox;
        ASPxTextBox txtCapacitycell3 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell3") as ASPxTextBox;
        ASPxTextBox txtCapacitycell4 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell4") as ASPxTextBox;

        if (dt.Rows.Count > 0)
        {
            txtCapacitycell1.Text = (!string.IsNullOrEmpty(dt.Rows[0]["BEGIN_CAP1"] + "") ? dt.Rows[0]["BEGIN_CAP1"] + "" : null);
            txtCapacitycell2.Text = (!string.IsNullOrEmpty(dt.Rows[0]["END_CAP1"] + "") ? dt.Rows[0]["END_CAP1"] + "" : null);
            txtCapacitycell3.Text = (!string.IsNullOrEmpty(dt.Rows[0]["BEGIN_CAP2"] + "") ? dt.Rows[0]["BEGIN_CAP2"] + "" : null);
            txtCapacitycell4.Text = (!string.IsNullOrEmpty(dt.Rows[0]["END_CAP2"] + "") ? dt.Rows[0]["END_CAP2"] + "" : null);
        }



        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int NO = int.Parse(dt.Rows[i]["NO"] + "");
            int? NROWADD = PraseIntbehind(gvw.GetRowValues(i, "NROWADD") + "");
            int? BEGIN_CAP1 = PraseIntbehind(dt.Rows[i]["BEGIN_CAP1"] + "");
            int? END_CAP1 = PraseIntbehind(dt.Rows[i]["END_CAP1"] + "");
            int? BEGIN_CAP2 = PraseIntbehind(dt.Rows[i]["BEGIN_CAP2"] + "");
            int? END_CAP2 = PraseIntbehind(dt.Rows[i]["END_CAP2"] + "");
            int? CAR_AMOUNT1 = PraseIntbehind(dt.Rows[i]["CAR_AMOUNT1"] + "");
            int? CAR_AMOUNT2 = PraseIntbehind(dt.Rows[i]["CAR_AMOUNT2"] + "");
            int? SEAL_HIT = PraseIntbehind(dt.Rows[i]["SEAL_HIT"] + "");
            lstAddnewrowgvw.Add(new TAddrow
             {
                 NO = NO,
                 NROWADD = NROWADD,
                 BEGIN_CAP1 = BEGIN_CAP1,
                 END_CAP1 = END_CAP1,
                 BEGIN_CAP2 = BEGIN_CAP2,
                 END_CAP2 = END_CAP2,
                 CAR_AMOUNT1 = CAR_AMOUNT1,
                 CAR_AMOUNT2 = CAR_AMOUNT2,
                 SEAL_HIT = SEAL_HIT,
                 ACTIVE_FLAG = "Y",
                 CONDITIONS_ID = null,
                 STYPECAP = ""
             });

        }




    }

    void ListHeaderGvw()
    {
        string sql = @"

SELECT ROWNUM as NO, sdata.BEGIN_CAP1, sdata.END_CAP1, sdata.BEGIN_CAP2, sdata.END_CAP2,  sdata.SEAL_HIT,  sdata.NROWADD,  sdata.ACTIVE_FLAG,sdata.CAR_AMOUNT1,sdata.CAR_AMOUNT2 FROM
(
        SELECT    gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        FROM TBL_CONDITIONS cdt
        LEFT JOIN 
        (
            SELECT MAX(BEGIN_CAP1) AS BEGIN_CAP1,MAX(END_CAP1) AS END_CAP1,MAX(BEGIN_CAP2) AS BEGIN_CAP2,MAX(END_CAP2) AS END_CAP2, 
                MAX(COL1) AS CAR_AMOUNT1,MAX(COL2) AS CAR_AMOUNT2,NROWADD FROM 
                (
                    SELECT 
                    CASE WHEN STYPECAP = '1' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP1, 
                    CASE WHEN STYPECAP = '2' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP2,
                    CASE WHEN STYPECAP = '1' THEN END_CAP ELSE 0 END AS END_CAP1,
                    CASE WHEN STYPECAP = '2' THEN END_CAP ELSE 0 END AS END_CAP2,
                    CASE WHEN STYPECAP = '1' THEN CAR_AMOUNT ELSE 0 END AS COL1 , 
                    CASE WHEN STYPECAP = '2' THEN CAR_AMOUNT ELSE 0 END AS COL2,
                    STYPECAP,
                    NROWADD
                    FROM TBL_CONDITIONS
                    WHERE  ACTIVE_FLAG = 'Y'
               ) o
            GROUP BY NROWADD
        )gdata
        ON cdt.NROWADD = gdata.NROWADD
        WHERE cdt.ACTIVE_FLAG = 'Y' 
        GROUP BY  gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        Order BY  cdt.NROWADD ASC
)sdata

";

        DataTable dt = CommonFunction.Get_Data(conn, sql);
        //if (dt.Rows.Count > 0)
        //{
        //    gvw.DataSource = dt;
        //    gvw.DataBind();
        //}
        //else
        //{
        //    gvw.DataSource = lstAddnewrowgvw;
        //    gvw.DataBind();
        //}

        //txtgvwrowcount.Text = gvw.VisibleRowCount + "";

        ASPxTextBox txtCapacitycell1 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell1") as ASPxTextBox;
        ASPxTextBox txtCapacitycell2 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data1"], "txtCapacitycell2") as ASPxTextBox;
        ASPxTextBox txtCapacitycell3 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell3") as ASPxTextBox;
        ASPxTextBox txtCapacitycell4 = gvw.FindHeaderTemplateControl((GridViewColumn)gvw.Columns["data2"], "txtCapacitycell4") as ASPxTextBox;

        if (dt.Rows.Count > 0)
        {
            txtCapacitycell1.Text = (!string.IsNullOrEmpty(dt.Rows[0]["BEGIN_CAP1"] + "") ? dt.Rows[0]["BEGIN_CAP1"] + "" : null);
            txtCapacitycell2.Text = (!string.IsNullOrEmpty(dt.Rows[0]["END_CAP1"] + "") ? dt.Rows[0]["END_CAP1"] + "" : null);
            txtCapacitycell3.Text = (!string.IsNullOrEmpty(dt.Rows[0]["BEGIN_CAP2"] + "") ? dt.Rows[0]["BEGIN_CAP2"] + "" : null);
            txtCapacitycell4.Text = (!string.IsNullOrEmpty(dt.Rows[0]["END_CAP2"] + "") ? dt.Rows[0]["END_CAP2"] + "" : null);
        }
    }

    void ListdataNew()
    {
        lstAddnewrowgvw.Clear();
        string sql = @"

SELECT ROWNUM as NO, sdata.BEGIN_CAP1, sdata.END_CAP1, sdata.BEGIN_CAP2, sdata.END_CAP2,  sdata.SEAL_HIT,  sdata.NROWADD,  sdata.ACTIVE_FLAG,sdata.CAR_AMOUNT1,sdata.CAR_AMOUNT2 FROM
(
        SELECT    gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        FROM TBL_CONDITIONS cdt
        LEFT JOIN 
        (
            SELECT MAX(BEGIN_CAP1) AS BEGIN_CAP1,MAX(END_CAP1) AS END_CAP1,MAX(BEGIN_CAP2) AS BEGIN_CAP2,MAX(END_CAP2) AS END_CAP2, 
                MAX(COL1) AS CAR_AMOUNT1,MAX(COL2) AS CAR_AMOUNT2,NROWADD FROM 
                (
                    SELECT 
                    CASE WHEN STYPECAP = '1' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP1, 
                    CASE WHEN STYPECAP = '2' THEN BEGIN_CAP ELSE 0 END AS BEGIN_CAP2,
                    CASE WHEN STYPECAP = '1' THEN END_CAP ELSE 0 END AS END_CAP1,
                    CASE WHEN STYPECAP = '2' THEN END_CAP ELSE 0 END AS END_CAP2,
                    CASE WHEN STYPECAP = '1' THEN CAR_AMOUNT ELSE 0 END AS COL1 , 
                    CASE WHEN STYPECAP = '2' THEN CAR_AMOUNT ELSE 0 END AS COL2,
                    STYPECAP,
                    NROWADD
                    FROM TBL_CONDITIONS
                    WHERE  ACTIVE_FLAG = 'Y'
               ) o
            GROUP BY NROWADD
        )gdata
        ON cdt.NROWADD = gdata.NROWADD
        WHERE cdt.ACTIVE_FLAG = 'Y' 
        GROUP BY  gdata.BEGIN_CAP1, gdata.END_CAP1, gdata.BEGIN_CAP2, gdata.END_CAP2,  cdt.SEAL_HIT,  cdt.NROWADD,  cdt.ACTIVE_FLAG,gdata.CAR_AMOUNT1,gdata.CAR_AMOUNT2
        Order BY  cdt.NROWADD ASC
)sdata

";

        DataTable dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
            gvw.DataBind();
        }
        else
        {
            gvw.DataSource = lstAddnewrowgvw;
            gvw.DataBind();
        }

        txtgvwrowcount.Text = gvw.VisibleRowCount + "";




        for (int i = 0; i < dt.Rows.Count; i++)
        {
            int NO = int.Parse(dt.Rows[i]["NO"] + "");
            int? NROWADD = PraseIntbehind(gvw.GetRowValues(i, "NROWADD") + "");
            int? BEGIN_CAP1 = PraseIntbehind(dt.Rows[i]["BEGIN_CAP1"] + "");
            int? END_CAP1 = PraseIntbehind(dt.Rows[i]["END_CAP1"] + "");
            int? BEGIN_CAP2 = PraseIntbehind(dt.Rows[i]["BEGIN_CAP2"] + "");
            int? END_CAP2 = PraseIntbehind(dt.Rows[i]["END_CAP2"] + "");
            int? CAR_AMOUNT1 = PraseIntbehind(dt.Rows[i]["CAR_AMOUNT1"] + "");
            int? CAR_AMOUNT2 = PraseIntbehind(dt.Rows[i]["CAR_AMOUNT2"] + "");
            int? SEAL_HIT = PraseIntbehind(dt.Rows[i]["SEAL_HIT"] + "");
            lstAddnewrowgvw.Add(new TAddrow
            {
                NO = NO,
                NROWADD = NROWADD,
                BEGIN_CAP1 = BEGIN_CAP1,
                END_CAP1 = END_CAP1,
                BEGIN_CAP2 = BEGIN_CAP2,
                END_CAP2 = END_CAP2,
                CAR_AMOUNT1 = CAR_AMOUNT1,
                CAR_AMOUNT2 = CAR_AMOUNT2,
                SEAL_HIT = SEAL_HIT,
                ACTIVE_FLAG = "Y",
                CONDITIONS_ID = null,
                STYPECAP = ""
            });

        }




    }

    private string Gen_ID()
    {
        string Result = "";

        string strsql = "SELECT CONDITIONS_ID FROM TBL_CONDITIONS ORDER BY CONDITIONS_ID DESC";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);


        if (dt.Rows.Count > 0)
        {

            string sID = dt.Rows[0]["CONDITIONS_ID"] + "";
            int nID = int.Parse(sID) + 1;

            string NewID = nID + "";


            Result = NewID;
        }
        else
        {

            Result = "1";
        }

        return Result;
    }

    private int? PraseIntbehind(string value)
    {
        int? result = null;

        if (!string.IsNullOrEmpty(value))
        {
            result = int.Parse(value);
        }
        return result;
    }

    #region class
    [Serializable]

    class TAddrow
    {
        public decimal? CONDITIONS_ID { get; set; }
        public int? BEGIN_CAP1 { get; set; }
        public int? END_CAP1 { get; set; }
        public int? BEGIN_CAP2 { get; set; }
        public int? END_CAP2 { get; set; }
        public int? SEAL_HIT { get; set; }
        public int? CAR_AMOUNT1 { get; set; }
        public int? CAR_AMOUNT2 { get; set; }
        public int? NROWADD { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public int? NO { get; set; }
        public string STYPECAP { get; set; }
    }


    #endregion
}