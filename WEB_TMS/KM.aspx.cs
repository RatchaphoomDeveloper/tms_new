﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.Globalization;
using System.IO;
using TMS_BLL.Transaction.KM;

public partial class KM : PageBase
{
    #region ViewState
    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return new DataTable();
        }
        set
        {
            ViewState["dt"] = value;
        }
    }

    private string Check
    {
        get
        {
            if ((string)ViewState["Check"] != null)
                return (string)ViewState["Check"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Check"] = value;
        }
    }

    private string KMCheck
    {
        get
        {
            if ((string)ViewState["KMCheck"] != null)
                return (string)ViewState["KMCheck"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["KMCheck"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            txtCreateDateStart.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            txtCreateDateEnd.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            this.InitialForm();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                dgvKM.Enabled = false;
                cmdSearch.Enabled = false;
                cmdSearchBook.Enabled = false;
                ImageButton2.Enabled = false;
                cmdSearchBook.Style.Add("cursor", "not-allowed");
                ImageButton2.Style.Add("cursor", "not-allowed");
            }
            if (!CanWrite)
            {
                cmdAddNewTopic.Enabled = false;
                cmdAddNewTopic.Style.Add("cursor", "not-allowed");
                cmdClear.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDepartment()
    {
        try
        {
            DataTable dtDepartment = DepartmentBLL.Instance.DepartmentSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlDepartment, dtDepartment, "DEPARTMENT_ID", "DEPARTMENT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadDivision(string department)
    {
        try
        {
            DataTable dtDivision = DivisionBLL.Instance.DivisionSelectBLL(" AND CACTIVE = 1 AND DEPARTMENT_ID = " + department);
            DropDownListHelper.BindDropDownList(ref ddlDivision, dtDivision, "DIVISION_ID", "DIVISION_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadKnowledgeType()
    {
        try
        {
            DataTable dtKnowledgeType = KnowledgeTypeBLL.Instance.KnowledgeTypeSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlKnowledgeType, dtKnowledgeType, "KNOWLEDGE_TYPE_ID", "KNOWLEDGE_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadProductTagHead()
    {
        try
        {
            DataTable dtProductTagHead = ProductTagBLL.Instance.ProductTagHeadSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlProductTagHead, dtProductTagHead, "KM_TAG_LVL1_ID", "KM_TAG_LVL1_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void KMCheckShare()
    {
        try
        {
            DataTable KMCheckShare = new DataTable();
            if (string.Equals(Session["CGROUP"].ToString().Trim(), "0") || string.Equals(Session["CGROUP"].ToString().Trim(), "6"))
                KMCheckShare = KMBLL.Instance.SelectKMCheckShareBLL(2, int.Parse(Session["UserID"].ToString()));
            else
                KMCheckShare = KMBLL.Instance.SelectKMCheckShareBLL(1, int.Parse(Session["UserID"].ToString()));

            if (KMCheckShare.Rows.Count > 0)
            {
                for (int i = 0; i < KMCheckShare.Rows.Count; i++)
                {
                    KMCheck = KMCheck + "," + KMCheckShare.Rows[i]["KM_ID"].ToString();
                }
            }
            if (KMCheck != "")
                KMCheck = KMCheck.Substring(1);

        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadDepartment();
            this.LoadKnowledgeType();
            this.LoadProductTagHead();
            this.KMCheckShare();
            this.GetDataCountKM();
            this.GetData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionSearch()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (!string.Equals(txtTopicID.Text.Trim(), ""))
            {
                sb.Append(" AND TOPIC_ID LIKE '%" + txtTopicID.Text.Trim() + "%'");
            }

            if (!string.Equals(txtAuthorName.Text.Trim(), ""))
            {
                sb.Append(" AND FULLNAME LIKE '%" + txtAuthorName.Text.Trim() + "%'");
            }

            if (ddlKnowledgeType.SelectedIndex > 0)
            {
                sb.Append(" AND KNOWLEDGE_TYPE_ID = '" + ddlKnowledgeType.SelectedValue + "'");
            }

            if (ddlDepartment.SelectedIndex > 0)
            {
                sb.Append(" AND DEPARTMENT_ID = '" + ddlDepartment.SelectedValue + "'");
            }

            if (ddlProductTagHead.SelectedIndex > 0)
            {
                if (ddlProductTagItem.SelectedIndex > 0)
                {
                    sb.Append(" AND TAGS LIKE '%" + ddlProductTagHead.SelectedItem + "-" + ddlProductTagItem.SelectedItem + "%'");
                }
                else
                {
                    sb.Append(" AND TAGS LIKE '%" + ddlProductTagHead.SelectedItem + "%'");
                }
            }

            if (ddlDivision.SelectedIndex > 0)
            {
                sb.Append(" AND DIVISION_ID = '" + ddlDivision.SelectedValue + "'");
            }

            if (!string.Equals(txtOtherKeyword.Text.Trim(), ""))
            {
                sb.Append(" AND OTHER_KEYWORDS LIKE '%" + txtOtherKeyword.Text.Trim() + "%'");
            }

            if (!string.Equals(txtYear.Text, "") && !string.Equals(txtCreateDateStart.Text, "") && !string.Equals(txtCreateDateEnd.Text, ""))
                throw new Exception("กรุณาเลือก Created Date หรือ Year อย่างใดอย่างหนึ่ง");

            if (!string.Equals(txtYear.Text, ""))
            {
                sb.Append(" AND TO_CHAR(CREATE_DATETIME,'yyyy') = '" + txtYear.Text + "' ");
            }
            else
            {
                string DateStart = string.Empty;
                string DateEnd = string.Empty;
                try
                {
                    DateStart = DateTime.ParseExact(txtCreateDateStart.Text.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy");
                    DateEnd = DateTime.ParseExact(txtCreateDateEnd.Text.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy");
                }
                catch
                {
                    throw new Exception("กรุณากรอกวันที่ให้ครบถ้วน");
                }

                sb.Append(" AND TO_DATE(TO_CHAR(CREATE_DATETIME,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE('" + DateStart + "','dd/mm/yyyy') AND TO_DATE('" + DateEnd + "','dd/mm/yyyy') ");
            }

            sb.Append(" AND STATUS_ID != 2 AND ((KM_ID IN (" + (KMCheck == "" ? "-1" : KMCheck) + ") and STATUS_ID = 1) OR (STATUS_ID = 0 AND CREATE_BY = " + int.Parse(Session["UserID"].ToString()) + ") OR (STATUS_ID = 1 AND CREATE_BY = " + int.Parse(Session["UserID"].ToString()) + ")) ");

            if (Check == "Book" || !string.Equals(Check, ""))
            {
                string condition = string.Empty;
                sb.Clear();
                DataTable dtBook = KMBLL.Instance.SelectKMBookBLL(" AND USER_ID = " + int.Parse(Session["UserID"].ToString()));
                if (dtBook.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBook.Rows.Count; i++)
                    {
                        if (i == 0)
                            condition += dtBook.Rows[i]["KM_ID"].ToString();
                        else
                            condition += "," + dtBook.Rows[i]["KM_ID"].ToString();
                    }
                }
                sb.Append(" AND STATUS_ID != 2 AND KM_ID IN (" + condition + ") ");
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadProductTagItem(string ProductTagHead)
    {
        try
        {
            DataTable dtProductTagItem = ProductTagBLL.Instance.ProductTagItemSelectBLL(ProductTagHead);
            DropDownListHelper.BindDropDownList(ref ddlProductTagItem, dtProductTagItem, "KM_TAG_LVL2_ID", "KM_TAG_LVL2_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        string ID = MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All);
        Response.Redirect("KMAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Add.ToString()), MachineKeyProtection.All) + "&id=" + ID);
    }
    protected void dgvKM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (string.Equals(e.CommandName, "View"))
            {
                int Index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
                string ID = dgvKM.DataKeys[Index].Value.ToString();
                ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);
                DataTable dtView = KMBLL.Instance.InsertKMViewBLL(int.Parse(dgvKM.DataKeys[Index]["KM_ID"].ToString()));
                if (dtView.Rows.Count > 0)
                    Response.Redirect("KMView.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + ID);
            }
            else if (string.Equals(e.CommandName, "Book"))
            {
                int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
                string ID = dgvKM.DataKeys[Index]["KM_ID"].ToString();
                DataTable dtBookMark = KMBLL.Instance.InsertKMBookmarkBLL(int.Parse(ID), int.Parse(Session["UserID"].ToString()), 1);
                if (dtBookMark.Rows.Count == 0)
                    throw new Exception("เกิดข้อผิดพลาด");
                else
                    this.GetData();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdAddNewTopic_Click(object sender, ImageClickEventArgs e)
    {
        string ID = MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All);
        Response.Redirect("KMAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Add.ToString()), MachineKeyProtection.All) + "&id=" + ID);
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        Check = "";
        this.GetData();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.SelectedIndex > 0)
            {
                this.LoadDivision(ddlDepartment.SelectedValue);
                ddlDivision.Enabled = true;
            }
            else
            {
                this.LoadDivision(string.Empty);
                ddlDivision.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetDataCountKM()
    {
        try
        {
            DataTable dtcount = KMBLL.Instance.SelectCountKMBLL(" AND SUID = " + int.Parse(Session["UserID"].ToString()));
            if (dtcount.Rows.Count > 0)
                lblCreate.Text = "Created by " + dtcount.Rows[0]["FULLNAME"].ToString() + " : " + dtcount.Rows[0]["CREATE_BY"].ToString() + " Topics";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GetData()
    {
        try
        {
            dt = KMBLL.Instance.SelectKMBLL(GetConditionSearch());
            //this.CheckBookmark(int KM);
            dgvKM.DataSource = dt;
            dgvKM.DataBind();

            DataTable dtmax = KMBLL.Instance.SelectKMBLL(string.Empty);
            lblTotal.Text = "Results : " + dt.Rows.Count + " of " + dtmax.Rows.Count + "";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdClear_Click(object sender, EventArgs e)
    {
        txtTopicID.Text = string.Empty;
        txtAuthorName.Text = string.Empty;
        ddlKnowledgeType.ClearSelection();
        ddlDepartment.ClearSelection();
        ddlProductTagHead.ClearSelection();
        ddlProductTagItem.ClearSelection();
        ddlDivision.ClearSelection();
        ddlProductTagItem.Enabled = false;
        ddlDivision.Enabled = false;
        txtOtherKeyword.Text = string.Empty;
        txtCreateDateStart.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
        txtCreateDateEnd.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
    }

    protected void dgvKM_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvKM.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvKM, dt);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdDownloadManual_Click(object sender, ImageClickEventArgs e)
    {
        string FullPath = string.Empty;//dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void ddlProductTagHead_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlProductTagHead.SelectedIndex > 0)
            {
                this.LoadProductTagItem(" AND KM_TAG_LVL1_ID = " + ddlProductTagHead.SelectedValue.Trim());
                ddlProductTagItem.Enabled = true;
            }
            else
            {
                DropDownListHelper.BindDropDownList(ref ddlProductTagItem, null, "KM_TAG_LVL2_ID", "KM_TAG_LVL2_NAME", true);
                ddlProductTagItem.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvKM_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int index = e.Row.RowIndex;
                string ID = dgvKM.DataKeys[index]["KM_ID"].ToString();
                DataTable dtBook = KMBLL.Instance.SelectKMBookBLL(" AND KM_ID = " + ID + " AND USER_ID = " + int.Parse(Session["UserID"].ToString()));
                if (dtBook.Rows.Count > 0)
                {
                    ImageButton imgBook;
                    imgBook = (ImageButton)e.Row.Cells[0].FindControl("imgBook");
                    imgBook.ImageUrl = "~/Images/ImageForKM/bookmark_icon3_active.png";
                }
                else
                {
                    ImageButton imgBook;
                    imgBook = (ImageButton)e.Row.Cells[0].FindControl("imgBook");
                    imgBook.ImageUrl = "~/Images/ImageForKM/bookmark_icon3_inactive.png";
                }

                string status = dgvKM.DataKeys[index]["STATUS_ID"].ToString();

                if (status == "0")
                {
                    e.Row.BackColor = Color.Yellow;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSearchBook_Click(object sender, ImageClickEventArgs e)
    {
        Check = "Book";
        this.GetData();
    }
}