﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using System.Reflection;
using DevExpress.XtraReports.UI;
using System.Drawing.Text;
using System.Drawing;


public partial class result_add4 : System.Web.UI.Page
{
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string sREQUESTID = "";
    private static string sFomateDate = ConfigurationManager.AppSettings["FormatDate"] + "";
    private static string sCheckWaterID = "01";

    private const string sService1 = "00002"; //เพิ่มแป้น
    private const string sService2 = "00012"; // ตัดเจียรแป้น
    private const string sService3 = "00014"; //ติดตั้งแผ่นเพลทใหม่
    private const string sService4 = "00004"; //ค่าบริการพ่นสาระสำคัญ
    private const string sService5 = "00016"; //การบริการเร่งด่วน
    private const string sService6 = "00017"; //สภาพรถไม่พร้อมที่จะรับการตวงน้ำ และวัดปริมาตร
    private static string CAR_NOCOMPLETE = ""; //จำนวนที่รถไม่ผ่านการตรวจ
    private static List<string> SERVICEID_CHECKFROM = new List<string>();

    private static List<TData_STDPrice> lstSTDPrice = new List<TData_STDPrice>();

    private static DataTable dtMainData = new DataTable();

    private static string USER_ID = "";
    private static string STATUSFLAG_ID = "";
    private static string VENDOR_ID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        gvwServiceOther.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwServiceOther_HtmlDataCellPrepared);
        gvwServiceOther.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwServiceOther_HtmlRowPrepared);

        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        else
        {
            if (!IsPostBack)
            {
                USER_ID = Session["UserID"] + "";
                ListDataCheckFrom();
                string strREQID = Request.QueryString["strRQID"];
                if (!string.IsNullOrEmpty(strREQID + ""))
                {
                    lstSTDPrice = new List<TData_STDPrice>();
                    dtMainData = new DataTable();

                    string[] arrREQID = STCrypt.DecryptURL(strREQID);
                    sREQUESTID = arrREQID[0];
                    if (!string.IsNullOrEmpty(sREQUESTID))
                    {
                        ListStandartPrice();
                        COUNTSERVICE_CARNOCOMPLETE(arrREQID[0]);
                        ListDataToPage(arrREQID[0]);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                    }
                  

                }
            }
        }
    }

    void gvwServiceOther_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {

        if (e.KeyValue != null)
        {
            if (SERVICEID_CHECKFROM.Contains(e.KeyValue.ToString()))
            //if(e.Row)
            {
                for (int i = 0; i < gvwServiceOther.VisibleColumns.Count - 1; i++)
                {

                    if (gvwServiceOther.Columns[i].Caption == "ประเภท")
                    {
                        if (e.Row.Cells.Count > 1)
                        {
                            e.Row.Cells[0].ColumnSpan = 3;
                        }
                    }

                    if (gvwServiceOther.Columns[i].Caption == "ราคา")
                    {
                        if (e.Row.Cells.Count > 1)
                        {
                            e.Row.Cells[1].Style.Add("display", "none");
                        }

                    }

                    if (gvwServiceOther.Columns[i].Caption == "จำนวน")
                    {
                        if (e.Row.Cells.Count > 1)
                        {
                            e.Row.Cells[2].Style.Add("display", "none");
                        }

                    }
                }
                //e.Row.Cells[i].Style.Add("display", "none");
            }

        }
    }

    void gvwServiceOther_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //if (e.KeyValue.ToString() == "00018" || e.KeyValue.ToString() == "00019")
        //{
        //    e.DataColumn.DataItemTemplate. =
        //}


        ASPxLabel lblSERVICECHARGE = gvwServiceOther.FindRowCellTemplateControl(e.VisibleIndex, null, "lblSERVICECHARGE") as ASPxLabel;
        ASPxLabel lblCal_Show = gvwServiceOther.FindRowCellTemplateControl(e.VisibleIndex, null, "lblCal_Show") as ASPxLabel;
        ASPxTextBox txtItem = gvwServiceOther.FindRowCellTemplateControl(e.VisibleIndex, null, "txtItem") as ASPxTextBox;
        ASPxTextBox lblCal = gvwServiceOther.FindRowCellTemplateControl(e.VisibleIndex, null, "lblCal") as ASPxTextBox;
        ASPxCheckBox chkService = gvwServiceOther.FindRowCellTemplateControl(e.VisibleIndex, null, "chkService") as ASPxCheckBox;

        lblSERVICECHARGE.ClientInstanceName = lblSERVICECHARGE.ID + "_" + e.VisibleIndex;
        lblCal_Show.ClientInstanceName = lblCal_Show.ID + "_" + e.VisibleIndex;
        txtItem.ClientInstanceName = txtItem.ID + "_" + e.VisibleIndex;
        lblCal.ClientInstanceName = lblCal.ID + "_" + e.VisibleIndex;
        chkService.ClientInstanceName = chkService.ID + "_" + e.VisibleIndex;

    }

    private void ListDataToPage(string sREQID)
    {
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.STATUS_FLAG,TRQ.REQUEST_DATE,TRQ.SERVICE_DATE,TRQ.APPROVE_DATE,TRQ.STRUCKID,TRQ.VEH_ID,TRQ.TU_ID,TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS,TRQ.TOTLE_CAP,TRQ.TOTLE_SERVCHAGE,TRQ.CCHECKING_WATER,
                        TRQ.WATER_EXPIRE_DATE as DWATEREXPIRE,TRK.SCARTYPEID,
                        TCAT.CARCATE_NAME,
                        TRQT.REQTYPE_ID,TRQT.REQTYPE_NAME,
                        TCAS.CAUSE_ID,TCAS.CAUSE_NAME,TRQ.REMARK_CAUSE,
                        TVD.SVENDORID, NVL(TVD.SABBREVIATION,TRK.SOWNER_NAME) as SABBREVIATION,
                        TUS.SFIRSTNAME,TUS.SLASTNAME
                        FROM TBL_REQUEST TRQ 
                        INNER JOIN TTRUCK TRK ON TRQ.STRUCKID = TRK.STRUCKID
                        LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TRK.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                        INNER JOIN TBL_REQTYPE TRQT ON TRQT.REQTYPE_ID = TRQ.REQTYPE_ID AND  TRQT.ISACTIVE_FLAG = 'Y'
                        LEFT JOIN TBL_CAUSE TCAS ON TCAS.CAUSE_ID = TRQ.CAUSE_ID
                        LEFT JOIN TVENDOR TVD ON TVD.SVENDORID = TRQ.VENDOR_ID
                        LEFT JOIN TUSER TUS ON TUS.SUID = TRQ.APPOINTMENT_BY
                        WHERE TRQ.REQUEST_ID = '{0}'";

        dtMainData = new DataTable();
        dtMainData = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        SetDataToPage();
    }

    private void SetDataToPage_T4(string sREQID)
    {
        //เซ็ตกริด
        string Query = @"SELECT ROWNUM as NROWS,TSM.SERVICE_ID,TSM.UNIT,TSM.FIX_FLAG,SVC.SERVICECHARGE,
                       --CASE WHEN TSM.SERVICE_ID = '00002' OR TSM.SERVICE_ID='00011'  THEN TSM.SERVICE_NAME||' '||TRI2.NITEM||' '||TSM.UNIT ELSE TSM.SERVICE_NAME END as sDetail,
                       --CASE WHEN TSM.SERVICE_ID = '00002' THEN ';รายละเอียดการ'||TSM.SERVICE_NAME||' ('||TRI.NITEM||' x '||SVC.SERVICECHARGE||')'   ELSE null END
                       --||' '||CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN '(ยกเว้นค่าบริการ)'  ELSE  null END  as sDetail,
                     --  CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN 0 ELSE TRI2.NPRICE END AS NPRICE,
                       TSM.SERVICE_NAME ,
                       CASE WHEN TSM.SERVICE_ID = '00002' THEN 1 ELSE 0 END AS NSETORDER,TRI2.NITEM as USERITEM,TRI2.NPRICE,TO_CHAR(TRI2.NPRICE,'9,999,999,999') AS NPRICE_SHOW,
                       TSM.CHECKFROM,'ค่าธรรมเนียมเพิ่มเติม' as TEXT ,'บาท' as UNITS
                       FROM TBL_SERVICE_MASTERDATA TSM 
                       --LEFT JOIN TBL_REQUEST_ITEM TRI ON TSM.SERVICE_ID = TRI.SERVICE_ID
                       LEFT JOIN TBL_SERVICECOST SVC
                       ON TSM.SERVICE_ID = SVC.SERVICE_ID AND TSM.SERVICE_ID <> '00001'
                       LEFT JOIN 
                       ( SELECT * FROM
                            TBL_REQUEST_ITEM2  WHERE REQUEST_ID = '" + sREQID + @"' 
                       )
                       TRI2
                       ON TRI2.SERVICE_ID = TSM.SERVICE_ID
                       WHERE TSM.ISACTIVE_FLAG = 'Y' AND TSM.ISACTIVE_FLAG = 'Y' AND (TSM.FIX_FLAG = 'N' OR  TSM.CHECKFROM = 'Y' )
                       
                      -- WHERE TRI.REQUEST_ID = '{0}'  AND TSM.ISACTIVE_FLAG = 'Y' AND NVL(TRI.CTYPE_ITEM,'99') <> '1'
                       ORDER BY TSM.SERVICE_ID ASC";
        DataTable dt = CommonFunction.Get_Data(strConn, Query);
        gvwServiceOther.DataSource = dt;
        gvwServiceOther.DataBind();

//        //เว็ตค่า Total
//        DataTable dtTotal = CommonFunction.Get_Data(strConn, @"SELECT '1',SUM(TRI2.NPRICE) as NPRICE,TO_CHAR(SUM(TRI2.NPRICE),'9,999,999,999') AS NPRICE_SHOW
//                       FROM TBL_SERVICE_MASTERDATA TSM 
//                       LEFT JOIN TBL_SERVICECOST SVC
//                       ON TSM.SERVICE_ID = SVC.SERVICE_ID AND TSM.SERVICE_ID <> '00001'
//                       LEFT JOIN 
//                       ( SELECT * FROM
//                            TBL_REQUEST_ITEM2  WHERE REQUEST_ID = '" + sREQID + @"' 
//                       )
//                       TRI2
//                       ON TRI2.SERVICE_ID = TSM.SERVICE_ID
//                       WHERE TSM.FIX_FLAG = 'N' AND TSM.ISACTIVE_FLAG = 'Y'  OR TSM.CHECKFROM = 'Y'
//                       GROUP BY '1'");
//        lblTotal_SHOW.Text = dtTotal.Rows[0]["NPRICE_SHOW"] + "";
//        lblTotal.Text = dtTotal.Rows[0]["NPRICE"] + "";

        //เซ็ตค่า สภาพไม่พร้อมที่จะตวงน้ำ

        DataTable dtTWATER = CommonFunction.Get_Data(strConn, @"SELECT * FROM TBL_REQUEST_ITEM2  WHERE REQUEST_ID = '" + sREQID + "' AND SERVICE_ID = '00017'");

        //กรณีที่ Userเปลี่ยนข้อมูลจะใช้ dtTWATER หรือในกรณีที่ใช้ของตัวเดิม TBL_REQUEST_ITEM2
        if (dtTWATER.Rows.Count > 0)
        {
            ckbCarNotCheckingWater.Checked = true;

            decimal std = 0;
            if (!string.IsNullOrEmpty(dtTWATER.Rows[0]["NITEM"] + ""))
            {
                txtCarNoComplete.Text = dtTWATER.Rows[0]["NITEM"] + "";
                std = !string.IsNullOrEmpty(dtTWATER.Rows[0]["NITEM"] + "") ? decimal.Parse(dtTWATER.Rows[0]["NITEM"] + "") : 0;
            }
            else
            {
                txtCarNoComplete.Text = CAR_NOCOMPLETE;
                std = !string.IsNullOrEmpty(CAR_NOCOMPLETE) ? decimal.Parse(CAR_NOCOMPLETE) : 0;
            }
            decimal ss = (GetStdPrice(sService6) * std);

            lblCarnocomplete.Text = ConvertFormatNumber(ss, 0);
            txtshowCarnocomplete.Text = GetStdPrice(sService6) + "";
            txtCarNoComplete.ClientEnabled = true;
        }
        else
        {
            //กรณีที่ USer ยังไม่เคยมีข้อมูล ใน TBL_REQUEST_ITEM2
            decimal std = 0;
            if (CAR_NOCOMPLETE != "0")
            {
                ckbCarNotCheckingWater.Checked = true;
                txtCarNoComplete.Text = CAR_NOCOMPLETE;
                std = !string.IsNullOrEmpty(CAR_NOCOMPLETE) ? decimal.Parse(CAR_NOCOMPLETE) : 0;
                decimal ss = (GetStdPrice(sService6) * std);

                lblCarnocomplete.Text = ConvertFormatNumber(ss, 0);
                txtshowCarnocomplete.Text = (GetStdPrice(sService6) * std) + "";
                txtCarNoComplete.ClientEnabled = true;
            }
            else
            {
                txtCarNoComplete.ClientEnabled = false;
                ckbCarNotCheckingWater.Checked = false;
            }


            
        }
        //เซ็ตจำนวน Row
        txtgvwrowcount.Text = dt.Rows.Count + "";

    }

    private void SetDataToPage()
    {
        if (dtMainData.Rows.Count > 0)
        {
            DataRow dr = null;
            dr = dtMainData.Rows[0];
            decimal nTemp = 0;

            if (dr["STATUS_FLAG"] + "" == "10")
            {
                btnSaveAndReportT4.ClientVisible = false;
               
            }
            else
            {
                btnSaveAndReportT4.ClientVisible = true;
               
            }

            STATUSFLAG_ID = dr["STATUS_FLAG"] + "";
            VENDOR_ID = dr["SVENDORID"] + "";

            lblREQUEST_DATE.Text = !string.IsNullOrEmpty(dr["REQUEST_DATE"] + "") ? Convert.ToDateTime(dr["REQUEST_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblDWATEREXPIRE.Text = !string.IsNullOrEmpty(dr["DWATEREXPIRE"] + "") ? Convert.ToDateTime(dr["DWATEREXPIRE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblSERVICE_DATE.Text = !string.IsNullOrEmpty(dr["SERVICE_DATE"] + "") ? Convert.ToDateTime(dr["SERVICE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "-";
            lblAPPOINTMENT_BY_DATE.Text = (!string.IsNullOrEmpty(dr["APPROVE_DATE"] + "") ? Convert.ToDateTime(dr["APPROVE_DATE"] + "", new CultureInfo("th-TH")).ToString(sFomateDate, new CultureInfo("th-TH")) : "") +
                                          (!string.IsNullOrEmpty(dr["SFIRSTNAME"] + "") && !string.IsNullOrEmpty(dr["SLASTNAME"] + "") ? " - คุณ" + dr["SFIRSTNAME"] + " " + dr["SLASTNAME"] : "-");


            lblREQTYPE_NAME.Text = dr["REQTYPE_NAME"] + "";
            lblCAUSE_NAME.Text = dr["CAUSE_NAME"] + "" + (!string.IsNullOrEmpty(dr["REMARK_CAUSE"] + "") ? (" (" + dr["REMARK_CAUSE"] + ")") : "");
            lblVendorName.Text = dr["SABBREVIATION"] + "";
            lblTypeCar.Text = dr["CARCATE_NAME"] + "";
            lblTotalCap.Text = (decimal.TryParse(dr["TOTLE_CAP"] + "", out nTemp) ? nTemp : 0).ToString(SystemFunction.CheckFormatNuberic(0));
            lblREGISTERNO.Text = dr["VEH_NO"] + "" + (!string.IsNullOrEmpty(dr["TU_NO"] + "") ? "/" + dr["TU_NO"] + "" : "");


            if (dr["REQTYPE_ID"] + "" == sCheckWaterID)
            {
                // set data to grid
                string sTruckID = "";
                switch (dr["SCARTYPEID"] + "")
                {
                    case "0": sTruckID = dr["VEH_ID"] + ""; break; // 10 ล้อ
                    case "3": sTruckID = dr["TU_ID"] + ""; break; // หัวลาก
                }
            }

            /**********/
            SetDataPriceToPage();

            SetDataToPage_T4(sREQUESTID);
        }
    }

    // ส่วนข้างล่าง
    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});"); return;
        }

        string[] spara = e.Parameter.Split(';');
        switch (spara[0])
        {
            
        case "SAVET4": 
            AddData();
            SetDataToPage_T4(sREQUESTID);
            ListDataToReport();
            //CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
            break;
            case "RedirectT1": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectImg": xcpn.JSProperties["cpRedirectTo"] = "CarImageAdd.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
        };
    }

    private void ListStandartPrice()
    {
        string sql = @"SELECT  TSM.SERVICE_ID,TSM.SERVICE_NAME,TSC.SERVICECHARGE,TSC.UNIT
 FROM TBL_SERVICE_MASTERDATA TSM LEFT JOIN TBL_SERVICECOST  TSC ON TSC.SERVICE_ID = TSM.SERVICE_ID
 WHERE TSM.ISACTIVE_FLAG = 'Y' AND TSM.SERVICE_ID IN ('" + CommonFunction.ReplaceInjection(sService1) + "','" + CommonFunction.ReplaceInjection(sService2) + "','" + CommonFunction.ReplaceInjection(sService3) + "','" + CommonFunction.ReplaceInjection(sService4) + "','" + CommonFunction.ReplaceInjection(sService5) + "','" + CommonFunction.ReplaceInjection(sService6) + "') ";

        lstSTDPrice = new List<TData_STDPrice>();
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);

        if (dt.Rows.Count > 0)
        {
            lstSTDPrice = SystemFunction.DataTableConvertToList<TData_STDPrice>(dt);
        }

    }

    private decimal GetStdPrice(string sServiceID)
    {
        decimal sResult = 0;

        var query = lstSTDPrice.Where(w => w.SERVICE_ID == sServiceID).FirstOrDefault();
        if (query != null)
            sResult = query.SERVICECHARGE;


        return sResult;
    }

    private string ConvertFormatNumber(decimal value, int ndecimal)
    {
        return value > 0 ? value.ToString(SystemFunction.CheckFormatNuberic(ndecimal)) : "0";
    }

    private string ConvertFormatNumber(string value, int ndecimal)
    {
        decimal nTemp = 0;
        bool cdecimal = decimal.TryParse(value, out nTemp);
        nTemp = 0;
        if (cdecimal)
        {
            nTemp = decimal.Parse(value);
            return nTemp > 0 ? nTemp.ToString(SystemFunction.CheckFormatNuberic(ndecimal)) : "0";
        }
        else
        {
            return "";
        }
    }

    private decimal ConvertStringToNumber(string value)
    {
        decimal nTemp = 0;
        bool cdecimal = decimal.TryParse(value, out nTemp);
        nTemp = 0;
        if (cdecimal)
        {
            nTemp = decimal.Parse(value);
        }

        return nTemp;
    }

    private void SetDataPriceToPage()
    {
        // ค่าแป้นที่เพิ่มขึ้น {0}*
        //lblStdPriceServiec1.Text = ConvertFormatNumber(GetStdPrice(sService1),0);
        //lblStdPriceServiec2.Text = ConvertFormatNumber(GetStdPrice(sService2), 0);
        //lblStdPriceServiec3.Text = ConvertFormatNumber(GetStdPrice(sService3), 0);
        //lblStdPriceServiec4.Text = ConvertFormatNumber(GetStdPrice(sService4), 0);
        //lblStdPriceServiec5.Text = ConvertFormatNumber(GetStdPrice(sService5), 0);
        lblStdPriceServiec6.Text = GetStdPrice(sService6) + "";
    }

    private void AddData()
    {
        //sql TBL_REQUEST 
        string sqlUpdate_TBL_RQ = @"UPDATE TBL_REQUEST SET SREMARK_OTHER_SERVICET4 = :SREMARK_OTHER_SERVICET4 WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'";


        //sql TBL_REQUEST_ITEM2
        string sqlInsert_RQ_ITEM = @"INSERT INTO TBL_REQUEST_ITEM2(REQUEST_ID,SERVICE_ID,NITEM,NPRICE,CTYPE_ITEM)
                                           VALUES(:REQUEST_ID,:SERVICE_ID,:NITEM,:NPRICE,:CTYPE_ITEM)";

        //TBL_REQUEST_ITEM
        string sqlDelREQ_ITEM = @"DELETE FROM TBL_REQUEST_ITEM2 WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "'  AND NVL(CTYPE_ITEM,'99') = '1'";
        SystemFunction.SQLExecuteNonQuery(strConn, sqlDelREQ_ITEM);


        using (OracleConnection con = new OracleConnection(strConn))
        {
            con.Open();
            using (OracleCommand com = new OracleCommand(sqlInsert_RQ_ITEM, con))
            {
                #region tbl RQ_ITEM

                int gvwcount = 0;
                if (!string.IsNullOrEmpty(txtgvwrowcount.Text))
                {
                    gvwcount = int.Parse(txtgvwrowcount.Text);
                }

                for (int i = 0; i < gvwcount; i++)
                {

                    string ServiceID = gvwServiceOther.GetRowValues(i, "SERVICE_ID") + "";

                    ASPxTextBox txtItem = gvwServiceOther.FindRowCellTemplateControl(i, null, "txtItem") as ASPxTextBox;
                    ASPxTextBox lblCal = gvwServiceOther.FindRowCellTemplateControl(i, null, "lblCal") as ASPxTextBox;
                    ASPxCheckBox chkService = gvwServiceOther.FindRowCellTemplateControl(i, null, "chkService") as ASPxCheckBox;

                    com.Parameters.Clear();

                    if (SERVICEID_CHECKFROM.Contains(ServiceID))
                    {
                        if (chkService.Checked == true)
                        {
                            com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                            com.Parameters.Add(":SERVICE_ID", OracleType.VarChar).Value = ServiceID;
                            com.Parameters.Add(":NITEM", OracleType.Number).Value = 1;
                            com.Parameters.Add(":NPRICE", OracleType.Number).Value = int.Parse(lblCal.Text);
                            com.Parameters.Add(":CTYPE_ITEM", OracleType.Number).Value = "1";
                            com.ExecuteNonQuery();
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtItem.Text) && !string.IsNullOrEmpty(lblCal.Text))
                        {
                            com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                            com.Parameters.Add(":SERVICE_ID", OracleType.VarChar).Value = ServiceID;
                            com.Parameters.Add(":NITEM", OracleType.Number).Value = int.Parse(txtItem.Text);
                            com.Parameters.Add(":NPRICE", OracleType.Number).Value = int.Parse(lblCal.Text);
                            com.Parameters.Add(":CTYPE_ITEM", OracleType.VarChar).Value = "1";
                            com.ExecuteNonQuery();
                        }
                        else
                        {

                        }
                    }
                }
                // กรณีรถไม่ผ่านการตรวจสภาพ
                if (ckbCarNotCheckingWater.Checked == true)
                {
                    com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQUESTID;
                    com.Parameters.Add(":SERVICE_ID", OracleType.VarChar).Value = sService6;
                    if (!string.IsNullOrEmpty(txtCarNoComplete.Text))
                    {
                        com.Parameters.Add(":NITEM", OracleType.Number).Value = int.Parse(txtCarNoComplete.Text);
                    }
                    else
                    {
                        com.Parameters.Add(":NITEM", OracleType.Number).Value = DBNull.Value;
                    }
                    com.Parameters.Add(":NPRICE", OracleType.Number).Value = int.Parse(txtshowCarnocomplete.Text);
                    com.Parameters.Add(":CTYPE_ITEM", OracleType.Number).Value = "1";
                    // com.Parameters.Add(":STATUS_FLAG", OracleType.VarChar).Value = "07";
                    com.ExecuteNonQuery();
                }

                #endregion
            }


            //หมายเหตุ
            using (OracleCommand com = new OracleCommand(sqlUpdate_TBL_RQ, con))
            {
                com.Parameters.Clear();
                if (!string.IsNullOrEmpty(txtRemark.Text.Trim()))
                    com.Parameters.Add(":SREMARK_OTHER_SERVICET4", OracleType.VarChar).Value = txtRemark.Text.Trim();
                else
                    com.Parameters.Add(":SREMARK_OTHER_SERVICET4", OracleType.VarChar).Value = DBNull.Value;
                com.ExecuteNonQuery();
            }

        }

        // Send mail
        if (ckbSendMailVendor.Checked)
        {
            if (dtMainData.Rows.Count > 0)
            {
                SendMailToVendor(sREQUESTID, dtMainData.Rows[0]["SABBREVIATION"] + "", dtMainData.Rows[0]["SVENDORID"] + "", txtRemark.Text.Trim());
            }
        }
    }

    private bool SendMailToVendor(string sREQID, string sVendorName, string sVendorID, string sRemark)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ค่าธรรมเนียมเพิ่ม";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = SystemFunction.GetUserMailForSend(USER_ID, STATUSFLAG_ID, VENDOR_ID, "M");
            //DataTable dt = new DataTable();
            //dt = SystemFunction.GetVendorToRecieveMail(sVendorID, "55");
            //foreach (DataRow dr in dt.Rows)
            //{
            //    if (!string.IsNullOrEmpty(dr["SEMAIL"] + ""))
            //    {
            //        _to += dr["SEMAIL"] + ";";
            //    }
            //}
        }

        #region html
        sHTML = @"<table width=""100%"" border=""0"" cellpadding=""3"" cellspacing=""1"">
            <tr>
                <td style=""font-size:15px;"">
                      เรียน  : " + sVendorName + @" 
                </td>
            </tr>
            <tr>
                <td style=""font-size:15px;"">
                      " + sRemark + @"
                </td>
            </tr>
          </table>";
        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(strConn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }

    private void ListDataToReport()
    {
        rpt_Invoice_RQ report = new rpt_Invoice_RQ();
        report.Name = sREQUESTID + "_" + "Invoice";

        DataTable dtSource = new DataTable();
        DataTable dtParameter = new DataTable();

        dtSource = ListData_Request_Item(sREQUESTID);
        dtParameter = ListData_Parameter_Report(sREQUESTID);

        report.DataSource = dtSource;

        DataRow dr = null;
        if (dtParameter.Rows.Count > 0)
        {
            dr = dtParameter.Rows[0];
            string VENDOR = !string.IsNullOrEmpty(dr["VENDOR_ID"]+"") ? "(" + dr["VENDOR_ID"] + ")" : "";
            report.Parameters["sReportNo"].Value = dr["REQUEST_ID"] + "";
            report.Parameters["sDay"].Value = DateTime.Now.Day + "";
            report.Parameters["sMonth"].Value = DateTime.Now.ToString("MMMM", new CultureInfo("th-TH"));
            report.Parameters["sYearTH"].Value = DateTime.Now.ToString("yyyy", new CultureInfo("th-TH"));
            report.Parameters["sCompanyName"].Value = dr["SABBREVIATION"] + VENDOR;
            report.Parameters["sAddress"].Value = dr["SNO"] + " " + dr["SDISTRICT"] + " " + dr["SREGION"] + " " + dr["SPROVINCE"] + " " + dr["SPROVINCECODE"] + "";
            report.Parameters["sRegisterNo"].Value = dr["SREGISTRATION"] + "";
            report.Parameters["sChasisNo"].Value = dr["VEH_CHASSIS"] + "" + (dr["TU_CHASSIS"] + "" != "" ? ("/" + dr["TU_CHASSIS"]) : "");
        }

        PrivateFontCollection fontColl = new PrivateFontCollection();
        //  fontColl.AddFontFile(Server.MapPath("~/font/THSarabun Bold.ttf"));
        fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

        foreach (Band b in report.Bands)
        {
            foreach (XRControl c in b.Controls)
            {
                c.Font = new Font(fontColl.Families[0].Name, 12, c.Font.Style);// c.Font.Size  
            }
        }

        ((XRTableCell)report.FindControl("xrTableCell1", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell2", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell3", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell5", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell6", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell8", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell9", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell11", true)).Font = new Font(fontColl.Families[0].Name, 12);
        ((XRTableCell)report.FindControl("xrTableCell4", true)).Font = new Font(fontColl.Families[0].Name, 12);


        string fileName = sREQUESTID + "_Invoice_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }

    private DataTable ListData_Request_Item(string _sreqid)
    {
        DataTable dt = new DataTable();
        string sql = @"SELECT ROWNUM as NROWS,TSM.SERVICE_ID,TRI.NITEM,
                       TSM.SERVICE_NAME||' '||TRI.NITEM||' '|| CASE WHEN TSM.SERVICE_ID = '00017' THEN 'ครั้ง' ELSE  TSM.UNIT END ||' '||CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN '(ยกเว้นค่าบริการ)'  ELSE  null END  as sDetail,
                        CASE WHEN TRI.FLAGE_SERVICECANCEL = 'Y' THEN 0 ELSE TRI.NPRICE END AS NPRICE,
                        CASE WHEN TSM.SERVICE_ID = '00002' THEN 1 ELSE 0 END AS NSETORDER
                        FROM TBL_SERVICE_MASTERDATA TSM 
                        INNER JOIN TBL_REQUEST_ITEM2 TRI ON TSM.SERVICE_ID = TRI.SERVICE_ID
                        WHERE TRI.REQUEST_ID = '{0}'  AND TSM.ISACTIVE_FLAG = 'Y' AND NVL(TRI.CTYPE_ITEM,'99') = '1' AND TRI.NITEM <> 0
                        ORDER BY NROWS ASC, NSETORDER ASC,TSM.SERVICE_NAME ASC";

        dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(_sreqid)));

        if (dt.Rows.Count > 0)
        {
            DataRow dr = null; // เต็มที่ได้ 14 row
            int ncheck = (12 - dt.Rows.Count);
            for (int i = 0; i < ncheck; i++)
            {
                dr = dt.NewRow();
                dr["SERVICE_ID"] = "9999";
                dt.Rows.Add(dr);
            }
        }

        return dt;
    }

    private DataTable ListData_Parameter_Report(string _sreqid)
    {
        DataTable dt = new DataTable();
        //set parameters 
        string sql = @"SELECT TRQ.REQUEST_ID,TRQ.VENDOR_ID,NVL(TVD.SABBREVIATION,TRK.SOWNER_NAME) as SABBREVIATION,TVS.SNO,TVS.SDISTRICT,TVS.SREGION,TVS.SPROVINCE,TVS.SPROVINCECODE,
                        TRT.REQTYPE_NAME,TCS.CAUSE_NAME,
                        CASE WHEN TRQ.TU_NO IS NOT NULL THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
                        TCC.CARCATE_NAME,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,
                        TRQ.VEH_NO,TRQ.TU_NO,TRQ.VEH_CHASSIS,TRQ.TU_CHASSIS
                         FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
                         LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
                         LEFT JOIN TVENDOR_SAP TVS ON TVS.SVENDORID = TRQ.VENDOR_ID
                         LEFT JOIN TTRUCK TRK ON TRK.STRUCKID = NVL(TRQ.TU_ID,TRQ.VEH_ID)
                         WHERE TRQ.REQUEST_ID = '{0}'";

        dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(_sreqid)));

        return dt;
    }

    #region class
    [Serializable]

    class TData_STDPrice
    {
        public string SERVICE_ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public decimal SERVICECHARGE { get; set; }
        public string UNIT { get; set; }
    }
    #endregion

    protected void btnSaveAndReportT4_Click(object sender, EventArgs e)
    {
        AddData();
        SetDataToPage_T4(sREQUESTID);
        ListDataToReport();
    }

    void ListDataCheckFrom()
    {
        SERVICEID_CHECKFROM.Clear();

        string Query = @"
SELECT SERVICE_ID,CHECKFROM FROM
(
    SELECT SERVICE_ID, ISACTIVE_FLAG,FIX_FLAG,CHECKFROM 
    FROM TBL_SERVICE_MASTERDATA 
    WHERE ISACTIVE_FLAG = 'Y' AND FIX_FLAG = 'N' OR CHECKFROM = 'Y'
)MS
WHERE CHECKFROM = 'Y' 
";
        DataTable dt = CommonFunction.Get_Data(strConn, Query);

        string SERVICE_ID = "";
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                SERVICEID_CHECKFROM.Add(SERVICE_ID = dt.Rows[i]["SERVICE_ID"] + "");


                //SERVICE_ID += "," + dt.Rows[i]["SERVICE_ID"] + "";
            }
            //SERVICE_ID = SERVICE_ID.Remove(0, 1);
            //SERVICEID_CHECKFROM = SERVICE_ID.Split(',');
        }
    }

    void COUNTSERVICE_CARNOCOMPLETE(string Req_ID)
    {
        DataTable dt = CommonFunction.Get_Data(strConn, "SELECT REQUEST_ID,NVERSION FROM TBL_TIME_INNER_CHECKINGS WHERE REQUEST_ID = '" + Req_ID + "' AND CCHECKING_WATER = 'N' GROUP BY REQUEST_ID,NVERSION");
        DataTable dt2 = CommonFunction.Get_Data(strConn, "SELECT REQUEST_ID,NVERSION FROM TBL_SCRUTINEERINGLIST WHERE REQUEST_ID = '" + Req_ID + "' AND STATUS_COMFIRM = 'N' GROUP BY REQUEST_ID,NVERSION");

        CAR_NOCOMPLETE = (dt.Rows.Count + dt2.Rows.Count) + "";
    }
}