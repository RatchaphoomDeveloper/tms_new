﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_suppliant_lst.aspx.cs" Inherits="admin_suppliant_lst" StylesheetTheme="Aqua" %>

<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath.GetValue() == "" || txtFilePath.GetValue() == null;
            uploader.SetClientVisible(bool);
            txtFileName.SetClientVisible(!bool);
            btnView.SetEnabled(!bool);
            btnDelFile.SetEnabled(!bool);
        }

    </script>
</asp:Content>
<%--if(gvw.IsEditing()){if(txtCheckFinal.GetValue() == "0"){PageControl.GetTab(5).SetEnabled(false);}};--%>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <script type="text/javascript">
        function closeModal() {
            $('#ShowScore').modal('hide');
        }

        function showModal() {
            $('#ShowScore').modal('show');
        }

        function closeModalAttach() {
            $('#ShowAttach').modal('hide');
        }

        function ShowLoginWindow() {
            pcLogin.Show();
        }
    </script>
    <asp:UpdatePanel ID="asdawe" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
                CausesValidation="False" OnLoad="xcpn_Load">
                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; inEndRequestHandler();}">
                </ClientSideEvents>
                <PanelCollection>
                    <dx:PanelContent ID="PanelContent2" runat="server">
                        <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                        <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                        <table width="100%">
                            <tr>
                                <td width="20%">
                                    <dx:ASPxTextBox ID="hideAppealID" runat="server" Width="10px" ClientInstanceName="hideAppealID"
                                        ClientVisible="false">
                                    </dx:ASPxTextBox>
                                    <dx:ASPxTextBox ID="hideReduceID" runat="server" Width="10px" ClientInstanceName="hideReduceID"
                                        ClientVisible="false">
                                    </dx:ASPxTextBox>
                                    <dx:ASPxTextBox ID="hideSentence" runat="server" Width="10px" ClientInstanceName="hideSentence"
                                        ClientVisible="false">
                                    </dx:ASPxTextBox>
                                    <dx:ASPxTextBox ID="hideCheckFinal" runat="server" Width="10px" ClientInstanceName="hideCheckFinal"
                                        ClientVisible="false">
                                    </dx:ASPxTextBox>
                                </td>
                                <td>
                                    <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ทะเบียนรถ,เลขที่สัญญา,พนักงานขับรถ">
                                    </dx:ASPxTextBox>
                                </td>
                                <td style="width:120px; text-align:right">
                                    <asp:Label ID="lblDate" runat="server" Text="วันที่พิจารณา :"></asp:Label>
                                </td>
                                <td style="width:200px">
                                    <%--<dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte" title="ช่วงวันที่เกิดเหตุ">
                                    </dx:ASPxDateEdit>--%>
                                    <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center; width:200px"></asp:TextBox>
                                </td>
                                <td>
                                    -
                                </td>
                                <td style="width:200px">
                                    <%--<dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte" title="ช่วงวันที่เกิดเหตุ">
                                    </dx:ASPxDateEdit>--%>
                                    <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center; width:200px"></asp:TextBox>
                                </td>
                                <td>
                                    <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus" Width="110px"
                                        SelectedIndex="0">
                                        <Items>
                                            <dx:ListEditItem Selected="true" Text="- สถานะ -" />
                                            <dx:ListEditItem Text="กำลังพิจารณา" Value="1" />
                                            <dx:ListEditItem Text="ตัดสิน" Value="2" />
                                            <dx:ListEditItem Text="รอพิจารณา" Value="3" />
                                        </Items>
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
                                    <asp:Button ID="btnExport" Text="Export" runat="server" CssClass="form-control" OnClick="btnExport_Click"/>
            </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExport" />
                                        </Triggers>
            </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="8">
                                    *H = Hold
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                        DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                        OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                                Width="5%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SAPPEALID" ShowInCustomizationForm="True"
                                                Visible="False" VisibleIndex="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DINCIDENT" VisibleIndex="2"
                                                Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการขนส่ง" FieldName="SVENDORNAME"
                                                ShowInCustomizationForm="True" VisibleIndex="2" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ประเภท" FieldName="SPROCESSNAME" ShowInCustomizationForm="True"
                                                VisibleIndex="3" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="หัวข้อปัญหา" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                                VisibleIndex="3" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                        VisibleIndex="8" Width="8%">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILREGISTERNO" ShowInCustomizationForm="True"
                                                        VisibleIndex="9" Width="8%">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <dx:GridViewBandColumn Caption="คะแนนที่ตัด" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="NPOINT" ShowInCustomizationForm="True"
                                                        VisibleIndex="8">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_POINT" ShowInCustomizationForm="True"
                                                        VisibleIndex="9">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <dx:GridViewBandColumn Caption="ค่าปรับ" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="COST" ShowInCustomizationForm="True"
                                                        VisibleIndex="8">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_COST" ShowInCustomizationForm="True"
                                                        VisibleIndex="9">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <dx:GridViewBandColumn Caption="ระงับ พขร." ShowInCustomizationForm="True" VisibleIndex="7">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                        VisibleIndex="8">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="หลัง" FieldName="TOTAL_DISABLE_DRIVER" ShowInCustomizationForm="True"
                                                        VisibleIndex="9">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <dx:GridViewBandColumn Caption="BLACKLIST" ShowInCustomizationForm="True" VisibleIndex="7">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ก่อน" FieldName="BLACKLIST" ShowInCustomizationForm="True"
                                                        VisibleIndex="8">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="หลัง" FieldName="BLACKLIST_A" ShowInCustomizationForm="True"
                                                        VisibleIndex="9">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </dx:GridViewBandColumn>
                                            <%--<dx:GridViewDataTextColumn Caption="คะแนน<br>ที่ตัด" FieldName="NPOINT" ShowInCustomizationForm="True"
                                                VisibleIndex="10" Width="5%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ค่าปรับ" FieldName="COST" ShowInCustomizationForm="True"
                                                VisibleIndex="10" Width="5%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ระงับ พขร.<br>(วัน)" FieldName="DISABLE_DRIVER"
                                                ShowInCustomizationForm="True" VisibleIndex="10" Width="5%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>--%>
                                            <dx:GridViewDataTextColumn Caption="เวลาดำเนินการ<br>(นับจากเกิดเหตุ)" FieldName="DINCIDENT"
                                                ShowInCustomizationForm="True" VisibleIndex="10" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="สถานะ<br>การอุทธรณ์" FieldName="STATUS" ShowInCustomizationForm="True"
                                                VisibleIndex="11" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataColumn Caption=" " Width="10%" CellStyle-Cursor="hand" VisibleIndex="13">
                                                <DataItemTemplate>
                                                    <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="false" Text="พิจารณา" CausesValidation="False"
                                                        Width="100px">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                    </dx:ASPxButton>
                                                </DataItemTemplate>
                                                <CellStyle Cursor="hand">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataTextColumn Caption="CHKSTATUS" FieldName="CHKSTATUS" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="SVENDORID" FieldName="SVENDORID" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="SAPPEALNO" FieldName="SAPPEALNO" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="SPROCESSID" FieldName="SPROCESSID" Visible="false">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxPageControl ID="PageControl" ClientInstanceName="PageControl" runat="server"
                                                                ActiveTabIndex="0" Width="100%">
                                                                <TabPages>
                                                                    <dx:TabPage Name="t1" Text="ข้อมูลทั่วไป">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" border="0" cellpadding="3" cellspacing="2">
                                                                                    <tr>
                                                                                        <td width="20%">
                                                                                            เลขที่คดี
                                                                                        </td>
                                                                                        <td width="30%">
                                                                                            <dx:ASPxLabel ID="lblNo" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td width="20%">
                                                                                            เลขที่ outbound
                                                                                        </td>
                                                                                        <td width="30%">
                                                                                            <dx:ASPxLabel ID="lblOutbound" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            วันที่เกิดเหตุ
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblDate" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td>
                                                                                            ชื่อลูกค้า
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblCustomer" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            สถานที่เกิดเหตุ
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblAddress" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <font color="red">ประวัติการกระทำผิด</font>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            เลขที่สัญญา
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblContractNo" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td>
                                                                                            วันที่เริ่มต้น - สิ้นสุดสัญญา
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblDStart" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                            -
                                                                                            <dx:ASPxLabel ID="lblDEnd" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            กระทำความผิดมาแล้ว
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblNum" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td>
                                                                                            จำนวนคะแนนที่ถูกตัด
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblScore" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Name="t2" Text="หลักฐานของปตท.">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                                    <tr>
                                                                                        <td valign="top" width="30%">
                                                                                            รายละเอียดปัญหา
                                                                                        </td>
                                                                                        <td width="70%">
                                                                                            <dx:ASPxMemo ID="txtDetail2" runat="server" Width="500px" Height="150px">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            หลักฐานการกระทำผิด<br />
                                                                                            (คลิกที่รูปเพื่อขยาย )
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Literal ID="ltrPicture2" runat="server"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            เอกสารอื่นๆ
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Literal ID="ltrDocument2" runat="server"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผู้บันทึกข้อมูล
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="txtNameCreate2" runat="server" Width="170px" ClientEnabled="false">
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Name="t3" Text="หลักฐานของผู้ประกอบการ">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                                    <tr>
                                                                                        <td width="30%">
                                                                                            วันที่ผู้ประกอบการยื่นหลักฐานเพิ่มเติม
                                                                                        </td>
                                                                                        <td width="70%">
                                                                                            <dx:ASPxLabel ID="lblDate3" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            คะแนนที่ตัด
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxLabel ID="lblScore3" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                            &nbsp คะแนน
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            รายละเอียดการขออุทธรณ์
                                                                                        </td>
                                                                                        <td valign="top">
                                                                                            <dx:ASPxMemo ID="txtAppealDetail3" runat="server" Width="500px" Height="150px">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            หลักฐานการกระทำผิด<br />
                                                                                            (คลิกที่รูปเพื่อขยาย )
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Literal ID="ltrPicture3" runat="server"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            เอกสารอื่นๆ
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Literal ID="ltrDocument3" runat="server"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผู้บันทึกข้อมูล
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="txtNameCreate3" runat="server" Width="170px" ClientEnabled="false">
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Name="t4" Text="รข.รับเรื่อง/พิจารณา">
                                                                        
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                                    <tr>
                                                                                        <td width="30%">
                                                                                            วันที่พิจารณาตัดสิน
                                                                                        </td>
                                                                                        <td width="70%">
                                                                                            <dx:ASPxLabel ID="lblDate4" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            สถานะ การรับเรื่องอุทธรณ์
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                                                <ContentTemplate>  
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <dx:ASPxRadioButtonList ID="rblCSENTENCE4" runat="server" Width="300px" Border-BorderStyle="None">
                                                                                                            <ClientSideEvents SelectedIndexChanged="function (s,e){if(s.GetValue() == '2'){cmbSENTENCERNAME.SetClientVisible(true);imbView.SetClientVisible(true);}else{cmbSENTENCERNAME.SetClientVisible(false);imbView.SetClientVisible(false);}; if(s.GetValue() == '1'){rblStatus4.SetEnabled(true);}else{rblStatus4.SetEnabled(false);};if(s.GetValue() == '4'){lblWait.SetClientVisible(true);txtWaitDocument.SetClientVisible(true);}else{lblWait.SetClientVisible(false);txtWaitDocument.SetClientVisible(false);};}">
                                                                                                            </ClientSideEvents>
                                                                                                            <Items>
                                                                                                                <dx:ListEditItem Selected="true" Text="รับเรื่องอุทธรณ์ (รข. พิจารณาเอง)" Value="1" />
                                                                                                                <dx:ListEditItem Text="รับเรื่องอุทธรณ์ (ส่งให้คณะกรรมการพิจารณา)" Value="2" />
                                                                                                                <dx:ListEditItem Text="ไม่รับการอุทธรณ์" Value="3" />
                                                                                                                <dx:ListEditItem Text="ขอเอกสารเพิ่มเติม" Value="4" />
                                                                                                            </Items>
                                                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                                                SetFocusOnError="True" Display="Dynamic">
                                                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                                                </ErrorFrameStyle>
                                                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล" />
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxRadioButtonList>
                                                                                                    </td>
                                                                                                    <td valign="top" style="padding-top: 32px">
                                                                                                        <dx:ASPxComboBox ID="cmbSENTENCERNAME" ClientInstanceName="cmbSENTENCERNAME" Width="150px"
                                                                                                            runat="server" TextField="ID1" ClientVisible="false" ValueField="SSENTENCERCODE"
                                                                                                            DataSourceID="sdsSENTENCERNAME">
                                                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                                                                                SetFocusOnError="True" Display="Dynamic">
                                                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                                                </ErrorFrameStyle>
                                                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล" />
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxComboBox>
                                                                                                        <asp:SqlDataSource ID="sdsSENTENCERNAME" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                                            CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                                            EnableCaching="True" CacheKeyDependency="ckdSENTENCER" SelectCommand="SELECT 'คณะกรรมการชุดที่ ' || ROW_NUMBER () OVER (ORDER BY SSENTENCERCODE) AS ID1,SSENTENCERCODE FROM TSENTENCER WHERE CACTIVE = '1' AND SENTENCER_TYPE_ID = 2 GROUP BY SSENTENCERCODE ">
                                                                                                        </asp:SqlDataSource>
                                                                                                    </td>
                                                                                                    <td valign="top" style="padding-top: 32px">
                                                                                                        <dx:ASPxButton ID="imbView" ClientInstanceName="imbView" runat="server" SkinID="dd"
                                                                                                            ClientVisible="false" CausesValidation="False" AutoPostBack="false" Cursor="pointer"
                                                                                                            EnableDefaultAppearance="False" EnableTheming="False" CssClass="dxeLineBreakFix">
                                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('imbClick');}" />
                                                                                                            <Image Height="16px" Url="Images/document.gif" Width="16px">
                                                                                                            </Image>
                                                                                                        </dx:ASPxButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                                    </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            <dx:ASPxLabel ID="lblWait" runat="server" ClientInstanceName="lblWait" Text="ขอเอกสารเพิ่มเติม"
                                                                                                ClientVisible="false">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxMemo ID="txtWaitDocument" runat="server" ClientInstanceName="txtWaitDocument"
                                                                                                Width="500px" Height="150px" ClientVisible="false">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผลการพิจารณา
                                                                                        </td>
                                                                                        <td>
                                                                                    <asp:UpdatePanel ID="adsfadsf" runat="server">
                                                                                                <ContentTemplate>        <dx:ASPxRadioButtonList ID="rblStatus4" runat="server" ClientInstanceName="rblStatus4" ViewStateMode="Enabled" EnableViewState="true"
                                                                                                Border-BorderStyle="None" >
                                                                                                <Items>
                                                                                                    <dx:ListEditItem Text="ยืนยันผลการพิจารณา (ตัดคะแนน)" Value="2" />
                                                                                                    <dx:ListEditItem Text="พิจารณาให้ไม่มีความผิด (คืนคะแนน)" Value="1" />
                                                                                                    <dx:ListEditItem Text="เปลี่ยนผลการพิจารณา" Value="3" />
                                                                                                </Items>
                                                                                            </dx:ASPxRadioButtonList>
                                                                                            
                                                                                                    <dx:ASPxButton ID="cmdAppeal" runat="server" Text="ผลการพิจารณาใหม่" Width="130px"
                                                                                                        OnClick="cmdAppeal_Click" />
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td colspan="5">
                                                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <dx:ASPxButton ID="cmdAttach" runat="server" Text="แนบไฟล์เอกสาร" Width="130px"
                                                                                                        OnClick="cmdAttach_Click" />
                                                                                                    <asp:Label ID="lblAttachTotal" runat="server" Text="มีไฟล์แนบทั้งหมด {0} เอกสาร"></asp:Label>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </td>
                                                                                    </tr
                                                                                    <tr>
                                                                                        <td>
                                                                                            &nbsp;
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            ข้อมูลประกอบการตัดสิน (ถ้ามี)
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxMemo ID="txtData4" runat="server" Width="500px" Height="150px">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" colspan="2">
                                                                                            <dx:ASPxButton ID="btnConfirm" runat="server" AutoPostBack="false" Text="บันทึก"
                                                                                                CssClass="dxeLineBreakFix" Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }">
                                                                                                </ClientSideEvents>
                                                                                            </dx:ASPxButton>
                                                                                            &nbsp
                                                                                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                                Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Name="t5" Text="คณะกรรมการพิจารณาฯ">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl5" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                                    <tr>
                                                                                        <td width="30%">
                                                                                            วันที่พิจารณาตัดสิน
                                                                                        </td>
                                                                                        <td width="70%">
                                                                                            <dx:ASPxLabel ID="lblDate5" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            คณะกรรมการพิจารณา
                                                                                        </td>
                                                                                        <td>
                                                                                            <%--EncodeHtml="false"--%>
                                                                                            <dx:ASPxCheckBoxList ID="chkSentencer5" runat="server" ValueField="SSENTENCERID"
                                                                                                Border-BorderStyle="None" TextField="FULLNAME" Width="100%" DataSourceID="sdsSentencer">
                                                                                            </dx:ASPxCheckBoxList>
                                                                                            <asp:SqlDataSource ID="sdsSentencer" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                                                SelectCommand="SELECT SSENTENCERID,'คุณ ' || ' ' || FNAME || ' ' || LNAME ||'  ตำแหน่ง '||SPOSITION AS FULLNAME FROM TSENTENCER WHERE SSENTENCERCODE = :SSENTENCERCODE">
                                                                                                <SelectParameters>
                                                                                                    <asp:SessionParameter Name="SSENTENCERCODE" SessionField="ssSSENTENCERCODE" Type="String" />
                                                                                                </SelectParameters>
                                                                                            </asp:SqlDataSource>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผลการพิจารณา
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxRadioButtonList ID="rblStatus5" Border-BorderStyle="None" runat="server">
                                                                                                <Items>
                                                                                                    <dx:ListEditItem Selected="true" Text="ตัดสินให้มีความผิด" Value="2" />
                                                                                                    <dx:ListEditItem Text="ไม่มีความผิด" Value="1" />
                                                                                                </Items>
                                                                                            </dx:ASPxRadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td valign="top">
                                                                                            ข้อมูลประกอบการตัดสิน (ถ้ามี)
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxMemo ID="txtDetail5" runat="server" Height="150px" Width="500px">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" colspan="2">
                                                                                            <dx:ASPxButton ID="btnConfirm1" runat="server" AutoPostBack="false" Text="บันทึก"
                                                                                                CssClass="dxeLineBreakFix" Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Save1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }">
                                                                                                </ClientSideEvents>
                                                                                            </dx:ASPxButton>
                                                                                            &nbsp
                                                                                            <dx:ASPxButton ID="btnCancel1" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                                Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Name="t6" Text="เปลี่ยนคำตัดสิน">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ContentControl6" runat="server" SupportsDisabledAttribute="True">
                                                                                <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                                                                    <tr>
                                                                                        <td width="30%">
                                                                                            วันที่เปลี่ยนคำตัดสิน
                                                                                        </td>
                                                                                        <td width="70%">
                                                                                            <dx:ASPxLabel ID="lblDate6" runat="server">
                                                                                            </dx:ASPxLabel>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxCheckBox ID="chkChange6" ClientInstanceName="chkChange6" runat="server" Text="เปลี่ยนแปลงคำตัดสิน">
                                                                                            </dx:ASPxCheckBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ระบุเหตุผลในการเปลี่ยนแปลงคำตัดสิน <font color="red">*</font>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxMemo ID="txtDetail6" runat="server" Height="150px" Width="500px">
                                                                                            </dx:ASPxMemo>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผลการพิจารณา
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxRadioButtonList ID="rblStatus6" runat="server" Border-BorderStyle="None">
                                                                                                <Items>
                                                                                                    <dx:ListEditItem Selected="true" Text="ตัดสินให้มีความผิด" Value="2" />
                                                                                                    <dx:ListEditItem Text="ไม่มีความผิด" Value="1" />
                                                                                                </Items>
                                                                                            </dx:ASPxRadioButtonList>
                                                                                            <dx:ASPxTextBox ID="txtOldStatus" runat="server" ClientVisible="false">
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            Allowed file types:
                                                                                            <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %>ฯลฯ<br>
                                                                                            Max file size:
                                                                                            <%= Resources.CommonResource.TooltipMaxFileSize1MB %>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            หนังสืออนุมัติให้เปลี่ยนแปลงคำตัดสิน
                                                                                        </td>
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="txtEvidence" runat="server" Width="200px">
                                                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                                                ValidationGroup="ChkAdd">
                                                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                                                </ErrorFrameStyle>
                                                                                                                <RequiredField ErrorText="กรุณาระบุข้อมูล" IsRequired="True" />
                                                                                                                <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล"></RequiredField>
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxUploadControl ID="uplExcel" runat="server" ClientInstanceName="uploader"
                                                                                                            NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                                                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                                                                AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                                            </ValidationSettings>
                                                                                                            <ClientSideEvents TextChanged="function(s,e){uploader.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath.SetValue((e.callbackData+'').split('|')[0]);txtFileName.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                                                                            </ClientSideEvents>
                                                                                                            <BrowseButton Text="แนบไฟล์">
                                                                                                            </BrowseButton>
                                                                                                        </dx:ASPxUploadControl>
                                                                                                        <dx:ASPxTextBox ID="txtFileName" runat="server" Width="300px" ClientInstanceName="txtFileName"
                                                                                                            ClientEnabled="false" ClientVisible="false">
                                                                                                        </dx:ASPxTextBox>
                                                                                                        <dx:ASPxTextBox ID="txtFilePath" runat="server" Width="220px" ClientInstanceName="txtFilePath"
                                                                                                            ClientVisible="false">
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <dx:ASPxButton ID="btnView" runat="server" Text="View" AutoPostBack="false" ClientInstanceName="btnView"
                                                                                                                        CssClass="dxeLineBreakFix" Width="60px" ClientEnabled="false">
                                                                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath.GetValue());}" />
                                                                                                                    </dx:ASPxButton>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <dx:ASPxButton ID="btnDelFile" runat="server" Text="Delete" AutoPostBack="false"
                                                                                                                        ClientInstanceName="btnDelFile" CssClass="dxeLineBreakFix" Width="60px" ClientEnabled="false">
                                                                                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath.GetValue() +';1');}" />
                                                                                                                    </dx:ASPxButton>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            ผู้บันทึกเปลี่ยนแปลงคำตัดสิน
                                                                                        </td>
                                                                                        <td>
                                                                                            <dx:ASPxTextBox ID="txtName6" runat="server" Width="200px">
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center" colspan="2">
                                                                                            <dx:ASPxButton ID="btnConfirm2" runat="server" AutoPostBack="false" Text="บันทึก"
                                                                                                CssClass="dxeLineBreakFix" Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) {if(ASPxClientEdit.ValidateGroup('add') && (!chkChange6.GetChecked() ? true: ASPxClientEdit.ValidateGroup('ChkAdd'))){xcpn.PerformCallback('Save2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}else{ return false;}}" />
                                                                                            </dx:ASPxButton>
                                                                                            &nbsp
                                                                                            <dx:ASPxButton ID="btnCancel2" runat="server" SkinID="_close" CssClass="dxeLineBreakFix"
                                                                                                Width="80px">
                                                                                                <ClientSideEvents Click="function (s, e) { gvw.CancelEdit()}"></ClientSideEvents>
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                </TabPages>
                                                            </dx:ASPxPageControl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPager AlwaysShowPager="True">
                                        </SettingsPager>
                                    </dx:ASPxGridView>
                                    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                        EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
                                    <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
                                        HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true"
                                        SkinID="popUp">
                                        <ContentCollection>
                                            <dx:PopupControlContentControl>
                                                <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                                                    <tr>
                                                        <td width='20' background='images/bd001_a.jpg' height='9'>
                                                        </td>
                                                        <td background='images/bd001_b.jpg'>
                                                            &nbsp;
                                                        </td>
                                                        <td width='20' height='20' align='left' valign='top' background='images/bd001_c.jpg'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                                                            <img src='images/bd001_d.jpg' width='20' height='164'>
                                                        </td>
                                                        <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        รายชื่อคณะกรรมการ
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                                                        </td>
                                                        <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                                            <img src='images/bd001_e.jpg' width='20' height='164'>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width='20'>
                                                            <img src='images/bd001_f.jpg' width='20' height='20'>
                                                        </td>
                                                        <td background='images/bd001_g.jpg'>
                                                            <img src='images/bd001_g.jpg' width='20' height='20'>
                                                        </td>
                                                        <td width='20'>
                                                            <img src='images/bd001_h.jpg' width='20' height='20'>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:PopupControlContentControl>
                                        </ContentCollection>
                                    </dx:ASPxPopupControl>
                                </td>
                            </tr>
                        </table>
                    </dx:PanelContent>
                </PanelCollection>
                
            </dx:ASPxCallbackPanel>
        </ContentTemplate>
        
    </asp:UpdatePanel>

    <div class="modal fade" id="ShowAttach" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"
        aria-hidden="true">
        <div class="modal-dialog" style="width:975px; height:700px">
            <asp:UpdatePanel ID="udpAttach" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label1" runat="server" Text="แนบไฟล์เอกสาร"></asp:Label></h4>
                        </div>
                        <div class="modal-body" style="width:950px; height:700px; overflow:scroll">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>
                                            <asp:Label ID="Label2" runat="server" Text="แนบไฟล์เอกสาร"></asp:Label></div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                                                            <asp:Table ID="Table3" runat="server" Width="100%">
                                                                                                            <asp:TableRow>
                                                                                                                <asp:TableCell Width="35%">
                                                                                                                    <asp:Label ID="lblUploadTypeTab3" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                                                                                                </asp:TableCell><asp:TableCell>
                                                                                                                    <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                                                                                                        Width="350px" DataValueField="UPLOAD_ID">
                                                                                                                    </asp:DropDownList>
                                                                                                                </asp:TableCell></asp:TableRow><asp:TableRow>
                                                                                                                <asp:TableCell Width="35%">
                                                                                                                    <asp:FileUpload ID="fileUpload" runat="server" Width="100%" />
                                                                                                                </asp:TableCell><asp:TableCell>
                                                                                                                    <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                                                                                                        OnClick="cmdUpload_Click" Style="width: 100px" />
                                                                                                                </asp:TableCell></asp:TableRow>
                                                                                                <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                                                                <asp:TableRow>
                                                                                                    <asp:TableCell ColumnSpan="5">
                                                                                                        <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                                                            CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                                                                                            EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                                                                                            EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                                                                                            OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                                                                                            OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                                                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField HeaderText="No.">
                                                                                                                    <ItemTemplate>
                                                                                                                        <%# Container.DataItemIndex + 1 %>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                                                                                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                                                                                                <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                                                                                                <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                                                                                                <asp:BoundField DataField="FULLPATH" Visible="false" />
                                                                                                                <asp:TemplateField HeaderText="Action">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                                                                                            Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp; <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                                                                                            Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                                        </asp:GridView>
                                                                                                    </asp:TableCell>
                                                                                                </asp:TableRow>
                                                                                                <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                                                                <asp:TableRow><asp:TableCell style="text-align:center" ColumnSpan="5"><asp:Button ID="cmdClose" runat="server" Text="ปิด" Width="80px" CssClass="btn btn-success" OnClick="cmdClose_Click" />

                                                                                                              </asp:TableCell></asp:TableRow> 
                                                                                                </asp:Table>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="cmdUpload" />
                                </Triggers>
                            </asp:UpdatePanel>
                    </div>
                        </div>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowScore" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static"
        aria-hidden="true">
        <div class="modal-dialog" style="width:975px; height:700px">
            <asp:UpdatePanel ID="updShowScore" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <%--<button type="button" id="Button4" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button>--%>
                            <h4 class="modal-title">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblScoreTitle" runat="server" Text="พิจารณาตัดคะแนน (อุทธรณ์)"></asp:Label></h4>
                        </div>
                        <div class="modal-body" style="width:950px; height:700px; overflow:scroll">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>
                                            <asp:Label ID="lblCusScoreHeader" runat="server" Text="การตัดคะแนน (เลขที่สัญญา {0})"></asp:Label></div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                    <asp:Table ID="Table111" Width="100%" runat="server">
                                                        <%--<asp:TableRow ID="Row1" runat="server">
                                                            <asp:TableCell Width="15%">&nbsp;</asp:TableCell><asp:TableCell Width="25%" ColumnSpan="2">
                                                                <asp:CheckBox ID="chkSaveTab3Draft" runat="server" Text="รับเรื่องรอพิจารณา" ForeColor="Red" />
                                                            </asp:TableCell>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>--%>
                                                        <asp:TableRow ID="Row2" runat="server">
                                                            <asp:TableCell Width="15%">
                                                                <div id="divTopic" runat="server">
                                                                    หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font></div>
                                                            </asp:TableCell><asp:TableCell Width="25%" ColumnSpan="4">
                                                                <asp:DropDownList ID="cboScore" runat="server" class="form-control" DataTextField="STOPICNAME"
                                                                    OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                                    AutoPostBack="true" Width="100%">
                                                                </asp:DropDownList>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row3" runat="server">
                                                            <asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell ColumnSpan="4">
                                                                <asp:Label ID="lblShowOption" runat="server" Text="[พขร. ทุจริต]" Visible="false"
                                                                    ForeColor="Red"></asp:Label></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow ID="Row4" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblScore1" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtScore1" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore1_OnKeyPress');" /></asp:TableCell><asp:TableCell
                                                                            Width="20%">&nbsp;</asp:TableCell><asp:TableCell Width="15%">
                                                                                <asp:Label ID="lblScore2" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell
                                                                                    Width="25%">
                                                                                    <asp:TextBox CssClass="form-control" ID="txtScore2" runat="server" Width="200px"
                                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore2_OnKeyPress');" /></asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row5" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblScore3" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtScore3" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore3_OnKeyPress');" /></asp:TableCell><asp:TableCell
                                                                            Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                                <asp:Label ID="lblScore4" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtScore4" runat="server" Width="200px"
                                                                                        Enabled="false" Style="text-align: center" onkeyup="__doPostBack(this.name,'txtScore4_OnKeyPress');" /></asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow ID="rowListCarTab3" Visible="false">
                                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px;
                                                                vertical-align: top;">
                                                                <asp:GridView ID="dgvScore" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                                    OnCheckedChanged="chkChooseScoreAll_CheckedChanged" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle Width="36px" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TRUCKID" Visible="false">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CARHEAD" HeaderText="ทะเบียนรถ (หัว)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CARDETAIL" HeaderText="ทะเบียนรถ (หาง)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell></asp:TableRow>
                                                        <asp:TableRow ID="rowListTotalCarTab3" runat="server" Visible="false">
                                                            <asp:TableCell Width="60%" ColumnSpan="5" Style="padding-left: 5px; padding-top: 5px;
                                                                vertical-align: top;">
                                                                <asp:GridView ID="dgvScoreTotalCar" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkChooseScoreAll" runat="server" AutoPostBack="true" Checked="true"
                                                                                    OnCheckedChanged="chkChooseScoreListCarAll_CheckedChanged" />
                                                                            </HeaderTemplate>
                                                                            <ItemStyle Width="36px" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkChooseScore" Checked="true" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DELIVERY_DATE" HeaderText="วันที่เกิดเหตุ">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TOTAL_CAR" HeaderText="จำนวนรถ (ตามเอกสาร)">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="จำนวนรถ (หักคะแนน)">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtChooseTotalCar" runat="server" Width="120px"></asp:TextBox></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell></asp:TableRow>
                                                        <asp:TableRow ID="rowTotalCarTab3" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label12" runat="server" Text="จำนวนรถ (คัน)"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox ID="txtTotalCarTab3" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow runat="server" ID="rowOil" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label10" runat="server" Text="ค่าน้ำมัน / ลิตร"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox ID="txtOilPrice" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilPrice_OnKeyPress');"
                                                                        CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="Label11" runat="server" Text="จำนวนน้ำมัน (ลิตร)"></asp:Label></asp:TableCell><asp:TableCell
                                                                                Width="20%">
                                                                                <asp:TextBox ID="txtOilQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtOilQty_OnKeyPress');"
                                                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow runat="server" ID="rowZeal" Visible="false">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblZeal1" runat="server" Text="ค่าปรับต่อ 1 ซิล"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox ID="txtZeal" runat="server" Enabled="false" CssClass="form-control"
                                                                        Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="Label9" runat="server" Text="จำนวน ซิล"></asp:Label></asp:TableCell><asp:TableCell
                                                                                Width="20%">
                                                                                <asp:TextBox ID="txtZealQty" Width="200px" runat="server" AutoPostBack="true" onkeyup="__doPostBack(this.name,'txtZealQty_OnKeyPress');"
                                                                                    CssClass="form-control" Style="text-align: center"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow ID="Row6" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCost" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="200px" ReadOnly="true"
                                                                        Enabled="false" Style="text-align: center" />
                                                                </asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                    <asp:Label ID="lblDisableDriverDay" runat="server" Text="ระงับ พชร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell>
                                                                        <asp:TextBox CssClass="form-control" ID="txtDisableDriver" runat="server" Width="200px"
                                                                            Enabled="false" Style="text-align: center" />
                                                                    </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow ID="Row7" runat="server">
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblScore5" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtScore5" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center" /></asp:TableCell><asp:TableCell Width="20%">&nbsp;</asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="lblScore6" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                <asp:TextBox CssClass="form-control" ID="txtScore6" runat="server" Width="200px"
                                                                                    Enabled="false" Style="text-align: center" /></asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="5">
                                                                <asp:GridView ID="dgvScoreList" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                                    OnRowDeleting="dgvScore_RowDeleting" OnRowCommand="dgvScoreList_RowCommand" AlternatingRowStyle-BackColor="White"
                                                                    RowStyle-ForeColor="#284775">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="STOPICID" Visible="false">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="STOPICNAME" HeaderText="หัวข้อตัดคะแนน">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle Width="430px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Score6Display" HeaderText="หักคะแนนต่อครั้ง">
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CostDisplay" HeaderText="ค่าปรับ(บาท)">
                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="false"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DisableDriverDisplay" HeaderText="ระงับ พขร.(วัน)">
                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="false" />
                                                                        </asp:BoundField>
                                                                        <asp:TemplateField HeaderText="Action">
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="false" />
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgViewScore" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Select" />&nbsp;
                                                                                <asp:ImageButton ID="imgDeleteScore" runat="server" ImageUrl="~/Images/bin1.png"
                                                                                    Width="23px" Height="23px" Style="cursor: pointer" CommandName="Delete" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                                                </asp:GridView>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer" style="text-align: right">
                                            <asp:Button ID="cmdAddScore" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px"
                                                UseSubmitBehavior="false" runat="server" Text="เพิ่มรายการ" OnClick="cmdAddScore_Click" />
                                            <br />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="panel panel-info" id="divScore" runat="server">
                                        <div class="panel-heading">
                                            <i class="fa fa-table"></i>สรุปผลการพิจารณา (หลังอุทธรณ์)
                                        </div>
                                        <div class="panel-body">
                                            <div class="dataTable_wrapper">
                                                <div class="panel-body">
                                                    <asp:Table ID="Table222" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:CheckBox ID="chkBlacklist" runat="server" Text="&nbsp;Blacklist" ForeColor="Red" Enabled="false" />
                                                            </asp:TableCell>
                                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="27%">
                                                                <asp:Label ID="lblCusScoreType" runat="server" Text="เงื่อนไขการตัดคะแนน"></asp:Label></asp:TableCell><asp:TableCell
                                                                    ColumnSpan="2" Width="27%">
                                                                    <asp:RadioButtonList ID="radCusScoreType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                        OnSelectedIndexChanged="radCusScoreType_SelectedIndexChanged">
                                                                    </asp:RadioButtonList>
                                                                </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="lblShowSumPoint" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                <asp:Label ID="lblAppealPoint" runat="server" Text="หักคะแนน (หลังอุทธรณ์)" Visible="false"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealPoint" runat="server" Width="140px"
                                                                                        Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="lblShowSumCost" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                <asp:Label ID="lblAppealCost" runat="server" Text="ค่าปรับ (หลังอุทธรณ์)" Visible="false"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealCost" runat="server" Width="140px"
                                                                                        Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblCostOther" runat="server" Text="ค่าเสียหายและหรือค่าใช้จ่าย"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtCostOther" runat="server" Width="200px"
                                                                        Style="text-align: center"> </asp:TextBox></asp:TableCell><asp:TableCell> </asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Label84" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px"
                                                                        Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                                            <asp:Label ID="lblShowSumDisable" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                <asp:Label ID="lblAppealDisable" runat="server" Text="ระงับ พขร. (หลังอุทธรณ์)" Visible="false"></asp:Label></asp:TableCell><asp:TableCell>
                                                                                    <asp:TextBox CssClass="form-control" ID="txtAppealDisable" runat="server" Width="140px"
                                                                                        Enabled="false" Style="text-align: center" Visible="false"></asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblOilLose" runat="server" Text="ปริมาณผลิตภัณฑ์เสียหาย/สูญหาย (ลิตร)"></asp:Label></asp:TableCell><asp:TableCell>
                                                                    <asp:TextBox CssClass="form-control" ID="txtOilLose" runat="server" Width="200px"
                                                                        Style="text-align: center"> </asp:TextBox></asp:TableCell><asp:TableCell> </asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblRemarkTab3" runat="server" Text="หมายเหตุ"></asp:Label></asp:TableCell><asp:TableCell
                                                                    ColumnSpan="4">
                                                                    <asp:TextBox CssClass="form-control" ID="txtRemarkTab3" runat="server" Width="100%"
                                                                        Height="70px"> </asp:TextBox></asp:TableCell></asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                                    </asp:Table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer" style="text-align: right">
                                            <%--<asp:Button ID="cmdShowAppeal" runat="server" Text="ข้อมูลการยื่นอุทธรณ์" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdShowAppeal_Click"
                                                Visible="false" />
                                            &nbsp;--%>
                                            
                                            <asp:Button ID="cmdSaveTab3" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdSaveTab3_Click" />
                                            &nbsp;
                                            <%--<asp:Button ID="cmdSaveTab3Draft" runat="server" Text="บันทึก (ร่าง)" CssClass="btn btn-md btn-hover btn-warning"
                                                UseSubmitBehavior="false" OnClick="cmdSaveTab3Draft_Click" Style="width: 100px" />
                                            &nbsp;
                                            <asp:Button ID="cmdChangeStatus" runat="server" Text="ขอเอกสารเพิ่มเติม" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 140px" OnClick="cmdChangeStatus_Click" />
                                            &nbsp;--%>
                                            <asp:Button ID="cmdCloseTab3" runat="server" Text="ปิด" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCloseTab3_Click" />
                                            <br />
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--<asp:DropDownList ID="cboScore" runat="server" class="form-control" DataTextField="STOPICNAME"
                                                OnSelectedIndexChanged="cboScore_SelectedIndexChanged" DataValueField="STOPICID"
                                                AutoPostBack="true" Width="100%">
                                            </asp:DropDownList>--%>
                            <%--<asp:Table ID="Table7" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Image ID="img1" runat="server" ImageUrl="~/ImagesTemp/1.png" Width="700px" Height="400px" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Image ID="img2" runat="server" ImageUrl="~/ImagesTemp/2.png" Width="700px" Height="400px" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>--%>
                            <br />
                            <button type="button" id="Button7" class="close" value="asdf">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
