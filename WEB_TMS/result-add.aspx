﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="result-add.aspx.cs" Inherits="result_add" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script language="javascript" type="text/javascript">

        function openFile(str) {
            window.open("" + str + "");
        }

        $(document).ready(function () {

            var v1 = $("input[id$=txtCheckReqType]").val(); //txtCheckReqType.GetValue();
            if (v1 == "01") {
                $("#trDetailSlot").show();
                $("#trDetailCap").show();
            }
            else {
                $("#trDetailSlot").hide();
                $("#trDetailCap").hide();
            }

        });

    </script>
    <style type="text/css">
        .cHideControl
        {
            display: none;
        }
        
        .cShowControl
        {
            display: block;
        }
        
        .cInline
        {
            display: inline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" ClientInstanceName="xcpn" OnCallback="xcpn_Callback"
        OnLoad="xcpn_Load" HideContentOnCallback="false" CausesValidation="False">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}" />
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                            <tr>
                                                <td colspan="4" align="right">
                                                    <%--<input type="submit" name="button3" id="button3" value="ดูประวัติรถ" onclick="javascript:location.href='car_history.htm'">--%>
                                                    <%--<input type="submit" name="button7" id="button7" value="ประวัติการรับบริการ" onclick="javascript:location.href='service_history.htm'">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">วันที่ยืนคำขอ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQUEST_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">วันที่หมดอายุวัดน้ำ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblDWATEREXPIRE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="18%" bgcolor="#B9EAEF">วันที่นัดหมาย</td>
                                                <td width="32%" class="active">
                                                    <dx:ASPxLabel ID="lblSERVICE_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="19%" bgcolor="#B9EAEF">วันที่ รข. อนุมัติคำขอ</td>
                                                <td width="31%">
                                                    <dx:ASPxLabel ID="lblAPPOINTMENT_BY_DATE" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ประเภทคำขอ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREQTYPE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">สาเหตุ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblCAUSE_NAME" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">ทะเบียนรถ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblREGISTERNO" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td bgcolor="#B9EAEF">ประเภทรถ</td>
                                                <td>
                                                    <dx:ASPxLabel ID="lblTypeCar" runat="server">
                                                    </dx:ASPxLabel>
                                                    - ความจุ
                                                    <dx:ASPxLabel ID="lblTotalCap" runat="server">
                                                    </dx:ASPxLabel>
                                                    ลิตร</td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#B9EAEF">บริษัทขนส่ง </td>
                                                <td colspan="3">
                                                    <dx:ASPxLabel ID="lblVendorName" runat="server">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr id="trDetailCap" style="display: none;">
                                                <td><img src="images/btnQuest1.gif" width="23" height="23" alt="" /> รายละเอียดความจุ
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td align="right" valign="bottom">
                                                    <%--<span class="active">*</span> มีการเพิ่มแป้นหรือเปลี่ยนแปลงความจุ--%>
                                                </td>
                                            </tr>
                                            <tr id="trDetailSlot" style="display: none;">
                                                <td colspan="4">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <%--<dx:ASPxGridView ID="gvwCapCar" runat="server" SkinID="_gvw" AutoGenerateColumns="false"
                                                                    Width="100%">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption=" " FieldName="SNAME" Width="20%">
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 1" FieldName="SLOT1" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 2" FieldName="SLOT2" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 3" FieldName="SLOT3" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 4" FieldName="SLOT4" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 5" FieldName="SLOT5" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 6" FieldName="SLOT6" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 7" FieldName="SLOT7" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 8" FieldName="SLOT8" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 9" FieldName="SLOT9" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่องที่ 10" FieldName="SLOT10" Width="8%">
                                                                            <CellStyle HorizontalAlign="Right">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <PropertiesTextEdit EncodeHtml="false" />
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                                                                </dx:ASPxGridView>--%>
                                                                <dx:ASPxGridView ID="gvwCapCar" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                                    Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="แป้น" HeaderStyle-HorizontalAlign="Center" Width="20%"
                                                                            Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SNAME") %>' runat="server" ID="xlbldCarConFirm" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 1" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT1")== null?"-":Eval("SLOT1").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT1" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 2" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT2") == null?"-":Eval("SLOT2").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT2" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 3" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT3")== null?"-":Eval("SLOT3").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT3" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 4" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT4")== null?"-":Eval("SLOT4").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT4" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 5" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT5")== null?"-":Eval("SLOT5").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT5" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 6" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT6")== null?"-":Eval("SLOT6").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT6" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 7" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT7")== null?"-":Eval("SLOT7").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT7" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 8" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT8")== null?"-":Eval("SLOT8").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT8" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 9" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT9")== null?"-":Eval("SLOT9").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT9" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ช่อง 10" HeaderStyle-HorizontalAlign="Center"
                                                                            Width="8%" Settings-AllowDragDrop="False">
                                                                            <Settings AllowDragDrop="False"></Settings>
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel Text='<%# Eval("SLOT10")== null?"-":Eval("SLOT10").ToString()%>' runat="server"
                                                                                    ID="xlbSLOT10" />
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <SettingsPager AlwaysShowPager="false" PageSize="3" Visible="false">
                                                                    </SettingsPager>
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right"><span class="corp">จำนวนช่อง :
                                                                <dx:ASPxLabel ID="lblMaxSlot" runat="server" Font-Bold="True">
                                                                </dx:ASPxLabel>
                                                                &nbsp;ช่อง&nbsp; ความจุรวม :
                                                                <dx:ASPxLabel ID="lblTotalCapacity_SumMaxSlot" runat="server" Font-Bold="True">
                                                                </dx:ASPxLabel>
                                                                ลิตร</span> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><img src="images/credit.png" width="16" height="16" alt="" />รายละเอียดค่าธรรมเนียม
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" bgcolor="#6AC6DF">
                                                    <table width="100%" border="0" cellpadding="5" cellspacing="1">
                                                        <tr>
                                                            <td width="20%" valign="top" bgcolor="#EAEAEA">ค่าธรรมเนียมบริการ</td>
                                                            <td width="80%" bgcolor="#FFFFFF">
                                                                <dx:ASPxLabel ID="lblTOTLE_SERVCHAGE" runat="server">
                                                                </dx:ASPxLabel>
                                                                บาท</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><img src="images/cv.png" width="16" height="16" alt="" /> เอกสารแนบ
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxGridView ID="gvwDocument" ClientInstanceName="gvwDocument" runat="server"
                                                                    SkinID="_gvw" Width="100%">
                                                                    <Columns>
                                                                        <dx:GridViewDataColumn Caption="No." Width="4%">
                                                                            <DataItemTemplate>
                                                                                <%#Container.ItemIndex + 1 %>.
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="รายการเอกสาร" Width="14%">
                                                                            <DataItemTemplate>
                                                                                <%#Eval("DOC_DESCRIPTION")%>
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Width="40%" Caption="sFileName">
                                                                            <DataItemTemplate>
                                                                                <%--<img src="images/ic_pdf2.gif" width="16" height="16" border="0" class='<%# (Eval("sOpenFile")).ToString() == "N" ? "cHideControl" : "cShowControl cInline" %>' />--%>
                                                                                <dx:ASPxLabel ID="lblsNameShow" runat="server" Text='<%#Bind("sNameShow") %>' Style="display: inline;">
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Width="10%" Caption="sConsider">
                                                                            <DataItemTemplate>
                                                                                <%#Eval("sConsiderShow")%>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Width="5%" Caption="sFileName">
                                                                            <DataItemTemplate>
                                                                                <img title="ดูเอกสาร" src="images/view1.png" width="25px" height="25px" border="0"
                                                                                class='<%# (Eval("sOpenFile")).ToString() == "N" ? "cHideControl" : "cShowControl" %>'
                                                                                onclick=<%# "javascript:openFile('" + Eval("sOpenFile") + "')" %> style="cursor: pointer;">
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="nFileID" Visible="false">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblnFileID" runat="server" Text='<%#Bind("nFileID") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="sSysFileName" Visible="false">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblsSysFileName" runat="server" Text='<%#Bind("sSysFileName") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="sPath" Visible="false">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblsPath" runat="server" Text='<%#Bind("sPath") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                        <dx:GridViewDataColumn Caption="DOCTYPE_ID" Visible="false">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblDOCTYPE_ID" runat="server" Text='<%#Bind("DOCTYPE_ID") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataColumn>
                                                                    </Columns>
                                                                    <SettingsPager Mode="ShowAllRecords" />
                                                                    <Settings ShowColumnHeaders="false" ShowFooter="false" />
                                                                    <Styles>
                                                                        <Table>
                                                                            <Border BorderStyle="None" />
                                                                        </Table>
                                                                        <Cell>
                                                                            <Border BorderWidth="0px" />
                                                                        </Cell>
                                                                    </Styles>
                                                                    <Border BorderWidth="0px" />
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtCheckReqType" runat="server" CssClass="cHideControl"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                                        <tr>
                                                            <td width="15%" align="center" bgcolor="#00CC33">
                                                                <dx:ASPxHyperLink ID="hplT1" runat="server" Text="ประวัติการตรวจ" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="hplT2" runat="server" Text="ตรวจสภาพภายนอก/ใน" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT2;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="hplT3" runat="server" Text="ผลตรวจลงน้ำ" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT3;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="hplT4" runat="server" Text="คำนวณค่าธรรมเนียมเพิ่ม" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT4;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                             <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="รูปภาพการตรวจสอบ" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectImg;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                            <td width="15%" align="center" bgcolor="#EDEDED">
                                                                <dx:ASPxHyperLink ID="hplT5" runat="server" Text="บันทึกผลปิดงาน" Style="text-decoration: none;
                                                                    cursor: pointer; color: Blue;">
                                                                    <ClientSideEvents Click=" function(s,e){ xcpn.PerformCallback('RedirectT5;')}" />
                                                                </dx:ASPxHyperLink>
                                                            </td>
                                                            <td width="10%">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp; </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <dx:ASPxGridView ID="gvwhistory" runat="server" ClientInstanceName="gvwhistory" AutoGenerateColumns="false"
                                                                    Width="100%" SkinID="_gvw" Style="margin-top: 0px">
                                                                    <Columns>
                                                                        <dx:GridViewDataDateColumn Visible="false" FieldName="REQUEST_ID">
                                                                        </dx:GridViewDataDateColumn>
                                                                        <dx:GridViewDataDateColumn Caption="วันที่" Width="15%" FieldName="EXAMDATE" PropertiesDateEdit-EditFormatString="dd/MM/yyyy">
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <PropertiesDateEdit EditFormatString="dd/MM/yyyy">
                                                                            </PropertiesDateEdit>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataDateColumn>
                                                                        <dx:GridViewDataTextColumn Caption="ประเภทคำขอ" Width="25%" FieldName="REQTYPE_NAME">
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="สาเหตุ" Width="55%" FieldName="CAUSE_NAME">
                                                                            <CellStyle HorizontalAlign="Left">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="" Width="5%">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxButton runat="server" ID="btnEditwaiting" AutoPostBack="false" EnableTheming="false"
                                                                                    EnableDefaultAppearance="False" Cursor="pointer" SkinID="dd">
                                                                                    <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('History;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                    <Image Url="Images/ic_search.gif" Height="20px" Width="20px">
                                                                                    </Image>
                                                                                </dx:ASPxButton>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:ASPxGridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="center">
                                                    <dx:ASPxButton ID="btnToAdd2" runat="server" Text="ตรวจสอบสภาพภายนอก/ใน" ClientInstanceName="btnToAdd2"
                                                        AutoPostBack="false" Width="200px" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e) {xcpn.PerformCallback('RedirectT2;');}" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton ID="bntExitT1" runat="server" ClientInstanceName="bntExitT1" AutoPostBack="false"
                                                        Text="ออกจากหน้านี้" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(s,e) {  window.location = 'approve_mv.aspx';}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
