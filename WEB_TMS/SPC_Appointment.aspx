﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SPC_Appointment.aspx.cs" Inherits="SPC_Appointment" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div style="font-family: 'Century Gothic'">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>
                        <asp:Label ID="lblHeaderTab1" runat="server" Text="Appointment"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblType" runat="server" Text="ประเภทนัดหมาย :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:RadioButtonList ID="radType" runat="server" Style="font-family: 'Century Gothic'" Font-Size="16px">
                                                <asp:ListItem Text="ตรวจรถก่อนเข้าสัญญา" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ตรวจรถประจำปี" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblVendor" runat="server" Text="ผู้ประกอบการขนส่ง :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblTruck" runat="server" Text="ทะเบียนรถ :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:DropDownList ID="ddlTruck" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblTotalTruck" runat="server" Text="Department :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtTotalTruck" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblAppointmentDate" runat="server" Text="ทะเบียนรถ :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtAppointmentDate" runat="server" CssClass="datepicker" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblPlace" runat="server" Text="Department :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtPlace" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>

                                    <asp:TableRow>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="lblAppointmentBy" runat="server" Text="นัดหมายโดย :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtAppointmentBy" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px" Enabled="false"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="15%" Style="text-align: right">
                                            <asp:Label ID="Label2" runat="server" Text="Department :&nbsp;" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="25%">
                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Style="font-family: 'Century Gothic'" Font-Size="16px"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Width="10%">
                                        &nbsp;
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>

                                <br />

                                <div class="row">
                                    <div style="text-align: left" class="col-sm-6">
                                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-success" />
                                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-warning" />
                                        <%--<asp:ImageButton ID="cmdSearchBook" runat="server" ImageUrl="~/Images/ImageForKM/show bookmark.png" Width="15%" Height="15%" Style="cursor: pointer" OnClick="cmdSearchBook_Click" />
                                        <asp:ImageButton ID="cmdAddNewTopic" runat="server" ImageUrl="~/Images/ImageForKM/add new topic.png" Width="15%" Height="15%" Style="cursor: pointer" OnClick="cmdAddNewTopic_Click" />
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/ImageForKM/manual download.png" Width="15%" Height="15%" Style="cursor: pointer" />--%>
                                    </div>
                                </div>

                                <br />
                                <%--<asp:GridView ID="dgvAppointment" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" DataKeyNames="TOPIC_ID,KM_ID,STATUS_ID" Font-Size="16px"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" HeaderStyle-ForeColor="Info" 
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnRowDataBound="dgvKM_RowDataBound"
                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnRowCommand="dgvKM_RowCommand"
                                    AllowPaging="true" OnPageIndexChanging="dgvKM_PageIndexChanging" PageSize="10"
                                    AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775">
                                    <Columns>
                                        <asp:BoundField DataField="TOPIC_ID" HeaderText="Topic ID">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Topic name">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkTopicName" runat="server" Text='<%# Eval("TOPIC_NAME")%>' CommandName="View" Font-Size="16px"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TAGS" HeaderText="Tags">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KNOWLEDGE_TYPE_NAME" HeaderText="Knowledge type">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DIVISION_NAME" HeaderText="Division">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DEPARTMENT_NAME" HeaderText="Department">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLNAME" HeaderText="Author name">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CREATE_DATE" HeaderText="Created date">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPDATE_DATE" HeaderText="Updated date">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Bookmark">
                                            <HeaderStyle HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemStyle BorderWidth="0" HorizontalAlign="Center" Font-Names="Century Gothic" Font-Size="16px"/>
                                            <ItemTemplate>
                                                <div class="col-sm-12">
                                                    <asp:ImageButton ID="imgBook" runat="server" CommandName="Book" Width="24" Height="24" Font-Names="Century Gothic" Font-Size="16px"/>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle CssClass="gvHeader-white"></HeaderStyle>
                                    <PagerStyle CssClass="pagination-ys" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                </asp:GridView>--%>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        &nbsp;
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
