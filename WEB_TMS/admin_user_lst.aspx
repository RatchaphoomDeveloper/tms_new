﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_user_lst.aspx.cs" Inherits="admin_user_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>

    <style type="text/css">
        .style13
        {
            height: 23px;
        }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="70%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" 
                                NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา" Style="margin-left: 0px" 
                                Width="250px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            กลุ่มผู้ใช้&nbsp;&nbsp; </td>
                        <td width="8%" class="style13">
                            <dx:ASPxComboBox ID="cbxGroup" runat="server" Width="120px" SelectedIndex="0">
                            <ClientSideEvents  SelectedIndexChanged="function (s, e) {xcpn.PerformCallback('selectgroup'); }" />
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="ผู้ประกอบการขนส่ง" Value="0" />
                                    <dx:ListEditItem Text="เจ้าหน้าที่ ปตท." Value="1" />
                                    <dx:ListEditItem Text="คลัง" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds"   OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" >
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption = "ที่" HeaderStyle-HorizontalAlign="Center" Width="5%" VisibleIndex="1">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="1" 
                                        FieldName="SUID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อ - นามสกุล" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        Width="23%" FieldName="FULLNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หน่วยงาน" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SVENDORNAME" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="กลุ่มผู้ใช้งาน" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="CGROUP1" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                      <dx:GridViewDataTextColumn Caption="สถานะ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="CACTIVE" Width="5%">
                                        <DataItemTemplate>
                                         <dx:ASPxLabel title='<%# (""+Eval("CACTIVE")=="2"?"คลิกเพื่อปลดล๊อค":"") %>' ID="lblStatus" runat="server" EncodeHtml="false" Text='<%# (""+Eval("CACTIVE")=="2"?"<font color=Orange >Lock</font>":(""+Eval("CACTIVE")=="0"?"<font color=Red >InActive</font>":"<font color=Green >Active</font>")) %>'>
                                         <ClientSideEvents Click="function(s,e){ if(s.GetText().indexOf('Lock')>-1){var indexs =s.name.substring(s.name.lastIndexOf('_')+1,s.name.length);  dxConfirm('ยืนยันการทำงาน','<b>ท่านต้องการทำรายปลดล๊อคการใช้งานให้บุคคลนี้</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); xcpn.PerformCallback('UnLock;'+indexs); } ,function(s,e){  dxPopupConfirm.Hide(); }); } }" />
                                         </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เข้าใช้งานระบบครั้งล่าสุด" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="MAXDCREATE" Width="20%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("MAXDCREATE", "{0:dd/MM/yyyy เวลา HH:mm น. - }") %>' runat="server" ID="xlbldLastLogIn" /><dx:ASPxLabel Text='<%# Eval("SDESCRIPTION") %>' runat="server" ID="xlbPosition" />&nbsp;
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="5">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>

                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY CAST(SUID AS INT)) AS ID1,u.SUID,(u.SFIRSTNAME || ' ' || u.SLASTNAME) AS FULLNAME,VS.SVENDORNAME AS SVENDORNAME, (CASE u.CGROUP WHEN '0' THEN 'ผู้ประกอบการขนส่ง' WHEN '1' THEN 'เจ้าหน้าที่ปตท.' ELSE 'คลัง' END) AS CGROUP1, u.DCREATE, u.SPOSITION,u.CACTIVE FROM  (TUSER u INNER JOIN TVENDOR v ON u.SVENDORID = V.SVENDORID ) INNER JOIN TVENDOR_SAP vs ON  V.SVENDORID = VS.SVENDORID where u.CGROUP = '0' and (u.SFIRSTNAME || u.SLASTNAME like '%' || :oSearch || '%' OR VS.SVENDORNAME like '%' || :oSearch || '%')" 
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" 
                                DeleteCommand="DELETE FROM TUser WHERE (SUID = :OUID)"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                               
                                <DeleteParameters>
                                    <asp:SessionParameter Name="oUID" SessionField="ouid" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch"  PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }" />
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
