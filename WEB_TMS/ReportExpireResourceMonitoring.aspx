﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="ReportExpireResourceMonitoring.aspx.cs" Inherits="ReportExpireResourceMonitoring"
    StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style14
        {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF">
                            <table width="100%">
                                <tr>
                                    <td colspan="3" align="right">
                                        <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                                            <tr>
                                                <td style="padding-right: 4px">
                                                    <div style="float: left">
                                                        <dx:ASPxButton ID="btnPdfExport2" ToolTip="Export to PDF" runat="server" UseSubmitBehavior="False"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                            OnClick="btnPdfExport2_Click">
                                                            <Image Width="16px" Height="16px" Url="Images/ic_pdf2.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </div>
                                                </td>
                                                &nbsp
                                                <td style="padding-right: 4px">
                                                    <div style="float: left">
                                                        <dx:ASPxButton ID="btnXlsExport2" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                            OnClick="btnXlsExport2_Click">
                                                            <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                                                            </Image>
                                                        </dx:ASPxButton>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <table border="0" width="100%" cellpadding="1" cellspacing="2">
                                            <tr>
                                                <td>
                                                    &nbsp;ช่วงเวลาที่ค้นหา&nbsp;
                                                </td>
                                                <td>
                                                    <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td>
                                                    &nbsp;ถึง&nbsp;
                                                </td>
                                                <td>
                                                    <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td>
                                                    &nbsp;&nbsp;&nbsp;โหมดการค้นหา&nbsp;
                                                </td>
                                                <td>
                                                    <dx:ASPxRadioButtonList ID="rblSearch" ClientInstanceName="rblSearch" runat="server"
                                                        SelectedIndex="0" SkinID="rblStatus">
                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){if(s.GetValue() == '1'){cbxYear.SetEnabled(false);}else{cbxYear.SetEnabled(true);} }" />
                                                        <Items>
                                                            <dx:ListEditItem Text="หมดอายุวัดน้ำ" Value="1" Selected="true" />
                                                            <dx:ListEditItem Text="หมดสภาพโดยระบุอายุการใช้งาน" Value="2" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                                <td>
                                                    <dx:ASPxComboBox ID="cbxYear" runat="server" ClientInstanceName="cbxYear" Width="80px"
                                                        ValueType="System.String" AutoPostBack="false" EnableIncrementalFiltering="true"
                                                        EnableCallbackMode="false">
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td>
                                                    &nbsp;ปี&nbsp;
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                                        <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                            DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%">
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                                    VisibleIndex="0" ExportWidth="25">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสรถ (หัว)" FieldName="STRUCKID" Visible="false"
                                                    ExportWidth="70">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="รหัสรถ (ท้าย)" FieldName="STRAILERID" Visible="false" ExportWidth="70">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="STRANSPORTTYPE" FieldName="STRANSPORTTYPE" ShowInCustomizationForm="True"
                                                    Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ทะเบียนรถ (หัว)" FieldName="SHEADREGISTERNO"
                                                    ShowInCustomizationForm="True" VisibleIndex="3" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO"
                                                    ShowInCustomizationForm="True" VisibleIndex="4" Width="10%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" ShowInCustomizationForm="True"
                                                    VisibleIndex="5" Width="14%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                                    VisibleIndex="6" Width="14%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ประเภทรถ" FieldName="SCARTYPE" ShowInCustomizationForm="True"
                                                    VisibleIndex="7" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ปริมาตร" FieldName="NTOTALCAPACITY" ShowInCustomizationForm="True"
                                                    VisibleIndex="8" Width="6%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataDateColumn Caption="วันที่หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE"
                                                    ShowInCustomizationForm="True" VisibleIndex="9" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="จดทะเบียนครั้งแรก" FieldName="DSIGNIN" ShowInCustomizationForm="True"
                                                    VisibleIndex="10" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataTextColumn Caption="อายุรถ" FieldName="SYEARMONTH" ShowInCustomizationForm="True"
                                                    VisibleIndex="11" Width="5%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataColumn ShowInCustomizationForm="True" Caption="จัดการ" VisibleIndex="12"
                                                    Width="6%" ExportWidth="0">
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="imbedit0" runat="server" Text="View" AutoPostBack="false" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('view;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager AlwaysShowPager="True">
                                            </SettingsPager>
                                            <Templates>
                                                <EditForm>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="center">
                                                                <table width="85%" border="0" cellpadding="1" cellspacing="1">
                                                                    <tr>
                                                                        <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">
                                                                            รายละเอียดรถ(หัว)
                                                                        </td>
                                                                    </tr>
                                                                    <asp:Literal ID="ltlTruckTotal" runat="server"></asp:Literal>
                                                                    <tr>
                                                                        <td align="left" valign="top" bgcolor="#FFEBD7">
                                                                            <span class="style24">ความจุในแต่ละช่อง</span>
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <span class="style24"></span><span class="style24"></span>
                                                                            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ช่องที่
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ระดับแป้นที่
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ความจุที่ระดับแป้น (ลิตร)
                                                                                    </td>
                                                                                </tr>
                                                                                <asp:Literal ID="ltlTruckTotal1" runat="server"></asp:Literal>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">
                                                                            รายละเอียดรถ(ท้าย)
                                                                        </td>
                                                                    </tr>
                                                                    <asp:Literal ID="ltlTruckTotal2_1" runat="server"></asp:Literal>
                                                                    <tr>
                                                                        <td align="left" valign="top" bgcolor="#FFEBD7">
                                                                            <span class="style24">ความจุในแต่ละช่อง</span>
                                                                        </td>
                                                                        <td colspan="3" align="left">
                                                                            <span class="style24"></span><span class="style24"></span>
                                                                            <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ช่องที่
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ระดับแป้นที่
                                                                                    </td>
                                                                                    <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                        ความจุที่ระดับแป้น (ลิตร)
                                                                                    </td>
                                                                                </tr>
                                                                                <asp:Literal ID="ltlTruckTotal2_2" runat="server"></asp:Literal>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="right">
                                                                            <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" Width="80px" AutoPostBack="False">
                                                                                <ClientSideEvents Click="function (s,e){gvw.CancelEdit() ;}" />
                                                                            </dx:ASPxButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EditForm>
                                            </Templates>
                                        </dx:ASPxGridView>
                                        <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                            CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                        </asp:SqlDataSource>
                                        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvw" PageHeader-Font-Names="Tahoma"
                                            PageHeader-Font-Bold="true">
                                            <Styles>
                                                <Header Font-Names="Tahoma" Font-Size="8">
                                                </Header>
                                                <Cell Font-Names="Tahoma" Font-Size="8">
                                                </Cell>
                                            </Styles>
                                            <PageHeader>
                                                <Font Bold="True" Names="Tahoma"></Font>
                                            </PageHeader>
                                        </dx:ASPxGridViewExporter>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
