﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Admin_VisitForm_Config_old.aspx.cs" Inherits="Admin_VisitForm_Config" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
    <meta http-equiv="Expires" content="0" />
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .marginTp {  margin-top: 5px; }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
    <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
    <asp:UpdatePanel runat="server" ID="uplMain" UpdateMode="Conditional" ViewStateMode="Enabled" ValidateRequestMode="Enabled">
        <ContentTemplate>
            <dx:ASPxGridView ID="grvMain" runat="server" AutoGenerateColumns="False" Width="100%" SkinID="_gvw" OnRowCommand="grvMain_RowCommand" KeyFieldName="ID">
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="YEAR" Caption="ปี" Width="80px">
                        <EditFormSettings Visible="False" />
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FORMNAME" Caption="แบบสอบถาม">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CREATE_DATE" Caption="วันที่สร้าง" Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="UPDATE_DATE" Caption="ปรับปรุงล่าสุดข้อมูลล่าสุด" Width="80px">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Width="70px" CellStyle-Cursor="hand" Caption="" CellStyle-HorizontalAlign="Center">
                        <HeaderStyle HorizontalAlign="Center" />
                        <DataItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="imgViewComplain" runat="server" ImageUrl="~/Images/btnSearch.png" Width="16px" Height="16px" Style="cursor: pointer" CommandName="Select" CausesValidation="False" />
                                    </td>
                                    <td style="padding-left: 2px;">
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/del.gif"
                                            Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" CausesValidation="False" /></td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <SettingsPager AlwaysShowPager="True">
                </SettingsPager>
            </dx:ASPxGridView>
            <br />
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                        </dx:ASPxButton>
                    </td>
                    <td style="padding-left: 5px;">
                        <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" Text="ปิด" CausesValidation="false">
                            <ClientSideEvents Click="function (s, e) {window.location = 'admin_VisitForm_lst.aspx';}" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="dialogAdd" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>เพิ่ม/แก้ไขข้อมูล</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="uplAdd" runat="server" ChildrenAsTriggers="false"
                        UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                            <table cellpadding="2" cellspacing="2">
                                <tr style="padding: 5px;">
                                    <td style="text-align: right">
                                        <label class="control-label">ปี: &nbsp;</label></td>
                                    <td>
                                        <asp:DropDownList class="form-control selectpicker" ID="cmbYear" runat="server" Width="100">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="padding: 5px;">
                                    <td style="text-align: right">
                                        <label class="control-label">แบบสอบถาม: &nbsp;</label></td>
                                    <td>
                                        <asp:DropDownList class="form-control selectpicker marginTp" ID="ddlName" runat="server" Width="400" DataValueField="NTYPEVISITFORMID" DataTextField="STYPEVISITFORMNAME"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <center>
                                <table>
                                    <tr>
                                        <td style="padding-right:5px;">
                                            <dx:ASPxButton ID="btnSave" runat="server" Text="บันทึก" OnClick="btnSave_Click" AutoPostBack="true">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="btnClose" runat="server" Text="ปิด" OnClick="btnClose_Click" AutoPostBack="true">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

