﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data.OracleClient;
using DevExpress.Web.ASPxCallbackPanel;

public partial class service_cost : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<TAddrowgvw> lstAddnewrowgvw = new List<TAddrowgvw>();
    private static List<TAddrow> lstAddnewrow = new List<TAddrow>();

    protected void Page_Load(object sender, EventArgs e)
    {


        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        // gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvwOhter.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwOhter_HtmlDataCellPrepared);
        gvwMs.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvwMs_HtmlDataCellPrepared);
        // Listdata();

        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            lstAddnewrowgvw.Clear();
            lstAddnewrow.Clear();
            Listdata();
            ListMSdata();
            this.AssignAuthen();
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                //Response.Redirect("NotAuthorize.aspx");  
            }
            if (!CanWrite)
            {
                btnEdit.Enabled = false;
                btnAdd.Enabled = false;
                btnAddother.Enabled = false;
                btnSubmit.Enabled = false;
               // gvw.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {



    }

    void gvwOhter_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "No")
        {
            ASPxLabel lblNoOther = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "lblNoOther") as ASPxLabel;
            lblNoOther.Text = "" + (e.VisibleIndex + 1) + ".";
        }
        ASPxTextBox txtMenu = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "txtMenu") as ASPxTextBox;
        ASPxTextBox txtOhterservice = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "txtOhterservice") as ASPxTextBox;
        ASPxTextBox txtDescription = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "txtDescription") as ASPxTextBox;
        ASPxCheckBox CheckFrom = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "CheckFrom") as ASPxCheckBox;
        ASPxButton btnDeleteOther = gvwOhter.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDeleteOther") as ASPxButton;

        txtMenu.ClientInstanceName = txtMenu.ID + "_" + e.VisibleIndex;
        txtOhterservice.ClientInstanceName = txtOhterservice.ID + "_" + e.VisibleIndex;
        txtDescription.ClientInstanceName = txtDescription.ID + "_" + e.VisibleIndex;
        CheckFrom.ClientInstanceName = CheckFrom.ID + "_" + e.VisibleIndex;

        if (!CanWrite)
        {
            btnDeleteOther.Visible = false;
        }
    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "No")
        {
            ASPxLabel lblNo = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "lblNo") as ASPxLabel;
            lblNo.Text = "" + (e.VisibleIndex + 1) + ".";

        }
                
        ASPxTextBox txtStartCapacity = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtStartCapacity") as ASPxTextBox;
        ASPxTextBox txtEndCapacity = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtEndCapacity") as ASPxTextBox;

        ASPxTextBox txtcarcustomer = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarcustomer") as ASPxTextBox;
        ASPxTextBox txtcarservice = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarservice") as ASPxTextBox;
        ASPxTextBox txtcargov = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcargov") as ASPxTextBox;
        ASPxTextBox txtcarother = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarother") as ASPxTextBox;

        ASPxTextBox txtcarcustomerO = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarcustomerO") as ASPxTextBox;
        ASPxTextBox txtcarserviceO = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarserviceO") as ASPxTextBox;
        ASPxTextBox txtcargovO = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcargovO") as ASPxTextBox;
        ASPxTextBox txtcarotherO = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtcarotherO") as ASPxTextBox;
        ASPxButton btnDelete = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDelete") as ASPxButton;
       
        //config ClientInstanceName
        txtStartCapacity.ClientInstanceName = txtStartCapacity.ID + "_" + e.VisibleIndex;
        txtEndCapacity.ClientInstanceName = txtEndCapacity.ID + "_" + e.VisibleIndex;

        txtcarcustomer.ClientInstanceName = txtcarcustomer.ID + "_" + e.VisibleIndex;
        txtcarservice.ClientInstanceName = txtcarservice.ID + "_" + e.VisibleIndex;
        txtcargov.ClientInstanceName = txtcargov.ID + "_" + e.VisibleIndex;
        txtcarother.ClientInstanceName = txtcarother.ID + "_" + e.VisibleIndex;
        txtcarcustomerO.ClientInstanceName = txtcarcustomerO.ID + "_" + e.VisibleIndex;
        txtcarserviceO.ClientInstanceName = txtcarserviceO.ID + "_" + e.VisibleIndex;
        txtcargovO.ClientInstanceName = txtcargovO.ID + "_" + e.VisibleIndex;
        txtcarotherO.ClientInstanceName = txtcarotherO.ID + "_" + e.VisibleIndex;
        if (!CanWrite)
        {
           btnDelete.Visible = false;
        }
    }

    void gvwMs_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        ASPxTextBox txtService = gvwMs.FindRowCellTemplateControl(e.VisibleIndex, null, "txtService") as ASPxTextBox;

        //config ClientInstanceName
        txtService.ClientInstanceName = txtService.ID + "_" + e.VisibleIndex;


    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");

        //เก็บข้อมูลลงลิสใหม่เพื่อนำไปใช้ใรการเก็บ
        GetValueTOListGvw(gvw);
        GetValueTOListGvwOther(gvwOhter);

        switch (param[0])
        {

            case "save":

                #region วัดน้ำ
                //เช็คว่ามีข้อมูลหรือไม่
                if (lstAddnewrowgvw.Count > 0)
                {
                    string strsql = @"INSERT INTO TBL_SERVICECOST(SERVICECOST_ID, SERVICE_ID, REQTYPE_ID,START_CAPACITY, END_CAPACITY, CARCATE_ID,CAR_GRADE, SERVICECHARGE, UNIT, CAUSE_ISALL, ISACTIVE_FLAG, CREATE_DATE, CREATE_BY,NROWADD) 
                                  VALUES ('{0}','00001','01',{1},{2},'{3}','{4}',{5}, 'ลิตร','Y', 'Y',sysdate,{6},{7})";

                    string sqlUpdate = @"UPDATE TBL_SERVICECOST
                                        SET    
                                               START_CAPACITY = {0},
                                               END_CAPACITY   = {1},
                                               SERVICECHARGE  = {2},
                                               UPDATE_DATE    = sysdate,
                                               UPDATE_BY      = {3}      
                                       WHERE CARCATE_ID = '{4}' AND CAR_GRADE = '{5}' AND NROWADD = {6} AND SERVICE_ID = '00001'";

                    foreach (var item in lstAddnewrowgvw)
                    {
                        string START_CAPACITY = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.START_CAPACITY) ? item.START_CAPACITY : "null"));
                        string END_CAPACITY = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.END_CAPACITY) ? item.END_CAPACITY : "null"));
                        string carcustomer = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carcustomer) ? item.carcustomer : "null"));
                        string carservice = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carservice) ? item.carservice : "null"));
                        string cargov = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.cargov) ? item.cargov : "null"));
                        string carother = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carother) ? item.carother : "null"));
                        string carcustomerO = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carcustomerO) ? item.carcustomerO : "null"));
                        string cargovO = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.cargovO) ? item.cargovO : "null"));
                        string carserviceO = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carserviceO) ? item.carserviceO : "null"));
                        string carotherO = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.carotherO) ? item.carotherO : "null"));
                        string NROWADD = CommonFunction.ReplaceInjection(item.NROWADD + "" != "" ? item.NROWADD + "" : "null");

                        #region ดึง NROWADD เพื่อเก็บว่า row ที่เท่าไหร่
                        string QuerynRowadd = "SELECT NROWADD FROM TBL_SERVICECOST WHERE SERVICE_ID = '00001' GROUP BY NROWADD ORDER BY NROWADD DESC";
                        DataTable dt = CommonFunction.Get_Data(conn, QuerynRowadd);

                        string nROw = "";
                        if (dt.Rows.Count > 0)
                        {
                            nROw = CommonFunction.ReplaceInjection((int.Parse(dt.Rows[0]["NROWADD"] + "") + 1) + "");
                        }
                        else
                        {
                            nROw = "0";
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(item.NROWADD + ""))
                        {
                            if (dt.Select("NROWADD = '" + item.NROWADD + "'").Count() > 0)
                            {
                                string sqlchkUpdate = @"SELECT START_CAPACITY ,END_CAPACITY,SERVICECHARGE FROM  TBL_SERVICECOST
                                                    WHERE CARCATE_ID = '{0}' AND CAR_GRADE = '{1}' AND NROWADD = {5} AND SERVICE_ID = '00001'
                                                    UNION 
                                                    SELECT {2},{3},{4}  FROM DUAL";


                                //รถใหม่ กรณีเป็นรถลูกค้า

                                if (CheckDataOld("01", "1", NROWADD) > 0)
                                {
                                    int chkcarcustomer = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "01", "1", START_CAPACITY, END_CAPACITY, carcustomer, NROWADD));
                                    if (chkcarcustomer > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carcustomer, UID, "01", "1", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "01", "1", carcustomer, UID, NROWADD));
                                }

                                if (CheckDataOld("02", "1", NROWADD) > 0)
                                {
                                    //รถใหม่ กรณีเป็นรถรับจ้าง
                                    int chkcarservice = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "02", "1", START_CAPACITY, END_CAPACITY, carservice, NROWADD));
                                    if (chkcarservice > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carservice, UID, "02", "1", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "02", "1", carservice, UID, NROWADD));
                                }

                                if (CheckDataOld("04", "1", NROWADD) > 0)
                                {
                                    //รถใหม่ กรณีเป็นรถราชการ
                                    int chkcargov = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "04", "1", START_CAPACITY, END_CAPACITY, cargov, NROWADD));
                                    if (chkcargov > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, cargov, UID, "04", "1", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "04", "1", cargov, UID, NROWADD));
                                }

                                if (CheckDataOld("03", "1", NROWADD) > 0)
                                {
                                    //รถใหม่ กรณีเป็นรถอื่นๆ
                                    int chkcarother = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "03", "1", START_CAPACITY, END_CAPACITY, carother, NROWADD));
                                    if (chkcarother > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carother, UID, "03", "1", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "03", "1", carother, UID, NROWADD));
                                }


                                if (CheckDataOld("01", "0", NROWADD) > 0)
                                {
                                    //รถเก่า กรณีเป็นรถลูกค้า
                                    int chkcarcustomerO = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "01", "0", START_CAPACITY, END_CAPACITY, carcustomerO, NROWADD));
                                    if (chkcarcustomerO > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carcustomerO, UID, "01", "0", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "01", "0", carcustomerO, UID, NROWADD));
                                }

                                if (CheckDataOld("02", "0", NROWADD) > 0)
                                {
                                    //รถเก่า กรณีเป็นรถรับจ้าง
                                    int chkcarserviceO = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "02", "0", START_CAPACITY, END_CAPACITY, carservice, NROWADD));
                                    if (chkcarserviceO > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carserviceO, UID, "02", "0", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "02", "0", carserviceO, UID, NROWADD));
                                }

                                if (CheckDataOld("04", "0", NROWADD) > 0)
                                {
                                    //รถเก่า กรณีเป็นรถราชการ
                                    int chkcargovO = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "04", "0", START_CAPACITY, END_CAPACITY, cargovO, NROWADD));
                                    if (chkcargovO > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, cargovO, UID, "04", "0", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "04", "0", cargovO, UID, NROWADD));
                                }

                                if (CheckDataOld("03", "0", NROWADD) > 0)
                                {
                                    //รถเก่า กรณีเป็นรถอื่นๆ
                                    int chkcarotherO = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdate, "03", "0", START_CAPACITY, END_CAPACITY, carotherO, NROWADD));
                                    if (chkcarotherO > 1)
                                    {
                                        string ToBase = string.Format(sqlUpdate, START_CAPACITY, END_CAPACITY, carotherO, UID, "03", "0", NROWADD);
                                        AddTODB(ToBase);
                                    }
                                }
                                else
                                {
                                    AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "03", "0", carotherO, UID, NROWADD));
                                }
                            }
                        }
                        else
                        {
                            //รถใหม่ กรณีเป็นรถลูกค้า
                            if (!string.IsNullOrEmpty(item.carcustomer))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "01", "1", carcustomer, UID, nROw));
                            }
                            //รถใหม่ กรณีเป็นรถรับจ้าง
                            if (!string.IsNullOrEmpty(item.carservice))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "02", "1", carservice, UID, nROw));
                            }
                            //รถใหม่ กรณีเป็นรถอื่นๆ
                            if (!string.IsNullOrEmpty(item.carother))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "03", "1", carother, UID, nROw));
                            }
                            //รถใหม่ กรณีเป็นรถราชการนๆ
                            if (!string.IsNullOrEmpty(item.cargov))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "04", "1", cargov, UID, nROw));
                            }

                            //รถเก่า กรณีเป็นรถลูกค้า
                            if (!string.IsNullOrEmpty(item.carcustomerO))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "01", "0", carcustomerO, UID, nROw));
                            }
                            //รถเก่า กรณีเป็นรถรับจ้าง
                            if (!string.IsNullOrEmpty(item.carserviceO))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "02", "0", carserviceO, UID, nROw));
                            }

                            //รถเก่า กรณีเป็นรถอื่นๆ
                            if (!string.IsNullOrEmpty(item.carotherO))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "03", "0", carotherO, UID, nROw));
                            }
                            //รถเก่า กรณีเป็นรถราชการ
                            if (!string.IsNullOrEmpty(item.cargovO))
                            {
                                AddTODB(string.Format(strsql, Gen_ID(), START_CAPACITY, END_CAPACITY, "04", "0", cargovO, UID, nROw));
                            }
                        }
                    }


                    //gvw.CancelEdit();

                }
                #endregion

                #region เพิ่มแป้น ตีซีล พ่นสาระสำคัญ ค่าสำเนาใบรับรองการตรวจ OLD

                //                string sqlchkUpdate2 = @"SELECT SERVICECHARGE FROM  TBL_SERVICECOST
                //                                      WHERE  SERVICE_ID = '{0}'";

                //                string sqlchkUpdateData2 = @"SELECT SERVICECHARGE FROM  TBL_SERVICECOST
                //                                      WHERE  SERVICE_ID = '{0}'
                //                                      UNION 
                //                                      SELECT {1}  FROM DUAL";

                //                string sqlUpdateToBase2 = @"UPDATE TBL_SERVICECOST
                //                                        SET    
                //                                               SERVICECHARGE  = {0},
                //                                               UPDATE_DATE    = sysdate,
                //                                               UPDATE_BY      = {1}      
                //                                       WHERE  SERVICE_ID = '{2}'";

                //                string strsql2 = @"INSERT INTO TBL_SERVICECOST(SERVICECOST_ID, SERVICE_ID, REQTYPE_ID, SERVICECHARGE, UNIT, CAUSE_ISALL, ISACTIVE_FLAG, CREATE_DATE, CREATE_BY) 
                //                                  VALUES ('{0}','{1}','{2}',{3},'{4}','Y', 'Y',sysdate,'{5}')";
                //                DataTable dtUpdate = new DataTable();

                //                dtUpdate = CommonFunction.Get_Data(conn, string.Format(sqlchkUpdate2, "00002"));
                //                string Pan = CommonFunction.ReplaceInjection(txtPanValue.Text);
                //                Pan = !string.IsNullOrEmpty(Pan) ? Pan : "null";
                //                if (dtUpdate.Rows.Count > 0)
                //                {

                //                    int chkDataToUpdate = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdateData2, "00002", Pan));
                //                    if (chkDataToUpdate > 1)
                //                    {
                //                        string Update = string.Format(sqlUpdateToBase2, Pan, UID, "00002");
                //                        AddTODB(Update);
                //                    }
                //                }
                //                else
                //                {
                //                    AddTODB(string.Format(strsql2, Gen_ID(), "00002", "02", Pan, "แป้น", UID));
                //                }



                //                string seal = CommonFunction.ReplaceInjection(txtSeal.Text);
                //                dtUpdate = CommonFunction.Get_Data(conn, string.Format(sqlchkUpdate2, "00003"));
                //                seal = !string.IsNullOrEmpty(seal) ? seal : "null";
                //                if (dtUpdate.Rows.Count > 0)
                //                {

                //                    int chkDataToUpdate = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdateData2, "00003", seal));
                //                    if (chkDataToUpdate > 1)
                //                    {
                //                        string Update = string.Format(sqlUpdateToBase2, seal, UID, "00003");
                //                        AddTODB(Update);
                //                    }
                //                }
                //                else
                //                {
                //                    AddTODB(string.Format(strsql2, Gen_ID(), "00003", "02", seal, "", UID));
                //                }



                //                string Document = CommonFunction.ReplaceInjection(txtDocument.Text);
                //                dtUpdate = CommonFunction.Get_Data(conn, string.Format(sqlchkUpdate2, "00005"));
                //                Document = !string.IsNullOrEmpty(Document) ? Document : "null";
                //                if (dtUpdate.Rows.Count > 0)
                //                {

                //                    int chkDataToUpdate = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdateData2, "00005", Document));
                //                    if (chkDataToUpdate > 1)
                //                    {
                //                        string Update = string.Format(sqlUpdateToBase2, Document, UID, "00005");
                //                        AddTODB(Update);
                //                    }
                //                }
                //                else
                //                {
                //                    AddTODB(string.Format(strsql2, Gen_ID(), "00005", "03", Document, "ชุด", UID));
                //                }



                //                string Paintsara = CommonFunction.ReplaceInjection(txtPaintsara.Text);
                //                dtUpdate = CommonFunction.Get_Data(conn, string.Format(sqlchkUpdate2, "00004"));
                //                Paintsara = !string.IsNullOrEmpty(Paintsara) ? Paintsara : "null";
                //                if (dtUpdate.Rows.Count > 0)
                //                {

                //                    int chkDataToUpdate = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdateData2, "00004", Paintsara));
                //                    if (chkDataToUpdate > 1)
                //                    {
                //                        string Update = string.Format(sqlUpdateToBase2, Paintsara, UID, "00004");
                //                        AddTODB(Update);
                //                    }
                //                }
                //                else
                //                {
                //                    AddTODB(string.Format(strsql2, Gen_ID(), "00004", "04", Paintsara, "", UID));
                //                }



                #endregion

                #region เพิ่มแป้น ตีซีล พ่นสาระสำคัญ ค่าสำเนาใบรับรองการตรวจ NEW

                int sCountgvwms = gvwMs.VisibleRowCount;
                for (int i = 0; i < sCountgvwms; i++)
                {
                    dynamic data = gvwMs.GetRowValues(i, "SERVICE_ID");
                    ASPxTextBox txtService = gvwMs.FindRowCellTemplateControl(i, null, "txtService") as ASPxTextBox;
                    string sValue = txtService.Text;
                    string ReqTypeID = "";
                    string ServiceID = data;

                    switch (ServiceID)
                    {
                        case "00002": ReqTypeID = "02";
                            break;
                        case "00003": ReqTypeID = "02";
                            break;
                        case "00004": ReqTypeID = "04";
                            break;
                        case "00005": ReqTypeID = "03";
                            break;
                        case "00011": ReqTypeID = "02";
                            break;
                    }

                    AddToBaseMs(data, UID, ReqTypeID, sValue);
                }


                #endregion

                #region งานบริการแก้ไขเพิ่มเติม

                string sqlserveiceother = @"INSERT INTO TBL_SERVICE_MASTERDATA (SERVICE_ID, SERVICE_NAME, ISACTIVE_FLAG, CREATE_DATE, CREATE_BY, FIX_FLAG) 
                                VALUES ( '{0}','{1}','{2}',sysdate,'{3}','N' )";
                string sqlserveiceother2 = @"INSERT INTO TBL_SERVICECOST (SERVICECOST_ID, SERVICE_ID, SERVICECHARGE, DESCRIPTION, CAUSE_ISALL, ISACTIVE_FLAG,CREATE_DATE, CREATE_BY) 
                                VALUES ( '{0}','{1}',{2},'{3}','Y','Y',sysdate,'{4}')";

                string sqlchkserveiceother = @"SELECT SERVICE_NAME,CHECKFROM
                                               FROM TBL_SERVICE_MASTERDATA
                                               WHERE SERVICE_ID = '{0}' AND ISACTIVE_FLAG='Y' AND FIX_FLAG ='N' 
                                               UNION 
                                               SELECT '{1}','{2}'  FROM DUAL";

                string sqlchkserveiceother2 = @"SELECT SERVICECHARGE,  DESCRIPTION
                                                FROM TBL_SERVICECOST
                                                WHERE SERVICE_ID = '{0}'
                                                UNION
                                                SELECT {1},'{2}'  FROM DUAL";
                //var dada = lstAddnewrow.Where(w => w.DESCRIPTION != "" || w.SERVICE_NAME != "" || w.SERVICECHARGE != "").Count();
                foreach (var item in lstAddnewrow.Where(w => w.DESCRIPTION != "" || w.SERVICE_NAME != "" || w.SERVICECHARGE != ""))
                {

                    string SERVICE_ID = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.SERVICE_ID) ? item.SERVICE_ID : "null"));
                    string SERVICE_NAME = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.SERVICE_NAME) ? item.SERVICE_NAME : "null"));
                    string SERVICECHARGE = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.SERVICECHARGE) ? item.SERVICECHARGE : "null"));
                    string DESCRIPTION = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.DESCRIPTION) ? item.DESCRIPTION : "null"));
                    string CHECKFROM = CommonFunction.ReplaceInjection((!string.IsNullOrEmpty(item.CHECKFROM) ? item.CHECKFROM : "null"));
                    //ดึงไอดีจาก ตารางมาสเตอร์มา+1แล้วเก็บเป็นไอดีถัดไป
                    string strsql = @"SELECT  SERVICE_ID FROM TBL_SERVICE_MASTERDATA ORDER BY SERVICE_ID DESC ";
                    DataTable dtSERVICE_ID = CommonFunction.Get_Data(conn, strsql);
                    string ID = (int.Parse(dtSERVICE_ID.Rows[0]["SERVICE_ID"] + "") + 1).ToString().PadLeft(5, '0');
                    string Updatesql3 = @"UPDATE TBL_SERVICE_MASTERDATA
                                         SET    SERVICE_NAME  = '{1}',
                                            CHECKFROM  = '{3}',
                                         UPDATE_DATE    = sysdate,
                                         UPDATE_BY      = '{2}'      
                                         WHERE  SERVICE_ID    = '{0}'";

                    string sqlUpdatedata3 = @"UPDATE TBL_SERVICECOST
                                            SET    SERVICECHARGE  = {1},
                                                   DESCRIPTION    = '{2}',
                                                   UPDATE_DATE    = sysdate,
                                                   UPDATE_BY      = '{3}'
                                            WHERE  SERVICE_ID = '{0}'";
                    if (!string.IsNullOrEmpty(item.SERVICE_ID))
                    {
                        int chk = CommonFunction.Count_Value(conn, string.Format(sqlchkserveiceother, SERVICE_ID, SERVICE_NAME, CHECKFROM));
                        if (chk > 1)
                        {
                            AddTODB(string.Format(Updatesql3, SERVICE_ID, NullreturnEmpty(SERVICE_NAME), UID, CHECKFROM));

                        }

                        int chk2 = CommonFunction.Count_Value(conn, string.Format(sqlchkserveiceother2, SERVICE_ID, SERVICECHARGE, DESCRIPTION));
                        if (chk2 > 1)
                        {
                            AddTODB(string.Format(sqlUpdatedata3, SERVICE_ID, SERVICECHARGE, NullreturnEmpty(DESCRIPTION), UID));
                        }

                    }
                    else
                    {
                        //เก็บข้อมูลมาสเตอร่ลง TBL_SERVICE_MASTERDATA
                        AddTODB(string.Format(sqlserveiceother, ID, NullreturnEmpty(SERVICE_NAME), "Y", UID));
                        //เก็บข้อมูล ค่าใช้จ่ายลง TBL_SERVICECOST
                        AddTODB(string.Format(sqlserveiceother2, Gen_ID(), ID, SERVICECHARGE, NullreturnEmpty(DESCRIPTION), UID));
                    }
                }
                #endregion

                #region Percent ความจุของแป้นที่ 2

                string QueryPan2Chk = @"SELECT CONFIG_ID, CONFIG_VALUE, CONFIG_UNIT,  CONFIG_DESCRIPTION, CONFIG_FLAG FROM TBL_CONFIGURATIONS WHERE CONFIG_ID = '002' AND CONFIG_FLAG = 'Y' AND CONFIG_VALUE = " + CommonFunction.ReplaceInjection(txtPan2Capacity.Text) + "";
                int ChkPan2 = CommonFunction.Count_Value(conn, QueryPan2Chk);
                if (ChkPan2 > 0)
                {
                    string UpdatePan2 = @"UPDATE TBL_CONFIGURATIONS
                                          SET    
                                          CONFIG_VALUE  = " + CommonFunction.ReplaceInjection(txtPan2Capacity.Text) + @",
                                          WHERE CONFIG_ID = '002' AND CONFIG_FLAG = 'Y';";
                }
                else
                {

                }



                #endregion
                lstAddnewrowgvw.Clear();
                lstAddnewrow.Clear();
                Listdata();
                break;
            case "cancel": xcpn.JSProperties["cpRedirectTo"] = "menustandard.aspx";
                break;
            case "delete":

                if (!string.IsNullOrEmpty(txtIndexgvw.Text))
                {
                    int row = int.Parse(txtIndexgvw.Text.Trim());

                    dynamic data = gvw.GetRowValues(row, "SERVICE_ID", "NROWADD");

                    if (data[0] != null && data[1] != null)
                    {
                        string sqldelete = @"UPDATE TBL_SERVICECOST
                                            SET    
                                               ISACTIVE_FLAG  = 'N',
                                               UPDATE_DATE    = sysdate,
                                               UPDATE_BY      ='{2}'
                                            WHERE  SERVICE_ID = '{0}' AND NROWADD ={1}";

                        AddTODB(string.Format(sqldelete, data[0], data[1], UID));

                        ListWATNUM();
                    }
                    else
                    {
                        if (lstAddnewrowgvw.Count > 0)
                        {
                            dynamic drow = gvw.GetRowValues(row, "NO");

                            lstAddnewrowgvw.RemoveAll(w => w.NO == drow);
                        }
                    }

                    gvw.DataSource = lstAddnewrowgvw;
                    gvw.DataBind();
                    //Listdata();
                }



                break;

            case "deleteService":

                if (!string.IsNullOrEmpty(txtIndexgvwService.Text))
                {
                    int row = int.Parse(txtIndexgvwService.Text.Trim());

                    dynamic data = gvwOhter.GetRowValues(row, "SERVICE_ID");

                    if (data != null && !string.IsNullOrEmpty(data))
                    {
                        string sqldelete = @"UPDATE TBL_SERVICECOST
                                            SET    
                                               ISACTIVE_FLAG  = 'N',
                                               UPDATE_DATE    = sysdate,
                                               UPDATE_BY      ='{1}'
                                            WHERE  SERVICE_ID = '{0}'";

                        string sqldeleteMS = @"UPDATE TBL_SERVICE_MASTERDATA
                                            SET    
                                               ISACTIVE_FLAG  = 'N',
                                               UPDATE_DATE    = sysdate,
                                               UPDATE_BY      ='{1}'
                                            WHERE  SERVICE_ID = '{0}'";

                        AddTODB(string.Format(sqldelete, data, UID));
                        AddTODB(string.Format(sqldeleteMS, data, UID));

                        ListServiceAdd();
                    }
                    else
                    {
                        if (lstAddnewrow.Count > 0)
                        {
                            dynamic drow = gvwOhter.GetRowValues(row, "NROWADD");
                            //lstAddnewrowgvw.RemoveAll(w => w.NO == drow);
                            lstAddnewrow.RemoveAll(w => w.NROWADD == drow);
                        }
                    }
                    gvwOhter.DataSource = lstAddnewrow;
                    gvwOhter.DataBind();
                    //Listdata();
                }



                break;



        }



    }
    //เมื่อกริด CallBack ทำงานฟังชั่นนี้
    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string UID = CommonFunction.ReplaceInjection(Session["UserID"] + "");
        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {
                    case "add":

                        lstAddnewrowgvw.Add(new TAddrowgvw
                        {
                            NROWADD = null,
                            START_CAPACITY = "",
                            END_CAPACITY = "",
                            carcustomer = "",
                            carservice = "",
                            cargov = "",
                            carother = "",
                            carcustomerO = "",
                            carserviceO = "",
                            cargovO = "",
                            carotherO = "",
                            NO = lstAddnewrowgvw.Count > 0 ? (lstAddnewrowgvw.Select(a => a.NO).OrderByDescending(o => o).FirstOrDefault() + 1) : 1,
                        });



                        int s = lstAddnewrowgvw.Count;
                        if (s > 0)
                        {


                            for (int i = 0; i < s; i++)
                            {
                                //lstAddnewrowgvw.RemoveAll(w => w.NO == i + 1);
                                lstAddnewrowgvw.RemoveAt(0);
                                ASPxTextBox txtStartCapacity = gvw.FindRowCellTemplateControl(i, null, "txtStartCapacity") as ASPxTextBox;
                                ASPxTextBox txtEndCapacity = gvw.FindRowCellTemplateControl(i, null, "txtEndCapacity") as ASPxTextBox;
                                ASPxTextBox txtcarcustomer = gvw.FindRowCellTemplateControl(i, null, "txtcarcustomer") as ASPxTextBox;
                                ASPxTextBox txtcarservice = gvw.FindRowCellTemplateControl(i, null, "txtcarservice") as ASPxTextBox;
                                ASPxTextBox txtcargov = gvw.FindRowCellTemplateControl(i, null, "txtcargov") as ASPxTextBox;
                                ASPxTextBox txtcarother = gvw.FindRowCellTemplateControl(i, null, "txtcarother") as ASPxTextBox;
                                ASPxTextBox txtcarcustomerO = gvw.FindRowCellTemplateControl(i, null, "txtcarcustomerO") as ASPxTextBox;
                                ASPxTextBox txtcarserviceO = gvw.FindRowCellTemplateControl(i, null, "txtcarserviceO") as ASPxTextBox;
                                ASPxTextBox txtcargovO = gvw.FindRowCellTemplateControl(i, null, "txtcargovO") as ASPxTextBox;
                                ASPxTextBox txtcarotherO = gvw.FindRowCellTemplateControl(i, null, "txtcarotherO") as ASPxTextBox;

                                int? NROWADD = null;
                                string sNROWADD = gvw.GetRowValues(i, "NROWADD") + "";
                                if (!string.IsNullOrEmpty(sNROWADD))
                                {
                                    NROWADD = int.Parse(sNROWADD);
                                }

                                lstAddnewrowgvw.Add(new TAddrowgvw
                                {
                                    NO = i + 1,
                                    NROWADD = NROWADD,
                                    START_CAPACITY = txtStartCapacity != null ? txtStartCapacity.Text : "",
                                    END_CAPACITY = txtEndCapacity != null ? txtEndCapacity.Text : "",
                                    carcustomer = txtcarcustomer != null ? txtcarcustomer.Text : "",
                                    carservice = txtcarservice != null ? txtcarservice.Text : "",
                                    cargov = txtcargov != null ? txtcargov.Text : "",
                                    carother = txtcarother != null ? txtcarother.Text : "",
                                    carcustomerO = txtcarcustomerO != null ? txtcarcustomerO.Text : "",
                                    carserviceO = txtcarserviceO != null ? txtcarserviceO.Text : "",
                                    cargovO = txtcargovO != null ? txtcargovO.Text : "",
                                    carotherO = txtcarotherO != null ? txtcarotherO.Text : "",
                                });
                            }
                        }
                        gvw.DataSource = lstAddnewrowgvw;
                        gvw.DataBind();

                        gvw.JSProperties["cpGvwCount"] = gvw.VisibleRowCount;


                        break;



                }
                break;
        }

    }

    protected void gvwOhter_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {
                    case "addother":

                        lstAddnewrow.Add(new TAddrow
                        {
                            NROWADD = null,
                            SERVICE_ID = "",
                            SERVICE_NAME = "",
                            SERVICECHARGE = "",
                            DESCRIPTION = "",
                            CHECKFROM = ""
                        });



                        int s = lstAddnewrow.Count;
                        if (s > 0)
                        {
                            for (int i = 0; i < s; i++)
                            {
                                lstAddnewrow.RemoveAt(0);
                                //lstAddnewrow.RemoveAll(w => w.NROWADD == i);


                                //ASPxTextBox txtID = gvwOhter.FindRowCellTemplateControl(i, null, "txtID") as ASPxTextBox;
                                string SERVICE_ID = gvwOhter.GetRowValues(i, "SERVICE_ID") + "";
                                ASPxTextBox txtMenu = gvwOhter.FindRowCellTemplateControl(i, null, "txtMenu") as ASPxTextBox;
                                ASPxTextBox txtOhterservice = gvwOhter.FindRowCellTemplateControl(i, null, "txtOhterservice") as ASPxTextBox;
                                ASPxTextBox txtDescription = gvwOhter.FindRowCellTemplateControl(i, null, "txtDescription") as ASPxTextBox;
                                ASPxCheckBox CheckFrom = gvwOhter.FindRowCellTemplateControl(i, null, "CheckFrom") as ASPxCheckBox;

                                int? NROWADD = null;
                                string sNROWADD = gvwOhter.GetRowValues(i, "NROWADD") + "";
                                if (!string.IsNullOrEmpty(sNROWADD))
                                {
                                    NROWADD = int.Parse(sNROWADD);
                                }

                                lstAddnewrow.Add(new TAddrow
                                {
                                    SERVICE_ID = SERVICE_ID,
                                    NROWADD = i+1,
                                    SERVICE_NAME = txtMenu != null ? txtMenu.Text : "",
                                    SERVICECHARGE = txtOhterservice != null ? txtOhterservice.Text : "",
                                    DESCRIPTION = txtDescription != null ? txtDescription.Text : "",
                                    CHECKFROM = CheckFrom != null ? (CheckFrom.Checked ? "Y" : "N") : "N"
                                });

                            }
                        }

                        gvwOhter.DataSource = lstAddnewrow;
                        gvwOhter.DataBind();

                        gvwOhter.JSProperties["cpGvwOhterCount"] = gvwOhter.VisibleRowCount;


                        break;
                }
                break;
        }

    }

    private string Gen_ID()
    {
        string Result = "";

        string strsql = "SELECT SERVICECOST_ID FROM TBL_SERVICECOST ORDER BY SERVICECOST_ID DESC";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);


        if (dt.Rows.Count > 0)
        {
            string Date = DateTime.Now.ToString("yyMM", new CultureInfo("en-US"));
            string sID = dt.Rows[0]["SERVICECOST_ID"] + "";
            sID = sID.Substring(4, 5);
            int nID = int.Parse(sID) + 1;

            string NewID = Date + nID.ToString().PadLeft(5, '0');


            Result = NewID;
        }
        else
        {

            Result = DateTime.Now.ToString("yyMM", new CultureInfo("en-US")) + "00001";
        }

        return Result;
    }

    private void AddTODB(string strQuery)
    {
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    private void Listdata()
    {
        #region วัดน้ำ

        //        string Query = @"SELECT NROWADD+1 as NOROW,
        //     Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG,NROWADD
        //     ,New1.New1Value
        //     ,New2.New2Value
        //     ,New3.New3Value
        //      ,Old1.Old1Value
        //     ,Old2.Old2Value
        //     ,Old3.Old3Value
        //FROM TBL_SERVICECOST Serv
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New1Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '01' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New1
        //ON Serv.SERVICE_ID = New1.SERVICE_ID  AND Serv.NROWADD = New1.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New2Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '02' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New2
        //ON Serv.SERVICE_ID = New2.SERVICE_ID  AND Serv.NROWADD = New2.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New3Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '03' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New3
        //ON Serv.SERVICE_ID = New3.SERVICE_ID AND Serv.NROWADD = New3.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old1Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '01' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old1
        //ON Serv.SERVICE_ID = Old1.SERVICE_ID AND  Serv.NROWADD = Old1.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old2Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '02' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old2
        //ON Serv.SERVICE_ID = Old2.SERVICE_ID AND  Serv.NROWADD = Old2.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old3Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '03' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old3
        //ON Serv.SERVICE_ID = Old3.SERVICE_ID AND  Serv.NROWADD = Old3.NROWADD
        //GROUP BY      Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG
        //,New1.New1Value,New2.New2Value,New3.New3Value,Old1.Old1Value,Old2.Old2Value ,Old3.Old3Value,NROWADD,NROWADD+1
        //ORDER BY NROWADD ASC
        //";

        string Query = @"
SELECT  ROWNUM as NO,  sdata.SERVICE_ID, sdata.REQTYPE_ID,sdata.START_CAPACITY, sdata.END_CAPACITY,sdata.UNIT,  sdata.CAUSE_ISALL, sdata.ISACTIVE_FLAG,sdata.NROWADD
     ,sdata.carcustomer
     ,sdata.carservice
     ,sdata.cargov
     ,sdata.carother
     ,sdata.carcustomerO
     ,sdata.carserviceO
     ,sdata.cargovO
     ,sdata.carotherO FROM
(
SELECT 
     Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv.UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG,Serv.NROWADD
     ,New1.carcustomer
     ,New2.carservice
     ,New3.carother
     ,New4.cargov
     ,Old1.carcustomerO
     ,Old2.carserviceO
     ,Old3.carotherO
     ,Old4.cargovO
FROM TBL_SERVICECOST Serv
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carcustomer,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '01' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New1
ON Serv.SERVICE_ID = New1.SERVICE_ID  AND Serv.NROWADD = New1.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carservice,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '02' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New2
ON Serv.SERVICE_ID = New2.SERVICE_ID  AND Serv.NROWADD = New2.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carother,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '03' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New3
ON Serv.SERVICE_ID = New3.SERVICE_ID AND Serv.NROWADD = New3.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS cargov,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '04' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New4
ON Serv.SERVICE_ID = New4.SERVICE_ID AND Serv.NROWADD = New4.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carcustomerO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '01' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old1
ON Serv.SERVICE_ID = Old1.SERVICE_ID AND  Serv.NROWADD = Old1.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carserviceO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '02' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old2
ON Serv.SERVICE_ID = Old2.SERVICE_ID AND  Serv.NROWADD = Old2.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carotherO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '03' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old3
ON Serv.SERVICE_ID = Old3.SERVICE_ID AND  Serv.NROWADD = Old3.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS cargovO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '04' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old4
ON Serv.SERVICE_ID = Old4.SERVICE_ID AND  Serv.NROWADD = Old4.NROWADD

WHERE Serv.ISACTIVE_FLAG = 'Y' AND Serv.SERVICE_ID = '00001'
GROUP BY      Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG
,New1.carcustomer,New2.carservice,New3.carother ,New4.cargov,Old1.carcustomerO,Old2.carserviceO ,Old3.carotherO,Old4.cargovO,Serv.NROWADD--,ROWNUM
ORDER BY NROWADD ASC
)sdata";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
            gvw.DataBind();

            txtgvwrowcount.Text = gvw.VisibleRowCount + "";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstAddnewrowgvw.Add(new TAddrowgvw
                {
                    START_CAPACITY = dt.Rows[i]["START_CAPACITY"] + "",
                    END_CAPACITY = dt.Rows[i]["END_CAPACITY"] + "",
                    carcustomer = dt.Rows[i]["carcustomer"] + "",
                    carservice = dt.Rows[i]["carservice"] + "",
                    cargov = dt.Rows[i]["cargov"] + "",
                    carother = dt.Rows[i]["carother"] + "",
                    carcustomerO = dt.Rows[i]["carcustomerO"] + "",
                    carserviceO = dt.Rows[i]["carserviceO"] + "",
                    carotherO = dt.Rows[i]["carotherO"] + "",
                    cargovO = dt.Rows[i]["cargovO"] + "",
                    NO = int.Parse(dt.Rows[i]["NO"] + ""),
                    NROWADD = int.Parse(dt.Rows[i]["NROWADD"] + "")
                });
            }

            gvw.JSProperties["cpGvwCount"] = gvw.VisibleRowCount;
        }
        else
        {
            if (lstAddnewrowgvw.Count > 0)
            {
                gvw.DataSource = lstAddnewrowgvw;
                gvw.DataBind();
                txtgvwrowcount.Text = lstAddnewrowgvw.Count + "";
            }

            gvw.JSProperties["cpGvwCount"] = lstAddnewrowgvw.Count + "";
        }

        #endregion

        #region เพิ่มแป้น ตีซีล พ่นสาระสำคัญ ค่าสำเนาใบรับรองการตรวจ
        //        string Query2 = @"SELECT SERVICECOST_ID, SERVICE_ID, REQTYPE_ID,  CAUSE_ID, TYPE_ID, SERVICENAME,  START_CAPACITY, END_CAPACITY, CARCATE_ID, 
        //                         CAR_GRADE, SERVICECHARGE, UNIT, DESCRIPTION, CAUSE_ISALL, ISACTIVE_FLAG, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY, NROWADD
        //                         FROM TBL_SERVICECOST
        //                         Where ISACTIVE_FLAG = 'Y'";

        //        DataTable dt2 = CommonFunction.Get_Data(conn, Query2);



        //        if (dt2.Rows.Count > 0)
        //        {
        //            txtPanValue.Text = dt2.Select("SERVICE_ID = '00002'").Count() > 0 ? dt2.Select("SERVICE_ID = '00002'")[0].ItemArray[10].ToString() : "";
        //            SetEnableControl(txtPanValue);
        //            txtSeal.Text = dt2.Select("SERVICE_ID = '00003'").Count() > 0 ? dt2.Select("SERVICE_ID = '00003'")[0].ItemArray[10].ToString() : "";
        //            SetEnableControl(txtSeal);
        //            txtPaintsara.Text = dt2.Select("SERVICE_ID = '00004'").Count() > 0 ? dt2.Select("SERVICE_ID = '00004'")[0].ItemArray[10].ToString() : "";
        //            SetEnableControl(txtPaintsara);
        //            txtDocument.Text = dt2.Select("SERVICE_ID = '00005'").Count() > 0 ? dt2.Select("SERVICE_ID = '00005'")[0].ItemArray[10].ToString() : "";
        //            SetEnableControl(txtDocument);
        //        }
        //        else
        //        {
        //            txtPanValue.ClientEnabled = true;
        //            txtSeal.ClientEnabled = true;
        //            txtPaintsara.ClientEnabled = true;
        //            txtDocument.ClientEnabled = true;
        //        }

        #endregion

        #region งานบริการแก้ไขเพิ่มเติม
        string Query3 = @"SELECT  ROWNUM AS NROWADD,MS.SERVICE_ID, MS.SERVICE_NAME,scost.SERVICECHARGE,scost.DESCRIPTION,MS.CHECKFROM FROM 
(
    SELECT SERVICE_ID, SERVICE_NAME,CHECKFROM
    FROM TBL_SERVICE_MASTERDATA WHERE FIX_FLAG ='N' AND ISACTIVE_FLAG ='Y'
)MS
LEFT JOIN 
(
    SELECT SERVICE_ID, SERVICECHARGE,DESCRIPTION FROM TBL_SERVICECOST WHERE  ISACTIVE_FLAG = 'Y'
)scost
ON MS.SERVICE_ID = scost.SERVICE_ID";

        DataTable dt3 = CommonFunction.Get_Data(conn, Query3);
        if (dt3.Rows.Count > 0)
        {
            gvwOhter.DataSource = dt3;
            gvwOhter.DataBind();

            txtgvwohterrowcount.Text = gvwOhter.VisibleRowCount + "";



            for (int i = 0; i < dt3.Rows.Count; i++)
            {
                lstAddnewrow.Add(new TAddrow
                {
                    NROWADD =  int.Parse(dt3.Rows[i]["NROWADD"] + "") ,
                    SERVICE_ID = dt3.Rows[i]["SERVICE_ID"] + "",
                    DESCRIPTION = dt3.Rows[i]["DESCRIPTION"] + "",
                    SERVICE_NAME = dt3.Rows[i]["SERVICE_NAME"] + "",
                    SERVICECHARGE = dt3.Rows[i]["SERVICECHARGE"] + "",
                    CHECKFROM = dt3.Rows[i]["CHECKFROM"] + "" != "" ? dt3.Rows[i]["CHECKFROM"] + "" : "N"
                });
            }
        }

        #endregion

        #region ความจุของแป้นที่ 2 ต้องไม่ต่ำกว่ากี่ % ของแป้นที่ 1

        string QueryPan2 = @"SELECT CONFIG_VALUE FROM TBL_CONFIGURATIONS WHERE CONFIG_ID = '002' AND CONFIG_FLAG = 'Y'";
        DataTable dtPan2 = CommonFunction.Get_Data(conn, QueryPan2);
        if (dtPan2.Rows.Count > 0)
        {
            txtPan2Capacity.Text = dtPan2.Rows[0]["CONFIG_VALUE"] + "";
        }
        else
        {
            txtPan2Capacity.Text = "";
        }

        #endregion
    }

    private void ListWATNUM()
    {
        #region วัดน้ำ

        //        string Query = @"SELECT NROWADD+1 as NOROW,
        //     Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG,NROWADD
        //     ,New1.New1Value
        //     ,New2.New2Value
        //     ,New3.New3Value
        //      ,Old1.Old1Value
        //     ,Old2.Old2Value
        //     ,Old3.Old3Value
        //FROM TBL_SERVICECOST Serv
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New1Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '01' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New1
        //ON Serv.SERVICE_ID = New1.SERVICE_ID  AND Serv.NROWADD = New1.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New2Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '02' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New2
        //ON Serv.SERVICE_ID = New2.SERVICE_ID  AND Serv.NROWADD = New2.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS New3Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '03' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
        //) New3
        //ON Serv.SERVICE_ID = New3.SERVICE_ID AND Serv.NROWADD = New3.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old1Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '01' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old1
        //ON Serv.SERVICE_ID = Old1.SERVICE_ID AND  Serv.NROWADD = Old1.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old2Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '02' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old2
        //ON Serv.SERVICE_ID = Old2.SERVICE_ID AND  Serv.NROWADD = Old2.NROWADD
        //Inner JOIN
        //(
        //    SELECT 
        //         SERVICE_ID ,SERVICECHARGE AS Old3Value,NROWADD
        //    FROM TBL_SERVICECOST
        //    WHERE CARCATE_ID = '03' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
        //) Old3
        //ON Serv.SERVICE_ID = Old3.SERVICE_ID AND  Serv.NROWADD = Old3.NROWADD
        //GROUP BY      Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG
        //,New1.New1Value,New2.New2Value,New3.New3Value,Old1.Old1Value,Old2.Old2Value ,Old3.Old3Value,NROWADD,NROWADD+1
        //ORDER BY NROWADD ASC
        //";

        string Query = @"
SELECT  ROWNUM as NO,  sdata.SERVICE_ID, sdata.REQTYPE_ID,sdata.START_CAPACITY, sdata.END_CAPACITY,sdata.UNIT,  sdata.CAUSE_ISALL, sdata.ISACTIVE_FLAG,sdata.NROWADD
     ,sdata.carcustomer
     ,sdata.carservice
     ,sdata.cargov
     ,sdata.carother
     ,sdata.carcustomerO
     ,sdata.carserviceO
     ,sdata.cargovO
     ,sdata.carotherO FROM
(
SELECT 
     Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv.UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG,Serv.NROWADD
     ,New1.carcustomer
     ,New2.carservice
     ,New3.carother
     ,New4.cargov
     ,Old1.carcustomerO
     ,Old2.carserviceO
     ,Old3.carotherO
     ,Old4.cargovO
FROM TBL_SERVICECOST Serv
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carcustomer,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '01' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New1
ON Serv.SERVICE_ID = New1.SERVICE_ID  AND Serv.NROWADD = New1.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carservice,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '02' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New2
ON Serv.SERVICE_ID = New2.SERVICE_ID  AND Serv.NROWADD = New2.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carother,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '03' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New3
ON Serv.SERVICE_ID = New3.SERVICE_ID AND Serv.NROWADD = New3.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS cargov,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '04' AND CAR_GRADE = '1' AND SERVICE_ID = '00001'
) New4
ON Serv.SERVICE_ID = New4.SERVICE_ID AND Serv.NROWADD = New4.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carcustomerO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '01' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old1
ON Serv.SERVICE_ID = Old1.SERVICE_ID AND  Serv.NROWADD = Old1.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carserviceO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '02' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old2
ON Serv.SERVICE_ID = Old2.SERVICE_ID AND  Serv.NROWADD = Old2.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS carotherO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '03' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old3
ON Serv.SERVICE_ID = Old3.SERVICE_ID AND  Serv.NROWADD = Old3.NROWADD
LEFT JOIN
(
    SELECT 
         SERVICE_ID ,SERVICECHARGE AS cargovO,NROWADD
    FROM TBL_SERVICECOST
    WHERE CARCATE_ID = '04' AND CAR_GRADE = '0' AND SERVICE_ID = '00001'
) Old4
ON Serv.SERVICE_ID = Old4.SERVICE_ID AND  Serv.NROWADD = Old4.NROWADD

WHERE Serv.ISACTIVE_FLAG = 'Y' AND Serv.SERVICE_ID = '00001'
GROUP BY      Serv.SERVICE_ID, Serv.REQTYPE_ID,Serv.START_CAPACITY, Serv.END_CAPACITY,Serv. UNIT,  Serv.CAUSE_ISALL, Serv.ISACTIVE_FLAG
,New1.carcustomer,New2.carservice,New3.carother ,New4.cargov,Old1.carcustomerO,Old2.carserviceO ,Old3.carotherO,Old4.cargovO,Serv.NROWADD--,ROWNUM
ORDER BY NROWADD ASC
)sdata";
        lstAddnewrowgvw.Clear();
        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;
            gvw.DataBind();

            txtgvwrowcount.Text = gvw.VisibleRowCount + "";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                lstAddnewrowgvw.Add(new TAddrowgvw
                {
                    START_CAPACITY = dt.Rows[i]["START_CAPACITY"] + "",
                    END_CAPACITY = dt.Rows[i]["END_CAPACITY"] + "",
                    carcustomer = dt.Rows[i]["carcustomer"] + "",
                    carservice = dt.Rows[i]["carservice"] + "",
                    cargov = dt.Rows[i]["cargov"] + "",
                    carother = dt.Rows[i]["carother"] + "",
                    carcustomerO = dt.Rows[i]["carcustomerO"] + "",
                    carserviceO = dt.Rows[i]["carserviceO"] + "",
                    carotherO = dt.Rows[i]["carotherO"] + "",
                    cargovO = dt.Rows[i]["cargovO"] + "",
                    NO = int.Parse(dt.Rows[i]["NO"] + ""),
                    NROWADD = int.Parse(dt.Rows[i]["NROWADD"] + "")
                });
            }

            gvw.JSProperties["cpGvwCount"] = gvw.VisibleRowCount;
        }
        else
        {
            if (lstAddnewrowgvw.Count > 0)
            {
                gvw.DataSource = lstAddnewrowgvw;
                gvw.DataBind();
                txtgvwrowcount.Text = lstAddnewrowgvw.Count + "";
            }

            gvw.JSProperties["cpGvwCount"] = lstAddnewrowgvw.Count + "";
        }

        #endregion
    }

    private void ListServiceAdd()
    {
        #region งานบริการแก้ไขเพิ่มเติม
        string Query3 = @"SELECT  ROWNUM AS NROWADD,MS.SERVICE_ID, MS.SERVICE_NAME,scost.SERVICECHARGE,scost.DESCRIPTION,MS.CHECKFROM FROM 
(
    SELECT SERVICE_ID, SERVICE_NAME,CHECKFROM
    FROM TBL_SERVICE_MASTERDATA WHERE FIX_FLAG ='N' AND ISACTIVE_FLAG ='Y'
)MS
LEFT JOIN 
(
    SELECT SERVICE_ID, SERVICECHARGE,DESCRIPTION FROM TBL_SERVICECOST WHERE  ISACTIVE_FLAG = 'Y'
)scost
ON MS.SERVICE_ID = scost.SERVICE_ID";
        lstAddnewrow.Clear();
        DataTable dt3 = CommonFunction.Get_Data(conn, Query3);
        if (dt3.Rows.Count > 0)
        {
            gvwOhter.DataSource = dt3;
            gvwOhter.DataBind();

            txtgvwohterrowcount.Text = gvwOhter.VisibleRowCount + "";



            for (int i = 0; i < dt3.Rows.Count; i++)
            {
                lstAddnewrow.Add(new TAddrow
                {
                    NROWADD = int.Parse(dt3.Rows[i]["NROWADD"] + ""),
                    SERVICE_ID = dt3.Rows[i]["SERVICE_ID"] + "",
                    DESCRIPTION = dt3.Rows[i]["DESCRIPTION"] + "",
                    SERVICE_NAME = dt3.Rows[i]["SERVICE_NAME"] + "",
                    SERVICECHARGE = dt3.Rows[i]["SERVICECHARGE"] + "",
                    CHECKFROM = dt3.Rows[i]["CHECKFROM"] + "" != "" ? dt3.Rows[i]["CHECKFROM"] + "" : "N"
                });
            }
        }

        #endregion
    }

    private void SetEnableControl(ASPxTextBox txt)
    {
        if (!string.IsNullOrEmpty(txt.Text))
        {
            txt.ClientEnabled = false;
        }
        else
        {
            txt.ClientEnabled = true;
        }

    }

    private void GetValueTOListGvw(ASPxGridView gvw)
    {
        int s = lstAddnewrowgvw.Count;
        if (s > 0)
        {


            for (int i = 0; i < s; i++)
            {
                dynamic drow = gvw.GetRowValues(i, "NO");

                lstAddnewrowgvw.RemoveAll(w => w.NO == drow);

                ASPxTextBox txtStartCapacity = gvw.FindRowCellTemplateControl(i, null, "txtStartCapacity") as ASPxTextBox;
                ASPxTextBox txtEndCapacity = gvw.FindRowCellTemplateControl(i, null, "txtEndCapacity") as ASPxTextBox;
                ASPxTextBox txtcarcustomer = gvw.FindRowCellTemplateControl(i, null, "txtcarcustomer") as ASPxTextBox;
                ASPxTextBox txtcarservice = gvw.FindRowCellTemplateControl(i, null, "txtcarservice") as ASPxTextBox;
                ASPxTextBox txtcarother = gvw.FindRowCellTemplateControl(i, null, "txtcarother") as ASPxTextBox;
                ASPxTextBox txtcargov = gvw.FindRowCellTemplateControl(i, null, "txtcargov") as ASPxTextBox;
                ASPxTextBox txtcarcustomerO = gvw.FindRowCellTemplateControl(i, null, "txtcarcustomerO") as ASPxTextBox;
                ASPxTextBox txtcarserviceO = gvw.FindRowCellTemplateControl(i, null, "txtcarserviceO") as ASPxTextBox;
                ASPxTextBox txtcargovO = gvw.FindRowCellTemplateControl(i, null, "txtcargovO") as ASPxTextBox;
                ASPxTextBox txtcarotherO = gvw.FindRowCellTemplateControl(i, null, "txtcarotherO") as ASPxTextBox;

                int? NROWADD = null;
                string sNROWADD = gvw.GetRowValues(i, "NROWADD") + "";
                if (!string.IsNullOrEmpty(sNROWADD))
                {
                    NROWADD = int.Parse(sNROWADD);
                }

                lstAddnewrowgvw.Add(new TAddrowgvw
                {
                    NO = int.Parse(drow + ""),
                    NROWADD = NROWADD,
                    START_CAPACITY = txtStartCapacity != null ? txtStartCapacity.Text : "",
                    END_CAPACITY = txtEndCapacity != null ? txtEndCapacity.Text : "",
                    carcustomer = txtcarcustomer != null ? txtcarcustomer.Text : "",
                    carservice = txtcarservice != null ? txtcarservice.Text : "",
                    cargov = txtcargov != null ? txtcargov.Text : "",
                    carother = txtcarother != null ? txtcarother.Text : "",
                    carcustomerO = txtcarcustomerO != null ? txtcarcustomerO.Text : "",
                    carserviceO = txtcarserviceO != null ? txtcarserviceO.Text : "",
                    cargovO = txtcargovO != null ? txtcargovO.Text : "",
                    carotherO = txtcarotherO != null ? txtcarotherO.Text : ""
                });
            }
        }
    }

    private void GetValueTOListGvwOther(ASPxGridView gvw)
    {
        int s = lstAddnewrow.Count;
        if (s > 0)
        {
            for (int i = 0; i < s; i++)
            {

                int? NROWADD = null;
                string sNROWADD = gvwOhter.GetRowValues(i, "NROWADD") + "";
                if (!string.IsNullOrEmpty(sNROWADD))
                {
                    NROWADD = int.Parse(sNROWADD);
                }

                lstAddnewrow.RemoveAll(w => w.NROWADD == NROWADD);


                string SERVICE_ID = gvw.GetRowValues(i, "SERVICE_ID") + "";
                ASPxTextBox txtMenu = gvw.FindRowCellTemplateControl(i, null, "txtMenu") as ASPxTextBox;
                ASPxTextBox txtOhterservice = gvw.FindRowCellTemplateControl(i, null, "txtOhterservice") as ASPxTextBox;
                ASPxTextBox txtDescription = gvw.FindRowCellTemplateControl(i, null, "txtDescription") as ASPxTextBox;
                ASPxCheckBox CheckFrom = gvw.FindRowCellTemplateControl(i, null, "CheckFrom") as ASPxCheckBox;

               

                lstAddnewrow.Add(new TAddrow
                {
                    SERVICE_ID = SERVICE_ID,
                    NROWADD = NROWADD ,
                    SERVICE_NAME = txtMenu != null ? txtMenu.Text : "",
                    SERVICECHARGE = txtOhterservice != null ? txtOhterservice.Text : "",
                    DESCRIPTION = txtDescription != null ? txtDescription.Text : "",
                    CHECKFROM = CheckFrom!= null ? (CheckFrom.Checked ? "Y" : "N") : "N"
                });

            }
        }
    }

    private string NullreturnEmpty(string Value)
    {
        string result = "";
        if (Value != "null")
        {
            result = Value;
        }
        return result;

    }

    private void ListMSdata()
    {
        string Query = @"SELECT  MS.SERVICE_ID, MS.SERVICE_NAME, MS.ISACTIVE_FLAG, MS.FIX_FLAG,SC.SERVICECHARGE,MS.UNIT
FROM TBL_SERVICE_MASTERDATA  MS
LEFT JOIN TBL_SERVICECOST SC
ON MS.SERVICE_ID = SC.SERVICE_ID
WHERE MS.SERVICE_ID NOT IN ('00001','00006') AND MS.ISACTIVE_FLAG = 'Y'  AND MS.FIX_FLAG= 'Y'
ORDER BY SERVICE_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            gvwMs.DataSource = dt;
            gvwMs.DataBind();
        }

        txtgvwMscount.Text = gvwMs.VisibleRowCount + "";
    }

    private void AddToBaseMs(string SERVICE_ID, string UID, string ReqTypeID, string Value)
    {
        string sqlchkUpdate2 = @"SELECT SERVICECHARGE FROM  TBL_SERVICECOST
                                      WHERE  SERVICE_ID = '{0}'";

        string sqlchkUpdateData2 = @"SELECT SERVICECHARGE FROM  TBL_SERVICECOST
                                      WHERE  SERVICE_ID = '{0}'
                                      UNION 
                                      SELECT {1}  FROM DUAL";

        string sqlUpdateToBase2 = @"UPDATE TBL_SERVICECOST
                                        SET    
                                               SERVICECHARGE  = {0},
                                               UPDATE_DATE    = sysdate,
                                               UPDATE_BY      = {1}      
                                       WHERE  SERVICE_ID = '{2}'";

        string strsql2 = @"INSERT INTO TBL_SERVICECOST(SERVICECOST_ID, SERVICE_ID, REQTYPE_ID, SERVICECHARGE, UNIT, CAUSE_ISALL, ISACTIVE_FLAG, CREATE_DATE, CREATE_BY) 
                                  VALUES ('{0}','{1}','{2}',{3},'{4}','Y', 'Y',sysdate,'{5}')";


        string sValue = CommonFunction.ReplaceInjection(Value);

        DataTable dtUpdate = new DataTable();
        dtUpdate = CommonFunction.Get_Data(conn, string.Format(sqlchkUpdate2, SERVICE_ID));

        sValue = !string.IsNullOrEmpty(sValue) ? sValue : "null";
        if (dtUpdate.Rows.Count > 0)
        {

            int chkDataToUpdate = CommonFunction.Count_Value(conn, string.Format(sqlchkUpdateData2, SERVICE_ID, sValue));
            if (chkDataToUpdate > 1)
            {
                string Update = string.Format(sqlUpdateToBase2, sValue, UID, SERVICE_ID);
                AddTODB(Update);
            }
        }
        else
        {
            AddTODB(string.Format(strsql2, Gen_ID(), SERVICE_ID, ReqTypeID, sValue, "", UID));
        }

    }

    private int CheckDataOld(string CARCATE_ID, string CAR_GRADE, string NROWADD)
    {
        int Result = 0;
        string sqlChkDataInsert = @"SELECT START_CAPACITY ,END_CAPACITY,SERVICECHARGE FROM  TBL_SERVICECOST
                                                    WHERE CARCATE_ID = '{0}' AND CAR_GRADE = '{1}' AND NROWADD = {2} AND SERVICE_ID = '00001'";

        int chkcarcustomer = CommonFunction.Count_Value(conn, string.Format(sqlChkDataInsert, CARCATE_ID, CAR_GRADE, NROWADD));

        Result = chkcarcustomer;

        return Result;
    }

    #region class
    [Serializable]

    class TAddrow
    {
        public string SERVICE_ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public string SERVICECHARGE { get; set; }
        public string DESCRIPTION { get; set; }
        public string CHECKFROM { get; set; }
        public int? NROWADD { get; set; }
    }

    class TAddrowgvw
    {
        public int NO { get; set; }
        public int? NROWADD { get; set; }
        public string START_CAPACITY { get; set; }
        public string END_CAPACITY { get; set; }
        public string CARCATE_ID { get; set; }
        public string carcustomer { get; set; }
        public string carservice { get; set; }
        public string cargov { get; set; }
        public string carother { get; set; }
        public string carcustomerO { get; set; }
        public string carserviceO { get; set; }
        public string cargovO { get; set; }
        public string carotherO { get; set; }
    }
    #endregion
}