﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.OracleClient;
using System.Configuration;

public partial class vendor_attachfile_mv : System.Web.UI.Page
{
    private static string sReq_ID = "";
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private const string sPathSave = "UploadFile/Request/{0}/File/{1}/{2}/";
    private const string sPathTemp = "UploadFile/Request/{0}/Temp/{1}/{2}/";
    private static int MaxSizeFileUpload = int.Parse(Resources.CommonResource.UploadMaxFileSize2MB);
    private static string sUserID = "";
    private static string sStatusID = "";
    private static string sREQTYPE_ID = "";
    private static string sDate = "";
    private static string _SysFileName = "", _sFileName = "", _sPathNow = "";
    private static List<DOCUMENT> lstDocument = new List<DOCUMENT>();
    private static List<DOCUMENT> lstDocumentOther = new List<DOCUMENT>();
    private static string sNextStatus_ID = "03"; // สถานะของ Step ต่อไป
    private static string sNextMVStatus_ID = "04"; // สถานะของ Step ต่อไป
    private static string sDocOther = "0002"; //sDocBill = "0001",
    protected void Page_Load(object sender, EventArgs e)
    {
        // gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);

        if (!IsPostBack)
        {
            sUserID = Session["UserID"] + "";
            sDate = DateTime.Now.ToShortDateString();
            string strRQ_ID = Request.QueryString["strReqID"]; // คำขอ
            string stStatus = Request.QueryString["statusID"]; // สถานะ

            if (!string.IsNullOrEmpty(strRQ_ID) && strRQ_ID != null)
            {
                txtDatenow.Text = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrRQID = STCrypt.DecryptURL(strRQ_ID);
                sReq_ID = arrRQID[0];

            }
            if (!string.IsNullOrEmpty(stStatus) && stStatus != null)
            {
                string[] _strStatus = STCrypt.DecryptURL(stStatus);
                sStatusID = _strStatus[0];

            }

            lstDocument.Clear();
            lstDocumentOther.Clear();

            ListData();
            ListCapacity();
            ListDoc(sReq_ID, "Y");
            ListDocOther(sReq_ID, "Y");
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            btnShowCalendar.ClientSideEvents.Click = "function(s,e){if($('tr[id$=trVisible]').attr('class') != 'displayTR' ) { $('tr[id$=trVisible]').attr('class', 'displayTR');reloadCal(" + month + "," + year + ");}else{$('tr[id$=trVisible]').attr('class', 'displayNone');}}";
            Session["sCalendar"] = "" + sReq_ID + ";SR;" + (!string.IsNullOrEmpty(lblCapacityTotal.Text) ? lblCapacityTotal.Text.Replace(",", "") : "") + ";" + (sREQTYPE_ID == "01" ? "0" : (sREQTYPE_ID == "03" ? "3" : "1")) + ""; // ถ้าตีซีลหรือพ่นสาระ = 1 //ถ้าขอเอกสาร = 3
            SetBtnSkin(sReq_ID, btnShowCalendar);
            txtUrlDownloadBill.Text = "download_invoice_request.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sReq_ID)) + "";
        }

    }

    //เปลี่ยนสี Text ความจุที่มี * ให้เป็นสีแดง
    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //if (e.DataColumn.Caption == "ช่อง 1")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT1") as ASPxLabel;
        //    string Text = xlbSLOT.Text + "";
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 2")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT2") as ASPxLabel;
        //    string Text = xlbSLOT.Text + "";
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 3")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT3") as ASPxLabel;
        //    string Text = xlbSLOT.Text + "";
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 4")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT4") as ASPxLabel;
        //    string Text = xlbSLOT.Text + "";
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 5")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT5") as ASPxLabel;
        //    string Text = xlbSLOT.Text + "";
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 6")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT6") as ASPxLabel;
        //    string Text = xlbSLOT.Text;
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 7")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT7") as ASPxLabel;
        //    string Text = xlbSLOT.Text;
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 8")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT8") as ASPxLabel;
        //    string Text = xlbSLOT.Text;
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 9")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT9") as ASPxLabel;
        //    string Text = xlbSLOT.Text;
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
        //if (e.DataColumn.Caption == "ช่อง 10")
        //{
        //    ASPxLabel xlbSLOT = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "xlbSLOT10") as ASPxLabel;
        //    string Text = xlbSLOT.Text;
        //    if (Text.Contains("*"))
        //    {
        //        xlbSLOT.ForeColor = System.Drawing.Color.Red;
        //    }
        //}
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListDescriptionData(sReq_ID, Session["UserID"] + "");
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        switch (paras[0])
        {
            case "SAVE":
                if (CheckAttachFile())
                {
                    if (!string.IsNullOrEmpty(sReq_ID) && !string.IsNullOrEmpty(sDate))
                    {
                        // ตรวจสอบวันที่จอง
                        if (CheckCalendarBooking())
                        {
                            string[] ArrDate = sDate.Split('/');
                            string sTempDate = (int.Parse(ArrDate[2]) - 543) + "/" + ArrDate[1] + "/" + ArrDate[0];// + " 00:00:00";

                            // ตารางคำขอ
                            string sql = @"UPDATE TBL_REQUEST SET APPOINTMENT_DATE = TO_DATE('{1}', 'YYYY-MM-DD'),STATUS_FLAG = :STATUS_FLAG,UPDATE_CODE = :UPDATE_CODE,UPDATE_DATE = SYSDATE,REQUEST_DATE =  SYSDATE,CONTACTNAME =:CONTACTNAME,CONTACTPHONE =:CONTACTPHONE WHERE REQUEST_ID = '{0}'";
                            using (OracleConnection con = new OracleConnection(conn))
                            {
                                con.Open();
                                using (OracleCommand com = new OracleCommand(string.Format(sql, CommonFunction.ReplaceInjection(sReq_ID), sTempDate), con))
                                {
                                    com.Parameters.Clear();
                                    //กรณีที่เป้น 06 รอส่งเอกสารปิดงาน จะต้องส่งสถานะเดิมกลับไป ในกรณีที่เป็น 07 คือ รถไม่ได้มาตราฐาน จะส่งไปยัง มว เลย
                                    com.Parameters.Add(":STATUS_FLAG", OracleType.VarChar).Value = (sStatusID == "06" ? sStatusID : (sStatusID == "07" ? sNextMVStatus_ID : sNextStatus_ID));
                                    com.Parameters.Add(":UPDATE_CODE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com.Parameters.Add(":CONTACTNAME", OracleType.VarChar).Value = txtName.Text;
                                    com.Parameters.Add(":CONTACTPHONE", OracleType.VarChar).Value = txtPhone.Text;
                                    com.ExecuteNonQuery();
                                }
                            }

                            // ตารางเอกสาร
                            Update_TBL_REQDOC(sReq_ID);

                            SystemFunction.Add_To_TBL_REQREMARK(sReq_ID, "Y", SystemFunction.GetRemark_WorkFlowRequest(2), lblStatusID.Text, "S", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(2), "", "N");
                            //ส่งเมล์
                            if (SendMailToUser(sReq_ID))
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='approve_mv.aspx';});");
                            }
                            else // Error send mail
                            {
                                //CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','บันทึกข้อมูลเรียบร้อยแล้ว <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้');");
                                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + " <br/> แต่ไม่สามารถส่ง E-mail ได้ในขณะนี้','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='approve_mv.aspx';});");
                            }



                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ได้มีการจองวันที่ " + sDate + " ครบจำนวนแล้ว');");
                        }
                    }
                }
                else
                {

                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาแนบเอกสารให้ครบ');");
                }


                break;

            case "BACK": xcpn.JSProperties["cpRedirectTo"] = "approve_mv.aspx";
                break;
        }
    }

    //กำหนดชื่อปุ่มปฎิทิน
    void SetBtnSkin(string sReq_ID, ASPxButton btn)
    {

        DataTable dt = CommonFunction.Get_Data(conn, "SELECT REQUEST_ID, REQTYPE_ID,STATUS_FLAG FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReq_ID) + "'");

        if (dt.Rows.Count > 0)
        {

            if (dt.Rows[0]["STATUS_FLAG"] + "" == "09")
            {
                btn.Text = "เลือกวันนัดหมาย";
            }
            else
            {
                btn.Text = "แก้ไขวันนัดหมาย";

            }
        }
        else
        {
            btn.Text = "เลือกวันนัดหมาย";
            //btn.SkinID = "_appointdate";
        }

    }

    //แสดงข้อมูลพื้นฐาน
    private void ListData()
    {
        if (!string.IsNullOrEmpty(sReq_ID))
        {
            string sql = @"SELECT TRT.REQTYPE_ID,TRT.REQTYPE_NAME,TCS.CAUSE_ID,TCS.CAUSE_NAME,
CASE WHEN NVL(TRQ.TU_NO,'xxx') <> 'xxx' THEN TRQ.VEH_NO || '/' || TRQ.TU_NO ELSE TRQ.VEH_NO END AS SREGISTRATION,
TCC.CARCATE_NAME,TVD.SABBREVIATION,TSR.STATUSREQ_ID,TSR.STATUSREQ_NAME,TRQ.APPOINTMENT_DATE,TRQ.TOTLE_CAP,TRQ.TOTLE_SLOT,TRQ.CONTACTNAME,TRQ.CONTACTPHONE,dw.DWATEREXPIRE
 FROM TBL_REQUEST TRQ LEFT JOIN  TBL_REQTYPE TRT ON TRQ.REQTYPE_ID = TRT.REQTYPE_ID AND TRT.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CAUSE TCS ON TRQ.CAUSE_ID = TCS.CAUSE_ID AND TCS.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TBL_CARCATE TCC ON TRQ.CARCATE_ID = TCC.CARCATE_ID AND TCC.ISACTIVE_FLAG = 'Y'
 LEFT JOIN TVENDOR TVD ON TRQ.VENDOR_ID = TVD.SVENDORID 
 LEFT JOIN TBL_STATUSREQ TSR ON TRQ.STATUS_FLAG = TSR.STATUSREQ_ID AND TSR.ISACTIVE_FLAG = 'Y'
 LEFT JOIN
 (
    SELECT h.STRUCKID,h.SHEADID,h.STRAILERID ,CASE WHEN h.SCARTYPEID = '3' THEN t.DWATEREXPIRE ELSE h.DWATEREXPIRE END as DWATEREXPIRE   FROM 
    (
        SELECT STRUCKID,SHEADID,STRAILERID ,DWATEREXPIRE,SCARTYPEID FROM TTRUCK WHERE SCARTYPEID in ('0','3')
    ) h
    LEFT JOIN 
    (
        SELECT STRUCKID ,DWATEREXPIRE FROM TTRUCK WHERE  SCARTYPEID = '4'
    )t
    ON h.STRAILERID = t.STRUCKID
 )dw
 ON TRQ.VEH_ID = dw.STRUCKID
 WHERE TRQ.REQUEST_ID = '{0}'";

            DataTable dt = new DataTable();
            DataRow dr = null;
            dt = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sReq_ID)));
            if (dt.Rows.Count > 0)
            {
                dr = dt.Rows[0];

                lblStatusRQ.Text = dr["STATUSREQ_NAME"] + "";
                lblRQType.Text = dr["REQTYPE_NAME"] + "";
                lblCause.Text = dr["CAUSE_NAME"] + "";
                lblRegistration.Text = dr["SREGISTRATION"] + "";
                lblCarType.Text = dr["CARCATE_NAME"] + "";
                lblCompany.Text = dr["SABBREVIATION"] + "";
                //CAUSE_ID_TeeSel = dr["CAUSE_ID"] + "";
                lblCapacityTotal.Text = !string.IsNullOrEmpty(dr["TOTLE_CAP"] + "") ? int.Parse(dr["TOTLE_CAP"] + "").ToString("#,###,###,###") : "-";
                lblCompartTotal.Text = dr["TOTLE_SLOT"] + "";
                lblStatusID.Text = dr["STATUSREQ_ID"] + "";
                txtName.Text = dr["CONTACTNAME"] + "";
                txtPhone.Text = dr["CONTACTPHONE"] + "";
                lblwaterexp.Text = !string.IsNullOrEmpty(dt.Rows[0]["DWATEREXPIRE"] + "") ? DateTime.Parse(dt.Rows[0]["DWATEREXPIRE"] + "").ToString("dd/MM/yyyy") : " - ";
                if (dr["APPOINTMENT_DATE"] + "" != "")
                {
                    string sDate = dr["APPOINTMENT_DATE"] + "";
                    DateTime dTemp = Convert.ToDateTime(sDate, new CultureInfo("th-TH"));
                    string dTH = dTemp.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                    txtCalendarDate.Text = dTH;
                    lblCalendarDate.Text = dTemp.ToString("dd MMMM yyyy", new CultureInfo("th-TH"));
                    txtCheckDate.Text = dTH;
                }

                sREQTYPE_ID = dr["REQTYPE_ID"] + "";
            }
        }
    }

    //แสดงประวัติที่เคยทำ
    private void ListDescriptionData(string Req_ID, string USERID)
    {


        DataTable dt = SystemFunction.List_TBL_REQREMARK(Req_ID, USERID);
        if (dt.Rows.Count > 0)
        {
            gvwhistory.DataSource = dt;
            gvwhistory.DataBind();
        }
        else
        {

        }

    }

    //แสดงความจุ
    private void ListCapacity()
    {
        DataTable _dtCompacity = new DataTable();

        _dtCompacity = SystemFunction.LISTCAPACITY(sReq_ID);
        if (_dtCompacity.Rows.Count > 0)
        {
            gvw.DataSource = _dtCompacity;
            gvw.DataBind();

            #region เช็คว่ามีแป้นเท่าไหร่
            string _chkPan3 = @"SELECT MAX(R.LEVEL_NO)
FROM TBL_REQSLOT r  WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sReq_ID) + @"' AND NVL(STATUS_PAN3,'xxx') <> '0'";
            DataTable _dtChkPan3 = new DataTable();

            _dtChkPan3 = CommonFunction.Get_Data(conn, _chkPan3);
            if (_dtChkPan3.Rows.Count > 0)
            {
                string _nPan = _dtChkPan3.Rows[0][0].ToString();
                if (_nPan != "3")
                {
                    //ถ้าไม่มีแป้นสามให้แสดง 2 แป้น
                    gvw.SettingsPager.PageSize = 2;
                }
            }
            #endregion

        }
    }

    #region Upload

    //สร้างโฟลเดอร์เพื่อไว้เก็บไฟล์
    private void CreateFolder(string Path)
    {
        try
        {

            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + Path.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + Path.Replace("/", "\\"));
            }
            #endregion
        }
        catch (Exception e)
        {
            // Console.WriteLine("The process failed: {0}", e.ToString());
        }
    }

    protected bool CheckTypeFileUpLoad(UploadedFile ful)
    {
        string[] nameFile = ful.FileName.Split('.');
        if (nameFile[nameFile.Length - 1].ToLower() == "pdf" || nameFile[nameFile.Length - 1].ToLower() == "jpg" || nameFile[nameFile.Length - 1].ToLower() == "jpeg" || nameFile[nameFile.Length - 1].ToLower() == "bmp" || nameFile[nameFile.Length - 1].ToLower() == "gif" || nameFile[nameFile.Length - 1].ToLower() == "png" || nameFile[nameFile.Length - 1].ToLower() == "doc" || nameFile[nameFile.Length - 1].ToLower() == "docx" || nameFile[nameFile.Length - 1].ToLower() == "xls" || nameFile[nameFile.Length - 1].ToLower() == "xlsx")
        {
            return true;
        }
        else
        {
            //SetBodyEventOnLoad("jAlertBox('" + SystemFunction.Msg_HeadAlert() + "','" + (string.Format(Resources.CommonResource.Msg_UploadFileOnly, "PDF")) + "')");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", string.Format("ShowZErrorDialog('{1}','{0}');", Resources.CommonResource.Msg_Alert_Title_Error, (string.Format(Resources.CommonResource.Msg_UploadFileOnly, "PDF"))), true);
            return false;
        }
    }

    protected void uclBill_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        string Name = "";
        switch (sREQTYPE_ID)
        {
            case "01":
                Name = "Measure";
                break;
            case "02":
                Name = "AddSeal";
                break;
            case "03":
                Name = "Paint";
                break;
            case "04":
                Name = "Doc";
                break;
        }

        string Path = string.Format(sPathTemp, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), sUserID);
        CreateFolder(Path);
        string[] FILETYPE = e.UploadedFile.FileName.Split('.');

        //ฟังชั่นเช็คประเภทรูปและขนาดในการอับโหลด
        string AlertText = SystemFunction.DOC_CheckSize(FILETYPE[FILETYPE.Length - 1], e.UploadedFile.FileBytes.Length);
        if (CheckTypeFileUpLoad(e.UploadedFile))
        {
            if (string.IsNullOrEmpty(AlertText))
            {
                // สร้างชื่อไฟล์
                string FILENAME = SystemFunction.SplitFileName(e.UploadedFile.FileName);
                //string[] FILETYPE = e.UploadedFile.FileName.Split('.');
                string SYSFILENAME = "REQ_" + @"_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + "." + FILETYPE[FILETYPE.Length - 1];

                //เก็บเอกสารลง Temp
                e.UploadedFile.SaveAs(Server.MapPath(Path + "/" + SYSFILENAME));
                if (File.Exists(Server.MapPath(Path + "/" + SYSFILENAME)))
                {
                    _SysFileName = SYSFILENAME;
                    _sFileName = FILENAME;
                    _sPathNow = Path;

                }
                else
                {

                }
                e.CallbackData = "";
            }
            else
            {
                e.CallbackData = AlertText;
            }
        }
        else
        {
            e.CallbackData = "เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น";
        }
    }

    protected void gvwBill_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] paras = e.Args[0].Split(';');
                int index = int.Parse(paras[1] + "");
                string DOCTYPE_ID = gvwBill.GetRowValues(index, "DOCTYPE_ID") + "";
                switch (paras[0])
                {
                    case "UPLOAD":

                        var doc = lstDocument.Where(d => d.DOCTYPE_ID == DOCTYPE_ID).FirstOrDefault();
                        if (doc != null)
                        {

                            doc.FILE_NAME = _sFileName;
                            doc.FILE_PATH = _sPathNow;
                            doc.FILE_SYSNAME = _SysFileName;
                            doc.SVISIBLE = !string.IsNullOrEmpty(_sFileName) ? "Y" : "N";
                            doc.CONSIDER = "";
                            doc.OPENFILE = _sPathNow + _SysFileName;
                            doc.REQUEST_ID = sReq_ID;
                            doc.DOC_ID = "1";
                        }


                        ListDoc("", "N");


                        break;

                    case "DEL":

                        var del = lstDocument.Where(d => d.DOCTYPE_ID == DOCTYPE_ID).FirstOrDefault();
                        if (del != null)
                        {
                            del.FILE_NAME = "";
                            del.FILE_PATH = "";
                            del.FILE_SYSNAME = "";
                            del.REQUEST_ID = sReq_ID;
                            del.SVISIBLE = "N";
                        }
                        ListDoc("", "N");


                        break;
                }
                break;
        }
    }

    private void ListDoc(string REQUEST_ID, string Getdatasql)
    {

        string Codition = "";

        switch (sREQTYPE_ID)
        {
            case "01": Codition = " AND MS.DOC_01 = 'Y'";
                break;
            case "02": Codition = " AND MS.DOC_02 = 'Y'";
                break;
            case "03": Codition = " AND MS.DOC_03 = 'Y'";
                break;
            case "04": Codition = " AND MS.DOC_04 = 'Y'";
                break;
        }

        Codition += SystemFunction.DOC_CLOSEWORK(sStatusID, REQUEST_ID);

        string Query = @"SELECT  MS.DOCTYPE_ID, MS.DOC_DESCRIPTION,MS.DOC_01,  MS.DOC_02, MS.DOC_03, MS.DOC_04, MS.ISACTIVE_FLAG, MS.DESCRIPTION,MS.CDYNAMIC
,MS.CDYNAMIC,  MS.CATTACH_DOC01, MS.CATTACH_DOC02, MS.CATTACH_DOC03, MS.CATTACH_DOC04
, DOC.FILE_NAME, DOC.FILE_SYSNAME, DOC.FILE_PATH, DOC.CONSIDER,CASE  WHEN REQ.STATUS_FLAG IN ('09','02') THEN 'Y' ELSE 'N' END as SALLOWEDIT,
CASE WHEN NVL(DOC.FILE_NAME,'xxx') <> 'xxx' THEN 'Y' ELSE 'N' END as SVISIBLE ,DOC.DOC_ID,DOC.DOC_ITEM
FROM TBL_DOCTYPE MS
LEFT JOIN 
(
    SELECT REQUEST_ID, DOC_ID, DOC_TYPE,  DOC_ITEM, FILE_NAME, FILE_SYSNAME, FILE_PATH, CONSIDER
    FROM TBL_REQDOC 
    WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQUEST_ID) + @"'
)DOC
ON MS.DOCTYPE_ID = DOC.DOC_TYPE
LEFT JOIN TBL_REQUEST REQ
ON DOC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 AND MS.CDYNAMIC = 'Y' AND MS.ISACTIVE_FLAG = 'Y' " + Codition + @"
ORDER BY DOCTYPE_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            if (Getdatasql == "Y")
            {
                lstDocument.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    lstDocument.Add(new DOCUMENT
                    {
                        DOCTYPE_ID = dr["DOCTYPE_ID"] + "",
                        DOC_ID = dr["DOC_ID"] + "",
                        DOC_DESCRIPTION = dr["DOC_DESCRIPTION"] + "",
                        DOC_ITEM = dr["DOC_ITEM"] + "",
                        CONSIDER = dr["CONSIDER"] + "",
                        CDYNAMIC = dr["CDYNAMIC"] + "",
                        SVISIBLE = !string.IsNullOrEmpty(dr["SVISIBLE"] + "") ? dr["SVISIBLE"] + "" : "N",
                        FILE_NAME = dr["FILE_NAME"] + "",
                        FILE_PATH = dr["FILE_PATH"] + "",
                        FILE_SYSNAME = dr["FILE_SYSNAME"] + "",
                        OPENFILE = dr["FILE_PATH"] + "" + dr["FILE_SYSNAME"] + "",
                        CATTACH_DOC01 = dr["DOC_01"] + "",
                        CATTACH_DOC02 = dr["DOC_02"] + "",
                        CATTACH_DOC03 = dr["DOC_03"] + "",
                        CATTACH_DOC04 = dr["DOC_04"] + "",
                    });
                }
            }

            gvwBill.DataSource = lstDocument;
            gvwBill.DataBind();
        }
        else
        {
            gvwBill.DataSource = lstDocument;
            gvwBill.DataBind();
        }
    }

    protected void ulcOtherDoc_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string Name = "";
        switch (sREQTYPE_ID)
        {
            case "01":
                Name = "Measure_Other";
                break;
            case "02":
                Name = "AddSeal_Other";
                break;
            case "03":
                Name = "Paint_Other";
                break;
            case "04":
                Name = "Doc_Other";
                break;
        }

        string Path = string.Format(sPathTemp, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), sUserID);
        CreateFolder(Path);

        string[] FILETYPE = e.UploadedFile.FileName.Split('.');

        //ฟังชั่นเช็คประเภทรูปและขนาดในการอับโหลด
        string AlertText = SystemFunction.DOC_CheckSize(FILETYPE[FILETYPE.Length - 1], e.UploadedFile.FileBytes.Length);
        if (CheckTypeFileUpLoad(e.UploadedFile))
        {
            if (string.IsNullOrEmpty(AlertText))
            {
                // สร้างชื่อไฟล์
                string FILENAME = SystemFunction.SplitFileName(e.UploadedFile.FileName);
                string SYSFILENAME = "REQ_OHTER_" + @"_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + "." + FILETYPE[FILETYPE.Length - 1];

                //เก็บเอกสารลง Temp
                e.UploadedFile.SaveAs(Server.MapPath(Path + "/" + SYSFILENAME));
                if (File.Exists(Server.MapPath(Path + "/" + SYSFILENAME)))
                {
                    _SysFileName = SYSFILENAME;
                    _sFileName = FILENAME;
                    _sPathNow = Path;
                }
                else
                {

                }
                e.CallbackData = "";
            }
            else
            {
                e.CallbackData = AlertText;
            }
        }
        else
        {
            e.CallbackData = "เฉพาะไฟล์ .pdf, .jpg, .jpeg, .bmp, .gif, .png, .doc, .docx, .xls, .xlsx เท่านั้น";
        }
    }

    protected void xcpnGvwotherDoc_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        //string Req_ID = txtReqID.Text;
        int inx = !string.IsNullOrEmpty(paras[1] + "") ? int.Parse(paras[1] + "") : 0;
        switch (paras[0])
        {
            case "UPLOAD":

                lstDocumentOther.Add(new DOCUMENT
                {
                    DOCTYPE_ID = "0002",
                    FILE_NAME = _sFileName,
                    FILE_PATH = _sPathNow,
                    FILE_SYSNAME = _SysFileName,
                    SVISIBLE = !string.IsNullOrEmpty(_sFileName) ? "Y" : "N",
                    DOC_ID = lstDocumentOther.Count > 0 ? (int.Parse(lstDocumentOther.OrderByDescending(s => s.DOC_ID).FirstOrDefault().DOC_ID) + 1) + "" : "1",
                    CONSIDER = "",
                    OPENFILE = _sPathNow + _SysFileName,
                    REQUEST_ID = sReq_ID
                });

                ListDocOther("", "N");

                break;

            case "DEL":
                string DOCTYPE_ID = gvwOtherDoc.GetRowValues(inx, "DOCTYPE_ID") + "";
                string DOC_ID = gvwOtherDoc.GetRowValues(inx, "DOC_ID") + "";

                lstDocumentOther.RemoveAll(w => w.DOC_ID == DOC_ID);
                ListDocOther("", "N");
                // DelFileUpLoad(txtReqID.Text, DOCTYPE_ID, DOC_ID);

                break;
        }

    }

    private void ListDocOther(string REQUEST_ID, string Getdatasql)
    {

        string Query = @"SELECT  MS.DOCTYPE_ID, MS.DOC_DESCRIPTION,MS.DOC_01,  MS.DOC_02, MS.DOC_03, MS.DOC_04, MS.ISACTIVE_FLAG, MS.DESCRIPTION
,MS.CDYNAMIC,  MS.CATTACH_DOC01, MS.CATTACH_DOC02, MS.CATTACH_DOC03, MS.CATTACH_DOC04
, DOC.FILE_NAME, DOC.FILE_SYSNAME, DOC.FILE_PATH, DOC.CONSIDER,CASE  WHEN REQ.STATUS_FLAG IN ('09','02') THEN 'Y' ELSE 'N' END as SALLOWEDIT,
CASE WHEN NVL(DOC.FILE_SYSNAME,'xxx') <> 'xxx' THEN 'Y' ELSE 'N' END as SVISIBLE ,DOC.DOC_ID,DOC.DOC_ITEM
FROM TBL_DOCTYPE MS
Inner JOIN 
(
    SELECT REQUEST_ID, DOC_ID, DOC_TYPE,  DOC_ITEM, FILE_NAME, FILE_SYSNAME, FILE_PATH, CONSIDER
    FROM TBL_REQDOC 
    WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(REQUEST_ID) + @"'
)DOC
ON MS.DOCTYPE_ID = DOC.DOC_TYPE
LEFT JOIN TBL_REQUEST REQ
ON DOC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 AND MS.DOCTYPE_ID = '0002' AND MS.ISACTIVE_FLAG = 'Y'  
ORDER BY DOCTYPE_ID ASC";

        DataTable dt = CommonFunction.Get_Data(conn, Query);
        if (dt.Rows.Count > 0)
        {
            if (Getdatasql == "Y")
            {
                lstDocumentOther.Clear();
                foreach (DataRow dr in dt.Rows)
                {
                    lstDocumentOther.Add(new DOCUMENT
                    {
                        DOCTYPE_ID = dr["DOCTYPE_ID"] + "",
                        DOC_ID = dr["DOC_ID"] + "",
                        DOC_DESCRIPTION = dr["DOC_DESCRIPTION"] + "",
                        DOC_ITEM = dr["DOC_ITEM"] + "",
                        CONSIDER = dr["CONSIDER"] + "",
                        SVISIBLE = dr["SVISIBLE"] + "",
                        FILE_NAME = dr["FILE_NAME"] + "",
                        FILE_PATH = dr["FILE_PATH"] + "",
                        FILE_SYSNAME = dr["FILE_SYSNAME"] + "",
                        OPENFILE = dr["FILE_PATH"] + "" + dr["FILE_SYSNAME"] + ""
                    });
                }
            }




        }
        else
        {
            gvwOtherDoc.Visible = false;
        }


        if (lstDocumentOther.Where(w => w.FILE_PATH != null && w.FILE_NAME != null).Count() > 0)
        {

            gvwOtherDoc.Visible = true;
        }
        else
        {
            gvwOtherDoc.Visible = false;
        }

        gvwOtherDoc.DataSource = lstDocumentOther.OrderBy(s => s.DOC_ID);
        gvwOtherDoc.DataBind();

    }

    #endregion

    //เช็คว่าแนบเอกสารที่ต้องแนบครบไหม
    private bool CheckAttachFile()
    {
        bool sResult = false;

        var query = lstDocument.Where(w => w.CDYNAMIC == "Y" && (sREQTYPE_ID == "01" ? w.CATTACH_DOC01 == "Y" : sREQTYPE_ID == "02" ? w.CATTACH_DOC02 == "Y" : sREQTYPE_ID == "03" ? w.CATTACH_DOC03 == "Y" : sREQTYPE_ID == "04" ? w.CATTACH_DOC04 == "Y" : false)).ToList();
        if (query.Count > 0)
        {
            var queryCheck = from q in query
                             from l in lstDocument.Where(w => w.DOCTYPE_ID == q.DOCTYPE_ID && w.FILE_SYSNAME != "" && w.FILE_PATH != "")
                             select new { q.DOCTYPE_ID };

            sResult = (query.Count == queryCheck.Count());
        }

        return sResult;
    }

    //เช็คก่อนเซฟว่าวันที่ได้เต็มแล้วหรือยัง
    private bool CheckCalendarBooking()
    {
        //booking calendar
        bool sResult = false;
        if (("" + txtCalendarDate.Text) != "" && ("" + Session["sCalendar"]) != "")
        {

            //รหัส;typeหน้า;ความจุ;SEAL_HIT
            //00001;01;170000;1
            string[] Para = (Session["sCalendar"] + "").Split(';');

            DateTime dTemp;
            DateTime dBooking = DateTime.TryParse(("" + txtCalendarDate.Text), out dTemp) ? dTemp : DateTime.Now;

            if (Para.Length >= 4)
            {
                if (Calendar.BookingCalendar(dBooking, Para[0], Para[1], Para[2], Para[3]) == 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','มีรายการจองรถในวันที่ " + dBooking.ToString("d MMMM yyyy") + " เต็มแล้ว! กรุณาเลือกวันที่จองรถใหม่' );");
                    //sResult = true; ;
                }
                else
                {
                    sResult = true;
                }
            }
        }

        return sResult;
    }

    //ส่งเมลล์ให้ รข.
    private bool SendMailToUser(string sREQID)
    {
        string sHTML = "";
        string sMsg = "";

        string _from = "" + ConfigurationSettings.AppSettings["SystemMail"].ToString()
            , _to = "" + ConfigurationSettings.AppSettings["demoMailRecv"].ToString()
            , sSubject = "ส่งเมลล์ให้ Vendor โดย มว.";

        if (ConfigurationSettings.AppSettings["usemail"].ToString() == "1") // ส่งจริง
        {
            _to = "";
        }

        #region html
        sHTML = @"<table width=""100%"" border=""0"" cellpadding=""3"" cellspacing=""1"">
            <tr>
                <td style=""font-size:15px;"">
                      ประเภทคำขอ  : " + lblRQType.Text + @"
                      สาเหตุ : " + lblCause.Text + @"
                </td>
            </tr>
            <tr>
                <td style=""font-size:15px;"">
                     มว. ทำเรื่องการสอบเทียบ
                </td>
            </tr>
          </table>";
        #endregion

        sMsg = sHTML;

        OracleConnection con = new OracleConnection(conn);
        con.Open();
        return CommonFunction.SendNetMail(_from, _to, sSubject, sMsg, con, "", "", "", "", "", "0");
    }

    //TBL_REQDOC ข้อมูลเอกสาร
    private void Update_TBL_REQDOC(string sREQID)
    {

        // Delete
        string sqlDel = @"DELETE  FROM TBL_REQDOC WHERE REQUEST_ID = '{0}' AND DOC_TYPE = '{1}'";

        // insert
        string sqlIns = @"INSERT INTO TBL_REQDOC(REQUEST_ID,DOC_ID,DOC_TYPE,DOC_ITEM,FILE_NAME,FILE_SYSNAME,FILE_PATH,CONSIDER)
                                       VALUES(:REQUEST_ID,:DOC_ID,:DOC_TYPE,:DOC_ITEM,:FILE_NAME,:FILE_SYSNAME,:FILE_PATH,:CONSIDER)";

        DataTable dt = CommonFunction.Get_Data(conn, "SELECT  REQUEST_ID, DOC_TYPE,DOC_ID, FILE_NAME, FILE_SYSNAME, FILE_PATH FROM TBL_REQDOC WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQID) + "'");

        #region ไฟล์ เอกสารทั่วไป
        using (OracleConnection con = new OracleConnection(conn))
        {
            con.Open();

            foreach (var item1 in lstDocument)
            {
                // ตรวจสอบการลบไฟล์ทั่วไป


                //ถ้ามากกว่า 0 แสดงว่ายังเป็นเอกสารเดิม
                if (dt.Select("DOC_TYPE = '" + CommonFunction.ReplaceInjection(item1.DOCTYPE_ID) + "' AND FILE_NAME = '" + CommonFunction.ReplaceInjection(item1.FILE_NAME) + "' AND FILE_SYSNAME = '" + CommonFunction.ReplaceInjection(item1.FILE_SYSNAME) + "' AND FILE_PATH = '" + CommonFunction.ReplaceInjection(item1.FILE_PATH) + "'").Count() > 0)
                {

                }
                else
                {
                    SystemFunction.SQLExecuteNonQuery(conn, string.Format(sqlDel, CommonFunction.ReplaceInjection(sREQID), CommonFunction.ReplaceInjection(item1.DOCTYPE_ID)));

                    // ไฟล์ใหม่
                    string Name = "";
                    switch (sREQTYPE_ID)
                    {
                        case "01":
                            Name = "Measure";
                            break;
                        case "02":
                            Name = "AddSeal";
                            break;
                        case "03":
                            Name = "Paint";
                            break;
                        case "04":
                            Name = "Doc";
                            break;
                    }

                    string Path = string.Format(sPathSave, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), sUserID);

                    string sPath = UpFile2Server_CreateDirectory(item1.FILE_SYSNAME, Path, item1.FILE_PATH);
                    using (OracleCommand com = new OracleCommand(sqlIns, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;
                        com.Parameters.Add(":DOC_ID", OracleType.Number).Value = item1.DOC_ID /*genDocID(sREQID)*/; //รหัสเอกสาร 
                        com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = item1.DOCTYPE_ID; //ประเภทเอกสาร
                        com.Parameters.Add(":DOC_ITEM", OracleType.Number).Value = 1; //เอกสารที่
                        com.Parameters.Add(":FILE_NAME", OracleType.VarChar).Value = item1.FILE_NAME; // ชื่อ ไฟล์ที่อัพโหลด
                        com.Parameters.Add(":FILE_SYSNAME", OracleType.VarChar).Value = item1.FILE_SYSNAME; // ชื่อที่ระบบจัดเก็บ
                        com.Parameters.Add(":FILE_PATH", OracleType.VarChar).Value = Path; //สถานที่เก็บ เอกสาร
                        com.Parameters.Add(":CONSIDER", OracleType.VarChar).Value = "Y";//สถานะการตรวจสอบเอกสาร
                        com.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion

        #region ไฟล์อื่นๆ

        // ตรวจสอบการลบไฟล์อื่นๆ
        foreach (var item2 in lstDocumentOther)
        {

            //var QcheckOtherDoc = lstDocumentOther.Where(w => w.DOCTYPE_ID == sDocOther).ToList();
            if (dt.Select("DOC_TYPE = '" + CommonFunction.ReplaceInjection(sDocOther) + "' AND FILE_NAME = '" + CommonFunction.ReplaceInjection(item2.FILE_NAME) + "' AND FILE_SYSNAME = '" + CommonFunction.ReplaceInjection(item2.FILE_SYSNAME) + "' AND FILE_PATH = '" + CommonFunction.ReplaceInjection(item2.FILE_PATH) + "' AND DOC_ID = " + CommonFunction.ReplaceInjection(item2.DOC_ID) + "").Count() > 0)
            {

            }
            else
            {
                string sqlDelDocOther = sqlDel + " AND DOC_ID = {2}";

                SystemFunction.SQLExecuteNonQuery(conn, string.Format(sqlDelDocOther, CommonFunction.ReplaceInjection(sREQID), CommonFunction.ReplaceInjection(sDocOther), item2.DOC_ID));
                // เพิ่มไฟล์
                if (lstDocumentOther.Count > 0)
                {

                    string Name = "";
                    switch (sREQTYPE_ID)
                    {
                        case "01":
                            Name = "Measure_Other";
                            break;
                        case "02":
                            Name = "AddSeal_Other";
                            break;
                        case "03":
                            Name = "Paint_Other";
                            break;
                        case "04":
                            Name = "Doc_Other";
                            break;
                    }

                    string Path = string.Format(sPathSave, Name, DateTime.Now.ToShortDateString().Replace('/', '-'), sUserID);

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        con.Open();
                        //var QnMax = lstDocumentOther.OrderByDescending(o => o.DOC_ID).FirstOrDefault();
                        //decimal i = QnMax != null ? int.Parse(QnMax.DOC_ID) : 1;
                        //i = i > 0 ? i : 1;

                        string sPath = UpFile2Server_CreateDirectory(item2.FILE_SYSNAME, Path, item2.FILE_PATH);
                        using (OracleCommand com = new OracleCommand(sqlIns, con))
                        {
                            com.Parameters.Clear();
                            com.Parameters.Add(":REQUEST_ID", OracleType.VarChar).Value = sREQID;
                            com.Parameters.Add(":DOC_ID", OracleType.Number).Value = /*item2.DOC_ID*/genDocID(sREQID, sDocOther); //รหัสเอกสาร 
                            com.Parameters.Add(":DOC_TYPE", OracleType.VarChar).Value = sDocOther; //ประเภทเอกสาร
                            com.Parameters.Add(":DOC_ITEM", OracleType.Number).Value = 1; //เอกสารที่
                            com.Parameters.Add(":FILE_NAME", OracleType.VarChar).Value = item2.FILE_NAME; // ชื่อ ไฟล์ที่อัพโหลด
                            com.Parameters.Add(":FILE_SYSNAME", OracleType.VarChar).Value = item2.FILE_SYSNAME; // ชื่อที่ระบบจัดเก็บ
                            com.Parameters.Add(":FILE_PATH", OracleType.VarChar).Value = Path; //สถานที่เก็บ เอกสาร
                            com.Parameters.Add(":CONSIDER", OracleType.VarChar).Value = DBNull.Value;//สถานะการตรวจสอบเอกสาร
                            com.ExecuteNonQuery();
                        }



                    }
                }
            }
        }
        #endregion

        // ลบไฟล์ที่มีการเปลี่ยนแปลงทั้งหมดในแต่ละโฟล์เดอร์
        //DeleteFileFromListDel(lstTempFileDel);
        //deleteFiletemp();
    }

    private string UpFile2Server_CreateDirectory(string sFileName, string _PathSave, string pathFileNow)
    {
        string sResult = "";
        if (File.Exists(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + sFileName))
        {
            CreateFolder(_PathSave); // Create Directory

            sResult = _PathSave;

            try
            {
                Directory.Move(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + sFileName, Server.MapPath("./") + _PathSave.Replace("/", "\\") + sFileName);
            }
            catch
            {
                //File.Copy(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + "\\" + sFileName, Server.MapPath("./") + _PathSave.Replace("/", "\\") + "\\" + sFileName);

                //if (Directory.Exists(Server.MapPath("./") + pathFileNow.Replace("/", "\\")))
                //{
                //    File.Delete(Server.MapPath("./") + pathFileNow.Replace("/", "\\") + "\\" + sFileName);
                //}
            }
        }

        return sResult;
    }

    private decimal genDocID(string sREQID, string DOC_TYPE)
    {
        string sql = @"SELECT * FROM TBL_REQDOC WHERE REQUEST_ID = '{0}' AND DOC_TYPE = '" + DOC_TYPE + "' ORDER BY DOC_ID DESC";
        decimal nTemp = 0;
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt = CommonFunction.Get_Data(conn, string.Format(sql, CommonFunction.ReplaceInjection(sREQID)));
        if (dt.Rows.Count > 0)
        {
            dr = dt.Rows[0];
            nTemp = decimal.TryParse(dr["DOC_ID"] + "", out nTemp) ? nTemp : 1;
            nTemp = nTemp + 1;
        }
        else
        {
            nTemp = 1;
        }

        return nTemp;
    }

    #region Structure
    public class DOCUMENT
    {
        public string DOCTYPE_ID { get; set; }
        public string DOC_DESCRIPTION { get; set; }
        public string REQUEST_ID { get; set; }
        public string DOC_ID { get; set; }
        public string DOC_ITEM { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_SYSNAME { get; set; }
        public string FILE_PATH { get; set; }
        public string CONSIDER { get; set; }
        public string SVISIBLE { get; set; }
        public string CDYNAMIC { get; set; }
        public string OPENFILE { get; set; }
        public string CATTACH_DOC01 { get; set; }
        public string CATTACH_DOC02 { get; set; }
        public string CATTACH_DOC03 { get; set; }
        public string CATTACH_DOC04 { get; set; }
    }
    #endregion
}