﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ContractManagement.aspx.cs" Inherits="ContractManagement" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <table width="100%">
        <tbody>
            <tr>
                <td>
                </td>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="50%" align="right">
                                <dx:ASPxTextBox ID="txtKeyword" runat="server" ClientInstanceName="xtxtKeyword" CssClass="dxeLineBreakFix"
                                    Width="250px" NullText="เลขที่สัญญา" NullTextStyle-HorizontalAlign="Left">
                                    <NullTextStyle HorizontalAlign="Left">
                                    </NullTextStyle>
                                </dx:ASPxTextBox>
                            </td>
                            <td style="width: 5%">
                                <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                                    SkinID="xdte" NullText="ถึงวันที่">
                                    <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                        RequiredField-IsRequired="true" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </td>
                            <td style="width: 5%">
                                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search"
                                    CausesValidation="true" ValidationGroup="search">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxGridView ID="xgvw" ClientInstanceName="xgvw" runat="server" SkinID="_gvw"
                        AutoGenerateColumns="False" KeyFieldName="SKEYID" DataSourceID="sdsContract"
                        OnAfterPerformCallback="xgvw_AfterPerformCallback">
                        <ClientSideEvents RowClick="function (s,e) {xgvw.StartEditRow(e.visibleIndex); } ">
                        </ClientSideEvents>
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="1%">
                                <HeaderTemplate>
                                    <dx:ASPxCheckBox ID="SelectAllCheckBox" ClientInstanceName="SelectAllCheckBox" runat="server"
                                        ToolTip="Select/Unselect all rows on the page" CheckState="Unchecked">
                                        <ClientSideEvents CheckedChanged="function(s, e) { xgvw.SelectAllRowsOnPage(s.GetChecked()); }" />
                                    </dx:ASPxCheckBox>
                                </HeaderTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="ที่." Width="2%">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="รหัส" Width="2%" FieldName="SCONTRACTID">
                                <EditFormSettings VisibleIndex="1" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" Width="20%" FieldName="SCONTRACTNO">
                                <EditFormSettings VisibleIndex="2" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="วันเริ่มต้น" Width="5%" FieldName="DBEGIN">
                                <EditFormSettings VisibleIndex="3" />
                                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                </PropertiesTextEdit>
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="วันสิ้นสุด" Width="5%" FieldName="DEND">
                                <EditFormSettings VisibleIndex="4" />
                                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                </PropertiesTextEdit>
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewBandColumn Caption="จำนวนรถ">
                                <HeaderStyle HorizontalAlign="Center" />
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="รถในสัญญา" Width="2%" FieldName="NTRUCK">
                                        <EditFormSettings VisibleIndex="5" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รถสำรอง" Width="2%" FieldName="">
                                        <EditFormSettings VisibleIndex="6" />
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:GridViewBandColumn>
                            <dx:GridViewDataTextColumn Caption="คลัง" Width="10%" FieldName="STERMINALID">
                                <EditFormSettings VisibleIndex="7" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="สถานะ" Width="2%" FieldName="CACTIVE">
                                <EditFormSettings VisibleIndex="8" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Templates>
                            <EditForm>
                                <dx:ASPxPageControl runat="server" ID="pctInfo" Width="100%">
                                    <TabPages>
                                        <dx:TabPage Text="ข้อมูลทั่วไป" Visible="true">
                                            <ContentCollection>
                                                <dx:ContentControl ID="ContentControl2" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="10%">
                                                                รหัสสัญญา </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtContractId" runat="server">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="10%">
                                                                เลขที่สัญญา </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtContractNo" runat="server">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="10%">
                                                                วันเริ่มต้นสัญญา </td>
                                                            <td>
                                                                <dx:ASPxDateEdit ID="dteBegin" runat="server">
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td style="10%">
                                                                วันสิ้นสุดสัญญา </td>
                                                            <td>
                                                                <dx:ASPxDateEdit ID="dteEnd" runat="server">
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="10%">
                                                                จำนวนรถในสัญญา </td>
                                                            <td>
                                                                <dx:ASPxTextBox ID="txtTruck_In" runat="server">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="10%">
                                                                จำนวนรถสำรอง </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="10%">
                                                                คลังที่วิ่ง </td>
                                                            <td>
                                                                <dx:ASPxCheckBoxList ID="cblTerminal" runat="server">
                                                                </dx:ASPxCheckBoxList>
                                                            </td>
                                                            <td style="10%">
                                                                สถานะ </td>
                                                            <td>
                                                                <dx:ASPxRadioButtonList ID="rdlActive" runat="server" >
                                                                    <Items>
                                                                        <dx:ListEditItem Value="Y" Text="Active" Selected="true" />
                                                                        <dx:ListEditItem Value="N" Text="InActive" />
                                                                    </Items>
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                        <dx:TabPage Text="Notes" Visible="true">
                                            <ContentCollection>
                                                <dx:ContentControl ID="ContentControl1" runat="server">
                                                    <dx:ASPxMemo runat="server" ID="notesEditor" Text='<%# Eval("SCONTRACTNO")%>' Width="100%"
                                                        Height="93px">
                                                    </dx:ASPxMemo>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                    </TabPages>
                                </dx:ASPxPageControl>
                            </EditForm>
                        </Templates>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </tbody>
    </table>
    <asp:SqlDataSource ID="sdsContract" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
        CancelSelectOnNullParameter="False" CacheKeyDependency="ckdContract" SelectCommand="SELECT rownum SKEYID ,SCONTRACTID,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,SDETAIL,NTRUCK,SREMARK,CACTIVE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,COIL,CGAS ,DCREATE,SCREATE,DUPDATE,SUPDATE 
        FROM TCONTRACT CONT WHERE CONT.scontractno LIKE '%'|| NVL(:CONT_SCONTRACTNO,CONT.scontractno) ||'%' AND TO_DATE( :DDATE,'dd/mm/yyyy') BETWEEN TO_DATE(DBEGIN,'dd/mm/yyyy') AND TO_DATE(DEND,'dd/mm/yyyy')  ">
        <SelectParameters>
            <asp:ControlParameter Name="CONT_SCONTRACTNO" ControlID="txtKeyword" PropertyName="Text" />
            <asp:ControlParameter Name="DDATE" ControlID="dteEnd" PropertyName="Value" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
