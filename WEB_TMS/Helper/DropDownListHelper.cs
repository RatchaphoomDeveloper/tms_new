﻿using System;
using System.Data;
using DevExpress.Web.ASPxEditors;

/// <summary>
/// Summary description for DropDownList
/// </summary>
public class DropDownListHelper
{
    public void BindDropDownList(ASPxComboBox ddl, DataTable dtSource, string ValueMember, string DisplayMember)
    {
        try
        {
            ddl.DataSource = dtSource;
            ddl.ValueField = ValueMember;
            ddl.TextField = DisplayMember;
            ddl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}