﻿using System;
using System.Data;
using System.Configuration;
using System.DirectoryServices;
using DevExpress.Web.ASPxEditors;

namespace WEB_TMS.Helper
{
    public class DomainHelper
    {
        public static bool CheckDomain(string UserName, string Password)
        {
            string ADPath = ConfigurationManager.AppSettings["ADPath"];
            string PTTADPath = ConfigurationManager.AppSettings["PTTADPath"];

            DirectoryEntry PTTentry = new DirectoryEntry(PTTADPath, UserName, Password, AuthenticationTypes.Secure);
            try
            {
                DirectorySearcher search = new DirectorySearcher(PTTentry);

                search.Filter = "(SAMAccountName=" + UserName + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                    return true;

            }
            catch (Exception ex)
            {
                if (isPasswordExpired(UserName))
                    throw new Exception("Password Expired");

                throw new Exception(ex.Message);
            }

            DirectoryEntry entry = new DirectoryEntry(ADPath, UserName, Password, AuthenticationTypes.Secure);
            try
            {
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "(SAMAccountName=" + UserName + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();

                if (null == result)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                if (isPasswordExpired(UserName))
                    throw new Exception("Password Expired");

                throw new Exception(ex.Message);
            }
        }

        private static bool isPasswordExpired(string UserName)
        {
            string ADPath = ConfigurationManager.AppSettings["ADPath"];

            DirectoryEntry de = new DirectoryEntry(ADPath);
            DirectorySearcher search = new DirectorySearcher(de);
            search.Filter = "(SAMAccountName=" + UserName + ")";
            search.PropertiesToLoad.Add("maxPwdAge");
            search.PropertiesToLoad.Add("pwdLastSet");
            search.PropertiesToLoad.Add("userAccountControl");
            try
            {
                SearchResult result = search.FindOne();
                Int32 val1 = (Int32)result.Properties["userAccountControl"][0];
                if ((val1 & 65536) == 65536)
                {
                    return false;
                }
                else
                {
                    Int64 val = (Int64)result.Properties["pwdLastSet"][0];
                    DateTime d = DateTime.FromFileTime(val);
                    TimeSpan maxPwdAge = GetMaxPasswordAge();
                    DateTime exp = d.Add(maxPwdAge);

                    return exp < DateTime.Now;
                }
            }
            catch
            {
            }
            return false;
        }

        private static TimeSpan GetMaxPasswordAge()
        {
            using (System.DirectoryServices.ActiveDirectory.Domain d = System.DirectoryServices.ActiveDirectory.Domain.GetCurrentDomain())
            using (DirectoryEntry domain = d.GetDirectoryEntry())
            {
                DirectorySearcher ds = new DirectorySearcher(
                  domain,
                  "(objectClass=*)",
                  null,
                  SearchScope.Base
                  );
                SearchResult sr = ds.FindOne();
                TimeSpan maxPwdAge = TimeSpan.MinValue;
                if (sr.Properties.Contains("maxPwdAge"))
                    maxPwdAge = TimeSpan.FromTicks((long)sr.Properties["maxPwdAge"][0]);
                return maxPwdAge.Duration();
            }
        }
    }
}