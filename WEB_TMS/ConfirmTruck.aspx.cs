﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Configuration;
using System.Globalization;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

public partial class ConfirmTruck : System.Web.UI.Page
{
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.PageIndexChanged += new EventHandler(gvw_PageIndexChanged);
        gvw.AfterPerformCallback += new DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventHandler(gvw_AfterPerformCallback);
        btnSearch.Click += new EventHandler(btnSearch_Click);
        btnExecl.Click += new EventHandler(btnExecl_Click);
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                //Response.Write(str);
                dteSearch.Date = Convert.ToDateTime(strQuery[0] + "");
                cbxTerminal.Value = strQuery[1] + "";
            }
            else { dteSearch.Date = DateTime.Today; }

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            BindData();
        }
    }
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
        if (e.Column.Caption == "วันที่ยืนยัน") e.DisplayText = Convert.ToDateTime(e.Value.ToString()).ToString("dd/MM/yyyy");
    }
    void btnExecl_Click(object sender, EventArgs e)
    {
        BindData();
        gridExport.WriteXlsxToResponse("ข้อมูลการยืนยันรถ");
    }
    void btnSearch_Click(object sender, EventArgs e)
    {
        Cache.Remove(sds.CacheKeyDependency);
        Cache[sds.CacheKeyDependency] = new object();
        sds.Select(new System.Web.UI.DataSourceSelectArguments());
        sds.DataBind();
        BindData();
    }
    void gvw_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        if (e.CallbackName == "SORT")
        {
            BindData();
        }
    }
    void gvw_PageIndexChanged(object sender, EventArgs e)
    {
        BindData();
    }

    protected void TP01RouteOnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;

        sdsOrganiz.SelectCommand = "SELECT STERMINALID, STERMINALNAME FROM (SELECT T.STERMINALID, TS.STERMINALNAME, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T  INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE T.STERMINALID || TS.STERMINALNAME LIKE :fillter AND (T.STERMINALID LIKE :Plant5 OR T.STERMINALID LIKE :Plant8))  WHERE RN BETWEEN :startIndex AND :endIndex";
        sdsOrganiz.SelectParameters.Clear();
        sdsOrganiz.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsOrganiz.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());
        sdsOrganiz.SelectParameters.Add("Plant5", PlantHelper.Plant5);
        sdsOrganiz.SelectParameters.Add("Plant8", PlantHelper.Plant8);

        comboBox.DataSource = sdsOrganiz;
        comboBox.DataBind();

    }

    protected void TP01RouteOnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    void BindData()
    {
        string strsql = "";
        string strstatus = "";
        string SVENDORID = cbxVendor.Value + "", STERMINALID = cbxTerminal.Value + "";

        if (cbxVendor.Value != "")
        {
            strstatus += " AND TC.SVENDORID LIKE '%" + SVENDORID + "%'";
        }
        if (cbxTerminal.Value != "")
        {
            strstatus += " AND  NVL(NVL(TT.STERMINALID,TC.SCONT_PLNT),' ') LIKE '%" + STERMINALID + "%'";
        }
        if (dteSearch.Value != null)
        {
            strstatus += " AND TTC.DDATE = TO_DATE('" + dteSearch.Date.AddDays(-1).ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','DD/MM/YYYY') ";
        }

        strsql = @"select DISTINCT TC.sContractID,sContractNo,TV.sAbbreviation,TC.SVENDORID,TT.SABBREVIATION sNameTerminal,TTC.DDATE+1 DDATE,sHeadRegisterNo,sTrailerRegisterNo
,TTR.SDRIVERNAME
from  TContract TC left OUTER join TVendor TV  on TV.SVENDORID = TC.SVENDORID
left OUTER join TTerminal TT on TT.STERMINALID = TC.STERMINALID
left OUTER join TContract_Truck TCT on TCT.sContractID = TC.sContractID
left OUTER join TTruck TTr on TTr.sTruckID = TCT.sTruckID 
left OUTER join TTruckConfirm TTC on TTC.sContractID = TC.sContractID
left OUTER join TTruckConfirmList TTCL on TTC.nConfirmID = TTCL.nConfirmID
where cConfirm = '1' AND TC.sContractNo LIKE '%' || :sContractID || '%'  " + strstatus;
        //Response.Write(strsql);

        sds.SelectCommand = strsql;
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add(":sContractID", txtSearch.Text.Trim());

        sds.DataBind();
        gvw.DataBind();

    }
}