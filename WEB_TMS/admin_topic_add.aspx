﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_topic_add.aspx.cs" Inherits="admin_topic_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15
        {
            color: #03527C;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" ></ClientSideEvents>

        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" width="30%">
                            <span class="style12">หัวข้อตัดคะแนนประเมิน</span><font color="#ff0000">*</font>
                        </td>
                        <td colspan="7">
                            <dx:ASPxTextBox ID="txtTopicName" runat="server" Width="500px" MaxLength="200">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                      <td>ทุจริต</td><td>
                          <dx:ASPxCheckBox ID="chkCheck" ClientInstanceName="chkCheck" runat="server">
                          <ClientSideEvents CheckedChanged="function(s , e){if(s.GetChecked()){txtPoint.SetValue('30');}}" ></ClientSideEvents>
                          </dx:ASPxCheckBox>
                      </td>
                    </tr>
                    <tr>
                        <td>
                            <span>มุมมองที่ใช้ประเมินความรุนแรง</span>
                        </td>
                        <td align="center">
                            <span>ภาพลักษณ์<br />
                                องค์กร</span>
                        </td>
                        <td align="center">
                            <span>ความพึงพอใจ<br />
                                ลูกค้า</span>
                        </td>
                        <td align="center">
                            <span>กฎ/ระเบียบฯ</span>,
                            <br />
                            หลักความปลอดภัย</td>
                        <td align="center" bgcolor="#FFFFFF">
                            <span>แผนงานขนส่ง</span>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                            <span>ผลคูณ<br />
                                ความรุนแรง</span>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                            <span>หักคะแนน<br />
                                ต่อครั้ง</span>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>ตัวคูณความรุนแรง<font color="#ff0000">*</font></span>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCal2" runat="server" Width="80px" ClientInstanceName="cboCal2"  ItemStyle-Wrap = "True"
                                ValueField="NIMPACTLEVEL" TextFormatString="{0}" TextField="NIMPACTLEVEL" 
                                DataSourceID="sdsCal2" >
                                <ClientSideEvents ValueChanged="function(s,e){if (cboCal2.GetValue() != null && cboCal3.GetValue() != null && cboCal4.GetValue() != null && cboCal6.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}" ></ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="100px" />
                                    <dx:ListBoxColumn Caption="Category" FieldName="SCATEGORYNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="250px" />
                                </Columns>

<ItemStyle Wrap="True"></ItemStyle>

                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCal2" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE SCATEGORYTYPEID = '02' ORDER BY NIMPACTLEVEL">
                            </asp:SqlDataSource>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCal3" runat="server" DataSourceID="sdsCal3" ClientInstanceName="cboCal3" ItemStyle-Wrap = "True"
                                ValueField="NIMPACTLEVEL" TextFormatString="{0}" TextField="NIMPACTLEVEL" 
                                Width="80px" >
                                <ClientSideEvents ValueChanged="function(s,e){if (cboCal2.GetValue() != null && cboCal3.GetValue() != null && cboCal4.GetValue() != null && cboCal6.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}" ></ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="100px" />
                                    <dx:ListBoxColumn Caption="Category" FieldName="SCATEGORYNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="250px" />
                                </Columns>

<ItemStyle Wrap="True"></ItemStyle>

                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" 
                                    SetFocusOnError="True" ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCal3" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE SCATEGORYTYPEID = '03' ORDER BY NIMPACTLEVEL">
                            </asp:SqlDataSource>
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cboCal4" runat="server" DataSourceID="sdsCal4" ClientInstanceName="cboCal4" ItemStyle-Wrap = "True"
                                ValueField="NIMPACTLEVEL" TextFormatString="{0}" TextField="NIMPACTLEVEL" Width="80px">
                                <ClientSideEvents ValueChanged="function(s,e){if (cboCal2.GetValue() != null && cboCal3.GetValue() != null && cboCal4.GetValue() != null && cboCal6.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}" ></ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="100px" />
                                    <dx:ListBoxColumn Caption="Category" FieldName="SCATEGORYNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="250px"  />
                                </Columns>

<ItemStyle Wrap="True"></ItemStyle>

                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" 
                                    SetFocusOnError="True" ValidationGroup="add">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCal4" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                                SelectCommand="SELECT c.NIMPACTLEVEL,C.SCATEGORYNAME, CASE WHEN TRIM(c.SDEFINE) = '-' THEN (SELECT cs.SDEFINE FROM TCATEGORY cs WHERE CS.SCATEGORYNAME = C.SCATEGORYNAME AND cs.SCATEGORYTYPEID = '05' ) || '' ELSE  c.SDEFINE END SDEFINE FROM TCATEGORY c WHERE c.SCATEGORYTYPEID = '04' ORDER BY c.NIMPACTLEVEL">
                            </asp:SqlDataSource>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                            <dx:ASPxComboBox ID="cboCal6" runat="server" Width="80px" ClientInstanceName="cboCal6" ItemStyle-Wrap = "True"
                                ValueField="NIMPACTLEVEL" TextFormatString="{0}" TextField="NIMPACTLEVEL" DataSourceID="sdsCal6">
                                <ClientSideEvents ValueChanged="function(s,e){if (cboCal2.GetValue() != null && cboCal3.GetValue() != null && cboCal4.GetValue() != null && cboCal6.GetValue() != null) {
                xcpn.PerformCallback('CheckScore');}}" ></ClientSideEvents>
                                <Columns>
                                    <dx:ListBoxColumn Caption="Score" FieldName="NIMPACTLEVEL" Width="100px" />
                                    <dx:ListBoxColumn Caption="Category" FieldName="SCATEGORYNAME" Width="100px" />
                                    <dx:ListBoxColumn Caption="นิยาม" FieldName="SDEFINE" Width="250px"  />
                                </Columns>

<ItemStyle Wrap="True"></ItemStyle>

                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsCal6" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT NIMPACTLEVEL, SCATEGORYNAME,  SDEFINE FROM TCATEGORY WHERE SCATEGORYTYPEID = '06' ORDER BY NIMPACTLEVEL">
                            </asp:SqlDataSource>
                        </td>   
                        <td align="center" bgcolor="#FFFFFF">
                            <dx:ASPxTextBox ID="txtMultipleImpact" runat="server" Width="80px" ClientInstanceName="txtMultipleImpact"
                                 ForeColor="#A6A6A6" ClientEnabled="false">
                                <ClientSideEvents TextChanged="function(s,e){xcpn.PerformCallback('CheckScore'); }" ></ClientSideEvents>
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                </ValidationSettings>
                                <Border BorderColor="#A6A6A6"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                            <dx:ASPxTextBox ID="txtPoint" ClientInstanceName="txtPoint" runat="server" Width="80px" ClientEnabled="false">
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                    SetFocusOnError="True" Display="Dynamic">
                                    <ErrorFrameStyle ForeColor="Red">
                                    </ErrorFrameStyle>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                    <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_MinusNumber %>">
                                    </RegularExpression>
                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                </ValidationSettings>
                                <Border BorderColor="#A6A6A6"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="center" bgcolor="#FFFFFF">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="style15">Process ที่ใช้งาน </span>
                        </td>
                        <td colspan="7" >
                            <dx:ASPxCheckBoxList ID="chkProcess" runat="server" DataSourceID="sdsProcess" 
                                RepeatColumns="4" RepeatDirection="Horizontal" ValueField="SPROCESSID" TextField="SPROCESSNAME" 
                                Border-BorderStyle="None" ItemSpacing="10px" >
<Border BorderStyle="None"></Border>
                            </dx:ASPxCheckBoxList>
                            <asp:SqlDataSource ID="sdsProcess" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>" 
                                SelectCommand="SELECT SPROCESSID, SPROCESSNAME FROM TPROCESS WHERE (SPROCESSID LIKE '%0')"></asp:SqlDataSource>
                        </td>
                      
                    </tr>
                    <tr>
                        <td>
                            คำอธิบายเพิ่มเติม
                        </td>
                        <td colspan="3" width="20%">
                            <dx:ASPxTextBox ID="txtDescription" runat="server" Width="300px" MaxLength="100">
                            </dx:ASPxTextBox>
                        </td>
                          <td align="left" bgcolor="#FFFFFF" style="text-align: left;" width="10%">
                            <dx:ASPxTextBox ID="txtTopicID" runat="server" ClientVisible="false" Width="80px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" width="10%" style="text-align: left;">
                            <dx:ASPxTextBox ID="txtDiffMultiple" runat="server" Width="80px" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" width="10%" style="text-align: left;">
                            <dx:ASPxTextBox ID="txtDiffPoint" runat="server" Width="80px" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" width="20%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>เวอร์ชั่น</span>
                        </td>
                        <td align="center">
                            <dx:ASPxTextBox ID="txtVersion" runat="server" Width="80px" ReadOnly="True" ForeColor="#A6A6A6"
                                NullText="N/A">
                                <Border BorderColor="#A6A6A6" />
                                <Border BorderColor="#A6A6A6"></Border>
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" colspan="6">
                            <span style="color: #A6A6A6;">* การแก้ไขหรือเพิ่มข้อมูล ระบบจะทำการปรับเวอร์ชั่นให้อัตโนมัติ</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>ประเภทหัวข้อ</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTopicType" runat="server" CssClass="form-control"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>ค่าปรับ</span>
                        </td>
                        <td>
                            <asp:TextBox CssClass="form-control" ID="txtCost" runat="server" Width="150px" style="text-align:center" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>ระงับ พขร. (วัน)</span>
                        </td>
                        <td>
                            <asp:TextBox CssClass="form-control" ID="txtDisableDay" runat="server" Width="150px" style="text-align:center" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Blacklist</span>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkBlackList" runat="server" Text="Blacklist" Checked="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>อนุญาติให้แก้ไขคะแนน</span>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCanEdit" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td bgcolor="#FFFFFF">
                            สถานะ <font color="#FF0000">*</font>
                        </td>
                        <td align="left" bgcolor="#FFFFFF" colspan="7">
                            <dx:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                SelectedIndex="0" EnableDefaultAppearance="False" ClientInstanceName="rblStatus"
                                SkinID="rblStatus" ID="rblStatus">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="Active" Value="1"></dx:ListEditItem>
                                    <dx:ListEditItem Text="InActive" Value="0"></dx:ListEditItem>
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" bgcolor="#FFFFFF" align="right">
                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit">
                                <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td colspan="4">
                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close">
                                <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_topic_lst.aspx'; }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF" colspan="8">
                            &nbsp;
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" 
                                 OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" >
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1"
                                        Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="STOPICID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัวข้อตัดคะแนนประเมินผล" FieldName="STOPICNAME"
                                        ShowInCustomizationForm="True" VisibleIndex="3" Width="30%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ประเมินความรุนแรงของแต่ละปัญหาที่เกิดขึ้น" ShowInCustomizationForm="True"
                                        VisibleIndex="4">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="ภาพลักษณ์<br>องค์กร" FieldName="SCOL02" ShowInCustomizationForm="True"
                                                VisibleIndex="4" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ความพึงพอใจ<br>ลูกค้า" FieldName="SCOL03" ShowInCustomizationForm="True"
                                                VisibleIndex="5" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="กฏ/ระเบียบ<br>ข้อห้าม" FieldName="SCOL04" ShowInCustomizationForm="True"
                                                VisibleIndex="6" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="แผนงาน<br>ขนส่ง" FieldName="SCOL06" ShowInCustomizationForm="True"
                                                VisibleIndex="7" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="ผลคูณ<br>ความรุนแรง" FieldName="NMULTIPLEIMPACT"
                                        ShowInCustomizationForm="True" VisibleIndex="11" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หักคะแนน<br>ต่อครั้ง" FieldName="NPOINT" ShowInCustomizationForm="True"
                                        VisibleIndex="12" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataCheckColumn VisibleIndex="13" Width="5%" Caption="สถานะ" HeaderStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'
                                                runat="server" ReadOnly="true">
                                            </dx:ASPxCheckBox>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataTextColumn Caption="Version" FieldName="SVERSION" ShowInCustomizationForm="True"
                                        VisibleIndex="14" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="15" Width="8%">
                                        <DataItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" SkinID="_edit">
                                                            <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add');xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); ASPxClientEdit.ClearGroup('add');}" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                    <td>
                                                        <dx:ASPxButton ID="imbDel" runat="server" CausesValidation="false" SkinID="_delete">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('DelClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="SREMARK" Visible="false"></dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="CPROCESS" Visible="false"></dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    CancelSelectOnNullParameter="False" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY sTopicID,sVersion) AS ID1, CPROCESS, SREMARK, sTopicID, sTopicName, sCol02, sCol03,sCol04,sCol06, nMultipleImpact,nPoint,cActive,sVersion FROM tTopic WHERE sTopicID = :oSearch"
                    DeleteCommand="DELETE FROM tTopic WHERE sTopicID = :sTopicID AND sVersion = :sVersion"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                    <DeleteParameters>
                        <asp:SessionParameter Name="sTopicID" SessionField="dTPID" Type="Int32" />
                        <asp:SessionParameter Name="sVersion" SessionField="dVSN" Type="Int32" />
                    </DeleteParameters>
                    <SelectParameters>
                        <asp:SessionParameter Name="oSearch" SessionField="oTopicID11" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
