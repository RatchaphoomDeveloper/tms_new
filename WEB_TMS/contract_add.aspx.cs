﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Globalization;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using TMS_BLL.Master;
using TMS_BLL.Transaction.SurpriseCheck;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;


public partial class contract_add : PageBase
{
    #region Member
    const string UploadContractDirectory = "UploadFile/Contract/";
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static string TCONTRACT_GUARANTEES = "";
    private static string TCONTRACT_DOC = "";
    DataSet ds = null;
    CreateClassgrp C_grp = new CreateClassgrp();
    private string BackUrl
    {
        get
        {
            if ((string)ViewState["BackUrl"] != null)
                return (string)ViewState["BackUrl"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["BackUrl"] = value;
        }
    }

    private string SMODE
    {
        get
        {
            if ((string)ViewState["SMODE"] != null)
                return (string)ViewState["SMODE"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SMODE"] = value;
        }
    }

    private string SCONTRACTID
    {
        get
        {
            if ((string)ViewState["SCONTRACTID"] != null)
                return (string)ViewState["SCONTRACTID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SCONTRACTID"] = value;
        }
    }

    private string SVENDORID
    {
        get
        {
            if ((string)ViewState["SVENDORID"] != null)
                return (string)ViewState["SVENDORID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SVENDORID"] = value;
        }
    }

    bool P4
    {
        get
        {
            if (ViewState["P4"] != null)
                return (bool)ViewState["P4"];
            else
                return false;
        }
        set
        {
            ViewState["P4"] = value;
        }
    }
    bool PK
    {
        get
        {
            if ((bool)ViewState["PK"] != null)
                return (bool)ViewState["PK"];
            else
                return false;
        }
        set
        {
            ViewState["PK"] = value;
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        Culture = "en-US";
        #region Event
        //gvwGuarantee.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwGuarantee_CustomColumnDisplayText);
        gvwGuarantee.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwGuarantee_AfterPerformCallback);

        //gvwContractDoc.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwContractDoc_CustomColumnDisplayText);
        //gvwContractDoc.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContractDoc_AfterPerformCallback);

        //gvwContractFile.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwContractFile_CustomColumnDisplayText);
        //gvwContractFile.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwContractFile_AfterPerformCallback);

        //gvwTerminal.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwTerminal_CustomColumnDisplayText);
        gvwTerminal.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTerminal_AfterPerformCallback);

        //gvwGProduct.CustomColumnDisplayText += new DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventHandler(gvwGProduct_CustomColumnDisplayText);
        gvwGProduct.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwGProduct_AfterPerformCallback);

        //gvwTruck.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwTruck_CustomColumnDisplayText);
        gvwTruck.AfterPerformCallback += new ASPxGridViewAfterPerformCallbackEventHandler(gvwTruck_AfterPerformCallback);
        gvwTruck.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvwTruck_HtmlRowPrepared);
        #endregion

        //if (!Permissions("50"))
        //{
        //    //  ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='admin_home.aspx';<script>"); return;
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='admin_home.aspx';", true); return;
        //}

        if (!IsPostBack)
        {
            InitialUpload();
            DrowDownList();
            //QUERY GENID
            TCONTRACT_GUARANTEES = SystemFunction.QUERY_TCONTRACT_GUARANTEES();
            TCONTRACT_DOC = SystemFunction.QUERY_TCONTRACT_DOC();
            ClearSession();

            string str = Request.QueryString["str"];
            P4 = PK = false;

            if (Request.QueryString["p4"] != null)
            {
                str = Request.QueryString["p4"];
                P4 = true;
                PK = false;
            }
            else if (Request.QueryString["pk"] != null)
            {
                str = Request.QueryString["pk"];
                PK = true;
                P4 = false;
            }
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                SMODE = "" + QueryString[0];
                SCONTRACTID = "" + QueryString[1];
                SVENDORID = "" + QueryString[2];
                if (P4 || PK)
                {

                }
                else
                {
                    ds = ContractBLL.Instance.ContractSelect(QueryString[1]);
                }

                Session["ssGuarantee"] = ds.Tables["GUARANTEE"];
                Session["ssContractPlant"] = ds.Tables["PLANT"];
                Session["ssGProduct"] = ds.Tables["GPRODUCT"];
                Session["ssContractTruck"] = ds.Tables["TRUCK"];
                DataTable dtup = ds.Tables["UPLOAD"];
                if (dtup.Rows.Count > 0)
                {
                    for (int i = 0; i < dtup.Rows.Count; i++)
                    {
                        dtUpload.Rows.Add(dtup.Rows[i]["UPLOAD_ID"].ToString(), dtup.Rows[i]["UPLOAD_NAME"].ToString(), dtup.Rows[i]["FILENAME_SYSTEM"].ToString(), dtup.Rows[i]["FILENAME_USER"].ToString(), dtup.Rows[i]["FULLPATH"].ToString());
                    }
                    GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
                }

                if ("" + QueryString[0] == "EDIT" || "" + QueryString[0] == "VIEW")
                {
                    SMODE = "EDIT";
                    BindData("BIND_CONTRACT_INFORMATION");
                    BindData("BIND_CONTRACT_GUARANTEE");
                    BindData("BIND_CONTRACT_PLANT");
                    BindData("BIND_CONTRACT_TRUCK");
                    //BindData("BIND_CONTRACT_DOC");
                    //BindData("BIND_CONTRACT_FILE");
                    BindData("BIND_CONTRACT_GPRODUCT");
                    ChangeMode("" + QueryString[0]);
                }
                else
                {
                    btnEditMode.Visible = false;
                    btnViewMode.Visible = false;
                    trModify_type.Visible = false;
                    btnTruckManagment.Enabled = false;
                    ChangeMode("EDIT");
                }
            }

            if (Session["CGROUP"] + string.Empty == "0")
            {
                btnEditMode.Visible = false;
                btnTruckManagment.Visible = false;
            }
        }
        else
        {
            cboMainPlant.DataSource = ViewState["DataTTERMINAL"];
            cboMainPlant.DataBind();
            cboPLANTOTHER.DataSource = ViewState["DataTTERMINAL"];
            cboPLANTOTHER.DataBind();
            cmbTransType.DataSource = ViewState["DataTransportType"];
            cmbTransType.DataBind();

            cmbContractType.DataSource = ViewState["DataContractType"];
            cmbContractType.DataBind();
            cmbVendor.DataSource = ViewState["DataTVendorSap"];
            cmbVendor.DataBind();
        }
        this.AssignAuthen();
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {
                btnEditMode.Enabled = false;
                btnSubmit.Enabled = false;
                btnUpload.Enabled = false;
                btnNewTerminal.Enabled = false;
                btnNewGuarantee.Enabled = false;
                btnNewGProduct.Enabled = false;
                btnMainPlant.Enabled = false;
                btnAddNewTerminal.Enabled = false;
                btnTruckManagment.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region xcpn_Callback
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "SAVE":
                //DataTable dtContractTruck = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractTruck"], ((DataTable)Session["ssContractTruck"]).Select("CSTANDBY='N'"));
                // 06/07/2558 - Kiattisak : เอาออกเนื่องจากไม่มีจำนวนรถในสัญญาให้ระบุ
                //int Check = dtContractTruck.Rows.Count;
                //int Car_Contract = SystemFunction.TRUCK_CONTRAC_CHECK(SVENDORID, SCONTRACTID);
                //if (Check == Car_Contract || SMODE.Equals("NEW"))
                //{
                //    SaveData(SMODE);
                //}
                //else
                //{
                //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาใส่รถในสัญญาให้ครบตามสัญญาจำนวน " + Car_Contract + " คัน');");
                //}
                SaveData(SMODE);


                break;
            case "TManagement":
                //ClearSession();
                string cEncrypt = Server.UrlEncode(STCrypt.Encrypt("EDIT&" + SCONTRACTID + "&" + SVENDORID))
                        , cUrl = "TruckContractManagement.aspx?str=";
                xcpn.JSProperties["cpRedirectOpen"] = cUrl + cEncrypt;
                break;
            case "ddlWordGroupSelectedIndexChanged":
                ddlGroupsDataBind(ddlWordGroup.Value + string.Empty);
                break;
            case "VendorChange":
                ListEditItemsRequestedByFilterConditionEventArgs c = new ListEditItemsRequestedByFilterConditionEventArgs(0, 29, "%%");

                cboVEH_OnItemsRequestedByFilterConditionSQL(cboVEH, c);
                cboVEH.Value = null;
                cboTU.Value = null;
                break;
        }
    }
    #endregion

    #region btnEditMode_Click
    protected void btnEditMode_Click(object sender, EventArgs e)
    {
        ChangeMode("EDIT");
    }
    #endregion

    #region btnViewMode_Click
    protected void btnViewMode_Click(object sender, EventArgs e)
    {
        ChangeMode("VIEW");
    }
    #endregion

    #region GridView

    protected void gvwContractDoc_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwContractDoc_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "SCONTRACTID", "SDOCID", "SDOCVERSION", "SDOCTYPE", "SFILENAME"
            , "SSYSFILENAME", "SDESCRIPTION", "SPATH", "NVERSION", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                //gvwContractDoc.CancelEdit();
                break;
            case "VIEWDOC":
                string sUrl_View = "openFile.aspx?str=" + dyData[7] + "" + dyData[5];
                //gvwContractDoc.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            case "DEL_CONTRACT_DOC":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
                //DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractDoc", "", "" + dyData[0], "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");

                //DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("SDOCID='" + dyData[1] + "'");

                //if (_drDataDels.Length > 0)
                //{
                //    foreach (DataRow _drDataDel in _drDataDels)
                //    {
                //        int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
                //        if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                //        {
                //            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
                //        }
                //        else
                //        {
                //            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
                //            dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                //            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
                //        }
                //    }
                //}

                //BindData("BIND_CONTRACT_DOC");
                break;
            case "BIND_CONTRACT_DOC":
                //BindData("BIND_CONTRACT_DOC");
                break;

        }
    }

    protected void gvwContractFile_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwContractFile_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "SCONTRACTID", "SDOCID", "SDOCVERSION", "SDOCTYPE", "SFILENAME"
            , "SSYSFILENAME", "SDESCRIPTION", "SPATH", "NVERSION", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                //gvwContractFile.CancelEdit();
                break;
            case "VIEWFILE":
                string sUrl_View = "openFile.aspx?str=" + dyData[7] + "" + dyData[5];
                //gvwContractFile.JSProperties["cpRedirectOpen"] = sUrl_View;
                break;

            //case "DEL_CONTRACT_FILE":
            //    //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
            //    DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractDoc", "", "" + dyData[0], "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");

            //    DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("SDOCID='" + dyData[1] + "'");

            //    if (_drDataDels.Length > 0)
            //    {
            //        foreach (DataRow _drDataDel in _drDataDels)
            //        {
            //            int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
            //            if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
            //            {
            //                dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
            //            }
            //            else
            //            {
            //                dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
            //                dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
            //                dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
            //            }
            //        }
            //    }

            //    BindData("BIND_CONTRACT_FILE");
            //    break;
            //case "BIND_CONTRACT_FILE":
            //    BindData("BIND_CONTRACT_FILE");
            //    break;

        }
    }

    protected void gvwGuarantee_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwGuarantee_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwGuarantee.CancelEdit();
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwGuarantee.StartEdit(visibleindex);
                dynamic dyData = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");
                ASPxComboBox cmbSGUARANTEETYPE = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbSGUARANTEETYPE");
                ASPxTextBox txtSGUARANTEETYPE = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSGUARANTEETYPE");
                ASPxTextBox txtNCGID = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtNCGID");
                ASPxTextBox txtSBOOKNO = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSBOOKNO");
                ASPxTextBox txtGuaranteeValue = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtGuaranteeValue");
                ASPxComboBox cmbRemark = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbRemark");
                cmbSGUARANTEETYPE.Value = "" + dyData[3];
                txtSGUARANTEETYPE.Text = "" + dyData[3];
                txtNCGID.Text = "" + dyData[0];
                txtSBOOKNO.Text = "" + dyData[2];
                txtGuaranteeValue.Text = "" + dyData[5];
                foreach (ListEditItem item in cmbRemark.Items)
                {
                    if (item.Text == "" + dyData[6])
                    {
                        item.Selected = true;
                        break;
                    }
                }
                #endregion
                break;
            case "NEWGUARANTEE":
                gvwGuarantee.AddNewRow();
                break;
            case "DELGUARANTEE":
                dynamic dyDataDel = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");

                DataTable dtGuarantee4Delete = PrepareDataTable("ssGuarantee", "", "" + dyDataDel[1], "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtGuarantee4Delete.Select("NCGID='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtGuarantee4Delete.Rows.IndexOf(_drDataDel);
                        if (dtGuarantee4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtGuarantee4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtGuarantee4Delete.Rows[idx_drTemp].BeginEdit();
                            dtGuarantee4Delete.Rows[idx_drTemp]["CDEL"] = (dtGuarantee4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGuarantee4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssGuarantee"] = dtGuarantee4Delete;
                gvwGuarantee.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGuarantee4Delete, dtGuarantee4Delete.Select("CDEL='0'"));
                gvwGuarantee.DataBind();
                break;
            case "SAVEGUARANTEE":
                dynamic dyDataSubmit = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE",
                    "NAMOUNT", "SREMARK");

                string guarantee_mode = (visibleindex == -1) ? "Add" : "Edit";
                /*NCGID NCONTRACTID SBOOKNO GUARANTEESTYPE_ID   SGUARANTEETYPE  NAMOUNT SREMARK CCHANGE CDEL*/
                DataTable dtGuarantee = PrepareDataTable("ssGuarantee", "", "" + dyDataSubmit[1], "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL");
                /*FindControl*/
                ASPxComboBox Save_cmbSGUARANTEETYPE = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbSGUARANTEETYPE");
                ASPxTextBox Save_txtSGUARANTEETYPE = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSGUARANTEETYPE");
                ASPxTextBox Save_txtNCGID = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtNCGID");
                ASPxTextBox Save_txtSBOOKNO = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtSBOOKNO");
                ASPxTextBox Save_txtGuaranteeValue = (ASPxTextBox)gvwGuarantee.FindEditFormTemplateControl("txtGuaranteeValue");
                ASPxComboBox Save_cmbRemark = (ASPxComboBox)gvwGuarantee.FindEditFormTemplateControl("cmbRemark");
                switch (guarantee_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */

                        string NCGID = "" + dtGuarantee.Compute("MIN(NCGID)", "");
                        NCGID = NCGID == "" ? "-1" : "" + (int.Parse(NCGID) - 1);
                        DataRow drNewRow = dtGuarantee.NewRow();
                        drNewRow["NCGID"] = "" + NCGID;
                        drNewRow["NCONTRACTID"] = SCONTRACTID == "" ? DBNull.Value : (object)SCONTRACTID;
                        drNewRow["SBOOKNO"] = "" + Save_txtSBOOKNO.Text;
                        drNewRow["GUARANTEESTYPE_ID"] = "" + Save_cmbSGUARANTEETYPE.Value;
                        drNewRow["SGUARANTEETYPE"] = "" + Save_cmbSGUARANTEETYPE.Text;
                        drNewRow["NAMOUNT"] = "" + Save_txtGuaranteeValue.Text;
                        drNewRow["SREMARK"] = "" + Save_cmbRemark.Text;
                        drNewRow["CNEW"] = "1";
                        drNewRow["CCHANGE"] = "0";
                        drNewRow["CDEL"] = "0";
                        dtGuarantee.Rows.Add(drNewRow);
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        DataRow[] _drData = dtGuarantee.Select("NCGID='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtGuarantee.Rows.IndexOf(_drData[0]);
                            dtGuarantee.Rows[idx_drTemp].BeginEdit();

                            //dtGuarantee.Rows[idx_drTemp]["NCGID"] = "" + NCGID;
                            dtGuarantee.Rows[idx_drTemp]["NCONTRACTID"] = SCONTRACTID;
                            dtGuarantee.Rows[idx_drTemp]["SBOOKNO"] = "" + Save_txtSBOOKNO.Text;
                            dtGuarantee.Rows[idx_drTemp]["GUARANTEESTYPE_ID"] = "" + Save_cmbSGUARANTEETYPE.Value;
                            dtGuarantee.Rows[idx_drTemp]["SGUARANTEETYPE"] = "" + Save_cmbSGUARANTEETYPE.Text;
                            dtGuarantee.Rows[idx_drTemp]["NAMOUNT"] = "" + Save_txtGuaranteeValue.Text;
                            dtGuarantee.Rows[idx_drTemp]["SREMARK"] = "" + Save_cmbRemark.Text;
                            dtGuarantee.Rows[idx_drTemp]["CNEW"] = dtGuarantee.Rows[idx_drTemp]["CNEW"] + "";
                            dtGuarantee.Rows[idx_drTemp]["CCHANGE"] = (dtGuarantee.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGuarantee.Rows[idx_drTemp]["CDEL"] = "0";

                            dtGuarantee.Rows[idx_drTemp].EndEdit();

                        }
                        #endregion
                        break;

                }
                Session["ssGuarantee"] = dtGuarantee;
                gvwGuarantee.CancelEdit();
                gvwGuarantee.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGuarantee, dtGuarantee.Select("CDEL='0'"));
                gvwGuarantee.DataBind();
                break;
        }
    }

    protected void gvwTerminal_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwTerminal_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex, "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                gvwTerminal.CancelEdit();
                break;
            case "CHECKPLANT":
                if (Session["ssContractPlant"] != null)
                {
                    if (((DataTable)Session["ssContractPlant"]).Select("STERMINALID='" + cboMainPlant.Value + "' AND CDEL='0' AND CMAIN_PLANT='0'").Length > 0)
                    {
                        CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลัง " + cboMainPlant.Text + "เป็นคลังต้นทางอื่นๆแล้ว! กรุณาตรวจสอบ');");

                    }
                    if (cboMainPlant.Value == null)
                    {
                        DataTable dt = (DataTable)Session["ssContractPlant"];
                        DataRow[] drs = dt.Select("CMAIN_PLANT='1'");

                        if (drs.Any())
                        {
                            drs[0]["CDEL"] = "1";
                        }
                        Session["ssContractPlant"] = dt;
                    }
                    BindData("BIND_CONTRACT_PLANT");
                }
                break;
            case "NEWCONTRACTPLANT":
                //gvwTerminal.AddNewRow();
                DataTable dtCONTRACT_Plant = PrepareDataTable("ssContractPlant", "", "" + dyData[2], "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");
                if (cboMainPlant.Value + "" == cboPLANTOTHER.Value + "")
                    CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลัง " + cboPLANTOTHER.Text + "เป็นคลังต้นทางหลักแล้ว! กรุณาตรวจสอบ');");
                else if (dtCONTRACT_Plant.Select("STERMINALID='" + cboPLANTOTHER.Value + "' AND CDEL='0'").Length > 0)
                    CommonFunction.SetPopupOnLoad(gvwTerminal, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','ท่านได้เลือกคลังต้นทางอื่นๆเป็น " + cboPLANTOTHER.Text + "แล้ว! กรุณาตรวจสอบ');");
                else
                {
                    DataRow drCONT_PLNT = dtCONTRACT_Plant.NewRow();

                    string KeyID = "" + dtCONTRACT_Plant.Compute("MIN(KEYID)", "");
                    KeyID = KeyID == "" ? "-1" : "" + (int.Parse(KeyID) - 1);

                    drCONT_PLNT["KEYID"] = "" + KeyID;
                    drCONT_PLNT["STERMINALID"] = "" + cboPLANTOTHER.Value;
                    drCONT_PLNT["STERMINALNAME"] = "" + cboPLANTOTHER.Text;
                    drCONT_PLNT["NCONTRACTID"] = SCONTRACTID;
                    drCONT_PLNT["CMAIN_PLANT"] = "0";
                    drCONT_PLNT["CACTIVE"] = "1";
                    drCONT_PLNT["CNEW"] = "1";
                    drCONT_PLNT["CCHANGE"] = "0";
                    drCONT_PLNT["CDEL"] = "0";
                    dtCONTRACT_Plant.Rows.Add(drCONT_PLNT);

                    Session["ssContractPlant"] = dtCONTRACT_Plant;

                }
                BindData("BIND_CONTRACT_PLANT");
                break;

            case "DEL_CONTRACT_PLANT":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK");
                DataTable dtCONTRACT_DOC4Delete = PrepareDataTable("ssContractPlant", "", "" + dyData[2], "STERMINALID", "STERMINALNAME", "NCONTRACTID", "CMAIN_PLANT", "CACTIVE", "CNEW", "CCHANGE", "CDEL", "KEYID");

                DataRow[] _drDataDels = dtCONTRACT_DOC4Delete.Select("STERMINALID='" + dyData[0] + "' AND NCONTRACTID='" + dyData[2] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtCONTRACT_DOC4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_DOC4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_DOC4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }

                BindData("BIND_CONTRACT_PLANT");
                break;
            case "BIND_CONTRACT_PLANT":
                BindData("BIND_CONTRACT_PLANT");
                break;

        }
    }

    protected void gvwGProduct_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwGProduct_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
                BindData("BIND_CONTRACT_GPRODUCT");
                break;
            case "STARTEDIT":
                #region STARTEDIT
                gvwGProduct.StartEdit(visibleindex);
                dynamic dyData = gvwGProduct.GetRowValues(visibleindex, "GID", "SCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                ASPxComboBox cmbProductType = (ASPxComboBox)gvwGProduct.FindEditFormTemplateControl("cmbProductType");
                cmbProductType.Value = "" + dyData[2];
                #endregion
                break;
            case "NEWGPRODUCT":
                gvwGProduct.AddNewRow();
                break;
            case "DELGPRODUCT":
                dynamic dyDataDel = gvwGProduct.GetRowValues(visibleindex, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                DataTable dtGProduct4Delete = PrepareDataTable("ssGProduct", "", "" + dyDataDel[1], "GID", "SCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

                DataRow[] _drDataDels = dtGProduct4Delete.Select("GID='" + dyDataDel[0] + "'");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        int idx_drTemp = dtGProduct4Delete.Rows.IndexOf(_drDataDel);
                        if (dtGProduct4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtGProduct4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            dtGProduct4Delete.Rows[idx_drTemp].BeginEdit();
                            dtGProduct4Delete.Rows[idx_drTemp]["CDEL"] = (dtGProduct4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGProduct4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssGProduct"] = dtGProduct4Delete;
                gvwGProduct.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGProduct4Delete, dtGProduct4Delete.Select("CDEL='0'"));
                gvwGProduct.DataBind();
                break;
            case "SAVEGPRODUCT":
                dynamic dyDataSubmit = gvwGProduct.GetRowValues(visibleindex, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");

                string GProduct_mode = (visibleindex == -1) ? "Add" : "Edit";
                DataTable dtGProdcut = PrepareDataTable("ssGProduct", "", "" + dyDataSubmit[1], "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL");
                /*FindControl*/
                ASPxComboBox Save_cmbProductType = (ASPxComboBox)gvwGProduct.FindEditFormTemplateControl("cmbProductType");

                switch (GProduct_mode)
                {
                    case "Add":
                        #region Add
                        /*CCHANGE:
                         1  ADD
                         2  EDIT
                         0  DEL
                         */

                        if (dtGProdcut.Select("CDEL='0' AND SPRODUCTTYPEID='" + Save_cmbProductType.Value + "'").Length > 0)
                        {
                            CommonFunction.SetPopupOnLoad(gvwGProduct, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ กลุ่มผลิตภัณฑ์นี้อยู่ในรายการแล้ว');");
                            BindData("BIND_CONTRACT_GPRODUCT");
                            return;
                        }

                        string GID = "" + dtGProdcut.Compute("MIN(GID)", "");
                        GID = GID == "" ? "-1" : "" + (int.Parse(GID) - 1);
                        DataRow drNewRow = dtGProdcut.NewRow();
                        drNewRow["GID"] = "" + GID;
                        drNewRow["NCONTRACTID"] = SCONTRACTID;
                        drNewRow["SPRODUCTTYPEID"] = "" + Save_cmbProductType.Value;
                        drNewRow["SPRODUCTTYPENAME"] = "" + Save_cmbProductType.Text;
                        drNewRow["CACTIVE"] = "1";
                        drNewRow["CNEW"] = "1";
                        drNewRow["CCHANGE"] = "0";
                        drNewRow["CDEL"] = "0";
                        dtGProdcut.Rows.Add(drNewRow);
                        #endregion
                        break;
                    case "Edit":
                        #region EDITE&CHANGE
                        if (dtGProdcut.Select("GID<>'" + dyDataSubmit[0] + "' AND CDEL='0' AND SPRODUCTTYPEID='" + Save_cmbProductType.Value + "'").Length > 0)
                        {
                            CommonFunction.SetPopupOnLoad(gvwGProduct, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ กลุ่มผลิตภัณฑ์นี้อยู่ในรายการแล้ว');");
                            BindData("BIND_CONTRACT_GPRODUCT");
                            return;
                        }

                        DataRow[] _drData = dtGProdcut.Select("GID='" + dyDataSubmit[0] + "'");

                        if (_drData.Length > 0)
                        {
                            int idx_drTemp = dtGProdcut.Rows.IndexOf(_drData[0]);
                            dtGProdcut.Rows[idx_drTemp].BeginEdit();

                            dtGProdcut.Rows[idx_drTemp]["NCONTRACTID"] = SCONTRACTID;
                            dtGProdcut.Rows[idx_drTemp]["SPRODUCTTYPEID"] = "" + Save_cmbProductType.Value;
                            dtGProdcut.Rows[idx_drTemp]["SPRODUCTTYPENAME"] = "" + Save_cmbProductType.Text;
                            dtGProdcut.Rows[idx_drTemp]["CACTIVE"] = "1";
                            dtGProdcut.Rows[idx_drTemp]["CNEW"] = dtGProdcut.Rows[idx_drTemp]["CNEW"] + "";
                            dtGProdcut.Rows[idx_drTemp]["CCHANGE"] = (dtGProdcut.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtGProdcut.Rows[idx_drTemp]["CDEL"] = "0";

                            dtGProdcut.Rows[idx_drTemp].EndEdit();
                        }
                        #endregion
                        break;

                }
                Session["ssGProduct"] = dtGProdcut;
                gvwGProduct.CancelEdit();
                gvwGProduct.DataSource = CommonFunction.ArrayDataRowToDataTable(dtGProdcut, dtGProdcut.Select("CDEL='0'"));
                gvwGProduct.DataBind();
                break;
        }
    }

    protected void gvwTruck_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "" && e.Column.Caption.Equals("ที่.")) e.DisplayText = string.Format("{0}.", e.VisibleRowIndex + 1);
    }
    protected void gvwTruck_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        int visibleindex = 0;
        string CallbackName = e.CallbackName;
        string[] eArgs = e.Args[0].Split(';');
        switch (CallbackName)
        {
            case "SORT":
            case "CANCELEDIT":
                CallbackName = e.CallbackName;
                break;
            default:
                CallbackName = eArgs[0] + "";
                visibleindex = int.TryParse(eArgs[1] + "", out visibleindex) ? int.Parse(eArgs[1] + "") : -1;
                break;
        }

        /*  ,"NCONTRACTID","STRUCKID","STRAILERID", "CSTANDBY",,"CREJECT","DSTART","DEND","KEYID","CNEW","CCHANGE","CDEL","Truck_Type","VEH_NO","TU_NO"  */
        dynamic dyData = ((ASPxGridView)sender).GetRowValues(visibleindex
            , "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL");

        switch (CallbackName.ToUpper())
        {
            default: break;
            case "SORT":
            case "CANCELEDIT":
            case "BIND_CONTRACT_TRUCK":
                BindData("BIND_CONTRACT_TRUCK");
                break;
            case "VIEWTRUCK":
                string scartype = CommonFunction.Get_Value(conn, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID='" + dyData[1] + "'");
                string sEncrypt = ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(dyData[1] + "") + "&CarTypeID=" + ConfigValue.GetEncodeText(scartype + "")
                        , sUrl = "truck_edit.aspx?str=";
                //string sEncrypt = Server.UrlEncode(STCrypt.Encrypt("View&" + dyData[1] + "&" + dyData[2] + "&" + scartype))
                //    , sUrl = "truck_edit.aspx?str=";
                gvwTruck.JSProperties["cpRedirectOpen"] = sUrl + sEncrypt;
                break;
            case "ADD_CONTRACT_TRUCK":
                string msg = "";

                #region ตรวจสอบการตรวจสภาพรถก่อนเข้าสัญญา

                // string truckname = string.Empty;

                // truckname = cboVEH.Value.ToString();

                // if (!string.IsNullOrEmpty(truckname))
                // {
                //     DataTable dt = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelect(truckname
                //, ""
                //, ""
                //, ""
                //, ""
                //, "");
                //     if (dt.Rows.Count > 0)
                //     {
                //         if (dt.Select("IS_ACTIVE = 1").Any() || dt.Select("IS_ACTIVE = 3").Any())
                //         {
                //             throw new Exception("รถทะเบียน " + cboVEH.Text.ToString() + " ยังทำการตรวจสภาพรถก่อนเข้าสัญญาไม่ผ่านรบกวนทำการตรวจสภาพรถก่อนเข้าสัญญา");
                //         }
                //     }
                //     else
                //     {
                //         throw new Exception("รถทะเบียน " + cboVEH.Text.ToString() + " ยังไม่ได้รับการตรจสภาพรถก่อนเข้าสัญญารบกวนทำการตรวจสภาพรถก่อนเข้าสัญญา");
                //     }
                // }

                #endregion

                #region policy
                DataTable dtCONTRACT_Truck = PrepareDataTable("ssContractTruck", "", "" + dyData[0], "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL", "MOVED");

                if (VEH_TYPE.Text != "" && cboVEH.Value + "" != "")
                {
                    switch (VEH_TYPE.Text)
                    {
                        case "สิบล้อ":
                            //สิบล้อ ต้องไม่มีหาง
                            if (cboTU.Value + "" != "")
                            {
                                msg += "<br /> - รถประเภทสิบล้อ ไม่สามารถระบุหางได้ ";
                            }
                            break;
                        case "Semi-Trailer":
                            //Semi-Trailer ต้องมีหาง
                            if (cboTU.Value + "" == "")
                            {
                                msg += "<br /> - รถประเภท semi-trailer จำเป็นต้องระบุทะเบียนหาง ";
                            }
                            break;

                    }
                }
                else
                {
                    msg += "<br /> - รถที่ท่านระบุไม่ได้อยู่ในประเภทที่ปตท.อนุญาติ ";
                }
                //เช็คหัวซ้ำในสัญญาเดียวกัน
                if (dtCONTRACT_Truck.Select("STRUCKID='" + cboVEH.Value + "'").Length > 0)
                {
                    msg += "<br />- ทะเบียนหัว " + cboVEH.Text + " เคยถูกระบุในสัญญานี้แล้ว";
                }
                //เช็คหางซ้ำในสัญญาเดียวกัน
                if (cboTU.Value + "" != "" && dtCONTRACT_Truck.Select("STRAILERID='" + cboTU.Value + "'").Length > 0)
                {
                    msg += "<br />- ทะเบียนหาง " + cboTU.Text + " เคยถูกระบุในสัญญานี้แล้ว";
                }

                string sqlCmd;
                if (SCONTRACTID == "")
                    sqlCmd = "SELECT c.SCONTRACTID,c.SVENDORID,c.SCONTRACTNO,ct.STRUCKID,ct.STRAILERID FROM TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON c.SCONTRACTID=ct.SCONTRACTID WHERE  (ct.STRUCKID='" + "" + cboVEH.Value + "' OR ct.STRAILERID='" + "" + cboTU.Value + "') AND c.CACTIVE='Y'";
                else
                    sqlCmd = "SELECT c.SCONTRACTID,c.SVENDORID,c.SCONTRACTNO,ct.STRUCKID,ct.STRAILERID FROM TCONTRACT c LEFT JOIN TCONTRACT_TRUCK ct ON c.SCONTRACTID=ct.SCONTRACTID WHERE c.SCONTRACTID!='" + SCONTRACTID + "" + "' AND (ct.STRUCKID='" + "" + cboVEH.Value + "' OR ct.STRAILERID='" + "" + cboTU.Value + "') AND c.CACTIVE='Y'";

                DataTable dtCheck = CommonFunction.Get_Data(conn, sqlCmd);
                //เช็คหัวซ้ำในสัญญาอื่น
                if (dtCheck.Select("STRUCKID='" + "" + cboVEH.Value + "'").Length > 0)
                {
                    DataRow[] dr = dtCheck.Select("STRUCKID='" + "" + cboVEH.Value + "'");
                    string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dr[0]["SCONTRACTID"] + "&" + dr[0]["SVENDORID"]))
                            , vsUrl = "contract_add.aspx?str=" + vsEncrypt;
                    string link = @"<a href=\""javascript:void(0);\"" onclick=\""javascript:window.open(\'" + vsUrl + @"\');\"">" + dr[0]["SCONTRACTNO"] + "</a>";
                    msg += @"<br />- ทะเบียนหัว " + cboVEH.Text + @" ได้ถูกระบุในสัญญาเลขที่<br/> " + link + " แล้ว";
                }
                //เช็คหางซ้ำในสัญญาอื่น
                if (dtCheck.Select("STRAILERID='" + "" + cboTU.Value + "'").Length > 0)
                {
                    DataRow[] dr = dtCheck.Select("STRAILERID='" + "" + cboTU.Value + "'");
                    string vsEncrypt = Server.UrlEncode(STCrypt.Encrypt("VIEW&" + dr[0]["SCONTRACTID"] + "&" + dr[0]["SVENDORID"]))
                            , vsUrl = "contract_add.aspx?str=" + vsEncrypt;
                    string link = @"<a href=\""javascript:void(0);\"" onclick=\""javascript:window.open(\'" + vsUrl + @"\');\"">" + dr[0]["SCONTRACTNO"] + "</a>";
                    msg += @"<br />- ทะเบียนหาง " + cboTU.Text + @" ได้ถูกระบุในสัญญาเลขที่<br/> " + link + " แล้ว";
                }

                if (msg != "")
                {
                    CommonFunction.SetPopupOnLoad(gvwTruck, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','กรุณาตรวจสอบ " + msg + "');");
                    BindData("BIND_CONTRACT_TRUCK");
                    return;
                }
                #endregion
                DataRow drCONT_PLNT = dtCONTRACT_Truck.NewRow();

                string KeyID = "" + dtCONTRACT_Truck.Compute("MIN(KEYID)", "");
                KeyID = KeyID == "" ? "-1" : "" + (int.Parse(KeyID) - 1);

                drCONT_PLNT["KEYID"] = "" + KeyID;
                drCONT_PLNT["NCONTRACTID"] = SCONTRACTID;
                drCONT_PLNT["STRUCKID"] = "" + cboVEH.Value;
                drCONT_PLNT["STRAILERID"] = "" + cboTU.Value;
                drCONT_PLNT["VEH_NO"] = "" + cboVEH.Text;
                drCONT_PLNT["TU_NO"] = "" + ((cboTU.Value + "" == "") ? "-" : cboTU.Text);
                drCONT_PLNT["Truck_Type"] = ("" + cboTU.Value == "") ? "10Wheel" : "Semi-Trailer";
                drCONT_PLNT["CSTANDBY"] = ckbIsStandBy.Checked ? "Y" : "N";
                //drCONT_PLNT["CREJECT"] = null;
                //drCONT_PLNT["DSTART"] = null;
                //drCONT_PLNT["DEND"] = null;

                drCONT_PLNT["CNEW"] = "1";
                drCONT_PLNT["CCHANGE"] = "0";
                drCONT_PLNT["CDEL"] = "0";
                drCONT_PLNT["MOVED"] = "0";
                dtCONTRACT_Truck.Rows.Add(drCONT_PLNT);
                Session["ssContractTruck"] = dtCONTRACT_Truck;
                cboVEH.Text = VEH_TYPE.Text = TU_TYPE.Text = cboTU.Text = "";
                #region message แสดงตอนนำส่งClassgrp
                C_grp.Veh_no = drCONT_PLNT["VEH_NO"].ToString();
                C_grp.TU_NO = drCONT_PLNT["TU_NO"].ToString();
                C_grp.contractType = cblProcurement.Value.ToString();
                C_grp.contractName = txtContract.Text.Trim().ToString();
                if (Session["ssGProduct"] != null && ((DataTable)Session["ssGProduct"]).Rows.Count > 0)
                {
                    C_grp.GproductType = ((DataTable)Session["ssGProduct"]).Rows[0]["SPRODUCTTYPEID"].ToString();
                }

                CommonFunction.SetPopupOnLoad(gvwTruck, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + C_grp.PupupAddClassgrp() + "');");
                #endregion
                BindData("BIND_CONTRACT_TRUCK");

                break;
            case "EDIT_CONTRACT_TRUCK":
                TRUCK_KEYID.Text = "" + dyData[9];
                cboVEH.Value = "" + dyData[1];
                cboTU.Value = "" + dyData[2];
                break;
            case "DEL_CONTRACT_TRUCK":
                //dynamic dyDataDelDoc = gvwGuarantee.GetRowValues(visibleindex, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK"); 
                DataTable dtCONTRACT_TRCK4Delete = PrepareDataTable("ssContractTruck", "", "" + dyData[0], "NCONTRACTID", "STRUCKID", "STRAILERID", "Truck_Type", "VEH_NO", "TU_NO", "CSTANDBY", "CREJECT", "DSTART", "DEND", "KEYID", "CNEW", "CCHANGE", "CDEL", "MOVED");
                DataRow[] _drDataDels = dtCONTRACT_TRCK4Delete.Select(" NCONTRACTID='" + dyData[0] + "' AND STRUCKID='" + dyData[1] + "' AND isnull(STRAILERID,'')='" + dyData[2] + "' ");

                if (_drDataDels.Length > 0)
                {
                    foreach (DataRow _drDataDel in _drDataDels)
                    {
                        if (_drDataDel["MOVED"].ToString().Trim() == "1")
                        {
                            CommonFunction.SetPopupOnLoad(gvwTruck, "dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากรถคันนี้ถูกโยกไปช่วยสัญญาอื่น');"); break;
                        }
                        int idx_drTemp = dtCONTRACT_TRCK4Delete.Rows.IndexOf(_drDataDel);
                        if (dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1")
                        {
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].Delete();
                        }
                        else
                        {
                            #region แสดง msg ยกเลิกClassgrp
                            C_grp.Veh_no = dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["VEH_NO"].ToString();
                            C_grp.TU_NO = dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["TU_NO"].ToString();
                            C_grp.contractType = cblProcurement.Value.ToString();
                            C_grp.contractName = txtContract.Text.Trim().ToString();
                            if (Session["ssGProduct"] != null && ((DataTable)Session["ssGProduct"]).Rows.Count > 0)
                            {
                                C_grp.GproductType = ((DataTable)Session["ssGProduct"]).Rows[0]["SPRODUCTTYPEID"].ToString();
                            }

                            CommonFunction.SetPopupOnLoad(gvwTruck, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + C_grp.PupupCancelClassgrp() + "');");
                            #endregion
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].BeginEdit();
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CDEL"] = (dtCONTRACT_TRCK4Delete.Rows[idx_drTemp]["CNEW"] + "" == "1") ? "0" : "1";
                            dtCONTRACT_TRCK4Delete.Rows[idx_drTemp].EndEdit();
                        }
                    }
                }
                Session["ssContractTruck"] = dtCONTRACT_TRCK4Delete;
                BindData("BIND_CONTRACT_TRUCK");
                break;


        }
    }
    protected void gvwTruck_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        switch (e.RowType)
        {
            case GridViewRowType.Data:
                if (e.GetValue("TU_NO") + "" == "")
                {
                    ASPxLabel lblTU_No = (ASPxLabel)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "lblTU_No");
                    lblTU_No.Text = "-";
                }
                ASPxButton btnDelTruck = (ASPxButton)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDelTruck");
                if (btnDelTruck != null)
                {
                    if (e.GetValue("MOVED").ToString().Trim() == "1")
                        btnDelTruck.ClientSideEvents.Click = "function(s,e){ dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากรถคันนี้ถูกโยกไปช่วยสัญญาอื่น'); }";
                    else


                        btnDelTruck.ClientSideEvents.Click = "function(s,e){ if(!gvwTruck.InCallback()) gvwTruck.PerformCallback('DEL_CONTRACT_TRUCK;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }";
                }
                break;
        }
    }

    #endregion

    #region Combo box
    private void DrowDownList()
    {
        ViewState["DataTTERMINAL"] = ContractBLL.Instance.TTERMINALSelect();
        cboMainPlant.DataSource = ViewState["DataTTERMINAL"];
        cboMainPlant.DataBind();
        cboPLANTOTHER.DataSource = ViewState["DataTTERMINAL"];
        cboPLANTOTHER.DataBind();

        ViewState["DataTransportType"] = ContractBLL.Instance.TransportTypeSelect();
        cmbTransType.DataSource = ViewState["DataTransportType"];
        cmbTransType.DataBind();

        ViewState["DataContractType"] = ContractBLL.Instance.ContractTypeSelect();
        cmbContractType.DataSource = ViewState["DataContractType"];
        cmbContractType.DataBind();


        //ASPxComboBox cmbProductType = (ASPxComboBox)gvwGProduct.FindEditFormTemplateControl("cmbProductType");
        //cmbProductType.DataSource = ContractBLL.Instance.ProductTypeSelect();
        //cmbProductType.DataBind();
        ddlWordGroup.TextField = "NAME";
        ddlWordGroup.ValueField = "ID";
        ddlWordGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWordGroup.DataBind();
        ddlWordGroup.Items.Insert(0, new ListEditItem()
        {
            Text = "เลือกกลุ่มงาน",
            Value = null
        });
        ddlWordGroup.SelectedIndex = 0;
        //ddlGroups.DataTextField = "NAME";
        //ddlGroups.DataValueField = "ID";
        //ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect();
        //ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListEditItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = null
        });
        ddlGroups.SelectedIndex = 0;
        ViewState["DataTVendorSap"] = VendorBLL.Instance.TVendorSapSelect();
        cmbVendor.DataSource = ViewState["DataTVendorSap"];
        cmbVendor.DataBind();

    }

    protected void ddlWordGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWordGroup.Value + string.Empty);
    }

    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroups.TextField = "NAME";
        ddlGroups.ValueField = "ID";
        ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListEditItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
        ddlGroups.SelectedIndex = 0;
    }

    protected void cboVEH_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVEH_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        if (!string.IsNullOrEmpty(cmbVendor.Value + string.Empty))
        {
            ASPxComboBox comboBox = (ASPxComboBox)source;
            comboBox.DataSource = ContractBLL.Instance.VEHSelectByPagesize(String.Format("%{0}%", e.Filter), e.BeginIndex + 1, e.EndIndex + 1, cmbVendor.Value + string.Empty);
            comboBox.DataBind();
        }

    }

    protected void cmbConTract_ItemRequestedByValue(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbConTract_ItemsRequestedByFilterCondition(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.DataSource = ContractBLL.Instance.MContractSelectByPagesize(String.Format("%{0}%", e.Filter), e.BeginIndex + 1, e.EndIndex + 1);
        comboBox.DataBind();
    }

    protected void cmbConTractNew_ItemRequestedByValue(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cmbConTractNew_ItemsRequestedByFilterCondition(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        comboBox.DataSource = ContractBLL.Instance.MContractSelectByPagesize(String.Format("%{0}%", e.Filter), e.BeginIndex + 1, e.EndIndex + 1);
        comboBox.DataBind();
    }
    protected void cboTU_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (cboVEH.Value + "" != "")
        {
            string VEH_ID = cboVEH.Value.ToString();
            cboTU.DataSource = ContractBLL.Instance.TUSelect(VEH_ID);
            cboTU.DataBind();
        }

    }

    #endregion

    #region Uploadfile

    //protected void ulcContractDoc_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    //{
    //    string SCONTRACTID = "" + Session["SCONTRACTID"];

    //    e.CallbackData = UploadFile2Server(e.UploadedFile
    //        , "ContractDoc_" + SCONTRACTID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
    //        , UploadContractDirectory + "Temp/ContractDoc/" + SCONTRACTID + "/");

    //    DataTable dtContractDoc = PrepareDataTable("ssContractDoc", "", "" + SCONTRACTID
    //        , "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");
    //    string[] sFileArray = e.CallbackData.Split('$');

    //    string DOCID = "" + dtContractDoc.Compute("MIN(SDOCID)", "");
    //    DOCID = (DOCID == "") ? "-1" : "" + (int.Parse(DOCID) - 1);
    //    DataRow drNewRow = dtContractDoc.NewRow();
    //    drNewRow["SDOCID"] = "" + DOCID;
    //    drNewRow["SCONTRACTID"] = "" + SCONTRACTID;
    //    drNewRow["SDOCTYPE"] = "DOC";
    //    drNewRow["SFILENAME"] = "" + sFileArray[1];
    //    drNewRow["SSYSFILENAME"] = "" + sFileArray[0];
    //    drNewRow["SDESCRIPTION"] = "เอกสารสัญญา";
    //    drNewRow["CACTIVE"] = "1";
    //    drNewRow["SPATH"] = UploadContractDirectory + "/Temp/ContractDoc/" + SCONTRACTID + "/";
    //    drNewRow["CNEW"] = "1";
    //    drNewRow["CCHANGE"] = "0";
    //    drNewRow["CDEL"] = "0";
    //    dtContractDoc.Rows.Add(drNewRow);

    //    Session["ssContractDoc"] = dtContractDoc;
    //}

    //protected void ulcContractFile_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    //{
    //    string SCONTRACTID = "" + Session["SCONTRACTID"];

    //    e.CallbackData = UploadFile2Server(e.UploadedFile
    //        , "ContractDoc_" + SCONTRACTID + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss")
    //        , UploadContractDirectory + "Temp/ContractFile/" + SCONTRACTID + "/");

    //    DataTable dtContractDoc = PrepareDataTable("ssContractDoc", "", "" + SCONTRACTID
    //        , "SDOCID", "SCONTRACTID", "SDOCTYPE", "SFILENAME", "SSYSFILENAME", "SDESCRIPTION", "CACTIVE", "SPATH", "CNEW", "CCHANGE", "CDEL");
    //    string[] sFileArray = e.CallbackData.Split('$');

    //    string DOCID = "" + dtContractDoc.Compute("MIN(SDOCID)", "");
    //    DOCID = (DOCID == "") ? "-1" : "" + (int.Parse(DOCID) - 1);
    //    DataRow drNewRow = dtContractDoc.NewRow();
    //    drNewRow["SDOCID"] = "" + DOCID;
    //    drNewRow["SCONTRACTID"] = "" + SCONTRACTID;
    //    drNewRow["SDOCTYPE"] = "FILE";
    //    drNewRow["SFILENAME"] = "" + sFileArray[1];
    //    drNewRow["SSYSFILENAME"] = "" + sFileArray[0];
    //    drNewRow["SDESCRIPTION"] = "เอกสารอื่นๆ";
    //    drNewRow["CACTIVE"] = "1";
    //    drNewRow["SPATH"] = UploadContractDirectory + "/Temp/ContractFile/" + SCONTRACTID + "/";
    //    drNewRow["CNEW"] = "1";
    //    drNewRow["CCHANGE"] = "0";
    //    drNewRow["CDEL"] = "0";
    //    dtContractDoc.Rows.Add(drNewRow);

    //    Session["ssContractDoc"] = dtContractDoc;
    //}
    //private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    //{
    //    string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
    //    FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
    //    string ResultFileName = ServerMapPath + File.Name;
    //    string sPath = Path.GetDirectoryName(ResultFileName)
    //            , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
    //            , sFileType = Path.GetExtension(ResultFileName);

    //    if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
    //    {
    //        #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
    //        if (!Directory.Exists(ServerMapPath))
    //        {
    //            Directory.CreateDirectory(ServerMapPath);
    //        }
    //        #endregion
    //        string fileName = (GenFileName + "" + sFileType.Trim());
    //        ful.SaveAs(ServerMapPath + fileName);
    //        //LogUser("1", "I", "อัพโหลดไฟล์(" + uploadmode + ") หน้ายืนยันรถตามสัญญา", "");///แก้ไขให้ระบบเก็บLoglว่าอัพโหลดExcel||Picture

    //        return fileName + "$" + sFileName.Replace("$", "") + sFileType;
    //    }
    //    else
    //        return "$";
    //}
    #endregion

    #region Method
    #region Permissions
    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    #endregion

    #region BindData
    void BindData(string mode)
    {
        DataTable dtContractInfo;
        DataView dvContractInfo;
        DataSourceSelectArguments args;
        switch (mode.ToUpper())
        {
            case "BIND_CONTRACT_GUARANTEE":
                #region หลักประกันสัญญา

                //if (Session["ssGuarantee"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsGUARANTEES.Select(args);
                //    Session["ssGuarantee"] = dvContractInfo.ToTable();
                //}

                gvwGuarantee.DataSource = PrepareDataTable("ssGuarantee", "", SCONTRACTID, "NCGID", "NCONTRACTID", "SBOOKNO", "GUARANTEESTYPE_ID", "SGUARANTEETYPE", "NAMOUNT", "SREMARK", "CNEW", "CCHANGE", "CDEL"); ;
                gvwGuarantee.DataBind();
                #endregion
                break;
            case "BIND_CONTRACT_INFORMATION":
                #region รายละเอียดสัญญา"
                dtContractInfo = ds.Tables["INFORMATION"];
                if (dtContractInfo.Rows.Count > 0)
                {
                    foreach (DataRow drInfo in dtContractInfo.Rows)
                    {
                        if ("" + drInfo["RENEWFROM"] != "")
                            txtRenewFrom.Text = "" + drInfo["RENEWFROM"] + "-" + drInfo["SCONTRACTID"];
                        else
                            txtRenewFrom.Text = "" + drInfo["SCONTRACTID"];
                        cblProcurement.Value = "" + drInfo["CSPACIALCONTRAC"];
                        //cmbConTract.Value = "" + drInfo["MCONTRACTID"];
                        txtContract.Text = "" + drInfo["SCONTRACTNO"];
                        txtNtruck.Text = "" + drInfo["NTRUCKCONTRACT"];
                        txtReserve.Text = "" + drInfo["NTRUCKRESERVE"];
                        ddlWordGroup.Value = drInfo["WORKGROUPID"] + string.Empty;
                        ddlGroupsDataBind(ddlWordGroup.Value + string.Empty);
                        ddlGroups.Value = drInfo["GROUPSID"] + string.Empty;
                        cmbVendor.Value = drInfo["SVENDORID"] + string.Empty;
                        //cmbVendor.DataSource = CommonFunction.Get_Data(conn, "SELECT v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORID='" + drInfo["SVENDORID"] + "'");
                        //cmbVendor.DataBind();
                        //cmbVendor.Value = "" + drInfo["SVENDORID"];
                        cmbTransType.Value = "" + drInfo["CGROUPCONTRACT"];
                        cmbContractType.Value = "" + drInfo["SCONTRACTTYPEID"];
                        dteStart.Text = Convert.ToDateTime("" + drInfo["DBEGIN"]).ToString("dd/MM/yyyy");
                        if (drInfo["DEND"] + "" != "")
                            dteEnd.Text = Convert.ToDateTime("" + drInfo["DEND"]).ToString("dd/MM/yyyy");
                        mmoRemark.Text = "" + drInfo["SREMARK"];
                        rblCACTIVE.Value = "" + drInfo["CACTIVE"];
                        break;
                    }
                }
                #endregion
                break;
            case "BIND_CONTRACT_DOC":
                #region แนบเอกสารสัญญา
                //if (Session["ssContractDoc"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsDocument.Select(args);
                //    Session["ssContractDoc"] = dvContractInfo.ToTable();
                //}

                //DataTable dtContractDoc = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractDoc"], ((DataTable)Session["ssContractDoc"]).Select("SDOCTYPE='DOC' AND CDEL='0'"));
                //gvwContractDoc.DataSource = dtContractDoc;
                //gvwContractDoc.DataBind();
                //dtContractDoc.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_FILE":
                #region แนบเอกสารอื่นๆ
                //if (Session["ssContractDoc"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsDocument.Select(args);
                //    Session["ssContractDoc"] = dvContractInfo.ToTable();
                //}

                //DataTable dtContractFile = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractDoc"], ((DataTable)Session["ssContractDoc"]).Select("SDOCTYPE='FILE' AND CDEL='0'"));
                //gvwContractFile.DataSource = dtContractFile;
                //gvwContractFile.DataBind();
                //dtContractFile.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_PLANT":
                #region คลังต้นทางอื่นๆ
                //if (Session["ssContractPlant"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsPlant.Select(args);
                //    Session["ssContractPlant"] = dvContractInfo.ToTable();

                //}
                foreach (DataRow dr in ((DataTable)Session["ssContractPlant"]).Select("CDEL='0' AND CMAIN_PLANT='1'"))
                {
                    cboMainPlant.Value = "" + dr["STERMINALID"];
                    //cboMainPlant.Text = "" + dr["STERMINALNAME"];

                }
                DataTable dtContractPlant = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractPlant"], ((DataTable)Session["ssContractPlant"]).Select("CDEL='0' AND CMAIN_PLANT='0' "));
                gvwTerminal.DataSource = dtContractPlant;
                gvwTerminal.DataBind();
                dtContractPlant.Dispose();
                #endregion
                break;
            case "BIND_CONTRACT_GPRODUCT":
                #region กลุ่มผลิตภัณฑ์
                //if (Session["ssGProduct"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsGPRODUCT.Select(args);
                //    Session["ssGProduct"] = dvContractInfo.ToTable();
                //}

                gvwGProduct.DataSource = PrepareDataTable("ssGProduct", "", SCONTRACTID, "GID", "NCONTRACTID", "SPRODUCTTYPEID", "SPRODUCTTYPENAME", "CACTIVE", "CNEW", "CCHANGE", "CDEL"); ;
                gvwGProduct.DataBind();
                #endregion
                break;
            case "BIND_CONTRACT_TRUCK":
                #region รายชื่อรถในสัญญา
                //if (Session["ssContractTruck"] == null)
                //{
                //    args = new DataSourceSelectArguments();
                //    dvContractInfo = (DataView)sdsContractTruck.Select(args);
                //    Session["ssContractTruck"] = dvContractInfo.ToTable();
                //}

                DataTable dtContractTruck = CommonFunction.ArrayDataRowToDataTable((DataTable)Session["ssContractTruck"], ((DataTable)Session["ssContractTruck"]).Select("CDEL='0'"));
                gvwTruck.DataSource = dtContractTruck;
                gvwTruck.DataBind();
                dtContractTruck.Dispose();
                #endregion
                break;
        }
    }
    #endregion

    #region ChangeMode
    void ChangeMode(string mode)
    {
        if (mode == "EDIT" && Session["chkurl"] + "" == "1")
        {
            mode = "VIEW";
            btnEditMode.ClientEnabled = false;
            btnTruckManagment.ClientEnabled = false;
        }
        bool V;
        string sColor;
        if (mode == "VIEW")
        {
            V = true;
            sColor = "#EEEEEE";
            btnEditMode.Visible = V;
            btnViewMode.Visible = !V;
        }
        else
        {
            V = false;
            sColor = "#FFFFFF";
            btnEditMode.Visible = V;
            btnViewMode.Visible = !V;
        }

        rpnInformation.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cboModify_type.ReadOnly = V;

        txtContract.ReadOnly = V;

        dteStart.ReadOnly = V;
        dteEnd.ReadOnly = V;
        dteStart.Enabled = !V;
        dteEnd.Enabled = !V;
        cmbTransType.ReadOnly = V;
        ddlWordGroup.ReadOnly = V;
        ddlGroups.ReadOnly = V;
        ddlUploadType.Enabled = !V;
        cblProcurement.ReadOnly = V;
        cmbVendor.ReadOnly = V;
        mmoRemark.ReadOnly = V;
        rblCACTIVE.ReadOnly = V;
        cmbContractType.ReadOnly = V;
        ASPxRoundPanel1.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);

        rpnGuarantee.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        gvwGuarantee.Columns[4].Visible = !V;
        tbladdgrt.Visible = !V;

        rpnplant.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cboMainPlant.ReadOnly = V;
        cboPLANTOTHER.ReadOnly = V;
        gvwTerminal.Columns[2].Visible = !V;
        tbladdplant.Visible = !V;

        rpnGProduct.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        gvwGProduct.Columns[3].Visible = !V;
        tbladdGProduct.Visible = !V;

        rpnCTruck.Content.BackColor = System.Drawing.ColorTranslator.FromHtml(sColor);
        cboVEH.ReadOnly = V;
        cboTU.ReadOnly = V;
        ckbIsStandBy.ReadOnly = V;
        tblctruck.Visible = !V;
        gvwTruck.Columns[5].Visible = V;
        gvwTruck.Columns[6].Visible = !V;

        btnSubmit.Visible = !V;
        btnCancel.Visible = !V;


    }
    #endregion

    #region SaveData
    void SaveData(string mode)
    {
        //DataTable ssContractDoc = (Session["ssContractDoc"] == null ? new DataTable() : (DataTable)Session["ssContractDoc"]);
        DataTable ssGuarantee = (Session["ssGuarantee"] == null ? new DataTable() : (DataTable)Session["ssGuarantee"]);
        DataTable ssContractPlant = (Session["ssContractPlant"] == null ? new DataTable() : (DataTable)Session["ssContractPlant"]);
        DataTable ssGProduct = (Session["ssGProduct"] == null ? new DataTable() : (DataTable)Session["ssGProduct"]);
        DataTable ssContractTruck = (Session["ssContractTruck"] == null ? new DataTable() : (DataTable)Session["ssContractTruck"]);


        string CONTRACTID = SCONTRACTID, I_MCONTRACTID = string.Empty, PLNT = "", I_ConTractNAME = txtContract.Text;

        if (mode == "EDIT" & cboModify_type.Value + "" == "1")
        {
            mode = "NEW";
            //I_MCONTRACTID = cmbConTractNew.Value + string.Empty;
            I_ConTractNAME = txtContractNew.Text;
            if (ssContractTruck.Rows.Count > 0)
            {
                if (ssContractTruck.Select("MOVED='0'").Length > 0)
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('แจ้งเตือน', 'ไม่สามารถลบรายการนี้ได้! เนื่องจากมีรถในสัญญาถูกโยกไปช่วยสัญญาอื่น');"); return;
                }
            }
        }
        foreach (DataRow dr in ssContractPlant.Rows) PLNT += "," + dr["STERMINALID"];//ส่งเป็น Para เข้าไป
        //DateTime dtestart = new DateTime();
        //if (!string.IsNullOrEmpty(dteStart.Text))
        //{
        //    dtestart = Convert.ToDateTime(dteStart.Text);
        //}
        //DateTime dteend = new DateTime();
        //if (!string.IsNullOrEmpty(dteStart.Text))
        //{
        //    dteend = Convert.ToDateTime(dteEnd.Text);
        //}
        int Ntruck, Reserve;
        string Mess = "บันทึกข้อมูลในระบบ TMS สำเร็จ<br/>";
        if (int.TryParse(txtNtruck.Text, out Ntruck) && int.TryParse(txtReserve.Text, out Reserve) && ssContractTruck != null && ssContractTruck.Rows.Count > 0)
        {
            DataRow[] drs = ssContractTruck.Select();
            if (drs.Count(it => it["CSTANDBY"] + string.Empty == "N" && it["CDEL"] + string.Empty != "1") < Ntruck)
            {
                Mess += "จำนวนรถในสัญญา น้อยกว่า รถในสัญญาตามสัญญา<br/>";
            }
            else
            {
                Mess += "จำนวนรถในสัญญา มากกว่า รถในสัญญาตามสัญญา<br/>";
            }
            if (drs.Count(it => it["CSTANDBY"] + string.Empty == "Y" && it["CDEL"] + string.Empty != "1") < Reserve)
            {
                Mess += "จำนวนรถสำรอง มากกว่า จำนวนรถที่เพิ่มเข้ามา<br/>";
            }
            if (drs.Count(it => it["CDEL"] + string.Empty != "1") < Reserve + Ntruck)
            {
                Mess += "จำนวนรถรวม มากกว่า จำนวนรถที่เพิ่มเข้ามา<br/>";
            }
        }
        else
        {
            //txtNtruck.Text = "0";
            //txtReserve.Text = "0";
        }
        DataTable dt = ContractBLL.Instance.ContractSave(CONTRACTID, cboModify_type.Value + string.Empty, cmbContractType.Value + string.Empty, I_ConTractNAME, I_ConTractNAME, cmbVendor.Value + string.Empty, dteStart.Text.Trim(), dteEnd.Text.Trim(), ssContractTruck.Select("CSTANDBY='N'").Length + string.Empty, mmoRemark.Text, rblCACTIVE.Value + string.Empty, Session["UserID"] + string.Empty, cblProcurement.Value + string.Empty, cboMainPlant.Value + string.Empty, PLNT, (cboModify_type.Value + "" == "1" ? txtRenewFrom.Text : "null"), ddlGroups.Value + string.Empty, txtNtruck.Text, txtReserve.Text, ssGuarantee, ssContractPlant, ssGProduct, ssContractTruck, dtUpload, mode);
        #region Save Old

        //                switch (mode)
        //        {
        //            case "NEW":
        //                using (OracleConnection con = new OracleConnection(conn))
        //                {
        //                    con.Open();

        //                    #region กรณีที่มีการต่ออายุสัญญา
        //                    if (cboModify_type.Value + "" == "1")
        //                    {
        //                        //ให้ปรับสถานะสัญญาเก่าเป็น 'N'
        //                        strSql = @"UPDATE TCONTRACT SET CACTIVE=:CACTIVE WHERE SCONTRACTID=:SCONTRACTID";

        //                        SystemFunction.SQLExecuteNonQuery(conn, strSql);

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
        //                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "N";
        //                            com.ExecuteNonQuery();
        //                        }

        //                        //Back Up ข้อมูลรถของสัญญาเก่าเก็บไว้ แล้วลบออกจาก Table TCONTRACT_TRUCK เพื่อให้เป็นไปตามเงื่อนไขที่ว่า รถหนี่งคันอยู่ในแค่หนึ่งสัญญา
        //                        DataView dvContractInfo = (DataView)sdsContractTruck.Select(new DataSourceSelectArguments());
        //                        foreach (DataRow dr in dvContractInfo.ToTable().Rows)
        //                        {
        //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

        //                            strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY(HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT, REF_SCONTRACTID,REF_PREVIOUSCONTRACT) (SELECT :HISTORY_ID,SYSDATE,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,SYSDATE,:SREJECT,REF_SCONTRACTID,REF_PREVIOUSCONTRACT FROM TCONTRACT_TRUCK WHERE
        //                            SCONTRACTID=:NCONTRACTID AND STRUCKID=:STRUCKID AND STRAILERID=:STRAILERID)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":HISTORY_ID", OracleType.Number).Value = "" + sGenID;
        //                                com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
        //                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"] != "" ? "" + dr["STRAILERID"] : null;
        //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        //ลบข้อมูลรถของสัญญาเก่าทิ้ง
        //                        strSql = @"DELETE FROM TCONTRACT_TRUCK WHERE SCONTRACTID=:NCONTRACTID";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + Session["SCONTRACTID"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion

        //                    #region รายละเอียดสัญญา

        //                    sGenID = CommonFunction.Gen_ID(con, "SELECT SCONTRACTID FROM (SELECT SCONTRACTID+0 As SCONTRACTID FROM TCONTRACT ORDER BY SCONTRACTID DESC)  WHERE ROWNUM <= 1");

        //                    CONTRACTID = sGenID;
        //                    Session["SCONTRACTID"] = sGenID;
        //                    Session["SVENDORID"] = "" + cmbVendor.Value;

        //                    string CGROUPCONTRACT = CommonFunction.Get_Value(conn, @"SELECT CGROUP FROM TCONTRACTTYPE WHERE SCONTRACTTYPEID='" + cmbContractType.Value + "'");
        //                    foreach (DataRow dr in ssContractPlant.Rows) PLNT += "," + dr["STERMINALID"];//ส่งเป็น Para เข้าไป

        //                    strSql = string.Format(@"INSERT INTO TCONTRACT(SCONTRACTID,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,SCONT_PLNT,RENEWFROM) Values ('{0}','{1}','{2}','{3}',{4},{5},'{6}','{7}','{8}',SYSDATE,'{9}',SYSDATE,'{9}','{10}','{11}','{12}','{13}',{14})"
        //                        , sGenID
        //                        , cmbContractType.Value + ""
        //                        , txtContractNo.Text
        //                        , cmbVendor.Value + ""
        //                        , "to_date('" + Convert.ToDateTime(dteStart.Text).ToString("MM/dd/yyyy", new CultureInfo("en-US")) + "','mm/dd/yyyy')"
        //                        , (dteEnd.Text == null ? "null" : "to_date('" + Convert.ToDateTime(dteEnd.Text).ToString("MM/dd/yyyy", new CultureInfo("en-US")) + "','mm/dd/yyyy')")
        //                        , ssContractTruck.Select("CSTANDBY='N'").Length
        //                        , mmoRemark.Text
        //                        , rblCACTIVE.Value
        //                        , Session["UserID"] + ""
        //                        , CGROUPCONTRACT
        //                        , cblProcurement.Value + ""
        //                        , cboMainPlant.Value + ""
        //                        , PLNT
        //                        , (cboModify_type.Value + "" == "1" ? "'" + txtRenewFrom.Text + "'" : "null")
        //                        );

        //                    SystemFunction.SQLExecuteNonQuery(conn, strSql);

        //                    #endregion

        //                    #region หลักประกันสัญญา

        //                    foreach (DataRow dr in ssGuarantee.Rows)
        //                    {
        //                        sGenID = CommonFunction.Gen_ID(con, TCONTRACT_GUARANTEES);

        //                        strSql = @"INSERT INTO TCONTRACT_GUARANTEES(NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) Values
        //                                (:NCGID,:NCONTRACTID,:SBOOKNO,:SGUARANTEETYPE,:NAMOUNT,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SREMARK,:GUARANTEESTYPE_ID)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":NCGID", OracleType.Number).Value = sGenID;
        //                            com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                            com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
        //                            com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
        //                            com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
        //                            com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion

        //                    #region เอกสารสำคัญของสัญญา

        //                    foreach (DataRow dr in ssContractDoc.Rows)
        //                    {
        //                        sGenID = CommonFunction.Gen_ID(con, TCONTRACT_DOC);

        //                        strSql = @"INSERT INTO TCONTRACT_DOC(SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH) Values
        //                                (:SCONTRACTID,:SDOCID,:SDOCVERSION,:SDOCTYPE,:SFILENAME,:SSYSFILENAME,:SDESCRIPTION,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SPATH)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                            com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = sGenID;
        //                            com.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = "1";
        //                            com.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "" + dr["SDOCTYPE"];
        //                            com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + dr["SFILENAME"];
        //                            com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + dr["SSYSFILENAME"];
        //                            com.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "" + dr["SDESCRIPTION"];
        //                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SPATH", OracleType.VarChar).Value = "" + dr["SPATH"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion

        //                    #region ขอบเขตการขนส่ง

        //                    if (cboMainPlant.Value + "" != "")//คลังต้นทางหลัก
        //                    {
        //                        strSql = @"INSERT INTO TCONTRACT_PLANT(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
        //                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                            com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + cboMainPlant.Value;
        //                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
        //                            com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "1";
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }

        //                    foreach (DataRow dr in ssContractPlant.Rows)//คลังต้นทางอื่นๆ
        //                    {
        //                        strSql = @"INSERT INTO TCONTRACT_PLANT(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
        //                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                            com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
        //                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + Session["UserID"];
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
        //                            com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion

        //                    #region กลุ่มผลิตภัณฑ์

        //                    foreach (DataRow dr in ssGProduct.Rows)
        //                    {

        //                        strSql = @"INSERT INTO TCONTRACT_GPRODUCT(SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE) Values
        //                                (:SCONTRACTID,:SPRODUCTTYPEID,:SPRODUCTTYPENAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                            com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                            com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion

        //                    #region รายชื่อรถในสัญญา

        //                    foreach (DataRow dr in ssContractTruck.Rows)
        //                    {

        //                        strSql = @"INSERT INTO TCONTRACT_TRUCK(SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
        //                                    (:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                            com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
        //                            com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + dr["CSTANDBY"];
        //                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                            com.ExecuteNonQuery();
        //                        }

        //                        //บันทึกเปลี่ยนแปลงข้อมูลสัญญาใน TTRUCK
        //                        strSql = @"UPDATE TTRUCK SET SCONTRACTID=:SCONTRACTID,SCONTRACTTYPE=:SCONTRACTTYPE WHERE STRUCKID IN (:STRUCKID,:STRAILERID)";

        //                        using (OracleCommand com = new OracleCommand(strSql, con))
        //                        {
        //                            com.Parameters.Clear();
        //                            com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                            com.Parameters.Add(":SCONTRACTTYPE", OracleType.VarChar).Value = cmbContractType.Value;
        //                            com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                            com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
        //                            com.ExecuteNonQuery();
        //                        }
        //                    }
        //                    #endregion


        //                }
        //                break;
        //            case "EDIT":
        //                using (OracleConnection con = new OracleConnection(conn))
        //                {
        //                    con.Open();

        //                    CONTRACTID = "" + Session["SCONTRACTID"];

        //                    #region รายละเอียดสัญญา
        //                    //เก็บประวัติ
        //                    strSql = @"INSERT INTO TCONTRACT_HIS(SCONTRACTID,NITEM,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,SDETAIL,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,COIL,CGAS,SCONT_PLNT,RENEWFROM) 
        //                        (SELECT SCONTRACTID,(SELECT NVL(MAX(NITEM),0)+1 FROM TCONTRACT_HIS WHERE SCONTRACTID=:SCONTRACTID) NITEM,SCONTRACTTYPEID,SCONTRACTNO,SVENDORID,DBEGIN,DEND,SDETAIL,NTRUCK,SREMARK,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CGROUPCONTRACT,CSPACIALCONTRAC,STERMINALID,COIL,CGAS,SCONT_PLNT,RENEWFROM FROM TCONTRACT WHERE SCONTRACTID=:SCONTRACTID)";

        //                    using (OracleCommand com = new OracleCommand(strSql, con))
        //                    {
        //                        com.Parameters.Clear();
        //                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                        com.ExecuteNonQuery();
        //                    }

        //                    strSql = @"UPDATE TCONTRACT SET SCONTRACTTYPEID=:SCONTRACTTYPEID,SCONTRACTNO=:SCONTRACTNO,SVENDORID=:SVENDORID,DBEGIN=:DBEGIN,DEND=:DEND,NTRUCK=:NTRUCK,SREMARK=:SREMARK,CACTIVE=:CACTIVE,DUPDATE=SYSDATE,SUPDATE=:SUPDATE,CGROUPCONTRACT=:CGROUPCONTRACT,CSPACIALCONTRAC=:CSPACIALCONTRAC,STERMINALID=:STERMINALID,SCONT_PLNT=:SCONT_PLNT WHERE SCONTRACTID=:SCONTRACTID";

        //                    using (OracleCommand com = new OracleCommand(strSql, con))
        //                    {
        //                        com.Parameters.Clear();
        //                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                        com.Parameters.Add(":SCONTRACTTYPEID", OracleType.VarChar).Value = cmbContractType.Value;
        //                        com.Parameters.Add(":SCONTRACTNO", OracleType.VarChar).Value = txtContractNo.Text;
        //                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cmbVendor.Value;
        //                        com.Parameters.Add(":DBEGIN", OracleType.DateTime).Value = Convert.ToDateTime(dteStart.Text);
        //                        if (dteEnd.Text == null) com.Parameters.Add(":DEND", OracleType.DateTime).Value = OracleDateTime.Null;
        //                        else com.Parameters.Add(":DEND", OracleType.DateTime).Value = Convert.ToDateTime(dteEnd.Text);
        //                        com.Parameters.Add(":NTRUCK", OracleType.Number).Value = ssContractTruck.Select("CSTANDBY='N'").Length;
        //                        com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = mmoRemark.Text + "";
        //                        com.Parameters.Add(":CACTIVE", OracleType.Char).Value = rblCACTIVE.Value + "";
        //                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + Session["UserID"];
        //                        com.Parameters.Add(":CGROUPCONTRACT", OracleType.Char).Value = CommonFunction.Get_Value(conn, @"SELECt CGROUP FROM TCONTRACTTYPE WHERE SCONTRACTTYPEID='" + cmbContractType.Value + "'") + "";
        //                        com.Parameters.Add(":CSPACIALCONTRAC", OracleType.Char).Value = cblProcurement.Value + "";
        //                        com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cboMainPlant.Value + "";
        //                        foreach (DataRow dr in ssContractPlant.Rows) { PLNT += "," + dr["STERMINALID"]; }
        //                        com.Parameters.Add(":SCONT_PLNT", OracleType.VarChar).Value = PLNT + "";
        //                        com.ExecuteNonQuery();
        //                    }
        //                    #endregion

        //                    #region หลักประกันสัญญา

        //                    foreach (DataRow dr in ssGuarantee.Rows)
        //                    {
        //                        if (dr["CNEW"] + "" == "1")
        //                        {
        //                            sGenID = CommonFunction.Gen_ID(con, TCONTRACT_GUARANTEES);

        //                            strSql = @"INSERT INTO TCONTRACT_GUARANTEES(NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) Values
        //                                (:NCGID,:NCONTRACTID,:SBOOKNO,:SGUARANTEETYPE,:NAMOUNT,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SREMARK,:GUARANTEESTYPE_ID)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":NCGID", OracleType.Number).Value = sGenID;
        //                                com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                                com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
        //                                com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
        //                                com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
        //                                com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            //กรณีที่มีการเปลี่ยนแปลงให้เก็บประวัติไว้ใน TCONTRACT_GUARANTEES_HIS ก่อน แล้วค่อยอัพเดทข้อมูลที่มีการเปลี่ยนแปลง
        //                            if (dr["CCHANGE"] + "" == "1" && dr["CDEL"] + "" == "0")
        //                            {
        //                                //Step 1
        //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GUARANTEES_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

        //                                strSql = @"INSERT INTO TCONTRACT_GUARANTEES_HIS(HISTORYID,DREJECT,SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) 
        //                                            (SELECT :HISTORYID,SYSDATE,:SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID FROM TCONTRACT_GUARANTEES WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID)";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
        //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.ExecuteNonQuery();
        //                                }

        //                                //Step 2
        //                                strSql = @"UPDATE TCONTRACT_GUARANTEES SET SBOOKNO=:SBOOKNO,SGUARANTEETYPE=:SGUARANTEETYPE,NAMOUNT=:NAMOUNT,DUPDATE=SYSDATE,SUPDATE=:SUPDATE,SREMARK=:SREMARK,GUARANTEESTYPE_ID=:GUARANTEESTYPE_ID WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
        //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SBOOKNO", OracleType.VarChar).Value = "" + dr["SBOOKNO"];
        //                                    com.Parameters.Add(":SGUARANTEETYPE", OracleType.VarChar).Value = "" + dr["SGUARANTEETYPE"];
        //                                    com.Parameters.Add(":NAMOUNT", OracleType.Number).Value = "" + dr["NAMOUNT"];
        //                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = "" + dr["SREMARK"];
        //                                    com.Parameters.Add(":GUARANTEESTYPE_ID", OracleType.Char).Value = "" + dr["GUARANTEESTYPE_ID"];
        //                                    com.ExecuteNonQuery();
        //                                }
        //                            }

        //                            //กรณีที่มีการลบข้อมูลทิ้งให้เก็บประวัติไว้ใน TCONTRACT_GUARANTEES_HIS ก่อน แล้วลบรายการใน TCONTRACT_GUARANTEES ทิ้ง
        //                            if (dr["CDEL"] + "" == "1")
        //                            {
        //                                //Step 1
        //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GUARANTEES_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

        //                                strSql = @"INSERT INTO TCONTRACT_GUARANTEES_HIS(HISTORYID,DREJECT,SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID) 
        //                                            (SELECT :HISTORYID,SYSDATE,:SREJECT,NCGID,NCONTRACTID,SBOOKNO,SGUARANTEETYPE,NAMOUNT,DCREATE,SCREATE,DUPDATE,SUPDATE,SREMARK,GUARANTEESTYPE_ID FROM TCONTRACT_GUARANTEES WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID)";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
        //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.ExecuteNonQuery();
        //                                }

        //                                //Step 2
        //                                strSql = @"DELETE FROM TCONTRACT_GUARANTEES WHERE NCGID=:NCGID AND NCONTRACTID=:NCONTRACTID";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":NCGID", OracleType.Number).Value = "" + dr["NCGID"];
        //                                    com.Parameters.Add(":NCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
        //                                    com.ExecuteNonQuery();
        //                                }
        //                            }

        //                        }
        //                    }
        //                    #endregion

        //                    #region เอกสารสำคัญของสัญญา

        //                    foreach (DataRow dr in ssContractDoc.Rows)
        //                    {
        //                        if (dr["CNEW"] + "" == "1")
        //                        {
        //                            sGenID = CommonFunction.Gen_ID(con, TCONTRACT_DOC);

        //                            strSql = @"INSERT INTO TCONTRACT_DOC(SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH) Values
        //                                        (:SCONTRACTID,:SDOCID,:SDOCVERSION,:SDOCTYPE,:SFILENAME,:SSYSFILENAME,:SDESCRIPTION,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:SPATH)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = sGenID;
        //                                com.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = "1";
        //                                com.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "" + dr["SDOCTYPE"];
        //                                com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "" + dr["SFILENAME"];
        //                                com.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "" + dr["SSYSFILENAME"];
        //                                com.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "" + dr["SDESCRIPTION"];
        //                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SPATH", OracleType.VarChar).Value = "" + dr["SPATH"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        else if (dr["CDEL"] + "" == "1")//กรณีที่มีการลบรายการทิ้งให้เก็บประวัติก่อน แล้วลบรายการเก่าทิ้ง
        //                        {
        //                            //Step 1
        //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_DOC_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

        //                            strSql = @"INSERT INTO TCONTRACT_DOC_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH)
        //                                        (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH FROM TCONTRACT_DOC WHERE SCONTRACTID=:SCONTRACTID AND SDOCID=:SDOCID)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["SCONTRACTID"];
        //                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = "" + dr["SDOCID"];
        //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.ExecuteNonQuery();
        //                            }

        //                            //Step 2
        //                            strSql = @"DELETE FROM TCONTRACT_DOC WHERE SCONTRACTID=:SCONTRACTID AND SDOCID=:SDOCID";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["SCONTRACTID"];
        //                                com.Parameters.Add(":SDOCID", OracleType.VarChar).Value = "" + dr["SDOCID"];
        //                                com.ExecuteNonQuery();
        //                            }

        //                        }
        //                    }
        //                    #endregion

        //                    #region ขอบเขตการขนส่ง

        //                    //คลังต้นทางหลัก
        //                    string mainplant = "";
        //                    mainplant = CommonFunction.Get_Value(conn, "SELECT STERMINALID FROM TCONTRACT_PLANT WHERE SCONTRACTID='" + CONTRACTID + "' AND CMAIN_PLANT='1'") + "";
        //                    if ((cboMainPlant.Value + "" != "" || mainplant + "" != "") && cboMainPlant.Value + "" != mainplant + "")
        //                    {
        //                        if (mainplant + "" != "")
        //                        {
        //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_PLANT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

        //                            strSql = @"INSERT INTO TCONTRACT_PLANT_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) 
        //                                    (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT FROM TCONTRACT_PLANT WHERE SCONTRACTID=:SCONTRACTID AND CMAIN_PLANT='1')";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.ExecuteNonQuery();
        //                            }

        //                            strSql = @"DELETE FROM TCONTRACT_PLANT WHERE SCONTRACTID=:SCONTRACTID AND CMAIN_PLANT='1'";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }

        //                        if (cboMainPlant.Value + "" != "")
        //                        {
        //                            strSql = @"INSERT INTO TCONTRACT_PLANT(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
        //                                    (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = cboMainPlant.Value + "";
        //                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "1";
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                    }

        //                    foreach (DataRow dr in ssContractPlant.Rows)//คลังต้นทางอื่นๆ
        //                    {
        //                        if (dr["CNEW"] + "" == "1")
        //                        {
        //                            strSql = @"INSERT INTO TCONTRACT_PLANT(SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) Values
        //                                        (:SCONTRACTID,:STERMINALID,:CACTIVE,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CMAIN_PLANT)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
        //                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        else if (dr["CDEL"] + "" == "1")
        //                        {
        //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_PLANT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");

        //                            strSql = @"INSERT INTO TCONTRACT_PLANT_HIS(HISTORYID,DREJECT,SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT) 
        //                                        (SELECT :HISTORYID,SYSDATE,:SREJECT,SCONTRACTID,STERMINALID,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,CMAIN_PLANT FROM TCONTRACT_PLANT WHERE SCONTRACTID=:SCONTRACTID AND STERMINALID=:STERMINALID AND CMAIN_PLANT=:CMAIN_PLANT)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
        //                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
        //                                com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.ExecuteNonQuery();
        //                            }

        //                            strSql = @"DELETE FROM TCONTRACT_PLANT WHERE SCONTRACTID=:SCONTRACTID AND STERMINALID=:STERMINALID AND CMAIN_PLANT=:CMAIN_PLANT";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":STERMINALID", OracleType.VarChar).Value = "" + dr["STERMINALID"];
        //                                com.Parameters.Add(":CMAIN_PLANT", OracleType.Char).Value = "" + dr["CMAIN_PLANT"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                    #region กลุ่มผลิตภัณฑ์

        //                    foreach (DataRow dr in ssGProduct.Rows)
        //                    {
        //                        if (dr["CNEW"] + "" == "1")
        //                        {
        //                            strSql = @"INSERT INTO TCONTRACT_GPRODUCT(SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE) Values
        //                                (:SCONTRACTID,:SPRODUCTTYPEID,:SPRODUCTTYPENAME,SYSDATE,:SCREATE,SYSDATE,:SUPDATE,:CACTIVE)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                                com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        else
        //                        {
        //                            //กรณีที่มีการเปลี่ยนแปลงให้เก็บประวัติไว้ใน TCONTRACT_GPRODUCT_HIS ก่อน แล้วค่อยอัพเดทข้อมูลที่มีการเปลี่ยนแปลง
        //                            if (dr["CCHANGE"] + "" == "1" && dr["CDEL"] + "" == "0")
        //                            {
        //                                //Step 1
        //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GPRODUCT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");
        //                                strSql = @"INSERT INTO TCONTRACT_GPRODUCT_HIS(HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DREJECT,SREJECT) 
        //                                            (SELECT :HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,SYSDATE,:SREJECT FROM TCONTRACT_GPRODUCT WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID)";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.ExecuteNonQuery();
        //                                }

        //                                //Step 2
        //                                strSql = @"UPDATE TCONTRACT_GPRODUCT SET SPRODUCTTYPEID=:SPRODUCTTYPEID,SPRODUCTTYPENAME=:SPRODUCTTYPENAME,DUPDATE=SYSDATE,SUPDATE=:SUPDATE
        //                                ,CACTIVE=:CACTIVE WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = CONTRACTID;
        //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                                    com.Parameters.Add(":SPRODUCTTYPENAME", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPENAME"];
        //                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.Parameters.Add(":CACTIVE", OracleType.Char).Value = "" + dr["CACTIVE"];
        //                                    com.ExecuteNonQuery();
        //                                }
        //                            }

        //                            //กรณีที่มีการลบข้อมูลทิ้งให้เก็บประวัติไว้ใน TCONTRACT_GPRODUCT_HIS ก่อน แล้วลบรายการใน TCONTRACT_GUARANTEES ทิ้ง
        //                            if (dr["CDEL"] + "" == "1")
        //                            {
        //                                //Step 1
        //                                sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORYID FROM (SELECT HISTORYID+0 As HISTORYID FROM TCONTRACT_GPRODUCT_HIS ORDER BY HISTORYID DESC)  WHERE ROWNUM <= 1");
        //                                strSql = @"INSERT INTO TCONTRACT_GPRODUCT_HIS(HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,DREJECT,SREJECT) 
        //                                            (SELECT :HISTORYID,SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE,SYSDATE,:SREJECT FROM TCONTRACT_GPRODUCT WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID)";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":HISTORYID", OracleType.Number).Value = "" + sGenID;
        //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.ExecuteNonQuery();
        //                                }

        //                                //Step 2
        //                                strSql = @"DELETE FROM TCONTRACT_GPRODUCT WHERE SCONTRACTID=:SCONTRACTID AND SPRODUCTTYPEID=:SPRODUCTTYPEID";

        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":SCONTRACTID", OracleType.VarChar).Value = "" + dr["NCONTRACTID"];
        //                                    com.Parameters.Add(":SPRODUCTTYPEID", OracleType.VarChar).Value = "" + dr["SPRODUCTTYPEID"];
        //                                    com.ExecuteNonQuery();
        //                                }
        //                            }

        //                        }
        //                    }
        //                    #endregion

        //                    #region รายชื่อรถในสัญญา

        //                    foreach (DataRow dr in ssContractTruck.Rows)
        //                    {
        //                        if (dr["CNEW"] + "" == "1")
        //                        {
        //                            strSql = @"INSERT INTO TCONTRACT_TRUCK(SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE) Values
        //                                        (:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,SYSDATE,:SCREATE,SYSDATE,:SUPDATE)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
        //                                com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + dr["CSTANDBY"];
        //                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                        else if (dr["CDEL"] + "" == "1")//กรณีที่มีการลบรายการทิ้งให้เก็บประวัติก่อน แล้วลบรายการเก่าทิ้ง
        //                        {
        //                            //Step 1
        //                            sGenID = CommonFunction.Gen_ID(con, "SELECT HISTORY_ID FROM (SELECT HISTORY_ID+0 As HISTORY_ID FROM TCONTRACT_TRUCK_HISTORY ORDER BY HISTORY_ID DESC)  WHERE ROWNUM <= 1");

        //                            strSql = @"INSERT INTO TCONTRACT_TRUCK_HISTORY(HISTORY_ID,DHISTORY,SCONTRACTID,STRUCKID,STRAILERID,CSTANDBY,DCREATE,SCREATE,DUPDATE,SUPDATE,DREJECT,SREJECT) Values (:HISTORY_ID,SYSDATE,:SCONTRACTID,:STRUCKID,:STRAILERID,:CSTANDBY,:DCREATE,:SCREATE,:DUPDATE,:SUPDATE,SYSDATE,:SREJECT)";

        //                            DataTable dtCTRUCK = CommonFunction.Get_Data(conn, @"SELECT * FROM TCONTRACT_TRUCK WHERE SCONTRACTID='" + CONTRACTID + "' AND STRUCKID='" + "" + dr["STRUCKID"] + "' AND '.'||STRAILERID='" + "." + dr["STRAILERID"] + "'");

        //                            foreach (DataRow r in dtCTRUCK.Rows)
        //                            {
        //                                using (OracleCommand com = new OracleCommand(strSql, con))
        //                                {
        //                                    com.Parameters.Clear();
        //                                    com.Parameters.Add(":HISTORY_ID", OracleType.VarChar).Value = sGenID;
        //                                    com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + r["SCONTRACTID"];
        //                                    com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + r["STRUCKID"];
        //                                    com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + r["STRAILERID"];
        //                                    com.Parameters.Add(":CSTANDBY", OracleType.Char).Value = "" + r["CSTANDBY"];
        //                                    com.Parameters.Add(":DCREATE", OracleType.DateTime).Value = Convert.ToDateTime("" + r["DCREATE"]);
        //                                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "" + r["SCREATE"];
        //                                    com.Parameters.Add(":DUPDATE", OracleType.DateTime).Value = Convert.ToDateTime("" + r["DUPDATE"]);
        //                                    com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = "" + r["SUPDATE"];
        //                                    com.Parameters.Add(":SREJECT", OracleType.VarChar).Value = Session["UserID"].ToString();
        //                                    com.ExecuteNonQuery();
        //                                }
        //                                break;
        //                            }

        //                            //Step 2
        //                            strSql = @"DELETE FROM TCONTRACT_TRUCK WHERE SCONTRACTID=:SCONTRACTID AND STRUCKID=:STRUCKID AND '.'||STRAILERID='.'||:STRAILERID";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = "" + dr["NCONTRACTID"];
        //                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
        //                                com.ExecuteNonQuery();
        //                            }

        //                            //บันทึกเปลี่ยนแปลงข้อมูลสัญญาใน TTRUCK
        //                            strSql = @"UPDATE TTRUCK SET SCONTRACTID=:SCONTRACTID,SCONTRACTTYPE=:SCONTRACTTYPE WHERE STRUCKID IN (:STRUCKID,:STRAILERID)";

        //                            using (OracleCommand com = new OracleCommand(strSql, con))
        //                            {
        //                                com.Parameters.Clear();
        //                                com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = CONTRACTID;
        //                                com.Parameters.Add(":SCONTRACTTYPE", OracleType.VarChar).Value = "" + cmbContractType.Value;
        //                                com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = "" + dr["STRUCKID"];
        //                                com.Parameters.Add(":STRAILERID", OracleType.VarChar).Value = "" + dr["STRAILERID"];
        //                                com.ExecuteNonQuery();
        //                            }
        //                        }
        //                    }
        //                    #endregion
        //                }
        //                break;
        //                }

        #endregion
        ClearSession();
        string regisNo;
        foreach (DataRow item in ssContractTruck.Rows)
        {
            C_grp.Veh_no = item["VEH_NO"] + string.Empty;
            C_grp.contractType = cblProcurement.Value + string.Empty;
            if (item["CNEW"] + "" == "1" && item["CDEL"] + "" == "0")
            {
                C_grp.AddClassgrpToTruck();
                C_grp.AddClassToSap();
                regisNo = LOG.Instance.GetRegisNoByTruckId(item["STRUCKID"] + string.Empty).Rows[0][0].ToString();
                LOG.Instance.SaveLog("นำรถเข้าสัญญา", regisNo, Session["UserID"].ToString(), "<span class=\"green\">" + txtContract.Text + "</span>", "");
                if (!string.IsNullOrEmpty(item["TU_NO"].ToString()))
                {
                    LOG.Instance.SaveLog("นำรถเข้าสัญญา", item["TU_NO"] + string.Empty, Session["UserID"] + string.Empty, "<span class=\"green\">" + txtContract.Text + "</span>", "");
                }
                Mess += "บันทึกรถทะเบียน " + C_grp.Veh_no + " เข้าสัญญาสำเร็จ<br/>";
            }
            else if (item["CNEW"] + "" == "0" && item["CDEL"] + "" == "1")
            {
                C_grp.CancelClassgrpToTruck();
                C_grp.AddClassToSap();
                regisNo = LOG.Instance.GetRegisNoByTruckId(item["STRUCKID"] + string.Empty).Rows[0][0].ToString();
                LOG.Instance.SaveLog("นำรถออกจากสัญญา", regisNo, Session["UserID"] + string.Empty, "<span class=\"red\">" + txtContract.Text + "</span>", "");
                if (!string.IsNullOrEmpty(item["TU_NO"].ToString()))
                {
                    LOG.Instance.SaveLog("นำรถออกจากสัญญา", item["TU_NO"] + string.Empty, Session["UserID"] + string.Empty, "<span class=\"red\">" + txtContract.Text + "</span>", "");
                }
                Mess += "บันทึกรถทะเบียน " + C_grp.Veh_no + " ออกสัญญาสำเร็จ<br/>";
            }

            DataTable dtAfter = ContractBLL.Instance.AfterContractSelect(item["STRUCKID"] + string.Empty, CONTRACTID);
            this.InsertContract(string.Empty, I_ConTractNAME, dtAfter.Rows[0]["P_CACTIVE"].ToString(), cmbContractType.Value.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, CONTRACTID);
            this.InsertContractSub(CONTRACTID, cmbVendor.Value + string.Empty, ddlGroups.Value + string.Empty, I_ConTractNAME);
            //this.UpdateContract(CONTRACTID, ddlGroups.SelectedIndex + string.Empty);
            this.TruckUpdateContract(dtAfter.Rows[0]["P_CHASSIS"].ToString(), dtAfter.Rows[0]["P_LICENSE_PLATE"].ToString(), dtAfter.Rows[0]["P_LICENSE_PLATE_BACK"].ToString(), dtAfter.Rows[0]["P_CONTRACTS_NUM"].ToString(), dtAfter.Rows[0]["P_CONTRACTS_UPDATE"].ToString(), dtAfter.Rows[0]["P_CONTRACTS_ENDING"].ToString(), dtAfter.Rows[0]["P_CARRIER_CODE"].ToString(), dtAfter.Rows[0]["P_CONTRACT_NAME"].ToString(), dtAfter.Rows[0]["P_TU_NAME"].ToString(), dtAfter.Rows[0]["P_TU_MODEL"].ToString(), dtAfter.Rows[0]["P_TRUCK_CATEGORY"].ToString());
            this.VehicleStatus(dtAfter.Rows[0]["P_LICENSE_PLATE"].ToString(), cmbVendor.Value + string.Empty, I_ConTractNAME, dtAfter.Rows[0]["P_SPLANTCODE"].ToString());
        }

        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Mess + "',function(){ window.location='contract_info.aspx';});");

    }

    #endregion

    #region ClearSession
    void ClearSession()
    {
        string[] sSession = new string[] { "SMODE", "SCONTRACTID", "SVENDORID", "ssContractDoc", "ssGuarantee", "ssContractPlant", "ssGProduct", "ssContractTruck" };
        foreach (string arr in sSession)
            Session.Remove(arr);
    }
    #endregion

    #region PrepareDataTable
    protected DataTable PrepareDataTable(string ss_name, string IsDel, string contractid, params string[] fields)
    {
        DataTable dtData = new DataTable();
        if (Session[ss_name] != null)
        {
            dtData = (DataTable)Session[ss_name];
            if (IsDel == "0")
            {
                foreach (DataRow drDel in dtData.Select("NCONTRACTID='" + contractid + "'"))
                {
                    int idx = dtData.Rows.IndexOf(drDel);
                    dtData.Rows[idx].Delete();
                }
            }
        }
        else
        {
            foreach (string field_type in fields)
            {
                dtData.Columns.Add(field_type + "");
            }
        }
        return dtData;
    }
    #endregion

    #endregion

    #region Upload
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
        }
        catch (Exception ex)
        {
            alertFail("ไม่สามารถ upload file ได้ :" + ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Vendor" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("CONTRACT");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
    }
    #endregion

    protected void ddlWordGroup_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

    }
    class CreateClassgrp
    {
        public string Veh_no { get; set; }
        private string tu_strailerid { get; set; }
        public string TU_NO
        {
            set
            {
                tu_strailerid = value;
            }
            get
            {
                if (tu_strailerid != null)
                {
                    return tu_strailerid = "/" + tu_strailerid;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string contractType { get; set; }
        public string contractName { get; set; }
        private string ntotalcapacity { get; set; }
        public string GproductType { get; set; }

        public string GenClassGrp()
        {
            try
            {
                string TypeVehicle = string.Empty;
                string GenClassGrp = string.Empty;
                DataTable dt = CarBLL.Instance.CreateClassgrp(this.Veh_no, contractType);
                GenClassGrp = dt.Rows[0]["CLASSGRP"].ToString();
                #region เลือกชนิดรถ
                //if (string.Equals(dt.Rows[0]["SCARTYPEID"].ToString(), "0") || string.Equals(dt.Rows[0]["SCARTYPEID"].ToString(), "3") || string.Equals(dt.Rows[0]["SCARTYPEID"].ToString(), "4"))
                //{
                //    GenClassGrp = "T";
                //}
                //else
                //{
                //    GenClassGrp = "V";
                //}
                //#endregion
                //#region ประเภทรถ

                ////GenClassGrp += GproductType;
                //GenClassGrp +="O";         
                //#endregion
                //#region ประเภทสัญญา
                //if (string.Equals(contractType, "N"))
                //{
                //    GenClassGrp += "C";
                //}
                //else 
                //{
                //    GenClassGrp += "S";
                //}

                //#endregion
                //#region ความจุรถ / 1000
                //this.ntotalcapacity = (string.IsNullOrEmpty(dt.Rows[0]["NTOTALCAPACITY"].ToString()) ? "0" : dt.Rows[0]["NTOTALCAPACITY"].ToString());
                //GenClassGrp += CheckCapacityLessValue(this.ntotalcapacity);

                //#endregion
                //#region คำนวณเพลารับน้ำหนัก
                //if(!string.IsNullOrEmpty(dt.Rows[0]["NSHAFTDRIVEN"].ToString()))
                //{
                //    if(string.Equals(dt.Rows[0]["NSHAFTDRIVEN"].ToString(), "1"))
                //    {
                //        GenClassGrp += "0";
                //    }
                //    else if(string.Equals(dt.Rows[0]["NSHAFTDRIVEN"].ToString(), "0"))
                //    {
                //        GenClassGrp += "0";
                //    }
                //    else
                //    {
                //        GenClassGrp += "1";
                //    }
                //}
                //else
                //{
                //    GenClassGrp += "0";
                //}
                //#endregion
                //#region อายุรถ
                //try
                //{
                //    if (!string.IsNullOrEmpty(dt.Rows[0]["DREGISTER"] + string.Empty))
                //    {
                //        DateTime dd = Convert.ToDateTime(dt.Rows[0]["DREGISTER"] + string.Empty);
                //        int DateRegis = DateTime.Now.Year - dd.Year;
                //        if (DateRegis <= 8)
                //        {
                //            GenClassGrp += "0";
                //        }
                //        else
                //        {
                //            GenClassGrp += "1";
                //        }
                //    }


                //}
                //catch (Exception ex)
                //{

                //    throw;
                //}


                #endregion

                return GenClassGrp;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        //คำนวณถ้าน้อยกว่าพันให้เติมศูนย์ด้านหน้า
        private string CheckCapacityLessValue(string ntotalcapacity)
        {
            string CheckLess = string.Empty;
            if (int.Parse(ntotalcapacity) > 0)
                CheckLess = (int.Parse(ntotalcapacity) / 1000).ToString();
            if (CheckLess.Length <= 1)
            {
                return "0" + CheckLess;
            }
            else
            {
                return CheckLess;
            }
        }
        public void AddClassgrpToTruck()
        {
            try
            {
                CarBLL.Instance.UpdateClassGrp(GenClassGrp(), this.Veh_no);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public void CancelClassgrpToTruck()
        {
            try
            {
                CarBLL.Instance.UpdateClassGrp(string.Empty, this.Veh_no);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public void AddClassToSap()
        {
            try
            {
                SAP_SYNCOUT_SI sap = new SAP_SYNCOUT_SI();
                sap.VEH_NO = this.Veh_no;
                sap.UDT_Vehicle_SYNC_OUT_SI();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public string PupupAddClassgrp()
        {
            string Classgrp = GenClassGrp();
            string msg = @"นำรถทะเบียน " + this.Veh_no + this.TU_NO + "<br/>เข้าในสัญญา " + this.contractName + "<br/>ความจุ " + this.ntotalcapacity + " ClassGroup :" + Classgrp + "<br/>อัพเดตข้อมูล SAP และยืนยันนำรถเข้าสัญญา <br/>กดบันทึก ด้านล่าง";
            return msg;

        }
        public string PupupCancelClassgrp()
        {
            string Classgrp = GenClassGrp();
            string msg = @"นำรถทะเบียน " + this.Veh_no + this.TU_NO + "<br/>ออกจากสัญญา " + this.contractName + "<br/>ความจุ " + this.ntotalcapacity + " ClassGroup : <br/>อัพเดตข้อมูล SAP และยืนยันนำรถออกจากสัญญา <br/>กดบันทึก ด้านล่าง";
            return msg;

        }

    }

    protected void cblProcurement_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cblProcurement.Value.ToString() == "Y")
        {
            txtNtruck.Value = string.Empty;
            txtReserve.Value = string.Empty;

            //txtNtruck.ReadOnly = true;
            //txtReserve.ReadOnly = true;

            //txtNtruck.Enabled = false;
            //txtReserve.Enabled = false;

            txtNtruck.ReadOnly = false;
            txtReserve.ReadOnly = false;

            txtNtruck.Enabled = true;
            txtReserve.Enabled = true;
        }
        else
        {
            txtNtruck.Value = string.Empty;
            txtReserve.Value = string.Empty;

            txtNtruck.ReadOnly = false;
            txtReserve.ReadOnly = false;

            txtNtruck.Enabled = true;
            txtReserve.Enabled = true;
        }
    }

    private void InsertContract(string I_CONTRACT_ID, string I_CONTRACT_NAME, string I_CONTRACT_STATUS, string I_CONTRACT_TYPE_ID, string I_CONTRACT_PROD_ID, string I_REMARK_AREA, string I_REMARK_FROM, string I_REMARK_TO, string I_CONTRACT_TMS_ID)
    {
        var YourModel = new { CONTRACT_ID = I_CONTRACT_ID, CONTRACT_NAME = I_CONTRACT_NAME, CONTRACT_STATUS = I_CONTRACT_STATUS, CONTRACT_TYPE_ID = I_CONTRACT_TYPE_ID, CONTRACT_PROD_ID = I_CONTRACT_PROD_ID, REMARK_AREA = I_REMARK_AREA, REMARK_FROM = I_REMARK_FROM, REMARK_TO = I_REMARK_TO, CONTRACT_TMS_ID = I_CONTRACT_TMS_ID };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_contract/InsertContract", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }
    private void InsertContractSub(string I_ID, string I_CARRIER_CODE, string I_CONTRACTS_TMS_ID, string I_CONTRACTS_NUM)
    {
        var YourModel = new { ID = I_ID, CARRIER_CODE = I_CARRIER_CODE, CONTRACTS_NUM = I_CONTRACTS_NUM, CONTRACT_TMS_ID = I_CONTRACTS_TMS_ID };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_contract_sub/InsertContractSub", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }

    private void UpdateContract(string I_ID, string I_GROUPNAME)
    {
        var YourModel = new { ID = I_ID, GROUPNAME = I_GROUPNAME };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_contract/UpdateContract", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }

    private void TruckUpdateContract(string I_CHASSIS, string I_LICENSE_PLATE, string I_LICENSE_PLATE_BACK, string I_CONTRACT_NUM, string I_CONTRACT_UPDATE, string I_CONTRACT_ENDING, string I_CARIER_CODE, string I_CONTRACT_NAME, string I_TU_NAME, string I_TU_MODEL, string I_TRUCK_CATEGORY)
    {
        var YourModel = new { CHASSIS = I_CHASSIS, LICENSE_PLATE = I_LICENSE_PLATE, LICENSE_PLATE_BACK = I_LICENSE_PLATE_BACK, CONTRACT_NUM = I_CONTRACT_NUM, CONTRACT_UPDATE = I_CONTRACT_UPDATE, CONTRACT_ENDING = I_CONTRACT_ENDING, CARIER_CODE = I_CARIER_CODE, CONTRACT_NAME = I_CONTRACT_NAME, TU_NAME = I_TU_NAME, TU_MODEL = I_TU_MODEL, TRUCK_CATEGORY = I_TRUCK_CATEGORY };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_truck/TruckUpdateContract", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }
    private void VehicleStatus(string I_LICENSE, string I_CARIERCODE, string I_CONTRACTNUM, string I_SPLANTCODE)
    {
        var YourModel = new { LICENSE = I_LICENSE, CARIERCODE = I_CARIERCODE, I_CONTRACTNUM = I_CONTRACTNUM, SPLANTCODE = I_SPLANTCODE };
        using (var client = new HttpClient())
        {
            var json = JsonConvert.SerializeObject(YourModel);
            var stringContent = new StringContent(json, Encoding.UTF8,
                        "application/json");
            var model = client
                               .PostAsync("http://hermes.pttdigital.com/tms_vehiclestatus/VehicleStatus", stringContent)
                               .Result;
            if (model.StatusCode == HttpStatusCode.OK)
            {
                //ผ่าน
            }

        }
    }
}