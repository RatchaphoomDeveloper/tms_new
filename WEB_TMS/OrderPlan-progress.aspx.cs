﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Web.Configuration;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;
using System.Data.OleDb;
using System.Text;

public partial class OrderPlan_progress : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private const string sPathSave = "/ORDER_PLAN/{0}/{1}/";
    double def_Double;
    //private static DataTable dt = new DataTable();
    public static string progress = "1;0";
    //private static string UserID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //UserID = Session["UserID"] + "";
        if (!IsPostBack)
        {
            LogUser("58", "R", "เปิดหน้า บันทึกจัดรายการขนส่งให้ลูกค้า", "");
            //Update_PLAN_Capacity("000", "20");
            popupControl.ShowOnPageLoad = true;
            //Set progress bar 1 and 0 percent
            progress = "1;0.0";

        }
    }

    private string GetExcelSheet(OleDbConnection excelcon, string _tableName)
    {
        // หาชื่อ sheet
        string _exxcelsheet = "";
        DataRowCollection _ar_dr = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" }).Rows;
        foreach (DataRow _dr in _ar_dr)
        {
            if (!_dr["TABLE_NAME"].ToString().IndexOf("$").Equals(-1) && !string.IsNullOrEmpty("" + _dr["TABLE_NAME"]) && _dr["TABLE_NAME"].ToString().Equals(_tableName + "$"))
            {
                _exxcelsheet = _dr["TABLE_NAME"].ToString();
                break;
            }
        }
        return _exxcelsheet;
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        DateTime outdate;

        switch (paras[0])
        {
            case "InsertExceltoDatabase":


                // ที่อยู่ไฟล์
                string sDataUpload = Session["sDataUpload"] + "";

                //เช็คไฟล์
                string connectionString = "";
                #region
                if (Path.GetExtension(sDataUpload).ToLower().Equals(".xlsx") || Path.GetExtension(sDataUpload).ToLower().Equals(".xls"))
                {
                    connectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + sDataUpload + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
                }




                DataSet ds = new DataSet();
                using (OleDbConnection con = new OleDbConnection(connectionString))
                {
                    con.Open();

                    //หาชื่อของวีทว่าชื่ออะไรเพื่อใช้ในการ Select
                    DataRowCollection _ar_dr = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows;
                    string _sheetname = _ar_dr[0]["TABLE_NAME"].ToString();
                    //string query = string.Format("SELECT * FROM [{0}]", _sheetname);
                    string excellSheet = GetExcelSheet(con, "TMS");


                    if (string.IsNullOrEmpty(excellSheet))
                    {
                        xcpn.JSProperties["cpError"] = "กรุณาตรวจสอบTemplate Excel <br>เนื่องจากชื่อ Sheet ไม่ได้ใช่ชื่อ TMS ";
                        return;
                    }


                    string query = string.Format("SELECT * FROM [{0}]", excellSheet);
                    OleDbDataAdapter adapter = new OleDbDataAdapter(query, con);
                    adapter.Fill(ds);


                    //dt.Clear();
                    DataTable dt = new DataTable();

                    if (ds.Tables[0].Rows.Count >= 0)
                    {
                        dt = ds.Tables[0];

                        //เช็คว่ามี Row ไหนที่มีค่าว่างทั้งหมด ให้นำออก
                        //dt = dt.Rows.Cast<DataRow>().Where(rows => !rows.ItemArray.All(field => field is System.DBNull)).CopyToDataTable();
                        dt = dt.Rows.Cast<DataRow>().Where(rows => !rows.ItemArray.All(field => field is System.DBNull || ((field as string) != null ? (field as string).Trim() == string.Empty : false))).CopyToDataTable();


                        // dt = dt.Rows.Cast<DataRow>().Where(rows => !rows.ItemArray.All(field => field is System.DBNull ||  string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();


                        Session["DT"] = dt;

                        //Progress 
                        double nprogress = 0.0;
                        double ncount = Convert.ToDouble(dt.Rows.Count);

                        //เช็คว่า Column ตรงกันหรือไม่
                        string[] arrayColumn = { "Company", "Contract Number", "Job Type", "Delivery Date", "Time Window", "Delivery Number" };
                        string sMsgLossColumn = "";
                        //if (arrayColumn.Length != dt.Columns.Count)
                        //{
                        foreach (string scolname in arrayColumn)
                        {
                            string colname = !string.IsNullOrEmpty(scolname) ? scolname.Trim() : "";

                            if (dt.Columns.IndexOf(colname) == -1)
                            {
                                sMsgLossColumn += "," + scolname;
                            }
                        }
                        if (sMsgLossColumn != "")
                        {
                            xcpn.JSProperties["cpError"] = "กรุณาตรวจสอบTemplate Excel <br>เนื่องจากขาดคอลัมป์ <b>" + sMsgLossColumn.Remove(0, 1) + "</b>!";
                            return;
                        }

                        // } //return;

                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {



                            //Progress bar 1
                            nprogress = Convert.ToDouble(i) / ncount * 100;
                            progress = "1;" + nprogress;
                        }

                    }

                    con.Close();
                }
                #endregion

                AddDataToDataBase();
                LogUser("58", "I", "บันทึก จัดรายการขนส่งให้ลูกค้า", "");
                break;
        }

    }

    protected void timerCallback_Callback(object source, DevExpress.Web.ASPxCallback.CallbackEventArgs e)
    {
        e.Result = progress;

    }

    public void AddDataToDataBase()
    {
        //Progress 
        int Sess = 0, Fail = 0;

        DataTable dt = Session["DT"] as DataTable;

        double nprogress = 0.0;
        double ncount = Convert.ToDouble(dt.Rows.Count);

        string UserID = Session["UserID"] + "";


        if (dt.Rows.Count > 0)
        {
            string ERROR = "";

            //DataTable dtVendor = CommonFunction.Get_Data(conn, "SELECT SVENDORID,SABBREVIATION FROM TVENDOR");
            DataTable dtCONTRACT = CommonFunction.Get_Data(conn, @"SELECT TCR.SCONTRACTID,TCR.SCONTRACTNO, NVL(TCR.STERMINALID, REPLACE( SUBSTR(TCR.SCONT_PLNT,0,5),',','' )) as  STERMINALID,TEM.SABBREVIATION ,TCR.SVENDORID,VEN.SABBREVIATION as VENDORNAME
FROM TCONTRACT TCR
LEFT JOIN TTERMINAL TEM
ON NVL(TCR.STERMINALID, REPLACE( SUBSTR(TCR.SCONT_PLNT,0,5),',','' ))  = TEM.STERMINALID
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID = TCR.SVENDORID");
            string StrSave = "";
            string sImporttype = Request.QueryString["v"] + "";
            //Progress
            nprogress = 0.0;

            //StringBuilder DetailAlertList = new StringBuilder("<table class='tbDetailAlert' width='100%' border='1' align='center' cellpadding='0' cellspacing='0'");
            //DetailAlertList.Append("<tr><th width='15%'>ผู้ขนส่ง</th><th width='13%'>สัญญา</th><th width='13%'>Type(1 = งานขนส่งภายในวัน, 2 = งานขนส่งล่วงหน้า)</th><th width='10%'>วันที่</th><th width='10%'>กะการขนส่ง</th><th width='10%'>DO</th><th>Remark</th></tr>");

            StringBuilder DetailAlertList = new StringBuilder("<table class='tbDetailAlert' width='100%' border='1' align='center' cellpadding='0' cellspacing='0'");
            DetailAlertList.Append("<tr><th width='15%'>Company</th><th width='13%'>Contract Number</th><th width='13%'>Job Type</th><th width='10%'>Delivery Date</th><th width='10%'>Time Window</th><th width='10%'>Delivery Number</th><th>Remark</th></tr>");

            //string FormateError = "<tr><td>&nbsp;{0}&nbsp;</td><td>&nbsp;{1}&nbsp;</td><td>&nbsp;{2}&nbsp;</td><td>&nbsp;{3}&nbsp;</td><td>&nbsp;{4}&nbsp;</td><td>&nbsp;{5}&nbsp;</td><td>&nbsp;{6}&nbsp;</td><td>&nbsp;{7}&nbsp;</td><td>&nbsp;{8}&nbsp;</td></tr>";

            #region วนเก็ต DO จาก Tdeliverysap ไปใส่ใน Tdelivery
            //string LOOP_DO = "";
            //for (int i = 0; i < dt.Rows.Count; i++)
            //{
            //    string PAD_DO = !string.IsNullOrEmpty(dt.Rows[i][5] + "") ? (dt.Rows[i][5] + "").Trim().PadLeft(10, '0') : "";
            //    LOOP_DO += ",'" + PAD_DO + "'";

            //}

            //LOOP_DO = !string.IsNullOrEmpty(LOOP_DO.Replace(",", "")) ? ("(" + LOOP_DO.Remove(0, 1) + ")") : "";

            //if (!string.IsNullOrEmpty(LOOP_DO.Replace(",", "")))
            //{
            //    TDS_TO_TD(LOOP_DO);//Tdeliverysap ไปใส่ใน Tdelivery
            //}
            #endregion
            DateTime Dtemp = new DateTime();

            if (sImporttype == "A")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string deliveryPad = !string.IsNullOrEmpty(dt.Rows[i][5] + "") ? (dt.Rows[i][5] + "").Trim().PadLeft(10, '0') : "";

                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                    bool ISGET_DO = false;
                    //เป็นตัวใช้เช็คว่าจะหา DO มาจากไหน ถ้ากรณีที่ไม่มีใน TDELIVERY จะไปดูจาก TDELIVERY_SAP ว่ามีไหม ถ้ามีจะเอามาจาก TDELIVERY_SAP แต่ถ้าไม่มีจะเอามาจาก TRANS_DO
                    if (dtDELIVERY.Rows.Count > 0)
                    {

                    }
                    else
                    {

                        if (deliveryPad.Contains(";"))
                        {

                        }
                        else
                        {
                            ISGET_DO = true;
                            DO_GET(deliveryPad);
                            dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                        }
                    }


                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? (DateTime.TryParse(dt.Rows[i][3] + "", out Dtemp) ? Dtemp.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "") : "";
                    string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? (DateTime.TryParse(dt.Rows[i][3] + "", out Dtemp) ? Dtemp.ToString("dd/MM/yyyy") : "") : "";
                    string SVENDORID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[4] + "" : "";
                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[3] + "" : "";
                    // string sSupplyPlant = "";
                    //if (dtDELIVERY.Rows.Count > 0)
                    //{
                    //    sSupplyPlant = GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "");
                    //}

                    STERMINALID = /*sSupplyPlant; (sSupplyPlant != "") ? sSupplyPlant :*/ (dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SPLNT_CODE"] + "") ? dtDELIVERY.Rows[0]["SPLNT_CODE"] + "" : GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "")) : "");
                    STERMINALNAME = CommonFunction.Get_Value(conn, "SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID='" + STERMINALID + "'");
                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["OTHER"] + "") ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null") : "null";



                    string SVENDORNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[5] + "" : "";
                    string SCONTRACTNO = dt.Rows[i][1] + "";
                    string ORDERTYPE = dt.Rows[i][2] + "";
                    string NWINDOWTIMEID = dt.Rows[i][4] + "";
                    string SDELIVERYNO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "") ? (dtDELIVERY.Rows[0]["DELIVERY_NO"] + "").Trim().PadLeft(10, '0') : "null") : "";
                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SHIP_TO"] + "") ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : SystemFunction.Get_SHIPTO_FROM_TBORDERPLAN(SDELIVERYNO)) : SystemFunction.Get_SHIPTO_FROM_TBORDERPLAN(SDELIVERYNO);

                    int? LATEDAY = null;
                    string LATETIME = string.Empty;
                    int? CheckLate = null;
                    if (dt.Columns.Contains("Destination Date") && dt.Columns.Contains("Destination Time"))
                    {
                        if (dt.Columns.Contains("Destination Date"))
                        {
                            LATEDAY = !string.IsNullOrEmpty((dt.Rows[i]["Destination Date"] + string.Empty).Trim()) ? (int?)int.Parse((dt.Rows[i]["Destination Date"] + string.Empty).Trim()) : (int?)null;
                        }
                        if (dt.Columns.Contains("Destination Time"))
                        {
                            LATETIME = !string.IsNullOrEmpty((dt.Rows[i]["Destination Time"] + string.Empty).Trim()) ? dt.Rows[i]["Destination Time"] + string.Empty : string.Empty;
                        }
                        if (!string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(NWINDOWTIMEID) && (LATEDAY == null || string.IsNullOrEmpty(LATETIME)))
                        {
                            DataTable TWINDOWTIME = CommonFunction.Get_Data(conn, "SELECT * FROM TWINDOWTIME TWT where TWT.STERMINALID =  '" + STERMINALID + "' AND TWT.NLINE = " + NWINDOWTIMEID + " ");

                            if (TWINDOWTIME != null && TWINDOWTIME.Rows.Count > 0)
                            {
                                if (LATEDAY == null)
                                {
                                    LATEDAY = int.Parse(TWINDOWTIME.Rows[0].IsNull("LATEDAY") ? "0" : TWINDOWTIME.Rows[0]["LATEDAY"] + string.Empty);

                                }
                                if (string.IsNullOrEmpty(LATETIME))
                                {
                                    LATETIME = TWINDOWTIME.Rows[0]["LATETIME"] + string.Empty;
                                }

                            }

                        }

                        if (dt.Columns.Contains("Check Late Delivery"))
                        {
                            CheckLate = !string.IsNullOrEmpty((dt.Rows[i]["Check Late Delivery"] + string.Empty).Trim()) ? (int?)int.Parse((dt.Rows[i]["Check Late Delivery"] + string.Empty).Trim()) : (int?)null;
                        }
                    }

                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + SDELIVERYNO + @"'  AND NVL(CACTIVE,'Y') = 'Y'");
                    //ถ้ามีในระบบให้อัพเดท
                    if (Check > 0)
                    {
                        StrSave = @"
                                    UPDATE TBL_ORDERPLAN
                                    SET    SVENDORID     = '" + SVENDORID + @"',
                                           SABBREVIATION = '" + SVENDORNAME + @"',
                                           SCONTRACTID   = '" + SCONTRACTID + @"',
                                           SCONTRACTNO   = '" + SCONTRACTNO + @"',
                                           ORDERTYPE     = '" + ORDERTYPE + @"',
                                           DDELIVERY    = TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
                                           NWINDOWTIMEID = " + NWINDOWTIMEID + @",
                                           --DATE_CREATE   = SYSDATE,
                                           --SCREATE       = '" + UserID + @"',
                                           DATE_UPDATE   = SYSDATE,
                                           SUPDATE       = '" + UserID + @"',
                                           STERMINALID    = '" + STERMINALID + @"',
                                           STERMINALNAME  = '" + STERMINALNAME + @"',
                                           NVALUE       = " + CAPACITY + @",
                                           SHIP_TO = '" + SHIP_TO + @"',
                                           LATEDAY = " + (LATEDAY == null ? (object)"NULL" : LATEDAY) + @",
                                           LATETIME = '" + LATETIME + @"',
                                           CHECKLATE = " + (CheckLate == null ? (object)"NULL" : (object)CheckLate) + @"
                                    WHERE  SDELIVERYNO   = '" + SDELIVERYNO + @"' AND CACTIVE = 'Y'
                        ";

                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }
                    else //ถ้าไม่มีในระบบให้ Save
                    {
                        StrSave = @"
INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DDELIVERY,NWINDOWTIMEID, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,NVALUE,CACTIVE,SHIP_TO,LATEDAY,LATETIME,CHECKLATE) 
VALUES ( '" + genID() + @"',
 '" + SVENDORID + @"',
 '" + SVENDORNAME + @"',
 '" + SCONTRACTID + @"',
 '" + SCONTRACTNO + @"',
 '" + ORDERTYPE + @"',
 TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
 " + NWINDOWTIMEID + @",
 '" + SDELIVERYNO + @"',
 SYSDATE,
 '" + UserID + @"',
 '" + STERMINALID + @"',
 '" + STERMINALNAME + @"',
 " + CAPACITY + @",
   'Y' ,
'" + SHIP_TO + @"'
," + (LATEDAY == null ? 0 : LATEDAY) + @"
,'" + LATETIME + @"'
," + (CheckLate == null ? (object)"NULL" : (object)CheckLate) + @")";
                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }
                    //เช็คว่ามี string ที่เป็น SDELIVERYNO ไหม ถ้ามีไม่ต้องเซฟ
                    if (ERROR.Contains(SDELIVERYNO))
                    {
                        //DetailAlertList.Append(ERROR);
                        Fail += 1;
                    }
                    else
                    {
                        AddTODB(StrSave);
                        Update_PLAN_Capacity(SDELIVERYNO, CAPACITY);
                        Sess += 1;
                    }


                    //Progress bar 2
                    nprogress = Convert.ToDouble(i) / ncount * 100;
                    progress = "2;" + nprogress;
                }

            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string deliveryPad = !string.IsNullOrEmpty(dt.Rows[i][5] + "") ? (dt.Rows[i][5] + "").Trim().PadLeft(10, '0') : "";
                    DataTable dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");

                    //เป็นตัวใช้เช็คว่าจะหา DO มาจากไหน ถ้ากรณีที่ไม่มีใน TDELIVERY จะไปดูจาก TDELIVERY_SAP ว่ามีไหม ถ้ามีจะเอามาจาก TDELIVERY_SAP แต่ถ้าไม่มีจะเอามาจาก TRANS_DO
                    bool ISGET_DO = false;
                    if (dtDELIVERY.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        if (deliveryPad.Contains(";"))
                        {

                        }
                        else
                        {
                            ISGET_DO = true;
                            DO_GET(deliveryPad);
                            dtDELIVERY = CommonFunction.Get_Data(conn, "SELECT DELIVERY_NO,OTHER,SHIP_TO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + CommonFunction.ReplaceInjection(deliveryPad) + "'");
                        }
                    }

                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? (DateTime.TryParse(dt.Rows[i][3] + "", out Dtemp) ? Dtemp.ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "") : "";
                    string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? (DateTime.TryParse(dt.Rows[i][3] + "", out Dtemp) ? Dtemp.ToString("dd/MM/yyyy") : "") : "";

                    //string sDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "";
                    //string eDate = !string.IsNullOrEmpty(dt.Rows[i][3] + "") ? DateTime.Parse(dt.Rows[i][3] + "").ToString("dd/MM/yyyy") : "";
                    string SVENDORID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[4] + "" : "";
                    string SCONTRACTID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[0] + "" : "";
                    string STERMINALID = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[2] + "" : "";
                    string STERMINALNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + "'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[3] + "" : "";
                    //string sSupplyPlant = "";
                    //if (dtDELIVERY.Rows.Count > 0)
                    //{
                    //    sSupplyPlant = GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "");
                    //}
                    //STERMINALID = sSupplyPlant;// (sSupplyPlant != "")? sSupplyPlant : STERMINALID; 
                    STERMINALID = /*sSupplyPlant; (sSupplyPlant != "") ? sSupplyPlant :*/ (dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SPLNT_CODE"] + "") ? dtDELIVERY.Rows[0]["SPLNT_CODE"] + "" : GetSupplyPlant(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "")) : "");
                    STERMINALNAME = CommonFunction.Get_Value(conn, "SELECT SABBREVIATION FROM TTERMINAL WHERE STERMINALID='" + STERMINALID + "'");
                    string CAPACITY = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["OTHER"] + "") ? dtDELIVERY.Rows[0]["OTHER"] + "" : "null") : "null";
                    string SHIP_TO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["SHIP_TO"] + "") ? dtDELIVERY.Rows[0]["SHIP_TO"] + "" : "null") : "null";

                    string SVENDORNAME = dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'").Count() > 0 ? dtCONTRACT.Select("SCONTRACTNO = '" + CommonFunction.ReplaceInjection(dt.Rows[i][1] + "") + @"'")[0].ItemArray[5] + "" : "";
                    string SCONTRACTNO = dt.Rows[i][1] + "";
                    string ORDERTYPE = dt.Rows[i][2] + "";
                    string NWINDOWTIMEID = dt.Rows[i][4] + "";
                    string SDELIVERYNO = dtDELIVERY.Rows.Count > 0 ? (!string.IsNullOrEmpty(dtDELIVERY.Rows[0]["DELIVERY_NO"] + "") ? (dtDELIVERY.Rows[0]["DELIVERY_NO"] + "").Trim().PadLeft(10, '0') : "null") : "";
                    int Check = CommonFunction.Count_Value(conn, "SELECT ORDERID,SDELIVERYNO FROM TBL_ORDERPLAN WHERE SDELIVERYNO = '" + SDELIVERYNO + @"' AND NVL(CACTIVE,'Y') = 'Y'");
                    //ถ้ามีในระบบไม่อัพเดท และให้ StrSave ค่าว่าง ไม่งั้นจะเกิด Error กรณี StrSave ไม่ได้โดนเคลียค่ามา
                    if (Check > 0)
                    {
                        StrSave = "";
                    }
                    else //ถ้าไม่มีในระบบให้ Save
                    {
                        StrSave = @"
INSERT INTO TBL_ORDERPLAN (ORDERID,SVENDORID,SABBREVIATION,SCONTRACTID,SCONTRACTNO,ORDERTYPE,DDELIVERY,NWINDOWTIMEID, SDELIVERYNO,DATE_CREATE, SCREATE,STERMINALID,STERMINALNAME,NVALUE,CACTIVE,SHIP_TO) 
VALUES ( '" + genID() + @"',
 '" + SVENDORID + @"',
 '" + SVENDORNAME + @"',
 '" + SCONTRACTID + @"',
 '" + SCONTRACTNO + @"',
 '" + ORDERTYPE + @"',
 TO_DATE('" + CommonFunction.ReplaceInjection(sDate) + @"','dd/MM/yyyy'),
 " + NWINDOWTIMEID + @",
 '" + SDELIVERYNO + @"',
 SYSDATE,
 '" + UserID + @"',
 '" + STERMINALID + @"',
 '" + STERMINALNAME + @"',
 " + CAPACITY + @",
  'Y',
 '" + SHIP_TO + @"')";

                        ERROR += CheckError(SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, STERMINALID, STERMINALNAME, SHIP_TO, CAPACITY, SDELIVERYNO, ORDERTYPE, NWINDOWTIMEID, eDate, deliveryPad, ISGET_DO, Dtemp);

                    }

                    //เช็คว่ามี string ที่เป็น SDELIVERYNO ไหม ถ้ามีไม่ต้องเซฟ
                    if (ERROR.Contains(deliveryPad))
                    {
                        // DetailAlertList.Append(ERROR);
                        Fail += 1;
                    }
                    else
                    {
                        AddTODB(StrSave);
                        Update_PLAN_Capacity(SDELIVERYNO, CAPACITY);//อัพเดทความจุของแผน
                        Sess += 1;
                    }



                    //Progress bar 2
                    nprogress = Convert.ToDouble(i) / ncount * 100;
                    progress = "2;" + nprogress;
                }


            }
            string strSess = "ผ่านจำนวน " + Sess + " รายการ <Br/> ไม่ผ่านจำนวน " + Fail + " รายการ <Br/>";
            //lblFail.Text = Fail + string.Empty;
            if (string.IsNullOrEmpty(ERROR))
            {

                //CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "')");
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Complete + "',function(){window.location='OrderPlan.aspx?str=1';});");
                return;
            }
            else
            {
                DetailAlertList.Append(strSess);
                DetailAlertList.Append(ERROR);
                // CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + ERROR + "')");
                //lblDetail.Text = ERROR;
                if (DetailAlertList.Length > 0)
                {
                    DetailAlertList.Append("</table>");
                    xcpn.JSProperties["cpError"] = DetailAlertList.ToString();
                    return;
                }
            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่พบช้อมูลเอกสารแนบใหม่อีกครั้ง')");
            return;
        }

    }

    private void AddTODB(string strQuery)
    {
        if (!string.IsNullOrEmpty(strQuery))
        {
            using (OracleConnection con = new OracleConnection(conn))
            {

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }

                con.Close();

            }
        }
    }

    private string genID()
    {
        string sql = @"SELECT ORDERID FROM TBL_ORDERPLAN WHERE  ORDERID LIKE '%" + DateTime.Now.ToString("yyMMdd") + "%' ORDER BY ORDERID DESC";
        string Result = "";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, sql);
        if (dt.Rows.Count > 0)
        {
            string Num_run = (dt.Rows[0]["ORDERID"] + "").Remove(0, 7);
            if (Num_run != "9999")
            {
                string sss = dt.Rows[0]["ORDERID"] + "";

                if (!string.IsNullOrEmpty(sss))
                {
                    sss = sss.Substring(7, 4);

                    Result = DateTime.Now.ToString("yyMMdd") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
                }
                else
                {

                }
            }
            else
            {
                string sql_new = @"SELECT ORDERID FROM TBL_ORDERPLAN WHERE  ORDERID LIKE '%" + DateTime.Now.AddDays(1).ToString("yyMMdd") + "%' ORDER BY ORDERID DESC";
                DataTable New_DATE = CommonFunction.Get_Data(conn, sql_new);

                if (New_DATE.Rows.Count > 0)
                {
                    string sss = New_DATE.Rows[0]["ORDERID"] + "";

                    if (!string.IsNullOrEmpty(sss))
                    {
                        sss = sss.Substring(7, 4);

                        Result = DateTime.Now.AddDays(1).ToString("yyMMdd") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
                    }
                    else
                    {

                    }
                }
                else
                {
                    Result = DateTime.Now.AddDays(1).ToString("yyMMdd") + "-0001";
                }
            }
        }
        else
        {

            Result = DateTime.Now.ToString("yyMMdd") + "-0001";
        }

        return Result;
    }

    private string CheckError(string SVENDORID, string SVENDORNAME, string SCONTRACTID, string SCONTRACTNO, string STERMINALID, string STERMINALNAME, string SHIP_TO, string CAPACITY, string SDELIVERYNO, string ORDERTYPE, string NWINDOWTIMEID, string sDate, string DO_SHOW, bool IS_GETDO, DateTime Dtemp)
    {
        string Result = "";

        //if (!string.IsNullOrEmpty(SDELIVERYNO))
        //{

        string ErrorText = "";
        string ErrorRemark = "";

        //ใส่ข้อมูล
        if (!string.IsNullOrEmpty(SVENDORNAME))
        {
            ErrorText += "<td>" + SVENDORNAME + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        if (!string.IsNullOrEmpty(SCONTRACTNO))
        {
            ErrorText += "<td>" + SCONTRACTNO + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        if (!string.IsNullOrEmpty(ORDERTYPE))
        {
            string ORDERTYPENAME = "";
            switch (ORDERTYPE)
            {
                case "1": ORDERTYPENAME = "งานขนส่งภายในวัน";
                    break;
                case "2": ORDERTYPENAME = "งานขนส่งล่วงหน้า";
                    break;
            }

            ErrorText += "<td>" + ORDERTYPENAME + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        if (!string.IsNullOrEmpty(sDate))
        {
            ErrorText += "<td>" + sDate + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        if (!string.IsNullOrEmpty(NWINDOWTIMEID))
        {
            ErrorText += "<td>" + NWINDOWTIMEID + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        if (!string.IsNullOrEmpty(DO_SHOW))
        {
            ErrorText += "<td>" + DO_SHOW + "</td>";
        }
        else
        {
            ErrorText += "<td>&nbsp;</td>";
        }

        //ใส่ Remark
        if (string.IsNullOrEmpty(SVENDORID))
        {
            ErrorRemark += "- ไม่พบบริษัท " + SVENDORNAME + " ในระบบ<br>";
        }

        if (string.IsNullOrEmpty(SCONTRACTID))
        {
            ErrorRemark += "- ไม่พบเลขที่สัญญา " + SCONTRACTNO + " ในระบบ<br>";
        }

        if (string.IsNullOrEmpty(ORDERTYPE))
        {
            ErrorRemark += "- ไม่ได้กำหนดประเภทการขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(sDate))
        {
            ErrorRemark += "- ไม่ได้กำหนดวันที่ขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(NWINDOWTIMEID))
        {
            ErrorRemark += "- ไม่ได้กำหนดกะการขนส่ง<br>";
        }

        if (string.IsNullOrEmpty(SDELIVERYNO))
        {
            if (IS_GETDO)
            {
                ErrorRemark += "- ไม่สามารถดึง DO จาก TRANS_DO เนื่องจากไม่พบเลข DO " + DO_SHOW + "<br>";
            }
            else
            {
                ErrorRemark += "- ไม่พบเลข DO " + DO_SHOW + "<br>";
            }
        }

        if (string.IsNullOrEmpty(STERMINALID))
        {
            ErrorRemark += "- ไม่พบคลังจากสัญญา<br>";
        }

        //if (!string.IsNullOrEmpty(SHIP_TO))
        //{
        //    ErrorText += " ไม่พบ" + SHIP_TO + "ในระบบ";
        //}

        //if (string.IsNullOrEmpty(CAPACITY))
        //{
        //    ErrorRemark += "- ไม่มีปริมาตรการขนส่งในระบบ</br>";
        //}

        DataTable dtFIFO = CommonFunction.Get_Data(conn, @"SELECT PCL.NPLANID,PCL.SDELIVERYNO,PS.CFIFO
FROM  TPLANSCHEDULELIST PCL 
LEFT JOIN TPLANSCHEDULE PS
ON PS.NPLANID = PCL.NPLANID
WHERE PCL.SDELIVERYNO = '" + CommonFunction.ReplaceInjection(DO_SHOW) + "' AND NVL(PS.CFIFO,'xxx') = '1' AND NVL(PCL.CACTIVE,'1') = '1'");
        if (dtFIFO.Rows.Count > 0)
        {
            ErrorRemark += "- " + DO_SHOW + " เลข DO นี้ได้มีการจัดแผนแบบ FIFO แล้ว<br>";
        }

        DataTable dtMAP_DO = CommonFunction.Get_Data(conn, @"SELECT LPAD(M_NO ,10,0) as M_NO FROM  TBMAP_DO WHERE  LPAD(M_NO ,10,0)  = '" + CommonFunction.ReplaceInjection(DO_SHOW) + "'");
        if (dtMAP_DO.Rows.Count > 0)
        {
            ErrorRemark += "- " + DO_SHOW + " เลข DO นี้ได้มีการจัดแผนจากทาง Iterminal แล้ว<br>";
        }




        //ถ้ามี error นำไปใส่ Result เพื่อรีเทินข้อความกลับ
        if (!string.IsNullOrEmpty(ErrorText))
        {
            if (!string.IsNullOrEmpty(ErrorRemark))
            {
                //เช็คว่าถ้ามี SVENDORID SCONTRACTID STERMINALID SDELIVERYNO ถือว่าไม่ Error
                //if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO) && dtFIFO.Rows.Count == 0)
                if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO))
                {
                    if (dtFIFO.Rows.Count > 0)
                    {
                        Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                    }
                    else if (dtMAP_DO.Rows.Count > 0)
                    {
                        Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                    }
                    else
                    {
                        Result = "<tr>" + ErrorText + "<td>&nbsp;</td></tr>";
                    }
                }
                else
                {
                    Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
                }
            }
            else
            {
                //เช็คว่าถ้ามี SVENDORID SCONTRACTID STERMINALID SDELIVERYNO ถือว่าไม่ Error
                if (!string.IsNullOrEmpty(SVENDORID) && !string.IsNullOrEmpty(SCONTRACTID) && !string.IsNullOrEmpty(STERMINALID) && !string.IsNullOrEmpty(SDELIVERYNO))
                {
                    Result = "";
                }
                else
                {
                    Result = "<tr>" + ErrorText + "<td>&nbsp;</td></tr>";
                }
            }
        }
        //}

        if (Dtemp < DateTime.Today || Dtemp > DateTime.Now.AddDays(6))
        {
            //ErrorText = DO_SHOW;
            ErrorRemark = "- " + Dtemp.ToString("dd/MM/yyyy") + " วันที่ไม่อยู่ในช่วงที่สามารถเพิ่ม DO ได้<br>";
            Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
        }

        string QUERY_MULTIDROP = "SELECT SDELIVERYNO,NPLANID FROM TPLANSCHEDULELIST WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "' AND CACTIVE = '1'";
        DataTable dt_ChkMultiDrop = CommonFunction.Get_Data(conn, QUERY_MULTIDROP);
        if (dt_ChkMultiDrop.Rows.Count > 0)
        {
            //ErrorText = DO_SHOW;
            ErrorRemark = "- DO " + SDELIVERYNO + " ถูกจัดแผนแล้วไม่สามารถแก้ไขได้<br>";
            Result = "<tr>" + ErrorText + "<td>" + ErrorRemark + "</td></tr>";
        }
        return Result;
    }

    protected string GetSupplyPlant(string DO_NO)
    {
        DataTable _dt = CommonFunction.Get_Data(conn, @"SELECT TBL.DO_NO ,TBL.PLNT_CODE ,TBL_MAXSTATUS.STATUS 
FROM(
    select DO_NO, PLNT_CODE ,(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END)  STATUS
    from TRANS_DO 
    WHERE 1=1 AND DO_NO='" + DO_NO + @"'
    GROUP BY DO_NO, PLNT_CODE ,(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END) 
) TBL
LEFT JOIN (
    select DO_NO, PLNT_CODE  ,MAX(CASE WHEN NVL(STATUS,'N')='N' THEN 0 ELSE 9 END)  STATUS
    from TRANS_DO 
    WHERE 1=1 and  DO_NO='" + DO_NO + @"'
    GROUP BY DO_NO, PLNT_CODE
) TBL_MAXSTATUS ON TBL.DO_NO=TBL_MAXSTATUS.DO_NO AND TBL.STATUS=TBL_MAXSTATUS.STATUS AND  TBL.PLNT_CODE =TBL_MAXSTATUS.PLNT_CODE
WHERE 1=1 AND TBL.DO_NO='" + DO_NO + @"'");
        //เช็คจาก TRANDO ถ้ามีให้เอามา
        if (_dt.Rows.Count > 0)
        {
            DO_NO = _dt.Rows[0]["PLNT_CODE"] + "";
        }
        else //ถ้าไม่มีให้เอามาจาก TDEIVERYSAP
        {
            string SAP_PLY = "SELECT DELIVERY_NO,SUPPLY_PLANT FROM TDELIVERY_SAP WHERE DELIVERY_NO = '" + DO_NO + "'";
            _dt = CommonFunction.Get_Data(conn, SAP_PLY);
            if (_dt.Rows.Count > 0)
            {
                DO_NO = _dt.Rows[0]["SUPPLY_PLANT"] + "";
            }
            else//ถ้าไม่มีให้เอามาจาก TDEIVERY
            {
                SAP_PLY = "SELECT DELIVERY_NO,SPLNT_CODE FROM TDELIVERY WHERE DELIVERY_NO = '" + DO_NO + "'";
                _dt = CommonFunction.Get_Data(conn, SAP_PLY);
                if (_dt.Rows.Count > 0)
                {
                    DO_NO = _dt.Rows[0]["SPLNT_CODE"] + "";
                }
                else
                {
                    DO_NO = "";
                }
            }
        }
        return DO_NO;
    }



    //void SaveImportDetail(string filename, string genname)
    //{
    //    using (OracleConnection con = new OracleConnection(sql))
    //    {
    //        string strsql = "INSERT INTO TIMPORTFILE(NIMPORTID,SFILENAME,SGENFILENAME,SUSERNAME,SCREATE,DCREATE) VALUES (:NIMPORTID,:SFILENAME,:SGENFILENAME,:SUSERNAME,:SCREATE,SYSDATE)";
    //        using (OracleCommand com = new OracleCommand(strsql, con))
    //        {
    //            string genImportID = CommonFunction.Gen_ID(con, "SELECT NIMPORTID FROM (SELECT NIMPORTID FROM TIMPORTFILE ORDER BY NIMPORTID DESC) WHERE ROWNUM <= 1");
    //            com.Parameters.Clear();
    //            com.Parameters.Add(":NIMPORTID", OracleType.Number).Value = Convert.ToInt32(genImportID);
    //            com.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = filename;
    //            com.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = genname;
    //            com.Parameters.Add(":SUSERNAME", OracleType.VarChar).Value = Session["UserName"] + "";
    //            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
    //            com.ExecuteNonQuery();
    //        }
    //    }
    //}

    private void DO_GET(string SDELIVERYNO)
    {

        string QUERY = @"BEGIN GET_DO_ORDERPLAN('" + SDELIVERYNO + "'); END;";
        AddTODB(QUERY);
        //if (!string.IsNullOrEmpty(QUERY))
        //{
        //    using (OracleConnection con = new OracleConnection(conn))
        //    {

        //        if (con.State == ConnectionState.Closed)
        //        {
        //            con.Open();
        //        }
        //        else
        //        {

        //        }

        //        using (OracleCommand com = new OracleCommand(QUERY, con))
        //        {
        //            com.Parameters.Clear();
        //            com.ExecuteNonQuery();
        //        }

        //        con.Close();

        //    }
        //}
    }

    private void TDS_TO_TD(string DO_NO)//Tdeliverysap ไปใส่ใน Tdelivery
    {
        //string QUERY = @"DECLARE DO_ID VARCHAR2(10);S_OUTBOUND VARCHAR(10);BEGIN SELECT TDS.DELIVERY_NO INTO DO_ID FROM TDELIVERY_SAP TDS WHERE TDS. DELIVERY_NO = '" + DO_NO + @"' GROUP BY TDS.DELIVERY_NO;S_OUTBOUND := DO_ID;DECLARE CURSOR deli_cur IS SELECT TDELIVERYSAP.SALES_ORDER DOC_NO,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,NVL(CASE WHEN LENGTH(CASE WHEN TDELIVERYSAP.DELIVERY_NO LIKE '008%' THEN TDELIVERYSAP.PLANT ELSE  TDELIVERYSAP.SHIP_TO END)=4 THEN LPAD('9'||TDELIVERYSAP.PLANT,10,'0') ELSE TDELIVERYSAP.SHIP_TO END,TDELIVERYSAP.PLANT ) SHIP_TO,SUM(NVL(TDELIVERYSAP.QUANTITY,0)) QUANTITY  ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED ,MAX(DATE_CREATED) DATE_UPDATED ,'SYS_TMS' USER_UPDATED FROM (SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROm TDELIVERY_SAP TN LEFT JOIN  (SELECT DISTINCT DELIVERY_NO FROM TDELIVERY_SAP  WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO = S_OUTBOUND)TG ON TN.DELIVERY_NO=TG.DELIVERY_NO INNER JOIN (SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP WHERE NVL(STATUS,'N')='N' AND DELIVERY_NO = S_OUTBOUND GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE 1=1 AND NVL(TN.STATUS,'N') ='N' AND TG.DELIVERY_NO is null AND TN.DELIVERY_NO = S_OUTBOUND GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') UNION SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROM TDELIVERY_SAP TN INNER JOIN (SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO = S_OUTBOUND GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE NVL(TN.STATUS,'N')='G' AND TN.DELIVERY_NO = S_OUTBOUND GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N'))TDELIVERYSAP LEFT JOIN TCUSTOMER ON TDELIVERYSAP.PLANT=TCUSTOMER.DEPO_ID GROUP BY TDELIVERYSAP.SALES_ORDER ,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,TDELIVERYSAP.SHIP_TO,TDELIVERYSAP.SUPPLY_PLANT,TDELIVERYSAP.PLANT,TCUSTOMER.SHIP_TO;deli_records deli_cur%ROWTYPE;BEGIN OPEN deli_cur; LOOP FETCH deli_cur INTO deli_records;EXIT WHEN deli_cur%NOTFOUND;DELETE FROM  TDELIVERY WHERE DELIVERY_NO = S_OUTBOUND;INSERT INTO TDELIVERY(SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO,OTHER  ,DATE_CREATED ,USER_CREATED ,DATE_UPDATED ,USER_UPDATED)VALUES(deli_records.DOC_NO ,deli_records.DELIVERY_NO ,deli_records.DELIVERY_DATE ,deli_records.SHIP_TO ,deli_records.QUANTITY,deli_records.DATE_CREATED   ,deli_records.USER_CREATED ,deli_records.DATE_UPDATED ,deli_records.USER_UPDATED);END LOOP;CLOSE deli_cur; END;COMMIT;EXCEPTION WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('NOdata');END;";

        string QUERY = @"BEGIN DECLARE CURSOR deli_cur IS SELECT TDELIVERYSAP.SALES_ORDER DOC_NO,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,NVL(CASE WHEN LENGTH(CASE WHEN TDELIVERYSAP.DELIVERY_NO LIKE '008%' THEN TDELIVERYSAP.PLANT ELSE  TDELIVERYSAP.SHIP_TO END)=4 THEN LPAD('9'||TDELIVERYSAP.PLANT,10,'0') ELSE TDELIVERYSAP.SHIP_TO END,TDELIVERYSAP.PLANT ) SHIP_TO,SUM(NVL(TDELIVERYSAP.QUANTITY,0)) QUANTITY  ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED ,MAX(DATE_CREATED) DATE_UPDATED ,'SYS_TMS' USER_UPDATED FROM (SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROM TDELIVERY_SAP TN LEFT JOIN(SELECT DISTINCT DELIVERY_NO FROM TDELIVERY_SAP  WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO IN " + DO_NO + @" ) TG ON TN.DELIVERY_NO=TG.DELIVERY_NO INNER JOIN(SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP  WHERE NVL(STATUS,'N')='N' AND DELIVERY_NO IN " + DO_NO + @" GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE 1=1  AND NVL(TN.STATUS,'N') ='N' AND TG.DELIVERY_NO is null AND TN.DELIVERY_NO IN " + DO_NO + @" GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N')UNION SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROM TDELIVERY_SAP TN INNER JOIN (SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO IN " + DO_NO + @" GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE NVL(TN.STATUS,'N')='G'    AND TN.DELIVERY_NO IN " + DO_NO + @" GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N'))TDELIVERYSAP LEFT JOIN TCUSTOMER ON TDELIVERYSAP.PLANT=TCUSTOMER.DEPO_ID GROUP BY TDELIVERYSAP.SALES_ORDER ,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,TDELIVERYSAP.SHIP_TO,TDELIVERYSAP.SUPPLY_PLANT,TDELIVERYSAP.PLANT,TCUSTOMER.SHIP_TO;deli_records deli_cur%ROWTYPE;BEGIN OPEN deli_cur;LOOP FETCH deli_cur INTO deli_records;EXIT WHEN deli_cur%NOTFOUND;INSERT INTO TDELIVERY(SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO,OTHER  ,DATE_CREATED ,USER_CREATED ,DATE_UPDATED ,USER_UPDATED )VALUES(deli_records.DOC_NO ,deli_records.DELIVERY_NO ,deli_records.DELIVERY_DATE ,deli_records.SHIP_TO ,deli_records.QUANTITY,deli_records.DATE_CREATED   ,deli_records.USER_CREATED ,deli_records.DATE_UPDATED ,deli_records.USER_UPDATED);END LOOP;CLOSE deli_cur;END;COMMIT;END;";
        AddTODB(QUERY);

    }

    private void TDO_TO_TD(string DO_NO)//Tdeliverysap ไปใส่ใน Tdelivery
    {
        //string QUERY = @"DECLARE DO_ID VARCHAR2(10);S_OUTBOUND VARCHAR(10);BEGIN SELECT TDS.DELIVERY_NO INTO DO_ID FROM TDELIVERY_SAP TDS WHERE TDS. DELIVERY_NO = '" + DO_NO + @"' GROUP BY TDS.DELIVERY_NO;S_OUTBOUND := DO_ID;DECLARE CURSOR deli_cur IS SELECT TDELIVERYSAP.SALES_ORDER DOC_NO,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,NVL(CASE WHEN LENGTH(CASE WHEN TDELIVERYSAP.DELIVERY_NO LIKE '008%' THEN TDELIVERYSAP.PLANT ELSE  TDELIVERYSAP.SHIP_TO END)=4 THEN LPAD('9'||TDELIVERYSAP.PLANT,10,'0') ELSE TDELIVERYSAP.SHIP_TO END,TDELIVERYSAP.PLANT ) SHIP_TO,SUM(NVL(TDELIVERYSAP.QUANTITY,0)) QUANTITY  ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED ,MAX(DATE_CREATED) DATE_UPDATED ,'SYS_TMS' USER_UPDATED FROM (SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY ,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROm TDELIVERY_SAP TN LEFT JOIN  (SELECT DISTINCT DELIVERY_NO FROM TDELIVERY_SAP  WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO = S_OUTBOUND)TG ON TN.DELIVERY_NO=TG.DELIVERY_NO INNER JOIN (SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP WHERE NVL(STATUS,'N')='N' AND DELIVERY_NO = S_OUTBOUND GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE 1=1 AND NVL(TN.STATUS,'N') ='N' AND TG.DELIVERY_NO is null AND TN.DELIVERY_NO = S_OUTBOUND GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') UNION SELECT TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO  ,TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N') STATUS ,SUM(NVL(TN.QUANTITY,0)) QUANTITY,MAX(DATE_CREATED) DATE_CREATED,MAX(USER_CREATED) USER_CREATED FROM TDELIVERY_SAP TN INNER JOIN (SELECT SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N') STATUS,MAX(DATE_CREATED) DUPDATE FROM TDELIVERY_SAP WHERE NVL(STATUS,'N')='G' AND DELIVERY_NO = S_OUTBOUND GROUP BY SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO ,ITEM ,MATERIAL_NO,NVL(STATUS,'N'))TMAXDATE ON TMAXDATE.SALES_ORDER =TN.SALES_ORDER AND TMAXDATE.DELIVERY_NO =TN.DELIVERY_NO  AND nvl(TMAXDATE.SHIP_TO,'SHIP2') =nvl(TN.SHIP_TO,'SHIP2') AND TMAXDATE.ITEM =TN.ITEM  AND TMAXDATE.MATERIAL_NO =TN.MATERIAL_NO  AND TMAXDATE.STATUS =TN.STATUS AND TMAXDATE.DUPDATE =TN.DATE_CREATED WHERE NVL(TN.STATUS,'N')='G' AND TN.DELIVERY_NO = S_OUTBOUND GROUP BY TN.SALES_ORDER , TN.DELIVERY_NO , TN.DELIVERY_DATE , TN.SHIP_TO, TN.SUPPLY_PLANT , TN.PLANT , NVL(TN.STATUS,'N'))TDELIVERYSAP LEFT JOIN TCUSTOMER ON TDELIVERYSAP.PLANT=TCUSTOMER.DEPO_ID GROUP BY TDELIVERYSAP.SALES_ORDER ,TDELIVERYSAP.DELIVERY_NO,TDELIVERYSAP.DELIVERY_DATE,TDELIVERYSAP.SHIP_TO,TDELIVERYSAP.SUPPLY_PLANT,TDELIVERYSAP.PLANT,TCUSTOMER.SHIP_TO;deli_records deli_cur%ROWTYPE;BEGIN OPEN deli_cur; LOOP FETCH deli_cur INTO deli_records;EXIT WHEN deli_cur%NOTFOUND;DELETE FROM  TDELIVERY WHERE DELIVERY_NO = S_OUTBOUND;INSERT INTO TDELIVERY(SALES_ORDER ,DELIVERY_NO ,DELIVERY_DATE ,SHIP_TO,OTHER  ,DATE_CREATED ,USER_CREATED ,DATE_UPDATED ,USER_UPDATED)VALUES(deli_records.DOC_NO ,deli_records.DELIVERY_NO ,deli_records.DELIVERY_DATE ,deli_records.SHIP_TO ,deli_records.QUANTITY,deli_records.DATE_CREATED   ,deli_records.USER_CREATED ,deli_records.DATE_UPDATED ,deli_records.USER_UPDATED);END LOOP;CLOSE deli_cur; END;COMMIT;EXCEPTION WHEN NO_DATA_FOUND THEN DBMS_OUTPUT.PUT_LINE('NOdata');END;";

        string QUERY = @"BEGIN SCHD_DELIVERY_GET_ORDER('" + DO_NO + "'); END;";
        AddTODB(QUERY);

    }

    private void Update_PLAN_Capacity(string SDELIVERYNO, string NVALUE)
    {
        decimal OTHER = 0;
        bool CHECK = decimal.TryParse(NVALUE, out OTHER);


        if (!string.IsNullOrEmpty(NVALUE) && CHECK == true && !string.IsNullOrEmpty(SDELIVERYNO))
        {
            string UPDATE_TPLANSCHEDULELIST = "UPDATE TPLANSCHEDULELIST SET NVALUE = " + NVALUE + " WHERE SDELIVERYNO = '" + CommonFunction.ReplaceInjection(SDELIVERYNO) + "'  AND CACTIVE = '1'";
            AddTODB(UPDATE_TPLANSCHEDULELIST);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }


}