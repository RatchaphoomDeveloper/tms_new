﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using System.Web.Configuration;
using System.Globalization;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Security;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.SurpriseCheckYear;

public partial class surpriseChk_Year : PageBase
{
    private string ID
    {
        get
        {
            if ((string)ViewState["ID"] != null)
                return (string)ViewState["ID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ID"] = value;
        }
    }
    private DataTable dtSurpriseCheckYearList
    {
        get
        {
            if ((DataTable)ViewState["dtSurpriseCheckYearList"] != null)
                return (DataTable)ViewState["dtSurpriseCheckYearList"];
            else
                return null;
        }
        set
        {
            ViewState["dtSurpriseCheckYearList"] = value;
        }
    }

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string vendorID;
    string truckID;
    int contractID;
    string strChkTruck;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            AppointDate.Text = DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            AssignAuthen();
            this.LoadVendor();
            this.LoadUser();
            this.LoadTeamCheck();
            this.LoadData();
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadVendor()
    {
        try
        {
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadData()
    {
        try
        {
            dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(this.GetCondition());
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    private void LoadUser()
    {
        try
        {
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND USERGROUP_ID IN (" + ConfigValue.UserGroup2 + "," + ConfigValue.UserGroup3 + "," + ConfigValue.UserGroup4 + ")");
            DropDownListHelper.BindDropDownList(ref ddlUserCheck, dtUser, "SUID", "FULLNAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadTeamCheck()
    {
        try
        {
            DataTable dtTeam = UserBLL.Instance.TeamSelectBLL(string.Empty);
            //DropDownListHelper.BindDropDownList(ref ddlTeamCheck, dtTeam, "USERGROUP_ID", "USERGROUP_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetCondition()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            if (ddlVendor.SelectedIndex > 0)
                sb.Append(" AND SVENDORID = '" + ddlVendor.SelectedValue + "'");
            if (ddlContract.SelectedIndex > 0)
                sb.Append(" AND SCONTRACTID = '" + ddlContract.SelectedValue + "'");
            if (!string.Equals(AppointDate.Text, string.Empty))
                sb.Append(" AND TO_CHAR(APPOINTDATE,'DD/MM/YYYY') = '" + AppointDate.Text + "'");
            if (ddlUserCheck.SelectedIndex > 0)
                sb.Append(" AND USERCHECK = " + ddlUserCheck.SelectedValue + "");
            sb.Append(" AND IS_ACTIVE IN ( 1,2 )");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.LoadData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            ddlVendor.ClearSelection();
            ddlVendor_SelectedIndexChanged(null, null);
            ddlContract.ClearSelection();
            AppointDate.Text = string.Empty;
            ddlUserCheck.ClearSelection();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtcontract = ContractBLL.Instance.ConTractSelectByVendor(ddlVendor.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlContract, dtcontract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvSurpriseCheckYear.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvSurpriseCheckYear, dtSurpriseCheckYearList);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvSurpriseCheckYear_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvSurpriseCheckYear.DataKeys[e.RowIndex]["ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("surpriseChk_ShowData.aspx?DocID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}