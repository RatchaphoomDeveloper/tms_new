﻿using GemBox.Spreadsheet;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;

public partial class SpotDelivery : PageBase
{
    #region + View State +
    private DataTable dtDelivery
    {
        get
        {
            if ((DataTable)ViewState["dtDelivery"] != null)
                return (DataTable)ViewState["dtDelivery"];
            else
                return null;
        }
        set
        {
            ViewState["dtDelivery"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            txtDeliveryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            this.LoadWarehouse();
            this.LoadContract();
            cmdSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadWarehouse()
    {
        try
        {
            DataTable dtWarehouse = new DataTable();
            dtWarehouse = WarehouseBLL.Instance.WarehouseSelectBLL();
            DropDownListHelper.BindDropDownList(ref ddlWarehouse, dtWarehouse, "STERMINALID", "STERMINALNAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadContract()
    {
        try
        {
            DataTable dtContract = new DataTable();
            dtContract = ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.CSPACIALCONTRAC = 'Y'");
            DropDownListHelper.BindDropDownList(ref ddlContract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            dtDelivery = ContractBLL.Instance.SpotDeliverySelectDAL(this.GetCondition());
            GridViewHelper.BindGridView(ref dgvTemplate, dtDelivery);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTemplate.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTemplate, dtDelivery);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetCondition()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (!string.Equals(txtDeliveryDate.Text.Trim(), string.Empty))
                sb.Append(" AND PICKING_DATE BETWEEN TO_DATE('" + txtDeliveryDate.Text.Trim() + "', 'DD/MM/YYYY') AND TO_DATE('" + txtDeliveryDate.Text.Trim() + "', 'DD/MM/YYYY')");

            if (ddlWarehouse.SelectedIndex > 0)
                sb.Append(" AND PLANT = '" + ddlWarehouse.SelectedValue + "'");

            if (ddlContract.SelectedIndex > 0)
                sb.Append(" AND SCONTRACTNO = '" + ddlContract.SelectedItem.Text + "'");

            if (!string.Equals(txtDeliveryNo.Text.Trim(), string.Empty))
                sb.Append(" AND DELIVERY_NO LIKE '%" + txtDeliveryNo.Text.Trim() + "%'");

            if (!string.Equals(txtLicense.Text.Trim(), string.Empty))
                sb.Append(" AND VEHICLE_NO LIKE '%" + txtLicense.Text.Trim() + "%'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdClear_Click(object sender, EventArgs e)
    {
        txtDeliveryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        ddlWarehouse.ClearSelection();
        ddlContract.ClearSelection();
        txtDeliveryNo.Text = string.Empty;
        txtLicense.Text = string.Empty;
    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        try
        {
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];
            worksheet.InsertDataTable(dtDelivery, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
            
            string Path = this.CheckPath();
            string FileName = "DeliveryPlanSpot_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
}