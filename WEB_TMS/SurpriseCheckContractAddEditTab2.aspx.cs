﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.SurpriseCheck;
using EmailHelper;
using TMS_BLL.Transaction.Complain;

public partial class SurpriseCheckContractAddEditTab2 : PageBase
{
    DataTable dt;
    DataSet ds;
    DataRow dr;
    DataRow[] drs;
    bool isEnabled = true;
    string MessSAPSuccess = "ข้อมูลพนักงานใน SAP สำเร็จ<br/ >";
    string MessSAPFail = "<span style=\"color:Red;\">{0}ข้อมูลพนักงานใน SAP ไม่สำเร็จ !!!{1}</span><br/ >";
    #region ViewState
    private DataTable dtData
    {
        get { return (DataTable)ViewState["dtData"]; }
        set { ViewState["dtData"] = value; }
    }
    private DataTable dtMCheckList
    {
        get { return (DataTable)ViewState["dtMCheckList"]; }
        set { ViewState["dtMCheckList"] = value; }
    }
    private DataTable dtTCheckList
    {
        get { return (DataTable)ViewState["dtTCheckList"]; }
        set { ViewState["dtTCheckList"] = value; }
    }
    private DataTable dtFileStep1
    {
        get { return (DataTable)ViewState["dtFileStep1"]; }
        set { ViewState["dtFileStep1"] = value; }
    }
    #endregion

    string TempDirectory = "UploadFile/SurpriseCheckContractAddEditTab2/{0}/{2}/{1}/";
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);
                liTab1.Visible = true;
                GeneralTab1.HRef = "SurpriseCheckContractAddEdit.aspx?str=" + str;


            }
            else
            {
                //txtUser.Text = Session["UserName"] + string.Empty;
                //hidREQUEST_BY.Value = Session["UserID"] + string.Empty;
                hidIS_ACTIVE.Value = "2";
            }
            if (Session["CGROUP"] + string.Empty == "0")
            {
                txtRemark.Enabled = false;
                isEnabled = false;
            }

            this.AssignAuthen();
            dtMCheckList = SurpriseCheckContractBLL.Instance.MCheckContractListSelect();
            gvCheckList.DataSource = dtMCheckList;
            gvCheckList.DataBind();
        }
    }

    #endregion
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {

            }
            if (!CanWrite)
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region SetData
    private void SetData(string ID)
    {
        hidID.Value = ID;
        ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractListSelectByID(ID);
        if (ds.Tables.Count > 0)
        {
            dtTCheckList = ds.Tables[0];
            dtFileStep1 = ds.Tables[1];
        }

        ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelectByID(ID);
        if (ds.Tables.Count > 0)
        {
            dtData = ds.Tables[0];
            hidIS_ACTIVE.Value = ds.Tables[0].Rows[0]["IS_ACTIVE"] + string.Empty;
            txtRemark.Text = ds.Tables[0].Rows[0]["REMARKTAB2"] + string.Empty;

            if (hidIS_ACTIVE.Value == "2" || hidIS_ACTIVE.Value == "4" || hidIS_ACTIVE.Value == "6")
            {
                btnSave.Visible = false;

            }

            if (Session["CGROUP"] + string.Empty == "0")
            {
                txtRemark.Enabled = false;
            }
            else
            {
                if (hidIS_ACTIVE.Value == "2" || hidIS_ACTIVE.Value == "3" || hidIS_ACTIVE.Value == "4")
                {
                    txtRemark.Enabled = true;
                }
                else
                {
                    txtRemark.Enabled = false;
                }
            }
        }
    }
    #endregion

    #region Save

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            SaveStep1();
            alertSuccess("บันทึกสำเร็จ", "javascript:window.close();");

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void SendEmail(int TemplateID, string Status, string Remarker = "")
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == 132)
                {
                    Body = Body.Replace("{CONTRACT}", dtData.Rows[0]["SCONTRACTNO"].ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["CHASIS_H"].ToString() : dtData.Rows[0]["CHASIS_H"].ToString() + "/" + dtData.Rows[0]["CHASIS_T"].ToString());
                    Body = Body.Replace("{LICENSE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["SHEADREGISTERNO"].ToString() : dtData.Rows[0]["SHEADREGISTERNO"].ToString() + "/" + dtData.Rows[0]["STRAILERREGISTERNO"].ToString());
                    Body = Body.Replace("{REMARK}", Remarker);
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", dtData.Rows[0]["SABBREVIATION"].ToString());

                    string str = Request.QueryString["str"];
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    string strQuery = Encoding.UTF8.GetString(decryptedBytes);

                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(strQuery);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body, "", "SurpriseCheckContrack1", ColumnEmailName);
                }
                else if (TemplateID == 131)
                {
                    Body = Body.Replace("{CONTRACT}", dtData.Rows[0]["SCONTRACTNO"].ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["CHASIS_H"].ToString() : dtData.Rows[0]["CHASIS_H"].ToString() + "/" + dtData.Rows[0]["CHASIS_T"].ToString());
                    Body = Body.Replace("{LICENSE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["SHEADREGISTERNO"].ToString() : dtData.Rows[0]["SHEADREGISTERNO"].ToString() + "/" + dtData.Rows[0]["STRAILERREGISTERNO"].ToString());
                    Body = Body.Replace("{APPOINTDATE}", dtData.Rows[0]["CHECK_DATE"].ToString());
                    Body = Body.Replace("{APPOINTLOCATE}", dtData.Rows[0]["PLACE"].ToString());
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", dtData.Rows[0]["SABBREVIATION"].ToString());

                    string str = Request.QueryString["str"];
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    string strQuery = Encoding.UTF8.GetString(decryptedBytes);

                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(strQuery);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body, "", "SurpriseCheckContrack2", ColumnEmailName);
                }
                else if (TemplateID == 133)
                {
                    Body = Body.Replace("{CONTRACT}", dtData.Rows[0]["SCONTRACTNO"].ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["CHASIS_H"].ToString() : dtData.Rows[0]["CHASIS_H"].ToString() + "/" + dtData.Rows[0]["CHASIS_T"].ToString());
                    Body = Body.Replace("{LICENSE}", dtData.Rows[0]["TRUCK_TYPE"].ToString() == "1" ? dtData.Rows[0]["SHEADREGISTERNO"].ToString() : dtData.Rows[0]["SHEADREGISTERNO"].ToString() + "/" + dtData.Rows[0]["STRAILERREGISTERNO"].ToString());
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", dtData.Rows[0]["SABBREVIATION"].ToString());

                    string str = Request.QueryString["str"];
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    string strQuery = Encoding.UTF8.GetString(decryptedBytes);

                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(strQuery);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dtData.Rows[0]["SVENDORID"].ToString(), true, true), Subject, Body, "", "SurpriseCheckContrack1", ColumnEmailName);
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    private void SaveStep1()
    {

        DataSet ds = new DataSet("DS");
        int STATUS_ID = 2;//กรณีผ่านหมดทุกข้อ
        if (dtTCheckList != null)
        {
            dtTCheckList = dtTCheckList.Clone();
            foreach (GridViewRow item in gvCheckList.Rows)
            {
                RadioButtonList rblIS_ACTIVE = (RadioButtonList)item.FindControl("rblIS_ACTIVE");
                TextBox txtREMARK = (TextBox)item.FindControl("txtREMARK");
                dr = dtTCheckList.NewRow();
                dr["M_CHECK_CONTRACT_LIST_ID"] = gvCheckList.DataKeys[item.RowIndex]["ID"] + string.Empty;
                dr["T_CHECK_CONTRACT_ID"] = hidID.Value;
                dr["REMARK"] = txtREMARK.Text;
                dr["IS_ACTIVE"] = !string.IsNullOrEmpty(rblIS_ACTIVE.SelectedValue) ? int.Parse(rblIS_ACTIVE.SelectedValue) : (object)DBNull.Value;
                dr["CREATE_BY"] = Session["UserID"] + string.Empty;
                dr["UPDATE_BY"] = Session["UserID"] + string.Empty;
                dtTCheckList.Rows.Add(dr);
                if (rblIS_ACTIVE.SelectedValue == "4")
                {
                    STATUS_ID = 4;//กรณีไม้ผ่านแม้แต่ข้อเดว
                }
                if (STATUS_ID != 4)
                {
                    if (rblIS_ACTIVE.SelectedValue == "3" || string.IsNullOrEmpty(rblIS_ACTIVE.SelectedValue))
                    {
                        STATUS_ID = 3;//กรณีไม้ผ่านแม้แต่ข้อเดว
                    }
                }
            }
        }
        if (!dtTCheckList.Columns.Contains("STATUS_ID"))
        {
            dtTCheckList.Columns.Add("STATUS_ID");
        }

        //for (int i = 0; i < dtTCheckList.Rows.Count; i++)
        //    if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == STATUS_ID.ToString())
        dtTCheckList.Rows[0]["STATUS_ID"] = STATUS_ID + string.Empty;

        //if (STATUS_ID == 2)
        //{
        //    string mess = "ไม่สามารถตรวจสภาพให้ผ่านได้เนื่องจากยังไม่ได้แนบไฟล์ <br/>";
        //    ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelectByID(hidID.Value);
        //    if (ds.Tables.Count > 0)
        //    {
        //        DataTable dtUploadType = AccidentBLL.Instance.AccidentRequestFile("T_CHECK_CONTRACT_FILE");
        //        foreach (DataRow item in dtUploadType.Rows)
        //        {
        //            DataRow[] drs = ds.Tables[1].Select("UPLOAD_ID = " + item["UPLOAD_ID"]);
        //            if (!drs.Any())
        //            {
        //                mess += " - " + item["UPLOAD_NAME"] + "<br/>";
        //            }
        //        }
        //        if (mess != "ไม่สามารถตรวจสภาพให้ผ่านได้เนื่องจากยังไม่ได้แนบไฟล์ <br/>")
        //        {
        //            alertFail(mess);
        //            return;
        //        }


        //        //dtUploadTypeExpired = UploadTypeBLL.Instance.UploadTypeSelectBLL("T_CHECK_CONTRACT_FILE_EXPIRED");
        //    }
        //}
        if (dtFileStep1.Columns.Contains("FULLPATH"))
        {
            dtFileStep1.Columns["FULLPATH"].ColumnName = "SFILEPATH";
        }
        ds = new DataSet("DS");
        dtTCheckList.TableName = "DT";
        ds.Tables.Add(dtTCheckList.Copy());
        dtFileStep1.TableName = "FILE";
        ds.Tables.Add(dtFileStep1.Copy());

        SurpriseCheckContractBLL.Instance.SurpriseCheckContractListSave(ds);

        if (Session["CGROUP"] + string.Empty == "0")
        {
            if (STATUS_ID == 3)
                this.SendEmail(133, "แก้ไข");
        }
        else
        {
            if (STATUS_ID == 2)
            {
                string Remark = string.Empty;
                for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                    if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "2")
                        if (Remark == string.Empty || Remark == "")
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark = dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                        else
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark += "," + dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(hidID.Value), 2, int.Parse(Session["UserID"] + string.Empty), Remark);
                this.SendEmail(132, "ผ่าน", Remark);
            }
            else if (STATUS_ID == 3)
            {
                string Remark = string.Empty;
                for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                    if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "3")
                        if (Remark == string.Empty || Remark == "")
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark = dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                        else
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark += "," + dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(hidID.Value), 3, int.Parse(Session["UserID"] + string.Empty), Remark);
                this.SendEmail(132, "แก้ไข", Remark);
            }
            else if (STATUS_ID == 4)
            {
                string Remark = string.Empty;
                for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                    if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "4")
                        if (Remark == string.Empty || Remark == "")
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark = dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                        else
                        {
                            if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                Remark += "," + dtTCheckList.Rows[i]["REMARK"].ToString();
                        }
                SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(hidID.Value), 4, int.Parse(Session["UserID"] + string.Empty), Remark);
                this.SendEmail(132, "ไม่ผ่านกรุณานัดหมายใหม่", Remark);
            }
        }
    }


    #endregion

    #region btnClose_Click
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("SurpriseCheckContract.aspx");

    }

    #endregion

    #region gvCheckList
    protected void gvCheckList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (e.Row.RowType == DataControlRowType.Header)
        {
            RadioButtonList rblIS_ACTIVEALL = (RadioButtonList)e.Row.FindControl("rblIS_ACTIVEALL");
            rblIS_ACTIVEALL.Enabled = isEnabled;
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            string ID = drv["ID"] + string.Empty;
            RadioButtonList rblIS_ACTIVE = (RadioButtonList)e.Row.FindControl("rblIS_ACTIVE");
            TextBox txtREMARK = (TextBox)e.Row.FindControl("txtREMARK");
            Button btn = (Button)e.Row.FindControl("btnStep1");
            if (string.IsNullOrEmpty(drv["CHECK_TYPE_ID"] + string.Empty))
            {
                rblIS_ACTIVE.Visible = false;
                txtREMARK.Visible = false;
                btn.Visible = false;
                e.Row.BackColor = System.Drawing.Color.Aqua;
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                if (!string.IsNullOrEmpty(drv["PARENT_ID"] + string.Empty))
                {
                    e.Row.BackColor = System.Drawing.Color.Azure;
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Center;
                }
            }
            else
            {
                rblIS_ACTIVE.Enabled = isEnabled;
                txtREMARK.Enabled = isEnabled;
            }
            if (dtTCheckList != null)
            {

                drs = dtTCheckList.Select("M_CHECK_CONTRACT_LIST_ID = " + ID);
                if (drs.Any())
                {
                    rblIS_ACTIVE.SelectedValue = drs[0]["IS_ACTIVE"] + string.Empty;
                    txtREMARK.Text = drs[0]["REMARK"] + string.Empty;
                    if (rblIS_ACTIVE.SelectedValue == "2")
                    {
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.Green;

                    }
                    else if (rblIS_ACTIVE.SelectedValue == "3")
                    {
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.Blue;
                    }
                    else if (rblIS_ACTIVE.SelectedValue == "4")
                    {
                        e.Row.Cells[2].ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
            if (hidIS_ACTIVE.Value != "1")
            {
                btn.Enabled = true;
            }
            if (dtFileStep1 != null)
            {
                if (dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + ID).Any())
                {
                    btn.CssClass = "btn btn-warning";
                    btn.Enabled = true;
                    //btn.Text = "แจ้งพบปัญหา";

                }
            }
        }
    }
    protected void gvCheckList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridView gv = (GridView)sender;
        if (gv != null)
        {
            DataKey dka = gv.DataKeys[int.Parse(e.CommandName)];
            hidRowIndex.Value = e.CommandName;
            hidM_CHECK_CONTRACT_LIST_ID.Value = dka["ID"] + string.Empty;

            lblSCHECKLISTNAME.Text = dka["DETAIL"] + string.Empty;
            DataRow[] drs = dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
            if (drs.Any())
            {
                gvFileStep1.DataSource = drs.CopyToDataTable();
            }
            else
            {
                gvFileStep1.DataSource = dtFileStep1.Clone();
            }
            gvFileStep1.DataBind();
            plCheckList.Visible = false;
            plStep1.CssClass = "";

        }
    }
    #endregion

    #region Step1
    protected void btnAddStep1_Click(object sender, EventArgs e)
    {
        try
        {

            if (fileUploadStep1.HasFile && fileUploadStep1.PostedFile.ContentType.Contains("image/") && !string.IsNullOrEmpty(txtDetailStep1.Text.Trim()))
            {
                string[] fileExt = fileUploadStep1.FileName.Split('.');
                int ncol = fileExt.Length - 1;
                string genName = "checktruck" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                string SFILEPATH = string.Format(TempDirectory, Session["SVDID"] + "", "uploader", Session["UserID"] + "");
                UploadFile2Server(fileUploadStep1, genName, SFILEPATH);

                DataRow dr = dtFileStep1.NewRow();
                //dr["SCHECKID"] = hidSCHECKID.Value;
                dr["T_CHECK_CONTRACT_ID"] = hidID.Value;
                dr["M_CHECK_CONTRACT_LIST_ID"] = hidM_CHECK_CONTRACT_LIST_ID.Value;

                dr["REMARK"] = txtDetailStep1.Text.Trim();
                dr["FILENAME_USER"] = fileUploadStep1.FileName;
                dr["FILENAME_SYSTEM"] = genName + "." + fileExt[ncol].Trim();
                dr["FULLPATH"] = SFILEPATH + genName + "." + fileExt[ncol].Trim();
                dr["CREATE_BY"] = Session["UserID"] + "";
                dtFileStep1.Rows.Add(dr);
                DataRow[] drs = dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
                if (drs.Any())
                {
                    gvFileStep1.DataSource = drs.CopyToDataTable();
                }
                else
                {
                    gvFileStep1.DataSource = dtFileStep1.Clone();
                }
                gvFileStep1.DataBind();
                txtDetailStep1.Text = string.Empty;
            }
            else
            {
                throw new Exception("กรุณาเลือกไฟล์ที่เป็นรูปภาพแล้วระบุรายละเอียด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void btnCloseStep1_Click(object sender, EventArgs e)
    {
        plStep1.CssClass = "hide";
        plCheckList.Visible = true;
        if (!string.IsNullOrEmpty(hidRowIndex.Value))
        {
            GridViewRow gvr = null;
            gvr = gvCheckList.Rows[int.Parse(hidRowIndex.Value)];

            Button btn = (Button)gvr.FindControl("btnStep1");

            DataRow[] drs = dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
            if (drs.Any())
            {
                //btn.Text = "แจ้งพบปัญหา";
                btn.CssClass = "btn btn-warning";
            }
            else
            {

                //btn.Text = "แจ้งตรวจพบปัญหา";
                btn.CssClass = "btn btn-info";
            }

        }

    }
    protected void gvFileStep1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataRow[] drs = dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);

        dtFileStep1.Rows.Remove(drs[e.RowIndex]);
        drs = dtFileStep1.Select("M_CHECK_CONTRACT_LIST_ID = " + hidM_CHECK_CONTRACT_LIST_ID.Value);
        if (drs.Any())
        {
            gvFileStep1.DataSource = drs.CopyToDataTable();
        }
        else
        {
            gvFileStep1.DataSource = dtFileStep1.Clone();
        }
        gvFileStep1.DataBind();
    }
    #endregion

    #region UploadFile2Server
    private bool UploadFile2Server(FileUpload ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }
    #endregion

    protected void rblIS_ACTIVEALL_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblIS_ACTIVEALL = (RadioButtonList)sender;
        foreach (GridViewRow item in gvCheckList.Rows)
        {
            RadioButtonList rblIS_ACTIVE = (RadioButtonList)item.FindControl("rblIS_ACTIVE");
            rblIS_ACTIVE.SelectedValue = rblIS_ACTIVEALL.SelectedValue;
            if (rblIS_ACTIVE.SelectedValue == "2")
            {
                item.Cells[2].ForeColor = System.Drawing.Color.Green;

            }
            else if (rblIS_ACTIVE.SelectedValue == "3")
            {
                item.Cells[2].ForeColor = System.Drawing.Color.Blue;
            }
            else if (rblIS_ACTIVE.SelectedValue == "4")
            {
                item.Cells[2].ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    protected void rblIS_ACTIVE_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblIS_ACTIVE = (RadioButtonList)sender;
        GridViewRow gvr = (GridViewRow)rblIS_ACTIVE.NamingContainer;
        if (gvr != null)
        {
            if (rblIS_ACTIVE.SelectedValue == "2")
            {
                gvr.Cells[2].ForeColor = System.Drawing.Color.Green;

            }
            else if (rblIS_ACTIVE.SelectedValue == "3")
            {
                gvr.Cells[2].ForeColor = System.Drawing.Color.Blue;
            }
            else if (rblIS_ACTIVE.SelectedValue == "4")
            {
                gvr.Cells[2].ForeColor = System.Drawing.Color.Red;
            }

        }

    }
}