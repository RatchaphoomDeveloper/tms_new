﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;
using System.Drawing;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;

public partial class ReportTeabpansCompletely : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static DataTable dtMainData = new DataTable();
    private static DataTable dtGraphData = new DataTable();
    private static string series = "";
    private static string argument = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            //ListData();
            SetCboYear();
        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบรับรอง")
        {
            ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
            txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
            ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
            txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
        }


    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": bindChart11();//ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string[] paras = e.Args[0].Split(';');
                string sr = txtSeriesName.Text.Trim();
                string ar = txtargument.Text.Trim();
                switch (paras[0])
                {

                    case "ListData":
                        switch (sr)
                        {
                            //เงื่อนไขคือผ่าน
                            case "S":
                                switch (ar)
                                {
                                    case "จำนวนที่วัดน้ำเสร็จ": ListData(" AND NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx'");
                                        argument = "จำนวนที่วัดน้ำเสร็จ";
                                        break;
                                    case "จำนวนที่ส่งเทียบแป้น": ListData(" AND NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx' AND NVL(DOC.DOC_TYPE,'xxx') <> 'xxx'");
                                        argument = "จำนวนที่ส่งเทียบแป้น";
                                        break;
                                    case "จำนวนที่เทียบแป้นผ่าน": ListData(" AND NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY = 'Y' ");
                                        argument = "จำนวนที่เทียบแป้นผ่าน";
                                        break;

                                }
                                break;
                            //เงื่อนไขคือไม่ผ่าน
                            case "S2":
                                switch (ar)
                                {
                                    //case "จำนวนที่วัดน้ำเสร็จ": ListData("1");
                                    //    break;
                                    case "จำนวนที่ส่งเทียบแป้น": ListData(" AND NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx' AND NVL(DOC.DOC_TYPE,'xxx') = 'xxx'");
                                        argument = "จำนวนที่ไม่ส่งเทียบแป้น";
                                        break;
                                    case "จำนวนที่เทียบแป้นผ่าน": ListData(" AND NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY = 'N' ");
                                        argument = "จำนวนที่เทียบแป้นไม่ผ่าน";
                                        break;

                                }
                                break;
                        }
                        break;
                }
                break;
        }
    }

    void ListData(string ss)
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
        {

            Condition += " AND TO_CHAR(TIC.EXAMDATE,'MM/yyyy') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(cboVendor.Text + ""))
        {
            Condition += " AND REQ.VENDOR_ID = '" + CommonFunction.ReplaceInjection(cboVendor.Value + "") + "'";
        }

        string QUERY = @"SELECT ROWNUM||'.' as NO
,TRUNC(TO_DATE(REQ.APPROVE_DATE,'dd/MM/yyyy')) as APPROVE_DATE
,TRUNC(TO_DATE(REQ.WATER_EXPIRE_DATE,'dd/MM/yyyy')) as WATER_EXPIRE_DATE
,TRUNC(TO_DATE(REQ.SERVICE_DATE,'dd/MM/yyyy')) as SERVICE_DATE
,VEN.SABBREVIATION
,CAT.CARCATE_NAME
,RQT.REQTYPE_NAME
,CAS.CAUSE_NAME
,NVL(DOC.DOC_TYPE,'xxx') as DOC_TYPE
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
LEFT JOIN TBL_CAUSE CAS ON CAS.CAUSE_ID = REQ.CAUSE_ID
LEFT JOIN TBL_CARCATE CAT ON CAT.CARCATE_ID = REQ.CARCATE_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,DOC_TYPE FROM TBL_REQDOC 
    WHERE DOC_TYPE = '0006' 
)DOC 
ON DOC.REQUEST_ID = REQ.REQUEST_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 " + ss + " " + Condition + "";
        dtMainData.Clear();
        dtMainData = CommonFunction.Get_Data(conn, QUERY);
        if (dtMainData.Rows.Count > 0)
        {
            gvw.DataSource = dtMainData;

        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {
        rpt_CarWaterExpire report = new rpt_CarWaterExpire();

        #region function report

        XRChart xrChart2 = report.FindControl("xrChart1", true) as XRChart;
        xrChart2.Series.Clear();
        //add Title
        ChartTitle chartTitle1 = new ChartTitle();
        chartTitle1.Text = "รายงานผลการนำรถส่งเทียบแป้น";
        chartTitle1.Font = new Font("Tahoma", 15);
        xrChart2.Titles.Add(chartTitle1);
        if (!string.IsNullOrEmpty(cboYear.Text))
        {
            ChartTitle chartTitle2 = new ChartTitle();
            chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text;
            chartTitle2.Font = new Font("Tahoma", 13);
            xrChart2.Titles.Add(chartTitle2);
        }

        if (dtGraphData.Rows.Count > 0)
        {
            Series series = new Series("S", ViewType.StackedBar);

            //Series series2 = new Series("%เทียบแป้นผ่าน/ส่งเทียบแป้น", ViewType.Bar);

            //Series series3 = new Series("%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น", ViewType.Bar);

            double Value = !string.IsNullOrEmpty(dtGraphData.Rows[0]["CCHECKING_WATER"] + "") ? double.Parse(dtGraphData.Rows[0]["CCHECKING_WATER"] + "") : 0;
            series.Points.Add(new SeriesPoint("จำนวนที่วัดน้ำเสร็จ", new double[] { Value }));

            double Value2 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["DOCSEND"] + "") ? double.Parse(dtGraphData.Rows[0]["DOCSEND"] + "") : 0;

            series.Points.Add(new SeriesPoint("จำนวนที่ส่งเทียบแป้น", new double[] { Value2 }));
            double Value4 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["RESULT_TRY"] + "") ? double.Parse(dtGraphData.Rows[0]["RESULT_TRY"] + "") : 0;
            series.Points.Add(new SeriesPoint("จำนวนที่เทียบแป้นผ่าน", new double[] { Value4 }));

            xrChart2.Series.Add(series);

            Series series2 = new Series("S2", ViewType.StackedBar);
            double Value3 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["DOCNOSEND"] + "") ? double.Parse(dtGraphData.Rows[0]["DOCNOSEND"] + "") : 0;
            series2.Points.Add(new SeriesPoint("จำนวนที่ส่งเทียบแป้น", new double[] { Value3 }));

            double Value5 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["NORESULT_TRY"] + "") ? double.Parse(dtGraphData.Rows[0]["NORESULT_TRY"] + "") : 0;
            series2.Points.Add(new SeriesPoint("จำนวนที่เทียบแป้นผ่าน", new double[] { Value5 }));
            xrChart2.Series.Add(series2);


            xrChart2.Legend.Visible = false;
            //xrChart2.Series.Add(series);
        }

        ((XRLabel)report.FindControl("xrLabel1", true)).Text = "รายงานผลการนำรถส่งเทียบแป้น";
        if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        {

            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "รายงานผลการนำรถส่งเทียบแป้น (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH")) + ")";
        }
        else
        {
            ((XRLabel)report.FindControl("xrLabel2", true)).Text = "";
        }
        ((XRLabel)report.FindControl("xrLabel3", true)).Text = argument + " " + dtMainData.Rows.Count + " รายการ";
        ((XRLabel)report.FindControl("xrLabel4", true)).Text = "วันที่จัดทำรายงาน: " + DateTime.Now.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));

        report.Name = "ReportTeabpansCompletely";
        report.DataSource = dtMainData;
        string fileName = "รายงานผลการนำรถส่งเทียบแป้น_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ListReport("P");
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ListReport("E");
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion

    #region SetAutoComplete

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID , SABBREVIATION FROM (SELECT ROW_NUMBER()OVER(ORDER BY SVENDORID) AS RN , SVENDORID , SABBREVIATION
          FROM ( 
                SELECT TVENDOR.SVENDORID,TVENDOR.SABBREVIATION FROM TVENDOR
                INNER JOIN 
                (
                    SELECT 
                       SVENDORID, TO_DATE(DEND) - TO_DATE(sysdate) as DateCountdown
                    FROM TCONTRACT
                    WHERE   TO_DATE(DEND) - TO_DATE(sysdate)  >= 0
                )O
                ON TVENDOR.SVENDORID = O.SVENDORID
                GROUP BY TVENDOR.SVENDORID,TVENDOR.SABBREVIATION)
         WHERE SABBREVIATION LIKE :fillter OR SVENDORID LIKE :fillter) WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        // sdsVendor.SelectParameters.Add("fillterID", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }

    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    #endregion

    void SetCboYear()
    {
        _SYEAR.Clear();
        string QUERY = @"SELECT TO_CHAR(MAX(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMAX,TO_CHAR(MIN(add_months(REQ.CREATE_DATE,6516)),'yyyy') as SYEARMIN
FROM TBL_REQUEST REQ
GROUP BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy') ORDER BY TO_CHAR(add_months(REQ.CREATE_DATE,6516),'yyyy')";

        string SYEAR = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));
        int NYEAR = int.Parse(SYEAR);
        int NYEARBACK = NYEAR - 10;
        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 9)
        {
            //ถ้าไม่ค่า MAX MIN เท่ากับปีปัจจุบัน ให้แสดง 10 จากปัจจุบันลงไป
            if (SYEAR == dt.Rows[0]["SYEARMAX"] + "" && SYEAR == dt.Rows[0]["SYEARMIN"] + "")
            {
                for (int i = NYEARBACK; i <= NYEAR; i++)
                {
                    _SYEAR.Add(new SYEAR
                    {
                        NVALUE = i,
                        SVALUE = (i + 543) + ""
                    });
                }
            }
            else
            {

                int dtNYEARMAX = int.Parse(dt.Rows[0]["SYEARMAX"] + "");
                int dtNYEARMIN = int.Parse(dt.Rows[0]["SYEARMIN"] + "");
                if ((dtNYEARMAX - dtNYEARMIN) > 10)
                {
                    //ถ้าค่า MAX - MIN มากกว่า 10 
                    for (int i = dtNYEARMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }

                }
                else
                { //ถ้าค่า MAX - MIN น้อยกว่า 10 
                    int MINMIN = dtNYEARMIN - (dtNYEARMAX - dtNYEARMIN);
                    for (int i = MINMIN; i <= dtNYEARMAX; i++)
                    {
                        _SYEAR.Add(new SYEAR
                        {
                            NVALUE = i,
                            SVALUE = (i + 543) + ""
                        });
                    }
                }



            }
        }
        else
        {
            //ถ้าไม่มีข้อมูลให้แสดง 10 จากปัจจุบันลงไป
            for (int i = NYEARBACK; i <= NYEAR; i++)
            {
                _SYEAR.Add(new SYEAR
                {
                    NVALUE = i,
                    SVALUE = (i + 543) + ""
                });
            }
        }

        cboYear.DataSource = _SYEAR.OrderByDescending(o => o.NVALUE);
        cboYear.TextField = "SVALUE"; cboYear.ValueField = "NVALUE";
        cboYear.DataBind();
    }

    private void bindChart11()
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(cboMonth.Text + "") && !string.IsNullOrEmpty(cboYear.Text + ""))
        {
            lblsTail.Text = "ประจำเดือน" + cboMonth.Text + " " + cboYear.Text + " (จัดทำรายงานวันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + ")";
            Condition += " AND TO_CHAR(TIC.EXAMDATE,'MM/yyyy') = '" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "/" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "'";
        }
        else
        {
            lblsTail.Text = "-";
        }
        if (!string.IsNullOrEmpty(cboVendor.Text + ""))
        {
            Condition += " AND REQ.VENDOR_ID = '" + CommonFunction.ReplaceInjection(cboVendor.Value + "") + "'";
        }

        string QUERY = @"
SELECT SUM(CASE WHEN NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx' THEN 1 ELSE 0 END) as CCHECKING_WATER
,SUM(CASE WHEN NVL(DOC.DOC_TYPE,'xxx') <> 'xxx' AND NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx' THEN 1 ELSE 0 END) as DOCSEND
,SUM(CASE WHEN NVL(DOC.DOC_TYPE,'xxx') = 'xxx' AND NVL(REQ.CCHECKING_WATER,'xxx') <> 'xxx'  THEN 1 ELSE 0 END) as DOCNOSEND
,SUM(CASE WHEN NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY='Y'  THEN 1 
ELSE NULL END) as RESULT_TRY
,SUM(CASE WHEN NVL(REQ.RESULT_TRY,'xxx') <> 'xxx' AND REQ.RESULT_TRY='N'  THEN 1 
ELSE NULL END) as NORESULT_TRY
FROM TBL_REQUEST REQ
LEFT JOIN 
(
    SELECT REQUEST_ID,DOC_TYPE FROM TBL_REQDOC 
    WHERE DOC_TYPE = '0006' 
)DOC 
ON DOC.REQUEST_ID = REQ.REQUEST_ID
LEFT JOIN 
(
    SELECT REQUEST_ID,MAX(EXAMDATE) as  EXAMDATE  FROM  TBL_TIME_INNER_CHECKINGS WHERE ISACTIVE_FLAG = 'Y' GROUP BY REQUEST_ID
)
TIC ON TIC.REQUEST_ID = REQ.REQUEST_ID
WHERE 1=1 " + Condition + @"
";
        dtGraphData.Clear();
        dtGraphData = CommonFunction.Get_Data(conn, QUERY);

        // DateTimeFormatInfo mfi = new DateTimeFormatInfo();
        //Series series = new Series("%เทียบแป้น/วัดน้ำเสร็จ", ViewType.Bar);

        //Series series2 = new Series("%เทียบแป้นผ่าน/ส่งเทียบแป้น", ViewType.Bar);

        //Series series3 = new Series("%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น", ViewType.Bar);
        if (dtGraphData.Rows.Count > 0)
        {
            Series series = new Series("S", ViewType.StackedBar);

            //Series series2 = new Series("%เทียบแป้นผ่าน/ส่งเทียบแป้น", ViewType.Bar);

            //Series series3 = new Series("%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น", ViewType.Bar);

            double Value = !string.IsNullOrEmpty(dtGraphData.Rows[0]["CCHECKING_WATER"] + "") ? double.Parse(dtGraphData.Rows[0]["CCHECKING_WATER"] + "") : 0;
            series.Points.Add(new SeriesPoint("จำนวนที่วัดน้ำเสร็จ", new double[] { Value }));

            double Value2 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["DOCSEND"] + "") ? double.Parse(dtGraphData.Rows[0]["DOCSEND"] + "") : 0;

            series.Points.Add(new SeriesPoint("จำนวนที่ส่งเทียบแป้น", new double[] { Value2 }));
            double Value4 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["RESULT_TRY"] + "") ? double.Parse(dtGraphData.Rows[0]["RESULT_TRY"] + "") : 0;
            series.Points.Add(new SeriesPoint("จำนวนที่เทียบแป้นผ่าน", new double[] { Value4 }));

            WebChartControl1.Series.Add(series);

            Series series2 = new Series("S2", ViewType.StackedBar);
            double Value3 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["DOCNOSEND"] + "") ? double.Parse(dtGraphData.Rows[0]["DOCNOSEND"] + "") : 0;
            series2.Points.Add(new SeriesPoint("จำนวนที่ส่งเทียบแป้น", new double[] { Value3 }));

            double Value5 = !string.IsNullOrEmpty(dtGraphData.Rows[0]["NORESULT_TRY"] + "") ? double.Parse(dtGraphData.Rows[0]["NORESULT_TRY"] + "") : 0;
            series2.Points.Add(new SeriesPoint("จำนวนที่เทียบแป้นผ่าน", new double[] { Value5 }));
            WebChartControl1.Series.Add(series2);

            //lblTitle1.Text = "%เทียบแป้น/วัดน้ำเสร็จ";
            //lblTitle2.Text = "%เทียบแป้นผ่าน/ส่งเทียบแป้น";
            //lblTitle3.Text = "%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น";

            ChartTitle chartTitle1 = new ChartTitle();
            chartTitle1.Text = "รายงานผลการนำรถส่งเทียบแป้น";
            chartTitle1.Font = new Font("Tahoma", 15);
            WebChartControl1.Titles.Add(chartTitle1);

            ChartTitle chartTitle2 = new ChartTitle();
            chartTitle2.Text = "เดือน" + cboMonth.Text + " ปี " + cboYear.Text + "";
            chartTitle2.Font = new Font("Tahoma", 13);
            WebChartControl1.Titles.Add(chartTitle2);

            //ChartTitle chartTitle3 = new ChartTitle();
            //chartTitle3.Text = "%เทียบแป้นไม่ผ่าน/ส่งเทียบแป้น";
            //chartTitle3.Font = new Font("Tahoma", 8);
            //chartTitle3.Dock = ChartTitleDockStyle.Right;
            //WebChartControl1.Titles.Add(chartTitle3);

            WebChartControl1.Legend.Visible = false;
            WebChartControl1.SeriesTemplate.ArgumentScaleType = ScaleType.Qualitative;
            WebChartControl1.SeriesTemplate.ValueScaleType = ScaleType.Numerical;
            WebChartControl1.SeriesTemplate.PointOptions.PointView = PointView.Values;
            WebChartControl1.SeriesTemplate.LegendPointOptions.PointView = PointView.Argument;

            ((DevExpress.XtraCharts.XYDiagram)WebChartControl1.Diagram).AxisX.Label.Angle = 0;

            WebChartControl1.DataSource = dtGraphData;
            WebChartControl1.DataBind();



        }


    }
}