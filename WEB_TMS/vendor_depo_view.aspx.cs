﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class vendor_depo_view : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();

            Session["oSternalID"] = null;

            LogUser("9", "R", "เปิดดูข้อมูลหน้า ข้อมูลคลัง ปตท.", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        BindData();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();

                break;

            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STERMINALID");
                string sternalID = Convert.ToString(data);

                Session["oSternalID"] = sternalID;
                xcpn.JSProperties["cpRedirectTo"] = "vendor_depo_desc.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void gvw_Init(object sender, EventArgs e)
    {
        string sLINE = CommonFunction.Get_Value(sql, "SELECT MAX(NLINE) AS MAXSTATUS FROM TWINDOWTIME");
        int nLINE = Convert.ToInt32(sLINE);

        if (nLINE > 0)
        {
            GridViewBandColumn sGridViewBandColumn = new GridViewBandColumn("TimeWindows");
            sGridViewBandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            for (int n = 1; n <= nLINE; n++)
            {
                GridViewDataColumn sGridViewDataColumn = new GridViewDataColumn("T" + n, "เที่ยวที่ " + n);
                sGridViewDataColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.Width = Unit.Percentage(10);

                sGridViewBandColumn.Columns.Add(sGridViewDataColumn);

            }
            gvw.Columns.Insert(2, sGridViewBandColumn);
        }

    }

    void BindData()
    {
        string strsql = "SELECT  ROW_NUMBER () OVER (ORDER BY T.STERMINALID) AS ID1, T.STERMINALID, TS.STERMINALNAME, T.TCARCONFIRM,T.TPLANCONFIRM,T.TCLOSEORDER,T.CSTATUS,R.REGION_DESC {0} FROM ((TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID) LEFT JOIN TREGION r ON T.SREGION = R.REGION) LEFT JOIN TWINDOWTIME w ON T.STERMINALID = W.STERMINALID  WHERE TS.STERMINALNAME LIKE '%' || :oSearch || '%' GROUP BY T.STERMINALID, TS.STERMINALNAME, T.TCARCONFIRM,T.TPLANCONFIRM,T.TCLOSEORDER,T.CSTATUS,R.REGION_DESC";
        string strTimeLine = "";

        string sLINE = CommonFunction.Get_Value(sql, "SELECT MAX(NLINE) AS MAXSTATUS FROM TWINDOWTIME");
        int nLINE = Convert.ToInt32(sLINE);

        if (nLINE > 0)
        {
            for (int i = 1; i <= nLINE; i++)
            {
                strTimeLine += string.Format(",MAX(CASE WHEN W.NLINE = {0} THEN W.TSTART || ' - ' || W.TEND  END) AS t{1}", i, i);
            }
        }

        sds.SelectCommand = string.Format(strsql, strTimeLine);
        sds.SelectParameters.Clear();
        sds.SelectParameters.Add("oSearch", txtSearch.Text);

        sds.DataBind();
        gvw.DataBind();
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
