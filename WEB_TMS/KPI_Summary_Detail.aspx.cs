﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using System.Web.UI.WebControls;

public partial class KPI_Summary_Detail : PageBase
{
    #region + View State +
    private string VendorID
    {
        get
        {
            if (ViewState["VendorID"] != null)
                return ViewState["VendorID"].ToString();
            else
                return string.Empty;
        }
        set
        {
            ViewState["VendorID"] = value;
        }
    }

    private string Year
    {
        get
        {
            if (ViewState["Year"] != null)
                return ViewState["Year"].ToString();
            else
                return string.Empty;
        }
        set
        {
            ViewState["Year"] = value;
        }
    }

    private int ContractID
    {
        get
        {
            if (ViewState["ContractID"] != null)
                return (int)ViewState["ContractID"];
            else
                return 0;
        }
        set
        {
            ViewState["ContractID"] = value;
        }
    }

    private DataTable dtMapping
    {
        get
        {
            if ((DataTable)ViewState["dtMapping"] != null)
                return (DataTable)ViewState["dtMapping"];
            else
                return null;
        }
        set
        {
            ViewState["dtMapping"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] QueryString = decryptedValue.Split('&');

                Year = QueryString[0];
                ContractID = int.Parse(QueryString[1]);
                VendorID = QueryString[2];

                DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DataRow[] Vendor = dtVendor.Select("SVENDORID = " + VendorID);

                DataTable dtContract = ComplainBLL.Instance.ContractSelectBLL(string.Empty);
                DataRow[] Contract = dtContract.Select("SCONTRACTID = " + ContractID.ToString());

                txtYear.Text = Year;
                txtVendor.Text = Vendor[0].ItemArray[2].ToString();
                txtContract.Text = Contract[0].ItemArray[1].ToString();

                this.GetData();
                this.Calculate();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("KPI_Summary.aspx");
    }

    private void GetData()
    {
        try
        {
            bool IsPTT = false;
            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                IsPTT = false;
            else
                IsPTT = true;

            KPI_Helper Helper = new KPI_Helper();
            DataSet ds = Helper.GetDataKPIYear(Year, VendorID, ContractID, IsPTT);
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                UserControl_ucKPIDetail dgvKPI = LoadControl("~/UserControl/ucKPIDetail.ascx") as UserControl_ucKPIDetail;
                dgvKPI.HeaderName = "ชุดประเมิน : " + ds.Tables[i].Rows[0]["HEADER_NAME"].ToString();
                dgvKPI.DataSource = ds.Tables[i];
                dgvKPI.DataBind();

                divGrid.Controls.Add(dgvKPI);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        //try
        //{
        //    DataTable dtHeader = KPIBLL.Instance.KPISelectListHeaderBLL(Year);
        //    DataTable dtDetail = new DataTable();
        //    DataTable dtMapping = new DataTable();
        //    string Point = string.Empty;
        //    string PointBefore = string.Empty;

        //    for (int i = 0; i < dtHeader.Rows.Count; i++)
        //    {
        //        dtDetail = KPIBLL.Instance.KPISelectListDetailBLL(int.Parse(dtHeader.Rows[i]["TOPIC_HEADER_ID"].ToString()));

        //        for (int j = 0; j < dtDetail.Rows.Count; j++)
        //        {
        //            for (int k = 1; k < 13; k++)
        //            {
        //                dtMapping = KPIBLL.Instance.KPIMappingFormSelect2BLL(Year, k, VendorID, ContractID, int.Parse(dtHeader.Rows[i]["TOPIC_HEADER_ID"].ToString()));

        //                if (dtMapping.Rows.Count > 0)
        //                {
        //                    Point = KPI_Helper.Calculate(int.Parse(dtMapping.Rows[j]["FORMULA_ID"].ToString()),
        //                                                 decimal.Parse(dtMapping.Rows[j]["VALUE_A"].ToString()),
        //                                                 decimal.Parse(dtMapping.Rows[j]["VALUE_B"].ToString()),
        //                                                 Year, k, VendorID, ContractID, dtMapping, j, ref PointBefore);

        //                    dtDetail.Rows[j]["MONTH_" + k.ToString()] = Point;
        //                }
        //            }
        //        }

        //        UserControl_ucKPIDetail dgvKPI = LoadControl("~/UserControl/ucKPIDetail.ascx") as UserControl_ucKPIDetail;
        //        dgvKPI.HeaderName = "ชุดประเมิน : " + dtDetail.Rows[0]["HEADER_NAME"].ToString();
        //        dgvKPI.DataSource = dtDetail;
        //        dgvKPI.DataBind();

        //        divGrid.Controls.Add(dgvKPI);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    throw new Exception(ex.Message);
        //}
    }

    private void Calculate()
    {
        try
        {
            decimal Sum = 0;
            int Max = -1;
            int Min = -1;
            decimal Weight = 0;
            decimal WeightSum = 0;
            int MonthCount = 0;
            int ColumnIndex = 0;
            decimal Point = 0;

            decimal PointSum = 0;
            decimal KPITotal = 0;

            DataTable dtCountTotalMonth = new DataTable();
            dtCountTotalMonth.Columns.Add("MONTH");

            foreach (UserControl_ucKPIDetail tmp in divGrid.Controls)
            {
                GridView dgv = (GridView)tmp.FindControl("dgv");

                WeightSum = 0;
                MonthCount = 0;

                for (int i = 0; i < dgv.Rows.Count; i++)
                    WeightSum += decimal.Parse(dgv.Rows[i].Cells[this.GetColumnIndexByName(dgv, "WEIGHT")].Text);

                for (int i = 0; i < dgv.Rows.Count; i++)
                {
                    Sum = 0;
                    Max = -1;
                    Min = -1;
                    Weight = 0;
                    ColumnIndex = 0;
                    Point = 0;

                    Weight = decimal.Parse(dgv.Rows[i].Cells[this.GetColumnIndexByName(dgv, "WEIGHT")].Text);

                    for (int j = 1; j < 13; j++)
                    {
                        ColumnIndex = this.GetColumnIndexByName(dgv, "MONTH_" + j.ToString());
                        if (!string.Equals(dgv.Rows[i].Cells[ColumnIndex].Text.Replace("&nbsp;", string.Empty), string.Empty))
                        {
                            Sum += int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text);

                            if (Min == -1)
                            {
                                Min = int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text);
                                Max = int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text);
                            }
                            else
                            {
                                if (Min > int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text))
                                    Min = int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text);

                                if (Max < int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text))
                                    Max = int.Parse(dgv.Rows[i].Cells[ColumnIndex].Text);
                            }

                            if (i == 0)
                            {
                                MonthCount++;
                                this.CountTotalMonth(ref dtCountTotalMonth, j);
                            }
                        }
                    }

                    if (MonthCount > 0)
                    {
                        int MethodID = 0;

                        Label lblMethodID = (Label)dgv.Rows[i].FindControl("lblMethodID");
                        if (lblMethodID != null)
                            MethodID = int.Parse(lblMethodID.Text);

                        if (MethodID == 1)                                  //Average
                            Point = Math.Round((Sum / MonthCount), 2, MidpointRounding.AwayFromZero);
                        else if (MethodID == 2)                             //Maximum
                            Point = Max;
                        else if (MethodID == 3)                             //Minimum
                            Point = Min;

                        dgv.Rows[i].Cells[this.GetColumnIndexByName(dgv, "POINT")].Text = Point.ToString();

                        PointSum = PointSum + (Point * Weight);
                    }
                }

                Label lbl = (Label)tmp.FindControl("lblKPI");
                lbl.Text = (PointSum / WeightSum).ToString();

                txtKPIYear.Text = (decimal.Parse(txtKPIYear.Text) + decimal.Parse(lbl.Text)).ToString();
                lbl.Text = "ผลประเมินเฉลี่ย : " + (PointSum / WeightSum).ToString();

                KPITotal += (PointSum / WeightSum) * MonthCount;

                PointSum = 0;
            }

            if (dtCountTotalMonth.Rows.Count == 0)
                KPITotal = 0;
            else
                KPITotal = KPITotal / dtCountTotalMonth.Rows.Count;
            txtKPIYear.Text = KPITotal.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CountTotalMonth(ref DataTable dtMonth, int Month)
    {
        try
        {
            bool Found = false;
            for (int i = 0; i < dtMonth.Rows.Count; i++)
            {
                if (string.Equals(dtMonth.Rows[i]["MONTH"].ToString(), Month.ToString()))
                {
                    Found = true;
                    break;
                }
            }

            if (!Found)
                dtMonth.Rows.Add(Month.ToString());
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int GetColumnIndexByName(GridView grid, string name)
    {
        //for (int i = 0; i < grid.Columns.Count; i++)
        //{
        //    if (grid.Columns[i].HeaderText.ToLower().Trim() == name.ToLower().Trim())
        //    {
        //        return i;
        //    }
        //}


        int i = 0;
        System.Web.UI.WebControls.BoundField tmp;
        foreach (var bfName in grid.Columns)
        {
            if (string.Equals(bfName.GetType().ToString(), "System.Web.UI.WebControls.BoundField"))
            {
                tmp = (System.Web.UI.WebControls.BoundField)bfName;
                string currentDataField = tmp.DataField;
                if (string.Equals(tmp.DataField, name))
                    return i;
            }

            i++;
        }

        return -1;
    }
}