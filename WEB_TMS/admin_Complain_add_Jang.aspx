﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_Complain_add_Jang.aspx.cs" Inherits="admin_Complain_add_Jang" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style16
        {
            color: #CC0000;
            font-weight: bold;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function VisibleControl() {

            var bool = txtFilePath0.GetValue() == "" || txtFilePath0.GetValue() == null;
            uploader0.SetClientVisible(bool);
            txtFileName0.SetClientVisible(!bool);
            btnView0.SetEnabled(!bool);
            btnDelFile0.SetEnabled(!bool);

            bool = txtFilePath1.GetValue() == "" || txtFilePath1.GetValue() == null;
            uploader1.SetClientVisible(bool);
            txtFileName1.SetClientVisible(!bool);
            btnView1.SetEnabled(!bool);
            btnDelFile1.SetEnabled(!bool);

            bool = txtFilePath2.GetValue() == "" || txtFilePath2.GetValue() == null;
            uploader2.SetClientVisible(bool);
            txtFileName2.SetClientVisible(!bool);
            btnView2.SetEnabled(!bool);
            btnDelFile2.SetEnabled(!bool);

            bool = txtFilePath3.GetValue() == "" || txtFilePath3.GetValue() == null;
            uploader3.SetClientVisible(bool);
            txtFileName3.SetClientVisible(!bool);
            btnView3.SetEnabled(!bool);
            btnDelFile3.SetEnabled(!bool);

            bool = txtFilePath4.GetValue() == "" || txtFilePath4.GetValue() == null;
            uploader4.SetClientVisible(bool);
            txtFileName4.SetClientVisible(!bool);
            btnView4.SetEnabled(!bool);
            btnDelFile4.SetEnabled(!bool);

            bool = txtFilePath5.GetValue() == "" || txtFilePath5.GetValue() == null;
            uploader5.SetClientVisible(bool);
            txtFileName5.SetClientVisible(!bool);
            btnView5.SetEnabled(!bool);
            btnDelFile5.SetEnabled(!bool);

            bool = txtFilePath6.GetValue() == "" || txtFilePath6.GetValue() == null;
            uploader6.SetClientVisible(bool);
            txtFileName6.SetClientVisible(!bool);
            btnView6.SetEnabled(!bool);
            btnDelFile6.SetEnabled(!bool);

            bool = txtFilePath7.GetValue() == "" || txtFilePath7.GetValue() == null;
            uploader7.SetClientVisible(bool);
            txtFileName7.SetClientVisible(!bool);
            btnView7.SetEnabled(!bool);
            btnDelFile7.SetEnabled(!bool);
        }

        function VisibleTab() {
            if (txtChkEdit.GetValue() == "0") {
                switch (txtTabVisible.GetValue()) {
                    case "1":
                        PageControl.tabs[1].SetEnabled(false);
                        PageControl.tabs[2].SetEnabled(false);
                        break;
                    case "2":
                        PageControl.tabs[1].SetEnabled(true);
                        PageControl.tabs[2].SetEnabled(false);
                        PageControl.SetActiveTabIndex(1);

                        break;
                    case "3":
                        PageControl.tabs[1].SetEnabled(true);

                        if (txtChkTerminal.GetValue() == "1") {
                            PageControl.tabs[2].SetEnabled(false);

                        } else {
                            PageControl.tabs[2].SetEnabled(true);
                            PageControl.SetActiveTabIndex(2);
                        }
                        break;
                }
            }


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;VisibleTab();}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <dx:ASPxTextBox ID="txtTabVisible" ClientInstanceName="txtTabVisible" Text="1" Width="10px"
                    ClientVisible="false" runat="server">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="txtChkEdit" ClientInstanceName="txtChkEdit" Text="0" Width="10px"
                    ClientVisible="false" runat="server">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="txtChkTerminal" ClientInstanceName="txtChkTerminal" Text="0"
                    Width="10px" ClientVisible="false" runat="server">
                </dx:ASPxTextBox>
                <dx:ASPxPageControl ID="ASPxPageControl1" ClientInstanceName="PageControl" runat="server"
                    Width="100%" ActiveTabIndex="0" 
                    OnActiveTabChanged="ASPxPageControl1_ActiveTabChanged">
                    <ClientSideEvents TabClick="function (s,e){if(!ASPxClientEdit.ValidateGroup('add')) e.cancel = true }" />
                    <TabPages>
                        <dx:TabPage Name="t1" Text="ข้อมูลทั่วไป">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF" style="width: 20%">
                                                <span class="style16">ข้อมูลทั่วไป</span>
                                            </td>
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 20%;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 30%;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                เลข Outbound
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" MaxLength="10"
                                                    ClientInstanceName="cboDelivery" EnableCallbackMode="True" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SDELIVERYNO" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){var svendor = s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'); var scontract = s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID');hideVendor.SetValue(svendor);cboVendor.SetValue(svendor);cboHeadRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')); txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));hideContract.SetValue(scontract);cboContract.SetValue(scontract);}else{hideContract.SetValue('');hideVendor.SetValue(svendor);};xcpn.PerformCallback();}" />
                                                    <%--<ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){cboVendor.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'));cboHeadRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')); txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO'));txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));cboContract.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID'));};xcpn.PerformCallback();}" />--%>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="Outbound No" FieldName="SDELIVERYNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนรถ(ท้าย)" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="STRUCKID" FieldName="STRUCKID" Width="60px" />
                                                        <dx:ListBoxColumn Caption="รหัสผปก." FieldName="SVENDORID" Width="80px" />
                                                        <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="60px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ประเภทผลิตภัณฑ์
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxRadioButtonList ID="rblTypeProduct" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="น้ำมัน" Value="O" />
                                                        <dx:ListEditItem Text="ก๊าซ" Value="G" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <span style="text-align: left">ทะเบียนรถ (หัว)</span>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cboHeadRegist" runat="server" CallbackPageSize="30" ClientInstanceName="cboHeadRegist"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboHeadRegist_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboHeadRegist_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SHEADREGISTERNO" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){var svendor = s.GetItem(s.GetSelectedIndex()).GetColumnText('SVENDORID'); var scontract = s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID');txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) ;cboContract.SetValue(scontract) ; txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));hideContract.SetValue(scontract);cboVendor.SetValue(svendor);}else{txtTrailerRegist.SetValue('');txtTruckID.SetValue('');hideContract.SetValue('');hideVendor.SetValue('');};xcpn.PerformCallback();}" />
                                                    <%--<ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtTrailerRegist.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRAILERREGISTERNO')) ;cboContract.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCONTRACTID')) ; txtTruckID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRUCKID'));};xcpn.PerformCallback();}" />--%><%--cboVendor.PerformCallback();cboDelivery.PerformCallback();cboContract.PerformCallback();--%>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" Width="100px" />
                                                        <dx:ListBoxColumn Caption="STRUCKID" FieldName="STRUCKID" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสผู้ขนส่ง" FieldName="SVENDORID" Width="80px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ทะเบียนรถ (ท้าย )
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtTrailerRegist" runat="server" ClientEnabled="False" ClientInstanceName="txtTrailerRegist"
                                                    Width="180px">
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtTruckID" runat="server" ClientInstanceName="txtTruckID" ClientVisible="False"
                                                    Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                บริษัทผู้ประกอบการขนส่ง
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxTextBox ID="hideVendor" ClientInstanceName="hideVendor" runat="server" Width="10px"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {xcpn.PerformCallback();}" />
                                                    <%--cboHeadRegist.PerformCallback();cboDelivery.PerformCallback();cboContract.PerformCallback();cmbPersonalNo.PerformCallback();--%>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                                                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                เลขที่สัญญา&nbsp<span style="color: Red">*</span>
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="hideContract" runat="server" ClientInstanceName="hideContract"
                                                    ClientVisible="false" Width="10px">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxComboBox ID="cboContract" runat="server" CallbackPageSize="30" ClientInstanceName="cboContract"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cboContract_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cboContract_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SCONTRACTID" Width="180px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="150px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุสัญญา" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsContract" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                เลขบัตรประชาชน พขร.
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientInstanceName="cmbPersonalNo"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SPERSONELNO" Width="150px">
                                                    <ClientSideEvents ValueChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){cboVendor.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('STRANS_ID'));txtEmployee.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('FULLNAME'));hideDriverID.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SEMPLOYEEID'));}else{hideDriverID.SetValue('');txtEmployee.SetValue('');hideContract.SetValue('');hideVendor.SetValue('');};xcpn.PerformCallback();}" />
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="เลขบัตรประชาชน" FieldName="SPERSONELNO" />
                                                        <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                        <dx:ListBoxColumn Caption="รหัสผู้ขนส่ง" FieldName="STRANS_ID" />
                                                        <dx:ListBoxColumn Caption="รหัสพนักงานขับ" FieldName="SEMPLOYEEID" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                ชื่อพขร. ที่ถูกร้องเรียน
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="hideDriverID" ClientInstanceName="hideDriverID" runat="server"
                                                    Width="10px" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtEmployee" runat="server" ClientInstanceName="txtEmployee"
                                                    Width="170px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                วันที่ขนส่ง
                                            </td>
                                            <td align="left" class="style27">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateTrans" runat="server" SkinID="xdte">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุเวลาแจ้งเหตุ" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            เวลา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="teTimeTrans" runat="server" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                        <td>
                                                            น.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                สถานที่เกิดเหตุ
                                            </td>
                                            <td align="left" class="style18">
                                                <dx:ASPxTextBox ID="txtComplainAddress" runat="server" Width="220px">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                คลังต้นทาง
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cbxOrganiz" runat="server" CallbackPageSize="10" ClientInstanceName="cbxOrganiz"
                                                    EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                                                    OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                                                    SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="150px">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" Width="80px" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" Width="100px" />
                                                    </Columns>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style27">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <span class="style16">รายละเอียดและสาเหตุ</span>
                                            </td>
                                            <td align="left" class="style27">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <span style="text-align: left">ชื่อผู้ร้องเรียน<font color="#ff0000">*</font></span>
                                            </td>
                                            <td align="left" class="style27">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtFName" runat="server" Width="200px" CssClass="dxeLineBreakFix">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุชื่อผู้ร้องเรียน" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtLName" runat="server" Width="10px" CssClass="dxeLineBreakFix"
                                                                Visible="false">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุนามสกุลผู้ร้องเรียน" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                วันที่รับร้องเรียน
                                            </td>
                                            <td align="left" class="style27">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateComplain" runat="server" SkinID="xdte">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุวันที่รับร้องเรียน" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            เวลา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="teTimeComplain" runat="server" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                        <td>
                                                            น.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <%--ประเภทผู้ร้องเรียน --%>
                                            </td>
                                            <td align="left" class="style27">
                                                <%--                      <dx:ASPxRadioButtonList ID="rblTypePerson" runat="server" ClientVisible="false" RepeatDirection="Horizontal"
                                                    SelectedIndex="0" SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="ลูกค้า" Value="1" />
                                                        <dx:ListEditItem Text="บุคคลทั่วไป" Value="2" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>--%>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                เรื่องที่ร้องเรียน <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27">
                                                <%-- <dx:ASPxTextBox ID="txtTopic" runat="server" Width="170px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุเรื่องที่ร้องเรียน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>--%>
                                                <dx:ASPxComboBox ID="cmbTopic" runat="server" Width="170px" SelectedIndex="0" DataSourceID="sdsTopicC1"
                                                    ValueField="NID" TextField="SDETAIL" ValueType="System.String">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณาระบุข้อมูล" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTopicC1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT NID,SDETAIL,STYPEID FROM LSTDETAIL WHERE STYPEID = 'C_1_1' ORDER BY NID">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ประเภทเรื่องร้องเรียน - หลัก <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxComboBox ID="cboComplainType" runat="server" DataSourceID="sdsComplainType"
                                                    TextField="SCOMPLAINTYPENAME" ValueField="SCOMPLAINTYPEID" ValueType="System.String">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือกประเภทเรื่องร้องเรียน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsComplainType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT SCOMPLAINTYPEID, SCOMPLAINTYPENAME FROM TCOMPLAINTYPE">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;&nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                สาเหตุ / รายละเอียด <font color="#ff0000">*</font>
                                            </td>
                                            <td align="left" class="style27" colspan="3">
                                                <dx:ASPxMemo ID="txtDetail" runat="server" Height="160px" Width="500px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุสาเหตุ/รายละเอียด" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                ผู้บันทึก
                                            </td>
                                            <td align="left" class="style27">
                                                <dx:ASPxLabel ID="lblComplainBy" runat="server">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" class="style18">
                                                &nbsp;
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix" Visible="false">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false;txtTabVisible.SetValue('2'); xcpn.PerformCallback('Save');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_Complain_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t2" Text="การดำเนินการแก้ไข">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td class="style28" width="30%">
                                                การดำเนินการ<font color="#ff0000">*</font>&nbsp;&nbsp;
                                            </td>
                                            <td align="left" class="style27" colspan="2">
                                                <dx:ASPxMemo ID="txtOperate" runat="server" Height="150px" Width="500px">
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณากรอกการดำเนินการ" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxMemo>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                <span style="text-align: left">วิธีการป้องกันการเกิดปัญหาซ้ำ </span>
                                            </td>
                                            <td align="left" class="style27" colspan="2">
                                                <dx:ASPxMemo ID="txtProtect" runat="server" Height="150px" Width="500px">
                                                </dx:ASPxMemo>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28">
                                                พนักงานผู้รับผิดชอบ
                                            </td>
                                            <td align="left" class="style27" width="35%">
                                                <dx:ASPxTextBox ID="txtsEmployee" runat="server" Width="180px">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td width="35%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style28" colspan="3">
                                                <b>เอกสารที่ต้องแนบการพิจารณา</b>Allowed file types:
                                                <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %>ฯลฯ<br>
                                                Max file size:
                                                <%= Resources.CommonResource.TooltipMaxFileSize1MB %>
                                            </td>
                                            <td align="left" class="style18">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                1) บันทึกการสอบสวน/คำให้การโดย ปง.<br />
                                                หรือ คลัง
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel0" runat="server" ClientInstanceName="uploader0"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ClientSideEvents TextChanged="function(s,e){uploader0.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png,.zip,.rar หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath0.SetValue((e.callbackData+'').split('|')[0]);txtFileName0.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName0" runat="server" Width="300px" ClientInstanceName="txtFileName0"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath0" runat="server" Width="220px" ClientInstanceName="txtFilePath0"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath0.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile0" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile0" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath0.GetValue() +';1');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2) บันทึกยอมรับผิด โดย พขร.
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel1" runat="server" ClientInstanceName="uploader1"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader1.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath1.SetValue((e.callbackData+'').split('|')[0]);txtFileName1.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName1" runat="server" Width="300px" ClientInstanceName="txtFileName1"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath1" runat="server" Width="220px" ClientInstanceName="txtFilePath1"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath1.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile1" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath1.GetValue() +';2');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3) ใบกำกับการขนส่ง
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel2" runat="server" ClientInstanceName="uploader2"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader2.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath2.SetValue((e.callbackData+'').split('|')[0]);txtFileName2.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName2" runat="server" Width="300px" ClientInstanceName="txtFileName2"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath2" runat="server" Width="220px" ClientInstanceName="txtFilePath2"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath2.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile2" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile2" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath2.GetValue() +';3');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                4) สรุปข้อมูล GPS
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel3" runat="server" ClientInstanceName="uploader3"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader3.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath3.SetValue((e.callbackData+'').split('|')[0]);txtFileName3.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName3" runat="server" Width="300px" ClientInstanceName="txtFileName3"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath3" runat="server" Width="220px" ClientInstanceName="txtFilePath3"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath3.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile3" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile3" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath3.GetValue() +';4');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                5) รูปถ่าย
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel4" runat="server" ClientInstanceName="uploader4"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader4.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath4.SetValue((e.callbackData+'').split('|')[0]);txtFileName4.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName4" runat="server" Width="300px" ClientInstanceName="txtFileName4"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath4" runat="server" Width="220px" ClientInstanceName="txtFilePath4"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath4.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile4" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile4" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath4.GetValue() +';5');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                6) ใบตรวจสอบสภาพรถขนส่ง
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel5" runat="server" ClientInstanceName="uploader5"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader5.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath5.SetValue((e.callbackData+'').split('|')[0]);txtFileName5.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName5" runat="server" Width="300px" ClientInstanceName="txtFileName5"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath5" runat="server" Width="220px" ClientInstanceName="txtFilePath5"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath5.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile5" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile5" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath5.GetValue() +';6');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                7) เอกสารอื่นๆ (ถ้ามี)
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel6" runat="server" ClientInstanceName="uploader6"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader6.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath6.SetValue((e.callbackData+'').split('|')[0]);txtFileName6.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName6" runat="server" Width="300px" ClientInstanceName="txtFileName6"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath6" runat="server" Width="220px" ClientInstanceName="txtFilePath6"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView6" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView6" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath6.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile6" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile6" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath6.GetValue() +';7');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                8) รายงานบันทึกการแก้ไขป้องกัน
                                            </td>
                                            <td>
                                                <dx:ASPxUploadControl ID="uplExcel7" runat="server" ClientInstanceName="uploader7"
                                                    NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uplExcel_FileUploadComplete">
                                                    <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                        AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                    </ValidationSettings>
                                                    <ClientSideEvents TextChanged="function(s,e){uploader7.Upload();}" FileUploadComplete="function(s, e) {if(e.callbackData ==''){dxWarning('แจ้งเตือน','ระบบสามารถ import file ได้เฉพาะ นามสกุล .xlsx,.xls,.doc,.docx,.pdf,.txt,.jpg,.jpeg,.jpg,.gif,.bmp,.png หรือ ขนาดไฟล์ใหญ่เกินกว่าที่ระบบกำหนดไว้ กรุณาตรวจสอบใหม่อีกที');}else{txtFilePath7.SetValue((e.callbackData+'').split('|')[0]);txtFileName7.SetValue((e.callbackData+'').split('|')[1]);VisibleControl();} }">
                                                    </ClientSideEvents>
                                                    <BrowseButton Text="แนบไฟล์">
                                                    </BrowseButton>
                                                </dx:ASPxUploadControl>
                                                <dx:ASPxTextBox ID="txtFileName7" runat="server" Width="300px" ClientInstanceName="txtFileName7"
                                                    ClientEnabled="false" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtFilePath7" runat="server" Width="220px" ClientInstanceName="txtFilePath7"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td>
                                                <dx:ASPxButton ID="btnView7" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnView7" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str='+ txtFilePath7.GetValue());}" />
                                                    <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btnDelFile7" runat="server" CausesValidation="False" AutoPostBack="false"
                                                    EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                    ClientInstanceName="btnDelFile7" CssClass="dxeLineBreakFix" Width="25px" ClientEnabled="false">
                                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('deleteFile;'+ txtFilePath7.GetValue() +';8');}" />
                                                    <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                    </Image>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                กำหนดแล้วเสร็จ
                                            </td>
                                            <td>
                                                <dx:ASPxDateEdit ID="dteFinish" runat="server" SkinID="xdte">
                                                </dx:ASPxDateEdit>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <dx:ASPxButton ID="ASPxButton3" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix" Visible="false">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; txtTabVisible.SetValue('3'); xcpn.PerformCallback('Save1');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton4" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_Complain_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="t3" Text="บันทึกตัดคะแนน">
                            <ContentCollection>
                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                                        <tr>
                                            <td bgcolor="#FFFFFF" width="30%">
                                                หัวข้อตัดคะแนนประเมิน<font color="#ff0000">*</font>
                                            </td>
                                            <td colspan="7">
                                                <dx:ASPxComboBox ID="cboTopicName" runat="server" Width="100%" DataSourceID="sdsTopicName"
                                                    ItemStyle-Wrap="True" ValueField="STOPICID" TextField="STOPICNAME" TextFormatString="{0}">
                                                    <ClientSideEvents ValueChanged="function(s,e){txtCal2.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCOL02'));txtCal3.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCOL03'));txtCal4.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCOL04'));
                                                    txtCal6.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SCOL06'));txtMultipleImpact.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NMULTIPLEIMPACT'));txtPoint.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('NPOINT'));txtVersion.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SVERSION'));
                                                    }"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="ชื่อหัวข้อ" FieldName="STOPICNAME" Width="400px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="SCOL02" Width="10px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="SCOL03" Width="10px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="SCOL04" Width="10px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="SCOL06" Width="10px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="NMULTIPLEIMPACT" Width="20px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="NPOINT" Width="20px" />
                                                        <dx:ListBoxColumn Caption=" " FieldName="SVERSION" Width="10px" />
                                                    </Columns>
                                                    <ItemStyle Wrap="True"></ItemStyle>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาระบุหัวข้อตัดคะแนนประเมิน" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="sdsTopicName" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                    SelectCommand="SELECT t.STOPICID, t.STOPICNAME, T.SCOL02,T.SCOL03,T.SCOL04,T.SCOL06,T.NMULTIPLEIMPACT,T.NPOINT, T.SVERSION,T.CPROCESS FROM TTOPIC t WHERE CACTIVE = '1' AND CPROCESS LIKE '%,080%' ORDER BY t.STOPICID">
                                                </asp:SqlDataSource>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>มุมมองที่ใช้ประเมินความรุนแรง</span>
                                            </td>
                                            <td align="center" width="7%">
                                                <span>ภาพลักษณ์<br />
                                                    องค์กร</span>
                                            </td>
                                            <td align="center" width="7%">
                                                <span>ความพึงพอใจ<br />
                                                    ลูกค้า</span>
                                            </td>
                                            <td align="center" width="7%">
                                                <span>กฎ/ระเบียบฯ</span>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF" width="10%">
                                                <span>แผนงานขนส่ง</span>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF" width="10%">
                                                <span>ผลคูณ<br />
                                                    ความรุนแรง</span>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF" width="10%">
                                                <span>หักคะแนน<br />
                                                    ต่อครั้ง</span>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF" width="20%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span>ตัวคูณความรุนแรง</span>
                                            </td>
                                            <td align="center">
                                                <dx:ASPxTextBox ID="txtCal2" runat="server" Width="80px" ClientInstanceName="txtCal2"
                                                    ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                                        <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>">
                                                        </RegularExpression>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center">
                                                <dx:ASPxTextBox ID="txtCal3" runat="server" Width="80px" ClientInstanceName="txtCal3"
                                                    ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                                        <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>">
                                                        </RegularExpression>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center">
                                                <dx:ASPxTextBox ID="txtCal4" runat="server" Width="80px" ClientInstanceName="txtCal4"
                                                    ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                                        <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>">
                                                        </RegularExpression>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF">
                                                <dx:ASPxTextBox ID="txtCal6" runat="server" Width="80px" ClientInstanceName="txtCal6"
                                                    ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                                        <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_DecimalNumber %>">
                                                        </RegularExpression>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF">
                                                <dx:ASPxTextBox ID="txtMultipleImpact" runat="server" Width="80px" ClientInstanceName="txtMultipleImpact"
                                                    ReadOnly="True" ForeColor="#A6A6A6" ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6" />
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF">
                                                <dx:ASPxTextBox ID="txtPoint" runat="server" ClientInstanceName="txtPoint" Width="80px"
                                                    ForeColor="#A6A6A6" ClientEnabled="False">
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล" />
                                                        <RegularExpression ErrorText="กรุณาระบุตัวเลข" ValidationExpression="<%$ Resources:ValidationResource, Valid_MinusNumber %>">
                                                        </RegularExpression>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                    <Border BorderColor="#A6A6A6"></Border>
                                                </dx:ASPxTextBox>
                                            </td>
                                            <td align="center" bgcolor="#FFFFFF">
                                                <dx:ASPxTextBox ID="txtVersion" runat="server" ClientInstanceName="txtVersion" Width="80px"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ตัดคะแนน
                                            </td>
                                            <td colspan="3">
                                                <dx:ASPxRadioButtonList ID="rblCut" runat="server" EnableTheming="True" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ตัด" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="ไม่ตัด" Value="0" />
                                                    </Items>
                                                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="add" ErrorFrameStyle-ForeColor="Red"
                                                        SetFocusOnError="True" Display="Dynamic">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                สถานะข้อร้องเรียน<font color="#ff0000">*</font>
                                            </td>
                                            <td colspan="3">
                                                <dx:ASPxRadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Text="ดำเนินการ" Value="1" Selected="true" />
                                                        <dx:ListEditItem Text="อุทธรณ์" Value="2" />
                                                        <dx:ListEditItem Text="ปิดเรื่อง" Value="3" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                        ValidationGroup="add">
                                                        <ErrorFrameStyle ForeColor="Red">
                                                        </ErrorFrameStyle>
                                                        <RequiredField ErrorText="กรุณาเลือก" IsRequired="True" />
                                                    </ValidationSettings>
                                                </dx:ASPxRadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                วันที่ปิดเรื่อง
                                            </td>
                                            <td align="left" colspan="4">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateClose" runat="server" SkinID="xdte">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณาระบุวันที่เกิดเหตุ" IsRequired="True" />
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td>
                                                            เวลา
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTimeEdit ID="teTimeClose" runat="server" Width="60px">
                                                            </dx:ASPxTimeEdit>
                                                        </td>
                                                        <td>
                                                            น.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                            <td align="left" bgcolor="#FFFFFF">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--OnPreRender="txtPassword_PreRender"--%>
                                            <td bgcolor="#FFFFFF">
                                                ผู้บันทึก
                                            </td>
                                            <td colspan="6">
                                                <dx:ASPxLabel ID="lblUserCreate" runat="server">
                                                </dx:ASPxLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8" align="center">
                                                <dx:ASPxButton ID="ASPxButton5" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix" Visible="false">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save2');}" />
                                                </dx:ASPxButton>
                                                &nbsp;
                                                <dx:ASPxButton ID="ASPxButton6" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'admin_Complain_lst.aspx'; }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF" align="left" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
