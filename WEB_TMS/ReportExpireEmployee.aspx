﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportExpireEmployee.aspx.cs" Inherits="ExpireEmployee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td style="background-color: #D3D3D3">
                            ช่วงเวลาที่ต้องการค้นหา :
                        </td>
                        <td colspan="2">
                            <div style="float: left">
                                &nbsp;วันที่เริ่มต้น&nbsp;
                            </div>
                            <div style="float: left">
                                <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                                </dx:ASPxDateEdit>
                            </div>
                            <div style="float: left">
                                &nbsp;วันที่สิ้นสุด&nbsp;
                            </div>
                            <div style="float: left">
                                <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                                </dx:ASPxDateEdit>
                            </div>
                            <div style="float: left">
                                &nbsp;ช่วงอายุ&nbsp;
                            </div>
                            <div style="float: left">
                                <dx:ASPxComboBox ID="cbxYear" runat="server" Width="80px" ValueType="System.String"
                                    AutoPostBack="false" EnableIncrementalFiltering="true" EnableCallbackMode="false">
                                    <ClientSideEvents ValueChanged="function(s,e){if(s.GetSelectedIndex() + '' == '0'){cbxYearEnd.SetVisible(false)}else{cbxYearEnd.SetVisible(true)}}" />
                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                        ValidationGroup="add">
                                        <ErrorFrameStyle ForeColor="Red">
                                        </ErrorFrameStyle>
                                        <RequiredField ErrorText="กรุณาระบุช่วงอายุ" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                            <div style="float: left">
                                &nbsp;ถึง&nbsp;
                            </div>
                            <div style="float: left">
                                <dx:ASPxComboBox ID="cbxYearEnd" ClientInstanceName="cbxYearEnd" runat="server" Width="80px" ValueType="System.String"
                                    AutoPostBack="false" EnableIncrementalFiltering="true" EnableCallbackMode="false">
                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                        ValidationGroup="add">
                                        <ErrorFrameStyle ForeColor="Red">
                                        </ErrorFrameStyle>
                                        <RequiredField ErrorText="กรุณาระบุช่วงอายุ" IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                            <div style="float: left">
                                &nbsp;ปี&nbsp;
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #D3D3D3">
                            &nbsp;
                        </td>
                        <td>
                            <div style="float: left">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_search" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Search');}" />
                                </dx:ASPxButton>
                            </div>
                            <div style="float: left">
                                &nbsp;</div>
                            <div style="float: left">
                                <dx:ASPxButton ID="ASPxButton2" runat="server" SkinID="_close">
                                    <ClientSideEvents Click="function(s,e){ window.location = 'Report.aspx'}" />
                                </dx:ASPxButton>
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewerID="ReportViewer1"
                                ShowDefaultButtons="False" Width="80%">
                                <Items>
                                    <dx:ReportToolbarButton ItemKind="Search" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="PrintReport" />
                                    <dx:ReportToolbarButton ItemKind="PrintPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                    <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                    <dx:ReportToolbarLabel ItemKind="PageLabel" />
                                    <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                    </dx:ReportToolbarComboBox>
                                    <dx:ReportToolbarLabel ItemKind="OfLabel" />
                                    <dx:ReportToolbarTextBox ItemKind="PageCount" />
                                    <dx:ReportToolbarButton ItemKind="NextPage" />
                                    <dx:ReportToolbarButton ItemKind="LastPage" />
                                    <dx:ReportToolbarSeparator />
                                    <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                                    <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                                    <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                        <Elements>
                                            <dx:ListElement Value="pdf" />
                                            <dx:ListElement Value="xls" />
                                            <dx:ListElement Value="xlsx" />
                                            <dx:ListElement Value="rtf" />
                                            <dx:ListElement Value="mht" />
                                            <dx:ListElement Value="html" />
                                            <dx:ListElement Value="txt" />
                                            <dx:ListElement Value="csv" />
                                            <dx:ListElement Value="png" />
                                        </Elements>
                                    </dx:ReportToolbarComboBox>
                                </Items>
                                <Styles>
                                    <LabelStyle>
                                        <Margins MarginLeft="3px" MarginRight="3px" />
                                    </LabelStyle>
                                </Styles>
                            </dx:ReportToolbar>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <dx:ReportViewer ID="ReportViewer1" runat="server">
                            </dx:ReportViewer>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
