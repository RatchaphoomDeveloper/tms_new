﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using System.Web.UI.HtmlControls;
using GemBox.Spreadsheet;
using System.IO;

public partial class User : PageBase
{
    DataSet ds;
    #region + View State +
    private DataTable dtSoldTo
    {
        get
        {
            if ((DataTable)ViewState["dtSoldTo"] != null)
                return (DataTable)ViewState["dtSoldTo"];
            else
                return null;
        }
        set
        {
            ViewState["dtSoldTo"] = value;
        }
    }
    private string ShipID
    {
        get
        {
            if ((string)ViewState["ShipID"] != null)
                return (string)ViewState["ShipID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ShipID"] = value;
        }
    }

    private string SoldID
    {
        get
        {
            if ((string)ViewState["SoldID"] != null)
                return (string)ViewState["SoldID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["SoldID"] = value;
        }
    }

    private DataTable dtDataTab1
    {
        get
        {
            if ((DataTable)ViewState["dtDataTab1"] != null)
                return (DataTable)ViewState["dtDataTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtDataTab1"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }

    private DataTable dtCGroup
    {
        get
        {
            if ((DataTable)ViewState["dtCGroup"] != null)
                return (DataTable)ViewState["dtCGroup"];
            else
                return null;
        }
        set
        {
            ViewState["dtCGroup"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdSearchTab1.Enabled = false;
                cmdRefresh.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdAdd.Enabled = false;
                dgvData.Columns[10].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadUserGroup();
            this.LoadStatus();
            this.LoadDepartment();
            ddlUnitNameTab1.Enabled = false;

            dgvSearch.DataSource = new DataTable();
            dgvSearch.DataBind();

            ddlUnitNameTab1.Visible = true;
            txtShipTo.Visible = false;
            aShipTo.Visible = false;
            aShipToClear.Visible = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void aShipTo_ServerClick(object sender, EventArgs e)
    {
        this.GetSoldTo();
    }

    private void GetSoldTo()
    {
        try
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowOTP", "$('#ShowOTP').modal();", true);
            updOTP.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void aShipToClear_ServerClick(object sender, EventArgs e)
    {
        txtShipTo.Text = string.Empty;
        ShipID = String.Empty;
        SoldID = String.Empty;
    }

    protected void dgvSearch_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            ImageButton imgButton = e.CommandSource as ImageButton;
            if (imgButton != null)
            {
                int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;

                if (string.Equals(e.CommandName, "ChooseData"))
                {
                    SoldID = dgvSearch.DataKeys[Index]["SOLD_ID"].ToString();
                    ShipID = dgvSearch.DataKeys[Index]["SHIP_ID"].ToString();

                    //txtSoldTo.Text = dgvSearch.DataKeys[Index]["SOLD_TO"].ToString();
                    txtShipTo.Text = dgvSearch.DataKeys[Index]["SHIP_NAME"].ToString();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowOTP').modal('hide');</script>", false);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ShowOTP().Hide();", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.Equals(txtInput.Value.Trim(), string.Empty))
                throw new Exception(string.Format("คำค้นหา"));

            if (txtInput.Value.Trim().Length < 5)
                throw new Exception("คำค้นหา ต้องมีความยาวอย่างน้อย 5 ตัวอักษร");

            dtSoldTo = UserBLL.Instance.SoldToSelectBLL("%" + txtInput.Value.Trim() + "%");
            GridViewHelper.BindGridView(ref dgvSearch, dtSoldTo, false);
            updOTP.Update();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadDepartment()
    {
        try
        {
            DataTable dtDepartment = DepartmentBLL.Instance.DepartmentSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlDepartment, dtDepartment, "DEPARTMENT_ID", "DEPARTMENT_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void LoadDivision(string Department)
    {
        try
        {
            DataTable dtDivision = DivisionBLL.Instance.DivisionSelectBLL(Department);
            DropDownListHelper.BindDropDownList(ref ddlDivision, dtDivision, "DIVISION_ID", "DIVISION_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadStatus()
    {
        try
        {
            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'USER_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlStatusTab1, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadUserGroup()
    {
        try
        {
            DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSearchTab1_Click(object sender, EventArgs e)
    {
        this.Search(dgvData, dtDataTab1, this.GetConditionSearchTab1());
    }

    private void Search(GridView dgv, DataTable dtData, string Condition)
    {
        try
        {
            dtData = UserBLL.Instance.UserSelectBLL(Condition);
            dtDataTab1 = dtData.Copy();
            GridViewHelper.BindGridView(ref dgv, dtData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string GetConditionSearchTab1()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (!string.Equals(txtUserNameTab1.Text.Trim(), string.Empty))
                sb.Append(" AND SUSERNAME LIKE '%" + txtUserNameTab1.Text.Trim() + "%'");

            if (ddlUnitNameTab1.SelectedIndex > 0 || !string.Equals(txtShipTo.Text.Trim(), string.Empty))
            {
                string cond = ddlUserGroup.SelectedValue == "20" ? ShipID : ddlUnitNameTab1.SelectedValue;
                sb.Append(" AND SVENDORID = '" + cond + "'");
            }

            if (!string.Equals(txtFirstNameTab1.Text.Trim(), string.Empty))
                sb.Append(" AND SFIRSTNAME LIKE '%" + txtFirstNameTab1.Text.Trim() + "%'");

            if (!string.Equals(txtLastNameTab1.Text.Trim(), string.Empty))
                sb.Append(" AND SLASTNAME LIKE '%" + txtLastNameTab1.Text.Trim() + "%'");

            if (!string.Equals(txtEmailTab1.Text.Trim(), string.Empty))
                sb.Append(" AND SEMAIL LIKE '%" + txtEmailTab1.Text.Trim() + "%'");

            if (ddlStatusTab1.SelectedIndex > 0)
                sb.Append(" AND CACTIVE = '" + ddlStatusTab1.SelectedValue + "'");

            if (ddlUserGroup.SelectedIndex > 0)
                sb.Append(" AND USERGROUP_ID = '" + ddlUserGroup.SelectedValue + "'");

            if (ddlDepartment.SelectedIndex > 0)
                sb.Append(" AND DEPARTMENT_ID = '" + ddlDepartment.SelectedValue + "'");

            if (ddlDivision.SelectedIndex > 0)
                sb.Append(" AND DIVISION_ID = '" + ddlDivision.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvData.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvData, dtDataTab1);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlUserGroup.SelectedIndex > 0)
        {
            dtCGroup = UserBLL.Instance.getCGroupFromUserGroupID(" AND USERGROUP_ID = " + ddlUserGroup.SelectedValue.ToString());
            string CGroup = "";
            if (dtCGroup.Rows.Count > 0)
            {
                CGroup = dtCGroup.Rows[0]["IS_ADMIN"].ToString();
                hCGroup.Value = CGroup;
            }
            string Condition = string.Empty;
            switch (CGroup)
            {
                case "0":
                    Condition = "SELECT SVENDORID AS VENDOR_CODE, SVENDORNAME AS VENDOR_ABBR FROM (SELECT v.SVENDORID, VS.SVENDORNAME, ROW_NUMBER()OVER(ORDER BY VS.SVENDORNAME ) AS RN FROM  TVENDOR v INNER JOIN TVENDOR_SAP vs ON V.SVENDORID = VS.SVENDORID WHERE v.CACTIVE = '1')";
                    //ผู้ขนส่ง
                    break;

                case "1":
                    Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                    //พนักงาน ปตท
                    break;

                case "2":
                    //Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE '5%' OR T.STERMINALID LIKE '8%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                    Condition = "SELECT STERMINALID AS VENDOR_CODE,SABBREVIATION AS VENDOR_ABBR FROM (SELECT T.STERMINALID, TS.STERMINALNAME, T.SABBREVIATION, T.CSTATUS, T.CACTIVE, ROW_NUMBER()OVER(ORDER BY T.STERMINALID) AS RN FROM TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID WHERE (T.STERMINALID LIKE 'H%' OR T.STERMINALID LIKE 'K%')) WHERE CACTIVE = '1' AND CSTATUS = '1' AND sabbreviation IS NOT NULL";
                    //พนักงานคลัง
                    break;

                case "3":
                    Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                    //พนักงาน ปตท
                    break;

                case "4":
                    Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                    //พนักงาน ปตท
                    break;

                case "5":
                    Condition = "SELECT UNITCODE AS VENDOR_CODE, UNITNAME AS VENDOR_ABBR FROM (SELECT UNITCODE, UNITNAME, ROW_NUMBER()OVER(ORDER BY UNITNAME) AS RN FROM TUNIT )";
                    //พนักงาน ปตท
                    break;

                case "6":
                    Condition = "SELECT DISTINCT SOLD_ID AS VENDOR_CODE,SOLD_NAME AS VENDOR_ABBR FROM M_SOLD_TO";
                    //ลูกค้า ปตท
                    break;
            }
            if (!string.Equals(CGroup, "6"))
            {
                DataTable dtVendorID = UserGroupBLL.Instance.DepartmentSelectBLL(Condition);
                DropDownListHelper.BindDropDownList(ref ddlUnitNameTab1, dtVendorID, "VENDOR_CODE", "VENDOR_ABBR", true);
                ddlUnitNameTab1.Visible = true;
                txtShipTo.Visible = false;
                aShipTo.Visible = false;
                aShipToClear.Visible = false;
            }
            else
            {
                ddlUnitNameTab1.Visible = false;
                txtShipTo.Visible = true;
                aShipTo.Visible = true;
                aShipToClear.Visible = true;
            }
            ddlUnitNameTab1.Enabled = true;
            if (string.Equals(CGroup, "1") || string.Equals(CGroup, "3") || string.Equals(CGroup, "4") || string.Equals(CGroup, "5"))
            {
                ddlDepartment.Enabled = true;
                ddlDivision.Enabled = false;
                ddlUnitNameTab1.Enabled = false;
                if (string.Equals(CGroup, "3"))
                {
                    //ddlTeam.Enabled = false;
                    //ddlTeam.ClearSelection();
                }
                else
                {
                    //ddlTeam.Enabled = true;
                    //ddlTeam.ClearSelection();
                }
                ddlUnitNameTab1.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlDivision.ClearSelection();
            }
            else
            {
                if (string.Equals(CGroup, "0") || string.Equals(CGroup, "6"))
                {
                    //txtPassword.Enabled = true;
                    //ddlTeam.Enabled = false;
                    //ddlTeam.ClearSelection();
                }
                else
                {
                    //txtPassword.Enabled = false;
                    //ddlTeam.Enabled = true;
                    //ddlTeam.ClearSelection();
                }
                ddlDepartment.Enabled = false;
                ddlDivision.Enabled = false;
                ddlUnitNameTab1.Enabled = true;
                ddlUnitNameTab1.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlDivision.ClearSelection();
            }
        }
        else
        {
            ddlUnitNameTab1.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlDivision.Enabled = false;
            ddlUnitNameTab1.ClearSelection();
            ddlDepartment.ClearSelection();
            ddlDivision.ClearSelection();
        }
    }

    protected void dgvData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (string.Equals(e.CommandName, "view") || string.Equals(e.CommandName, "editData"))
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            string ID = dgvData.DataKeys[Index].Value.ToString();
            ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);

            switch (e.CommandName)
            {
                case "view": Response.Redirect("UserAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + ID); break;
                case "editData": Response.Redirect("UserAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Edit.ToString()), MachineKeyProtection.All) + "&id=" + ID); break;

                default:
                    break;
            }
        }
    }
    protected void cmdAdd_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("UserAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Add.ToString()), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All));
    }
    protected void cmdRefresh_ServerClick(object sender, EventArgs e)
    {
        try
        {
            ddlUserGroup.ClearSelection();
            ddlUnitNameTab1.ClearSelection();
            ddlDepartment.ClearSelection();
            ddlDivision.ClearSelection();
            txtFirstNameTab1.Text = string.Empty;
            txtLastNameTab1.Text = string.Empty;
            txtUserNameTab1.Text = string.Empty;
            txtEmailTab1.Text = string.Empty;
            ddlStatusTab1.ClearSelection();
            ddlDepartment.Enabled = false;
            ddlDivision.Enabled = false;
            ddlUnitNameTab1.Enabled = false;
            //dtDataTab1 = UserBLL.Instance.UserSelectBLL(string.Empty);
            GridViewHelper.BindGridView(ref dgvData, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlDepartment.SelectedIndex > 0)
            {
                this.LoadDivision(" AND DEPARTMENT_ID = " + ddlDepartment.SelectedValue.Trim());
                ddlDivision.Enabled = true;
            }
            else
            {
                DropDownListHelper.BindDropDownList(ref ddlDivision, null, "DIVISION_ID", "DIVISION_NAME", true);
                ddlDivision.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }


    private void Export()
    {
        try
        {
            DataView view = new DataView(dtDataTab1);
            DataTable dt2 = view.ToTable(false, "SUSERNAME", "FULLNAME", "VENDOR_NAME", "DEPARTMENT_NAME", "DIVISION_NAME", "SPOSITION", "SEMAIL", "USERGROUP_NAME", "STEL", "CREATE_DATE", "MAXDCREATE", "STATUS_NAME");

            if (dt2 == null || dt2.Rows.Count == 0)
            {
                throw new Exception("ไม่มีข้อมูลที่ต้องการ Export");
            }

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];
            DataTable dtFinal = dt2.Copy();

            //dtFinal.Columns.Remove("GAS_ID");

            //string DateStart = DateTime.ParseExact(txtDeliveryDateStart.Text.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            //string DateEnd = DateTime.ParseExact(txtDeliveryDateStop.Text.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
            this.SetFormatCell(worksheet.Cells[0, 0], "รายชื่อผู้ใช้งานในระบบ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 15);
            worksheet.Cells.GetSubrangeAbsolute(0, 0, 0, 11).Merged = true;

            this.SetFormatCell(worksheet.Cells[1, 0], "USERNAME", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 1], "ชื่อ - นามสกุล", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 2], "หน่วยงาน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 3], "DEPARTMENT", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 4], "DIVISION", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 5], "ตำแหน่ง", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 6], "อีเมล์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 7], "กลุ่มผู้ใช้งาน", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 8], "เบอร์โทรศัพท์", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 9], "Create Date", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 10], "Last Login", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);
            this.SetFormatCell(worksheet.Cells[1, 11], "สถานะ", VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false, 12);

            for (int i = 2; i <= dtFinal.Rows.Count + 1; i++)
            {
                for (int j = 0; j < dtFinal.Columns.Count; j++)
                {//Export Detail
                    this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 2][j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, false, 12);
                }
            }

            int columnCount = worksheet.CalculateMaxUsedColumns();

            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.3);
            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText, int FontSize)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = FontSize * 20;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdExport_Click(object sender, EventArgs e)
    {
        try
        {
            this.Export();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}