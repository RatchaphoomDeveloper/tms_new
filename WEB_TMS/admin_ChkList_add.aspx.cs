﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;

public partial class admin_ChkList_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            var dt = new List<DT1>();
            var dt1 = new List<DT2>();
            var dt3 = new List<DT3>();
            Session["dt1"] = dt;
            Session["dt2"] = dt1;
            Session["dt3"] = dt3;

            if (Session["oTypeCheckListID"] != null)
            {
                listData();
            }
            this.AssignAuthen();
        }

        bindDatacboProb3();
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {                
            }
            if (!CanWrite)
            {
                btnAdd2.Enabled = false;
                btnAdd3.Enabled = false;
                btnSave.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
        if (PagControl.ActiveTabIndex == 1)
        {
            gvw.DataSource = Session["dt1"];
            gvw.DataBind();
        }
        else if (PagControl.ActiveTabIndex == 2)
        {
            gvwTopicQ.DataSource = Session["dt2"];
            gvwTopicQ.DataBind();
        }

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        if (CanWrite)
        {
            switch (paras[0])
            {

                case "Save":

                    var sdt1 = (List<DT1>)Session["dt1"];
                    var sdt2 = (List<DT2>)Session["dt2"];

                    if (sdt1.Count < 1 && sdt1.Count < 1)
                    {
                        return;
                    }


                    if (Session["oTypeCheckListID"] == null)
                    {
                        //insert
                        if (CommonFunction.Count_Value(sql, "SELECT sTypeCheckListID FROM TTypeOfCheckList where cType = '" + rblTypeName.Value + "'") > 0)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถเพิ่มข้อมูลได้เนื่องประเภทแบบฟอร์มมีในระบบแล้ว');");
                            return;
                        }

                        string GenIDForm = CommonFunction.Gen_ID(sql, "SELECT sTypeCheckListID FROM (SELECT sTypeCheckListID FROM TTypeOfCheckList ORDER BY CAST(sTypeCheckListID AS INT) DESC) WHERE ROWNUM <= 1");
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();
                            string strsql = "INSERT INTO TTypeOfCheckList(sTypeCheckListID,sTypeCheckListName,cType,dUse,cCase,cDespoA,cDespoB,cSureprise,cVendor,cActive,dCreate,sCreate,dUpdate,sUpdate) Values (:sTypeCheckListID,:sTypeCheckListName,:cType,:dUse,:cCase,:cDespoA,:cDespoB,:cSureprise,:cVendor,:cActive,SYSDATE,:sCreate,SYSDATE,:sUpdate)";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                com.Parameters.Clear();
                                com.Parameters.Add(":cType", OracleType.Char).Value = rblTypeName.Value;
                                com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = GenIDForm;
                                com.Parameters.Add(":sTypeCheckListName", OracleType.VarChar).Value = txtExam1.Text;
                                com.Parameters.Add(":dUse", OracleType.DateTime).Value = Convert.ToDateTime(dteDateStart1.Value);
                                com.Parameters.Add(":cCase", OracleType.Char).Value = (chkPag1n1.Checked) ? '1' : '0';
                                com.Parameters.Add(":cDespoA", OracleType.Char).Value = (chkPag1n2.Checked) ? '1' : '0';
                                com.Parameters.Add(":cDespoB", OracleType.Char).Value = (chkPag1n3.Checked) ? '1' : '0';
                                com.Parameters.Add(":cSureprise", OracleType.Char).Value = (chkPag1n4.Checked) ? '1' : '0';
                                com.Parameters.Add(":cVendor", OracleType.Char).Value = (chkPag1n5.Checked) ? '1' : '0';
                                com.Parameters.Add(":cActive", OracleType.Char).Value = rblStatus1.Value;
                                com.Parameters.Add(":sCreate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.ExecuteNonQuery();
                            }


                            string strsql1 = "INSERT INTO TGroupOfCheckList(sGroupID,sTypeCheckListID,sGroupName,cOil,cGas,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:sGroupID,:sTypeCheckListID,:sGroupName,:cOil,:cGas,:cActive,:dCreate,:sCreate,:dUpdate,:sUpdate)";
                            foreach (var dr in sdt1)
                            {
                                using (OracleCommand com = new OracleCommand(strsql1, con))
                                {

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":sGroupID", OracleType.Number).Value = dr.dtsID;
                                    com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = GenIDForm;
                                    com.Parameters.Add(":sGroupName", OracleType.VarChar).Value = dr.dtsGroupName;
                                    com.Parameters.Add(":cOil", OracleType.Char).Value = dr.dtcOil;
                                    com.Parameters.Add(":cGas", OracleType.Char).Value = dr.dtcGas;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = dr.dtcActive;
                                    com.Parameters.Add(":dCreate", OracleType.DateTime).Value = dr.userupdate;
                                    com.Parameters.Add(":sCreate", OracleType.VarChar).Value = dr.userid;
                                    com.Parameters.Add(":dUpdate", OracleType.DateTime).Value = dr.userupdate;
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = dr.userid;
                                    com.ExecuteNonQuery();
                                }
                            }

                            string GenIDCheckList = CommonFunction.Gen_ID(sql, "SELECT sCheckListID FROM (SELECT sCheckListID FROM TCheckList ORDER BY sCheckListID DESC) WHERE ROWNUM <= 1");
                            string strsql2 = "INSERT INTO TCheckList(sCheckListID,sVersionList,sTypeCheckListID,sGroupID,sTopicID,sVersion,nList,sCheckListname,cActive,dCreate,sCreate,dUpdate,sUpdate,cCut,cBan,NDAY_MA,SCHECKLISTGROUP) VALUES (:sCheckListID,:sVersionList,:sTypeCheckListID,:sGroupID,:sTopicID,:sVersion,:nList,:sCheckListname,:cActive,:dCreate,:sCreate,:dUpdate,:sUpdate,:cCut,:cBan,:NDAY_MA,:SCHECKLISTGROUP)";
                            foreach (var dr1 in sdt2)
                            {
                                using (OracleCommand com = new OracleCommand(strsql2, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":sCheckListID", OracleType.Number).Value = dr1.dtnTopicQID;
                                    com.Parameters.Add(":sVersionList", OracleType.Number).Value = dr1.dtnVersion;
                                    com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = GenIDForm;
                                    com.Parameters.Add(":sGroupID", OracleType.Number).Value = dr1.dtnProbID;
                                    com.Parameters.Add(":sTopicID", OracleType.Number).Value = dr1.dtnTopicID;
                                    com.Parameters.Add(":sVersion", OracleType.Number).Value = dr1.dtnTopicVersion;
                                    com.Parameters.Add(":sCheckListname", OracleType.VarChar).Value = dr1.dtnTopicQName;
                                    com.Parameters.Add(":SCHECKLISTGROUP", OracleType.VarChar).Value = dr1.dtnTopicGroupName;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = dr1.dtcActive;
                                    com.Parameters.Add(":dCreate", OracleType.DateTime).Value = dr1.userupdate;
                                    com.Parameters.Add(":sCreate", OracleType.VarChar).Value = dr1.userid;
                                    com.Parameters.Add(":dUpdate", OracleType.DateTime).Value = dr1.userupdate;
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = dr1.userid;
                                    com.Parameters.Add(":cCut", OracleType.Char).Value = dr1.dtcCut;
                                    com.Parameters.Add(":nList", OracleType.Int32).Value = dr1.dtnnList;
                                    if (dr1.dtcBan == '2')
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = "";
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = dr1.dtnDAY_MA;
                                    }
                                    else if (dr1.dtcBan == '1')
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = '1';
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = 0;
                                    }
                                    else
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = '0';
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = 0;
                                    }
                                    com.ExecuteNonQuery();
                                }
                            }


                        }

                        LogUser("36", "I", "บันทึกข้อมูลหน้า แบบฟอร์มตรวจสอบ", GenIDForm + "");
                    }
                    else
                    {    //update
                        if (!("" + rblTypeName.Value).Equals(CheckUpdate.Text.Trim() + ""))
                        {
                            if (CommonFunction.Count_Value(sql, "SELECT sTypeCheckListID FROM TTypeOfCheckList where cType = '" + rblTypeName.Value + "'") > 0)
                            {
                                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถบันทึกข้อมูลได้เนื่องประเภทแบบฟอร์มมีในระบบแล้ว');");
                                return;
                            }
                        }

                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            con.Open();

                            string strsql = "UPDATE TTypeOfCheckList Set sTypeCheckListName = :sTypeCheckListName,cType = :cType, dUse = :dUse,cCase = :cCase,cDespoA = :cDespoA,cDespoB = :cDespoB,cSureprise = :cSureprise,cVendor = :cVendor,cActive = :cActive,dUpdate = SYSDATE,sUpdate = :sUpdate WHERE sTypeCheckListID = :sTypeCheckListID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {

                                com.Parameters.Clear();
                                com.Parameters.Add(":cType", OracleType.Char).Value = rblTypeName.Value;
                                com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = Session["oTypeCheckListID"].ToString();
                                com.Parameters.Add(":sTypeCheckListName", OracleType.VarChar).Value = txtExam1.Text;
                                com.Parameters.Add(":dUse", OracleType.DateTime).Value = Convert.ToDateTime(dteDateStart1.Value);
                                com.Parameters.Add(":cCase", OracleType.Char).Value = (chkPag1n1.Checked) ? '1' : '0';
                                com.Parameters.Add(":cDespoA", OracleType.Char).Value = (chkPag1n2.Checked) ? '1' : '0';
                                com.Parameters.Add(":cDespoB", OracleType.Char).Value = (chkPag1n3.Checked) ? '1' : '0';
                                com.Parameters.Add(":cSureprise", OracleType.Char).Value = (chkPag1n4.Checked) ? '1' : '0';
                                com.Parameters.Add(":cVendor", OracleType.Char).Value = (chkPag1n5.Checked) ? '1' : '0';
                                com.Parameters.Add(":cActive", OracleType.Char).Value = rblStatus1.Value;
                                com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = Session["UserID"].ToString();
                                com.ExecuteNonQuery();
                            }

                            using (OracleCommand com = new OracleCommand("DELETE FROM TCheckList WHERE sTypeCheckListID = :sTypeCheckListID", con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = Session["oTypeCheckListID"].ToString();
                                com.ExecuteNonQuery();
                            }

                            using (OracleCommand com = new OracleCommand("DELETE FROM TGroupOfCheckList WHERE sTypeCheckListID = :sTypeCheckListID", con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = Session["oTypeCheckListID"].ToString();
                                com.ExecuteNonQuery();
                            }

                            string strsql1 = "INSERT INTO TGroupOfCheckList(sGroupID,sTypeCheckListID,sGroupName,cOil,cGas,cActive,dCreate,sCreate,dUpdate,sUpdate) VALUES (:sGroupID,:sTypeCheckListID,:sGroupName,:cOil,:cGas,:cActive,:dCreate,:sCreate,:dUpdate,:sUpdate)";
                            foreach (var dr in sdt1)
                            {
                                using (OracleCommand com = new OracleCommand(strsql1, con))
                                {

                                    com.Parameters.Clear();
                                    com.Parameters.Add(":sGroupID", OracleType.Number).Value = dr.dtsID;
                                    com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = Session["oTypeCheckListID"].ToString();
                                    com.Parameters.Add(":sGroupName", OracleType.VarChar).Value = dr.dtsGroupName;
                                    com.Parameters.Add(":cOil", OracleType.Char).Value = dr.dtcOil;
                                    com.Parameters.Add(":cGas", OracleType.Char).Value = dr.dtcGas;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = dr.dtcActive;
                                    com.Parameters.Add(":dCreate", OracleType.DateTime).Value = dr.userupdate;
                                    com.Parameters.Add(":sCreate", OracleType.VarChar).Value = dr.userid;
                                    com.Parameters.Add(":dUpdate", OracleType.DateTime).Value = dr.userupdate;
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = dr.userid;
                                    com.ExecuteNonQuery();
                                }
                            }

                            string strsql2 = "INSERT INTO TCheckList(sCheckListID,sVersionList,sTypeCheckListID,sGroupID,sTopicID,sVersion,nList,sCheckListname,cActive,dCreate,sCreate,dUpdate,sUpdate,cCut,cBan,NDAY_MA,SCHECKLISTGROUP) VALUES (:sCheckListID,:sVersionList,:sTypeCheckListID,:sGroupID,:sTopicID,:sVersion,:nList,:sCheckListname,:cActive,:dCreate,:sCreate,:dUpdate,:sUpdate,:cCut,:cBan,:NDAY_MA,:SCHECKLISTGROUP)";
                            foreach (var dr1 in sdt2)
                            {
                                using (OracleCommand com = new OracleCommand(strsql2, con))
                                {
                                    com.Parameters.Clear();
                                    com.Parameters.Add(":sCheckListID", OracleType.Number).Value = dr1.dtnTopicQID;
                                    com.Parameters.Add(":sVersionList", OracleType.Number).Value = dr1.dtnVersion;
                                    com.Parameters.Add(":sTypeCheckListID", OracleType.VarChar).Value = Session["oTypeCheckListID"].ToString();
                                    com.Parameters.Add(":sGroupID", OracleType.Number).Value = dr1.dtnProbID;
                                    com.Parameters.Add(":sTopicID", OracleType.Number).Value = dr1.dtnTopicID;
                                    com.Parameters.Add(":sVersion", OracleType.Number).Value = dr1.dtnTopicVersion;
                                    com.Parameters.Add(":sCheckListname", OracleType.VarChar).Value = dr1.dtnTopicQName;
                                    com.Parameters.Add(":SCHECKLISTGROUP", OracleType.VarChar).Value = dr1.dtnTopicGroupName;
                                    com.Parameters.Add(":cActive", OracleType.Char).Value = dr1.dtcActive;
                                    com.Parameters.Add(":dCreate", OracleType.DateTime).Value = dr1.userupdate;
                                    com.Parameters.Add(":sCreate", OracleType.VarChar).Value = dr1.userid;
                                    com.Parameters.Add(":dUpdate", OracleType.DateTime).Value = dr1.userupdate;
                                    com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = dr1.userid;
                                    com.Parameters.Add(":cCut", OracleType.Char).Value = dr1.dtcCut;
                                    com.Parameters.Add(":nList", OracleType.Int32).Value = dr1.dtnnList;
                                    if (dr1.dtcBan == '2')
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = "";
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = dr1.dtnDAY_MA;
                                    }
                                    else if (dr1.dtcBan == '1')
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = '1';
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = 0;
                                    }
                                    else
                                    {
                                        com.Parameters.Add(":cBan", OracleType.Char).Value = '0';
                                        com.Parameters.Add(":NDAY_MA", OracleType.Number).Value = 0;
                                    }
                                    com.ExecuteNonQuery();
                                }
                            }

                        }

                        LogUser("36", "E", "แก้ไขข้อมูลหน้า แบบฟอร์มตรวจสอบ", Session["oTypeCheckListID"] + "");
                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_ChkList_lst.aspx';});");
                    break;

                case "editdt1":
                    txtIndex.Text = paras[1].ToString();
                    txtProb2.Text = gvw.GetRowValues(int.Parse(paras[1]), "dtsGroupName").ToString();
                    chkProductP2n1.Checked = gvw.GetRowValues(int.Parse(paras[1]), "dtcOil").ToString() == "1" ? true : false;
                    chkProductP2n2.Checked = gvw.GetRowValues(int.Parse(paras[1]), "dtcGas").ToString() == "1" ? true : false;
                    rblStatus2.Value = gvw.GetRowValues(int.Parse(paras[1]), "dtcActive").ToString();

                    break;

                case "AddData1":
                    //add
                    if (String.IsNullOrEmpty(txtExam1.Text + "") || String.IsNullOrEmpty(dteDateStart1.Text + ""))
                    {
                        if (String.IsNullOrEmpty(txtExam1.Text + ""))
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณากรอกชื่อแบบฟอร์ม');");
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาเลือกวันที่เริ่มใช้งาน');");
                        }

                        PagControl.ActiveTabIndex = 0;
                        break;
                    }

                    if (chkProductP2n1.Checked == false && chkProductP2n2.Checked == false)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาเลือกผลิตภัณฑ์');");
                        break;
                    }

                    if (string.IsNullOrEmpty(txtIndex.Text + ""))
                    {
                        var addData = (List<DT1>)Session["dt1"];
                        int max = (addData.Count > 0) ? addData.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                        addData.Add(new DT1
                        {
                            dtsID = max,
                            dtsGroupName = txtProb2.Text,
                            dtcOil = (chkProductP2n1.Checked) ? '1' : '0',
                            dtcGas = (chkProductP2n2.Checked) ? '1' : '0',
                            dtcActive = Convert.ToChar(rblStatus2.Value),
                            userid = Session["UserID"].ToString(),
                            userupdate = DateTime.Now
                        });

                        gvw.DataSource = addData;
                        gvw.DataBind();

                        Session["dt1"] = addData;
                        clearDataGroup();
                        bindDatacboProb3();
                    }
                    //edit
                    else
                    {
                        var editData = (List<DT1>)Session["dt1"];
                        int index = Convert.ToInt32(txtIndex.Text);
                        var id = editData[index].dtsID;
                        editData.RemoveAt(index);
                        editData.Insert(index, new DT1
                        {
                            dtsID = id,
                            dtsGroupName = txtProb2.Text,
                            dtcOil = (chkProductP2n1.Checked) ? '1' : '0',
                            dtcGas = (chkProductP2n2.Checked) ? '1' : '0',
                            dtcActive = Convert.ToChar(rblStatus2.Value),
                            userid = Session["UserID"].ToString(),
                            userupdate = DateTime.Now
                        });

                        gvw.DataSource = editData;
                        gvw.DataBind();

                        Session["dt1"] = editData;
                        clearDataGroup();
                        bindDatacboProb3();

                    }

                    break;

                case "deletedt1":
                    var delsdt1 = (List<DT1>)Session["dt1"];
                    delsdt1.RemoveAt(Convert.ToInt32(paras[1]));

                    gvw.DataSource = delsdt1;
                    gvw.DataBind();

                    Session["dt1"] = delsdt1;
                    clearDataGroup();
                    bindDatacboProb3();
                    break;

                case "cboProbSelectChanged":
                    var Probdt1 = (List<DT1>)Session["dt1"];
                    foreach (var dt in Probdt1.Where(s => s.dtsID == Convert.ToInt32(cboProb3.Value)))
                    {
                        chkProductP3n1.Checked = dt.dtcOil == '1' ? true : false;
                        chkProductP3n2.Checked = dt.dtcGas == '1' ? true : false;
                    }
                    bindDatacboProb3();
                    break;


                case "editDT2":


                    txtIndexQ3.Text = paras[1];
                    txtIDChkList.Text = gvwTopicQ.GetRowValues(int.Parse(paras[1]), "dtnTopicQID").ToString();
                    cboProb3.Value = Convert.ToInt32(gvwTopicQ.GetRowValues(int.Parse(paras[1]), "dtnProbID"));

                    var Probdt2 = (List<DT1>)Session["dt1"];
                    foreach (var dt in Probdt2.Where(s => s.dtsID == Convert.ToInt32(cboProb3.Value)))
                    {
                        chkProductP3n1.Checked = dt.dtcOil == '1' ? true : false;
                        chkProductP3n2.Checked = dt.dtcGas == '1' ? true : false;
                    }

                    int indexx = int.Parse(paras[1]);
                    cboTopic3.Value = (gvwTopicQ.GetRowValues(indexx, "dtnTopicID")).ToString() + "_" + (gvwTopicQ.GetRowValues(indexx, "dtnTopicVersion")).ToString();
                    txtTopicQ3.Text = gvwTopicQ.GetRowValues(indexx, "dtnTopicQName").ToString();
                    chkCut3.Checked = gvwTopicQ.GetRowValues(indexx, "dtcCut").ToString() == "1" ? true : false;
                    rblBan.Value = gvwTopicQ.GetRowValues(indexx, "dtcBan").ToString();
                    txtVersion3.Text = gvwTopicQ.GetRowValues(indexx, "dtnVersion").ToString();
                    rblStatus3.Value = gvwTopicQ.GetRowValues(indexx, "dtcActive").ToString();
                    txtEditDay.Text = gvwTopicQ.GetRowValues(indexx, "dtnDAY_MA").ToString();

                    break;

                case "AddData2":

                    var sdtQ1 = (List<DT2>)Session["dt2"];
                    //add
                    if (String.IsNullOrEmpty(txtIndexQ3.Text + ""))
                    {
                        int maxQ = (sdtQ1.Count > 0) ? sdtQ1.Select(s => s.dtnTopicQID).OrderByDescending(o => o).First() + 1 : 1;

                        string[] topicsplit = cboTopic3.Value.ToString().Split('_');
                        int num1 = 0;
                        sdtQ1.Add(new DT2
                        {

                            dtnTopicQID = maxQ,
                            dtnTopicQName = txtTopicQ3.Text,
                            dtnTopicID = Convert.ToInt32(topicsplit[0]),
                            dtnTopicVersion = Convert.ToInt32(topicsplit[1]),
                            dtnProbID = Convert.ToInt32(cboProb3.Value),
                            dtsTopicName = cboTopic3.Text,
                            dtcCut = (chkCut3.Checked) ? '1' : '0',
                            dtcActive = Convert.ToChar(rblStatus3.Value),
                            dtnVersion = 1,
                            userid = Session["UserID"].ToString(),
                            userupdate = DateTime.Now,
                            dtnnList = maxQ,
                            dtcBan = Convert.ToChar(rblBan.Value + ""),
                            dtnDAY_MA = Int32.TryParse(txtEditDay.Text, out num1) ? num1 : 0,
                            dtnTopicGroupName = cboCheckListGroup.Text
                        });

                    }
                    //edit
                    else
                    {

                        if (sdtQ1.Count > 0)
                        {
                            int index = Convert.ToInt32(txtIndexQ3.Text);
                            var oldTopicQ = sdtQ1[index];

                            int Oldversion = sdtQ1.Where(o => o.dtnTopicQID == Convert.ToInt32(txtIDChkList.Text)).Select(s => s.dtnVersion).OrderByDescending(o => o).First();
                            int i = sdtQ1.FindIndex(delegate(DT2 dt) { return dt.dtnTopicQID == Convert.ToInt32(txtIDChkList.Text) && dt.dtnVersion == Oldversion; });
                            var OldData = sdtQ1[i];
                            sdtQ1.RemoveAt(i);

                            sdtQ1.Add(new DT2
                            {

                                dtnTopicQID = OldData.dtnTopicQID,
                                dtnTopicQName = OldData.dtnTopicQName,
                                dtnTopicID = OldData.dtnTopicID,
                                dtnTopicVersion = OldData.dtnTopicVersion,
                                dtnProbID = OldData.dtnProbID,
                                dtsTopicName = OldData.dtsTopicName,
                                dtcCut = OldData.dtcCut,
                                dtcActive = '0',
                                dtnVersion = OldData.dtnVersion,
                                userid = OldData.userid,
                                userupdate = OldData.userupdate,
                                dtnnList = OldData.dtnnList,
                                dtcBan = OldData.dtcBan,
                                dtnDAY_MA = OldData.dtnDAY_MA

                            });

                            int version = (sdtQ1.Count > 0) ? sdtQ1.Where(o => o.dtnTopicQID == Convert.ToInt32(txtIDChkList.Text)).Select(s => s.dtnVersion).OrderByDescending(o => o).First() + 1 : 1;

                            string[] topicsplit = cboTopic3.Value.ToString().Split('_');
                            int num = 0;
                            sdtQ1.Add(new DT2
                            {

                                dtnTopicQID = Convert.ToInt32(txtIDChkList.Text),
                                dtnTopicQName = txtTopicQ3.Text,
                                dtnTopicID = Convert.ToInt32(topicsplit[0]),
                                dtnTopicVersion = Convert.ToInt32(topicsplit[1]),
                                dtnProbID = Convert.ToInt32(cboProb3.Value),
                                dtsTopicName = cboTopic3.Text,
                                dtcCut = (chkCut3.Checked) ? '1' : '0',
                                dtcActive = '1',
                                dtnVersion = version,
                                userid = Session["UserID"].ToString(),
                                userupdate = DateTime.Now,
                                dtnnList = Convert.ToInt32(txtIDChkList.Text),
                                dtcBan = Convert.ToChar(rblBan.Value + ""),
                                dtnDAY_MA = Int32.TryParse(txtEditDay.Text, out num) ? num : 0

                            });
                        }

                    }
                    gvwTopicQ.DataSource = sdtQ1.OrderBy(o => o.dtnTopicQID);
                    gvwTopicQ.DataBind();

                    Session["dt2"] = sdtQ1.OrderBy(o => o.dtnTopicQID).ToList();
                    clearDataTopicQ();

                    break;

                case "deleteDT2":
                    var delsDT2 = (List<DT2>)Session["dt2"];
                    if (txtDelIndex.Text != "")
                    {
                        delsDT2.RemoveAt(Convert.ToInt32(txtDelIndex.Text));

                        gvwTopicQ.DataSource = delsDT2.OrderBy(o => o.dtnTopicQID);
                        gvwTopicQ.DataBind();

                        Session["dt2"] = delsDT2;
                        clearDataGroup();
                    }
                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }

    }

    private static bool FindList(DT2 bk, int id1, int id2)
    {

        if (bk.dtnTopicQID == id1 && bk.dtnVersion == id2)
        {
            return true;
        }
        {
            return false;
        }

    }

    void listData()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        DataTable dt3 = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT c.sCheckListID,c.sVersionList,c.sTypeCheckListID,c.sGroupID,c.sTopicID,c.sVersion,c.sCheckListname,c.cActive,c.dCreate,c.sCreate,c.dUpdate,c.sUpdate,c.cCut,c.nList,c.cBan,T.STOPICNAME,c.NDAY_MA,c.SCHECKLISTGROUP FROM TCheckList c INNER JOIN tTopic t ON c.sTopicID = t.sTopicID AND c.sVersion = t.sVersion WHERE sTypeCheckListID = '" + Session["oTypeCheckListID"] + "" + "' Order by c.sCheckListID,c.sVersionList");
        dt1 = CommonFunction.Get_Data(sql, "SELECT sGroupID,sTypeCheckListID,sGroupName,cOil,cGas,cActive,dCreate,sCreate,dUpdate,sUpdate FROM TGroupOfCheckList WHERE sTypeCheckListID = '" + Session["oTypeCheckListID"] + "" + "'");
        dt2 = CommonFunction.Get_Data(sql, "SELECT CTYPE,sTypeCheckListID,sTypeCheckListName,dUse,cCase,cDespoA,cDespoB,cSureprise,cVendor,cActive FROM TTypeOfCheckList WHERE sTypeCheckListID = '" + Session["oTypeCheckListID"] + "" + "'");
        dt3 = CommonFunction.Get_Data(sql, "SELECT SCHECKLISTGROUP_ID,SCHECKLISTGROUP_NAME FROM TCHECKLISTGROUP");

        if (dt2.Rows.Count > 0)
        {
            CheckUpdate.Text = dt2.Rows[0]["CTYPE"] + "";
            rblTypeName.Value = dt2.Rows[0]["CTYPE"] + "";
            txtExam1.Text = dt2.Rows[0]["STYPECHECKLISTNAME"] + "";
            lblFormName2.Text = dt2.Rows[0]["STYPECHECKLISTNAME"] + "";
            lblFormName3.Text = dt2.Rows[0]["STYPECHECKLISTNAME"] + "";
            dteDateStart1.Date = Convert.ToDateTime(dt2.Rows[0]["DUSE"] + "");
            chkPag1n1.Checked = (dt2.Rows[0]["CCASE"] + "" == "1") ? true : false;
            chkPag1n2.Checked = (dt2.Rows[0]["CDESPOA"] + "" == "1") ? true : false;
            chkPag1n3.Checked = (dt2.Rows[0]["CDESPOB"] + "" == "1") ? true : false;
            chkPag1n4.Checked = (dt2.Rows[0]["CSUREPRISE"] + "" == "1") ? true : false;
            chkPag1n5.Checked = (dt2.Rows[0]["CVENDOR"] + "" == "1") ? true : false;
            rblStatus1.Value = dt2.Rows[0]["CACTIVE"] + "";

            chkPag2n1.Checked = (dt2.Rows[0]["CCASE"] + "" == "1") ? true : false;
            chkPag2n2.Checked = (dt2.Rows[0]["CDESPOA"] + "" == "1") ? true : false;
            chkPag2n3.Checked = (dt2.Rows[0]["CDESPOB"] + "" == "1") ? true : false;
            chkPag2n4.Checked = (dt2.Rows[0]["CSUREPRISE"] + "" == "1") ? true : false;
            chkPag2n5.Checked = (dt2.Rows[0]["CVENDOR"] + "" == "1") ? true : false;

            chkPag3n1.Checked = (dt2.Rows[0]["CCASE"] + "" == "1") ? true : false;
            chkPag3n2.Checked = (dt2.Rows[0]["CDESPOA"] + "" == "1") ? true : false;
            chkPag3n3.Checked = (dt2.Rows[0]["CDESPOB"] + "" == "1") ? true : false;
            chkPag3n4.Checked = (dt2.Rows[0]["CSUREPRISE"] + "" == "1") ? true : false;
            chkPag3n5.Checked = (dt2.Rows[0]["CVENDOR"] + "" == "1") ? true : false;

            List<DT1> data1 = new List<DT1>(dt1.Rows.Count);

            foreach (DataRow dr in dt1.Rows)
            {
                data1.Add(new DT1
                {
                    dtsID = Convert.ToInt32(dr["SGROUPID"]),
                    dtsGroupName = dr["SGROUPNAME"] + "",
                    dtcOil = Convert.ToChar(dr["COIL"]),
                    dtcGas = Convert.ToChar(dr["CGAS"]),
                    dtcActive = Convert.ToChar(dr["CACTIVE"]),
                    userid = dr["SUPDATE"] + "",
                    userupdate = Convert.ToDateTime(dr["DUPDATE"])
                });
            }

            List<DT2> data2 = new List<DT2>(dt.Rows.Count);
            int num = 0;
            foreach (DataRow dr1 in dt.Rows)
            {
                data2.Add(new DT2
                {
                    dtnTopicQID = Convert.ToInt32(dr1["SCHECKLISTID"]),
                    dtnTopicQName = dr1["SCHECKLISTNAME"] + "",
                    dtnTopicID = Convert.ToInt32(dr1["STOPICID"]),
                    dtnTopicVersion = Convert.ToInt32(dr1["SVERSION"]),
                    dtnProbID = Convert.ToInt32(dr1["SGROUPID"]),
                    dtsTopicName = dr1["STOPICNAME"] + "",
                    dtcCut = Convert.ToChar(dr1["CCUT"]),
                    dtcActive = Convert.ToChar(dr1["CACTIVE"]),
                    dtnVersion = Convert.ToInt32(dr1["SVERSIONLIST"]),
                    userid = dr1["SUPDATE"] + "",
                    userupdate = Convert.ToDateTime(dr1["DUPDATE"]),
                    dtnnList = Convert.ToInt32(dr1["NLIST"]),
                    dtcBan = "" + dr1["CBAN"] == "1" ? '1' : '2',
                    dtnDAY_MA = Int32.TryParse(dr1["NDAY_MA"] + "", out num) ? num : 0,
                    dtnTopicGroupName = dr1["SCHECKLISTGROUP"] + ""
                });
            }

            List<DT3> data3 = new List<DT3>(dt3.Rows.Count);

            foreach (DataRow dr in dt3.Rows)
            {
                data3.Add(new DT3
                {
                    dtsCheckListGroupID = Convert.ToInt32(dr["SCHECKLISTGROUP_ID"]),
                    dtsCheckListGroupName = dr["SCHECKLISTGROUP_NAME"] + ""
                });
            }

            gvw.DataSource = data1;
            gvw.DataBind();

            Session["dt1"] = data1;
            clearDataGroup();
            bindDatacboProb3();

            gvwTopicQ.DataSource = data2;
            gvwTopicQ.DataBind();

            Session["dt2"] = data2;
            clearDataTopicQ();

            Session["dt3"] = data3;
            bindDatacboCheckListGroup();


        }

        else
        {
            Response.Redirect("admin_ChkList_lst.aspx");
        }



    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        int VisibleIndex = e.VisibleIndex;
        ASPxButton imbedit = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbedit");
        ASPxButton imbDel = (ASPxButton)gvw.FindRowCellTemplateControl(VisibleIndex, null, "imbDel");

        if (!CanWrite)
        {
            imbedit.Enabled = false;
            imbDel.Enabled = false;
        }
    }

    protected void gvwTopicQ_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {

        int VisibleIndex = e.VisibleIndex;
        ASPxButton imbedit0 = (ASPxButton)gvwTopicQ.FindRowCellTemplateControl(VisibleIndex, null, "imbedit0");
        ASPxButton imbDel0 = (ASPxButton)gvwTopicQ.FindRowCellTemplateControl(VisibleIndex, null, "imbDel0");

        if (!CanWrite)
        {
            imbedit0.Enabled = false;
            imbDel0.Enabled = false;
        }
    }
    protected void cboTopic3_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTopic.SelectCommand = @"SELECT STOPICID, STOPICID_SVERSION,STOPICNAME,SACTIVE FROM (SELECT STOPICID, STOPICID || '_' || SVERSION AS STOPICID_SVERSION , STOPICNAME ,CASE WHEN CACTIVE = '1' THEN 'Active' ELSE 'InActive' END AS SACTIVE,ROW_NUMBER()OVER(ORDER BY CACTIVE DESC,STOPICID) AS RN   FROM TTOPIC WHERE  STOPICNAME LIKE :fillter ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTopic.SelectParameters.Clear();
        sdsTopic.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTopic.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTopic.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTopic;
        comboBox.DataBind();

    }
    protected void cboTopic3_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    void clearDataGroup()
    {
        txtIndex.Text = "";
        txtProb2.Text = "";
        chkProductP2n1.Checked = false;
        chkProductP2n2.Checked = false;
        rblStatus1.Value = "1";
    }

    void clearDataTopicQ()
    {
        cboProb3.SelectedIndex = -1;
        cboTopic3.SelectedIndex = -1;
        txtTopicQ3.Text = "";
        txtIndexQ3.Text = "";
        txtVersion3.Text = "";
        chkCut3.Checked = false;
        rblStatus3.Value = "1";
        rblBan.Value = "1";
        txtEditDay.Text = "";
    }
    #region struct
    [Serializable]
    struct DT1
    {
        public int dtsID { get; set; }
        public string dtsGroupName { get; set; }
        public char dtcOil { get; set; }
        public char dtcGas { get; set; }
        public char dtcActive { get; set; }
        public string userid { get; set; }
        public DateTime userupdate { get; set; }
    }

    struct DT2
    {
        public int dtnTopicQID { get; set; }
        public string dtnTopicQName { get; set; }
        public int dtnTopicID { get; set; }
        public int dtnTopicVersion { get; set; }
        public int dtnProbID { get; set; }
        public string dtsTopicName { get; set; }
        public char dtcCut { get; set; }
        public char dtcActive { get; set; }
        public int dtnVersion { get; set; }
        public string userid { get; set; }
        public DateTime userupdate { get; set; }
        public int dtnnList { get; set; }
        public char dtcBan { get; set; }
        public int dtnDAY_MA { get; set; }
        public string dtnTopicGroupName { get; set; }
    }

    struct DT3
    {
        public int dtsCheckListGroupID { get; set; }
        public string dtsCheckListGroupName { get; set; }
    }

    #endregion

    void bindDatacboProb3()
    {
        cboProb3.DataSource = (List<DT1>)Session["dt1"];
        cboProb3.DataBind();
    }
    void bindDatacboCheckListGroup()
    {
        cboCheckListGroup.DataSource = (List<DT3>)Session["dt3"];
        cboCheckListGroup.DataBind();
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}