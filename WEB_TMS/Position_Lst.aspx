﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Position_Lst.aspx.cs" Inherits="Position_Lst" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" Runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="row form-group">
                </div>
                <div class="row form-group">
                    <center>
                            <label class="control-label">จัดการข้อมูลตำแหน่ง</label>
                        </center>
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                </div>
                <div class="row form-group">
                    <label class="col-md-2 control-label">ตำแหน่ง</label>
                    <div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtPosition" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-md-1 control-label">สถานะ</label>
                    <div class="col-md-3">
                        <asp:RadioButtonList runat="server" ID="rblCactive" RepeatDirection="Horizontal">
                            <asp:ListItem Text="ทั้งหมด"  Value="" />
                            <asp:ListItem Text="ใช้งาน" Value="1" Selected="True" />
                            <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <center>
                            <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" /></center>
                    </div>
                </div>
                <div class="row form-group" style="color:red;">
                    
                    ***Row แรกมีไว้สำหรับเพิ่มข้อมูล
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <asp:GridView runat="server" ID="gvPosition" AutoGenerateColumns="false" CssClass="table table-striped table-bordered" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader" DataKeyNames="PERSON_TYPE" OnRowDataBound="gvPosition_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="PERSON_TYPE" Visible="false" />
                                <asp:TemplateField HeaderText="ชื่อตำแหน่ง">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtPersonTypeDesc" CssClass="form-control" />
                                        <asp:HiddenField runat="server" ID="hidID" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ(Min)">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtAgeMin" CssClass="form-control number" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ(Max)">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="txtAgeMax" CssClass="form-control number" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="rblCactive" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="ใช้งาน" Value="1" />
                                            <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="กระบวนการ">
                                    <ItemTemplate>
                                        <input id='<%# "btnSave" + Eval("PERSON_TYPE") %>' type="button" value="บันทึก" data-toggle="modal"  class="btn btn-md bth-hover btn-info" data-target='<%# "#ModalConfirmBeforeSave" + Eval("PERSON_TYPE") %>'/>
                                        <uc1:ModelPopup runat="server" ID="mpConfirmSave"  IDModel='<%# "ModalConfirmBeforeSave" + Eval("PERSON_TYPE") %>' IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmSave_ClickOK" TextTitle="ยืนยันการแก้ไข" TextDetail="คุณต้องการแก้ไขข้อมูลใช้หรือไม่ ?"  CommandArgument='<%# Container.DataItemIndex %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
           <asp:HiddenField ID="hidRequireTypeName" ClientIDMode="Static" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>

