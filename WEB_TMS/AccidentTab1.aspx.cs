﻿using DevExpress.Web.ASPxEditors;
using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.OrderPlan;

public partial class AccidentTab1 : PageBase
{
    DataTable dt;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string id = "0";
            SetDrowDownList();
            hidCactive.Value = "0";
            hidCGROUP.Value = Session["CGROUP"] + string.Empty;
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);

                liTab2.Visible = true;
                GeneralTab2.HRef = "AccidentTab2.aspx?str=" + str;

                if (int.Parse(hidCactive.Value) >= 4)
                {
                    liTab3.Visible = true;
                    GeneralTab3.HRef = "AccidentTab3.aspx?str=" + str;
                    if (hidCGROUP.Value == "0")
                    {
                        GeneralTab3.HRef = "AccidentTab3_Vendor.aspx?str=" + str;
                    }
                    if (int.Parse(hidCactive.Value) >= 8)
                    {
                        liTab4.Visible = true;
                        GeneralTab4.HRef = "AccidentTab4.aspx?str=" + str;
                    }
                }
            }
            this.AssignAuthen();
        }
        SetEnabledControlByID(hidCactive.Value);
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnEmpSearch.Enabled = false;
                btnShipmentNoSearch.Enabled = false;
                btnTruckSearch.Enabled = false;
                btnShipmentSearch.Enabled = false;
                btnTTERMINALSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;
                btnSave.CssClass = "btn btn-md bth-hover btn-info";
                btnApprove.Disabled = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region SetData
    private void SetData(string ACCIDENT_ID)
    {
        dt = AccidentBLL.Instance.AccidentTab1Select(ACCIDENT_ID);
        if (dt != null && dt.Rows.Count > 0)
        {
            gvDO.DataSource = dt;
            gvDO.DataBind();
            DataRow dr = dt.Rows[0];
            txtAccidentID.Text = dr["ACCIDENT_ID"] + string.Empty;
            if (!dr.IsNull("ACCIDENT_DATE"))
            {
                txtAccidentDate.Text = DateTime.Parse(dr["ACCIDENT_DATE"] + string.Empty).ToString(DateTimeFormat);
            }
            if (!dr.IsNull("SYS_TIME"))
                txtAccidentDateSystem.Text = DateTime.Parse(dr["SYS_TIME"] + string.Empty).ToString(DateTimeFormat);
            rblREPORTER.SelectedValue = dr["REPORTER"] + string.Empty;
            rblREPORTER_SelectedIndexChanged(null, null);
            if (!dr.IsNull("REPORT_PTT"))
                txtAccidentDatePtt.Text = DateTime.Parse(dr["REPORT_PTT"] + string.Empty).ToString(DateTimeFormat);
            txtAccidentName.Text = dr["INFORMER_NAME"] + string.Empty;
            ddlAccidentType.SelectedValue = dr["ACCIDENTTYPE_ID"] + string.Empty;
            txtShipmentNo.Text = dr["SHIPMENT_NO"] + string.Empty;
            txtTruck.Text = dr["SHEADREGISTERNO"] + string.Empty;
            hidSTRUCKID.Value = dr["STRUCKID"] + string.Empty;
            ddlVendor.SelectedValue = dr["SVENDORID"] + string.Empty;
            ddlVendor_SelectedIndexChanged(null, null);
            ddlContract.SelectedValue = dr["SCONTRACTID"] + string.Empty;
            ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
            ddlWorkGroup_SelectedIndexChanged(null, null);
            ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
            txtEMPNAME.Text = dr["EMPNAME"] + string.Empty;
            txtPERS_CODE.Text = dr["PERS_CODE"] + string.Empty;
            hidSEMPLOYEEID.Value = dr["SEMPLOYEEID"] + string.Empty;
            txtAGE.Text = dr["AGE"] + string.Empty;
            txtEMPNAME2.Text = dr["EMPNAME2"] + string.Empty;
            txtPERS_CODE2.Text = dr["PERS_CODE2"] + string.Empty;
            hidSEMPLOYEEID2.Value = dr["SEMPLOYEEID2"] + string.Empty;
            txtAGE2.Text = dr["AGE2"] + string.Empty;
            txtEMPNAME3.Text = dr["EMPNAME3"] + string.Empty;
            txtPERS_CODE3.Text = dr["PERS_CODE3"] + string.Empty;
            hidSEMPLOYEEID3.Value = dr["SEMPLOYEEID3"] + string.Empty;
            txtAGE3.Text = dr["AGE3"] + string.Empty;
            txtEMPNAME4.Text = dr["EMPNAME4"] + string.Empty;
            txtPERS_CODE4.Text = dr["PERS_CODE4"] + string.Empty;
            hidSEMPLOYEEID4.Value = dr["SEMPLOYEEID4"] + string.Empty;
            txtAGE4.Text = dr["AGE4"] + string.Empty;
            txtSABBREVIATION.Text = dr["SOURCE"] + string.Empty;
            //hidSTERMINALID.Value = dr["STERMINALID"] + string.Empty;
            txtLocation.Text = dr["LOCATIONS"] + string.Empty;
            txtGPSL.Text = dr["GPSL"] + string.Empty;
            txtGPSR.Text = dr["GPSR"] + string.Empty;
            hidID.Value = txtAccidentID.Text;
            hidCactive.Value = dr["CACTIVE"] + string.Empty;
            txtAccidentDate.ReadOnly = true;
            switch (dr["DRIVER_NO"] + string.Empty)
            {
                case "1": rbDRIVER_NO1.Checked = true; break;
                case "2": rbDRIVER_NO2.Checked = true; break;
                case "3": rbDRIVER_NO3.Checked = true; break;
                case "4": rbDRIVER_NO4.Checked = true; break;
                default:
                    rbDRIVER_NO1.Checked = true;
                    break;
            }
            if (dr["CACTIVE"] + string.Empty != "1")
            {
                //plTab1.Enabled = false;
                ddlAccidentType.Enabled = false;
            }
            else
            {

                //plTab1.Enabled = true;
            }
            if (Session["CGROUP"] + string.Empty == "0")
            {
                plTab1.Enabled = false;
            }
            ddlAccidentType_SelectedIndexChanged(null, null);
        }
    }
    #endregion

    #region DrowDownList
    private void SetDrowDownList()
    {
        ViewState["DataAccidentType"] = AccidentBLL.Instance.AccidentTypeSelect(string.Empty, "1");
        ddlAccidentType.DataTextField = "NAME";
        ddlAccidentType.DataValueField = "ID";
        ddlAccidentType.DataSource = ViewState["DataAccidentType"];
        ddlAccidentType.DataBind();
        ddlAccidentType.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.DataTextField = "NAME";
        ddlWorkGroup.DataValueField = "ID";
        ddlWorkGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWorkGroup.DataBind();
        ddlWorkGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.SelectedIndex = 0;
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlGroup.SelectedIndex = 0;
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;
    }
    #endregion

    #region ddl
    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else if (ddlVendor.SelectedIndex == 0)
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
                ddlContract.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                    ddlWorkGroup_SelectedIndexChanged(null, null);
                    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlWorkGroup_SelectedIndexChanged
    protected void ddlWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWorkGroup.SelectedValue + string.Empty);
    }
    #endregion

    #region ddlGroupsDataBind
    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroup.DataTextField = "NAME";
        ddlGroup.DataValueField = "ID";
        ddlGroup.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroup.DataBind();
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
    }
    #endregion

    #region ddlAccidentType_SelectedIndexChanged
    protected void ddlAccidentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitialRequireField();
        //lblReqtxtShipmentNo.Visible = cbB100.Checked ? false : true;
    }


    private void ClearRequire()
    {
        lblReqddlContract.Visible = false;
        lblReqddlGroup.Visible = false;
        lblReqddlVendor.Visible = false;
        lblReqddlWorkGroup.Visible = false;
        lblReqtxtAGE.Visible = false;
        lblReqtxtAGE2.Visible = false;
        lblReqtxtAGE3.Visible = false;
        lblReqtxtAGE4.Visible = false;
        lblReqtxtGPS.Visible = false;
        lblReqtxtLocation.Visible = false;
        lblReqtxtPERS_CODE.Visible = false;
        lblReqtxtPERS_CODE2.Visible = false;
        lblReqtxtPERS_CODE3.Visible = false;
        lblReqtxtPERS_CODE4.Visible = false;
        lblReqtxtSABBREVIATION.Visible = false;
        lblReqtxtShipmentNo.Visible = false;
        lblReqtxtTruck.Visible = false;
    }

    private void InitialRequireField()
    {
        try
        {
            this.ClearRequire();

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlAccidentType.SelectedValue.ToString()), "ACCIDENTTAB1");

            object tmp;
            Label tmpReq;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                tmp = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                if (tmp != null)
                {
                    tmpReq = (Label)this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID_REQ"].ToString());

                    if (tmpReq != null)
                    {
                        if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                        {//Require Field
                            tmpReq.Visible = true;
                            if (hidCactive.Value == "2")
                            {
                                Type = tmp.GetType().ToString();
                                if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                                {
                                    DropDownList ddl = (DropDownList)tmp;
                                    ddl.Enabled = false;
                                }
                                else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                                {
                                    TextBox txt = (TextBox)tmp;
                                    txt.Enabled = false;
                                    switch (txt.ID)
                                    {
                                        case "txtShipmentNo":
                                            btnShipmentNoSearch.Enabled = false;
                                            btnShipmentNoSearchClear.Enabled = false;
                                            break;
                                        case "txtTruck":
                                            btnTruck.Enabled = false;
                                            btnTruckClear.Enabled = false;
                                            break;
                                        case "txtPERS_CODE":
                                            //btnPERS_CODE.Enabled = false;
                                            //btnPERS_CODEClear.Enabled = false;
                                            break;
                                        case "txtSABBREVIATION":
                                            btnTERMINAL.Enabled = false;
                                            btnTERMINALClear.Enabled = false;
                                            break;
                                        case "txtGPSL":
                                            txtGPSR.Enabled = false;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "2"))
                        {//Optional Field
                            tmpReq.Visible = false;
                        }
                        else if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "3"))
                        {//Disable Field
                            Type = tmp.GetType().ToString();
                            if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                            {
                                DropDownList ddl = (DropDownList)tmp;
                                ddl.SelectedIndex = 0;
                                ddl.Enabled = false;
                            }
                            else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                            {
                                TextBox txt = (TextBox)tmp;
                                txt.Text = string.Empty;
                                txt.Enabled = false;
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    #endregion

    #region rblREPORTER_SelectedIndexChanged
    protected void rblREPORTER_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblREPORTER.SelectedValue == "1")
        {
            txtAccidentDatePtt.ReadOnly = false;
            txtAccidentName.ReadOnly = false;

        }
        else
        {
            txtAccidentDatePtt.ReadOnly = true;
            txtAccidentName.ReadOnly = true;
            txtAccidentDatePtt.Text = string.Empty;
            txtAccidentName.Text = string.Empty;
        }
    }
    #endregion
    #endregion

    #region Btn_Cilck
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string accID = SaveData(2);
            if (!string.IsNullOrEmpty(accID))
            {
                byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                alertSuccess("บันทึกสำเร็จ", "AccidentTab1.aspx?str=" + encryptedValue);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region mpSaveSend_ClickOK
    protected void mpSaveSend_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = Validate();
            if (!string.IsNullOrEmpty(mess))
            {
                alertFail(RemoveSpecialCharacters(mess));
            }
            else
            {
                string accID = SaveData(2);
                mess += "บันทึกสำเร็จและส่งข้อมูลสำเร็จ";
                if (!string.IsNullOrEmpty(accID))
                {
                    if (SendEmail(ConfigValue.EmailAccident1, accID))
                    {
                        mess += "<br/>ส่ง E-mail สำเร็จ";
                    }
                    else
                    {
                        mess += "<span style=\"color:Red;\">ส่ง E-mail ไม่สำเร็จ</span>";
                    }
                    //send email
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    alertSuccess(mess, "AccidentTab1.aspx?str=" + encryptedValue);
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }
    #endregion

    #region mpBack_ClickOK
    protected void mpBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("~/AccidentList.aspx");
    }
    #endregion

    #endregion

    #region SaveData
    private string SaveData(int CACTIVE)
    {
        dt = new DataTable("DT");
        dt.Columns.Add("DELIVERY_NO", typeof(string));
        dt.Columns.Add("MATERIAL_NAME", typeof(string));
        dt.Columns.Add("DLV", typeof(string));
        dt.Columns.Add("CUST_NAME", typeof(string));
        dt.Columns.Add("LOADING_END", typeof(string));
        foreach (GridViewRow item in gvDO.Rows)
        {
            dt.Rows.Add(item.Cells[0].Text.Trim().Replace("&nbsp;", ""), item.Cells[1].Text.Trim().Replace("&nbsp;", ""), item.Cells[2].Text.Trim().Replace("&nbsp;", ""), item.Cells[3].Text.Trim().Replace("&nbsp;", ""), item.Cells[4].Text.Trim().Replace("&nbsp;", ""));
        }
        DataSet ds = new DataSet("DS");
        ds.Tables.Add(dt.Copy());
        int DRIVER_NO = 1;
        if (rbDRIVER_NO1.Checked)
        {

        }
        else if (rbDRIVER_NO2.Checked)
        {
            DRIVER_NO = 2;
        }
        else if (rbDRIVER_NO2.Checked)
        {
            DRIVER_NO = 3;
        }
        else if (rbDRIVER_NO2.Checked)
        {
            DRIVER_NO = 4;
        }
        dt = AccidentBLL.Instance.SaveTab1(hidID.Value, txtAccidentDate.Text.Trim(), txtAccidentDatePtt.Text.Trim(), rblREPORTER.SelectedValue, string.Empty, string.Empty, string.Empty, Session["UserID"] + string.Empty, txtAccidentName.Text.Trim(), CACTIVE, ddlAccidentType.SelectedValue, txtShipmentNo.Text, hidSTRUCKID.Value, ddlVendor.SelectedValue, ddlGroup.SelectedValue, ddlContract.SelectedValue, hidSEMPLOYEEID.Value, txtAGE.Text.Trim(), hidSEMPLOYEEID2.Value, txtAGE2.Text.Trim(), hidSEMPLOYEEID3.Value, txtAGE3.Text.Trim(), hidSEMPLOYEEID4.Value, txtAGE4.Text.Trim(), DRIVER_NO, txtSABBREVIATION.Text.Trim(), txtLocation.Text.Trim(), txtGPSL.Text.Trim(), txtGPSR.Text.Trim(), string.Empty, ds);
        return dt.Rows.Count > 0 ? dt.Rows[0][0] + string.Empty : "";
    }
    #endregion

    #region Shipment
    #region btnShipmentNoSearch_Click
    protected void btnShipmentNoSearch_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowTruckSearch", "$('#ShowShipmentSearch').modal();", true);
        updShipmentSearch.Update();
    }
    #endregion

    #region btnShipmentSearch_Click
    protected void btnShipmentSearch_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.ShipmentSelect(txtShipmentSearch.Text.Trim());
        gvShipmentSearch.DataSource = dt;
        gvShipmentSearch.DataBind();
        updShipmentSearch.Update();
    }
    #endregion

    #region gvShipmentSearch_RowUpdating
    protected void gvShipmentSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSHEADREGISTERNO = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSHEADREGISTERNO");
        HiddenField hidSTRUCKID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSTRUCKID");
        HiddenField hidSCONTRACTID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSCONTRACTID");
        HiddenField hidSVENDORID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSVENDORID");
        HiddenField hidGROUPID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidGROUPID");
        HiddenField hidWORKGROUPID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidWORKGROUPID");

        HiddenField hidSHIPMENT_NO = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSHIPMENT_NO");
        //HiddenField hidSTERMINALID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSTERMINALID");
        HiddenField hidTTERMINAL_NAME = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidTTERMINAL_NAME");
        HiddenField hidSEMPLOYEEID = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID");
        HiddenField hidPERS_CODE = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidPERS_CODE");
        HiddenField hidEMPNAME = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidEMPNAME");
        HiddenField hidAGE = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidAGE");
        HiddenField hidSEMPLOYEEID2 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID2");
        HiddenField hidPERS_CODE2 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidPERS_CODE2");
        HiddenField hidEMPNAME2 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidEMPNAME2");
        HiddenField hidAGE2 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidAGE2");
        HiddenField hidSEMPLOYEEID3 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID3");
        HiddenField hidPERS_CODE3 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidPERS_CODE3");
        HiddenField hidEMPNAME3 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidEMPNAME3");
        HiddenField hidAGE3 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidAGE3");
        HiddenField hidSEMPLOYEEID4 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID4");
        HiddenField hidPERS_CODE4 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidPERS_CODE4");
        HiddenField hidEMPNAME4 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidEMPNAME4");
        HiddenField hidAGE4 = (HiddenField)gvShipmentSearch.Rows[e.RowIndex].FindControl("hidAGE4");
        txtShipmentNo.Text = hidSHIPMENT_NO.Value;
        this.hidSTRUCKID.Value = hidSTRUCKID.Value;
        if (!string.IsNullOrEmpty(hidSVENDORID.Value))
        {
            ddlVendor.SelectedValue = hidSVENDORID.Value;
            ddlVendor_SelectedIndexChanged(null, null);
            ddlContract.SelectedValue = hidSCONTRACTID.Value;
            ddlWorkGroup.SelectedValue = hidWORKGROUPID.Value;
            ddlWorkGroup_SelectedIndexChanged(null, null);
            ddlGroup.SelectedValue = hidGROUPID.Value;
        }

        txtTruck.Text = hidSHEADREGISTERNO.Value;

        //this.hidSTERMINALID.Value = hidSTERMINALID.Value;
        txtSABBREVIATION.Text = hidTTERMINAL_NAME.Value;

        txtEMPNAME.Text = hidEMPNAME.Value;
        txtPERS_CODE.Text = hidPERS_CODE.Value;
        this.hidSEMPLOYEEID.Value = hidSEMPLOYEEID.Value;
        txtAGE.Text = hidAGE.Value;
        txtEMPNAME2.Text = hidEMPNAME2.Value;
        txtPERS_CODE2.Text = hidPERS_CODE2.Value;
        this.hidSEMPLOYEEID2.Value = hidSEMPLOYEEID2.Value;
        txtAGE2.Text = hidAGE2.Value;
        txtEMPNAME3.Text = hidEMPNAME3.Value;
        txtPERS_CODE3.Text = hidPERS_CODE3.Value;
        this.hidSEMPLOYEEID3.Value = hidSEMPLOYEEID3.Value;
        txtAGE3.Text = hidAGE3.Value;
        txtEMPNAME4.Text = hidEMPNAME4.Value;
        txtPERS_CODE4.Text = hidPERS_CODE4.Value;
        this.hidSEMPLOYEEID4.Value = hidSEMPLOYEEID4.Value;
        txtAGE4.Text = hidAGE4.Value;

        dt = AccidentBLL.Instance.ShipmentSelect(hidSHIPMENT_NO.Value);
        gvDO.DataSource = dt;
        gvDO.DataBind();

        #region Enable
        ddlVendor.Enabled = false;
        ddlContract.Enabled = false;
        ddlWorkGroup.Enabled = false;
        btnTruck.Enabled = false;
        btnTruckClear.Enabled = false;
        ddlGroup.Enabled = false;
        btnTERMINAL.Enabled = false;
        btnTERMINALClear.Enabled = false;
        //btnPERS_CODE.Enabled = false;
        //btnPERS_CODEClear.Enabled = false;

        #endregion
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowShipmentSearch').modal('hide');</script>", false);
    }
    #endregion

    #region gvShipmentSearch_PageIndexChanging
    protected void gvShipmentSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvShipmentSearch.PageIndex = e.NewPageIndex;
        btnShipmentSearch_Click(null, null);
    }
    #endregion

    #region btnShipmentNoSearchClear_Click
    protected void btnShipmentNoSearchClear_Click(object sender, EventArgs e)
    {
        txtShipmentNo.Text = string.Empty;
        this.hidSTRUCKID.Value = string.Empty;
        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        ddlContract.SelectedIndex = 0;
        txtTruck.Text = string.Empty;
        ddlWorkGroup.SelectedIndex = 0;
        ddlWorkGroup_SelectedIndexChanged(null, null);
        ddlGroup.SelectedIndex = 0;
        //this.hidSTERMINALID.Value = string.Empty;
        txtSABBREVIATION.Text = string.Empty;
        hidSEMPLOYEEID.Value = string.Empty;
        txtEMPNAME.Text = string.Empty;
        txtPERS_CODE.Text = string.Empty;
        txtAGE.Text = string.Empty;

        hidSEMPLOYEEID2.Value = string.Empty;
        txtEMPNAME2.Text = string.Empty;
        txtPERS_CODE2.Text = string.Empty;
        txtAGE2.Text = string.Empty;
        hidSEMPLOYEEID3.Value = string.Empty;
        txtEMPNAME3.Text = string.Empty;
        txtPERS_CODE3.Text = string.Empty;
        txtAGE3.Text = string.Empty;
        hidSEMPLOYEEID4.Value = string.Empty;
        txtEMPNAME4.Text = string.Empty;
        txtPERS_CODE4.Text = string.Empty;
        txtAGE4.Text = string.Empty;
        dt = new DataTable();
        dt.Columns.Add("DELIVERY_NO", typeof(string));
        dt.Columns.Add("MATERIAL_NAME", typeof(string));
        dt.Columns.Add("DLV", typeof(string));
        dt.Columns.Add("CUST_NAME", typeof(string));
        dt.Columns.Add("LOADING_END", typeof(string));
        gvDO.DataSource = dt;
        gvDO.DataBind();

        #region Enable
        ddlVendor.Enabled = true;
        ddlContract.Enabled = true;
        ddlWorkGroup.Enabled = true;
        btnTruck.Enabled = true;
        btnTruckClear.Enabled = true;
        ddlGroup.Enabled = true;
        btnTERMINAL.Enabled = true;
        btnTERMINALClear.Enabled = true;
        btnPERS_CODE.Enabled = true;
        btnPERS_CODEClear.Enabled = true;
        btnPERS_CODE2.Enabled = true;
        btnPERS_CODE2Clear.Enabled = true;
        btnPERS_CODE3.Enabled = true;
        btnPERS_CODE3Clear.Enabled = true;
        btnPERS_CODE4.Enabled = true;
        btnPERS_CODE4Clear.Enabled = true;

        #endregion
    }
    #endregion
    #endregion

    #region Truck
    #region btnTruckSearch_Click
    protected void btnTruckSearch_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.TruckSelect(txtTruckSearch.Text.Trim());
        gvTruckSearch.DataSource = dt;
        gvTruckSearch.DataBind();
        updTruckSearch.Update();
    }
    #endregion

    #region btnTruck_Click
    protected void btnTruck_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowTruckSearch", "$('#ShowTruckSearch').modal();", true);
        updTruckSearch.Update();
    }
    #endregion

    #region gvTruckSearch_PageIndexChanging
    protected void gvTruckSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTruckSearch.PageIndex = e.NewPageIndex;
        btnTruckSearch_Click(null, null);
    }
    #endregion

    #region gvTruckSearch_RowUpdating
    protected void gvTruckSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSHEADREGISTERNO = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidSHEADREGISTERNO");
        HiddenField hidSTRUCKID = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidSTRUCKID");
        HiddenField hidSCONTRACTID = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidSCONTRACTID");
        HiddenField hidSVENDORID = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidSVENDORID");
        HiddenField hidGROUPID = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidGROUPID");
        HiddenField hidWORKGROUPID = (HiddenField)gvTruckSearch.Rows[e.RowIndex].FindControl("hidWORKGROUPID");
        this.hidSTRUCKID.Value = hidSTRUCKID.Value;
        ddlVendor.SelectedValue = hidSVENDORID.Value;
        ddlVendor_SelectedIndexChanged(null, null);
        ddlContract.SelectedValue = hidSCONTRACTID.Value;
        txtTruck.Text = hidSHEADREGISTERNO.Value;
        ddlWorkGroup.SelectedValue = hidWORKGROUPID.Value;
        ddlWorkGroup_SelectedIndexChanged(null, null);
        ddlGroup.SelectedValue = hidGROUPID.Value;

        #region Enable
        ddlVendor.Enabled = false;
        ddlContract.Enabled = false;
        ddlWorkGroup.Enabled = false;
        ddlGroup.Enabled = false;

        #endregion
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowTruckSearch').modal('hide');</script>", false);
    }
    #endregion

    #region btnTruckClear_Click
    protected void btnTruckClear_Click(object sender, EventArgs e)
    {
        this.hidSTRUCKID.Value = string.Empty;
        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        ddlContract.SelectedIndex = 0;
        txtTruck.Text = string.Empty;
        ddlWorkGroup.SelectedIndex = 0;
        ddlWorkGroup_SelectedIndexChanged(null, null);
        ddlGroup.SelectedIndex = 0;

        #region Enable
        ddlVendor.Enabled = true;
        ddlContract.Enabled = true;
        ddlWorkGroup.Enabled = true;
        ddlGroup.Enabled = true;

        #endregion
    }
    #endregion
    #endregion

    #region Emp
    #region btnPERS_CODE_Click
    protected void btnPERS_CODE_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowPERS_CODESearch", "$('#ShowPERS_CODESearch').modal();", true);
        updPERS_CODESearch.Update();
    }
    #endregion

    #region btnEmpSearch_Click
    protected void btnEmpSearch_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.EmpSelect(txtEmpSearch.Text.Trim(), ddlVendor.SelectedValue);
        gvEmpSearch.DataSource = dt;
        gvEmpSearch.DataBind();
        updPERS_CODESearch.Update();
    }
    #endregion

    #region gvEmpSearch_PageIndexChanging
    protected void gvEmpSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmpSearch.PageIndex = e.NewPageIndex;
        btnEmpSearch_Click(null, null);
    }
    #endregion

    #region gvEmpSearch_RowUpdating
    protected void gvEmpSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSEMPLOYEEID = (HiddenField)gvEmpSearch.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID");
        HiddenField hidEMPNAME = (HiddenField)gvEmpSearch.Rows[e.RowIndex].FindControl("hidEMPNAME");
        HiddenField hidPERS_CODE = (HiddenField)gvEmpSearch.Rows[e.RowIndex].FindControl("hidPERS_CODE");
        HiddenField hidAGE = (HiddenField)gvEmpSearch.Rows[e.RowIndex].FindControl("hidAGE");
        this.hidSEMPLOYEEID.Value = hidSEMPLOYEEID.Value;
        txtPERS_CODE.Text = hidPERS_CODE.Value;
        txtEMPNAME.Text = hidEMPNAME.Value;
        txtAGE.Text = hidAGE.Value;


        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowPERS_CODESearch').modal('hide');</script>", false);
    }
    #endregion

    #region btnPERS_CODEClear_Click
    protected void btnPERS_CODEClear_Click(object sender, EventArgs e)
    {
        txtPERS_CODE.Text = string.Empty;
        txtEMPNAME.Text = string.Empty;
        txtAGE.Text = string.Empty;
    }
    #endregion
    #endregion

    #region Emp2
    #region btnPERS_CODE2_Click
    protected void btnPERS_CODE2_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowPERS_CODE2Search", "$('#ShowPERS_CODE2Search').modal();", true);
        updPERS_CODE2Search.Update();
    }
    #endregion

    #region btnEmp2Search_Click
    protected void btnEmp2Search_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.EmpSelect(txtEmp2Search.Text.Trim(), ddlVendor.SelectedValue);
        gvEmp2Search.DataSource = dt;
        gvEmp2Search.DataBind();
        updPERS_CODE2Search.Update();
    }
    #endregion

    #region gvEmp2Search_PageIndexChanging
    protected void gvEmp2Search_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp2Search.PageIndex = e.NewPageIndex;
        btnEmp2Search_Click(null, null);
    }
    #endregion

    #region gvEmp2Search_RowUpdating
    protected void gvEmp2Search_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSEMPLOYEEID = (HiddenField)gvEmp2Search.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID");
        HiddenField hidEMPNAME = (HiddenField)gvEmp2Search.Rows[e.RowIndex].FindControl("hidEMPNAME");
        HiddenField hidPERS_CODE = (HiddenField)gvEmp2Search.Rows[e.RowIndex].FindControl("hidPERS_CODE");
        HiddenField hidAGE = (HiddenField)gvEmp2Search.Rows[e.RowIndex].FindControl("hidAGE");
        this.hidSEMPLOYEEID2.Value = hidSEMPLOYEEID.Value;
        txtPERS_CODE2.Text = hidPERS_CODE.Value;
        txtEMPNAME2.Text = hidEMPNAME.Value;
        txtAGE2.Text = hidAGE.Value;


        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowPERS_CODE2Search').modal('hide');</script>", false);
    }
    #endregion

    #region btnPERS_CODE2Clear_Click
    protected void btnPERS_CODE2Clear_Click(object sender, EventArgs e)
    {
        this.hidSEMPLOYEEID2.Value = string.Empty;
        txtPERS_CODE2.Text = string.Empty;
        txtEMPNAME2.Text = string.Empty;
        txtAGE2.Text = string.Empty;
    }
    #endregion
    #endregion

    #region Emp3
    #region btnPERS_CODE3_Click
    protected void btnPERS_CODE3_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowPERS_CODE3Search", "$('#ShowPERS_CODE3Search').modal();", true);
        updPERS_CODE3Search.Update();
    }
    #endregion

    #region btnEmp3Search_Click
    protected void btnEmp3Search_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.EmpSelect(txtEmp3Search.Text.Trim(), ddlVendor.SelectedValue);
        gvEmp3Search.DataSource = dt;
        gvEmp3Search.DataBind();
        updPERS_CODE3Search.Update();
    }
    #endregion

    #region gvEmp3Search_PageIndexChanging
    protected void gvEmp3Search_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp3Search.PageIndex = e.NewPageIndex;
        btnEmp3Search_Click(null, null);
    }
    #endregion

    #region gvEmp3Search_RowUpdating
    protected void gvEmp3Search_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSEMPLOYEEID = (HiddenField)gvEmp3Search.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID");
        HiddenField hidEMPNAME = (HiddenField)gvEmp3Search.Rows[e.RowIndex].FindControl("hidEMPNAME");
        HiddenField hidPERS_CODE = (HiddenField)gvEmp3Search.Rows[e.RowIndex].FindControl("hidPERS_CODE");
        HiddenField hidAGE = (HiddenField)gvEmp3Search.Rows[e.RowIndex].FindControl("hidAGE");
        this.hidSEMPLOYEEID3.Value = hidSEMPLOYEEID.Value;
        txtPERS_CODE3.Text = hidPERS_CODE.Value;
        txtEMPNAME3.Text = hidEMPNAME.Value;
        txtAGE3.Text = hidAGE.Value;


        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowPERS_CODE3Search').modal('hide');</script>", false);
    }
    #endregion

    #region btnPERS_CODE3Clear_Click
    protected void btnPERS_CODE3Clear_Click(object sender, EventArgs e)
    {
        this.hidSEMPLOYEEID3.Value = string.Empty;
        txtPERS_CODE3.Text = string.Empty;
        txtEMPNAME3.Text = string.Empty;
        txtAGE3.Text = string.Empty;
    }
    #endregion
    #endregion

    #region Emp4
    #region btnPERS_CODE4_Click
    protected void btnPERS_CODE4_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowPERS_CODE4Search", "$('#ShowPERS_CODE4Search').modal();", true);
        updPERS_CODE4Search.Update();
    }
    #endregion

    #region btnEmp4Search_Click
    protected void btnEmp4Search_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.EmpSelect(txtEmp4Search.Text.Trim(), ddlVendor.SelectedValue);
        gvEmp4Search.DataSource = dt;
        gvEmp4Search.DataBind();
        updPERS_CODE4Search.Update();
    }
    #endregion

    #region gvEmp4Search_PageIndexChanging
    protected void gvEmp4Search_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmp4Search.PageIndex = e.NewPageIndex;
        btnEmp4Search_Click(null, null);
    }
    #endregion

    #region gvEmp4Search_RowUpdating
    protected void gvEmp4Search_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hidSEMPLOYEEID = (HiddenField)gvEmp4Search.Rows[e.RowIndex].FindControl("hidSEMPLOYEEID");
        HiddenField hidEMPNAME = (HiddenField)gvEmp4Search.Rows[e.RowIndex].FindControl("hidEMPNAME");
        HiddenField hidPERS_CODE = (HiddenField)gvEmp4Search.Rows[e.RowIndex].FindControl("hidPERS_CODE");
        HiddenField hidAGE = (HiddenField)gvEmp4Search.Rows[e.RowIndex].FindControl("hidAGE");
        this.hidSEMPLOYEEID4.Value = hidSEMPLOYEEID.Value;
        txtPERS_CODE4.Text = hidPERS_CODE.Value;
        txtEMPNAME4.Text = hidEMPNAME.Value;
        txtAGE4.Text = hidAGE.Value;


        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowPERS_CODE4Search').modal('hide');</script>", false);
    }
    #endregion

    #region btnPERS_CODE4Clear_Click
    protected void btnPERS_CODE4Clear_Click(object sender, EventArgs e)
    {

        this.hidSEMPLOYEEID4.Value = string.Empty;
        txtPERS_CODE4.Text = string.Empty;
        txtEMPNAME4.Text = string.Empty;
        txtAGE4.Text = string.Empty;
    }
    #endregion
    #endregion

    #region TERMINAL
    #region btnTERMINAL_Click
    protected void btnTERMINAL_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowTTERMINALSearch", "$('#ShowTTERMINALSearch').modal();", true);
        updTTERMINALSearch.Update();
    }
    #endregion

    #region btnTTERMINALSearch_Click
    protected void btnTTERMINALSearch_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.TerminalSelect(txtTTERMINALSearch.Text.Trim());
        gvTTERMINALSearch.DataSource = dt;
        gvTTERMINALSearch.DataBind();
        updTTERMINALSearch.Update();

    }
    #endregion

    #region gvTTERMINALSearch_PageIndexChanging
    protected void gvTTERMINALSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTTERMINALSearch.PageIndex = e.NewPageIndex;
        btnTTERMINALSearch_Click(null, null);
    }
    #endregion

    #region gvTTERMINALSearch_RowUpdating
    protected void gvTTERMINALSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //HiddenField hidSTERMINALID = (HiddenField)gvTTERMINALSearch.Rows[e.RowIndex].FindControl("hidSTERMINALID");
        HiddenField hidSABBREVIATION = (HiddenField)gvTTERMINALSearch.Rows[e.RowIndex].FindControl("hidSABBREVIATION");
        //this.hidSTERMINALID.Value = hidSTERMINALID.Value;
        txtSABBREVIATION.Text = hidSABBREVIATION.Value;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ShowTTERMINALSearch').modal('hide');</script>", false);
    }
    #endregion

    #region btnTERMINALClear_Click
    protected void btnTERMINALClear_Click(object sender, EventArgs e)
    {
        //this.hidSTERMINALID.Value = string.Empty;
        txtSABBREVIATION.Text = string.Empty;
    }
    #endregion
    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {
            if (ddlAccidentType.SelectedIndex == 0)
            {
                mess += "กรุณาเลือก ขั้นตอนการขนส่งขณะเกิดเหตุ !!!<br/>";
                return mess;
            }
            if (string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
            {
                mess += "กรุณาป้อน วันที่-เวลาเกิดเหตุ !!!<br/>";
                return mess;
            }

            DataTable dtRequire = ComplainBLL.Instance.ComplainRequireFieldBLL(int.Parse(ddlAccidentType.SelectedValue), "ACCIDENTTAB1");

            StringBuilder sb = new StringBuilder();
            object c;
            string Type;

            for (int i = 0; i < dtRequire.Rows.Count; i++)
            {
                if (string.Equals(dtRequire.Rows[i]["REQUIRE_TYPE"].ToString(), "1"))
                {
                    c = this.FindControlRecursive(Page, dtRequire.Rows[i]["CONTROL_ID"].ToString());

                    Type = c.GetType().ToString();

                    if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxComboBox"))
                    {
                        ASPxComboBox cbo = (ASPxComboBox)c;
                        if (cbo.Value == null)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!! <br/>");
                    }
                    else if (string.Equals(Type, "DevExpress.Web.ASPxEditors.ASPxTextBox"))
                    {
                        ASPxTextBox txt = (ASPxTextBox)c;
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.TextBox"))
                    {
                        TextBox txt = (TextBox)c;
                        if (txt.ID == "txtShipmentNo" && cbB100.Checked)
                        {
                            continue;
                        }
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.WebControls.DropDownList"))
                    {
                        DropDownList cbo = (DropDownList)c;
                        if (cbo.SelectedIndex < 1)
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                    else if (string.Equals(Type, "System.Web.UI.HtmlControls.HtmlInputText"))
                    {
                        HtmlInputText txt = (HtmlInputText)c;
                        if (string.Equals(txt.Value.Trim(), string.Empty))
                            sb.Append("กรุณาป้อน " + dtRequire.Rows[i]["DESCRIPTION"].ToString() + " !!!<br/>");
                    }
                }
            }
            if (!string.Equals(sb.ToString(), string.Empty))
                mess += sb.ToString();
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    #endregion

    #region SendEmail
    private bool SendEmail(int TemplateID, string accID)
    {
        try
        {
            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                string EmailList = string.Empty;
                if (TemplateID == ConfigValue.EmailAccident1)
                {
                    #region EmailAccident1
                    EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true);
                    //if (!string.IsNullOrEmpty(EmailList))
                    //{
                    //    EmailList += ",";
                    //}
                    //                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com
                    //                                        ,TMS_yutasak.c@pttor.com,TMS_kittipong.l@pttor.com
                    //                                        ,TMS_suphakit.k@pttor.com,TMS_apipat.k@pttor.com
                    //                                        ,TMS_nut.t@pttor.com
                    //                                        ,TMS_kritsada.h@pttor.com
                    //                                        ,TMS_bodin.a@pttor.com,TMS_kwanploy.p@pttor.com
                    //                                        ,TMS_manatsawee.j@pttor.com,TMS_promthong.s@pttor.com
                    //                                        ,TMS_witawat.t@pttor.com
                    //                                        ,TMS_nontivat.i@pttor.com,zsuntipab.k@pttict.com";

                    //EmailList += "raviwan.t@pttor.com,chansak.t@pttor.com,praewpailin.p@pttor.com,YUTASAK.C@PTTOR.COM,CHOOSAK.A@PTTOR.COM,nut.t@pttor.com,terapat.p@pttor.com,manatsawee.j@pttor.com,rerkset.t@pttor.com,wanvisa.c@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thrathorn.v@pttor.com,patrapol.n@pttor.com,chutapha.c@pttor.com,kittipong.l@pttor.com,norawit.k@pttor.com,settasak.t@pttor.com,kwanploy.p@pttor.com,nattira.a@pttor.com ,teerapat.le@pttor.com,nontivat.i@pttor.com,patkanit.s@pttor.com,witawat.t@pttor.com,kritsada.h@pttor.com,SUDTADA.S@PTTOR.COM,PRASARN.N@PTTOR.COM";

                    var sb = new StringBuilder();
                    gvDO.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
                    Subject = Subject.Replace("{ACCID}", accID);
                    Body = Body.Replace("{ACCID}", accID);
                    Body = Body.Replace("{DRIVER}", txtEMPNAME.Text.Trim());
                    Body = Body.Replace("{CAR}", txtTruck.Text.Trim());
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem != null ? ddlVendor.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem != null ? ddlContract.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{ACCSTATUS}", ddlAccidentType.SelectedItem != null ? ddlAccidentType.SelectedItem.Text.Trim() : "-");
                    Body = Body.Replace("{SOURCE}", txtSABBREVIATION.Text.Trim());
                    Body = Body.Replace("{ACCPOINT}", txtLocation.Text.Trim());
                    Body = Body.Replace("{GPS}", txtGPSL.Text.Trim() + "," + txtGPSR.Text.Trim());
                    Body = Body.Replace("{CREATE_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
                    Body = Body.Replace("{REMARK}", sb.ToString());
                    if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
                    {
                        string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
                        if (AccidentDate.Any())
                        {
                            Body = Body.Replace("{DATE}", AccidentDate[0]);
                            Body = Body.Replace("{TIME}", AccidentDate[1]);
                        }

                    }

                    byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
                    string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab2.aspx?str=" + ID;
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendMail(EmailList, Subject, Body, "", "EmailAccident1", ColumnEmailName);
                    #endregion

                }

                isREs = true;

            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region SetEnabledControlByID
    private void SetEnabledControlByID(string ID)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetEnabledControl", "SetEnabledControlByID('" + ID + "');", true);
    }
    #endregion


    protected void cbB100_CheckedChanged(object sender, EventArgs e)
    {
        if (cbB100.Checked)
        {
            lblReqtxtShipmentNo.Visible = false;
        }
        else
        {
            if (ddlAccidentType.SelectedIndex > 0)
            {
                ddlAccidentType_SelectedIndexChanged(null, null);
            }

        }
    }

    protected void btnSaveSend_Click(object sender, EventArgs e)
    {
        mpSaveSend.TextDetail = "<br/><label class='col-md-04 control-label' style='font-weight:normal'>";
        mpSaveSend.TextDetail += "วันที่เกิดเหตุ : ";
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<label class='col-md-04 control-label'>";
        mpSaveSend.TextDetail += txtAccidentDate.Text.Trim();
        mpSaveSend.TextDetail += "</label>";

        mpSaveSend.TextDetail += "<br/><label class='col-md-04 control-label' style='font-weight:normal'>";
        mpSaveSend.TextDetail += "ทะเบียนรถ : ";
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<label class='col-md-04 control-label'>";
        mpSaveSend.TextDetail += txtTruck.Text.Trim();
        mpSaveSend.TextDetail += "</label>";

        mpSaveSend.TextDetail += "<br/><label class='col-md-04 control-label' style='font-weight:normal'>";
        mpSaveSend.TextDetail += "ผู้ขนส่ง : ";
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<label class='col-md-04 control-label'>";
        mpSaveSend.TextDetail += ddlVendor.SelectedItem != null ? ddlVendor.SelectedItem.Text : "-";
        mpSaveSend.TextDetail += "</label>";

        mpSaveSend.TextDetail += "<br/><label class='col-md-04 control-label' style='font-weight:normal'>";
        mpSaveSend.TextDetail += "พิกัด GPS : ";
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<label class='col-md-04 control-label'>";
        mpSaveSend.TextDetail += txtGPSL.Text.Trim() + "," + txtGPSR.Text.Trim();
        mpSaveSend.TextDetail += "</label>";


        mpSaveSend.TextDetail += "<br/><label class='col-md-04 control-label' style='font-weight:normal'>";
        mpSaveSend.TextDetail += "สถานที่เกิดเหตุ : ";
        for (int i = 0; i < txtLocation.Text.Trim().Replace("\n", "|").Split('|').Count(); i++)
        {
            mpSaveSend.TextDetail += "<br/>";
        }
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<label class='col-md-04 control-label'>";
        mpSaveSend.TextDetail += txtLocation.Text.Trim().Replace("\n", "<br/>");
        mpSaveSend.TextDetail += "</label>";
        mpSaveSend.TextDetail += "<br/><label class='col-md-08 control-label'>";
        mpSaveSend.TextDetail += "คุณต้องการบันทึกและส่งข้อมูลใช่หรือไม่ ?";
        mpSaveSend.TextDetail += "</label>";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "<script type='text/javascript'>$('#ModalConfirmBeforeSave').find('.TextDetail').empty();$('#ModalConfirmBeforeSave').find('.TextDetail').append(\"" + mpSaveSend.TextDetail + "\");$('#btnSave2').click();</script>", false);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        // specified ASP.NET server control at run time.
        // No code required here.
    }
}