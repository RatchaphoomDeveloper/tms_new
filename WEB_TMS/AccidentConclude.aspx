﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" 
    CodeFile="AccidentConclude.aspx.cs" Inherits="AccidentConclude" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <div class="form-horizontal">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-table"></i>ค้นหา
            </div>
            <div class="panel-body">
                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">วันที่เกิดเหตุ</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlYear">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เดือน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlMonth">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">กลุ่มงาน</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlWorkGroup" OnSelectedIndexChanged="ddlWorkGroup_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                            <label class="col-md-2 control-label">กลุ่มที่</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">บริษัทผู้ขนส่ง</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-md-2 control-label">เลขที่สัญญา</label>
                            <div class="col-md-3">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3 control-label">อุบัติเหตุที่เกิดขึ้นเข้าข่าย</label>
                            <div class="col-md-3">
                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblSERIOUS">
                                    <asp:ListItem Text="&nbsp;ร้ายแรง&nbsp;&nbsp;" Value="0" />
                                    <asp:ListItem Text="&nbsp;ไม่ร้ายแรง" Value="1" />
                                </asp:RadioButtonList>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="row form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-4">
                        <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                            <ContentTemplate>
                                <asp:Button Text="ค้นหา" runat="server" ID="btnSearch" class="btn btn-md bth-hover btn-info" OnClick="btnSearch_Click" />
                                <asp:Button Text="Clear" runat="server" ID="btnClear" class="btn btn-md bth-hover btn-info" OnClick="btnClear_Click" />
                                <asp:Button Text="Export" runat="server" ID="btnExport" class="btn btn-md bth-hover btn-info" OnClick="btnExport_Click" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExport" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanel3">
            <ContentTemplate>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>ผลการค้นหา จำนวน
            <asp:Label Text="0" runat="server" ID="lblItem" />
                        รายการ
                    </div>
                    <div class="panel-body">
                        <div class="row form-group">
                            <label class="col-md-12">F = เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด</label>
                           <label class="col-md-12">A = ไม่เข้าข่ายอุบัติเหตุร้ายแรงตามที่ ปตท. กำหนด</label>
                        </div>
                        <div class="form-group">
                            <%-- <div id="divGv" style="overflow-x:scroll;max-width:1300px">--%>
                            <dx:ASPxGridView ID="gvAccident" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" OnHtmlRowCreated="grid_HtmlRowCreated" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" KeyFieldName="SCONTRACTNO">

                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SABBREVIATION" CellStyle-Wrap="False" Width="230px">
                                        <HeaderStyle HorizontalAlign="Center" />

                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" CellStyle-Wrap="False" Width="160px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="กลุ่มที่" FieldName="GROUPNAME" CellStyle-Wrap="False" Width="130">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="รวม" HeaderStyle-HorizontalAlign="Center" Name="">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="TOTAL0" Name="0" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="TOTAL1" Name="1" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ม.ค." HeaderStyle-HorizontalAlign="Center" Name="JAN">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="JAN0" Name="0" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="JAN1" Name="1" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ก.พ." HeaderStyle-HorizontalAlign="Center" Name="FEB">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="FEB0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="FEB1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="มี.ค." HeaderStyle-HorizontalAlign="Center" Name="MAR">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="MAR0" Name="0" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="MAR1" Name="1" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="เม.ย." HeaderStyle-HorizontalAlign="Center" Name="APR">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="APR0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="APR1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="พ.ค." HeaderStyle-HorizontalAlign="Center" Name="MAY">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="MAY0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="MAY1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="มิ.ย." HeaderStyle-HorizontalAlign="Center" Name="JUN">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="JUN0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="JUN1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ก.ค." HeaderStyle-HorizontalAlign="Center" Name="JUL">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="JUL0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="JUL1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ส.ค." HeaderStyle-HorizontalAlign="Center" Name="AUG">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="AUG0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="AUG1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ก.ย." HeaderStyle-HorizontalAlign="Center" Name="SEP">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="SEP0" Name="0" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="SEP1" Name="1" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ต.ค." HeaderStyle-HorizontalAlign="Center" Name="OCT">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="OCT0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="OCT1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="พ.ย." HeaderStyle-HorizontalAlign="Center" Name="NOV">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="NOV0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="NOV1" Name="1">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ธ.ค." HeaderStyle-HorizontalAlign="Center" Name="DEC">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="F" FieldName="DEC0" Name="0">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="A" FieldName="DEC1" Name="1" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                </Columns>
                                <Settings />
                                <Paddings />
                            </dx:ASPxGridView>
                            <%-- </div>--%>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script>
        $(document).ready(function () {
            $("#divGv").css("max-width", $(window).width() - 50);
            $(window).resize(function () {
                $("#divGv").css("max-width", $(window).width() - 50);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

