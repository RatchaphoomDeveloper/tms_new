﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="admin_Complain_ReqDoc.aspx.cs" Inherits="admin_Complain_ReqDoc" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <contenttemplate>
            <div id="pnlPTT" runat="server">
                <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="ข้อมูลทั่วไป"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="tblAddHeader" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>หมายเลขเอกสาร</asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtDocID" runat="server" style="width: 250px; text-align: center"
                                                    readonly="readonly" value="Generate by System" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:Label ID="lblEditDate" runat="server" Text="(เอกสารนี้สามารถแก้ไขได้ ภายในวันที่&nbsp;{0})"
                                                    Visible="false" ForeColor="Blue" Font-Underline="true"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ประเภทร้องเรียน
                                                <asp:Label ID="lblReqcmbTopic" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cmbTopic" runat="server" CssClass="form-control" DataTextField="TOPIC_NAME" Enabled="false"
                                                    DataValueField="TOPIC_ID" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="cmbTopic_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell>
                                                คลังต้นทาง
                                                <asp:Label ID="lblReqcbxOrganiz" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cbxOrganiz" runat="server" CssClass="form-control" DataTextField="STERMINALNAME"
                                                    DataValueField="STERMINALID" Width="250px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                หัวข้อร้องเรียน
                                                <asp:Label ID="lblReqcboComplainType" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboComplainType" runat="server" CssClass="form-control" DataTextField="COMPLAIN_TYPE_NAME" Enabled="false"
                                                    DataValueField="COMPLAIN_TYPE_ID" AutoPostBack="true" Width="250px" OnSelectedIndexChanged="cboComplainType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:CheckBox ID="chkLock" runat="server" Text="หยุดพักการขนส่งชั่วคราว (พขร.)" ForeColor="Red" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                บริษัทผู้ประกอบการขนส่ง
                                                <asp:Label ID="lblReqcboVendor" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboVendor" runat="server" CssClass="form-control" DataTextField="SVENDORNAME"
                                                    AutoPostBack="true" DataValueField="SVENDORID" Width="250px" OnSelectedIndexChanged="cboVendor_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>ประเภทผลิตภัณฑ์</asp:TableCell>
                                            <asp:TableCell>
                                                <dx:ASPxRadioButtonList ID="rblTypeProduct" runat="server" RepeatDirection="Horizontal"
                                                    SkinID="rblStatus">
                                                    <Items>
                                                        <dx:ListEditItem Selected="True" Text="น้ำมัน" Value="O" />
                                                        <dx:ListEditItem Text="ก๊าซ" Value="G" />
                                                    </Items>
                                                </dx:ASPxRadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                เลขที่สัญญา
                                                <asp:Label ID="lblReqcboContract" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="cboContract" runat="server" CssClass="form-control" DataTextField="SCONTRACTNO"
                                                    AutoPostBack="true" DataValueField="SCONTRACTID" Width="250px" OnSelectedIndexChanged="cboContract_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                Delivery No
                                                <asp:Label ID="lblReqcboDelivery" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboDelivery" runat="server" CssClass="form-control" DataTextField="SDELIVERYNO"
                                                                DataValueField="SDELIVERYNO" Width="215px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%--<asp:TextBox ID="txtDelivery" runat="server"></asp:TextBox>--%>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ทะเบียนรถ (หัว)
                                                <asp:Label ID="lblReqcboHeadRegist" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cboHeadRegist" runat="server" CssClass="form-control" DataTextField="SHEADREGISTERNO"
                                                                AutoPostBack="true" DataValueField="STRUCKID" Width="215px" OnSelectedIndexChanged="cboHeadRegist_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                            <asp:TableCell> &nbsp; </asp:TableCell>
                                            <asp:TableCell> ทะเบียนรถ (ท้าย) </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" id="txtTrailerRegist" runat="server" disabled="disabled"
                                                                style="width: 250px" value="" />
                                                        </td>
                                                        <td style="padding-left: 4px;"></td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน
                                                <asp:Label ID="lblReqcmbPersonalNo" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbPersonalNo" runat="server" CssClass="form-control" DataTextField="FULLNAME"
                                                                DataValueField="SPERSONELNO" Width="215px" OnSelectedIndexChanged="cmbPersonalNo_SelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน 2
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbPersonalNo2" runat="server" CssClass="form-control" DataTextField="FULLNAME"
                                                                DataValueField="SPERSONELNO" Width="215px" OnSelectedIndexChanged="cmbPersonalNo2_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน 3
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbPersonalNo3" runat="server" CssClass="form-control" DataTextField="FULLNAME"
                                                                DataValueField="SPERSONELNO" Width="215px" OnSelectedIndexChanged="cmbPersonalNo3_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                ชื่อ พขร. ที่ถูกร้องเรียน 4
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="cmbPersonalNo4" runat="server" CssClass="form-control" DataTextField="FULLNAME"
                                                                DataValueField="SPERSONELNO" Width="215px" OnSelectedIndexChanged="cmbPersonalNo4_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                สถานที่เกิดเหตุ
                                                <asp:Label ID="lblReqtxtComplainAddress" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <input class="form-control" id="txtComplainAddress" runat="server" style="width: 250px" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <table>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                วันที่เกิดเหตุ
                                                <asp:Label ID="lblReqtxtDateTrans" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                <asp:TextBox ID="txtDateTrans" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>
                        &nbsp;
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                จำนวนรถ (คัน)
                                                <asp:Label ID="lblReqtxtTotalCar" runat="server" Visible="false" ForeColor="Red"
                                                    Text="&nbsp;*"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <%--<input class="form-control" id="txtTotalCar" runat="server" style="width: 250px; text-align: center"/>--%>
                                                <asp:TextBox ID="txtTotalCar" CssClass="number form-control" runat="server" Style="width: 250px; text-align: center" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                ปลายทาง
                                            </asp:TableCell>
                                            <asp:TableCell Width="100px">
                                                <asp:TextBox ID="txtShipTo" runat="server" CssClass="form-control" TextMode="MultiLine" Height="90px" Enabled="false"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                            <asp:TableCell>
                        
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <div style="width: 99%">
                                        <asp:GridView ID="dgvHeader" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnRowDeleting="dgvHeader_RowDeleting"
                                            OnRowCommand="dgvHeader_RowCommand" OnRowDataBound="dgvHeader_RowDataBound">
                                            <Columns>
                                                <asp:BoundField DataField="TopicID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TopicName" HeaderText="ประเภทร้องเรียน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ComplainID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ComplainName" HeaderText="หัวข้อร้องเรียน">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ContractID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ContractName" HeaderText="เลขที่สัญญา">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CarHead" HeaderText="ทะเบียนรถ(หัว)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TotalCar" HeaderText="จำนวนรถ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DeliveryDate" HeaderText="วันที่เกิดเหตุ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <%--<asp:TemplateField HeaderText="Action">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgViewComplain" runat="server" ImageUrl="~/Images/btnSearch.png"
                                                                Width="16px" Height="16px" Style="cursor: pointer" CommandName="Select" />&nbsp;
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/btnEdit.png" Width="16px"
                                                                Height="16px" Style="cursor: pointer" CommandName="EditHeader" />&nbsp;
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="imgDeleteComplain" runat="server" ImageUrl="~/Images/bin1.png"
                                                                Width="16px" Height="16px" Style="cursor: pointer" CommandName="Delete" />&nbsp;
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>รายละเอียดและสาเหตุ
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="Table2" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell Width="10%">
                                    สาเหตุ / รายละเอียด <font color="#ff0000">&nbsp;*</font>
                                            </asp:TableCell>
                                            <asp:TableCell Width="90%">
                                                <textarea id="txtDetail" runat="server" cols="1" rows="20" class="form-control" style="width: 100%; height: 260px;" readonly="readonly"></textarea>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>เอกสารแนบประกอบการพิจารณา
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:GridView ID="dgvRequestFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:BoundField DataField="COMPLAIN_TYPE_NAME" HeaderText="หัวข้อ">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COMPLAIN_TYPE_ID" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ">
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: left">
                        &nbsp;* กรณีเอกสารแนบเป็นไฟล์เดียวกัน สามารถแนบเอกสารเพียงครั้งเดียวได้
                    </div>
                    <div class="panel-footer" style="text-align: left">
                        &nbsp;* ถ้าเอกสารแนบเป็นคนละไฟล์ แต่มีชื่อประเภทไฟล์เอกสารตรงกัน กรุณาตั้งชื่อไฟล์
                    (ตามผู้ใช้งาน) ให้แตกต่างกัน
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i>แนบเอกสาร หลักฐาน
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table ID="Table3" runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="35%">
                                            <asp:Label ID="lblUploadTypeTab3" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                        </asp:TableCell><asp:TableCell>
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="cboUploadType" runat="server" class="form-control" Width="350px">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Width="35%">
                                            <asp:FileUpload ID="fileUpload" runat="server" />
                                        </asp:TableCell><asp:TableCell>
                                            <asp:Button ID="cmdUpload" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                                UseSubmitBehavior="false" OnClick="cmdUpload_Click" Style="width: 100px" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                                <br />
                                <asp:GridView ID="dgvUploadFile" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                    EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                    OnRowDeleting="dgvUploadFile_RowDeleting" OnRowUpdating="dgvUploadFile_RowUpdating"
                                    OnRowDataBound="dgvUploadFile_RowDataBound" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" />
                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                                        <asp:BoundField DataField="FULLPATH" Visible="false" />
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/64/blue-37.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/imgDelete.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก"
                            UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" OnClick="cmdSave_Click"
                            Style="width: 120px" />
                        &nbsp;
                    </div>
                    <br />
                </div>
            </div>
        </contenttemplate>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
