﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="KPI.aspx.cs" Inherits="KPI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        function CheckPhaseVal(Index, PHASE) {

            var strCheckError = "";

            for (var i = 1; i < 6; i++) {

                /*เช็คว่าถ้าเป้น Textbox ตัวที่คีย์ไม่ต้องเช็ค*/
                if (i == PHASE) {

                    if (window["txtT" + i + "_" + Index + ""] != undefined && parseFloat(window["txtT" + i + "_" + Index + ""] + "") != NaN) {

                    }
                    else {
                        strCheckError = "ไม่สามารถกรอตัวอักษรได้ยกเว้น -";
                    }
                }
                else {
                    /*เช็คว่าเจอคอนโทรนไหม และแปลงเป็น Float ได้หรือเปล่า*/
                    if (window["txtT" + i + "_" + Index + ""] != undefined && parseFloat(window["txtT" + i + "_" + Index + ""] + "") != NaN) {

                        var txtNew = window["txtT" + PHASE + "_" + Index + ""].GetValue();
                        var txtOld = window["txtT" + i + "_" + Index + ""].GetValue();
                        /*เช็คว่าเจอคอนโทรนไหม และแปลงเป็น Float ได้หรือเปล่า*/
                        if (txtNew != undefined && txtNew != NaN && txtNew != null && txtOld != null) {
                            /*เช็คว่า User ได้ใส่ค่าใหม่เป็นช่วงมาหรือเปล่า และค่าเก่าเป็นช่วงด้วย*/
                            if (txtNew.indexOf("-") > -1 && txtOld.indexOf("-") > -1) {
                                strCheckError = TwoAndTwo(txtNew, txtOld);
                            }
                            /*เช็คว่า User ได้ใส่ค่าใหม่เป็นช่วงมาหรือเปล่า และค่าเก่าไม่เป็นช่วง*/
                            else if (txtNew.indexOf("-") > -1 && txtOld.indexOf("-") == -1) {
                                strCheckError = TwoAndOne(txtNew, txtOld);
                            }
                            /*เช็คว่า User ได้ใส่ค่าใหม่ไม่เป็นช่วง และค่าเก่าเป็นช่วง*/
                            else if (txtNew.indexOf("-") == -1 && txtOld.indexOf("-") > -1) {
                                strCheckError = TwoAndOne(txtOld, txtNew);
                            }
                            /*เช็คว่า User ได้ใส่ค่าใหม่ไม่เป็นช่วง และค่าเก่าไม่เป็นช่วง*/
                            else if (txtNew.indexOf("-") == -1 && txtOld.indexOf("-") == -1) {
                                strCheckError = OneAndOne(txtNew, txtOld);
                            }
                        }

                    }
                    else {
                        strCheckError = "ไม่สามารถกรอตัวอักษรได้ยกเว้น -";
                    }
                }

                if (strCheckError != "") {
                    dxError('พบข้อผิดพลาด', strCheckError);
                    window["txtT" + PHASE + "_" + Index + ""].SetText('');
                    break;
                }
            }

        }

        /*Val1 และ Val2 มีค่าที่เป็นช่วง*/
        function TwoAndTwo(Val1, Val2) {
            var strArr = Val1.split("-");
            var strArr2 = Val2.split("-");

            if (parseFloat(strArr2[0]) <= parseFloat(strArr[0]) && parseFloat(strArr[0]) <= parseFloat(strArr2[1])) {
                return "ไม่สามารถระบุช่วงที่ทับซ้อนกันได้";
            }
            else if (parseFloat(strArr2[0]) <= parseFloat(strArr[1]) && parseFloat(strArr[1]) <= parseFloat(strArr2[1])) {
                return "ไม่สามารถระบุช่วงที่ทับซ้อนกันได้";
            }
            else {
                return "";
            }
        }


        /*Val1 สลับ Val2 เอาค่าที่เป็นช่วงขึ้น*/
        function TwoAndOne(Val1, Val2) {
            var strArr = Val1.split("-");
            if (parseFloat(strArr[0]) <= parseFloat(Val2) && parseFloat(Val2) <= parseFloat(strArr[1])) {
                return "ไม่สามารถระบุช่วงที่ทับซ้อนกันได้";
            }
            else {
                return "";
            }
        }

        /*Val1 ค่าใหม่ที่ใส่ Val2 ค่าเก่า*/
        function OneAndOne(Val1, Val2) {

            if (Val1 == Val2) {
                return "ไม่สามารถระบุช่วงที่ทับซ้อนกันได้";
            }
            else {
                return "";
            }

        }

        function isNumber(evt) {

            if (evt != null) {
                var iKeyCode = evt.keyCode;
                if (iKeyCode == '45' || (iKeyCode > 47 && iKeyCode < 58)) {

                }
                else {
                    evt.preventDefault();

                }

            }
        }

        function Check100(CRT) {

            txtWeightAll.SetText('0');

            var ValueOld = CRT.GetValue();
            var Value = 0;
            var gvwrow = txtgvwrowcount.GetValue();
            for (var i = 0; i < gvwrow; i++) {
                if (window["txtWeight_" + i + ""].GetText() != null && window["txtWeight_" + i + ""].GetText() != "") {
                    Value = parseInt(window["txtWeight_" + i + ""].GetText()) + parseInt(txtWeightAll.GetValue());
                    txtWeightAll.SetText(Value);
                }
            }

            if (parseInt(Value) < 101) {

                txtWeightAll.SetText(Value);
            }
            else {
                dxError('พบข้อผิดพลาด', 'ไม่สามารถ Weight รวมกันเกิน 100 ได้');
                CRT.SetText('');
                txtWeightAll.SetText(parseInt(Value) - parseInt(ValueOld));
            }
        }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxPageControl ID="ASPxPageControl" runat="server" ActiveTabIndex="0" Width="100%"
        OnActiveTabChanged="ASPxPageControl_ActiveTabChanged">
        <%--<ClientSideEvents ActiveTabChanged="function (s, e) {gvw.CancelEdit();gvwT3.CancelEdit();}" />--%>
        <TabPages>
            <dx:TabPage Name="p1" Text="จัดการหัวข้อการประเมินแบบ KPI" Visible="true">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
                            ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                            </ClientSideEvents>
                            <PanelCollection>
                                <dx:PanelContent>
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <%--<tr>
                                            <td>
                                                จัดการหัวข้อการประเมินแบบ KPI
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>
                                                <dx:ASPxTextBox runat="server" ID="txtWeightAll" ClientInstanceName="txtWeightAll"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox runat="server" ID="txtgvwrowcount" ClientInstanceName="txtgvwrowcount"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxGridView runat="server" ID="gvwT1" Width="100%" KeyFieldName="KPI_ID">
                                                    <Columns>
                                                        <dx:GridViewDataColumn FieldName="KPI_ID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="CACTIVE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ชื่อหัวข้อประเมิน" Width="17%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtHeadKPI" Text='<%# Eval("KPI_NAME") %>'>
                                                                    <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุหัวข้อ KPI" IsRequired="true" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ความถี่" Width="11%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxComboBox runat="server" ID="cboFrequency" Width="95%" Value='<%# Eval("KPI_FREQUENCY") %>'>
                                                                    <Items>
                                                                        <dx:ListEditItem Text="รายเดือน" Value="0" />
                                                                        <dx:ListEditItem Text="รายไตรมาส" Value="1" />
                                                                        <dx:ListEditItem Text="รายครึ่งปี" Value="2" />
                                                                        <dx:ListEditItem Text="รายปี" Value="3" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="วิธีคำนวณผลรวม" Width="10%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxComboBox runat="server" ID="cboSum" Width="95%" Value='<%# Eval("KPI_SUM") %>'>
                                                                    <Items>
                                                                        <dx:ListEditItem Text="ค่าเฉลี่ย" Value="0" />
                                                                        <dx:ListEditItem Text="ค่าต่ำสุด" Value="1" />
                                                                        <dx:ListEditItem Text="ค่าสูงสุด" Value="2" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="1" Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtT1" Text='<%# Eval("PHASE1") %>' HorizontalAlign="Center">
                                                                    <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="2" Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtT2" Text='<%# Eval("PHASE2") %>' HorizontalAlign="Center">
                                                                    <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="3" Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtT3" Text='<%# Eval("PHASE3") %>' HorizontalAlign="Center">
                                                                    <%--   <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="4" Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtT4" Text='<%# Eval("PHASE4") %>' HorizontalAlign="Center">
                                                                    <%--     <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="5" Width="8%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtT5" Text='<%# Eval("PHASE5") %>' HorizontalAlign="Center">
                                                                    <%--  <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event); }" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="Weight<br>(ผลรวมไม่เกิน 100)" Width="13%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxTextBox runat="server" ID="txtWeight" Text='<%# Eval("KPI_SUMWEIGHT") %>'
                                                                    HorizontalAlign="Right">
                                                                    <%--  <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                        ValidationGroup="add">
                                                                        <RequiredField ErrorText="ระบุค่า" IsRequired="true" />
                                                                    </ValidationSettings>--%>
                                                                    <ClientSideEvents KeyPress="function(s,e){ isNumber (event);}" TextChanged="function(s,e){ Check100(s);}" />
                                                                </dx:ASPxTextBox>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="การจัดการ" Width="9%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton runat="server" ID="btnDelete" Text="ลบหัวข้อ" AutoPostBack="false">
                                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <SettingsPager PageSize="100">
                                                    </SettingsPager>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxButton runat="server" ID="btnAdd" Text="เพิ่มหัวข้อ" CssClass="dxeLineBreakFix"
                                                    AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Add'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <dx:ASPxButton runat="server" ID="btnSave" Text="บันทึกค่า" CssClass="dxeLineBreakFix"
                                                    AutoPostBack="false">
                                                    <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('Save'); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton runat="server" ID="btnCancel" Text="ยกเลิก" CssClass="dxeLineBreakFix"
                                                    AutoPostBack="false">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Cancel'); }" />
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="p2" Text="ประเมินผลผู้ขนส่งตาม KPI" Visible="true">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                        <dx:ASPxCallbackPanel ID="xcpnT2" runat="server" HideContentOnCallback="False" OnCallback="xcpnT2_Callback"
                            ClientInstanceName="xcpnT2" CausesValidation="False" OnLoad="xcpnT2_Load">
                            <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                            </ClientSideEvents>
                            <PanelCollection>
                                <dx:PanelContent>
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="6%" align="right">
                                                            ผู้ขนส่ง:
                                                        </td>
                                                        <td width="30%" align="left">
                                                            <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="SVENDORID"
                                                                TextFormatString="{1}" Width="100%" Height="25px" ClientInstanceName="cboVendor"
                                                                CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboVendor" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboVendor_ItemsRequestedByFilterConditionSQL">
                                                                <Columns>
                                                                    <dx:ListBoxColumn FieldName="SVENDORID" Width="90px" Caption="#"></dx:ListBoxColumn>
                                                                    <dx:ListBoxColumn FieldName="SABBREVIATION" Width="230px" Caption="ผู้ขนส่ง"></dx:ListBoxColumn>
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td width="6%" align="right">
                                                            สัญญา:
                                                        </td>
                                                        <td width="30%" align="left">
                                                            <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="SCONTRACTID"
                                                                TextFormatString="{1}" Width="100%" Height="25px" ClientInstanceName="cboSCONTRACT"
                                                                CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboSCONTRACT" OnItemRequestedByValue="cboSCONTRACT_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboSCONTRACT_ItemsRequestedByFilterConditionSQL">
                                                                <Columns>
                                                                    <dx:ListBoxColumn FieldName="SCONTRACTID" Width="20%" Caption="#"></dx:ListBoxColumn>
                                                                    <dx:ListBoxColumn FieldName="SCONTRACTNO" Width="80%" Caption="สัญญา"></dx:ListBoxColumn>
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td width="6%" align="right">
                                                            ปี:
                                                        </td>
                                                        <td width="12%" align="left">
                                                           <dx:ASPxComboBox runat="server" ID="cboYear" Width="80px"></dx:ASPxComboBox>
                                                        </td>
                                                        <td width="10%">
                                                            <dx:ASPxButton runat="server" ID="btnSearch" AutoPostBack="false" SkinID="_search">
                                                                <ClientSideEvents Click="function(s,e){ xcpnT2.PerformCallback('Search'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwT2" Width="100%">
                                                    <Columns>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" Width="45%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="35%">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="การจัดการ" Width="15%">
                                                            <DataItemTemplate>
                                                                <dx:ASPxButton runat="server" ID="btnEdit" AutoPostBack="false" Text="ประเมินผล KPI">
                                                                    <ClientSideEvents Click="function(s,e){ xcpnT2.PerformCallback('KPI;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                </dx:ASPxButton>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dx:ASPxCallbackPanel>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
