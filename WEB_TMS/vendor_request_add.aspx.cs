﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.Configuration;
using System.Data.OracleClient;
using System.Configuration;



public partial class vendor_request_add : System.Web.UI.Page
{
    string _conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<STDataReq01> _dataReq01 = new List<STDataReq01>();
    //private static List<STStatusPan> _dataStatusPan = new List<STStatusPan>();
    string _sServiceWaterID = "00001";//รหัสบริการวัดน้ำ
    string _sServiceAddPanID = "00002";//รหัสบริการเพิ่มแป้น
    string _sConfigID = "001";//รหัสค่าวันที่คอนฟิก
    private static string sUserID = "";//User
    private static string _sTotalCap = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Session["UserID"] + "") && !string.IsNullOrEmpty(Session["SVDID"] + ""))
        {

            if (!IsPostBack)
            {
                // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
                sUserID = Session["UserID"] + "";
                
                string strReqType = Request.QueryString["reqtype"];
                string strVehID = Request.QueryString["vehID"];
                string strTuID = Request.QueryString["tuID"];
                string strCarTypeID = Request.QueryString["carTypeID"];


                string[] _strReqType;
                string[] _strVehID;
                string[] _strTuID;
                string[] _strCarTypeID;

                string reqTypeID = "";
                string vehID = "";
                string tuID = "";
                string sCarTypeID = "";

                //CarTypeID
                if (!string.IsNullOrEmpty(strCarTypeID))
                {
                    _strCarTypeID = STCrypt.DecryptURL(strCarTypeID);
                    sCarTypeID = _strCarTypeID[0];
                }

                //Veh ID
                if (!string.IsNullOrEmpty(strVehID))
                {
                    _strVehID = STCrypt.DecryptURL(strVehID);
                    vehID = _strVehID[0];
                }
                //Tu ID
                if (!string.IsNullOrEmpty(strTuID))
                {
                    _strTuID = STCrypt.DecryptURL(strTuID);
                    tuID = _strTuID[0];
                }
                //Req Type 
                if (!string.IsNullOrEmpty(strReqType))
                {
                    _strReqType = STCrypt.DecryptURL(strReqType);
                    reqTypeID = _strReqType[0];

                }

                //เซ็ต ประเภทคำขอ และสาเหตุ
                SetComBoReqType(cboRequestType, reqTypeID);
                SetComBoCause(cboCause);

                //เซ็ตข้อมูลพื้นฐาน โดยจะเช็คว่า ถ้าประเภทรถเป็น 0 จะส่งหัวไปหาข้อมูล แต่ถ้าเป็นหางจะส่งหางไปหาข้อมูล
                _dataReq01.Clear();
                _dataReq01.Add(new STDataReq01
                {
                    sReqTypeID = reqTypeID,
                    VEH_ID = vehID,
                    Tu_ID = tuID,
                    sUserID = sUserID,
                    sCauseID = "00001",
                    sCarTypeID = sCarTypeID
                });


                SetBasicData(sCarTypeID, vehID, tuID);
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {



    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (!string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            string[] paras = e.Parameter.Split(';');

            switch (paras[0])
            {
                case "saveNewReq":

                    foreach (var _i in _dataReq01.ToList())
                    {
                        if (!string.IsNullOrEmpty(_i.nCapacityAll) || _i.nCompartAll != "0" || _i.nPanAll != "0")
                        {
                            string _reqID = "";
                            string _VehChasisID = _i.sVehChasis;
                            string _carcatID = _i.sCarCateID;
                            string _causeID = _i.sCauseID;
                            string _reqTypeID = _i.sReqTypeID;
                            string _vendorID = _i.sVendorID;
                            string _VehID = _i.VEH_ID;
                            string _VehNo = _i.VEH_No;

                            string _TuID = _i.Tu_ID;
                            string _TuNo = _i.Tu_No;
                            string _TuChasisID = _i.sTuChasis;
                            string _total = _i.nTotal;
                            string _nCapTotal = _i.nCapacityAll;
                            string _CODE = _i.sUserID;
                            string _servicePrice = _i.nServicePrice;
                            string _servicePriceID = _i.sServicePriceID;
                            //string _REQUEST_OTHER_FLAG = _i.sReqOther;
                            string _REQUEST_OTHER_FLAG = "";
                            string _TOTLE_CAP = _i.nCapacityAll;
                            string _nCompart = _i.nCompartAll;

                            string CodiTionDateWaterExpire = "";
                            string sTempDate = "";
                            if (!string.IsNullOrEmpty(lblWaterExpire.Text))
                            {
                                string[] ArrDate = lblWaterExpire.Text.Split('/');
                                sTempDate = (int.Parse(ArrDate[2]) - 543) + "/" + ArrDate[1] + "/" + ArrDate[0];// + " 00:00:00";
                                CodiTionDateWaterExpire = "TO_DATE('" + sTempDate + "', 'YYYY-MM-DD')";
                            }
                            else
                            {
                                CodiTionDateWaterExpire = "null";
                            }
                            using (OracleConnection con = new OracleConnection(_conn))
                            {
                                con.Open();

                                #region Check exsit request
                                if (SystemFunction.CheckCar(_dataReq01.FirstOrDefault().VEH_ID, _dataReq01.FirstOrDefault().Tu_ID))
                                {

                                }
                                else
                                {
                                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Error + "','มีการยื่นคำขอแล้ว ไม่สามารถยื่นซ้ำได้',function(){window.location='vendor_HomeAlert.aspx';});");
                                    return;
                                }
                                #endregion
                                _reqID = SystemFunction.genID_TBL_REQUEST("01");

                                #region Query
                                string _qryInsertReq = @"INSERT INTO TBL_REQUEST(REQUEST_ID,REQTYPE_ID,CAUSE_ID , CARCATE_ID ,STRUCKID,VEH_ID,VEH_NO ,TU_ID,TU_NO,VEH_CHASSIS,TU_CHASSIS,VENDOR_ID 
                            ,TOTLE_CAP,TOTLE_SLOT,TOTLE_SERVCHAGE,STATUS_FLAG,CREATE_CODE,CREATE_DATE,REQUEST_OTHER_FLAG,RK_FLAG,WATER_EXPIRE_DATE) VALUES ("
                         + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_reqTypeID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_causeID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_carcatID) + "'"
                                    // + ",SYSDATE"
                         + ",'" + CommonFunction.ReplaceInjection(_VehID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_VehID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_VehNo) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_TuID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_TuNo) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_VehChasisID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_TuChasisID) + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_vendorID) + "'"
                         + "," + CommonFunction.ReplaceInjection(_TOTLE_CAP + "") + ""
                         + ",'" + CommonFunction.ReplaceInjection(_nCompart + "") + "'"
                           + ",'" + CommonFunction.ReplaceInjection(_i.nTotal + "") + "'"
                         + ",'" + CommonFunction.ReplaceInjection("09") + "'"
                         + ",'" + CommonFunction.ReplaceInjection(_CODE) + "'"
                         + ",SYSDATE"
                         + ",'" + CommonFunction.ReplaceInjection(_REQUEST_OTHER_FLAG) + "'"
                         + ",'" + CommonFunction.ReplaceInjection("N") + "'"
                          + ", " + CodiTionDateWaterExpire + @""
                         + ")";
                                #endregion
                                using (OracleCommand cmd = new OracleCommand(_qryInsertReq, con))
                                {
                                    if (cmd.ExecuteNonQuery() > 0)
                                    {
                                        SaveCapacityDetail(_reqID, int.Parse(_nCompart));
                                        SaveTBLReqItem(_reqID, _sServiceWaterID, "1", _i.nServicePrice);
                                        string nNewPan = !string.IsNullOrEmpty(_i.nPanAll) ? _i.nPanAll : "0";
                                        if (nNewPan != "0")
                                        {
                                            SaveTBLReqItem(_reqID, _sServiceAddPanID, nNewPan, _i.nPricePan);
                                        }
                                        SystemFunction.Add_To_TBL_REQREMARK(_reqID, "N", SystemFunction.GetRemark_WorkFlowRequest(1), "09", "S", Session["UserID"] + "", SystemFunction.GetDesc_WorkFlowRequest(1), "", "N");
                                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='download_invoice_request.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(_reqID)) + "';});");
                                    }
                                }
                            }
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxError('" + Resources.CommonResource.Msg_Alert_Title_Error + "','" + Resources.CommonResource.Msg_Error + " เนื่องจากรถไม่มีความจุ')");
                        }
                    }
                    break;

                case "back":
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_HomeAlert.aspx";
                    break;

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
    }

    private void SetComBoReqType(ASPxComboBox comboBox, string ReqTypeID)
    {
        comboBox.Items.Clear();
        comboBox.DataSource = sdsReqType;
        comboBox.ValueField = "ID";
        comboBox.TextField = "NAME";
        comboBox.DataBind();
        comboBox.Value = ReqTypeID;
        comboBox.ClientEnabled = false;
    }

    private void SetComBoCause(ASPxComboBox comboBox)
    {
        comboBox.Items.Clear();
        comboBox.DataSource = sdsCause;
        comboBox.ValueField = "ID";
        comboBox.TextField = "NAME";
        comboBox.DataBind();
        comboBox.Value = "00001";
        comboBox.ClientEnabled = false;
    }

    public void SetBasicData(string sCarTypeID, string STRUCKID, string STRUCKIDTAIL)
    {

        string VENDOR_ID = SystemFunction.GET_VENDORID(sUserID);

        #region เซตความจุรวม
        string _qrySumCap = @"
                SELECT STRUCKID ,COUNT(NCOMPARTNO) as NCOMPARTNO,SUM(CAP) as NCAPTOTAL
                FROM 
                (
                    SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) as CAP
                    FROM TTRUCK_COMPART 
                    WHERE STRUCKID ='" + CommonFunction.ReplaceInjection(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL) + @"' AND NPANLEVEL <> 0
                    GROUP BY STRUCKID,NCOMPARTNO
                )
                GROUP BY STRUCKID";


        _sTotalCap = "";
        DataTable _dtSumCap = new DataTable();
        _dtSumCap = CommonFunction.Get_Data(_conn, _qrySumCap);
        if (_dtSumCap.Rows.Count > 0)
        {
            _sTotalCap = _dtSumCap.Rows[0]["NCAPTOTAL"] + "";
            lblCompartTotal.Text = _dtSumCap.Rows[0]["NCOMPARTNO"] + "";
            lblCapacityTotal.Text = !string.IsNullOrEmpty(_sTotalCap) ? int.Parse(_sTotalCap).ToString("#,###,###,###") : "0";
        }
        #endregion

        //เซ็ตความจุของรถ
        SetCapacityData(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL);

        //เซ็ตข้อมูลรถ
        SetDetailCar(sCarTypeID, STRUCKID, STRUCKIDTAIL, VENDOR_ID);

    }

    //เซ็ตรถ
    private void SetDetailCar(string sCarTypeID, string STRUCKID, string STRUCKIDTAIL, string VENDOR_ID)
    {
        string _qry = "";
        switch (sCarTypeID)
        {
            case "0"://รถบรรทุก ไม่มีหาง
                _qry = @"
                SELECT VEH_ID,VEH_CHASIS, VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO,MAX(SLOT1) as SLOT1,SUM(CAP1) as CAP1, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID FROM
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID,T.SHEADREGISTERNO  as REGISNO
                    ,TC.NCOMPARTNO as SLOT1,MAX(TC.NCAPACITY) as CAP1,NULL as SLOT2,NULL as CAP2,NULL as TU_ID,NULL as TU_CHASIS,NULL as TU_No,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO ,V.SVENDORID
                )
                GROUP BY VEH_ID,VEH_CHASIS,VEH_No,VENDORNAME,CARCATE_NAME,CARCATE_ID,REGISNO, SLOT2,CAP2,TU_ID,TU_CHASIS,TU_No,DWATEREXPIRE,SVENDORID";
                break;
            case "3":
                _qry = @"
                SELECT * FROM 
                (
                    SELECT T.STRUCKID as VEH_ID,T.SCHASIS as VEH_CHASIS,T.SHEADREGISTERNO as VEH_No,T.STRAILERREGISTERNO as TU_No,V.SVENDORID,V.SABBREVIATION as VENDORNAME,CT.CARCATE_NAME,CT.CARCATE_ID
                ,T.SHEADREGISTERNO||'/'||T.STRAILERREGISTERNO  as REGISNO,COUNT(TC.NCOMPARTNO) as SLOT1 ,SUM(TC.NCAPACITY) as CAP1
                FROM  TTRUCK T 
                LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID
                WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKID) + @"%'
                GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,V.SVENDORID
                )T1,
                (
                    SELECT TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,COUNT(SLOT2) as SLOT2,SUM(CAP2) as CAP2,DWATEREXPIRE
                    FROM
                    (
                        SELECT T.STRUCKID as TU_ID,T.SCHASIS as TU_CHASIS,CT.CARCATE_NAME,CT.CARCATE_ID,TC.NCOMPARTNO as SLOT2,MAX(TC.NCAPACITY) as CAP2,TRUNC(T.DWATEREXPIRE) as DWATEREXPIRE
                    FROM  TTRUCK T 
                    LEFT JOIN TVENDOR V ON T.STRANSPORTID = V.SVENDORID 
                    LEFT JOIN TBL_CARCATE CT ON T.CARCATE_ID = CT.CARCATE_ID 
                    LEFT JOIN TTRUCK_COMPART TC  ON T.STRUCKID =TC.STRUCKID AND TC.NPANLEVEL <> 0
                    WHERE   T.STRANSPORTID ='" + CommonFunction.ReplaceInjection(VENDOR_ID) + @"'
                    AND T.STRUCKID LIKE '%" + CommonFunction.ReplaceInjection(STRUCKIDTAIL) + @"%'

                    GROUP BY T.STRUCKID ,T.SCHASIS ,T.SHEADREGISTERNO,T.STRAILERREGISTERNO ,V.SABBREVIATION ,CT.CARCATE_NAME,CT.CARCATE_ID,T.DWATEREXPIRE,TC.NCOMPARTNO
                )
                GROUP BY TU_ID,TU_CHASIS,CARCATE_NAME,CARCATE_ID,DWATEREXPIRE
                ) T2";
                break;
        }



        DataTable _dtVendor = new DataTable();
        _dtVendor = CommonFunction.Get_Data(_conn, _qry);
        if (_dtVendor.Rows.Count > 0)
        {

            //lblWaterExpire.Text = String.Format("{0:dd/MM/yyyy}", Convert.ToString(_dtVendor.Rows[0]["DWATEREXPIRE"].ToString())).ToString();//_dtVendor.Rows[0]["DWATEREXPIRE"].ToString();
            lblWaterExpire.Text = !string.IsNullOrEmpty("" + _dtVendor.Rows[0]["DWATEREXPIRE"]) ? DateTime.Parse(_dtVendor.Rows[0]["DWATEREXPIRE"] + "").ToString("dd/MM/yyyy") : " - ";
            lblVendorName.Text = _dtVendor.Rows[0]["VENDORNAME"].ToString();
            lblLicenseNumber.Text = _dtVendor.Rows[0]["REGISNO"].ToString();
            lblTruckType.Text = _dtVendor.Rows[0]["CARCATE_NAME"].ToString();
            string _nSlot1 = _dtVendor.Rows[0]["SLOT1"].ToString();
            string _nSlot2 = _dtVendor.Rows[0]["SLOT2"].ToString();
            string _sChasis1 = _dtVendor.Rows[0]["VEH_CHASIS"].ToString();
            string _sChasis2 = _dtVendor.Rows[0]["TU_CHASIS"].ToString();

            #region Set รถใหม่/รถเก่า
            string _statusCar = "";

            #region เช็คประเภทรถแบบเก่า
            //if (sCarTypeID == "0")
            //{
            //    if (!string.IsNullOrEmpty(_nSlot1))
            //    {
            //        _statusCar = "0";
            //    }
            //    else
            //    {
            //        if (!string.IsNullOrEmpty(_sChasis1))
            //        {
            //            _statusCar = "0";
            //        }
            //        else
            //        {
            //            _statusCar = "1";
            //        }

            //    }

            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) && !string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (!string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) || string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) || !string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "0";
            //    }
            //    else if (string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT1"].ToString()) && string.IsNullOrEmpty(_dtVendor.Rows[0]["SLOT2"].ToString()))
            //    {
            //        _statusCar = "1";
            //    }
            //}
            #endregion

            #region เช็คประเภทรถแบบใหม่ เช็คจาก SCERT_NO

            _statusCar = CheckNewCarOrOldCar(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL);

            #endregion

            #endregion

            #region Update List
            _dataReq01.ToList().ForEach(f => f.VEH_No = _dtVendor.Rows[0]["VEH_No"].ToString());//update VEH_No
            _dataReq01.ToList().ForEach(f => f.Tu_No = _dtVendor.Rows[0]["Tu_No"].ToString());//update Tu_No
            _dataReq01.ToList().ForEach(f => f.sVehChasis = _dtVendor.Rows[0]["VEH_CHASIS"].ToString());//update VEH_CHASIS
            _dataReq01.ToList().ForEach(f => f.sTuChasis = _dtVendor.Rows[0]["TU_CHASIS"].ToString());//update TU_CHASIS
            _dataReq01.ToList().ForEach(f => f.sVendorName = _dtVendor.Rows[0]["VENDORNAME"].ToString());//update VENDORNAME
            _dataReq01.ToList().ForEach(f => f.sVendorID = _dtVendor.Rows[0]["SVENDORID"].ToString());//update VENDORNAME
            _dataReq01.ToList().ForEach(f => f.nCompartAll = (sCarTypeID == "0" ? _dtVendor.Rows[0]["SLOT1"] + "" : _dtVendor.Rows[0]["SLOT2"] + ""));//update SLOT   
            _dataReq01.ToList().ForEach(f => f.nCapacityAll = (sCarTypeID == "0" ? _dtVendor.Rows[0]["CAP1"] + "" : _dtVendor.Rows[0]["CAP2"] + ""));//update CAP
            _dataReq01.ToList().ForEach(f => f.sCarCate = _dtVendor.Rows[0]["CARCATE_NAME"].ToString());//update CARCATE_NAME
            _dataReq01.ToList().ForEach(f => f.sCarCateID = _dtVendor.Rows[0]["CARCATE_ID"].ToString());//update CARCATE_NAME
            _dataReq01.ToList().ForEach(f => f.sStatusCar = _statusCar);//update sStatusCar
            _dataReq01.ToList().ForEach(f => f.nPricePan = GetPricePan());//update nPricePan
            _dataReq01.ToList().ForEach(f => f.nCapacityAll = _sTotalCap);
            _dataReq01.ToList().ForEach(f => f.dWaterExpire = _dtVendor.Rows[0]["DWATEREXPIRE"].ToString());
            #endregion

            int Service = int.Parse(GetServicePrice(_sTotalCap, _statusCar, cboRequestType.Value + "", "00001", _dtVendor.Rows[0]["CARCATE_ID"].ToString()));
            int PricePan = int.Parse(GetPricePan());
            int CountPan = int.Parse(GetCalculatePan(sCarTypeID == "0" ? STRUCKID : STRUCKIDTAIL));

            _dataReq01.ToList().ForEach(f => f.nServicePrice = Service + "");
            _dataReq01.ToList().ForEach(f => f.nPricePan = PricePan + "");
            _dataReq01.ToList().ForEach(f => f.nTotal = (Service + (PricePan * CountPan)) + "");
            lblTotal.Text = "";
            lblTotal.Text = (Service + (PricePan * CountPan)).ToString(SystemFunction.CheckFormatNuberic(0));
        }
    }

    //เซ็ตความจุของรถ
    private void SetCapacityData(string STRUCKID)
    {

        ClearCaPacity();
        DataTable _dt = new DataTable();
        string _selectPan = "";


        _selectPan = @"SELECT 
 STRUCKID, NCOMPARTNO, NPANLEVEL
 ,NCAPACITY
FROM TTRUCK_COMPART
WHERE
NPANLEVEL = {0}
 AND STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";

        // statusPan 0 = แป้นเก่า 1 = เพิ่มแป้นใหม่ 
        DataTable dtPan1 = new DataTable();
        dtPan1 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "1"));
        DataTable dtPan2 = new DataTable();
        dtPan2 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "2"));
        DataTable dtPan3 = new DataTable();
        dtPan3 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "3"));

        #region Pan1
        if (dtPan1.Rows.Count > 0)
        {
            for (int i = 0; i < dtPan1.Rows.Count; i++)
            {
                switch (dtPan1.Rows[i]["NCOMPARTNO"].ToString())//ถ้าไม่มีค่าเป็น ถ้ามีค่าเป็น 0 = แป้นเก่า 1 = แป้นใหม่
                {
                    case "1": cboCapacity11.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2": cboCapacity12.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3": cboCapacity13.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4": cboCapacity14.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5": cboCapacity15.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6": cboCapacity16.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7": cboCapacity17.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8": cboCapacity18.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9": cboCapacity19.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10": cboCapacity110.Text = int.Parse(dtPan1.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }
        #endregion

        #region Pan2
        if (dtPan2.Rows.Count > 0)
        {
            for (int i = 0; i < dtPan2.Rows.Count; i++)
            {
                switch (dtPan2.Rows[i]["NCOMPARTNO"].ToString())
                {
                    case "1": cboCapacity21.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2": cboCapacity22.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3": cboCapacity23.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4": cboCapacity24.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5": cboCapacity25.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6": cboCapacity26.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7": cboCapacity27.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8": cboCapacity28.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9": cboCapacity29.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10": cboCapacity210.Text = int.Parse(dtPan2.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }

        #endregion

        #region Pan3
        if (dtPan3.Rows.Count > 0)
        {
            tbPan3.Visible = true;
            for (int i = 0; i < dtPan3.Rows.Count; i++)
            {
                switch (dtPan3.Rows[i]["NCOMPARTNO"].ToString())
                {
                    case "1": lblCapacity31.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "2": lblCapacity32.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "3": lblCapacity33.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "4": lblCapacity34.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "5": lblCapacity35.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "6": lblCapacity36.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "7": lblCapacity37.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "8": lblCapacity38.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "9": lblCapacity39.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                    case "10": lblCapacity310.Text = int.Parse(dtPan3.Rows[i]["NCAPACITY"] + "").ToString(SystemFunction.CheckFormatNuberic(0));
                        break;
                }
            }
        }
        else
        {
            tbPan3.Visible = false;
            lblCapacity31.Text = "";
            lblCapacity32.Text = "";
            lblCapacity33.Text = "";
            lblCapacity34.Text = "";
            lblCapacity35.Text = "";
            lblCapacity36.Text = "";
            lblCapacity37.Text = "";
            lblCapacity38.Text = "";
            lblCapacity39.Text = "";
            lblCapacity310.Text = "";
        }
        #endregion
    }

    //ข้อมูลราคาของการเพิ่มแป้น
    public string GetPricePan()
    {
        string _price = "0";

        string _qryPrice = @"SELECT P.SERVICECOST_ID, P.SERVICECHARGE
FROM  TBL_SERVICECOST P
WHERE P.SERVICE_ID='" + CommonFunction.ReplaceInjection("00002") + "' "
+ "AND P.ISACTIVE_FLAG='Y'";
        DataTable _dtPrice = new DataTable();
        _dtPrice = CommonFunction.Get_Data(_conn, _qryPrice);
        if (_dtPrice.Rows.Count > 0)
        {
            _price = _dtPrice.Rows[0]["SERVICECHARGE"].ToString();

        }
        return _price;
    }

    //เช็ครถเก่ารถใหม่
    private string CheckNewCarOrOldCar(string STRUCKID)
    {
        string Result = "";

        string Query = "SELECT STRUCKID,SCAR_NUM, SCERT_NO,SCHASIS FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
        DataTable dt = CommonFunction.Get_Data(_conn, Query);
        if (dt.Rows.Count > 0)
        {
            if (!string.IsNullOrEmpty(dt.Rows[0]["SCAR_NUM"] + ""))
            {
                Result = "0";
            }
            else
            {
                Result = "1";
            }

        }
        else
        {
            Result = "1";
        }

        return Result;
    }

    //เช็คราคาการให้บริการของรถ
    public string GetServicePrice(string _capacity, string _carType, string _reqType, string _serviceID, string _carCate)
    {
        string _price = "0";
        string Codition = "";
        if (!string.IsNullOrEmpty(_capacity))
        {
            Codition = " AND (" + CommonFunction.ReplaceInjection(_capacity) + " BETWEEN P.START_CAPACITY AND  P.END_CAPACITY)";
        }
        else
        {
            return _price;
        }

        string _qryPrice = @"SELECT P.SERVICECOST_ID, P.SERVICECHARGE
                            FROM  TBL_SERVICECOST P
                            WHERE P.CAR_GRADE ='" + CommonFunction.ReplaceInjection(_carType) + "'"
                            + " AND P.REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + "' "
                            + " AND P.SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + "' "
                            + " AND P.ISACTIVE_FLAG='Y'"
                            + " AND P.CARCATE_ID ='" + CommonFunction.ReplaceInjection(_carCate) + "' " + Codition + "";

        DataTable _dtPrice = new DataTable();
        _dtPrice = CommonFunction.Get_Data(_conn, _qryPrice);
        if (_dtPrice.Rows.Count > 0)
        {
            _price = _dtPrice.Rows[0]["SERVICECHARGE"].ToString();
            _dataReq01.ToList().ForEach(f => f.nServicePrice = _dtPrice.Rows[0]["SERVICECHARGE"].ToString());//update _priceService
            _dataReq01.ToList().ForEach(f => f.sServicePriceID = _dtPrice.Rows[0]["SERVICECOST_ID"].ToString());//update SERVICECOST_ID
        }
        else
        {
            string _qryPrice2 = @"
            SELECT S.SERVICECHARGE   ,L.SERVICECOST_ID 
            FROM  
            (
                SELECT MAX(SERVICECHARGE)  as SERVICECHARGE FROM  TBL_SERVICECOST  WHERE CAR_GRADE ='" + CommonFunction.ReplaceInjection(_carType) + @"' AND REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + @"'  AND SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + @"'  AND ISACTIVE_FLAG='Y' AND CARCATE_ID ='" + CommonFunction.ReplaceInjection(_carCate) + @"'
            )S
            INNER  JOIN TBL_SERVICECOST l 
            ON s.SERVICECHARGE = l.SERVICECHARGE AND  L.CAR_GRADE = '" + CommonFunction.ReplaceInjection(_carType) + @"' AND  L.CARCATE_ID = '" + CommonFunction.ReplaceInjection(_carCate) + "' AND L.REQTYPE_ID='" + CommonFunction.ReplaceInjection(_reqType) + "' AND L.SERVICE_ID='" + CommonFunction.ReplaceInjection(_serviceID) + "'   AND L.ISACTIVE_FLAG='Y'";

            DataTable _dtPrice2 = new DataTable();
            _dtPrice2 = CommonFunction.Get_Data(_conn, _qryPrice2);

            if (_dtPrice2.Rows.Count > 0)
            {
                _price = _dtPrice2.Rows[0]["SERVICECHARGE"].ToString();
                _dataReq01.ToList().ForEach(f => f.nServicePrice = _dtPrice2.Rows[0]["SERVICECHARGE"].ToString());//update _priceService
                _dataReq01.ToList().ForEach(f => f.sServicePriceID = _dtPrice2.Rows[0]["SERVICECOST_ID"].ToString());//update SERVICECOST_ID
            }
        }

        return _price;
    }

    //คำนวนว่าแป้นสองมีกี่แป้น
    public string GetCalculatePan(string STRUCKID)
    {
        string Result = "0";

        string _selectPan = @"SELECT 
 STRUCKID, NCOMPARTNO, NPANLEVEL
 ,NCAPACITY
FROM TTRUCK_COMPART
WHERE
NPANLEVEL = {0}
 AND STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCKID) + "'";
        DataTable dtPan2 = new DataTable();
        dtPan2 = CommonFunction.Get_Data(_conn, string.Format(_selectPan, "2"));
        //เหตุผลที่ Count Row เนื่องจากมีบางคันที่รถเกินใส่ช่องไฟสลับกันทำให้เกิดช่องว่าง จะทำให้นับช่องเกินจริง
        Result = dtPan2.Rows.Count + "";

        _dataReq01.ToList().ForEach(f => f.nPanAll = Result);

        return Result;
    }

    public void SaveCapacityDetail(string _reqID, int _nCompart)
    {

        string _pan1 = "";
        string _pan2 = "";
        string _pan3 = "";


        //ที่ให้วนหมดเพราะต้องการให้ใส่ข้อมูลเข้าไปถูก อาจจะมีการเว้น่ช่องของข้อมูล
        for (int _compartNo = 1; _compartNo <= 10; _compartNo++)
        {
            switch (_compartNo)
            {
                case 1:

                    _pan1 = cboCapacity11.Text;
                    _pan2 = cboCapacity21.Text;
                    _pan3 = lblCapacity31.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }

                    break;
                case 2:
                    _pan1 = cboCapacity12.Text;
                    _pan2 = cboCapacity22.Text;
                    _pan3 = lblCapacity32.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 3:
                    _pan1 = cboCapacity13.Text;
                    _pan2 = cboCapacity23.Text;
                    _pan3 = lblCapacity33.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 4:
                    _pan1 = cboCapacity14.Text;
                    _pan2 = cboCapacity24.Text;
                    _pan3 = lblCapacity34.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 5:
                    _pan1 = cboCapacity15.Text;
                    _pan2 = cboCapacity25.Text;
                    _pan3 = lblCapacity35.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 6:
                    _pan1 = cboCapacity16.Text;
                    _pan2 = cboCapacity26.Text;
                    _pan3 = lblCapacity36.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 7:
                    _pan1 = cboCapacity17.Text;
                    _pan2 = cboCapacity27.Text;
                    _pan3 = lblCapacity37.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 8:
                    _pan1 = cboCapacity18.Text;
                    _pan2 = cboCapacity28.Text;
                    _pan3 = lblCapacity38.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 9:
                    _pan1 = cboCapacity19.Text;
                    _pan2 = cboCapacity29.Text;
                    _pan3 = lblCapacity39.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
                case 10:
                    _pan1 = cboCapacity110.Text;
                    _pan2 = cboCapacity210.Text;
                    _pan3 = lblCapacity310.Text;
                    if (!string.IsNullOrEmpty(_pan1))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "1", _pan1, "0");
                    }
                    if (!string.IsNullOrEmpty(_pan1) && !string.IsNullOrEmpty(_pan2))
                    {
                        SaveTBLReqSlot(_reqID, _compartNo + "", "2", _pan2, "1");
                    }
                    if (!string.IsNullOrEmpty(_pan1) || !string.IsNullOrEmpty(_pan2) || !string.IsNullOrEmpty(_pan3))
                    {
                        if (!string.IsNullOrEmpty(_pan3))
                        {
                            SaveTBLReqSlot(_reqID, _compartNo + "", "3", _pan3, "0");
                        }
                    }
                    break;
            }
        }

    }

    public void SaveTBLReqSlot(string _reqID, string _slot, string _pan, string _capacity, string _status)
    {
        if (_capacity.Contains(','))
        {
            _capacity = _capacity.Replace(",", "");
        }


        string Check = @"SELECT REQUEST_ID, SLOT_NO, LEVEL_NO,  CAPACITY, LEVEL_STATUS
FROM TBL_REQSLOT WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";

        int cCount = CommonFunction.Count_Value(_conn, Check);


        string _qryInsertDetail = "";

        string _qryUpdateDetail = @"UPDATE TBL_REQSLOT
SET   CAPACITY     = " + CommonFunction.ReplaceInjection(_capacity + "") + @"
WHERE  REQUEST_ID   = '" + CommonFunction.ReplaceInjection(_reqID) + @"'
AND    SLOT_NO      = " + CommonFunction.ReplaceInjection(_slot + "") + @"
AND    LEVEL_NO     =" + CommonFunction.ReplaceInjection(_pan + "") + @"
AND    LEVEL_STATUS     ='" + CommonFunction.ReplaceInjection(_status + "") + @"'
";


        _qryInsertDetail = @" INSERT INTO TBL_REQSLOT VALUES ("
              + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
              + "," + CommonFunction.ReplaceInjection(_slot + "")
              + "," + CommonFunction.ReplaceInjection(_pan + "")
              + "," + CommonFunction.ReplaceInjection(!string.IsNullOrEmpty(_capacity + "") ? _capacity + "" : "null")
              + ",'" + CommonFunction.ReplaceInjection(_status + "") + "'"
              + ",''"
              + ")";
        using (OracleConnection con = new OracleConnection(_conn))
        {
            con.Open();
            if (cCount > 0)
            {
                using (OracleCommand cmd = new OracleCommand(_qryUpdateDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                using (OracleCommand cmd = new OracleCommand(_qryInsertDetail, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public void SaveTBLReqItem(string _reqID, string _serviceID, string _nItem, string _nPrice)
    {
        if (string.IsNullOrEmpty(_nPrice))
        {
            _nPrice = "0";
            if (string.IsNullOrEmpty(_nItem))
            {
                _nItem = "0";
            }
        }

        string _nTotal = (int.Parse(_nItem) * int.Parse(_nPrice)) + "";
        string _qryInsertDetail = "";
        _qryInsertDetail = @" INSERT INTO TBL_REQUEST_ITEM (REQUEST_ID, SERVICE_ID, NITEM, 
   NPRICE) VALUES ("
              + "'" + CommonFunction.ReplaceInjection(_reqID) + "'"
              + ",'" + CommonFunction.ReplaceInjection(_serviceID) + "'"
              + "," + CommonFunction.ReplaceInjection(_nItem)
              + "," + CommonFunction.ReplaceInjection(_nTotal)

              + ")";
        using (OracleConnection con = new OracleConnection(_conn))
        {
            con.Open();
            using (OracleCommand cmd = new OracleCommand(_qryInsertDetail, con))
            {
                cmd.ExecuteNonQuery();
            }
        }
    }

    void ClearCaPacity()
    {
        cboCapacity11.Text = "";
        cboCapacity12.Text = "";
        cboCapacity13.Text = "";
        cboCapacity14.Text = "";
        cboCapacity15.Text = "";
        cboCapacity16.Text = "";
        cboCapacity17.Text = "";
        cboCapacity18.Text = "";
        cboCapacity19.Text = "";
        cboCapacity110.Text = "";
        cboCapacity21.Text = "";
        cboCapacity22.Text = "";
        cboCapacity23.Text = "";
        cboCapacity24.Text = "";
        cboCapacity25.Text = "";
        cboCapacity26.Text = "";
        cboCapacity27.Text = "";
        cboCapacity28.Text = "";
        cboCapacity29.Text = "";
        cboCapacity210.Text = "";
        lblCapacity31.Text = "";
        lblCapacity32.Text = "";
        lblCapacity33.Text = "";
        lblCapacity34.Text = "";
        lblCapacity35.Text = "";
        lblCapacity36.Text = "";
        lblCapacity37.Text = "";
        lblCapacity38.Text = "";
        lblCapacity39.Text = "";
        lblCapacity310.Text = "";
    }

    #region Structure
    public class STDataReq01
    {
        public string sUserID { get; set; }
        public string sVendorID { get; set; }
        public string sVendorName { get; set; }
        public string sCarCateID { get; set; }
        public string sCarCate { get; set; }
        public string sCauseID { get; set; }
        public string sReqTypeID { get; set; }
        public string VEH_ID { get; set; }
        public string Tu_ID { get; set; }
        public string VEH_No { get; set; }
        public string Tu_No { get; set; }
        public string sVehChasis { get; set; }
        public string sTuChasis { get; set; }
        public string sStatusCar { get; set; }
        public string sCarTypeID { get; set; }
        public string dWaterExpire { get; set; }
        public string nPanAll { get; set; }
        public string nCompartAll { get; set; }
        public string nCapacityAll { get; set; }
        public string nTotal { get; set; }
        public string nServicePrice { get; set; }
        public string sServicePriceID { get; set; }
        public string nPricePan { get; set; }

    }
    #endregion

}