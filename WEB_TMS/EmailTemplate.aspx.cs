﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;
using TMS_BLL.Master;
using System.Web.Security;


public partial class EmailTemplate : PageBase
{
    #region + View State +
    private DataTable dtTemplate
    {
        get
        {
            if ((DataTable)ViewState["dtTemplate"] != null)
                return (DataTable)ViewState["dtTemplate"];
            else
                return null;
        }
        set
        {
            ViewState["dtTemplate"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //DataTable dt = new DataTable();
        //dt.Columns.Add("No");
        //dt.Columns.Add("EMAIL_TEMPLATE_ID");
        //dt.Columns.Add("EMAIL_TEMPLATE_NAME");
        //dt.Columns.Add("DETAIL");
        //dt.Columns.Add("Status");

        //dt.Rows.Add("1", "1", "Email Template 1 .....", "Detail ...", "ใช้งาน");
        //dt.Rows.Add("2", "2", "Email Template 2 .....", "Detail ...", "ใช้งาน");
        //dt.Rows.Add("3", "3", "Email Template 3 .....", "Detail ...", "ใช้งาน");
        //dt.Rows.Add("4", "4", "Email Template 4 .....", "Detail ...", "ไม่ใช้งาน");

        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadEmailType();
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadEmailType()
    {
        try
        {
            DataTable dtEmailType = EmailTemplateBLL.Instance.EmailTypeSelectBLL(" AND M_EMAIL_TYPE.ISACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlEmailType, dtEmailType, "EMAIL_TYPE_ID", "EMAIL_TYPE_NAME", true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectAllBLL(Condition);
            GridViewHelper.BindGridView(ref dgvTemplate, dtTemplate);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlEmailType.SelectedIndex < 1)
                this.SearchData(" AND TEMPLATE_NAME LIKE '%" + txtSearch.Text.Trim() + "%'");
            else
                this.SearchData(" AND M_EMAIL_TEMPLATE.EMAIL_TYPE_ID = " + ddlEmailType.SelectedValue + " AND TEMPLATE_NAME LIKE '%" + txtSearch.Text.Trim() + "%'");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvTemplate.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvTemplate, dtTemplate);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void dgvTemplate_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvTemplate.DataKeys[e.RowIndex]["TEMPLATE_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            
            MsgAlert.OpenForm("EmailTemplateAdd.aspx?TEMPLATE_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}