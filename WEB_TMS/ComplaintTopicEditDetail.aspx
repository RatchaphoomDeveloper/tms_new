﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ComplaintTopicEditDetail.aspx.cs" Inherits="ComplaintTopicEditDetail" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="lblHeaderTab1" runat="server" Text="ประเภท และหัวข้อ"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Table ID="tblAddHeader" Width="100%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell Style="width: 250px">
                                                <asp:RadioButtonList ID="radType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="COMPLAIN" Text="Complain" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="CHECKTRUCK" Text="Surprise Check"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="lblTopicName" runat="server" Text="ประเภท :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlTopic" runat="server" CssClass="form-control" Width="300px"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="lblStatus" runat="server" Text="สถานะ :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radTopicStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="Active" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="Label1" runat="server" Text="หัวข้อ :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell></asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlComplainType" runat="server" CssClass="form-control" Width="300px" AutoPostBack="true" OnSelectedIndexChanged="ddlComplainType_SelectedIndexChanged"></asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell Style="text-align: right">
                                                <asp:Label ID="Label2" runat="server" Text="สถานะ :"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:RadioButtonList ID="radComplainStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="1" Text="Active"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="InActive"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                        <asp:TableRow>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdCancel_Click" />
                        </div>
                    </div>

                    <%--<div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="Label3" runat="server" Text="Require Field"></asp:Label>
                        </div>
                        <div class="panel-body" style="text-align:center">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <asp:Panel ID="pnlRequireField" runat="server">
                                        <asp:Table runat="server" style="width:100%">
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblTopic" runat="server" Text="ประเภทร้องเรียน : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqTopic" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblOrganiz" runat="server" Text="คลังต้นทาง : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqOrganiz" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblComplainType" runat="server" Text="หัวข้อร้องเรียน : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqComplainType" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblVendor" runat="server" Text="ผู้ประกอบการขนส่ง : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqVendor" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblContract" runat="server" Text="เลขที่สัญญา : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqContract" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblDelivery" runat="server" Text="Delivery No. : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqDelivery" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblHeadRegist" runat="server" Text="ทะเบียนรถ (หัว) : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqHeaderRegist" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblPersonalNo" runat="server" Text="ชื่อ พขร. ที่ถูกร้องเรียน : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqPersonalNo" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblComplainAddress" runat="server" Text="สถานที่เกิดเหตุ : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqComplainAddress" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblDateTran" runat="server" Text="วันที่เกิดเหตุ : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqDateTran" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="text-align:right">
                                                    <asp:Label ID="lblTotalCar" runat="server" Text="จำนวนรถ (คัน) : &nbsp;"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="ddlReqTotalCar" runat="server" CssClass="form-control" Width="250px"></asp:DropDownList>
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:right">
                                                    
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:table>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <%--<div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="Label4" runat="server" Text="Attach File"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <%--<div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>
                            <asp:Label ID="Label5" runat="server" Text="Other"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
