﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_VisitForm_add_old.aspx.cs" Inherits="admin_ChkList_add" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15 {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF">
                            <dx:ASPxPageControl ID="PagControl" runat="server" ActiveTabIndex="0" ClientInstanceName="PagControl"
                                Width="100%">
                                <ClientSideEvents TabClick="function (s,e){if(!ASPxClientEdit.ValidateGroup('add') && PagControl.GetActiveTabIndex() == 0) e.cancel = true ; txtRunNumber.SetValue(gvw.GetVisibleRowsOnPage() +1)}" />
                                <TabPages>
                                    <dx:TabPage Name="t1" Text="ชื่อแบบฟอร์ม">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table cellpadding="2px" cellspacing="2px">
                                                    <tr>
                                                        <td bgcolor="#FFFFFF">ชื่อแบบฟอร์ม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style21">
                                                            <dx:ASPxTextBox ID="txtExam1" runat="server" MaxLength="200" Width="270px" ClientInstanceName="txtExam1">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                            <table>
                                                                <tr>
                                                                    <td>ระดับคะแนน <font color="#ff0000">*</font>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnAddd" runat="server" SkinID="_add" CssClass="dxeLineBreakFix"
                                                                            Width="70px">
                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('AddClick'); }"></ClientSideEvents>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxGridView ID="sgvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true" Border-BorderStyle="None"
                                                                Settings-ShowColumnHeaders="false" Style="margin-top: 0px" ClientInstanceName="sgvw"
                                                                Width="100%" KeyFieldName="dtsID" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                                                SkinID="_gvw">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                                                        VisibleIndex="0">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataColumn Caption="รหัส" FieldName="dtsID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataTextColumn VisibleIndex="3" Width="20%" FieldName="dtsScoreName">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxTextBox ID="txtScoreName" runat="server" Width="150px" Text='<%# Eval("dtsScoreName") %>'>
                                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                    ValidationGroup="add">
                                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                                </ValidationSettings>
                                                                            </dx:ASPxTextBox>
                                                                        </DataItemTemplate>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn VisibleIndex="4" Width="10%" FieldName="dd" CellStyle-ForeColor="#0066FF">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxLabel ID="lblScore" runat="server" Text="คะแนนที่ได้">
                                                                            </dx:ASPxLabel>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle ForeColor="#0066FF" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="คะแนนที่ได้" VisibleIndex="5" Width="30%">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxTextBox ID="txtScore" runat="server" Width="100px" Text='<%# Eval("dtcScore") %>'>
                                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                                                ValidationGroup="add">
                                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                                </ErrorFrameStyle>
                                                                                                <RequiredField ErrorText="กรุณากรอกข้อมูล" IsRequired="True" />
                                                                                                <RegularExpression ErrorText="กรุณาระบุตัวเลขจำนวนเต็ม" ValidationExpression="<%$ Resources:ValidationResource, Valid_Number %>" />
                                                                                            </ValidationSettings>
                                                                                        </dx:ASPxTextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" SkinID="_delete"
                                                                                            Width="10px">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('deleteList;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                                <Settings ShowColumnHeaders="False" GridLines="None"></Settings>

                                                                <Border BorderStyle="None"></Border>
                                                            </dx:ASPxGridView>
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none;">
                                                        <td>ปีที่ใช้งาน <font color="#ff0000">*</font></td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px" SelectedIndex="0"></dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>วันที่เริ่มใช้งาน
                                                        <font color="#ff0000">*</font></td>
                                                        <td>
                                                            <dx:ASPxDateEdit ID="dteDateStart1" runat="server" SkinID="xdte" ClientInstanceName="dteDateStart1">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" style="width: 80%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus1" runat="server" ClientInstanceName="rblStatus1"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td align="center">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnSAVE1" runat="server" Text="บันทึก" AutoPostBack="false">
                                                                            <ClientSideEvents Click="function (s, e) {txtSave.SetValue('1'); if(!ASPxClientEdit.ValidateGroup('add')){txtSave.SetValue(''); return false }; xcpn.PerformCallback('Save1'); }" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" AutoPostBack="False" Text="ปิด" CausesValidation="false">
                                                                            <ClientSideEvents Click="function (s, e) {window.location = 'admin_VisitForm_lst.aspx';}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxTextBox ID="txtSave" runat="server" ClientInstanceName="txtSave" Width="0px" ReadOnly="true" Border-BorderColor="White"
                                                                            ForeColor="White">
                                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithText" SetFocusOnError="True"
                                                                                ValidationGroup="add">
                                                                                <ErrorFrameStyle ForeColor="Red">
                                                                                </ErrorFrameStyle>
                                                                                <RequiredField ErrorText="กรุณากดปุ่มบันทึก" IsRequired="True" />
                                                                            </ValidationSettings>

                                                                            <Border BorderColor="White"></Border>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="t2" Text="หมวดปัญหา">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%">ลำดับการแสดงผล
                                                        </td>
                                                        <td colspan="2">
                                                            <dx:ASPxTextBox ID="txtRunNumber" ClientInstanceName="txtRunNumber" runat="server"
                                                                Width="50px">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxTextBox ID="txtGroupID" runat="server" ClientInstanceName="txtGroupID" ClientVisible="false"
                                                                Width="10px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style32">หมวดปัญหา <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2" class="style32">
                                                            <dx:ASPxTextBox ID="txtProb" runat="server" Width="300px" MaxLength="200">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" width="20%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus2" runat="server" ClientInstanceName="rblStatus2"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td align="center">
                                                            <dx:ASPxButton ID="btnAdd2" runat="server" Text="เพิ่ม/แก้ไข" SkinID="_add">
                                                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('Save2');}"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtDelIndex" runat="server" ClientInstanceName="txtDelIndex" Width="10px" ClientVisible="false"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="left">
                                                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                                                KeyFieldName="ID1" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText" SkinID="_gvw"
                                                                Style="margin-top: 0px" Width="100%" DataSourceID="sds">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1"
                                                                        Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NGROUPID" Visible="False">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ลำดับ" FieldName="NNO" Visible="False" VisibleIndex="2">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="CACTIVE" Visible="False" VisibleIndex="2">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หมวดปัญหา" FieldName="SGROUPNAME" VisibleIndex="3"
                                                                        Width="33%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataCheckColumn Caption="สถานะ" VisibleIndex="13" Width="5%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" runat="server" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'
                                                                                ReadOnly="true">
                                                                            </dx:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataCheckColumn>
                                                                    <dx:GridViewDataColumn VisibleIndex="15" Width="8%">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" SkinID="_edit">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbDel" runat="server" CausesValidation="false" SkinID="_delete">
                                                                                            <ClientSideEvents Click="function (s, e) {txtDelIndex.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); checkBeforeDelete(function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete1'); },function(s, e) { dxPopupConfirm.Hide(); }); }"></ClientSideEvents>
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsBehavior AllowSort="False" />
                                                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                CancelSelectOnNullParameter="False" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY NNO) AS ID1, NGROUPID,SGROUPNAME,CACTIVE,NNO FROM TGROUPOFVISITFORM WHERE NTYPEVISITFORMID = :NTYPEVISITFORMID"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="NTYPEVISITFORMID" SessionField="NTYPEVISITFORMID" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="t3" Text="หัวข้อคำถาม">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%">ชื่อหมวดคำถาม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2">
                                                            <dx:ASPxComboBox ID="cboGroup" runat="server" TextField="SGROUPNAME" ValueField="NGROUPID"
                                                                Width="300px" DataSourceID="sdsGroup" IncrementalFilteringMode="StartsWith" EnableSynchronization="False">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsGroup" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NGROUPID, SGROUPNAME FROM TGROUPOFVISITFORM WHERE (NTYPEVISITFORMID = :NTYPEVISITFORMID)">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="NTYPEVISITFORMID" SessionField="NTYPEVISITFORMID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:top">ชื่อหัวข้อคำถาม <font color="#ff0000">*</font>
                                                        </td>
                                                        <td colspan="2" style="vertical-align: top;">
                                                            <div style="float: left">
                                                            </div>
                                                            <dx:ASPxMemo runat="server" ID="txtTopicQ" Width="600px" MaxLength="2000" Height="100px">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxMemo>
                                                            <%--<dx:ASPxTextBox ID="txtTopicQ" runat="server" Width="600px" MaxLength="2000" >
                                                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณาเลือกข้อมูล"></RequiredField>
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>--%>
                                                            <dx:ASPxTextBox ID="txtVisitformID" runat="server" ClientVisible="false" Width="10px">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>ตัวคูณคะแนน <font color="#ff0000">*</font> </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtWeight" runat="server" Width="80px" DisplayFormatString="#0.00">
                                                                <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                                    ValidationGroup="add">
                                                                    <ErrorFrameStyle ForeColor="Red">
                                                                    </ErrorFrameStyle>
                                                                    <RequiredField IsRequired="True" ErrorText="กรุณากรอกข้อมูล"></RequiredField>
                                                                    <RegularExpression ErrorText="กรุณากรอกตัวเลข" ValidationExpression="\d+(\.\d{0,2})?" />
                                                                </ValidationSettings>
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">วันที่ปรับปรุงข้อมูล
                                                        </td>
                                                        <td class="style15" colspan="2">
                                                            <dx:ASPxLabel ID="lblDateUpdate" runat="server">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">ผู้ปรับปรุงข้อมูล
                                                        </td>
                                                        <td class="style15" colspan="2">
                                                            <dx:ASPxLabel ID="lblUpdateBy" runat="server">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#FFFFFF" class="style25" valign="top">สถานะ<font color="#ff0000">*</font>
                                                        </td>
                                                        <td align="left" width="20%">
                                                            <dx:ASPxRadioButtonList ID="rblStatus3" runat="server" ClientInstanceName="rblStatus3"
                                                                SelectedIndex="0" SkinID="rblStatus">
                                                                <Items>
                                                                    <dx:ListEditItem Selected="True" Text="Active" Value="1" />
                                                                    <dx:ListEditItem Text="InActive" Value="0" />
                                                                </Items>
                                                            </dx:ASPxRadioButtonList>
                                                        </td>
                                                        <td align="center">
                                                            <dx:ASPxButton ID="btnAdd3" runat="server" Text="เพิ่ม/แก้ไข" SkinID="_add">
                                                                <ClientSideEvents Click="function (s, e) {if(!ASPxClientEdit.ValidateGroup('add')) return false;xcpn.PerformCallback('Save3');}"></ClientSideEvents>

                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <dx:ASPxTextBox ID="txtDelIndex1" runat="server" ClientInstanceName="txtDelIndex1" ClientVisible="false" Width="10px"></dx:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <dx:ASPxGridView ID="gvwTopicQ" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvwTopicQ"
                                                                KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%" DataSourceID="sds1"
                                                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                                                        VisibleIndex="0">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="NVISITFORMID" Visible="false">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="คะแนน" FieldName="NWEIGHT" Visible="false"></dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัสหัวข้อคำถาม" FieldName="NGROUPID" Visible="false">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="CACTIVE" Visible="false">
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="NWEIGHT" FieldName="NWEIGHT" ShowInCustomizationForm="True"
                                                                        VisibleIndex="8" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หัวข้อคำถาม" FieldName="SGROUPNAME" ShowInCustomizationForm="True"
                                                                        VisibleIndex="8" Width="20%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ชื่อหัวข้อคำถาม" FieldName="SVISITFORMNAME" ShowInCustomizationForm="True"
                                                                        VisibleIndex="9" Width="40%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataCheckColumn Caption="สถานะ" ShowInCustomizationForm="True" VisibleIndex="11"
                                                                        Width="5%">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxCheckBox ID="ASPxCheckBox3" runat="server" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'
                                                                                ReadOnly="true">
                                                                            </dx:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataCheckColumn>
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="12" Width="8%">
                                                                        <DataItemTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbedit0" runat="server" CausesValidation="False" SkinID="_edit">
                                                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));ASPxClientEdit.ClearGroup('add'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dx:ASPxButton ID="imbDel0" runat="server" CausesValidation="false" SkinID="_delete">
                                                                                            <ClientSideEvents Click="function (s, e) {txtDelIndex1.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); checkBeforeDelete(function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete2'); },function(s, e) { dxPopupConfirm.Hide(); }); }"></ClientSideEvents>
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsBehavior AllowSort="False" />
                                                                <SettingsBehavior AllowSort="False"></SettingsBehavior>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                CancelSelectOnNullParameter="False" SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY v.NVISITFORMID) AS ID1,v.NVISITFORMID,v.SVISITFORMNAME,v.CACTIVE,V.NGROUPID, G.SGROUPNAME,v.NWEIGHT  FROM TVISITFORM v LEFT JOIN TGROUPOFVISITFORM g ON V.NGROUPID = G.NGROUPID WHERE v.NTYPEVISITFORMID = :NTYPEVISITFORMID"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter Name="NTYPEVISITFORMID" SessionField="NTYPEVISITFORMID" Type="Int32" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <table>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <dx:ASPxButton ID="btnSubmit0" runat="server" AutoPostBack="False" Text="ปิด" Width="60px" CausesValidation="false">
                                                                            <ClientSideEvents Click="function (s, e) {window.location = 'admin_VisitForm_lst.aspx';}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#FFFFFF" align="left">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <%--OnPreRender="txtPassword_PreRender"--%>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img height="1px" src="images/spacer.GIF" width="250px"></img></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
