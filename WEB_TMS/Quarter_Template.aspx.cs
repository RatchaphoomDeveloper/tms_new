﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Quarter_Template : PageBase
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReadText();
        }
    }
    #endregion

    #region Read Test
    private void ReadText()
    {
        string text = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor.txt"));
        txtBody.Html = text;
        text = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Email.txt"));
        txtEmail.Html = text;
        text = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Font.txt"));
        txtFont.Text = text.Split('|')[0];
        txtSize.Text = text.Split('|')[1];
    }
    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string str = txtBody.Html;
            string Authority = Request.Url.Authority;
            str = str.Replace("<img src=\"/", "<img src=\"https://" + Authority + "/");
            File.WriteAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Font.txt"), txtFont.Text.Trim() + "|" + txtSize.Text.Trim());
            File.WriteAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor.txt"), str);
            File.WriteAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Email.txt"), txtEmail.Html);
            alertSuccess("บันทึกสำเร็จ", "Quarter_Template.aspx");
        }
        catch (Exception ex)
        {
            alertFail("บันทึกไม่สำเร็จเนื่องจาก : " + ex.Message);
        }
        
    }
}