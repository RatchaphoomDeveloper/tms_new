﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using System.Diagnostics;
using System.IO;
using DevExpress.XtraPrinting;
using DevExpress.Web.ASPxGridView;

public partial class ApproveRequest : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void btnPlanPrinting_Click(object sender, EventArgs e)
    {
        CreateReportPlan("" + dteStart.Value, "" + dteEnd.Value);
    }
    protected void btnPrintJob_Click(object sender, EventArgs e)
    {
        ASPxButton __btn = (ASPxButton)sender;
        string REQ_ID = __btn.CommandArgument;
        CreateReportJob(REQ_ID);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ASPxButton __btn = (ASPxButton)sender;
        string REQ_ID = __btn.CommandArgument;
        string sEnCript = Server.UrlEncode(STCrypt.Encrypt("" + REQ_ID + "&"))
        , sUrl = "Requesting.aspx?str=";
        Response.Redirect(sUrl + sEnCript);
    }
    #region ฟังก์ชั่นต่างๆ
    private void CreateReportPlan(string DStart, string DEnd)
    {
        #region Prepare SOURCE

        string _sql = "", _condi = "", sDuration = "";

        DateTime sDT;
        if (DateTime.TryParse(DStart, out sDT))
            DStart = sDT.ToString();
        else
            DStart = "";
        if (DateTime.TryParse(DEnd, out sDT))
            DEnd = sDT.ToString();
        else
            DEnd = "";

        if (!string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        {
            _condi = " AND REQ.REQUEST_DATE BETWEEN To_DATE('" + Convert.ToDateTime(DStart).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM') AND To_DATE('" + Convert.ToDateTime(DEnd).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
            sDuration = "ช่วงวันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        }
        else if (!string.IsNullOrEmpty(DStart) && string.IsNullOrEmpty(DEnd))
        {
            _condi = " AND REQ.REQUEST_DATE >= To_DATE('" + Convert.ToDateTime(DStart).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
            sDuration = "ตั้งแต่วันที่ " + Convert.ToDateTime(DStart).ToString("dd/MM/yyyy");
        }
        else if (string.IsNullOrEmpty(DStart) && !string.IsNullOrEmpty(DEnd))
        {
            _condi = " AND REQ.REQUEST_DATE <= To_DATE('" + Convert.ToDateTime(DEnd).ToString(new CultureInfo("en-US")) + "','mm/dd/yyyy HH:MI:SS AM')";
            sDuration = "ก่อนวันที่ " + Convert.ToDateTime(DEnd).ToString("dd/MM/yyyy");
        }

        _sql = @"SELECT REQ.REQUEST_ID,TRU.SCERT_NO As SCERT_NO,TU_NO,VEH_NO,TRT.SCARCATEGORY As SCARTYPE,REQ.TOTLE_CAP,VEN.SABBREVIATION,TYP.REQTYPE_NAME,CAU.CAUSE_NAME
        ,REQ.REQUEST_DATE,CASE WHEN REQ.ACCEPT_FLAG='Y' THEN REQ.ACCEPT_DATE END As ACCEPT_DATE,REQ.SERVICE_DATE,STA.STATUSREQ_NAME
        FROM TBL_REQUEST REQ
        LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
        LEFT JOIN TTRUCKTYPE TRT ON TRU.SCARTYPEID=TRT.SCARTYPEID
        LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
        LEFT JOIN TBL_REQTYPE TYP ON REQ.REQTYPE_ID=TYP.REQTYPE_ID
        LEFT JOIN TBL_CAUSE CAU ON REQ.CAUSE_ID=CAU.CAUSE_ID
        LEFT JOIN TBL_STATUSREQ STA ON REQ.Status_Flag=STA.STATUSREQ_ID
        WHERE 1=1 " + _condi;

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        dt.TableName = "dtReport";

        DataTable dtSum = new DataTable("dtSummary");
        dtSum.Columns.Add("STATUSREQ_NAME");
        dtSum.Columns.Add("NREQ");
        dtSum.Columns.Add("SREQ");

        if (dt.Rows.Count > 0)
        {
            string STATUSREQ_NAME = "", NREQ = "", SREQ = "";
            foreach (DataRow dr in CommonFunction.GroupDatable("", dt, "STATUSREQ_NAME", "", "STATUSREQ_NAME").Rows)
            {
                STATUSREQ_NAME += (string.IsNullOrEmpty(STATUSREQ_NAME) ? "" : "\r\n") + dr["STATUSREQ_NAME"];
                NREQ += (string.IsNullOrEmpty(NREQ) ? "" : "\r\n") + dt.Compute("COUNT(REQUEST_ID)", "STATUSREQ_NAME LIKE '" + dr["STATUSREQ_NAME"] + "'");
                SREQ += (string.IsNullOrEmpty(SREQ) ? "" : "\r\n") + "คำขอ";
            }
            dtSum.Rows.Add(new object[] { STATUSREQ_NAME, NREQ, SREQ });
        }

        dsReportRequest ds = new dsReportRequest();
        ds.dtSummary.Merge(dtSum);
        ds.dtReport.Merge(dt);

        #endregion

        xrtReportRequest report = new xrtReportRequest();
        report.DataSource = ds;
        report.Parameters["Duration"].Value = sDuration;

        //report.Name = "แผนงานประจำวัน_" + DateTime.Now.ToString("MMddyyyyHHmmss");
        string fileName = "แผนงานประจำวัน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToXls(stream);

        Response.ContentType = "application/xls";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + ".xls");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    private void CreateReportJob(string REQ_ID)
    {
        #region Prepare SOURCE

        string _sql = "";

        _sql = @"SELECT REQ.REQUEST_ID,VEN.SABBREVIATION,TRU.SHEADREGISTERNO As REG_NO,TRU.SCERT_NO As SCERT_NO,TRU.NWHEELS,CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN '1' END As TRAILER
,CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.VEH_NO END As HTRAILER,(CASE WHEN TRU.SCARTYPEID IN ('3','4') THEN (SELECT SBRAND FROM TTRUCK WHERE STRUCKID=REQ.STRUCKID AND ROWNUM=1) ELSE TRU.SBRAND END) As SBRAND,REQ.TOTLE_CAP,REQ.TOTLE_SLOT
,(SELECT RTRIM (XMLAGG (XMLELEMENT (e, NCAPACITY || ',')).EXTRACT ('//text()'), ',') FROM 
    (SELECT STRUCKID,NCOMPARTNO,MAX(NCAPACITY) As NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO ORDER BY STRUCKID,NCOMPARTNO)
    WHERE STRUCKID=TRU.STRUCKID GROUP BY STRUCKID) As NSLOT,NULL As FPRESSURE,NULL As BPRESSURE,'' As COMPAN,REQ.SERVICE_DATE
FROM TBL_REQUEST REQ LEFT JOIN TTRUCK TRU ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=TRU.STRUCKID
LEFT JOIN TVENDOR VEN ON REQ.VENDOR_ID=VEN.SVENDORID
WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'";

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        dt.TableName = "dtReport";

        _sql = @"SELECT REQ.REQUEST_ID,COMP.NCOMPARTNO,COMP.NPANLEVEL,COMP.NCAPACITY FROM TBL_REQUEST REQ
                 LEFT JOIN TTRUCK_COMPART COMP ON CASE WHEN NVL(REQ.TU_ID,'')<>'' THEN REQ.TU_ID ELSE REQ.STRUCKID END=COMP.STRUCKID
                 WHERE 1=1 AND REQ.REQUEST_ID='" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                 ORDER BY COMP.NCOMPARTNO,COMP.NPANLEVEL";
        DataTable dt2 = CommonFunction.Get_Data(conn, _sql);
        dt2.TableName = "dtCompart";


        dsReportJob ds = new dsReportJob();
        ds.dtReport.Merge(dt);
        ds.dtCompart.Merge(dt2);

        #endregion

        xrtReportJob report = new xrtReportJob();
        report.DataSource = ds;
        report.Parameters["sDate"].Value = DateTime.Now.Date;
        report.Parameters["PlaceVerify"].Value = "หอวัดน้ำ คน.ลก.";
        report.Parameters["PlaceLoad"].Value = "คน.ลก.";
        report.Parameters["Auditor"].Value = "(นายอำนาจ  บุญเรือง)";

        string fileName = "FM-มว.-009_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        report.ExportToPdf(stream);

        Response.ContentType = "application/pdf";
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + ".pdf");
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    #endregion
}