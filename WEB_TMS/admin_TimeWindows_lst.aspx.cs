﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class admin_TimeWindows_lst : PageBase
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();

            Session["stmID"] = null;
            Session["nline"] = null;

            LogUser("31", "R", "เปิดดูข้อมูลหน้า TimeWindows", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        BindData();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();
                break;

            case "delete":
                if (CanWrite)
                {
                    var ld = gvw.GetSelectedFieldValues("ID1", "STERMINALID")
                        .Cast<object[]>()
                        .Select(s => new { ID1 = s[0].ToString(), STERMINALID = s[1].ToString() });

                    string delid = "";

                    foreach (var l in ld)
                    {
                        Session["oTerminalID"] = l.STERMINALID;
                        sds.Delete();

                        delid += l.STERMINALID + ",";
                    }

                    LogUser("31", "D", "ลบข้อมูลหน้า TimeWindows รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                    gvw.Selection.UnselectAll();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "STERMINALID");
                string stmID = Convert.ToString(data);

                Session["stmID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_TimeWindows_add.aspx";

                break;

        }
    }

    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;

        if (!CanWrite)
        {
            imbedit.Enabled = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_TimeWindows_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    void BindData()
    {

        string strsql = "SELECT  ROW_NUMBER () OVER (ORDER BY W.STERMINALID) AS ID1, W.STERMINALID,'' AS STYPE, TS.STERMINALNAME {0}  FROM ((TWINDOWTIME W INNER JOIN TTERMINAL T ON W.STERMINALID =  T.STERMINALID) INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID) LEFT JOIN TUSER u ON W.SUPDATE = U.SUID WHERE TS.STERMINALNAME LIKE '%' || :oSearch || '%' GROUP BY W.STERMINALID, TS.STERMINALNAME ";
        string strTimeLine = "";

        string sLINE = CommonFunction.Get_Value(sql, "SELECT MAX(NLINE) AS MAXSTATUS FROM TWINDOWTIME");
        int num = 0;
        int nLINE = int.TryParse(sLINE, out num) ? num : 0;

        if (nLINE > 0)
        {
            for (int i = 1; i <= nLINE; i++)
            {
                strTimeLine += string.Format(",MAX(CASE WHEN W.NLINE = {0} THEN W.TSTART || ' - ' || W.TEND  END) AS t{1}", i, i);
                strTimeLine += string.Format(",MAX(CASE WHEN W.NLINE = {0} THEN LATEDAY END) AS LATEDAY{1}", i, i);
                strTimeLine += string.Format(",MAX(CASE WHEN W.NLINE = {0} THEN LATETIME END) AS LATETIME{1}", i, i);
            }
        }

        sds.SelectCommand = string.Format(strsql, strTimeLine);
        sds.SelectParameters.Add("oSearch", txtSearch.Text + "");
        sds.DataBind();
        gvw.DataBind();
    }

    protected void gvw_Init(object sender, EventArgs e)
    {

        string sLINE = CommonFunction.Get_Value(sql, "SELECT MAX(NLINE) AS MAXSTATUS FROM TWINDOWTIME");
        int num = 0;
        int nLINE = int.TryParse(sLINE, out num) ? num : 0;

        if (nLINE > 0)
        {
            GridViewBandColumn sGridViewBandColumn = new GridViewBandColumn("เที่ยวที่");
            sGridViewBandColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            for (int n = 1; n <= nLINE; n++)
            {
                GridViewBandColumn sGridViewBandColumn2 = new GridViewBandColumn("" + n);
                sGridViewBandColumn2.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

                GridViewDataColumn sGridViewDataColumn = new GridViewDataColumn("T" + n, "ช่วงเวลา");
                sGridViewDataColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.Width = Unit.Percentage(10);
                sGridViewBandColumn2.Columns.Add(sGridViewDataColumn);

                sGridViewDataColumn = new GridViewDataColumn("LATEDAY" + n, "วันที่ถึง<br/>ปลายทาง");
                sGridViewDataColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.Width = Unit.Percentage(10);
                //sGridViewDataColumn.PropertiesEdit = n
                sGridViewBandColumn2.Columns.Add(sGridViewDataColumn);

                sGridViewDataColumn = new GridViewDataColumn("LATETIME" + n, "เวลาที่ถึง<br/>ปลายทาง");
                sGridViewDataColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                sGridViewDataColumn.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                //sGridViewDataColumn.PropertiesEdit.EncodeHtml = false;
                sGridViewDataColumn.Width = Unit.Percentage(10);
                sGridViewBandColumn2.Columns.Add(sGridViewDataColumn);

                sGridViewBandColumn.Columns.Add(sGridViewBandColumn2);

            }
            gvw.Columns.Insert(2, sGridViewBandColumn);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
