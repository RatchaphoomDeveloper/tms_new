﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.IO;

public partial class ReportDefectPercentage : System.Web.UI.Page
{
    string Conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            cboYear.Value = DateTime.Now.Year + "";
            cboMonth.Value = DateTime.Now.Month.ToString().PadLeft(2, '0');
            switch (DateTime.Now.Month.ToString().PadLeft(2, '0'))
            {
                case "01":
                case "02":
                case "03":
                    cboQuarter.Value = "Q_01_02_03";
                    break;
                case "04":
                case "05":
                case "06":
                    cboQuarter.Value = "Q_04_05_06";
                    break;
                case "07":
                case "08":
                case "09":
                    cboQuarter.Value = "Q_07_08_09";
                    break;
                case "10":
                case "11":
                case "12":
                    cboQuarter.Value = "Q_10_11_12";
                    break;
            }

        }
    }
    protected void GetData(string fileType)
    {
        string Qry_Data = @"SELECT TOPIC.STOPICID ,TOPIC.STOPICNAME ,TOPIC.NPOINT 
 ,COUNT(RDPT.NREDUCEID) ALL_REDUCE
,SUM(CASE WHEN NVL(RDPT.IS_APPEAL,0)=1 THEN 1  ELSE 0  END)  IS_APPEAL
,SUM(CASE WHEN NVL(RDPT.IS_APPEAL,0)=0 THEN 1  ELSE 0  END)  ISNOT_APPEAL
,SUM(CASE WHEN NVL(RDPT.IS_APPEAL,0)=1 AND NVL(RDPT.IS_SENTENCE,0)=0 THEN 1  ELSE 0  END)  ISNOT_RETURN
,SUM(CASE WHEN NVL(RDPT.IS_APPEAL,0)=1 AND NVL(RDPT.IS_SENTENCE,0)=1 THEN 1  ELSE 0  END)  IS_RETURN
,0 NCOUNT ,0 NAPPEAL 
FROM TTOPIC TOPIC
LEFT JOIN (
    SELECT (CASE WHEN APPL.NREDUCEID IS NULL THEN 0 ELSE 1 END) IS_APPEAL
--    , CASE WHEN NVL(APPL.CSTATUS,'0') ='3' THEN (  CASE WHEN NVL(APPL.SSENTENCER,'2') ='1' THEN 1 ELSE 0 END) ELSE 0 END IS_SENTENCE
    , CASE WHEN APPL.SAPPEALNO IS NULL THEN 0 ELSE
    CASE WHEN NVL(APPL.CSTATUS,'0') ='3'  THEN 
    (  CASE WHEN NVL(APPL.SSENTENCER,'2') ='1' THEN 1 ELSE 0 END) ELSE 0 END 
    END IS_SENTENCE , VEH.STRUCKID ,NVL(RDPT.SCONTRACTID, CONTTRCK.SCONTRACTID) CONTRACT_ID ,CONT.SVENDORID
    ,RDPT.* 
    FROM TREDUCEPOINT RDPT
    LEFT JOIN TAPPEAL APPL ON RDPT.NREDUCEID = APPL.NREDUCEID
    LEFT JOIN TTRUCK VEH ON NVL(RDPT.SHEADREGISTERNO,'ss.nn-nnnn') = NVL(VEH.SHEADREGISTERNO,'ss.nn-nnnn') 
    LEFT JOIN TCONTRACT_TRUCK CONTTRCK ON VEH.STRUCKID=CONTTRCK.STRUCKID
    LEFT JOIN TCONTRACT CONT ON NVL(RDPT.SCONTRACTID, CONTTRCK.SCONTRACTID) = CONT.SCONTRACTID
    WHERE 1=1 {0}  
) RDPT ON TOPIC.STOPICID = RDPT.STOPICID
WHERE 1=1
AND  NVL(TOPIC.CACTIVE,'1')='1'  

GROUP BY TOPIC.STOPICID ,TOPIC.STOPICNAME ,TOPIC.NPOINT 
ORDER BY TOPIC.STOPICID ASC
 ", cond = "";
        cond += " AND EXTRACT(year from RDPT.DREDUCE ) ='" + CommonFunction.ReplaceInjection(cboYear.Value + "") + "' ";
        switch (rblDefactMode.Value + "")
        {
            case "Month":
                cond += " AND EXTRACT(month from RDPT.DREDUCE ) ='" + CommonFunction.ReplaceInjection(cboMonth.Value + "") + "'";
                break;
            case "Quarter":
                string sQuarter = CommonFunction.ReplaceInjection(cboQuarter.Value + "").Remove(0, 2).Replace("_", "','");
                cond += " AND EXTRACT(month from RDPT.DREDUCE ) IN ('" + sQuarter + "') ";
                break;
            case "Year":
                cond += "";
                break;
        }
        if (!string.IsNullOrEmpty(cbxVendor.Value + ""))
        {

            cond += " AND CONT.SVENDORID='" + CommonFunction.ReplaceInjection(cbxVendor.Value + "") + "'";
        }
        if (!string.IsNullOrEmpty(cboSCONTRACTNO.Value + ""))
        {

            cond += " AND NVL(RDPT.SCONTRACTID, CONTTRCK.SCONTRACTID)='" + CommonFunction.ReplaceInjection(cboSCONTRACTNO.Value + "") + "'";
        }

        DataTable dtDefectPercentage = CommonFunction.Get_Data(Conn, string.Format(Qry_Data, cond));

        foreach (DataRow drDefectPercentage in dtDefectPercentage.Rows)
        {
            drDefectPercentage["NCOUNT"] = int.Parse(dtDefectPercentage.Compute("SUM(ALL_REDUCE)", "") + "");
            drDefectPercentage["NAPPEAL"] = int.Parse(dtDefectPercentage.Compute("SUM(IS_APPEAL)", "") + "");

        }
        xrtReportDefectPercentage report = new xrtReportDefectPercentage();
        report.DataSource = dtDefectPercentage;
        string fileName = "DefectPercentage_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();
        if (fileType == "xls")
            report.ExportToXls(stream);
        else
            report.ExportToPdf(stream);

        Response.ContentType = "application/" + fileType;
        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + fileName + "." + fileType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        GetData("pdf");
    }
    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        GetData("xls");
    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();
    }
    protected void cboSCONTRACTNO_ItemRequestedByValue(object source, DevExpress.Web.ASPxEditors.ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboSCONTRACTNO_ItemsRequestedByFilterCondition(object source, DevExpress.Web.ASPxEditors.ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;

        dscboSCONTRACTNO.SelectCommand = "SELECT * FROM TCONTRACT WHERE SCONTRACTNO  LIKE :fillter  ";
        dscboSCONTRACTNO.SelectParameters.Clear();
        dscboSCONTRACTNO.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));

        comboBox.DataSource = dscboSCONTRACTNO;
        comboBox.DataBind();
    }

}