﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="report_kpi.aspx.cs" Inherits="report_kpi" Culture="en-US"  %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup"  %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
    <style type="text/css">
    th
    {
        text-align:center;
    }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <br />
    <h5>การจัดการแบบประเมินผลKPI</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>แบบประเมินผล KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <asp:GridView ID="dgvreportkpi" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" DataKeyNames="KPI_EVA_HEAD_ID"
                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="ที่">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:BoundField DataField="NAME" HeaderText="ชื่อแบบฟอร์ม">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AMOUNT" HeaderText="คำถาม" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("STATUS").ToString()== "1" ? "ใช้งาน" : "ไม่ใช้งาน"  %>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:BoundField DataField="SFIRSTNAME" HeaderText="ปรับปรุงข้อมูลล่าสุด" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}">
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>                                
                                <asp:BoundField DataField="UPDATE_DATE" HeaderText="วันที่ปรับปรุงล่าสุด" HeaderStyle-HorizontalAlign="Center">
                                    
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" runat="server" Text="แก้ไข" CommandArgument="<%# Container.DataItemIndex %>" OnClick="Edit_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                    </asp:GridView>
                </div>
                <div class="col-md-12">
                    <asp:Button runat="server" ID="ADD_Report" CssClass="btn btn-success" Text="เพิ่มแบบฟอร์ม" OnClick="ADD_Report_Click" />
                    <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-danger" style="width: 120px;" />
                </div> 
            </div>
        </div>
    </div>
    <uc1:modelpopup runat="server" id="mpConfirmBack" idmodel="ModalConfirmBack" isbuttontypesubmit="true" isonclick="true" isshowclose="true" onclickok="mpConfirmBack_ClickOK" texttitle="ยืนยันการย้อนกลับ" textdetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />
</asp:Content>


