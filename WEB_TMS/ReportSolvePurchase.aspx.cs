﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;

public partial class ReportSolvePurchase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ORA_ConnectionStrings = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

        if (IsPostBack)
        {

            string sqlquery = string.Format(@"SELECT TERMINAL_ID ,TERMINAL_NAME
 ,CASE  
 WHEN ALL_TRIP<= 0 THEN 'Normal'
 WHEN ALL_TRIP> MIN2STAT THEN 'Critical('||ALL_TRIP||')'
 WHEN ALL_TRIP<MAX2STAT And ALL_TRIP >= MIN2STAT THEN 'Warning('||ALL_TRIP||')'
 ELSE 'Normal('||ALL_TRIP||')'
 END  ALL_TRIP 
,MIN2STAT- ALL_TRIP MIN2STAT ,MAX2STAT-ALL_TRIP MAX2STAT

FROM 
(
    SELECT TBL.TERMINAL_ID ,PLANT.SABBREVIATION TERMINAL_NAME
    ,SUM((NVL(TBL.TRUCK_AMOUNT,0) * NVL(TBL.STD_VALUE,0)))  ALL_TRIP, MIN(TBL_CHG2STAT.MIN2STAT) MIN2STAT ,MAX(TBL_CHG2STAT.MAX2STAT) MAX2STAT

    FROM TTERMINAL PLANT 
    LEFT JOIN TBL_SOLVEPURCHASE TBL ON TBL.TERMINAL_ID=PLANT.STERMINALID
    LEFT JOIN (
        SELECT CHG2STAT.STERMINALID,CHG2STAT.CPRODUCT_GROUP,CHG2STAT.MAX2STAT,CHG2STAT.MIN2STAT ,PRDTYP.SPRODUCTTYPEID
        FROM TCHANGE2STAT CHG2STAT
        LEFT JOIN TPRODUCTTYPE PRDTYP ON CHG2STAT.CPRODUCT_GROUP = PRDTYP.CPRODUCT_GROUP
        WHERE NVL(CACTIVE,'1') ='1'
    ) TBL_CHG2STAT ON TBL.PRODUCTTYPE_ID=TBL_CHG2STAT.SPRODUCTTYPEID AND PLANT.STERMINALID =TBL_CHG2STAT.STERMINALID
    WHERE 1=1 AND SUBSTR(PLANT.STERMINALID,1,1) IN( '{0}' )
    AND TRUNC(TBL.DATA_DATE) >= TO_DATE('{1}','DD/MM/YYYY') AND TRUNC(TBL.DATA_DATE) <=TO_DATE('{2}','DD/MM/YYYY') 
    AND TBL.PRODUCTTYPE_ID IN ('{3}') 
    GROUP BY TBL.TERMINAL_ID ,PLANT.SABBREVIATION 
) TBL_Final 
WHERE 1=1
", ("5,8").Replace(",", "','")
 , "" + CommonFunction.ReplaceInjection(dteStart.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")))
 , "" + CommonFunction.ReplaceInjection(dteEnd.Date.ToString("dd/MM/yyyy", new CultureInfo("en-US")))
 , "" + CommonFunction.ReplaceInjection((cboProdGrp.Value + "" != "") ? cboProdGrp.Value.ToString().Split(':')[1] : "").Replace(",", "','"));



            //Response.Write(sqlquery); return;
            //DataTable dt = new DataTable();
            //dt = CommonFunction.Get_Data(sql,sqlquery);


            dsReport dsRep = new dsReport();
            using (OracleConnection con = new OracleConnection(ORA_ConnectionStrings))
            {
                if (con.State == System.Data.ConnectionState.Closed) con.Open();
                using (OracleDataAdapter adapter = new OracleDataAdapter(string.Format(sqlquery, "", "", "")
                    , con))
                {
                    adapter.Fill(dsRep.dsSolvePurchase);
                }
            }

            xrtSolvePurchase report = new xrtSolvePurchase();
            report.DataSource = dsRep.dsSolvePurchase;
            if (dteStart.Value != null && dteEnd.Value != null)
            {
                string ProdGRP=CommonFunction.ReplaceInjection((cboProdGrp.Value + "" != "") ? cboProdGrp.Value.ToString().Split(':')[0] : "").Replace(",", "','")
                    , ProdList = cboProdGrp.Text;

                report.Parameters["ReportName"].Value = "Solve Purchse Report";
                report.Parameters["ProdGrp"].Value = "Product Group " + ProdGRP + @" (" + ProdList + ")";
                report.Parameters["pStart"].Value = dteStart.Date.ToString("dd/MM/yyyy");
                report.Parameters["pEnd"].Value = dteEnd.Date.ToString("dd/MM/yyyy");
            }
            report.Name = "Detail SolvePurchase";
            this.rptvw.Report = report;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

}