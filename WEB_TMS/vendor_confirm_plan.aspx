﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_confirm_plan.aspx.cs" Inherits="vendor_confirm_plan" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript"> 
        window.onload = function () {
            return true;
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" >
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="40%">
                            <dx:ASPxButton ID="btnAdd" runat="server" AutoPostBack="False" Text="ส่งข้อมูลให้ ปตท."
                                Width="140px">
                                <ClientSideEvents Click="function (s, e) {if(gvw.GetSelectedRowCount() > 0){dxConfirm('ยืนยันการทำงาน','<b>ท่านต้องการยืนยันรถตามแผน</b> ต่อใช่หรือไม่?',function(s,e){ dxPopupConfirm.Hide(); gvw.PerformCallback('SendData');} ,function(s,e){ dxPopupConfirm.Hide(); })}else{dxWarning('แจ้งให้ทราบ', 'กรุณาเลือกรายการที่ต้องการส่ง');}}" >
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="180px" NullText="ค้นหาจาก ทะเบียนรถ">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboTerminal1" runat="server" ClientInstanceName="cboTerminal1"
                                DataSourceID="sqlTerminal1" TextFormatString="{1}" ValueField="STERMINALID"
                                Width="120px">
                                <Columns>
                                <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" />
                                <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" />
                                </Columns>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sqlTerminal1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                SelectCommand="SELECT t.STERMINALID, ts.STERMINALNAME FROM TTERMINAL t INNER JOIN TTERMINAL_SAP ts ON t.STERMINALID = ts.STERMINALID WHERE T.STERMINALID LIKE 'H%' OR  T.STERMINALID LIKE 'K%' ORDER BY t.STERMINALID">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboStatus" runat="server" Width="100px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Text="รอการยืนยัน" Value="0" />
                                    <dx:ListEditItem Text="ยืนยันรถ" Value="1" />
                                    <dx:ListEditItem Text="ทั้งหมด" Value="2" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" >
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                       <td colspan="8" align="right" style="color:Red"> มีรายการรอยืนยันแผนงาน 
                           <dx:ASPxLabel ID="lblConfirmCar" runat="server" Text="0">
                           </dx:ASPxLabel> &nbsp;รายการ  </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds" OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared"
                                OnAfterPerformCallback="gvw_AfterPerformCallback" OnHtmlRowPrepared="gvw_HtmlRowPrepared"
                                OnCustomColumnDisplayText="gvw_CustomColumnDisplayText">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="1%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัสแผน" VisibleIndex="2"  HeaderStyle-HorizontalAlign="Center"
                                        FieldName="NPLANID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่จัดส่ง" VisibleIndex="4" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DDELIVERY">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="เที่ยว" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="STIMEWINDOW" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="จุดส่งรถ" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="STERMINALNAME" Width="18%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel ID="lblSterminal" runat="server" Text='<%# Eval("STERMINALNAME") %>'
                                                CssClass="dxeLineBreakFix">
                                            </dx:ASPxLabel>
                                            <dx:ASPxButton ID="imbView" runat="server" SkinID="dd" CausesValidation="False" AutoPostBack="false"
                                                Cursor="pointer" EnableDefaultAppearance="False" EnableTheming="False" CssClass="dxeLineBreakFix">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('imbClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                                <Image Height="16px" Url="Images/document.gif" Width="16px">
                                                </Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" HeaderStyle-HorizontalAlign="Center" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                                VisibleIndex="2" FieldName="SHEADREGISTERNO">
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                                VisibleIndex="3" FieldName="STRAILERREGISTERNO">
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" VisibleIndex="9" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SCONFIRM" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="CCONFIRM" VisibleIndex="10" Caption="CCONFIRM" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="พนักงานขับรถ" VisibleIndex="11" HeaderStyle-HorizontalAlign="Center"
                                        Width="15%">
                                        <DataItemTemplate>
                                            <dx:ASPxComboBox ID="cmbPersonalNo" runat="server" CallbackPageSize="30" ClientInstanceName="cmbPersonalNo" ValueType="System.String"
                                                EnableCallbackMode="True" OnItemRequestedByValue="cmbPersonalNo_OnItemRequestedByValueSQL"
                                                OnItemsRequestedByFilterCondition="cmbPersonalNo_OnItemsRequestedByFilterConditionSQL"
                                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="SEMPLOYEEID" Width="150px"> 
                                             <%--   <ClientSideEvents ValueChanged ="function (s, e) {xcpn.PerformCallback('cboPersonalTextChange;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                <Columns>
                                                    <dx:ListBoxColumn Caption="ชื่อพนักงานขับรถ" FieldName="FULLNAME" />
                                                </Columns>
                                            </dx:ASPxComboBox>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn VisibleIndex="12" Width="15%" Caption=" ">
                                        <DataItemTemplate>
                                        <table><tr>
                                        <td> <dx:ASPxTextBox ID="txtConfirm" runat="server" ClientInstanceName="txtConfirm"  Text="1"
                                             ClientVisible="false"  >
                                            </dx:ASPxTextBox></td>
                                            <td> <dx:ASPxButton ID="btnConfirm" runat="server" SkinID="_confirm" ClientInstanceName="btnConfirm"
                                                AutoPostBack="false" CausesValidation="False" CssClass="dxeLineBreakFix" Width="60px"
                                                ClientEnabled="false">
                                            </dx:ASPxButton></td>
                                            <td> <dx:ASPxButton ID="btnCancel" runat="server"  AutoPostBack="false" Text = "ไม่ยืนยัน" ClientInstanceName="btnCancel"
                                                 CausesValidation="False" CssClass="dxeLineBreakFix" Width="70px">
                                            </dx:ASPxButton></td>
                                            <td>  </td>
                                            </tr></table>
                                        </DataItemTemplate>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn FieldName="ISEXPIRE" Caption="IsExpire" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DPLAN" Caption="DPLAN" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="DDELIVERY" Caption="DDELIVERY" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SEMPLOYEEID" Caption="SEMPLOYEEID" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="CFIFO" Caption="CFIFO" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="SPLANDATE" Caption="SPLANDATE" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True" Mode="ShowAllRecords">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
                                HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true" SkinID="popUp">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                        <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                                            <tr>
                                                <td width='20' background='images/bd001_a.jpg' height='9'>
                                                </td>
                                                <td background='images/bd001_b.jpg'>
                                                    &nbsp;</td>
                                                <td width='20' height='20' align='left' valign='top' background='images/bd001_c.jpg'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                                                    <img src='images/bd001_d.jpg' width='20' height='164'>
                                                </td>
                                                <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                รายละเอียดการส่งมอบ
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                                                </td>
                                                <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                                    <img src='images/bd001_e.jpg' width='20' height='164'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='20'>
                                                    <img src='images/bd001_f.jpg' width='20' height='20'>
                                                </td>
                                                <td background='images/bd001_g.jpg'>
                                                    <img src='images/bd001_g.jpg' width='20' height='20'>
                                                </td>
                                                <td width='20'>
                                                    <img src='images/bd001_h.jpg' width='20' height='20'>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                            <asp:SqlDataSource ID="sds" runat="server" EnableCaching="True" CancelSelectOnNullParameter="false" CacheKeyDependency="ckdConfirm"
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="sdsPersonal" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                       <td colspan="8">
                       <table><tr>
                       <td >สถานะของสี</td>
                       <td style=" border-style:solid; border-width:1px; border-color:Black; background-color: #F5F5F5" width="15px">&nbsp;</td>
                       <td> = ยังไม่ได้ยืนยันแผน</td>
                       <td style=" background-color: #CEECF5" width="15px"></td>
                       <td> = ยืนยันแผนงานแล้ว</td>
                       <td style=" background-color: #F6E3CE" width="15px"></td>
                       <td> = เลยระยะเวลายืนยันแผน</td>
                       </tr></table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
