﻿<%@ Page MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true" EnableViewState="true"
    CodeFile="Config_QuarterReport.aspx.cs" Inherits="Config_QuarterReport" StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />

            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหา
                </div>
                <div class="panel-body">
                    <div class="row">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                        <div class="form-group">
                            <div class="col-md-4 text-right">
                                ปี :

                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">&nbsp;</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-md-4 text-right">
                                ไตรมาส :

                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList ID="ddlQuerter" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true">
                                    <asp:ListItem Text="ทั้งหมด" Value="0" />
                                    <asp:ListItem Text="ไตรมาส 1" Value="1" />
                                    <asp:ListItem Text="ไตรมาส 2" Value="2" />
                                    <asp:ListItem Text="ไตรมาส 3" Value="3" />
                                    <asp:ListItem Text="ไตรมาส 4" Value="4" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="form-group " id="divCcident" runat="server">
                            <div class="col-md-4 text-right">
                                หัวข้อ :

                            </div>
                            <div class="col-md-8">

                                <asp:CheckBoxList ID="chkaccident" runat="server" RepeatColumns="2">
                                </asp:CheckBoxList>

                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <p>&nbsp;</p>
            <asp:HiddenField ID="hidID" runat="server" />
            <asp:HiddenField ID="hidSVENDORID" runat="server" />
                        </ContentTemplate>
    </asp:UpdatePanel>
                        <div class="form-group">
                            <div class="col-md-4 text-right">

                            </div>
                            <div class="col-md-8 ">
                                <asp:Button Text="Preview" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn btn-default" runat="server" />
                                <input id="btnSave" runat="server" type="button" value="บันทึกการแสดงผลประจำไตรมาส" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-default" />
                                <asp:Button ID="btnExportPDF" Text="Export PDF" CssClass="btn btn-default" runat="server" OnClick="btn_ExportPDF_Click" />
                                <asp:Button Text="Export Excel" ID="btnExportExcel" CssClass="btn btn-default" runat="server" OnClick="btnExportExcel_Click" />
                                <asp:Button Text="ย้อนกลับ" CssClass="btn btn-default" PostBackUrl="~/Quartery_Report.aspx" ID="btnBack" Visible="false" runat="server" />
                            </div>
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        
    <p>&nbsp;</p>
    <dx:ASPxGridView ID="gvDownload" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" KeyFieldName="QUARTER">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ไตรมาส" FieldName="QUARTER">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle  HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ชื่อเอกสาร" FieldName="DOCNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="เลขที่เอกสาร" FieldName="DOCNO">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataColumn Caption="ผลการประเมินการทำงาน">
                <DataItemTemplate>
                    <asp:Button Text='Download' CssClass="btn btn-default" ID="btnDownload" runat="server" OnClick="btnDownload_Click" CommandArgument='<%# Container.VisibleIndex  %>' />
                </DataItemTemplate>
            </dx:GridViewDataColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="SVENDORID" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="YEARS" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="DATESTR" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="LOGVERSION" Visible="false">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <p>&nbsp;</p>
    <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" SettingsBehavior-AllowSort="false" SkinID="_gvw" Width="100%" SettingsPager-Mode="ShowAllRecords" OnHtmlRowCreated="grid_HtmlRowCreated" OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared" KeyFieldName="SCONTRACTNO">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SABBREVIATION">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="กลุ่มที่" FieldName="GROUPNAME">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 1" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER1">
                <Columns>
                    <dx:GridViewDataColumn Caption="ม.ค." Width="3%" FieldName="JAN" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="ก.พ." Width="3%" FieldName="FEB" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="มี.ค." Width="3%" FieldName="MAR" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Average" FieldName="QUARTER1" Name="COL_QUARTER1">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER1" Visible="false">
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 2" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER2">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="เม.ย." ShowInCustomizationForm="True" Width="3%" FieldName="APR" Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="พ.ค." Width="3%" FieldName="MAY" Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="มิ.ย." ShowInCustomizationForm="True" Width="3%" FieldName="JUN" Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Average" FieldName="QUARTER2" Name="COL_QUARTER2">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER2" Visible="false">
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 3" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER3">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ก.ค." ShowInCustomizationForm="True" Width="3%" FieldName="JUL" Name="COL_QUARTER3">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ส.ค." ShowInCustomizationForm="True" Width="3%" FieldName="AUG" Name="COL_QUARTER3">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ก.ย." ShowInCustomizationForm="True" Width="3%" FieldName="SEP" Name="COL_QUARTER3">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Average" FieldName="QUARTER3" Name="COL_QUARTER3">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER3" Visible="false">
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewBandColumn Caption="ไตรมาส 4" HeaderStyle-HorizontalAlign="Center" Name="COL_QUARTER4">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ต.ค." ShowInCustomizationForm="True" Width="3%" FieldName="OCT" Name="COL_QUARTER4">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="พ.ย." ShowInCustomizationForm="True" Width="3%" FieldName="NOV" Name="COL_QUARTER4">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="ธ.ค." ShowInCustomizationForm="True" Width="3%" FieldName="DEC" Name="COL_QUARTER4">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Average" FieldName="QUARTER4" Name="COL_QUARTER4">
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataColumn Name="COL_QUARTER4" Visible="false">
                    </dx:GridViewDataColumn>
                </Columns>
            </dx:GridViewBandColumn>
            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTID" Visible="false">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบการ" FieldName="SVENDORID" Visible="false">
                <HeaderStyle HorizontalAlign="Center" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="ID1" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="ID2" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="ID3" Visible="false">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="" FieldName="ID4" Visible="false">
            </dx:GridViewDataTextColumn>
        </Columns>
        <Paddings />
    </dx:ASPxGridView>
    

    <uc1:ModelPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="btnSave_Click" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
</asp:Content>
