﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ReportCauseCarNotComplete.aspx.cs" Inherits="ReportCauseCarNotComplete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){ eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined){ window.location = s.cpRedirectTo; }}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td align="right">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                            <tr>
                                                <td width="70%" align="right">
                                                    <dx:ASPxComboBox ID="cboVendor" runat="server" Width="80%" ClientInstanceName="cboVendor"
                                                        TextFormatString="{0} - {1}" CallbackPageSize="30" EnableCallbackMode="True"
                                                        OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" ValueField="SVENDORID" AutoPostBack="false">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                        </Columns>
                                                    </dx:ASPxComboBox>
                                                    <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                    </asp:SqlDataSource>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="2%">
                                                    <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text=" - " CssClass="dxeLineBreakFix">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td width="9%">
                                                    <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                    </dx:ASPxDateEdit>
                                                </td>
                                                <td width="10%">
                                                    <dx:ASPxButton runat="server" ID="btnSearch" SkinID="_search" AutoPostBack="false">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td width="87%" height="35">
                                        <dx:ASPxLabel runat="server" ID="lblsHead" Text="รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท ระหว่างวันที่ "
                                            CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                        <dx:ASPxLabel runat="server" ID="lblsTail" Text="" CssClass="dxeLineBreakFix">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td width="13%">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                            <tr>
                                                <td width="37%"></td>
                                                <td width="37%"></td>
                                                <td width="13%">
                                                    <%--<dx:ASPxButton ID="btnDowload" runat="server" Text="Download" ClientInstanceName="btnDowload"
                                                        AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('PDF');}" />
                                                    </dx:ASPxButton>--%>
                                                    <dx:ASPxButton runat="server" ID="btnDowload" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" CssClass="dxeLineBreakFix" Text="PDF">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('PDF');}" />
                                                        <Image Url="images/ic_pdf2.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                                <td width="13%">
                                                    <dx:ASPxButton runat="server" ID="btnDowload2" AutoPostBack="false" EnableTheming="false"
                                                        EnableDefaultAppearance="false" Cursor="pointer" CssClass="dxeLineBreakFix" Text="EXCEl">
                                                        <ClientSideEvents Click="function(e,s){ rvw.SaveToDisk('xls');}" />
                                                        <Image Url="images/ic_ms_excel.gif">
                                                        </Image>
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView runat="server" ID="gvw" Width="100%" KeyFieldName="REQUEST_ID">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="บริษัท" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"
                                                    FieldName="SABBREVIATION" GroupIndex="0">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ที่" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="NO">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รายละเอียด" Width="30%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Left" FieldName="FAKETEXT">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblSab" Text='<%# Eval("CHECKLIST_NAME_REPORT") %>'>
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ม.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M1">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.พ." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M2">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="มี.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M3">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="เม.ษ." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M4">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="พ.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M5">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="มิ.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M6">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M7">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ส.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M8">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ก.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M9">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ต.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M10">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="พ.ย." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M11">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ธ.ค." Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="M12">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รวม" Width="5%" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" FieldName="TOTAL">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <Settings ShowGroupPanel="false" />
                                            <GroupSummary>
                                                <%--<dx:ASPxSummaryItem FieldName="SNO" SummaryType="None" />--%>
                                                <dx:ASPxSummaryItem FieldName="SABBREVIATION" SummaryType="Count" DisplayFormat="จำนวน {0} รายการ" />
                                            </GroupSummary>
                                            <TotalSummary>
                                                <dx:ASPxSummaryItem FieldName="FAKETEXT" SummaryType="MAX" ShowInGroupFooterColumn="รายละเอียด"
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M1" SummaryType="SUM" ShowInGroupFooterColumn="ม.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M2" SummaryType="SUM" ShowInGroupFooterColumn="ก.พ."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M3" SummaryType="SUM" ShowInGroupFooterColumn="มี.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M4" SummaryType="SUM" ShowInGroupFooterColumn="เม.ษ."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M5" SummaryType="SUM" ShowInGroupFooterColumn="พ.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M6" SummaryType="SUM" ShowInGroupFooterColumn="มิ.ย."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M7" SummaryType="SUM" ShowInGroupFooterColumn="ก.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M8" SummaryType="SUM" ShowInGroupFooterColumn="ส.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M9" SummaryType="SUM" ShowInGroupFooterColumn="ก.ย."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M10" SummaryType="SUM" ShowInGroupFooterColumn="ต.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M11" SummaryType="SUM" ShowInGroupFooterColumn="พ.ย."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="M12" SummaryType="SUM" ShowInGroupFooterColumn="ธ.ค."
                                                    DisplayFormat="{0:#,#}" />
                                                <dx:ASPxSummaryItem FieldName="TOTAL" SummaryType="SUM" ShowInGroupFooterColumn="รวม"
                                                    DisplayFormat="{0:#,#}" />
                                            </TotalSummary>
                                            <Settings ShowFooter="true" />
                                            <SettingsPager PageSize="50">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="display: none">
                                        <dx:ReportViewer ID="rvw" runat="server" ClientInstanceName="rvw">
                                        </dx:ReportViewer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
