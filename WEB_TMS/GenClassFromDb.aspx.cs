using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.Data.SqlClient;
using System.Data.OracleClient;
using System.IO;

public partial class GenClassFromDb : System.Web.UI.Page
{
    // Connection
    //public static SqlConnection _conn = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
    public static OracleConnection _conn = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btngen_Click(object sender, EventArgs e)
    {
        // Reopen connection if close
        if (_conn.State == ConnectionState.Closed) _conn.Open();
        //
        string[] restrictions = new string[] { null, this.txttable.Text, null };
        DataTable dt = _conn.GetSchema("Columns", restrictions); 
        // �ó���辺���͵��ҧ㹰ҹ������
        if (dt.Rows.Count.Equals(0))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "complete", "alert('��辺���ҧ㹰ҹ������ ��سҵ�Ǩ�ͺ�����');", true);
            return;
        }
        DataRow[] dr = dt.Select("", "ID ASC");
        DataRow[] drs = null;

        string str = "using System;\n" +
                     "using System.Data;\n" +
                     "using System.Configuration;\n" +
                     "using System.Web;\n" +
                     "using System.Web.Security;\n" +
                     "using System.Web.UI;\n" +
                     "using System.Web.UI.WebControls;\n" +
                     "using System.Web.UI.WebControls.WebParts;\n" +
                     "using System.Web.UI.HtmlControls;\n" +
                     "using System.Data.OracleClient;\n";

        str += "\n" +
               "/// <summary>\n" +
               "/// Summary description for " + this.txttable.Text + "\n" +
               "/// </summary>\n";
        str += "public class " + this.txttable.Text + "\n" +
               "{\n" +
               "    public static OracleConnection conn;\n" +
               "    public static Page page;\n" +
               "    //public " + this.txttable.Text + "() { }\n" +
               "    public " + this.txttable.Text + "(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }\n";

        // Create Data
        str += "    #region Data\n";
        foreach (DataRow _dr in dr)
        {
            str += "    private string f" + _dr["COLUMN_NAME"].ToString() + ";\n";
        }
        str += "    #endregion\n";
        // Create Property
        str += "    #region Property\n";
        foreach (DataRow _dr in dr)
        {
            str += "    public string " + _dr["COLUMN_NAME"].ToString() + "\n" +
                   "    {\n" +
                   "        get { return this.f" + _dr["COLUMN_NAME"].ToString() + "; }\n" +
                   "        set { this.f" + _dr["COLUMN_NAME"].ToString() + " = value; }\n" +
                   "    }\n";
        }
        str += "    #endregion\n";
        // Create Method

        //Method Add
        str += "    #region Method\n";
        str += "    public int Insert()\n";
        str += "    {\n";
        str += "        int result = -1;\n";
        str += "        try\n";
        str += "        {\n";
        str += "            // Reopen connection if close\n";
        str += "            if (conn.State == ConnectionState.Closed) conn.Open();\n";
        str += "            //\n";
        str += "            OracleCommand cmd = new OracleCommand(\"I" + this.txttable.Text + "\", conn);\n";
        str += "            cmd.CommandType = CommandType.StoredProcedure;\n";
        foreach (DataRow _dr in dr)
        {
            str += "            cmd.Parameters.AddWithValue(\"@" + _dr["COLUMN_NAME"].ToString() + "\", " + _dr["COLUMN_NAME"].ToString() + ");\n";
        }
        str += "            \n";
        str += "            result = cmd.ExecuteNonQuery();\n";
        str += "        }\n";
        str += "        catch (Exception err)\n";
        str += "        {\n";
        str += "            if (ConfigurationSettings.AppSettings[\"alerttrycatch\"].Equals(\"1\"))\n";
        str += "            {\n";
        str += "                page.RegisterStartupScript(\"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\");\n";
        str += "                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), \"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\", false);\n";
        str += "            }\n";
        str += "        }\n";
        str += "        finally\n";
        str += "        {\n";
        str += "            \n";
        str += "        }\n";
        str += "        return result;\n";
        str += "    }\n";

        //Method Update
        str += "    public int Update()\n";
        str += "    {\n";
        str += "        int result = -1;\n";
        str += "        try\n";
        str += "        {\n";
        str += "            // Reopen connection if close\n";
        str += "            if (conn.State == ConnectionState.Closed) conn.Open();\n";
        str += "            //\n";
        str += "            OracleCommand cmd = new OracleCommand(\"U" + this.txttable.Text + "\", conn);\n";
        str += "            cmd.CommandType = CommandType.StoredProcedure;\n";
        foreach (DataRow _dr in dr)
        {
            str += "            cmd.Parameters.AddWithValue(\"@" + _dr["COLUMN_NAME"].ToString() + "\", " + _dr["COLUMN_NAME"].ToString() + ");\n";
        }
        str += "            \n";
        str += "            result = cmd.ExecuteNonQuery();\n";
        str += "        }\n";
        str += "        catch (Exception err)\n";
        str += "        {\n";
        str += "            if (ConfigurationSettings.AppSettings[\"alerttrycatch\"].Equals(\"1\"))\n";
        str += "            {\n";
        str += "                page.RegisterStartupScript(\"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\");\n";
        str += "                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), \"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\", false);\n";
        str += "            }\n";
        str += "        }\n";
        str += "        finally\n";
        str += "        {\n";
        str += "            \n";
        str += "        }\n";
        str += "        return result;\n";
        str += "    }\n";

        //Method Delete
        str += "    public int Delete()\n";
        str += "    {\n";
        str += "        int result = -1;\n";
        str += "        try\n";
        str += "        {\n";
        str += "            // Reopen connection if close\n";
        str += "            if (conn.State == ConnectionState.Closed) conn.Open();\n";
        str += "            //\n";
        str += "            OracleCommand cmd = new OracleCommand(\"D" + this.txttable.Text + "\", conn);\n";
        str += "            cmd.CommandType = CommandType.StoredProcedure;\n";
        // �Ҥ��ѹ����� PK
        drs = dt.Select("NULLABLE = 'NO'", "ID ASC");
        foreach (DataRow _dr in drs)
        {
            str += "            cmd.Parameters.AddWithValue(\"@" + _dr["COLUMN_NAME"].ToString() + "\", " + _dr["COLUMN_NAME"].ToString() + ");\n";
        }
        str += "            \n";
        str += "            result = cmd.ExecuteNonQuery();\n";
        str += "        }\n";
        str += "        catch (Exception err)\n";
        str += "        {\n";
        str += "            if (ConfigurationSettings.AppSettings[\"alerttrycatch\"].Equals(\"1\"))\n";
        str += "            {\n";
        str += "                page.RegisterStartupScript(\"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\");\n";
        str += "                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), \"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\", false);\n";
        str += "            }\n";
        str += "        }\n";
        str += "        finally\n";
        str += "        {\n";
        str += "            \n";
        str += "        }\n";
        str += "        return result;\n";
        str += "    }\n";

        //Method Open
        str += "    public bool Open()\n";
        str += "    {\n";
        str += "        string sqlstr;\n";
        str += "        bool result = false;\n";
        str += "        try\n";
        str += "        {\n";
        str += "            // Reopen connection if close\n";
        str += "            if (conn.State == ConnectionState.Closed) conn.Open();\n";
        str += "            //\n";
        str += "            sqlstr = \"SELECT * FROM " + this.txttable.Text + " ";
        // �Ҥ��ѹ����� PK
        drs = dt.Select("NULLABLE = 'NO'", "ID ASC");
        string _feild = "";
        string[] _afeild = null;
        sbyte bi = 1;
        foreach (DataRow _dr in drs)
        {
            _feild += "," + _dr["COLUMN_NAME"].ToString();
        }
        if (!_feild.Equals(""))
        {
            _feild = _feild.Remove(0, 1);
            str += " WHERE ";
            _afeild = _feild.Split(",".ToCharArray());
            foreach (string _f in _afeild)
            {
                str += _f + " = '\" + " + _f + " + \"'\" ";
                if (bi != _afeild.Length) { str += " + \" AND "; }
                else { str += ";\n"; }
                bi++;
            } 
        } else { str += "\";\n"; }
        str += "            DataTable dt = new DataTable();\n";
        str += "            new OracleDataAdapter(sqlstr, conn).Fill(dt);\n";

        str += "            if (dt.Rows.Count >= 1)\n";
        str += "            {\n";
        str += "                DataRow row = dt.Rows[0];\n";
        foreach (DataRow _dr in dr)
        {
            str += "                " + _dr["COLUMN_NAME"].ToString() + " = row[\"" + _dr["COLUMN_NAME"].ToString() + "\"].ToString();\n";
        }
        str += "                dt.Dispose();\n";
        str += "                result = true;\n";
        str += "            }\n";
        str += "        }\n";
        str += "        catch (Exception err)\n";
        str += "        {\n";
        str += "            if (ConfigurationSettings.AppSettings[\"alerttrycatch\"].Equals(\"1\"))\n";
        str += "            {\n";
        str += "                page.RegisterStartupScript(\"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\");\n";
        str += "                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), \"alert\", \"<script>alert(\\\"Source : \" + err.Source.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nMessage : \" + err.Message.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\\nStack Trace : \\\\n\" + err.StackTrace.Replace(\"\\\\\", \"\\\\\\\\\").Replace(\"\\r\", \"\").Replace(\"\\n\", \"\\\\n\") + \"\\\");</script>\", false);\n";
        str += "            }\n";
        str += "        }\n";
        str += "        finally\n";
        str += "        {\n";
        str += "            \n";
        str += "        }\n";
        str += "        return result;\n";
        str += "    }\n";
        str += "    #endregion\n"; 
        str += "}\n";

        string sPath = Server.MapPath("./");

        if (!Directory.Exists(sPath + "\\Create_Class\\"))
        {
            Directory.CreateDirectory(sPath + "\\Create_Class\\");
        }
        if (File.Exists(sPath + "\\Create_Class\\" + this.txttable.Text + ".cs"))
        {
            File.Delete(sPath + "\\Create_Class\\" + this.txttable.Text + ".cs");
        }

        FileInfo file = new FileInfo(sPath + "\\Create_Class\\" + this.txttable.Text + ".cs");
        StreamWriter sw = file.CreateText();
        sw.Write(str);
        sw.Close();
        // clear resource
        dr = null;
        _afeild = null;
        dt.Dispose();
        //
        ScriptManager.RegisterStartupScript(this, this.GetType(), "complete", "alert('Gen Class �����');", true);
    }
}
