﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TMS_D_UpdateVehicle_SYNC_OUT_SI;
using TMS_D_UpdateTU_SYNC_OUT_SI;
using System.Web.Configuration;
using System.IO;
using System.Globalization;


/// <summary>
/// Summary description for SAP_UPDATE_TU
/// </summary>
public class SAP_UPDATE_TU
{
    public static OracleConnection conn;
    public static Page page;
    public SAP_UPDATE_TU()
    {
    }
    #region Data
    private string fVEH_NO;
    #endregion
    #region Property
    public string VEH_NO
    {
        get { return this.fVEH_NO; }
        set { this.fVEH_NO = value; }

    }
    #endregion

    #region Method
    public string UDT_TU_SYNC_OUT_SI()
    {
        string result = "";
        bool IsUpdated = false;

        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT--,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TUAS.TU_Assignment_Items,TU.STRUCKID STRAILERID--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END STRAILERID
,TU.SHEADREGISTERNO STRAILERREGISTERNO--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. SHEADREGISTERNO  ELSE TRCK.STRAILERREGISTERNO END STRAILERREGISTERNO
,CAST(TRCK.SCHASIS AS varchar2(18)) VEH_ID   ,CAST(TU.SCHASIS AS varchar2(18))  TU_ID ,TRCK.STRANSPORTID CARRIER
,NVL(TU.STERMINALID ,TRCK.STERMINALID ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY
,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
,TRCK.CLASSGRP CLASS_GRP
,TU.CLASSGRP TU_CLASS_GRP ,TRCK.TPPOINT TPPOINT , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT, TRCK.DREGISTER VEH_REG ,  TU.DREGISTER TU_REG
,TRCK.ROUTE ROUTE ,TRCK.TRUCK_CATEGORY TYPE_ID,TU.TRUCK_CATEGORY TU_TYPE
,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
,NVL(TU.DPREV_SERV,TRCK.DPREV_SERV) DPREV_SERV  
,NVL(TRCK.CHOLD,'0') as CHOLD
,(
CASE 
WHEN NVL(TRCK.CACTIVE,'Y')  = 'Y' AND NVL(TRCK.CHOLD,'0') <> '1' THEN ' ' 
WHEN NVL(TRCK.CACTIVE,'Y')  = 'Y' AND NVL(TRCK.CHOLD,'0') = '1' THEN '1' 
WHEN NVL(TRCK.CACTIVE,'Y')  = 'N' THEN '1' 
WHEN  NVL(TRCK.CACTIVE,'Y') =  'D' THEN '2' 
ELSE ' ' END
--CASE NVL(TRCK.CACTIVE,'Y') 
--WHEN 'N' THEN '1' 
--WHEN 'D' THEN '2' 
--ELSE ' ' END
) CACTIVE
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL(TRCK.STRUCKID,'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
LEFT JOIN
(
    SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID STRAILERID,TU.STRAILERREGISTERNO
    ,COUNT(VEH.STRUCKID) TU_Assignment_Items
    FROM( SELECT * FROM TTRUCK  WHERE SCARTYPEID='0' OR SCARTYPEID='4' ) VEH
    LEFT JOIN ( SELECT *  FROM TTRUCK WHERE SCARTYPEID='4' ) TU ON VEH.STRAILERID =TU.STRUCKID
    WHERE 1=1
    GROUP BY   VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID ,TU.STRAILERREGISTERNO
)TUAS
ON TUAS.SHEADREGISTERNO = TRCK.SHEADREGISTERNO
WHERE TRCK.SCARTYPEID in('0','4')
AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC
", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];                
                    #region Update Truck      

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "4")
                            {//10Wheel
                                #region Compartment
                                //เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                int TU_compartment_Items = ((!string.IsNullOrEmpty(dt.Rows[0]["NSLOT"] + "") ? int.Parse(dt.Rows[0]["NSLOT"] + "") : 0) == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                if (TU_compartment_Items != dt.Rows.Count)
                                {
                                    for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                    {
                                        if (dt.Rows.Count > 0)
                                        {
                                            DataRow dr_Compart = dt.Rows[idxCompart];
                                            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                            {
                                                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            };

                                        }
                                        else
                                        {
                                            sErr = "no compartment data";
                                            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                            {
                                                TU_NUMBER = "#!",
                                                CMP_MAXVOL = "#!",
                                                COM_NUMBER = "#!",
                                                LOAD_SEQ = "#!",
                                                SEQ_NMBR = "#!",
                                            };
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (DataRow dr_Compart in dt.Rows)
                                    {
                                        int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                        tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                        {
                                            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                        };

                                    }
                                }
                                #endregion
                                #region TU
                                string sCarType_ID = row["SCARTYPEID"] + "";
                                string val_MAXWGT = "0", val_UNLWGT = "0";
                                if (sCarType_ID == "0")
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "10000";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                }
                                else if (sCarType_ID == "3")
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "25000";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                }
                                else if (sCarType_ID == "4")
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "25000";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                }
                                else
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "0";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                }
                                var trailer = new UpdateTU_Src_Req_DT()
                                {
                                    TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                    TU_ID = "#!",// row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                    ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                    VOL_UOM = "#!",
                                    TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                    TU_STATUS = " ",
                                    TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                    TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                    L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                    EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                    CMP_ITEM = tu_compartment
                                };
                                #endregion
                                var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                for (int i = 0; i <= Res_TU_Row; i++)
                                {
                                    IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                    if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                    {
                                        //foreach (var Return in Res_TU.GT_RETURN_TU)
                                        //{
                                        //    Msg += "," + Return.MESSAGE;
                                        //}
                                        Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                    }
                                }
                            }
                                result = Msg;
                        }
                    
                    #endregion
                
            }
        }
        return result;

    }
    protected void SaveResult2TextFIle(List<string> _lst)
    {

        string codeVersion = "1.0.0";
        string logName = "SAP_SYNTRCK_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
        using (System.IO.TextWriter tw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("UploadFile/SAP_Log/" + logName), true))
        {
            tw.WriteLine("Time: " + DateTime.Now.ToString("HH:mm:ss"));
            foreach (string str in _lst)
            {
                tw.WriteLine(_lst.IndexOf(str).ToString() + ": " + str);
            }
            tw.WriteLine("-------");
        }
    }
//    public string UDT_TU_SYNC_OUT_SI()
//    {
//        string result = "";
//        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
//        {
//            DataTable dt = new DataTable();
//            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID ,TRCK.NSLOT  ,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
//,TU.STRUCKID STRAILERID--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END STRAILERID
//,TU.SHEADREGISTERNO STRAILERREGISTERNO--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. SHEADREGISTERNO  ELSE TRCK.STRAILERREGISTERNO END STRAILERREGISTERNO
//,TRCK.STRANSPORTID CARRIER,TRCK.STERMINALID DEPOT ,TRCK.NTOTALCAPACITY ,TRCK.NWEIGHT ,TRCK.NCALC_WEIGHT ,TRCK.NLOAD_WEIGHT ,TRCK.DWATEREXPIRE ,TRCK.FUELTYPE
//,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS
//,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY
//,'' CLASS_GRP ,'' TPPOINT ,'' VEH_TEXT
//FROM TTRUCK TRCK
//LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL((CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END ),'TRYYnnnnn')
//LEFT JOIN 
//(
//    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
//    FROM TTRUCK_COMPART 
//    GROUP BY STRUCKID,NCOMPARTNO
//) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
//WHERE TRCK.SCARTYPEID in('4')
//AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
//ORDER BY TRCKCOMP.NCOMPARTNO ASC", connection).Fill(dt);
//            if (dt.Rows.Count >= 1)
//            {
//                DataRow row = dt.Rows[0];
//                int Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
//                using (var ClientTU = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port"))
//                {
//                    ClientTU.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
//                    ClientTU.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
//                    var Assignment = new UpdateTU_Src_Req_DTItem[Assignment_Items];
//                    var TUnit = new UpdateTU_Src_Req_DT()
//                    {
//                        CMP_ITEM = Assignment,
//                        EXPIRE_DAT = "",
//                        L_NUMBER = "",
//                        TU_MAXWGT = "",
//                        TU_NUMBER = "",
//                        TU_STATUS = "",
//                        TU_TEXT = "",
//                        TU_UNLWGT = "",
//                        VOL_UOM = "",
//                        ZSTART_DAT = ""
//                    };

//                    var response = ClientTU.UpdateTU_SYNC_OUT_SI(TUnit);
//                }
//            }
//        }
//        return result;
//    }
    protected string MapClassGroup(string sClassGroup)
    {
        if (sClassGroup.Substring(0, 1) == "O")
        {

        }
        return sClassGroup;
    }
    private string DateTimeCheck(string sDate)
    {
        string Date_Result = "1900-01-01";

        if (!string.IsNullOrEmpty(sDate))
        {
            if (Convert.ToDateTime(sDate).Year > 1500)
            {
                Date_Result = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
            }
        }

        return Date_Result;
    }
    #endregion
}