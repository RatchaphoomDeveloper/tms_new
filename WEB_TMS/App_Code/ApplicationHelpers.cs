﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Utility
{
    public class ApplicationHelpers
    { 
        public static decimal? ConvertToDecimal(String str)
        {
            decimal result;
            bool no_error = decimal.TryParse(str, out result);
            return no_error ? result : new Nullable<decimal>();
        }

        public static short? ConvertToShort(String str)
        {
            short result;
            bool no_error = short.TryParse(str, out result);
            return no_error ? result : new Nullable<short>();
        }

        public static int? ConvertToInt(String str)
        {
            int result;
            bool no_error = int.TryParse(str, out result);
            return no_error ? result : new Nullable<int>();
        }

        public static string GetDataField(string fieldName, GridViewRowEventArgs e)
        {
            var data = DataBinder.Eval(e.Row.DataItem, fieldName);
            return data != null ? data.ToString() : null;
        }

        public static decimal? ParseDecimal(String str)
        {
            string result = str;

            if (str.IndexOf(",") != -1)
            {
                result = result.Trim().Replace(",", string.Empty);
            }

            return ConvertToDecimal(result);
        }

        #region "Decimal Format"

        public static string FormatDecimal(decimal d)
        {
            return d.ToString("#,##0.00");
        }

        public static string FormatDecimal(decimal? d)
        {
            string result = string.Empty;

            if (d.HasValue)
            {
                result = d.Value.ToString("#,##0.00");
            }

            return result;
        }

        #endregion

        public static void DataToDropDown(object obj,
            DropDownList ddl,
            string TextName,
            string ValueName,
            string EmptyText)
        {

            if (ddl != null)
            {
                ddl.Enabled = true;
                ddl.Items.Clear();
                ddl.DataSource = obj;
                ddl.DataTextField = TextName;
                ddl.DataValueField = ValueName;
                ddl.DataBind();

                if (EmptyText != "")
                    ddl.Items.Insert(0, new ListItem(EmptyText, ""));
            }
        }

        public static string CheckNull(object T)
        {
            string result;

            if (T == null)
            {
                result = string.Empty;
            }
            else
            {
                result = T.ToString();
            }

            return result;
        }
         
    }
}
