﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TMSEnum
/// </summary>
public class TMSEnum
{
	public TMSEnum()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public enum TruckDocType
    {
        VehicleRegistration = 1, // ใบจดทะเบียนรถ
        TaxCertificate = 2, // หลักฐานการเสียภาษี
        CompartmentInfo = 3, // เอกสาร Compartment
        FirstRegistration = 4, // ใบจดทะเบียนรถครั้งแรก
        Others = 9
    }

    public enum TruckType
    {
        Truck = 0,
        HeadSemiTrailer = 3,
        TrailSemiTrailer = 4
    }
}