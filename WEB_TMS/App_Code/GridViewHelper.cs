﻿using System;
using System.Web.UI.WebControls;
using System.Data;

/// <summary>
/// Summary description for GridView
/// </summary>
public static class GridViewHelper
{
    public static void BindGridView(ref GridView dgv, DataTable dtSource, bool AssignColor = false)
    {
        try
        {
            if (dtSource != null)
            {
                dgv.DataSource = dtSource;
                dgv.DataBind();

                if (AssignColor)
                    SetColor(dgv, dtSource);
            }
            else
            {
                dgv.DataSource = null;
                dgv.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void SetColor(GridView dgvSource, DataTable dataSource)
    {
        try
        {
            string ColorName = string.Empty;
            //var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Color));

            for (int i = 0; i < dgvSource.Rows.Count; i++)
            {
                if (!string.Equals(dataSource.Rows[i]["ROW_COLOR"].ToString(), string.Empty))
                {
                    ColorName = dataSource.Rows[i]["ROW_COLOR"].ToString();
                    dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static int GetColumnIndexByName(GridView dgv, string ColumnName)
    {
        int i = 0;
        string currentDataField = string.Empty;

        foreach (Object bfName in dgv.Columns)
        {
            if (string.Equals(bfName.GetType().ToString(), "System.Web.UI.WebControls.BoundField"))
            {
                currentDataField = ((BoundField)bfName).DataField;
                if (string.Equals(((BoundField)bfName).DataField, ColumnName))
                    return i;
            }
            else if (string.Equals(bfName.GetType().ToString(), "System.Web.UI.WebControls.TemplateField"))
            {
                currentDataField = ((TemplateField)bfName).HeaderText;
                if (string.Equals(((TemplateField)bfName).HeaderText, ColumnName))
                    return i;
            }

            i++;
        }

        return -1;
    }
}