﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class AuthenResources
{

    private static global::System.Resources.ResourceManager resourceMan;

    private static global::System.Globalization.CultureInfo resourceCulture;

    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
    public AuthenResources()
    {
    }

    /// <summary>
    ///   Returns the cached ResourceManager instance used by this class.
    /// </summary>
    [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
    public static global::System.Resources.ResourceManager ResourceManager
    {
        get
        {
            if (object.ReferenceEquals(resourceMan, null))
            {
                global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Application.Properties.Resources", typeof(AuthenResources).Assembly);
                resourceMan = temp;
            }
            return resourceMan;
        }
    }

    /// <summary>
    ///   Overrides the current thread's CurrentUICulture property for all
    ///   resource lookups using this strongly typed resource class.
    /// </summary>
    [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
    public static global::System.Globalization.CultureInfo Culture
    {
        get
        {
            return resourceCulture;
        }
        set
        {
            resourceCulture = value;
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to เพิ่ม.
    /// </summary>
    public static string Add
    {
        get
        {
            return ResourceManager.GetString("Add", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to อนุมัติ.
    /// </summary>
    public static string Approve
    {
        get
        {
            return ResourceManager.GetString("Approve", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ยกเลิก.
    /// </summary>
    public static string Cancel
    {
        get
        {
            return ResourceManager.GetString("Cancel", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to เคลียร์.
    /// </summary>
    public static string Clear
    {
        get
        {
            return ResourceManager.GetString("Clear", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to สัญญา.
    /// </summary>
    public static string Contract
    {
        get
        {
            return ResourceManager.GetString("Contract", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to เลขที่สัญญา.
    /// </summary>
    public static string ContractNo
    {
        get
        {
            return ResourceManager.GetString("ContractNo", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ชื่อย่อบริษัทผู้ขนส่ง.
    /// </summary>
    public static string CutName
    {
        get
        {
            return ResourceManager.GetString("CutName", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ลบ.
    /// </summary>
    public static string Delete
    {
        get
        {
            return ResourceManager.GetString("Delete", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to พนักงานขับรถ.
    /// </summary>
    public static string Driver
    {
        get
        {
            return ResourceManager.GetString("Driver", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to -- เลือกข้อมูล --.
    /// </summary>
    public static string DropDownChooseText
    {
        get
        {
            return "เลือกข้อมูล";//ResourceManager.GetString("DropDownChooseText", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กรุณาเลือกข้อมูล {0}.
    /// </summary>
    public static string DropDownRequire
    {
        get
        {
            return "กรุณาเลือกข้อมูล";// ResourceManager.GetString("DropDownRequire", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to แก้ไข.
    /// </summary>
    public static string Edit
    {
        get
        {
            return ResourceManager.GetString("Edit", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to Export.
    /// </summary>
    public static string Export
    {
        get
        {
            return ResourceManager.GetString("Export", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to Field.
    /// </summary>
    public static string Field
    {
        get
        {
            return ResourceManager.GetString("Field", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กรุณาแนบเอกสาร {0}.
    /// </summary>
    public static string FileRequire
    {
        get
        {
            return "กรุณาแนบเอกสาร";// ResourceManager.GetString("FileRequire", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ไม่มีไฟล์ที่ต้องการอัพโหลด.
    /// </summary>
    public static string FileUploadNotFound
    {
        get
        {
            return ResourceManager.GetString("FileUploadNotFound", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กลุ่มที่.
    /// </summary>
    public static string Group
    {
        get
        {
            return ResourceManager.GetString("Group", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to จับคู่.
    /// </summary>
    public static string GroupBind
    {
        get
        {
            return ResourceManager.GetString("GroupBind", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กลุ่มงาน.
    /// </summary>
    public static string GroupWork
    {
        get
        {
            return ResourceManager.GetString("GroupWork", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to แจ้งผลรายการ.
    /// </summary>
    public static string HeaderInformation
    {
        get
        {
            return ResourceManager.GetString("HeaderInformation", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กรุณาป้อน {0} เป็นตัวเลขเท่านั้น.
    /// </summary>
    public static string IntegerOnly
    {
        get
        {
            return ResourceManager.GetString("IntegerOnly", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ส่งข้อมูลไป SAP เรียบร้อย.
    /// </summary>
    public static string InterfaceSAPSuccess
    {
        get
        {
            return ResourceManager.GetString("InterfaceSAPSuccess", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to วันที่ขนส่ง (สิ้นสุด) ห้ามมีค่ามากกว่า หรือ เท่ากับ วันปัจจุบัน.
    /// </summary>
    public static string InvalidateDateInputSpecial
    {
        get
        {
            return ResourceManager.GetString("InvalidateDateInputSpecial", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to หมายเลขบัตรประชาชนไม่ถูกต้องไม่สามารถเพิ่มข้อมูลได้.
    /// </summary>
    public static string InvalidCardID
    {
        get
        {
            return ResourceManager.GetString("InvalidCardID", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ป้อนรูปแบบวันที่ผิด.
    /// </summary>
    public static string InvalidDateInput
    {
        get
        {
            return ResourceManager.GetString("InvalidDateInput", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to หน้าจอ.
    /// </summary>
    public static string Menu
    {
        get
        {
            return ResourceManager.GetString("Menu", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ขอข้อมูลเพิ่มเติม.
    /// </summary>
    public static string MoreInformation
    {
        get
        {
            return ResourceManager.GetString("MoreInformation", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ข้อมูลที่ขอเพิ่ม.
    /// </summary>
    public static string MoreInformationRequest
    {
        get
        {
            return ResourceManager.GetString("MoreInformationRequest", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ไม่พบข้อมูลตามเงื่อนไขที่ค้นหา.
    /// </summary>
    public static string NotFoundData
    {
        get
        {
            return ResourceManager.GetString("NotFoundData", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กรุณาใส่ข้อมูล {0} เป็นตัวเลขเท่านั้น.
    /// </summary>
    public static string NumberOnly
    {
        get
        {
            return ResourceManager.GetString("NumberOnly", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to จำนวนต้องมีค่า มากกว่า หรือ เท่ากับ ศูนย์.
    /// </summary>
    public static string NumberValue
    {
        get
        {
            return ResourceManager.GetString("NumberValue", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to คลังต้นทาง.
    /// </summary>
    public static string Plant
    {
        get
        {
            return ResourceManager.GetString("Plant", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ตำแหน่ง.
    /// </summary>
    public static string Position
    {
        get
        {
            return ResourceManager.GetString("Position", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ปฏิเสธ.
    /// </summary>
    public static string Reject
    {
        get
        {
            return ResourceManager.GetString("Reject", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ส่งคำขอ.
    /// </summary>
    public static string Request
    {
        get
        {
            return ResourceManager.GetString("Request", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to Require.
    /// </summary>
    public static string Require
    {
        get
        {
            return ResourceManager.GetString("Require", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to บันทึก.
    /// </summary>
    public static string Save
    {
        get
        {
            return ResourceManager.GetString("Save", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ไม่มีข้อมูลที่ต้องการบันทึก.
    /// </summary>
    public static string SaveNotFoundData
    {
        get
        {
            return "ไม่มีข้อมูลที่ต้องการบันทึก";
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to บันทึกข้อมูลเรียบร้อย.
    /// </summary>
    public static string SaveSuccess
    {
        get
        {
            return "บันทึกข้อมูลเรียบร้อย";
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ค้นหา.
    /// </summary>
    public static string Search
    {
        get
        {
            return "ค้นหา";
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ผลการค้นหา จำนวน {0} รายการ.
    /// </summary>
    public static string SearchResult
    {
        get
        {
            return ResourceManager.GetString("SearchResult", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ส่งอีเมล์เรียบร้อย.
    /// </summary>
    public static string SendEmailComplete
    {
        get
        {
            return ResourceManager.GetString("SendEmailComplete", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to ไม่สามารถส่งอีเมล์ได้.
    /// </summary>
    public static string SendEmailNotComplete
    {
        get
        {
            return ResourceManager.GetString("SendEmailNotComplete", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to กรุณาใส่ข้อมูล {0}.
    /// </summary>
    public static string TextBoxRequire
    {
        get
        {
            return "กรุณาใส่ข้อมูล";
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to มีข้อมูลซ้ำภายในระบบ.
    /// </summary>
    public static string TextDuplicateData
    {
        get
        {
            return ResourceManager.GetString("TextDuplicateData", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to แนบไฟล์เอกสารยังไม่ครบ.
    /// </summary>
    public static string UploadFileRequest
    {
        get
        {
            return ResourceManager.GetString("UploadFileRequest", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to อัพโหลดไฟล์.
    /// </summary>
    public static string UploadType
    {
        get
        {
            return ResourceManager.GetString("UploadType", resourceCulture);
        }
    }

    /// <summary>
    ///   Looks up a localized string similar to บริษัทผู้ขนส่ง.
    /// </summary>
    public static string Vendor
    {
        get
        {
            return ResourceManager.GetString("Vendor", resourceCulture);
        }
    }
}