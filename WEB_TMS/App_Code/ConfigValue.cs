﻿
/// <summary>
/// Summary description for EmailTemplate
/// </summary>
using System.Text;
using System.Web.Security;
public static class ConfigValue
{
    //Send Email
    public static int EmailComplainCreateNoLock = 1;                               //แจ้งเรื่องร้องเรียน ไม่ระงับ พขร.
    public static int EmailComplainCreateLock = 2;                               //แจ้งเรื่องร้องเรียน ระงับ พขร.
    public static int EmailReportMonthlyNotPass = 6;                         //ผลการตรวจสอบรายงาน (ไม่ผ่าน)
    public static int EmailComplainRequest = 7;                              //เรื่องร้องเรียน ขอเอกสารเพิ่มเติม
    public static int EmailKnowledgeManagement = 122;                              //เรื่อง Knowledge management
    public static int EmailTruckAdd = 31;                                     // เพิ่มอีเมลล์แจ้งเพิ่มรถ
    public static int EmailTruckEdit = 32;                                    // แก้ไขข้อมูลรถ
    public static int EmailTruckComment = 33;                                    // ขอข้อมูลเพิ่มเติมม
    public static int EmailTruckNoApprove = 34;                                    // ไม่อนุม้ติรถ
    public static int EmailTruckApprove = 35;                                    // อนุม้ติรถ
    public static int EmailVehicleAdd = 36;                                    // เพิ่มการผูกรถ
    public static int EmailVehicleApprove = 37;                                    // เพิ่มอีเมลล์อนุมัติการผูกรถ
    public static int EmailVehicleNoApprove = 38;                                    // เพิ่มการไม่อนุมัติการผูกรถ
    public static int EmailStopCar = 39;                                   //รถระงับชั่วคราว
    public static int EmailApproveCar = 40;                                     //แจ้งอนุญาติใช้รถ
    public static int EmailBlacklistCar = 41;                                     //แจ้งBlacklist
    public static int EmailComplainCusScore = 18;                                       //พิจารณาตัดคะแนนเรียบร้อย
    public static int EmailComplainAppeal = 19;                                       //ผู้ขนส่งยื่นอุทธรณ์
    public static int EmailComplainAppealConfirm = 20;                                       //แจ้งผลการอุทธรณ์
    public static int EmailNotSendTruck = 42;                                       //แจ้งเตือนผู้ขนส่งไม่ยืนยันรถ

    public static string[] ComplainAdminUser = { "sysadmin", "ComplainAdmin", "570083" };

    //Department
    public static string DeliveryDepartmentCode = "80000368";                //รหัสหน่วยงาน รข.

    public static string DeliveryDepartmentCodeAdmin = "80000368";                //รหัสหน่วยงาน ผจ. รข.
    public static string DeliveryDepartmentCodeAdmin2 = "80000369";                //รหัสหน่วยงาน ผจ. ปง.

    public static string DeliveryDepartmentCodeAdminPosition = "ผจ.ขปน.";                //ชื่อตำแหน่ง ผจ. รข.
    public static string DeliveryDepartmentCodeAdmin2Position = "ผจ.ปง.";                //ชื่อตำแหน่ง ผจ. ปง.

    //Doc Status
    public static int DocStatus1 = 1;                                             //ดำเนินการ
    public static int DocStatus2 = 2;                                             //พิจารณา (บันทึกร่าง)
    public static int DocStatus3 = 3;                                             //พิจารณา (รอบันทึกตัดคะแนน)
    public static int DocStatus4 = 9;                                             //อยู่ระหว่างการพิจารณาของ รข.
    public static int DocStatus5 = 4;                                             //บันทึกการตัดคะแนนเรียบร้อย (รออุธรณ์)
    public static int DocStatus6 = 5;                                             //อุธรณ์
    public static int DocStatus7 = 6;                                             //ปิดเรื่อง ไม่มีค่าปรับ
    public static int DocStatus8 = 7;                                             //พิจารณา (ขอเอกสารเพิ่มเติม)
    public static int DocStatus9 = 8;                                           //ยกเลิกเอกสาร
    public static int DocStatus10 = 10;                                             //ปิดเรื่อง ชำระค่าปรับแล้ว
    public static int DocStatus11 = 11;                                             //ปิดเรื่อง ยังไม่ชำระค่าปรับ
    
    //Driver Status
    public static string DriverEnable = " ";                                             //ใช้งาน
    public static string DriverDisable = "1";                                            //ระงับชั่วคราว
    public static string DriverOut = "2";                                                //Blacklist

    //Doc Status Monthly Report
    public static int MonthlyStatus1 = 1;                                             //ผู้ขนส่ง บันทึกร่าง
    public static int MonthlyStatus2 = 2;                                             //รอตรวจสอบ
    public static int MonthlyStatus3 = 3;                                             //รข บันทึกร่าง
    public static int MonthlyStatus4 = 4;                                             //รข บันทึก (ยืนยัน)
    public static int MonthlyStatus5 = 5;                                             //Initial Data Report

    //Monthly Report Type
    public static int Monthly1 = 1;                                             //รายงานประจำเดือน
    public static int Monthly2 = 2;                                             //รายงานประจำ 3 เดือน
    public static int Monthly3 = 3;                                             //รายงานประจำ 6 เดือน

    public static int UserGroup1 = 0;                                           //ผู้ประกอบการขนส่ง
    public static int UserGroup2 = 1;                                           //เจ้าหน้าที่ ปตท.
    public static int UserGroup3 = 2;                                           //คลัง
    public static int UserGroup4 = 3;                                           //แอดมิน
    public static int UserGroup5 = 4;                                           //ผจ.

    public static int VendorEmployeeAdd = 22;//แจ้งเตือนข้อมูล พขร. กรณีเพิ่ม/ เปลี่ยนแปลงข้อมูล
    
    public static int VendorEmployeeAddReturn = 23;//แจ้งเตือนข้อมูล พขร. กรณีเพิ่ม/ เปลี่ยนแปลงข้อมูล(รข. ถึงผู้ขนส่ง)

    public static int VendorEmployeeBeforeExpired = 24;//แจ้งเตือนข้อมูล พขร./รถ หมดอายุ (เตือน 3 เดือน/ 1 เดือน) (TMS ถึง ผู้ขนส่ง)
    public static int VendorEmployeeExpired = 25;//แจ้งเตือนข้อมูล พขร./รถ หมดอายุ (วันหมดอายุ) (TMS ถึง ผู้ขนส่ง)
    public static int VendorEmployeeOver = 26;//แจ้งเตือนข้อมูล พขร./รถบรรทุก อายุเกินกำหนด
    public static int VendorEmployeeStatus = 27;//แจ้งเรื่องสถานะการทำงาน

    public static int SurpriseCheck1 = 28;//เรื่อง แจ้งพบปัญหาการตรวจสภาพรถ (Surprise Check)
    public static int SurpriseCheck2 = 29;//เรื่อง แจ้งผลการแก้ไขการตรวจสภาพรถ (Surprise Check)
    public static int SurpriseCheck3 = 30;//เรื่อง แจ้งผลตรวจสอบการแก้ไขสภาพรถของ ผขส. (Surprise Check)
    public static int EmailAccident1 = 43;//เรื่อง แจ้งเรื่องรถขนส่งเกิดอุบัติเหตุ
    public static int EmailAccident2 = 44;//เรื่อง ผขส. ส่งสรุปรายงานอุบัติเหตุเบื้องต้น(กรณีไม่เข้าข่ายร้านแรง ไม่ระงับ พขร.)
    public static int EmailAccident3 = 45;//เรื่อง ผขส. ส่งสรุปรายงานอุบัติเหตุเบื้องต้น(กรณีเข้าข่ายร้านแรง)
    public static int EmailAccident4 = 46;//	เรื่อง แจ้งผลการพิจารณาการขออุทธรณ์กรณีรถเกิดอุบัติเหตุ
    public static int EmailAccident5 = 47;//	เรื่อง ปตท. แจ้งผลการตรวจสอบรายงานวิเคราะห์สาเหตุการเกิดอุบัติเหตุ
    public static int EmailAccident6 = 48;//เรื่อง ปตท. แจ้งผลการพิจารณากรณีรถเกิดอุบัติเหตุ
    public static int EmailAccident7 = 49;//	เรื่อง ผขส. ขออุทธรณ์ผลการพิจารณากรณีรถเกิดอุบัติเหตุ
    public static int EmailAccident8 = 50;//	เรื่อง ผขส. ส่งสรุปรายงานวิเคราะห์สาเหตุการเกิดอุบัติเหตุ
    public static int EmailKPI1 = 51;//	เรื่อง ผขส. ส่งสรุปรายงานวิเคราะห์สาเหตุการเกิดอุบัติเหตุ

    public static string GetClickHere(string URL)
    {
        return "<a href=\"" + URL + "\">คลิกที่นี่</a>";
    }

    public static string GetEncodeText(string Key)
    {
        var plaintextBytes = Encoding.UTF8.GetBytes(Key);
        var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

        return encryptedValue;
    }
}