﻿using System.Data;
using System;
using TMS_BLL.Master;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;

/// <summary>
/// Summary description for KPI_Helper
/// </summary>
public class KPI_Helper : System.Web.UI.Page
{
    public string Calculate(int FormulaID, decimal ValueA, decimal ValueB, string Year, int MonthID, string VendorID, int ContractID, DataTable dtMapping, int i, ref string PointBefore)
    {
        try
        {
            decimal Point = 0;
            string ReturnValue = string.Empty;

            if (FormulaID == 1)
            {//ขับรถเร็ว, การสูญหาย
                Point = ValueA / ValueB;
            }
            else if (FormulaID == 2)
            {//ทุจริต
                DataTable dt = KPIBLL.Instance.KPIFindSumBLL(Year, VendorID, ContractID, MonthID, FormulaID, int.Parse(dtMapping.Rows[i]["TOPIC_HEADER_ID"].ToString()), int.Parse(dtMapping.Rows[i]["TOPIC_DETAIL_ID"].ToString()));
                ValueA = ValueA + decimal.Parse(dt.Rows[0]["SUM_A"].ToString());
                Point = ValueA / ValueB;
            }
            else if (FormulaID == 3)
            {//อุบัติเหตุ
                DataTable dt = KPIBLL.Instance.KPIFindSumBLL(Year, VendorID, ContractID, MonthID, FormulaID, int.Parse(dtMapping.Rows[i]["TOPIC_HEADER_ID"].ToString()), int.Parse(dtMapping.Rows[i]["TOPIC_DETAIL_ID"].ToString()));
                ValueA = ValueA + decimal.Parse(dt.Rows[0]["SUM_A"].ToString());
                ValueB = ValueB + decimal.Parse(dt.Rows[0]["SUM_B"].ToString());
                if (!string.Equals(dt.Rows[0]["COUNT"].ToString(), string.Empty) && !string.Equals(dt.Rows[0]["COUNT"].ToString(), "0"))
                    ValueB = ValueB / (int.Parse(dt.Rows[0]["COUNT"].ToString()) + 1);

                //Point = ValueA / ValueB * int.Parse(dt.Rows[0]["MONTH_COUNT"].ToString()) * 1000000;
                Point = ValueA * 1000000 / (ValueB * int.Parse(dt.Rows[0]["MONTH_COUNT"].ToString()));
            }
            else if (FormulaID == 4)
            {//สูญหาย
                Point = ValueA / ValueB * 100;
            }

            PointBefore = Math.Round(Point, 3).ToString();
            Point = Math.Round(Point, 3);

            string Level1Start = dtMapping.Rows[i]["LEVEL1_START"].ToString();
            string Level1End = dtMapping.Rows[i]["LEVEL1_END"].ToString();

            string Level2Start = dtMapping.Rows[i]["LEVEL2_START"].ToString();
            string Level2End = dtMapping.Rows[i]["LEVEL2_END"].ToString();

            string Level3Start = dtMapping.Rows[i]["LEVEL3_START"].ToString();
            string Level3End = dtMapping.Rows[i]["LEVEL3_END"].ToString();

            string Level4Start = dtMapping.Rows[i]["LEVEL4_START"].ToString();
            string Level4End = dtMapping.Rows[i]["LEVEL4_END"].ToString();

            string Level5Start = dtMapping.Rows[i]["LEVEL5_START"].ToString();
            string Level5End = dtMapping.Rows[i]["LEVEL5_END"].ToString();

            if (!string.Equals(Level1Start, string.Empty) && !string.Equals(Level1End, string.Empty))
            {
                if (Point >= decimal.Parse(Level1Start) && Point <= decimal.Parse(Level1End))
                    ReturnValue = "1";
            }
            if (!string.Equals(Level2Start, string.Empty) && !string.Equals(Level2End, string.Empty))
            {
                if (Point >= decimal.Parse(Level2Start) && Point <= decimal.Parse(Level2End))
                    ReturnValue = "2";
            }
            if (!string.Equals(Level3Start, string.Empty) && !string.Equals(Level3End, string.Empty))
            {
                if (Point >= decimal.Parse(Level3Start) && Point <= decimal.Parse(Level3End))
                    ReturnValue = "3";
            }
            if (!string.Equals(Level4Start, string.Empty) && !string.Equals(Level4End, string.Empty))
            {
                if (Point >= decimal.Parse(Level4Start) && Point <= decimal.Parse(Level4End))
                    ReturnValue = "4";
            }
            if (!string.Equals(Level5Start, string.Empty) && !string.Equals(Level5End, string.Empty))
            {
                if (Point >= decimal.Parse(Level5Start) && Point <= decimal.Parse(Level5End))
                    ReturnValue = "5";
            }

            return ReturnValue;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public DataSet GetDataKPIYear(string Year, string VendorID, int ContractID, bool IsPTT)
    {
        try
        {
            DataSet ds = new DataSet();

            DataTable dtHeader = KPIBLL.Instance.KPISelectListHeaderBLL(Year);
            DataTable dtDetail;
            DataTable dtMapping = new DataTable();
            string Point = string.Empty;
            string PointBefore = string.Empty;

            for (int i = 0; i < dtHeader.Rows.Count; i++)
            {
                dtDetail = new DataTable();
                dtDetail = KPIBLL.Instance.KPISelectListDetailBLL(int.Parse(dtHeader.Rows[i]["TOPIC_HEADER_ID"].ToString()));
                dtDetail.TableName = "Table" + i.ToString();

                for (int j = 0; j < dtDetail.Rows.Count; j++)
                {
                    for (int k = 1; k < 13; k++)
                    {
                        dtMapping = KPIBLL.Instance.KPIMappingFormSelect2BLL(Year, k, VendorID, ContractID, int.Parse(dtHeader.Rows[i]["TOPIC_HEADER_ID"].ToString()), IsPTT);

                        if (dtMapping.Rows.Count > 0)
                        {
                            Point = Calculate(int.Parse(dtMapping.Rows[j]["FORMULA_ID"].ToString()),
                                                         decimal.Parse(dtMapping.Rows[j]["VALUE_A"].ToString()),
                                                         decimal.Parse(dtMapping.Rows[j]["VALUE_B"].ToString()),
                                                         Year, k, VendorID, ContractID, dtMapping, j, ref PointBefore);

                            dtDetail.Rows[j]["MONTH_" + k.ToString()] = Point;
                        }
                    }
                }
                //DataView dv = dtDetail.DefaultView;
                //dv.Sort = " TOPIC_DETAIL_ID ASC ";
                //DataTable dt = dv.ToTable();
                //DataRow[] dr = dtDetail.Select(" ORDER BY TOPIC_DETAIL_ID ASC ");
                ds.Tables.Add(dtDetail.Copy());
            }

            return ds;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public decimal GetKPIYear(string Year, string VendorID, int ContractID, bool IsPTT)
    {
        try
        {
            decimal Sum = 0;
            int Max = -1;
            int Min = -1;
            decimal Weight = 0;
            decimal WeightSum = 0;
            int MonthCount = 0;
            decimal Point = 0;

            decimal PointSum = 0;
            decimal KPITotal = 0;

            DataTable dtCountTotalMonth = new DataTable();
            dtCountTotalMonth.Columns.Add("MONTH");

            DataSet ds = GetDataKPIYear(Year, VendorID, ContractID, IsPTT);
            foreach (DataTable dt in ds.Tables)
            {
                WeightSum = 0;
                MonthCount = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                    WeightSum += decimal.Parse(dt.Rows[i]["WEIGHT"].ToString());

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Sum = 0;
                    Max = -1;
                    Min = -1;
                    Weight = 0;
                    Point = 0;

                    Weight = decimal.Parse(dt.Rows[i]["WEIGHT"].ToString());

                    for (int j = 1; j < 13; j++)
                    {
                        if (!string.Equals(dt.Rows[i]["MONTH_" + j.ToString()].ToString().Replace("&nbsp;", string.Empty), string.Empty))
                        {
                            Sum += int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString());

                            if (Min == -1)
                            {
                                Min = int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString());
                                Max = int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString());
                            }
                            else
                            {
                                if (Min > int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString()))
                                    Min = int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString());

                                if (Max < int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString()))
                                    Max = int.Parse(dt.Rows[i]["MONTH_" + j.ToString()].ToString());
                            }

                            if (i == 0)
                            {
                                MonthCount++;
                                this.CountTotalMonth(ref dtCountTotalMonth, j);
                            }
                        }
                    }

                    if (MonthCount > 0)
                    {
                        int MethodID = 0;

                        MethodID = int.Parse(dt.Rows[i]["METHOD_ID"].ToString());

                        if (MethodID == 1)                                  //Average
                            Point = Math.Round((Sum / MonthCount), 2, MidpointRounding.AwayFromZero);
                            //Point = Sum / MonthCount;
                        else if (MethodID == 2)                             //Maximum
                            Point = Max;
                        else if (MethodID == 3)                             //Minimum
                            Point = Min;

                        dt.Rows[i]["POINT"] = Point.ToString();

                        PointSum = PointSum + (Point * Weight);
                    }
                }

                KPITotal += (PointSum / WeightSum) * MonthCount;

                PointSum = 0;
            }

            if (dtCountTotalMonth.Rows.Count == 0)
                KPITotal = 0;
            else
                KPITotal = KPITotal / dtCountTotalMonth.Rows.Count;

            return (KPITotal * 40) / 5;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CountTotalMonth(ref DataTable dtMonth, int Month)
    {
        try
        {
            bool Found = false;
            for (int i = 0; i < dtMonth.Rows.Count; i++)
            {
                if (string.Equals(dtMonth.Rows[i]["MONTH"].ToString(), Month.ToString()))
                {
                    Found = true;
                    break;
                }
            }

            if (!Found)
                dtMonth.Rows.Add(Month.ToString());
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}