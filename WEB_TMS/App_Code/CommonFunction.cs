﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.OracleClient;
using System.Web.Mail;
using System.Configuration;
using DevExpress.Web.ASPxGridView;
using System.Text.RegularExpressions;
using System.IO;
/// <summary>
/// ฟังก์ชันทั่วๆไป สามารถใช้กับระบบอื่นๆได้
/// </summary>
public class CommonFunction
{
    /// <summary>
    /// ฟังค์ชันที่แปลงข้อมูลจาก Linq เป็น DataTable
    /// </summary>
    /// <typeparam name="T">Class ที่สอดคล้องกับข้อมูล Linq</typeparam>
    /// <param name="Data">ข้อมูลที่เป็น Linq</param>
    /// <returns>ข้อมูลที่เป็น DataTable</returns>
    public static DataTable LinqToDataTable<T>(IEnumerable<T> Data)
    {
        DataTable dtReturn = new DataTable();
        if (Data.ToList().Count == 0) return null;
        // Could add a check to verify that there is an element 0

        T TopRec = Data.ElementAt(0);

        // Use reflection to get property names, to create table

        // column names

        PropertyInfo[] oProps =
        ((Type)TopRec.GetType()).GetProperties();

        foreach (PropertyInfo pi in oProps)
        {

            Type colType = pi.PropertyType; if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {

                colType = colType.GetGenericArguments()[0];

            }

            dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
        }

        foreach (T rec in Data)
        {

            DataRow dr = dtReturn.NewRow(); foreach (PropertyInfo pi in oProps)
            {
                dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
            }
            dtReturn.Rows.Add(dr);

        }
        return dtReturn;
    }

    /// <summary>
    /// Function สำหรับเซต Popup (จะเข้า Event EndCallback ของ CallbackPanel ที่ส่งมา)
    /// </summary>
    /// <param name="xcpn">CallbackPanel ที่ครอบไว้แต่ละหน้า</param>
    /// <param name="myFunc">Function reference ที่ส่งมาจากแต่ละหน้า</param>
    public static void SetPopupOnLoad(ASPxCallbackPanel xcpn, string myFunc)
    {
        xcpn.JSProperties["cpPopup"] = myFunc;
    }

    /// <summary>
    /// Function สำหรับเซต Popup (จะเข้า Event EndCallback ของ CallbackPanel ที่ส่งมา)
    /// </summary>
    /// <param name="dvw">GridView ที่ครอบไว้แต่ละหน้า</param>
    /// <param name="myFunc">Function reference ที่ส่งมาจากแต่ละหน้า</param>
    public static void SetPopupOnLoad(ASPxGridView xcpn, string myFunc)
    {
        xcpn.JSProperties["cpPopup"] = myFunc;
    }
    public static string Get_Value(string Conn, string _sql)
    {
        using (OracleConnection _conn = new OracleConnection(Conn))
        {
            DataTable _dt = new DataTable();
            _conn.Open();
            new OracleDataAdapter(_sql, _conn).Fill(_dt);
            if (_dt.Rows.Count >= 1)
            {
                string _return = _dt.Rows[0][0].ToString();
                _dt.Dispose();
                return _return;
            }
            return "";
        }
    }

    public static string Get_Value(OracleConnection _conn, string _sql)
    {
        DataTable _dt = new DataTable();
        new OracleDataAdapter(_sql, _conn).Fill(_dt);
        if (_dt.Rows.Count >= 1)
        {
            string _return = _dt.Rows[0][0].ToString();
            _dt.Dispose();
            return _return;
        }
        return "";
    }

    public static DataTable Get_Data(string Conn, string _sql)
    {
        using (OracleConnection _conn = new OracleConnection(Conn))
        {
            DataTable _dt = new DataTable();
            _conn.Open();
            new OracleDataAdapter(_sql, _conn).Fill(_dt);

            return _dt;
        }
    }
    /// <summary>
    /// ฟังชั่นดึงข้อมูล
    /// </summary>
    /// <param name="Conn">OracleConnection</param>
    /// <param name="_sql">Query</param>
    /// <returns>DataTable</returns>
    public static DataTable Get_Data(OracleConnection Conn, string _sql)
    {

        DataTable _dt = new DataTable();
        if (Conn.State == ConnectionState.Closed) Conn.Open();
        new OracleDataAdapter(_sql, Conn).Fill(_dt);
        return _dt;

    }

    public static int Count_Value(string Conn, string _sql)
    {
        using (OracleConnection _conn = new OracleConnection(Conn))
        {
            DataTable _dt = new DataTable();
            _conn.Open();
            new OracleDataAdapter(_sql, _conn).Fill(_dt);
            if (_dt.Rows.Count > 0)
            {
                int _return = _dt.Rows.Count;
                _dt.Dispose();
                return _return;
            }
            return 0;
        }

    }

    public static int Count_Value(OracleConnection Conn, string _sql)
    {

        DataTable _dt = new DataTable();
        if (Conn.State == ConnectionState.Closed) Conn.Open();
        new OracleDataAdapter(_sql, Conn).Fill(_dt);
        if (_dt.Rows.Count > 0)
        {
            int _return = _dt.Rows.Count;
            _dt.Dispose();
            return _return;
        }
        return 0;

    }

    public static string Gen_ID(string Conn, string _sql)
    {
        using (OracleConnection _conn = new OracleConnection(Conn))
        {
            string sReturn = "";
            DataTable _dt = new DataTable();
            _conn.Open();
            new OracleDataAdapter(_sql, _conn).Fill(_dt);

            if (_dt.Rows.Count >= 1)
            {
                sReturn = "" + (Convert.ToDecimal(_dt.Rows[0][0]) + 1);
            }
            else
            {
                sReturn = "1";
            }
            _dt.Dispose();
            return sReturn;
        }
    }
    public static string Gen_ID(OracleConnection Conn, string _sql)
    {
        if (Conn.State == ConnectionState.Closed) Conn.Open();
        string sReturn = "";
        DataTable _dt = new DataTable();

        new OracleDataAdapter(_sql, Conn).Fill(_dt);

        if (_dt.Rows.Count >= 1)
        {
            sReturn = "" + (Convert.ToDecimal(_dt.Rows[0][0]) + 1);
        }
        else
        {
            sReturn = "1";
        }
        _dt.Dispose();
        return sReturn;
    }

    public static sbyte FindIndexColumnOfDataFieldInGrid(DataGrid _dgd, string _fieldname)
    {
        sbyte _i = 0; bool _c = false;
        for (; _i < _dgd.Columns.Count; _i++)
        {
            if (_dgd.Columns[_i].GetType().ToString().Equals("System.Web.UI.WebControls.TemplateColumn")) continue;
            //
            if (((BoundColumn)_dgd.Columns[_i]).DataField.Equals(_fieldname)) { _c = true; break; }
        }
        if (!_c) _i = -1;
        return _i;
    }
    // Anti SQL Injection
    public static string ReplaceInjection(string str)
    {
        string[] _blacklist = new string[] { "'", "\\", "\"", "*/", ";", "--", "<script", "/*", "</script>" };
        string strRep = str;
        if (strRep == null || strRep.Trim().Equals(String.Empty))
            return strRep;
        foreach (string _blk in _blacklist) { strRep = strRep.Replace(_blk, ""); }

        return strRep;
    }
    public static bool SendMail(string sfrom, string sto, string subject, string message, string sfilepath)
    {
        System.Net.Mail.MailMessage oMsg = new System.Net.Mail.MailMessage();//MailMessage oMsg = new MailMessage();
        try
        {
            // TODO: Replace with sender e-mail address. 
            oMsg.From = new System.Net.Mail.MailAddress(sfrom); //oMsg.From = sfrom;

            // TODO: Replace with recipient e-mail address.
            foreach (var address in sto.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct())
            {
                oMsg.To.Add(address);
            }// oMsg.To = sto;

            oMsg.Subject = subject;

            // SEND IN HTML FORMAT (comment this line to send plain text).
            oMsg.IsBodyHtml = true; //oMsg.BodyFormat = MailFormat.Html;

            // HTML Body (remove HTML tags for plain text).
            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(message, null, "text/html");
            oMsg.AlternateViews.Add(htmlView); //oMsg.Body = "<HTML><BODY>" + message + "</BODY></HTML>";

            // TODO: Replace with the name of your remote SMTP server.
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Port = 25;
            smtp.Host = ConfigurationSettings.AppSettings["smtpmail"];//SmtpMail.SmtpServer = ConfigurationManager.AppSettings["smtpmail"];
            smtp.Send(oMsg);//SmtpMail.Send(oMsg);
            oMsg = null; 
            return true;
             
        }
        catch (Exception err)
        {
            {
                string sXcute = @"INSERT INTO TLOGEMAIL (  DLOG, DSEND,  SUSERSEND, SEMAILFROM, SUSERRECIVE,  SEMAILTO, SREFID, CSTATUS,  SSUBJECT, SMESSAGE,SENDCOMPLETE,SERROR) 
VALUES ( SYSDATE, SYSDATE,'{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', 'F' ,'{9}') ";
                if (message.Length > 4000)
                {
                    message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "");

                }
                else
                {
                    message = message.Replace("'", "");
                }

                sXcute = string.Format(sXcute, "", "" + "", oMsg.From, "", "" + oMsg.To, "", "", "" + oMsg.Subject, "" + message, CommonFunction.ReplaceInjection(err.Message.ToString()));
                using (OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    // Reopen connection if close
                    if (conn.State == ConnectionState.Closed) conn.Open();
                    OracleCommand com = new OracleCommand(sXcute, conn);
                    com.ExecuteNonQuery();
                }
            }
            return false;
        }
    }
    public static bool SendNetMail(string sfrom, string sto, string subject, string message, OracleConnection ORA_Connection, string _sfilepath, params string[] _param)
    {

        bool IsSend = false;

        try
        {
            System.Net.Mail.MailMessage oMsg = new System.Net.Mail.MailMessage();

            // TODO: Replace with sender e-mail address. 
            oMsg.From = new System.Net.Mail.MailAddress(sfrom);
            // TODO: Replace with recipient e-mail address.
            // oMsg.To.Add(sto);
            foreach (var address in sto.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            {
                oMsg.To.Add(address);
            }

            // TODO: Replace with subject.
            oMsg.Subject = subject;

            // SEND IN HTML FORMAT (comment this line to send plain text).
            oMsg.IsBodyHtml = true;

            // HTML Body (remove HTML tags for plain text).
            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(message, null, "text/html");
            oMsg.AlternateViews.Add(htmlView);

            // TODO: Replace with the name of your remote SMTP server.
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Port = 25;
            smtp.Host = ConfigurationSettings.AppSettings["smtpmail"];
            smtp.Send(oMsg);
            oMsg = null;
            IsSend = true;//return true;
        }
        catch (Exception e)
        {
            IsSend = false; //return false;
        }
        //IsSend = true;
        if (IsSend)
        {//SUSERSEND ,SUSERRECIVE ,SREFID ,CSTATUS
            string sXcute = @"INSERT INTO TLOGEMAIL (  DLOG, DSEND,  SUSERSEND, SEMAILFROM, SUSERRECIVE,  SEMAILTO, SREFID, CSTATUS,  SSUBJECT, SMESSAGE,SENDCOMPLETE) 
VALUES ( SYSDATE, SYSDATE,'{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}','T' ) ";
            if (message.Length > 4000)
            {
                message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "");

            }
            else
            {
                message = message.Replace("'", "");
            }

            sXcute = string.Format(sXcute, "", "" + _param[0], "" + sfrom, "" + _param[1], "" + sto, "" + _param[3], "" + _param[4], "" + subject, "" + message);
            OracleCommand com = new OracleCommand(sXcute, ORA_Connection);
            com.ExecuteNonQuery();
        }
        else
        {
            string sXcute = @"INSERT INTO TLOGEMAIL (  DLOG, DSEND,  SUSERSEND, SEMAILFROM, SUSERRECIVE,  SEMAILTO, SREFID, CSTATUS,  SSUBJECT, SMESSAGE,SENDCOMPLETE) 
VALUES ( SYSDATE, SYSDATE,'{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', 'F' ) ";
            if (message.Length > 4000)
            {
                message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "");

            }
            else
            {
                message = message.Replace("'", "");
            }

            sXcute = string.Format(sXcute, "", "" + _param[0], "" + sfrom, "" + _param[1], "" + sto, "" + _param[3], "" + _param[4], "" + subject, "" + message);
            OracleCommand com = new OracleCommand(sXcute, ORA_Connection);
            com.ExecuteNonQuery();
        }

        return IsSend;
    }
    /// <summary>
    ///  กรุ๊ป ดาต้าเทเบิล 
    /// </summary>
    /// <param name="name">ชื่อของกร๊ปดาต้า</param>
    /// <param name="Table">Datatable ที่จะเก็บข้อมูล</param> 
    ///  <param name="select">ชื่อคอลัม ที่ต้องการนำมาใช้  ex.  Field1,Field2,Field3...</param> 
    ///  <param name="where">เงื่อนไขในการค้นหา  ปกติ เรามักใส่ตอน select อยุ่ แล้ว แต่ อาจมีนอกเหนือกว่านั้น</param> 
    ///  <param name="group">ชื่อคอลัม ที่ต้องการกร๊ป  ex.  Field1,Field2,Field3...</param> 
    /// <returns>ดาต้าเทเบิลที่เลือกมาได้  หมายเหตุ: การเรียงลำดับต้อง จัดเรียงเอง</returns>
    public static DataTable GroupDatable(string name, DataTable Table, string select, string where, string group)
    {
        DataSetHelper.DataSetHelper dsHelper = new DataSetHelper.DataSetHelper();
        return dsHelper.SelectGroupByInto(name, Table, select, where, group);
        // return dsHelper.SelectGroupByInto("groupdgdMain", Table, "questno,questname", "", "questno,questname");
    }
    /// <summary>
    ///  กรุ๊ป ดาต้าเทเบิล 
    /// </summary> 
    /// <param name="Table">Datatable ที่จะเก็บข้อมูล</param>  
    ///  <param name="group">ชื่อคอลัม ที่ต้องการกร๊ป  ex.  Field1,Field2,Field3...</param> 
    /// <returns>ดาต้าเทเบิลที่เลือกมาได้  หมายเหตุ: การเรียงลำดับต้อง จัดเรียงเอง</returns>
    public static DataTable GroupDataTable(DataTable dt, string groupColumn)
    {
        string[] arrColumn = groupColumn.Replace(" ", "").Split(',');

        DataTable newDataTable = dt.Copy();

        for (sbyte s = 0; s < newDataTable.Columns.Count; s++)
        {
            if (Array.IndexOf(arrColumn, newDataTable.Columns[s].ColumnName.ToUpper().Replace(" ", "")) == -1)
            {
                newDataTable.Columns.Remove(newDataTable.Columns[s]);
                s--;
            }
        }

        for (int i = 0; i < newDataTable.Rows.Count; i++)
        {
            for (int j = i + 1; j < newDataTable.Rows.Count; j++)
            {
                sbyte isSame = 0;
                for (sbyte s = 0; s < newDataTable.Columns.Count; s++)
                {
                    if (newDataTable.Rows[i][newDataTable.Columns[s]].ToString() == newDataTable.Rows[j][newDataTable.Columns[s]].ToString())
                    {
                        isSame++;
                    }
                }
                if (isSame == newDataTable.Columns.Count)
                {
                    newDataTable.Rows.Remove(newDataTable.Rows[j]);
                    j--;
                }
            }
        }

        return newDataTable;
    }
    /// <summary>
    ///  
    /// </summary>
    /// <param name="number">Datatable ที่จะเก็บข้อมูล</param>
    /// <param name="number">array DataRow ex. [DatatableName].Select("[file] IS NOT NUll")</param> 
    /// <returns>ดาต้าเทเบิลที่เลือกมาได้</returns>
    public static DataTable ArrayDataRowToDataTable(DataTable _dt, DataRow[] _ardr)
    {
        DataTable dt = _dt.Copy();
        dt.Rows.Clear();
        foreach (DataRow _dr in _ardr) dt.ImportRow(_dr);
        return dt;
    }

    public bool IsEnglish(string inputstring)
    {
        Regex regex = new Regex(@"[A-Za-z0-9 .,-=+(){}\[\]\\]");
        MatchCollection matches = regex.Matches(inputstring);

        if (matches.Count.Equals(inputstring.Length))
            return true;
        else
            return false;
    }
    /// <summary>
    ///  ฟังชั่นลบไฟล์ในพาทที่กำหนด
    /// </summary>
    /// <param name="int">จำนวนวันที่ลบย้อนหลัง n วัน (ไม่ต้องใส่เครื่อง - )</param>
    /// <param name="string">Path ไฟล์ ที่จะลบ</param> 
    /// <returns></returns>
    public static void CleanFileInFolder(int afterDays, string Path)
    {
        afterDays = 7;
        DirectoryInfo di = new DirectoryInfo(HttpContext.Current.Server.MapPath(Path));
        if (di.Exists)
        {
            FileInfo[] fis = di.GetFiles("*.*");

            foreach (FileInfo fi in fis) if (fi.LastWriteTime < DateTime.Now.AddDays(-afterDays)) fi.Delete();
        }
    }
}