﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using TMS_D_UpdateVehicle_SYNC_OUT_SI;
using TMS_D_UpdateTU_SYNC_OUT_SI;
using System.Web.Configuration;
using System.IO;
using System.Globalization;

/// <summary>
/// Summary description for SAP_STATUS
/// </summary>
public class SAP_STATUS
{
    public SAP_STATUS()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Data
    private string fVEH_NO;
    #endregion
    #region Property
    public string VEH_NO
    {
        get { return this.fVEH_NO; }
        set { this.fVEH_NO = value; }

    }
    #endregion
    #region Method
    public string ApproveUse_Vehicle_SYNC_OUT_SI()
    {
        string result = "";
        bool IsUpdated = false;

        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT--,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TUAS.TU_Assignment_Items,TU.STRUCKID STRAILERID ,TU.SHEADREGISTERNO STRAILERREGISTERNO  ,CAST(TRCK.SCHASIS  AS varchar2(18)) VEH_ID   ,CAST(TU.SCHASIS AS varchar2(18))  TU_ID,TRCK.STRANSPORTID CARRIER
,TRCK.VEH_TEXT , TU.VEH_TEXT TU_TEXT, TRCK.DREGISTER VEH_REG ,  TU.DREGISTER TU_REG
,NVL(TU.DEPOT ,TRCK.DEPOT ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS  ,(CASE WHEN NVL(TU.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) TU_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY,TRCK.CACTIVE
,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
,TRCK.CLASSGRP CLASS_GRP
,TU.CLASSGRP TU_CLASS_GRP , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT
,TRCK.TPPOINT TPPOINT ,TRCK.ROUTE ROUTE ,TRCK.TRUCK_CATEGORY TYPE_ID,TU.TRUCK_CATEGORY TU_TYPE
,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
,NVL(TU.DPREV_SERV,TRCK.DPREV_SERV) DPREV_SERV  
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL((CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END ),'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
LEFT JOIN
(
    SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID STRAILERID,TU.STRAILERREGISTERNO
    ,COUNT(VEH.STRUCKID) TU_Assignment_Items
    FROM( SELECT * FROM TTRUCK  WHERE SCARTYPEID='0' OR SCARTYPEID='3' ) VEH
    LEFT JOIN ( SELECT *  FROM TTRUCK WHERE SCARTYPEID='4' ) TU ON VEH.STRAILERID =TU.STRUCKID
    WHERE 1=1
    GROUP BY   VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID ,TU.STRAILERREGISTERNO
)TUAS
ON TUAS.SHEADREGISTERNO = TRCK.SHEADREGISTERNO
WHERE TRCK.SCARTYPEID in('0','3')
AND TRCK.VIEHICLE_TYPE IN('34','00')
AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC
", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];

                //if (row["SCARTYPEID"] + "" == "3")
                //{//10Wheel

                //    #region Update Semi
                //    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                //    {
                //        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                //        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                //        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                //        {
                //            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                //            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                //            string Msg = "", sErr = "";

                //            if (row["CACTIVE"] + "" == "Y")
                //            {
                //                IsUpdated = true;
                //            }

                //            if (IsUpdated)
                //            {
                //                #region Assignment
                //                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                //                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                //                //for (int nitems = 0; nitems < TU_Assignment_Items; nitems++)
                //                //{
                //                //    tuAssignment[nitems] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head

                //                //}
                //                if (row["SCARTYPEID"] + "" == "3")
                //                {//Head semi-trailer
                //                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment tail
                //                }
                //                else
                //                {
                //                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                //                }
                //                #endregion
                //                #region VEHICLE
                //                var vehicle = new UpdateVehicle_Src_Req_DT()
                //                {
                //                    VEHICLE = VEH_NO,
                //                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                //                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                //                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",
                //                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                //                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                //                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                //                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                //                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                //                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                //                    VEH_ID = "#!",//VEH_ID = row["VEH_ID"] + "",
                //                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                //                    VEH_STATUS = "",
                //                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                //                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                //                    VHCLSIGN = "B4",
                //                    TU_ITEM = tuAssignment,
                //                };

                //                #endregion
                //                try
                //                {
                //                    var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);
                //                    int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                //                    for (int i = 0; i <= Res_VEH_Row; i++)
                //                    {
                //                        IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                //                        if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                //                        {
                //                            Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                //                        }
                //                    }
                //                }
                //                catch (Exception ex)
                //                {
                //                    IsUpdated = false;
                //                    Msg = "," + ex.Message.ToString();
                //                }
                //                //IsUpdated = !Res_VEH.GT_RETURN[0].MSGTYP.Equals("E");
                //                //if (Res_VEH.GT_RETURN[0].MSGTYP.Equals("E"))
                //                //{
                //                //    foreach (var Return in Res_VEH.GT_RETURN)
                //                //    {
                //                //        Msg += "," + Return.MESSAGE;
                //                //    }
                //                //}

                //                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                //                //กรณีที่หัวไม่มีใน SAP
                //                if (Msg.Contains("Entry " + VEH_NO + "does not exist"))
                //                {
                //                    veh_create.sVehicle = VEH_NO + "";
                //                    Msg = veh_create.CRT_Vehicle_SOS();
                //                }


                //            }

                //            result = Msg;
                //        }
                //    }
                //    #endregion
                //}
                //else
                //{
                    #region Update Truck
                    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                    {
                        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "3")
                            {//10Wheel
                                #region Compartment
                                //เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                int TU_compartment_Items = ((!string.IsNullOrEmpty(dt.Rows[0]["NSLOT"] + "") ? int.Parse(dt.Rows[0]["NSLOT"] + "") : 0) == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                if (TU_compartment_Items != dt.Rows.Count)
                                {
                                    for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                    {
                                        if (dt.Rows.Count > 0)
                                        {
                                            DataRow dr_Compart = dt.Rows[idxCompart];
                                            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                            {
                                                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            };

                                        }
                                        else
                                        {
                                            sErr = "no compartment data";
                                            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                            {
                                                TU_NUMBER = "#!",
                                                CMP_MAXVOL = "#!",
                                                COM_NUMBER = "#!",
                                                LOAD_SEQ = "#!",
                                                SEQ_NMBR = "#!",
                                            };
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (DataRow dr_Compart in dt.Rows)
                                    {
                                        int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                        tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                        {
                                            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                        };

                                    }
                                }
                                #endregion
                                #region TU
                                string sCarType_ID = row["SCARTYPEID"] + "";
                                string val_MAXWGT = "0", val_UNLWGT = "0";
                                if (sCarType_ID == "0")
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "10000";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                }
                                else if (sCarType_ID == "3")
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "25000";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                }
                                else
                                {
                                    row["NWEIGHT"] = val_UNLWGT = "0";
                                    row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                }
                                var trailer = new UpdateTU_Src_Req_DT()
                                {
                                    TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                    TU_ID = "#!",// row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                    ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                    VOL_UOM = "#!",
                                    TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                    TU_STATUS = "",
                                    //TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                    TU_TEXT = row["TU_TEXT"] + "" == "" ? "#!" : row["TU_TEXT"] + "",
                                    TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                    //L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                    L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "#!" : row["SLAST_REQ_ID"] + "",
                                    EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                    CMP_ITEM = tu_compartment
                                };
                                #endregion
                                var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                for (int i = 0; i <= Res_TU_Row; i++)
                                {
                                    IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                    if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                    {
                                        //foreach (var Return in Res_TU.GT_RETURN_TU)
                                        //{
                                        //    Msg += "," + Return.MESSAGE;
                                        //}
                                        Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                    }
                                }



                            }

                            if (IsUpdated)
                            {
                                #region Assignment
                                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                                if (row["SCARTYPEID"] + "" == "0")
                                {
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                                }
                                else
                                {//Head semi-trailer
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "2" };//Assignment tail
                                }
                                #endregion
                                #region VEHICLE
                                var vehicle = new UpdateVehicle_Src_Req_DT()
                                {
                                    VEHICLE = VEH_NO,
                                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",

                                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                                    VEH_ID = "#!",//row["VEH_ID"] + "",
                                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                                    VEH_STATUS = "",
                                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                                    VHCLSIGN = "B4",//SAP ส่งได้แต่ B4   VHCLSIGN = row["FUELTYPE"] + "" == "" ? "B4" : row["FUELTYPE"] + "",
                                    TU_ITEM = tuAssignment,
                                };

                                #endregion
                                var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);


                                int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                                for (int i = 0; i <= Res_VEH_Row; i++)
                                {
                                    IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                                    if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                                    {
                                        //foreach (var Return in Res_VEH.GT_RETURN)
                                        //{
                                        //    Msg += "," + Return.MESSAGE;
                                        //}
                                        Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                                    }
                                }


                            }

                            result = Msg;
                        }
                    }
                    #endregion
                //}
            }
        }
        return result;

    }
    public string LOCK_Vehicle_SYNC_OUT_SI()
    {
        string result = "";
        //bool IsUpdated = false;
        bool IsUpdated = true;

        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT--,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TUAS.TU_Assignment_Items,TU.STRUCKID STRAILERID ,TU.SHEADREGISTERNO STRAILERREGISTERNO  ,CAST(TRCK.SCHASIS  AS varchar2(18)) VEH_ID   ,CAST(TU.SCHASIS AS varchar2(18))  TU_ID,TRCK.STRANSPORTID CARRIER
,TRCK.VEH_TEXT , TU.VEH_TEXT TU_TEXT, TRCK.DREGISTER VEH_REG ,  TU.DREGISTER TU_REG
,NVL(TU.DEPOT ,TRCK.DEPOT ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS  ,(CASE WHEN NVL(TU.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) TU_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY,TRCK.CACTIVE
,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
,TRCK.CLASSGRP CLASS_GRP
,TU.CLASSGRP TU_CLASS_GRP , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT
,TRCK.TPPOINT TPPOINT ,TRCK.ROUTE ROUTE ,TRCK.TRUCK_CATEGORY TYPE_ID,TU.TRUCK_CATEGORY TU_TYPE
,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
,NVL(TU.DPREV_SERV,TRCK.DPREV_SERV) DPREV_SERV  
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL((CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END ),'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
LEFT JOIN
(
    SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID STRAILERID,TU.STRAILERREGISTERNO
    ,COUNT(VEH.STRUCKID) TU_Assignment_Items
    FROM( SELECT * FROM TTRUCK  WHERE SCARTYPEID='0' OR SCARTYPEID='3' ) VEH
    LEFT JOIN ( SELECT *  FROM TTRUCK WHERE SCARTYPEID='4' ) TU ON VEH.STRAILERID =TU.STRUCKID
    WHERE 1=1
    GROUP BY   VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID ,TU.STRAILERREGISTERNO
)TUAS
ON TUAS.SHEADREGISTERNO = TRCK.SHEADREGISTERNO
WHERE TRCK.SCARTYPEID in('0','3')
AND TRCK.VIEHICLE_TYPE IN('34','00')
AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC
", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];

                if (row["SCARTYPEID"] + "" == "3")
                {//10Wheel

                    #region Update Semi
                    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                    {
                        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (IsUpdated)
                            {
                                #region Assignment
                                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                                //for (int nitems = 0; nitems < TU_Assignment_Items; nitems++)
                                //{
                                //    tuAssignment[nitems] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head

                                //}
                                if (row["SCARTYPEID"] + "" == "3")
                                {//Head semi-trailer
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment tail
                                }
                                else
                                {
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                                }
                                #endregion
                                #region VEHICLE
                                var vehicle = new UpdateVehicle_Src_Req_DT()
                                {
                                    VEHICLE = VEH_NO,
                                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",
                                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                                    VEH_ID = "#!",//VEH_ID = row["VEH_ID"] + "",
                                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                                    VEH_STATUS = "1",
                                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                                    VHCLSIGN = "B4",
                                    TU_ITEM = tuAssignment,
                                };

                                #endregion
                                try
                                {
                                    var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);
                                    int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                                    for (int i = 0; i <= Res_VEH_Row; i++)
                                    {
                                        IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                                        if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                                        {
                                            Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    IsUpdated = false;
                                    Msg = "," + ex.Message.ToString();
                                }
                                //IsUpdated = !Res_VEH.GT_RETURN[0].MSGTYP.Equals("E");
                                //if (Res_VEH.GT_RETURN[0].MSGTYP.Equals("E"))
                                //{
                                //    foreach (var Return in Res_VEH.GT_RETURN)
                                //    {
                                //        Msg += "," + Return.MESSAGE;
                                //    }
                                //}

                                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                                //กรณีที่หัวไม่มีใน SAP
                                if (Msg.Contains("Entry " + VEH_NO + "does not exist"))
                                {
                                    veh_create.sVehicle = VEH_NO + "";
                                    Msg = veh_create.CRT_Vehicle_SOS();
                                }


                            }

                            result = Msg;

                            if (row["CACTIVE"] + "" == "Y")
                            {
                                if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "3")
                                {//10Wheel
                                    #region Compartment
                                    //เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                    int TU_compartment_Items = (int.Parse(dt.Rows[0]["NSLOT"] + "") == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                    var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                    if (TU_compartment_Items != dt.Rows.Count)
                                    {
                                        for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                        {
                                            if (dt.Rows.Count > 0)
                                            {
                                                DataRow dr_Compart = dt.Rows[idxCompart];
                                                tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                                {
                                                    TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                                    CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                                    COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                    LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                    SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                };

                                            }
                                            else
                                            {
                                                sErr = "no compartment data";
                                                tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                                {
                                                    TU_NUMBER = "#!",
                                                    CMP_MAXVOL = "#!",
                                                    COM_NUMBER = "#!",
                                                    LOAD_SEQ = "#!",
                                                    SEQ_NMBR = "#!",
                                                };
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (DataRow dr_Compart in dt.Rows)
                                        {
                                            int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                            {
                                                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            };

                                        }
                                    }
                                    #endregion
                                    #region TU
                                    string sCarType_ID = row["SCARTYPEID"] + "";
                                    string val_MAXWGT = "0", val_UNLWGT = "0";
                                    if (sCarType_ID == "0")
                                    {
                                        row["NWEIGHT"] = val_UNLWGT = "10000";
                                        row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                    }
                                    else if (sCarType_ID == "3")
                                    {
                                        row["NWEIGHT"] = val_UNLWGT = "25000";
                                        row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                    }
                                    else
                                    {
                                        row["NWEIGHT"] = val_UNLWGT = "0";
                                        row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                    }
                                    var trailer = new UpdateTU_Src_Req_DT()
                                    {
                                        TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                        TU_ID = "#!",//row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                        ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                        VOL_UOM = "#!",
                                        TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                        TU_STATUS = "1",
                                        //TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                        TU_TEXT = row["TU_TEXT"] + "" == "" ? "#!" : row["TU_TEXT"] + "",
                                        TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                        //L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                        L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "#!" : row["SLAST_REQ_ID"] + "",
                                        EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                        CMP_ITEM = tu_compartment
                                    };
                                    #endregion
                                    try
                                    {
                                        var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                        //IsUpdated = !Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E");
                                        //if (Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E"))
                                        //{
                                        //    foreach (var Return in Res_TU.GT_RETURN_TU)
                                        //    {
                                        //        Msg += "," + Return.MESSAGE;
                                        //    }
                                        //}

                                        int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                        for (int i = 0; i <= Res_TU_Row; i++)
                                        {
                                            IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                            if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                            {
                                                Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        IsUpdated = false;
                                        Msg = "," + ex.Message.ToString();
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Update Truck
                    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                    {
                        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "3")
                            {//10Wheel
                                //#region Compartment
                                ////เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                //int TU_compartment_Items = ((!string.IsNullOrEmpty(dt.Rows[0]["NSLOT"] + "") ? int.Parse(dt.Rows[0]["NSLOT"] + "") : 0) == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                //var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                //if (TU_compartment_Items != dt.Rows.Count)
                                //{
                                //    for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                //    {
                                //        if (dt.Rows.Count > 0)
                                //        {
                                //            DataRow dr_Compart = dt.Rows[idxCompart];
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            };

                                //        }
                                //        else
                                //        {
                                //            sErr = "no compartment data";
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = "#!",
                                //                CMP_MAXVOL = "#!",
                                //                COM_NUMBER = "#!",
                                //                LOAD_SEQ = "#!",
                                //                SEQ_NMBR = "#!",
                                //            };
                                //        }
                                //    }
                                //}
                                //else
                                //{
                                //    foreach (DataRow dr_Compart in dt.Rows)
                                //    {
                                //        int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                //        tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //        {
                                //            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //        };

                                //    }
                                //}
                                //#endregion
                                //#region TU
                                //string sCarType_ID = row["SCARTYPEID"] + "";
                                //string val_MAXWGT = "0", val_UNLWGT = "0";
                                //if (sCarType_ID == "0")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "10000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                //}
                                //else if (sCarType_ID == "3")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "25000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                //}
                                //else
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "0";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                //}
                                //var trailer = new UpdateTU_Src_Req_DT()
                                //{
                                //    TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                //    TU_ID = "#!",// row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                //    ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                //    VOL_UOM = "#!",
                                //    TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                //    TU_STATUS = "1",
                                //    //TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                //    TU_TEXT = row["TU_TEXT"] + "" == "" ? "#!" : row["TU_TEXT"] + "",
                                //    TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                //    //L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                //    L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "#!" : row["SLAST_REQ_ID"] + "",
                                //    EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                //    CMP_ITEM = tu_compartment
                                //};
                                //#endregion
                                //var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                //int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                //for (int i = 0; i <= Res_TU_Row; i++)
                                //{
                                //    IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                //    if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                //    {
                                //        //foreach (var Return in Res_TU.GT_RETURN_TU)
                                //        //{
                                //        //    Msg += "," + Return.MESSAGE;
                                //        //}
                                //        Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                //    }
                                //}



                            }

                            if (IsUpdated)
                            {
                                #region Assignment
                                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                                tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                                if (row["SCARTYPEID"] + "" == "3")
                                {//Head semi-trailer
                                    tuAssignment[1] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "2" };//Assignment tail
                                }
                                #endregion
                                #region VEHICLE
                                var vehicle = new UpdateVehicle_Src_Req_DT()
                                {
                                    VEHICLE = VEH_NO,
                                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",

                                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                                    VEH_ID = "#!",//row["VEH_ID"] + "",
                                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                                    VEH_STATUS = "1",
                                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                                    VHCLSIGN = "B4",//SAP ส่งได้แต่ B4   VHCLSIGN = row["FUELTYPE"] + "" == "" ? "B4" : row["FUELTYPE"] + "",
                                    TU_ITEM = tuAssignment,
                                };

                                #endregion
                                var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);


                                int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                                for (int i = 0; i <= Res_VEH_Row; i++)
                                {
                                    IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                                    if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                                    {
                                        //foreach (var Return in Res_VEH.GT_RETURN)
                                        //{
                                        //    Msg += "," + Return.MESSAGE;
                                        //}
                                        Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                                    }
                                }


                            }

                            result = Msg;
                        }
                    }
                    #endregion
                }
            }
        }
        return result;

    }
    public string BlACKLIST_Vehicle_SYNC_OUT_SI()
    {
        string result = "";
        //bool IsUpdated = false;
        bool IsUpdated = true;

        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT--,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TUAS.TU_Assignment_Items,TU.STRUCKID STRAILERID ,TU.SHEADREGISTERNO STRAILERREGISTERNO  ,CAST(TRCK.SCHASIS  AS varchar2(18)) VEH_ID   ,CAST(TU.SCHASIS AS varchar2(18))  TU_ID,TRCK.STRANSPORTID CARRIER
,TRCK.VEH_TEXT , TU.VEH_TEXT TU_TEXT, TRCK.DREGISTER VEH_REG ,  TU.DREGISTER TU_REG
,NVL(TU.DEPOT ,TRCK.DEPOT ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS  ,(CASE WHEN NVL(TU.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) TU_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY,TRCK.CACTIVE
,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
,TRCK.CLASSGRP CLASS_GRP
,TU.CLASSGRP TU_CLASS_GRP , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT
,TRCK.TPPOINT TPPOINT ,TRCK.ROUTE ROUTE ,TRCK.TRUCK_CATEGORY TYPE_ID,TU.TRUCK_CATEGORY TU_TYPE
,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
,NVL(TU.DPREV_SERV,TRCK.DPREV_SERV) DPREV_SERV  
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL((CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END ),'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
LEFT JOIN
(
    SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID STRAILERID,TU.STRAILERREGISTERNO
    ,COUNT(VEH.STRUCKID) TU_Assignment_Items
    FROM( SELECT * FROM TTRUCK  WHERE SCARTYPEID='0' OR SCARTYPEID='3' ) VEH
    LEFT JOIN ( SELECT *  FROM TTRUCK WHERE SCARTYPEID='4' ) TU ON VEH.STRAILERID =TU.STRUCKID
    WHERE 1=1
    GROUP BY   VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID ,TU.STRAILERREGISTERNO
)TUAS
ON TUAS.SHEADREGISTERNO = TRCK.SHEADREGISTERNO
WHERE TRCK.SCARTYPEID in('0','3')
AND TRCK.VIEHICLE_TYPE IN('34','00')
AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC
", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];

                if (row["SCARTYPEID"] + "" == "3")
                {//10Wheel

                    #region Update Semi
                    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                    {
                        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "3")
                            {//10Wheel
                                //#region Compartment
                                ////เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                //int TU_compartment_Items = (int.Parse(dt.Rows[0]["NSLOT"] + "") == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                //var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                //if (TU_compartment_Items != dt.Rows.Count)
                                //{
                                //    for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                //    {
                                //        if (dt.Rows.Count > 0)
                                //        {
                                //            DataRow dr_Compart = dt.Rows[idxCompart];
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            };

                                //        }
                                //        else
                                //        {
                                //            sErr = "no compartment data";
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = "#!",
                                //                CMP_MAXVOL = "#!",
                                //                COM_NUMBER = "#!",
                                //                LOAD_SEQ = "#!",
                                //                SEQ_NMBR = "#!",
                                //            };
                                //        }
                                //    }
                                //}
                                //else
                                //{
                                //    foreach (DataRow dr_Compart in dt.Rows)
                                //    {
                                //        int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                //        tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //        {
                                //            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //        };

                                //    }
                                //}
                                //#endregion
                                //#region TU
                                //string sCarType_ID = row["SCARTYPEID"] + "";
                                //string val_MAXWGT = "0", val_UNLWGT = "0";
                                //if (sCarType_ID == "0")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "10000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                //}
                                //else if (sCarType_ID == "3")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "25000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                //}
                                //else
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "0";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                //}
                                //var trailer = new UpdateTU_Src_Req_DT()
                                //{
                                //    TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                //    TU_ID = "#!",//row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                //    ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                //    VOL_UOM = "#!",
                                //    TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                //    TU_STATUS = "2",
                                //    //TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                //    TU_TEXT = row["TU_TEXT"] + "" == "" ? "#!" : row["TU_TEXT"] + "",
                                //    TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                //    //L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                //    L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "#!" : row["SLAST_REQ_ID"] + "",
                                //    EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                //    CMP_ITEM = tu_compartment
                                //};
                                //#endregion
                                //try
                                //{
                                //    var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                //    //IsUpdated = !Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E");
                                //    //if (Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E"))
                                //    //{
                                //    //    foreach (var Return in Res_TU.GT_RETURN_TU)
                                //    //    {
                                //    //        Msg += "," + Return.MESSAGE;
                                //    //    }
                                //    //}

                                //    int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                //    for (int i = 0; i <= Res_TU_Row; i++)
                                //    {
                                //        IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                //        if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                //        {
                                //            Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                //        }
                                //    }
                                //}
                                //catch (Exception ex)
                                //{
                                //    IsUpdated = false;
                                //    Msg = "," + ex.Message.ToString();
                                //}
                            }

                            if (IsUpdated)
                            {
                                #region Assignment
                                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                                //for (int nitems = 0; nitems < TU_Assignment_Items; nitems++)
                                //{
                                //    tuAssignment[nitems] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head

                                //}
                                if (row["SCARTYPEID"] + "" == "3")
                                {//Head semi-trailer
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment tail
                                }
                                else
                                {
                                    tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                                }
                                #endregion
                                #region VEHICLE
                                var vehicle = new UpdateVehicle_Src_Req_DT()
                                {
                                    VEHICLE = VEH_NO,
                                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",
                                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                                    VEH_ID = "#!",//VEH_ID = row["VEH_ID"] + "",
                                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                                    VEH_STATUS = "2",
                                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                                    VHCLSIGN = "B4",
                                    TU_ITEM = tuAssignment,
                                };

                                #endregion
                                try
                                {
                                    var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);
                                    int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                                    for (int i = 0; i <= Res_VEH_Row; i++)
                                    {
                                        IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                                        if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                                        {
                                            Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    IsUpdated = false;
                                    Msg = "," + ex.Message.ToString();
                                }
                                //IsUpdated = !Res_VEH.GT_RETURN[0].MSGTYP.Equals("E");
                                //if (Res_VEH.GT_RETURN[0].MSGTYP.Equals("E"))
                                //{
                                //    foreach (var Return in Res_VEH.GT_RETURN)
                                //    {
                                //        Msg += "," + Return.MESSAGE;
                                //    }
                                //}

                                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                                //กรณีที่หัวไม่มีใน SAP
                                if (Msg.Contains("Entry " + VEH_NO + "does not exist"))
                                {
                                    veh_create.sVehicle = VEH_NO + "";
                                    Msg = veh_create.CRT_Vehicle_SOS();
                                }


                            }

                            result = Msg;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Update Truck
                    using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
                    {
                        dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        using (var TU_Client = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port3"))
                        {
                            TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                            TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                            string Msg = "", sErr = "";

                            if (row["SCARTYPEID"] + "" == "0" || row["SCARTYPEID"] + "" == "3")
                            {//10Wheel
                                //#region Compartment
                                ////เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                                //int TU_compartment_Items = ((!string.IsNullOrEmpty(dt.Rows[0]["NSLOT"] + "") ? int.Parse(dt.Rows[0]["NSLOT"] + "") : 0) == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                                //var tu_compartment = new UpdateTU_Src_Req_DTItem[TU_compartment_Items];
                                //if (TU_compartment_Items != dt.Rows.Count)
                                //{
                                //    for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                //    {
                                //        if (dt.Rows.Count > 0)
                                //        {
                                //            DataRow dr_Compart = dt.Rows[idxCompart];
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //                CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //                COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //                SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            };

                                //        }
                                //        else
                                //        {
                                //            sErr = "no compartment data";
                                //            tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //            {
                                //                TU_NUMBER = "#!",
                                //                CMP_MAXVOL = "#!",
                                //                COM_NUMBER = "#!",
                                //                LOAD_SEQ = "#!",
                                //                SEQ_NMBR = "#!",
                                //            };
                                //        }
                                //    }
                                //}
                                //else
                                //{
                                //    foreach (DataRow dr_Compart in dt.Rows)
                                //    {
                                //        int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                //        tu_compartment[idxCompart] = new UpdateTU_Src_Req_DTItem()
                                //        {
                                //            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + "",
                                //            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                //            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                //        };

                                //    }
                                //}
                                //#endregion
                                //#region TU
                                //string sCarType_ID = row["SCARTYPEID"] + "";
                                //string val_MAXWGT = "0", val_UNLWGT = "0";
                                //if (sCarType_ID == "0")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "10000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                                //}
                                //else if (sCarType_ID == "3")
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "25000";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                                //}
                                //else
                                //{
                                //    row["NWEIGHT"] = val_UNLWGT = "0";
                                //    row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                                //}
                                //var trailer = new UpdateTU_Src_Req_DT()
                                //{
                                //    TU_NUMBER = "" + dt.Rows[0]["STRAILERREGISTERNO"],
                                //    TU_ID = "#!",// row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                //    ZSTART_DAT = row["DPREV_SERV"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DPREV_SERV"] + ""),
                                //    VOL_UOM = "#!",
                                //    TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                //    TU_STATUS = "2",
                                //    //TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                //    TU_TEXT = row["TU_TEXT"] + "" == "" ? "#!" : row["TU_TEXT"] + "",
                                //    TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                //    //L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                //    L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "#!" : row["SLAST_REQ_ID"] + "",
                                //    EXPIRE_DAT = row["DWATEREXPIRE"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["DWATEREXPIRE"] + ""),

                                //    CMP_ITEM = tu_compartment
                                //};
                                //#endregion
                                //var Res_TU = TU_Client.UpdateTU_SYNC_OUT_SI(trailer);

                                //int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                //for (int i = 0; i <= Res_TU_Row; i++)
                                //{
                                //    IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                //    if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                //    {
                                //        //foreach (var Return in Res_TU.GT_RETURN_TU)
                                //        //{
                                //        //    Msg += "," + Return.MESSAGE;
                                //        //}
                                //        Msg += "," + Res_TU.GT_RETURN_TU[i].MESSAGE;
                                //    }
                                //}



                            }

                            if (IsUpdated)
                            {
                                #region Assignment
                                int TU_Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                                var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items];
                                tuAssignment[0] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["SHEADREGISTERNO"] + "", SEQ_NMBR = "1" };//Assignment Head
                                if (row["SCARTYPEID"] + "" == "3")
                                {//Head semi-trailer
                                    tuAssignment[1] = new UpdateVehicle_Src_Req_DTItem() { VEHICLE = row["SHEADREGISTERNO"] + "", TU_NUMBER = row["STRAILERREGISTERNO"] + "", SEQ_NMBR = "2" };//Assignment tail
                                }
                                #endregion
                                #region VEHICLE
                                var vehicle = new UpdateVehicle_Src_Req_DT()
                                {
                                    VEHICLE = VEH_NO,
                                    //parameters ถ้าใน TMS ไม่มีค่ากำหนด Blank(“”) เนื่องจาก ก่อนหน้าที่ส่งค่าไป เป็นPF0015  ซึ่งเป็นข้อมูลที่ผิดพลาด 06102558 ป่าน//
                                    //CARRIER = row["CARRIER"] + "" == "" ? "PF0015" : row["CARRIER"] + "",
                                    CARRIER = row["CARRIER"] + "" == "" ? "" : row["CARRIER"] + "",

                                    //CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "",
                                    CLASS_GRP = row["CLASS_GRP"] + "" == "" ? "" : row["CLASS_GRP"] + "",

                                    DEPOT = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "",

                                    //REG_DATE = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + ""),
                                    REG_DATE = row["VEH_REG"] + "" == "" ? "" : DateTimeCheck(row["VEH_REG"] + ""),

                                    ROUTE = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "",
                                    VEH_ID = "#!",//row["VEH_ID"] + "",
                                    TPPOINT = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "",
                                    VEH_STATUS = "2",
                                    //VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "",
                                    VEH_TEXT = row["VEH_TEXT"] + "" == "" ? "#!" : row["VEH_TEXT"] + "",
                                    VHCLSIGN = "B4",//SAP ส่งได้แต่ B4   VHCLSIGN = row["FUELTYPE"] + "" == "" ? "B4" : row["FUELTYPE"] + "",
                                    TU_ITEM = tuAssignment,
                                };

                                #endregion
                                var Res_VEH = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);


                                int Res_VEH_Row = Res_VEH.GT_RETURN.Length > 0 ? (Res_VEH.GT_RETURN.Length - 1) : 0;
                                for (int i = 0; i <= Res_VEH_Row; i++)
                                {
                                    IsUpdated = !Res_VEH.GT_RETURN[i].MSGTYP.Equals("E");
                                    if (Res_VEH.GT_RETURN[i].MSGTYP.Equals("E"))
                                    {
                                        //foreach (var Return in Res_VEH.GT_RETURN)
                                        //{
                                        //    Msg += "," + Return.MESSAGE;
                                        //}
                                        Msg += "," + Res_VEH.GT_RETURN[i].MESSAGE;
                                    }
                                }


                            }

                            result = Msg;
                        }
                    }
                    #endregion
                }
            }
        }
        return result;

    }
    protected void SaveResult2TextFIle(List<string> _lst)
    {

        string codeVersion = "1.0.0";
        string logName = "SAP_SYNTRCK_" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
        using (System.IO.TextWriter tw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("UploadFile/SAP_Log/" + logName), true))
        {
            tw.WriteLine("Time: " + DateTime.Now.ToString("HH:mm:ss"));
            foreach (string str in _lst)
            {
                tw.WriteLine(_lst.IndexOf(str).ToString() + ": " + str);
            }
            tw.WriteLine("-------");
        }
    }
    public string UDT_TU_SYNC_OUT_SI()
    {
        string result = "";
        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID ,TRCK.NSLOT  ,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TU.STRUCKID STRAILERID--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END STRAILERID
,TU.SHEADREGISTERNO STRAILERREGISTERNO--,CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. SHEADREGISTERNO  ELSE TRCK.STRAILERREGISTERNO END STRAILERREGISTERNO
,TRCK.STRANSPORTID CARRIER,TRCK.STERMINALID DEPOT ,TRCK.NTOTALCAPACITY ,TRCK.NWEIGHT ,TRCK.NCALC_WEIGHT ,TRCK.NLOAD_WEIGHT ,TRCK.DWATEREXPIRE ,TRCK.FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY
,'' CLASS_GRP ,'' TPPOINT ,'' VEH_TEXT
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL((CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK. STRUCKID  ELSE TRCK.STRAILERID END ),'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
WHERE TRCK.SCARTYPEID in('4')
AND TRCK.SHEADREGISTERNO ='" + VEH_NO + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                int Assignment_Items = int.Parse(dt.Rows[0]["TU_Assignment_Items"] + "");
                using (var ClientTU = new UpdateTU_SYNC_OUT_SIClient("HTTP_Port"))
                {
                    ClientTU.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                    ClientTU.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
                    var Assignment = new UpdateTU_Src_Req_DTItem[Assignment_Items];
                    var TUnit = new UpdateTU_Src_Req_DT()
                    {
                        CMP_ITEM = Assignment,
                        EXPIRE_DAT = "",
                        L_NUMBER = "",
                        TU_MAXWGT = "",
                        TU_NUMBER = "",
                        TU_STATUS = "",
                        TU_TEXT = "",
                        TU_UNLWGT = "",
                        VOL_UOM = "",
                        ZSTART_DAT = ""
                    };

                    var response = ClientTU.UpdateTU_SYNC_OUT_SI(TUnit);
                }
            }
        }
        return result;
    }
    protected string MapClassGroup(string sClassGroup)
    {
        if (sClassGroup.Substring(0, 1) == "O")
        {

        }
        return sClassGroup;
    }
    private string DateTimeCheck(string sDate)
    {
        string Date_Result = "1900-01-01";

        if (!string.IsNullOrEmpty(sDate))
        {
            if (Convert.ToDateTime(sDate).Year > 1500)
            {
                Date_Result = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
            }
        }

        return Date_Result;
    }
    #endregion
}