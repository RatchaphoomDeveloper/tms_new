using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for TCHECKPRODUCTITEM
/// </summary>
public class TCHECKPRODUCTITEM
{
    public static OracleConnection conn;
    public static Page page;
    //public TCHECKPRODUCTITEM() { }
    public TCHECKPRODUCTITEM(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }
    #region Data
    private string fSCHECKID;
    private string fSCHECKLISTID;
    private string fSVERSIONLIST;
    private string fSTYPECHECKLISTID;
    private string fDCHECKLIST;
    private string fCOTHER;
    private string fSOTHERREMARK;
    private string fDCREATE;
    private string fSCREATE;
    private string fDUPDATE;
    private string fSUPDATE;
    private string fNPOINT;
    #endregion
    #region Property
    public string SCHECKID
    {
        get { return this.fSCHECKID; }
        set { this.fSCHECKID = value; }
    }
    public string SCHECKLISTID
    {
        get { return this.fSCHECKLISTID; }
        set { this.fSCHECKLISTID = value; }
    }
    public string SVERSIONLIST
    {
        get { return this.fSVERSIONLIST; }
        set { this.fSVERSIONLIST = value; }
    }
    public string STYPECHECKLISTID
    {
        get { return this.fSTYPECHECKLISTID; }
        set { this.fSTYPECHECKLISTID = value; }
    }
    public string DCHECKLIST
    {
        get { return this.fDCHECKLIST; }
        set { this.fDCHECKLIST = value; }
    }
    public string COTHER
    {
        get { return this.fCOTHER; }
        set { this.fCOTHER = value; }
    }
    public string SOTHERREMARK
    {
        get { return this.fSOTHERREMARK; }
        set { this.fSOTHERREMARK = value; }
    }
    public string DCREATE
    {
        get { return this.fDCREATE; }
        set { this.fDCREATE = value; }
    }
    public string SCREATE
    {
        get { return this.fSCREATE; }
        set { this.fSCREATE = value; }
    }
    public string DUPDATE
    {
        get { return this.fDUPDATE; }
        set { this.fDUPDATE = value; }
    }
    public string SUPDATE
    {
        get { return this.fSUPDATE; }
        set { this.fSUPDATE = value; }
    }
    public string NPOINT
    {
        get { return this.fNPOINT; }
        set { this.fNPOINT = value; }
    }
    #endregion
    #region Method
    public int Insert()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("IUTCHECKPRODUCTITEM", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("S_CHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("S_CHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("S_VERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("S_TYPECHECKLISTID", STYPECHECKLISTID);
            cmd.Parameters.AddWithValue("D_CHECKLIST", DCHECKLIST);
            cmd.Parameters.AddWithValue("C_OTHER", COTHER);
            cmd.Parameters.AddWithValue("S_OTHERREMARK", SOTHERREMARK);
            cmd.Parameters.AddWithValue("D_CREATE", DCREATE);
            cmd.Parameters.AddWithValue("S_CREATE", SCREATE);
            cmd.Parameters.AddWithValue("D_UPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("S_UPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("N_POINT", NPOINT); 
            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    public int InsertTab2(string SCONTRACTID, string STRUCKID, string DCHECK ,string SREMARK)
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("IUTCHECKPRODUCTITEM_TAB2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("S_CHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("S_CHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("S_VERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("S_TYPECHECKLISTID", STYPECHECKLISTID);
            cmd.Parameters.AddWithValue("D_CHECKLIST", DCHECKLIST);
            cmd.Parameters.AddWithValue("C_OTHER", COTHER);
            cmd.Parameters.AddWithValue("S_OTHERREMARK", SOTHERREMARK);
            cmd.Parameters.AddWithValue("D_CREATE", DCREATE);
            cmd.Parameters.AddWithValue("S_CREATE", SCREATE);
            cmd.Parameters.AddWithValue("D_UPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("S_UPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("N_POINT", NPOINT);
            cmd.Parameters.AddWithValue("S_CONTRACTID", SCONTRACTID);
            cmd.Parameters.AddWithValue("S_TRUCKID", STRUCKID);
            cmd.Parameters.AddWithValue("D_CHECK", DCHECK);
            cmd.Parameters.AddWithValue("S_REMARK", SREMARK);
            //cmd.Parameters.AddWithValue("N_DAY_MA", NDAY_MA);
            //cmd.Parameters.AddWithValue("C_MA", CMA);
            //cmd.Parameters.AddWithValue("S_REMARK", SREMARK);
            //cmd.Parameters.AddWithValue("D_BEGIN_LIST", DBEGIN_LIST);
            //cmd.Parameters.AddWithValue("D_END_LIST", DEND_LIST);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public int Update()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("UTCHECKPRODUCTITEM", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SCHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("@SCHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("@SVERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("@STYPECHECKLISTID", STYPECHECKLISTID);
            cmd.Parameters.AddWithValue("@DCHECKLIST", DCHECKLIST);
            cmd.Parameters.AddWithValue("@COTHER", COTHER);
            cmd.Parameters.AddWithValue("@SOTHERREMARK", SOTHERREMARK);
            cmd.Parameters.AddWithValue("@DCREATE", DCREATE);
            cmd.Parameters.AddWithValue("@SCREATE", SCREATE);
            cmd.Parameters.AddWithValue("@DUPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("@SUPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("@NPOINT", NPOINT);
            
            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    public int Delete()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("DTCHECKPRODUCTITEM", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    public bool Open()
    {
        string sqlstr;
        bool result = false;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            sqlstr = "SELECT * FROM TCHECKPRODUCTITEM ";
            DataTable dt = new DataTable();
            new OracleDataAdapter(sqlstr, conn).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                SCHECKID = row["SCHECKID"].ToString();
                SCHECKLISTID = row["SCHECKLISTID"].ToString();
                SVERSIONLIST = row["SVERSIONLIST"].ToString();
                STYPECHECKLISTID = row["STYPECHECKLISTID"].ToString();
                DCHECKLIST = row["DCHECKLIST"].ToString();
                COTHER = row["COTHER"].ToString();
                SOTHERREMARK = row["SOTHERREMARK"].ToString();
                DCREATE = row["DCREATE"].ToString();
                SCREATE = row["SCREATE"].ToString();
                DUPDATE = row["DUPDATE"].ToString();
                SUPDATE = row["SUPDATE"].ToString();
                NPOINT = row["NPOINT"].ToString();
                dt.Dispose();
                result = true;
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    #endregion
}
