﻿using System;
using DevExpress.Web.ASPxEditors;
using System.Data;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for DropDownList
/// </summary>
public static class DropDownListHelper
{
    public static void BindDropDownList(ref DropDownList ddl, DataTable dtSource, string ValueMember, string DisplayMember, bool AddHeader)
    {
        try
        {
            if ((AddHeader) && (dtSource != null))
            {
                DataRow dr = dtSource.NewRow();
                dr[DisplayMember] = "- เลือกข้อมูล -";
                dtSource.Rows.InsertAt(dr, 0);
            }

            if (dtSource == null)
            {
                ddl.Items.Clear();
            }
            else
            {
                ddl.DataSource = dtSource;
                ddl.DataValueField = ValueMember;
                ddl.DataTextField = DisplayMember;
                ddl.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void BindDropDownList(ref ASPxComboBox ddl, DataTable dtSource, string ValueMember, string DisplayMember, bool AddHeader)
    {
        try
        {
            if (AddHeader)
            {
                DataRow dr = dtSource.NewRow();
                dr[DisplayMember] = "- เลือกข้อมูล -";
                dtSource.Rows.InsertAt(dr, 0);
            }

            ddl.DataSource = dtSource;
            ddl.Value = ValueMember;
            ddl.Text = DisplayMember;
            ddl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public static void BindCheckBoxList(ref CheckBoxList chk, DataTable dtSource, string ValueMember, string DisplayMember)
    {
        try
        {
            chk.DataSource = dtSource;
            chk.DataValueField = ValueMember;
            chk.DataTextField = DisplayMember;
            chk.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void BindRadioButton(ref RadioButtonList rad, DataTable dtSource, string ValueMember, string DisplayMember)
    {
        try
        {
            rad.DataSource = dtSource;
            rad.DataValueField = ValueMember;
            rad.DataTextField = DisplayMember;
            rad.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void BindDropDown(ref DropDownList cbo, DataTable dtSource, string valueFlied, string valueText, bool isHeader, string ChooseText)
    {
        try
        {
            if (dtSource == null)
            {
                cbo.Items.Clear();
                cbo.DataBind();

                if (isHeader)
                {
                    dtSource = new DataTable();
                    dtSource.Columns.Add(valueFlied);
                    dtSource.Columns.Add(valueText);

                    DataRow dr = dtSource.NewRow();
                    dr[valueText] = ChooseText;
                    dtSource.Rows.InsertAt(dr, 0);
                }
                cbo.DataSource = dtSource;
                cbo.DataValueField = valueFlied;
                cbo.DataTextField = valueText;
                cbo.DataBind();
            }
            else
            {
                if (isHeader)
                {
                    DataRow dr = dtSource.NewRow();
                    dr[valueText] = ChooseText;
                    dtSource.Rows.InsertAt(dr, 0);
                }
                cbo.DataSource = dtSource;
                cbo.DataValueField = valueFlied;
                cbo.DataTextField = valueText;
                cbo.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
      
}