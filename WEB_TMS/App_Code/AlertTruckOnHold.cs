﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OracleClient;
using System.Web.UI;
using System.Data;
using System.Text;
using System.Configuration;
using System.Globalization;

/// <summary>
/// Summary description for AlertTruckOnHold
/// </summary>
public class AlertTruckOnHold
{
    public static OracleConnection conn;
    public static Page page;
    public AlertTruckOnHold(Page _page, OracleConnection _conn)
    {
        //
        // TODO: Add constructor logic here
        //
        page = _page; conn = _conn;
    }
    public AlertTruckOnHold(OracleConnection _conn)
    {
        //
        // TODO: Add constructor logic here
        //
        conn = _conn;
    }

    #region Data
    private string _VehID;
    private string _VehNo;
    private string _TUID;
    private string _TUNo;
    private string _Subject;
    private string _Message;
    private DateTime _EventDate;
    #endregion

    #region Property
    /// <summary>
    /// รหัสหัว
    /// </summary>
    public string VehID
    {
        get { return this._VehID; }
        set { this._VehID = value; }
    }
    /// <summary>
    /// ทะเบียนหัว
    /// </summary>
    public string VehNo
    {
        get { return this._VehNo; }
        set { this._VehNo = value; }
    }
    /// <summary>
    /// รหัสหาง
    /// </summary>
    public string TUID
    {
        get { return this._TUID; }
        set { this._TUID = value; }
    }
    /// <summary>
    /// ทะเบียนหาง
    /// </summary>
    public string TUNo
    {
        get { return this._TUNo; }
        set { this._TUNo = value; }
    }
    /// <summary>
    /// เนื่องจาก/สาเหตุ
    /// </summary>
    public string Subject
    {
        get { return this._Subject; }
        set { this._Subject = value; }
    }
    /// <summary>
    /// ข้อความ
    /// </summary>
    public string Message
    {
        get { return this._Message; }
        set { this._Message = value; }
    }
    /// <summary>
    /// วันที่เกิดเหตุ
    /// </summary>
    public DateTime EventDate
    {
        get { return this._EventDate; }
        set { this._EventDate = value; }
    }

    #endregion

    public string[] listPlan_do()
    {
        string Qry_Plan = @"SELECT   PLN.SVENDORID,PLN.SHEADREGISTERNO ,PLN.STRAILERREGISTERNO,PLN.DDELIVERY , PLN.CCONFIRM ,PLNLST.SDELIVERYNO , USR.SFIRSTNAME,USR.SLASTNAME,usr.SEMAIL
FROM TPLANSCHEDULE PLN
LEFT JOIN TPLANSCHEDULELIST PLNLST ON PLN.NPLANID = PLNLST.NPLANID
LEFT JOIN TUSER USR ON NVL(PLN.SCREATE,'guess')=cast(USR.SUID as varchar2(50))
WHERE ( PLN.SHEADREGISTERNO ='" + _VehNo + "'  OR '" + _TUNo + @"'=PLN.STRAILERREGISTERNO)
AND TRUNC( PLN.DDELIVERY) >= TRUNC(SYSDATE) AND TRUNC( PLN.DDELIVERY) <=TRUNC(SYSDATE)+7
GROUP BY  PLN.SVENDORID,PLN.SHEADREGISTERNO ,PLN.STRAILERREGISTERNO,PLN.DDELIVERY , PLN.CCONFIRM ,PLNLST.SDELIVERYNO, USR.SFIRSTNAME,USR.SLASTNAME ,usr.SEMAIL
ORDER BY PLN.DDELIVERY ASC";
        DataTable _dt = CommonFunction.Get_Data(conn, Qry_Plan);
        string OP = "", PLAN_Mail = "", VEN_ID = "";
        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            if (con.State == ConnectionState.Closed) con.Open();
            foreach (DataRow _dr in _dt.Rows)
            {
                //OracleCommand ora_cmd = new OracleCommand("UPDATE TPLANSCHEDULELIST SET CACTIVE='0' WHERE SDELIVERYNO='" + _dr["SDELIVERYNO"] + "' --AND NVL(SHEADID,'TRxxxxxxx')='" + _dr["VEH_ID"] + "' AND STRAILERID='" + _dr["TU_ID"] + @"'", con);
                OracleCommand ora_cmd = new OracleCommand("UPDATE TPLANSCHEDULELIST SET CACTIVE='0' WHERE SDELIVERYNO='" + _dr["SDELIVERYNO"] + "'", con);
                ora_cmd.ExecuteNonQuery();

                OP += "^[" + (_dt.Rows.IndexOf(_dr) + 1) + ".]:[" + _dr["SHEADREGISTERNO"] + "-" + _dr["STRAILERREGISTERNO"] + "]:[" + _dr["DDELIVERY"] + "]:[" + _dr["SDELIVERYNO"] + "]:[" + _dr["CCONFIRM"] + "]";
                PLAN_Mail += (_dr["SEMAIL"] + "" != "") ? "," + _dr["SEMAIL"] : "";
                VEN_ID = "" + _dr["SVENDORID"];

            }
            //UPDATE Master record when inactive child's rows
            OracleCommand cmd = new OracleCommand("UDT_INACTIVE_PLAN", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();

        }
        string[] OP_ARAY = { "", "", "" };
        OP_ARAY[0] = OP;
        OP_ARAY[1] = ((PLAN_Mail.Length > 1) ? PLAN_Mail.Remove(0, 1) : "");
        OP_ARAY[2] = VEN_ID;
        return OP_ARAY;
    }
    public string listConfirm_Car()
    {
        string Qry_VEH = @"SELECT CONF.DDATE  ,CONF.DDATE+1 Confirm_Date, CONF.CCONFIRM ,LST.SHEADREGISTERNO ,LST.STRAILERREGISTERNO ,VEH.STRUCKID VEH_ID,TU.STRUCKID TU_ID
FROM TTRUCKCONFIRMLIST LST
LEFT JOIN TTRUCKCONFIRM CONF ON CONF.NCONFIRMID=LST.NCONFIRMID
LEFT JOIN TTRUCK VEH ON VEH.STRUCKID=LST.SHEADID
LEFT JOIN TTRUCK TU ON TU.STRUCKID=LST.STRAILERID
WHERE (VEH.SHEADREGISTERNO='" + _VehNo + "' OR  '" + _TUNo + @"'=TU.SHEADREGISTERNO)
AND TRUNC(DDATE)+1 >= TRUNC(SYSDATE) AND TRUNC(DDATE)+1 <=TRUNC(SYSDATE)+1
GROUP By CONF.DDATE , CONF.CCONFIRM ,LST.SHEADREGISTERNO ,LST.STRAILERREGISTERNO,VEH.STRUCKID ,TU.STRUCKID
ORDER BY CONF.DDATE ASC
 
";
        DataTable _dt = CommonFunction.Get_Data(conn, Qry_VEH);
        string OP = "", Exec_Confirm_Car = "";
        foreach (DataRow _dr in _dt.Rows)
        {
            using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();
                OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCKCONFIRMLIST SET CCONFIRM='0' ,SUPDATE='ALT_TMS4',DUPDATE=sysdate WHERE NCONFIRMID IN(  SELECT NCONFIRMID FROM TTRUCKCONFIRM WHERE DDATE = TO_DATE('" + Convert.ToDateTime(_dr["DDATE"] + "").ToString("MM/dd/yyyy", new CultureInfo("en-US")) + "','MM/dd/yyyy') ) AND NVL(SHEADID,'TRxxxxxxx')='" + _dr["VEH_ID"] + "' --AND STRAILERID='" + _dr["TU_ID"] + @"'", con);
                ora_cmd.ExecuteNonQuery();
            }
            OP += "^[" + (_dt.Rows.IndexOf(_dr) + 1) + ".]:[" + _dr["SHEADREGISTERNO"] + "-" + _dr["STRAILERREGISTERNO"] + "]:[" + _dr["Confirm_Date"] + "]:[" + _dr["CCONFIRM"] + "]";
        }

        return OP;
    }
    public string SendTo()
    {
        string[] PLAN = listPlan_do();
        string CAR = listConfirm_Car();
        string message = "";
        string VND_MAIL = ""
            , PLAN_MAIL = ""
            , DEPOT_MAIL = "";
        #region Set PLANNER MAIL
        PLAN_MAIL = ((PLAN[1] != "") ? "" + PLAN[1] : "");
        #endregion
        #region Set Vendor Mail
        if (_VehNo != "")
        {
            VND_MAIL = CommonFunction.Get_Value(conn, @"SELECT LTRIM(WM_CONCAT(DISTINCT SEMAIL),',')
FROM TUSER USR
LEFT JOIN TPERMISSION PRMS ON USR.SUID=PRMS.SUID
WHERE SVENDORID = (SELECT DISTINCT SVENDORID FROM TCONTRACT 
LEFT JOIN TCONTRACT_TRUCK ON TCONTRACT.SCONTRACTID=TCONTRACT_TRUCK.SCONTRACTID
WHERE TCONTRACT.CACTIVE = 'Y' AND STRUCKID IN (SELECT STRUCKID FROM TTRUCK WHERE SHEADREGISTERNO='" + _VehNo + "'))  AND NVL(MAIL2ME,'0')='1'"); // Update by Kiattisak 15/07/2558
            VND_MAIL = VND_MAIL.Replace(',', ';'); // Update by Kiattisak 15/07/2558
        }
        #endregion
        #region Set DEPOT_MAIL

        DEPOT_MAIL = CommonFunction.Get_Value(conn, @"SELECT LTRIM(WM_CONCAT(DISTINCT USR.SEMAIL),',') DEPOT_MAIL
--CONT.SCONTRACTID ,NVL(CONT.STERMINALID,SUBSTR(CONT.SCONT_PLNT,2,4)) STERMINALID ,CONT_PLANT.*
FROM TCONTRACT_TRUCK CONTTRCK
LEFT JOIN TCONTRACT CONT ON CONTTRCK.SCONTRACTID=CONT.SCONTRACTID
--LEFT JOIN TCONTRACT_TERMINAL CONT_PLANT ON CONTTRCK.SCONTRACTID=CONT_PLANT.SCONTRACTID
LEFT JOIN TTRUCK VEH ON CONTTRCK.STRUCKID = VEH.STRUCKID
LEFT JOIN  TTRUCK TU ON CONTTRCK.STRAILERID = TU.STRUCKID
LEFT JOIN TUSER USR  ON NVL(CONT.STERMINALID,SUBSTR(CONT.SCONT_PLNT,2,4)) =USR.SVENDORID
LEFT JOIN TPERMISSION PRMS ON USR.SUID=PRMS.SUID
WHERE ( VEH.SHEADREGISTERNO ='" + _VehNo + "'  OR '" + _TUNo + @"'=TU.SHEADREGISTERNO)
 AND NVL(MAIL2ME,'0')='1'"); // Update by Kiattisak 15/07/2558

        DEPOT_MAIL = DEPOT_MAIL.Replace(',', ';'); // Update by Kiattisak 15/07/2558
        #endregion
        StringBuilder str_bld = new StringBuilder();
        str_bld.AppendLine("เรียน ผู้เกี่ยวข้อง");
        str_bld.AppendLine("เรื่อง การห้ามวิ่ง รถทะเบียน " + _VehNo + " " + _TUNo);
        str_bld.AppendLine(@"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จากรถทะเบียน" + _VehNo + " " + _TUNo + @" ถูกห้ามวิ่ง เนื่องจาก " + _Subject);
        if ((PLAN[0] != "") || (CAR != ""))
        {
            str_bld.AppendLine("โดยมีผลกระทบ ดังรายละเอียดดังนี้");
        }
        #region แผนงาน
        if (PLAN[0] != "")
        {
            str_bld.AppendLine("# แผนงาน  ");
            string PLN_MSG = @"<table><tr><td>@</td><td>ทะเบียน</td><td>วันที่ขนส่ง</td><td>DO</td><td>Ststus</td></tr>{0}</table>"
                , stringDETAIL_MSG = ""
            , DETAIL_MSG_4MAT = @"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>"
            , DETAIL_MSG_Dflt = @"<tr><td colspal=5 >no data</td></tr>";

            foreach (string str_aray in PLAN[0].Split('^'))
            {
                if (str_aray != "")
                {
                    string[] data_aray = ("]:" + str_aray + ":[").Replace("]:[", "|").Split('|');
                    stringDETAIL_MSG += string.Format(DETAIL_MSG_4MAT, "" + data_aray[1], "" + data_aray[2], "" + data_aray[3], "" + data_aray[4], "" + data_aray[5]);
                }
            }
            str_bld.AppendLine(string.Format(PLN_MSG, ((stringDETAIL_MSG != "") ? stringDETAIL_MSG : DETAIL_MSG_Dflt)));
        }
        #endregion
        #region การยืนยันรถตามสัญญา

        if (CAR != "")
        {
            str_bld.AppendLine("# การยืนยันรถตามสัญญา ");
            string PLN_MSG = @"<table><tr><td>@</td><td>ทะเบียน</td><td>ประจำวันที</td><td>Ststus</td></tr>{0}</table>"
                , DETAIL_MSG_4MAT = @"<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>"
                , stringDETAIL_MSG = ""
            , DETAIL_MSG_Dflt = @"<tr><td colspal=4 >no data</td></tr>";
            foreach (string str_aray in CAR.Split('^'))
            {
                if (str_aray != "")
                {
                    string[] data_aray = ("]:" + str_aray + ":[").Replace("]:[", "|").Split('|');
                    stringDETAIL_MSG += string.Format(DETAIL_MSG_4MAT, "" + data_aray[1], "" + data_aray[2], "" + data_aray[3], "" + data_aray[4]);
                }
            }
            str_bld.AppendLine(string.Format(PLN_MSG, ((stringDETAIL_MSG != "") ? stringDETAIL_MSG : DETAIL_MSG_Dflt)));
        }
        #endregion
        str_bld.AppendLine("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อทราบและดำเนินการแก้ไขในส่วนการทำงานที่เกี่ยวข้อง");
        string _frm = "" + ConfigurationSettings.AppSettings["SystemMail"], _to = "", _subj = "แจ้งสถานะ " + _VehNo + "-" + _TUNo + " ห้ามวิ่ง";


        if (ConfigurationSettings.AppSettings["usemail"] + "" == "1")
        {
            _to += (VND_MAIL != "") ? "" + VND_MAIL : "";
            _to += (PLAN_MAIL != "") ? (((_to != "") ? ";" : "") + PLAN_MAIL) : "";
            _to += (DEPOT_MAIL != "") ? (((_to != "") ? ";" : "") + DEPOT_MAIL) : "";
        }
        else
        {
            str_bld.AppendLine("Vendor : " + VND_MAIL);
            str_bld.AppendLine("Planner : " + PLAN_MAIL);
            str_bld.AppendLine("Depot : " + DEPOT_MAIL);
            _to = ConfigurationSettings.AppSettings["demoMailRecv"] + "";
        }
        //bool IsSended = CommonFunction.SendNetMail(_frm, _to, subject, str_bld.ToString(), conn, ""  , "", _frm, "", _to, "", "0", _Subject, str_bld.ToString());
        message = str_bld.ToString();
        bool IsSended = CommonFunction.SendMail(_frm, _to, _subj, str_bld.ToString().Replace("\r\n", "<br>"), "");

        string Cartype = "0";
        if (!string.IsNullOrEmpty(_TUNo))
        {
            Cartype = "3";
        }

        string SAP_DESCRIPTION = ((IsSended) ? "S" : "E") + ":" + Lock_SAP(_VehID, Cartype);

        if (IsSended)
        {//SUSERSEND ,SUSERRECIVE ,SREFID ,CSTATUS
            string sXcute = @"INSERT INTO TLOGEMAIL (  DLOG, DSEND,  SUSERSEND, SEMAILFROM, SUSERRECIVE,  SEMAILTO, SREFID, CSTATUS,  SSUBJECT, SMESSAGE,SENDCOMPLETE) 
VALUES ( SYSDATE, SYSDATE,'{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}','T' ) ";
            if (message.Length > 4000)
            {
                message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "");

            }
            else
            {
                message = message.Replace("'", "");
            }



            sXcute = string.Format(sXcute, "", "", "" + _frm, "", "" + _to, "", "", "" + _subj, "" + message);
            OracleCommand com = new OracleCommand(sXcute, conn);
            com.ExecuteNonQuery();
        }
        else
        {
            //            string sXcute = @"INSERT INTO TLOGEMAIL (  DLOG, DSEND,  SUSERSEND, SEMAILFROM, SUSERRECIVE,  SEMAILTO, SREFID, CSTATUS,  SSUBJECT, SMESSAGE,SENDCOMPLETE,SERROR) 
            //VALUES ( SYSDATE, SYSDATE,'{1}','{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', 'F' ,'ELSE OF ELSE') ";
            //            if (message.Length > 4000)
            //            {
            //                message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "");

            //            }
            //            else
            //            {
            //                message = message.Replace("'", "");
            //            }
            //            SAP_DESCRIPTION = SAP_DESCRIPTION.Length > 4000 ? message = message.Replace("'", "").Replace(" ", "").Replace("&nbsp;", "") : SAP_DESCRIPTION;

            //            sXcute = string.Format(sXcute, "", "", "" + _frm, "", "" + _to, "", "", "" + _subj, "" + SAP_DESCRIPTION);
            //            OracleCommand com = new OracleCommand(sXcute, conn);
            //            com.ExecuteNonQuery();
        }

        return (IsSended) ? "S" : "E";
    }

    public string Lock_SAP(string STRUCK, string ScarType)
    {
        string msg = "";
        string sMsg = "";

        //string VEH_NO = CommonFunction.Get_Value(new OracleConnection(conn), "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");

        DataTable dt_VEH = CommonFunction.Get_Data(conn, "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");
        string VEH_NO = dt_VEH.Rows.Count > 0 ? dt_VEH.Rows[0]["SHEADREGISTERNO"] + "" : "";
        if (VEH_NO.Trim() != "") // cSAP_SYNC = 1 ให้มีการ Syncข้อมูลไปยัง SAP
        {
            SAP_SYNCOUT_SI veh_syncout = new SAP_SYNCOUT_SI();
            veh_syncout.VEH_NO = VEH_NO + "";
            sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
            if (sMsg != "")
            {
                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                //กรณีที่มีการอัพเดทข้อมูลรถ แต่รถยังไม่มีใน SAP
                if (sMsg.Contains("does not exist"))
                {

                    //กรณีที่หัวไม่มีใน SAP
                    if (sMsg.Contains("Entry " + VEH_NO + " does not exist"))
                    {
                        //รถบรรทุก
                        if (ScarType == "0")
                        {
                            veh_create.sVehicle = VEH_NO + "";
                            sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                            if (sMsg.Contains("Transport unit " + VEH_NO + " already exists"))
                            {
                                veh_create.sVehicle = VEH_NO + "";
                                sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                                if (sMsg == "")
                                {
                                    sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                                }
                            }
                        }
                        else
                        {
                            veh_create.sVehicle = VEH_NO + "";
                            sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                            if (sMsg == "")
                            {
                                sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                            }
                        }
                        //ส่งรถไปอัพเดท
                        veh_syncout.VEH_NO = VEH_NO + "";
                        sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    }
                    else
                    {
                        veh_create.sVehicle = VEH_NO + "";
                        sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                        if (sMsg == "")
                        {
                            sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                        }
                        //ส่งรถไปอัพเดท
                        veh_syncout.VEH_NO = VEH_NO + "";
                        sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    }
                }
                else
                {
                    veh_create.sVehicle = VEH_NO + "";
                    sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                    if (sMsg == "")
                    {
                        sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    }
                    //ส่งรถไปอัพเดท
                    veh_syncout.VEH_NO = VEH_NO + "";
                    sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                }
            }

            SystemFunction.SynSapError("LockCar_SAP", sMsg);


        }

        return sMsg;
    }
}