﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.OracleClient;
//using SAP.Middleware.Connector;

/// <summary>
/// Summary description for FunctionUtilities
/// </summary>
public class FunctionUtilities
{

    //public static DataTable GetDataTableFromRFCTable(IRfcTable myrfcTable)
    //{
    //    DataTable loTable = new DataTable();
    //    int liElement = 0;
    //    for (liElement = 0; liElement <= myrfcTable.ElementCount - 1; liElement++)
    //    {
    //        RfcElementMetadata metadata = myrfcTable.GetElementMetadata(liElement); loTable.Columns.Add(metadata.Name);
    //    }
    //    foreach (IRfcStructure Row in myrfcTable)
    //    {
    //        DataRow ldr = loTable.NewRow();
    //        for (liElement = 0; liElement <= myrfcTable.ElementCount - 1; liElement++)
    //        {
    //            RfcElementMetadata metadata = myrfcTable.GetElementMetadata(liElement);
    //            ldr[metadata.Name] = Row.GetString(metadata.Name);
    //        }
    //        loTable.Rows.Add(ldr);
    //    } return loTable;
    //}
    public static DataTable Get_Data(OracleConnection Conn, string _sql)
    {

        DataTable _dt = new DataTable();
        if (Conn.State == ConnectionState.Closed) Conn.Open();
        new OracleDataAdapter(_sql, Conn).Fill(_dt);
        return _dt;

    }

    public static string ConvertToDateTime(string _date, string _Format, string _ToCul, bool ToUTC)
    {
        string datetime = "";
        DateTime expectedDate = Convert.ToDateTime(_date);
        _ToCul = (ToUTC && expectedDate.Year > 2500) ? "en-US" : _ToCul;
        datetime = expectedDate.ToString(_Format, new System.Globalization.CultureInfo(_ToCul));
        //string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "yyyy/MM/dd", "yyyy-MM-dd" };
        //
        //if (!DateTime.TryParseExact(_date, formats, new CultureInfo(_ToCul), DateTimeStyles.None, out expectedDate))
        //{
        //    Console.Write("Thank you Mario, but the DateTime is in another format.");
        //}
        return datetime;
    }
}