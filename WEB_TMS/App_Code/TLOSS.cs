using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for TLOSS
/// </summary>
public class TLOSS
{
    public static OracleConnection conn;
    public static Page page;
    //public TLOSS() { }
    public TLOSS(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }
    #region Data
    private string fSLOSSID;
    private string fDELIVERY_NO;
    private string fSHIPMENT_NO;
    private string fTRUCK_ID;
    private string fTRANS_ID;
    private string fTRANS_TYPE;
    private string fCONTRACT_ID;
    private string fREGISTER_NUM;
    private string fDDELIVERY;
    private string fSPRODUCTID;
    private string fNORDERVOLUMN;
    private string fNLOSSVOLUMN;
    private string fSFROMTERMINAL;
    private string fSTOTERMINAL;
    private string fSSHIPTO;
    private string fDCREATE;
    private string fSCREATE;
    private string fDUPDATE;
    private string fSUPDATE;
    private string fSCHECKID;
    private string fNPLANID;
    private string fNPLANLISTID;
    private string fNPRODUCTUNITID;
    #endregion
    #region Property
    public string SLOSSID
    {
        get { return this.fSLOSSID; }
        set { this.fSLOSSID = value; }
    }
    public string DELIVERY_NO
    {
        get { return this.fDELIVERY_NO; }
        set { this.fDELIVERY_NO = value; }
    }
    public string SHIPMENT_NO
    {
        get { return this.fSHIPMENT_NO; }
        set { this.fSHIPMENT_NO = value; }
    }
    public string TRUCK_ID
    {
        get { return this.fTRUCK_ID; }
        set { this.fTRUCK_ID = value; }
    }
    public string TRANS_ID
    {
        get { return this.fTRANS_ID; }
        set { this.fTRANS_ID = value; }
    }
    public string TRANS_TYPE
    {
        get { return this.fTRANS_TYPE; }
        set { this.fTRANS_TYPE = value; }
    }
    public string CONTRACT_ID
    {
        get { return this.fCONTRACT_ID; }
        set { this.fCONTRACT_ID = value; }
    }
    public string REGISTER_NUM
    {
        get { return this.fREGISTER_NUM; }
        set { this.fREGISTER_NUM = value; }
    }
    public string DDELIVERY
    {
        get { return this.fDDELIVERY; }
        set { this.fDDELIVERY = value; }
    }
    public string SPRODUCTID
    {
        get { return this.fSPRODUCTID; }
        set { this.fSPRODUCTID = value; }
    }
    public string NORDERVOLUMN
    {
        get { return this.fNORDERVOLUMN; }
        set { this.fNORDERVOLUMN = value; }
    }
    public string NLOSSVOLUMN
    {
        get { return this.fNLOSSVOLUMN; }
        set { this.fNLOSSVOLUMN = value; }
    }
    public string SFROMTERMINAL
    {
        get { return this.fSFROMTERMINAL; }
        set { this.fSFROMTERMINAL = value; }
    }
    public string STOTERMINAL
    {
        get { return this.fSTOTERMINAL; }
        set { this.fSTOTERMINAL = value; }
    }
    public string SSHIPTO
    {
        get { return this.fSSHIPTO; }
        set { this.fSSHIPTO = value; }
    }
    public string DCREATE
    {
        get { return this.fDCREATE; }
        set { this.fDCREATE = value; }
    }
    public string SCREATE
    {
        get { return this.fSCREATE; }
        set { this.fSCREATE = value; }
    }
    public string DUPDATE
    {
        get { return this.fDUPDATE; }
        set { this.fDUPDATE = value; }
    }
    public string SUPDATE
    {
        get { return this.fSUPDATE; }
        set { this.fSUPDATE = value; }
    }
    public string SCHECKID
    {
        get { return this.fSCHECKID; }
        set { this.fSCHECKID = value; }
    }
    public string NPLANID
    {
        get { return this.fNPLANID; }
        set { this.fNPLANID = value; }
    }
    public string NPLANLISTID
    {
        get { return this.fNPLANLISTID; }
        set { this.fNPLANLISTID = value; }
    }
    public string NPRODUCTUNITID
    {
        get { return this.fNPRODUCTUNITID; }
        set { this.fNPRODUCTUNITID = value; }
    }
    #endregion
    #region Method
    public int Insert()
    {
        int result = -1;
        try
        {
        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        //
        OracleCommand cmd = new OracleCommand("IUTLOSS", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("S_LOSSID", SLOSSID);
        cmd.Parameters.AddWithValue("S_DELIVERY_NO", DELIVERY_NO);
        cmd.Parameters.AddWithValue("S_SHIPMENT_NO", SHIPMENT_NO);
        cmd.Parameters.AddWithValue("S_TRUCK_ID", TRUCK_ID);
        cmd.Parameters.AddWithValue("S_TRANS_ID", TRANS_ID);
        cmd.Parameters.AddWithValue("S_TRANS_TYPE", TRANS_TYPE);
        cmd.Parameters.AddWithValue("S_CONTRACT_ID", CONTRACT_ID);
        cmd.Parameters.AddWithValue("S_REGISTER_NUM", REGISTER_NUM);
        cmd.Parameters.AddWithValue("D_DELIVERY", Convert.ToDateTime(DDELIVERY));
        cmd.Parameters.AddWithValue("S_PRODUCTID", SPRODUCTID);
        cmd.Parameters.AddWithValue("N_ORDERVOLUMN", NORDERVOLUMN);
        cmd.Parameters.AddWithValue("N_LOSSVOLUMN", NLOSSVOLUMN);
        cmd.Parameters.AddWithValue("S_FROMTERMINAL", SFROMTERMINAL);
        cmd.Parameters.AddWithValue("S_TOTERMINAL", STOTERMINAL);
        cmd.Parameters.AddWithValue("S_SHIPTO", SSHIPTO);
        cmd.Parameters.AddWithValue("D_CREATE", DCREATE);
        cmd.Parameters.AddWithValue("S_CREATE", SCREATE);
        cmd.Parameters.AddWithValue("D_UPDATE", DUPDATE);
        cmd.Parameters.AddWithValue("S_UPDATE", SUPDATE);
        cmd.Parameters.AddWithValue("S_CHECKID", SCHECKID);
        cmd.Parameters.AddWithValue("N_PLANID", NPLANID);
        cmd.Parameters.AddWithValue("N_PLANLISTID", NPLANLISTID);
        cmd.Parameters.AddWithValue("N_PRODUCTUNITID", NPRODUCTUNITID);

        result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public int Update()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("UTLOSS", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SLOSSID", SLOSSID);
            cmd.Parameters.AddWithValue("@DELIVERY_NO", DELIVERY_NO);
            cmd.Parameters.AddWithValue("@SHIPMENT_NO", SHIPMENT_NO);
            cmd.Parameters.AddWithValue("@TRUCK_ID", TRUCK_ID);
            cmd.Parameters.AddWithValue("@TRANS_ID", TRANS_ID);
            cmd.Parameters.AddWithValue("@TRANS_TYPE", TRANS_TYPE);
            cmd.Parameters.AddWithValue("@CONTRACT_ID", CONTRACT_ID);
            cmd.Parameters.AddWithValue("@REGISTER_NUM", REGISTER_NUM);
            cmd.Parameters.AddWithValue("@DDELIVERY", DDELIVERY);
            cmd.Parameters.AddWithValue("@SPRODUCTID", SPRODUCTID);
            cmd.Parameters.AddWithValue("@NORDERVOLUMN", NORDERVOLUMN);
            cmd.Parameters.AddWithValue("@NLOSSVOLUMN", NLOSSVOLUMN);
            cmd.Parameters.AddWithValue("@SFROMTERMINAL", SFROMTERMINAL);
            cmd.Parameters.AddWithValue("@STOTERMINAL", STOTERMINAL);
            cmd.Parameters.AddWithValue("@SSHIPTO", SSHIPTO);
            cmd.Parameters.AddWithValue("@DCREATE", DCREATE);
            cmd.Parameters.AddWithValue("@SCREATE", SCREATE);
            cmd.Parameters.AddWithValue("@DUPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("@SUPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("@SCHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("@NPLANID", NPLANID);
            cmd.Parameters.AddWithValue("@NPLANLISTID", NPLANLISTID);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public int Delete()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("DTLOSS", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public bool Open()
    {
        string sqlstr;
        bool result = false;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            sqlstr = "SELECT * FROM TLOSS ";
            DataTable dt = new DataTable();
            new OracleDataAdapter(sqlstr, conn).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                SLOSSID = row["SLOSSID"].ToString();
                DELIVERY_NO = row["DELIVERY_NO"].ToString();
                SHIPMENT_NO = row["SHIPMENT_NO"].ToString();
                TRUCK_ID = row["TRUCK_ID"].ToString();
                TRANS_ID = row["TRANS_ID"].ToString();
                TRANS_TYPE = row["TRANS_TYPE"].ToString();
                CONTRACT_ID = row["CONTRACT_ID"].ToString();
                REGISTER_NUM = row["REGISTER_NUM"].ToString();
                DDELIVERY = row["DDELIVERY"].ToString();
                SPRODUCTID = row["SPRODUCTID"].ToString();
                NORDERVOLUMN = row["NORDERVOLUMN"].ToString();
                NLOSSVOLUMN = row["NLOSSVOLUMN"].ToString();
                SFROMTERMINAL = row["SFROMTERMINAL"].ToString();
                STOTERMINAL = row["STOTERMINAL"].ToString();
                SSHIPTO = row["SSHIPTO"].ToString();
                DCREATE = row["DCREATE"].ToString();
                SCREATE = row["SCREATE"].ToString();
                DUPDATE = row["DUPDATE"].ToString();
                SUPDATE = row["SUPDATE"].ToString();
                SCHECKID = row["SCHECKID"].ToString();
                NPLANID = row["NPLANID"].ToString();
                NPLANLISTID = row["NPLANLISTID"].ToString();
                dt.Dispose();
                result = true;
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    #endregion
}
