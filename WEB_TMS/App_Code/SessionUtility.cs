﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using TMS_Entity;
public static class SessionUtility
{
    public static void SetUserProfileSession(UserProfile user_profile)
    {
        try
        {
            if (System.Web.HttpContext.Current.Session["USER_PROFILE"] == null)
            {
                HttpContext.Current.Session.Add("USER_PROFILE", user_profile);
            }
            else
            {
                HttpContext.Current.Session["USER_PROFILE"] = user_profile;
            }

        }
        catch
        {
            
        }
    }
    public static UserProfile GetUserProfileSession()
    {
        try
        {     
            UserProfile user_profile = new UserProfile() { 
                USER_ID = System.Web.HttpContext.Current.Session["UserID"].ToString(),
                SVDID = System.Web.HttpContext.Current.Session["SVDID"].ToString(),
                CGROUP = System.Web.HttpContext.Current.Session["CGROUP"].ToString(),
                USERNAME = System.Web.HttpContext.Current.Session["UserName"].ToString(),
                UserGroup = System.Web.HttpContext.Current.Session["UserGroup"].ToString(),
                CHKCHANGEPASSWORD = System.Web.HttpContext.Current.Session["CHKCHANGEPASSWORD"].ToString()
            };
            return user_profile;
        }
        catch
        {
            return null;
        }
    }

    public static void RedirectToLogin()
    {

        System.Web.HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings["LOGON_PAGE"], true);

    }
}


public class GROUP_PERMISSION
{
    public const string VENDOR = "0";
    public const string UNIT = "1";
}
