using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for TCHECKTRUCK
/// </summary>
public class TCHECKTRUCK
{
    public static OracleConnection conn;
    public static OracleTransaction oTransaction;
    public static Page page;
    //public TCHECKTRUCK() { }
    public TCHECKTRUCK(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }
    public TCHECKTRUCK(Page _page, OracleConnection _conn, OracleTransaction _oTransaction) { page = _page; conn = _conn; oTransaction = _oTransaction; }
    public TCHECKTRUCK(OracleConnection _conn, OracleTransaction _oTransaction) { conn = _conn; oTransaction = _oTransaction; }
    #region Data
    private string fSCHECKID;
    private string fDCHECK;
    private string fCGROUPCHECK;
    private string fSCONTRACTID;
    private string fNPLANID;
    private string fSTRUCKID;
    private string fSHEADERREGISTERNO;
    private string fSTRAILERREGISTERNO;
    private string fSREMARK;
    private string fDCREATE;
    private string fSCREATE;
    private string fDUPDATE;
    private string fSUPDATE;
    private string fSTERMINALID;
    private string fCCLEAN;
    private string fCMA;
    private string fDBEGIN_MA;
    private string fDFINAL_MA;
    private string fNDAY_MA;
    private string fCOK;
    private string fIS_YEAR;
    private string fNSURPRISECHECKID;
    private string fPATH_PTTGENFORM;
    #endregion
    #region Property
    public string SCHECKID
    {
        get { return this.fSCHECKID; }
        set { this.fSCHECKID = value; }
    }
    public string DCHECK
    {
        get { return this.fDCHECK; }
        set { this.fDCHECK = value; }
    }
    public string CGROUPCHECK
    {
        get { return this.fCGROUPCHECK; }
        set { this.fCGROUPCHECK = value; }
    }
    public string SCONTRACTID
    {
        get { return this.fSCONTRACTID; }
        set { this.fSCONTRACTID = value; }
    }
    public string NPLANID
    {
        get { return this.fNPLANID; }
        set { this.fNPLANID = value; }
    }
    public string STRUCKID
    {
        get { return this.fSTRUCKID; }
        set { this.fSTRUCKID = value; }
    }
    public string SHEADERREGISTERNO
    {
        get { return this.fSHEADERREGISTERNO; }
        set { this.fSHEADERREGISTERNO = value; }
    }
    public string STRAILERREGISTERNO
    {
        get { return this.fSTRAILERREGISTERNO; }
        set { this.fSTRAILERREGISTERNO = value; }
    }
    public string SREMARK
    {
        get { return this.fSREMARK; }
        set { this.fSREMARK = value; }
    }
    public string DCREATE
    {
        get { return this.fDCREATE; }
        set { this.fDCREATE = value; }
    }
    public string SCREATE
    {
        get { return this.fSCREATE; }
        set { this.fSCREATE = value; }
    }
    public string DUPDATE
    {
        get { return this.fDUPDATE; }
        set { this.fDUPDATE = value; }
    }
    public string SUPDATE
    {
        get { return this.fSUPDATE; }
        set { this.fSUPDATE = value; }
    }
    public string STERMINALID
    {
        get { return this.fSTERMINALID; }
        set { this.fSTERMINALID = value; }
    }
    public string CCLEAN
    {
        get { return this.fCCLEAN; }
        set { this.fCCLEAN = value; }
    }
    public string CMA
    {
        get { return this.fCMA; }
        set { this.fCMA = value; }
    }
    public string DBEGIN_MA
    {
        get { return this.fDBEGIN_MA; }
        set { this.fDBEGIN_MA = value; }
    }
    public string DFINAL_MA
    {
        get { return this.fDFINAL_MA; }
        set { this.fDFINAL_MA = value; }
    }
    public string NDAY_MA
    {
        get { return this.fNDAY_MA; }
        set { this.fNDAY_MA = value; }
    }
    public string COK
    {
        get { return this.fCOK; }
        set { this.fCOK = value; }
    }
    public string IS_YEAR
    {
        get { return this.fIS_YEAR; }
        set { this.fIS_YEAR = value; }
    }
    public string NSURPRISECHECKID
    {
        get { return this.fNSURPRISECHECKID; }
        set { this.fNSURPRISECHECKID = value; }
    }
    #endregion

    #region Method
    public int Insert()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //

            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("IUTCHECKTRUCK", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("IUTCHECKTRUCK", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("S_CHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("D_CHECK", DCHECK);
            cmd.Parameters.AddWithValue("C_GROUPCHECK", CGROUPCHECK);
            cmd.Parameters.AddWithValue("S_CONTRACTID", SCONTRACTID);
            cmd.Parameters.AddWithValue("N_PLANID", NPLANID);
            cmd.Parameters.AddWithValue("S_TRUCKID", STRUCKID);
            cmd.Parameters.AddWithValue("S_HEADERREGISTERNO", SHEADERREGISTERNO);
            cmd.Parameters.AddWithValue("S_TRAILERREGISTERNO", STRAILERREGISTERNO);
            cmd.Parameters.AddWithValue("S_REMARK", SREMARK);
            cmd.Parameters.AddWithValue("D_CREATE", DCREATE);
            cmd.Parameters.AddWithValue("S_CREATE", SCREATE);
            cmd.Parameters.AddWithValue("D_UPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("S_UPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("S_TERMINALID", STERMINALID);
            cmd.Parameters.AddWithValue("C_CLEAN", CCLEAN);
            cmd.Parameters.AddWithValue("C_MA", CMA);
            cmd.Parameters.AddWithValue("D_BEGIN_MA", DBEGIN_MA);
            cmd.Parameters.AddWithValue("D_FINAL_MA", DFINAL_MA);
            cmd.Parameters.AddWithValue("N_DAY_MA", NDAY_MA);
            cmd.Parameters.AddWithValue("C_OK", COK);
            cmd.Parameters.AddWithValue("S_NSURPRISECHECKID", NSURPRISECHECKID);
            if (!string.IsNullOrEmpty(IS_YEAR))
            {
                cmd.Parameters.AddWithValue("S_IS_YEAR", IS_YEAR);
            }
            

            OracleParameter para = new OracleParameter();
            para.ParameterName = "CHECKID";
            para.OracleType = OracleType.Number;
            para.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(para);

            result = cmd.ExecuteNonQuery();

            SCHECKID = cmd.Parameters["CHECKID"].Value.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {

        }
        return result;
    }
    public int Update()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //

            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("UTCHECKTRUCK", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("UTCHECKTRUCK", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SCHECKID", SCHECKID);
            cmd.Parameters.AddWithValue("@DCHECK", DCHECK);
            cmd.Parameters.AddWithValue("@CGROUPCHECK", CGROUPCHECK);
            cmd.Parameters.AddWithValue("@SCONTRACTID", SCONTRACTID);
            cmd.Parameters.AddWithValue("@NPLANID", NPLANID);
            cmd.Parameters.AddWithValue("@STRUCKID", STRUCKID);
            cmd.Parameters.AddWithValue("@SHEADERREGISTERNO", SHEADERREGISTERNO);
            cmd.Parameters.AddWithValue("@STRAILERREGISTERNO", STRAILERREGISTERNO);
            cmd.Parameters.AddWithValue("@SREMARK", SREMARK);
            cmd.Parameters.AddWithValue("@DCREATE", DCREATE);
            cmd.Parameters.AddWithValue("@SCREATE", SCREATE);
            cmd.Parameters.AddWithValue("@DUPDATE", DUPDATE);
            cmd.Parameters.AddWithValue("@SUPDATE", SUPDATE);
            cmd.Parameters.AddWithValue("@STERMINALID", STERMINALID);
            cmd.Parameters.AddWithValue("@CCLEAN", CCLEAN);
            cmd.Parameters.AddWithValue("@CMA", CMA);
            cmd.Parameters.AddWithValue("@DBEGIN_MA", DBEGIN_MA);
            cmd.Parameters.AddWithValue("@DFINAL_MA", DFINAL_MA);
            cmd.Parameters.AddWithValue("@NDAY_MA", NDAY_MA);
            cmd.Parameters.AddWithValue("@COK", COK);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        finally
        {

        }
        return result;
    }
    public int UpdateMA()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //

            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("UTCHECKTRUCK_MA", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("UTCHECKTRUCK_MA", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("S_CHECKID", SCHECKID);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        finally
        {

        }
        return result;
    }
    public int Delete()
    {
        int result = -1;
        try
        {
            // Reopen connection if close

            if (conn.State == ConnectionState.Closed) conn.Open();
            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("DTCHECKTRUCK", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("DTCHECKTRUCK", conn);
            }
            //
            cmd.CommandType = CommandType.StoredProcedure;

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        finally
        {

        }
        return result;
    }
    public bool Open()
    {
        string sqlstr;
        bool result = false;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            sqlstr = "SELECT * FROM TCHECKTRUCK ";
            DataTable dt = new DataTable();
            new OracleDataAdapter(sqlstr, conn).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                SCHECKID = row["SCHECKID"].ToString();
                DCHECK = row["DCHECK"].ToString();
                CGROUPCHECK = row["CGROUPCHECK"].ToString();
                SCONTRACTID = row["SCONTRACTID"].ToString();
                NPLANID = row["NPLANID"].ToString();
                STRUCKID = row["STRUCKID"].ToString();
                SHEADERREGISTERNO = row["SHEADERREGISTERNO"].ToString();
                STRAILERREGISTERNO = row["STRAILERREGISTERNO"].ToString();
                SREMARK = row["SREMARK"].ToString();
                DCREATE = row["DCREATE"].ToString();
                SCREATE = row["SCREATE"].ToString();
                DUPDATE = row["DUPDATE"].ToString();
                SUPDATE = row["SUPDATE"].ToString();
                STERMINALID = row["STERMINALID"].ToString();
                CCLEAN = row["CCLEAN"].ToString();
                CMA = row["CMA"].ToString();
                DBEGIN_MA = row["DBEGIN_MA"].ToString();
                DFINAL_MA = row["DFINAL_MA"].ToString();
                NDAY_MA = row["NDAY_MA"].ToString();
                COK = row["COK"].ToString();
                dt.Dispose();
                result = true;
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        finally
        {

        }
        return result;
    }
    #endregion
}
