﻿using System;
using System.Web;
using System.Data;
using System.Text;
using System.IO;
using System.Data.OracleClient;


/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    OracleConnection connection;
    OracleCommand command;
    OracleDataReader reader;
    StringBuilder query;

    int isComplete = -1;
    public int IsComplete { get { return isComplete; } }

    string tableName = "";
    public string TableName { set { tableName = value; } }

    public DAL(OracleConnection conn)
    {
        connection = conn;
        command = new OracleCommand();
        query = new StringBuilder();
    }

    public void SetValue(string parameter, object value)
    {
        if (value == null)
        {
            command.Parameters.AddWithValue(parameter, DBNull.Value);
        }
        else
        {
            if (value.GetType().FullName.Equals("System.String") && string.IsNullOrEmpty(((string)value).Trim())) { command.Parameters.AddWithValue(parameter, DBNull.Value); return; }

            command.Parameters.AddWithValue(parameter, value);
        }
    }
    public void ClearParameter()
    {
        command.Parameters.Clear();
    }
    public void Insert()
    {
        query.Remove(0, query.Length);

        string fields = "";
        string value = "";

        foreach (OracleParameter parameter in command.Parameters)
        {
            fields += "," + parameter.ParameterName.Remove(0, 1);
            value += "," + parameter.ParameterName;
        }
        if (fields.Length > 0) fields = fields.Remove(0, 1);
        if (value.Length > 0) value = value.Remove(0, 1);

        query.AppendFormat(@"INSERT INTO {0} ({1}) VALUES ({2})", tableName, fields, value);

        this.ExecuteNonQuery();
    }
    public void Update(params string[] whereFields)
    {
        query.Remove(0, query.Length);

        string fields = "";
        string where = "";
        string whereLink = " AND ";

        foreach (OracleParameter parameter in command.Parameters)
        {
            if (Array.IndexOf(whereFields, parameter.ParameterName) != -1)
            {
                // Where Clause
                where += string.Format(@"{2}{0} = {1}", parameter.ParameterName.Remove(0, 1), parameter.ParameterName, whereLink);
            }
            else
            {
                // Normal Clause
                fields += string.Format(@", {0} = {1}", parameter.ParameterName.Remove(0, 1), parameter.ParameterName);
            }
        }
        if (fields.Length > 0) fields = fields.Remove(0, 1);
        if (where.Length > (whereLink.Length - 1)) where = where.Remove(0, whereLink.Length);

        query.AppendFormat(@"UPDATE {0} SET {1} WHERE {2}", tableName, fields, where);

        this.ExecuteNonQuery();
    }
    public void Delete(params string[] whereFields)
    {
        query.Remove(0, query.Length);

        string where = "";
        string whereLink = " AND ";

        foreach (OracleParameter parameter in command.Parameters)
        {
            if (Array.IndexOf(whereFields, parameter.ParameterName) != -1)
            {
                // Where Clause
                where += string.Format(@"{2}{0} = {1}", parameter.ParameterName.Remove(0, 1), parameter.ParameterName, whereLink);
            }
        }
        if (where.Length > (whereLink.Length - 1)) where = where.Remove(0, whereLink.Length);

        query.AppendFormat(@"DELETE FROM {0} WHERE {1}", tableName, where);

        this.ExecuteNonQuery();
    }
    public bool ExistData(string sqlQuery)
    {
        query.Remove(0, query.Length);

        query.Append(sqlQuery);

        DataTable data = this.ExecuteReader();

        return (data != null && data.Rows.Count != 0 && data.Rows[0][0] != DBNull.Value);
    }
    public DataTable SelectMany(string sqlQuery)
    {
        query.Remove(0, query.Length);

        query.Append(sqlQuery);

        return this.ExecuteReader();
    }
    public string SelectOne(string sqlQuery)
    {
        query.Remove(0, query.Length);

        query.Append(sqlQuery);

        return this.ExecuteScalar();
    }
    private string ExecuteScalar()
    {
        try
        {
            if (connection.State == ConnectionState.Closed) connection.Open();

            command.Connection = connection;
            command.CommandText = query.ToString();
            command.CommandType = CommandType.Text;
            reader = command.ExecuteReader();

            DataTable data = new DataTable();
            data.Load(reader);
            return (data.Rows.Count > 0 ? data.Rows[0][0].ToString() : "");
        }
        catch (Exception error)
        {
            this.CheckCreateDirectoryPath();
            this.CleanLogFile();
            this.WriteLogFile(error);
        }
        finally
        {
            connection.Close();
        }
        return null;
    }
    private DataTable ExecuteReader()
    {
        try
        {
            if (connection.State == ConnectionState.Closed) connection.Open();

            command.Connection = connection;
            command.CommandText = query.ToString();
            command.CommandType = CommandType.Text;
            reader = command.ExecuteReader();

            DataTable data = new DataTable();
            data.Load(reader);
            return data;
        }
        catch (Exception error)
        {
            this.CheckCreateDirectoryPath();
            this.CleanLogFile();
            this.WriteLogFile(error);
        }
        finally
        {
            connection.Close();
        }
        return null;
    }
    private void ExecuteNonQuery()
    {
        try
        {
            if (connection.State == ConnectionState.Closed) connection.Open();

            command.Connection = connection;
            command.CommandText = query.ToString();
            command.CommandType = CommandType.Text;
            this.isComplete = command.ExecuteNonQuery();
        }
        catch (Exception error)
        {
            this.isComplete = -1;
            this.CheckCreateDirectoryPath();
            this.CleanLogFile();
            this.WriteLogFile(error);
        }
        finally
        {
            connection.Close();
        }
    }

    private void WriteLogFile(Exception error)
    {
        string codeVersion = "1.0.0";
        string logName = DateTime.Now.ToString("yyyy-MM-dd") + ".log";

        using (System.IO.TextWriter tw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("UploadFile/Log/" + logName), true))
        {
            tw.WriteLine("Time: " + DateTime.Now.ToString("HH:mm:ss"));
            tw.WriteLine("Version Code: " + codeVersion);
            tw.WriteLine("Source: " + error.Source);
            tw.WriteLine("Message: " + error.Message);
            tw.WriteLine("StackTrace: ");
            tw.WriteLine(error.StackTrace);
            tw.WriteLine("Query: " + query.ToString());
            tw.WriteLine("[Parameters:Value]: ");
            foreach (OracleParameter op in command.Parameters)
            {
                tw.WriteLine(op.ParameterName + ":" + op.Value);
            }
            tw.WriteLine("-------");
        }
    }
    private void CleanLogFile()
    {
        int afterDays = 7;

        DirectoryInfo di = new DirectoryInfo(HttpContext.Current.Server.MapPath("UploadFile/Log"));
        FileInfo[] fis = di.GetFiles("*.log");

        foreach (FileInfo fi in fis) if (fi.LastWriteTime < DateTime.Now.AddDays(-afterDays)) fi.Delete();
    }
    void CheckCreateDirectoryPath()
    {
        string[] paths = new string[] { "UploadFile/Log" };
        foreach (string path in paths)
        {
            string checkPath = HttpContext.Current.Server.MapPath(path);
            if (!Directory.Exists(checkPath)) Directory.CreateDirectory(checkPath);
        }
    }
}