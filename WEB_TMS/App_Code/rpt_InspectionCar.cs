﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rpt_InspectionCar
/// </summary>
public class rpt_InspectionCar : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
	private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private ReportHeaderBand ReportHeader;
    private XRLabel xrLabel1;
    private XRLabel xrLabel7;
    private XRLabel xrLabel6;
    private XRLabel lblDate;
    private XRLabel xrLabel4;
    private XRLabel xrLabel3;
    private XRLabel xrLabel2;
    private XRLabel xrLabel13;
    private XRLabel xrLabel12;
    private XRLabel xrLabel11;
    private XRLabel xrLabel10;
    private XRLabel xrLabel9;
    private XRLabel xrLabel8;
    private XRLabel xrLabel17;
    private XRLabel xrLabel18;
    private XRLabel xrLabel19;
    private XRLabel xrLabel14;
    private XRLabel xrLabel15;
    private XRLabel xrLabel16;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell3;
    private XRLabel xrLabel20;
    private XRLabel xrLabel21;
    private XRLabel xrLabel22;
    private XRLabel lblG6_25_1;
    private XRLabel xrLabel25;
    private XRLabel lblG6_26_1;
    private XRLabel xrLabel26;
    private XRLabel xrLabel27;
    private XRCheckBox ckbContractType1;
    private XRCheckBox ckbContractType2;
    private XRCheckBox ckbContractType3;
    private XRCheckBox ckbContractType4;
    private XRLabel xrLabel28;
    private XRLabel xrLabel29;
    private XRLabel xrLabel31;
    private XRLabel xrLabel35;
    private XRLabel xrLabel32;
    private XRLabel xrLabel30;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell4;
    private XRLabel xrLabel33;
    private XRCheckBox ckbG1_6;
    private XRCheckBox ckbG1_5;
    private XRCheckBox ckbG1_4;
    private XRCheckBox ckbG1_3;
    private XRCheckBox ckbG1_2;
    private XRCheckBox ckbG1_1;
    private XRLabel xrLabel34;
    private XRLabel xrLabel36;
    private XRLabel xrLabel37;
    private XRCheckBox ckbG2_2;
    private XRCheckBox ckbG2_1;
    private XRLabel xrLabel43;
    private XRLabel xrLabel41;
    private XRLabel xrLabel40;
    private XRLabel xrLabel39;
    private XRLabel xrLabel38;
    private XRCheckBox ckbG2_9_1;
    private XRCheckBox ckbG2_8_1;
    private XRCheckBox ckbG2_7_1;
    private XRLabel xrLabel45;
    private XRLabel lblG2_10_1;
    private XRLabel xrLabel46;
    private XRLabel lblG2_10_2;
    private XRLabel xrLabel44;
    private XRLabel lblG2_10_3;
    private XRLabel xrLabel52;
    private XRLabel lblG2_11_3;
    private XRLabel xrLabel42;
    private XRLabel xrLabel49;
    private XRLabel lblG2_11_1;
    private XRLabel lblG2_11_2;
    private XRLabel xrLabel51;
    private XRCheckBox ckbG2_12_1;
    private XRLabel lblG3_13_2;
    private XRLabel xrLabel53;
    private XRLabel lblG3_13_1;
    private XRLabel xrLabel48;
    private XRLabel xrLabel47;
    private XRLabel xrLabel55;
    private XRLabel xrLabel59;
    private XRLabel lblG3_14_1;
    private XRLabel xrLabel58;
    private XRLabel xrLabel56;
    private XRLabel xrLabel50;
    private XRLabel lblG3_14_2;
    private XRCheckBox ckbG3_14_3;
    private XRLabel xrLabel67;
    private XRLabel xrLabel68;
    private XRCheckBox ckbG3_16_3;
    private XRLabel lblG3_16_1;
    private XRLabel xrLabel63;
    private XRLabel xrLabel64;
    private XRLabel lblG3_16_2;
    private XRLabel xrLabel62;
    private XRLabel lblG3_15_2;
    private XRLabel xrLabel60;
    private XRLabel lblG3_15_1;
    private XRLabel xrLabel54;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell8;
    private XRLabel xrLabel57;
    private XRLabel lblG4_17_2;
    private XRLabel xrLabel70;
    private XRLabel xrLabel66;
    private XRLabel xrLabel61;
    private XRLabel lblG4_17_1;
    private XRCheckBox ckbG4_1;
    private XRLabel xrLabel73;
    private XRLabel xrLabel74;
    private XRCheckBox ckbG4_18_3;
    private XRLabel lblG4_18_2;
    private XRLabel xrLabel65;
    private XRLabel xrLabel69;
    private XRLabel lblG4_18_1;
    private XRLabel lblG4_19_2;
    private XRLabel xrLabel78;
    private XRCheckBox ckbG4_19_3;
    private XRLabel xrLabel76;
    private XRLabel xrLabel71;
    private XRLabel lblG4_19_1;
    private XRLabel xrLabel75;
    private XRLabel xrLabel80;
    private XRLabel lblG4_20_1;
    private XRLabel xrLabel82;
    private XRLabel xrLabel79;
    private XRLabel lblG4_20_2;
    private XRLabel xrLabel77;
    private XRCheckBox ckbG4_20_3;
    private XRLabel xrLabel72;
    private XRLabel lblG5_21_2;
    private XRLabel xrLabel86;
    private XRLabel xrLabel84;
    private XRLabel xrLabel81;
    private XRLabel lblG5_21_1;
    private XRLabel xrLabel90;
    private XRLabel xrLabel88;
    private XRLabel lblG5_22_1;
    private XRLabel xrLabel87;
    private XRLabel lblG5_22_2;
    private XRLabel xrLabel85;
    private XRCheckBox ckbG5_22_3;
    private XRLabel lblG5_23_2;
    private XRLabel xrLabel93;
    private XRLabel xrLabel91;
    private XRLabel xrLabel83;
    private XRLabel lblG5_23_1;
    private XRLabel xrLabel95;
    private XRLabel lblG5_24_2;
    private XRLabel xrLabel97;
    private XRLabel xrLabel89;
    private XRLabel xrLabel92;
    private XRLabel lblG5_24_1;
    private XRTable xrTable4;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell10;
    private XRLabel xrLabel23;
    private XRLabel xrLabel94;
    private XRLabel lblG7_1;
    private XRLabel xrLabel24;
    private XRCheckBox ckbG7_27_3;
    private XRLabel xrLabel100;
    private XRLabel lblG7_27_2;
    private XRLabel xrLabel98;
    private XRLabel lblG7_27_1;
    private XRLabel xrLabel101;
    private XRLabel lblG7_28_1;
    private XRLabel xrLabel103;
    private XRCheckBox ckbG7_28_3;
    private XRLabel xrLabel96;
    private XRLabel lblG7_28_2;
    private XRCheckBox ckbG7_29_3;
    private XRLabel xrLabel105;
    private XRLabel lblG7_29_2;
    private XRLabel xrLabel99;
    private XRLabel lblG7_29_1;
    private XRLabel xrLabel104;
    private XRCheckBox ckbG7_31_1;
    private XRLabel xrLabel106;
    private XRCheckBox ckbG7_30_1;
    private XRLabel xrLabel102;
    private XRLabel xrLabel107;
    private XRCheckBox ckbG7_32_1;
    private XRCheckBox ckbG7_33_1;
    private XRLabel xrLabel108;
    private XRCheckBox ckbG7_35_1;
    private XRLabel xrLabel110;
    private XRLabel xrLabel109;
    private XRCheckBox ckbG7_34_1;
    private XRLabel xrLabel111;
    private XRLabel xrLabel113;
    private XRCheckBox ckbG10_37_1;
    private XRCheckBox ckbG10_36_1;
    private XRLabel xrLabel112;
    private XRCheckBox ckbG10_39_1;
    private XRLabel xrLabel115;
    private XRCheckBox ckbG10_38_1;
    private XRLabel xrLabel114;
    private XRCheckBox ckbG10_41_1;
    private XRLabel xrLabel117;
    private XRCheckBox ckbG10_40_1;
    private XRLabel xrLabel116;
    private XRCheckBox ckbG10_42_1;
    private XRLabel xrLabel118;
    private XRCheckBox ckbG10_43_1;
    private XRLabel xrLabel119;
    private XRCheckBox ckbG10_44_1;
    private XRLabel xrLabel120;
    private XRCheckBox ckbG10_45_2;
    private XRLabel xrLabel123;
    private XRLabel lblG10_45_1;
    private XRLabel xrLabel121;
    private XRCheckBox ckbG10_46_1;
    private XRLabel xrLabel122;
    private XRCheckBox ckbG10_47_1;
    private XRLabel xrLabel124;
    private XRCheckBox ckbG10_48_1;
    private XRLabel xrLabel125;
    private XRCheckBox ckbG10_49_1;
    private XRLabel xrLabel126;
    private XRCheckBox ckbG10_50_1;
    private XRLabel xrLabel127;
    private XRLabel lblG10_51_1;
    private XRLabel xrLabel128;
    private XRLabel xrLabel129;
    private XRLabel lblG10_52_1;
    private XRLabel xrLabel130;
    private XRCheckBox ckbG10_53_1;
    private XRLabel xrLabel131;
    private XRLabel xrLabel137;
    private XRLabel lblG11_55_1;
    private XRLabel xrLabel135;
    private XRLabel xrLabel134;
    private XRLabel lblG11_54_1;
    private XRLabel xrLabel132;
    private XRLabel xrLabel138;
    private XRLabel lblG11_56_1;
    private XRLabel xrLabel133;
    private XRLabel xrLabel140;
    private XRLabel lblG11_57_1;
    private XRLabel xrLabel136;
    private XRLabel xrLabel142;
    private XRLabel lblG11_58_1;
    private XRLabel xrLabel139;
    private XRLabel xrLabel144;
    private XRLabel lblG11_59_1;
    private XRLabel xrLabel141;
    private XRLabel xrLabel146;
    private XRLabel lblG11_60_1;
    private XRLabel xrLabel143;
    private XRLabel xrLabel148;
    private XRLabel lblG11_61_1;
    private XRLabel xrLabel145;
    private XRLabel xrLabel150;
    private XRLabel lblG11_62_1;
    private XRLabel xrLabel147;
    private XRLabel xrLabel152;
    private XRLabel lblG11_63_1;
    private XRLabel xrLabel149;
    private XRLabel xrLabel151;
    private XRLabel xrLabel157;
    private XRLabel lblG11_64_2;
    private XRLabel xrLabel155;
    private XRLabel xrLabel154;
    private XRLabel lblG11_64_1;
    private XRLabel xrLabel161;
    private XRLabel lblG11_65_2;
    private XRLabel xrLabel159;
    private XRLabel xrLabel158;
    private XRLabel lblG11_65_1;
    private XRLabel xrLabel153;
    private XRTable xrTable5;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell11;
    private XRLabel xrLabel156;
    private XRLabel xrLabel160;
    private XRLine xrLine1;
    private XRLine xrLine3;
    private XRLine xrLine2;
    private XRTable xrTable6;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell12;
    private XRTableCell xrTableCell13;
    private XRLabel xrLabel162;
    private XRLabel xrLabel163;
    private XRLabel xrLabel166;
    private XRLabel xrLabel165;
    private XRLabel xrLabel164;
    private XRLabel xrLabel170;
    private XRLabel xrLabel171;
    private XRLabel xrLabel169;
    private XRLabel xrLabel167;
    private XRLabel xrLabel168;
    private XRLabel xrLabel172;
    private XRLabel xrLabel173;
    private PageFooterBand PageFooter;
    private XRLabel xrLabel174;
    private XRLabel xrLabel175;
    private dsInspectionCar dsInspectionCar1;
    private XRCheckBox ckbG6_26_2;
    private XRCheckBox ckbG6_25_2;
    private DevExpress.XtraReports.Parameters.Parameter sOldDateCheckWater;
    private DevExpress.XtraReports.Parameters.Parameter sBandCar;
    private DevExpress.XtraReports.Parameters.Parameter sSendOilEnd;
    private DevExpress.XtraReports.Parameters.Parameter sSendOilStart;
    private DevExpress.XtraReports.Parameters.Parameter sOpinion;
    private dsInspectionCar dsInspectionCar2;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public rpt_InspectionCar()
	{
		InitializeComponent();
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "rpt_InspectionCar.resx";
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel172 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel166 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel165 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel164 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel163 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel173 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel170 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel171 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel169 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel167 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel168 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
        this.sOpinion = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.ckbG7_35_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_34_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG7_33_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_32_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG7_31_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_30_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_29_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_29_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_29_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_28_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_28_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_28_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG7_27_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_27_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_27_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG7_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_50_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_49_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_48_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_47_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_46_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_45_2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG10_45_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_44_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_43_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_42_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_41_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_40_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_39_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_38_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_37_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG10_36_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG10_51_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG10_52_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG10_53_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel161 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_65_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_65_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_64_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel155 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_64_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel151 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel152 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_63_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_62_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_61_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_60_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel143 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_59_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_58_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_57_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_56_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_55_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG11_54_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_20_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_20_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG4_20_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.lblG4_19_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG4_19_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_19_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG4_18_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.lblG4_18_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_18_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_17_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG4_17_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG4_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_24_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_24_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_23_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_23_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_22_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_22_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_21_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG5_21_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG5_22_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.ckbG6_26_2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG6_25_2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG6_26_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG6_25_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG1_6 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG1_5 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG1_4 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG1_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG1_2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG1_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.ckbG2_12_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG2_11_3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG2_10_3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG2_10_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG2_10_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG2_9_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG2_8_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbG2_7_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG2_2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG2_1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.lblG2_11_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG2_11_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG3_16_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.lblG3_16_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_16_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_15_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_15_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_14_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_14_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_13_2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblG3_13_1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbG3_14_3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
        this.sSendOilEnd = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
        this.sSendOilStart = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.ckbContractType4 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbContractType3 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbContractType2 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.ckbContractType1 = new DevExpress.XtraReports.UI.XRCheckBox();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.sBandCar = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.sOldDateCheckWater = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblDate = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
        this.xrLabel175 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
        this.dsInspectionCar1 = new dsInspectionCar();
        this.dsInspectionCar2 = new dsInspectionCar();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsInspectionCar1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsInspectionCar2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.Detail.HeightF = 0F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.StylePriority.UseFont = false;
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // TopMargin
        // 
        this.TopMargin.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.TopMargin.HeightF = 22F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.StylePriority.UseFont = false;
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.BottomMargin.HeightF = 24.69489F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.StylePriority.UseFont = false;
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel14,
            this.xrLabel15,
            this.xrLabel16,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.lblDate,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
        this.ReportHeader.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.ReportHeader.HeightF = 1756.285F;
        this.ReportHeader.Name = "ReportHeader";
        this.ReportHeader.StylePriority.UseFont = false;
        // 
        // xrTable6
        // 
        this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(12F, 1598.82F);
        this.xrTable6.Name = "xrTable6";
        this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
        this.xrTable6.SizeF = new System.Drawing.SizeF(789.0682F, 157.4647F);
        this.xrTable6.StylePriority.UseBorders = false;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell12});
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Weight = 1D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel172,
            this.xrLabel166,
            this.xrLabel165,
            this.xrLabel164,
            this.xrLabel163,
            this.xrLabel162});
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.StylePriority.UseTextAlignment = false;
        this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell13.Weight = 1.5D;
        // 
        // xrLabel172
        // 
        this.xrLabel172.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel172.LocationFloat = new DevExpress.Utils.PointFloat(125.5F, 122.4647F);
        this.xrLabel172.Name = "xrLabel172";
        this.xrLabel172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel172.SizeF = new System.Drawing.SizeF(125F, 25F);
        this.xrLabel172.StylePriority.UseBorders = false;
        this.xrLabel172.StylePriority.UseTextAlignment = false;
        this.xrLabel172.Text = "ผู้ตรวจสอบ";
        this.xrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel166
        // 
        this.xrLabel166.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel166.LocationFloat = new DevExpress.Utils.PointFloat(285.0595F, 103.4648F);
        this.xrLabel166.Name = "xrLabel166";
        this.xrLabel166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel166.SizeF = new System.Drawing.SizeF(11.45689F, 22.99988F);
        this.xrLabel166.StylePriority.UseBorders = false;
        this.xrLabel166.StylePriority.UseTextAlignment = false;
        this.xrLabel166.Text = ")";
        this.xrLabel166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel165
        // 
        this.xrLabel165.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel165.LocationFloat = new DevExpress.Utils.PointFloat(86.46398F, 103.4648F);
        this.xrLabel165.Name = "xrLabel165";
        this.xrLabel165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
        this.xrLabel165.SizeF = new System.Drawing.SizeF(11.45689F, 22.99988F);
        this.xrLabel165.StylePriority.UseBorders = false;
        this.xrLabel165.StylePriority.UseTextAlignment = false;
        this.xrLabel165.Text = "(";
        this.xrLabel165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel164
        // 
        this.xrLabel164.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel164.LocationFloat = new DevExpress.Utils.PointFloat(90.97296F, 98.46484F);
        this.xrLabel164.Name = "xrLabel164";
        this.xrLabel164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel164.SizeF = new System.Drawing.SizeF(196.7068F, 23.99988F);
        this.xrLabel164.StylePriority.UseBorders = false;
        this.xrLabel164.StylePriority.UseTextAlignment = false;
        this.xrLabel164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrLabel163
        // 
        this.xrLabel163.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel163.LocationFloat = new DevExpress.Utils.PointFloat(90.97296F, 69.57629F);
        this.xrLabel163.Name = "xrLabel163";
        this.xrLabel163.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel163.SizeF = new System.Drawing.SizeF(196.7068F, 24F);
        this.xrLabel163.StylePriority.UseBorders = false;
        // 
        // xrLabel162
        // 
        this.xrLabel162.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(70.8111F, 13.99993F);
        this.xrLabel162.Name = "xrLabel162";
        this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel162.SizeF = new System.Drawing.SizeF(237.5F, 25F);
        this.xrLabel162.StylePriority.UseBorders = false;
        this.xrLabel162.StylePriority.UseTextAlignment = false;
        this.xrLabel162.Text = "จึงเรียนมาเพื่อโปรดทราบ และดำเนินการต่อไป";
        this.xrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel173,
            this.xrLabel170,
            this.xrLabel171,
            this.xrLabel169,
            this.xrLabel167,
            this.xrLabel168});
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.Weight = 1.5D;
        // 
        // xrLabel173
        // 
        this.xrLabel173.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel173.LocationFloat = new DevExpress.Utils.PointFloat(155.9659F, 122.4647F);
        this.xrLabel173.Name = "xrLabel173";
        this.xrLabel173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel173.SizeF = new System.Drawing.SizeF(125F, 25F);
        this.xrLabel173.StylePriority.UseBorders = false;
        this.xrLabel173.StylePriority.UseTextAlignment = false;
        this.xrLabel173.Text = "เจ้าของรถ / ผู้แทน";
        this.xrLabel173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel170
        // 
        this.xrLabel170.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel170.LocationFloat = new DevExpress.Utils.PointFloat(105.9533F, 69.57629F);
        this.xrLabel170.Name = "xrLabel170";
        this.xrLabel170.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel170.SizeF = new System.Drawing.SizeF(196.7068F, 24F);
        this.xrLabel170.StylePriority.UseBorders = false;
        // 
        // xrLabel171
        // 
        this.xrLabel171.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel171.LocationFloat = new DevExpress.Utils.PointFloat(155.9659F, 40.57629F);
        this.xrLabel171.Name = "xrLabel171";
        this.xrLabel171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel171.SizeF = new System.Drawing.SizeF(125F, 25F);
        this.xrLabel171.StylePriority.UseBorders = false;
        this.xrLabel171.StylePriority.UseTextAlignment = false;
        this.xrLabel171.Text = "รับทราบ";
        this.xrLabel171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrLabel169
        // 
        this.xrLabel169.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel169.LocationFloat = new DevExpress.Utils.PointFloat(105.9659F, 98.46484F);
        this.xrLabel169.Name = "xrLabel169";
        this.xrLabel169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel169.SizeF = new System.Drawing.SizeF(196.7068F, 23.99988F);
        this.xrLabel169.StylePriority.UseBorders = false;
        this.xrLabel169.StylePriority.UseTextAlignment = false;
        this.xrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrLabel167
        // 
        this.xrLabel167.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel167.LocationFloat = new DevExpress.Utils.PointFloat(299.6728F, 104.5762F);
        this.xrLabel167.Name = "xrLabel167";
        this.xrLabel167.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel167.SizeF = new System.Drawing.SizeF(11.45689F, 22.99988F);
        this.xrLabel167.StylePriority.UseBorders = false;
        this.xrLabel167.StylePriority.UseTextAlignment = false;
        this.xrLabel167.Text = ")";
        this.xrLabel167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel168
        // 
        this.xrLabel168.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel168.LocationFloat = new DevExpress.Utils.PointFloat(100.9533F, 104.5763F);
        this.xrLabel168.Name = "xrLabel168";
        this.xrLabel168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel168.SizeF = new System.Drawing.SizeF(11.45689F, 22.99988F);
        this.xrLabel168.StylePriority.UseBorders = false;
        this.xrLabel168.StylePriority.UseTextAlignment = false;
        this.xrLabel168.Text = "(";
        this.xrLabel168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTable5
        // 
        this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(12F, 1454.399F);
        this.xrTable5.Name = "xrTable5";
        this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
        this.xrTable5.SizeF = new System.Drawing.SizeF(789.0682F, 133.6115F);
        this.xrTable5.StylePriority.UseBorders = false;
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11});
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.Weight = 1D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine2,
            this.xrLine3,
            this.xrLine1,
            this.xrLabel160,
            this.xrLabel156});
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Weight = 3D;
        // 
        // xrLine2
        // 
        this.xrLine2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(22.21249F, 81.325F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.SizeF = new System.Drawing.SizeF(756.8556F, 12.88306F);
        this.xrLine2.StylePriority.UseBorders = false;
        // 
        // xrLine3
        // 
        this.xrLine3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(22.21263F, 103.9998F);
        this.xrLine3.Name = "xrLine3";
        this.xrLine3.SizeF = new System.Drawing.SizeF(756.8556F, 12.88306F);
        this.xrLine3.StylePriority.UseBorders = false;
        // 
        // xrLine1
        // 
        this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(22.21251F, 61.44189F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.SizeF = new System.Drawing.SizeF(756.8556F, 12.88306F);
        this.xrLine1.StylePriority.UseBorders = false;
        // 
        // xrLabel160
        // 
        this.xrLabel160.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel160.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sOpinion, "Text", "")});
        this.xrLabel160.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(22.2125F, 45.99988F);
        this.xrLabel160.Multiline = true;
        this.xrLabel160.Name = "xrLabel160";
        this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
        this.xrLabel160.SizeF = new System.Drawing.SizeF(756.8557F, 71.99988F);
        this.xrLabel160.StylePriority.UseBorders = false;
        this.xrLabel160.StylePriority.UseFont = false;
        // 
        // sOpinion
        // 
        this.sOpinion.Name = "sOpinion";
        // 
        // xrLabel156
        // 
        this.xrLabel156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel156.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(10.00009F, 10F);
        this.xrLabel156.Name = "xrLabel156";
        this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel156.SizeF = new System.Drawing.SizeF(468.3716F, 23.99988F);
        this.xrLabel156.StylePriority.UseBorders = false;
        this.xrLabel156.StylePriority.UseFont = false;
        this.xrLabel156.StylePriority.UseTextAlignment = false;
        this.xrLabel156.Text = "รายละเอียดการตรวจสอบและความเห็นของผู้ตรายละเอียดการตรวจสอบและความเห็นของผู้ตรวจสอ" +
            "บ";
        this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTable4
        // 
        this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(11.55759F, 851.4424F);
        this.xrTable4.Name = "xrTable4";
        this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
        this.xrTable4.SizeF = new System.Drawing.SizeF(789.5107F, 598.4374F);
        this.xrTable4.StylePriority.UseBorders = false;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell9,
            this.xrTableCell10});
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.Weight = 1D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ckbG7_35_1,
            this.xrLabel110,
            this.xrLabel109,
            this.ckbG7_34_1,
            this.ckbG7_33_1,
            this.xrLabel108,
            this.xrLabel107,
            this.ckbG7_32_1,
            this.ckbG7_31_1,
            this.xrLabel106,
            this.ckbG7_30_1,
            this.xrLabel102,
            this.ckbG7_29_3,
            this.xrLabel105,
            this.lblG7_29_2,
            this.xrLabel99,
            this.lblG7_29_1,
            this.xrLabel104,
            this.xrLabel101,
            this.lblG7_28_1,
            this.xrLabel103,
            this.ckbG7_28_3,
            this.xrLabel96,
            this.lblG7_28_2,
            this.ckbG7_27_3,
            this.xrLabel100,
            this.lblG7_27_2,
            this.xrLabel98,
            this.lblG7_27_1,
            this.xrLabel24,
            this.xrLabel94,
            this.lblG7_1,
            this.xrLabel23});
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Weight = 0.912209717808819D;
        // 
        // ckbG7_35_1
        // 
        this.ckbG7_35_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_35_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 537.9999F);
        this.ckbG7_35_1.Name = "ckbG7_35_1";
        this.ckbG7_35_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_35_1.StylePriority.UseBorders = false;
        this.ckbG7_35_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_35_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel110
        // 
        this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(10.44251F, 513.9999F);
        this.xrLabel110.Multiline = true;
        this.xrLabel110.Name = "xrLabel110";
        this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel110.SizeF = new System.Drawing.SizeF(229.624F, 24F);
        this.xrLabel110.StylePriority.UseBorders = false;
        this.xrLabel110.StylePriority.UseTextAlignment = false;
        this.xrLabel110.Text = "35. ขารับถังเจาะรู 0.5 นิ้ว มองผ่านได้";
        this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel109
        // 
        this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(10.44251F, 464.9999F);
        this.xrLabel109.Multiline = true;
        this.xrLabel109.Name = "xrLabel109";
        this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel109.SizeF = new System.Drawing.SizeF(229.624F, 24F);
        this.xrLabel109.StylePriority.UseBorders = false;
        this.xrLabel109.StylePriority.UseTextAlignment = false;
        this.xrLabel109.Text = "34. เชื่อมน๊อตหน้าแปลนตรงข้ามวาล์วสกัดทุกตัว";
        this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_34_1
        // 
        this.ckbG7_34_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_34_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 488.9999F);
        this.ckbG7_34_1.Name = "ckbG7_34_1";
        this.ckbG7_34_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_34_1.StylePriority.UseBorders = false;
        this.ckbG7_34_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_34_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_33_1
        // 
        this.ckbG7_33_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_33_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 439.9999F);
        this.ckbG7_33_1.Name = "ckbG7_33_1";
        this.ckbG7_33_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_33_1.StylePriority.UseBorders = false;
        this.ckbG7_33_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_33_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel108
        // 
        this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(10.4424F, 392F);
        this.xrLabel108.Multiline = true;
        this.xrLabel108.Name = "xrLabel108";
        this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel108.SizeF = new System.Drawing.SizeF(229.624F, 48F);
        this.xrLabel108.StylePriority.UseBorders = false;
        this.xrLabel108.StylePriority.UseTextAlignment = false;
        this.xrLabel108.Text = "33. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวสุดท้าย \r\n     ตรงข้าม 2 ตัว";
        this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel107
        // 
        this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(10.44251F, 318.9999F);
        this.xrLabel107.Multiline = true;
        this.xrLabel107.Name = "xrLabel107";
        this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel107.SizeF = new System.Drawing.SizeF(229.624F, 48F);
        this.xrLabel107.StylePriority.UseBorders = false;
        this.xrLabel107.StylePriority.UseTextAlignment = false;
        this.xrLabel107.Text = "32. เชื่อมน๊อตหน้าแปลนวาล์วสกัดตัวที่ 1 \r\n     ตรงข้าม 2 ตัว";
        this.xrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_32_1
        // 
        this.ckbG7_32_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_32_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 366.9999F);
        this.ckbG7_32_1.Name = "ckbG7_32_1";
        this.ckbG7_32_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_32_1.StylePriority.UseBorders = false;
        this.ckbG7_32_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_32_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_31_1
        // 
        this.ckbG7_31_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_31_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 294F);
        this.ckbG7_31_1.Name = "ckbG7_31_1";
        this.ckbG7_31_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_31_1.StylePriority.UseBorders = false;
        this.ckbG7_31_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_31_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel106
        // 
        this.xrLabel106.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(10.44251F, 269.9999F);
        this.xrLabel106.Multiline = true;
        this.xrLabel106.Name = "xrLabel106";
        this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel106.SizeF = new System.Drawing.SizeF(229.624F, 24F);
        this.xrLabel106.StylePriority.UseBorders = false;
        this.xrLabel106.StylePriority.UseTextAlignment = false;
        this.xrLabel106.Text = "31. เจาะรูแกนวาล์วหรือด้ามบอลล์วาล์วทุกตัว";
        this.xrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_30_1
        // 
        this.ckbG7_30_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_30_1.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 245F);
        this.ckbG7_30_1.Name = "ckbG7_30_1";
        this.ckbG7_30_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_30_1.StylePriority.UseBorders = false;
        this.ckbG7_30_1.StylePriority.UseTextAlignment = false;
        this.ckbG7_30_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel102
        // 
        this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(10.44245F, 220.9999F);
        this.xrLabel102.Multiline = true;
        this.xrLabel102.Name = "xrLabel102";
        this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel102.SizeF = new System.Drawing.SizeF(229.624F, 24F);
        this.xrLabel102.StylePriority.UseBorders = false;
        this.xrLabel102.StylePriority.UseTextAlignment = false;
        this.xrLabel102.Text = "30. มือหมุนวาล์วหรือก้านวาล์วเชื่อมติดแกนทุกตัว";
        this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_29_3
        // 
        this.ckbG7_29_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_29_3.LocationFloat = new DevExpress.Utils.PointFloat(100.4424F, 195.9999F);
        this.ckbG7_29_3.Name = "ckbG7_29_3";
        this.ckbG7_29_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_29_3.StylePriority.UseBorders = false;
        this.ckbG7_29_3.StylePriority.UseTextAlignment = false;
        this.ckbG7_29_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel105
        // 
        this.xrLabel105.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(67.06472F, 195.9999F);
        this.xrLabel105.Name = "xrLabel105";
        this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel105.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel105.StylePriority.UseBorders = false;
        this.xrLabel105.StylePriority.UseTextAlignment = false;
        this.xrLabel105.Text = "ตัว";
        this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_29_2
        // 
        this.lblG7_29_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_29_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_29_2.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 195.9999F);
        this.lblG7_29_2.Name = "lblG7_29_2";
        this.lblG7_29_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_29_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_29_2.StylePriority.UseBorderDashStyle = false;
        this.lblG7_29_2.StylePriority.UseBorders = false;
        this.lblG7_29_2.StylePriority.UseTextAlignment = false;
        this.lblG7_29_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel99
        // 
        this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(187.9766F, 171F);
        this.xrLabel99.Name = "xrLabel99";
        this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel99.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel99.StylePriority.UseBorders = false;
        this.xrLabel99.StylePriority.UseTextAlignment = false;
        this.xrLabel99.Text = "นิ้ว";
        this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_29_1
        // 
        this.lblG7_29_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_29_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_29_1.LocationFloat = new DevExpress.Utils.PointFloat(152.4424F, 171F);
        this.lblG7_29_1.Name = "lblG7_29_1";
        this.lblG7_29_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_29_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_29_1.StylePriority.UseBorderDashStyle = false;
        this.lblG7_29_1.StylePriority.UseBorders = false;
        this.lblG7_29_1.StylePriority.UseTextAlignment = false;
        this.lblG7_29_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel104
        // 
        this.xrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(10.44245F, 170.9999F);
        this.xrLabel104.Multiline = true;
        this.xrLabel104.Name = "xrLabel104";
        this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel104.SizeF = new System.Drawing.SizeF(142F, 24F);
        this.xrLabel104.StylePriority.UseBorders = false;
        this.xrLabel104.StylePriority.UseTextAlignment = false;
        this.xrLabel104.Text = "29. วาล์วท่อจ่ายเพาเวอร์ขนาด \r\n";
        this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel101
        // 
        this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(183.518F, 121F);
        this.xrLabel101.Name = "xrLabel101";
        this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel101.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel101.StylePriority.UseBorders = false;
        this.xrLabel101.StylePriority.UseTextAlignment = false;
        this.xrLabel101.Text = "นิ้ว";
        this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_28_1
        // 
        this.lblG7_28_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_28_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_28_1.LocationFloat = new DevExpress.Utils.PointFloat(145.4424F, 121F);
        this.lblG7_28_1.Name = "lblG7_28_1";
        this.lblG7_28_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_28_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_28_1.StylePriority.UseBorderDashStyle = false;
        this.lblG7_28_1.StylePriority.UseBorders = false;
        this.lblG7_28_1.StylePriority.UseTextAlignment = false;
        this.lblG7_28_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel103
        // 
        this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(10.4424F, 121F);
        this.xrLabel103.Name = "xrLabel103";
        this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel103.SizeF = new System.Drawing.SizeF(135F, 24F);
        this.xrLabel103.StylePriority.UseBorders = false;
        this.xrLabel103.StylePriority.UseTextAlignment = false;
        this.xrLabel103.Text = "28. วาล์วแยกแต่ละช่องขนาด";
        this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG7_28_3
        // 
        this.ckbG7_28_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_28_3.LocationFloat = new DevExpress.Utils.PointFloat(100.4424F, 146F);
        this.ckbG7_28_3.Name = "ckbG7_28_3";
        this.ckbG7_28_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_28_3.StylePriority.UseBorders = false;
        this.ckbG7_28_3.StylePriority.UseTextAlignment = false;
        this.ckbG7_28_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel96
        // 
        this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(67.06472F, 146F);
        this.xrLabel96.Name = "xrLabel96";
        this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel96.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel96.StylePriority.UseBorders = false;
        this.xrLabel96.StylePriority.UseTextAlignment = false;
        this.xrLabel96.Text = "ตัว";
        this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_28_2
        // 
        this.lblG7_28_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_28_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_28_2.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 146F);
        this.lblG7_28_2.Name = "lblG7_28_2";
        this.lblG7_28_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_28_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_28_2.StylePriority.UseBorderDashStyle = false;
        this.lblG7_28_2.StylePriority.UseBorders = false;
        this.lblG7_28_2.StylePriority.UseTextAlignment = false;
        this.lblG7_28_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // ckbG7_27_3
        // 
        this.ckbG7_27_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG7_27_3.LocationFloat = new DevExpress.Utils.PointFloat(100.4424F, 95.99998F);
        this.ckbG7_27_3.Name = "ckbG7_27_3";
        this.ckbG7_27_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG7_27_3.StylePriority.UseBorders = false;
        this.ckbG7_27_3.StylePriority.UseTextAlignment = false;
        this.ckbG7_27_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel100
        // 
        this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(67.06472F, 95.99996F);
        this.xrLabel100.Name = "xrLabel100";
        this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel100.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel100.StylePriority.UseBorders = false;
        this.xrLabel100.StylePriority.UseTextAlignment = false;
        this.xrLabel100.Text = "ตัว";
        this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_27_2
        // 
        this.lblG7_27_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_27_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_27_2.LocationFloat = new DevExpress.Utils.PointFloat(27.97838F, 95.99996F);
        this.lblG7_27_2.Name = "lblG7_27_2";
        this.lblG7_27_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_27_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_27_2.StylePriority.UseBorderDashStyle = false;
        this.lblG7_27_2.StylePriority.UseBorders = false;
        this.lblG7_27_2.StylePriority.UseTextAlignment = false;
        this.lblG7_27_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel98
        // 
        this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(162.8956F, 71.99997F);
        this.xrLabel98.Name = "xrLabel98";
        this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel98.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel98.StylePriority.UseBorders = false;
        this.xrLabel98.StylePriority.UseTextAlignment = false;
        this.xrLabel98.Text = "นิ้ว";
        this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_27_1
        // 
        this.lblG7_27_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_27_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_27_1.LocationFloat = new DevExpress.Utils.PointFloat(126.7572F, 71.99997F);
        this.lblG7_27_1.Name = "lblG7_27_1";
        this.lblG7_27_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_27_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG7_27_1.StylePriority.UseBorderDashStyle = false;
        this.lblG7_27_1.StylePriority.UseBorders = false;
        this.lblG7_27_1.StylePriority.UseTextAlignment = false;
        this.lblG7_27_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel24
        // 
        this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(10.44245F, 71.99994F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(115.016F, 24F);
        this.xrLabel24.StylePriority.UseBorders = false;
        this.xrLabel24.StylePriority.UseTextAlignment = false;
        this.xrLabel24.Text = "27. วาล์วฉุกเฉิน ขนาด";
        this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel94
        // 
        this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(10.15492F, 47.99994F);
        this.xrLabel94.Name = "xrLabel94";
        this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel94.SizeF = new System.Drawing.SizeF(47.80746F, 24F);
        this.xrLabel94.StylePriority.UseBorders = false;
        this.xrLabel94.StylePriority.UseTextAlignment = false;
        this.xrLabel94.Text = "ชนิดวาล์ว ";
        this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG7_1
        // 
        this.lblG7_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG7_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG7_1.LocationFloat = new DevExpress.Utils.PointFloat(58.27701F, 47.99982F);
        this.lblG7_1.Name = "lblG7_1";
        this.lblG7_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG7_1.SizeF = new System.Drawing.SizeF(171.7894F, 24.00012F);
        this.lblG7_1.StylePriority.UseBorderDashStyle = false;
        this.lblG7_1.StylePriority.UseBorders = false;
        this.lblG7_1.StylePriority.UseTextAlignment = false;
        this.lblG7_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel23
        // 
        this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel23.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(10.44245F, 10F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel23.SizeF = new System.Drawing.SizeF(60.04858F, 24F);
        this.xrLabel23.StylePriority.UseBorders = false;
        this.xrLabel23.StylePriority.UseFont = false;
        this.xrLabel23.StylePriority.UseTextAlignment = false;
        this.xrLabel23.Text = "ตรวจวาล์ว";
        this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel129,
            this.ckbG10_50_1,
            this.xrLabel127,
            this.ckbG10_49_1,
            this.xrLabel126,
            this.ckbG10_48_1,
            this.xrLabel125,
            this.ckbG10_47_1,
            this.xrLabel124,
            this.ckbG10_46_1,
            this.xrLabel122,
            this.ckbG10_45_2,
            this.xrLabel123,
            this.lblG10_45_1,
            this.xrLabel121,
            this.ckbG10_44_1,
            this.xrLabel120,
            this.ckbG10_43_1,
            this.xrLabel119,
            this.ckbG10_42_1,
            this.xrLabel118,
            this.ckbG10_41_1,
            this.xrLabel117,
            this.ckbG10_40_1,
            this.xrLabel116,
            this.ckbG10_39_1,
            this.xrLabel115,
            this.ckbG10_38_1,
            this.xrLabel114,
            this.xrLabel113,
            this.ckbG10_37_1,
            this.ckbG10_36_1,
            this.xrLabel112,
            this.xrLabel111,
            this.lblG10_51_1,
            this.xrLabel128,
            this.lblG10_52_1,
            this.xrLabel130,
            this.ckbG10_53_1});
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.Weight = 0.99322310253210133D;
        // 
        // xrLabel129
        // 
        this.xrLabel129.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(10.37785F, 542.9999F);
        this.xrLabel129.Multiline = true;
        this.xrLabel129.Name = "xrLabel129";
        this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel129.SizeF = new System.Drawing.SizeF(150F, 23.99988F);
        this.xrLabel129.StylePriority.UseBorders = false;
        this.xrLabel129.StylePriority.UseTextAlignment = false;
        this.xrLabel129.Text = "52. ชนิดของน๊อตฝา MANHOLD ";
        this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_50_1
        // 
        this.ckbG10_50_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_50_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 467.9999F);
        this.ckbG10_50_1.Name = "ckbG10_50_1";
        this.ckbG10_50_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_50_1.StylePriority.UseBorders = false;
        this.ckbG10_50_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_50_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel127
        // 
        this.xrLabel127.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(10.33099F, 468.9999F);
        this.xrLabel127.Multiline = true;
        this.xrLabel127.Name = "xrLabel127";
        this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel127.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel127.StylePriority.UseBorders = false;
        this.xrLabel127.StylePriority.UseTextAlignment = false;
        this.xrLabel127.Text = "50. ภายในถังล้างสะอาดไม่มีคราบน้ำมัน ";
        this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_49_1
        // 
        this.ckbG10_49_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_49_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 442.9999F);
        this.ckbG10_49_1.Name = "ckbG10_49_1";
        this.ckbG10_49_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_49_1.StylePriority.UseBorders = false;
        this.ckbG10_49_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_49_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel126
        // 
        this.xrLabel126.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(10.37788F, 443.9999F);
        this.xrLabel126.Multiline = true;
        this.xrLabel126.Name = "xrLabel126";
        this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel126.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel126.StylePriority.UseBorders = false;
        this.xrLabel126.StylePriority.UseTextAlignment = false;
        this.xrLabel126.Text = "49. ตัดแป้นเก่าออก,แต่งรอยให้เรียบ";
        this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_48_1
        // 
        this.ckbG10_48_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_48_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 394.9998F);
        this.ckbG10_48_1.Name = "ckbG10_48_1";
        this.ckbG10_48_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_48_1.StylePriority.UseBorders = false;
        this.ckbG10_48_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_48_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel125
        // 
        this.xrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(10.37788F, 394.9998F);
        this.xrLabel125.Multiline = true;
        this.xrLabel125.Name = "xrLabel125";
        this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel125.SizeF = new System.Drawing.SizeF(195.6799F, 48F);
        this.xrLabel125.StylePriority.UseBorders = false;
        this.xrLabel125.StylePriority.UseTextAlignment = false;
        this.xrLabel125.Text = "48. มีวงแหวนกันงัดที่ฝาแมนโฮลและ\r\n     เต็มวงทุกช่อง ";
        this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_47_1
        // 
        this.ckbG10_47_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_47_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 369.9998F);
        this.ckbG10_47_1.Name = "ckbG10_47_1";
        this.ckbG10_47_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_47_1.StylePriority.UseBorders = false;
        this.ckbG10_47_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_47_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel124
        // 
        this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(10.37785F, 370.9998F);
        this.xrLabel124.Multiline = true;
        this.xrLabel124.Name = "xrLabel124";
        this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel124.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel124.StylePriority.UseBorders = false;
        this.xrLabel124.StylePriority.UseTextAlignment = false;
        this.xrLabel124.Text = "47. ไม่มีแผ่นเหล็กบังสายตาภายใน ";
        this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_46_1
        // 
        this.ckbG10_46_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_46_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 344.9998F);
        this.ckbG10_46_1.Name = "ckbG10_46_1";
        this.ckbG10_46_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_46_1.StylePriority.UseBorders = false;
        this.ckbG10_46_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_46_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel122
        // 
        this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(9.999967F, 345.9998F);
        this.xrLabel122.Multiline = true;
        this.xrLabel122.Name = "xrLabel122";
        this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel122.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel122.StylePriority.UseBorders = false;
        this.xrLabel122.StylePriority.UseTextAlignment = false;
        this.xrLabel122.Text = "46. ท่ออุ่นน้ำมันภายในถัง";
        this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_45_2
        // 
        this.ckbG10_45_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_45_2.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 320F);
        this.ckbG10_45_2.Name = "ckbG10_45_2";
        this.ckbG10_45_2.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_45_2.StylePriority.UseBorders = false;
        this.ckbG10_45_2.StylePriority.UseTextAlignment = false;
        this.ckbG10_45_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel123
        // 
        this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(134.5164F, 320.9999F);
        this.xrLabel123.Multiline = true;
        this.xrLabel123.Name = "xrLabel123";
        this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel123.SizeF = new System.Drawing.SizeF(71.16348F, 24F);
        this.xrLabel123.StylePriority.UseBorders = false;
        this.xrLabel123.StylePriority.UseTextAlignment = false;
        this.xrLabel123.Text = "ทะลุผ่านถึงกัน ";
        this.xrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG10_45_1
        // 
        this.lblG10_45_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG10_45_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG10_45_1.LocationFloat = new DevExpress.Utils.PointFloat(100.3779F, 320.9999F);
        this.lblG10_45_1.Name = "lblG10_45_1";
        this.lblG10_45_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG10_45_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG10_45_1.StylePriority.UseBorderDashStyle = false;
        this.lblG10_45_1.StylePriority.UseBorders = false;
        this.lblG10_45_1.StylePriority.UseTextAlignment = false;
        this.lblG10_45_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel121
        // 
        this.xrLabel121.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(9.999985F, 320.9999F);
        this.xrLabel121.Multiline = true;
        this.xrLabel121.Name = "xrLabel121";
        this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel121.SizeF = new System.Drawing.SizeF(90.37793F, 24F);
        this.xrLabel121.StylePriority.UseBorders = false;
        this.xrLabel121.StylePriority.UseTextAlignment = false;
        this.xrLabel121.Text = "45. ภายในถังช่องที่ ";
        this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_44_1
        // 
        this.ckbG10_44_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_44_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 294.9999F);
        this.ckbG10_44_1.Name = "ckbG10_44_1";
        this.ckbG10_44_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_44_1.StylePriority.UseBorders = false;
        this.ckbG10_44_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_44_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel120
        // 
        this.xrLabel120.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(10.37788F, 492.9999F);
        this.xrLabel120.Multiline = true;
        this.xrLabel120.Name = "xrLabel120";
        this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel120.SizeF = new System.Drawing.SizeF(124.1385F, 24F);
        this.xrLabel120.StylePriority.UseBorders = false;
        this.xrLabel120.StylePriority.UseTextAlignment = false;
        this.xrLabel120.Text = "51. อู่ต่อถังรถบรรทุกน้ำมัน ";
        this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_43_1
        // 
        this.ckbG10_43_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_43_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 269.9999F);
        this.ckbG10_43_1.Name = "ckbG10_43_1";
        this.ckbG10_43_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_43_1.StylePriority.UseBorders = false;
        this.ckbG10_43_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_43_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel119
        // 
        this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(10.37788F, 270.9999F);
        this.xrLabel119.Multiline = true;
        this.xrLabel119.Name = "xrLabel119";
        this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel119.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel119.StylePriority.UseBorders = false;
        this.xrLabel119.StylePriority.UseTextAlignment = false;
        this.xrLabel119.Text = "43. ขอบฝาถังเจาะรูและลึกไม่เกิน 25 ซม. ";
        this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_42_1
        // 
        this.ckbG10_42_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_42_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 222F);
        this.ckbG10_42_1.Name = "ckbG10_42_1";
        this.ckbG10_42_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_42_1.StylePriority.UseBorders = false;
        this.ckbG10_42_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_42_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel118
        // 
        this.xrLabel118.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(10.33095F, 221.9999F);
        this.xrLabel118.Multiline = true;
        this.xrLabel118.Name = "xrLabel118";
        this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel118.SizeF = new System.Drawing.SizeF(195.6799F, 48F);
        this.xrLabel118.StylePriority.UseBorders = false;
        this.xrLabel118.StylePriority.UseTextAlignment = false;
        this.xrLabel118.Text = "42. ช่องลงน้ำมัน/ตำแหน่งแป้นอยู่กลาง\r\n     ระหว่างตองกั้นถัง ";
        this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_41_1
        // 
        this.ckbG10_41_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_41_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 196.9999F);
        this.ckbG10_41_1.Name = "ckbG10_41_1";
        this.ckbG10_41_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_41_1.StylePriority.UseBorders = false;
        this.ckbG10_41_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_41_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel117
        // 
        this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(10F, 197.9999F);
        this.xrLabel117.Multiline = true;
        this.xrLabel117.Name = "xrLabel117";
        this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel117.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel117.StylePriority.UseBorders = false;
        this.xrLabel117.StylePriority.UseTextAlignment = false;
        this.xrLabel117.Text = "41. สภาพถังไม่บุบเสียรูปทรง ";
        this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_40_1
        // 
        this.ckbG10_40_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_40_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0578F, 148.9998F);
        this.ckbG10_40_1.Name = "ckbG10_40_1";
        this.ckbG10_40_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_40_1.StylePriority.UseBorders = false;
        this.ckbG10_40_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_40_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel116
        // 
        this.xrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(9.999967F, 149.9997F);
        this.xrLabel116.Multiline = true;
        this.xrLabel116.Name = "xrLabel116";
        this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel116.SizeF = new System.Drawing.SizeF(195.6799F, 48F);
        this.xrLabel116.StylePriority.UseBorders = false;
        this.xrLabel116.StylePriority.UseTextAlignment = false;
        this.xrLabel116.Text = "40. มีวาล์วระบายไอแก๊สที่ฝาปิด\r\n     ช่องลงน้ำมัน ";
        this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_39_1
        // 
        this.ckbG10_39_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_39_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 123.9998F);
        this.ckbG10_39_1.Name = "ckbG10_39_1";
        this.ckbG10_39_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_39_1.StylePriority.UseBorders = false;
        this.ckbG10_39_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_39_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel115
        // 
        this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(10.37788F, 124.9998F);
        this.xrLabel115.Multiline = true;
        this.xrLabel115.Name = "xrLabel115";
        this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel115.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel115.StylePriority.UseBorders = false;
        this.xrLabel115.StylePriority.UseTextAlignment = false;
        this.xrLabel115.Text = "39. สลักฝาปิดช่องลงน้ำมันเชื่อมติดแกน ";
        this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_38_1
        // 
        this.ckbG10_38_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_38_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 98.99982F);
        this.ckbG10_38_1.Name = "ckbG10_38_1";
        this.ckbG10_38_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_38_1.StylePriority.UseBorders = false;
        this.ckbG10_38_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_38_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel114
        // 
        this.xrLabel114.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(10.37782F, 99.99982F);
        this.xrLabel114.Multiline = true;
        this.xrLabel114.Name = "xrLabel114";
        this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel114.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel114.StylePriority.UseBorders = false;
        this.xrLabel114.StylePriority.UseTextAlignment = false;
        this.xrLabel114.Text = "38. ตองกั้นถังต้องแข็งแรงไม่บิดตัว";
        this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel113
        // 
        this.xrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(10.37785F, 74.99982F);
        this.xrLabel113.Multiline = true;
        this.xrLabel113.Name = "xrLabel113";
        this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel113.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel113.StylePriority.UseBorders = false;
        this.xrLabel113.StylePriority.UseTextAlignment = false;
        this.xrLabel113.Text = "37. ภายในตองกั้นถังต้องไม่เป็น 2 ชั้น ";
        this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_37_1
        // 
        this.ckbG10_37_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_37_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 73.99982F);
        this.ckbG10_37_1.Name = "ckbG10_37_1";
        this.ckbG10_37_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_37_1.StylePriority.UseBorders = false;
        this.ckbG10_37_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_37_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_36_1
        // 
        this.ckbG10_36_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_36_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 47.99994F);
        this.ckbG10_36_1.Name = "ckbG10_36_1";
        this.ckbG10_36_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_36_1.StylePriority.UseBorders = false;
        this.ckbG10_36_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_36_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel112
        // 
        this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(10.37785F, 47.99994F);
        this.xrLabel112.Multiline = true;
        this.xrLabel112.Name = "xrLabel112";
        this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel112.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel112.StylePriority.UseBorders = false;
        this.xrLabel112.StylePriority.UseTextAlignment = false;
        this.xrLabel112.Text = "36. ใต้ท้องถังต้องไม่ทำเป็น 2 ชั้น";
        this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel111
        // 
        this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel111.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(12.33634F, 9.999939F);
        this.xrLabel111.Name = "xrLabel111";
        this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel111.SizeF = new System.Drawing.SizeF(71.06305F, 24F);
        this.xrLabel111.StylePriority.UseBorders = false;
        this.xrLabel111.StylePriority.UseFont = false;
        this.xrLabel111.StylePriority.UseTextAlignment = false;
        this.xrLabel111.Text = "ตรวจถังน้ำมัน";
        this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG10_51_1
        // 
        this.lblG10_51_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG10_51_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG10_51_1.LocationFloat = new DevExpress.Utils.PointFloat(29.87599F, 516.9999F);
        this.lblG10_51_1.Name = "lblG10_51_1";
        this.lblG10_51_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG10_51_1.SizeF = new System.Drawing.SizeF(227.8526F, 24F);
        this.lblG10_51_1.StylePriority.UseBorderDashStyle = false;
        this.lblG10_51_1.StylePriority.UseBorders = false;
        this.lblG10_51_1.StylePriority.UseTextAlignment = false;
        this.lblG10_51_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel128
        // 
        this.xrLabel128.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(9.999967F, 296F);
        this.xrLabel128.Multiline = true;
        this.xrLabel128.Name = "xrLabel128";
        this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel128.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel128.StylePriority.UseBorders = false;
        this.xrLabel128.StylePriority.UseTextAlignment = false;
        this.xrLabel128.Text = "44. ภายในถังไม่เจาะทะลุออกเพื่อการใด ";
        this.xrLabel128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG10_52_1
        // 
        this.lblG10_52_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG10_52_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG10_52_1.LocationFloat = new DevExpress.Utils.PointFloat(160.3779F, 542.9999F);
        this.lblG10_52_1.Name = "lblG10_52_1";
        this.lblG10_52_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG10_52_1.SizeF = new System.Drawing.SizeF(97.35071F, 23.99976F);
        this.lblG10_52_1.StylePriority.UseBorderDashStyle = false;
        this.lblG10_52_1.StylePriority.UseBorders = false;
        this.lblG10_52_1.StylePriority.UseTextAlignment = false;
        this.lblG10_52_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel130
        // 
        this.xrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(9.999985F, 568.9998F);
        this.xrLabel130.Multiline = true;
        this.xrLabel130.Name = "xrLabel130";
        this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel130.SizeF = new System.Drawing.SizeF(195.6799F, 24F);
        this.xrLabel130.StylePriority.UseBorders = false;
        this.xrLabel130.StylePriority.UseTextAlignment = false;
        this.xrLabel130.Text = "53. ช่องว่างระยะติดแป้น ไม่ต่ำกว่า 15 ซม. ";
        this.xrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG10_53_1
        // 
        this.ckbG10_53_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG10_53_1.LocationFloat = new DevExpress.Utils.PointFloat(206.0577F, 567.9997F);
        this.ckbG10_53_1.Name = "ckbG10_53_1";
        this.ckbG10_53_1.SizeF = new System.Drawing.SizeF(51.67081F, 25F);
        this.ckbG10_53_1.StylePriority.UseBorders = false;
        this.ckbG10_53_1.StylePriority.UseTextAlignment = false;
        this.ckbG10_53_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel161,
            this.lblG11_65_2,
            this.xrLabel159,
            this.xrLabel158,
            this.lblG11_65_1,
            this.xrLabel153,
            this.xrLabel157,
            this.lblG11_64_2,
            this.xrLabel155,
            this.xrLabel154,
            this.lblG11_64_1,
            this.xrLabel151,
            this.xrLabel152,
            this.lblG11_63_1,
            this.xrLabel149,
            this.xrLabel150,
            this.lblG11_62_1,
            this.xrLabel147,
            this.xrLabel148,
            this.lblG11_61_1,
            this.xrLabel145,
            this.xrLabel146,
            this.lblG11_60_1,
            this.xrLabel143,
            this.xrLabel144,
            this.lblG11_59_1,
            this.xrLabel141,
            this.xrLabel142,
            this.lblG11_58_1,
            this.xrLabel139,
            this.xrLabel140,
            this.lblG11_57_1,
            this.xrLabel136,
            this.xrLabel138,
            this.lblG11_56_1,
            this.xrLabel133,
            this.xrLabel137,
            this.lblG11_55_1,
            this.xrLabel135,
            this.xrLabel134,
            this.lblG11_54_1,
            this.xrLabel132,
            this.xrLabel131});
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.Weight = 1.09456717965908D;
        // 
        // xrLabel161
        // 
        this.xrLabel161.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel161.LocationFloat = new DevExpress.Utils.PointFloat(144.3559F, 335.9997F);
        this.xrLabel161.Name = "xrLabel161";
        this.xrLabel161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel161.SizeF = new System.Drawing.SizeF(45.15991F, 24F);
        this.xrLabel161.StylePriority.UseBorders = false;
        this.xrLabel161.StylePriority.UseTextAlignment = false;
        this.xrLabel161.Text = "กระป๋อง";
        this.xrLabel161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_65_2
        // 
        this.lblG11_65_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_65_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_65_2.LocationFloat = new DevExpress.Utils.PointFloat(110.2176F, 335.9997F);
        this.lblG11_65_2.Name = "lblG11_65_2";
        this.lblG11_65_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_65_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_65_2.StylePriority.UseBorderDashStyle = false;
        this.lblG11_65_2.StylePriority.UseBorders = false;
        this.lblG11_65_2.StylePriority.UseTextAlignment = false;
        this.lblG11_65_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel159
        // 
        this.xrLabel159.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(60.25638F, 335.9997F);
        this.xrLabel159.Multiline = true;
        this.xrLabel159.Name = "xrLabel159";
        this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel159.SizeF = new System.Drawing.SizeF(49.40552F, 24F);
        this.xrLabel159.StylePriority.UseBorders = false;
        this.xrLabel159.StylePriority.UseTextAlignment = false;
        this.xrLabel159.Text = "สีน้ำเงิน ";
        this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel158
        // 
        this.xrLabel158.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(144.356F, 311.9997F);
        this.xrLabel158.Name = "xrLabel158";
        this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel158.SizeF = new System.Drawing.SizeF(45.15991F, 24F);
        this.xrLabel158.StylePriority.UseBorders = false;
        this.xrLabel158.StylePriority.UseTextAlignment = false;
        this.xrLabel158.Text = "กระป๋อง";
        this.xrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_65_1
        // 
        this.lblG11_65_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_65_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_65_1.LocationFloat = new DevExpress.Utils.PointFloat(110.2177F, 311.9997F);
        this.lblG11_65_1.Name = "lblG11_65_1";
        this.lblG11_65_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_65_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_65_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_65_1.StylePriority.UseBorders = false;
        this.lblG11_65_1.StylePriority.UseTextAlignment = false;
        this.lblG11_65_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel153
        // 
        this.xrLabel153.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(9.476349F, 311.9997F);
        this.xrLabel153.Multiline = true;
        this.xrLabel153.Name = "xrLabel153";
        this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel153.SizeF = new System.Drawing.SizeF(100.7412F, 24F);
        this.xrLabel153.StylePriority.UseBorders = false;
        this.xrLabel153.StylePriority.UseTextAlignment = false;
        this.xrLabel153.Text = "65. สีสเปรย์ สีขาว ";
        this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel157
        // 
        this.xrLabel157.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(258.7589F, 287.9995F);
        this.xrLabel157.Name = "xrLabel157";
        this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel157.SizeF = new System.Drawing.SizeF(29.29852F, 24F);
        this.xrLabel157.StylePriority.UseBorders = false;
        this.xrLabel157.StylePriority.UseTextAlignment = false;
        this.xrLabel157.Text = "อัน";
        this.xrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_64_2
        // 
        this.lblG11_64_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_64_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_64_2.LocationFloat = new DevExpress.Utils.PointFloat(223.6542F, 287.9995F);
        this.lblG11_64_2.Name = "lblG11_64_2";
        this.lblG11_64_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_64_2.SizeF = new System.Drawing.SizeF(35.10492F, 24F);
        this.lblG11_64_2.StylePriority.UseBorderDashStyle = false;
        this.lblG11_64_2.StylePriority.UseBorders = false;
        this.lblG11_64_2.StylePriority.UseTextAlignment = false;
        this.lblG11_64_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel155
        // 
        this.xrLabel155.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel155.LocationFloat = new DevExpress.Utils.PointFloat(173.6544F, 287.9995F);
        this.xrLabel155.Multiline = true;
        this.xrLabel155.Name = "xrLabel155";
        this.xrLabel155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel155.SizeF = new System.Drawing.SizeF(50F, 24F);
        this.xrLabel155.StylePriority.UseBorders = false;
        this.xrLabel155.StylePriority.UseTextAlignment = false;
        this.xrLabel155.Text = "ดอกสว่าน";
        this.xrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel154
        // 
        this.xrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(144.3559F, 287.9996F);
        this.xrLabel154.Name = "xrLabel154";
        this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel154.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel154.StylePriority.UseBorders = false;
        this.xrLabel154.StylePriority.UseTextAlignment = false;
        this.xrLabel154.Text = "อัน";
        this.xrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_64_1
        // 
        this.lblG11_64_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_64_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_64_1.LocationFloat = new DevExpress.Utils.PointFloat(110.2175F, 287.9997F);
        this.lblG11_64_1.Name = "lblG11_64_1";
        this.lblG11_64_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_64_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_64_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_64_1.StylePriority.UseBorders = false;
        this.lblG11_64_1.StylePriority.UseTextAlignment = false;
        this.lblG11_64_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel151
        // 
        this.xrLabel151.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel151.LocationFloat = new DevExpress.Utils.PointFloat(9.476714F, 287.9997F);
        this.xrLabel151.Multiline = true;
        this.xrLabel151.Name = "xrLabel151";
        this.xrLabel151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel151.SizeF = new System.Drawing.SizeF(100.1852F, 24F);
        this.xrLabel151.StylePriority.UseBorders = false;
        this.xrLabel151.StylePriority.UseTextAlignment = false;
        this.xrLabel151.Text = "64. ดอกรีเวท ";
        this.xrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel152
        // 
        this.xrLabel152.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel152.LocationFloat = new DevExpress.Utils.PointFloat(258.759F, 263.9997F);
        this.xrLabel152.Name = "xrLabel152";
        this.xrLabel152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel152.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel152.StylePriority.UseBorders = false;
        this.xrLabel152.StylePriority.UseTextAlignment = false;
        this.xrLabel152.Text = "แผ่น";
        this.xrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_63_1
        // 
        this.lblG11_63_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_63_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_63_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6207F, 263.9997F);
        this.lblG11_63_1.Name = "lblG11_63_1";
        this.lblG11_63_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_63_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_63_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_63_1.StylePriority.UseBorders = false;
        this.lblG11_63_1.StylePriority.UseTextAlignment = false;
        this.lblG11_63_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel149
        // 
        this.xrLabel149.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(9.47638F, 263.9997F);
        this.xrLabel149.Multiline = true;
        this.xrLabel149.Name = "xrLabel149";
        this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel149.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel149.StylePriority.UseBorders = false;
        this.xrLabel149.StylePriority.UseTextAlignment = false;
        this.xrLabel149.Text = "63. ลวดเชื่อม 3.2 มม.";
        this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel150
        // 
        this.xrLabel150.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(258.7589F, 239.9996F);
        this.xrLabel150.Name = "xrLabel150";
        this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel150.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel150.StylePriority.UseBorders = false;
        this.xrLabel150.StylePriority.UseTextAlignment = false;
        this.xrLabel150.Text = "แผ่น";
        this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_62_1
        // 
        this.lblG11_62_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_62_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_62_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6205F, 239.9996F);
        this.lblG11_62_1.Name = "lblG11_62_1";
        this.lblG11_62_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_62_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_62_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_62_1.StylePriority.UseBorders = false;
        this.lblG11_62_1.StylePriority.UseTextAlignment = false;
        this.lblG11_62_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel147
        // 
        this.xrLabel147.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(9.476418F, 239.9997F);
        this.xrLabel147.Multiline = true;
        this.xrLabel147.Name = "xrLabel147";
        this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel147.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel147.StylePriority.UseBorders = false;
        this.xrLabel147.StylePriority.UseTextAlignment = false;
        this.xrLabel147.Text = "62. ป้ายบอกระยะน้ำเงิน";
        this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel148
        // 
        this.xrLabel148.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(258.759F, 215.9997F);
        this.xrLabel148.Name = "xrLabel148";
        this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel148.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel148.StylePriority.UseBorders = false;
        this.xrLabel148.StylePriority.UseTextAlignment = false;
        this.xrLabel148.Text = "แผ่น";
        this.xrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_61_1
        // 
        this.lblG11_61_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_61_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_61_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6206F, 215.9997F);
        this.lblG11_61_1.Name = "lblG11_61_1";
        this.lblG11_61_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_61_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_61_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_61_1.StylePriority.UseBorders = false;
        this.lblG11_61_1.StylePriority.UseTextAlignment = false;
        this.lblG11_61_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel145
        // 
        this.xrLabel145.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(9.476456F, 215.9997F);
        this.xrLabel145.Multiline = true;
        this.xrLabel145.Name = "xrLabel145";
        this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel145.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel145.StylePriority.UseBorders = false;
        this.xrLabel145.StylePriority.UseTextAlignment = false;
        this.xrLabel145.Text = "61. ป้ายบอกระยะสีแดง";
        this.xrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel146
        // 
        this.xrLabel146.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(258.7591F, 191.9995F);
        this.xrLabel146.Name = "xrLabel146";
        this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel146.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel146.StylePriority.UseBorders = false;
        this.xrLabel146.StylePriority.UseTextAlignment = false;
        this.xrLabel146.Text = "ตัว";
        this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_60_1
        // 
        this.lblG11_60_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_60_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_60_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6207F, 191.9997F);
        this.lblG11_60_1.Name = "lblG11_60_1";
        this.lblG11_60_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_60_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_60_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_60_1.StylePriority.UseBorders = false;
        this.lblG11_60_1.StylePriority.UseTextAlignment = false;
        this.lblG11_60_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel143
        // 
        this.xrLabel143.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel143.LocationFloat = new DevExpress.Utils.PointFloat(9.47647F, 191.9996F);
        this.xrLabel143.Multiline = true;
        this.xrLabel143.Name = "xrLabel143";
        this.xrLabel143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel143.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel143.StylePriority.UseBorders = false;
        this.xrLabel143.StylePriority.UseTextAlignment = false;
        this.xrLabel143.Text = "60. ซีลตะกั่ว";
        this.xrLabel143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel144
        // 
        this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(258.7591F, 167.9996F);
        this.xrLabel144.Name = "xrLabel144";
        this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel144.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel144.StylePriority.UseBorders = false;
        this.xrLabel144.StylePriority.UseTextAlignment = false;
        this.xrLabel144.Text = "ตัว";
        this.xrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_59_1
        // 
        this.lblG11_59_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_59_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_59_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6205F, 167.9997F);
        this.lblG11_59_1.Name = "lblG11_59_1";
        this.lblG11_59_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_59_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_59_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_59_1.StylePriority.UseBorders = false;
        this.lblG11_59_1.StylePriority.UseTextAlignment = false;
        this.lblG11_59_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel141
        // 
        this.xrLabel141.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(9.476592F, 167.9996F);
        this.xrLabel141.Multiline = true;
        this.xrLabel141.Name = "xrLabel141";
        this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel141.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel141.StylePriority.UseBorders = false;
        this.xrLabel141.StylePriority.UseTextAlignment = false;
        this.xrLabel141.Text = "59. ฟาสซีล";
        this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel142
        // 
        this.xrLabel142.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(258.7589F, 143.9997F);
        this.xrLabel142.Name = "xrLabel142";
        this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel142.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel142.StylePriority.UseBorders = false;
        this.xrLabel142.StylePriority.UseTextAlignment = false;
        this.xrLabel142.Text = "ม.";
        this.xrLabel142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_58_1
        // 
        this.lblG11_58_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_58_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_58_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6205F, 143.9996F);
        this.lblG11_58_1.Name = "lblG11_58_1";
        this.lblG11_58_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_58_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_58_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_58_1.StylePriority.UseBorders = false;
        this.lblG11_58_1.StylePriority.UseTextAlignment = false;
        this.lblG11_58_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel139
        // 
        this.xrLabel139.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(9.476456F, 143.9997F);
        this.xrLabel139.Multiline = true;
        this.xrLabel139.Name = "xrLabel139";
        this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel139.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel139.StylePriority.UseBorders = false;
        this.xrLabel139.StylePriority.UseTextAlignment = false;
        this.xrLabel139.Text = "58. ลวดสลิงขนาด 2.5 มม. ร้อยแป้นระดับ";
        this.xrLabel139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel140
        // 
        this.xrLabel140.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(258.759F, 119.9997F);
        this.xrLabel140.Name = "xrLabel140";
        this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel140.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel140.StylePriority.UseBorders = false;
        this.xrLabel140.StylePriority.UseTextAlignment = false;
        this.xrLabel140.Text = "ม.";
        this.xrLabel140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_57_1
        // 
        this.lblG11_57_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_57_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_57_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6207F, 119.9997F);
        this.lblG11_57_1.Name = "lblG11_57_1";
        this.lblG11_57_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_57_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_57_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_57_1.StylePriority.UseBorders = false;
        this.lblG11_57_1.StylePriority.UseTextAlignment = false;
        this.lblG11_57_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel136
        // 
        this.xrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(9.476456F, 119.9997F);
        this.xrLabel136.Multiline = true;
        this.xrLabel136.Name = "xrLabel136";
        this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel136.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel136.StylePriority.UseBorders = false;
        this.xrLabel136.StylePriority.UseTextAlignment = false;
        this.xrLabel136.Text = "57. ลวดสลิง1.5 มม. ขนาดร้อยฝาแมนโฮล ";
        this.xrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel138
        // 
        this.xrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(258.759F, 95.99967F);
        this.xrLabel138.Name = "xrLabel138";
        this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel138.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel138.StylePriority.UseBorders = false;
        this.xrLabel138.StylePriority.UseTextAlignment = false;
        this.xrLabel138.Text = "ตัว";
        this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_56_1
        // 
        this.lblG11_56_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_56_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_56_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6205F, 95.99975F);
        this.lblG11_56_1.Name = "lblG11_56_1";
        this.lblG11_56_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_56_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_56_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_56_1.StylePriority.UseBorders = false;
        this.lblG11_56_1.StylePriority.UseTextAlignment = false;
        this.lblG11_56_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel133
        // 
        this.xrLabel133.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(9.476456F, 95.99976F);
        this.xrLabel133.Multiline = true;
        this.xrLabel133.Name = "xrLabel133";
        this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel133.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel133.StylePriority.UseBorders = false;
        this.xrLabel133.StylePriority.UseTextAlignment = false;
        this.xrLabel133.Text = "56. เหล็กร้อยซีล";
        this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel137
        // 
        this.xrLabel137.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(9.476456F, 71.99982F);
        this.xrLabel137.Multiline = true;
        this.xrLabel137.Name = "xrLabel137";
        this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel137.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel137.StylePriority.UseBorders = false;
        this.xrLabel137.StylePriority.UseTextAlignment = false;
        this.xrLabel137.Text = "55. เหล็กรองแป้น 2 ม.  \r\n";
        this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_55_1
        // 
        this.lblG11_55_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_55_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_55_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6206F, 71.99982F);
        this.lblG11_55_1.Name = "lblG11_55_1";
        this.lblG11_55_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_55_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_55_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_55_1.StylePriority.UseBorders = false;
        this.lblG11_55_1.StylePriority.UseTextAlignment = false;
        this.lblG11_55_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel135
        // 
        this.xrLabel135.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(258.7589F, 71.99979F);
        this.xrLabel135.Name = "xrLabel135";
        this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel135.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel135.StylePriority.UseBorders = false;
        this.xrLabel135.StylePriority.UseTextAlignment = false;
        this.xrLabel135.Text = "อัน";
        this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel134
        // 
        this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(258.7589F, 47.99981F);
        this.xrLabel134.Name = "xrLabel134";
        this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel134.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel134.StylePriority.UseBorders = false;
        this.xrLabel134.StylePriority.UseTextAlignment = false;
        this.xrLabel134.Text = "อัน";
        this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG11_54_1
        // 
        this.lblG11_54_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG11_54_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG11_54_1.LocationFloat = new DevExpress.Utils.PointFloat(224.6205F, 47.99981F);
        this.lblG11_54_1.Name = "lblG11_54_1";
        this.lblG11_54_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG11_54_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG11_54_1.StylePriority.UseBorderDashStyle = false;
        this.lblG11_54_1.StylePriority.UseBorders = false;
        this.lblG11_54_1.StylePriority.UseTextAlignment = false;
        this.lblG11_54_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel132
        // 
        this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(9.476592F, 47.99982F);
        this.xrLabel132.Name = "xrLabel132";
        this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel132.SizeF = new System.Drawing.SizeF(214.1779F, 24F);
        this.xrLabel132.StylePriority.UseBorders = false;
        this.xrLabel132.StylePriority.UseTextAlignment = false;
        this.xrLabel132.Text = "54. แป้นทองเหลือง(ไม่มีตัวเลข) ";
        this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel131
        // 
        this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel131.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(9.476567F, 9.99991F);
        this.xrLabel131.Name = "xrLabel131";
        this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel131.SizeF = new System.Drawing.SizeF(124.207F, 24.00006F);
        this.xrLabel131.StylePriority.UseBorders = false;
        this.xrLabel131.StylePriority.UseFont = false;
        this.xrLabel131.StylePriority.UseTextAlignment = false;
        this.xrLabel131.Text = "รายการวัสดุที่ใช้ไปทั้งหมด";
        this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTable3
        // 
        this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(12F, 505.5666F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(789.0684F, 343.8759F);
        this.xrTable3.StylePriority.UseBorders = false;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8});
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Weight = 1D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel80,
            this.lblG4_20_1,
            this.xrLabel82,
            this.xrLabel79,
            this.lblG4_20_2,
            this.xrLabel77,
            this.ckbG4_20_3,
            this.lblG4_19_2,
            this.xrLabel78,
            this.ckbG4_19_3,
            this.xrLabel76,
            this.xrLabel71,
            this.lblG4_19_1,
            this.xrLabel75,
            this.xrLabel73,
            this.xrLabel74,
            this.ckbG4_18_3,
            this.lblG4_18_2,
            this.xrLabel65,
            this.xrLabel69,
            this.lblG4_18_1,
            this.lblG4_17_2,
            this.xrLabel70,
            this.xrLabel66,
            this.xrLabel61,
            this.lblG4_17_1,
            this.ckbG4_1,
            this.xrLabel57});
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.Weight = 0.91103905315685674D;
        // 
        // xrLabel80
        // 
        this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(9.712509F, 264.2536F);
        this.xrLabel80.Name = "xrLabel80";
        this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel80.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel80.StylePriority.UseBorders = false;
        this.xrLabel80.StylePriority.UseTextAlignment = false;
        this.xrLabel80.Text = "20. แหนบท้ายซ้าย ";
        this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_20_1
        // 
        this.lblG4_20_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_20_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_20_1.LocationFloat = new DevExpress.Utils.PointFloat(126.3148F, 264.2536F);
        this.lblG4_20_1.Name = "lblG4_20_1";
        this.lblG4_20_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_20_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_20_1.StylePriority.UseBorderDashStyle = false;
        this.lblG4_20_1.StylePriority.UseBorders = false;
        this.lblG4_20_1.StylePriority.UseTextAlignment = false;
        this.lblG4_20_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel82
        // 
        this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 264.2536F);
        this.xrLabel82.Name = "xrLabel82";
        this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel82.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel82.StylePriority.UseBorders = false;
        this.xrLabel82.StylePriority.UseTextAlignment = false;
        this.xrLabel82.Text = "แผ่น";
        this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel79
        // 
        this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 289.2536F);
        this.xrLabel79.Name = "xrLabel79";
        this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel79.SizeF = new System.Drawing.SizeF(29.29858F, 24.00009F);
        this.xrLabel79.StylePriority.UseBorders = false;
        this.xrLabel79.StylePriority.UseTextAlignment = false;
        this.xrLabel79.Text = "แผ่น";
        this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_20_2
        // 
        this.lblG4_20_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_20_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_20_2.LocationFloat = new DevExpress.Utils.PointFloat(126.3147F, 289.2536F);
        this.lblG4_20_2.Name = "lblG4_20_2";
        this.lblG4_20_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_20_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_20_2.StylePriority.UseBorderDashStyle = false;
        this.lblG4_20_2.StylePriority.UseBorders = false;
        this.lblG4_20_2.StylePriority.UseTextAlignment = false;
        this.lblG4_20_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel77
        // 
        this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(22.21251F, 289.2536F);
        this.xrLabel77.Name = "xrLabel77";
        this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel77.SizeF = new System.Drawing.SizeF(102.8035F, 24F);
        this.xrLabel77.StylePriority.UseBorders = false;
        this.xrLabel77.StylePriority.UseTextAlignment = false;
        this.xrLabel77.Text = "แหนบท้ายขวา ";
        this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG4_20_3
        // 
        this.ckbG4_20_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG4_20_3.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 314.2537F);
        this.ckbG4_20_3.Name = "ckbG4_20_3";
        this.ckbG4_20_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG4_20_3.StylePriority.UseBorders = false;
        this.ckbG4_20_3.StylePriority.UseTextAlignment = false;
        this.ckbG4_20_3.Text = " ถุงลม";
        this.ckbG4_20_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_19_2
        // 
        this.lblG4_19_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_19_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_19_2.LocationFloat = new DevExpress.Utils.PointFloat(126.3147F, 215.2536F);
        this.lblG4_19_2.Name = "lblG4_19_2";
        this.lblG4_19_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_19_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_19_2.StylePriority.UseBorderDashStyle = false;
        this.lblG4_19_2.StylePriority.UseBorders = false;
        this.lblG4_19_2.StylePriority.UseTextAlignment = false;
        this.lblG4_19_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel78
        // 
        this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 215.2536F);
        this.xrLabel78.Name = "xrLabel78";
        this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel78.SizeF = new System.Drawing.SizeF(97.4801F, 24F);
        this.xrLabel78.StylePriority.UseBorders = false;
        this.xrLabel78.StylePriority.UseTextAlignment = false;
        this.xrLabel78.Text = "แหนบกลางขวา";
        this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG4_19_3
        // 
        this.ckbG4_19_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG4_19_3.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 239.2536F);
        this.ckbG4_19_3.Name = "ckbG4_19_3";
        this.ckbG4_19_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG4_19_3.StylePriority.UseBorders = false;
        this.ckbG4_19_3.StylePriority.UseTextAlignment = false;
        this.ckbG4_19_3.Text = " ถุงลม";
        this.ckbG4_19_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel76
        // 
        this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 215.2536F);
        this.xrLabel76.Name = "xrLabel76";
        this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel76.SizeF = new System.Drawing.SizeF(29.29858F, 24.00009F);
        this.xrLabel76.StylePriority.UseBorders = false;
        this.xrLabel76.StylePriority.UseTextAlignment = false;
        this.xrLabel76.Text = "แผ่น";
        this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel71
        // 
        this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(9.712521F, 190.2536F);
        this.xrLabel71.Name = "xrLabel71";
        this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel71.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel71.StylePriority.UseBorders = false;
        this.xrLabel71.StylePriority.UseTextAlignment = false;
        this.xrLabel71.Text = "19. แหนบกลางซ้าย";
        this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_19_1
        // 
        this.lblG4_19_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_19_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_19_1.LocationFloat = new DevExpress.Utils.PointFloat(126.3147F, 190.2536F);
        this.lblG4_19_1.Name = "lblG4_19_1";
        this.lblG4_19_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_19_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_19_1.StylePriority.UseBorderDashStyle = false;
        this.lblG4_19_1.StylePriority.UseBorders = false;
        this.lblG4_19_1.StylePriority.UseTextAlignment = false;
        this.lblG4_19_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel75
        // 
        this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 190.2536F);
        this.xrLabel75.Name = "xrLabel75";
        this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel75.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel75.StylePriority.UseBorders = false;
        this.xrLabel75.StylePriority.UseTextAlignment = false;
        this.xrLabel75.Text = "แผ่น";
        this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel73
        // 
        this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 117.2535F);
        this.xrLabel73.Name = "xrLabel73";
        this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel73.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel73.StylePriority.UseBorders = false;
        this.xrLabel73.StylePriority.UseTextAlignment = false;
        this.xrLabel73.Text = "แผ่น";
        this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel74
        // 
        this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(162.4532F, 141.2535F);
        this.xrLabel74.Name = "xrLabel74";
        this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel74.SizeF = new System.Drawing.SizeF(29.29858F, 24.00009F);
        this.xrLabel74.StylePriority.UseBorders = false;
        this.xrLabel74.StylePriority.UseTextAlignment = false;
        this.xrLabel74.Text = "แผ่น";
        this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG4_18_3
        // 
        this.ckbG4_18_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG4_18_3.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 165.2536F);
        this.ckbG4_18_3.Name = "ckbG4_18_3";
        this.ckbG4_18_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG4_18_3.StylePriority.UseBorders = false;
        this.ckbG4_18_3.StylePriority.UseTextAlignment = false;
        this.ckbG4_18_3.Text = " ถุงลม";
        this.ckbG4_18_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_18_2
        // 
        this.lblG4_18_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_18_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_18_2.LocationFloat = new DevExpress.Utils.PointFloat(126.3147F, 141.2536F);
        this.lblG4_18_2.Name = "lblG4_18_2";
        this.lblG4_18_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_18_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_18_2.StylePriority.UseBorderDashStyle = false;
        this.lblG4_18_2.StylePriority.UseBorders = false;
        this.lblG4_18_2.StylePriority.UseTextAlignment = false;
        this.lblG4_18_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel65
        // 
        this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(9.712509F, 117.2536F);
        this.xrLabel65.Name = "xrLabel65";
        this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel65.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel65.StylePriority.UseBorders = false;
        this.xrLabel65.StylePriority.UseTextAlignment = false;
        this.xrLabel65.Text = "18. แหนบหน้าซ้าย ";
        this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel69
        // 
        this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 141.2536F);
        this.xrLabel69.Name = "xrLabel69";
        this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel69.SizeF = new System.Drawing.SizeF(97.4801F, 24F);
        this.xrLabel69.StylePriority.UseBorders = false;
        this.xrLabel69.StylePriority.UseTextAlignment = false;
        this.xrLabel69.Text = "แหนบหน้าขวา";
        this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_18_1
        // 
        this.lblG4_18_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_18_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_18_1.LocationFloat = new DevExpress.Utils.PointFloat(126.3148F, 117.2536F);
        this.lblG4_18_1.Name = "lblG4_18_1";
        this.lblG4_18_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_18_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_18_1.StylePriority.UseBorderDashStyle = false;
        this.lblG4_18_1.StylePriority.UseBorders = false;
        this.lblG4_18_1.StylePriority.UseTextAlignment = false;
        this.lblG4_18_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // lblG4_17_2
        // 
        this.lblG4_17_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_17_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_17_2.LocationFloat = new DevExpress.Utils.PointFloat(56.83453F, 93.25352F);
        this.lblG4_17_2.Name = "lblG4_17_2";
        this.lblG4_17_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_17_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_17_2.StylePriority.UseBorderDashStyle = false;
        this.lblG4_17_2.StylePriority.UseBorders = false;
        this.lblG4_17_2.StylePriority.UseTextAlignment = false;
        this.lblG4_17_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel70
        // 
        this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(90.97296F, 93.25365F);
        this.xrLabel70.Name = "xrLabel70";
        this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel70.SizeF = new System.Drawing.SizeF(38.74103F, 24.00003F);
        this.xrLabel70.StylePriority.UseBorders = false;
        this.xrLabel70.StylePriority.UseTextAlignment = false;
        this.xrLabel70.Text = "ปอนด์";
        this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel66
        // 
        this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(27.53596F, 93.25352F);
        this.xrLabel66.Name = "xrLabel66";
        this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel66.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel66.StylePriority.UseBorders = false;
        this.xrLabel66.StylePriority.UseTextAlignment = false;
        this.xrLabel66.Text = "ลม";
        this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel61
        // 
        this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(9.712521F, 69.25357F);
        this.xrLabel61.Multiline = true;
        this.xrLabel61.Name = "xrLabel61";
        this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel61.SizeF = new System.Drawing.SizeF(154.0861F, 24F);
        this.xrLabel61.StylePriority.UseBorders = false;
        this.xrLabel61.StylePriority.UseTextAlignment = false;
        this.xrLabel61.Text = "17. ขนาดยางล้อหลังเทเลอร์ขนาด \r\n";
        this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG4_17_1
        // 
        this.lblG4_17_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG4_17_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG4_17_1.LocationFloat = new DevExpress.Utils.PointFloat(163.7986F, 69.25356F);
        this.lblG4_17_1.Name = "lblG4_17_1";
        this.lblG4_17_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG4_17_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG4_17_1.StylePriority.UseBorderDashStyle = false;
        this.lblG4_17_1.StylePriority.UseBorders = false;
        this.lblG4_17_1.StylePriority.UseTextAlignment = false;
        this.lblG4_17_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // ckbG4_1
        // 
        this.ckbG4_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG4_1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 44.25357F);
        this.ckbG4_1.Name = "ckbG4_1";
        this.ckbG4_1.SizeF = new System.Drawing.SizeF(116.3147F, 25F);
        this.ckbG4_1.StylePriority.UseBorders = false;
        this.ckbG4_1.StylePriority.UseTextAlignment = false;
        this.ckbG4_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel57
        // 
        this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel57.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(10F, 9.999969F);
        this.xrLabel57.Name = "xrLabel57";
        this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel57.SizeF = new System.Drawing.SizeF(153.7986F, 24F);
        this.xrLabel57.StylePriority.UseBorders = false;
        this.xrLabel57.StylePriority.UseFont = false;
        this.xrLabel57.StylePriority.UseTextAlignment = false;
        this.xrLabel57.Text = "ตรวจยางรถตัวเทเลอร์และแหนบ";
        this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel95,
            this.lblG5_24_2,
            this.xrLabel97,
            this.xrLabel89,
            this.xrLabel92,
            this.lblG5_24_1,
            this.lblG5_23_2,
            this.xrLabel93,
            this.xrLabel91,
            this.xrLabel83,
            this.lblG5_23_1,
            this.xrLabel90,
            this.xrLabel88,
            this.lblG5_22_1,
            this.xrLabel87,
            this.lblG5_22_2,
            this.xrLabel85,
            this.lblG5_21_2,
            this.xrLabel86,
            this.xrLabel84,
            this.xrLabel81,
            this.lblG5_21_1,
            this.xrLabel72,
            this.ckbG5_22_3});
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.Weight = 0.99178991695374807D;
        // 
        // xrLabel95
        // 
        this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(186.9838F, 142.2538F);
        this.xrLabel95.Name = "xrLabel95";
        this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel95.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel95.StylePriority.UseBorders = false;
        this.xrLabel95.StylePriority.UseTextAlignment = false;
        this.xrLabel95.Text = "เมตร";
        this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_24_2
        // 
        this.lblG5_24_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_24_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_24_2.LocationFloat = new DevExpress.Utils.PointFloat(83.39935F, 167.2538F);
        this.lblG5_24_2.Name = "lblG5_24_2";
        this.lblG5_24_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_24_2.SizeF = new System.Drawing.SizeF(60.95496F, 24F);
        this.lblG5_24_2.StylePriority.UseBorderDashStyle = false;
        this.lblG5_24_2.StylePriority.UseBorders = false;
        this.lblG5_24_2.StylePriority.UseTextAlignment = false;
        this.lblG5_24_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel97
        // 
        this.xrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(145.3544F, 167.2537F);
        this.xrLabel97.Name = "xrLabel97";
        this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel97.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel97.StylePriority.UseBorders = false;
        this.xrLabel97.StylePriority.UseTextAlignment = false;
        this.xrLabel97.Text = "ลิตร";
        this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel89
        // 
        this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(10.37784F, 142.2538F);
        this.xrLabel89.Name = "xrLabel89";
        this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel89.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel89.StylePriority.UseBorders = false;
        this.xrLabel89.StylePriority.UseTextAlignment = false;
        this.xrLabel89.Text = "24. ยาวรวมทั้งหมด";
        this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel92
        // 
        this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(29.87595F, 167.2538F);
        this.xrLabel92.Name = "xrLabel92";
        this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel92.SizeF = new System.Drawing.SizeF(53.52344F, 24F);
        this.xrLabel92.StylePriority.UseBorders = false;
        this.xrLabel92.StylePriority.UseTextAlignment = false;
        this.xrLabel92.Text = "ความจุรวม ";
        this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_24_1
        // 
        this.lblG5_24_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_24_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_24_1.LocationFloat = new DevExpress.Utils.PointFloat(122.8778F, 142.2538F);
        this.lblG5_24_1.Name = "lblG5_24_1";
        this.lblG5_24_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_24_1.SizeF = new System.Drawing.SizeF(63.10599F, 24F);
        this.lblG5_24_1.StylePriority.UseBorderDashStyle = false;
        this.lblG5_24_1.StylePriority.UseBorders = false;
        this.lblG5_24_1.StylePriority.UseTextAlignment = false;
        this.lblG5_24_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // lblG5_23_2
        // 
        this.lblG5_23_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_23_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_23_2.LocationFloat = new DevExpress.Utils.PointFloat(186.3148F, 118.2538F);
        this.lblG5_23_2.Name = "lblG5_23_2";
        this.lblG5_23_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_23_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG5_23_2.StylePriority.UseBorderDashStyle = false;
        this.lblG5_23_2.StylePriority.UseBorders = false;
        this.lblG5_23_2.StylePriority.UseTextAlignment = false;
        this.lblG5_23_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel93
        // 
        this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(222.8778F, 118.2538F);
        this.xrLabel93.Name = "xrLabel93";
        this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel93.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel93.StylePriority.UseBorders = false;
        this.xrLabel93.StylePriority.UseTextAlignment = false;
        this.xrLabel93.Text = "ท่อ";
        this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel91
        // 
        this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(157.0163F, 118.2538F);
        this.xrLabel91.Name = "xrLabel91";
        this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel91.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel91.StylePriority.UseBorders = false;
        this.xrLabel91.StylePriority.UseTextAlignment = false;
        this.xrLabel91.Text = "นิ้ว";
        this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel83
        // 
        this.xrLabel83.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(10.33093F, 118.2538F);
        this.xrLabel83.Name = "xrLabel83";
        this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel83.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel83.StylePriority.UseBorders = false;
        this.xrLabel83.StylePriority.UseTextAlignment = false;
        this.xrLabel83.Text = "23. ท่อใต้ท้อง ขนาด ";
        this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_23_1
        // 
        this.lblG5_23_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_23_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_23_1.LocationFloat = new DevExpress.Utils.PointFloat(122.5F, 118.2538F);
        this.lblG5_23_1.Name = "lblG5_23_1";
        this.lblG5_23_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_23_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG5_23_1.StylePriority.UseBorderDashStyle = false;
        this.lblG5_23_1.StylePriority.UseBorders = false;
        this.lblG5_23_1.StylePriority.UseTextAlignment = false;
        this.lblG5_23_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel90
        // 
        this.xrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(29.87595F, 93.25367F);
        this.xrLabel90.Name = "xrLabel90";
        this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel90.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel90.StylePriority.UseBorders = false;
        this.xrLabel90.StylePriority.UseTextAlignment = false;
        this.xrLabel90.Text = "ยาว";
        this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel88
        // 
        this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(10.37784F, 69.25359F);
        this.xrLabel88.Name = "xrLabel88";
        this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel88.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel88.StylePriority.UseBorders = false;
        this.xrLabel88.StylePriority.UseTextAlignment = false;
        this.xrLabel88.Text = "22. ท่อร่วมทางจ่าย ";
        this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_22_1
        // 
        this.lblG5_22_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_22_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_22_1.LocationFloat = new DevExpress.Utils.PointFloat(122.8779F, 69.25359F);
        this.lblG5_22_1.Name = "lblG5_22_1";
        this.lblG5_22_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_22_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG5_22_1.StylePriority.UseBorderDashStyle = false;
        this.lblG5_22_1.StylePriority.UseBorders = false;
        this.lblG5_22_1.StylePriority.UseTextAlignment = false;
        this.lblG5_22_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel87
        // 
        this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(157.0163F, 69.25359F);
        this.xrLabel87.Name = "xrLabel87";
        this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel87.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel87.StylePriority.UseBorders = false;
        this.xrLabel87.StylePriority.UseTextAlignment = false;
        this.xrLabel87.Text = "นิ้ว";
        this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_22_2
        // 
        this.lblG5_22_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_22_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_22_2.LocationFloat = new DevExpress.Utils.PointFloat(59.17447F, 93.25369F);
        this.lblG5_22_2.Name = "lblG5_22_2";
        this.lblG5_22_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_22_2.SizeF = new System.Drawing.SizeF(41.2034F, 24F);
        this.lblG5_22_2.StylePriority.UseBorderDashStyle = false;
        this.lblG5_22_2.StylePriority.UseBorders = false;
        this.lblG5_22_2.StylePriority.UseTextAlignment = false;
        this.lblG5_22_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel85
        // 
        this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(102.1565F, 93.25375F);
        this.xrLabel85.Name = "xrLabel85";
        this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel85.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel85.StylePriority.UseBorders = false;
        this.xrLabel85.StylePriority.UseTextAlignment = false;
        this.xrLabel85.Text = "เมตร";
        this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_21_2
        // 
        this.lblG5_21_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_21_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_21_2.LocationFloat = new DevExpress.Utils.PointFloat(185.9838F, 45.25354F);
        this.lblG5_21_2.Name = "lblG5_21_2";
        this.lblG5_21_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_21_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG5_21_2.StylePriority.UseBorderDashStyle = false;
        this.lblG5_21_2.StylePriority.UseBorders = false;
        this.lblG5_21_2.StylePriority.UseTextAlignment = false;
        this.lblG5_21_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel86
        // 
        this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(222.8779F, 45.25354F);
        this.xrLabel86.Name = "xrLabel86";
        this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel86.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel86.StylePriority.UseBorders = false;
        this.xrLabel86.StylePriority.UseTextAlignment = false;
        this.xrLabel86.Text = "ท่อ";
        this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel84
        // 
        this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(156.6853F, 45.25354F);
        this.xrLabel84.Name = "xrLabel84";
        this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel84.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel84.StylePriority.UseBorders = false;
        this.xrLabel84.StylePriority.UseTextAlignment = false;
        this.xrLabel84.Text = "นิ้ว";
        this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel81
        // 
        this.xrLabel81.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(10.37784F, 45.25354F);
        this.xrLabel81.Name = "xrLabel81";
        this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel81.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel81.StylePriority.UseBorders = false;
        this.xrLabel81.StylePriority.UseTextAlignment = false;
        this.xrLabel81.Text = "21. ท่อจ่ายน้ำมันขนาด ";
        this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG5_21_1
        // 
        this.lblG5_21_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG5_21_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG5_21_1.LocationFloat = new DevExpress.Utils.PointFloat(122.5469F, 45.25354F);
        this.lblG5_21_1.Name = "lblG5_21_1";
        this.lblG5_21_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG5_21_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG5_21_1.StylePriority.UseBorderDashStyle = false;
        this.lblG5_21_1.StylePriority.UseBorders = false;
        this.lblG5_21_1.StylePriority.UseTextAlignment = false;
        this.lblG5_21_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel72
        // 
        this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel72.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(10.37784F, 9.999908F);
        this.xrLabel72.Name = "xrLabel72";
        this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel72.SizeF = new System.Drawing.SizeF(90F, 24F);
        this.xrLabel72.StylePriority.UseBorders = false;
        this.xrLabel72.StylePriority.UseFont = false;
        this.xrLabel72.StylePriority.UseTextAlignment = false;
        this.xrLabel72.Text = "ตรวจท่อจ่ายน้ำมัน";
        this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG5_22_3
        // 
        this.ckbG5_22_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG5_22_3.LocationFloat = new DevExpress.Utils.PointFloat(143.2482F, 93.25375F);
        this.ckbG5_22_3.Name = "ckbG5_22_3";
        this.ckbG5_22_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG5_22_3.StylePriority.UseBorders = false;
        this.ckbG5_22_3.StylePriority.UseTextAlignment = false;
        this.ckbG5_22_3.Text = " ไม่มี";
        this.ckbG5_22_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ckbG6_26_2,
            this.ckbG6_25_2,
            this.xrLabel20,
            this.xrLabel25,
            this.lblG6_26_1,
            this.lblG6_25_1,
            this.xrLabel21,
            this.xrLabel26,
            this.xrLabel22});
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Weight = 1.0971710298893951D;
        // 
        // ckbG6_26_2
        // 
        this.ckbG6_26_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG6_26_2.LocationFloat = new DevExpress.Utils.PointFloat(205.5052F, 69.25354F);
        this.ckbG6_26_2.Name = "ckbG6_26_2";
        this.ckbG6_26_2.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG6_26_2.StylePriority.UseBorders = false;
        this.ckbG6_26_2.StylePriority.UseTextAlignment = false;
        this.ckbG6_26_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG6_25_2
        // 
        this.ckbG6_25_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG6_25_2.LocationFloat = new DevExpress.Utils.PointFloat(205.5052F, 44.25355F);
        this.ckbG6_25_2.Name = "ckbG6_25_2";
        this.ckbG6_25_2.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG6_25_2.StylePriority.UseBorders = false;
        this.ckbG6_25_2.StylePriority.UseTextAlignment = false;
        this.ckbG6_25_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel20
        // 
        this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel20.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10.00012F, 9.999908F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel20.SizeF = new System.Drawing.SizeF(110F, 24F);
        this.xrLabel20.StylePriority.UseBorders = false;
        this.xrLabel20.StylePriority.UseFont = false;
        this.xrLabel20.StylePriority.UseTextAlignment = false;
        this.xrLabel20.Text = "วัดค่า Gas Detector";
        this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel25
        // 
        this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(154.3293F, 45.2536F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(51.17596F, 24F);
        this.xrLabel25.StylePriority.UseBorders = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.Text = "%LEL";
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG6_26_1
        // 
        this.lblG6_26_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG6_26_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG6_26_1.LocationFloat = new DevExpress.Utils.PointFloat(89.75F, 69.2536F);
        this.lblG6_26_1.Name = "lblG6_26_1";
        this.lblG6_26_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG6_26_1.SizeF = new System.Drawing.SizeF(64.57922F, 24F);
        this.lblG6_26_1.StylePriority.UseBorderDashStyle = false;
        this.lblG6_26_1.StylePriority.UseBorders = false;
        this.lblG6_26_1.StylePriority.UseTextAlignment = false;
        this.lblG6_26_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // lblG6_25_1
        // 
        this.lblG6_25_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG6_25_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG6_25_1.LocationFloat = new DevExpress.Utils.PointFloat(89.75012F, 45.2536F);
        this.lblG6_25_1.Name = "lblG6_25_1";
        this.lblG6_25_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG6_25_1.SizeF = new System.Drawing.SizeF(64.57916F, 24F);
        this.lblG6_25_1.StylePriority.UseBorderDashStyle = false;
        this.lblG6_25_1.StylePriority.UseBorders = false;
        this.lblG6_25_1.StylePriority.UseTextAlignment = false;
        this.lblG6_25_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel21
        // 
        this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.00006F, 45.2536F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(79.75006F, 24F);
        this.xrLabel21.StylePriority.UseBorders = false;
        this.xrLabel21.StylePriority.UseTextAlignment = false;
        this.xrLabel21.Text = "25. Gas1(HC) =";
        this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel26
        // 
        this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(154.3291F, 69.2536F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel26.SizeF = new System.Drawing.SizeF(51.17615F, 24F);
        this.xrLabel26.StylePriority.UseBorders = false;
        this.xrLabel26.StylePriority.UseTextAlignment = false;
        this.xrLabel26.Text = "%vol(O2)";
        this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel22
        // 
        this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10.00006F, 69.2536F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(79.75F, 24F);
        this.xrLabel22.StylePriority.UseBorders = false;
        this.xrLabel22.StylePriority.UseTextAlignment = false;
        this.xrLabel22.Text = "26. Gas2(O2) =";
        this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTable2
        // 
        this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12F, 226.3867F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(789.0684F, 273.8759F);
        this.xrTable2.StylePriority.UseBorders = false;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5});
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Weight = 1D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel36,
            this.xrLabel34,
            this.ckbG1_6,
            this.ckbG1_5,
            this.ckbG1_4,
            this.ckbG1_3,
            this.ckbG1_2,
            this.ckbG1_1,
            this.xrLabel33});
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Weight = 0.91103922719639241D;
        // 
        // xrLabel36
        // 
        this.xrLabel36.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(39.29856F, 195.3309F);
        this.xrLabel36.Name = "xrLabel36";
        this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel36.SizeF = new System.Drawing.SizeF(189.5342F, 24.00003F);
        this.xrLabel36.StylePriority.UseBorderDashStyle = false;
        this.xrLabel36.StylePriority.UseBorders = false;
        this.xrLabel36.StylePriority.UseTextAlignment = false;
        this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel34
        // 
        this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(10F, 195.3309F);
        this.xrLabel34.Name = "xrLabel34";
        this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
        this.xrLabel34.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel34.StylePriority.UseBorders = false;
        this.xrLabel34.StylePriority.UseTextAlignment = false;
        this.xrLabel34.Text = "อื่นๆ";
        this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_6
        // 
        this.ckbG1_6.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_6.LocationFloat = new DevExpress.Utils.PointFloat(10F, 170.3309F);
        this.ckbG1_6.Name = "ckbG1_6";
        this.ckbG1_6.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_6.StylePriority.UseBorders = false;
        this.ckbG1_6.StylePriority.UseTextAlignment = false;
        this.ckbG1_6.Text = "6. สายดินประจำรถ 2 จุด";
        this.ckbG1_6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_5
        // 
        this.ckbG1_5.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_5.LocationFloat = new DevExpress.Utils.PointFloat(10F, 145.3309F);
        this.ckbG1_5.Name = "ckbG1_5";
        this.ckbG1_5.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_5.StylePriority.UseBorders = false;
        this.ckbG1_5.StylePriority.UseTextAlignment = false;
        this.ckbG1_5.Text = "5. ท่อไอเสียไม่รั่ว , ไม่ผุ";
        this.ckbG1_5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_4
        // 
        this.ckbG1_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_4.LocationFloat = new DevExpress.Utils.PointFloat(10F, 120.3309F);
        this.ckbG1_4.Name = "ckbG1_4";
        this.ckbG1_4.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_4.StylePriority.UseBorders = false;
        this.ckbG1_4.StylePriority.UseTextAlignment = false;
        this.ckbG1_4.Text = "4. แชชชีย์ไม่ผุ";
        this.ckbG1_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_3
        // 
        this.ckbG1_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_3.LocationFloat = new DevExpress.Utils.PointFloat(10F, 95.33096F);
        this.ckbG1_3.Name = "ckbG1_3";
        this.ckbG1_3.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_3.StylePriority.UseBorders = false;
        this.ckbG1_3.StylePriority.UseTextAlignment = false;
        this.ckbG1_3.Text = "3. ถังดับเพลิงเคมี ขนาด 20 ปอนด์";
        this.ckbG1_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_2
        // 
        this.ckbG1_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_2.LocationFloat = new DevExpress.Utils.PointFloat(10F, 70.33095F);
        this.ckbG1_2.Name = "ckbG1_2";
        this.ckbG1_2.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_2.StylePriority.UseBorders = false;
        this.ckbG1_2.StylePriority.UseTextAlignment = false;
        this.ckbG1_2.Text = "2. แบตเตอร์รี่มีฝาฉนวนครอบ";
        this.ckbG1_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG1_1
        // 
        this.ckbG1_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG1_1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 45.33092F);
        this.ckbG1_1.Name = "ckbG1_1";
        this.ckbG1_1.SizeF = new System.Drawing.SizeF(218.8327F, 25F);
        this.ckbG1_1.StylePriority.UseBorders = false;
        this.ckbG1_1.StylePriority.UseTextAlignment = false;
        this.ckbG1_1.Text = "1. สภาพสายไฟ / คัทเอาท์ดี";
        this.ckbG1_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel33
        // 
        this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel33.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(10F, 10F);
        this.xrLabel33.Name = "xrLabel33";
        this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel33.SizeF = new System.Drawing.SizeF(135F, 23.99998F);
        this.xrLabel33.StylePriority.UseBorders = false;
        this.xrLabel33.StylePriority.UseFont = false;
        this.xrLabel33.StylePriority.UseTextAlignment = false;
        this.xrLabel33.Text = "ตรวจอุปกรณ์ความปลอดภัย";
        this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ckbG2_12_1,
            this.xrLabel52,
            this.lblG2_11_3,
            this.xrLabel42,
            this.xrLabel49,
            this.lblG2_10_3,
            this.xrLabel44,
            this.xrLabel46,
            this.lblG2_10_2,
            this.xrLabel45,
            this.lblG2_10_1,
            this.ckbG2_9_1,
            this.ckbG2_8_1,
            this.ckbG2_7_1,
            this.xrLabel43,
            this.xrLabel41,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel38,
            this.ckbG2_2,
            this.xrLabel37,
            this.ckbG2_1,
            this.lblG2_11_1,
            this.lblG2_11_2,
            this.xrLabel51});
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Weight = 0.99178980092739089D;
        // 
        // ckbG2_12_1
        // 
        this.ckbG2_12_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_12_1.LocationFloat = new DevExpress.Utils.PointFloat(192.0325F, 243.3309F);
        this.ckbG2_12_1.Name = "ckbG2_12_1";
        this.ckbG2_12_1.SizeF = new System.Drawing.SizeF(56.9892F, 25F);
        this.ckbG2_12_1.StylePriority.UseBorders = false;
        this.ckbG2_12_1.StylePriority.UseTextAlignment = false;
        this.ckbG2_12_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel52
        // 
        this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(40.43539F, 219.3309F);
        this.xrLabel52.Name = "xrLabel52";
        this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel52.SizeF = new System.Drawing.SizeF(42.96393F, 24.00003F);
        this.xrLabel52.StylePriority.UseBorders = false;
        this.xrLabel52.StylePriority.UseTextAlignment = false;
        this.xrLabel52.Text = "อยู่ด้าน";
        this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG2_11_3
        // 
        this.lblG2_11_3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_11_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_11_3.LocationFloat = new DevExpress.Utils.PointFloat(83.39929F, 219.3309F);
        this.lblG2_11_3.Name = "lblG2_11_3";
        this.lblG2_11_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_11_3.SizeF = new System.Drawing.SizeF(73.28592F, 23.99997F);
        this.lblG2_11_3.StylePriority.UseBorderDashStyle = false;
        this.lblG2_11_3.StylePriority.UseBorders = false;
        this.lblG2_11_3.StylePriority.UseTextAlignment = false;
        this.lblG2_11_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel42
        // 
        this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(10.37778F, 195.3309F);
        this.xrLabel42.Name = "xrLabel42";
        this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel42.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel42.StylePriority.UseBorders = false;
        this.xrLabel42.StylePriority.UseTextAlignment = false;
        this.xrLabel42.Text = "11.จรวดใส่สายยางขนาด";
        this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel49
        // 
        this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(156.6852F, 195.3309F);
        this.xrLabel49.Name = "xrLabel49";
        this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel49.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel49.StylePriority.UseBorders = false;
        this.xrLabel49.StylePriority.UseTextAlignment = false;
        this.xrLabel49.Text = "นิ้ว";
        this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG2_10_3
        // 
        this.lblG2_10_3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_10_3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_10_3.LocationFloat = new DevExpress.Utils.PointFloat(83.39932F, 171.3309F);
        this.lblG2_10_3.Name = "lblG2_10_3";
        this.lblG2_10_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_10_3.SizeF = new System.Drawing.SizeF(73.28595F, 23.99997F);
        this.lblG2_10_3.StylePriority.UseBorderDashStyle = false;
        this.lblG2_10_3.StylePriority.UseBorders = false;
        this.lblG2_10_3.StylePriority.UseTextAlignment = false;
        this.lblG2_10_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel44
        // 
        this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(40.43539F, 171.3309F);
        this.xrLabel44.Name = "xrLabel44";
        this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel44.SizeF = new System.Drawing.SizeF(42.96393F, 24.00003F);
        this.xrLabel44.StylePriority.UseBorders = false;
        this.xrLabel44.StylePriority.UseTextAlignment = false;
        this.xrLabel44.Text = "อยู่ด้าน";
        this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel46
        // 
        this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(222.8778F, 147.331F);
        this.xrLabel46.Name = "xrLabel46";
        this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel46.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel46.StylePriority.UseBorders = false;
        this.xrLabel46.StylePriority.UseTextAlignment = false;
        this.xrLabel46.Text = "ท่อ";
        this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG2_10_2
        // 
        this.lblG2_10_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_10_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_10_2.LocationFloat = new DevExpress.Utils.PointFloat(185.9838F, 147.331F);
        this.lblG2_10_2.Name = "lblG2_10_2";
        this.lblG2_10_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_10_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG2_10_2.StylePriority.UseBorderDashStyle = false;
        this.lblG2_10_2.StylePriority.UseBorders = false;
        this.lblG2_10_2.StylePriority.UseTextAlignment = false;
        this.lblG2_10_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel45
        // 
        this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(156.6853F, 147.3309F);
        this.xrLabel45.Name = "xrLabel45";
        this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel45.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel45.StylePriority.UseBorders = false;
        this.xrLabel45.StylePriority.UseTextAlignment = false;
        this.xrLabel45.Text = "นิ้ว";
        this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG2_10_1
        // 
        this.lblG2_10_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_10_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_10_1.LocationFloat = new DevExpress.Utils.PointFloat(122.5468F, 147.3309F);
        this.lblG2_10_1.Name = "lblG2_10_1";
        this.lblG2_10_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_10_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG2_10_1.StylePriority.UseBorderDashStyle = false;
        this.lblG2_10_1.StylePriority.UseBorders = false;
        this.lblG2_10_1.StylePriority.UseTextAlignment = false;
        this.lblG2_10_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // ckbG2_9_1
        // 
        this.ckbG2_9_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_9_1.LocationFloat = new DevExpress.Utils.PointFloat(165.9461F, 122.3309F);
        this.ckbG2_9_1.Name = "ckbG2_9_1";
        this.ckbG2_9_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG2_9_1.StylePriority.UseBorders = false;
        this.ckbG2_9_1.StylePriority.UseTextAlignment = false;
        this.ckbG2_9_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG2_8_1
        // 
        this.ckbG2_8_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_8_1.LocationFloat = new DevExpress.Utils.PointFloat(165.9461F, 97.33093F);
        this.ckbG2_8_1.Name = "ckbG2_8_1";
        this.ckbG2_8_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG2_8_1.StylePriority.UseBorders = false;
        this.ckbG2_8_1.StylePriority.UseTextAlignment = false;
        this.ckbG2_8_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG2_7_1
        // 
        this.ckbG2_7_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_7_1.LocationFloat = new DevExpress.Utils.PointFloat(165.9461F, 72.33094F);
        this.ckbG2_7_1.Name = "ckbG2_7_1";
        this.ckbG2_7_1.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG2_7_1.StylePriority.UseBorders = false;
        this.ckbG2_7_1.StylePriority.UseTextAlignment = false;
        this.ckbG2_7_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel43
        // 
        this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(10.37778F, 243.3309F);
        this.xrLabel43.Name = "xrLabel43";
        this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel43.SizeF = new System.Drawing.SizeF(181.6547F, 24.00003F);
        this.xrLabel43.StylePriority.UseBorders = false;
        this.xrLabel43.StylePriority.UseTextAlignment = false;
        this.xrLabel43.Text = "12. จรวดเจาะรู สามารถมองทะลุผ่านได้";
        this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel41
        // 
        this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(10.37778F, 147.3309F);
        this.xrLabel41.Name = "xrLabel41";
        this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel41.SizeF = new System.Drawing.SizeF(112.1691F, 24F);
        this.xrLabel41.StylePriority.UseBorders = false;
        this.xrLabel41.StylePriority.UseTextAlignment = false;
        this.xrLabel41.Text = "10. จรวดใส่น้ำมันขนาด";
        this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel40
        // 
        this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(10.37777F, 121.3309F);
        this.xrLabel40.Name = "xrLabel40";
        this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel40.SizeF = new System.Drawing.SizeF(146.3075F, 24F);
        this.xrLabel40.StylePriority.UseBorders = false;
        this.xrLabel40.StylePriority.UseTextAlignment = false;
        this.xrLabel40.Text = "9. มีน๊อตยึดฐานสามารถถอดได้";
        this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel39
        // 
        this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(10.37777F, 96.33093F);
        this.xrLabel39.Name = "xrLabel39";
        this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel39.SizeF = new System.Drawing.SizeF(146.3075F, 24F);
        this.xrLabel39.StylePriority.UseBorders = false;
        this.xrLabel39.StylePriority.UseTextAlignment = false;
        this.xrLabel39.Text = "8. ฐานจรวดไม่ปิดข้าง";
        this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel38
        // 
        this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(10.37778F, 72.33095F);
        this.xrLabel38.Name = "xrLabel38";
        this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel38.SizeF = new System.Drawing.SizeF(146.3075F, 24F);
        this.xrLabel38.StylePriority.UseBorders = false;
        this.xrLabel38.StylePriority.UseTextAlignment = false;
        this.xrLabel38.Text = "7. ฐานจรวดหนักไม่เกิน 1.5 หุน";
        this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG2_2
        // 
        this.ckbG2_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_2.LocationFloat = new DevExpress.Utils.PointFloat(108.9569F, 45.33096F);
        this.ckbG2_2.Name = "ckbG2_2";
        this.ckbG2_2.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG2_2.StylePriority.UseBorders = false;
        this.ckbG2_2.StylePriority.UseTextAlignment = false;
        this.ckbG2_2.Text = " ไม่ใช้งาน";
        this.ckbG2_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel37
        // 
        this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel37.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(10.37777F, 10F);
        this.xrLabel37.Name = "xrLabel37";
        this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel37.SizeF = new System.Drawing.SizeF(98.57913F, 24F);
        this.xrLabel37.StylePriority.UseBorders = false;
        this.xrLabel37.StylePriority.UseFont = false;
        this.xrLabel37.StylePriority.UseTextAlignment = false;
        this.xrLabel37.Text = "ตรวจอุปกรณ์จรวด";
        this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG2_1
        // 
        this.ckbG2_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG2_1.LocationFloat = new DevExpress.Utils.PointFloat(10.37778F, 45.33095F);
        this.ckbG2_1.Name = "ckbG2_1";
        this.ckbG2_1.SizeF = new System.Drawing.SizeF(98.57913F, 25F);
        this.ckbG2_1.StylePriority.UseBorders = false;
        this.ckbG2_1.StylePriority.UseTextAlignment = false;
        this.ckbG2_1.Text = " ใช้งาน(ใส่น้ำมัน)";
        this.ckbG2_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG2_11_1
        // 
        this.lblG2_11_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_11_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_11_1.LocationFloat = new DevExpress.Utils.PointFloat(122.5468F, 195.3309F);
        this.lblG2_11_1.Name = "lblG2_11_1";
        this.lblG2_11_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_11_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG2_11_1.StylePriority.UseBorderDashStyle = false;
        this.lblG2_11_1.StylePriority.UseBorders = false;
        this.lblG2_11_1.StylePriority.UseTextAlignment = false;
        this.lblG2_11_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // lblG2_11_2
        // 
        this.lblG2_11_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG2_11_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG2_11_2.LocationFloat = new DevExpress.Utils.PointFloat(185.9837F, 195.331F);
        this.lblG2_11_2.Name = "lblG2_11_2";
        this.lblG2_11_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG2_11_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG2_11_2.StylePriority.UseBorderDashStyle = false;
        this.lblG2_11_2.StylePriority.UseBorders = false;
        this.lblG2_11_2.StylePriority.UseTextAlignment = false;
        this.lblG2_11_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel51
        // 
        this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(222.8778F, 195.3309F);
        this.xrLabel51.Name = "xrLabel51";
        this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel51.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel51.StylePriority.UseBorders = false;
        this.xrLabel51.StylePriority.UseTextAlignment = false;
        this.xrLabel51.Text = "ท่อ";
        this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel67,
            this.xrLabel68,
            this.ckbG3_16_3,
            this.lblG3_16_1,
            this.xrLabel63,
            this.xrLabel64,
            this.lblG3_16_2,
            this.xrLabel62,
            this.lblG3_15_2,
            this.xrLabel60,
            this.lblG3_15_1,
            this.xrLabel54,
            this.xrLabel59,
            this.lblG3_14_1,
            this.xrLabel58,
            this.xrLabel56,
            this.xrLabel50,
            this.lblG3_14_2,
            this.xrLabel55,
            this.lblG3_13_2,
            this.xrLabel53,
            this.lblG3_13_1,
            this.xrLabel48,
            this.xrLabel47,
            this.ckbG3_14_3});
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Weight = 1.0971709718762164D;
        // 
        // xrLabel67
        // 
        this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(160.7408F, 148.3309F);
        this.xrLabel67.Name = "xrLabel67";
        this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel67.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel67.StylePriority.UseBorders = false;
        this.xrLabel67.StylePriority.UseTextAlignment = false;
        this.xrLabel67.Text = "แผ่น";
        this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel68
        // 
        this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(160.7407F, 172.331F);
        this.xrLabel68.Name = "xrLabel68";
        this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel68.SizeF = new System.Drawing.SizeF(29.29858F, 24.00009F);
        this.xrLabel68.StylePriority.UseBorders = false;
        this.xrLabel68.StylePriority.UseTextAlignment = false;
        this.xrLabel68.Text = "แผ่น";
        this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG3_16_3
        // 
        this.ckbG3_16_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG3_16_3.LocationFloat = new DevExpress.Utils.PointFloat(205.5054F, 148.3309F);
        this.ckbG3_16_3.Name = "ckbG3_16_3";
        this.ckbG3_16_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG3_16_3.StylePriority.UseBorders = false;
        this.ckbG3_16_3.StylePriority.UseTextAlignment = false;
        this.ckbG3_16_3.Text = " ถุงลม";
        this.ckbG3_16_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_16_1
        // 
        this.lblG3_16_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_16_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_16_1.LocationFloat = new DevExpress.Utils.PointFloat(126.6024F, 147.331F);
        this.lblG3_16_1.Name = "lblG3_16_1";
        this.lblG3_16_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_16_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_16_1.StylePriority.UseBorderDashStyle = false;
        this.lblG3_16_1.StylePriority.UseBorders = false;
        this.lblG3_16_1.StylePriority.UseTextAlignment = false;
        this.lblG3_16_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel63
        // 
        this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(10.00008F, 147.331F);
        this.xrLabel63.Name = "xrLabel63";
        this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel63.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel63.StylePriority.UseBorders = false;
        this.xrLabel63.StylePriority.UseTextAlignment = false;
        this.xrLabel63.Text = "16. แหนบหน้าซ้าย";
        this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel64
        // 
        this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(29.12225F, 172.331F);
        this.xrLabel64.Name = "xrLabel64";
        this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel64.SizeF = new System.Drawing.SizeF(97.4801F, 24F);
        this.xrLabel64.StylePriority.UseBorders = false;
        this.xrLabel64.StylePriority.UseTextAlignment = false;
        this.xrLabel64.Text = "แหนบหน้าขวา";
        this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_16_2
        // 
        this.lblG3_16_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_16_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_16_2.LocationFloat = new DevExpress.Utils.PointFloat(126.6024F, 172.331F);
        this.lblG3_16_2.Name = "lblG3_16_2";
        this.lblG3_16_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_16_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_16_2.StylePriority.UseBorderDashStyle = false;
        this.lblG3_16_2.StylePriority.UseBorders = false;
        this.lblG3_16_2.StylePriority.UseTextAlignment = false;
        this.lblG3_16_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel62
        // 
        this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(224.1777F, 123.331F);
        this.xrLabel62.Name = "xrLabel62";
        this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel62.SizeF = new System.Drawing.SizeF(38.74103F, 24.00003F);
        this.xrLabel62.StylePriority.UseBorders = false;
        this.xrLabel62.StylePriority.UseTextAlignment = false;
        this.xrLabel62.Text = "ปอนด์";
        this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_15_2
        // 
        this.lblG3_15_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_15_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_15_2.LocationFloat = new DevExpress.Utils.PointFloat(190.0393F, 123.331F);
        this.lblG3_15_2.Name = "lblG3_15_2";
        this.lblG3_15_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_15_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_15_2.StylePriority.UseBorderDashStyle = false;
        this.lblG3_15_2.StylePriority.UseBorders = false;
        this.lblG3_15_2.StylePriority.UseTextAlignment = false;
        this.lblG3_15_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel60
        // 
        this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(160.7407F, 123.331F);
        this.xrLabel60.Name = "xrLabel60";
        this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel60.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel60.StylePriority.UseBorders = false;
        this.xrLabel60.StylePriority.UseTextAlignment = false;
        this.xrLabel60.Text = "ลม";
        this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_15_1
        // 
        this.lblG3_15_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_15_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_15_1.LocationFloat = new DevExpress.Utils.PointFloat(126.6022F, 123.3309F);
        this.lblG3_15_1.Name = "lblG3_15_1";
        this.lblG3_15_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_15_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_15_1.StylePriority.UseBorderDashStyle = false;
        this.lblG3_15_1.StylePriority.UseBorders = false;
        this.lblG3_15_1.StylePriority.UseTextAlignment = false;
        this.lblG3_15_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel54
        // 
        this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 123.3309F);
        this.xrLabel54.Name = "xrLabel54";
        this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel54.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel54.StylePriority.UseBorders = false;
        this.xrLabel54.StylePriority.UseTextAlignment = false;
        this.xrLabel54.Text = "15. ยางแปดล้อหลังขนาด";
        this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel59
        // 
        this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(29.12222F, 98.33092F);
        this.xrLabel59.Name = "xrLabel59";
        this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel59.SizeF = new System.Drawing.SizeF(97.4801F, 24F);
        this.xrLabel59.StylePriority.UseBorders = false;
        this.xrLabel59.StylePriority.UseTextAlignment = false;
        this.xrLabel59.Text = "แหนบหน้าขวา";
        this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_14_1
        // 
        this.lblG3_14_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_14_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_14_1.LocationFloat = new DevExpress.Utils.PointFloat(126.6023F, 73.33096F);
        this.lblG3_14_1.Name = "lblG3_14_1";
        this.lblG3_14_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_14_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_14_1.StylePriority.UseBorderDashStyle = false;
        this.lblG3_14_1.StylePriority.UseBorders = false;
        this.lblG3_14_1.StylePriority.UseTextAlignment = false;
        this.lblG3_14_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel58
        // 
        this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(10.00008F, 73.33096F);
        this.xrLabel58.Name = "xrLabel58";
        this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel58.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel58.StylePriority.UseBorders = false;
        this.xrLabel58.StylePriority.UseTextAlignment = false;
        this.xrLabel58.Text = "14. แหนบหน้าซ้าย";
        this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel56
        // 
        this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(160.7408F, 73.33096F);
        this.xrLabel56.Name = "xrLabel56";
        this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel56.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel56.StylePriority.UseBorders = false;
        this.xrLabel56.StylePriority.UseTextAlignment = false;
        this.xrLabel56.Text = "แผ่น";
        this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel50
        // 
        this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(160.7408F, 98.33092F);
        this.xrLabel50.Name = "xrLabel50";
        this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel50.SizeF = new System.Drawing.SizeF(29.29858F, 24.00009F);
        this.xrLabel50.StylePriority.UseBorders = false;
        this.xrLabel50.StylePriority.UseTextAlignment = false;
        this.xrLabel50.Text = "แผ่น";
        this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_14_2
        // 
        this.lblG3_14_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_14_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_14_2.LocationFloat = new DevExpress.Utils.PointFloat(126.6024F, 97.33097F);
        this.lblG3_14_2.Name = "lblG3_14_2";
        this.lblG3_14_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_14_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_14_2.StylePriority.UseBorderDashStyle = false;
        this.lblG3_14_2.StylePriority.UseBorders = false;
        this.lblG3_14_2.StylePriority.UseTextAlignment = false;
        this.lblG3_14_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel55
        // 
        this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(224.1776F, 46.33104F);
        this.xrLabel55.Name = "xrLabel55";
        this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel55.SizeF = new System.Drawing.SizeF(38.74103F, 24.00003F);
        this.xrLabel55.StylePriority.UseBorders = false;
        this.xrLabel55.StylePriority.UseTextAlignment = false;
        this.xrLabel55.Text = "ปอนด์";
        this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_13_2
        // 
        this.lblG3_13_2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_13_2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_13_2.LocationFloat = new DevExpress.Utils.PointFloat(190.0393F, 46.33104F);
        this.lblG3_13_2.Name = "lblG3_13_2";
        this.lblG3_13_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_13_2.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_13_2.StylePriority.UseBorderDashStyle = false;
        this.lblG3_13_2.StylePriority.UseBorders = false;
        this.lblG3_13_2.StylePriority.UseTextAlignment = false;
        this.lblG3_13_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel53
        // 
        this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(160.7408F, 46.33099F);
        this.xrLabel53.Name = "xrLabel53";
        this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel53.SizeF = new System.Drawing.SizeF(29.29856F, 24.00003F);
        this.xrLabel53.StylePriority.UseBorders = false;
        this.xrLabel53.StylePriority.UseTextAlignment = false;
        this.xrLabel53.Text = "ลม";
        this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblG3_13_1
        // 
        this.lblG3_13_1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblG3_13_1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblG3_13_1.LocationFloat = new DevExpress.Utils.PointFloat(126.6024F, 46.33097F);
        this.lblG3_13_1.Name = "lblG3_13_1";
        this.lblG3_13_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblG3_13_1.SizeF = new System.Drawing.SizeF(34.13843F, 24F);
        this.lblG3_13_1.StylePriority.UseBorderDashStyle = false;
        this.lblG3_13_1.StylePriority.UseBorders = false;
        this.lblG3_13_1.StylePriority.UseTextAlignment = false;
        this.lblG3_13_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
        // 
        // xrLabel48
        // 
        this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(10.00012F, 46.33092F);
        this.xrLabel48.Name = "xrLabel48";
        this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel48.SizeF = new System.Drawing.SizeF(116.6022F, 24F);
        this.xrLabel48.StylePriority.UseBorders = false;
        this.xrLabel48.StylePriority.UseTextAlignment = false;
        this.xrLabel48.Text = "13. ยางสองล้อหน้าขนาด";
        this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel47
        // 
        this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel47.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(10.00006F, 10F);
        this.xrLabel47.Name = "xrLabel47";
        this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel47.SizeF = new System.Drawing.SizeF(174.0396F, 23.99998F);
        this.xrLabel47.StylePriority.UseBorders = false;
        this.xrLabel47.StylePriority.UseFont = false;
        this.xrLabel47.StylePriority.UseTextAlignment = false;
        this.xrLabel47.Text = "ตรวจยางรถหัวลากเทเลอร์และแหนบ";
        this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbG3_14_3
        // 
        this.ckbG3_14_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbG3_14_3.LocationFloat = new DevExpress.Utils.PointFloat(205.5054F, 73.33094F);
        this.ckbG3_14_3.Name = "ckbG3_14_3";
        this.ckbG3_14_3.SizeF = new System.Drawing.SizeF(83.07556F, 25F);
        this.ckbG3_14_3.StylePriority.UseBorders = false;
        this.ckbG3_14_3.StylePriority.UseTextAlignment = false;
        this.ckbG3_14_3.Text = " ถุงลม";
        this.ckbG3_14_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrTable1
        // 
        this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12F, 118.7752F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
        this.xrTable1.SizeF = new System.Drawing.SizeF(789.0684F, 100F);
        this.xrTable1.StylePriority.UseBorders = false;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3});
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Weight = 1D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel30,
            this.xrLabel35,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel29,
            this.xrLabel28,
            this.ckbContractType4,
            this.ckbContractType3,
            this.ckbContractType2,
            this.ckbContractType1,
            this.xrLabel27});
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Weight = 7.8906842041015626D;
        // 
        // xrLabel30
        // 
        this.xrLabel30.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sSendOilEnd, "Text", "")});
        this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(302.1799F, 66.99999F);
        this.xrLabel30.Name = "xrLabel30";
        this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel30.SizeF = new System.Drawing.SizeF(198.8309F, 23.99998F);
        this.xrLabel30.StylePriority.UseBorderDashStyle = false;
        this.xrLabel30.StylePriority.UseBorders = false;
        this.xrLabel30.StylePriority.UseTextAlignment = false;
        this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // sSendOilEnd
        // 
        this.sSendOilEnd.Name = "sSendOilEnd";
        // 
        // xrLabel35
        // 
        this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(9.999996F, 66.99999F);
        this.xrLabel35.Name = "xrLabel35";
        this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel35.SizeF = new System.Drawing.SizeF(46.45688F, 22.99998F);
        this.xrLabel35.StylePriority.UseBorders = false;
        this.xrLabel35.StylePriority.UseTextAlignment = false;
        this.xrLabel35.Text = "ต้นทาง";
        this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel32
        // 
        this.xrLabel32.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sSendOilStart, "Text", "")});
        this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(57.83459F, 66.99999F);
        this.xrLabel32.Name = "xrLabel32";
        this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
        this.xrLabel32.SizeF = new System.Drawing.SizeF(187.3021F, 23.99998F);
        this.xrLabel32.StylePriority.UseBorderDashStyle = false;
        this.xrLabel32.StylePriority.UseBorders = false;
        this.xrLabel32.StylePriority.UseTextAlignment = false;
        this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // sSendOilStart
        // 
        this.sSendOilStart.Name = "sSendOilStart";
        // 
        // xrLabel31
        // 
        this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(246.5145F, 66.99995F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(55.66541F, 23.00002F);
        this.xrLabel31.StylePriority.UseBorders = false;
        this.xrLabel31.Text = "ปลายทาง";
        // 
        // xrLabel29
        // 
        this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel29.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(10.00003F, 37.00001F);
        this.xrLabel29.Name = "xrLabel29";
        this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel29.SizeF = new System.Drawing.SizeF(85.92084F, 23.99998F);
        this.xrLabel29.StylePriority.UseBorders = false;
        this.xrLabel29.StylePriority.UseFont = false;
        this.xrLabel29.StylePriority.UseTextAlignment = false;
        this.xrLabel29.Text = "คลังที่เข้ารับน้ำมัน";
        this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel28
        // 
        this.xrLabel28.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(376.9189F, 7F);
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(124.0919F, 23.99999F);
        this.xrLabel28.StylePriority.UseBorderDashStyle = false;
        this.xrLabel28.StylePriority.UseBorders = false;
        this.xrLabel28.StylePriority.UseTextAlignment = false;
        this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbContractType4
        // 
        this.ckbContractType4.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbContractType4.LocationFloat = new DevExpress.Utils.PointFloat(309.7769F, 6F);
        this.ckbContractType4.Name = "ckbContractType4";
        this.ckbContractType4.SizeF = new System.Drawing.SizeF(67.14209F, 24.99999F);
        this.ckbContractType4.StylePriority.UseBorders = false;
        this.ckbContractType4.StylePriority.UseTextAlignment = false;
        this.ckbContractType4.Text = "ขนส่งอื่นๆ";
        this.ckbContractType4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbContractType3
        // 
        this.ckbContractType3.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbContractType3.LocationFloat = new DevExpress.Utils.PointFloat(251.9604F, 7F);
        this.ckbContractType3.Name = "ckbContractType3";
        this.ckbContractType3.SizeF = new System.Drawing.SizeF(56.35071F, 24.99999F);
        this.ckbContractType3.StylePriority.UseBorders = false;
        this.ckbContractType3.StylePriority.UseTextAlignment = false;
        this.ckbContractType3.Text = "รถลูกค้า";
        this.ckbContractType3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbContractType2
        // 
        this.ckbContractType2.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbContractType2.LocationFloat = new DevExpress.Utils.PointFloat(180.1295F, 7F);
        this.ckbContractType2.Name = "ckbContractType2";
        this.ckbContractType2.SizeF = new System.Drawing.SizeF(70.51434F, 24.99999F);
        this.ckbContractType2.StylePriority.UseBorders = false;
        this.ckbContractType2.StylePriority.UseTextAlignment = false;
        this.ckbContractType2.Text = "ขนส่ง COD";
        this.ckbContractType2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // ckbContractType1
        // 
        this.ckbContractType1.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.ckbContractType1.LocationFloat = new DevExpress.Utils.PointFloat(95.92087F, 7F);
        this.ckbContractType1.Name = "ckbContractType1";
        this.ckbContractType1.SizeF = new System.Drawing.SizeF(81.96939F, 24.99999F);
        this.ckbContractType1.StylePriority.UseBorders = false;
        this.ckbContractType1.StylePriority.UseTextAlignment = false;
        this.ckbContractType1.Text = "ขนส่งโอนคลัง";
        this.ckbContractType1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel27
        // 
        this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel27.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(10.00003F, 7F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel27.SizeF = new System.Drawing.SizeF(73.02155F, 23.99999F);
        this.xrLabel27.StylePriority.UseBorders = false;
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UseTextAlignment = false;
        this.xrLabel27.Text = "ประเภทสัญญา";
        this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel17
        // 
        this.xrLabel17.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.TU_CHASSIS")});
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(357.8922F, 88.5F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(187.6798F, 24F);
        this.xrLabel17.StylePriority.UseBorderDashStyle = false;
        this.xrLabel17.StylePriority.UseBorders = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel18
        // 
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(546.572F, 88.5F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel18.SizeF = new System.Drawing.SizeF(56.83459F, 24F);
        this.xrLabel18.StylePriority.UseTextAlignment = false;
        this.xrLabel18.Text = "รหัสรถ";
        this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel19
        // 
        this.xrLabel19.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.SCAR_NUM")});
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(603.4066F, 88.50001F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel19.SizeF = new System.Drawing.SizeF(198.8309F, 24F);
        this.xrLabel19.StylePriority.UseBorderDashStyle = false;
        this.xrLabel19.StylePriority.UseBorders = false;
        this.xrLabel19.StylePriority.UseTextAlignment = false;
        this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel14
        // 
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(11.55759F, 88.50001F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(113.6349F, 23.99998F);
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.Text = "ทะเบียนรถ(เทรลเลอร์)";
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel15
        // 
        this.xrLabel15.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.TU_NO")});
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(125.1925F, 88.50001F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(174.8651F, 23.99998F);
        this.xrLabel15.StylePriority.UseBorderDashStyle = false;
        this.xrLabel15.StylePriority.UseBorders = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel16
        // 
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(300.0576F, 88.5F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(56.83459F, 24F);
        this.xrLabel16.StylePriority.UseTextAlignment = false;
        this.xrLabel16.Text = "เลขแชชซีย์";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel13
        // 
        this.xrLabel13.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sBandCar, "Text", "")});
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(603.2374F, 62.51258F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(198.8309F, 24F);
        this.xrLabel13.StylePriority.UseBorderDashStyle = false;
        this.xrLabel13.StylePriority.UseBorders = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // sBandCar
        // 
        this.sBandCar.Name = "sBandCar";
        // 
        // xrLabel12
        // 
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(546.4028F, 62.51258F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(56.83459F, 24F);
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "ยี่ห้อรถ";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel11
        // 
        this.xrLabel11.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.VEH_CHASSIS")});
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(357.5144F, 62.51258F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(187.6798F, 24F);
        this.xrLabel11.StylePriority.UseBorderDashStyle = false;
        this.xrLabel11.StylePriority.UseBorders = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel10
        // 
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(299.6798F, 62.51258F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel10.SizeF = new System.Drawing.SizeF(56.83459F, 24F);
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.Text = "เลขแชชซีย์";
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel9
        // 
        this.xrLabel9.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.VEH_NO")});
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(112F, 62.51257F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel9.SizeF = new System.Drawing.SizeF(187.6798F, 24F);
        this.xrLabel9.StylePriority.UseBorderDashStyle = false;
        this.xrLabel9.StylePriority.UseBorders = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel8
        // 
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(12F, 62.51258F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(100F, 24F);
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.Text = "ทะเบียนรถ(หัวลาก)";
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel7
        // 
        this.xrLabel7.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sOldDateCheckWater, "Text", "")});
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(666.8165F, 37.51258F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(135.2518F, 24F);
        this.xrLabel7.StylePriority.UseBorderDashStyle = false;
        this.xrLabel7.StylePriority.UseBorders = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // sOldDateCheckWater
        // 
        this.sOldDateCheckWater.Name = "sOldDateCheckWater";
        // 
        // xrLabel6
        // 
        this.xrLabel6.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.SABBREVIATION")});
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(187.9389F, 37.51258F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(414.2986F, 24F);
        this.xrLabel6.StylePriority.UseBorderDashStyle = false;
        this.xrLabel6.StylePriority.UseBorders = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // lblDate
        // 
        this.lblDate.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash;
        this.lblDate.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.lblDate.LocationFloat = new DevExpress.Utils.PointFloat(51.29856F, 37.51258F);
        this.lblDate.Name = "lblDate";
        this.lblDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblDate.SizeF = new System.Drawing.SizeF(100.455F, 24F);
        this.lblDate.StylePriority.UseBorderDashStyle = false;
        this.lblDate.StylePriority.UseBorders = false;
        this.lblDate.StylePriority.UseTextAlignment = false;
        this.lblDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel4
        // 
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(603.2374F, 37.51258F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(63.57916F, 24F);
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.Text = "วัดน้ำเดิมเมื่อ";
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel3
        // 
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(152.6871F, 37.51258F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(35.25182F, 24F);
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "บริษัท";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel2
        // 
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(12F, 37.51258F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(39.29856F, 24F);
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "วันที่";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
        // 
        // xrLabel1
        // 
        this.xrLabel1.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Bold);
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(287.5F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(242.9856F, 23F);
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.StylePriority.UseTextAlignment = false;
        this.xrLabel1.Text = "แบบตรวจสภาพรถบรรทุกน้ำมันก่อนเข้าวัดน้ำ";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // PageFooter
        // 
        this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel175,
            this.xrLabel174});
        this.PageFooter.HeightF = 23F;
        this.PageFooter.Name = "PageFooter";
        // 
        // xrLabel175
        // 
        this.xrLabel175.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel175.LocationFloat = new DevExpress.Utils.PointFloat(701.0682F, 0F);
        this.xrLabel175.Name = "xrLabel175";
        this.xrLabel175.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel175.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel175.StylePriority.UseFont = false;
        this.xrLabel175.StylePriority.UseTextAlignment = false;
        this.xrLabel175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        // 
        // xrLabel174
        // 
        this.xrLabel174.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(12F, 0F);
        this.xrLabel174.Name = "xrLabel174";
        this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel174.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel174.StylePriority.UseFont = false;
        // 
        // dsInspectionCar1
        // 
        this.dsInspectionCar1.DataSetName = "dsInspectionCar";
        this.dsInspectionCar1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // dsInspectionCar2
        // 
        this.dsInspectionCar2.DataSetName = "dsInspectionCar";
        this.dsInspectionCar2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // rpt_InspectionCar
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter});
        this.Margins = new System.Drawing.Printing.Margins(10, 25, 22, 25);
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.sOldDateCheckWater,
            this.sBandCar,
            this.sSendOilStart,
            this.sSendOilEnd,
            this.sOpinion});
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsInspectionCar1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsInspectionCar2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
