﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Authentication;
using System.Web.Security;
using GemBox.Spreadsheet;
using System.Drawing;
using System.Linq;

public class PageBase : System.Web.UI.Page
{

    protected string MessEmailSuccess = "ส่ง Email ถึงผู้เกี่ยวข้อง สำเร็จ<br/ >";
    protected string MessEmailFail = "<span style=\"color:Red;\">ส่ง Email ถึงผู้เกี่ยวข้อง ไม่สำเร็จ !!!</span><br/ >";
    byte[] plaintextBytes;
    protected enum IS_ACTIVE_STATUS
    {
        Active = 1,               //ใช้งาน
        Inactive = 0,             //ไม่ใช้งาน
    }
    protected enum REQUIRE_TYPE
    {
        REQUIRE = 1, OPTIONAL = 2, DISABLE = 3
    }

    public enum ServerNames
    {
        TEST,PRODUCTION
    }
    
    public ServerNames ServerName
    {
        get
        {
            return (ServerNames)Session["ServerName"];
        }
        set
        {
            Session["ServerName"] = value;
        }
    }

    public string ColumnEmailName
    {
        get {
            return ServerName == ServerNames.TEST ? "EMAIL_TEST" : "EMAIL";
        }
    }
    public string DateFormat
    {
        get
        {
            
            return ConfigurationManager.AppSettings["DateFormat"];
        }
    }

    public string DateTimeFormat
    {
        get
        {
            return ConfigurationManager.AppSettings["DateTimeFormat"];
        }
    }

    public string ReplaceCharacters(string msg)
    {
        msg = msg.Replace("'", "\"");
        msg = msg.Replace(Environment.NewLine, "<br/>");
        msg = msg.Replace("\r", "<br/>");

        return msg;
    }

    protected DataTable GetUploadType(ref DropDownList ddlUploadType, string type)
    {
        DataTable dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(type);
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        return dtUploadType;
    }
    protected DataTable GetTableUpload()
    {
        DataTable dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");

        return dtUpload;
    }
    #region GetDataToSave
    /// <summary>
    /// ดึง ข้อมูลจาก TextBox หรือ DropDrown ต่างๆ ในหน้าจอ
    /// ตาม FieldType จากหน้าจอ Field.aspx
    /// </summary>
    /// <param name="FieldType"></param>
    /// <returns></returns>
    protected DataTable GetDataToSave(string FieldType)
    {
        DataTable dt = GetField(FieldType);
        DataTable dtData = new DataTable("DT");
        DataRow dr = dtData.NewRow();
        foreach (DataRow item in dt.Rows)
        {
            if (!dtData.Columns.Contains(item["COLUMN_NAME"] + string.Empty))
            {
                dtData.Columns.Add(item["COLUMN_NAME"] + string.Empty, typeof(string));
                dtData.AcceptChanges();
            }
            Control con = Master.FindControl("cph_Main");
            if (con != null)
            {
                con = con.FindControl(item["CONTROL_ID"] + string.Empty);
                if (con != null)
                {
                    if (con.GetType() == typeof(TextBox))
                    {
                        TextBox txt = (TextBox)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = txt.Text.Trim();
                    }
                    else if (con.GetType() == typeof(DropDownList))
                    {
                        DropDownList ddl = (DropDownList)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = ddl.SelectedValue;
                    }
                    else if (con.GetType() == typeof(RadioButtonList))
                    {
                        RadioButtonList rbl = (RadioButtonList)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = rbl.SelectedValue;
                    }
                    else if (con.GetType() == typeof(HiddenField))
                    {
                        HiddenField hid = (HiddenField)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = hid.Value;
                    }
                    else if (con.GetType() == typeof(CheckBox))
                    {
                        CheckBox cb = (CheckBox)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = cb.Checked ? (int)IS_ACTIVE_STATUS.Active + string.Empty : (int)IS_ACTIVE_STATUS.Inactive + string.Empty;
                    }
                    else if (con.GetType() == typeof(System.Web.UI.WebControls.Image))
                    {
                        System.Web.UI.WebControls.Image image = (System.Web.UI.WebControls.Image)con;
                        dr[item["COLUMN_NAME"] + string.Empty] = image.ImageUrl;
                    }
                }
            }

        }
        dtData.Rows.Add(dr);

        return dtData;
    }
    #endregion
    protected string EncodeQueryString(string str)
    {
        plaintextBytes = Encoding.UTF8.GetBytes(str);
        return MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
    }
    protected string[] DecodeQueryString(string str)
    {
        plaintextBytes = MachineKey.Decode(str, MachineKeyProtection.All);
        str = Encoding.UTF8.GetString(plaintextBytes);
        return str.Split('&');
    }
    #region SetField
    /// <summary>
    /// Set ค่า Field ที่หน้าจอเลย
    /// </summary>
    /// <param name="FieldType"></param>
    protected string SetField(string FieldType)
    {
        DataTable dt = GetField(FieldType);
        return SetField(dt, Mode.Edit.ToString(), true);
    }
    /// <summary>
    /// Set ค่า Field ที่หน้าจอ ตามเงื่อนไขการ Require
    /// </summary>
    /// <param name="FieldType"></param>
    /// <param name="FieldReference"></param>
    protected string SetField(string FieldType, string FieldReference)
    {
        DataTable dt = GetField(FieldType, FieldReference);
        return SetField(dt, Mode.Edit.ToString(), true);
    }

    protected string SetField(string FieldType, string FieldReference, string mode)
    {
        DataTable dt = GetField(FieldType, FieldReference);
        return SetField(dt, mode, true);
    }
    protected string SetField(DataTable dt)
    {
        return SetField(dt, Mode.Edit.ToString(), true);
    }
    protected string SetField(DataTable dt, bool isRequire)
    {
        return SetField(dt, Mode.Edit.ToString(), isRequire);
    }

    protected string SetField(DataTable dt, string mode, bool isRequire)
    {
        string MenuName = string.Empty;
        bool isEnabled = true;//จากหน้าจอ Require
        foreach (DataRow item in dt.Rows)
        {
            Control conLabel = Master.FindControl("cph_Main").FindControl(item["CONTROL_LABEL_ID"] + string.Empty);
            Control con = Master.FindControl("cph_Main").FindControl(item["CONTROL_ID"] + string.Empty);
            if (conLabel != null)
            {
                if (conLabel.GetType() == typeof(Label))
                {
                    Label lbl = (Label)conLabel;
                    lbl.Text = item["CONTROL_LABEL_NAME"] + string.Empty;
                    //if (SystemID == (int)SYSTEMNAME.VESSEL && !string.IsNullOrEmpty(item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                    //{
                    //    lbl.Text = item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                    //}

                    if (!item.IsNull("REQUIRE_TYPE") && item["REQUIRE_TYPE"] + string.Empty == (int)REQUIRE_TYPE.REQUIRE + string.Empty && isRequire)
                    {
                        lbl.Text += " *";
                        lbl.ForeColor = Color.Red;
                    }
                    else
                    {
                        lbl.ForeColor = Color.Black;
                    }
                }
            }
            if (con != null)
            {
                if (!item.IsNull("REQUIRE_TYPE") && item["REQUIRE_TYPE"] + string.Empty == (int)REQUIRE_TYPE.DISABLE + string.Empty)
                {
                    isEnabled = false;
                }
                else
                {
                    isEnabled = true;
                }
                if (con.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)con;
                    txt.Enabled = mode == Mode.View.ToString() ? false : isEnabled;
                }
                else if (con.GetType() == typeof(DropDownList))
                {
                    DropDownList ddl = (DropDownList)con;
                    ddl.Enabled = mode == Mode.View.ToString() ? false : isEnabled;


                }
                else if (con.GetType() == typeof(RadioButtonList))
                {
                    RadioButtonList rbl = (RadioButtonList)con;
                    rbl.Enabled = mode == Mode.View.ToString() ? false : isEnabled;


                }
                else if (con.GetType() == typeof(HiddenField))
                {

                }
                else if (con.GetType() == typeof(CheckBox))
                {
                    CheckBox cb = (CheckBox)con;
                    cb.Enabled = mode == Mode.View.ToString() ? false : isEnabled;

                }
                else if (con.GetType() == typeof(System.Web.UI.WebControls.Image))
                {
                    System.Web.UI.WebControls.Image image = (System.Web.UI.WebControls.Image)con;

                    image.Enabled = mode == Mode.View.ToString() ? false : isEnabled;
                }
            }
            if (dt.Columns.Contains("MENU_NAME"))
            {
                MenuName = item["MENU_NAME"] + string.Empty;
            }

        }
        return MenuName;
    }
    #endregion
    #region SetGridView
    /// <summary>
    /// Set Text Header ตาม DataTable จาก ข้อมูลที่ได้ในหน้าจอ Field.aspx
    /// โดยมีเงื่อนไขว่า IS_VIEW ต้องเป็น Active
    /// </summary>
    /// <param name="gvData"></param>
    /// <param name="dtField"></param>
    protected void SetGridView(ref GridView gvData, DataTable dtField)
    {
        DataRow[] drs = dtField.Select();
        if (drs.Any())
        {
            dtField = drs.CopyToDataTable();
            DataTable dtData = new DataTable();
            foreach (DataControlField col in gvData.Columns)
            {
                if (col is BoundField)
                {
                    BoundField bf = (BoundField)col;
                    drs = dtField.Select("COLUMN_NAME = '" + bf.DataField + "'");
                    if (drs.Any())
                    {
                        DataRow dr = drs[0];
                        bf.HeaderText = dr["CONTROL_LABEL_NAME"] + string.Empty;
                        if (!string.IsNullOrEmpty(dr["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                        {
                            bf.HeaderText = dr["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                        }
                        if (!dtData.Columns.Contains(bf.DataField))
                        {
                            dtData.Columns.Add(bf.DataField);
                        }
                    }
                }
                else if (col is TemplateField)
                {
                    TemplateField bf = (TemplateField)col;
                    drs = dtField.Select("COLUMN_NAME = '" + bf.AccessibleHeaderText + "'");
                    if (drs.Any())
                    {
                        DataRow dr = drs[0];
                        bf.HeaderText = dr["CONTROL_LABEL_NAME"] + string.Empty;
                        if (!string.IsNullOrEmpty(dr["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                        {
                            bf.HeaderText = dr["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                        }
                        if (!dtData.Columns.Contains(bf.AccessibleHeaderText))
                        {
                            dtData.Columns.Add(bf.AccessibleHeaderText);
                        }
                    }
                }
            }
            gvData.DataSource = dtData;
            gvData.DataBind();
        }

    }
    #endregion
    public string RemoveSpecialCharacters(string str)
    {
        return Regex.Replace(str, "[^a-zA-Z0-9<>_. ก-๙]+", "", RegexOptions.Compiled);
    }
    #region GetField
    /// <summary>
    /// กรณี ดึง Field มาครั้งแรกแบบไม่มีเงื่อนไข
    /// </summary>
    /// <param name="FieldType"></param>
    /// <returns></returns>
    protected DataTable GetField(string FieldType)
    {
        return RequireBLL.Instance.FieldSelect(FieldType);
    }
    /// <summary>
    /// กรณี ดึง Field แบบมีเงื่อนไข Reference เพื่อ Require Field
    /// </summary>
    /// <param name="FieldType"></param>
    /// <param name="FieldReference"></param>
    /// <returns></returns>
    protected DataTable GetField(string FieldType, string FieldReference)
    {
        return RequireBLL.Instance.RequireFieldSelect(FieldType, FieldReference);
    }
    #endregion
    #region SetFieldValue
    /// <summary>
    /// Set ค่า Field ที่หน้าจอเลย
    /// </summary>
    /// <param name="FieldType"></param>
    protected string SetFieldValue(string FieldType, DataTable dtData, string mode)
    {
        DataTable dt = GetField(FieldType);
        return SetFieldValue(dt, dtData, mode);
    }
    /// <summary>
    /// Set ค่า Field ที่หน้าจอ ตามเงื่อนไขการ Require
    /// </summary>
    /// <param name="FieldType"></param>
    /// <param name="FieldReference"></param>
    protected string SetFieldValue(string FieldType, string FieldReference, DataTable dtData, string mode)
    {
        DataTable dt = GetField(FieldType, FieldReference);
        return SetFieldValue(dt, dtData, mode);
    }
    /// <summary>
    /// Set ข้อมูลให้ TextBox หรือ DropDrown ต่างๆในหนา้จอ
    /// dt ข้อมูล Field จากหน้าจอ Field.aspx
    /// dtData ข้อมูลจากหน้าจอ
    /// mode Edit หรือ View
    /// </summary>
    /// <param name="dt"></param>
    /// <param name="dtData"></param>
    /// <param name="mode"></param>
    /// <returns></returns>
    protected string SetFieldValue(DataTable dt, DataTable dtData, string mode)
    {
        string MenuName = string.Empty;
        if (dtData != null)
        {
            DataRow dr = dtData.Rows[0];
            bool isEnabled = true;//จากหน้าจอ Require
            foreach (DataRow item in dt.Rows)
            {
                Control conLabel = Master.FindControl("cph_Main").FindControl(item["CONTROL_LABEL_ID"] + string.Empty);
                Control con = Master.FindControl("cph_Main").FindControl(item["CONTROL_ID"] + string.Empty);
                if (conLabel != null && conLabel.GetType() == typeof(Label))
                {
                    Label lbl = (Label)conLabel;
                    lbl.Text = item["CONTROL_LABEL_NAME"] + string.Empty;

                    if (!string.IsNullOrEmpty(item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                    {
                        lbl.Text = item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                    }
                    if (!item.IsNull("REQUIRE_TYPE") && item["REQUIRE_TYPE"] + string.Empty == (int)REQUIRE_TYPE.REQUIRE + string.Empty)
                    {
                        lbl.Text += " *";
                        lbl.ForeColor = Color.Red;
                    }
                    else
                    {
                        lbl.ForeColor = Color.Black;
                    }
                }
                if (con != null && dtData.Columns.Contains(item["COLUMN_NAME"] + string.Empty))
                {
                    if (!item.IsNull("REQUIRE_TYPE") && item["REQUIRE_TYPE"] + string.Empty == (int)REQUIRE_TYPE.DISABLE + string.Empty)
                    {
                        isEnabled = false;
                    }
                    else
                    {
                        isEnabled = true;
                    }
                    if (con.GetType() == typeof(Label))
                    {
                        Label txt = (Label)con;

                        if (dr[item["COLUMN_NAME"] + string.Empty].GetType() == typeof(DateTime))
                        {
                            txt.Text = Convert.ToDateTime(dr[item["COLUMN_NAME"] + string.Empty] + string.Empty).ToString("dd/mm/yyyy");
                        }
                        else
                        {
                            txt.Text = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;
                        }
                        txt.Enabled = mode == Mode.View.ToString() ? false : isEnabled;

                    }
                    else if (con.GetType() == typeof(TextBox))
                    {
                        TextBox txt = (TextBox)con;

                        if (dr[item["COLUMN_NAME"] + string.Empty].GetType() == typeof(DateTime))
                        {
                            txt.Text = Convert.ToDateTime(dr[item["COLUMN_NAME"] + string.Empty] + string.Empty).ToString("dd/mm/yyyy");
                        }
                        else
                        {
                            txt.Text = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;
                        }
                        txt.Enabled = mode == Mode.View.ToString() ? false : isEnabled;

                    }
                    else if (con.GetType() == typeof(DropDownList))
                    {
                        DropDownList ddl = (DropDownList)con;
                        if (ddl != null && ddl.Visible)
                        {
                            if (!dr.IsNull(item["COLUMN_NAME"] + string.Empty))
                                ddl.SelectedValue = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;
                            ddl.Enabled = mode == Mode.View.ToString() ? false : isEnabled;
                        }

                    }
                    else if (con.GetType() == typeof(RadioButtonList))
                    {
                        RadioButtonList rbl = (RadioButtonList)con;
                        if (rbl != null && rbl.Visible)
                        {
                            if (!dr.IsNull(item["COLUMN_NAME"] + string.Empty))
                                rbl.SelectedValue = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;
                            rbl.Enabled = mode == Mode.View.ToString() ? false : isEnabled;
                        }

                    }
                    else if (con.GetType() == typeof(HiddenField))
                    {
                        HiddenField hid = (HiddenField)con;
                        if (!dr.IsNull(item["COLUMN_NAME"] + string.Empty))
                            hid.Value = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;

                    }
                    else if (con.GetType() == typeof(CheckBox))
                    {
                        CheckBox cb = (CheckBox)con;
                        if (!dr.IsNull(item["COLUMN_NAME"] + string.Empty))
                            cb.Checked = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty == (int)IS_ACTIVE_STATUS.Active + string.Empty;
                        cb.Enabled = mode == Mode.View.ToString() ? false : isEnabled;

                    }
                    else if (con.GetType() == typeof(System.Web.UI.WebControls.Image))
                    {
                        System.Web.UI.WebControls.Image image = (System.Web.UI.WebControls.Image)con;
                        if (!dr.IsNull(item["COLUMN_NAME"] + string.Empty))
                            image.ImageUrl = dr[item["COLUMN_NAME"] + string.Empty] + string.Empty;
                        image.Enabled = mode == Mode.View.ToString() ? false : isEnabled;
                    }

                }
                MenuName = item["MENU_NAME"] + string.Empty;
            }
        }
        return MenuName;
    }
    #endregion
    #region CheckRequestFile
    /// <summary>
    /// ตรวจสอบว่า ไฟล์ที่ Request ได้ถูก Upload แล้วหรือไม่ ถ้า Upแล้ว จะ ติ๊กถูกที่ CheckBox
    /// </summary>
    /// <param name="dgvUploadRequestFile"></param>
    /// <param name="dtUploadRequestFile"></param>
    /// <param name="dtUpload"></param>
    protected void CheckRequestFile(ref GridView dgvUploadRequestFile, DataTable dtUploadRequestFile, DataTable dtUpload)
    {
        try
        {
            string UploadID = string.Empty;
            CheckBox chk = new CheckBox();

            if (dtUploadRequestFile != null)
            {
                for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                {
                    chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                    if (chk != null)
                        chk.Checked = false;
                }

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                    {
                        if (string.Equals(UploadID, dtUploadRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                        {
                            chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                            if (chk != null)
                                chk.Checked = true;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region Validate
    /// <summary>
    /// Validate ข้อมูลว่ามีการกรอกข้อมูลตามที่ Request ไว้หรือไม่
    /// จากหน้าจอ Request.aspx
    /// FieldType ได้มาจากหน้าจอ Field.aspx
    /// FieldReference ขึ้นอยู่กับแต่ละหน่อยจอว่ามี หลายเงื่อนไขหรือไม่ ถ้าไม่มีก็ส่ง string.Empty
    /// DataToSave ข้อมูลที่ได้จาก หน้าจอ ในรูปแบบ DataTable
    /// </summary>
    /// <param name="FieldType"></param>
    /// <param name="FieldReference"></param>
    /// <param name="DataToSave"></param>
    /// <returns></returns>
    protected string Validate(string FieldType, string FieldReference, DataTable DataToSave)
    {

        DataTable dtRequire = GetField(FieldType, FieldReference);
        string mess = string.Empty, CONTROL_LABEL_NAME = string.Empty;

        if (DataToSave != null && DataToSave.Rows.Count > 0)
        {
            DataRow dr = DataToSave.Rows[0];
            foreach (DataRow item in dtRequire.Rows)
            {
                if (item["REQUIRE_TYPE"] + string.Empty == (int)REQUIRE_TYPE.REQUIRE + string.Empty)
                {
                    if (string.IsNullOrEmpty(dr[item["COLUMN_NAME"] + string.Empty] + string.Empty))
                    {
                        CONTROL_LABEL_NAME = item["CONTROL_LABEL_NAME"] + string.Empty;
                        //if (SystemID == (int)SYSTEMNAME.VESSEL && !string.IsNullOrEmpty(item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                        //{
                        //    CONTROL_LABEL_NAME = item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                        //}
                        mess += string.Format("กรุณากรอกข้อมูล", CONTROL_LABEL_NAME) + "<br/>";
                    }
                }

            }
        }
        return mess;
    }

    /// <summary>
    /// ValidateFile จาก GridView เพราะมี CheckBox ตรวจสอบไว้อยู่แล้ว
    /// </summary>
    /// <param name="dgvUploadRequestFile"></param>
    /// <returns></returns>
    protected string ValidateFile(GridView dgvUploadRequestFile)
    {
        string mess = string.Empty;
        CheckBox chk = new CheckBox();
        for (int i = 0; i < dgvUploadRequestFile.Rows.Count; i++)
        {
            chk = (CheckBox)dgvUploadRequestFile.Rows[i].FindControl("chkUploaded");
            if (chk != null)
            {
                if (!chk.Checked)
                    mess += string.Format("กรุณาแนบเอกสาร", dgvUploadRequestFile.Rows[i].Cells[2].Text.Trim()) + "<br/>";
            }
        }

        return mess;
    }
    #endregion
    #region StartUpload
    /// <summary>
    /// Upload และแสดง ไฟล์ที่ Upload ใน Grid
    /// </summary>
    /// <param name="ddlUploadType"></param>
    /// <param name="fileUpload1"></param>
    /// <param name="dtUploadType"></param>
    /// <param name="dtUploadRequestFile"></param>
    /// <param name="dtUpload"></param>
    /// <param name="dgvUploadFile"></param>
    /// <param name="dgvUploadRequestFile"></param>
    /// <returns></returns>
    protected DataTable StartUpload(ref DropDownList ddlUploadType, FileUpload fileUpload1, DataTable dtUploadType, DataTable dtUploadRequestFile, DataTable dtUpload, ref GridView dgvUploadFile, ref GridView dgvUploadRequestFile)
    {
        ValidateUpload(ref ddlUploadType);

        if (fileUpload1.HasFile)
        {
            string FileNameUser = fileUpload1.PostedFile.FileName;
            string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

            ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload1.PostedFile.ContentLength, dtUploadType, ddlUploadType);

            string Path = this.CheckPath();
            fileUpload1.SaveAs(Path + "\\" + FileNameSystem);

            dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload, false);

            this.CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);

        }
        else
        {
            alertFail("ไม่มีไฟล์ที่ต้องการอัพโหลด");
        }
        return dtUpload;

    }
    #endregion
    #region ValidateUploadFile
    /// <summary>
    /// ตรวจสอบ ขนาดไฟล์และ นามสกุล
    /// </summary>
    /// <param name="ExtentionUser"></param>
    /// <param name="FileSize"></param>
    /// <param name="dtUploadType"></param>
    /// <param name="ddlUploadType"></param>
    protected void ValidateUploadFile(string ExtentionUser, int FileSize, DataTable dtUploadType, DropDownList ddlUploadType)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region GetUploadRequire
    /// <summary>
    /// Set ค่าให้ GridView ของ RequestFile ตาม UPLOAD_TYPE และ GridView ที่ส่งเข้ามา
    /// </summary>
    /// <param name="dgvUploadRequestFile"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    protected DataTable GetUploadRequire(ref GridView dgvUploadRequestFile, string type)
    {
        DataTable dtUploadRequestFile = UploadBLL.Instance.UploadRequestFileBLL(" AND M_REQUEST_FILE.UPLOAD_TYPE = '" + type + "'");
        GridViewHelper.BindGridView(ref dgvUploadRequestFile, dtUploadRequestFile, false);
        return dtUploadRequestFile;
    }
    #endregion

    #region ValidateUpload
    /// <summary>
    /// ValidateUpload ตรวจสอบว่าเลือก ประเภทไฟล์เอกสาร หรือไม่
    /// </summary>
    /// <param name="ddlUploadType"></param>
    protected void ValidateUpload(ref DropDownList ddlUploadType)
    {
        try
        {
            if (ddlUploadType.SelectedIndex == 0)
                throw new Exception(string.Format("กรุณาเลือกข้อมูล", "ประเภทไฟล์เอกสาร"));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    protected void ExportExcel(string FieldType, string FileTemplate, DataTable dtData, int StartRow, int StartColumn)
    {

        DataTable dt = GetField(FieldType);

        List<string> DataDelete = new List<string>();
        foreach (DataColumn item in dtData.Columns)
        {
            DataRow[] drs = dt.Select("IS_EXPORT = " + (int)IS_ACTIVE_STATUS.Active + " AND COLUMN_NAME = '" + item.ColumnName + "'");
            if (!drs.Any())
            {
                DataDelete.Add(item.ColumnName);
            }

        }
        foreach (string item in DataDelete)
        {
            if (dtData.Columns.Contains(item))
            {
                dtData.Columns.Remove(item);
            }
        }
        foreach (DataRow item in dt.Select("IS_EXPORT = " + (int)IS_ACTIVE_STATUS.Active + ""))
        {
            if (dtData.Columns.Contains(item["COLUMN_NAME"] + string.Empty))
            {

                int LISTNO = 0;
                int.TryParse((item["LISTNO"] + string.Empty), out LISTNO);
                dtData.Columns[item["COLUMN_NAME"] + string.Empty].SetOrdinal(LISTNO - 1);
                string CONTROL_LABEL_NAME = item["CONTROL_LABEL_NAME"] + string.Empty;
                //if (SystemID == (int)SYSTEMNAME.VESSEL && !string.IsNullOrEmpty(item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty))
                //{
                //    CONTROL_LABEL_NAME = item["CONTROL_LABEL_NAME_VESSEL"] + string.Empty;
                //}
                dtData.Columns[item["COLUMN_NAME"] + string.Empty].ColumnName = CONTROL_LABEL_NAME;
            }
        }
        SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
        ExcelFile workbook = new ExcelFile();
        ExcelWorksheet worksheet;
        if (string.IsNullOrEmpty(FileTemplate))
        {
            workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            worksheet = workbook.Worksheets["Export"];
        }
        else
        {
            workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/" + FileTemplate + ".xlsx"));
            worksheet = workbook.Worksheets["Sheets"];
        }

        worksheet.InsertDataTable(dtData, new InsertDataTableOptions(StartRow, StartColumn) { ColumnHeaders = false });
        for (int j = 0; j < dtData.Columns.Count; j++)
        {//Export Detail
            this.SetFormatCell(worksheet.Cells[0, j], dtData.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
            worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
            worksheet.Columns[j].AutoFit();
        }
        string Path = this.CheckPath();
        string FileName = FieldType + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

        workbook.Save(Path + "\\" + FileName);
        this.DownloadFile(Path, FileName);
    }
    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public DateTime? ConvertToDateNull(string strDate, string format = "dd/MM/yyyy")
    {
        try
        {
            return DateTime.ParseExact(strDate, format, new CultureInfo("en-US"), DateTimeStyles.None);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    protected enum Mode
    {
        View, Edit, Add
    }

    public DateTime? ConvertToDateTimeNull(string strDate)
    {
        try
        {
            return DateTime.ParseExact(strDate, DateTimeFormat, null, DateTimeStyles.None);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    bool invalid = false;

    public bool IsValidEmail(string strIn)
    {
        invalid = false;
        if (String.IsNullOrEmpty(strIn))
            return false;

        // Use IdnMapping class to convert Unicode domain names.
        try
        {
            strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                  RegexOptions.None);
        }
        catch (Exception)
        {
            return false;
        }


        if (invalid)
            return false;

        // Return true if strIn is in valid e-mail format.
        try
        {
            return Regex.IsMatch(strIn,
                  @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                  RegexOptions.IgnoreCase);
        }
        catch (Exception)
        {
            return false;
        }
    }
    private string DomainMapper(Match match)
    {
        // IdnMapping class with default property values.
        IdnMapping idn = new IdnMapping();

        string domainName = match.Groups[2].Value;
        try
        {
            domainName = idn.GetAscii(domainName);
        }
        catch (ArgumentException)
        {
            invalid = true;
        }
        return match.Groups[1].Value + domainName;
    }
       
    #region Permissions
    protected bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }
    #endregion


    #region alert

    public void alertSuccess(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertSuccess('" + ReplaceCharacters(msg) + "','')", true);
    }

    public void alertSuccess(string msg, string url)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertSuccess('" + ReplaceCharacters(msg) + "','" + url + "')", true);
    }

    public void alertFail(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "javascript:alertFail('" + ReplaceCharacters(msg) + "');", true);
    }

    #endregion

    #region InitialStatus
    public void InitialStatus(DropDownList ddlStatus, string Condition)
    {
        try
        {
            DataTable dtStatus = getStatusBLL.Instance.StatusSelectBLL(Condition);
            DropDownListHelper.BindDropDown(ref ddlStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true, AuthenResources.DropDownChooseText);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void InitialStatus(RadioButtonList radStatus, string Condition)
    {
        try
        {
            DataTable dtStatus = getStatusBLL.Instance.StatusSelectBLL(Condition);
            radStatus.DataSource = dtStatus;
            radStatus.DataValueField = "STATUS_VALUE";
            radStatus.DataTextField = "STATUS_NAME";
            radStatus.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    public static int findGridColumnByCSS(ref GridView grid, string cssClassName)
    {
        try
        {
            int i;
            // Warning!!! Optional parameters not supported
            // Warning!!! Optional parameters not supported
            string cssClass;
            for (i = 0; (i <= (grid.Columns.Count - 1)); i++)
            {
                cssClass = grid.Columns[i].ItemStyle.CssClass.ToLower();
                if (((cssClass != "") && cssClass.Contains(cssClassName.ToLower())))
                {
                    return i;
                }
            }

            // if cannot find in ItemStyle, try FooterStyle
            for (i = 0; (i <= (grid.Columns.Count - 1)); i++)
            {
                cssClass = grid.Columns[i].FooterStyle.CssClass.ToLower();
                if (((cssClass != "") && cssClass.Contains(cssClassName.ToLower())))
                {
                    return i;
                }
            }

            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public int UserID
    {
        get
        {
            return (int)ViewState["UserID"];
        }
        set
        {
            ViewState["UserID"] = value;
        }
    }

    #region CheckAuthen
    private void CheckAuthen()
    {
        try
        {
            string[] file = Request.CurrentExecutionFilePath.Split('/');
            string URL = "/" + file[file.Length - 2];
            URL = file[file.Length - 1];
            URL = URL.ToLower();

            if (URL.Contains("vehicle_edit.aspx")
                || URL.Contains("vendor_edit_p4.aspx")
                || URL.Contains("vendor_employee_add1.aspx")
                || URL.Contains("truck_history_view.aspx")
                || URL.Contains("truck_edit.aspx")
                || URL.Contains("default.aspx")
                || URL.Contains("default1.aspx")
                || URL.Contains("vehicle_detail.aspx")
                || URL.Contains("contract_add.aspx")
                || URL.Contains("service_add.aspx")
                || URL.Contains("confirmtruckdetailcheckivms.aspx")
                || URL.Contains("admin_topic_add.aspx")
                || URL.Contains("truck_add.aspx")
                || URL.Contains("contract_add.aspx")
                || URL.Contains("result-add.aspx")
                || URL.Contains("result-add2.aspx")
                || URL.Contains("result-add3.aspx")
                || URL.Contains("result-add4.aspx")
                || URL.Contains("result-add5.aspx")
                || URL.Contains("surprisecheckaddedit.aspx")
                || URL.Contains("surprisecheckcontract.aspx")
                || URL.Contains("surprisecheckcontractaddedit.aspx")
                || URL.Contains("surprisecheckcontractaddedittab2.aspx")
                )
            {
                CanRead = true;
                CanWrite = true;
                return;
            }
            ///////////////////////////////////////////////////

            //ตรวจสอบเอกสาร (รข.)
            URL = Regex.Replace(URL, "approve_add.aspx", "approve.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "service_add_other.aspx", "approve.aspx", RegexOptions.IgnoreCase);
            //รายการคำขอวัดน้ำ (มว.)
            URL = Regex.Replace(URL, "approve_add_mv.aspx", "approve_mv.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "service_add_mv.aspx", "approve_mv.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "result-add.aspx", "approve_mv.aspx", RegexOptions.IgnoreCase);
            //ข้อมูลวันหยุด มว.
            URL = Regex.Replace(URL, "admin_Holiday_add_mv.aspx", "admin_Holiday_lst_mv.aspx", RegexOptions.IgnoreCase);

            //อัตราค่าบริการ - ไม่มีหน้าย่อย
            //URL = Regex.Replace(URL,"service_cost.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            //จำนวนการให้บริการตรวจวัดน้ำ - ไม่มีหน้าย่อย
            //URL = Regex.Replace(URL,"order_car.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            //เพิ่มเอกสารพื้นฐาน - ไม่มีหน้าย่อย
            //URL = Regex.Replace(URL,"doc_set.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            //สรุปสถานะรถพร้อมปฎิบัติงาน
            URL = Regex.Replace(URL, "ConfirmTruckDetail.aspx", "ConfirmTruckConclude.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ConfirmTruckDetailCheckIVMS.aspx", "ConfirmTruckConclude.aspx", RegexOptions.IgnoreCase);

            //ยืนยันรถพร้อมปฎิบัติงาน - ไม่มีหน้าย่อย
            //vendor_confirm_contracts 

            //ยืนยันตามแผนการขนส่ง
            //PlanTransport

            //จัดสรรแผนการขนส่ง (แผนงาน)
            string rawURLStr = HttpContext.Current.Request.ServerVariables["query_string"];
            string paramBlStr = HttpUtility.ParseQueryString(HttpContext.Current.Request.RawUrl).Get("str");
            if (rawURLStr == "str=1" || paramBlStr == "1")
            {
                URL = Regex.Replace(URL, "OrderPlan.aspx", "OrderPlan.aspx?str=1", RegexOptions.IgnoreCase);
            }

            URL = Regex.Replace(URL, "OrderPlan_Jang.aspx", "OrderPlan.aspx?str=1", RegexOptions.IgnoreCase);

            //จัดสรรแผนการขนส่ง (ลงคิว-FIFO) - ไม่มีหน้าย่อย
            //book-car

            //สรุปแผนการขนส่ง - ไม่มีหน้าย่อย
            //transportation-plan

            //ตรวจสอบรายงานการขนส่ง - ไม่มีหน้าย่อย
            //ReportMonthly 

            //แจ้งเรื่องร้องเรียน
            URL = Regex.Replace(URL, "admin_Complain_add.aspx", "admin_Complain_lst.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_Complain_add_New.aspx", "admin_Complain_lst_New.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_Complain_add_Jang.aspx", "admin_Complain_lst.aspx", RegexOptions.IgnoreCase);

            //รถขนส่งเกิดอุบัติเหตุ
            URL = Regex.Replace(URL, "AccidentTab1.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentTab2.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentTab3.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentTab3_Vendor.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentTab4.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentConclude.aspx", "AccidentList.aspx", RegexOptions.IgnoreCase);

            string rawURL = HttpContext.Current.Request.ServerVariables["query_string"];
            string paramBl = HttpUtility.ParseQueryString(HttpContext.Current.Request.RawUrl).Get("bl");
            if (rawURL == "bl=blEdit" || paramBl == "blEdit")
            {    //แจ้งระงับ/Black list พขร.
                URL = Regex.Replace(URL, "vendor_employee.aspx", "vendor_employee.aspx?bl=blEdit", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add.aspx", "vendor_employee.aspx?bl=blEdit", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add1.aspx", "vendor_employee.aspx?bl=blEdit", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add2.aspx", "vendor_employee.aspx?bl=blEdit", RegexOptions.IgnoreCase);
            }
            if (rawURL == "bl=bl" || paramBl == "bl")
            {   //สรุปข้อมูล พขร. ถูกลงโทษ
                URL = Regex.Replace(URL, "vendor_employee.aspx", "vendor_employee.aspx?bl=bl", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add.aspx", "vendor_employee.aspx?bl=bl", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add1.aspx", "vendor_employee.aspx?bl=bl", RegexOptions.IgnoreCase);
                URL = Regex.Replace(URL, "vendor_employee_add2.aspx", "vendor_employee.aspx?bl=bl", RegexOptions.IgnoreCase);
            }

            //แจ้งแก้ไขปัญหาสภาพรถ
            //VendorConfirmChecktruck.aspx

            //พบปัญหาสภาพรถ (Surprise Check)
            URL = Regex.Replace(URL, "surpriseChk_lst.aspx", "surpriseChk_show.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "surpriseChk_ShowData.aspx", "surpriseChk_show.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "surpriseChk_ShowData.aspx", "surpriseChk_lst.aspx", RegexOptions.IgnoreCase);

            //พิจารณาผลการแก้ไขสภาพรถ
            if (URL != "confirmchecktruck.aspx" && URL != "vendorconfirmchecktruck.aspx")
            {
                URL = Regex.Replace(URL, "Checktruck.aspx", "ConfirmChecktruck.aspx", RegexOptions.IgnoreCase);
            }

            //แจ้งระงับการใช้งานรถ
            URL = Regex.Replace(URL, "Vehicle_Edit.aspx", "truck_status.aspx", RegexOptions.IgnoreCase);
            
            if (Session["CGROUP"] + "" == "0")
            {
                //แจ้งระงับการใช้งานรถ กรณีผู้ขนส่ง
                URL = Regex.Replace(URL, "Vehicle_Edit.aspx", "vendor_request.aspx", RegexOptions.IgnoreCase);
            }
            

            //แบบฟอร์มการตรวจสภาพรถ
            URL = Regex.Replace(URL, "admin_ChkList_add.aspx", "admin_ChkList_lst.aspx", RegexOptions.IgnoreCase);

            //พิจารณาคำขอแก้ไขข้อมูล 
            URL = Regex.Replace(URL, "vendor_edit_pk.aspx", "approve_pk.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "truck_history_view.aspx", "approve_pk.aspx", RegexOptions.IgnoreCase);

            //ฐานข้อมูลรถบรรทุก
            URL = Regex.Replace(URL, "TruckContractManagement.aspx", "truck_info.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "truck_add.aspx", "truck_info.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "truck_edit.aspx", "truck_info.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "truck_info.aspx", "truck_info.aspx", RegexOptions.IgnoreCase);
            //URL = Regex.Replace(URL,"Vehicle_Edit.aspx", "truck_info.aspx", RegexOptions.IgnoreCase);

            //ยื่นเรื่องขอจับคู่ TC
            URL = Regex.Replace(URL, "Vehicle_approve.aspx", "Vehicle.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Vehicle_detail.aspx", "Vehicle.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Vehicle_Edit.aspx", "Vehicle.aspx", RegexOptions.IgnoreCase);

            //ฐานข้อมูลพนักงาน
            URL = Regex.Replace(URL, "vendor_employee_add.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_add1.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_add2.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "employee_history.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_add_p4.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_edt.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_edt_p4.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "vendor_employee_edt_pk.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "EmployeeExpire.aspx", "vendor_employee.aspx", RegexOptions.IgnoreCase);

            string rawURLStrID = HttpContext.Current.Request.ServerVariables["query_string"];
            string paramStrID = HttpUtility.ParseQueryString(HttpContext.Current.Request.RawUrl).Get("StrID");
            if (rawURLStrID == "" || paramStrID == "")
            {
                //ฐานข้อมูลสัญญาจ้างฯ(ผกข.)
                URL = Regex.Replace(URL, "vendor_edit_p4.aspx", "Vendor_Detail.aspx", RegexOptions.IgnoreCase);
            }
            else
            {
                //ยื่นคำขอแก้ไขข้อมูล
                URL = Regex.Replace(URL, "vendor_edit_p4.aspx", "vendor_request.aspx", RegexOptions.IgnoreCase);
            }

            //ฐานข้อมูลสัญญาจ้างฯ 
            URL = Regex.Replace(URL, "contract_add.aspx", "contract_info.aspx", RegexOptions.IgnoreCase);

            //ฐานข้อมูลผู้ขนส่ง
            URL = Regex.Replace(URL, "vendor_edit.aspx", "vendor_list.aspx", RegexOptions.IgnoreCase);

            //ฐานข้อมูลสัญญาจ้างฯ - ไม่มีหน้าย่อย

            //เกณฑ์การหักคะแนนประเมินผล
            URL = Regex.Replace(URL, "admin_multiplier_add.aspx", "admin_multiplier_lst.aspx", RegexOptions.IgnoreCase);

            //การตัดคะแนนประเมินผลการทำงาน
            URL = Regex.Replace(URL, "AnnualScoreDetails.aspx", "annualscoremain.aspx", RegexOptions.IgnoreCase);

            //พิจารณาการยื่นเรื่องอุทธรณ์ - ไม่มีหน้าย่อย
            //admin_suppliant_lst กรณี ไม่ใช่ขนส่ง

            URL = Regex.Replace(URL, "admin_suppliant_Add.aspx", "admin_suppliant_lst_New.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_suppliant_PTT_Add.aspx", "admin_suppliant_lst_New.aspx", RegexOptions.IgnoreCase);
            //vendor_appeal_lst  กรณ๊ ขนส่ง

            //การประเมินผลการทำงานขนส่ง
            URL = Regex.Replace(URL, "Quarter_AlertVendor.aspx", "Quartery_Report.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "QuarterResult.aspx", "Quartery_Report.aspx", RegexOptions.IgnoreCase);            

            //ดัชนีชี้วัดประสิทธิภาพการทำงาน (KPI)
            URL = Regex.Replace(URL, "KPI_Evaluation_Kit.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Evaluation_Kit_AddEdit.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Formula.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Formula_AddEdit.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Mapping_Form.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Manual.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Manual_AddEdit.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Result.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Import.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Summary.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KPI_Summary_Detail.aspx", "KPI_Index.aspx", RegexOptions.IgnoreCase);

            //การประเมินผลการบริการงาน
            URL = Regex.Replace(URL, "questionnaire_form.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "questionnaire_compare.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_VisitForm_lst.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_VisitForm_add.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Admin_VisitForm_Config.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_VisitForm_config_add.aspx", "questionnaire.aspx", RegexOptions.IgnoreCase);

            //ประเมินผลการปฏิบัติงานรอบ 12 เดือน
            URL = Regex.Replace(URL, "AnnualReportHistory.aspx", "final-grade.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AnnualReportNotice.aspx", "final-grade.aspx", RegexOptions.IgnoreCase);

            //คทง. พิจารณาโทษ/อุทธรณ์
            URL = Regex.Replace(URL, "admin_committe_add.aspx", "admin_committe_lst.aspx", RegexOptions.IgnoreCase);

            //หัวข้อบทปรับ/ลงโทษตามสัญญา
            URL = Regex.Replace(URL, "admin_topic_add.aspx", "admin_topic_lst.aspx", RegexOptions.IgnoreCase);

            //เกณฑ์คะแนนที่ใช้พิจารณาเกรด
            URL = Regex.Replace(URL, "admin_grade_add.aspx", "admin_grade_lst.aspx", RegexOptions.IgnoreCase);

            //สร้างรหัส OTP - ไม่มีหน้าย่อย                      

            //ผู้มีสิทธิใช้งานระบบ
            URL = Regex.Replace(URL, "admin_user_add.aspx", "admin_user_lst.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_user_add1.aspx", "admin_user_lst.aspx", RegexOptions.IgnoreCase);

            //ข้อมูลพื้นฐานทั่วไป
            URL = Regex.Replace(URL, "RequireField.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "EmailTemplate.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "EmailTemplateAdd.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "EmailSchedule.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "EmailScheduleAdd.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ComplaintTopic.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ComplaintTopicAdd.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ComplaintTopicAddDetail.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ComplaintTopicEditDetail.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Position_Lst.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "WorkGroup.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Group.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "UploadFile.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "UploadFileAdd.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "MaintenanceTruck.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Quarter_Template.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AccidentType.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "AnnualReportEmailFormat.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Maintain.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            URL = Regex.Replace(URL, "final-grade_old.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Information_standard.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Information_standard_history.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "product.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            URL = Regex.Replace(URL, "AutoChangeStatusLog.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            URL = Regex.Replace(URL, "Department.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "DepartmentAddEdit.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "Division.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "DivisionAddEdit.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KnowledgeType.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KnowledgeTypeAddEdit.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ProductTagHead.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ProductTagHeadAddEdit.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ProductTagItem.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "ProductTagItemAddEdit.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "SpotDelivery.aspx", "menustandard.aspx", RegexOptions.IgnoreCase);

            //ข้อมูลคลัง ปตท. - ไม่มีหน้าย่อย
            URL = Regex.Replace(URL, "admin_TimeWindows_add.aspx", "admin_TimeWindows_lst.aspx", RegexOptions.IgnoreCase);

            //Time Windows
            URL = Regex.Replace(URL, "admin_TimeLine_add.aspx", "admin_TimeLine_lst.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_TimeLine_add.aspx", "admin_TimeLine_lst.aspx", RegexOptions.IgnoreCase);

            //Time Line - ไม่มีหน้าย่อย

            //Category
            URL = Regex.Replace(URL, "admin_category_add.aspx", "admin_category_lst.aspx", RegexOptions.IgnoreCase);

            //ข้อมูลวันหยุด ปตท.
            URL = Regex.Replace(URL, "admin_Holiday_add.aspx", "admin_Holiday_lst.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "admin_Holiday_add.aspx", "admin_Holiday_lst.aspx", RegexOptions.IgnoreCase);


            //กำหนดสิทธิ์การใช้งาน - ไม่มีหน้าย่อย

            //กำหนดกลุ่มผู็ใช้งาน
            URL = Regex.Replace(URL, "usergroupaddedit1.aspx", "usergroup1.aspx", RegexOptions.IgnoreCase);

            //ผู้มีสิทธิใช้งานระบบ(ใหม่)
            URL = Regex.Replace(URL, "UserAddEdit.aspx", "user.aspx", RegexOptions.IgnoreCase);

            //knowledgeManagement
            URL = Regex.Replace(URL, "KMAddEdit.aspx", "KM.aspx", RegexOptions.IgnoreCase);
            URL = Regex.Replace(URL, "KMView.aspx", "KM.aspx", RegexOptions.IgnoreCase);
                     
            DataTable dtAuthen = (DataTable)Session["dtAuthen"];
            CanRead = false;
            CanWrite = false;
            if (dtAuthen != null)
            {
                DataRow[] dr = dtAuthen.Select("VISIBLE = 1 AND MENU_URL_TMS LIKE '%" + URL + "'");
                if (dr.Length == 0)
                {
                    Response.Redirect("NotAuthorize.aspx");
                }
                else
                {
                    CanRead = string.Equals(dr[0]["READ"].ToString(), "1") ? true : false;
                    CanWrite = string.Equals(dr[0]["WRITE"].ToString(), "1") ? true : false;
                }
                // Session["CanRead"] = CanRead2;
                // ViewState["CanRead"] = CanRead;
                // bool CanWrite2 = string.Equals(dr[0]["WRITE"].ToString(), "1") ? true : false;      
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public bool CanRead
    {
        get
        {
            return (bool)ViewState["CanRead"];
        }
        set
        {
            ViewState["CanRead"] = value;
        }
    }

    public bool CanWrite
    {
        get
        {
            return (bool)ViewState["CanWrite"];
        }
        set
        {
            ViewState["CanWrite"] = value;
        }
    }
    

    #endregion

    #region OnLoad

    protected override void OnLoad(EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                this.CheckAuthen();
            }
            base.OnLoad(e);
        }
        catch (Exception ex)
        {
            alertFail("ระบบเกิดความผิดพลาด :" + RemoveSpecialCharacters(ex.Message));
            //throw new Exception(ex.Message);
        }
    }
    #endregion
}

