﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

/// <summary>
/// Summary description for AuthenHelper
/// </summary>
public class AuthenHelper
{
	public AuthenHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

	private static string sqlCon = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
	private static int loginFailCount = 0;
	private static string txtUsernameHist = "";

	public static DataTable CheckLogin(string Username, string Password, ref string Result, string DomainCheck)
	{
		try
		{
			string userDomain = "";
			string user = "";
			if(Username.Contains("\\"))
			{
				string[] users = Username.Split('\\');
				userDomain = users[0];
				user = users[1];
			}
			else
			{
				user = Username;
			}

			/* สำหรับทดสอบ user ที่เป็น คลัง Production ควร Comment Code ชุดนี้ออก */
            if (user == "klung")
			{
				DomainCheck = "1";
			}
			/**********************************/

			//int count;
			if(!String.IsNullOrEmpty(user + "") && !String.IsNullOrEmpty(Password + ""))
			{
				var EncodePass = STCrypt.encryptMD5(Password);
				DataTable dt = new DataTable();
				if(string.Equals(DomainCheck, "0"))//toey Edit
				{//Domain
					if(DomainHelper.CheckDomain(Username, Password))
					{
						dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME " +
															 " ,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE " +
															 " , USERGROUP_NAME  ,M_USERGROUP.USERGROUP_ID , IS_ADMIN  " +
															 "  FROM TUSER INNER JOIN M_USERGROUP ON TUSER.USERGROUP_ID = M_USERGROUP.USERGROUP_ID " +
															 "  WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(user) + "') AND CACTIVE = '1'");
						if(dt.Rows.Count == 0)
							throw new Exception("ไม่พบข้อมูลรหัสพนักงานในระบบ");
					}
				}
				else if(string.Equals(DomainCheck, "1"))//toey Edit
				{
					//Ping
					#region + Old Code +
					//dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(txtUsername.Text.Trim()) + "') AND SPASSWORD = '" + EncodePass + "' AND CACTIVE = '1'");                    
					#endregion

					#region + New Code +
					dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME," +
														 " CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE," +
														 "  USERGROUP_NAME ,M_USERGROUP.USERGROUP_ID , IS_ADMIN  " +
														 "  FROM TUSER " +
														 "  INNER JOIN M_USERGROUP ON TUSER.USERGROUP_ID = M_USERGROUP.USERGROUP_ID " +
														 "  WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(user) + "') " +
														 "  AND SPASSWORD = '" + EncodePass + "' AND CACTIVE = '1'");


					//if(dt.Rows.Count > 0)
					//	if(!string.Equals(dt.Rows[0]["IS_ADMIN"].ToString(), "0") && !string.Equals(dt.Rows[0]["IS_ADMIN"].ToString(), "6"))
					//		throw new Exception("ชื่อผู้ใช้งานของท่านไม่ได้อยู่ในกลุ่มผู้ขนส่งหรือลูกค้า กรุณาตรวจสอบ Domain");
					#endregion
				}
				else//toey Edit
				{
					throw new Exception("กรุณาเลือก Domain ก่อนทำการ Login");
				}
				if(dt.Rows.Count <= 0)
				{
					dt = CommonFunction.Get_Data(sqlCon, "SELECT SUID,SVENDORID,CGROUP,SUSERNAME,CACTIVE,SFIRSTNAME || ' ' || SLASTNAME AS SFULLNAME,CASE WHEN (SYSDATE - nvl(DCHANGEPASSWORD,SYSDATE)) > 90 THEN '1' ELSE '0' END CHKDATE  FROM TUSER WHERE LOWER(SUSERNAME) = LOWER('" + CommonFunction.ReplaceInjection(user) + "') AND SPASSWORD = '" + EncodePass + "'");
					if(dt.Rows.Count > 0)
					{
						#region CACTIVE
						switch(dt.Rows[0]["CACTIVE"] + "")
						{
							case "1":
								if(txtUsernameHist == user)
								{
									loginFailCount++;
								}
								else
								{
									txtUsernameHist = user;
									loginFailCount = 1;
								}
								Result = "Username/Password ของท่านไม่ถูกต้องครั้งที่ " + loginFailCount + " กรุณาตรวจสอบอีกครั้ง หากผิดพลาดเกิน 3 ครั้งระบบจะ Lock การใช้งานของ Username ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
								break;

							case "0":
								Result = "Username ของท่านไม่มีสิทธิ์เข้าระบบ ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
								break;
							case "2":
								Result = "Username ของท่านถูก Lock ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
								break;
						}
						#endregion
					}
					else
					{
						if(txtUsernameHist == user)
						{
							loginFailCount++;
						}
						else
						{
							txtUsernameHist = user;
							loginFailCount = 1;
						}
						Result = "Username/Password ของท่านไม่ถูกต้องครั้งที่ " + loginFailCount + " กรุณาตรวจสอบอีกครั้ง หากผิดพลาดเกิน 3 ครั้งระบบจะ Lock การใช้งานของ Username ให้ท่านติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
					}
					//lblErrorMsg.ClientVisible = true;
					//CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','" + Result + "');");
				}
				else
				{
					loginFailCount = 0;
					return dt;
				}
				if(loginFailCount >= 3)
				{
					Result = "Username ของท่านถูกล๊อคแล้ว กรุณาติดต่อผู้ดูแลระบบเพื่อเปิดใช้งานใหม่";
					//lblErrorMsg.ClientVisible = true;
					//CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','" + Result + "');");
					//function lock userid
					LockUsername(user);
				}
			}
			else
			{
				throw new Exception("กรุณากรอกข้อมูล Username หรือ Password ให้ครบถ้วน");
			}
			return null;
		}
		catch(Exception ex)
		{
			string result = string.Empty;
			if(ex.Message == "The user name or password is incorrect.\r\n" || ex.Message == "Logon failure: unknown user name or bad password.\r\n")
				result = " Username หรือ Password ไม่ถูกต้องกรุณาตรวจสอบอีกครั้ง";
			else
				result = ex.Message;

			throw new Exception(result);
		}
	}

	private static void LockUsername(string _Username)
	{
		using(OracleConnection con = new OracleConnection(sqlCon))
		{
			con.Open();
			string sql = "UPDATE TUSER SET CACTIVE = '2' WHERE SUSERNAME = '" + _Username + "'";
			using(OracleCommand com = new OracleCommand(sql, con))
			{
				com.ExecuteNonQuery();
			}
		}

		//UserTrace trace = new UserTrace(this, sqlCon);
		//trace.SUID = _Username + "";
		//trace.SMENUID = "100";
		//trace.SCREATE = _Username + "";
		//trace.STYPE = "F";
		//trace.SDESCRIPTION = "ป้อน Password  ผิดเกิน 3 ครั้ง";
		//trace.SREFERENTID = "";
		//trace.Insert();
	}
}