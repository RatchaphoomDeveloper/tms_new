using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for TSHIPMENT
/// </summary>
public class TSHIPMENT
{
    public static OracleConnection conn;
    public static Page page;
    //public TSHIPMENT() { }
    public TSHIPMENT(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }
    #region Data
    private string fSHIPMENT_NO;
    private string fSHIPMENT_ITEM;
    private string fDELIVERY_NO;
    private string fDELIVERY_ITEM;
    private string fCUSTOMER_NO;
    private string fSHIP_TO;
    private string fPLANT;
    private string fMATERIAL_NO;
    private string fMAT_FRT_GRP;
    private string fLOAD_CONF;
    private string fLOAD_CONF_UNIT;
    private string fLOAD_BASE;
    private string fLOAD_BASE_UNIT;
    private string fDLV;
    private string fDLV_UNIT;
    private string fDLV_BASE;
    private string fDLV_BASE_UNIT;
    private string fREFERENCE_DOC;
    private string fREF_DOC_ITEM;
    private string fDATETIME_STAMP;
    private string fRETURN_FLAG;
    private string fDEL_FLAG;
    private string fDBSYSDATE;
    private string fVEH_HEAD;
    private string fVEH_TRAIL;
    private string fSHIPMENT_STS;
    #endregion
    #region Property
    public string SHIPMENT_NO
    {
        get { return this.fSHIPMENT_NO; }
        set { this.fSHIPMENT_NO = value; }
    }
    public string SHIPMENT_ITEM
    {
        get { return this.fSHIPMENT_ITEM; }
        set { this.fSHIPMENT_ITEM = value; }
    }
    public string DELIVERY_NO
    {
        get { return this.fDELIVERY_NO; }
        set { this.fDELIVERY_NO = value; }
    }
    public string DELIVERY_ITEM
    {
        get { return this.fDELIVERY_ITEM; }
        set { this.fDELIVERY_ITEM = value; }
    }
    public string CUSTOMER_NO
    {
        get { return this.fCUSTOMER_NO; }
        set { this.fCUSTOMER_NO = value; }
    }
    public string SHIP_TO
    {
        get { return this.fSHIP_TO; }
        set { this.fSHIP_TO = value; }
    }
    public string PLANT
    {
        get { return this.fPLANT; }
        set { this.fPLANT = value; }
    }
    public string MATERIAL_NO
    {
        get { return this.fMATERIAL_NO; }
        set { this.fMATERIAL_NO = value; }
    }
    public string MAT_FRT_GRP
    {
        get { return this.fMAT_FRT_GRP; }
        set { this.fMAT_FRT_GRP = value; }
    }
    public string LOAD_CONF
    {
        get { return this.fLOAD_CONF; }
        set { this.fLOAD_CONF = value; }
    }
    public string LOAD_CONF_UNIT
    {
        get { return this.fLOAD_CONF_UNIT; }
        set { this.fLOAD_CONF_UNIT = value; }
    }
    public string LOAD_BASE
    {
        get { return this.fLOAD_BASE; }
        set { this.fLOAD_BASE = value; }
    }
    public string LOAD_BASE_UNIT
    {
        get { return this.fLOAD_BASE_UNIT; }
        set { this.fLOAD_BASE_UNIT = value; }
    }
    public string DLV
    {
        get { return this.fDLV; }
        set { this.fDLV = value; }
    }
    public string DLV_UNIT
    {
        get { return this.fDLV_UNIT; }
        set { this.fDLV_UNIT = value; }
    }
    public string DLV_BASE
    {
        get { return this.fDLV_BASE; }
        set { this.fDLV_BASE = value; }
    }
    public string DLV_BASE_UNIT
    {
        get { return this.fDLV_BASE_UNIT; }
        set { this.fDLV_BASE_UNIT = value; }
    }
    public string REFERENCE_DOC
    {
        get { return this.fREFERENCE_DOC; }
        set { this.fREFERENCE_DOC = value; }
    }
    public string REF_DOC_ITEM
    {
        get { return this.fREF_DOC_ITEM; }
        set { this.fREF_DOC_ITEM = value; }
    }
    public string DATETIME_STAMP
    {
        get { return this.fDATETIME_STAMP; }
        set { this.fDATETIME_STAMP = value; }
    }
    public string RETURN_FLAG
    {
        get { return this.fRETURN_FLAG; }
        set { this.fRETURN_FLAG = value; }
    }
    public string DEL_FLAG
    {
        get { return this.fDEL_FLAG; }
        set { this.fDEL_FLAG = value; }
    }
    public string DBSYSDATE
    {
        get { return this.fDBSYSDATE; }
        set { this.fDBSYSDATE = value; }
    }
    public string VEH_HEAD
    {
        get { return this.fVEH_HEAD; }
        set { this.fVEH_HEAD = value; }
    }
    public string VEH_TRAIL
    {
        get { return this.fVEH_TRAIL; }
        set { this.fVEH_TRAIL = value; }
    }
    public string SHIPMENT_STS
    {
        get { return this.fSHIPMENT_STS; }
        set { this.fSHIPMENT_STS = value; }
    }
    #endregion
    #region Method
    public int Insert()
    {
        int result = -1;
        //try
        //{
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("IUTSHIPMENT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("I_SHIPMENT_NO", SHIPMENT_NO);
            cmd.Parameters.AddWithValue("I_SHIPMENT_ITEM", SHIPMENT_ITEM);
            cmd.Parameters.AddWithValue("I_DELIVERY_NO", DELIVERY_NO);
            cmd.Parameters.AddWithValue("I_DELIVERY_ITEM", DELIVERY_ITEM);
            cmd.Parameters.AddWithValue("I_CUSTOMER_NO", CUSTOMER_NO);
            cmd.Parameters.AddWithValue("I_SHIP_TO", SHIP_TO);
            cmd.Parameters.AddWithValue("I_PLANT", PLANT);
            cmd.Parameters.AddWithValue("I_MATERIAL_NO", MATERIAL_NO);
            cmd.Parameters.AddWithValue("I_MAT_FRT_GRP", MAT_FRT_GRP);
            cmd.Parameters.AddWithValue("I_LOAD_CONF", LOAD_CONF);
            cmd.Parameters.AddWithValue("I_LOAD_CONF_UNIT", LOAD_CONF_UNIT);
            cmd.Parameters.AddWithValue("I_LOAD_BASE", LOAD_BASE);
            cmd.Parameters.AddWithValue("I_LOAD_BASE_UNIT", LOAD_BASE_UNIT);
            cmd.Parameters.AddWithValue("I_DLV", DLV);
            cmd.Parameters.AddWithValue("I_DLV_UNIT", DLV_UNIT);
            cmd.Parameters.AddWithValue("I_DLV_BASE", DLV_BASE);
            cmd.Parameters.AddWithValue("I_DLV_BASE_UNIT", DLV_BASE_UNIT);
            cmd.Parameters.AddWithValue("I_REFERENCE_DOC", REFERENCE_DOC);
            cmd.Parameters.AddWithValue("I_REF_DOC_ITEM", REF_DOC_ITEM);
            cmd.Parameters.AddWithValue("I_DATETIME_STAMP", DATETIME_STAMP);
            cmd.Parameters.AddWithValue("I_RETURN_FLAG", RETURN_FLAG);
            cmd.Parameters.AddWithValue("I_DEL_FLAG", DEL_FLAG);
            cmd.Parameters.AddWithValue("I_DBSYSDATE", DBSYSDATE);
            cmd.Parameters.AddWithValue("I_VEH_HEAD", VEH_HEAD);
            cmd.Parameters.AddWithValue("I_VEH_TRAIL", VEH_TRAIL);
            cmd.Parameters.AddWithValue("I_SHIPMENT_STS", SHIPMENT_STS);
            
            result = cmd.ExecuteNonQuery();
        //}
        //catch (Exception err)
        //{
        //    if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
        //    {
        //        page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
        //        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
        //    }
        //}
        //finally
        //{
            
        //}
        return result;
    }
    public int Update()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("UTSHIPMENT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SHIPMENT_NO", SHIPMENT_NO);
            cmd.Parameters.AddWithValue("@SHIPMENT_ITEM", SHIPMENT_ITEM);
            cmd.Parameters.AddWithValue("@DELIVERY_NO", DELIVERY_NO);
            cmd.Parameters.AddWithValue("@DELIVERY_ITEM", DELIVERY_ITEM);
            cmd.Parameters.AddWithValue("@CUSTOMER_NO", CUSTOMER_NO);
            cmd.Parameters.AddWithValue("@SHIP_TO", SHIP_TO);
            cmd.Parameters.AddWithValue("@PLANT", PLANT);
            cmd.Parameters.AddWithValue("@MATERIAL_NO", MATERIAL_NO);
            cmd.Parameters.AddWithValue("@MAT_FRT_GRP", MAT_FRT_GRP);
            cmd.Parameters.AddWithValue("@LOAD_CONF", LOAD_CONF);
            cmd.Parameters.AddWithValue("@LOAD_CONF_UNIT", LOAD_CONF_UNIT);
            cmd.Parameters.AddWithValue("@LOAD_BASE", LOAD_BASE);
            cmd.Parameters.AddWithValue("@LOAD_BASE_UNIT", LOAD_BASE_UNIT);
            cmd.Parameters.AddWithValue("@DLV", DLV);
            cmd.Parameters.AddWithValue("@DLV_UNIT", DLV_UNIT);
            cmd.Parameters.AddWithValue("@DLV_BASE", DLV_BASE);
            cmd.Parameters.AddWithValue("@DLV_BASE_UNIT", DLV_BASE_UNIT);
            cmd.Parameters.AddWithValue("@REFERENCE_DOC", REFERENCE_DOC);
            cmd.Parameters.AddWithValue("@REF_DOC_ITEM", REF_DOC_ITEM);
            cmd.Parameters.AddWithValue("@DATETIME_STAMP", DATETIME_STAMP);
            cmd.Parameters.AddWithValue("@RETURN_FLAG", RETURN_FLAG);
            cmd.Parameters.AddWithValue("@DEL_FLAG", DEL_FLAG);
            cmd.Parameters.AddWithValue("@DBSYSDATE", DBSYSDATE);
            cmd.Parameters.AddWithValue("@VEH_HEAD", VEH_HEAD);
            cmd.Parameters.AddWithValue("@VEH_TRAIL", VEH_TRAIL);
            
            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    public int Delete()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = new OracleCommand("DTSHIPMENT", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    public bool Open()
    {
        string sqlstr;
        bool result = false;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            sqlstr = "SELECT * FROM TSHIPMENT ";
            DataTable dt = new DataTable();
            new OracleDataAdapter(sqlstr, conn).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                SHIPMENT_NO = row["SHIPMENT_NO"].ToString();
                SHIPMENT_ITEM = row["SHIPMENT_ITEM"].ToString();
                DELIVERY_NO = row["DELIVERY_NO"].ToString();
                DELIVERY_ITEM = row["DELIVERY_ITEM"].ToString();
                CUSTOMER_NO = row["CUSTOMER_NO"].ToString();
                SHIP_TO = row["SHIP_TO"].ToString();
                PLANT = row["PLANT"].ToString();
                MATERIAL_NO = row["MATERIAL_NO"].ToString();
                MAT_FRT_GRP = row["MAT_FRT_GRP"].ToString();
                LOAD_CONF = row["LOAD_CONF"].ToString();
                LOAD_CONF_UNIT = row["LOAD_CONF_UNIT"].ToString();
                LOAD_BASE = row["LOAD_BASE"].ToString();
                LOAD_BASE_UNIT = row["LOAD_BASE_UNIT"].ToString();
                DLV = row["DLV"].ToString();
                DLV_UNIT = row["DLV_UNIT"].ToString();
                DLV_BASE = row["DLV_BASE"].ToString();
                DLV_BASE_UNIT = row["DLV_BASE_UNIT"].ToString();
                REFERENCE_DOC = row["REFERENCE_DOC"].ToString();
                REF_DOC_ITEM = row["REF_DOC_ITEM"].ToString();
                DATETIME_STAMP = row["DATETIME_STAMP"].ToString();
                RETURN_FLAG = row["RETURN_FLAG"].ToString();
                DEL_FLAG = row["DEL_FLAG"].ToString();
                DBSYSDATE = row["DBSYSDATE"].ToString();
                VEH_HEAD = row["VEH_HEAD"].ToString();
                VEH_TRAIL = row["VEH_TRAIL"].ToString();
                dt.Dispose();
                result = true;
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {
            
        }
        return result;
    }
    #endregion
}
