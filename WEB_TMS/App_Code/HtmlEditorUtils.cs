﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using DevExpress.Web.ASPxHtmlEditor;

public static class HtmlEditorUtils {

    const string htmlLocation = "~/";
    static readonly ICollection<string> darkThemeNames = new string[] { "BlackGlass", "PlasticBlue", "RedWine" };    

    public static void SetASPxHtmlEditorHtml(Page page, ASPxHtmlEditor htmlEditor, string htmlFileName) {
        string resolvedPath = page.MapPath(Path.Combine(htmlLocation, htmlFileName));
        htmlEditor.Html = File.ReadAllText(resolvedPath);
    }
    public static string GetASPxHtmlEditorHtml(Page page, string htmlFileName) {
        string resolvedPath = page.MapPath(Path.Combine(htmlLocation, htmlFileName));
        return File.ReadAllText(resolvedPath); 
    }

    public static bool IsDarkTheme(string themeName) {
        return darkThemeNames.Contains(themeName);
    }

}
