﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Data;

/// <summary>
/// Summary description for ComboBoxHelper
/// </summary>
public static class ComboBoxHelper
{
    public static void BindComboBox(ref ASPxComboBox cbo, System.Data.DataTable dtSource, bool isHeader, string ChooseText, string HeaderTextField)
    {
        try
        {
            if (isHeader)
            {
                DataRow dr = dtSource.NewRow();
                dr[HeaderTextField] = "- " + ChooseText + " -";
                dtSource.Rows.InsertAt(dr, 0);
            }
            cbo.DataSource = dtSource;
            cbo.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}