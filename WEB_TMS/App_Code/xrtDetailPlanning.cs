﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for xrtDetailPlanning
/// </summary>
public class xrtDetailPlanning : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
	private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private DetailReportBand DetailReport;
    private DetailBand Detail1;
    private PageHeaderBand PageHeader;
    private XRTable xrTable4;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell13;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell18;
    private XRTableCell xrTableCell21;
    private XRTable xrTable5;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell23;
    private XRTableCell xrTableCell24;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell31;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell34;
    private XRTableCell xrTableCell37;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell32;
    private XRTableCell xrTableCell28;
    private XRTableCell xrTableCell29;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell33;
    private XRTableCell xrTableCell35;
    private XRTableCell xrTableCell38;
    private XRTableCell xrTableCell36;
    private XRTableCell xrTableCell41;
    private XRTableCell xrTableCell39;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell8;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell10;
    private XRTableCell xrTableCell12;
    private XRTableCell xrTableCell15;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell11;
    private XRTableCell xrTableCell14;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell101;
    private XRTableCell xrTableCell96;
    private XRTableCell xrTableCell124;
    private XRTableCell xrTableCell94;
    private XRTableCell xrTableCell125;
    private XRTableCell xrTableCell16;
    private XRTableCell xrTableCell47;
    private XRTableCell xrTableCell20;
    private XRTableCell xrTableCell30;
    private XRTableCell xrTableCell22;
    private XRTableCell xrTableCell40;
    private XRTableCell xrTableCell42;
    private XRTableCell xrTableCell43;
    private XRTableCell xrTableCell44;
    private XRTableCell xrTableCell45;
    private XRTableCell xrTableCell46;
    private XRTableCell xrTableCell48;
    private XRTableCell xrTableCell27;
    private dsDetailPlanning dsDetailPlanning1;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell49;
    private XRTableCell xrTableCell52;
    private XRTableCell xrTableCell54;
    private XRTableCell xrTableCell57;
    private XRTableCell xrTableCell58;
    private XRTableCell xrTableCell59;
    private XRTableCell xrTableCell60;
    private XRTableCell xrTableCell61;
    private XRTableCell xrTableCell62;
    private XRTableCell xrTableCell63;
    private XRTableCell xrTableCell64;
    private XRTableCell xrTableCell65;
    private XRTableCell xrTableCell66;
    private XRTableCell xrTableCell67;
    private XRTableCell xrTableCell68;
    private DetailReportBand DetailReport1;
    private DetailBand Detail2;
    private GroupFooterBand GroupFooter2;
    private GroupHeaderBand GroupHeader2;
    private XRTable xrTable6;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell72;
    private XRTableCell xrTableCell73;
    private XRTableCell xrTableCell76;
    private XRTableCell xrTableCell77;
    private XRTableCell xrTableCell78;
    private XRTableCell xrTableCell79;
    private XRTableCell xrTableCell80;
    private XRTableCell xrTableCell81;
    private XRTableCell xrTableCell82;
    private XRTableCell xrTableCell83;
    private XRTableCell xrTableCell84;
    private XRTableCell xrTableCell85;
    private XRTableCell xrTableCell86;
    private XRTableCell xrTableCell87;
    private XRTableCell xrTableCell70;
    public DetailReportBand DetailReport2;
    private DetailBand Detail3;
    private DetailReportBand DetailReport3;
    private DetailBand Detail4;
    private GroupFooterBand GroupFooter3;
    public DetailReportBand DetailReport4;
    private DetailBand Detail5;
    private XRTable xrTable7;
    private XRTableRow xrTableRow9;
    private XRTableCell xrTableCell19;
    private XRTableCell xrTableCell50;
    private XRTableCell xrTableCell51;
    private XRTableCell xrTableCell53;
    private XRTableCell xrTableCell69;
    private XRTableCell xrTableCell71;
    private XRTableCell xrTableCell88;
    private XRTableCell xrTableCell92;
    private XRTableCell xrTableCell93;
    private XRTableCell xrTableCell95;
    private XRTableCell xrTableCell97;
    private XRTableCell xrTableCell98;
    private XRTableCell xrTableCell99;
    private XRTableCell xrTableCell100;
    private XRTableCell xrTableCell102;
    private XRTableCell xrTableCell103;
    private XRTableCell xrTableCell104;
    private XRTableCell xrTableCell105;
    public DetailReportBand DetailReport5;
    private DetailBand Detail6;
    private GroupFooterBand GroupFooter4;
    private GroupHeaderBand GroupHeader1;
    public DetailReportBand DetailReport6;
    private DetailBand Detail7;
    private GroupFooterBand GroupFooter1;
    private XRTable xrTable9;
    private XRTableRow xrTableRow11;
    private XRTableCell xrTableCell128;
    private XRTableCell xrTableCell129;
    private XRTableCell xrTableCell130;
    private XRTableCell xrTableCell133;
    private XRTableCell xrTableCell134;
    private XRTableCell xrTableCell135;
    private XRTableCell xrTableCell136;
    private XRTableCell xrTableCell137;
    private XRTableCell xrTableCell138;
    private XRTableCell xrTableCell139;
    private XRTableCell xrTableCell140;
    private XRTableCell xrTableCell141;
    private XRTableCell xrTableCell142;
    private XRTableCell xrTableCell143;
    private XRTableCell xrTableCell144;
    private CalculatedField COLUMN2;
    private CalculatedField COLUMN4;
    private CalculatedField COLUMN7;
    private CalculatedField COLUMN12;
    private XRTableCell xrTableCell25;
    private CalculatedField COLUMN15;
    private XRTable xrTable8;
    private XRTableRow xrTableRow10;
    private XRTableCell xrTableCell106;
    private XRTableCell xrTableCell107;
    private XRTableCell xrTableCell108;
    private XRTableCell xrTableCell111;
    private XRTableCell xrTableCell112;
    private XRTableCell xrTableCell113;
    private XRTableCell xrTableCell114;
    private XRTableCell xrTableCell115;
    private XRTableCell xrTableCell116;
    private XRTableCell xrTableCell117;
    private XRTableCell xrTableCell118;
    private XRTableCell xrTableCell119;
    private XRTableCell xrTableCell120;
    private XRTableCell xrTableCell121;
    private XRTableCell xrTableCell122;
    private XRTableRow xrTableRow12;
    private XRTableCell xrTableCell123;
    private XRTableCell xrTableCell126;
    private XRTableCell xrTableCell127;
    private XRTableCell xrTableCell145;
    private XRTableCell xrTableCell146;
    private XRTableCell xrTableCell147;
    private XRTableCell xrTableCell148;
    private XRTableCell xrTableCell149;
    private XRTableCell xrTableCell150;
    private XRTableCell xrTableCell151;
    private XRTableCell xrTableCell152;
    private XRTableCell xrTableCell153;
    private XRTableCell xrTableCell154;
    private XRLabel xrLabel1;
    private XRTableCell xrTableCell91;
    private CalculatedField FML_RPT_COL1;
    private CalculatedField FML_RPT_COL7;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public xrtDetailPlanning()
	{
		InitializeComponent();
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "xrtDetailPlanning.resx";
        DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary19 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary20 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary21 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary22 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary23 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary24 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary25 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary26 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary27 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary28 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary29 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary30 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary31 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary32 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary33 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary34 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary35 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary36 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary37 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary38 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary39 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary40 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary41 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary42 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary43 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary44 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary45 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
        this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.dsDetailPlanning1 = new dsDetailPlanning();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
        this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
        this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
        this.GroupFooter3 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail5 = new DevExpress.XtraReports.UI.DetailBand();
        this.DetailReport5 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail6 = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupFooter4 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.DetailReport6 = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail7 = new DevExpress.XtraReports.UI.DetailBand();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
        this.COLUMN2 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN4 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN7 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN12 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN15 = new DevExpress.XtraReports.UI.CalculatedField();
        this.FML_RPT_COL1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.FML_RPT_COL7 = new DevExpress.XtraReports.UI.CalculatedField();
        xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailPlanning1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // xrTableCell17
        // 
        xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.TERMINALEND")});
        xrTableCell17.Name = "xrTableCell17";
        xrTableCell17.StylePriority.UseTextAlignment = false;
        xrTableCell17.Text = "xrTableCell17";
        xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell17.Weight = 2.0000000202758268D;
        // 
        // xrTableCell26
        // 
        xrTableCell26.Name = "xrTableCell26";
        xrTableCell26.StylePriority.UseTextAlignment = false;
        xrTableCell26.Text = "[COLUMN1]";
        xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell26.Weight = 0.99999974371026534D;
        // 
        // xrTableCell55
        // 
        xrTableCell55.BackColor = System.Drawing.Color.Moccasin;
        xrTableCell55.Name = "xrTableCell55";
        xrTableCell55.StylePriority.UseBackColor = false;
        xrTableCell55.StylePriority.UseTextAlignment = false;
        xrTableCell55.Text = "ทั้งหมด";
        xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell55.Weight = 2.0000000202758268D;
        // 
        // xrTableCell56
        // 
        xrTableCell56.BackColor = System.Drawing.Color.Moccasin;
        xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN1")});
        xrTableCell56.Name = "xrTableCell56";
        xrTableCell56.StylePriority.UseBackColor = false;
        xrTableCell56.StylePriority.UseTextAlignment = false;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        xrTableCell56.Summary = xrSummary1;
        xrTableCell56.Text = "xrTableCell56";
        xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell56.Weight = 0.99999974371026534D;
        // 
        // xrTableCell74
        // 
        xrTableCell74.BackColor = System.Drawing.Color.Yellow;
        xrTableCell74.Name = "xrTableCell74";
        xrTableCell74.StylePriority.UseBackColor = false;
        xrTableCell74.StylePriority.UseTextAlignment = false;
        xrTableCell74.Text = "ทั้งหมด";
        xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell74.Weight = 2.0000000202758268D;
        // 
        // xrTableCell75
        // 
        xrTableCell75.BackColor = System.Drawing.Color.Yellow;
        xrTableCell75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN1")});
        xrTableCell75.Name = "xrTableCell75";
        xrTableCell75.StylePriority.UseBackColor = false;
        xrTableCell75.StylePriority.UseTextAlignment = false;
        xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        xrTableCell75.Summary = xrSummary2;
        xrTableCell75.Text = "xrTableCell75";
        xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell75.Weight = 0.99999974371026534D;
        // 
        // xrTableCell89
        // 
        xrTableCell89.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.TERMINALEND")});
        xrTableCell89.Name = "xrTableCell89";
        xrTableCell89.StylePriority.UseTextAlignment = false;
        xrTableCell89.Text = "xrTableCell17";
        xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell89.Weight = 2.0000000202758268D;
        // 
        // xrTableCell90
        // 
        xrTableCell90.Name = "xrTableCell90";
        xrTableCell90.StylePriority.UseTextAlignment = false;
        xrTableCell90.Text = "[COLUMN1]";
        xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell90.Weight = 0.99999974371026534D;
        // 
        // xrTableCell131
        // 
        xrTableCell131.BackColor = System.Drawing.Color.Yellow;
        xrTableCell131.Name = "xrTableCell131";
        xrTableCell131.StylePriority.UseBackColor = false;
        xrTableCell131.StylePriority.UseTextAlignment = false;
        xrTableCell131.Text = "ทั้งหมด";
        xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell131.Weight = 2.0000000202758268D;
        // 
        // xrTableCell132
        // 
        xrTableCell132.BackColor = System.Drawing.Color.Yellow;
        xrTableCell132.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN1")});
        xrTableCell132.Name = "xrTableCell132";
        xrTableCell132.StylePriority.UseBackColor = false;
        xrTableCell132.StylePriority.UseTextAlignment = false;
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        xrTableCell132.Summary = xrSummary3;
        xrTableCell132.Text = "xrTableCell75";
        xrTableCell132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell132.Weight = 0.99999974371026534D;
        // 
        // xrTableCell109
        // 
        xrTableCell109.BackColor = System.Drawing.Color.Moccasin;
        xrTableCell109.Name = "xrTableCell109";
        xrTableCell109.StylePriority.UseBackColor = false;
        xrTableCell109.StylePriority.UseTextAlignment = false;
        xrTableCell109.Text = "ทั้งหมด";
        xrTableCell109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell109.Weight = 2.0000000202758268D;
        // 
        // xrTableCell110
        // 
        xrTableCell110.BackColor = System.Drawing.Color.Moccasin;
        xrTableCell110.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN1")});
        xrTableCell110.Name = "xrTableCell110";
        xrTableCell110.StylePriority.UseBackColor = false;
        xrTableCell110.StylePriority.UseTextAlignment = false;
        xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        xrTableCell110.Summary = xrSummary4;
        xrTableCell110.Text = "xrTableCell56";
        xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell110.Weight = 0.99999974371026534D;
        // 
        // Detail
        // 
        this.Detail.Expanded = false;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TERMINALEND", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // TopMargin
        // 
        this.TopMargin.Font = new System.Drawing.Font("Angsana New", 14F);
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.StylePriority.UseFont = false;
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.BottomMargin.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.BottomMargin_BeforePrint);
        // 
        // DetailReport
        // 
        this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.DetailReport1});
        this.DetailReport.DataMember = "TReports";
        this.DetailReport.DataSource = this.dsDetailPlanning1;
        this.DetailReport.Level = 0;
        this.DetailReport.Name = "DetailReport";
        // 
        // Detail1
        // 
        this.Detail1.Expanded = false;
        this.Detail1.HeightF = 30F;
        this.Detail1.Name = "Detail1";
        // 
        // DetailReport1
        // 
        this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.GroupFooter2,
            this.GroupHeader2});
        this.DetailReport1.Level = 0;
        this.DetailReport1.Name = "DetailReport1";
        // 
        // Detail2
        // 
        this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.Detail2.HeightF = 30F;
        this.Detail2.Name = "Detail2";
        // 
        // xrTable2
        // 
        this.xrTable2.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable2.StylePriority.UseFont = false;
        this.xrTable2.StylePriority.UseTextAlignment = false;
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell96,
            this.xrTableCell124,
            this.xrTableCell94,
            this.xrTableCell125,
            this.xrTableCell16,
            this.xrTableCell47,
            xrTableCell17,
            xrTableCell26,
            this.xrTableCell25,
            this.xrTableCell27,
            this.xrTableCell20,
            this.xrTableCell30,
            this.xrTableCell22,
            this.xrTableCell40,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell48});
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.StylePriority.UseBorders = false;
        this.xrTableRow2.Weight = 0.99653962401674279D;
        // 
        // xrTableCell101
        // 
        this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableCell101.Name = "xrTableCell101";
        this.xrTableCell101.StylePriority.UseBorders = false;
        this.xrTableCell101.Weight = 0.02322843243127401D;
        // 
        // xrTableCell96
        // 
        this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell96.Name = "xrTableCell96";
        this.xrTableCell96.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell96.StylePriority.UseBorders = false;
        this.xrTableCell96.Text = "[dStart]";
        this.xrTableCell96.Weight = 0.94000000189098387D;
        // 
        // xrTableCell124
        // 
        this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell124.Name = "xrTableCell124";
        this.xrTableCell124.StylePriority.UseBorders = false;
        this.xrTableCell124.Weight = 0.036771470080832314D;
        // 
        // xrTableCell94
        // 
        this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell94.CanShrink = true;
        this.xrTableCell94.Name = "xrTableCell94";
        this.xrTableCell94.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell94.StylePriority.UseBorders = false;
        this.xrTableCell94.Text = "[dEnd]";
        this.xrTableCell94.Weight = 0.94000001810068412D;
        // 
        // xrTableCell125
        // 
        this.xrTableCell125.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell125.Name = "xrTableCell125";
        this.xrTableCell125.StylePriority.UseBorders = false;
        this.xrTableCell125.Weight = 0.060000077867919832D;
        // 
        // xrTableCell16
        // 
        this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell16.CanShrink = true;
        this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.TERMINALSTART")});
        this.xrTableCell16.Multiline = true;
        this.xrTableCell16.Name = "xrTableCell16";
        this.xrTableCell16.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell16.StylePriority.UseBorders = false;
        this.xrTableCell16.StylePriority.UseTextAlignment = false;
        xrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell16.Summary = xrSummary5;
        this.xrTableCell16.Text = "xrTableCell16";
        this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell16.Weight = 0.940000017499514D;
        // 
        // xrTableCell47
        // 
        this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell47.Name = "xrTableCell47";
        this.xrTableCell47.StylePriority.UseBorders = false;
        this.xrTableCell47.Weight = 0.059999848986077908D;
        // 
        // xrTableCell25
        // 
        this.xrTableCell25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN2", "{0:0.00%}")});
        this.xrTableCell25.Name = "xrTableCell25";
        this.xrTableCell25.NullValueText = "0";
        this.xrTableCell25.Text = "xrTableCell25";
        this.xrTableCell25.Weight = 1.500000055496199D;
        // 
        // xrTableCell27
        // 
        this.xrTableCell27.Name = "xrTableCell27";
        this.xrTableCell27.Text = "[COLUMN3]";
        this.xrTableCell27.Weight = 1.0000006527313399D;
        // 
        // xrTableCell20
        // 
        this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN4", "{0:0.00%}")});
        this.xrTableCell20.Multiline = true;
        this.xrTableCell20.Name = "xrTableCell20";
        this.xrTableCell20.NullValueText = "0";
        this.xrTableCell20.StylePriority.UseTextAlignment = false;
        this.xrTableCell20.Text = "xrTableCell20";
        this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell20.Weight = 1.5160388112436274D;
        // 
        // xrTableCell30
        // 
        this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell30.Multiline = true;
        this.xrTableCell30.Name = "xrTableCell30";
        this.xrTableCell30.Text = "[COLUMN5]";
        this.xrTableCell30.Weight = 1.4800012234767266D;
        // 
        // xrTableCell22
        // 
        this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell22.Multiline = true;
        this.xrTableCell22.Name = "xrTableCell22";
        this.xrTableCell22.Text = "[COLUMN6]";
        this.xrTableCell22.Weight = 1.5000000082073324D;
        // 
        // xrTableCell40
        // 
        this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN7", "{0:0.00%}")});
        this.xrTableCell40.Name = "xrTableCell40";
        this.xrTableCell40.NullValueText = "0";
        this.xrTableCell40.StylePriority.UseTextAlignment = false;
        this.xrTableCell40.Text = "xrTableCell40";
        this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell40.Weight = 1.5000000753423171D;
        // 
        // xrTableCell42
        // 
        this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell42.Name = "xrTableCell42";
        this.xrTableCell42.Text = "xrTableCell42";
        this.xrTableCell42.Weight = 1.4999988597603831D;
        // 
        // xrTableCell43
        // 
        this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11")});
        this.xrTableCell43.Name = "xrTableCell43";
        this.xrTableCell43.Text = "xrTableCell43";
        this.xrTableCell43.Weight = 1.0000016620972003D;
        // 
        // xrTableCell44
        // 
        this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN12", "{0:0.00%}")});
        this.xrTableCell44.Multiline = true;
        this.xrTableCell44.Name = "xrTableCell44";
        this.xrTableCell44.NullValueText = "0";
        this.xrTableCell44.Text = "xrTableCell44";
        this.xrTableCell44.Weight = 1.5100012227511281D;
        // 
        // xrTableCell45
        // 
        this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell45.Multiline = true;
        this.xrTableCell45.Name = "xrTableCell45";
        this.xrTableCell45.Text = "xrTableCell45";
        this.xrTableCell45.Weight = 1.4899987890944635D;
        // 
        // xrTableCell46
        // 
        this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14")});
        this.xrTableCell46.Multiline = true;
        this.xrTableCell46.Name = "xrTableCell46";
        this.xrTableCell46.Text = "xrTableCell46";
        this.xrTableCell46.Weight = 1.5000000051666145D;
        // 
        // xrTableCell48
        // 
        this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN15", "{0:0.00%}")});
        this.xrTableCell48.Multiline = true;
        this.xrTableCell48.Name = "xrTableCell48";
        this.xrTableCell48.NullValueText = "0";
        this.xrTableCell48.StylePriority.UseTextAlignment = false;
        this.xrTableCell48.Text = "xrTableCell48";
        this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell48.Weight = 1.5139596074077506D;
        // 
        // GroupFooter2
        // 
        this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
        this.GroupFooter2.HeightF = 30F;
        this.GroupFooter2.Name = "GroupFooter2";
        // 
        // xrTable3
        // 
        this.xrTable3.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable3.StylePriority.UseFont = false;
        this.xrTable3.StylePriority.UseTextAlignment = false;
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell52,
            this.xrTableCell54,
            xrTableCell55,
            xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68});
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.StylePriority.UseBorders = false;
        this.xrTableRow3.Weight = 0.99653962401674279D;
        // 
        // xrTableCell49
        // 
        this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell49.CanShrink = true;
        this.xrTableCell49.Name = "xrTableCell49";
        this.xrTableCell49.StylePriority.UseBorders = false;
        this.xrTableCell49.Weight = 1.0000000569909848D;
        // 
        // xrTableCell52
        // 
        this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell52.Name = "xrTableCell52";
        this.xrTableCell52.StylePriority.UseBorders = false;
        this.xrTableCell52.Weight = 0.99999994338070919D;
        // 
        // xrTableCell54
        // 
        this.xrTableCell54.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell54.Name = "xrTableCell54";
        this.xrTableCell54.StylePriority.UseBorders = false;
        this.xrTableCell54.Weight = 0.99999986648559192D;
        // 
        // xrTableCell57
        // 
        this.xrTableCell57.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN2")});
        this.xrTableCell57.Name = "xrTableCell57";
        this.xrTableCell57.StylePriority.UseBackColor = false;
        xrSummary6.FormatString = "{0:0.00%}";
        xrSummary6.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell57.Summary = xrSummary6;
        this.xrTableCell57.Text = "xrTableCell57";
        this.xrTableCell57.Weight = 1.5000006723540706D;
        // 
        // xrTableCell58
        // 
        this.xrTableCell58.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell58.Name = "xrTableCell58";
        this.xrTableCell58.StylePriority.UseBackColor = false;
        xrSummary7.FormatString = "{0:0.00%}";
        xrSummary7.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell58.Summary = xrSummary7;
        this.xrTableCell58.Weight = 1.0000000358734684D;
        // 
        // xrTableCell59
        // 
        this.xrTableCell59.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN4")});
        this.xrTableCell59.Multiline = true;
        this.xrTableCell59.Name = "xrTableCell59";
        this.xrTableCell59.StylePriority.UseBackColor = false;
        this.xrTableCell59.StylePriority.UseTextAlignment = false;
        xrSummary8.FormatString = "{0:0.00%}";
        xrSummary8.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell59.Summary = xrSummary8;
        this.xrTableCell59.Text = "xrTableCell59";
        this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell59.Weight = 1.5160388112436274D;
        // 
        // xrTableCell60
        // 
        this.xrTableCell60.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell60.Multiline = true;
        this.xrTableCell60.Name = "xrTableCell60";
        this.xrTableCell60.StylePriority.UseBackColor = false;
        xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell60.Summary = xrSummary9;
        this.xrTableCell60.Text = "xrTableCell60";
        this.xrTableCell60.Weight = 1.4800012234767266D;
        // 
        // xrTableCell61
        // 
        this.xrTableCell61.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell61.Multiline = true;
        this.xrTableCell61.Name = "xrTableCell61";
        this.xrTableCell61.StylePriority.UseBackColor = false;
        xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell61.Summary = xrSummary10;
        this.xrTableCell61.Text = "xrTableCell61";
        this.xrTableCell61.Weight = 1.5000000082073324D;
        // 
        // xrTableCell62
        // 
        this.xrTableCell62.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN7")});
        this.xrTableCell62.Name = "xrTableCell62";
        this.xrTableCell62.StylePriority.UseBackColor = false;
        this.xrTableCell62.StylePriority.UseTextAlignment = false;
        xrSummary11.FormatString = "{0:0.00%}";
        xrSummary11.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell62.Summary = xrSummary11;
        this.xrTableCell62.Text = "xrTableCell62";
        this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell62.Weight = 1.5000000753423171D;
        // 
        // xrTableCell63
        // 
        this.xrTableCell63.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell63.Name = "xrTableCell63";
        this.xrTableCell63.StylePriority.UseBackColor = false;
        xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell63.Summary = xrSummary12;
        this.xrTableCell63.Text = "xrTableCell63";
        this.xrTableCell63.Weight = 1.4999988597603831D;
        // 
        // xrTableCell64
        // 
        this.xrTableCell64.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell64.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11")});
        this.xrTableCell64.Name = "xrTableCell64";
        this.xrTableCell64.StylePriority.UseBackColor = false;
        xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell64.Summary = xrSummary13;
        this.xrTableCell64.Text = "xrTableCell64";
        this.xrTableCell64.Weight = 1.0000016620972003D;
        // 
        // xrTableCell65
        // 
        this.xrTableCell65.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN12")});
        this.xrTableCell65.Multiline = true;
        this.xrTableCell65.Name = "xrTableCell65";
        this.xrTableCell65.StylePriority.UseBackColor = false;
        xrSummary14.FormatString = "{0:0.00%}";
        xrSummary14.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell65.Summary = xrSummary14;
        this.xrTableCell65.Text = "xrTableCell65";
        this.xrTableCell65.Weight = 1.5100012227511281D;
        // 
        // xrTableCell66
        // 
        this.xrTableCell66.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell66.Multiline = true;
        this.xrTableCell66.Name = "xrTableCell66";
        this.xrTableCell66.StylePriority.UseBackColor = false;
        xrSummary15.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell66.Summary = xrSummary15;
        this.xrTableCell66.Text = "xrTableCell66";
        this.xrTableCell66.Weight = 1.4899987890944635D;
        // 
        // xrTableCell67
        // 
        this.xrTableCell67.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14")});
        this.xrTableCell67.Multiline = true;
        this.xrTableCell67.Name = "xrTableCell67";
        this.xrTableCell67.StylePriority.UseBackColor = false;
        xrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell67.Summary = xrSummary16;
        this.xrTableCell67.Text = "xrTableCell67";
        this.xrTableCell67.Weight = 1.5000000051666145D;
        // 
        // xrTableCell68
        // 
        this.xrTableCell68.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN15")});
        this.xrTableCell68.Multiline = true;
        this.xrTableCell68.Name = "xrTableCell68";
        this.xrTableCell68.StylePriority.UseBackColor = false;
        this.xrTableCell68.StylePriority.UseTextAlignment = false;
        xrSummary17.FormatString = "{0:0.00%}";
        xrSummary17.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary17.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell68.Summary = xrSummary17;
        this.xrTableCell68.Text = "xrTableCell68";
        this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell68.Weight = 1.5139596074077506D;
        // 
        // GroupHeader2
        // 
        this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
        this.GroupHeader2.Expanded = false;
        this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TERMINALSTART", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader2.HeightF = 23F;
        this.GroupHeader2.Name = "GroupHeader2";
        this.GroupHeader2.Visible = false;
        // 
        // xrLabel1
        // 
        this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.TERMINALSTART")});
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel1.Text = "xrLabel1";
        // 
        // dsDetailPlanning1
        // 
        this.dsDetailPlanning1.DataSetName = "dsDetailPlanning";
        this.dsDetailPlanning1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable5,
            this.xrTable4});
        this.PageHeader.HeightF = 178F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrTable1
        // 
        this.xrTable1.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable1.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable1.ForeColor = System.Drawing.Color.White;
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(500F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow7,
            this.xrTableRow12});
        this.xrTable1.SizeF = new System.Drawing.SizeF(1801F, 175.7F);
        this.xrTable1.StylePriority.UseBackColor = false;
        this.xrTable1.StylePriority.UseBorders = false;
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.StylePriority.UseForeColor = false;
        this.xrTable1.StylePriority.UseTextAlignment = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.BackColor = System.Drawing.Color.SlateBlue;
        this.xrTableRow1.BorderColor = System.Drawing.Color.SlateBlue;
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell31,
            this.xrTableCell5,
            this.xrTableCell34,
            this.xrTableCell37,
            this.xrTableCell6,
            this.xrTableCell8,
            this.xrTableCell7,
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell15});
        this.xrTableRow1.ForeColor = System.Drawing.Color.White;
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.StylePriority.UseBackColor = false;
        this.xrTableRow1.StylePriority.UseBorderColor = false;
        this.xrTableRow1.StylePriority.UseForeColor = false;
        this.xrTableRow1.Weight = 1.0555555852254233D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell1.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell1.Multiline = true;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseBorderColor = false;
        this.xrTableCell1.Text = "(1) \r\nจำนวนวันที่จัดแผน";
        this.xrTableCell1.Weight = 0.29999999618530254D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell3.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell3.Multiline = true;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseBorderColor = false;
        this.xrTableCell3.Text = "(2)\r\n% วันที่จัดแผน / \r\nจำนวนวันที่ออกรายงาน";
        this.xrTableCell3.Weight = 0.44999999618530273D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell4.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell4.Multiline = true;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.StylePriority.UseBackColor = false;
        this.xrTableCell4.StylePriority.UseBorderColor = false;
        this.xrTableCell4.Text = "(3) \r\nวันที่ไม่ได้จัดแผน";
        this.xrTableCell4.Weight = 0.30000001525878883D;
        // 
        // xrTableCell31
        // 
        this.xrTableCell31.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell31.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell31.Multiline = true;
        this.xrTableCell31.Name = "xrTableCell31";
        this.xrTableCell31.StylePriority.UseBackColor = false;
        this.xrTableCell31.StylePriority.UseBorderColor = false;
        this.xrTableCell31.Text = "(4)\r\n% วันที่ไม่ได้จัดแผน / \r\nจำนวนวันที่ออกรายงาน";
        this.xrTableCell31.Weight = 0.45481182670593251D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell5.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell5.Multiline = true;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.StylePriority.UseBackColor = false;
        this.xrTableCell5.StylePriority.UseBorderColor = false;
        this.xrTableCell5.Text = "(5)\r\n จำนวนงานขนส่งทั้งหมด";
        this.xrTableCell5.Weight = 0.44518816995620708D;
        // 
        // xrTableCell34
        // 
        this.xrTableCell34.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell34.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell34.Multiline = true;
        this.xrTableCell34.Name = "xrTableCell34";
        this.xrTableCell34.StylePriority.UseBackColor = false;
        this.xrTableCell34.StylePriority.UseBorderColor = false;
        this.xrTableCell34.Text = "(6) \r\nจำนวนงานที่จัดแผนผ่านระบบ";
        this.xrTableCell34.Weight = 0.4499999989271164D;
        // 
        // xrTableCell37
        // 
        this.xrTableCell37.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell37.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell37.Multiline = true;
        this.xrTableCell37.Name = "xrTableCell37";
        this.xrTableCell37.StylePriority.UseBackColor = false;
        this.xrTableCell37.StylePriority.UseBorderColor = false;
        this.xrTableCell37.Text = "(7)\r\n%  จำนวนงานที่จัดแผนผ่านระบบ /  จำนวนงานขนส่งทั้งหมด";
        this.xrTableCell37.Weight = 0.44999999839067456D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell6.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell6.Multiline = true;
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseBackColor = false;
        this.xrTableCell6.StylePriority.UseBorderColor = false;
        this.xrTableCell6.Text = "(10) \r\nงานส่งลูกค้าทั้งหมด";
        this.xrTableCell6.Weight = 0.44999963298439977D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell8.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell8.Multiline = true;
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.StylePriority.UseBackColor = false;
        this.xrTableCell8.StylePriority.UseBorderColor = false;
        this.xrTableCell8.Text = "(11) \r\nงานส่งลูกค้าที่จัดแผนผ่านระบบ";
        this.xrTableCell8.Weight = 0.30300018270313739D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell7.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell7.Multiline = true;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.StylePriority.UseBackColor = false;
        this.xrTableCell7.StylePriority.UseBorderColor = false;
        this.xrTableCell7.Text = "(12)\r\n% งานส่งลูกค้าที่จัดแผนผ่านระบบ / งานส่งลูกค้าทั้งหมด";
        this.xrTableCell7.Weight = 0.44999999979883432D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell10.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell10.Multiline = true;
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.StylePriority.UseBackColor = false;
        this.xrTableCell10.StylePriority.UseBorderColor = false;
        this.xrTableCell10.Text = "(13) \r\nงานโอนคลังทั้งหมด";
        this.xrTableCell10.Weight = 0.44999999989941708D;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell12.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell12.Multiline = true;
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.StylePriority.UseBackColor = false;
        this.xrTableCell12.StylePriority.UseBorderColor = false;
        this.xrTableCell12.Text = "(14) \r\nงานโอนคลังที่จัดแผนผ่านระบบ";
        this.xrTableCell12.Weight = 0.44999999994970852D;
        // 
        // xrTableCell15
        // 
        this.xrTableCell15.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell15.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell15.Multiline = true;
        this.xrTableCell15.Name = "xrTableCell15";
        this.xrTableCell15.StylePriority.UseBackColor = false;
        this.xrTableCell15.StylePriority.UseBorderColor = false;
        this.xrTableCell15.Text = "(15)\r\n% งานโอนคลังที่จัดแผนผ่านระบบ / งานโอนคลังทั้งหมด";
        this.xrTableCell15.Weight = 0.45000018305517731D;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell2,
            this.xrTableCell33,
            this.xrTableCell35,
            this.xrTableCell38,
            this.xrTableCell36,
            this.xrTableCell41,
            this.xrTableCell39,
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell14});
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.Weight = 0.26388885921902133D;
        // 
        // xrTableCell32
        // 
        this.xrTableCell32.Name = "xrTableCell32";
        this.xrTableCell32.Text = "วัน";
        this.xrTableCell32.Weight = 0.29999999237060532D;
        // 
        // xrTableCell28
        // 
        this.xrTableCell28.Name = "xrTableCell28";
        this.xrTableCell28.Text = "%";
        this.xrTableCell28.Weight = 0.45000000000000018D;
        // 
        // xrTableCell29
        // 
        this.xrTableCell29.Name = "xrTableCell29";
        this.xrTableCell29.Text = "-";
        this.xrTableCell29.Weight = 0.30000000381469716D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Text = "%";
        this.xrTableCell2.Weight = 0.4548118305206299D;
        // 
        // xrTableCell33
        // 
        this.xrTableCell33.Name = "xrTableCell33";
        this.xrTableCell33.Text = "-";
        this.xrTableCell33.Weight = 0.44518817186355586D;
        // 
        // xrTableCell35
        // 
        this.xrTableCell35.Name = "xrTableCell35";
        this.xrTableCell35.Text = "-";
        this.xrTableCell35.Weight = 0.45000000047683719D;
        // 
        // xrTableCell38
        // 
        this.xrTableCell38.Name = "xrTableCell38";
        this.xrTableCell38.Text = "%";
        this.xrTableCell38.Weight = 0.45000000047683708D;
        // 
        // xrTableCell36
        // 
        this.xrTableCell36.Name = "xrTableCell36";
        this.xrTableCell36.Text = "-";
        this.xrTableCell36.Weight = 0.45000000011920926D;
        // 
        // xrTableCell41
        // 
        this.xrTableCell41.Name = "xrTableCell41";
        this.xrTableCell41.Text = "-";
        this.xrTableCell41.Weight = 0.30300036633014682D;
        // 
        // xrTableCell39
        // 
        this.xrTableCell39.Name = "xrTableCell39";
        this.xrTableCell39.Text = "%";
        this.xrTableCell39.Weight = 0.4499996339082718D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.Text = "-";
        this.xrTableCell9.Weight = 0.45000000005960472D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Text = "-";
        this.xrTableCell11.Weight = 0.45000000002980223D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Text = "%";
        this.xrTableCell14.Weight = 0.45000000002980234D;
        // 
        // xrTableRow12
        // 
        this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154});
        this.xrTableRow12.Name = "xrTableRow12";
        this.xrTableRow12.Weight = 0.3131357939718547D;
        // 
        // xrTableCell123
        // 
        this.xrTableCell123.Name = "xrTableCell123";
        this.xrTableCell123.Text = "-";
        this.xrTableCell123.Weight = 0.29999999237060532D;
        // 
        // xrTableCell126
        // 
        this.xrTableCell126.Name = "xrTableCell126";
        this.xrTableCell126.Text = "(1) / (0)";
        this.xrTableCell126.Weight = 0.45000000000000018D;
        // 
        // xrTableCell127
        // 
        this.xrTableCell127.Name = "xrTableCell127";
        this.xrTableCell127.Text = "-";
        this.xrTableCell127.Weight = 0.30000000381469716D;
        // 
        // xrTableCell145
        // 
        this.xrTableCell145.Name = "xrTableCell145";
        this.xrTableCell145.Text = "((0) - (1)) / (0)";
        this.xrTableCell145.Weight = 0.4548118305206299D;
        // 
        // xrTableCell146
        // 
        this.xrTableCell146.Name = "xrTableCell146";
        this.xrTableCell146.Text = "-";
        this.xrTableCell146.Weight = 0.44518817186355586D;
        // 
        // xrTableCell147
        // 
        this.xrTableCell147.Name = "xrTableCell147";
        this.xrTableCell147.Text = "-";
        this.xrTableCell147.Weight = 0.45000000047683719D;
        // 
        // xrTableCell148
        // 
        this.xrTableCell148.Name = "xrTableCell148";
        this.xrTableCell148.Text = "(6) / (5)";
        this.xrTableCell148.Weight = 0.45000000047683708D;
        // 
        // xrTableCell149
        // 
        this.xrTableCell149.Name = "xrTableCell149";
        this.xrTableCell149.Text = "-";
        this.xrTableCell149.Weight = 0.45000000011920926D;
        // 
        // xrTableCell150
        // 
        this.xrTableCell150.Name = "xrTableCell150";
        this.xrTableCell150.Text = "-";
        this.xrTableCell150.Weight = 0.30300036633014682D;
        // 
        // xrTableCell151
        // 
        this.xrTableCell151.Name = "xrTableCell151";
        this.xrTableCell151.Text = "(11) / (10)";
        this.xrTableCell151.Weight = 0.4499996339082718D;
        // 
        // xrTableCell152
        // 
        this.xrTableCell152.Name = "xrTableCell152";
        this.xrTableCell152.Text = "-";
        this.xrTableCell152.Weight = 0.45000000005960472D;
        // 
        // xrTableCell153
        // 
        this.xrTableCell153.Name = "xrTableCell153";
        this.xrTableCell153.Text = "-";
        this.xrTableCell153.Weight = 0.45000000002980223D;
        // 
        // xrTableCell154
        // 
        this.xrTableCell154.Name = "xrTableCell154";
        this.xrTableCell154.Text = "\'(14) / (13)";
        this.xrTableCell154.Weight = 0.45000000002980234D;
        // 
        // xrTable5
        // 
        this.xrTable5.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable5.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable5.ForeColor = System.Drawing.Color.White;
        this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(200F, 0F);
        this.xrTable5.Name = "xrTable5";
        this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
        this.xrTable5.SizeF = new System.Drawing.SizeF(300F, 178F);
        this.xrTable5.StylePriority.UseBackColor = false;
        this.xrTable5.StylePriority.UseBorders = false;
        this.xrTable5.StylePriority.UseFont = false;
        this.xrTable5.StylePriority.UseForeColor = false;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24});
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Weight = 1.7142857142857144D;
        // 
        // xrTableCell23
        // 
        this.xrTableCell23.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell23.Name = "xrTableCell23";
        this.xrTableCell23.StylePriority.UseBackColor = false;
        this.xrTableCell23.StylePriority.UseBorders = false;
        this.xrTableCell23.StylePriority.UseTextAlignment = false;
        this.xrTableCell23.Text = "หน่วยงาน";
        this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell23.Weight = 1.612866960472267D;
        // 
        // xrTableCell24
        // 
        this.xrTableCell24.Name = "xrTableCell24";
        this.xrTableCell24.StylePriority.UseTextAlignment = false;
        this.xrTableCell24.Text = "ต้นทาง";
        this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell24.Weight = 3.225733920944533D;
        // 
        // xrTable4
        // 
        this.xrTable4.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable4.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable4.ForeColor = System.Drawing.Color.White;
        this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable4.Name = "xrTable4";
        this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5});
        this.xrTable4.SizeF = new System.Drawing.SizeF(200F, 178F);
        this.xrTable4.StylePriority.UseBackColor = false;
        this.xrTable4.StylePriority.UseFont = false;
        this.xrTable4.StylePriority.UseForeColor = false;
        this.xrTable4.StylePriority.UseTextAlignment = false;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.Weight = 1.0421296351278471D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell13.Multiline = true;
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.StylePriority.UseBackColor = false;
        this.xrTableCell13.StylePriority.UseBorders = false;
        this.xrTableCell13.StylePriority.UseTextAlignment = false;
        this.xrTableCell13.Text = "(0) \r\nวันที่จัดทำรายงาน\r\n[COLUMN0] วัน";
        this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell13.Weight = 16.128669604722671D;
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell21});
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.Weight = 0.25210644225296519D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell18.Multiline = true;
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.StylePriority.UseBorders = false;
        this.xrTableCell18.StylePriority.UseTextAlignment = false;
        this.xrTableCell18.Text = "วันที่เริ่ม";
        this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell18.Weight = 8.0643348023613353D;
        // 
        // xrTableCell21
        // 
        this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell21.Multiline = true;
        this.xrTableCell21.Name = "xrTableCell21";
        this.xrTableCell21.StylePriority.UseBorders = false;
        this.xrTableCell21.StylePriority.UseTextAlignment = false;
        this.xrTableCell21.Text = "วันที่สิ้นสุด";
        this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell21.Weight = 8.0643348023613353D;
        // 
        // xrTable6
        // 
        this.xrTable6.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable6.Name = "xrTable6";
        this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
        this.xrTable6.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable6.StylePriority.UseFont = false;
        this.xrTable6.StylePriority.UseTextAlignment = false;
        this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow8
        // 
        this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70,
            this.xrTableCell72,
            this.xrTableCell73,
            xrTableCell74,
            xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87});
        this.xrTableRow8.Name = "xrTableRow8";
        this.xrTableRow8.StylePriority.UseBorders = false;
        this.xrTableRow8.Weight = 0.99653962401674279D;
        // 
        // xrTableCell70
        // 
        this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell70.Name = "xrTableCell70";
        this.xrTableCell70.StylePriority.UseBorders = false;
        this.xrTableCell70.Weight = 0.99999990440309017D;
        // 
        // xrTableCell72
        // 
        this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell72.Name = "xrTableCell72";
        this.xrTableCell72.StylePriority.UseBorders = false;
        this.xrTableCell72.Weight = 1.000000095968604D;
        // 
        // xrTableCell73
        // 
        this.xrTableCell73.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell73.Name = "xrTableCell73";
        this.xrTableCell73.StylePriority.UseBackColor = false;
        this.xrTableCell73.StylePriority.UseBorders = false;
        this.xrTableCell73.Text = "ทั้งหมด";
        this.xrTableCell73.Weight = 0.99999986648559192D;
        // 
        // xrTableCell76
        // 
        this.xrTableCell76.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN2")});
        this.xrTableCell76.Name = "xrTableCell76";
        this.xrTableCell76.StylePriority.UseBackColor = false;
        xrSummary18.FormatString = "{0:0.00%}";
        xrSummary18.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary18.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell76.Summary = xrSummary18;
        this.xrTableCell76.Text = "xrTableCell76";
        this.xrTableCell76.Weight = 1.5000006723540706D;
        // 
        // xrTableCell77
        // 
        this.xrTableCell77.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell77.Name = "xrTableCell77";
        this.xrTableCell77.StylePriority.UseBackColor = false;
        this.xrTableCell77.Weight = 1.0000000358734684D;
        // 
        // xrTableCell78
        // 
        this.xrTableCell78.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN4")});
        this.xrTableCell78.Multiline = true;
        this.xrTableCell78.Name = "xrTableCell78";
        this.xrTableCell78.StylePriority.UseBackColor = false;
        this.xrTableCell78.StylePriority.UseTextAlignment = false;
        xrSummary19.FormatString = "{0:0.00%}";
        xrSummary19.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary19.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell78.Summary = xrSummary19;
        this.xrTableCell78.Text = "xrTableCell78";
        this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell78.Weight = 1.5160388112436274D;
        // 
        // xrTableCell79
        // 
        this.xrTableCell79.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell79.Multiline = true;
        this.xrTableCell79.Name = "xrTableCell79";
        this.xrTableCell79.StylePriority.UseBackColor = false;
        xrSummary20.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell79.Summary = xrSummary20;
        this.xrTableCell79.Text = "xrTableCell60";
        this.xrTableCell79.Weight = 1.4800012234767266D;
        // 
        // xrTableCell80
        // 
        this.xrTableCell80.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell80.Multiline = true;
        this.xrTableCell80.Name = "xrTableCell80";
        this.xrTableCell80.StylePriority.UseBackColor = false;
        xrSummary21.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell80.Summary = xrSummary21;
        this.xrTableCell80.Text = "xrTableCell61";
        this.xrTableCell80.Weight = 1.5000000082073324D;
        // 
        // xrTableCell81
        // 
        this.xrTableCell81.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN7")});
        this.xrTableCell81.Name = "xrTableCell81";
        this.xrTableCell81.StylePriority.UseBackColor = false;
        this.xrTableCell81.StylePriority.UseTextAlignment = false;
        xrSummary22.FormatString = "{0:0.00%}";
        xrSummary22.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary22.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell81.Summary = xrSummary22;
        this.xrTableCell81.Text = "xrTableCell81";
        this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell81.Weight = 1.5000000753423171D;
        // 
        // xrTableCell82
        // 
        this.xrTableCell82.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell82.Name = "xrTableCell82";
        this.xrTableCell82.StylePriority.UseBackColor = false;
        xrSummary23.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell82.Summary = xrSummary23;
        this.xrTableCell82.Text = "xrTableCell63";
        this.xrTableCell82.Weight = 1.4999988597603831D;
        // 
        // xrTableCell83
        // 
        this.xrTableCell83.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell83.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11")});
        this.xrTableCell83.Name = "xrTableCell83";
        this.xrTableCell83.StylePriority.UseBackColor = false;
        xrSummary24.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell83.Summary = xrSummary24;
        this.xrTableCell83.Text = "xrTableCell64";
        this.xrTableCell83.Weight = 1.0000016620972003D;
        // 
        // xrTableCell84
        // 
        this.xrTableCell84.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell84.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN12")});
        this.xrTableCell84.Multiline = true;
        this.xrTableCell84.Name = "xrTableCell84";
        this.xrTableCell84.StylePriority.UseBackColor = false;
        xrSummary25.FormatString = "{0:0.00%}";
        xrSummary25.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary25.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell84.Summary = xrSummary25;
        this.xrTableCell84.Text = "xrTableCell84";
        this.xrTableCell84.Weight = 1.5100012227511281D;
        // 
        // xrTableCell85
        // 
        this.xrTableCell85.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell85.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell85.Multiline = true;
        this.xrTableCell85.Name = "xrTableCell85";
        this.xrTableCell85.StylePriority.UseBackColor = false;
        xrSummary26.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell85.Summary = xrSummary26;
        this.xrTableCell85.Text = "xrTableCell66";
        this.xrTableCell85.Weight = 1.4899987890944635D;
        // 
        // xrTableCell86
        // 
        this.xrTableCell86.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell86.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14")});
        this.xrTableCell86.Multiline = true;
        this.xrTableCell86.Name = "xrTableCell86";
        this.xrTableCell86.StylePriority.UseBackColor = false;
        xrSummary27.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell86.Summary = xrSummary27;
        this.xrTableCell86.Text = "xrTableCell67";
        this.xrTableCell86.Weight = 1.5000000051666145D;
        // 
        // xrTableCell87
        // 
        this.xrTableCell87.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell87.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN15")});
        this.xrTableCell87.Multiline = true;
        this.xrTableCell87.Name = "xrTableCell87";
        this.xrTableCell87.StylePriority.UseBackColor = false;
        this.xrTableCell87.StylePriority.UseTextAlignment = false;
        xrSummary28.FormatString = "{0:0.00%}";
        xrSummary28.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary28.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell87.Summary = xrSummary28;
        this.xrTableCell87.Text = "xrTableCell87";
        this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell87.Weight = 1.5139596074077506D;
        // 
        // DetailReport2
        // 
        this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.DetailReport3});
        this.DetailReport2.DataMember = "TReports";
        this.DetailReport2.DataSource = this.dsDetailPlanning1;
        this.DetailReport2.Level = 1;
        this.DetailReport2.Name = "DetailReport2";
        // 
        // Detail3
        // 
        this.Detail3.Expanded = false;
        this.Detail3.HeightF = 30F;
        this.Detail3.Name = "Detail3";
        // 
        // DetailReport3
        // 
        this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.GroupFooter3});
        this.DetailReport3.Level = 0;
        this.DetailReport3.Name = "DetailReport3";
        // 
        // Detail4
        // 
        this.Detail4.Expanded = false;
        this.Detail4.HeightF = 0F;
        this.Detail4.Name = "Detail4";
        // 
        // GroupFooter3
        // 
        this.GroupFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
        this.GroupFooter3.HeightF = 30F;
        this.GroupFooter3.Name = "GroupFooter3";
        // 
        // DetailReport4
        // 
        this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail5,
            this.DetailReport5,
            this.DetailReport6});
        this.DetailReport4.DataMember = "TReports";
        this.DetailReport4.DataSource = this.dsDetailPlanning1;
        this.DetailReport4.Level = 2;
        this.DetailReport4.Name = "DetailReport4";
        // 
        // Detail5
        // 
        this.Detail5.HeightF = 0F;
        this.Detail5.Name = "Detail5";
        this.Detail5.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TERMINALSTART", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        // 
        // DetailReport5
        // 
        this.DetailReport5.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail6,
            this.GroupFooter4,
            this.GroupHeader1});
        this.DetailReport5.Level = 0;
        this.DetailReport5.Name = "DetailReport5";
        // 
        // Detail6
        // 
        this.Detail6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
        this.Detail6.HeightF = 30F;
        this.Detail6.Name = "Detail6";
        this.Detail6.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        // 
        // xrTable7
        // 
        this.xrTable7.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable7.Name = "xrTable7";
        this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
        this.xrTable7.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable7.StylePriority.UseFont = false;
        this.xrTable7.StylePriority.UseTextAlignment = false;
        this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow9
        // 
        this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell53,
            this.xrTableCell69,
            this.xrTableCell71,
            this.xrTableCell88,
            xrTableCell89,
            xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell95,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105});
        this.xrTableRow9.Name = "xrTableRow9";
        this.xrTableRow9.StylePriority.UseBorders = false;
        this.xrTableRow9.Weight = 0.99653962401674279D;
        // 
        // xrTableCell19
        // 
        this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableCell19.Name = "xrTableCell19";
        this.xrTableCell19.StylePriority.UseBorders = false;
        this.xrTableCell19.Weight = 0.02322843243127401D;
        // 
        // xrTableCell50
        // 
        this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell50.Name = "xrTableCell50";
        this.xrTableCell50.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell50.StylePriority.UseBorders = false;
        this.xrTableCell50.Text = "[dEnd]";
        this.xrTableCell50.Weight = 0.94000000189098387D;
        // 
        // xrTableCell51
        // 
        this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell51.Name = "xrTableCell51";
        this.xrTableCell51.StylePriority.UseBorders = false;
        this.xrTableCell51.Weight = 0.036771470080832314D;
        // 
        // xrTableCell53
        // 
        this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell53.CanShrink = true;
        this.xrTableCell53.Name = "xrTableCell53";
        this.xrTableCell53.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell53.StylePriority.UseBorders = false;
        this.xrTableCell53.Text = "[dEnd]";
        this.xrTableCell53.Weight = 0.94000001810068412D;
        // 
        // xrTableCell69
        // 
        this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell69.Name = "xrTableCell69";
        this.xrTableCell69.StylePriority.UseBorders = false;
        this.xrTableCell69.Weight = 0.060000077867919832D;
        // 
        // xrTableCell71
        // 
        this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell71.CanShrink = true;
        this.xrTableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.TERMINALSTART")});
        this.xrTableCell71.Multiline = true;
        this.xrTableCell71.Name = "xrTableCell71";
        this.xrTableCell71.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell71.StylePriority.UseBorders = false;
        this.xrTableCell71.StylePriority.UseTextAlignment = false;
        xrSummary29.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell71.Summary = xrSummary29;
        this.xrTableCell71.Text = "xrTableCell71";
        this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell71.Weight = 0.940000017499514D;
        // 
        // xrTableCell88
        // 
        this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell88.Name = "xrTableCell88";
        this.xrTableCell88.StylePriority.UseBorders = false;
        this.xrTableCell88.Weight = 0.059999848986077908D;
        // 
        // xrTableCell91
        // 
        this.xrTableCell91.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11", "{0:0.00%}")});
        this.xrTableCell91.Name = "xrTableCell91";
        this.xrTableCell91.NullValueText = "0";
        this.xrTableCell91.Text = "xrTableCell91";
        this.xrTableCell91.Weight = 1.5000006723540706D;
        // 
        // xrTableCell92
        // 
        this.xrTableCell92.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell92.Name = "xrTableCell92";
        this.xrTableCell92.StylePriority.UseBackColor = false;
        this.xrTableCell92.Weight = 1.0000000358734684D;
        // 
        // xrTableCell93
        // 
        this.xrTableCell93.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell93.Multiline = true;
        this.xrTableCell93.Name = "xrTableCell93";
        this.xrTableCell93.StylePriority.UseBackColor = false;
        this.xrTableCell93.StylePriority.UseTextAlignment = false;
        this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell93.Weight = 1.5160388112436274D;
        // 
        // xrTableCell95
        // 
        this.xrTableCell95.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell95.Multiline = true;
        this.xrTableCell95.Name = "xrTableCell95";
        this.xrTableCell95.Text = "[COLUMN5]";
        this.xrTableCell95.Weight = 1.4800012234767266D;
        // 
        // xrTableCell97
        // 
        this.xrTableCell97.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell97.Multiline = true;
        this.xrTableCell97.Name = "xrTableCell97";
        this.xrTableCell97.Text = "[COLUMN6]";
        this.xrTableCell97.Weight = 1.5000000082073324D;
        // 
        // xrTableCell98
        // 
        this.xrTableCell98.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14", "{0:0.00%}")});
        this.xrTableCell98.Name = "xrTableCell98";
        this.xrTableCell98.NullValueText = "0";
        this.xrTableCell98.StylePriority.UseTextAlignment = false;
        this.xrTableCell98.Text = "xrTableCell98";
        this.xrTableCell98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell98.Weight = 1.5000000753423171D;
        // 
        // xrTableCell99
        // 
        this.xrTableCell99.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell99.Name = "xrTableCell99";
        this.xrTableCell99.Text = "xrTableCell42";
        this.xrTableCell99.Weight = 1.4999988597603831D;
        // 
        // xrTableCell100
        // 
        this.xrTableCell100.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell100.Name = "xrTableCell100";
        this.xrTableCell100.StylePriority.UseBackColor = false;
        this.xrTableCell100.Weight = 1.0000016620972003D;
        // 
        // xrTableCell102
        // 
        this.xrTableCell102.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell102.Multiline = true;
        this.xrTableCell102.Name = "xrTableCell102";
        this.xrTableCell102.StylePriority.UseBackColor = false;
        this.xrTableCell102.Weight = 1.5100012227511281D;
        // 
        // xrTableCell103
        // 
        this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell103.Multiline = true;
        this.xrTableCell103.Name = "xrTableCell103";
        this.xrTableCell103.Text = "xrTableCell45";
        this.xrTableCell103.Weight = 1.4899987890944635D;
        // 
        // xrTableCell104
        // 
        this.xrTableCell104.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell104.Multiline = true;
        this.xrTableCell104.Name = "xrTableCell104";
        this.xrTableCell104.StylePriority.UseBackColor = false;
        this.xrTableCell104.Weight = 1.5000000051666145D;
        // 
        // xrTableCell105
        // 
        this.xrTableCell105.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell105.Multiline = true;
        this.xrTableCell105.Name = "xrTableCell105";
        this.xrTableCell105.StylePriority.UseBackColor = false;
        this.xrTableCell105.StylePriority.UseTextAlignment = false;
        this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell105.Weight = 1.5139596074077506D;
        // 
        // GroupFooter4
        // 
        this.GroupFooter4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
        this.GroupFooter4.HeightF = 30F;
        this.GroupFooter4.Name = "GroupFooter4";
        // 
        // xrTable8
        // 
        this.xrTable8.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable8.Name = "xrTable8";
        this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
        this.xrTable8.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable8.StylePriority.UseFont = false;
        this.xrTable8.StylePriority.UseTextAlignment = false;
        this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow10
        // 
        this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            xrTableCell109,
            xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122});
        this.xrTableRow10.Name = "xrTableRow10";
        this.xrTableRow10.StylePriority.UseBorders = false;
        this.xrTableRow10.Weight = 0.99653962401674279D;
        // 
        // xrTableCell106
        // 
        this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell106.CanShrink = true;
        this.xrTableCell106.Name = "xrTableCell106";
        this.xrTableCell106.StylePriority.UseBorders = false;
        this.xrTableCell106.Weight = 1.0000000569909848D;
        // 
        // xrTableCell107
        // 
        this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell107.Name = "xrTableCell107";
        this.xrTableCell107.StylePriority.UseBorders = false;
        this.xrTableCell107.Weight = 0.99999994338070919D;
        // 
        // xrTableCell108
        // 
        this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell108.Name = "xrTableCell108";
        this.xrTableCell108.StylePriority.UseBorders = false;
        this.xrTableCell108.Weight = 0.99999986648559192D;
        // 
        // xrTableCell111
        // 
        this.xrTableCell111.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11")});
        this.xrTableCell111.Name = "xrTableCell111";
        this.xrTableCell111.StylePriority.UseBackColor = false;
        xrSummary30.FormatString = "{0:0.00%}";
        xrSummary30.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary30.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell111.Summary = xrSummary30;
        this.xrTableCell111.Text = "xrTableCell111";
        this.xrTableCell111.Weight = 1.5000006723540706D;
        // 
        // xrTableCell112
        // 
        this.xrTableCell112.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell112.Name = "xrTableCell112";
        this.xrTableCell112.StylePriority.UseBackColor = false;
        xrSummary31.FormatString = "{0:0.00%}";
        xrSummary31.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell112.Summary = xrSummary31;
        this.xrTableCell112.Weight = 1.0000000358734684D;
        // 
        // xrTableCell113
        // 
        this.xrTableCell113.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell113.Multiline = true;
        this.xrTableCell113.Name = "xrTableCell113";
        this.xrTableCell113.StylePriority.UseBackColor = false;
        this.xrTableCell113.StylePriority.UseTextAlignment = false;
        xrSummary32.FormatString = "{0:0.00%}";
        xrSummary32.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell113.Summary = xrSummary32;
        this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell113.Weight = 1.5160388112436274D;
        // 
        // xrTableCell114
        // 
        this.xrTableCell114.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell114.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell114.Multiline = true;
        this.xrTableCell114.Name = "xrTableCell114";
        this.xrTableCell114.StylePriority.UseBackColor = false;
        xrSummary33.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell114.Summary = xrSummary33;
        this.xrTableCell114.Text = "xrTableCell60";
        this.xrTableCell114.Weight = 1.4800012234767266D;
        // 
        // xrTableCell115
        // 
        this.xrTableCell115.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell115.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell115.Multiline = true;
        this.xrTableCell115.Name = "xrTableCell115";
        this.xrTableCell115.StylePriority.UseBackColor = false;
        xrSummary34.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell115.Summary = xrSummary34;
        this.xrTableCell115.Text = "xrTableCell61";
        this.xrTableCell115.Weight = 1.5000000082073324D;
        // 
        // xrTableCell116
        // 
        this.xrTableCell116.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell116.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14")});
        this.xrTableCell116.Name = "xrTableCell116";
        this.xrTableCell116.StylePriority.UseBackColor = false;
        this.xrTableCell116.StylePriority.UseTextAlignment = false;
        xrSummary35.FormatString = "{0:0.00%}";
        xrSummary35.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary35.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell116.Summary = xrSummary35;
        this.xrTableCell116.Text = "xrTableCell116";
        this.xrTableCell116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell116.Weight = 1.5000000753423171D;
        // 
        // xrTableCell117
        // 
        this.xrTableCell117.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell117.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell117.Name = "xrTableCell117";
        this.xrTableCell117.StylePriority.UseBackColor = false;
        xrSummary36.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell117.Summary = xrSummary36;
        this.xrTableCell117.Text = "xrTableCell63";
        this.xrTableCell117.Weight = 1.4999988597603831D;
        // 
        // xrTableCell118
        // 
        this.xrTableCell118.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell118.Name = "xrTableCell118";
        this.xrTableCell118.StylePriority.UseBackColor = false;
        this.xrTableCell118.Weight = 1.0000016620972003D;
        // 
        // xrTableCell119
        // 
        this.xrTableCell119.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell119.Multiline = true;
        this.xrTableCell119.Name = "xrTableCell119";
        this.xrTableCell119.StylePriority.UseBackColor = false;
        xrSummary37.FormatString = "{0:0.00%}";
        xrSummary37.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell119.Summary = xrSummary37;
        this.xrTableCell119.Weight = 1.5100012227511281D;
        // 
        // xrTableCell120
        // 
        this.xrTableCell120.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell120.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell120.Multiline = true;
        this.xrTableCell120.Name = "xrTableCell120";
        this.xrTableCell120.StylePriority.UseBackColor = false;
        xrSummary38.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell120.Summary = xrSummary38;
        this.xrTableCell120.Text = "xrTableCell66";
        this.xrTableCell120.Weight = 1.4899987890944635D;
        // 
        // xrTableCell121
        // 
        this.xrTableCell121.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell121.Multiline = true;
        this.xrTableCell121.Name = "xrTableCell121";
        this.xrTableCell121.StylePriority.UseBackColor = false;
        this.xrTableCell121.Weight = 1.5000000051666145D;
        // 
        // xrTableCell122
        // 
        this.xrTableCell122.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell122.Multiline = true;
        this.xrTableCell122.Name = "xrTableCell122";
        this.xrTableCell122.StylePriority.UseBackColor = false;
        this.xrTableCell122.StylePriority.UseTextAlignment = false;
        xrSummary39.FormatString = "{0:0.00%}";
        xrSummary39.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell122.Summary = xrSummary39;
        this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell122.Weight = 1.5139596074077506D;
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Expanded = false;
        this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("TERMINALSTART", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader1.Name = "GroupHeader1";
        // 
        // DetailReport6
        // 
        this.DetailReport6.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail7,
            this.GroupFooter1});
        this.DetailReport6.Level = 1;
        this.DetailReport6.Name = "DetailReport6";
        // 
        // Detail7
        // 
        this.Detail7.Expanded = false;
        this.Detail7.Name = "Detail7";
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
        this.GroupFooter1.HeightF = 30F;
        this.GroupFooter1.Name = "GroupFooter1";
        // 
        // xrTable9
        // 
        this.xrTable9.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable9.Name = "xrTable9";
        this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
        this.xrTable9.SizeF = new System.Drawing.SizeF(2301F, 30F);
        this.xrTable9.StylePriority.UseFont = false;
        this.xrTable9.StylePriority.UseTextAlignment = false;
        this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow11
        // 
        this.xrTableRow11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            xrTableCell131,
            xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144});
        this.xrTableRow11.Name = "xrTableRow11";
        this.xrTableRow11.StylePriority.UseBorders = false;
        this.xrTableRow11.Weight = 0.99653962401674279D;
        // 
        // xrTableCell128
        // 
        this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell128.Name = "xrTableCell128";
        this.xrTableCell128.StylePriority.UseBorders = false;
        this.xrTableCell128.Weight = 0.99999990440309017D;
        // 
        // xrTableCell129
        // 
        this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell129.Name = "xrTableCell129";
        this.xrTableCell129.StylePriority.UseBorders = false;
        this.xrTableCell129.Weight = 1.000000095968604D;
        // 
        // xrTableCell130
        // 
        this.xrTableCell130.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell130.Name = "xrTableCell130";
        this.xrTableCell130.StylePriority.UseBackColor = false;
        this.xrTableCell130.StylePriority.UseBorders = false;
        this.xrTableCell130.Text = "ทั้งหมด";
        this.xrTableCell130.Weight = 0.99999986648559192D;
        // 
        // xrTableCell133
        // 
        this.xrTableCell133.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell133.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN11")});
        this.xrTableCell133.Name = "xrTableCell133";
        this.xrTableCell133.StylePriority.UseBackColor = false;
        xrSummary40.FormatString = "{0:0.00%}";
        xrSummary40.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary40.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell133.Summary = xrSummary40;
        this.xrTableCell133.Text = "xrTableCell133";
        this.xrTableCell133.Weight = 1.5000006723540706D;
        // 
        // xrTableCell134
        // 
        this.xrTableCell134.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell134.Name = "xrTableCell134";
        this.xrTableCell134.StylePriority.UseBackColor = false;
        this.xrTableCell134.Weight = 1.0000000358734684D;
        // 
        // xrTableCell135
        // 
        this.xrTableCell135.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell135.Multiline = true;
        this.xrTableCell135.Name = "xrTableCell135";
        this.xrTableCell135.StylePriority.UseBackColor = false;
        this.xrTableCell135.StylePriority.UseTextAlignment = false;
        this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell135.Weight = 1.5160388112436274D;
        // 
        // xrTableCell136
        // 
        this.xrTableCell136.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell136.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN5")});
        this.xrTableCell136.Multiline = true;
        this.xrTableCell136.Name = "xrTableCell136";
        this.xrTableCell136.StylePriority.UseBackColor = false;
        xrSummary41.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell136.Summary = xrSummary41;
        this.xrTableCell136.Text = "xrTableCell60";
        this.xrTableCell136.Weight = 1.4800012234767266D;
        // 
        // xrTableCell137
        // 
        this.xrTableCell137.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell137.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN6")});
        this.xrTableCell137.Multiline = true;
        this.xrTableCell137.Name = "xrTableCell137";
        this.xrTableCell137.StylePriority.UseBackColor = false;
        xrSummary42.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell137.Summary = xrSummary42;
        this.xrTableCell137.Text = "xrTableCell61";
        this.xrTableCell137.Weight = 1.5000000082073324D;
        // 
        // xrTableCell138
        // 
        this.xrTableCell138.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell138.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN14")});
        this.xrTableCell138.Name = "xrTableCell138";
        this.xrTableCell138.StylePriority.UseBackColor = false;
        this.xrTableCell138.StylePriority.UseTextAlignment = false;
        xrSummary43.FormatString = "{0:0.00%}";
        xrSummary43.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary43.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell138.Summary = xrSummary43;
        this.xrTableCell138.Text = "xrTableCell138";
        this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell138.Weight = 1.5000000753423171D;
        // 
        // xrTableCell139
        // 
        this.xrTableCell139.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell139.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN10")});
        this.xrTableCell139.Name = "xrTableCell139";
        this.xrTableCell139.StylePriority.UseBackColor = false;
        xrSummary44.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell139.Summary = xrSummary44;
        this.xrTableCell139.Text = "xrTableCell63";
        this.xrTableCell139.Weight = 1.4999988597603831D;
        // 
        // xrTableCell140
        // 
        this.xrTableCell140.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell140.Name = "xrTableCell140";
        this.xrTableCell140.StylePriority.UseBackColor = false;
        this.xrTableCell140.Weight = 1.0000016620972003D;
        // 
        // xrTableCell141
        // 
        this.xrTableCell141.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell141.Multiline = true;
        this.xrTableCell141.Name = "xrTableCell141";
        this.xrTableCell141.StylePriority.UseBackColor = false;
        this.xrTableCell141.Weight = 1.5100012227511281D;
        // 
        // xrTableCell142
        // 
        this.xrTableCell142.BackColor = System.Drawing.Color.Yellow;
        this.xrTableCell142.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports.COLUMN13")});
        this.xrTableCell142.Multiline = true;
        this.xrTableCell142.Name = "xrTableCell142";
        this.xrTableCell142.StylePriority.UseBackColor = false;
        xrSummary45.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell142.Summary = xrSummary45;
        this.xrTableCell142.Text = "xrTableCell66";
        this.xrTableCell142.Weight = 1.4899987890944635D;
        // 
        // xrTableCell143
        // 
        this.xrTableCell143.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell143.Multiline = true;
        this.xrTableCell143.Name = "xrTableCell143";
        this.xrTableCell143.StylePriority.UseBackColor = false;
        this.xrTableCell143.Weight = 1.5000000051666145D;
        // 
        // xrTableCell144
        // 
        this.xrTableCell144.BackColor = System.Drawing.SystemColors.ControlDark;
        this.xrTableCell144.Multiline = true;
        this.xrTableCell144.Name = "xrTableCell144";
        this.xrTableCell144.StylePriority.UseBackColor = false;
        this.xrTableCell144.StylePriority.UseTextAlignment = false;
        this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell144.Weight = 1.5139596074077506D;
        // 
        // COLUMN2
        // 
        this.COLUMN2.DataMember = "TReports";
        this.COLUMN2.Expression = "[COLUMN1]/[COLUMN0]";
        this.COLUMN2.Name = "COLUMN2";
        // 
        // COLUMN4
        // 
        this.COLUMN4.DataMember = "TReports";
        this.COLUMN4.Expression = "([COLUMN0] - [COLUMN1])  / [COLUMN0]";
        this.COLUMN4.Name = "COLUMN4";
        // 
        // COLUMN7
        // 
        this.COLUMN7.DataMember = "TReports";
        this.COLUMN7.Expression = "Iif(\r\n([COLUMN6] / [COLUMN5])<0\r\n,0  \r\n,([COLUMN6] / [COLUMN5])\r\n)";
        this.COLUMN7.Name = "COLUMN7";
        // 
        // COLUMN12
        // 
        this.COLUMN12.DataMember = "TReports";
        this.COLUMN12.Expression = "Iif(([COLUMN11] / [COLUMN10])<0\r\n,0  \r\n,([COLUMN11] / [COLUMN10])\r\n)";
        this.COLUMN12.Name = "COLUMN12";
        // 
        // COLUMN15
        // 
        this.COLUMN15.DataMember = "TReports";
        this.COLUMN15.Expression = "Iif(([COLUMN14] / [COLUMN13])<0\r\n,0  \r\n ,([COLUMN14] / [COLUMN13])\r\n)";
        this.COLUMN15.Name = "COLUMN15";
        // 
        // FML_RPT_COL1
        // 
        this.FML_RPT_COL1.DataMember = "TReports";
        this.FML_RPT_COL1.DisplayName = "FML_RPT_COL1";
        this.FML_RPT_COL1.Expression = "Iif(\r\n[COLUMN3]<=0\r\n,0  \r\n,1\r\n)";
        this.FML_RPT_COL1.Name = "FML_RPT_COL1";
        // 
        // FML_RPT_COL7
        // 
        this.FML_RPT_COL7.DataMember = "TReports";
        this.FML_RPT_COL7.DisplayName = "FML_RPT_COL7";
        this.FML_RPT_COL7.Expression = "Iif(\r\n([COLUMN6] / [COLUMN5])<0\r\n,0  \r\n,([COLUMN6] / [COLUMN5])\r\n)";
        this.FML_RPT_COL7.Name = "FML_RPT_COL7";
        // 
        // xrtDetailPlanning
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.PageHeader,
            this.DetailReport2,
            this.DetailReport4});
        this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.COLUMN2,
            this.COLUMN4,
            this.COLUMN7,
            this.COLUMN12,
            this.COLUMN15,
            this.FML_RPT_COL1,
            this.FML_RPT_COL7});
        this.DataMember = "TReports";
        this.DataSource = this.dsDetailPlanning1;
        this.Margins = new System.Drawing.Printing.Margins(26, 100, 100, 100);
        this.PageWidth = 3000;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailPlanning1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion

    private void BottomMargin_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {

    }
}
