﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;

/// <summary>
/// Summary description for TBL_Vendor
/// </summary>
public class TBL_Vendor
{
    public static OracleConnection conn;
    public static Page page;
    public TBL_Vendor(Page _page, OracleConnection _conn)
    {
        //
        // TODO: Add constructor logic here
        //
        page = _page; conn = _conn;
    }
    private string ConvertToDateTime(string _date, string _Format, string _ToCul, bool ToUTC)
    {
        string datetime = "";
        if (!string.IsNullOrEmpty(_date))
        {
            DateTime expectedDate;
            //if (DateTime.TryParseExact(_date, _Format, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            //{
            //    _ToCul = (ToUTC && expectedDate.Year > 2500) ? "en-US" : _ToCul;
            //    datetime = expectedDate.ToString(_Format, new CultureInfo(_ToCul));
            //}
            if (DateTime.TryParseExact(_date, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "M/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "MM/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "M/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "M/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "MM/d/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else if (DateTime.TryParseExact(_date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
            {
                datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
            }
            else
            {
                datetime = "";
            }

            if (!string.IsNullOrEmpty(datetime))
            {

                int nYear_Todate = int.Parse(DateTime.Now.ToString("yyyy", new CultureInfo("en-US"))) + 100;
                string[] dDelivery = ("" + datetime).Split('/');
                string[] dDelivery2 = ("" + datetime).Split('-');
                string YYYY = dDelivery.Length > 2 ? dDelivery[2] + "" : dDelivery2[2] + "";
                string formateDelivery = "";
                if (YYYY.StartsWith("30"))
                {
                    formateDelivery = (YYYY.StartsWith("30")) ? "en-US" : "th-TH";
                }
                else if (int.Parse((YYYY).Substring(0, 2)) > 30)//กรณีที่ค่าปีเริ่มต้นมากกว่า30
                {
                    formateDelivery = "en-US";
                }
                else if (int.Parse((YYYY)) > nYear_Todate)//กรณีที่มากกว่า ค.ศ.+100ปี
                {
                    formateDelivery = "en-US";
                }
                //else if (int.Parse((YYYY)) < nYear_Todate)//กรณีที่ค่าน้อยกว่า ค.ศ.+100ปี
                //{
                //    formateDelivery = "en-US";
                //}
                else
                {
                    formateDelivery = "th-TH";
                }

                try
                {
                    datetime = DateTime.Parse(datetime).ToString("dd/MM/yyyy", new CultureInfo(formateDelivery));
                }
                catch
                {
                    if (DateTime.TryParseExact(datetime, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out expectedDate))
                    {
                        datetime = expectedDate.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                }
            }
            //}
            //catch
            //{

            //}
        }

        //Result = !string.IsNullOrEmpty(datetime) ? "TO_DATE('" + datetime + "','dd/MM/yyyy ')" : "null";
        return datetime;
        

        //string[] formats = { "MM/dd/yyyy", "dd/MM/yyyy", "yyyy/MM/dd", "yyyy-MM-dd" };
        //
        //if (!DateTime.TryParseExact(_date, formats, new CultureInfo(_ToCul), DateTimeStyles.None, out expectedDate))
        //{
        //    Console.Write("Thank you Mario, but the DateTime is in another format.");
        //}

    }
    
    #region Data
    private string _EMPLOYEEID;
    private string _EMPTPYE;
    private string _FNAME;
    private string _LNAME;
    private string _PERS_CODE;
    private string _TRANS_ID;
    private string _LICENSE_NO;
    private string _BIRTHDATE;
    private string _TEL;
    private string _TEL2;
    private string _MAIL;
    private string _MAIL2;
    private string _DRIVERNO_BEGIN;
    private string _DRIVERNO_Expire;
    private string _DRVSTATUS;
    #endregion
    #region Property
    /// <summary>
    /// รหัสพนักงาน/รหัส พขร.ต้องเหมือนใน SAP
    /// </summary>
    public string EMPLOYEEID
    {
        get { return this._EMPLOYEEID; }
        set { this._EMPLOYEEID = value; }
    }
    /// <summary>
    /// ประเภทรหัสพนักงาน/รหัส พขร.Look in TEMPLOYEETYPES
    /// </summary>
    public string EMPTPYE
    {
        get { return this._EMPTPYE; }
        set { this._EMPTPYE = value; }
    }
    /// <summary>
    /// ชื่อพนักงาน
    /// </summary>
    public string FNAME
    {
        get { return this._FNAME; }
        set { this._FNAME = value; }
    }
    /// <summary>
    /// นามสกุลพนักงาน
    /// </summary>
    public string LNAME
    {
        get { return this._LNAME; }
        set { this._LNAME = value; }
    }
    /// <summary>
    /// รหัสบัตรประชาชน
    /// </summary>
    public string PERS_CODE
    {
        get { return this._PERS_CODE; }
        set { this._PERS_CODE = value; }
    }
    /// <summary>
    /// รหัสผู้ขนส่ง
    /// </summary>
    public string TRANS_ID
    {
        get { return this._TRANS_ID; }
        set { this._TRANS_ID = value; }
    }
    /// <summary>
    /// หมายเลขใบขับขี่ประเภท 4
    /// </summary>
    public string LICENSE_NO
    {
        get { return this._LICENSE_NO; }
        set { this._LICENSE_NO = value; }
    }
    /// <summary>
    /// วัน/เดือน/ปี เกิด พนักงาน
    /// </summary>
    public string BIRTHDATE
    {
        get { return this._BIRTHDATE; }
        set { this._BIRTHDATE = ConvertToDateTime(value, "yyyy-MM-dd", "en-US", true); }
    }
    /// <summary>
    /// เบอร์โทรศัพท์หลัก
    /// </summary>
    public string TEL
    {
        get { return this._TEL; }
        set { this._TEL = value; }
    }
    /// <summary>
    /// เบอร์โทรศัพท์สำรอง
    /// </summary>
    public string TEL2
    {
        get { return this._TEL2; }
        set { this._TEL2 = value; }
    }
    /// <summary>
    /// เมล์หลัก
    /// </summary>
    public string MAIL
    {
        get { return this._MAIL; }
        set { this._MAIL = value; }
    }
    /// <summary>
    /// เมล์สำรอง
    /// </summary>
    public string MAIL2
    {
        get { return this._MAIL2; }
        set { this._MAIL2 = value; }
    }
    /// <summary>
    /// วันเริ่มต้นใบขับขี่ประเภท 4 
    /// </summary>
    public string DRIVERNO_BEGIN
    {
        get { return this._DRIVERNO_BEGIN; }
        set { this._DRIVERNO_BEGIN = ConvertToDateTime(value, "yyyy-MM-dd", "en-US", true); }
    }
    /// <summary>
    /// วันสิ้นสุดใบขับขี่ประเภท 4 
    /// </summary>
    public string DRIVERNO_Expire
    {
        get { return this._DRIVERNO_Expire; }
        set { this._DRIVERNO_Expire = ConvertToDateTime(value, "yyyy-MM-dd", "en-US", true); }
    }
    /// <summary>
    /// สถานะ 
    /// </summary>
    public string DRVSTATUS
    {
        get { return this._DRVSTATUS; }
        set { this._DRVSTATUS = value; }
    }
    #endregion
    #region Method
    public bool Open()
    {
        string sqlstr;
        bool result = false;

        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        //
        sqlstr = @"SELECT EMP_SAP.SEMPLOYEEID,EMP_SAP.FNAME,EMP_SAP.LNAME,EMP_SAP.PERS_CODE
 FROM TEMPLOYEE_SAP EMP_SAP
LEFT JOIN TEMPLOYEE  EMP_TMS ON EMP_SAP.SEMPLOYEEID=EMP_TMS.SEMPLOYEEID
WHERE EMP_SAP.PERS_CODE='" + PERS_CODE + "'";
        DataTable dt = new DataTable();
        new OracleDataAdapter(sqlstr, conn).Fill(dt);
        if (dt.Rows.Count >= 1)
        {
            DataRow row = dt.Rows[0];
            EMPLOYEEID = row["SEMPLOYEEID"].ToString();
            FNAME = row["FNAME"].ToString();
            LNAME = row["LNAME"].ToString();
            PERS_CODE = row["PERS_CODE"].ToString();
            dt.Dispose();
            result = true;
        }

        return result;
    }
    #endregion
}