﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.OracleClient;
using System.Web.Configuration;

/// <summary>
/// Summary description for Calendar
/// </summary>
public class Calendar
{
    static string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    public Calendar()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// ฟังค์ชันที่สร้างโค้ดจาวาสคริปปฏิทินทั้งเดือน
    /// </summary>
    /// <param name="_ltr">เป็นตำแหน่งที่สร้างสคริปขึ้น</param>
    /// <param name="_ctr">Control TextBox กับ Toggle สลับกัน 2 คู่</param>
    public static void GenerateJavaScriptFullMonthCalendar(Literal _ltr, int _Month, int _Year)
    {

        _ltr.Text = //String.Format(
        @"
<script type='text/javascript'>
var nOldMonth ;
var nOldYear ;

	   $(document).ready(function() {
             // list Month
             monthsList('#months-list');
              $(document).on('click', '#months-list a', function() {
                // get month from month anchor hash
                var month = this.hash.substring(1);
                // go to new month
                $('#calendar').fullCalendar('gotoDate', goToMonth(month));
                return false;
            });

      
	        });	

          




           function reloadCal(_Month,_Year) {



                var current_url = '';
                var new_url     = '';



		            $('#calendar').fullCalendar({
                        lang:'th',
                        year: _Year,
                        month: _Month -1 ,
			            theme: true,
			            editable: false ,
                         events: function( start, end, timezone, callback  ) {
           var month = start._d.getMonth();
           var year = start._d.getFullYear();
           var emonth = end._d.getMonth();
           var eyear = end._d.getFullYear();
            
            new_url  = 'ashx/calendarjson.ashx?sMonth=' + month + '&sYear=' + year + '&eMonth=' + emonth + '&eYear=' + eyear + '&sProcess=' + 1 ;


            //เช็คเดือน เพื่อไปกำหนด class ให้ li active เดือน ปัจจุบัน หรือเดือนที่เลือก
            var nCheckCurrentMonth =  ( emonth == 0) ? 12 : emonth;


           $('li[id$=li' + nCheckCurrentMonth + ']').attr('class', 'mCurrentMonth');
           $('li[id$=li' + nOldMonth + ']').attr('class', '');

              nOldMonth = nCheckCurrentMonth;

            if( new_url != current_url ){

                $.ajax({
                    url: new_url,
                    dataType: 'json',
                    type: 'POST',
                    success: function( response ) {

                        current_url = new_url;
                        user_events = response;

                        callback(response);
                    }
                })
           }else{
               callback(user_events);
           }
                    },
                      eventRender: function (event, element) {
                        element.find('span.fc-event-title').html(element.find('span.fc-event-title').text());           
                     } ,
			             loading: function(bool) {
				        // $('#loading').toggle(bool);

                           if(bool){
                              LoadingPanel.Show();
                          }else{
                                LoadingPanel.Hide();
                           }
			             }
                    
                     });

         }

           function goToMonth(month) {
                var date = new Date();
                var d = date.getDate();
                var m = month !== undefined ? parseInt(month, 0) : date.getMonth();
                var y = date.getFullYear();
                return new Date(y, m, d);
            }


              function monthsList(element) {
             
               var date = new Date();
               var mCheck = date.getMonth()

                var monthNames = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
                for (var m = 0; m < monthNames.length; m++) {
                     
                
                    $(element).append('<li id=""li' + (m+1) + '"" class=""""><a href=""#' + m + '"" >' + monthNames[m] + '</a></li>');
                }
            }

        

        </script>
        ";




    }
    public static void GenerateJavaScriptFullMonthCalendarDetail(Literal _ltr, int _Month, int _Year,string sDate)
    {

        _ltr.Text = //String.Format(
        @"
<script type='text/javascript'>
var nOldMonth ;
var nOldYear ;

	   $(document).ready(function() {
             // list Month
             monthsList('#months-list');
              $(document).on('click', '#months-list a', function() {
                // get month from month anchor hash
                var month = this.hash.substring(1);
                // go to new month
                $('#calendar').fullCalendar('gotoDate', goToMonth(month));
                return false;
            });

      
	        });	

          




           function reloadCal(_Month,_Year) {



                var current_url = '';
                var new_url     = '';



		            $('#calendar').fullCalendar({
                        lang:'th',
                        year: _Year,
                        month: _Month -1 ,
			            theme: true,
			            editable: false ,
                        //disableDragging:false,
                         events: function( start, end, timezone, callback  ) {
           var month = start._d.getMonth();
           var year = start._d.getFullYear();
           var emonth = end._d.getMonth();
           var eyear = end._d.getFullYear();
            
              new_url  = 'ashx/calendarjson.ashx?sMonth=' + month + '&sYear=' + year + '&eMonth=' + emonth + '&eYear=' + eyear + '&sProcess=' + 2 ;


            //เช็คเดือน เพื่อไปกำหนด class ให้ li active เดือน ปัจจุบัน หรือเดือนที่เลือก
            var nCheckCurrentMonth =  ( emonth == 0) ? 12 : emonth;


           $('li[id$=li' + nCheckCurrentMonth + ']').attr('class', 'mCurrentMonth');
           $('li[id$=li' + nOldMonth + ']').attr('class', '');

              nOldMonth = nCheckCurrentMonth;

            if( new_url != current_url ){

                $.ajax({
                    url: new_url,
                    dataType: 'json',
                    type: 'POST',
                    success: function( response ) {

                        current_url = new_url;
                        user_events = response;

                        callback(response);
                    }
                })
           }else{
               callback(user_events);
           }
                    },
                      eventRender: function (event, element) {
                        element.find('span.fc-event-title').html(element.find('span.fc-event-title').text());           
                     } ,
			             loading: function(bool) {
				        // $('#loading').toggle(bool);

                           if(bool){
                              LoadingPanel.Show();
                          }else{
                                LoadingPanel.Hide();
                           }
			             }
                    
                     });

         }

           function goToMonth(month) {
                var date = new Date();
                var d = date.getDate();
                var m = month !== undefined ? parseInt(month, 0) : date.getMonth();
                var y = date.getFullYear();
                return new Date(y, m, d);
            }


              function monthsList(element) {
             
               var date = new Date();
               var mCheck = date.getMonth()

                var monthNames = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
                for (var m = 0; m < monthNames.length; m++) {
                     
                
                    $(element).append('<li id=""li' + (m+1) + '"" class=""""><a href=""#' + m + '"" >' + monthNames[m] + '</a></li>');
                }
            }

        

        </script>
        ";




    }
    public static void GenerateJavaScriptFullMonthCalendarDetailOnly(Literal _ltr, int _Month, int _Year)
    {

        _ltr.Text = //String.Format(
        @"
<script type='text/javascript'>
var nOldMonth ;
var nOldYear ;

	   $(document).ready(function() {
             reloadCal(" + _Month + "," + _Year + @");

             // list Month
             monthsList('#months-list');
              $(document).on('click', '#months-list a', function() {
                // get month from month anchor hash
                var month = this.hash.substring(1);
                // go to new month
                $('#calendar').fullCalendar('gotoDate', goToMonth(month));
                return false;
            });

      
	        });	

          




           function reloadCal(_Month,_Year) {



                var current_url = '';
                var new_url     = '';



		            $('#calendar').fullCalendar({
                        lang:'th',
                        year: _Year,
                        month: _Month -1 ,
			            theme: true,
			            editable: false ,
                         events: function( start, end, timezone, callback  ) {
           var month = start._d.getMonth();
           var year = start._d.getFullYear();
           var emonth = end._d.getMonth();
           var eyear = end._d.getFullYear();
            
              new_url  = 'ashx/calendarjson.ashx?sMonth=' + month + '&sYear=' + year + '&eMonth=' + emonth + '&eYear=' + eyear + '&sProcess=' + 3 ;


            //เช็คเดือน เพื่อไปกำหนด class ให้ li active เดือน ปัจจุบัน หรือเดือนที่เลือก
            var nCheckCurrentMonth =  ( emonth == 0) ? 12 : emonth;


           $('li[id$=li' + nCheckCurrentMonth + ']').attr('class', 'mCurrentMonth');
           $('li[id$=li' + nOldMonth + ']').attr('class', '');

              nOldMonth = nCheckCurrentMonth;

            if( new_url != current_url ){

                $.ajax({
                    url: new_url,
                    dataType: 'json',
                    type: 'POST',
                    success: function( response ) {

                        current_url = new_url;
                        user_events = response;

                        callback(response);
                    }
                })
           }else{
               callback(user_events);
           }
                    },
                      eventRender: function (event, element) {
                        element.find('span.fc-event-title').html(element.find('span.fc-event-title').text());           
                     } ,
			             loading: function(bool) {
				        // $('#loading').toggle(bool);

                           if(bool){
                              LoadingPanel.Show();
                          }else{
                                LoadingPanel.Hide();
                           }
			             }
                    
                     });

         }

           function goToMonth(month) {
                var date = new Date();
                var d = date.getDate();
                var m = month !== undefined ? parseInt(month, 0) : date.getMonth();
                var y = date.getFullYear();
                return new Date(y, m, d);
            }


              function monthsList(element) {
             
               var date = new Date();
               var mCheck = date.getMonth()

                var monthNames = ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
                for (var m = 0; m < monthNames.length; m++) {
                     
                
                    $(element).append('<li id=""li' + (m+1) + '"" class=""""><a href=""#' + m + '"" >' + monthNames[m] + '</a></li>');
                }
            }

        

        </script>
        ";




    }
    public static void GenerateJavaScriptadditemday(Literal _ltr, string _event)
    {
        _ltr.Text = //String.Format(
        @"<script type='text/javascript'>
        	          $('.fc-day-number').each(function() {
        
            var day = parseInt($(this).html());
        
       
            if(urls[day] != undefined) {

                $(this).html('<a href=' + urls[day] + '>' + day + " + _event + "'</a>');}});</script>";



    }



    public static int BookingCalendar(DateTime DateBooking, string _ID, string _TypePage, string _Capacity, string _SEAL_HIT)
    {

        if (_ID != "")
        {

            int temp;
            int nSEAL_HIT = int.TryParse(_SEAL_HIT, out temp) ? temp : 0;
            int nCapacity = int.TryParse(_Capacity.Replace(",", ""), out temp) ? temp : 0;//edit by Bow 4-07-2014


            string sDate = DateBooking.ToString("dd/MM/yyyy", new CultureInfo("en-US"));

            //ถ้าเป็น ตีซิล ไม่ต้องเช็ค รถใหญ่รถเล็ก CASE WHEN {2} = 0 THEN 1 ELSE 0 END   // 0 คือไม่ ตีซิล

            string sqlQuery = @"SELECT COUNT(*) FROM (

SELECT
    m.CAR_AMOUNT as CAR_AMOUNT1,  (SELECT COUNT(*)  FROM TBL_CALENDAR_BOOKING WHERE SREFERENTID != '{3}'  AND    SREFERENTTYPE  != '{4}'  AND  SEAL_HIT != 3 AND NCAPACITY BETWEEN m.BEGIN_CAP AND  m.END_CAP AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') )   + CASE WHEN  M.STYPECAP = '1' THEN CASE WHEN {2} = 0 THEN 1 ELSE 0 END ELSE 0 END   AS CURRENT_CAR_AMOUNT1 ,  
     ml.CAR_AMOUNT as CAR_AMOUNT2,    (SELECT COUNT(*) FROM TBL_CALENDAR_BOOKING WHERE SREFERENTID != '{3}'  AND    SREFERENTTYPE  != '{4}'  AND SEAL_HIT != 3 AND NCAPACITY BETWEEN ml.BEGIN_CAP AND  ml.END_CAP AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') )  + CASE WHEN  M.STYPECAP = '2' THEN CASE WHEN {2} = 0 THEN 1 ELSE 0 END ELSE 0 END  AS CURRENT_CAR_AMOUNT2,
    m.SEAL_HIT ,NVL( (SELECT SUM(NVL(SEAL_HIT,0)) FROM TBL_CALENDAR_BOOKING WHERE SREFERENTID != '{3}'  AND    SREFERENTTYPE  != '{4}'  AND SEAL_HIT != 3 AND TRUNC(DBOOKING) = TO_DATE('{1}','fmdd/mm/yyyy') ),0) + {2}  AS CURRENT_SEAL_HIT

FROM TBL_CONDITIONS m
INNER JOIN TBL_CONDITIONS ml ON M.NROWADD = ml.NROWADD AND NVL(ml.ACTIVE_FLAG,'Y') = 'Y' AND ML.CONDITIONS_ID != M.CONDITIONS_ID
WHERE NVL(m.ACTIVE_FLAG,'Y') = 'Y'
AND {0}  BETWEEN m.BEGIN_CAP AND m.END_CAP

) o
WHERE
   o.CAR_AMOUNT1 >=  o.CURRENT_CAR_AMOUNT1
   AND 
     o.CAR_AMOUNT2 >= o.CURRENT_CAR_AMOUNT2
     AND
    o.SEAL_HIT >=  o.CURRENT_SEAL_HIT
";

            //ตรวจสอบว่ามีการ booking วันนี้แล้วหรือยัง
            string sValue = nSEAL_HIT != 3 ? CommonFunction.Get_Value(conn, string.Format(sqlQuery, nCapacity.ToString(), sDate, nSEAL_HIT.ToString(), _ID, _TypePage)) : "3";

            if (sValue != "0")
            {

                DataTable dtCheck = CommonFunction.Get_Data(conn, string.Format("SELECT * FROM TBL_CALENDAR_BOOKING WHERE SREFERENTID = '{0}' AND SREFERENTTYPE = '{1}'", _ID, _TypePage));

                string sSQL = "";
                if (dtCheck.Rows.Count > 0)  //มีข้อมูลอยู่แล้วให้ทำการ update 1 วัน ต่อ 1 รายการ
                {
                    sSQL = string.Format(@"UPDATE TBL_CALENDAR_BOOKING SET 
   DBOOKING = TO_DATE('{0}','fmdd/mm/yyyy'), NCAPACITY = {1}, 
   SEAL_HIT = {2}   WHERE  SREFERENTID = '{3}' AND SREFERENTTYPE = '{4}'", sDate, nCapacity, nSEAL_HIT.ToString(), _ID, _TypePage);
                }
                else
                {
                    sSQL = string.Format(@"INSERT INTO TBL_CALENDAR_BOOKING (SBOOKINGID, 
   DBOOKING, NCAPACITY, 
   SEAL_HIT, SREFERENTID, SREFERENTTYPE) 
VALUES ( '{0}',TO_DATE('{1}','fmdd/mm/yyyy'),
{2},
 {3},
 '{4}',
 '{5}')", Gen_IDCalendar(), sDate, nCapacity, nSEAL_HIT.ToString(), _ID, _TypePage);

                }

                if (AddTODB(sSQL) > 0)
                {
                    return 1;
                }
            }
            else
            {
                return 0;
            }

        }

        return 0;
    }

    private static int AddTODB(string strQuery)
    {
        int nCount = 0;

        try
        {

            using (OracleConnection con = new OracleConnection(conn))
            {

                if (con.State == ConnectionState.Closed) con.Open();

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    nCount = com.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        catch (Exception ex)
        {
            return 0;
        }

        return nCount;
    }

    private static string Gen_IDCalendar()
    {
        string Result = "";

        string strsql = "SELECT SBOOKINGID FROM (SELECT SBOOKINGID FROM TBL_CALENDAR_BOOKING ORDER BY SBOOKINGID DESC) WHERE ROWNUM <= 1";

        DataTable dt = CommonFunction.Get_Data(conn, strsql);


        if (dt.Rows.Count > 0)
        {

            string sID = Gen_ID_yymmddxxx(dt.Rows[0]["SBOOKINGID"] + "", 3);

            Result = sID;
        }
        else
        {

            Result = Gen_ID_yymmddxxx("", 3);
        }

        return Result;
    }

    private static string Gen_ID_yymmddxxx(string _wwcode, int _length)
    {

        string sReturn = "";
        int length = _length;

        string yy = DateTime.Now.ToString("yy", new CultureInfo("en-Us"));

        string mm = DateTime.Now.Month.ToString().PadLeft(2, '0');

        string dd = DateTime.Now.Day.ToString().PadLeft(2, '0');

        if (_wwcode != "")
        {

            int id = Convert.ToInt32(_wwcode.Substring(6, _length));
            sReturn = yy + mm + dd + (id + 1).ToString().PadLeft(length, '0');
        }
        else
        {
            sReturn = yy + mm + dd + (1).ToString().PadLeft(length, '0');
        }

        return sReturn;
    }

}
