﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxHiddenField;
using Helper;
using System.Reflection;

public enum MessageType
{
    None = 0
    ,
    Message = 1
    ,
    Confirm = 3
}

public enum MessageFlag
{
    None = 0
    ,
    Warning = 2
    ,
    Error = 3
    ,
    Wating = 4
    ,
    Completed = 5
    ,
    Exception = 6
    ,
    Information = 7
}
public sealed class Util
{
    public static void MesssgeBox(System.Web.UI.Page page, string Message, string headerText = null)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplMessageBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplMessageBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("MessageBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("MessageBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("MessageBoxMessage");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        up.Update();

    }
    public static void MesssgeBox(System.Web.UI.Page page, string Message, string headerText, string initFunction)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplMessageBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplMessageBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("MessageBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("MessageBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("MessageBoxMessage");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        msg.ClientSideEvents.Closing = "function(s, e) {" + initFunction + "}";
        up.Update();
    }
    public static void ConfirmBox(Page page, string Message, string headerText = null)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplMessageBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplMessageBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("MessageBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("MessageBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("ConfirmBox_Message");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        up.Update();
    }
    public static void ConfirmBox(Page page, string Message, string headerText, string onConfirmFunction, string onCancelFunction)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplMessageBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplMessageBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("ConfirmBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("ConfirmBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("ConfirmBox_Message");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        up.Update();
    }

    public static void ErrorBox(System.Web.UI.Page page, string Message, string headerText = null)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplErrorBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplErrorBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("ErrorBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("ErrorBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("ErrorBoxMessage");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        up.Update();

    }
    public static void ErrorBox(System.Web.UI.Page page, string Message, string headerText, string initFunction)
    {
        UpdatePanel up = (UpdatePanel)page.Master.FindControl("uplErrorBox");
        if (up == null) up = (UpdatePanel)page.Master.Master.FindControl("uplErrorBox");
        ASPxPopupControl msg = (ASPxPopupControl)page.Master.FindControl("ErrorBox");
        if (msg == null) msg = (ASPxPopupControl)page.Master.Master.FindControl("ErrorBox");

        if (String.IsNullOrEmpty(headerText))
            headerText = "Message";
        ASPxLabel msg_control = (ASPxLabel)msg.FindControl("ErrorBoxMessage");
        msg.HeaderText = headerText;
        msg_control.Text = Message;
        msg.ShowOnPageLoad = true;
        msg.ClientSideEvents.Closing = "function(s, e) {" + initFunction + "}";
        up.Update();
    }

    public static void OnMessageBox(System.Web.UI.Page page
        , String Message
        , String headerText
        , MessageType messagetype
        , MessageFlag messageFlag
        , String onMethodConfirm = null
        , String onMethodCancel = null
        , Object[] parameters = null)
    {
        UpdatePanel upl = (UpdatePanel)page.Master.FindControl("uplMessage");
        if (upl == null) upl = (UpdatePanel)page.Master.Master.FindControl("uplMessage");
        try
        {
            //////////////////////////////////////////////////////////////////////////
            ASPxPopupControl popupMessage = (ASPxPopupControl)page.Master.FindControl("popupMessage");
            if (popupMessage == null) popupMessage = (ASPxPopupControl)page.Master.Master.FindControl("popupMessage");
            //////////////////////////////////////////////////////////////////////////
            ASPxLabel lbMessage = (ASPxLabel)popupMessage.FindControl("lblMessage");
            if (lbMessage == null) lbMessage = (ASPxLabel)page.Master.Master.FindControl("lblMessage");
            lbMessage.EncodeHtml = (false);
            ////////////////////////////////////////////////////////////////////////// 
            ASPxHiddenField hdnMessage = (ASPxHiddenField)page.Master.FindControl("hdnMessage");
            if (hdnMessage == null) hdnMessage = (ASPxHiddenField)page.Master.Master.FindControl("hdnMessage");
            //////////////////////////////////////////////////////////////////////////
            popupMessage.HeaderText = headerText;
            lbMessage.Text = Message;
            popupMessage.ShowOnPageLoad = true;
            popupMessage.Modal = true;
            //////////////////////////////////////////////////////////////////////////
            ASPxImage imgMessage = (ASPxImage)popupMessage.FindControl("imgMessage");
            if (imgMessage != null)
            {
                if (messageFlag == MessageFlag.Completed)
                {
                    imgMessage.ImageUrl = String.Format("~/Images/dialog-information.png");
                    imgMessage.Visible = true;
                }
                else if (messageFlag == MessageFlag.Error)
                {
                    imgMessage.ImageUrl = String.Format("~/Images/dialog-error.png");
                    imgMessage.Visible = true;
                }
                else if (messageFlag == MessageFlag.Exception)
                {
                    imgMessage.ImageUrl = String.Format("~/Images/dialog-error.png");
                    imgMessage.Visible = true;
                }
                else if (messageFlag == MessageFlag.Warning)
                {
                    imgMessage.ImageUrl = String.Format("~/Images/dialog-warning.png");
                    imgMessage.Visible = true;
                }
                else if (messageFlag == MessageFlag.Information)
                {
                    imgMessage.ImageUrl = String.Format("~/Images/dialog-information.png");
                    imgMessage.Visible = true;
                }
                else if (messageFlag == MessageFlag.None)
                {
                    imgMessage.Visible = false;
                }
            }
            //////////////////////////////////////////////////////////////////////////
            ASPxButton btnConfirm = (ASPxButton)popupMessage.FindControl("btnMessageConfirm");
            if (btnConfirm != null) { btnConfirm.Visible = false; }

            ASPxButton btnCancel = (ASPxButton)popupMessage.FindControl("btnMessageCancel");
            if (btnCancel != null) { btnCancel.Visible = false; }
            //////////////////////////////////////////////////////////////////////////
            if (messagetype == MessageType.Confirm)
            {
                //////////////////////////////////////////////////////////////////////////
                hdnMessage.Set("MethodConfirmBox", onMethodConfirm);
                hdnMessage.Set("MethodCancelBox", onMethodCancel);
                hdnMessage.Set("MessageParameters", parameters);
                btnConfirm.Visible = true;
                btnCancel.Visible = true;
            }
            else if (messagetype == MessageType.Message)
            {
                //////////////////////////////////////////////////////////////////////////
                hdnMessage.Set("MethodConfirmBox", onMethodConfirm);
                hdnMessage.Set("MethodCancelBox", null);
                hdnMessage.Set("MessageParameters", parameters);
                btnConfirm.Visible = true;
                btnCancel.Visible = false;
            }
            else if (messagetype == MessageType.None)
            {
                //////////////////////////////////////////////////////////////////////////
                hdnMessage.Set("MethodConfirmBox", onMethodConfirm);
                hdnMessage.Set("MethodCancelBox", null);
                hdnMessage.Set("MessageParameters", parameters);
                btnConfirm.Visible = true;
                btnCancel.Visible = false;
            }

        }
        catch (Exception ex)
        {
            throw;
        }
        finally
        {
            upl.Update();
        }
    }

    public static void PreScript(Page page, string code)
    {
        ScriptManager.RegisterStartupScript(page, page.GetType(), "title", code, true);
    }
}