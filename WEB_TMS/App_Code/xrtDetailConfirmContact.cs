﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for xrtDetailConfirmContact
/// </summary>
public class xrtDetailConfirmContact : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private FormattingRule formattingRule1;
    private dsDetailConfirmContact dsDetailConfirmContact1;
    private DetailReportBand DetailReport;
    private DetailBand Detail1;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell23;
    private XRTableCell xrTableCell25;
    private XRTableCell xrTableCell27;
    private XRTableCell xrTableCell29;
    private XRTableCell xrTableCell30;
    private XRTableCell xrTableCell31;
    private XRTableCell xrTableCell32;
    private XRTableCell xrTableCell33;
    private XRTableCell xrTableCell34;
    private XRTableCell xrTableCell35;
    private XRTableCell xrTableCell36;
    private XRTableCell xrTableCell37;
    private XRTableCell xrTableCell38;
    private XRTableCell xrTableCell39;
    private XRTableCell xrTableCell40;
    private XRTableCell xrTableCell41;
    private XRTableCell xrTableCell42;
    private XRTableCell xrTableCell43;
    private XRTableCell xrTableCell44;
    private XRTableCell xrTableCell46;
    private XRLabel xrLabel1;
    private GroupHeaderBand GroupHeader1;
    private GroupFooterBand GroupFooter1;
    private XRTable xrTable3;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell70;
    private XRTableCell xrTableCell72;
    private XRTableCell xrTableCell73;
    private XRTableCell xrTableCell74;
    private XRTableCell xrTableCell75;
    private XRTableCell xrTableCell76;
    private XRTableCell xrTableCell77;
    private XRTableCell xrTableCell78;
    private XRTableCell xrTableCell79;
    private XRTableCell xrTableCell80;
    private XRTableCell xrTableCell81;
    private XRTableCell xrTableCell82;
    private XRTableCell xrTableCell83;
    private XRTableCell xrTableCell84;
    private XRTableCell xrTableCell85;
    private XRTableCell xrTableCell86;
    private XRTableCell xrTableCell87;
    private XRTableCell xrTableCell88;
    private XRTableCell xrTableCell89;
    private XRTableCell xrTableCell90;
    private XRTableCell xrTableCell91;
    private XRTableCell xrTableCell92;
    private XRPageBreak xrPageBreak1;
    private XRTableCell xrTableCell96;
    private XRTableCell xrTableCell94;
    private XRTableCell xrTableCell99;
    private XRTableCell xrTableCell97;
    private XRTableCell xrTableCell98;
    private XRTableCell xrTableCell47;
    private ReportHeaderBand ReportHeader;
    private PageHeaderBand PageHeader;
    private XRTable xrTable4;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell48;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell45;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell11;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell8;
    private XRTableCell xrTableCell10;
    private XRTableCell xrTableCell12;
    private XRTableCell xrTableCell14;
    private XRTableCell xrTableCell13;
    private XRTableCell xrTableCell15;
    private XRTableCell xrTableCell16;
    private XRTableCell xrTableCell17;
    private XRTableCell xrTableCell18;
    private XRTableCell xrTableCell19;
    private XRTableCell xrTableCell20;
    private XRTableCell xrTableCell21;
    private XRTableCell xrTableCell22;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell49;
    private XRTableCell xrTableCell50;
    private XRTableCell xrTableCell51;
    private XRTableCell xrTableCell52;
    private XRTableCell xrTableCell53;
    private XRTableCell xrTableCell54;
    private XRTableCell xrTableCell55;
    private XRTableCell xrTableCell56;
    private XRTableCell xrTableCell57;
    private XRTableCell xrTableCell58;
    private XRTableCell xrTableCell59;
    private XRTableCell xrTableCell60;
    private XRTableCell xrTableCell61;
    private XRTableCell xrTableCell62;
    private XRTableCell xrTableCell63;
    private XRTableCell xrTableCell64;
    private XRTableCell xrTableCell65;
    private XRTableCell xrTableCell66;
    private XRTableCell xrTableCell67;
    private XRTableCell xrTableCell68;
    private XRTableCell xrTableCell69;
    private XRTable xrTable5;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell93;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell71;
    private XRTableCell xrTableCell95;
    private ReportFooterBand ReportFooter;
    private XRTable xrTable6;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell100;
    private XRTableCell xrTableCell102;
    private XRTableCell xrTableCell103;
    private XRTableCell xrTableCell104;
    private XRTableCell xrTableCell105;
    private XRTableCell xrTableCell106;
    private XRTableCell xrTableCell107;
    private XRTableCell xrTableCell108;
    private XRTableCell xrTableCell109;
    private XRTableCell xrTableCell110;
    private XRTableCell xrTableCell111;
    private XRTableCell xrTableCell112;
    private XRTableCell xrTableCell113;
    private XRTableCell xrTableCell114;
    private XRTableCell xrTableCell115;
    private XRTableCell xrTableCell116;
    private XRTableCell xrTableCell117;
    private XRTableCell xrTableCell118;
    private XRTableCell xrTableCell119;
    private XRTableCell xrTableCell120;
    private XRTableCell xrTableCell121;
    private XRTableCell xrTableCell122;
    private XRTableCell xrTableCell123;
    private CalculatedField COLUMN4;
    private CalculatedField COLUMN5;
    private CalculatedField COLUMN7;
    private CalculatedField COLUMN9;
    private CalculatedField COLUMN10;
    private CalculatedField COLUMN11;
    private CalculatedField COLUMN12;
    private CalculatedField COLUMN13;
    private CalculatedField COLUMN15;
    private CalculatedField COLUMN16;
    private CalculatedField COLUMN17;
    private CalculatedField COLUMN18;
    private CalculatedField COLUMN19;
    private CalculatedField COLUMN20;
    private CalculatedField COLUMN21;
    private CalculatedField COLUMN2;
    private CalculatedField dStart;
    private CalculatedField dEnd;
    private CalculatedField COLUMN14;
    private XRLabel xrLabel2;
    private XRTableCell xrTableCell101;
    private XRTableCell xrTableCell124;
    private XRTableCell xrTableCell125;
    private XRTableRow xrTableRow9;
    private XRTableCell xrTableCell126;
    private XRTableCell xrTableCell127;
    private XRTableCell xrTableCell128;
    private XRTableCell xrTableCell129;
    private XRTableCell xrTableCell130;
    private XRTableCell xrTableCell131;
    private XRTableCell xrTableCell132;
    private XRTableCell xrTableCell133;
    private XRTableCell xrTableCell134;
    private XRTableCell xrTableCell135;
    private XRTableCell xrTableCell136;
    private XRTableCell xrTableCell137;
    private XRTableCell xrTableCell138;
    private XRTableCell xrTableCell139;
    private XRTableCell xrTableCell140;
    private XRTableCell xrTableCell141;
    private XRTableCell xrTableCell142;
    private XRTableCell xrTableCell143;
    private XRTableCell xrTableCell144;
    private XRTableCell xrTableCell145;
    private XRTableCell xrTableCell146;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public xrtDetailConfirmContact()
	{
		InitializeComponent();
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "xrtDetailConfirmContact.resx";
        DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
        this.dsDetailConfirmContact1 = new dsDetailConfirmContact();
        this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
        this.COLUMN4 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN5 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN7 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN9 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN10 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN11 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN12 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN13 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN15 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN16 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN17 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN18 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN19 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN20 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN21 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN2 = new DevExpress.XtraReports.UI.CalculatedField();
        this.dStart = new DevExpress.XtraReports.UI.CalculatedField();
        this.dEnd = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN14 = new DevExpress.XtraReports.UI.CalculatedField();
        xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailConfirmContact1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // xrTableCell24
        // 
        xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.SCONTRACTNO")});
        xrTableCell24.Name = "xrTableCell24";
        xrTableCell24.StylePriority.UseTextAlignment = false;
        xrTableCell24.Text = "[TReports01.SCONTRACTNO]";
        xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell24.Weight = 0.97999785728776911D;
        // 
        // xrTableCell26
        // 
        xrTableCell26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN2")});
        xrTableCell26.Name = "xrTableCell26";
        xrTableCell26.StylePriority.UseTextAlignment = false;
        xrTableCell26.Text = "xrTableCell26";
        xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        xrTableCell26.Weight = 1.3079168701171875D;
        // 
        // xrTableCell28
        // 
        xrTableCell28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN4", "{0:0.00%}")});
        xrTableCell28.Multiline = true;
        xrTableCell28.Name = "xrTableCell28";
        xrTableCell28.Text = "xrTableCell28";
        xrTableCell28.Weight = 0.92416687011718746D;
        // 
        // Detail
        // 
        this.Detail.BackColor = System.Drawing.Color.Lime;
        this.Detail.HeightF = 0F;
        this.Detail.MultiColumn.ColumnCount = 2;
        this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
        this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.StylePriority.UseBackColor = false;
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 19F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 114F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // formattingRule1
        // 
        this.formattingRule1.Name = "formattingRule1";
        // 
        // dsDetailConfirmContact1
        // 
        this.dsDetailConfirmContact1.DataSetName = "dsDetailConfirmContact";
        this.dsDetailConfirmContact1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // DetailReport
        // 
        this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupFooter1,
            this.GroupHeader1,
            this.ReportFooter});
        this.DetailReport.Level = 0;
        this.DetailReport.Name = "DetailReport";
        // 
        // Detail1
        // 
        this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.Detail1.HeightF = 30F;
        this.Detail1.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
        this.Detail1.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
        this.Detail1.Name = "Detail1";
        // 
        // xrTable2
        // 
        this.xrTable2.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(3490F, 30F);
        this.xrTable2.StylePriority.UseFont = false;
        this.xrTable2.StylePriority.UseTextAlignment = false;
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell96,
            this.xrTableCell124,
            this.xrTableCell94,
            this.xrTableCell125,
            this.xrTableCell23,
            this.xrTableCell47,
            xrTableCell24,
            this.xrTableCell25,
            xrTableCell26,
            this.xrTableCell27,
            xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell46});
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.StylePriority.UseBorders = false;
        this.xrTableRow2.Weight = 0.99653962401674279D;
        // 
        // xrTableCell101
        // 
        this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableCell101.Name = "xrTableCell101";
        this.xrTableCell101.StylePriority.UseBorders = false;
        this.xrTableCell101.Weight = 0.02322843243127401D;
        // 
        // xrTableCell96
        // 
        this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell96.CanShrink = true;
        this.xrTableCell96.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.dStart")});
        this.xrTableCell96.Name = "xrTableCell96";
        this.xrTableCell96.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell96.StylePriority.UseBorders = false;
        this.xrTableCell96.Text = "xrTableCell96";
        this.xrTableCell96.Weight = 0.68406173223753419D;
        // 
        // xrTableCell124
        // 
        this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell124.Name = "xrTableCell124";
        this.xrTableCell124.StylePriority.UseBorders = false;
        this.xrTableCell124.Weight = 0.020000000524657502D;
        // 
        // xrTableCell94
        // 
        this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell94.CanShrink = true;
        this.xrTableCell94.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dEnd")});
        this.xrTableCell94.Name = "xrTableCell94";
        this.xrTableCell94.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell94.StylePriority.UseBorders = false;
        this.xrTableCell94.Text = "xrTableCell94";
        this.xrTableCell94.Weight = 0.707290356234322D;
        // 
        // xrTableCell125
        // 
        this.xrTableCell125.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell125.Name = "xrTableCell125";
        this.xrTableCell125.StylePriority.UseBorders = false;
        this.xrTableCell125.Weight = 0.020000000524657502D;
        // 
        // xrTableCell23
        // 
        this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell23.CanShrink = true;
        this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.SVENDORNAME")});
        this.xrTableCell23.Multiline = true;
        this.xrTableCell23.Name = "xrTableCell23";
        this.xrTableCell23.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.Suppress;
        this.xrTableCell23.StylePriority.UseBorders = false;
        this.xrTableCell23.StylePriority.UseTextAlignment = false;
        xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell23.Summary = xrSummary1;
        this.xrTableCell23.Text = "[TReports01.SVENDORNAME]";
        this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell23.Weight = 1.0312516983601048D;
        // 
        // xrTableCell47
        // 
        this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell47.Name = "xrTableCell47";
        this.xrTableCell47.StylePriority.UseBorders = false;
        this.xrTableCell47.Weight = 0.020000000524657481D;
        // 
        // xrTableCell25
        // 
        this.xrTableCell25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN1")});
        this.xrTableCell25.Multiline = true;
        this.xrTableCell25.Name = "xrTableCell25";
        this.xrTableCell25.StylePriority.UseTextAlignment = false;
        this.xrTableCell25.Text = "[COLUMN1]";
        this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell25.Weight = 1.3275009155273676D;
        // 
        // xrTableCell27
        // 
        this.xrTableCell27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN3")});
        this.xrTableCell27.Name = "xrTableCell27";
        this.xrTableCell27.Text = "xrTableCell27";
        this.xrTableCell27.Weight = 1.0866662597656249D;
        // 
        // xrTableCell29
        // 
        this.xrTableCell29.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.Column5")});
        this.xrTableCell29.Multiline = true;
        this.xrTableCell29.Name = "xrTableCell29";
        this.xrTableCell29.StylePriority.UseTextAlignment = false;
        this.xrTableCell29.Text = "xrTableCell29";
        this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell29.Weight = 0.86312499999999992D;
        // 
        // xrTableCell30
        // 
        this.xrTableCell30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN6")});
        this.xrTableCell30.Multiline = true;
        this.xrTableCell30.Name = "xrTableCell30";
        this.xrTableCell30.Text = "xrTableCell30";
        this.xrTableCell30.Weight = 1.3742712402343753D;
        // 
        // xrTableCell31
        // 
        this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN7", "{0:0.00%}")});
        this.xrTableCell31.Multiline = true;
        this.xrTableCell31.Name = "xrTableCell31";
        this.xrTableCell31.Text = "xrTableCell31";
        this.xrTableCell31.Weight = 1.1871356201171874D;
        // 
        // xrTableCell32
        // 
        this.xrTableCell32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.Column8")});
        this.xrTableCell32.Name = "xrTableCell32";
        this.xrTableCell32.StylePriority.UseTextAlignment = false;
        this.xrTableCell32.Text = "xrTableCell32";
        this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell32.Weight = 0.99999998593193218D;
        // 
        // xrTableCell33
        // 
        this.xrTableCell33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN9", "{0:0.00%}")});
        this.xrTableCell33.Name = "xrTableCell33";
        this.xrTableCell33.Text = "[COLUMN9]";
        this.xrTableCell33.Weight = 1.8773309466852559D;
        // 
        // xrTableCell34
        // 
        this.xrTableCell34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN10", "{0:0.00%}")});
        this.xrTableCell34.Name = "xrTableCell34";
        this.xrTableCell34.Text = "[COLUMN10]";
        this.xrTableCell34.Weight = 1.624012451171875D;
        // 
        // xrTableCell35
        // 
        this.xrTableCell35.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN11", "{0:0.00%}")});
        this.xrTableCell35.Multiline = true;
        this.xrTableCell35.Name = "xrTableCell35";
        this.xrTableCell35.Text = "[COLUMN11]";
        this.xrTableCell35.Weight = 1.4319311523437508D;
        // 
        // xrTableCell36
        // 
        this.xrTableCell36.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN12")});
        this.xrTableCell36.Multiline = true;
        this.xrTableCell36.Name = "xrTableCell36";
        this.xrTableCell36.Text = "[COLUMN12]";
        this.xrTableCell36.Weight = 1.1165136718750006D;
        // 
        // xrTableCell37
        // 
        this.xrTableCell37.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN13", "{0:0.00%}")});
        this.xrTableCell37.Multiline = true;
        this.xrTableCell37.Name = "xrTableCell37";
        this.xrTableCell37.Text = "[COLUMN13]";
        this.xrTableCell37.Weight = 1.3030480957031252D;
        // 
        // xrTableCell38
        // 
        this.xrTableCell38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN14")});
        this.xrTableCell38.Multiline = true;
        this.xrTableCell38.Name = "xrTableCell38";
        this.xrTableCell38.StylePriority.UseTextAlignment = false;
        this.xrTableCell38.Text = "[COLUMN14]";
        this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell38.Weight = 3.1150634765625D;
        // 
        // xrTableCell39
        // 
        this.xrTableCell39.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN15", "{0:0.00%}")});
        this.xrTableCell39.Name = "xrTableCell39";
        this.xrTableCell39.StylePriority.UseTextAlignment = false;
        this.xrTableCell39.Text = "COLUMN15";
        this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell39.Weight = 1.5783642673764846D;
        // 
        // xrTableCell40
        // 
        this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN16")});
        this.xrTableCell40.Multiline = true;
        this.xrTableCell40.Name = "xrTableCell40";
        this.xrTableCell40.StylePriority.UseTextAlignment = false;
        this.xrTableCell40.Text = "xrTableCell40";
        this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell40.Weight = 2.4554467620415177D;
        // 
        // xrTableCell41
        // 
        this.xrTableCell41.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN17", "{0:0.00%}")});
        this.xrTableCell41.Multiline = true;
        this.xrTableCell41.Name = "xrTableCell41";
        this.xrTableCell41.StylePriority.UseTextAlignment = false;
        this.xrTableCell41.Text = "xrTableCell41";
        this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell41.Weight = 1.4991650448007479D;
        // 
        // xrTableCell42
        // 
        this.xrTableCell42.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN18", "{0:#,0.00}")});
        this.xrTableCell42.Name = "xrTableCell42";
        this.xrTableCell42.StylePriority.UseTextAlignment = false;
        this.xrTableCell42.Text = "xrTableCell42";
        this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell42.Weight = 1.5358374023437511D;
        // 
        // xrTableCell43
        // 
        this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN19", "{0:0.00%}")});
        this.xrTableCell43.Name = "xrTableCell43";
        this.xrTableCell43.StylePriority.UseTextAlignment = false;
        this.xrTableCell43.Text = "xrTableCell43";
        this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell43.Weight = 1.7783349609375008D;
        // 
        // xrTableCell44
        // 
        this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN20", "{0:0.00}")});
        this.xrTableCell44.Name = "xrTableCell44";
        this.xrTableCell44.StylePriority.UseBorders = false;
        this.xrTableCell44.StylePriority.UseTextAlignment = false;
        this.xrTableCell44.Text = "xrTableCell44";
        this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell44.Weight = 1.3683349609374993D;
        // 
        // xrTableCell46
        // 
        this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN21", "{0:0.00%}")});
        this.xrTableCell46.Name = "xrTableCell46";
        this.xrTableCell46.Text = "xrTableCell46";
        this.xrTableCell46.Weight = 1.6600048828124994D;
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrPageBreak1});
        this.GroupFooter1.HeightF = 30.00001F;
        this.GroupFooter1.Name = "GroupFooter1";
        this.GroupFooter1.RepeatEveryPage = true;
        // 
        // xrTable3
        // 
        this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable3.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
        this.xrTable3.SizeF = new System.Drawing.SizeF(3490F, 30F);
        this.xrTable3.StylePriority.UseBorders = false;
        this.xrTable3.StylePriority.UseFont = false;
        this.xrTable3.StylePriority.UseTextAlignment = false;
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell70,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92});
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.StylePriority.UseBorders = false;
        this.xrTableRow4.Weight = 0.99653962401674279D;
        // 
        // xrTableCell99
        // 
        this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell99.Name = "xrTableCell99";
        this.xrTableCell99.StylePriority.UseBorders = false;
        this.xrTableCell99.Weight = 0.72729003906250034D;
        // 
        // xrTableCell97
        // 
        this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell97.Name = "xrTableCell97";
        this.xrTableCell97.StylePriority.UseBorders = false;
        this.xrTableCell97.Weight = 0.72729034423828143D;
        // 
        // xrTableCell98
        // 
        this.xrTableCell98.Name = "xrTableCell98";
        this.xrTableCell98.Weight = 1.0512510681152343D;
        // 
        // xrTableCell70
        // 
        this.xrTableCell70.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell70.Name = "xrTableCell70";
        this.xrTableCell70.StylePriority.UseBackColor = false;
        this.xrTableCell70.StylePriority.UseTextAlignment = false;
        xrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell70.Summary = xrSummary2;
        this.xrTableCell70.Text = "ทั้งหมด";
        this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell70.Weight = 0.9799986267089843D;
        // 
        // xrTableCell72
        // 
        this.xrTableCell72.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN1")});
        this.xrTableCell72.Multiline = true;
        this.xrTableCell72.Name = "xrTableCell72";
        this.xrTableCell72.StylePriority.UseBackColor = false;
        this.xrTableCell72.StylePriority.UseTextAlignment = false;
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell72.Summary = xrSummary3;
        this.xrTableCell72.Text = "xrTableCell72";
        this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell72.Weight = 1.3274999999999995D;
        // 
        // xrTableCell73
        // 
        this.xrTableCell73.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN2")});
        this.xrTableCell73.Name = "xrTableCell73";
        this.xrTableCell73.StylePriority.UseBackColor = false;
        this.xrTableCell73.StylePriority.UseTextAlignment = false;
        xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell73.Summary = xrSummary4;
        this.xrTableCell73.Text = "xrTableCell73";
        this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell73.Weight = 1.3079168701171875D;
        // 
        // xrTableCell74
        // 
        this.xrTableCell74.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell74.Name = "xrTableCell74";
        this.xrTableCell74.StylePriority.UseBackColor = false;
        this.xrTableCell74.Weight = 1.0866662597656249D;
        // 
        // xrTableCell75
        // 
        this.xrTableCell75.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell75.Multiline = true;
        this.xrTableCell75.Name = "xrTableCell75";
        this.xrTableCell75.StylePriority.UseBackColor = false;
        this.xrTableCell75.Weight = 0.92416687011718746D;
        // 
        // xrTableCell76
        // 
        this.xrTableCell76.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell76.Multiline = true;
        this.xrTableCell76.Name = "xrTableCell76";
        this.xrTableCell76.StylePriority.UseBackColor = false;
        this.xrTableCell76.Weight = 0.86312499999999992D;
        // 
        // xrTableCell77
        // 
        this.xrTableCell77.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell77.Multiline = true;
        this.xrTableCell77.Name = "xrTableCell77";
        this.xrTableCell77.StylePriority.UseBackColor = false;
        this.xrTableCell77.Weight = 1.3742712402343753D;
        // 
        // xrTableCell78
        // 
        this.xrTableCell78.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell78.Multiline = true;
        this.xrTableCell78.Name = "xrTableCell78";
        this.xrTableCell78.StylePriority.UseBackColor = false;
        this.xrTableCell78.Weight = 1.1871356201171874D;
        // 
        // xrTableCell79
        // 
        this.xrTableCell79.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell79.Name = "xrTableCell79";
        this.xrTableCell79.StylePriority.UseBackColor = false;
        this.xrTableCell79.Weight = 1D;
        // 
        // xrTableCell80
        // 
        this.xrTableCell80.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell80.Name = "xrTableCell80";
        this.xrTableCell80.StylePriority.UseBackColor = false;
        this.xrTableCell80.Weight = 1.8773309326171881D;
        // 
        // xrTableCell81
        // 
        this.xrTableCell81.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell81.Name = "xrTableCell81";
        this.xrTableCell81.StylePriority.UseBackColor = false;
        this.xrTableCell81.Weight = 1.624012451171875D;
        // 
        // xrTableCell82
        // 
        this.xrTableCell82.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell82.Multiline = true;
        this.xrTableCell82.Name = "xrTableCell82";
        this.xrTableCell82.StylePriority.UseBackColor = false;
        this.xrTableCell82.Weight = 1.4319311523437508D;
        // 
        // xrTableCell83
        // 
        this.xrTableCell83.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell83.Multiline = true;
        this.xrTableCell83.Name = "xrTableCell83";
        this.xrTableCell83.StylePriority.UseBackColor = false;
        this.xrTableCell83.Weight = 1.1165136718750006D;
        // 
        // xrTableCell84
        // 
        this.xrTableCell84.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell84.Multiline = true;
        this.xrTableCell84.Name = "xrTableCell84";
        this.xrTableCell84.StylePriority.UseBackColor = false;
        this.xrTableCell84.Weight = 1.3030480957031252D;
        // 
        // xrTableCell85
        // 
        this.xrTableCell85.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell85.Multiline = true;
        this.xrTableCell85.Name = "xrTableCell85";
        this.xrTableCell85.StylePriority.UseBackColor = false;
        this.xrTableCell85.StylePriority.UseTextAlignment = false;
        this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell85.Weight = 3.1150634765625D;
        // 
        // xrTableCell86
        // 
        this.xrTableCell86.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell86.Name = "xrTableCell86";
        this.xrTableCell86.StylePriority.UseBackColor = false;
        this.xrTableCell86.StylePriority.UseTextAlignment = false;
        this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell86.Weight = 1.5783691406250009D;
        // 
        // xrTableCell87
        // 
        this.xrTableCell87.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell87.Multiline = true;
        this.xrTableCell87.Name = "xrTableCell87";
        this.xrTableCell87.StylePriority.UseBackColor = false;
        this.xrTableCell87.StylePriority.UseTextAlignment = false;
        this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell87.Weight = 2.4554443359375004D;
        // 
        // xrTableCell88
        // 
        this.xrTableCell88.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell88.Multiline = true;
        this.xrTableCell88.Name = "xrTableCell88";
        this.xrTableCell88.StylePriority.UseBackColor = false;
        this.xrTableCell88.StylePriority.UseTextAlignment = false;
        this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell88.Weight = 1.4991625976562486D;
        // 
        // xrTableCell89
        // 
        this.xrTableCell89.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell89.Name = "xrTableCell89";
        this.xrTableCell89.StylePriority.UseBackColor = false;
        this.xrTableCell89.StylePriority.UseTextAlignment = false;
        this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell89.Weight = 1.5358374023437511D;
        // 
        // xrTableCell90
        // 
        this.xrTableCell90.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell90.Name = "xrTableCell90";
        this.xrTableCell90.StylePriority.UseBackColor = false;
        this.xrTableCell90.StylePriority.UseTextAlignment = false;
        this.xrTableCell90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell90.Weight = 1.7783349609375008D;
        // 
        // xrTableCell91
        // 
        this.xrTableCell91.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell91.Name = "xrTableCell91";
        this.xrTableCell91.StylePriority.UseBackColor = false;
        this.xrTableCell91.StylePriority.UseBorders = false;
        this.xrTableCell91.StylePriority.UseTextAlignment = false;
        this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell91.Weight = 1.3683349609374993D;
        // 
        // xrTableCell92
        // 
        this.xrTableCell92.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell92.Name = "xrTableCell92";
        this.xrTableCell92.StylePriority.UseBackColor = false;
        this.xrTableCell92.Weight = 1.6600048828124994D;
        // 
        // xrPageBreak1
        // 
        this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 28.00001F);
        this.xrPageBreak1.Name = "xrPageBreak1";
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1});
        this.GroupHeader1.Expanded = false;
        this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("SVENDORNAME", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader1.HeightF = 80.20834F;
        this.GroupHeader1.Name = "GroupHeader1";
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.SVENDORNAME")});
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(1750F, 25F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel2.Text = "xrLabel1";
        this.xrLabel2.Visible = false;
        // 
        // xrLabel1
        // 
        this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.SVENDORNAME")});
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(148.583F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel1.Text = "xrLabel1";
        this.xrLabel1.Visible = false;
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
        this.ReportFooter.HeightF = 30F;
        this.ReportFooter.KeepTogether = true;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // xrTable6
        // 
        this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable6.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable6.Name = "xrTable6";
        this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
        this.xrTable6.SizeF = new System.Drawing.SizeF(3490F, 30F);
        this.xrTable6.StylePriority.UseBorders = false;
        this.xrTable6.StylePriority.UseFont = false;
        this.xrTable6.StylePriority.UseTextAlignment = false;
        this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow8
        // 
        this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell100,
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123});
        this.xrTableRow8.Name = "xrTableRow8";
        this.xrTableRow8.StylePriority.UseBorders = false;
        this.xrTableRow8.Weight = 0.99653962401674279D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.StylePriority.UseBorders = false;
        this.xrTableCell2.Weight = 0.72729003906250034D;
        // 
        // xrTableCell100
        // 
        this.xrTableCell100.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell100.Name = "xrTableCell100";
        this.xrTableCell100.StylePriority.UseBorders = false;
        this.xrTableCell100.Weight = 0.72729034423828143D;
        // 
        // xrTableCell102
        // 
        this.xrTableCell102.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell102.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell102.Name = "xrTableCell102";
        this.xrTableCell102.StylePriority.UseBackColor = false;
        this.xrTableCell102.StylePriority.UseBorders = false;
        this.xrTableCell102.StylePriority.UseTextAlignment = false;
        xrSummary5.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell102.Summary = xrSummary5;
        this.xrTableCell102.Text = "ทั้งหมด";
        this.xrTableCell102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell102.Weight = 2.0312496948242185D;
        // 
        // xrTableCell103
        // 
        this.xrTableCell103.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell103.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN1")});
        this.xrTableCell103.Multiline = true;
        this.xrTableCell103.Name = "xrTableCell103";
        this.xrTableCell103.StylePriority.UseBackColor = false;
        this.xrTableCell103.StylePriority.UseBorders = false;
        this.xrTableCell103.StylePriority.UseTextAlignment = false;
        xrSummary6.IgnoreNullValues = true;
        xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell103.Summary = xrSummary6;
        this.xrTableCell103.Text = "xrTableCell103";
        this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell103.Weight = 1.3274999999999995D;
        // 
        // xrTableCell104
        // 
        this.xrTableCell104.BackColor = System.Drawing.Color.BurlyWood;
        this.xrTableCell104.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell104.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TReports01.COLUMN2")});
        this.xrTableCell104.Name = "xrTableCell104";
        this.xrTableCell104.StylePriority.UseBackColor = false;
        this.xrTableCell104.StylePriority.UseBorders = false;
        this.xrTableCell104.StylePriority.UseTextAlignment = false;
        xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell104.Summary = xrSummary7;
        this.xrTableCell104.Text = "xrTableCell104";
        this.xrTableCell104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell104.Weight = 1.3079168701171875D;
        // 
        // xrTableCell105
        // 
        this.xrTableCell105.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell105.Name = "xrTableCell105";
        this.xrTableCell105.StylePriority.UseBackColor = false;
        this.xrTableCell105.StylePriority.UseBorders = false;
        this.xrTableCell105.Weight = 1.0866662597656249D;
        // 
        // xrTableCell106
        // 
        this.xrTableCell106.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell106.Multiline = true;
        this.xrTableCell106.Name = "xrTableCell106";
        this.xrTableCell106.StylePriority.UseBackColor = false;
        this.xrTableCell106.Weight = 0.92416687011718746D;
        // 
        // xrTableCell107
        // 
        this.xrTableCell107.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell107.Multiline = true;
        this.xrTableCell107.Name = "xrTableCell107";
        this.xrTableCell107.StylePriority.UseBackColor = false;
        this.xrTableCell107.Weight = 0.86312499999999992D;
        // 
        // xrTableCell108
        // 
        this.xrTableCell108.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell108.Multiline = true;
        this.xrTableCell108.Name = "xrTableCell108";
        this.xrTableCell108.StylePriority.UseBackColor = false;
        this.xrTableCell108.Weight = 1.3742712402343753D;
        // 
        // xrTableCell109
        // 
        this.xrTableCell109.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell109.Multiline = true;
        this.xrTableCell109.Name = "xrTableCell109";
        this.xrTableCell109.StylePriority.UseBackColor = false;
        this.xrTableCell109.Weight = 1.1871356201171874D;
        // 
        // xrTableCell110
        // 
        this.xrTableCell110.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell110.Name = "xrTableCell110";
        this.xrTableCell110.StylePriority.UseBackColor = false;
        this.xrTableCell110.Weight = 1D;
        // 
        // xrTableCell111
        // 
        this.xrTableCell111.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell111.Name = "xrTableCell111";
        this.xrTableCell111.StylePriority.UseBackColor = false;
        this.xrTableCell111.Weight = 1.8773309326171881D;
        // 
        // xrTableCell112
        // 
        this.xrTableCell112.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell112.Name = "xrTableCell112";
        this.xrTableCell112.StylePriority.UseBackColor = false;
        this.xrTableCell112.Weight = 1.624012451171875D;
        // 
        // xrTableCell113
        // 
        this.xrTableCell113.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell113.Multiline = true;
        this.xrTableCell113.Name = "xrTableCell113";
        this.xrTableCell113.StylePriority.UseBackColor = false;
        this.xrTableCell113.Weight = 1.4319311523437508D;
        // 
        // xrTableCell114
        // 
        this.xrTableCell114.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell114.Multiline = true;
        this.xrTableCell114.Name = "xrTableCell114";
        this.xrTableCell114.StylePriority.UseBackColor = false;
        this.xrTableCell114.Weight = 1.1165136718750006D;
        // 
        // xrTableCell115
        // 
        this.xrTableCell115.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell115.Multiline = true;
        this.xrTableCell115.Name = "xrTableCell115";
        this.xrTableCell115.StylePriority.UseBackColor = false;
        this.xrTableCell115.Weight = 1.3030480957031252D;
        // 
        // xrTableCell116
        // 
        this.xrTableCell116.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell116.Multiline = true;
        this.xrTableCell116.Name = "xrTableCell116";
        this.xrTableCell116.StylePriority.UseBackColor = false;
        this.xrTableCell116.StylePriority.UseTextAlignment = false;
        this.xrTableCell116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell116.Weight = 3.1150634765625D;
        // 
        // xrTableCell117
        // 
        this.xrTableCell117.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell117.Name = "xrTableCell117";
        this.xrTableCell117.StylePriority.UseBackColor = false;
        this.xrTableCell117.StylePriority.UseTextAlignment = false;
        this.xrTableCell117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell117.Weight = 1.5783691406250009D;
        // 
        // xrTableCell118
        // 
        this.xrTableCell118.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell118.Multiline = true;
        this.xrTableCell118.Name = "xrTableCell118";
        this.xrTableCell118.StylePriority.UseBackColor = false;
        this.xrTableCell118.StylePriority.UseTextAlignment = false;
        this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell118.Weight = 2.4554418945312508D;
        // 
        // xrTableCell119
        // 
        this.xrTableCell119.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell119.Multiline = true;
        this.xrTableCell119.Name = "xrTableCell119";
        this.xrTableCell119.StylePriority.UseBackColor = false;
        this.xrTableCell119.StylePriority.UseTextAlignment = false;
        this.xrTableCell119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell119.Weight = 1.4991650390624987D;
        // 
        // xrTableCell120
        // 
        this.xrTableCell120.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell120.Name = "xrTableCell120";
        this.xrTableCell120.StylePriority.UseBackColor = false;
        this.xrTableCell120.StylePriority.UseTextAlignment = false;
        this.xrTableCell120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell120.Weight = 1.5358374023437511D;
        // 
        // xrTableCell121
        // 
        this.xrTableCell121.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell121.Name = "xrTableCell121";
        this.xrTableCell121.StylePriority.UseBackColor = false;
        this.xrTableCell121.StylePriority.UseTextAlignment = false;
        this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell121.Weight = 1.7783349609375008D;
        // 
        // xrTableCell122
        // 
        this.xrTableCell122.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell122.Name = "xrTableCell122";
        this.xrTableCell122.StylePriority.UseBackColor = false;
        this.xrTableCell122.StylePriority.UseBorders = false;
        this.xrTableCell122.StylePriority.UseTextAlignment = false;
        this.xrTableCell122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell122.Weight = 1.3683349609374993D;
        // 
        // xrTableCell123
        // 
        this.xrTableCell123.BackColor = System.Drawing.Color.Gray;
        this.xrTableCell123.Name = "xrTableCell123";
        this.xrTableCell123.StylePriority.UseBackColor = false;
        this.xrTableCell123.Weight = 1.6600048828124994D;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Expanded = false;
        this.ReportHeader.HeightF = 106F;
        this.ReportHeader.KeepTogether = true;
        this.ReportHeader.Name = "ReportHeader";
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable1,
            this.xrTable5});
        this.PageHeader.HeightF = 270.0236F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrTable4
        // 
        this.xrTable4.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(145.4581F, 0F);
        this.xrTable4.Name = "xrTable4";
        this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
        this.xrTable4.SizeF = new System.Drawing.SizeF(203.12F, 270.0235F);
        this.xrTable4.StylePriority.UseBackColor = false;
        this.xrTable4.StylePriority.UseBorders = false;
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell48});
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.Weight = 1.3714285714285712D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell1.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTableCell1.ForeColor = System.Drawing.Color.White;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.StylePriority.UseBorders = false;
        this.xrTableCell1.StylePriority.UseFont = false;
        this.xrTableCell1.StylePriority.UseForeColor = false;
        this.xrTableCell1.StylePriority.UseTextAlignment = false;
        this.xrTableCell1.Text = "ผู้ขนส่ง";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell1.Weight = 1.5526181344077725D;
        // 
        // xrTableCell48
        // 
        this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell48.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTableCell48.ForeColor = System.Drawing.Color.White;
        this.xrTableCell48.Name = "xrTableCell48";
        this.xrTableCell48.StylePriority.UseBorders = false;
        this.xrTableCell48.StylePriority.UseFont = false;
        this.xrTableCell48.StylePriority.UseForeColor = false;
        this.xrTableCell48.StylePriority.UseTextAlignment = false;
        this.xrTableCell48.Text = "สัญญา";
        this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell48.Weight = 1.4473818655922275D;
        // 
        // xrTable1
        // 
        this.xrTable1.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable1.ForeColor = System.Drawing.Color.White;
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(348.583F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow9});
        this.xrTable1.SizeF = new System.Drawing.SizeF(3141.42F, 270.0236F);
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.StylePriority.UseForeColor = false;
        this.xrTable1.StylePriority.UseTextAlignment = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableRow1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
        this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell3,
            this.xrTableCell9,
            this.xrTableCell7,
            this.xrTableCell11,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell8,
            this.xrTableCell10,
            this.xrTableCell12,
            this.xrTableCell14,
            this.xrTableCell13,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22});
        this.xrTableRow1.ForeColor = System.Drawing.Color.White;
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.StylePriority.UseBackColor = false;
        this.xrTableRow1.StylePriority.UseBorderColor = false;
        this.xrTableRow1.StylePriority.UseBorderDashStyle = false;
        this.xrTableRow1.StylePriority.UseBorders = false;
        this.xrTableRow1.StylePriority.UseForeColor = false;
        this.xrTableRow1.Weight = 1.5360002625000448D;
        // 
        // xrTableCell45
        // 
        this.xrTableCell45.CanGrow = false;
        this.xrTableCell45.Multiline = true;
        this.xrTableCell45.Name = "xrTableCell45";
        this.xrTableCell45.Text = "(1)\r\n จำนวนรถใน\r\nสัญญา";
        this.xrTableCell45.Weight = 1.3274996731257978D;
        this.xrTableCell45.WordWrap = false;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.Multiline = true;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.StylePriority.UseTextAlignment = false;
        this.xrTableCell3.Text = "(2) \r\nจำนวนรถที่ต้อง\r\nยืนยันครบตามสัญญา";
        this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell3.Weight = 1.3079165649414064D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.Multiline = true;
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.StylePriority.UseTextAlignment = false;
        this.xrTableCell9.Text = "(3) \r\nส่งข้อมูล\r\nเข้าระบบ";
        this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell9.Weight = 1.0866665649414062D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.Multiline = true;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.Text = "(4)\r\n% ส่งข้อมูลเข้าระบบ / จำนวนวันที่ออกรายงาน";
        this.xrTableCell7.Weight = 0.92416687011718757D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Multiline = true;
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.Text = "(5)\r\n รายการวันที่\r\nไม่ส่งข้อมูลเข้าระบบ";
        this.xrTableCell11.Weight = 0.86312499999999992D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.Multiline = true;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Text = "(6) \r\nยืนยันรถ\r\nครบตามสัญญา";
        this.xrTableCell4.Weight = 1.3742718505859375D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Multiline = true;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Text = "(7)\r\n% ยืนยันรถครบตามสัญญา / ส่งข้อมูลเข้าระบบ";
        this.xrTableCell5.Weight = 1.1871350097656253D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.Multiline = true;
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.Text = "(8)\r\nรายการวันที่ยืนยันรถไม่ครบตามสัญญา";
        this.xrTableCell6.Weight = 0.999999894669874D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.Multiline = true;
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Text = "(9)\r\n% ยืนยันรถครบตามสัญญา / จำนวนวันที่ออกรายงาน";
        this.xrTableCell8.Weight = 1.8773310379473129D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.Multiline = true;
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.Text = "(10)\r\n% ไม่ส่งข้อมูลเข้าระบบ / จำนวนวันที่ออกรายงาน";
        this.xrTableCell10.Weight = 1.6240124511718754D;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.Multiline = true;
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.Text = "(11)\r\n% ยืนยันรถไม่ครบตามสัญญา / จำนวนวันที่ออกรายงาน";
        this.xrTableCell12.Weight = 1.4319311523437501D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.Multiline = true;
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Text = "(12)\r\nจำนวนรถที่ขาดเนื่องจาก\r\nไม่ส่งข้อมูลเข้าระบบ";
        this.xrTableCell14.Weight = 1.1165136718750008D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.Multiline = true;
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.Text = "(13)\r\n% จำนวนรถที่ขาดเนื่องจาก\r\nไม่ส่งข้อมูลเข้าระบบ / จำนวนรถที่ต้องยืนยันครบตาม" +
            "สัญญา";
        this.xrTableCell13.Weight = 1.3030480957031254D;
        // 
        // xrTableCell15
        // 
        this.xrTableCell15.Multiline = true;
        this.xrTableCell15.Name = "xrTableCell15";
        this.xrTableCell15.Text = "(14) \r\nจำนวนรถที่ขาด\r\nเนื่องจากการปฏิเสธ";
        this.xrTableCell15.Weight = 3.1150659179687503D;
        // 
        // xrTableCell16
        // 
        this.xrTableCell16.Multiline = true;
        this.xrTableCell16.Name = "xrTableCell16";
        this.xrTableCell16.StylePriority.UseTextAlignment = false;
        this.xrTableCell16.Text = "(15)\r\n% จำนวนรถที่ขาด\r\nเนื่องจากการปฏิเสธ / จำนวนรถที่ต้องยืนยันครบตามสัญญา";
        this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell16.Weight = 1.5783645308605019D;
        // 
        // xrTableCell17
        // 
        this.xrTableCell17.Multiline = true;
        this.xrTableCell17.Name = "xrTableCell17";
        this.xrTableCell17.StylePriority.UseTextAlignment = false;
        this.xrTableCell17.Text = "(16) \r\nจำนวนรถที่ยืนยันไม่ครบตามสัญญา";
        this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell17.Weight = 2.4554417933476045D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.Multiline = true;
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.StylePriority.UseTextAlignment = false;
        this.xrTableCell18.Text = "(17)\r\n% จำนวนรถที่ยืนยัน\r\nไม่ครบตามสัญญา / จำนวนรถที่ต้องยืนยันครบตามสัญญา";
        this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell18.Weight = 1.4991673086043944D;
        // 
        // xrTableCell19
        // 
        this.xrTableCell19.Multiline = true;
        this.xrTableCell19.Name = "xrTableCell19";
        this.xrTableCell19.StylePriority.UseTextAlignment = false;
        this.xrTableCell19.Text = "(18) \r\nค่าเฉลี่ยของจำนวนรถที่\r\nยืนยันทั้งหมด\r\nจำนวนวันที่จัดทำรายงาน";
        this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell19.Weight = 1.5358374023437487D;
        // 
        // xrTableCell20
        // 
        this.xrTableCell20.Multiline = true;
        this.xrTableCell20.Name = "xrTableCell20";
        this.xrTableCell20.StylePriority.UseTextAlignment = false;
        this.xrTableCell20.Text = "(19)\r\n% ค่าเฉลี่ยของจำนวนรถที่ยืนยัน / จำนวนรถในสัญญา";
        this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell20.Weight = 1.7783325195312512D;
        // 
        // xrTableCell21
        // 
        this.xrTableCell21.Multiline = true;
        this.xrTableCell21.Name = "xrTableCell21";
        this.xrTableCell21.StylePriority.UseTextAlignment = false;
        this.xrTableCell21.Text = "(20) \r\nค่าเฉลี่ยของรถที่ยืนยัน / จำนวนวันที่ส่งข้อมูลเข้าระบบ";
        this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell21.Weight = 1.3683374023437507D;
        // 
        // xrTableCell22
        // 
        this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell22.Multiline = true;
        this.xrTableCell22.Name = "xrTableCell22";
        this.xrTableCell22.StylePriority.UseBorders = false;
        this.xrTableCell22.StylePriority.UseTextAlignment = false;
        this.xrTableCell22.Text = "(21) \r\n% ค่าเฉลี่ยของรถที่ยืนยัน / จำนวนรถในสัญญา";
        this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell22.Weight = 1.6600024414062482D;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69});
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.StylePriority.UseBackColor = false;
        this.xrTableRow3.StylePriority.UseBorders = false;
        this.xrTableRow3.Weight = 0.41323274716134295D;
        // 
        // xrTableCell49
        // 
        this.xrTableCell49.Name = "xrTableCell49";
        this.xrTableCell49.Text = "คัน";
        this.xrTableCell49.Weight = 1.3274996731257978D;
        // 
        // xrTableCell50
        // 
        this.xrTableCell50.Name = "xrTableCell50";
        this.xrTableCell50.Text = "คัน";
        this.xrTableCell50.Weight = 1.3079165649414064D;
        // 
        // xrTableCell51
        // 
        this.xrTableCell51.Name = "xrTableCell51";
        this.xrTableCell51.Text = "วัน";
        this.xrTableCell51.Weight = 1.0866665649414062D;
        // 
        // xrTableCell52
        // 
        this.xrTableCell52.Name = "xrTableCell52";
        this.xrTableCell52.Text = "%";
        this.xrTableCell52.Weight = 0.92416687011718757D;
        // 
        // xrTableCell53
        // 
        this.xrTableCell53.Name = "xrTableCell53";
        this.xrTableCell53.Text = "-";
        this.xrTableCell53.Weight = 0.86312499999999992D;
        // 
        // xrTableCell54
        // 
        this.xrTableCell54.Name = "xrTableCell54";
        this.xrTableCell54.Text = "วัน";
        this.xrTableCell54.Weight = 1.3742718505859375D;
        // 
        // xrTableCell55
        // 
        this.xrTableCell55.Name = "xrTableCell55";
        this.xrTableCell55.Text = "%";
        this.xrTableCell55.Weight = 1.1871350097656253D;
        // 
        // xrTableCell56
        // 
        this.xrTableCell56.Name = "xrTableCell56";
        this.xrTableCell56.Text = "-";
        this.xrTableCell56.Weight = 0.999999894669874D;
        // 
        // xrTableCell57
        // 
        this.xrTableCell57.Name = "xrTableCell57";
        this.xrTableCell57.Text = "%";
        this.xrTableCell57.Weight = 1.8773310379473129D;
        // 
        // xrTableCell58
        // 
        this.xrTableCell58.Name = "xrTableCell58";
        this.xrTableCell58.Text = "%";
        this.xrTableCell58.Weight = 1.6240124511718754D;
        // 
        // xrTableCell59
        // 
        this.xrTableCell59.Name = "xrTableCell59";
        this.xrTableCell59.Text = "%";
        this.xrTableCell59.Weight = 1.4319311523437501D;
        // 
        // xrTableCell60
        // 
        this.xrTableCell60.Name = "xrTableCell60";
        this.xrTableCell60.Text = "คัน";
        this.xrTableCell60.Weight = 1.1165136718750008D;
        // 
        // xrTableCell61
        // 
        this.xrTableCell61.Name = "xrTableCell61";
        this.xrTableCell61.Text = "%";
        this.xrTableCell61.Weight = 1.3030480957031254D;
        // 
        // xrTableCell62
        // 
        this.xrTableCell62.Name = "xrTableCell62";
        this.xrTableCell62.Text = "คัน";
        this.xrTableCell62.Weight = 3.1150659179687503D;
        // 
        // xrTableCell63
        // 
        this.xrTableCell63.Name = "xrTableCell63";
        this.xrTableCell63.Text = "%";
        this.xrTableCell63.Weight = 1.5783641493908089D;
        // 
        // xrTableCell64
        // 
        this.xrTableCell64.Name = "xrTableCell64";
        this.xrTableCell64.Weight = 2.4554441584597013D;
        // 
        // xrTableCell65
        // 
        this.xrTableCell65.Name = "xrTableCell65";
        this.xrTableCell65.Text = "%";
        this.xrTableCell65.Weight = 1.4991653249619912D;
        // 
        // xrTableCell66
        // 
        this.xrTableCell66.Name = "xrTableCell66";
        this.xrTableCell66.Text = "คัน/วัน";
        this.xrTableCell66.Weight = 1.5358374023437487D;
        // 
        // xrTableCell67
        // 
        this.xrTableCell67.Name = "xrTableCell67";
        this.xrTableCell67.Text = "%";
        this.xrTableCell67.Weight = 1.7783325195312512D;
        // 
        // xrTableCell68
        // 
        this.xrTableCell68.Name = "xrTableCell68";
        this.xrTableCell68.Text = "คัน/วัน";
        this.xrTableCell68.Weight = 1.3683374023437507D;
        // 
        // xrTableCell69
        // 
        this.xrTableCell69.Name = "xrTableCell69";
        this.xrTableCell69.Text = "%";
        this.xrTableCell69.Weight = 1.6600024414062482D;
        // 
        // xrTableRow9
        // 
        this.xrTableRow9.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.xrTableCell145,
            this.xrTableCell146});
        this.xrTableRow9.Name = "xrTableRow9";
        this.xrTableRow9.StylePriority.UseBackColor = false;
        this.xrTableRow9.StylePriority.UseBorders = false;
        this.xrTableRow9.Weight = 0.32905767511560813D;
        // 
        // xrTableCell126
        // 
        this.xrTableCell126.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell126.Name = "xrTableCell126";
        this.xrTableCell126.StylePriority.UseBackColor = false;
        this.xrTableCell126.Text = "-";
        this.xrTableCell126.Weight = 1.3274996731257978D;
        // 
        // xrTableCell127
        // 
        this.xrTableCell127.Name = "xrTableCell127";
        this.xrTableCell127.Text = "-";
        this.xrTableCell127.Weight = 1.3079165649414064D;
        // 
        // xrTableCell128
        // 
        this.xrTableCell128.Name = "xrTableCell128";
        this.xrTableCell128.Text = "-";
        this.xrTableCell128.Weight = 1.0866665649414062D;
        // 
        // xrTableCell129
        // 
        this.xrTableCell129.Name = "xrTableCell129";
        this.xrTableCell129.Text = "(3) / (0)";
        this.xrTableCell129.Weight = 0.92416687011718757D;
        // 
        // xrTableCell130
        // 
        this.xrTableCell130.Name = "xrTableCell130";
        this.xrTableCell130.Text = "-";
        this.xrTableCell130.Weight = 0.86312499999999992D;
        // 
        // xrTableCell131
        // 
        this.xrTableCell131.Name = "xrTableCell131";
        this.xrTableCell131.Text = "-";
        this.xrTableCell131.Weight = 1.3742718505859375D;
        // 
        // xrTableCell132
        // 
        this.xrTableCell132.Name = "xrTableCell132";
        this.xrTableCell132.Text = "(6) / (3)";
        this.xrTableCell132.Weight = 1.1871350097656253D;
        // 
        // xrTableCell133
        // 
        this.xrTableCell133.Name = "xrTableCell133";
        this.xrTableCell133.Text = "-";
        this.xrTableCell133.Weight = 0.999999894669874D;
        // 
        // xrTableCell134
        // 
        this.xrTableCell134.Name = "xrTableCell134";
        this.xrTableCell134.Text = "(6) / (0)";
        this.xrTableCell134.Weight = 1.8773310379473129D;
        // 
        // xrTableCell135
        // 
        this.xrTableCell135.Name = "xrTableCell135";
        this.xrTableCell135.Text = "((0)-(3)) / (0)";
        this.xrTableCell135.Weight = 1.6240124511718754D;
        // 
        // xrTableCell136
        // 
        this.xrTableCell136.Name = "xrTableCell136";
        this.xrTableCell136.Text = "((3) - (6)) / (0)";
        this.xrTableCell136.Weight = 1.4319311523437501D;
        // 
        // xrTableCell137
        // 
        this.xrTableCell137.Name = "xrTableCell137";
        this.xrTableCell137.Text = "((0)-(3)) x (1)";
        this.xrTableCell137.Weight = 1.1165136718750008D;
        // 
        // xrTableCell138
        // 
        this.xrTableCell138.Name = "xrTableCell138";
        this.xrTableCell138.Text = "(12) / (2)";
        this.xrTableCell138.Weight = 1.3030480957031254D;
        // 
        // xrTableCell139
        // 
        this.xrTableCell139.Name = "xrTableCell139";
        this.xrTableCell139.Text = "-";
        this.xrTableCell139.Weight = 3.1150659179687503D;
        // 
        // xrTableCell140
        // 
        this.xrTableCell140.Name = "xrTableCell140";
        this.xrTableCell140.Text = "(14) / (2)";
        this.xrTableCell140.Weight = 1.5783641493908089D;
        // 
        // xrTableCell141
        // 
        this.xrTableCell141.Name = "xrTableCell141";
        this.xrTableCell141.Text = "(12) + (14)";
        this.xrTableCell141.Weight = 2.4554441584597013D;
        // 
        // xrTableCell142
        // 
        this.xrTableCell142.Name = "xrTableCell142";
        this.xrTableCell142.Text = "(16) / (2)";
        this.xrTableCell142.Weight = 1.4991653249619912D;
        // 
        // xrTableCell143
        // 
        this.xrTableCell143.Name = "xrTableCell143";
        this.xrTableCell143.Text = "((0)x(1) - (16)) / (0)";
        this.xrTableCell143.Weight = 1.5358374023437487D;
        // 
        // xrTableCell144
        // 
        this.xrTableCell144.Name = "xrTableCell144";
        this.xrTableCell144.Text = "(18) / (1)";
        this.xrTableCell144.Weight = 1.7783325195312512D;
        // 
        // xrTableCell145
        // 
        this.xrTableCell145.Name = "xrTableCell145";
        this.xrTableCell145.Text = "((0)x(1) - (16)) / (3)";
        this.xrTableCell145.Weight = 1.3683374023437507D;
        // 
        // xrTableCell146
        // 
        this.xrTableCell146.Name = "xrTableCell146";
        this.xrTableCell146.Text = "(20) / (1)";
        this.xrTableCell146.Weight = 1.6600024414062482D;
        // 
        // xrTable5
        // 
        this.xrTable5.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable5.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable5.ForeColor = System.Drawing.Color.White;
        this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(4.248047F, 0F);
        this.xrTable5.Name = "xrTable5";
        this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow7});
        this.xrTable5.SizeF = new System.Drawing.SizeF(141.2101F, 269.916F);
        this.xrTable5.StylePriority.UseBackColor = false;
        this.xrTable5.StylePriority.UseBorders = false;
        this.xrTable5.StylePriority.UseFont = false;
        this.xrTable5.StylePriority.UseForeColor = false;
        this.xrTable5.StylePriority.UseTextAlignment = false;
        this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93});
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Weight = 1.1415094810734494D;
        // 
        // xrTableCell93
        // 
        this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell93.Multiline = true;
        this.xrTableCell93.Name = "xrTableCell93";
        this.xrTableCell93.StylePriority.UseBackColor = false;
        this.xrTableCell93.StylePriority.UseBorderColor = false;
        this.xrTableCell93.StylePriority.UseBorders = false;
        this.xrTableCell93.Text = "(0)\r\nจำนวนวันที่จัดทำรายงาน\r\n[COLUMN0]\r\n\r\n วัน";
        this.xrTableCell93.Weight = 3D;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell95});
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.Weight = 0.19279247042584105D;
        // 
        // xrTableCell71
        // 
        this.xrTableCell71.Name = "xrTableCell71";
        this.xrTableCell71.Text = "วันที่เริ่ม";
        this.xrTableCell71.Weight = 1.4999998164221218D;
        // 
        // xrTableCell95
        // 
        this.xrTableCell95.Name = "xrTableCell95";
        this.xrTableCell95.Text = "วันที่สิ้นสุด";
        this.xrTableCell95.Weight = 1.5000001835778782D;
        // 
        // COLUMN4
        // 
        this.COLUMN4.DataMember = "TReports01";
        this.COLUMN4.DisplayName = "COLUMN4";
        this.COLUMN4.Expression = "Iif(([COLUMN3] / [COLUMN0])  < 0, 0 ,\r\nIif(([COLUMN3] / [COLUMN0])  >= 1,1  ,([CO" +
            "LUMN3] / [COLUMN0])  ) )";
        this.COLUMN4.Name = "COLUMN4";
        // 
        // COLUMN5
        // 
        this.COLUMN5.DataMember = "TReports01";
        this.COLUMN5.Name = "COLUMN5";
        // 
        // COLUMN7
        // 
        this.COLUMN7.DataMember = "TReports01";
        this.COLUMN7.Expression = "Iif(([COLUMN6]/[COLUMN3])  < 0,0  ,\r\nIif(([COLUMN6]/[COLUMN3])  >= 1,1  ,([COLUMN" +
            "6]/[COLUMN3])  ) )";
        this.COLUMN7.Name = "COLUMN7";
        // 
        // COLUMN9
        // 
        this.COLUMN9.DataMember = "TReports01";
        this.COLUMN9.Expression = "Iif(([COLUMN6]/[COLUMN0]) < 0,0  , \r\nIif(([COLUMN6]/[COLUMN0]) > 1, 1  ,([COLUMN6" +
            "]/[COLUMN0]) ))";
        this.COLUMN9.Name = "COLUMN9";
        // 
        // COLUMN10
        // 
        this.COLUMN10.DataMember = "TReports01";
        this.COLUMN10.Expression = "Iif((([COLUMN0]-[COLUMN3])/[COLUMN0])  < 0,0  , \r\nIif((([COLUMN0]-[COLUMN3])/[COL" +
            "UMN0])  >= 1, 1  ,(([COLUMN0]-[COLUMN3])/[COLUMN0]) ))";
        this.COLUMN10.Name = "COLUMN10";
        // 
        // COLUMN11
        // 
        this.COLUMN11.DataMember = "TReports01";
        this.COLUMN11.Expression = "Iif((([COLUMN3]-[COLUMN6])/[COLUMN0])  < 0,0  , \r\nIif((([COLUMN3]-[COLUMN6])/[COL" +
            "UMN0])  >= 1,1  ,(([COLUMN3]-[COLUMN6])/[COLUMN0])  ))";
        this.COLUMN11.Name = "COLUMN11";
        // 
        // COLUMN12
        // 
        this.COLUMN12.DataMember = "TReports01";
        this.COLUMN12.Expression = "Iif((([COLUMN0]-[COLUMN3])*[COLUMN1]) < 0,0  ,(([COLUMN0]-[COLUMN3])*[COLUMN1]) )" +
            "";
        this.COLUMN12.Name = "COLUMN12";
        // 
        // COLUMN13
        // 
        this.COLUMN13.DataMember = "TReports01";
        this.COLUMN13.Expression = "Iif(([COLUMN12]/[COLUMN2]) < 0,0  ,Iif(([COLUMN12]/[COLUMN2]) >= 1,1  ,([COLUMN12" +
            "]/[COLUMN2]) ) )";
        this.COLUMN13.Name = "COLUMN13";
        // 
        // COLUMN15
        // 
        this.COLUMN15.DataMember = "TReports01";
        this.COLUMN15.Expression = "Iif(([COLUMN14] / [COLUMN2]) <0,0  ,\r\nIif(([COLUMN14] / [COLUMN2]) >=1,1  ,([COLU" +
            "MN14] / [COLUMN2])  ) )";
        this.COLUMN15.Name = "COLUMN15";
        // 
        // COLUMN16
        // 
        this.COLUMN16.DataMember = "TReports01";
        this.COLUMN16.Expression = "([COLUMN12]+[COLUMN14])";
        this.COLUMN16.Name = "COLUMN16";
        // 
        // COLUMN17
        // 
        this.COLUMN17.DataMember = "TReports01";
        this.COLUMN17.Expression = "Iif((([COLUMN16])/[COLUMN2]) < 0,0  ,\r\nIif((([COLUMN16])/[COLUMN2]) >= 1,1  ,(([C" +
            "OLUMN16])/[COLUMN2]) ) )";
        this.COLUMN17.Name = "COLUMN17";
        // 
        // COLUMN18
        // 
        this.COLUMN18.DataMember = "TReports01";
        this.COLUMN18.Expression = "Iif((([COLUMN0]*[COLUMN1]) - [COLUMN16]) / [COLUMN0] > 0\r\n,(([COLUMN0]*[COLUMN1])" +
            " - [COLUMN16]) / [COLUMN0]\r\n, 0)";
        this.COLUMN18.Name = "COLUMN18";
        // 
        // COLUMN19
        // 
        this.COLUMN19.DataMember = "TReports01";
        this.COLUMN19.Expression = "Iif([COLUMN18]/[COLUMN1] <0,0  , Iif([COLUMN18]/[COLUMN1] >= 1,1  ,[COLUMN18]/[CO" +
            "LUMN1] ))";
        this.COLUMN19.Name = "COLUMN19";
        // 
        // COLUMN20
        // 
        this.COLUMN20.DataMember = "TReports01";
        this.COLUMN20.Expression = "Iif((([COLUMN0]*[COLUMN1])-[COLUMN16]) / [COLUMN3]<0,0  ,(([COLUMN0]*[COLUMN1])-[" +
            "COLUMN16]) / [COLUMN3] )";
        this.COLUMN20.Name = "COLUMN20";
        // 
        // COLUMN21
        // 
        this.COLUMN21.DataMember = "TReports01";
        this.COLUMN21.Expression = "Iif(([COLUMN20] / [COLUMN1]) < 0 , 0  ,\r\nIif(([COLUMN20] / [COLUMN1]) >= 1, 1 ,([" +
            "COLUMN20] / [COLUMN1]) ) )";
        this.COLUMN21.Name = "COLUMN21";
        // 
        // COLUMN2
        // 
        this.COLUMN2.DataMember = "TReports01";
        this.COLUMN2.Expression = "[COLUMN0] * [COLUMN1]";
        this.COLUMN2.Name = "COLUMN2";
        // 
        // dStart
        // 
        this.dStart.DataMember = "TReports01";
        this.dStart.Name = "dStart";
        // 
        // dEnd
        // 
        this.dEnd.Name = "dEnd";
        // 
        // COLUMN14
        // 
        this.COLUMN14.DataMember = "TReports01";
        this.COLUMN14.Name = "COLUMN14";
        // 
        // xrtDetailConfirmContact
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.ReportHeader,
            this.PageHeader});
        this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.COLUMN4,
            this.COLUMN5,
            this.COLUMN7,
            this.COLUMN9,
            this.COLUMN10,
            this.COLUMN11,
            this.COLUMN12,
            this.COLUMN13,
            this.COLUMN15,
            this.COLUMN16,
            this.COLUMN17,
            this.COLUMN18,
            this.COLUMN19,
            this.COLUMN20,
            this.COLUMN21,
            this.COLUMN2,
            this.dStart,
            this.dEnd,
            this.COLUMN14});
        this.DataMember = "TReports01";
        this.DataSource = this.dsDetailConfirmContact1;
        this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(2, 0, 19, 114);
        this.PageHeight = 855;
        this.PageWidth = 3590;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailConfirmContact1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
