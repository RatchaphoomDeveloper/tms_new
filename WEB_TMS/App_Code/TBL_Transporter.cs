﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Data.OracleClient;
using System.Data;
using System.Globalization;
/// <summary>
/// Summary description for TBL_Transporter
/// </summary>
public class TBL_Transporter
{
    public static OracleConnection conn;
    public static Page page;
    public TBL_Transporter(Page _page, OracleConnection _conn)
    {  // TODO: Add constructor logic here 
        page = _page; conn = _conn;
    }
    #region Data
    private string _VENDORID;
    private string _VENDORNAME;
    private string _NO;
    private string _DISTRICT;
    private string _REGION;
    private string _PROVINCE;
    private string _PROVINCECODE;
    private string _ACTIVE;
    private string _CREATEED_DATE;
    private string _CREATE_By;
    private string _UPDATE_DATE;
    private string _UPDATE_By;
    private string _SAPStatus;
    private string _Regis_date;
    private string _Capital;
    private string _Cert_Begin;
    private string _Cert_End;
    private string _Statute_Begin;
    private string _Statute_End;

    private string _LINE_NO;
    private string _NAME;
    private string _POSITION;
    private string _PHONE;
    private string _PHONE2;
    private string _EMAIL;
    private string _IS_ACTIVE;
    #endregion

    #region Property
    /// <summary>
    /// รหัสผู้ขนส่ง.ต้องเหมือนใน SAP
    /// </summary>
    public string VENDORID
    {
        get { return this._VENDORID; }
        set { this._VENDORID = value; }
    }
    /// <summary>
    /// ชื่อผู้ขนส่ง
    /// </summary>
    public string VENDORNAME
    {
        get { return this._VENDORNAME; }
        set { this._VENDORNAME = value; }
    }
    /// <summary>
    /// ที่อยู่
    /// </summary>
    public string NO
    {
        get { return this._NO; }
        set { this._NO = value; }
    }
    /// <summary>
    /// ตำบล/เขต/แขวง
    /// </summary>
    public string DISTRICT
    {
        get { return this._DISTRICT; }
        set { this._DISTRICT = value; }
    }
    /// <summary>
    /// อำเภอ
    /// </summary>
    public string REGION
    {
        get { return this._REGION; }
        set { this._REGION = value; }
    }
    /// <summary>
    /// จังหวัด
    /// </summary>
    public string PROVINCE
    {
        get { return this._PROVINCE; }
        set { this._PROVINCE = value; }
    }
    /// <summary>
    /// รหัสไปรษณีย์
    /// </summary>
    public string PROVINCECODE
    {
        get { return this._PROVINCECODE; }
        set { this._PROVINCECODE = value; }
    }
    /// <summary>
    /// สถานะการใช้งาน
    /// </summary>
    public string ACTIVE
    {
        get { return this._ACTIVE; }
        set { this._ACTIVE = value; }
    }
    /// <summary>
    /// วันที่สร้างรายการ
    /// </summary>
    public string CREATEED_DATE
    {
        get { return this._CREATEED_DATE; }
        set { this._CREATEED_DATE = value; }
    }
    /// <summary>
    /// ผุ้สร้างรายการ
    /// </summary>
    public string CREATE_By
    {
        get { return this._CREATE_By; }
        set { this._CREATE_By = value; }
    }
    /// <summary>
    /// วันที่อัพเดทรายการล้าสุด
    /// </summary>
    public string UPDATE_DATE
    {
        get { return this._UPDATE_DATE; }
        set { this._UPDATE_DATE = value; }
    }
    /// <summary>
    /// ผู้อัพเดทรายการล่าสุด
    /// </summary>
    public string UPDATE_By
    {
        get { return this._UPDATE_By; }
        set { this._UPDATE_By = value; }
    }
    /// <summary>
    /// สถานะ SAP
    /// </summary>
    public string SAPStatus
    {
        get { return this._SAPStatus; }
        set { this._SAPStatus = value; }
    }
    /// <summary>
    /// ลำดับ
    /// </summary>
    public string LINE_NO
    {
        get { return this._LINE_NO; }
        set { this._LINE_NO = value; }
    }
    /// <summary>
    /// ชื่อสกุล
    /// </summary>
    public string NAME
    {
        get { return this._NAME; }
        set { this._NAME = value; }
    }
    /// <summary>
    /// ตำแหน่ง
    /// </summary>
    public string POSITION
    {
        get { return this._POSITION; }
        set { this._POSITION = value; }
    }
    /// <summary>
    /// เบอร์โทรศัพท์
    /// </summary>
    public string PHONE
    {
        get { return this._PHONE; }
        set { this._PHONE = value; }
    }
    /// <summary>
    /// เบอร์โทรศัพท์สำรอง
    /// </summary>
    public string PHONE2
    {
        get { return this._PHONE2; }
        set { this._PHONE2 = value; }
    }
    /// <summary>
    /// อีเมล์
    /// </summary>
    public string EMAIL
    {
        get { return this._EMAIL; }
        set { this._EMAIL = value; }
    }
    /// <summary>
    /// สถานะการใช้งาน ผู้ลงนาม
    /// </summary>
    public string IS_ACTIVE
    {
        get { return this._IS_ACTIVE; }
        set { this._IS_ACTIVE = value; }
    }
    /// <summary>
    /// วันที่ลงทะเบียนคู่ค้า
    /// </summary>
    public string Regis_date
    {
        get { return this._Regis_date; }
        set { this._Regis_date = value; }
    }
    /// <summary>
    /// ทุนจดทะเบียน
    /// </summary>
    public string Capital
    {
        get { return this._Capital; }
        set { this._Capital = value; }
    }
    /// <summary>
    /// อายุใบอนุญาตขนส่ง_ตั้งแต่วันที่
    /// </summary>
    public string Cert_Begin
    {
        get { return this._Cert_Begin; }
        set { this._Cert_Begin = value; }
    }
    /// <summary>
    /// อายุใบอนุญาตขนส่ง_ถึงวันที่
    /// </summary>
    public string Cert_End
    {
        get { return this._Cert_End; }
        set { this._Cert_End = value; }
    }
    /// <summary>
    /// อายุมาตรา 12_ตั่งแต่วันที่
    /// </summary>
    public string Statute_Begin
    {
        get { return this._Statute_Begin; }
        set { this._Statute_Begin = value; }
    }
    /// <summary>
    /// อายุมาตรา 12_ถึงวันที่
    /// </summary>
    public string Statute_End
    {
        get { return this._Statute_End; }
        set { this._Statute_End = value; }
    }
    #endregion

    #region Method
    public bool Open()
    {
        string sqlstr;
        bool result = false;

        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        //
        sqlstr = @"SELECT VEND_SAP.SVENDORID,VEND_SAP.SABBREVIATION 
 FROM TVENDOR_SAP VEND_SAP
LEFT JOIN TVENDOR  VEND_TMS ON EMP_SAP.SEMPLOYEEID=VEND_TMS.SEMPLOYEEID
WHERE VEND_SAP.SVENDORID='" + VENDORID + "'";
        DataTable dt = new DataTable();
        new OracleDataAdapter(sqlstr, conn).Fill(dt);
        if (dt.Rows.Count >= 1)
        {
            DataRow row = dt.Rows[0];
            VENDORID = row["SVENDORID"].ToString();
            VENDORNAME = row["SABBREVIATION"].ToString();
            dt.Dispose();
            result = true;
        }

        return result;
    }
    #endregion


}