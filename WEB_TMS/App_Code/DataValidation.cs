﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace Utility
{
    public class  TypeOfData
    {
        public const int Date = 0;
        public const int IDCard = 0;
    }

    public static class DataValidation
    {
        public static bool IsIDCard(string _str)
        {
            return (_str.Replace(",", "").Replace("-", "").Count(char.IsDigit) == 13);
        }
        public static bool IsInputValid(TextBox _textbox , int _type)
        {
            if ((_type == TypeOfData.Date) && !String.IsNullOrEmpty(_textbox.Text)) 
            {
                return (DateTimeHelpers.ParseDateTime(_textbox.Text).HasValue);
            }
            else if ((_type == TypeOfData.IDCard) && !String.IsNullOrEmpty(_textbox.Text)) 
            {
                return (IsIDCard(_textbox.Text));
            }
            return true;
        }
    }
}
