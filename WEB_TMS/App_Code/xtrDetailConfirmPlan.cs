﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for xtrDetailConfirmPlan
/// </summary>
public class xtrDetailConfirmPlan : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand PageHeader;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell8;
    private DetailReportBand DetailReport;
    private DetailBand Detail1;
    private GroupHeaderBand GroupHeader1;
    private GroupFooterBand GroupFooter1;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell10;
    private XRTableCell xrTableCell11;
    private XRTableCell xrTableCell12;
    private XRTableCell xrTableCell14;
    private XRTableCell xrTableCell15;
    private XRTableCell xrTableCell16;
    private XRTableCell xrTableCell17;
    private XRLabel xrLabel1;
    private CalculatedField SUMCOLUMN3_1;
    private XRTableCell xrTableCell20;
    private FormattingRule formattingRule1;
    private dsDetailConfirmPlan dsDetailConfirmPlan1;
    private XRTable xrTable4;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell13;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell18;
    private XRTableCell xrTableCell21;
    private XRTable xrTable5;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell23;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell26;
    private XRTableCell xrTableCell32;
    private XRTableCell xrTableCell27;
    private XRTableCell xrTableCell28;
    private XRTableCell xrTableCell29;
    private XRTableCell xrTableCell30;
    private XRTableCell xrTableCell24;
    private XRTableCell xrTableCell25;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell31;
    private XRTableCell xrTableCell34;
    private XRTableCell xrTableCell37;
    private XRTableCell xrTableCell33;
    private XRTableCell xrTableCell35;
    private XRTableCell xrTableCell38;
    private XRTableCell xrTableCell36;
    private XRTableCell xrTableCell41;
    private XRTableCell xrTableCell39;
    private XRTableCell xrTableCell42;
    private XRTableCell xrTableCell40;
    private XRTableCell xrTableCell43;
    private XRTableCell xrTableCell44;
    private XRTableCell xrTableCell45;
    private XRTableCell xrTableCell46;
    private XRTableCell xrTableCell47;
    private XRTableCell xrTableCell48;
    private XRTableCell xrTableCell49;
    private XRTableCell xrTableCell51;
    private XRTableCell xrTableCell50;
    private XRTableCell xrTableCell53;
    private XRTableCell xrTableCell54;
    private XRTableCell xrTableCell55;
    private XRTableCell xrTableCell56;
    private XRTableCell xrTableCell57;
    private XRTableCell xrTableCell58;
    private XRTableCell xrTableCell59;
    private XRTableCell xrTableCell60;
    private XRTableCell xrTableCell61;
    private XRTableCell xrTableCell62;
    private XRTableCell xrTableCell63;
    private ReportFooterBand ReportFooter;
    private XRTable xrTable6;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell65;
    private XRTableCell xrTableCell66;
    private XRTableCell xrTableCell68;
    private XRTableCell xrTableCell69;
    private XRTableCell xrTableCell70;
    private XRTableCell xrTableCell72;
    private XRTableCell xrTableCell73;
    private XRTableCell xrTableCell74;
    private XRTableCell xrTableCell75;
    private XRTableCell xrTableCell76;
    private XRTableCell xrTableCell77;
    private XRTableCell xrTableCell78;
    private XRTableCell xrTableCell79;
    private XRTableCell xrTableCell80;
    private XRTableCell xrTableCell81;
    private XRTableCell xrTableCell82;
    private XRControlStyle xrControlStyle1;
    private XRTableCell xrTableCell67;
    private XRTableCell xrTableCell52;
    private CalculatedField COLUMN4_1;
    private XRTableCell xrTableCell71;
    private CalculatedField COLUMN5_1;
    private CalculatedField COLUMN6_1;
    private CalculatedField COLUMN7_1;
    private CalculatedField COLUMN8_1;
    private CalculatedField COLUMN9_1;
    private XRTableCell xrTableCell83;
    private XRTableCell xrTableCell64;
    private XRTableCell xrTableCell19;
    private XRTableCell xrTableCell22;
    private XRTableCell xrTableCell84;
    private XRTableRow xrTableRow9;
    private XRTableCell xrTableCell85;
    private XRTableCell xrTableCell86;
    private XRTableCell xrTableCell87;
    private XRTableCell xrTableCell88;
    private XRTableCell xrTableCell89;
    private XRTableCell xrTableCell90;
    private XRTableCell xrTableCell91;
    private XRTableCell xrTableCell92;
    private XRTableCell xrTableCell93;
    private XRTableCell xrTableCell94;
    private XRTableCell xrTableCell95;
    private XRTableCell xrTableCell96;
    private XRTableCell xrTableCell97;
    private XRTableCell xrTableCell98;
    private CalculatedField dStart;
    private CalculatedField dEnd;
    private CalculatedField SUMCOLUMN4_1;
    private CalculatedField ALLSUMCOLUMN4_1;
    private CalculatedField SUMCOLUMN5_1;
    private CalculatedField SUMCOLUMN6_1;
    private CalculatedField ALLSUMCOLUMN5_1;
    private CalculatedField ALLSUMCOLUMN6_1;
    private CalculatedField ALLSUMCOLUMN3_1;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public xtrDetailConfirmPlan()
	{
		InitializeComponent();
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "xtrDetailConfirmPlan.resx";
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary19 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary20 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary21 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary22 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary23 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary24 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary25 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary26 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary27 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary28 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.XtraReports.UI.XRSummary xrSummary29 = new DevExpress.XtraReports.UI.XRSummary();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
        this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
        this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
        this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
        this.dsDetailConfirmPlan1 = new dsDetailConfirmPlan();
        this.SUMCOLUMN3_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
        this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
        this.COLUMN4_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN5_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN6_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN7_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN8_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.COLUMN9_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.dStart = new DevExpress.XtraReports.UI.CalculatedField();
        this.dEnd = new DevExpress.XtraReports.UI.CalculatedField();
        this.SUMCOLUMN4_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.ALLSUMCOLUMN4_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.SUMCOLUMN5_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.SUMCOLUMN6_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.ALLSUMCOLUMN5_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.ALLSUMCOLUMN6_1 = new DevExpress.XtraReports.UI.CalculatedField();
        this.ALLSUMCOLUMN3_1 = new DevExpress.XtraReports.UI.CalculatedField();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailConfirmPlan1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Expanded = false;
        this.Detail.HeightF = 53.125F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // TopMargin
        // 
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.StylePriority.UseBorders = false;
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable1,
            this.xrTable5});
        this.PageHeader.HeightF = 105F;
        this.PageHeader.Name = "PageHeader";
        // 
        // xrTable4
        // 
        this.xrTable4.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable4.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable4.ForeColor = System.Drawing.Color.White;
        this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable4.Name = "xrTable4";
        this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5});
        this.xrTable4.SizeF = new System.Drawing.SizeF(200F, 105F);
        this.xrTable4.StylePriority.UseBackColor = false;
        this.xrTable4.StylePriority.UseFont = false;
        this.xrTable4.StylePriority.UseForeColor = false;
        this.xrTable4.StylePriority.UseTextAlignment = false;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13});
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.Weight = 1D;
        // 
        // xrTableCell13
        // 
        this.xrTableCell13.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell13.Multiline = true;
        this.xrTableCell13.Name = "xrTableCell13";
        this.xrTableCell13.StylePriority.UseBackColor = false;
        this.xrTableCell13.StylePriority.UseBorders = false;
        this.xrTableCell13.StylePriority.UseTextAlignment = false;
        this.xrTableCell13.Text = "(0) \r\nวันที่ขนส่ง\r\n ([nDate]) วัน";
        this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell13.Weight = 16.128669604722671D;
        // 
        // xrTableRow5
        // 
        this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell21});
        this.xrTableRow5.Name = "xrTableRow5";
        this.xrTableRow5.Weight = 1D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell18.Multiline = true;
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.StylePriority.UseBorders = false;
        this.xrTableCell18.StylePriority.UseTextAlignment = false;
        this.xrTableCell18.Text = "วันที่เริ่ม";
        this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell18.Weight = 8.0643348023613353D;
        // 
        // xrTableCell21
        // 
        this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell21.Multiline = true;
        this.xrTableCell21.Name = "xrTableCell21";
        this.xrTableCell21.StylePriority.UseBorders = false;
        this.xrTableCell21.StylePriority.UseTextAlignment = false;
        this.xrTableCell21.Text = "วันที่สิ้นสุด";
        this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell21.Weight = 8.0643348023613353D;
        // 
        // xrTable1
        // 
        this.xrTable1.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable1.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable1.ForeColor = System.Drawing.Color.White;
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(700F, 0F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow7,
            this.xrTableRow9});
        this.xrTable1.SizeF = new System.Drawing.SizeF(1400F, 105F);
        this.xrTable1.StylePriority.UseBackColor = false;
        this.xrTable1.StylePriority.UseBorders = false;
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.StylePriority.UseForeColor = false;
        this.xrTable1.StylePriority.UseTextAlignment = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.BackColor = System.Drawing.Color.SlateBlue;
        this.xrTableRow1.BorderColor = System.Drawing.Color.SlateBlue;
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell31,
            this.xrTableCell5,
            this.xrTableCell34,
            this.xrTableCell37});
        this.xrTableRow1.ForeColor = System.Drawing.Color.White;
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.StylePriority.UseBackColor = false;
        this.xrTableRow1.StylePriority.UseBorderColor = false;
        this.xrTableRow1.StylePriority.UseForeColor = false;
        this.xrTableRow1.Weight = 1D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell1.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.StylePriority.UseBackColor = false;
        this.xrTableCell1.StylePriority.UseBorderColor = false;
        this.xrTableCell1.Text = "(3) งานที่จัดแผนแล้ว";
        this.xrTableCell1.Weight = 0.59999999618530253D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell3.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseBorderColor = false;
        this.xrTableCell3.Text = "(4) งานที่ปตท. อัพให้ผู้ขนส่ง";
        this.xrTableCell3.Weight = 0.59999999618530264D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell4.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.StylePriority.UseBackColor = false;
        this.xrTableCell4.StylePriority.UseBorderColor = false;
        this.xrTableCell4.Text = "(5) งานที่คลังจัดแผนFIFO";
        this.xrTableCell4.Weight = 0.60000001525878877D;
        // 
        // xrTableCell31
        // 
        this.xrTableCell31.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell31.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell31.Name = "xrTableCell31";
        this.xrTableCell31.StylePriority.UseBackColor = false;
        this.xrTableCell31.StylePriority.UseBorderColor = false;
        this.xrTableCell31.Text = "(6) งานที่ผู้ขนส่งยืนยันแผน";
        this.xrTableCell31.Weight = 0.59999999809265125D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell5.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.StylePriority.UseBackColor = false;
        this.xrTableCell5.StylePriority.UseBorderColor = false;
        this.xrTableCell5.Text = "(7) งานที่เข้ารับผลิตภัณฑ์แล้ว";
        this.xrTableCell5.Weight = 0.59999999856948838D;
        // 
        // xrTableCell34
        // 
        this.xrTableCell34.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell34.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell34.Name = "xrTableCell34";
        this.xrTableCell34.StylePriority.UseBackColor = false;
        this.xrTableCell34.StylePriority.UseBorderColor = false;
        this.xrTableCell34.Text = "(8) งานที่ยังไม่เข้ารับผลิตภัณฑ์";
        this.xrTableCell34.Weight = 0.59999999892711631D;
        // 
        // xrTableCell37
        // 
        this.xrTableCell37.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell37.BorderColor = System.Drawing.Color.Black;
        this.xrTableCell37.Name = "xrTableCell37";
        this.xrTableCell37.StylePriority.UseBackColor = false;
        this.xrTableCell37.StylePriority.UseBorderColor = false;
        this.xrTableCell37.Text = "(9) งานที่ส่งถึงปลายทางแล้ว(Status 6)";
        this.xrTableCell37.Weight = 0.59999999678134919D;
        // 
        // xrTableRow7
        // 
        this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell32,
            this.xrTableCell27,
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell2,
            this.xrTableCell33,
            this.xrTableCell35,
            this.xrTableCell30,
            this.xrTableCell38,
            this.xrTableCell36,
            this.xrTableCell41,
            this.xrTableCell39,
            this.xrTableCell42});
        this.xrTableRow7.Name = "xrTableRow7";
        this.xrTableRow7.Weight = 1D;
        // 
        // xrTableCell26
        // 
        this.xrTableCell26.Name = "xrTableCell26";
        this.xrTableCell26.Text = "#";
        this.xrTableCell26.Weight = 0.29999999618530265D;
        // 
        // xrTableCell32
        // 
        this.xrTableCell32.Name = "xrTableCell32";
        this.xrTableCell32.Text = "%";
        this.xrTableCell32.Weight = 0.29999999618530265D;
        // 
        // xrTableCell27
        // 
        this.xrTableCell27.Name = "xrTableCell27";
        this.xrTableCell27.Text = "#";
        this.xrTableCell27.Weight = 0.29999999237060554D;
        // 
        // xrTableCell28
        // 
        this.xrTableCell28.Name = "xrTableCell28";
        this.xrTableCell28.Text = "%";
        this.xrTableCell28.Weight = 0.30000000762939455D;
        // 
        // xrTableCell29
        // 
        this.xrTableCell29.Name = "xrTableCell29";
        this.xrTableCell29.Text = "#";
        this.xrTableCell29.Weight = 0.30000000381469716D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Text = "%";
        this.xrTableCell2.Weight = 0.30000000190734866D;
        // 
        // xrTableCell33
        // 
        this.xrTableCell33.Name = "xrTableCell33";
        this.xrTableCell33.Text = "#";
        this.xrTableCell33.Weight = 0.30000000047683711D;
        // 
        // xrTableCell35
        // 
        this.xrTableCell35.Name = "xrTableCell35";
        this.xrTableCell35.Text = "%";
        this.xrTableCell35.Weight = 0.30000000047683717D;
        // 
        // xrTableCell30
        // 
        this.xrTableCell30.Name = "xrTableCell30";
        this.xrTableCell30.Text = "#";
        this.xrTableCell30.Weight = 0.30000000023841855D;
        // 
        // xrTableCell38
        // 
        this.xrTableCell38.Name = "xrTableCell38";
        this.xrTableCell38.Text = "%";
        this.xrTableCell38.Weight = 0.30000000023841855D;
        // 
        // xrTableCell36
        // 
        this.xrTableCell36.Name = "xrTableCell36";
        this.xrTableCell36.Text = "#";
        this.xrTableCell36.Weight = 0.3000000001192093D;
        // 
        // xrTableCell41
        // 
        this.xrTableCell41.Name = "xrTableCell41";
        this.xrTableCell41.Text = "%";
        this.xrTableCell41.Weight = 0.3000000001192093D;
        // 
        // xrTableCell39
        // 
        this.xrTableCell39.Name = "xrTableCell39";
        this.xrTableCell39.Text = "#";
        this.xrTableCell39.Weight = 0.3000000001192093D;
        // 
        // xrTableCell42
        // 
        this.xrTableCell42.Name = "xrTableCell42";
        this.xrTableCell42.Text = "%";
        this.xrTableCell42.Weight = 0.3000000001192093D;
        // 
        // xrTableRow9
        // 
        this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell98});
        this.xrTableRow9.Name = "xrTableRow9";
        this.xrTableRow9.Weight = 1D;
        // 
        // xrTableCell85
        // 
        this.xrTableCell85.Name = "xrTableCell85";
        this.xrTableCell85.Text = "-";
        this.xrTableCell85.Weight = 0.29999999618530265D;
        // 
        // xrTableCell86
        // 
        this.xrTableCell86.Name = "xrTableCell86";
        this.xrTableCell86.Text = "-";
        this.xrTableCell86.Weight = 0.29999999618530265D;
        // 
        // xrTableCell87
        // 
        this.xrTableCell87.Name = "xrTableCell87";
        this.xrTableCell87.Text = "-";
        this.xrTableCell87.Weight = 0.29999999237060554D;
        // 
        // xrTableCell88
        // 
        this.xrTableCell88.Name = "xrTableCell88";
        this.xrTableCell88.Text = "#(4) / #(3)";
        this.xrTableCell88.Weight = 0.30000000762939455D;
        // 
        // xrTableCell89
        // 
        this.xrTableCell89.Name = "xrTableCell89";
        this.xrTableCell89.Text = "-";
        this.xrTableCell89.Weight = 0.30000000381469716D;
        // 
        // xrTableCell90
        // 
        this.xrTableCell90.Name = "xrTableCell90";
        this.xrTableCell90.Text = "#(5) / #(3)";
        this.xrTableCell90.Weight = 0.30000000190734866D;
        // 
        // xrTableCell91
        // 
        this.xrTableCell91.Name = "xrTableCell91";
        this.xrTableCell91.Text = "-";
        this.xrTableCell91.Weight = 0.30000000047683711D;
        // 
        // xrTableCell92
        // 
        this.xrTableCell92.Name = "xrTableCell92";
        this.xrTableCell92.Text = "#6/#4";
        this.xrTableCell92.Weight = 0.30000000047683717D;
        // 
        // xrTableCell93
        // 
        this.xrTableCell93.Name = "xrTableCell93";
        this.xrTableCell93.Text = "-";
        this.xrTableCell93.Weight = 0.30000000023841855D;
        // 
        // xrTableCell94
        // 
        this.xrTableCell94.Name = "xrTableCell94";
        this.xrTableCell94.Text = "#(7) / #(6)";
        this.xrTableCell94.Weight = 0.30000000023841855D;
        // 
        // xrTableCell95
        // 
        this.xrTableCell95.Name = "xrTableCell95";
        this.xrTableCell95.Text = "-";
        this.xrTableCell95.Weight = 0.3000000001192093D;
        // 
        // xrTableCell96
        // 
        this.xrTableCell96.Name = "xrTableCell96";
        this.xrTableCell96.Text = "#(8) / #(6) ";
        this.xrTableCell96.Weight = 0.3000000001192093D;
        // 
        // xrTableCell97
        // 
        this.xrTableCell97.Name = "xrTableCell97";
        this.xrTableCell97.Text = "-";
        this.xrTableCell97.Weight = 0.3000000001192093D;
        // 
        // xrTableCell98
        // 
        this.xrTableCell98.Name = "xrTableCell98";
        this.xrTableCell98.Text = "#(9) / #(7)";
        this.xrTableCell98.Weight = 0.3000000001192093D;
        // 
        // xrTable5
        // 
        this.xrTable5.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable5.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable5.ForeColor = System.Drawing.Color.White;
        this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(200F, 0F);
        this.xrTable5.Name = "xrTable5";
        this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
        this.xrTable5.SizeF = new System.Drawing.SizeF(500F, 105F);
        this.xrTable5.StylePriority.UseBackColor = false;
        this.xrTable5.StylePriority.UseBorders = false;
        this.xrTable5.StylePriority.UseFont = false;
        this.xrTable5.StylePriority.UseForeColor = false;
        // 
        // xrTableRow6
        // 
        this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25});
        this.xrTableRow6.Name = "xrTableRow6";
        this.xrTableRow6.Weight = 1D;
        // 
        // xrTableCell23
        // 
        this.xrTableCell23.BackColor = System.Drawing.Color.SteelBlue;
        this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell23.Name = "xrTableCell23";
        this.xrTableCell23.StylePriority.UseBackColor = false;
        this.xrTableCell23.StylePriority.UseBorders = false;
        this.xrTableCell23.StylePriority.UseTextAlignment = false;
        this.xrTableCell23.Text = "ต้นทาง";
        this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell23.Weight = 1.612866960472267D;
        // 
        // xrTableCell24
        // 
        this.xrTableCell24.Name = "xrTableCell24";
        this.xrTableCell24.StylePriority.UseTextAlignment = false;
        this.xrTableCell24.Text = "ผู้ขนส่ง";
        this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell24.Weight = 3.2257339209445339D;
        // 
        // xrTableCell25
        // 
        this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell25.Name = "xrTableCell25";
        this.xrTableCell25.StylePriority.UseBorders = false;
        this.xrTableCell25.StylePriority.UseTextAlignment = false;
        this.xrTableCell25.Text = "สัญญา";
        this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell25.Weight = 3.2257339209445339D;
        // 
        // xrTable2
        // 
        this.xrTable2.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(2100F, 25F);
        this.xrTable2.StylePriority.UseFont = false;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell50,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell67});
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Weight = 1D;
        // 
        // xrTableCell51
        // 
        this.xrTableCell51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell51.Name = "xrTableCell51";
        this.xrTableCell51.StylePriority.UseBorders = false;
        this.xrTableCell51.Weight = 0.99999993262392817D;
        // 
        // xrTableCell50
        // 
        this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell50.Name = "xrTableCell50";
        this.xrTableCell50.StylePriority.UseBorders = false;
        this.xrTableCell50.Weight = 1.0000000852118292D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.BackColor = System.Drawing.Color.Transparent;
        this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseBackColor = false;
        this.xrTableCell6.StylePriority.UseBorders = false;
        this.xrTableCell6.Weight = 0.99999976096254428D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.StylePriority.UseBackColor = false;
        this.xrTableCell7.StylePriority.UseBorders = false;
        this.xrTableCell7.StylePriority.UseTextAlignment = false;
        this.xrTableCell7.Text = "ทั้งหมด";
        this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell7.Weight = 3.9999998322209684D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN3")});
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.StylePriority.UseBackColor = false;
        this.xrTableCell8.StylePriority.UseBorders = false;
        this.xrTableCell8.StylePriority.UseTextAlignment = false;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell8.Summary = xrSummary1;
        this.xrTableCell8.Text = "[DataTable1.COLUMN3]";
        this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell8.Weight = 0.99999997712871291D;
        // 
        // xrTableCell52
        // 
        this.xrTableCell52.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell52.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.SUMCOLUMN3_1")});
        this.xrTableCell52.Name = "xrTableCell52";
        this.xrTableCell52.StylePriority.UseBackColor = false;
        this.xrTableCell52.StylePriority.UseBorders = false;
        this.xrTableCell52.StylePriority.UseTextAlignment = false;
        xrSummary2.FormatString = "{0:0.00%}";
        xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell52.Summary = xrSummary2;
        this.xrTableCell52.Text = "xrTableCell52";
        this.xrTableCell52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell52.Weight = 0.999999961234149D;
        // 
        // xrTableCell53
        // 
        this.xrTableCell53.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell53.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN4")});
        this.xrTableCell53.Name = "xrTableCell53";
        this.xrTableCell53.StylePriority.UseBackColor = false;
        this.xrTableCell53.StylePriority.UseBorders = false;
        this.xrTableCell53.StylePriority.UseTextAlignment = false;
        xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell53.Summary = xrSummary3;
        this.xrTableCell53.Text = "xrTableCell53";
        this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell53.Weight = 0.999999961234149D;
        // 
        // xrTableCell54
        // 
        this.xrTableCell54.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.SUMCOLUMN4_1", "{0:0.00%}")});
        this.xrTableCell54.Name = "xrTableCell54";
        this.xrTableCell54.StylePriority.UseBackColor = false;
        this.xrTableCell54.StylePriority.UseBorders = false;
        this.xrTableCell54.StylePriority.UseTextAlignment = false;
        xrSummary4.FormatString = "{0:0.00%}";
        xrSummary4.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell54.Summary = xrSummary4;
        this.xrTableCell54.Text = "xrTableCell54";
        this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell54.Weight = 0.99999995487632365D;
        // 
        // xrTableCell55
        // 
        this.xrTableCell55.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell55.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN5")});
        this.xrTableCell55.Name = "xrTableCell55";
        this.xrTableCell55.StylePriority.UseBackColor = false;
        this.xrTableCell55.StylePriority.UseBorders = false;
        this.xrTableCell55.StylePriority.UseTextAlignment = false;
        xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell55.Summary = xrSummary5;
        this.xrTableCell55.Text = "xrTableCell55";
        this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell55.Weight = 0.999999953286868D;
        // 
        // xrTableCell56
        // 
        this.xrTableCell56.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell56.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell56.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.SUMCOLUMN5_1", "{0:0.00%}")});
        this.xrTableCell56.Name = "xrTableCell56";
        this.xrTableCell56.StylePriority.UseBackColor = false;
        this.xrTableCell56.StylePriority.UseBorders = false;
        this.xrTableCell56.StylePriority.UseTextAlignment = false;
        xrSummary6.FormatString = "{0:0.00%}";
        this.xrTableCell56.Summary = xrSummary6;
        this.xrTableCell56.Text = "xrTableCell56";
        this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell56.Weight = 0.99999995249213924D;
        // 
        // xrTableCell57
        // 
        this.xrTableCell57.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN6")});
        this.xrTableCell57.Name = "xrTableCell57";
        this.xrTableCell57.StylePriority.UseBackColor = false;
        this.xrTableCell57.StylePriority.UseBorders = false;
        this.xrTableCell57.StylePriority.UseTextAlignment = false;
        xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell57.Summary = xrSummary7;
        this.xrTableCell57.Text = "xrTableCell57";
        this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell57.Weight = 0.99999995209477532D;
        // 
        // xrTableCell58
        // 
        this.xrTableCell58.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.SUMCOLUMN6_1", "{0:0.00%}")});
        this.xrTableCell58.Name = "xrTableCell58";
        this.xrTableCell58.StylePriority.UseBackColor = false;
        this.xrTableCell58.StylePriority.UseBorders = false;
        this.xrTableCell58.StylePriority.UseTextAlignment = false;
        xrSummary8.FormatString = "{0:0.00%}";
        xrSummary8.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell58.Summary = xrSummary8;
        this.xrTableCell58.Text = "xrTableCell58";
        this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell58.Weight = 0.99999995189609292D;
        // 
        // xrTableCell59
        // 
        this.xrTableCell59.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN7")});
        this.xrTableCell59.Name = "xrTableCell59";
        this.xrTableCell59.StylePriority.UseBackColor = false;
        this.xrTableCell59.StylePriority.UseBorders = false;
        this.xrTableCell59.StylePriority.UseTextAlignment = false;
        xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell59.Summary = xrSummary9;
        this.xrTableCell59.Text = "xrTableCell59";
        this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell59.Weight = 0.9999999517967515D;
        // 
        // xrTableCell60
        // 
        this.xrTableCell60.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "COLUMN7_1")});
        this.xrTableCell60.Name = "xrTableCell60";
        this.xrTableCell60.StylePriority.UseBackColor = false;
        this.xrTableCell60.StylePriority.UseBorders = false;
        this.xrTableCell60.StylePriority.UseTextAlignment = false;
        xrSummary10.FormatString = "{0:0.00%}";
        xrSummary10.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell60.Summary = xrSummary10;
        this.xrTableCell60.Text = "xrTableCell60";
        this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell60.Weight = 0.999999951747081D;
        // 
        // xrTableCell61
        // 
        this.xrTableCell61.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN8")});
        this.xrTableCell61.Name = "xrTableCell61";
        this.xrTableCell61.StylePriority.UseBackColor = false;
        this.xrTableCell61.StylePriority.UseBorders = false;
        this.xrTableCell61.StylePriority.UseTextAlignment = false;
        xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell61.Summary = xrSummary11;
        this.xrTableCell61.Text = "xrTableCell61";
        this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell61.Weight = 0.99999995172224621D;
        // 
        // xrTableCell62
        // 
        this.xrTableCell62.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN8_1")});
        this.xrTableCell62.Name = "xrTableCell62";
        this.xrTableCell62.StylePriority.UseBackColor = false;
        this.xrTableCell62.StylePriority.UseBorders = false;
        this.xrTableCell62.StylePriority.UseTextAlignment = false;
        xrSummary12.FormatString = "{0:0.00%}";
        xrSummary12.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell62.Summary = xrSummary12;
        this.xrTableCell62.Text = "xrTableCell62";
        this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell62.Weight = 0.99999995170982836D;
        // 
        // xrTableCell63
        // 
        this.xrTableCell63.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell63.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN9")});
        this.xrTableCell63.Name = "xrTableCell63";
        this.xrTableCell63.StylePriority.UseBackColor = false;
        this.xrTableCell63.StylePriority.UseBorders = false;
        this.xrTableCell63.StylePriority.UseTextAlignment = false;
        xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell63.Summary = xrSummary13;
        this.xrTableCell63.Text = "xrTableCell63";
        this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell63.Weight = 1.0000011724066855D;
        // 
        // xrTableCell67
        // 
        this.xrTableCell67.BackColor = System.Drawing.Color.Moccasin;
        this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "COLUMN9_1")});
        this.xrTableCell67.Name = "xrTableCell67";
        this.xrTableCell67.StylePriority.UseBackColor = false;
        this.xrTableCell67.StylePriority.UseBorders = false;
        this.xrTableCell67.StylePriority.UseTextAlignment = false;
        xrSummary14.FormatString = "{0:0.00%}";
        xrSummary14.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
        this.xrTableCell67.Summary = xrSummary14;
        this.xrTableCell67.Text = "xrTableCell67";
        this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell67.Weight = 1.0000011724066857D;
        // 
        // DetailReport
        // 
        this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1,
            this.GroupFooter1,
            this.ReportFooter});
        this.DetailReport.DataSource = this.dsDetailConfirmPlan1;
        this.DetailReport.Level = 0;
        this.DetailReport.Name = "DetailReport";
        this.DetailReport.ReportPrintOptions.PrintOnEmptyDataSource = false;
        // 
        // Detail1
        // 
        this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
        this.Detail1.HeightF = 25F;
        this.Detail1.KeepTogether = true;
        this.Detail1.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
        this.Detail1.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnCount;
        this.Detail1.Name = "Detail1";
        this.Detail1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("COLUMN0", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        // 
        // xrTable3
        // 
        this.xrTable3.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(2100F, 25F);
        this.xrTable3.StylePriority.UseFont = false;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell64,
            this.xrTableCell19,
            this.xrTableCell83,
            this.xrTableCell22,
            this.xrTableCell84,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12,
            this.xrTableCell20,
            this.xrTableCell14,
            this.xrTableCell40,
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell43,
            this.xrTableCell17,
            this.xrTableCell45,
            this.xrTableCell44,
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49});
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Weight = 11.5D;
        // 
        // xrTableCell64
        // 
        this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Left;
        this.xrTableCell64.Name = "xrTableCell64";
        this.xrTableCell64.StylePriority.UseBorders = false;
        this.xrTableCell64.Weight = 0.05375477865349898D;
        // 
        // xrTableCell19
        // 
        this.xrTableCell19.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.dStart", "{0:MM/dd/yyyy}")});
        this.xrTableCell19.Name = "xrTableCell19";
        this.xrTableCell19.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell19.StylePriority.UseBorders = false;
        this.xrTableCell19.StylePriority.UseTextAlignment = false;
        this.xrTableCell19.Text = "xrTableCell19";
        this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell19.Weight = 1.6843164015655603D;
        // 
        // xrTableCell83
        // 
        this.xrTableCell83.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell83.Name = "xrTableCell83";
        this.xrTableCell83.StylePriority.UseBorders = false;
        this.xrTableCell83.Weight = 0.05375477865349898D;
        // 
        // xrTableCell22
        // 
        this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell22.CanShrink = true;
        this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.dEnd", "{0:MM/dd/yyyy}")});
        this.xrTableCell22.Name = "xrTableCell22";
        this.xrTableCell22.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell22.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell22.StylePriority.UseBorders = false;
        this.xrTableCell22.StylePriority.UseTextAlignment = false;
        this.xrTableCell22.Text = "xrTableCell22";
        this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell22.Weight = 1.7559888749211725D;
        // 
        // xrTableCell84
        // 
        this.xrTableCell84.Borders = DevExpress.XtraPrinting.BorderSide.Right;
        this.xrTableCell84.Name = "xrTableCell84";
        this.xrTableCell84.StylePriority.UseBorders = false;
        this.xrTableCell84.Weight = 0.035836932035689134D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.BackColor = System.Drawing.Color.Transparent;
        this.xrTableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrTableCell9.CanShrink = true;
        this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN0")});
        this.xrTableCell9.EvenStyleName = "xrControlStyle1";
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.ProcessDuplicates = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrTableCell9.StylePriority.UseBackColor = false;
        this.xrTableCell9.StylePriority.UseBorders = false;
        this.xrTableCell9.StylePriority.UseTextAlignment = false;
        this.xrTableCell9.Text = "xrTableCell9";
        this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell9.Weight = 1.7918248341207552D;
        // 
        // xrTableCell10
        // 
        this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN1")});
        this.xrTableCell10.Name = "xrTableCell10";
        this.xrTableCell10.StylePriority.UseBorders = false;
        this.xrTableCell10.Text = "xrTableCell10";
        this.xrTableCell10.Weight = 3.5836520638983167D;
        // 
        // xrTableCell11
        // 
        this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell11.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN2")});
        this.xrTableCell11.Name = "xrTableCell11";
        this.xrTableCell11.StylePriority.UseBorders = false;
        this.xrTableCell11.Text = "xrTableCell11";
        this.xrTableCell11.Weight = 3.5836516537818861D;
        // 
        // xrTableCell12
        // 
        this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN3")});
        this.xrTableCell12.Name = "xrTableCell12";
        this.xrTableCell12.StylePriority.UseBorders = false;
        this.xrTableCell12.StylePriority.UseTextAlignment = false;
        this.xrTableCell12.Text = "xrTableCell12";
        this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell12.Weight = 1.7918256826554109D;
        // 
        // xrTableCell20
        // 
        this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.SUMCOLUMN3_1", "{0:0.00%}")});
        this.xrTableCell20.Name = "xrTableCell20";
        this.xrTableCell20.StylePriority.UseBorders = false;
        this.xrTableCell20.StylePriority.UseTextAlignment = false;
        this.xrTableCell20.Text = "[DataTable1.SUMCOLUMN3_1] %";
        this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell20.Weight = 1.7918245837986122D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN4")});
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.StylePriority.UseBorders = false;
        this.xrTableCell14.StylePriority.UseTextAlignment = false;
        this.xrTableCell14.Text = "xrTableCell14";
        this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell14.Weight = 1.7918257804095275D;
        // 
        // xrTableCell40
        // 
        this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell40.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN4_1", "{0:0.00%}")});
        this.xrTableCell40.Name = "xrTableCell40";
        this.xrTableCell40.StylePriority.UseBorders = false;
        this.xrTableCell40.StylePriority.UseTextAlignment = false;
        this.xrTableCell40.Text = "xrTableCell40";
        this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell40.Weight = 1.7918257804095272D;
        // 
        // xrTableCell15
        // 
        this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN5")});
        this.xrTableCell15.Name = "xrTableCell15";
        this.xrTableCell15.StylePriority.UseBorders = false;
        this.xrTableCell15.StylePriority.UseTextAlignment = false;
        this.xrTableCell15.Text = "[DataTable1.COLUMN5]";
        this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell15.Weight = 1.7918257702728737D;
        // 
        // xrTableCell16
        // 
        this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN5_1", "{0:0.00%}")});
        this.xrTableCell16.Name = "xrTableCell16";
        this.xrTableCell16.NullValueText = "0";
        this.xrTableCell16.StylePriority.UseBorders = false;
        this.xrTableCell16.StylePriority.UseTextAlignment = false;
        this.xrTableCell16.Text = "xrTableCell16";
        this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell16.Weight = 1.7918257702728735D;
        // 
        // xrTableCell43
        // 
        this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell43.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN6")});
        this.xrTableCell43.Name = "xrTableCell43";
        this.xrTableCell43.StylePriority.UseBorders = false;
        this.xrTableCell43.StylePriority.UseTextAlignment = false;
        this.xrTableCell43.Text = "[DataTable1.COLUMN6]";
        this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell43.Weight = 1.7918257114802598D;
        // 
        // xrTableCell17
        // 
        this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN6_1", "{0:0.00%}")});
        this.xrTableCell17.Name = "xrTableCell17";
        this.xrTableCell17.NullValueText = "0";
        this.xrTableCell17.StylePriority.UseBorders = false;
        this.xrTableCell17.StylePriority.UseTextAlignment = false;
        this.xrTableCell17.Text = "xrTableCell17";
        this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell17.Weight = 1.791825729141908D;
        // 
        // xrTableCell45
        // 
        this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN7")});
        this.xrTableCell45.Name = "xrTableCell45";
        this.xrTableCell45.StylePriority.UseBorders = false;
        this.xrTableCell45.StylePriority.UseTextAlignment = false;
        this.xrTableCell45.Text = "xrTableCell45";
        this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell45.Weight = 1.7918257379727325D;
        // 
        // xrTableCell44
        // 
        this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell44.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN7_1", "{0:0.00%}")});
        this.xrTableCell44.Name = "xrTableCell44";
        this.xrTableCell44.NullValueText = "0";
        this.xrTableCell44.StylePriority.UseBorders = false;
        this.xrTableCell44.StylePriority.UseTextAlignment = false;
        this.xrTableCell44.Text = "xrTableCell44";
        this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell44.Weight = 1.7918257423881445D;
        // 
        // xrTableCell46
        // 
        this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN8")});
        this.xrTableCell46.Name = "xrTableCell46";
        this.xrTableCell46.StylePriority.UseBorders = false;
        this.xrTableCell46.StylePriority.UseTextAlignment = false;
        this.xrTableCell46.Text = "xrTableCell46";
        this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell46.Weight = 1.7918257445958512D;
        // 
        // xrTableCell47
        // 
        this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN8_1", "{0:0.00%}")});
        this.xrTableCell47.Name = "xrTableCell47";
        this.xrTableCell47.NullValueText = "0";
        this.xrTableCell47.StylePriority.UseBorders = false;
        this.xrTableCell47.StylePriority.UseTextAlignment = false;
        xrSummary15.FormatString = "{0:0.00%}";
        this.xrTableCell47.Summary = xrSummary15;
        this.xrTableCell47.Text = "xrTableCell47";
        this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell47.Weight = 1.7918257456997044D;
        // 
        // xrTableCell48
        // 
        this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell48.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN9")});
        this.xrTableCell48.Name = "xrTableCell48";
        this.xrTableCell48.StylePriority.UseBorders = false;
        this.xrTableCell48.StylePriority.UseTextAlignment = false;
        this.xrTableCell48.Text = "xrTableCell48";
        this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell48.Weight = 1.7918284803607418D;
        // 
        // xrTableCell49
        // 
        this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)));
        this.xrTableCell49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN9_1", "{0:0.00%}")});
        this.xrTableCell49.Name = "xrTableCell49";
        this.xrTableCell49.NullValueText = "0";
        this.xrTableCell49.StylePriority.UseBorders = false;
        this.xrTableCell49.StylePriority.UseTextAlignment = false;
        this.xrTableCell49.Text = "xrTableCell49";
        this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell49.Weight = 1.7918284803607418D;
        // 
        // GroupHeader1
        // 
        this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1});
        this.GroupHeader1.Expanded = false;
        this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("COLUMN0", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
        this.GroupHeader1.HeightF = 23F;
        this.GroupHeader1.Name = "GroupHeader1";
        this.GroupHeader1.Visible = false;
        // 
        // xrLabel1
        // 
        this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN0")});
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel1.Text = "xrLabel1";
        // 
        // GroupFooter1
        // 
        this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.GroupFooter1.HeightF = 25F;
        this.GroupFooter1.Name = "GroupFooter1";
        this.GroupFooter1.RepeatEveryPage = true;
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
        this.ReportFooter.HeightF = 25F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // xrTable6
        // 
        this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable6.Name = "xrTable6";
        this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
        this.xrTable6.SizeF = new System.Drawing.SizeF(2100F, 25F);
        // 
        // xrTableRow8
        // 
        this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82});
        this.xrTableRow8.Name = "xrTableRow8";
        this.xrTableRow8.Weight = 1D;
        // 
        // xrTableCell65
        // 
        this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell65.Name = "xrTableCell65";
        this.xrTableCell65.StylePriority.UseBorders = false;
        this.xrTableCell65.Weight = 0.99999993262392828D;
        // 
        // xrTableCell66
        // 
        this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell66.Name = "xrTableCell66";
        this.xrTableCell66.StylePriority.UseBorders = false;
        this.xrTableCell66.Weight = 0.99999993262392817D;
        // 
        // xrTableCell68
        // 
        this.xrTableCell68.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell68.Font = new System.Drawing.Font("Angsana New", 14F);
        this.xrTableCell68.Name = "xrTableCell68";
        this.xrTableCell68.StylePriority.UseBackColor = false;
        this.xrTableCell68.StylePriority.UseBorders = false;
        this.xrTableCell68.StylePriority.UseFont = false;
        this.xrTableCell68.StylePriority.UseTextAlignment = false;
        this.xrTableCell68.Text = "ทั้งหมด";
        this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.xrTableCell68.Weight = 4.9999997457714134D;
        // 
        // xrTableCell69
        // 
        this.xrTableCell69.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN3")});
        this.xrTableCell69.Name = "xrTableCell69";
        this.xrTableCell69.StylePriority.UseBackColor = false;
        this.xrTableCell69.StylePriority.UseBorders = false;
        this.xrTableCell69.StylePriority.UseTextAlignment = false;
        xrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell69.Summary = xrSummary16;
        this.xrTableCell69.Text = "xrTableCell69";
        this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell69.Weight = 0.99999997712871291D;
        // 
        // xrTableCell70
        // 
        this.xrTableCell70.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.ALLSUMCOLUMN3_1", "{0:0.00%}")});
        this.xrTableCell70.Name = "xrTableCell70";
        this.xrTableCell70.StylePriority.UseBackColor = false;
        this.xrTableCell70.StylePriority.UseBorders = false;
        this.xrTableCell70.StylePriority.UseTextAlignment = false;
        xrSummary17.FormatString = "{0:0.00%}";
        xrSummary17.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell70.Summary = xrSummary17;
        this.xrTableCell70.Text = "xrTableCell70";
        this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell70.Weight = 0.99999996441306216D;
        // 
        // xrTableCell71
        // 
        this.xrTableCell71.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN4")});
        this.xrTableCell71.Name = "xrTableCell71";
        this.xrTableCell71.StylePriority.UseBackColor = false;
        this.xrTableCell71.StylePriority.UseBorders = false;
        this.xrTableCell71.StylePriority.UseTextAlignment = false;
        xrSummary18.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell71.Summary = xrSummary18;
        this.xrTableCell71.Text = "xrTableCell71";
        this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell71.Weight = 0.99999995646577977D;
        // 
        // xrTableCell72
        // 
        this.xrTableCell72.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.ALLSUMCOLUMN4_1", "{0:0.00%}")});
        this.xrTableCell72.Name = "xrTableCell72";
        this.xrTableCell72.StylePriority.UseBackColor = false;
        this.xrTableCell72.StylePriority.UseBorders = false;
        this.xrTableCell72.StylePriority.UseTextAlignment = false;
        xrSummary19.FormatString = "{0:0.00%}";
        xrSummary19.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell72.Summary = xrSummary19;
        this.xrTableCell72.Text = "xrTableCell72";
        this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell72.Weight = 0.99999995646577977D;
        // 
        // xrTableCell73
        // 
        this.xrTableCell73.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN5")});
        this.xrTableCell73.Name = "xrTableCell73";
        this.xrTableCell73.StylePriority.UseBackColor = false;
        this.xrTableCell73.StylePriority.UseBorders = false;
        this.xrTableCell73.StylePriority.UseTextAlignment = false;
        xrSummary20.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell73.Summary = xrSummary20;
        this.xrTableCell73.Text = "xrTableCell73";
        this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell73.Weight = 0.999999953286868D;
        // 
        // xrTableCell74
        // 
        this.xrTableCell74.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.ALLSUMCOLUMN5_1", "{0:0.00%}")});
        this.xrTableCell74.Name = "xrTableCell74";
        this.xrTableCell74.StylePriority.UseBackColor = false;
        this.xrTableCell74.StylePriority.UseBorders = false;
        this.xrTableCell74.StylePriority.UseTextAlignment = false;
        xrSummary21.FormatString = "{0:0.00%}";
        xrSummary21.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell74.Summary = xrSummary21;
        this.xrTableCell74.Text = "xrTableCell74";
        this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell74.Weight = 0.99999995249213924D;
        // 
        // xrTableCell75
        // 
        this.xrTableCell75.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN6")});
        this.xrTableCell75.Name = "xrTableCell75";
        this.xrTableCell75.StylePriority.UseBackColor = false;
        this.xrTableCell75.StylePriority.UseBorders = false;
        this.xrTableCell75.StylePriority.UseTextAlignment = false;
        xrSummary22.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell75.Summary = xrSummary22;
        this.xrTableCell75.Text = "xrTableCell75";
        this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell75.Weight = 0.99999995209477532D;
        // 
        // xrTableCell76
        // 
        this.xrTableCell76.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.ALLSUMCOLUMN6_1", "{0:0.00%}")});
        this.xrTableCell76.Name = "xrTableCell76";
        this.xrTableCell76.StylePriority.UseBackColor = false;
        this.xrTableCell76.StylePriority.UseBorders = false;
        this.xrTableCell76.StylePriority.UseTextAlignment = false;
        xrSummary23.FormatString = "{0:0.00%}";
        xrSummary23.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        this.xrTableCell76.Summary = xrSummary23;
        this.xrTableCell76.Text = "xrTableCell76";
        this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell76.Weight = 0.99999995189609292D;
        // 
        // xrTableCell77
        // 
        this.xrTableCell77.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN7")});
        this.xrTableCell77.Name = "xrTableCell77";
        this.xrTableCell77.StylePriority.UseBackColor = false;
        this.xrTableCell77.StylePriority.UseBorders = false;
        this.xrTableCell77.StylePriority.UseTextAlignment = false;
        xrSummary24.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell77.Summary = xrSummary24;
        this.xrTableCell77.Text = "xrTableCell77";
        this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell77.Weight = 0.9999999517967515D;
        // 
        // xrTableCell78
        // 
        this.xrTableCell78.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "COLUMN7_1")});
        this.xrTableCell78.Name = "xrTableCell78";
        this.xrTableCell78.StylePriority.UseBackColor = false;
        this.xrTableCell78.StylePriority.UseBorders = false;
        this.xrTableCell78.StylePriority.UseTextAlignment = false;
        xrSummary25.FormatString = "{0:0.00%}";
        xrSummary25.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary25.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell78.Summary = xrSummary25;
        this.xrTableCell78.Text = "xrTableCell78";
        this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell78.Weight = 0.999999951747081D;
        // 
        // xrTableCell79
        // 
        this.xrTableCell79.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN8")});
        this.xrTableCell79.Name = "xrTableCell79";
        this.xrTableCell79.StylePriority.UseBackColor = false;
        this.xrTableCell79.StylePriority.UseBorders = false;
        this.xrTableCell79.StylePriority.UseTextAlignment = false;
        xrSummary26.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell79.Summary = xrSummary26;
        this.xrTableCell79.Text = "xrTableCell79";
        this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell79.Weight = 0.99999995172224621D;
        // 
        // xrTableCell80
        // 
        this.xrTableCell80.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "COLUMN8_1")});
        this.xrTableCell80.Name = "xrTableCell80";
        this.xrTableCell80.StylePriority.UseBackColor = false;
        this.xrTableCell80.StylePriority.UseBorders = false;
        this.xrTableCell80.StylePriority.UseTextAlignment = false;
        xrSummary27.FormatString = "{0:0.00%}";
        xrSummary27.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary27.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell80.Summary = xrSummary27;
        this.xrTableCell80.Text = "xrTableCell80";
        this.xrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell80.Weight = 0.99999995170982836D;
        // 
        // xrTableCell81
        // 
        this.xrTableCell81.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataTable1.COLUMN9")});
        this.xrTableCell81.Name = "xrTableCell81";
        this.xrTableCell81.StylePriority.UseBackColor = false;
        this.xrTableCell81.StylePriority.UseBorders = false;
        this.xrTableCell81.StylePriority.UseTextAlignment = false;
        xrSummary28.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell81.Summary = xrSummary28;
        this.xrTableCell81.Text = "xrTableCell81";
        this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell81.Weight = 1.0000011724066855D;
        // 
        // xrTableCell82
        // 
        this.xrTableCell82.BackColor = System.Drawing.Color.Gold;
        this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                    | DevExpress.XtraPrinting.BorderSide.Right)
                    | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell82.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "COLUMN9_1")});
        this.xrTableCell82.Name = "xrTableCell82";
        this.xrTableCell82.StylePriority.UseBackColor = false;
        this.xrTableCell82.StylePriority.UseBorders = false;
        this.xrTableCell82.StylePriority.UseTextAlignment = false;
        xrSummary29.FormatString = "{0:0.00%}";
        xrSummary29.Func = DevExpress.XtraReports.UI.SummaryFunc.Avg;
        xrSummary29.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.xrTableCell82.Summary = xrSummary29;
        this.xrTableCell82.Text = "xrTableCell82";
        this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrTableCell82.Weight = 1.0000011724066855D;
        // 
        // dsDetailConfirmPlan1
        // 
        this.dsDetailConfirmPlan1.DataSetName = "dsDetailConfirmPlan";
        this.dsDetailConfirmPlan1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // SUMCOLUMN3_1
        // 
        this.SUMCOLUMN3_1.DataMember = "DataTable1";
        this.SUMCOLUMN3_1.Expression = " ToFloat([COLUMN3]) / ToFloat( [SUMCOLUMN3]) ";
        this.SUMCOLUMN3_1.FieldType = DevExpress.XtraReports.UI.FieldType.Float;
        this.SUMCOLUMN3_1.Name = "SUMCOLUMN3_1";
        // 
        // formattingRule1
        // 
        this.formattingRule1.DataMember = "DataTable1";
        this.formattingRule1.Name = "formattingRule1";
        // 
        // xrControlStyle1
        // 
        this.xrControlStyle1.Name = "xrControlStyle1";
        this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        // 
        // COLUMN4_1
        // 
        this.COLUMN4_1.DataMember = "DataTable1";
        this.COLUMN4_1.Expression = "Iif(IsNull([COLUMN3])  Or [COLUMN3]  ==  0 ,0  ,[COLUMN4] / [COLUMN3])";
        this.COLUMN4_1.Name = "COLUMN4_1";
        // 
        // COLUMN5_1
        // 
        this.COLUMN5_1.DataMember = "DataTable1";
        this.COLUMN5_1.Expression = "Iif(IsNull([COLUMN3])  Or [COLUMN3]  ==  0,0  ,[COLUMN5] / [COLUMN3])";
        this.COLUMN5_1.Name = "COLUMN5_1";
        // 
        // COLUMN6_1
        // 
        this.COLUMN6_1.DataMember = "DataTable1";
        this.COLUMN6_1.Expression = "Iif(IsNull([COLUMN4])   Or [COLUMN4]  ==  0,0  ,[COLUMN6] / [COLUMN4])";
        this.COLUMN6_1.Name = "COLUMN6_1";
        // 
        // COLUMN7_1
        // 
        this.COLUMN7_1.DataMember = "DataTable1";
        this.COLUMN7_1.Expression = "[COLUMN7] / [COLUMN6]";
        this.COLUMN7_1.Name = "COLUMN7_1";
        // 
        // COLUMN8_1
        // 
        this.COLUMN8_1.DataMember = "DataTable1";
        this.COLUMN8_1.Expression = "[COLUMN8] / [COLUMN6]";
        this.COLUMN8_1.Name = "COLUMN8_1";
        // 
        // COLUMN9_1
        // 
        this.COLUMN9_1.DataMember = "DataTable1";
        this.COLUMN9_1.Expression = "[COLUMN9] / [COLUMN7]";
        this.COLUMN9_1.Name = "COLUMN9_1";
        // 
        // dStart
        // 
        this.dStart.DataMember = "DataTable1";
        this.dStart.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
        this.dStart.Name = "dStart";
        // 
        // dEnd
        // 
        this.dEnd.DataMember = "DataTable1";
        this.dEnd.FieldType = DevExpress.XtraReports.UI.FieldType.DateTime;
        this.dEnd.Name = "dEnd";
        // 
        // SUMCOLUMN4_1
        // 
        this.SUMCOLUMN4_1.DataMember = "DataTable1";
        this.SUMCOLUMN4_1.Expression = "Iif(IsNull([SUMCOLUMN3])   Or [SUMCOLUMN3]  ==  0,0  , [SUMCOLUMN4]/[SUMCOLUMN3])" +
            "";
        this.SUMCOLUMN4_1.Name = "SUMCOLUMN4_1";
        // 
        // ALLSUMCOLUMN4_1
        // 
        this.ALLSUMCOLUMN4_1.DataMember = "DataTable1";
        this.ALLSUMCOLUMN4_1.Expression = "Iif(  IsNull([ALLSUMCOLUMN3]) Or [ALLSUMCOLUMN3] == 0,0  ,[ALLSUMCOLUMN4]/[ALLSUM" +
            "COLUMN3] )";
        this.ALLSUMCOLUMN4_1.Name = "ALLSUMCOLUMN4_1";
        // 
        // SUMCOLUMN5_1
        // 
        this.SUMCOLUMN5_1.DataMember = "DataTable1";
        this.SUMCOLUMN5_1.Expression = "Iif(IsNull([SUMCOLUMN3])   Or [SUMCOLUMN3]  ==  0,0  , [SUMCOLUMN5]/[SUMCOLUMN3])" +
            "\r\n";
        this.SUMCOLUMN5_1.Name = "SUMCOLUMN5_1";
        // 
        // SUMCOLUMN6_1
        // 
        this.SUMCOLUMN6_1.DataMember = "DataTable1";
        this.SUMCOLUMN6_1.Expression = "Iif(IsNull([SUMCOLUMN4])   Or [SUMCOLUMN4]  ==  0,  0,[SUMCOLUMN6]/[SUMCOLUMN4])\r" +
            "\n\r\n";
        this.SUMCOLUMN6_1.Name = "SUMCOLUMN6_1";
        // 
        // ALLSUMCOLUMN5_1
        // 
        this.ALLSUMCOLUMN5_1.DataMember = "DataTable1";
        this.ALLSUMCOLUMN5_1.Expression = "Iif(  IsNull([ALLSUMCOLUMN3]) Or [ALLSUMCOLUMN3] == 0,0  ,[ALLSUMCOLUMN5]/[ALLSUM" +
            "COLUMN3] )";
        this.ALLSUMCOLUMN5_1.Name = "ALLSUMCOLUMN5_1";
        // 
        // ALLSUMCOLUMN6_1
        // 
        this.ALLSUMCOLUMN6_1.DataMember = "DataTable1";
        this.ALLSUMCOLUMN6_1.Expression = "Iif(  IsNull([ALLSUMCOLUMN4]) Or [ALLSUMCOLUMN4] == 0,0  ,[ALLSUMCOLUMN6]/[ALLSUM" +
            "COLUMN4] )";
        this.ALLSUMCOLUMN6_1.Name = "ALLSUMCOLUMN6_1";
        // 
        // ALLSUMCOLUMN3_1
        // 
        this.ALLSUMCOLUMN3_1.DataMember = "DataTable1";
        this.ALLSUMCOLUMN3_1.Expression = "Iif( IsNull([ALLSUMCOLUMN3]) Or [ALLSUMCOLUMN3] == 0,0  ,[ALLSUMCOLUMN3]/[ALLSUMC" +
            "OLUMN3] )";
        this.ALLSUMCOLUMN3_1.Name = "ALLSUMCOLUMN3_1";
        // 
        // xtrDetailConfirmPlan
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.DetailReport});
        this.CalculatedFields.AddRange(new DevExpress.XtraReports.UI.CalculatedField[] {
            this.SUMCOLUMN3_1,
            this.COLUMN4_1,
            this.COLUMN5_1,
            this.COLUMN6_1,
            this.COLUMN7_1,
            this.COLUMN8_1,
            this.COLUMN9_1,
            this.dStart,
            this.dEnd,
            this.SUMCOLUMN4_1,
            this.ALLSUMCOLUMN4_1,
            this.SUMCOLUMN5_1,
            this.SUMCOLUMN6_1,
            this.ALLSUMCOLUMN5_1,
            this.ALLSUMCOLUMN6_1,
            this.ALLSUMCOLUMN3_1});
        this.DataMember = "DataTable1";
        this.DataSource = this.dsDetailConfirmPlan1;
        this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(22, 25, 100, 100);
        this.PageHeight = 1276;
        this.PageWidth = 3000;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsDetailConfirmPlan1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
