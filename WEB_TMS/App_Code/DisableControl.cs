﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class DisableControl
{
    public static void DisableControls(System.Web.UI.Control control)
    {
        try
        {
            foreach (System.Web.UI.Control c in control.Controls)
            {
                // Get the Enabled property by reflection.

                if (!string.Equals(c.ID, "cmdLogout") && !string.Equals(c.ID, "cmdModalClose") && !string.Equals(c.ID, "cmdModalSave"))
                {//ContentPlaceHolder1_dgvUploadFile_imgView_0
                    Type type = c.GetType();
                    System.Reflection.PropertyInfo prop = type.GetProperty("Enabled");

                    // Set it to False to disable the control.
                    if (prop != null)
                        prop.SetValue(c, false, null);
                }

                // Recurse into child controls.
                if (c.Controls.Count > 0)
                    DisableControls(c);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public static void EnableControls(System.Web.UI.Control control)
    {
        foreach (System.Web.UI.Control c in control.Controls)
        {
            // Get the Enabled property by reflection.
            Type type = c.GetType();
            System.Reflection.PropertyInfo prop = type.GetProperty("Enabled");

            // Set it to False to disable the control.
            if (prop != null)
                prop.SetValue(c, true, null);

            // Recurse into child controls.
            if (c.Controls.Count > 0)
                EnableControls(c);
        }
    }
}