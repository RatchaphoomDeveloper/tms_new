using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for TREDUCEPOINT
/// </summary>
public class TREDUCEPOINT
{
    public static OracleConnection conn;
    public static OracleTransaction oTransaction;
    public static Page page;
    //public TREDUCEPOINT() { }
    public TREDUCEPOINT(Page _page, OracleConnection _conn) { page = _page; conn = _conn; }
    public TREDUCEPOINT(Page _page, OracleConnection _conn, OracleTransaction _oTransaction) { page = _page; conn = _conn; oTransaction = _oTransaction; }
    public TREDUCEPOINT(OracleConnection _conn, OracleTransaction _oTransaction) { conn = _conn; oTransaction = _oTransaction; }
    #region Data
    private string fNREDUCEID;
    private string fDREDUCE;
    private string fSREDUCETYPE;
    private string fSPROCESSID;
    private string fSCHECKLISTID;
    private string fSTOPICID;
    private string fSVERSIONLIST;
    private string fSREDUCENAME;
    private string fNPOINT;
    private string fSREDUCEBY;
    private string fCACTIVE;

    private string fSREFERENCEID;
    private string fSCONTRACTID;
    private string fSHEADID;
    private string fSHEADREGISTERNO;
    private string fSTRAILERID;
    private string fSTRAILERREGISTERNO;
    private string fSDELIVERYNO;

    #endregion
    #region Property
    public string NREDUCEID
    {
        get { return this.fNREDUCEID; }
        set { this.fNREDUCEID = value; }
    }
    public string DREDUCE
    {
        get { return this.fDREDUCE; }
        set { this.fDREDUCE = value; }
    }
    public string SREDUCETYPE
    {
        get { return this.fSREDUCETYPE; }
        set { this.fSREDUCETYPE = value; }
    }
    public string SPROCESSID
    {
        get { return this.fSPROCESSID; }
        set { this.fSPROCESSID = value; }
    }
    public string SCHECKLISTID
    {
        get { return this.fSCHECKLISTID; }
        set { this.fSCHECKLISTID = value; }
    }
    public string STOPICID
    {
        get { return this.fSTOPICID; }
        set { this.fSTOPICID = value; }
    }
    public string SVERSIONLIST
    {
        get { return this.fSVERSIONLIST; }
        set { this.fSVERSIONLIST = value; }
    }
    public string SREDUCENAME
    {
        get { return this.fSREDUCENAME; }
        set { this.fSREDUCENAME = value; }
    }
    public string NPOINT
    {
        get { return this.fNPOINT; }
        set { this.fNPOINT = value; }
    }
    public string SREDUCEBY
    {
        get { return this.fSREDUCEBY; }
        set { this.fSREDUCEBY = value; }
    }
    public string CACTIVE
    {
        get { return this.fCACTIVE; }
        set { this.fCACTIVE = value; }
    }
    public string SREFERENCEID
    {
        get { return this.fSREFERENCEID; }
        set { this.fSREFERENCEID = value; }
    }
    public string SCONTRACTID
    {
        get { return this.fSCONTRACTID; }
        set { this.fSCONTRACTID = value; }
    }
    public string SHEADID
    {
        get { return this.fSHEADID; }
        set { this.fSHEADID = value; }
    }
    public string SHEADREGISTERNO
    {
        get { return this.fSHEADREGISTERNO; }
        set { this.fSHEADREGISTERNO = value; }
    }
    public string STRAILERID
    {
        get { return this.fSTRAILERID; }
        set { this.fSTRAILERID = value; }
    }
    public string STRAILERREGISTERNO
    {
        get { return this.fSTRAILERREGISTERNO; }
        set { this.fSTRAILERREGISTERNO = value; }
    }
    public string SDELIVERYNO
    {
        get { return this.fSDELIVERYNO; }
        set { this.fSDELIVERYNO = value; }
    }
    #endregion
    #region Method
    public int Insert()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("IUTREDUCEPOINT", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("IUTREDUCEPOINT", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("N_REDUCEID", NREDUCEID);
            cmd.Parameters.AddWithValue("D_REDUCE", DREDUCE);
            cmd.Parameters.AddWithValue("S_REDUCETYPE", SREDUCETYPE);
            cmd.Parameters.AddWithValue("S_PROCESSID", SPROCESSID);
            cmd.Parameters.AddWithValue("S_CHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("S_TOPICID", STOPICID);
            cmd.Parameters.AddWithValue("S_VERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("S_REDUCENAME", SREDUCENAME);
            cmd.Parameters.AddWithValue("N_POINT", NPOINT);
            cmd.Parameters.AddWithValue("S_REDUCEBY", SREDUCEBY);
            cmd.Parameters.AddWithValue("C_ACTIVE", CACTIVE);
            cmd.Parameters.AddWithValue("S_REFERENCEID", SREFERENCEID);
            cmd.Parameters.AddWithValue("S_CONTRACTID", SCONTRACTID);
            cmd.Parameters.AddWithValue("S_HEADID", SHEADID);
            cmd.Parameters.AddWithValue("S_HEADREGISTERNO", SHEADREGISTERNO);
            cmd.Parameters.AddWithValue("S_TRAILERID", STRAILERID);
            cmd.Parameters.AddWithValue("S_TRAILERREGISTERNO", STRAILERREGISTERNO);
            cmd.Parameters.AddWithValue("S_DELIVERYNO", SDELIVERYNO);

            OracleParameter para = new OracleParameter();
            para.ParameterName = "REDUCEID";
            para.OracleType = OracleType.Number;
            para.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(para);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
            throw new Exception(err.Message);
        }
        finally
        {

        }
        return result;
    }

    public int Insert(string sp_mode)
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand(sp_mode, conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand(sp_mode, conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("N_REDUCEID", NREDUCEID);
            cmd.Parameters.AddWithValue("D_REDUCE", DREDUCE);
            cmd.Parameters.AddWithValue("S_REDUCETYPE", SREDUCETYPE);
            cmd.Parameters.AddWithValue("S_PROCESSID", SPROCESSID);
            cmd.Parameters.AddWithValue("S_CHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("S_TOPICID", STOPICID);
            cmd.Parameters.AddWithValue("S_VERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("S_REDUCENAME", SREDUCENAME);
            cmd.Parameters.AddWithValue("N_POINT", NPOINT);
            cmd.Parameters.AddWithValue("S_REDUCEBY", SREDUCEBY);
            cmd.Parameters.AddWithValue("C_ACTIVE", CACTIVE);
            cmd.Parameters.AddWithValue("S_REFERENCEID", SREFERENCEID);
            cmd.Parameters.AddWithValue("S_CONTRACTID", SCONTRACTID);
            cmd.Parameters.AddWithValue("S_HEADID", SHEADID);
            cmd.Parameters.AddWithValue("S_HEADREGISTERNO", SHEADREGISTERNO);
            cmd.Parameters.AddWithValue("S_TRAILERID", STRAILERID);
            cmd.Parameters.AddWithValue("S_TRAILERREGISTERNO", STRAILERREGISTERNO);
            cmd.Parameters.AddWithValue("S_DELIVERYNO", SDELIVERYNO);

            OracleParameter para = new OracleParameter();
            para.ParameterName = "REDUCEID";
            para.OracleType = OracleType.Number;
            para.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(para);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public int Update()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("UTREDUCEPOINT", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("UTREDUCEPOINT", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@NREDUCEID", NREDUCEID);
            cmd.Parameters.AddWithValue("@DREDUCE", DREDUCE);
            cmd.Parameters.AddWithValue("@SREDUCETYPE", SREDUCETYPE);
            cmd.Parameters.AddWithValue("@SPROCESSID", SPROCESSID);
            cmd.Parameters.AddWithValue("@SCHECKLISTID", SCHECKLISTID);
            cmd.Parameters.AddWithValue("@STOPICID", STOPICID);
            cmd.Parameters.AddWithValue("@SVERSIONLIST", SVERSIONLIST);
            cmd.Parameters.AddWithValue("@SREDUCENAME", SREDUCENAME);
            cmd.Parameters.AddWithValue("@NPOINT", NPOINT);
            cmd.Parameters.AddWithValue("@SREDUCEBY", SREDUCEBY);
            cmd.Parameters.AddWithValue("@CACTIVE", CACTIVE);

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public int Delete()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            OracleCommand cmd = null;
            if (oTransaction != null)
            {
                cmd = new OracleCommand("DTREDUCEPOINT", conn, oTransaction);
            }
            else
            {
                cmd = new OracleCommand("DTREDUCEPOINT", conn);
            }
            cmd.CommandType = CommandType.StoredProcedure;

            result = cmd.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    public bool Open()
    {
        string sqlstr;
        bool result = false;
        try
        {
            // Reopen connection if close
            if (conn.State == ConnectionState.Closed) conn.Open();
            //
            sqlstr = "SELECT * FROM TREDUCEPOINT ";
            DataTable dt = new DataTable();
            new OracleDataAdapter(sqlstr, conn).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                NREDUCEID = row["NREDUCEID"].ToString();
                DREDUCE = row["DREDUCE"].ToString();
                SREDUCETYPE = row["SREDUCETYPE"].ToString();
                SPROCESSID = row["SPROCESSID"].ToString();
                SCHECKLISTID = row["SCHECKLISTID"].ToString();
                STOPICID = row["STOPICID"].ToString();
                SVERSIONLIST = row["SVERSIONLIST"].ToString();
                SREDUCENAME = row["SREDUCENAME"].ToString();
                NPOINT = row["NPOINT"].ToString();
                SREDUCEBY = row["SREDUCEBY"].ToString();
                CACTIVE = row["CACTIVE"].ToString();
                dt.Dispose();
                result = true;
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
    #endregion
}
