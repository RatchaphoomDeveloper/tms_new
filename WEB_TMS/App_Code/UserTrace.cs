﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;

/// <summary>
/// Summary description for UserTrace
/// </summary>
public class UserTrace
{
    public static string sconn;
    public static Page page;

    public UserTrace(Page _page, string _conn) { page = _page; sconn = _conn; }

    public UserTrace(WebServiceTMS webServiceTMS, string sql)
    {
        // TODO: Complete member initialization
        this.webServiceTMS = webServiceTMS;
        sconn = sql;
    }
    private string fUID;
    private string fMENUID;
    private string fDCREATE;
    private string fSCREATE;
    private string fSTYPE;
    private string fSDESCRIPTION;
    private string fSREFERENTID;
    private WebServiceTMS webServiceTMS;
    private string sql;

    public string SUID
    {
        get { return this.fUID; }
        set { this.fUID = value; }
    }

    public string SMENUID
    {
        get { return this.fMENUID; }
        set { this.fMENUID = value; }
    }

    public string DCREATE
    {
        get { return this.fDCREATE; }
        set { this.fDCREATE = value; }
    }

    public string SCREATE
    {
        get { return this.fSCREATE; }
        set { this.fSCREATE = value; }
    }

    public string STYPE
    {
        get { return this.fSTYPE; }
        set { this.fSTYPE = value; }
    }

    public string SDESCRIPTION
    {
        get { return this.fSDESCRIPTION; }
        set { this.fSDESCRIPTION = value; }
    }

    public string SREFERENTID
    {
        get { return this.fSREFERENTID; }
        set { this.fSREFERENTID = value; }
    }

    public int Insert()
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            using (OracleConnection conn = new OracleConnection(sconn))
            {
                if (conn.State == ConnectionState.Closed) conn.Open();
                string sql = "INSERT INTO TUSERTRACE(NID,SUID,SMENUID,SCREATE,DCREATE,STYPE,SDESCRIPTION,SREFERENTID) VALUES (FC_GENID_TUSERTRACE(0),:SUID,:SMENUID,:SCREATE,sysdate,:STYPE,:SDESCRIPTION,:SREFERENTID)";
                using (OracleCommand cmd = new OracleCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue(":SUID", SUID);
                    cmd.Parameters.AddWithValue(":SMENUID", SMENUID);
                    cmd.Parameters.AddWithValue(":SCREATE", SCREATE);
                    cmd.Parameters.AddWithValue(":STYPE", STYPE);
                    cmd.Parameters.AddWithValue(":SDESCRIPTION", SDESCRIPTION);
                    cmd.Parameters.AddWithValue(":SREFERENTID", SREFERENTID);
                    result = cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
            }
        }
        finally
        {

        }
        return result;
    }
}