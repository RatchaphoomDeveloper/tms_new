﻿using System;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utility
{
    /// <summary>
    /// Utilities สำหรับจัดการ Data Time และ Formatting
    /// Add BY ORN.K
    /// </summary>
    public static class DateTimeHelpers
    {
        public const int SECOND = 1;
        public const int MINUTE = 60 * SECOND;
        public const int WORKING_HOUR = 8;

        private static Nullable<DateTime> nullDateTime = new Nullable<DateTime>();

        public class Resources
        {
            // Get message from web resource
            public static string GetMessage(string resourceKey)
            {
                string message = string.Empty;

                try
                {
                    switch (resourceKey)
                    {
                        case "FormatDate": message = "dd/MM/yyyy";
                            break;
                        case "FormatDateTime": message = "dd/MM/yyyy";
                            break;
                        default:
                            break;
                    }
                    //message = (String)System.Web.HttpContext.GetGlobalResourceObject("SiteResources", resourceKey);
                }
                catch
                {
                    message = "Could not find global resource.";
                }

                return message;
            }
        }

        public class Cultural
        {
            public static IFormatProvider GetThCulture()
            {
                return new CultureInfo("th-TH", true);
            }

            public static IFormatProvider GetUsCulture()
            {
                return new CultureInfo("es-US", true);
            }
        }

        public static DateTime? ParseDateTime(string strDate)
        {
            DateTime date;

            bool no_error = DateTime.TryParseExact(strDate,
                                                   Resources.GetMessage("FormatDate"),
                                                   Cultural.GetUsCulture(),
                                                   DateTimeStyles.NoCurrentDateDefault,
                                                   out date);

            return no_error ? date : nullDateTime;
        }

        public static String FormatDate(DateTime? date)
        {
            string result = null;

            if (date.HasValue)
            {
                result = date.Value.ToString(Resources.GetMessage("FormatDate"));
            }

            return result;
        }
    }
}
