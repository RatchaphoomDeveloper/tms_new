﻿using System;
using System.Data;

namespace Application.Helper
{
    public static class ConfigValueHelper
    {
        public static DataTable GetDefaultStatus()
        {
            try
            {
                DataTable dtStatus = new DataTable();
                dtStatus.Columns.Add("StatusID");
                dtStatus.Columns.Add("StatusName");

                dtStatus.Rows.Add("1", "ใช้งาน");
                dtStatus.Rows.Add("0", "ไม่ใช้งาน");

                return dtStatus;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}