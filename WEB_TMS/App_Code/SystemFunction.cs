﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Web.Configuration;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Web.UI.WebControls;
using TMS_BLL.Transaction.Complain;

/// <summary>
/// Summary description for SystemFunction
/// </summary>
public class SystemFunction
{
    public static string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    public SystemFunction()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string genID_TBL_REQUEST(string sServiceType) //เลขที่การขอรับบริการ Format[ปีคศ.+เดือน/รหัสประเภทบริการ-AutoGen4หลัก] =>>> 1405/01-0001
    {


        DataTable dtGenID_TBL_REQUEST = ComplainBLL.Instance.dtGenID_TBL_REQUEST_TMP(sServiceType);

        //string sResut = "";
        //DataTable dt = new DataTable();
        DataRow dr = null;
        dr = getDataRow(dtGenID_TBL_REQUEST);
        string _REQUEST_ID = dr["REQUEST_ID_F"].ToString();
        //string _sYeartNow = DateTime.Now.ToString("yy", new System.Globalization.CultureInfo("en-US"));
        //string _sMonthNow = DateTime.Now.ToString("MM", new System.Globalization.CultureInfo("en-US"));
        //int nTemp1 = 0;

        //// 0 = รหัสประเภทบริการ
        //string sql = @"SELECT REQUEST_ID,SUBSTR(REQUEST_ID,0,2) AS SYEAR,SUBSTR(REQUEST_ID,3,2) AS SMONTH,SUBSTR(REQUEST_ID,9,4) AS NORDER 
        //               FROM  TBL_REQUEST WHERE REQTYPE_ID = '{0}' ORDER BY SYEAR DESC,SMONTH DESC,NORDER DESC";
        //dt = CommonFunction.Get_Data(strConn, string.Format(sql, CommonFunction.ReplaceInjection(sServiceType)));
        //if (dt.Rows.Count > 0)
        //{
        //    dr = getDataRow(dt);
        //    if ((dr["SYEAR"] + "") == _sYeartNow && (dr["SMONTH"] + "") == _sMonthNow)
        //    {
        //        nTemp1 = int.TryParse(dr["NORDER"] + "", out nTemp1) ? nTemp1 : 0;

        //        nTemp1 = nTemp1 + 1;

        //        sResut = (dr["SYEAR"] + "") + (dr["SMONTH"] + "") + "/" + sServiceType + "-" + ConverFormat(nTemp1 + "", 4);

        //    }
        //    else
        //    {
        //        nTemp1 = 1;
        //        sResut = _sYeartNow + _sMonthNow + "/" + sServiceType + "-" + ConverFormat(nTemp1 + "", 4);
        //    }
        //}
        //else
        //{
        //    nTemp1 = 1;
        //    sResut = _sYeartNow + _sMonthNow + "/" + sServiceType + "-" + ConverFormat(nTemp1 + "", 4);
        //}


        return _REQUEST_ID;
    }

    public static string ConverFormat(string str, int nLength)
    {
        string sResut = "";
        int nStrLength = str.Length;
        int devLength = nLength - nStrLength;
        for (int i = 0; i < devLength; i++)
        {
            sResut += "0";
        }

        return sResut + str;
    }

    public static DataRow getDataRow(DataTable _dt)
    {
        DataRow dr = null;
        if (_dt.Rows.Count > 0)
        {
            dr = _dt.Rows[0];
        }

        return dr;
    }

    public static void AddHistoryRQ(string _STATUSREQ_ID, string sRemark)
    {

    }

    public static string ReplaceNameFileUpload(string filename)
    {
        string[] _blacklist = { "  ", "%", "/", "'", "?", "*", ".." };

        string strRep = filename;
        if (strRep == null || strRep.Trim().Equals(String.Empty))
            return strRep;
        foreach (string _blk in _blacklist) { strRep = strRep.Replace(_blk, ""); }

        return strRep;
    }

    public static void SQLExecuteNonQuery(string _strConn, string _strSQL)
    {
        using (OracleConnection con = new OracleConnection(_strConn))
        {
            con.Open();

            using (OracleCommand com = new OracleCommand(_strSQL, con))
            {
                com.ExecuteNonQuery();
            }
        }
    }

    public static void ClearFileInTemp(string sPathServer, string sPathFile, string FileType) // ลบไฟล์ที่เคยอัพมาแล้ว ไม่ได้ลบออกจากโฟล์เดอร์ Temp
    {
        try
        {
            DateTime DateNow = DateTime.Now;

            DirectoryInfo d = new DirectoryInfo(sPathServer + sPathFile.Replace("/", "\\"));
            string sType = "";
            string[] ArrFileType = FileType.Split(',');
            foreach (string stype in ArrFileType)
            {
                sType = stype.Replace("*", "").Replace(".", "");
                FileInfo[] Files = d.GetFiles("*." + sType); //, SearchOption.AllDirectories

                foreach (FileInfo file in Files)
                {
                    DateTime dTimeFile = file.CreationTime;

                    if (dTimeFile.Date < DateNow.Date)
                    {
                        try
                        {
                            if (File.Exists(sPathServer + sPathFile.Replace("/", "\\") + file.Name))
                            {
                                File.Delete(sPathServer + sPathFile.Replace("/", "\\") + file.Name);
                            }
                        }
                        catch { }

                    }
                }
            }
        }
        catch { }
    }

    //เช็คว่ารถมีในใบรายการ Request ไหม ถ้ามีให้เด้งกลับไปหน้าลิส
    public static bool CheckCar(string head, string tail)
    {
        bool sRetuen = true;
        string codition = "";

        if (!string.IsNullOrEmpty(head))
        {
            codition += " AND VEH_ID = '" + CommonFunction.ReplaceInjection(head) + "'";
        }

        if (!string.IsNullOrEmpty(tail))
        {
            codition += " AND TU_ID = '" + CommonFunction.ReplaceInjection(tail) + "'";
        }


        string chk = @"SELECT  VEH_ID, TU_ID FROM TBL_REQUEST WHERE STATUS_FLAG = '09' " + codition + "";
        DataTable dt = CommonFunction.Get_Data(strConn, chk);

        if (dt.Rows.Count > 0)
        {
            sRetuen = false;
        }
        else
        {

        }

        return sRetuen;
    }

    public static string Check_SCHASIS(string STRUCKID)
    {
        string sRetuen = "";


        string chk = @"SELECT HEAD.STRUCKID,TAIL.STRUCKID as TAILSTRUCKID ,HEAD.SCARTYPEID,HEAD.SCHASIS ,TAIL.TAILSCHASIS,HEAD.SHEADREGISTERNO,TAIL.TAILSHEADREGISTERNO
FROM TTRUCK HEAD
LEFT JOIN 
(
    SELECT STRUCKID,SCARTYPEID  as Tail,SCHASIS as TAILSCHASIS,SHEADREGISTERNO as TAILSHEADREGISTERNO
    FROM TTRUCK
)TAIL
ON HEAD.STRAILERID = TAIL.STRUCKID
WHERE HEAD.SCARTYPEID in ('0','3') AND HEAD.STRUCKID = '" + STRUCKID + "'";
        DataTable dt = CommonFunction.Get_Data(strConn, chk);

        if (dt.Rows.Count > 0)
        {

            if (dt.Rows[0]["SCARTYPEID"] + "" == "0")
            {
                if (string.IsNullOrEmpty(dt.Rows[0]["SCHASIS"] + ""))
                {
                    sRetuen = "เนื่องจากรถทะเบียน " + dt.Rows[0]["SHEADREGISTERNO"] + " ไม่มีเลข SCHASIS ในระบบไม่สามารถดำเนินการได้";
                }
            }
            else
            {
                //กรณีหัวหรือหางไม่มีค่าว่าง
                if (!string.IsNullOrEmpty(dt.Rows[0]["SCHASIS"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["TAILSCHASIS"] + ""))
                {

                }
                //กรณีหัวหรือหางตัวใดตัวหนึ่งมีค่าว่าง
                else if (!string.IsNullOrEmpty(dt.Rows[0]["SCHASIS"] + "") || !string.IsNullOrEmpty(dt.Rows[0]["TAILSCHASIS"] + ""))
                {
                    if (string.IsNullOrEmpty(dt.Rows[0]["SCHASIS"] + ""))
                    {
                        sRetuen = "เนื่องจากรถทะเบียน " + dt.Rows[0]["SHEADREGISTERNO"] + " ไม่มีเลข SCHASIS ในระบบไม่สามารถดำเนินการได้";
                    }
                    else
                    {
                        sRetuen = "เนื่องจากรถทะเบียน " + dt.Rows[0]["TAILSHEADREGISTERNO"] + " ไม่มีเลข SCHASIS ในระบบไม่สามารถดำเนินการได้";
                    }

                }
                //กรณีหัวและหางเป็นค่าว่าง
                else
                {
                    sRetuen = "เนื่องจากรถทะเบียน " + dt.Rows[0]["SHEADREGISTERNO"] + " และ " + dt.Rows[0]["TAILSHEADREGISTERNO"] + " ไม่มีเลข SCHASIS ในระบบไม่สามารถดำเนินการได้";
                }

            }
        }
        else
        {

        }

        return sRetuen;
    }

    //เก็บข้อมูลการทำ Flow 
    // REPORT2VEND = มีการตอบสนองกับ user หรือไม่ เช่น มีการส่งเมล์
    // REMARKS = กรณี รข. จะมีการใส่หมายเหตุ, กรณี user ใช้คำว่า ส่งคำขอ
    // REMARK_STEP = STATUSREQ_ID
    // REMARK_STATUS = S คือ ส่งคำขอ, A = Approve , E = ปรับแก้ไข, C = Cacel

    public static void Add_To_TBL_REQREMARK(string REQUEST_ID, string REPORT2VEND, string REMARKS, string REMARK_STEP, string REMARK_STATUS, string REMARK_BY, string SDESCRIPTION, string USERDescription, string POSITION)
    {
        string Query = @"INSERT INTO TBL_REQREMARK (REQUEST_ID, REPORT2VEND, REMARKS, REMARK_STEP, REMARK_STATUS, REMARK_ID, REMARK_DATE, REMARK_BY,SDESCRIPTION,USERDESCRIPTION,POSITION) 
                                VALUES ( '{0}','{1}','{2}','{3}','{4}',{5},sysdate,'{6}','{7}','{8}','{9}')";

        string ID = "SELECT REMARK_ID FROM (SELECT REMARK_ID FROM TBL_REQREMARK ORDER BY REMARK_ID DESC) WHERE ROWNUM <= 1";

        DataTable dt = CommonFunction.Get_Data(strConn, ID);

        string sID = "";
        if (dt.Rows.Count > 0)
        {
            sID = (int.Parse(dt.Rows[0]["REMARK_ID"] + "") + 1) + "";
        }
        else
        {
            sID = "1";
        }


        string InsertRemark = string.Format(Query, REQUEST_ID, REPORT2VEND, REMARKS, REMARK_STEP, REMARK_STATUS, sID, REMARK_BY, SDESCRIPTION, USERDescription, POSITION);
        using (OracleConnection con = new OracleConnection(strConn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (OracleCommand com = new OracleCommand(InsertRemark, con))
            {
                com.Parameters.Clear();
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }

    public static DataTable List_TBL_REQREMARK(string Req_ID, string UserID)
    {
        DataTable dt = new DataTable();
        //หาไอดีของบริษัทพนักงาน
        //        string VenID = @"SELECT SUID, SVENDORID, CACTIVE FROM TUSER WHERE SUID = '" + CommonFunction.ReplaceInjection(UserID) + "'";
        //        DataTable dtvenID = CommonFunction.Get_Data(strConn, VenID);
        //        string scondition = "";
        //        string strSQLCondition = ""; // ผู้ประกอบการ
        //        if (dtvenID.Rows.Count > 0)
        //        {
        //            strSQLCondition = " AND REQ.VENDOR_ID = '" + CommonFunction.ReplaceInjection(dtvenID.Rows[0]["SVENDORID"] + "") + "'"; // ผู้ประกอบการ
        //            string sql = @"SELECT TBL_REQREMARK.REMARK_ID, TBL_REQREMARK.REQUEST_ID, TBL_REQREMARK.REMARK_DATE, 
        //                       TBL_REQREMARK.REMARK_STEP,TBL_REQREMARK.SDESCRIPTION, TBL_REQREMARK.REMARK_STATUS, 
        //                       TBL_REQREMARK.REMARK_BY, TBL_REQREMARK.REPORT2VEND,TBL_STATUSREQ.STATUSREQ_NAME,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME as sName,
        //                       TVEN.SVENDORID, NVL(TBL_REQREMARK.REMARKS,TBL_REQREMARK.USERDESCRIPTION) as REMARKS,TBL_REQREMARK.USERDESCRIPTION,TBL_REQREMARK.REMARKS as REMARKORIGINAL
        //                    FROM TBL_REQREMARK
        //                    LEFT JOIN TBL_STATUSREQ
        //                    ON TBL_STATUSREQ.STATUSREQ_ID = TBL_REQREMARK.REMARK_STEP
        //                    LEFT JOIN TUSER
        //                    ON TUSER.SUID = TBL_REQREMARK.REMARK_BY
        //                    LEFT JOIN TBL_REQUEST REQ
        //                    ON REQ.REQUEST_ID = TBL_REQREMARK.REQUEST_ID
        //                    LEFT JOIN TVENDOR TVEN
        //                    ON TUSER.SVENDORID = TVEN.SVENDORID
        //                    WHERE TBL_REQREMARK.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'
        //                    " + scondition + @"
        //                    " + strSQLCondition + @"
        //                    AND TBL_REQREMARK.REMARK_BY='" + CommonFunction.ReplaceInjection(UserID) + @"'
        //                    ORDER BY TBL_REQREMARK.REMARK_DATE DESC";

        //            dt = CommonFunction.Get_Data(strConn, sql);
        //        }
        //        else
        //        {

        if (!string.IsNullOrEmpty(UserID))
        {
            //เอาคำขอของ User ขึ้นก่อน
            string sql = @"SELECT TBL_REQREMARK.REMARK_ID, TBL_REQREMARK.REQUEST_ID, TBL_REQREMARK.REMARK_DATE, 
                       TBL_REQREMARK.REMARK_STEP,TBL_REQREMARK.SDESCRIPTION, TBL_REQREMARK.REMARK_STATUS, 
                       TBL_REQREMARK.REMARK_BY, TBL_REQREMARK.REPORT2VEND,TBL_STATUSREQ.STATUSREQ_NAME
                    ,CASE 
                    WHEN POSITION = 'Y' THEN 'เจ้าหน้าที่ รข.'
                    WHEN POSITION = 'M' THEN 'เจ้าหน้าที่ มว.'
                    ELSE TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME END  as sName,
                    REQ.VENDOR_ID, NVL(TBL_REQREMARK.USERDESCRIPTION,TBL_REQREMARK.REMARKS) as REMARKS,TBL_REQREMARK.USERDESCRIPTION,TBL_REQREMARK.REMARKS as REMARKORIGINAL
                    FROM TBL_REQREMARK
                    LEFT JOIN TBL_STATUSREQ
                    ON TBL_STATUSREQ.STATUSREQ_ID = TBL_REQREMARK.REMARK_STEP
                    LEFT JOIN TUSER
                    ON TUSER.SUID = TBL_REQREMARK.REMARK_BY
                    LEFT JOIN TBL_REQUEST REQ
                    ON REQ.REQUEST_ID = TBL_REQREMARK.REQUEST_ID
                    WHERE TBL_REQREMARK.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'
                    ORDER BY TBL_REQREMARK.REMARK_DATE DESC";

            dt = CommonFunction.Get_Data(strConn, sql);
        }
        else
        {
            string sql = @"SELECT TBL_REQREMARK.REMARK_ID, TBL_REQREMARK.REQUEST_ID, TBL_REQREMARK.REMARK_DATE, 
                       TBL_REQREMARK.REMARK_STEP,TBL_REQREMARK.SDESCRIPTION, TBL_REQREMARK.REMARK_STATUS, 
                       TBL_REQREMARK.REMARK_BY, TBL_REQREMARK.REPORT2VEND,TBL_STATUSREQ.STATUSREQ_NAME,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME as sName,
                       REQ.VENDOR_ID, NVL(TBL_REQREMARK.REMARKS,TBL_REQREMARK.USERDESCRIPTION) as REMARKS,TBL_REQREMARK.USERDESCRIPTION,TBL_REQREMARK.REMARKS as REMARKORIGINAL
                    FROM TBL_REQREMARK
                    LEFT JOIN TBL_STATUSREQ
                    ON TBL_STATUSREQ.STATUSREQ_ID = TBL_REQREMARK.REMARK_STEP
                    LEFT JOIN TUSER
                    ON TUSER.SUID = TBL_REQREMARK.REMARK_BY
                    LEFT JOIN TBL_REQUEST REQ
                    ON REQ.REQUEST_ID = TBL_REQREMARK.REQUEST_ID
                    WHERE TBL_REQREMARK.REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'
                    ORDER BY TBL_REQREMARK.REMARK_DATE DESC";

            dt = CommonFunction.Get_Data(strConn, sql);
        }
        //}
        return dt;
    }

    public static DataTable List_SERVICE(string Req_ID, string UserID)
    {
        DataTable dt = new DataTable();
        //หาไอดีของบริษัทพนักงาน
        //string VenID = @"SELECT SUID, SVENDORID, CACTIVE FROM TUSER WHERE SUID = '" + CommonFunction.ReplaceInjection(UserID) + "'";
        //DataTable dtvenID = CommonFunction.Get_Data(strConn, VenID);
        string ID = CommonFunction.ReplaceInjection(Req_ID);

        //if (dtvenID.Rows.Count > 0)
        //{

        string sql = @"SELECT  ITEM.REQUEST_ID,ITEM.SERVICE_ID,
CASE WHEN NVL(MS.UNIT,'xxx') <> 'xxx' THEN ITEM.NITEM ELSE null END as NITEM, 
   TO_CHAR(ITEM.NPRICE,'999,999,999') as NPRICE, ITEM.FLAGE_SERVICECANCEL,MS.SERVICE_NAME,MS.UNIT,MS.UNITSERVICE
FROM TBL_REQUEST_ITEM ITEM
LEFT JOIN TBL_SERVICE_MASTERDATA MS
ON MS.SERVICE_ID = ITEM.SERVICE_ID
Where REQUEST_ID = '" + ID + @"'
UNION 
SELECT  ITEM.REQUEST_ID,'Total' , null, 
 TO_CHAR(SUM(ITEM.NPRICE),'999,999,999') as NPRICE, '' as FLAGE_SERVICECANCEL,'รวมค่าบริการ' ,'','บาท'
FROM TBL_REQUEST_ITEM ITEM
Where REQUEST_ID = '" + ID + @"'
GROUP BY  ITEM.REQUEST_ID,'Total',0,'','รวมค่าบริการ' ";

        dt = CommonFunction.Get_Data(strConn, sql);
        //}
        return dt;
    }

    public static string GetRemark_WorkFlowRequest(int nValue)
    {
        string sResult = "";
        switch (nValue)
        {
            case 1: sResult = "ยังไม่แนบใบชำระเงิน"; break;
            case 2: sResult = "แนบเอกสารกำกับคำขอ"; break;
            case 3: sResult = "เอกสารไม่สมบรูณ์"; break;
            case 4: sResult = "ยังไม่แนบใบชำระเงิน"; break;
            case 5: sResult = "มว. ตรวจสภาพรถไม่ผ่านเกณฑ์"; break;
            case 6: sResult = "มว. ตรวจสภาพรถผ่านเกณฑ์"; break;
            case 7: sResult = "มว. ตรวจสรุปผลเทียบแป้นไม่ผ่าน"; break;
            case 8: sResult = "มว. ตรวจสรุปผลเทียบแป้นผ่าน"; break;
            case 9: sResult = "มว. ยืนยันคิวนัดหมาย"; break;
            case 10: sResult = "แก้ไขคำขอที่ รข. แจ้งปรับแก้ไข"; break;
            case 11: sResult = "แก้ไขคำขอที่ มว. แจ้งรถไม่ได้มาตรฐาน"; break;
            case 12: sResult = "แนบเอกสารปิดงาน"; break;
        }

        return sResult;
    }

    public static string GetDesc_WorkFlowRequest(int nValue)
    {
        string sResult = "";
        switch (nValue)
        {
            case 1: sResult = "บันทึกคำขอ"; break;
            case 2: sResult = "ส่งคำขอ"; break;
            case 3: sResult = "รข. อนุมัติคำขอ"; break;
            case 4: sResult = "รข. แจ้งปรับแก้ไข"; break;
            case 5: sResult = "รข. ยกเลิกคำขอ"; break;
            case 6: sResult = "มว. บันทึกคำขอ"; break;
            case 7: sResult = "มว. ส่งคำขอ"; break;
            case 8: sResult = "รข. บันทึกคำขอ"; break;
            case 9: sResult = "รข. ส่งคำขอ"; break;
            case 10: sResult = "มว. แจ้งปรับแก้ไข"; break;
            case 11: sResult = "มว. ตรวจสภาพภายนอก/ใน"; break;
            case 12: sResult = "มว. ตรวจลงน้ำ"; break;
            case 13: sResult = "มว. ผลการเทียบแป้น"; break;
            case 14: sResult = "มว. อนุมัติคำขอ"; break;
            case 15: sResult = "มว. บันทึกผลปิดงาน"; break;
        }

        return sResult;
    }

    public static string FormatDate(string Data)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Data))
        {
            Result = DateTime.Parse(Data).ToString("dd/MM/yyyy", new CultureInfo("th-TH"));


        }
        else
        {
            Result = " - ";
        }


        return Result;
    }

    public static string CheckFormatNuberic(int ndicemal)
    {
        string resule = "N";
        if (ndicemal == 0)
        {
            resule = "#,###,###,###,###,###";
        }
        else if (ndicemal == 1)
        {
            resule = "#,###,###,###,###,##0.0";
        }
        else if (ndicemal == 2)
        {
            resule = "#,###,###,###,###,##0.00";
        }
        else if (ndicemal == 3)
        {
            resule = "#,###,###,###,###,##0.000";
        }
        else
        {
            resule = "#,###,###,###,###,##0.0000";
        }
        //resule = "{0:n" + ndicemal.ToString() + "}";

        return resule;
    }

    public static List<T> DataTableConvertToList<T>(DataTable datatable) where T : new()
    {
        List<T> Temp = new List<T>();
        try
        {
            List<string> columnsNames = new List<string>();
            foreach (DataColumn DataColumn in datatable.Columns)
                columnsNames.Add(DataColumn.ColumnName);
            Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
            return Temp;
        }
        catch
        {
            return Temp;
        }

    }

    public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
    {
        T obj = new T();
        try
        {
            string columnname = "";
            string value = "";
            PropertyInfo[] Properties;
            Properties = typeof(T).GetProperties();
            foreach (PropertyInfo objProperty in Properties)
            {
                columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                if (!string.IsNullOrEmpty(columnname))
                {
                    value = row[columnname].ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                        {
                            value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                            objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                        }
                        else
                        {
                            value = row[columnname].ToString().Replace("%", "");
                            objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                        }
                    }
                }
            }
            return obj;
        }
        catch
        {
            return obj;
        }
    }

    public static DataTable GetVendorToRecieveMail(string sVendorID, string sMemuID)
    {
        DataTable dt = new DataTable();
        string sql = @"SELECT TUS.*,TPS.*
                        FROM TUSER TUS INNER JOIN TPERMISSION TPS ON TUS.SUID = TPS.SUID
                        WHERE SVENDORID = '{0}' AND TPS.SMENUID = '{0}' AND TPS.MAIL2ME = '1'";
        dt = CommonFunction.Get_Data(strConn, string.Format(sql, sVendorID, sMemuID));

        return dt;
    }

    public static string CheckPosition(string Req_ID)
    {
        string Result = "";

        string Query = "SELECT  REQUEST_ID ,RK_FLAG FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + "'";
        DataTable dt = CommonFunction.Get_Data(strConn, Query);

        if (dt.Rows.Count > 0)
        {
            Result = dt.Rows[0]["RK_FLAG"] + "";
        }
        return Result;

    }

    public static string GetCapacityFromCar(string VendorID, string StruckID)
    {
        string Result = "";

        string Query = @" /*T1 หัว, TCAR2 หาง*/
                        SELECT T1.SVENDORID, T1.SABBREVIATION, T1.STRUCKID, T1.SHEADREGISTERNO, T1.STRAILERREGISTERNO, T1.SCARTYPEID, T1.SCHASIS AS SCHASIS_H,TCAR2.SCHASIS AS SCHASIS_T,T1.STRUCKID AS VEH_ID,TCAR2.STRUCKID AS TU_ID,T1.TXT,T1.CARCATE_ID,T1.CARCATE_NAME,
                        CASE WHEN T1.SCARTYPEID = '0' THEN T1.NSLOT ELSE TCAR2.NSLOT END AS NSLOT,
                        CASE WHEN T1.SCARTYPEID = '0'  THEN T1.NTOTALCAPACITY ELSE TCAR2.NTOTALCAPACITY END AS NTOTALCAPACITY
                         FROM
                        (
                            SELECT TVD.SVENDORID, TVD.SABBREVIATION, TCAR.STRUCKID, TCAR.SHEADREGISTERNO, TCAR.STRAILERREGISTERNO, TCAR.SCARTYPEID, TCAR.SCHASIS,
                            CASE WHEN  TCAR.SCARTYPEID = '3' THEN TCAR.SHEADREGISTERNO ||  '/ ' || TCAR.STRAILERREGISTERNO  
                            ELSE  TCAR.SHEADREGISTERNO END AS  TXT,
                            TCAR.CARCATE_ID, TCAT.CARCATE_NAME,TCAR.NSLOT,TCAR.NTOTALCAPACITY
                            FROM TVENDOR TVD INNER JOIN TTRUCK TCAR ON TVD.SVENDORID = TCAR.STRANSPORTID AND TCAR.CACTIVE = 'Y' AND TCAR.SCARTYPEID IN ('0','3')
                            LEFT JOIN TBL_CARCATE TCAT ON TCAT.CARCATE_ID = TCAR.CARCATE_ID AND TCAT.ISACTIVE_FLAG  = 'Y'
                            WHERE TVD.SVENDORID = '{0}' 
                        )  T1
                        LEFT JOIN
                        TTRUCK TCAR2 ON T1.STRAILERREGISTERNO = TCAR2.SHEADREGISTERNO AND TCAR2.STRANSPORTID = '{0}'
                        WHERE T1.STRUCKID = '{1}'
                        ORDER BY  T1.SCARTYPEID";

        DataTable dt = CommonFunction.Get_Data(strConn, string.Format(Query, VendorID, StruckID));
        if (dt.Rows.Count > 0)
        {
            Result = dt.Rows[0]["NTOTALCAPACITY"] + "";
        }


        return Result;
    }

    public static string GetNameReqType(string ReqType_ID)
    {
        string Result = "";

        string Query = @"SELECT 
  REQTYPE_ID, REQTYPE_NAME, ISACTIVE_FLAG
FROM TBL_REQTYPE WHERE REQTYPE_ID='" + ReqType_ID + "' AND ISACTIVE_FLAG = 'Y'";

        DataTable dt = CommonFunction.Get_Data(strConn, string.Format(Query, ReqType_ID));
        if (dt.Rows.Count > 0)
        {
            Result = dt.Rows[0]["REQTYPE_NAME"] + "";
        }


        return Result;
    }

    //ใช้เช็คว่ามีปบริการเพิ่มเติมไหม
    public static int GetServceAdd(string Req_ID)
    {
        int Result = 0;

        Result = CommonFunction.Count_Value(strConn, @"SELECT REQUEST_ID, SERVICE_ID FROM TBL_REQUEST_ITEM2
                                       Where REQUEST_ID = '" + Req_ID + "'");
        return Result;
    }
    //ใช้เช็คว่าเอกสารบริการเพิ่มเติม และเอกสารเทียบแป้นจะแสดงตอน Status ไหน
    public static string DOC_CLOSEWORK(string StatusID, string Req_ID)
    {
        string Result = "";

        if (StatusID == "06" || StatusID == "10")
        {
            //ใช้เช็คว่ามีปบริการเพิ่มเติมไหม
            int CheckService = CommonFunction.Count_Value(strConn, @"SELECT REQUEST_ID, SERVICE_ID FROM TBL_REQUEST_ITEM2
                                       Where REQUEST_ID = '" + Req_ID + "'");
            if (CheckService > 0)
            {

            }
            else
            {
                Result += " AND DOCTYPE_ID <> '0005'";
            }

        }
        else
        {

            Result += " AND DOCTYPE_ID <> '0005'";
            Result += " AND DOCTYPE_ID <> '0006' ";
        }
        return Result;
    }

    public static string GET_VENDORID(string SUID)
    {
        string Result = "";

        string Query = @"SELECT SUID, SVENDORID FROM TUSER WHERE SUID = '" + SUID + "'";
        DataTable dt = CommonFunction.Get_Data(strConn, Query);
        if (dt.Rows.Count > 0)
        {
            Result = dt.Rows[0]["SVENDORID"] + "";
        }

        return Result;
    }

    //split arraystring
    public static string SplitFileName(string FileName)
    {
        string Result = "";
        string[] Type = new string[] { ".pdf", ".jpg", ".jpeg", ".bmp", ".gif", ".png", ".doc", ".docx", ".xls", ".xlsx", ".PDF", ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG", ".DOC", ".DOCX", ".XLS", ".XLSX" };
        string[] Name = FileName.Split(Type, StringSplitOptions.RemoveEmptyEntries);
        Result = Name[0];

        return Result;

    }

    //ความจุ
    public static DataTable LISTCAPACITY(string Req_ID)
    {
        DataTable dt = new DataTable();

        string _qry = @"SELECT msdata.SNAME,
    msdata.NLEVEL as LEVEL_NO,
    jdata.SLOT1,
    jdata.SLOT2,
    jdata.SLOT3,
    jdata.SLOT4,
    jdata.SLOT5,
    jdata.SLOT6,
    jdata.SLOT7,
    jdata.SLOT8,
    jdata.SLOT9,
    jdata.SLOT10
 FROM
(
SELECT   'ความจุที่ระดับแป้น 1(ลิตร)'  SNAME , 1 NLEVEL FROM DUAL
UNION
SELECT   'ความจุที่ระดับแป้น 2(ลิตร)'  SNAME , 2 NLEVEL FROM DUAL
UNION
SELECT   'ความจุที่ระดับแป้น 3(ลิตร)'  SNAME , 3 NLEVEL FROM DUAL
)msdata
LEFT JOIN
(
    SELECT 
    sdata.LEVEL_NO ,
    sdata.SLOT1,
    sdata.SLOT2,
    sdata.SLOT3,
    sdata.SLOT4,
    sdata.SLOT5,
    sdata.SLOT6,
    sdata.SLOT7,
    sdata.SLOT8,
    sdata.SLOT9,
    sdata.SLOT10
    FROM
    (
        SELECT LEVEL_NO,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 1 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT1  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 2 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT2  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 3 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT3  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 4 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT4  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 5 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT5  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 6 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT6  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO =7 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT7  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 8 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT8  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 9 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT9  ,
        TO_CHAR( MAX(CASE WHEN SLOT_NO = 10 THEN ( CASE  WHEN LEVEL_STATUS = 0 THEN TO_CHAR(CAPACITY,'999,999,999') ELSE TO_CHAR(CAPACITY,'999,999,999') END )  ELSE null  END) ) as SLOT10 
        FROM TBL_REQSLOT WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(Req_ID) + @"'  AND NVL(STATUS_PAN3,'xxx') <> '0'
        GROUP BY LEVEL_NO
    )sdata
)jdata
ON msdata.NLEVEL = jdata.LEVEL_NO 
 ORDER BY SNAME";

        dt = CommonFunction.Get_Data(strConn, _qry);

        return dt;
    }

    //เช็คชนิดไฟล์และตรวจสอบMaxsize 
    public static string DOC_CheckSize(string FILETYPE, int FileSiz)
    {
        FILETYPE = "." + FILETYPE;

        string Result = "";
        string[] DOC = new string[] { ".pdf", ".doc", ".docx", ".xls", ".xlsx", ".PDF", ".DOC", ".DOCX", ".XLS", ".XLSX" };
        string[] JPG = new string[] { ".jpg", ".jpeg", ".bmp", ".gif", ".png", ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        int MaxSizeFileUpload2MB = int.Parse(Resources.CommonResource.UploadMaxFileSize2MB);
        int MaxSizeFileUpload5MB = int.Parse(Resources.CommonResource.UploadMaxFileSize5MB);

        if (DOC.Contains(FILETYPE))
        {
            if (FileSiz > MaxSizeFileUpload5MB)
            {
                Result = "ไฟล์เอกสารขนาดต้องไม่เกิน 5 MB";
            }
        }
        else if (JPG.Contains(FILETYPE))
        {
            if (FileSiz > MaxSizeFileUpload2MB)
            {
                Result = "ไฟล์รูปขนาดต้องไม่เกิน 2 MB";
            }
        }

        return Result;

    }

    public static string Form_Email(string Nfrom, string UserID)
    {

        string DateTimeToDay = "วันที่ " + DateTime.Now.Day + " เดือน " + DateTime.Now.ToString("MMMM") + " พ.ศ. " + (DateTime.Now.Year + 543) + "";
        string Html = "";

        DataTable dt = CommonFunction.Get_Data(strConn, "SELECT SUID,SFIRSTNAME||' '||SLASTNAME as SNAME FROM TUSER WHERE SUID = '" + CommonFunction.ReplaceInjection(UserID) + "'");
        string SNAME = "";
        if (dt.Rows.Count > 0)
        {
            SNAME = dt.Rows[0]["SNAME"] + "";
        }

        switch (Nfrom)
        {
            //Userส่งคำขอ.
            case "1":

                Html = @"        
         <table width='600px' cellpadding='3' cellspacing='1' border='0' >
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>" + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ขอนำรถบรรทุกเข้าตรวจสอบ
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน บริษัท ปตท. จำกัด
                    (มหาชน) ส่วนเครื่องมือวัดและระบบควบคุมอัตโนมัติ</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    {0} ได้จัดทำคำขอนำรถบรรทุกน้ำมัน ทะเบียน {1} เข้าตรวจสอบ <br> วันที่นัดหมาย : {4}  <br> ประเภทคำขอ : {2}  <br> สาเหตุ
                    : {3}</td>
            </tr>
            </table>";

                #region OLDMAIL
                //                Html = @"        
                //         <table width='830px' cellpadding='3' cellspacing='1' border='0' >
                //            <tr>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>" + DateTimeToDay + @"</td>
                //            </tr>
                //            <tr>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ขอนำรถบรรทุกเข้าตรวจสอบ
                //                </td>
                //            </tr>
                //            <tr>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน บริษัท ปตท. จำกัด
                //                    (มหาชน) แผนกตรวจสอบปริมาตรถังส่วนมาตรวัด</td>
                //            </tr>
                //            <tr>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                //                    {0} ได้จัดทำคำขอนำรถบรรทุกน้ำมัน ทะเบียน {1} เข้าตรวจสอบ วันที่นัดหมาย {4} ประเภทคำขอ : {2} สาเหตุ
                //                    : {3}</td>
                //            </tr>
                //            <tr>
                //                <td width='50%'></td>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;'
                //                    align='center'>จึงเรียนมาเพื่อโปรดทราบ และดำเนินการต่อไป</td>
                //            </tr>
                //            <tr>
                //                <td width='50%'></td>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;'
                //                    align='center'>ขอแสดงความนับถือ</td>
                //            </tr>
                //            <tr>
                //                <td width='50%'></td>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;'
                //                    align='center'>{5}</td>
                //            </tr>
                //             <tr>
                //                <td width='50%'></td>
                //                <td style='font-size: 10px; font-family: Tahoma,Sans-Serif;'align='center'>{0}</td>
                //            </tr>
                //            </table>";
                #endregion

                break;
            //เตรวจสอบโดย รข.
            case "2":

                Html = @"        
                         <table width='600px' cellpadding='3' cellspacing='1' border='0' >
                            <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center' colspan='2'>
                                <img src='Images/LOGOPTT.png'></image></td>
                            </tr>
                            <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>" + DateTimeToDay + @"</td>
                            </tr>
                            <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง ผลการตรวจสอบเอกสารจาก รข.
                                </td>
                            </tr>
                            <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน {0} </td>
                            </tr>
                            <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   เนื่องจาก {0} ได้จัดทำคำขอนำรถบรรทุกน้ำมัน ทะเบียน {1} เข้าตรวจสอบ วันที่นัดหมาย {4} ประเภทคำขอ : {2} สาเหตุ : {3}</td>
                            </tr>
                             <tr>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   ผลการตรวจสอบของ รข. คือ {5} </td>
                            </tr>
                            <tr>
                                <td width='60%'></td>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;'
                                    align='center'>จึงเรียนมาเพื่อโปรดทราบ และดำเนินการต่อไป</td>
                            </tr>
                            <tr>
                                <td width='60%'></td>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;'
                                    align='center'>ขอแสดงความนับถือ</td>
                            </tr>
                            <tr>
                                <td width='60%'></td>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;'
                                    align='center'>" + SNAME + @"</td>
                            </tr>
                             <tr>
                                <td width='60%'></td>
                                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;'align='center'></td>
                            </tr>
                            </table>";

                break;
            //ยืนยันวันนีดหมาย มว.
            case "3":

                Html = @"        
                         <table width='600px' cellpadding='3' cellspacing='1' border='0'>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;padding-left:362px' align='left' colspan='2'>
                    ส่วนเครื่องมือวัด
                    <br />
                    และระบบควบคุมอัตโนมัติ </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;padding-left:362px' align='left' colspan='2'>
                    " + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง แจ้งยืนยันกำหนดวันนำรถบรรทุกน้ำมันเข้าตรวจสอบ{0}
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน ผู้จัดการบริษัทฯ , เจ้าของรถบรรทุกน้ำมันฯ  และเจ้าหน้าที่ผู้เกี่ยวข้อง</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   ตามที่บริษัทฯ ได้จัดทำคำขอนำรถบรรทุกน้ำมัน ทะเบียน{1} เข้าตรวจสอบ{0} ปตท. จึงขอยืนยันกำหนดวันที่เข้าดำเนินการในวันที่ {2} 
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อนึ่ง ก่อนนำรถเข้ารับบริการฯ  ขอให้ บริษัทฯ  เตรียมความพร้อมการใช้งานของรถฯ และตรวจสอบก่อนการรับบริการให้เรียบร้อย (ตามรายละเอียดเอกสารแนบ พร้อมส่งให้ ปตท.)</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>จึงเรียนมาเพื่อโปรดทราบ
                    และดำเนินการต่อไป</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>ขอแสดงความนับถือ
                </td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>" + SNAME + @"
                </td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
        </table>";

                break;

            //เลื่อนวันนีดหมาย มว.
            case "4":

                Html = @"        
                         <table width='600px' cellpadding='3' cellspacing='1' border='0'>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;padding-left:362px' align='left' colspan='2'>
                   ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;padding-left:362px' align='left' colspan='2'>
                    " + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง แจ้งเปลี่ยนกำหนดวันนำรถบรรทุกน้ำมันเข้าตรวจสอบ{0}
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน ผู้จัดการบริษัทฯ , เจ้าของรถบรรทุกน้ำมันฯ  และเจ้าหน้าที่ผู้เกี่ยวข้อง</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   ตามที่บริษัทฯ ได้จัดทำคำขอนำรถบรรทุกน้ำมัน ทะเบียน{1} เข้าตรวจสอบ{0} ในวันที่ {2} 
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เนื่อง ผู้ขนส่ง/เจ้าของรถฯ มีเหตุจำเป็นขอเปลี่ยนวันดำเนินการนั้น เพื่อความเหมาะสมในการบริการที่มีคุณภาพ ปตท. 
                    จึงขอเปลี่ยนกำหนดวันที่เข้าดำเนินการเป็นวันที่ {3} แทน  <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อนึ่ง ก่อนนำรถเข้ารับบริการฯ  ขอให้ บริษัทฯ  เตรียมความพร้อมการใช้งานของรถฯ และตรวจสอบก่อนการรับบริการให้เรียบร้อย (ตามรายละเอียดเอกสารแนบ พร้อมส่งให้ ปตท.)</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>จึงเรียนมาเพื่อโปรดทราบ
                    และดำเนินการต่อไป</td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>ขอแสดงความนับถือ
                </td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>" + SNAME + @"
                </td>
            </tr>
            <tr>
                <td width='60%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
        </table>";

                break;
            //มว.ตรวจสอบผ่าน
            case "5":
                Html = @"<table width='600px' cellpadding='3' cellspacing='1' border='0'>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left'>
                    เลขที่ <u>{0}</u> </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif; padding-left: 120px'
                    align='left'>ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>
                    " + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง แจ้งผลการตรวจสอบสภาพรถ/การตวงวัดน้ำ
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน เจ้าของรถบรรทุกน้ำมัน
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    ด้วยรถบรรทุกน้ำมันขนส่งในนาม <u>{1}</u> รหัสรถ <u>{2}</u>
                    <br />
                    ทะเบียน({3}) <u>{4}</u> เลขแชชซีย์ <u>{5}</u> ชนิด <u>{6}</u> ล้อ จำนวนช่อง <u>{7}</u>
                    ช่อง
                    <br />
                    ความจุรวม <u>{8}</u> ลิตร วัดน้ำครั้งสุดท้าย <u>{9}</u> และนำรถเข้ามาตรวจสอบวัดน้ำเมื่อ
                    <u>{10}</u> </td>
            </tr>
            <tr>
                <td style='font-size: 12px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>สรุปผลการตรวจสอบ</b>
                    <u><i>ผ่าน</i></u> <b>เกณฑ์มาตรฐาน ปตท.</b>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>กรุณานำรถบรรทุกน้ำมันของท่านดำเนินการดังนี้</b>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>1. ทำความสะอาดภายในถัง ก่อนทำการรับผลิตภัณฑ์
                        เพื่อคุณภาพและปลอดภัยตามข้อกำหนด ปตท.</b>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>2. เมื่อพร้อมแล้ว ให้ทำยื่นคำขอเทียบแป้นด้วยมิเตอร์ที่คลังน้ำมัน
                        ปตท. เพื่อยืนยันความถูกต้อง</b>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>3. ส่งผลเทียบแป้นผ่านระบบฯ ภายใน ระยะเวลา
                        30 วัน นับถัดจากวันที่ตรวจสอบล่าสุด</b>
                    <br />
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left' colspan='2'>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อดำเนินการเทียบแป้นกับมิเตอร์จ่ายน้ำมัน
                    ต่อไป</td>
            </tr>
            <tr>
                <td width='50%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'><u>___" + SNAME + @"___</u>
                    <br />
                    (" + SNAME + @")
                    <br />
                    ผู้ตรวจสอบ </td>
            </tr>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;'>รับทราบ
                </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>________________________________<br />
                    (_________________________________)<br />
                    เจ้าของรถ / ผู้แทน </td>
                <td width='50%'></td>
            </tr>
        </table>";
                break;

            //มว.ตรวจสอบไม่ผ่าน
            case "6":
                Html = @"<table width='600px' cellpadding='3' cellspacing='1' border='0'>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left'>
                    เลขที่ <u>{0}</u> </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif; padding-left: 120px'
                    align='left'>ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>
                    " + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง แจ้งสาเหตุการตรวจสอบสภาพรถ/การตวงวัดน้ำที่ไม่ผ่าน
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน เจ้าของรถบรรทุกน้ำมัน
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    ด้วยรถบรรทุกน้ำมันขนส่งในนาม <u>{1}</u> รหัสรถ <u>{2}</u>
                    <br />
                    ทะเบียน({3}) <u>{4}</u> เลขแชชซีย์ <u>{5}</u> ชนิด <u>{6}</u> ล้อ จำนวนช่อง <u>{7}</u>
                    ช่อง
                    <br />
                    ความจุรวม <u>{8}</u> ลิตร วัดน้ำครั้งสุดท้าย <u>{9}</u> และนำรถเข้ามาตรวจสอบวัดน้ำเมื่อ
                    <u>{10}</u> </td>
            </tr>
            <tr>
                <td style='font-size: 12px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>สรุปผลการตรวจสอบ</b>
                    <u><i>ไม่ผ่าน</i></u> <b>เกณฑ์มาตรฐาน ปตท.</b>
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{11}
                    <br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{12}
<br />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. ความเห็นผู้ตรวจสอบ <br>&nbsp;&nbsp;&nbsp;{13}
                </td>
            </tr>       
             <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left' colspan='2'>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;กรุณานำรถบรรทุกน้ำมันของท่านไปทำการแก้ไขสภาพให้ถูกต้องและปลอดภัยตามข้อกำหนด เมื่อพร้อมแล้ว  ให้ทำยื่นคำขอในระบบ TMS เพื่อขอนำรถเข้าตรวจสอบเนื่องจากตรวจสอบไม่ผ่าน และนัดหมายวันในระบบต่อไป </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left' colspan='2'>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อดำเนินการแก้ไขต่อไป</td>
            </tr>
            <tr>
                <td width='50%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'><u>_____________</u>
                    <br />
                    (" + SNAME + @")
                    <br />
                    ผู้ตรวจสอบ </td>
            </tr>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;'>รับทราบ
                </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>sdsds
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>________________________________<br />
                    (_________________________________)<br />
                    เจ้าของรถ / ผู้แทน </td>
                <td width='50%'></td>
            </tr>
        </table>";
                break;


            //มว.ตรวจสอบผ่านภายนอก/ภายใน
            case "7":
                Html = @"<table width='600px' cellpadding='3' cellspacing='1' border='0'>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left'>
                    เลขที่ <u>{0}</u> </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif; padding-left: 120px'
                    align='left'>ส่วนเครื่องมือวัด<br />
                    และระบบควบคุมอัตโนมัติ</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='right' colspan='2'>
                    " + DateTimeToDay + @"</td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรื่อง แจ้งผลการตรวจสอบสภาพรถ/การตวงวัดน้ำ
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>เรียน เจ้าของรถบรรทุกน้ำมัน
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    ด้วยรถบรรทุกน้ำมันขนส่งในนาม <u>{1}</u> รหัสรถ <u>{2}</u>
                    <br />
                    ทะเบียน({3}) <u>{4}</u> เลขแชชซีย์ <u>{5}</u> ชนิด <u>{6}</u> ล้อ จำนวนช่อง <u>{7}</u>
                    ช่อง
                    <br />
                    ความจุรวม <u>{8}</u> ลิตร วัดน้ำครั้งสุดท้าย <u>{9}</u> และนำรถเข้ามาตรวจสอบวัดน้ำเมื่อ
                    <u>{10}</u> </td>
            </tr>
            <tr>
                <td style='font-size: 12px; font-family: Tahoma,Sans-Serif;' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>สรุปผลการตรวจภายนอก/ภายในสอบ</b>
                    <u><i>ผ่าน</i></u> <b>เกณฑ์มาตรฐาน ปตท.</b>
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='left' colspan='2'>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;จึงเรียนมาเพื่อดำเนินการเทียบแป้นต่อไป</td>
            </tr>
            <tr>
                <td width='50%'></td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'><u>___" + SNAME + @"___</u>
                    <br />
                    (" + SNAME + @")
                    <br />
                    ผู้ตรวจสอบ </td>
            </tr>
            <tr>
                <td width='50%' style='font-size: 13px; font-family: Tahoma,Sans-Serif;'>รับทราบ
                </td>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>
                </td>
            </tr>
            <tr>
                <td style='font-size: 13px; font-family: Tahoma,Sans-Serif;' align='center'>________________________________<br />
                    (_________________________________)<br />
                    เจ้าของรถ / ผู้แทน </td>
                <td width='50%'></td>
            </tr>
        </table>";
                break;
        }



        return Html;

    }

    public static string GenCARNUM(string STRUCK_ID)
    {
        string Result = "";

        //        DataTable dt = CommonFunction.Get_Data(strConn, @"SELECT CASE WHEN TCK.SCARTYPEID = '0' THEN SUBSTR(TCK.SHEADREGISTERNO,0,2) ELSE SUBSTR(TCK.STRAILERREGISTERNO,0,2)  END AS HEADREGIS,NVL(LVS.SREGION,'NA') as SREGION,
        // CASE WHEN TCK.SCARTYPEID = '0' THEN TCK.STRUCKID ELSE TCK.STRAILERID END AS STRUCKID,TCK.SCARTYPEID
        //FROM  TTRUCK TCK
        //LEFT JOIN LSTPROVINCES LVS
        //ON CASE WHEN TCK.SCARTYPEID = '0' THEN SUBSTR(TCK.SHEADREGISTERNO,0,2) ELSE SUBSTR(TCK.STRAILERREGISTERNO,0,2)  END = LVS.SPROVINCESHORT
        //WHERE TCK.SCARTYPEID in (0,3) AND  CASE WHEN TCK.SCARTYPEID = '0' THEN TCK.STRUCKID ELSE TCK.STRAILERID END = '" + CommonFunction.ReplaceInjection(STRUCK_ID) + "'");

        DataTable dt = CommonFunction.Get_Data(strConn, @"SELECT LVS.CAR_TYPE
FROM  TTRUCK TCK
LEFT JOIN TBL_CARCATE LVS
ON TCK.CARCATE_ID = LVS.CARCATE_ID WHERE TCK.STRUCKID='" + STRUCK_ID + "'");

        string SID = "";
        string ss = @"SELECT SCAR_NUM FROM  
            (
                SELECT SCAR_NUM FROM TTRUCK
                UNION
                SELECT SCAR_NUM FROM TBL_REQUEST
            ) WHERE SCAR_NUM LIKE '%" + DateTime.Now.ToString("yyMM") + "%' ORDER BY SCAR_NUM DESC ";
        DataTable CHECKSCARNUM = CommonFunction.Get_Data(strConn, ss);

        if (CHECKSCARNUM.Rows.Count > 0)
        {
            string sss = CHECKSCARNUM.Rows[0]["SCAR_NUM"] + "";

            if (!string.IsNullOrEmpty(sss))
            {
                sss = sss.Substring(5, 3);

                Result = DateTime.Now.ToString("yyMM") + "-" + (int.Parse(sss) + 1).ToString().PadLeft(3, '0') + "-" + dt.Rows[0]["CAR_TYPE"] + "";
            }
            else
            {

            }
        }
        else
        {

            Result = DateTime.Now.ToString("yyMM") + "-001-" + dt.Rows[0]["CAR_TYPE"] + "";
        }

        return Result;

    }

    public static string GetUserMailForSend(string SUID, string STATUS_FLAG, string SVENDORID, string WHO)
    {
        //string Result = "";
        string QUERY = "";
        switch (WHO)
        {
            //USER
            case "N":
                switch (STATUS_FLAG)
                {
                    //รอแนบใบเสร็จ
                    case "09":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('56')";
                        break;
                    //ปรับแก้ไขคำขอ
                    case "02":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('56')";
                        break;
                    //รถไม่ได้มาตรฐาน
                    case "07":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('57')";
                        break;
                    //รอนำส่งเอกสารปิดงาน
                    case "06":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('57')";
                        break;
                }
                break;
            //RK
            case "Y":
                switch (STATUS_FLAG)
                {
                    //รอพิจารณาโดย  รข. 
                    case "01":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID NOT IN ('56','57')
                                  WHERE U.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";
                        break;
                    //มว. แจ้ง รข. ปรับแก้ไข
                    case "12":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('57')";
                        break;
                }
                break;
            //MV
            case "M":
                switch (STATUS_FLAG)
                {
                    //รอพิจารณาโดย มว.
                    case "03":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID NOT IN ('56','57')
                                  WHERE U.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";
                        break;
                    //รอเข้ารับบริการ/รอดำเนินการ
                    case "04":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID NOT IN ('56','57')
                                  WHERE U.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";
                        break;
                    //รอนำส่งเอกสารปิดงาน
                    case "06":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID NOT IN ('56','57')
                                  WHERE U.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";
                        break;

                    case "07":
                        QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID IN ('57')";
                        break;
                }
                break;
        }
        string EMAIL = "";
        DataTable dt = CommonFunction.Get_Data(strConn, QUERY);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                EMAIL += ";" + dt.Rows[i]["SEMAIL"];
            }
        }

        EMAIL = !string.IsNullOrEmpty(EMAIL) ? EMAIL.Remove(0, 1) : "";

        return EMAIL;
    }

    public static string GetUserMailForSend(string SVENDORID, string SMENUID)
    {
        //string Result = "";
        string QUERY = @"SELECT U.SUID, U.SVENDORID, U.CGROUP,  U.SEMAIL,P.SMENUID,P.CPERMISSION
                                  FROM TUSER U
                                  INNER JOIN TPERMISSION P
                                  ON U.SUID = P.SUID AND P.MAIL2ME = '1' AND P.CPERMISSION <> '0' AND P.SMENUID  IN ('" + SMENUID + @"')
                                  WHERE  U.SVENDORID = '" + SVENDORID + "'";


        string EMAIL = "";
        DataTable dt = CommonFunction.Get_Data(strConn, QUERY);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                EMAIL += ";" + dt.Rows[i]["SEMAIL"];
            }
        }

        EMAIL = !string.IsNullOrEmpty(EMAIL) ? EMAIL.Remove(0, 1) : "";

        return EMAIL;
    }

    public static string AddToTREQ_DATACHANGE(string REF_ID, string TYPE, string USERID, string STATUS, string DESCRIPTION, string SVENDORID, string CACTIVE)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(REF_ID))
        {
            string QUERY_CHK = "SELECT REQ_ID,REF_ID FROM TREQ_DATACHANGE WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(SVENDORID) + "'  AND REF_ID = '" + CommonFunction.ReplaceInjection(REF_ID) + "' AND TYPE = '" + CommonFunction.ReplaceInjection(TYPE) + "' AND STATUS <> '2'";
            DataTable dt = CommonFunction.Get_Data(strConn, QUERY_CHK);
            //เช็คว่า User มีคำร้องของ Refference อยู่แล้วหรือไม่ ถ้ามีให้อัพเดท
            if (dt.Rows.Count > 0)
            {
                //try
                //{
                string REQ_ID = dt.Rows[0]["REQ_ID"] + "";
                Result = REQ_ID;
                string QUERY_UPD = @"UPDATE TREQ_DATACHANGE
                                SET    DESCRIPTION = '" + CommonFunction.ReplaceInjection(DESCRIPTION) + @"',
                                       DUPDATE     =  SYSDATE,
                                       SUPDATE     = '" + CommonFunction.ReplaceInjection(USERID) + @"' 
                                WHERE  REQ_ID      = '" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                                AND    REF_ID      = '" + CommonFunction.ReplaceInjection(REF_ID) + @"'
                                AND    TYPE        = '" + CommonFunction.ReplaceInjection(TYPE) + @"'
                                AND    SVENDORID   = '" + CommonFunction.ReplaceInjection(SVENDORID) + @"'";
                SQLExecuteNonQuery(strConn, QUERY_UPD);
                //}
                //catch
                //{
                //    Result = "ไม่สามรถเปลี่ยนแปลงข้อมูลลงเบสได้";
                //}
            }
            else
            {
                Result = GENID_TREQ_DATACHANGE();
                string QUERY_INS = @"INSERT INTO TREQ_DATACHANGE (REQ_ID, REF_ID, TYPE,SVENDORID ,DESCRIPTION, STATUS, DCREATE, SCREATE,CACTIVE) 
VALUES ( '" + Result + @"'
,'" + CommonFunction.ReplaceInjection(REF_ID) + @"'
,'" + CommonFunction.ReplaceInjection(TYPE) + @"'
,'" + CommonFunction.ReplaceInjection(SVENDORID) + @"'
,'" + CommonFunction.ReplaceInjection(DESCRIPTION) + @"'
,'" + CommonFunction.ReplaceInjection(STATUS) + @"'
,SYSDATE
,'" + CommonFunction.ReplaceInjection(USERID) + @"','" + CommonFunction.ReplaceInjection(CACTIVE) + "')";

                //AddTODB();
                SQLExecuteNonQuery(strConn, QUERY_INS);

            }
        }
        else
        {
            Result = GENID_TREQ_DATACHANGE();
        }



        return Result;
    }
    #region Overide Add Sdescription
    public static string AddToTREQ_DATACHANGE(string REF_ID, string TYPE, string USERID, string STATUS, string DESCRIPTION, string SVENDORID, string CACTIVE, string SDESCRIPTION)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(REF_ID))
        {
            string QUERY_CHK = "SELECT REQ_ID,REF_ID FROM TREQ_DATACHANGE WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(SVENDORID) + "'  AND REF_ID = '" + CommonFunction.ReplaceInjection(REF_ID) + "' AND TYPE = '" + CommonFunction.ReplaceInjection(TYPE) + "' AND STATUS <> '2'";
            DataTable dt = CommonFunction.Get_Data(strConn, QUERY_CHK);
            //เช็คว่า User มีคำร้องของ Refference อยู่แล้วหรือไม่ ถ้ามีให้อัพเดท
            if (dt.Rows.Count > 0)
            {
                //try
                //{
                string REQ_ID = dt.Rows[0]["REQ_ID"] + "";
                Result = REQ_ID;
                string QUERY_UPD = @"UPDATE TREQ_DATACHANGE
                                SET    DESCRIPTION = '" + CommonFunction.ReplaceInjection(DESCRIPTION) + @"',
                                        STATUS = '" + CommonFunction.ReplaceInjection(STATUS) + @"',
                                       DUPDATE     =  SYSDATE,
                                       SDESCRIPTION ='" + CommonFunction.ReplaceInjection(SDESCRIPTION) + @"',
                                       SUPDATE     = '" + CommonFunction.ReplaceInjection(USERID) + @"' 
                                WHERE  REQ_ID      = '" + CommonFunction.ReplaceInjection(REQ_ID) + @"'
                                AND    REF_ID      = '" + CommonFunction.ReplaceInjection(REF_ID) + @"'
                                AND    TYPE        = '" + CommonFunction.ReplaceInjection(TYPE) + @"'
                                AND    SVENDORID   = '" + CommonFunction.ReplaceInjection(SVENDORID) + @"'";
                SQLExecuteNonQuery(strConn, QUERY_UPD);
                //}
                //catch
                //{
                //    Result = "ไม่สามรถเปลี่ยนแปลงข้อมูลลงเบสได้";
                //}
            }
            else
            {
                Result = GENID_TREQ_DATACHANGE();
                string QUERY_INS = @"INSERT INTO TREQ_DATACHANGE (REQ_ID, REF_ID, TYPE,SVENDORID ,DESCRIPTION, STATUS, DCREATE, SCREATE,CACTIVE,SDESCRIPTION) 
VALUES ( '" + Result + @"'
,'" + CommonFunction.ReplaceInjection(REF_ID) + @"'
,'" + CommonFunction.ReplaceInjection(TYPE) + @"'
,'" + CommonFunction.ReplaceInjection(SVENDORID) + @"'
,'" + CommonFunction.ReplaceInjection(DESCRIPTION) + @"'
,'" + CommonFunction.ReplaceInjection(STATUS) + @"'
,SYSDATE
,'" + CommonFunction.ReplaceInjection(USERID) + @"'
,'" + CommonFunction.ReplaceInjection(CACTIVE) + @"'
,'" + CommonFunction.ReplaceInjection(SDESCRIPTION) + "')";

                //AddTODB();
                SQLExecuteNonQuery(strConn, QUERY_INS);

            }
        }
        else
        {
            Result = GENID_TREQ_DATACHANGE();
        }



        return Result;
    }
    
    #endregion
    public static string GENID_TREQ_DATACHANGE()
    {
        string sql = @"SELECT REQ_ID FROM TREQ_DATACHANGE ORDER BY REQ_ID DESC";
        string Result = "";
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(strConn, sql);
        if (dt.Rows.Count > 0)
        {
            string sss = dt.Rows[0]["REQ_ID"] + "";

            if (!string.IsNullOrEmpty(sss))
            {
                sss = sss.Substring(6, 4);

                Result = DateTime.Now.ToString("yyMMdd", new CultureInfo("th-TH")) + (int.Parse(sss) + 1).ToString().PadLeft(4, '0') + "";
            }
            else
            {

            }
        }
        else
        {

            Result = DateTime.Now.ToString("yyMMdd") + "0001";
        }

        return Result;
    }

    public static string QUERY_TCONTRACT_GUARANTEES()
    {
        string Result = "";
        return Result = @"SELECT NCGID FROM (SELECT NCGID+0 As NCGID FROM 
                        (
                         SELECT NCGID FROM  TCONTRACT_GUARANTEES 
                         UNION
                         SELECT NCGID FROM  TCONTRACT_GUARANTEES_TEMP
                        )
                        ORDER BY NCGID DESC)  WHERE ROWNUM <= 1";

    }

    public static string QUERY_TCONTRACT_DOC()
    {
        string Result = "";
        return Result = @"SELECT SDOCID FROM (SELECT SDOCID+0 As SDOCID FROM 
                        (
                         SELECT SDOCID FROM  TCONTRACT_DOC
                         UNION
                         SELECT SDOCID FROM  TCONTRACT_DOC_TEMP
                        )
                        ORDER BY SDOCID DESC)  WHERE ROWNUM <= 1";

    }

    public static string QUERY_TTRUCK_DOC()
    {
        string Result = "";
        return Result = @"SELECT DOCID FROM (SELECT DOCID+0 As DOCID FROM 
                        (
                         SELECT DOCID FROM  TTRUCK_DOC
                         UNION
                         SELECT DOCID FROM  TTRUCK_DOC_TEMP
                        )
                        ORDER BY DOCID DESC)  WHERE ROWNUM <= 1";

    }

    public static string QUERY_TTRUCK_INSUREDOC()
    {
        string Result = "";
        return Result = @"SELECT DOCID FROM (SELECT DOCID+0 As DOCID FROM 
                        (
                         SELECT DOCID FROM  TTRUCK_INSUREDOC
                         UNION
                         SELECT DOCID FROM  TTRUCK_INSUREDOC_TEMP
                        )
                        ORDER BY DOCID DESC)  WHERE ROWNUM <= 1";

    }

    public static string QUERY_TTRUCK_MWATERDOC()
    {
        string Result = "";
        return Result = @"SELECT DOCID FROM (SELECT DOCID+0 As DOCID FROM 
                        (
                         SELECT DOCID FROM  TTRUCK_MWATERDOC
                         UNION
                         SELECT DOCID FROM  TTRUCK_MWATERDOC_TEMP
                        )
                        ORDER BY DOCID DESC)  WHERE ROWNUM <= 1";

    }

    public static int TRUCK_CONTRAC_CHECK(string SVENDORID, string CONTRACTID)
    {
        //        string QUERY = @"SELECT MS.SCONTRACTID,MS.TRUCK_CONTRACT,MS.NTRUCK FROM
        //(
        //SELECT TC.SCONTRACTID,TC.NTRUCK,TC.SVENDORID,COUNT(TCT.STRUCKID) as TRUCK_CONTRACT FROM TCONTRACT TC
        //INNER JOIN TCONTRACT_TRUCK TCT
        //ON TC.SCONTRACTID = TCT.SCONTRACTID AND TCT.CSTANDBY = 'N'
        //WHERE TC.SVENDORID = '"+SVENDORID+@"' 
        //GROUP BY  TC.SCONTRACTID,TC.NTRUCK,TC.SVENDORID//)MS
        //WHERE  MS.TRUCK_CONTRACT = MS.NTRUCK AND MS.SCONTRACTID = " + CONTRACTID + "";
        string QUERY = "SELECT TC.SCONTRACTID,TC.NTRUCK,TC.SVENDORID FROM TCONTRACT TC WHERE TC.SVENDORID = '" + SVENDORID + "' AND TC.SCONTRACTID = '" + CONTRACTID + "'";
        DataTable dt = CommonFunction.Get_Data(strConn, QUERY);
        int Result = 0;
        if (dt.Rows.Count > 0)
        {
            return Result = !string.IsNullOrEmpty(dt.Rows[0]["NTRUCK"] + "") ? int.Parse(dt.Rows[0]["NTRUCK"] + "") : 0;
        }
        else
        {
            return Result;
        }

    }

    public static void SynSapError(string TABLE_NAME, string PROCESS_NAME)
    {string PROCESS_NAME_TRIM = "";

        if (!string.IsNullOrEmpty(PROCESS_NAME))
        {
            PROCESS_NAME_TRIM = PROCESS_NAME.Trim();
            PROCESS_NAME_TRIM = PROCESS_NAME.Replace(" ", "");
            PROCESS_NAME_TRIM = PROCESS_NAME_TRIM.Length > 500 ? PROCESS_NAME_TRIM.Substring(0, 499) : PROCESS_NAME_TRIM;
        }
        else
        {
        };

        //TABLE_NAME หน้าที่ SysSap error;PROCESS_NAME สิ่งที่ Error
        string Logsap = @"DECLARE SYNSAPID number;BEGIN SELECT NVL(MAX(SYNC_ID),0)+ 1 into SYNSAPID FROM TSYNC_LOG WHERE ROWNUM = 1 ORDER BY  SYNC_ID DESC;INSERT INTO TSYNC_LOG (SYNC_ID, SYNC_DATE, STATUS_FLAG, TABLE_NAME, PROCESS_NAME) VALUES (SYNSAPID,SYSDATE,null," + (!string.IsNullOrEmpty(TABLE_NAME) ? "'" + CommonFunction.ReplaceInjection(TABLE_NAME) + @"'" : "null") + "," + (!string.IsNullOrEmpty(TABLE_NAME) ? "'" + CommonFunction.ReplaceInjection(PROCESS_NAME_TRIM) + @"'" : "null") + @");END;";


        SQLExecuteNonQuery(strConn, Logsap);
    }

    public static sbyte FindIndexColumnOfDataFieldInGrid(DataGrid _dgd, string _fieldname)
    {
        sbyte _i = 0; bool _c = false;
        for (; _i < _dgd.Columns.Count; _i++)
        {
            // ãËé¢éÒÁ¿ÔÅ·ÕèÁÕª¹Ô´à»ç¹ TemplateColumn
            if (_dgd.Columns[_i].GetType().ToString().Equals("System.Web.UI.WebControls.TemplateColumn")) continue;
            //
            if (((BoundColumn)_dgd.Columns[_i]).DataField.Equals(_fieldname)) { _c = true; break; }
        }
        if (!_c) _i = -1;
        return _i;
    }

    public static List<T> ConvertObject<T>(object objData)
    {
        List<T> Temp = new List<T>();
        if (objData != null)
        {
            Temp = (List<T>)objData;
        }
        return Temp;
    }

    public static DataTable ConvertObject(object objData)
    {
        DataTable dt = new DataTable();
        if (objData != null)
        {
            dt = (DataTable)objData;
        }
        return dt;
    }

    public static int ConvertObjectInt(string objData)
    {
        int Result = 0;
        if (objData != null)
        {
            int flag;
            if (int.TryParse(objData, out flag))
            {
                Result = flag;
            }
            else
            {

            }
        }

        return Result;
    }

    public static decimal ConvertObjectdecimal(string objData)
    {
        decimal Result = 0;
        if (objData != null)
        {
            decimal flag;
            if (decimal.TryParse(objData, out flag))
            {
                Result = flag;
            }
            else
            {

            }
        }

        return Result;
    }

    public static TimeSpan ConvertObjectTime(object objData)
    {
        TimeSpan dt = new TimeSpan();
        if (objData != null)
        {
            dt = (TimeSpan)objData;
        }
        return dt;
    }

    public static bool ConvertObjectBool(string objData)
    {
        bool Result = false;
        if (objData != null)
        {
            bool flag;
            if (Boolean.TryParse(objData, out flag))
            {
                Result = true;
            }
            else
            {

            }
        }

        return Result;
    }

    public static string Get_SHIPTO_FROM_TBORDERPLAN(string SDELIVERY_NO)
    {
        string Result = "null";

        if (!string.IsNullOrEmpty(SDELIVERY_NO))
        {
            DataTable dt_SHIP_TO = CommonFunction.Get_Data(strConn, @"SELECT  NVL(TD.SHIP_TO,LPAD('9'||TBORDER.RECIEVEPOINT,10,'0')) SHIP_TO
                                FROM TDELIVERY  TD
                                LEFT JOIN TBORDER ON TD.SALES_ORDER=TBORDER.DOCNO
                                WHERE TD.SHIP_TO is null AND TD.DELIVERY_NO LIKE '008%' AND TBORDER.RECIEVEPOINT is not null AND TD.DELIVERY_NO = '" + SDELIVERY_NO + @"'");
            if (dt_SHIP_TO.Rows.Count > 0)
            {

                if (!string.IsNullOrEmpty(dt_SHIP_TO.Rows[0]["SHIP_TO"] + ""))
                {

                    Result = dt_SHIP_TO.Rows[0]["SHIP_TO"] + "";
                    string SQL = @"UPDATE  TDELIVERY TDT
                            SET TDT.SHIP_TO = '" + dt_SHIP_TO.Rows[0]["SHIP_TO"] + @"'
                            WHERE TDT.DELIVERY_NO = '" + SDELIVERY_NO + "'";

                    if (!string.IsNullOrEmpty(SQL))
                    {
                        using (OracleConnection con = new OracleConnection(strConn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }

                            using (OracleCommand com = new OracleCommand(SQL, con))
                            {
                                com.ExecuteNonQuery();
                            }

                            con.Close();

                        }
                    }
                }
            }

        }

        return Result;
    }
}