﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
using System.Web.Configuration;
using iTerminalBLL;
using iTerminalDAL;
using iTerminalDATA;
/// <summary>
/// Summary description for iTerminal
/// </summary>
public class iTerminal
{

    string sconn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    public string UpdateMAP(string Outbound, string Time_Window, string Vehno, string Tu_no, string Depot, string Driver_no, string Plan_id, string Name, string Surname, string User_type, DateTime Plant_date)
    {

        string[] sFullName = Name.Split(' ');
        if (sFullName.Length > 1)
        {
            Name = sFullName[0];
            Surname = sFullName[1];
        }

        if (Tu_no == "")
        {
            Tu_no = Vehno;
        }

        Outbound = Outbound.Replace(",", ";");
        string OutboundCheck = Outbound;
        OutboundCheck = ";" + OutboundCheck;
        Outbound = OutboundCheck.Replace(";8", ";008").Remove(0, 1);

        DateTime datetemp;
        int ntemp;

        //iTerminalServiceService.iTerminalService WSS = new iTerminalServiceService.iTerminalService();
        //WSS.Url = WebConfigurationManager.AppSettings["iTerminalServiceService.iTerminalService"] + "";
        //string sUserName = WebConfigurationManager.AppSettings["iTerminalServiceService.UserName"] + "";
        //string sPassword = WebConfigurationManager.AppSettings["iTerminalServiceService.Password"] + "";

        //if (sUserName != "")
        //{
        //    WSS.Credentials = new System.Net.NetworkCredential(sUserName, sPassword);
        //}
        //else
        //{
        //    WSS.Credentials = new System.Net.NetworkCredential("kioskservice", "pass@kiosk");
        //}

        
        MapDOInput MapDO = new MapDOInput();
        MapDO.Outbound = convertToStrNumber(Outbound);
        MapDO.Time_Window = int.TryParse(Time_Window, out ntemp) ? ntemp : 0;
        MapDO.Vehno = Vehno;
        MapDO.Tu_no = Tu_no;
        MapDO.Depot = Depot;
        MapDO.Driver_no = (Driver_no == "") ? "0000000000" : Driver_no;
        MapDO.Plan_id = Plan_id;
        MapDO.Name = Name;
        MapDO.Surname = Surname;
        MapDO.User_type = GetGroupName(User_type);
        MapDO.Plant_date = Plant_date;
        MapDO.Status = "N";
        Response respons = iTerminalBLL.ValidateBLL.ValidateMapDO(MapDO);
        if (respons.Response_code.ToUpper() != "E")
        {
            MapDOBLL map = new MapDOBLL();
            respons = map.UpdateMapDOService(MapDO);
        }
        


        InsertiTerminalError(Outbound, Time_Window, Vehno, Tu_no, Depot, Driver_no, Plan_id, Name, Surname, User_type, Plant_date, respons.Response_code, respons.Response_description);

        return respons.Response_code + ";" + respons.Response_description;

    }

    public string DeleteMAP(string Outbound, string Time_Window, string Vehno, string Tu_no, string Depot, string Driver_no, string Plan_id, string Name, string Surname, string User_type, DateTime Plant_date)
    {
        string[] sFullName = Name.Split(' ');
        if (sFullName.Length > 1)
        {
            Name = sFullName[0];
            Surname = sFullName[1];
        }

        Outbound = Outbound.Replace(",", ";");

        string OutboundCheck = Outbound;
        OutboundCheck = ";" + OutboundCheck;
        Outbound = OutboundCheck.Replace(";8", ";008").Remove(0, 1);

        DateTime datetemp;
        int ntemp;
        //iTerminalServiceService.iTerminalService WSS = new iTerminalServiceService.iTerminalService();
        //WSS.Url = WebConfigurationManager.AppSettings["iTerminalServiceService.iTerminalService"] + "";
        //string sUserName = WebConfigurationManager.AppSettings["iTerminalServiceService.UserName"] + "";
        //string sPassword = WebConfigurationManager.AppSettings["iTerminalServiceService.Password"] + "";

        //if (sUserName != "")
        //{
        //    WSS.Credentials = new System.Net.NetworkCredential(sUserName, sPassword);
        //}
        //else
        //{
        //    WSS.Credentials = new System.Net.NetworkCredential("kioskservice", "pass@kiosk");
        //}

        MapDOInput MapDO = new MapDOInput();
        MapDO.Outbound = convertToStrNumber(Outbound);
        MapDO.Time_Window = int.TryParse(Time_Window, out ntemp) ? ntemp : 0;
        MapDO.Vehno = Vehno;
        MapDO.Tu_no = "";
        MapDO.Depot = Depot;
        MapDO.Driver_no = (Driver_no == "") ? "0000000000" : Driver_no;
        MapDO.Plan_id = Plan_id;
        MapDO.Name = Name;
        MapDO.Surname = Surname;
        MapDO.User_type = GetGroupName(User_type);
        MapDO.Plant_date = Plant_date;
        MapDO.Status = "D";

        Response respons = iTerminalBLL.ValidateBLL.ValidateMapDO(MapDO);
        if (respons.Response_code.ToUpper() != "E")
        {
            MapDOBLL map = new MapDOBLL();
            respons = map.UpdateMapDOService(MapDO);
        }
        InsertiTerminalError(Outbound, Time_Window, Vehno, Tu_no, Depot, Driver_no, Plan_id, Name, Surname, User_type, Plant_date, respons.Response_code, respons.Response_description);


        return respons.Response_code + ";" + respons.Response_description;

    }


    private string GetGroupName(string sID)
    {
        string GroupName = "";

        switch (sID)
        {
            case "0": GroupName = "ผู้ขนส่ง";
                break;
            case "1": GroupName = "คลัง";
                break;
            case "2": GroupName = "เจ้าหน้าที่ปตท";
                break;
        }
        return GroupName;
    }



    public int InsertiTerminalError(string Outbound, string Time_Window, string Vehno, string Tu_no, string Depot, string Driver_no, string Plan_id, string Name, string Surname, string User_type, DateTime Plant_date, string sTypeError, string sDesscription)
    {
        int result = -1;
        try
        {
            // Reopen connection if close
            using (OracleConnection conn = new OracleConnection(sconn))
            {
                if (conn.State == ConnectionState.Closed) conn.Open();
                string sql = "INSERT INTO iTerminalErrorLog(Outbound,Time_Window,Vehno,Tu_no,Depot,Driver_no,Plan_id,sTypeError,sDesscription,Plant_date,dCreate ) values(:Outbound,:Time_Window,:Vehno,:Tu_no,:Depot,:Driver_no,:Plan_id,:sTypeError,:sDesscription,:Plant_date,sysdate)";
                using (OracleCommand cmd = new OracleCommand(sql, conn))
                {
                    cmd.Parameters.AddWithValue(":Outbound", Outbound);
                    cmd.Parameters.AddWithValue(":Time_Window", Time_Window);
                    cmd.Parameters.AddWithValue(":Vehno", Vehno);
                    cmd.Parameters.AddWithValue(":Tu_no", Tu_no);
                    cmd.Parameters.AddWithValue(":Depot", Depot);
                    cmd.Parameters.AddWithValue(":Driver_no", Driver_no);
                    cmd.Parameters.AddWithValue(":Plan_id", Plan_id);
                    cmd.Parameters.AddWithValue(":sTypeError", sTypeError);
                    cmd.Parameters.AddWithValue(":sDesscription", sDesscription);
                    cmd.Parameters.AddWithValue(":Plant_date", Plant_date);
                    result = cmd.ExecuteNonQuery();

                    //Page page = HttpContext.Current.Handler as Page;
                    //if (page != null)
                    //{
                    //    page.RegisterStartupScript("alert", "<script>alert('iTerminal Error : " + sDesscription + "');</script>");
                    //    ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert('iTerminal Error : " + sDesscription + "');</script>", false);
                    //}
                }
            }
        }
        catch (Exception err)
        {
            if (ConfigurationSettings.AppSettings["alerttrycatch"].Equals("1"))
            {
                Page page = HttpContext.Current.Handler as Page;

                if (page != null)
                {
                    page.RegisterStartupScript("alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>");
                    ScriptManager.RegisterClientScriptBlock(page, page.GetType(), "alert", "<script>alert(\"Source : " + err.Source.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nMessage : " + err.Message.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\\nStack Trace : \\n" + err.StackTrace.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n") + "\");</script>", false);
                }

            }
        }
        finally
        {

        }
        return result;
    }

    private string convertToStrNumber(string do_no)
    {
        string ret = "";
        string[] ar_do_no = do_no.Split(';');
        foreach (string item in ar_do_no)
        {
            if (!string.IsNullOrEmpty(item))
            {
                if (ret == "")
                {
                    ret = Convert.ToDecimal(item).ToString();
                }
                else
                {
                    ret = ret + ";" + Convert.ToDecimal(item).ToString();
                }
            }
            
        }

        return ret;
    }

}