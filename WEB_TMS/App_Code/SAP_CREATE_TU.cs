﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.IO;
using TMS_D_UpdateVehicle_SYNC_OUT_SI;
using TMS_D_UpdateTU_SYNC_OUT_SI;
using TMS_D_CreateVehicle_SYNC_OUT_SI;
using TMS_D_CreateTU_SYNC_OUT_SI;
using System.Globalization;
/// <summary>
/// Summary description for SAP_CREATE_TU
/// </summary>
public class SAP_CREATE_TU
{
    public SAP_CREATE_TU()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Data
    private string fVEHICLE;
    private string fLANGUAGE;
    private string fVEH_TEXT;
    private string fVEH_TYPE;
    private string fROUTE;
    private string fDEPOT;
    private string fTPPOINT;
    private string fVHCLSIGN;
    private string fCLASS_GRP;
    private string fCARRIER;
    private string fVEH_STATUS;
    private string fVEH_ID;
    private string fREG_DATE;
    private int fTU_Assignment_Items;
    private string fTU_NO;
    private string fCarType_ID;
    #endregion
    #region Property
    public string sVehicle
    {
        get { return this.fVEHICLE; }
        set { this.fVEHICLE = DBNull(value); }
    }
    public string sLanguage
    {
        get { return this.fLANGUAGE; }
        set { this.fLANGUAGE = DBNull(value); }
    }
    public string sVeh_Text
    {
        get { return this.fVEH_TEXT; }
        set { this.fVEH_TEXT = DBNull(value); }
    }
    public string sVeh_Type
    {
        get { return this.fVEH_TYPE; }
        set { this.fVEH_TYPE = DBNull(value); }
    }
    public string sRoute
    {
        get { return this.fROUTE; }
        set { this.fROUTE = DBNull(value); }
    }
    public string sDepot
    {
        get { return this.fDEPOT; }
        set { this.fDEPOT = DBNull(value); }
    }
    public string sTPPoint
    {
        get { return this.fTPPOINT; }
        set { this.fTPPOINT = DBNull(value); }
    }
    public string sVhclSign
    {
        get { return this.fVHCLSIGN; }
        set { this.fVHCLSIGN = DBNull(value); }
    }
    public string sClass_Grp
    {
        get { return this.fCLASS_GRP; }
        set { this.fCLASS_GRP = DBNull(value); }
    }
    public string sCarrier
    {
        get { return this.fCARRIER; }
        set { this.fCARRIER = DBNull(value); }
    }
    public string sVeh_Status
    {
        get { return this.fVEH_STATUS; }
        set { this.fVEH_STATUS = DBNull(value); }
    }
    public string sVeh_ID
    {
        get { return this.fVEH_ID; }
        set { this.fVEH_ID = DBNull(value); }
    }
    public string sReg_Date
    {
        get { return this.fREG_DATE; }
        set { this.fREG_DATE = DBNull(value); }
    }
    public int nTU_Assignment_Items
    {
        get { return this.fTU_Assignment_Items; }
        set { this.fTU_Assignment_Items = (value); }
    }
    public string sTU_NO
    {
        get { return this.fTU_NO; }
        set { this.fTU_NO = DBNull(value); }
    }
    public string sCarType_ID
    {
        get { return this.fCarType_ID; }
        set { this.fCarType_ID = DBNull(value); }
    }
    #endregion

    #region Method
    private string DBNull(string _data)
    {//#!
        return _data + "" == "" ? " " : _data;
    }
    public string CRT_TU_SYNC_OUT_SI()
    {
        string result = "";
        bool IsUpdated = false;

        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            DataTable dt = new DataTable();
            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT--,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
,TUAS.TU_Assignment_Items,TU.STRUCKID STRAILERID ,TU.SHEADREGISTERNO STRAILERREGISTERNO  ,CAST(TRCK.SCHASIS  AS varchar2(18)) VEH_ID   ,CAST(TU.SCHASIS AS varchar2(18))  TU_ID,TRCK.STRANSPORTID CARRIER
,TRCK.VEH_TEXT , TU.VEH_TEXT TU_TEXT, TRCK.DREGISTER VEH_REG ,  TU.DREGISTER TU_REG
,NVL(TU.DEPOT ,TRCK.DEPOT ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS  ,(CASE WHEN NVL(TU.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) TU_STATUS
,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY
,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
,TRCK.CLASSGRP CLASS_GRP
,TU.CLASSGRP TU_CLASS_GRP , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT
,TRCK.TPPOINT TPPOINT ,TRCK.ROUTE ROUTE ,TRCK.TRUCK_CATEGORY TYPE_ID,TU.TRUCK_CATEGORY TU_TYPE
,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
,NVL(TU.DPREV_SERV,TRCK.DPREV_SERV) DPREV_SERV  
FROM TTRUCK TRCK
LEFT JOIN TTRUCK TU ON  NVL(TU.STRUCKID,'TRYYnnnnn')= NVL(TRCK.STRUCKID,'TRYYnnnnn')
LEFT JOIN 
(
    SELECT STRUCKID,NCOMPARTNO ,MAX(NCAPACITY)  NCAPACITY
    FROM TTRUCK_COMPART 
    GROUP BY STRUCKID,NCOMPARTNO
) TRCKCOMP ON TU.STRUCKID=TRCKCOMP.STRUCKID
LEFT JOIN
(
    SELECT VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID STRAILERID,TU.STRAILERREGISTERNO
    ,COUNT(VEH.STRUCKID) TU_Assignment_Items
    FROM( SELECT * FROM TTRUCK  WHERE SCARTYPEID='0' OR SCARTYPEID='4' ) VEH
    LEFT JOIN ( SELECT *  FROM TTRUCK WHERE SCARTYPEID='4' ) TU ON VEH.STRAILERID =TU.STRUCKID
    WHERE 1=1
    GROUP BY   VEH.STRUCKID ,VEH.SHEADREGISTERNO ,VEH.SCARTYPEID ,TU.STRUCKID ,TU.STRAILERREGISTERNO
)TUAS
ON TUAS.SHEADREGISTERNO = TRCK.SHEADREGISTERNO
WHERE TRCK.SCARTYPEID in('0','4')
AND TRCK.SHEADREGISTERNO ='" + sVehicle + @"'
ORDER BY TRCKCOMP.NCOMPARTNO ASC", connection).Fill(dt);
            if (dt.Rows.Count >= 1)
            {
                DataRow row = dt.Rows[0];
                sCarType_ID = row["SCARTYPEID"] + "";
                nTU_Assignment_Items = int.Parse(row["TU_Assignment_Items"] + "");
                sTU_NO = row["STRAILERREGISTERNO"] + "";
                sDepot = row["DEPOT"] + "" == "" ? "#!" : row["DEPOT"] + "";
                sVeh_Status = " ";
                sVeh_ID = row["VEH_ID"] + "";
                sCarrier = row["CARRIER"] + "";
                sReg_Date = row["VEH_REG"] + "" == "" ? "1900-01-01" : DateTimeCheck(row["VEH_REG"] + "");
                sClass_Grp = row["CLASS_GRP"] + "" == "" ? "O120" : row["CLASS_GRP"] + "";
                sTPPoint = row["TPPOINT"] + "" == "" ? "#!" : row["TPPOINT"] + "";
                sRoute = row["ROUTE"] + "" == "" ? "ZTD001" : row["ROUTE"] + "";
                sVeh_Type = row["TYPE_ID"] + "" == "" ? "A110" : row["TYPE_ID"] + "";
                sVeh_Text = row["VEH_TEXT"] + "" == "" ? "XX(0,0,0,0,0,0)" : row["VEH_TEXT"] + "";                
                    using (var TU_Client = new CreateTU_SYNC_OUT_SIClient("HTTP_Port2"))
                    {
                        TU_Client.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
                        TU_Client.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";

                        string Msg = "", sErr = "";

                        if (sCarType_ID == "0" || sCarType_ID == "4")
                        {//10Wheel
                            #region Compartment
                            //เช็คว่าในกรณีที่ NSLOT มีค่าเท่ากับจำนวน Row ใน Datatable ใช้ NSLOT แต่ถ้าไม่ใช่ ใช้  dt.Rows.Count แทน
                            int TU_compartment_Items = ((!string.IsNullOrEmpty(dt.Rows[0]["NSLOT"] + "") ? int.Parse(dt.Rows[0]["NSLOT"] + "") : 0) == dt.Rows.Count ? int.Parse(dt.Rows[0]["NSLOT"] + "") : dt.Rows.Count);
                            var tu_compartment = new CreateTU_Src_Req_DTItem[TU_compartment_Items];
                            if (TU_compartment_Items != dt.Rows.Count)
                            {

                                for (int idxCompart = 0; idxCompart < (TU_compartment_Items); idxCompart++)
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        DataRow dr_Compart = dt.Rows[idxCompart];
                                        tu_compartment[idxCompart] = new CreateTU_Src_Req_DTItem()
                                        {
                                            CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                            COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                            TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + ""
                                        };

                                    }
                                    else
                                    {
                                        sErr = "no compartment data";
                                        tu_compartment[idxCompart] = new CreateTU_Src_Req_DTItem()
                                        {
                                            CMP_MAXVOL = "#!",
                                            COM_NUMBER = "#!",
                                            LOAD_SEQ = "#!",
                                            SEQ_NMBR = "#!",
                                            TU_NUMBER = "#!"
                                        };
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataRow dr_Compart in dt.Rows)
                                {
                                    int idxCompart = dt.Rows.IndexOf(dr_Compart);
                                    tu_compartment[idxCompart] = new CreateTU_Src_Req_DTItem()
                                    {
                                        CMP_MAXVOL = dr_Compart["NCAPACITY"] + "" == "" ? "#!" : dr_Compart["NCAPACITY"] + "",
                                        COM_NUMBER = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                        LOAD_SEQ = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                        SEQ_NMBR = dr_Compart["NCOMPARTNO"] + "" == "" ? "#!" : dr_Compart["NCOMPARTNO"] + "",
                                        TU_NUMBER = dr_Compart["STRAILERREGISTERNO"] + "" == "" ? "#!" : dr_Compart["STRAILERREGISTERNO"] + ""
                                    };
                                }
                            }
                            #endregion
                            #region TU
                            string WATER_EXPIRE = row["DWATEREXPIRE"] + "" == "" ? "" : DateTimeCheck(row["DWATEREXPIRE"] + "");
                            string START_DATE = ((row["DPREV_SERV"] + "" == "") ? (WATER_EXPIRE != "" ? Convert.ToDateTime(WATER_EXPIRE).AddYears(-3).ToString("yyyy-MM-dd", new CultureInfo("en-US")) : "") : DateTimeCheck(row["DPREV_SERV"] + ""));

                            START_DATE = (START_DATE + "" == "" ? "1900-01-01" : START_DATE);
                            WATER_EXPIRE = (WATER_EXPIRE + "" == "" ? "1900-01-01" : WATER_EXPIRE);

                            string val_MAXWGT = "0", val_UNLWGT = "0";
                            if (sCarType_ID == "0")
                            {
                                row["NWEIGHT"] = val_UNLWGT = "10000";
                                row["NCALC_WEIGHT"] = val_MAXWGT = "60000";
                            }
                            else if (sCarType_ID == "3")
                            {
                                row["NWEIGHT"] = val_UNLWGT = "25000";
                                row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                            }
                            else if (sCarType_ID == "4")
                            {
                                row["NWEIGHT"] = val_UNLWGT = "25000";
                                row["NCALC_WEIGHT"] = val_MAXWGT = "80000";
                            }
                            else
                            {
                                row["NWEIGHT"] = val_UNLWGT = "0";
                                row["NCALC_WEIGHT"] = val_MAXWGT = "1";
                            }
                            var trailer = new CreateTU_Src_Req_DT()
                            {
                                TU_NUMBER = sTU_NO,
                                ZSTART_DAT = START_DATE,
                                VOL_UOM = "#!",
                                TU_UNLWGT = row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "",
                                TU_STATUS = " ",//row["TU_STATUS"] + "" == "" ? " " : row["TU_STATUS"] + "",
                                TU_TEXT = row["TU_TEXT"] + "" == "" ? "XX(0,0,0,0)" : row["TU_TEXT"] + "",
                                TU_MAXWGT = row["NCALC_WEIGHT"] + "" == "" ? (int.Parse((row["NWEIGHT"] + "" == "" ? "0" : row["NWEIGHT"] + "")) + 0.1) + "" : row["NCALC_WEIGHT"] + "",
                                L_NUMBER = row["SLAST_REQ_ID"] + "" == "" ? "RQXXXXX" : row["SLAST_REQ_ID"] + "",
                                EXPIRE_DAT = WATER_EXPIRE,
                                //TU_ID = row["TU_ID"] + "" == "" ? " " : row["TU_ID"] + "",
                                TU_TYPE = row["TU_TYPE"] + "" == "" ? "A110" : row["TU_TYPE"] + "",
                                LANGUAGE = "T",
                                WGT_UOM = "KG",
                                CMP_ITEM = tu_compartment
                            };
                            sErr = (sErr == "" && WATER_EXPIRE == "") ? "no expire date" : "";
                            sErr = (sErr == "" && START_DATE == "") ? "no start date" : sErr;
                            #endregion
                            if (sErr == "")
                            {
                                try
                                {
                                    var Res_TU = TU_Client.CreateTU_SYNC_OUT_SI(trailer);
                                    //IsUpdated = !Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E");
                                    //if (Res_TU.GT_RETURN_TU[0].MSGTYP.Equals("E"))
                                    //{
                                    //    foreach (var Return in Res_TU.GT_RETURN_TU)
                                    //    {
                                    //        Msg += "," + Return.MESSAGE;
                                    //    }
                                    //}

                                    int Res_TU_Row = Res_TU.GT_RETURN_TU.Length > 0 ? (Res_TU.GT_RETURN_TU.Length - 1) : 0;
                                    for (int i = 0; i <= Res_TU_Row; i++)
                                    {
                                        IsUpdated = !Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E");
                                        if (Res_TU.GT_RETURN_TU[i].MSGTYP.Equals("E"))
                                        {
                                            foreach (var Return in Res_TU.GT_RETURN_TU)
                                            {
                                                Msg += "," + Return.MESSAGE;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    IsUpdated = false;
                                    Msg = "," + ex.Message.ToString();
                                }

                            }
                            else { Msg = sErr; IsUpdated = false; }
                        }                        
                        result = Msg;
                        //SaveResult2TextFIle(result);
                    }

               
            }
        }

        return result;
    }    
    private string DateTimeCheck(string sDate)
    {
        string Date_Result = "1900-01-01";

        if (!string.IsNullOrEmpty(sDate))
        {
            if (Convert.ToDateTime(sDate).Year > 1500)
            {
                Date_Result = Convert.ToDateTime(sDate).ToString("yyyy-MM-dd", new CultureInfo("en-US"));
            }
        }

        return Date_Result;
    }
    #endregion
    #region Function
    private string GetError(object ResRETURN)
    {
        string sError = "";

        return sError;
    }

    //protected void SaveResult2TextFIle(string log)
    //{

    //    string codeVersion = "1.0.0";
    //    string logName = "SAP_SYNTRCK_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
    //    using (System.IO.TextWriter tw = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("UploadFile/SAP_Log/" + logName), true))
    //    {
    //        tw.WriteLine("Time: " + DateTime.Now.ToString("HH:mm:ss"));

    //        tw.WriteLine(":> " + log);

    //        tw.WriteLine("-------");
    //    }

    //}
    #endregion

}