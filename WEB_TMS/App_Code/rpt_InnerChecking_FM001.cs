﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rpt_InnerChecking_FM001
/// </summary>
public class rpt_InnerChecking_FM001 : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private ReportHeaderBand ReportHeader;
    private PageFooterBand PageFooter;
    private ReportFooterBand ReportFooter;
    private XRLabel xrLabel2;
    private XRLabel xrLabel4;
    private XRLabel xrLabel8;
    private XRLabel xrLabel7;
    private XRLabel xrLabel6;
    private XRLabel xrlblsMonth;
    private XRLabel rxlblsYear;
    private XRLabel xrlblsDay;
    private XRLabel xrLabel9;
    private XRLabel xrLabel10;
    private XRLabel xrLabel11;
    private XRLabel xrLabel12;
    private XRLabel xrLabel13;
    private XRLine xrLine3;
    private XRLabel xrLabel14;
    private XRLine xrLine4;
    private XRLine xrLine5;
    private XRLabel xrLabel15;
    private XRLabel xrLabel16;
    private XRLabel xrLabel17;
    private XRLine xrLine6;
    private XRLine xrLine7;
    private XRLabel xrLabel18;
    private XRLine xrLine8;
    private XRLabel xrLabel19;
    private XRLine xrLine9;
    private XRLine xrLine10;
    private XRLabel xrLabel20;
    private XRLabel xrLabel21;
    private XRLine xrLine11;
    private XRLabel xrLabel23;
    private XRLine xrLine12;
    private XRLabel xrLabel22;
    private XRLine xrLine13;
    private XRLabel xrLabel24;
    private XRLabel xrLabel26;
    private XRLine xrLine15;
    private XRLabel xrLabel25;
    private XRLine xrLine14;
    private XRLine xrLine16;
    private XRLabel xrLabel27;
    private XRLabel xrLabel28;
    private XRLine xrLine17;
    private XRLine xrLine19;
    private XRLabel xrLabel29;
    private XRLine xrLine18;
    private XRLabel xrLabel30;
    private XRLine xrLine20;
    private XRLabel xrLabel31;
    private XRLabel xrLabel32;
    private XRLine xrLine21;
    private XRLine xrLine22;
    private XRLine xrLine23;
    private XRLabel xrLabel33;
    private XRLabel xrLabel35;
    private XRLabel xrLabel36;
    private XRLine xrLine24;
    private XRLabel xrLabel38;
    private XRLine xrLine26;
    private XRLabel xrLabel37;
    private XRLine xrLine25;
    private XRLabel xrLabel39;
    private XRLabel xrLabel40;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell11;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell10;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell8;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell6;
    private XRTableCell xrTableCell5;
    private XRTable xrTable3;
    private XRTableRow xrTableRow4;
    private XRTableCell xrTableCell15;
    private XRTableCell xrTableCell17;
    private XRTableCell xrTableCell18;
    private XRTableCell xrTableCell19;
    private XRTableCell xrTableCell20;
    private XRTableCell xrTableCell21;
    private XRTableCell xrTableCell22;
    private XRTableCell xrTableCell23;
    private XRTableCell xrTableCell24;
    private XRTableCell xrTableCell25;
    private XRLabel xrLabel41;
    private XRTable xrTable4;
    private XRTableRow xrTableRow5;
    private XRTableCell xrTableCell29;
    private XRTableCell xrTableCell31;
    private XRLabel xrLabel42;
    private XRLabel xrLabel5;
    private XRPageInfo xrPageInfo1;
    private XRLabel xrLabel55;
    private XRLabel xrLabel56;
    private DevExpress.XtraReports.Parameters.Parameter nSumCapacity;
    private DevExpress.XtraReports.Parameters.Parameter sCompName;
    private DevExpress.XtraReports.Parameters.Parameter sVEH_NO;
    private DevExpress.XtraReports.Parameters.Parameter sTruckID;
    private DevExpress.XtraReports.Parameters.Parameter sSerialNumber;
    private DevExpress.XtraReports.Parameters.Parameter sTU_CHASSIS;
    private DevExpress.XtraReports.Parameters.Parameter sCarType;
    private DevExpress.XtraReports.Parameters.Parameter sTU_NO;
    private DevExpress.XtraReports.Parameters.Parameter sBRAND;
    private DevExpress.XtraReports.Parameters.Parameter sVEH_CHASSIS;
    private DevExpress.XtraReports.Parameters.Parameter nNumSlot;
    private DevExpress.XtraReports.Parameters.Parameter nSum_Capacity;
    private DevExpress.XtraReports.Parameters.Parameter nWeight_Sum;
    private DevExpress.XtraReports.Parameters.Parameter sNB;
    private DevExpress.XtraReports.Parameters.Parameter dLastCheckWater;
    private DevExpress.XtraReports.Parameters.Parameter sHeight_H2T;
    private DevExpress.XtraReports.Parameters.Parameter sHeight_EndT;
    private DevExpress.XtraReports.Parameters.Parameter sTypeMaterailTank;
    private DevExpress.XtraReports.Parameters.Parameter sAddress;
    private DevExpress.XtraReports.Parameters.Parameter dService;
    private DevExpress.XtraReports.Parameters.Parameter sTime_dService;
    private DevExpress.XtraReports.Parameters.Parameter sTime_StopCheckWater;
    private DevExpress.XtraReports.Parameters.Parameter sTime_ReturnWater;
    private DevExpress.XtraReports.Parameters.Parameter nSumTimeChecking;
    private DevExpress.XtraReports.Parameters.Parameter dNextCheckWater;
    private DevExpress.XtraReports.Parameters.Parameter dCompletCheckWater;
    private XRLabel xrLabel66;
    private XRLabel xrLabel65;
    private XRLabel xrLabel64;
    private XRLabel xrLabel63;
    private XRLabel xrLabel62;
    private XRLabel xrLabel61;
    private XRLabel xrLabel60;
    private XRLabel xrLabel59;
    private XRLabel xrLabel58;
    private XRLabel xrLabel57;
    private XRLabel xrLabel75;
    private XRLabel xrLabel74;
    private XRLabel xrLabel73;
    private XRLabel xrLabel72;
    private XRLabel xrLabel71;
    private XRLabel xrLabel70;
    private XRLabel xrLabel69;
    private XRLabel xrLabel68;
    private XRLabel xrLabel67;
    private XRLabel xrLabel81;
    private XRLabel xrLabel80;
    private XRLabel xrLabel79;
    private XRLabel xrLabel78;
    private XRLabel xrLabel77;
    private XRLabel xrLabel76;
    private XRPictureBox xrPictureBox1;
    private XRLabel xrlblNo2;
    private XRLabel xrlblNo1;
    private XRLabel xrLabel51;
    private XRLine xrLine1;
    private XRLabel xrLabel50;
    private XRLabel xrLabel34;
    private DevExpress.XtraReports.Parameters.Parameter sTime_DownWater;
    private XRLine xrLine27;
    private XRLabel xrlblComment;
    private XRLabel xrLabel53;
    private ds_rpt_InnerChecking ds_rpt_InnerChecking1;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell12;
    private XRTableCell xrTableCell13;
    private XRLabel xrLabel52;
    private XRLabel xrLabel54;
    private XRTableCell xrTableCell3;
    private XRTableCell xrTableCell16;
    private XRTableCell xrTableCell14;
    private XRTable xrTable7;
    private XRTableRow xrTableRow8;
    private XRTableCell xrTableCell33;
    private XRLabel xrlblsYear2;
    private XRLabel xrlblsMonth2;
    private XRLabel xrLabel49;
    private XRLabel xrLabel45;
    private XRLabel xrlblsDay2;
    private XRLabel xrLabel48;
    private XRLabel xrblbPositionControl;
    private XRLabel xrLabel47;
    private XRLabel xrlblFullNameControl;
    private XRLabel xrLabel46;
    private XRTable xrTable6;
    private XRTableRow xrTableRow7;
    private XRTableCell xrTableCell32;
    private XRTable xrTable5;
    private XRTableRow xrTableRow6;
    private XRTableCell xrTableCell28;
    private XRLabel xrLabel43;
    private XRPanel xrPanel1;
    private XRLabel xrLabel1;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public rpt_InnerChecking_FM001()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            string resourceFileName = "rpt_InnerChecking_FM001.resx";
            System.Resources.ResourceManager resources = global::Resources.rpt_InnerChecking_FM001.ResourceManager;
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ds_rpt_InnerChecking1 = new ds_rpt_InnerChecking();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTime_DownWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblNo2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblNo1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.sVEH_NO = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.sVEH_CHASSIS = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTypeMaterailTank = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTU_CHASSIS = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTU_NO = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTruckID = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTime_StopCheckWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTime_ReturnWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.sTime_dService = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.sSerialNumber = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.sNB = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.sHeight_H2T = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.sHeight_EndT = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.sCompName = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.sCarType = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.sBRAND = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.sAddress = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.nWeight_Sum = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.nSumTimeChecking = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.nSum_Capacity = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.nNumSlot = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.dService = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.dNextCheckWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.dLastCheckWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.dCompletCheckWater = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine27 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine22 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine21 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.rxlblsYear = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblsDay = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblsMonth = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblsYear2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblsMonth2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblsDay2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrblbPositionControl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblFullNameControl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblComment = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.nSumCapacity = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_rpt_InnerChecking1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.9776658F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(783.9996F, 25F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell3,
            this.xrTableCell16,
            this.xrTableCell14});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.COMPART_NO")});
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 0.29673034667968745D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.PAN_LEVEL")});
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Text = "xrTableCell17";
            this.xrTableCell17.Weight = 0.32190093994140639D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.NCAPACITY", "{0:#,#}")});
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Weight = 0.64999999999999991D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.HEIGHT1", "{0:#,#}")});
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Weight = 0.61999999999999988D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.HEIGHT2", "{0:#,#}")});
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Weight = 0.62000000000000011D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.EMER_VALUE")});
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.49999999999999994D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.SEAL_NO")});
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Weight = 0.70000000000000018D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.SEAL_ERR")});
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.6D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.NEW_SEAL_NO")});
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Weight = 0.6000000000000002D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "dt1.LEAD_SEAL")});
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.69999876025280872D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.ds_rpt_InnerChecking1, "dt1.MANHOLD_NO1")});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.60000027728971328D;
            // 
            // ds_rpt_InnerChecking1
            // 
            this.ds_rpt_InnerChecking1.DataSetName = "ds_rpt_InnerChecking";
            this.ds_rpt_InnerChecking1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.ds_rpt_InnerChecking1, "dt1.MANHOLD_NO2")});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "xrTableCell16";
            this.xrTableCell16.Weight = 0.60000027728971328D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 1.0313653098489257D;
            // 
            // TopMargin
            // 
            this.TopMargin.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.TopMargin.HeightF = 14F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.StylePriority.UseFont = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.BottomMargin.HeightF = 4F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.StylePriority.UseFont = false;
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel54,
            this.xrTable2,
            this.xrLabel52,
            this.xrLabel34,
            this.xrLabel51,
            this.xrLine1,
            this.xrLabel50,
            this.xrlblNo2,
            this.xrlblNo1,
            this.xrPictureBox1,
            this.xrLabel81,
            this.xrLabel80,
            this.xrLabel79,
            this.xrLabel78,
            this.xrLabel77,
            this.xrLabel76,
            this.xrLabel75,
            this.xrLabel74,
            this.xrLabel73,
            this.xrLabel72,
            this.xrLabel71,
            this.xrLabel70,
            this.xrLabel69,
            this.xrLabel68,
            this.xrLabel67,
            this.xrLabel66,
            this.xrLabel65,
            this.xrLabel64,
            this.xrLabel63,
            this.xrLabel62,
            this.xrLabel61,
            this.xrLabel60,
            this.xrLabel59,
            this.xrLabel58,
            this.xrLabel57,
            this.xrLabel5,
            this.xrTable1,
            this.xrLabel40,
            this.xrLine27,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLine26,
            this.xrLabel37,
            this.xrLine25,
            this.xrLabel36,
            this.xrLine24,
            this.xrLabel35,
            this.xrLine23,
            this.xrLabel33,
            this.xrLine22,
            this.xrLabel32,
            this.xrLine21,
            this.xrLabel31,
            this.xrLine20,
            this.xrLabel30,
            this.xrLine18,
            this.xrLabel29,
            this.xrLine19,
            this.xrLabel28,
            this.xrLine17,
            this.xrLabel27,
            this.xrLine16,
            this.xrLabel26,
            this.xrLine15,
            this.xrLabel25,
            this.xrLine14,
            this.xrLabel24,
            this.xrLine13,
            this.xrLabel23,
            this.xrLine12,
            this.xrLabel22,
            this.xrLine11,
            this.xrLabel21,
            this.xrLine10,
            this.xrLabel20,
            this.xrLine9,
            this.xrLabel19,
            this.xrLine8,
            this.xrLabel18,
            this.xrLine7,
            this.xrLabel17,
            this.xrLine6,
            this.xrLabel16,
            this.xrLine5,
            this.xrLabel15,
            this.xrLine4,
            this.xrLabel14,
            this.xrLine3,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.rxlblsYear,
            this.xrlblsDay,
            this.xrlblsMonth,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabel2});
            this.ReportHeader.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.ReportHeader.HeightF = 439.308F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(305.7872F, 86.0445F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(186.0536F, 23F);
            this.xrLabel1.Text = "รายงานผลการสอบเทียบรถบรรทุกน้ำมัน";
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(681.8407F, 367.808F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(103.1366F, 71.5F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "หมายเหตุ";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(561.8407F, 401.808F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(120F, 37.5F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell13});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.0368962738147587D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "ที่ 1";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 0.59999999999999987D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "ที่ 2";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.59999999999999942D;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(561.8407F, 367.808F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(120F, 35F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "หมายเลขซีลฝา Manhold";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTime_DownWater, "Text", "")});
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(387.5285F, 288.1226F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(68.73926F, 23F);
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "xrLabel34";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sTime_DownWater
            // 
            this.sTime_DownWater.Description = "เวลาลงน้ำ";
            this.sTime_DownWater.Name = "sTime_DownWater";
            // 
            // xrLabel51
            // 
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(632.5621F, 288.1226F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(152.4152F, 23F);
            this.xrLabel51.Text = "น.  เชื่อมแป้น / ตรวจสอบแป้น";
            // 
            // xrLine1
            // 
            this.xrLine1.LineWidth = 0;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(387.5285F, 299.7647F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(68.73926F, 11.35785F);
            // 
            // xrLabel50
            // 
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(342.7757F, 288.1226F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(44.75275F, 23F);
            this.xrLabel50.Text = "น.  ลงน้ำ";
            // 
            // xrlblNo2
            // 
            this.xrlblNo2.LocationFloat = new DevExpress.Utils.PointFloat(123.8699F, 20.54796F);
            this.xrlblNo2.Name = "xrlblNo2";
            this.xrlblNo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblNo2.SizeF = new System.Drawing.SizeF(67.67131F, 23F);
            // 
            // xrlblNo1
            // 
            this.xrlblNo1.LocationFloat = new DevExpress.Utils.PointFloat(51.19859F, 20.54796F);
            this.xrlblNo1.Name = "xrlblNo1";
            this.xrlblNo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblNo1.SizeF = new System.Drawing.SizeF(67.67131F, 23F);
            this.xrlblNo1.StylePriority.UseTextAlignment = false;
            this.xrlblNo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(337.5F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(121.0314F, 68.86331F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel81
            // 
            this.xrLabel81.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sVEH_NO, "Text", "")});
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(113.4804F, 196.1225F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(123.7719F, 23F);
            this.xrLabel81.Text = "[Parameters.sVEH_NO]";
            // 
            // sVEH_NO
            // 
            this.sVEH_NO.Name = "sVEH_NO";
            // 
            // xrLabel80
            // 
            this.xrLabel80.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sVEH_CHASSIS, "Text", "")});
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(500.0997F, 196.1226F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(284.9003F, 23F);
            this.xrLabel80.Text = "[Parameters.sVEH_CHASSIS]";
            // 
            // sVEH_CHASSIS
            // 
            this.sVEH_CHASSIS.Name = "sVEH_CHASSIS";
            // 
            // xrLabel79
            // 
            this.xrLabel79.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTypeMaterailTank, "Text", "")});
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(573.7122F, 242.1225F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(211.3101F, 23.00002F);
            this.xrLabel79.Text = "[Parameters.sTypeMaterailTank]";
            // 
            // sTypeMaterailTank
            // 
            this.sTypeMaterailTank.Description = "ชนิดวัสดุที่ใช้ทำถัง";
            this.sTypeMaterailTank.Name = "sTypeMaterailTank";
            // 
            // xrLabel78
            // 
            this.xrLabel78.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTU_CHASSIS, "Text", "")});
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(218.8699F, 173.1225F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(168.6586F, 23F);
            this.xrLabel78.Text = "[Parameters.sTU_CHASSIS]";
            // 
            // sTU_CHASSIS
            // 
            this.sTU_CHASSIS.Name = "sTU_CHASSIS";
            // 
            // xrLabel77
            // 
            this.xrLabel77.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTU_NO, "Text", "")});
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(500.0997F, 150.1225F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(111.1571F, 23.00002F);
            this.xrLabel77.Text = "[Parameters.sTU_NO]";
            // 
            // sTU_NO
            // 
            this.sTU_NO.Name = "sTU_NO";
            // 
            // xrLabel76
            // 
            this.xrLabel76.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTruckID, "Text", "")});
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(648.7568F, 150.1226F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(136.2432F, 23F);
            this.xrLabel76.Text = "[Parameters.sTruckID]";
            // 
            // sTruckID
            // 
            this.sTruckID.Name = "sTruckID";
            // 
            // xrLabel75
            // 
            this.xrLabel75.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTime_StopCheckWater, "Text", "")});
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(554.8367F, 288.1226F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(78.16742F, 23F);
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "[Parameters.sTime_StopCheckWater]";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sTime_StopCheckWater
            // 
            this.sTime_StopCheckWater.Description = "หยุดวัดน้ำ/แป้น";
            this.sTime_StopCheckWater.Name = "sTime_StopCheckWater";
            // 
            // xrLabel74
            // 
            this.xrLabel74.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTime_ReturnWater, "Text", "")});
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(139.1198F, 313.1226F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(105.0068F, 23.00003F);
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.Text = "[Parameters.sTime_ReturnWater]";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sTime_ReturnWater
            // 
            this.sTime_ReturnWater.Description = "วัดแห้ง/สูบน้ำคืน";
            this.sTime_ReturnWater.Name = "sTime_ReturnWater";
            // 
            // xrLabel73
            // 
            this.xrLabel73.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sTime_dService, "Text", "")});
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(257.4846F, 288.1226F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(83.41437F, 23F);
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "[Parameters.sTime_dService]";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sTime_dService
            // 
            this.sTime_dService.Description = "นำรถเข้าตรวจ";
            this.sTime_dService.Name = "sTime_dService";
            // 
            // xrLabel72
            // 
            this.xrLabel72.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sSerialNumber, "Text", "")});
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(51.19866F, 173.1225F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel72.Text = "[Parameters.sSerialNumber]";
            // 
            // sSerialNumber
            // 
            this.sSerialNumber.Name = "sSerialNumber";
            // 
            // xrLabel71
            // 
            this.xrLabel71.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sNB, "Text", "")});
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(473.8347F, 219.1225F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(99.87756F, 23F);
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "[Parameters.sNB]";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sNB
            // 
            this.sNB.Description = "นบ.";
            this.sNB.Name = "sNB";
            // 
            // xrLabel70
            // 
            this.xrLabel70.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sHeight_H2T, "Text", "")});
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(151.1986F, 242.1225F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(106.2859F, 23F);
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = "[Parameters.sHeight_H2T]";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sHeight_H2T
            // 
            this.sHeight_H2T.Description = "ความสูงจากพิ้นถึงหลังถัง  ที่หัวถัง";
            this.sHeight_H2T.Name = "sHeight_H2T";
            // 
            // xrLabel69
            // 
            this.xrLabel69.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sHeight_EndT, "Text", "")});
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(337.5F, 242.1227F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(106.2859F, 23F);
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "[Parameters.sHeight_EndT]";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // sHeight_EndT
            // 
            this.sHeight_EndT.Description = "ที่ท้ายถัง";
            this.sHeight_EndT.Name = "sHeight_EndT";
            // 
            // xrLabel68
            // 
            this.xrLabel68.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sCompName, "Text", "")});
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(201.1003F, 150.1225F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(238.8278F, 23.00002F);
            this.xrLabel68.Text = "[Parameters.sCompName]";
            // 
            // sCompName
            // 
            this.sCompName.Name = "sCompName";
            // 
            // xrLabel67
            // 
            this.xrLabel67.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sCarType, "Text", "")});
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(453.8408F, 173.1225F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(322.1592F, 23F);
            this.xrLabel67.Text = "[Parameters.sCarType]";
            // 
            // sCarType
            // 
            this.sCarType.Name = "sCarType";
            // 
            // xrLabel66
            // 
            this.xrLabel66.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sBRAND, "Text", "")});
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(281.7769F, 196.1225F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(130.3839F, 23F);
            this.xrLabel66.Text = "[Parameters.sBRAND]";
            // 
            // sBRAND
            // 
            this.sBRAND.Name = "sBRAND";
            // 
            // xrLabel65
            // 
            this.xrLabel65.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.sAddress, "Text", "")});
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(30.6507F, 265.1226F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(754.3492F, 23F);
            this.xrLabel65.Text = "[Parameters.sAddress]";
            // 
            // sAddress
            // 
            this.sAddress.Name = "sAddress";
            // 
            // xrLabel64
            // 
            this.xrLabel64.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.nWeight_Sum, "Text", "")});
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(312.1608F, 219.1225F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "[Parameters.nWeight_Sum]";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // nWeight_Sum
            // 
            this.nWeight_Sum.Description = "นร.";
            this.nWeight_Sum.Name = "nWeight_Sum";
            // 
            // xrLabel63
            // 
            this.xrLabel63.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.nSumTimeChecking, "Text", "")});
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(342.7757F, 313.1226F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "[Parameters.nSumTimeChecking]";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // nSumTimeChecking
            // 
            this.nSumTimeChecking.Description = "เวลารวมทั้งหมด";
            this.nSumTimeChecking.Name = "nSumTimeChecking";
            // 
            // xrLabel62
            // 
            this.xrLabel62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.nSum_Capacity, "Text", "")});
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(181.4925F, 219.1225F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(75.99199F, 23F);
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "[Parameters.nSum_Capacity]";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // nSum_Capacity
            // 
            this.nSum_Capacity.Description = "ความจุ(ลิตร)";
            this.nSum_Capacity.Name = "nSum_Capacity";
            // 
            // xrLabel61
            // 
            this.xrLabel61.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.nNumSlot, "Text", "")});
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(62.84079F, 219.1225F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(56.02913F, 23F);
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "[Parameters.nNumSlot]";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // nNumSlot
            // 
            this.nNumSlot.Description = "จำนวนช่อง";
            this.nNumSlot.Name = "nNumSlot";
            // 
            // xrLabel60
            // 
            this.xrLabel60.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.dService, "Text", "")});
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(51.19866F, 288.1226F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(106.2859F, 23F);
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "[Parameters.dService]";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // dService
            // 
            this.dService.Description = "นัดวัดน้ำ";
            this.dService.Name = "dService";
            // 
            // xrLabel59
            // 
            this.xrLabel59.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.dNextCheckWater, "Text", "")});
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(569.6182F, 313.1226F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(122.1418F, 23.00003F);
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "[Parameters.dNextCheckWater]";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // dNextCheckWater
            // 
            this.dNextCheckWater.Name = "dNextCheckWater";
            // 
            // xrLabel58
            // 
            this.xrLabel58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.dLastCheckWater, "Text", "")});
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(678.6972F, 219.1225F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(106.3028F, 22.99998F);
            this.xrLabel58.Text = "[Parameters.dLastCheckWater]";
            // 
            // dLastCheckWater
            // 
            this.dLastCheckWater.Description = "วัดน้ำครั้งสุดท้าย";
            this.dLastCheckWater.Name = "dLastCheckWater";
            // 
            // xrLabel57
            // 
            this.xrLabel57.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.dCompletCheckWater, "Text", "")});
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(409.8081F, 336.1227F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(125.8795F, 23F);
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "[Parameters.dCompletCheckWater]";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // dCompletCheckWater
            // 
            this.dCompletCheckWater.Name = "dCompletCheckWater";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(51.19863F, 20.54795F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(149.9016F, 23F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "________/______";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.9776611F, 367.808F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(560.863F, 69F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell11,
            this.xrTableCell2,
            this.xrTableCell10,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell4,
            this.xrTableCell6,
            this.xrTableCell5});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Text = "ช่อง\r\nที่";
            this.xrTableCell1.Weight = 0.29672988891601559D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Multiline = true;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "ระดับ\r\nแป้นที่";
            this.xrTableCell11.Weight = 0.32190078735351579D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "พิกัดแป้น\r\n(ลิตร)";
            this.xrTableCell2.Weight = 0.64999999999999991D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = "จากปากถัง\r\n ถึงแป้น\r\n (มม.)";
            this.xrTableCell10.Weight = 0.62D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "จากก้นถัง \r\nถึงแป้น\r\n(มม.)";
            this.xrTableCell7.Weight = 0.61999999999999988D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Text = "วาล์ว\r\nฉุกเฉิน";
            this.xrTableCell8.Weight = 0.49999999999999994D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Text = "หมายเลขซีล\r\nแป้นเดิม";
            this.xrTableCell9.Weight = 0.70000000000000029D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Text = "สภาพ\r\nซีลเดิม\r\nชำรุด";
            this.xrTableCell4.Weight = 0.6D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Text = "หมายเลข\r\nซีลใหม่";
            this.xrTableCell6.Weight = 0.6D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Text = "หมายเลขซีล\r\nตะกั่ว\r\n";
            this.xrTableCell5.Weight = 0.70000000000000062D;
            // 
            // xrLabel40
            // 
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(535.6877F, 336.1227F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(143.0095F, 23F);
            this.xrLabel40.Text = "ดังผลการตรวจสอบดังต่อไปนี้";
            // 
            // xrLine27
            // 
            this.xrLine27.LineWidth = 0;
            this.xrLine27.LocationFloat = new DevExpress.Utils.PointFloat(401.3481F, 347.7647F);
            this.xrLine27.Name = "xrLine27";
            this.xrLine27.SizeF = new System.Drawing.SizeF(125.8795F, 11.35785F);
            // 
            // xrLabel39
            // 
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(0F, 336.1227F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(412.0382F, 23F);
            this.xrLabel39.Text = "สภาพรถ (ตามแบบตรวจ ฯ แนบ) วัดน้ำติดระดับแป้น/เทียบระดับแป้น/เทียบแป้น เสร็จ วันที" +
    "่";
            // 
            // xrLabel38
            // 
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(691.7599F, 313.1227F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(93.21735F, 22.99997F);
            this.xrLabel38.Text = " ได้ทำการตรวจสอบ";
            // 
            // xrLine26
            // 
            this.xrLine26.LineWidth = 0;
            this.xrLine26.LocationFloat = new DevExpress.Utils.PointFloat(569.6182F, 324.7647F);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.SizeF = new System.Drawing.SizeF(122.1419F, 11.35785F);
            // 
            // xrLabel37
            // 
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(448.089F, 313.1227F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(121.5292F, 23F);
            this.xrLabel37.Text = "ชม. กำหนดวัดน้ำครั้งต่อไป";
            // 
            // xrLine25
            // 
            this.xrLine25.LineWidth = 0;
            this.xrLine25.LocationFloat = new DevExpress.Utils.PointFloat(342.7757F, 324.7647F);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.SizeF = new System.Drawing.SizeF(101.0102F, 11.35785F);
            // 
            // xrLabel36
            // 
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(250.8945F, 313.1227F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(90.00449F, 22.99997F);
            this.xrLabel36.Text = "น.    รวมทั้งหมด";
            // 
            // xrLine24
            // 
            this.xrLine24.LineWidth = 0;
            this.xrLine24.LocationFloat = new DevExpress.Utils.PointFloat(138.8407F, 324.7648F);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.SizeF = new System.Drawing.SizeF(106.2859F, 11.35785F);
            // 
            // xrLabel35
            // 
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(0F, 313.1226F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(138.8407F, 23.00006F);
            this.xrLabel35.Text = "ตีซีล/วัดแห้ง / สูบน้ำกลับคืน";
            // 
            // xrLine23
            // 
            this.xrLine23.LineWidth = 0;
            this.xrLine23.LocationFloat = new DevExpress.Utils.PointFloat(554.8367F, 299.7647F);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.SizeF = new System.Drawing.SizeF(78.16742F, 11.35788F);
            // 
            // xrLabel33
            // 
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(457.2677F, 288.1226F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(97.56894F, 23F);
            this.xrLabel33.Text = "น.  หยุดน้ำ / วัดแป้น";
            // 
            // xrLine22
            // 
            this.xrLine22.LineWidth = 0;
            this.xrLine22.LocationFloat = new DevExpress.Utils.PointFloat(257.4846F, 299.7647F);
            this.xrLine22.Name = "xrLine22";
            this.xrLine22.SizeF = new System.Drawing.SizeF(83.41437F, 11.35788F);
            // 
            // xrLabel32
            // 
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(163.4846F, 288.1226F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(93.99995F, 23F);
            this.xrLabel32.Text = "นำรถเข้าตราจสอบ";
            // 
            // xrLine21
            // 
            this.xrLine21.LineWidth = 0;
            this.xrLine21.LocationFloat = new DevExpress.Utils.PointFloat(51.19863F, 299.7647F);
            this.xrLine21.Name = "xrLine21";
            this.xrLine21.SizeF = new System.Drawing.SizeF(106.2859F, 11.35785F);
            // 
            // xrLabel31
            // 
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 288.1226F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(51.19864F, 23F);
            this.xrLabel31.Text = "นัดวัดน้ำ";
            // 
            // xrLine20
            // 
            this.xrLine20.LineWidth = 0;
            this.xrLine20.LocationFloat = new DevExpress.Utils.PointFloat(30.65069F, 276.7647F);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.SizeF = new System.Drawing.SizeF(755.3493F, 11.35785F);
            // 
            // xrLabel30
            // 
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(0F, 265.1226F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(30.65069F, 23F);
            this.xrLabel30.Text = "ที่อยู่\r\n";
            // 
            // xrLine18
            // 
            this.xrLine18.LineWidth = 0;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(573.7123F, 253.7647F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(212.2876F, 11.35785F);
            // 
            // xrLabel29
            // 
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(449.08F, 242.1226F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(124.6322F, 23F);
            this.xrLabel29.Text = "มม.      ชนิดวัสดุที่ใช้ทำถัง";
            // 
            // xrLine19
            // 
            this.xrLine19.LineWidth = 0;
            this.xrLine19.LocationFloat = new DevExpress.Utils.PointFloat(337.5F, 253.7647F);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.SizeF = new System.Drawing.SizeF(106.2859F, 11.35785F);
            // 
            // xrLabel28
            // 
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(260.4846F, 242.1226F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(77.01541F, 23F);
            this.xrLabel28.Text = "มม.     ที่ท้ายถัง";
            // 
            // xrLine17
            // 
            this.xrLine17.LineWidth = 0;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(151.1986F, 253.7647F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(106.2859F, 11.35785F);
            // 
            // xrLabel27
            // 
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(4.674874E-06F, 242.1226F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(151.1986F, 23F);
            this.xrLabel27.Text = "ความสูงจากพื้นถึงหลังถัง ที่หัวถัง";
            // 
            // xrLine16
            // 
            this.xrLine16.LineWidth = 0;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(678.6972F, 230.7647F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(107.3027F, 11.35783F);
            // 
            // xrLabel26
            // 
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(573.7122F, 219.1226F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(104.985F, 23F);
            this.xrLabel26.Text = "กก.   วัดน้ำครั้งสุดท้าย";
            // 
            // xrLine15
            // 
            this.xrLine15.LineWidth = 0;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(473.8347F, 230.7647F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(99.87744F, 11.35783F);
            // 
            // xrLabel25
            // 
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(422.1585F, 219.1226F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(51.67621F, 23F);
            this.xrLabel25.Text = "กก.   นบ.";
            // 
            // xrLine14
            // 
            this.xrLine14.LineWidth = 0;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(312.1608F, 230.7647F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(99.87744F, 11.35783F);
            // 
            // xrLabel24
            // 
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(260.4846F, 219.1226F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(51.67621F, 23F);
            this.xrLabel24.Text = "ลิตร   นร.";
            // 
            // xrLine13
            // 
            this.xrLine13.LineWidth = 0;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(181.4925F, 230.7647F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(75.99211F, 11.35783F);
            // 
            // xrLabel23
            // 
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(118.8699F, 219.1226F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(62.62259F, 23F);
            this.xrLabel23.Text = "ช่อง  ความจุ";
            // 
            // xrLine12
            // 
            this.xrLine12.LineWidth = 0;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(62.84079F, 230.7647F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(50.63961F, 11.35783F);
            // 
            // xrLabel22
            // 
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 219.1226F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(62.84079F, 23F);
            this.xrLabel22.Text = "จำนวนช่อง";
            // 
            // xrLine11
            // 
            this.xrLine11.LineWidth = 0;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(500.8401F, 207.7647F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(285.1599F, 11.35783F);
            // 
            // xrLabel21
            // 
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(422.1585F, 196.1226F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(77.94116F, 23F);
            this.xrLabel21.Text = "หมายเลขแชชซีย์";
            // 
            // xrLine10
            // 
            this.xrLine10.LineWidth = 0;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(281.6543F, 207.7647F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(130.3839F, 11.35783F);
            // 
            // xrLabel20
            // 
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(250.8945F, 196.1225F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(30.7598F, 23.00002F);
            this.xrLabel20.Text = "ยี่ห้อ";
            // 
            // xrLine9
            // 
            this.xrLine9.LineWidth = 0;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(113.4804F, 207.7647F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(123.7719F, 11.35783F);
            // 
            // xrLabel19
            // 
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 196.1226F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(113.4804F, 23F);
            this.xrLabel19.Text = "หัวลากเทเลอร์ทะเบียน";
            // 
            // xrLine8
            // 
            this.xrLine8.LineWidth = 0;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(451.08F, 184.7647F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(334.9199F, 11.35783F);
            // 
            // xrLabel18
            // 
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(392.8055F, 173.1226F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(58.27448F, 23.00002F);
            this.xrLabel18.Text = "ประเภทรถ";
            // 
            // xrLine7
            // 
            this.xrLine7.LineWidth = 0;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(218.8699F, 184.7647F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(168.6586F, 11.35783F);
            // 
            // xrLabel17
            // 
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(163.4846F, 173.1226F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(55.38525F, 23F);
            this.xrLabel17.Text = "เลขแชชซีย์";
            // 
            // xrLine6
            // 
            this.xrLine6.LineWidth = 0;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(51.19863F, 184.7647F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(100F, 11.35783F);
            // 
            // xrLabel16
            // 
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 173.1225F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(51.19863F, 23F);
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "เลขเครื่อง";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.LineWidth = 0;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(648.7568F, 161.7647F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(137.2432F, 11.35783F);
            // 
            // xrLabel15
            // 
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(621.6734F, 150.1226F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(27.08337F, 23F);
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "รหัส";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LineWidth = 0;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(500.84F, 161.7647F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(110.4167F, 11.35783F);
            // 
            // xrLabel14
            // 
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(439.9281F, 150.1225F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(60.17157F, 23F);
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "ทะเบียนรถ";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LineWidth = 0;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(201.1003F, 161.7647F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(224.1219F, 11.35783F);
            // 
            // xrLabel13
            // 
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(51.19865F, 150.1225F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(149.9016F, 23F);
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "ด้วยรถบรรทุกน้ามัน ขนส่งในนาม";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(51.19864F, 109.0445F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(186.0536F, 23.00001F);
            this.xrLabel12.Text = "ผจ.มว.";
            // 
            // xrLabel11
            // 
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 109.0445F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(30.6507F, 23.00001F);
            this.xrLabel11.Text = "เรียน";
            // 
            // xrLabel10
            // 
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(51.19863F, 86.04451F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(186.0536F, 23F);
            this.xrLabel10.Text = "การตรวจสอบปริมาตรรถบรรทุกน้ำมัน";
            // 
            // xrLabel9
            // 
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 86.04452F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(30.6507F, 23F);
            this.xrLabel9.Text = "เรื่อง";
            // 
            // rxlblsYear
            // 
            this.rxlblsYear.LocationFloat = new DevExpress.Utils.PointFloat(737.222F, 58.00285F);
            this.rxlblsYear.Name = "rxlblsYear";
            this.rxlblsYear.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.rxlblsYear.SizeF = new System.Drawing.SizeF(45.86957F, 23F);
            this.rxlblsYear.StylePriority.UseTextAlignment = false;
            this.rxlblsYear.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrlblsDay
            // 
            this.xrlblsDay.LocationFloat = new DevExpress.Utils.PointFloat(593.7586F, 58.00285F);
            this.xrlblsDay.Name = "xrlblsDay";
            this.xrlblsDay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblsDay.SizeF = new System.Drawing.SizeF(28.08209F, 23F);
            this.xrlblsDay.StylePriority.UseTextAlignment = false;
            this.xrlblsDay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblsMonth
            // 
            this.xrlblsMonth.LocationFloat = new DevExpress.Utils.PointFloat(648.7568F, 58.00285F);
            this.xrlblsMonth.Name = "xrlblsMonth";
            this.xrlblsMonth.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblsMonth.SizeF = new System.Drawing.SizeF(65.33362F, 23F);
            this.xrlblsMonth.StylePriority.UseTextAlignment = false;
            this.xrlblsMonth.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(714.0905F, 58.00285F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(23.58737F, 23F);
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "พ.ศ.";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(621.8407F, 58.00285F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(28.7243F, 23F);
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "เดือน";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(573.3819F, 58.00285F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(20.37668F, 23F);
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "วัน";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.54795F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(51.19863F, 23F);
            this.xrLabel4.Text = "เลขที่";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(574.337F, 23F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(199.9193F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "ส่วนเครื่องมือวัดและระบบควบคุมอัตโนมัติ";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel56,
            this.xrPageInfo1,
            this.xrLabel55});
            this.PageFooter.HeightF = 23F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel56
            // 
            this.xrLabel56.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(637.5428F, 0F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(737.5428F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(48.45721F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel55.StylePriority.UseFont = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrLabel53,
            this.xrlblComment,
            this.xrLabel42,
            this.xrTable4,
            this.xrLabel41});
            this.ReportFooter.HeightF = 287.5161F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.StylePriority.UseBorders = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6,
            this.xrTable5,
            this.xrLabel43,
            this.xrTable7});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0.9777148F, 111.4584F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(785.0223F, 166.0577F);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable6.KeepTogether = true;
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 96.05773F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable6.SizeF = new System.Drawing.SizeF(323.8408F, 70F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UsePadding = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrTableCell32.Multiline = true;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "\r\nผจ.มว. อนุมัติ        ....................................   ........./........" +
    ".../..........";
            this.xrTableCell32.Weight = 3D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable5.KeepTogether = true;
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.05778F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable5.SizeF = new System.Drawing.SizeF(323.8408F, 70F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UsePadding = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrTableCell28.Multiline = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "เจ้าหน้าที่ ปตท.ผู้ตรวจสอบ\r\nตรวจสอบ / ถูกต้อง ..................................." +
    ".   ........./.........../..........";
            this.xrTableCell28.Weight = 3D;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel43.KeepTogether = true;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.057766F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(126.348F, 23.00001F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.Text = "จึงเรียนมาเพื่อโปรดทราบ";
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.xrTable7.KeepTogether = true;
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(459.2958F, 26.05769F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable7.SizeF = new System.Drawing.SizeF(323.8408F, 140F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UsePadding = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell33});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblsYear2,
            this.xrlblsMonth2,
            this.xrLabel49,
            this.xrLabel45,
            this.xrlblsDay2,
            this.xrLabel48,
            this.xrblbPositionControl,
            this.xrLabel47,
            this.xrlblFullNameControl,
            this.xrLabel46});
            this.xrTableCell33.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrTableCell33.Multiline = true;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseFont = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell33.Weight = 3D;
            // 
            // xrlblsYear2
            // 
            this.xrlblsYear2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblsYear2.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrlblsYear2.LocationFloat = new DevExpress.Utils.PointFloat(186.6653F, 93.99997F);
            this.xrlblsYear2.Name = "xrlblsYear2";
            this.xrlblsYear2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblsYear2.SizeF = new System.Drawing.SizeF(39.93219F, 23F);
            this.xrlblsYear2.StylePriority.UseBorders = false;
            this.xrlblsYear2.StylePriority.UseFont = false;
            // 
            // xrlblsMonth2
            // 
            this.xrlblsMonth2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblsMonth2.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrlblsMonth2.LocationFloat = new DevExpress.Utils.PointFloat(139.6563F, 93.99997F);
            this.xrlblsMonth2.Name = "xrlblsMonth2";
            this.xrlblsMonth2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblsMonth2.SizeF = new System.Drawing.SizeF(39.93219F, 23F);
            this.xrlblsMonth2.StylePriority.UseBorders = false;
            this.xrlblsMonth2.StylePriority.UseFont = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(0.02249479F, 116F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(322.8406F, 23.00002F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "ผู้ควบคุมการปฏิบัติงาน";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.99997F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(322.8406F, 23.00002F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "...........................................................................";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrlblsDay2
            // 
            this.xrlblsDay2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblsDay2.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrlblsDay2.LocationFloat = new DevExpress.Utils.PointFloat(102.5529F, 94.00002F);
            this.xrlblsDay2.Name = "xrlblsDay2";
            this.xrlblsDay2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblsDay2.SizeF = new System.Drawing.SizeF(30.93219F, 23F);
            this.xrlblsDay2.StylePriority.UseBorders = false;
            this.xrlblsDay2.StylePriority.UseFont = false;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(0F, 92.99998F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(322.8406F, 23.00002F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = ".........../.............../..............";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrblbPositionControl
            // 
            this.xrblbPositionControl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrblbPositionControl.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrblbPositionControl.LocationFloat = new DevExpress.Utils.PointFloat(0.02252197F, 69.99994F);
            this.xrblbPositionControl.Name = "xrblbPositionControl";
            this.xrblbPositionControl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrblbPositionControl.SizeF = new System.Drawing.SizeF(322.818F, 22.99994F);
            this.xrblbPositionControl.StylePriority.UseBorders = false;
            this.xrblbPositionControl.StylePriority.UseFont = false;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(1F, 69.99988F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(322.8406F, 23.00002F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "........................................................";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrlblFullNameControl
            // 
            this.xrlblFullNameControl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblFullNameControl.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrlblFullNameControl.LocationFloat = new DevExpress.Utils.PointFloat(3.576279E-05F, 46.99995F);
            this.xrlblFullNameControl.Name = "xrlblFullNameControl";
            this.xrlblFullNameControl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFullNameControl.SizeF = new System.Drawing.SizeF(322.818F, 22.99994F);
            this.xrlblFullNameControl.StylePriority.UseBorders = false;
            this.xrlblFullNameControl.StylePriority.UseFont = false;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(0F, 46.99988F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(322.8406F, 23.00002F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "(....................................................................)";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(52.22135F, 62.84313F);
            this.xrLabel53.Multiline = true;
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(733.7787F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrlblComment
            // 
            this.xrlblComment.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrlblComment.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrlblComment.LocationFloat = new DevExpress.Utils.PointFloat(51.19859F, 73.25982F);
            this.xrlblComment.Multiline = true;
            this.xrlblComment.Name = "xrlblComment";
            this.xrlblComment.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblComment.SizeF = new System.Drawing.SizeF(733.7787F, 23F);
            this.xrlblComment.StylePriority.UseBorders = false;
            this.xrlblComment.StylePriority.UseFont = false;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(0F, 62.84313F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(51.19861F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.Text = "บันทึก";
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0.9777031F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable4.SizeF = new System.Drawing.SizeF(127.8631F, 25F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell31});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "รวม";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 1.3461850943121636D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.nSumCapacity, "Text", "")});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell31.Weight = 1.4362068789017419D;
            // 
            // nSumCapacity
            // 
            this.nSumCapacity.Name = "nSumCapacity";
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.Font = new System.Drawing.Font("TH Sarabun New", 12F);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(0.9777125F, 24.99999F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(646.77F, 23F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "ระยะที่ระบุนี้ มีไว้เพื่อการตรวจสอบ ความคลาดเคลื่อนระดับแป้นเท่านั้น จะไม่ถือเป็น" +
    "ปริมาตรความจุทางเชิงพาณิชย์ต่อเนื่องกับบุคคลอื่น";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // rpt_InnerChecking_FM001
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(26, 15, 14, 4);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.nSumCapacity,
            this.sCompName,
            this.sVEH_NO,
            this.sTruckID,
            this.sSerialNumber,
            this.sTU_CHASSIS,
            this.sCarType,
            this.sTU_NO,
            this.sBRAND,
            this.sVEH_CHASSIS,
            this.nNumSlot,
            this.nSum_Capacity,
            this.nWeight_Sum,
            this.sNB,
            this.dLastCheckWater,
            this.sHeight_H2T,
            this.sHeight_EndT,
            this.sTypeMaterailTank,
            this.sAddress,
            this.dService,
            this.sTime_dService,
            this.sTime_StopCheckWater,
            this.sTime_ReturnWater,
            this.nSumTimeChecking,
            this.dNextCheckWater,
            this.dCompletCheckWater,
            this.sTime_DownWater});
            this.Version = "14.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_rpt_InnerChecking1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion
}
