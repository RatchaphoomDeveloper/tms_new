﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
/// <summary>
/// Summary description for TTRUCKCONFIRMLIST
/// </summary>
public class TTRUCKCONFIRMLIST
{
    OracleConnection conn;
    public TTRUCKCONFIRMLIST(OracleConnection _conn)
    {
        conn = _conn;
    }
    #region Data
    private string fNLISTNO;
    private string fNCONFIRMID;
    private string fSTRUCKID;
    private string fSHEADID;
    private string fSHEADREGISTERNO;
    private string fSTRAILERID;
    private string fSTRAILERREGISTERNO;
    private string fCCONFIRM;
    private string fCSTANBY;
    private string fCPASSED;
    private string fDCREATE;
    private string fSCREATE;
    private string fDUPDATE;
    private string fSUPDATE;
    private string fSPERSONALCODE;
    private string fDELIVERY_PART;
    private string fCAR_STATUS;
    private string fWAREHOUSE;
    private string fDELIVERY_NUM;
    private string fGPS_STATUS;
    private string fCCTV_STATUS;
    private string fLAT_LONG;
    private string fKM2PLANT;
    private string fHR2PLANT;
    private string fADDRESS_CURRENT;
    private string fREMARK;
    #endregion
    #region Property
    public string NLISTNO
    {
        get { return this.fNLISTNO; }
        set { this.fNLISTNO = CommonFunction.ReplaceInjection(value); }
    }
    public string NCONFIRMID
    {
        get { return this.fNCONFIRMID; }
        set { this.fNCONFIRMID = CommonFunction.ReplaceInjection(value); }
    }
    public string STRUCKID
    {
        get { return this.fSTRUCKID; }
        set { this.fSTRUCKID = CommonFunction.ReplaceInjection(value); }
    }
    public string SHEADID
    {
        get { return this.fSHEADID; }
        set { this.fSHEADID = CommonFunction.ReplaceInjection(value); }
    }
    public string SHEADREGISTERNO
    {
        get { return this.fSHEADREGISTERNO; }
        set { this.fSHEADREGISTERNO = CommonFunction.ReplaceInjection(value); }
    }
    public string STRAILERID
    {
        get { return this.fSTRAILERID; }
        set { this.fSTRAILERID = CommonFunction.ReplaceInjection(value); }
    }
    public string STRAILERREGISTERNO
    {
        get { return this.fSTRAILERREGISTERNO; }
        set { this.fSTRAILERREGISTERNO = CommonFunction.ReplaceInjection(value); }
    }
    public string CCONFIRM
    {
        get { return this.fCCONFIRM; }
        set { this.fCCONFIRM = CommonFunction.ReplaceInjection(value); }
    }
    public string CSTANBY
    {
        get { return this.fCSTANBY; }
        set { this.fCSTANBY = CommonFunction.ReplaceInjection(value); }
    }
    public string CPASSED
    {
        get { return this.fCPASSED; }
        set { this.fCPASSED = CommonFunction.ReplaceInjection(value); }
    }
    public string DCREATE
    {
        get { return this.fDCREATE; }
        set { this.fDCREATE = CommonFunction.ReplaceInjection(value); }
    }
    public string SCREATE
    {
        get { return this.fSCREATE; }
        set { this.fSCREATE = CommonFunction.ReplaceInjection(value); }
    }
    public string DUPDATE
    {
        get { return this.fDUPDATE; }
        set { this.fDUPDATE = CommonFunction.ReplaceInjection(value); }
    }
    public string SUPDATE
    {
        get { return this.fSUPDATE; }
        set { this.fSUPDATE = CommonFunction.ReplaceInjection(value); }
    }
    public string SPERSONALCODE
    {
        get { return this.fSPERSONALCODE; }
        set { this.fSPERSONALCODE = CommonFunction.ReplaceInjection(value); }
    }
    public string DELIVERY_PART
    {
        get { return this.fDELIVERY_PART; }
        set { this.fDELIVERY_PART = CommonFunction.ReplaceInjection(value); }
    }
    public string CAR_STATUS
    {
        get { return this.fCAR_STATUS; }
        set { this.fCAR_STATUS = CommonFunction.ReplaceInjection(value); }
    }
    public string WAREHOUSE
    {
        get { return this.fWAREHOUSE; }
        set { this.fWAREHOUSE = CommonFunction.ReplaceInjection(value); }
    }
    public string DELIVERY_NUM
    {
        get { return this.fDELIVERY_NUM; }
        set { this.fDELIVERY_NUM = CommonFunction.ReplaceInjection(value); }
    }
    public string GPS_STATUS
    {
        get { return this.fGPS_STATUS; }
        set { this.fGPS_STATUS = CommonFunction.ReplaceInjection(value); }
    }
    public string CCTV_STATUS
    {
        get { return this.fCCTV_STATUS; }
        set { this.fCCTV_STATUS = CommonFunction.ReplaceInjection(value); }
    }
    public string LAT_LONG
    {
        get { return this.fLAT_LONG; }
        set { this.fLAT_LONG = CommonFunction.ReplaceInjection(value); }
    }
    public string KM2PLANT
    {
        get { return this.fKM2PLANT; }
        set { this.fKM2PLANT = CommonFunction.ReplaceInjection(value); }
    }
    public string HR2PLANT
    {
        get { return this.fHR2PLANT; }
        set { this.fHR2PLANT = CommonFunction.ReplaceInjection(value); }
    }
    public string ADDRESS_CURRENT
    {
        get { return this.fADDRESS_CURRENT; }
        set { this.fADDRESS_CURRENT = CommonFunction.ReplaceInjection(value); }
    }
    public string REMARK
    {
        get { return this.fREMARK; }
        set { this.fREMARK = CommonFunction.ReplaceInjection(value); }
    }
    #endregion
    public int Insert()
    {
        int result = -1;
        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        //
        OracleCommand cmd = new OracleCommand("IU_TTRUCKCONFIRMLIST", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("N_LISTNO", NLISTNO);
        cmd.Parameters.AddWithValue("N_CONFIRMID", NCONFIRMID);
        cmd.Parameters.AddWithValue("S_STRUCKID", STRUCKID);
        cmd.Parameters.AddWithValue("S_HEADID", SHEADID);
        cmd.Parameters.AddWithValue("S_HEADREGISTERNO", SHEADREGISTERNO);
        cmd.Parameters.AddWithValue("S_TRAILERID", STRAILERID);
        cmd.Parameters.AddWithValue("S_TRAILERREGISTERNO", STRAILERREGISTERNO);
        cmd.Parameters.AddWithValue("C_CONFIRM", CCONFIRM);
        cmd.Parameters.AddWithValue("C_STANBY", CSTANBY);
        cmd.Parameters.AddWithValue("C_CPASSED", CPASSED);
        cmd.Parameters.AddWithValue("D_CREATE", DCREATE);
        cmd.Parameters.AddWithValue("S_CREATE", SCREATE);
        cmd.Parameters.AddWithValue("D_UPDATE", DUPDATE);
        cmd.Parameters.AddWithValue("S_UPDATE", SUPDATE);
        cmd.Parameters.AddWithValue("S_PERSONALCODE", SPERSONALCODE);
        cmd.Parameters.AddWithValue("I_DELIVERY_PART", DELIVERY_PART);
        cmd.Parameters.AddWithValue("I_CAR_STATUS", CAR_STATUS);
        cmd.Parameters.AddWithValue("I_WAREHOUSE", WAREHOUSE);
        cmd.Parameters.AddWithValue("I_DELIVERY_NUM", DELIVERY_NUM);
        cmd.Parameters.AddWithValue("I_GPS_STATUS", GPS_STATUS);
        cmd.Parameters.AddWithValue("I_CCTV_STATUS", CCTV_STATUS);
        cmd.Parameters.AddWithValue("I_LAT_LONG", LAT_LONG);
        cmd.Parameters.AddWithValue("I_KM2PLANT", KM2PLANT);
        cmd.Parameters.AddWithValue("I_HR2PLANT", HR2PLANT);
        cmd.Parameters.AddWithValue("I_ADDRESS_CURRENT", ADDRESS_CURRENT);
        cmd.Parameters.AddWithValue("I_REMARK", REMARK);

        result = cmd.ExecuteNonQuery();
        return result;
    }
    public int CommitChild()
    {
        int result = -1;
        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        //
        OracleCommand cmd = new OracleCommand("UPDATE TTRUCKCONFIRM SET CCONFIRM='1' WHERE NCONFIRMID='" + NCONFIRMID + "' ", conn);
        cmd.CommandType = CommandType.Text;

        result = cmd.ExecuteNonQuery();
        return result;
    }

    public int CommitConfirmAmount(string _CONFIRMID, int _Amount, bool _Mode)
    {
        int result = -1;
        // Reopen connection if close
        if (conn.State == ConnectionState.Closed) conn.Open();
        // 
        OracleCommand cmd = new OracleCommand("UPDATE TTRUCKCONFIRM SET " + ((_Mode) ? "B4_DEADLINE ='" + _Amount + "'" : "AF_DEADLINE ='" + _Amount + "'") + " WHERE NCONFIRMID='" + _CONFIRMID + "' ", conn);
        cmd.CommandType = CommandType.Text;

        result = cmd.ExecuteNonQuery();
        return result;
    }
}