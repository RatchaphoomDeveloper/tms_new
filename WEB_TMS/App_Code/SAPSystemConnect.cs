﻿using System;
using System.Collections.Generic;

using System.Web;
using SAP.Middleware.Connector;
using System.Configuration;

//namespace RcfSAPProxy
//{

public class SAPSystemConnect : IDestinationConfiguration
{
    public RfcConfigParameters GetParameters(string destinationName)
    {
        RfcConfigParameters param = new RfcConfigParameters();
        switch (destinationName)
        {
            case "TMS":
                param.Add(RfcConfigParameters.AppServerHost, "" + ConfigurationSettings.AppSettings["RFC_HOST"]);//10.120.2.40  //ConfigurationSettings.AppSettings["alerttrycatch"]
                param.Add(RfcConfigParameters.SystemID, "" + ConfigurationSettings.AppSettings["RFC_SYSTEMID"]);// "ECP"
                param.Add(RfcConfigParameters.SystemNumber, "" + ConfigurationSettings.AppSettings["RFC_INSTANCE"]);// "01"
                param.Add(RfcConfigParameters.User, "" + ConfigurationSettings.AppSettings["RFC_USR"]);//"0024500048"
                param.Add(RfcConfigParameters.Password, "" + ConfigurationSettings.AppSettings["RFC_PWD"]);//"eDIFICE01"
                param.Add(RfcConfigParameters.Client, "" + ConfigurationSettings.AppSettings["RFC_CLIENT"]);// "202"
                param.Add(RfcConfigParameters.Language, "" + ConfigurationSettings.AppSettings["RFC_LANG"]);//"EN"
                param.Add(RfcConfigParameters.PoolSize, "" + ConfigurationSettings.AppSettings["RFC_PoolSize"]);
                param.Add(RfcConfigParameters.MaxPoolSize, "" + ConfigurationSettings.AppSettings["RFC_MaxPoolSize"]);
                param.Add(RfcConfigParameters.IdleTimeout, "" + ConfigurationSettings.AppSettings["RFC_IdleTimeout"]);
                break;
            case "iTerminal":
                param.Add(RfcConfigParameters.AppServerHost, "10.120.2.40");
                param.Add(RfcConfigParameters.SystemNumber, "01");
                param.Add(RfcConfigParameters.User, "0024500048");
                param.Add(RfcConfigParameters.Password, "eDIFICE01");
                param.Add(RfcConfigParameters.Client, "202");
                param.Add(RfcConfigParameters.Language, "EN");
                //param.Add(RfcConfigParameters.PoolSize, "5");
                //param.Add(RfcConfigParameters.MaxPoolSize, "10");
                //param.Add(RfcConfigParameters.IdleTimeout, "600");
                break;
        }

        //throw new NotImplementedException();
        return param;
    }

    public bool ChangeEventsSupported()
    {
        // throw new NotImplementedException();
        return false;
    }
    public event RfcDestinationManager.ConfigurationChangeHandler ConfigurationChanged;
}
//}