﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Caching;

using DevExpress.Web.ASPxClasses.Internal;

public static class UploadingUtils {
    public static void RemoveFileWithDelay(string key, string fullPath, int delay) {
        if(HttpUtils.GetCache()[key] == null) {
            DateTime absoluteExpiration = DateTime.Now.Add(new TimeSpan(0, delay, 0));
            HttpUtils.GetCache().Insert(key, fullPath, null, absoluteExpiration,
                Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, new CacheItemRemovedCallback(RemovedCallback));
        }
    }
    public static void RemovedCallback(string key, object value, CacheItemRemovedReason reason) {
        if (File.Exists(value.ToString()))
            File.Delete(value.ToString());
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="fullPath"></param>
    /// <param name="delay"></param>
    /// <param name="UploadedFile"></param>
    public static void RemoveFileWithDelay(string key, string fullPath, int delay, DevExpress.Web.ASPxUploadControl.UploadedFile uploadedFile)
    {
        if (HttpUtils.GetCache()[key] == null)
        {
            DateTime absoluteExpiration = DateTime.Now.Add(new TimeSpan(0, delay, 0));
            HttpUtils.GetCache().Insert(key, fullPath, null, absoluteExpiration,
                Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, new CacheItemRemovedCallback(RemovedCallback));
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <param name="reason"></param>
    /// <param name="UploadedFile"></param>
    public static void RemovedCallback(string key, object value, CacheItemRemovedReason reason, DevExpress.Web.ASPxUploadControl.UploadedFile uploadedFile)
    {
        if (File.Exists(value.ToString()))
        {
            string sPath = Path.GetDirectoryName(value.ToString())
                , sFileName = Path.GetFileNameWithoutExtension(value.ToString())
                , sFileType = Path.GetExtension(value.ToString());
            int nExists = Directory.GetFiles(Path.GetDirectoryName(value.ToString()), Path.GetFileNameWithoutExtension(value.ToString()) + "*").Length;
            sPath = sPath + "\\" + sFileName + "(" + nExists + ")" + sFileType;
            File.Copy(value.ToString(), sPath, true);
            uploadedFile.SaveAs(value.ToString());
            //File.Delete(value.ToString());
        }
    }
}
