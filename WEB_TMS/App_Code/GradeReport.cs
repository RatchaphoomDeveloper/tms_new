﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for GradeReport
/// </summary>
public class GradeReport : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
	private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private ReportHeaderBand ReportHeader;
    private XRLabel xrLabel3;
    private XRLabel xrLabel2;
    private XRLabel xrLabel1;
    private XRLabel xrLabel12;
    private XRLabel xrLabel11;
    private XRLabel xrLabel10;
    private XRLabel xrLabel9;
    private XRLabel xrLabel8;
    private XRLabel xrLabel7;
    private XRLabel xrLabel6;
    private XRLabel xrLabel5;
    private ReportFooterBand ReportFooter;
    private XRLabel xrLabel14;
    private XRLabel xrLabel13;
    private XRLabel xrLabel17;
    private XRLabel xrLabel16;
    private XRLabel xrLabel15;
    private XRLabel xrLabel20;
    private XRLabel xrLabel19;
    private XRLabel xrLabel18;
    private XRLabel xrLabel21;
    private dsTest dsTest1;
    private XRLabel xrLabel22;
    private XRLabel xrLabel23;
    private XRLabel xrLabel4;
    protected DevExpress.XtraReports.Parameters.Parameter CONTRACT;
    private XRLabel xrLabel24;
    protected DevExpress.XtraReports.Parameters.Parameter COUNTCONTRACT;
    protected DevExpress.XtraReports.Parameters.Parameter VENDORNAME;
    private XRLabel xrLabel25;
    private XRLabel xrLabel26;
    protected DevExpress.XtraReports.Parameters.Parameter YEAR;
    private XRLabel xrDate;
    private XRLabel xrLabel27;
    private XRLabel xrLabel28;
    private XRLabel xrLabel30;
    private XRLabel xrLabel29;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public GradeReport()
	{
		InitializeComponent();
		//
		// TODO: Add constructor logic here
		//
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "GradeReport.resx";
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrDate = new DevExpress.XtraReports.UI.XRLabel();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.YEAR = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.VENDORNAME = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.COUNTCONTRACT = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.CONTRACT = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.dsTest1 = new dsTest();
        ((System.ComponentModel.ISupportInitialize)(this.dsTest1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9});
        this.Detail.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.Detail.HeightF = 28.125F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.StylePriority.UseFont = false;
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel23
        // 
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(568.9584F, 0F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel23.SizeF = new System.Drawing.SizeF(71.16669F, 23F);
        this.xrLabel23.Text = "[SGRADENAME]";
        // 
        // xrLabel22
        // 
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(431.875F, 0F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(34.375F, 23F);
        this.xrLabel22.Text = "[NGRADE]";
        // 
        // xrLabel21
        // 
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(128.7501F, 0F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(191.6667F, 23F);
        this.xrLabel21.Text = "[SCONTRACTNO]";
        // 
        // xrLabel12
        // 
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(513.7501F, 0F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(55.20831F, 23F);
        this.xrLabel12.Text = "ได้เกรด";
        // 
        // xrLabel11
        // 
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(466.25F, 0F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(46.04184F, 23F);
        this.xrLabel11.Text = "คะแนน";
        // 
        // xrLabel10
        // 
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(331.875F, 0F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel10.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel10.Text = "ผลการประเมินได้";
        // 
        // xrLabel9
        // 
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel9.SizeF = new System.Drawing.SizeF(118.7501F, 23F);
        this.xrLabel9.Text = "อ้างถึง เลขที่สัญญา";
        // 
        // TopMargin
        // 
        this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel27,
            this.xrDate});
        this.TopMargin.HeightF = 129.25F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel27
        // 
        this.xrLabel27.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 72.50002F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel27.SizeF = new System.Drawing.SizeF(128.7501F, 23F);
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.Text = "ที่........................";
        // 
        // xrDate
        // 
        this.xrDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrDate.LocationFloat = new DevExpress.Utils.PointFloat(592F, 96.25002F);
        this.xrDate.Name = "xrDate";
        this.xrDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrDate.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrDate.StylePriority.UseFont = false;
        this.xrDate.Text = "16/01/2556";
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 44F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // ReportHeader
        // 
        this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel28,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel24,
            this.xrLabel4,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
        this.ReportHeader.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.ReportHeader.HeightF = 256.25F;
        this.ReportHeader.Name = "ReportHeader";
        this.ReportHeader.StylePriority.UseFont = false;
        // 
        // xrLabel28
        // 
        this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.YEAR, "Text", "")});
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(470.4167F, 191.5417F);
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel28.Text = "xrLabel26";
        // 
        // YEAR
        // 
        this.YEAR.Name = "YEAR";
        // 
        // xrLabel26
        // 
        this.xrLabel26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.YEAR, "Text", "")});
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(241.25F, 10.00001F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel26.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel26.Text = "xrLabel26";
        // 
        // xrLabel25
        // 
        this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.VENDORNAME, "Text", "")});
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(154.7917F, 43.70832F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(358.9584F, 23F);
        this.xrLabel25.Text = "xrLabel25";
        // 
        // VENDORNAME
        // 
        this.VENDORNAME.Name = "VENDORNAME";
        // 
        // xrLabel24
        // 
        this.xrLabel24.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.COUNTCONTRACT, "Text", "")});
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(306.7916F, 77.08334F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(25.08337F, 23F);
        this.xrLabel24.Text = "xrLabel24";
        // 
        // COUNTCONTRACT
        // 
        this.COUNTCONTRACT.Name = "COUNTCONTRACT";
        // 
        // xrLabel4
        // 
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.CONTRACT, "Text", "")});
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(51.58345F, 114.4583F);
        this.xrLabel4.Multiline = true;
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(588.5416F, 23F);
        this.xrLabel4.Text = "xrLabel4";
        // 
        // CONTRACT
        // 
        this.CONTRACT.Name = "CONTRACT";
        // 
        // xrLabel8
        // 
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 223.25F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(477.5F, 23F);
        this.xrLabel8.Text = "ท่านได้รับผลการประเมิน  โดยมีรายละเอียด ดังนี้";
        // 
        // xrLabel7
        // 
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(51.66667F, 191.5417F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(418.75F, 23F);
        this.xrLabel7.Text = "บริษัท ปตท. จำกัด (มหาชน)  ขอแจ้งผลการปฏิบัติงานขนส่งประจำ ปี พ.ศ.";
        // 
        // xrLabel6
        // 
        this.xrLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 147.875F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(321.875F, 23F);
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.Text = "สิ่งที่ส่งมาด้วย    บันทึกผลการประเมิน ประจำไตรมาส";
        // 
        // xrLabel5
        // 
        this.xrLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(331.875F, 77.08334F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(53.125F, 23F);
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.Text = "ฉบับ";
        // 
        // xrLabel3
        // 
        this.xrLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 77.08334F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(295.75F, 27.04166F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.Text = "อ้างถึง  สัญญาจ้างขนส่งผลิตภัณฑ์ปิโตรเลียม จำนวน";
        // 
        // xrLabel2
        // 
        this.xrLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 43.70832F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(144.7917F, 23F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.Text = "เรียน    กรรมการผู้จัดการ ";
        // 
        // xrLabel1
        // 
        this.xrLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(231.25F, 23F);
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.Text = "เรื่อง    แจ้งผลการประเมินประจำ ปี พ.ศ.";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel30,
            this.xrLabel29,
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13});
        this.ReportFooter.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.ReportFooter.HeightF = 404.1667F;
        this.ReportFooter.Name = "ReportFooter";
        this.ReportFooter.StylePriority.UseFont = false;
        // 
        // xrLabel30
        // 
        this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 118.7083F);
        this.xrLabel30.Name = "xrLabel30";
        this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel30.SizeF = new System.Drawing.SizeF(639.9999F, 23F);
        this.xrLabel30.Text = "ตามเอกสารแนบท้ายสัญญา ชุดที่";
        // 
        // xrLabel29
        // 
        this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(11.04174F, 40.54165F);
        this.xrLabel29.Name = "xrLabel29";
        this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel29.SizeF = new System.Drawing.SizeF(639.9999F, 23F);
        this.xrLabel29.Text = "ที่อ้างถึงเพิ่มเติม โดย ปตท. จะแจ้งสาเหตุการปรับเปลี่ยนคะแนนประเมินผลให้ท่านทราบเ" +
            "ป็นลายลักษณ์อักษรทุกกรณี";
        // 
        // xrLabel20
        // 
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 371.1667F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel20.SizeF = new System.Drawing.SizeF(171.25F, 23F);
        this.xrLabel20.Text = "โทรสาร    0-2239-7422";
        // 
        // xrLabel19
        // 
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 348.1667F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel19.SizeF = new System.Drawing.SizeF(171.25F, 23F);
        this.xrLabel19.Text = "โทรศัพท์   0-2239-7460";
        // 
        // xrLabel18
        // 
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 313.7917F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel18.SizeF = new System.Drawing.SizeF(218.75F, 23F);
        this.xrLabel18.Text = "ส่วนระบบการประเมินผลการขนส่ง";
        // 
        // xrLabel17
        // 
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(403.4166F, 270.125F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(237.7503F, 22.99998F);
        this.xrLabel17.Text = "ผู้จัดการส่วนระบบและประมวลผลการขนส่ง";
        // 
        // xrLabel16
        // 
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(460.4167F, 235.7499F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(120.8333F, 23F);
        this.xrLabel16.Text = "(.........................)";
        // 
        // xrLabel15
        // 
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(460.4167F, 173.9167F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(120.8333F, 23F);
        this.xrLabel15.Text = "ขอแสดงความนับถือ";
        // 
        // xrLabel14
        // 
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(11.04167F, 90.50001F);
        this.xrLabel14.Multiline = true;
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(640F, 17.79166F);
        this.xrLabel14.Text = "         จึงเรียนมาเพื่อแจ้งให้ทราบผลการประเมินของท่าน ซึ่งเป็นไปตามหลักเกณฑ์การป" +
            "ระเมิน ผลการปฏิบัติงาน";
        // 
        // xrLabel13
        // 
        this.xrLabel13.Font = new System.Drawing.Font("Tahoma", 9.75F);
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 10.00001F);
        this.xrLabel13.Multiline = true;
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(640F, 20.12498F);
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UsePadding = false;
        this.xrLabel13.Text = "         ทั้งนี้ ปตท. ขอสงวนสิทธิ์ปรับปรุงแก้ไขคะแนนประเมินผล หาก ปตท. ตรวจสอบพบข" +
            "้อมูลการปฏิบัติผิดสัญญา";
        // 
        // dsTest1
        // 
        this.dsTest1.DataSetName = "dsTest";
        this.dsTest1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // GradeReport
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.ReportFooter});
        this.Margins = new System.Drawing.Printing.Margins(66, 69, 129, 44);
        this.PageHeight = 1169;
        this.PageWidth = 827;
        this.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.CONTRACT,
            this.COUNTCONTRACT,
            this.VENDORNAME,
            this.YEAR});
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.dsTest1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion
}
