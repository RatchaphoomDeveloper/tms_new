﻿using System;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for ConvertDataSetToList
/// </summary>
public static class ConvertDataSetToList
{
    public static string Convert(DataSet ds)
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < ds.Tables.Count; i++)
            {
                for (int j = 0; j < ds.Tables[i].Rows.Count; j++)
                {
                    if (!string.Equals(ds.Tables[i].Rows[j]["EMAIL"].ToString(), string.Empty))
                    {
                        if (!string.Equals(sb.ToString(), string.Empty))
                            sb.Append(",");

                        sb.Append(ds.Tables[i].Rows[j]["EMAIL"].ToString());
                    }
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}