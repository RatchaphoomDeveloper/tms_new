﻿using System.Web.UI;
using System.Web.UI.WebControls;


/// <summary>
/// Summary description for FormHelper
/// </summary>
public class FormHelper
{
	public FormHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void setTarget(string url, string target, Page targetPage)
    {
        string strScript = "<script language=javascript1.2>" + "function fctRedirection() {" + "eval(\"window.open('" + url + "', target='" + target + "')\");}" + "fctRedirection();" + "</script>";
        targetPage.RegisterStartupScript("Target", strScript);
    }

    public static void Alert(string strAlert, Page tragetPage)
    {
        string strScript = "<SCRIPT language='javascript'>alert(\"" + strAlert + "\")</SCRIPT>";
        tragetPage.RegisterStartupScript("Alert", strScript);
    }

    public static void setFocus(string CtrlName, Page targetPage)
    {
        string strScript = "<SCRIPT language='javascript'>MyForm." + CtrlName + ".focus(); </SCRIPT>";
        targetPage.RegisterStartupScript("focus", strScript);
    }

    public static void comfirmMsg(Button btnConfirm, string Message)
    {
        btnConfirm.Attributes.Add("OnClick", "javascript:if(confirm('" + Message + "')" + "== false) return false;");
    }

    public static void ajaxFocus(string clientID, Page targetPage)
    {
        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "focus",
            "document.forms[0]." + clientID + ".focus();document.forms[0]." + clientID + ".select();", true);
    }

    public static void ajaxDdlFocus(string clientID, Page targetPage)
    {
        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "focus",
            "document.forms[0]." + clientID + ".focus();", true);
    }

    public static void ajaxAlert(string alert, Page targetPage)
    {
        string script = "alert(\"" + alert.Replace("'", "\'").Replace("\"", "\\\"").Replace("\r", "\\r").Replace("\n", "\\n") + "\");";
        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "jsCall", script, true);
    }

    public static void OpenForm(string path, Page targetPage)
    {

        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "open"
        , "window.open('" + path + "');", true);
    }

    public static void OpenForm(string path1, string Path2, Page targetPage)
    {

        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "open"
        , "window.open('" + path1 + "'); window.open('" + Path2 + "');", true);
    }

    public static void RedirectLogin(string pathLogin, string alert, Page targetPage)
    {
        ScriptManager.RegisterStartupScript(targetPage, targetPage.GetType(), "location", "alert(\"" + alert + "\");window.parent.location='" + pathLogin + "';", true);
    }
}