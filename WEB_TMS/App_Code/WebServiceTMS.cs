﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.Services.Protocols;
using Newtonsoft.Json;
using TMS_BLL.Master;
using TMS_BLL.Transaction.KM;
using TMS_BLL.WebService;
using System.Text.RegularExpressions;
using TMS_BLL.Transaction.SurpriseCheck;
using System.Data.OracleClient;
using System.ComponentModel;
using TMS_BLL.Transaction.Accident;
using System.Web.UI.WebControls;
using TMS_BLL.Transaction.SurpriseCheckYear;
using System.Web.Security;
using TMS_BLL.Transaction.Complain;
using EmailHelper;
using GemBox.Spreadsheet;


/// <summary>
/// Summary description for WebServiceTMS
/// </summary>
[WebService(Namespace = "https://ptttms.pttplc.com/TMSWebService/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebServiceTMS : System.Web.Services.WebService
{
    public TMSAuthentication AuthenticationInfo;
    public WebServiceTMS()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    string TempDirectory = "UploadFile/checktruck/{0}/{2}/{1}/";
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    DateTime dateCheck = new DateTime();
    string Topicc = string.Empty;
    int countLoop = 0;
    int MaximumRow = 100;
    string MaximimRowMessage = "Result data over {0} rows";
    string messSendEnail = string.Empty;
    protected string MessEmailSuccess = "ส่ง Email ถึงผู้เกี่ยวข้อง สำเร็จ<br/ >";
    protected string MessEmailFail = "<span style=\"color:Red;\">ส่ง Email ถึงผู้เกี่ยวข้อง ไม่สำเร็จ !!!</span><br/ >";

    #region CheckLogin
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Check User Login Web service")]
    public string CheckLogin(string user, string pw, string checkDomain)
    {
        string result = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("CheckLogin", user, pw, checkDomain);
                DataTable dt = AuthenHelper.CheckLogin(user, pw, ref result, checkDomain);
                if (string.IsNullOrEmpty(result) && dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    //DataSet ds = new DataSet("DS");
                    //ds.Tables.Add(dt);
                    int sid = Convert.ToInt32(dt.Rows[0]["SUID"]);
                    DataSet ds = WebServiceBLL.Instance.GetUserLogin(sid);
                    return JsonConvert.SerializeObject(ds, Formatting.Indented);
                }
            }
            return result;
        }
        catch (Exception ex)
        {
            //LogService.ExceptionLog(ex.Message);
            return ex.Message;
        }
    }
    #endregion

    #region GET_KM_SEARCH_CONDITION
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Search Condition for KM")]
    public string GET_KM_SEARCH_CONDITION()
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtKMType = GetKMType();
                DataTable dtTagHD = GetTagHD();
                DataTable dtTagItems = GetTagItems();
                DataTable dtDivision = GetDivision();
                DataTable dtDepartment = GetDepartment();
                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dtKMType);
                ds.Tables.Add(dtTagHD);
                ds.Tables.Add(dtTagItems);
                ds.Tables.Add(dtDivision);
                ds.Tables.Add(dtDepartment);
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_KM_LIST
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get KM")]
    public string GET_KM_LIST(string topicID = "", string authorName = "", string knowlwdgeTypeID = "", string tagHDID = "", string tagItemID = "", string departmentID = "", string divisionID = "", string keyWord = "", string year = "", string dateFrom = "", string dateTo = "", string userID = "", string FlagBookmark = "")
    {
        string condition = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = new DataSet();
                string defaultYear = "";
                if (string.IsNullOrEmpty(dateFrom) && string.IsNullOrEmpty(dateTo))
                {
                    if (string.IsNullOrEmpty(year))
                    {
                        CultureInfo ci = new CultureInfo("en-US");
                        defaultYear = DateTime.Now.ToString("yyyy", ci);
                    }
                    else
                    {
                        defaultYear = year;
                    }
                }

                condition = GenerateKMCondition(topicID, authorName, knowlwdgeTypeID, tagHDID, tagItemID, departmentID, divisionID, keyWord, defaultYear, dateFrom, dateTo, userID, FlagBookmark);
                ds = WebServiceBLL.Instance.GetKM(condition);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dtKM = ds.Tables[0];
                    List<string> listKMIDs = new List<string>();
                    string kmidIN = "";
                    foreach (DataRow drKM in dtKM.Rows)
                    {
                        string kmid = drKM["KM_ID"].ToString();
                        if (!listKMIDs.Any(it => it == kmid))
                        {
                            listKMIDs.Add(kmid);
                            kmidIN += kmid + ",";
                        }
                    }
                    kmidIN = kmidIN.Remove(kmidIN.Length - 1);
                    if (listKMIDs.Any())
                    {
                        DataTable dtBook = KMBLL.Instance.SelectKMBookBLL(" AND KM_ID IN(" + kmidIN + ") AND USER_ID = " + userID);
                        if (dtBook != null && dtBook.Rows != null && dtBook.Rows.Count > 0)
                        {
                            foreach (DataRow drKM in dtKM.Rows)
                            {
                                string kmid = drKM["KM_ID"].ToString();
                                foreach (DataRow drBook in dtBook.Rows)
                                {
                                    string kmid_ = drBook["KM_ID"].ToString();
                                    if (kmid_ == kmid)
                                    {
                                        drKM["IS_BOOKMARK"] = 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            DataSet ds = new DataSet();
            return JsonConvert.SerializeObject(ds, Formatting.Indented);
        }
    }
    #endregion

    #region GET_KM_DETAIL
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get KM DETAIL")]
    public string GET_KM_DETAIL(string topicID, string userID = "")
    {
        int step = 0;
        string condition = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = new DataSet();
                string fullPath = "";
                string downloadServerUrl = GetDownloadServerUrl();
                step = 1;
                if (!string.IsNullOrEmpty(topicID))
                {
                    condition += " AND TOPIC_ID = '" + topicID + "'";
                    step = 2;
                    ds = WebServiceBLL.Instance.GetKM(condition);
                    step = 3;
                    DataTable dtContentFile = GetContentFilePath(topicID);
                    step = 4;
                    DataTable dt = ds.Tables[0];
                    step = 5;
                    dt.Columns.Add("VIEW_TOTAL");
                    dt.Columns.Add("LIKES");
                    dt.Columns.Add("IS_LIKE");
                    step = 6;
                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            string km_id = dr["KM_ID"].ToString();
                            if (!string.IsNullOrEmpty(km_id))
                            {
                                DataTable dtView = KMBLL.Instance.InsertKMViewBLL(int.Parse(km_id));
                                step = 7;
                                DataTable dtCountLike = KMBLL.Instance.SelectLikeViewKMBLL(" AND KM_ID = " + km_id);
                                step = 8;
                                if (dtCountLike != null && dtCountLike.Rows != null && dtCountLike.Rows.Count > 0)
                                {
                                    dr["VIEW_TOTAL"] = dtCountLike.Rows[0]["VIEW_TOTAL"].ToString();
                                    dr["LIKES"] = dtCountLike.Rows[0]["LIKES"].ToString();
                                }
                                step = 9;
                            }
                        }
                    }

                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            DataTable dtUserLike = KMBLL.Instance.SelectUserLikeKMBLL(" AND T_KM_HEADER.TOPIC_ID = '" + topicID + "' AND T_KM_LIKE.USER_ID = '" + userID + "'");
                            if (dtUserLike != null && dtUserLike.Rows != null && dtUserLike.Rows.Count > 0)
                            {
                                dr["IS_LIKE"] = dtUserLike.Rows[0]["IS_LIKE"].ToString();
                            }
                            else
                            {
                                dr["IS_LIKE"] = "0";
                            }
                        }
                    }

                    step = 10;
                    if (dtContentFile != null && dtContentFile.Rows != null && dtContentFile.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtContentFile.Rows)
                        {
                            String originalPath = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)).OriginalString;
                            dr["FULLPATH"] = originalPath + "/TMS" + "/" + (dr["FULLPATH"].ToString().Replace("E:\\Appl\\TMS\\", ""));
                        }
                    }
                    ds.Tables.Add(dtContentFile);
                    step = 11;

                    return JsonConvert.SerializeObject(ds, Formatting.Indented);
                }
                else
                {
                    return JsonConvert.SerializeObject(ds, Formatting.Indented);
                }
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + step.ToString();
        }
    }
    #endregion

    #region UPDATE_LIKE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "UPDATE LIKE(1:Like, 2:Unlike)")]
    public string UPDATE_LIKE(string kmID, string likeStatus, string suid)
    {
        DataSet ds = new DataSet("DS");
        DataTable dtLikeResult = new DataTable("UpdateLikeResult");
        dtLikeResult.Columns.Add("KMID");
        dtLikeResult.Columns.Add("LikeStatus");
        dtLikeResult.Columns.Add("UpdateStatus");
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtLike = KMBLL.Instance.InsertKMLikeWebBLL(Convert.ToInt32(kmID), Convert.ToInt32(suid), Convert.ToInt32(likeStatus));
                if (dtLike.Rows.Count > 0)
                {
                    dtLikeResult.Rows.Add(kmID, likeStatus, 1);
                }
                else
                {
                    dtLikeResult.Rows.Add(kmID, likeStatus, 2);
                }

                dtLikeResult.Columns.Add("LIKES");
                if (dtLikeResult != null && dtLikeResult.Rows != null && dtLikeResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtLikeResult.Rows)
                    {
                        DataTable dtUserLike = KMBLL.Instance.SelectUserLikeKMBLL(" AND T_KM_HEADER.KM_ID = '" + kmID + "'");
                        if (dtUserLike != null && dtUserLike.Rows != null && dtUserLike.Rows.Count > 0)
                        {
                            dr["LIKES"] = dtUserLike.Rows[0]["IS_LIKE"].ToString();
                        }
                        else
                        {
                            dr["LIKES"] = "0";
                        }
                    }
                }

                ds.Tables.Add(dtLikeResult);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region UPDATE_BOOKMARK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "UPDATE BOOKMARK(1:Bookmark, 2:ยกเลิก Bookmark)")]
    public string UPDATE_BOOKMARK(string kmID, string bookmarkStatus, string suid)
    {
        DataSet ds = new DataSet("DS");
        DataTable dtBookMarkResult = new DataTable("UpdateBookmarkResult");
        dtBookMarkResult.Columns.Add("KMID");
        dtBookMarkResult.Columns.Add("BookmarkStatus");
        dtBookMarkResult.Columns.Add("UpdateStatus");
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtBookMark = KMBLL.Instance.InsertKMBookmarkBLL(Convert.ToInt32(kmID), Convert.ToInt32(suid), Convert.ToInt32(bookmarkStatus));
                if (dtBookMark.Rows.Count > 0)
                {
                    dtBookMarkResult.Rows.Add(kmID, bookmarkStatus, 1);
                }
                else
                {
                    dtBookMarkResult.Rows.Add(kmID, bookmarkStatus, 2);
                }
                ds.Tables.Add(dtBookMarkResult);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region UPDATE_COMMENT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "UPDATE COMMENT()")]
    public string UPDATE_COMMENT(string kmID, string action, string commentID, string commentText, string suid)
    {
        DataSet ds = new DataSet("DS");
        DataTable dtCommentResult = new DataTable("UpdateCommentResult");
        dtCommentResult.Columns.Add("KMID");
        dtCommentResult.Columns.Add("CommentID");
        dtCommentResult.Columns.Add("Action");
        dtCommentResult.Columns.Add("UpdateStatus");
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtComment;
                switch (action)
                {
                    case "1":
                        //List
                        dtComment = KMBLL.Instance.SelectKMCommentBLL(" AND KM_ID = " + kmID);
                        break;
                    case "2":
                        //Insert
                        dtComment = KMBLL.Instance.InsertKMCommentBLL(0, Convert.ToInt32(kmID), Convert.ToInt32(suid), 1, commentText);
                        break;
                    case "3":
                        //Update
                        dtComment = KMBLL.Instance.InsertKMCommentBLL(Convert.ToInt32(commentID), Convert.ToInt32(kmID), Convert.ToInt32(suid), 1, commentText);
                        break;
                    case "4":
                        //Delete
                        dtComment = KMBLL.Instance.InsertKMCommentBLL(Convert.ToInt32(commentID), Convert.ToInt32(kmID), Convert.ToInt32(suid), 0, commentText);
                        break;
                    default:
                        dtComment = KMBLL.Instance.SelectKMCommentBLL(" AND KM_ID = " + kmID);
                        break;
                }

                if (dtComment.Rows.Count > 0)
                {
                    dtCommentResult.Rows.Add(kmID, commentID, action, 1);
                    ds.Tables.Add(dtCommentResult);
                    if (action == "1")
                    {
                        dtComment.Columns.Add("CAN_EDIT");
                        for (int i = 0; i < dtComment.Rows.Count; i++)
                        {
                            dtComment.Rows[i]["CAN_EDIT"] = dtComment.Rows[i]["CREATE_BY"].ToString().Trim() == suid ? 1 : 0;
                        }
                        ds.Tables.Add(dtComment.Copy());
                    }
                }
                else
                {
                    dtCommentResult.Rows.Add(kmID, commentID, action, 2);
                    ds.Tables.Add(dtCommentResult);
                    ds.Tables.Add(dtComment.Copy());
                }
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_DRIVER_LIST
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Driver List")]
    public string GET_DRIVER_LIST(string vendorID = "", string permission = "", string workContract = "", string searchText = "", string documentTypeID = "", string documentExpiredDay = "", string documentExpiredDateFrom = "", string documentExpiredDateTo = "", string statusexpired = "")
    {
        string condition = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                condition = GenerateDriverCondition(vendorID, permission, workContract, searchText, documentTypeID, documentExpiredDay, documentExpiredDateFrom, documentExpiredDateTo, statusexpired);
                DataTable dt = VendorBLL.Instance.VendorWebServiceList(condition);
                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dt.Copy());
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + condition;
        }
    }
    #endregion

    #region GET_DRIVER_DETAIL
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Driver Detail")]
    public string GET_DRIVER_DETAIL(string CardID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = new DataSet("DS");
                if (!string.IsNullOrEmpty(CardID))
                {
                    DataTable dt = VendorBLL.Instance.DriverSelectWebBLL(CardID);
                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            String originalPath = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)).OriginalString;
                            dr["SPATH"] = originalPath + "/TMS" + "/" + dr["SPATH"].ToString() + "/" + dr["SSYSFILENAME"].ToString();
                        }
                        ds.Tables.Add(dt.Copy());
                    }
                    else
                    {
                        ds.Tables.Add(dt.Copy());
                    }
                }
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + CardID;
        }
    }
    #endregion

    #region GET_DRIVER_LOG
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Driver Log")]
    public string GET_DRIVER_LOG(string CardID = "")
    {
        string idCardNo = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = new DataSet("DS");
                if (!string.IsNullOrEmpty(CardID))
                {
                    DataTable dt = VendorBLL.Instance.DriverSelectWebBLL(CardID);
                    if (dt.Rows.Count > 0)
                    {
                        idCardNo = dt.Rows[0]["PERS_CODE"] + "";
                    }
                    DataTable dtEmployeeHistory = VendorBLL.Instance.EmployeeHistorySelect(idCardNo);
                    if (dtEmployeeHistory != null && dtEmployeeHistory.Rows != null && dtEmployeeHistory.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtEmployeeHistory.Rows)
                        {
                            dr["DETAIL"] = StripHTML(dr["DETAIL"].ToString());
                        }
                        ds.Tables.Add(dtEmployeeHistory.Copy());
                    }
                }
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + idCardNo;
        }
    }
    #endregion

    #region GET_TRUCK_LIST
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Truck List")]
    public string GET_TRUCK_LIST(string workgroup = "", string vendorID = "", string contract = "", string licensePlate = "", string permission = "", string truckAge = "", string documnettype = "", string documentExpire = "", string startdate = "", string stopdate = "", string statusexpired = "")
    {
        string condition = "";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                condition = GenerateTruckCondition(workgroup, vendorID, contract, licensePlate, permission, truckAge, documnettype, documentExpire, startdate, stopdate, statusexpired);
                DataSet ds = WebServiceBLL.Instance.GetTruckList(condition);
                DataTable dt = ds.Tables[0];
                DataTable dtResult = dt.Clone();
                if (dt.Rows.Count > 0)
                {
                    DataRow[] results = dt.Select("UPLOAD_ID = 344");

                    if (results.Any())
                    {
                        string[] listSTRUCKID = new string[results.Count()];
                        listSTRUCKID = new string[results.Count()];
                        int i = 0;
                        foreach (DataRow row in results)
                        {
                            dtResult.ImportRow(row);
                            listSTRUCKID[i] = row["STRUCKID"] + string.Empty;
                            i++;
                        }

                        EnumerableRowCollection<DataRow> drs = dt.AsEnumerable().Where(it => !listSTRUCKID.Contains(it["STRUCKID"] + string.Empty));
                        if (drs.Any())
                        {
                            DataTable dtnot = drs.CopyToDataTable();
                            if (dtnot.Rows.Count > 0)
                            {
                                dtnot = dtnot.AsEnumerable().GroupBy(r => new { Col1 = r["STRUCKID"] }).Select(it => it.First()).CopyToDataTable();
                                foreach (DataRow row in dtnot.Rows)
                                {
                                    dtResult.ImportRow(row);
                                }
                            }
                        }

                    }
                    else
                    {
                        dt = dt.AsEnumerable().GroupBy(r => new { Col1 = r["STRUCKID"] }).Select(it => it.First()).CopyToDataTable();
                        dtResult = dt.Copy();
                    }
                }
                DataSet dsConvert = new DataSet();
                dtResult.TableName = "TruckList";
                dsConvert.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsConvert, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + condition;
        }
    }
    #endregion

    private void CheckEmpty(ref DataTable dt)
    {
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (string.Equals(dt.Columns[j].DataType.FullName, "System.String") && string.Equals(dt.Rows[i][j].ToString(), string.Empty))
                {
                    dt.Rows[i][j] = "-";
                }
            }
        }
    }

    #region GET_TRUCK_DETAIL
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Truck Detail")]
    public string GET_TRUCK_DETAIL(string headLicensePlate = "", string tailLicensePlate = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetTruckDetail(headLicensePlate, tailLicensePlate);
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message + ":" + headLicensePlate + ":" + tailLicensePlate;
        }
    }
    #endregion

    #region GET_TRUCK_LOG
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Truck Log")]
    public string GET_TRUCK_LOG(string licensePlate = "", string licenseTrailPlate = "", string userID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                if (!string.IsNullOrEmpty(userID))
                {
                    DataSet dsUser = WebServiceBLL.Instance.GetUserLogin(Convert.ToInt32(userID));
                    if (dsUser != null && dsUser.Tables != null && dsUser.Tables.Count > 0)
                    {
                        DataTable dtUser = dsUser.Tables["User"];
                        if (CheckTableNotEmpty(dtUser))
                        {
                            string userGroup = dtUser.Rows[0]["USERGROUP_TYPE"].ToString();
                            if (userGroup == "PTT" || userGroup == "WAREHOUSE" || userGroup == "ADMIN" || userGroup == "BOSS" || userGroup == "BSA")
                            {
                                DataSet ds = WebServiceBLL.Instance.GetTruckLog(licensePlate, licenseTrailPlate);

                                DataTable dtTruckHeadHistory = ds.Tables["TruckLogHead"];
                                if (dtTruckHeadHistory != null && dtTruckHeadHistory.Rows != null && dtTruckHeadHistory.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtTruckHeadHistory.Rows)
                                    {
                                        dr["DETAIL"] = StripHTML(dr["DETAIL"].ToString().Replace("<br/>", " "));
                                    }
                                }
                                DataTable dtTruckTrailHistory = ds.Tables["TruckLogTrail"];
                                if (dtTruckTrailHistory != null && dtTruckTrailHistory.Rows != null && dtTruckTrailHistory.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtTruckTrailHistory.Rows)
                                    {
                                        dr["DETAIL"] = StripHTML(dr["DETAIL"].ToString().Replace("<br/>", " "));
                                    }
                                }
                                return JsonConvert.SerializeObject(ds, Formatting.Indented);
                            }
                            else
                            {
                                DataSet ds = new DataSet();
                                return JsonConvert.SerializeObject(ds, Formatting.Indented);
                            }
                        }
                        else
                        {
                            DataSet ds = new DataSet();
                            return JsonConvert.SerializeObject(ds, Formatting.Indented);
                        }
                    }
                    else
                    {
                        DataSet ds = new DataSet();
                        return JsonConvert.SerializeObject(ds, Formatting.Indented);
                    }
                }
                return "PTT User Only!!";
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            DataSet ds = new DataSet();
            return JsonConvert.SerializeObject(ds, Formatting.Indented);
        }
    }
    #endregion

    #region GET_SHIP_TO
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Ship To")]
    public string GET_SHIP_TO(string ShipToName)
    {
        string condition = string.Empty;
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                condition = " AND SHIP_ID LIKE '%" + ShipToName + "%' OR UPPER(SHIP_NAME) LIKE '%" + ShipToName.ToUpper() + "%'";
                DataSet ds = WebServiceBLL.Instance.GetShipToList(condition);

                if (ds.Tables[0].Rows.Count > MaximumRow)
                    throw new Exception(string.Format(MaximimRowMessage, MaximumRow.ToString()));

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_MATERIAL
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Material")]
    public string GET_MATERIAL(string Material_Name)
    {
        string condition = string.Empty;
        try
        {
            //if (AuthenticationWebService(AuthenticationInfo))
            //{
                condition = " AND MATERIAL_NAME LIKE '%" + Material_Name + "%' OR UPPER(MATERIAL_NAME) LIKE '%" + Material_Name.ToUpper() + "%'";
                DataSet ds = WebServiceBLL.Instance.GetMaterial(condition);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
                //return JsonConvert.SerializeObject(ds, Formatting.Indented);
            //}
            //return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_STATUS
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Status")]
    public string GET_STATUS(string STATUS_TYPE = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dt = SurpriseCheckContractBLL.Instance.StatusSelectByType(STATUS_TYPE);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt.Copy());

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_REQUIRE_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Require File")]
    public string GET_REQUIRE_FILE(string REQUIRE_TYPE = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtRequestFile = AccidentBLL.Instance.AccidentRequestFile(REQUIRE_TYPE);
                DataSet ds = new DataSet();
                ds.Tables.Add(dtRequestFile.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_UPLOAD_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Upload File")]
    public string GET_UPLOAD_FILE(string UPLOAD_TYPE = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(UPLOAD_TYPE);
                DataSet ds = new DataSet();
                ds.Tables.Add(dtUploadType.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_MATERIAL_GROUP
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Material Group")]
    public string GET_MATERIAL_GROUP(string Material_Number)
    {
        string condition = string.Empty;
        try
        {
            //if (AuthenticationWebService(AuthenticationInfo))
            //{
            if (!string.Equals(Material_Number, string.Empty) && !string.Equals(Material_Number, "[null]"))
                {
                    Material_Number = Material_Number.Replace("[", string.Empty);
                    Material_Number = Material_Number.Replace("]", string.Empty);
                    Material_Number = Material_Number.Replace("\"", "'");
                    condition = " AND MATERIAL_NUMBER IN (" + Material_Number + ")";
                }
                DataSet ds = WebServiceBLL.Instance.GetMaterialGroup(condition);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
                //return JsonConvert.SerializeObject(ds, Formatting.Indented);
            //}
            //return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_PRV_REG
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Province And Region")]
    public string GET_PRV_REG()
    {
        string condition = string.Empty;
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetPrvReg(condition);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_CHECKER
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Checker")]
    public string GET_CHECKER(string I_TEXTSEARCH = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetCheckerService(condition, I_TEXTSEARCH);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_TEAMCHECK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Team Check")]
    public string GET_TEAMCHECK(string I_TEXTSEARCH = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtTeam = UserBLL.Instance.TeamSelectBLL(I_TEXTSEARCH);
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtTeam.Copy());
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_VENDOR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Vendor")]
    public string GET_VENDOR(string VENDOR_NAME = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                string condition = " AND (SABBREVIATION LIKE '%" + VENDOR_NAME + "%' OR UPPER(SABBREVIATION) LIKE '%" + VENDOR_NAME.ToUpper() + "%')";
                DataTable dt_vendor = VendorBLL.Instance.SelectVendorName(condition);
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dt_vendor.Copy());

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_CONTRACT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Contract")]
    public string GET_CONTRACT(string VENDOR_ID = "", string CONTRACTNO = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtcontract = ContractBLL.Instance.ConTractSelectByVendor(VENDOR_ID, CONTRACTNO);
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtcontract.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_LIST_GROUP
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get List Group")]
    public string GET_LIST_GROUP(string CONDITION = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtcontract = ContractBLL.Instance.ConTractDetailSelect(CONDITION);
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtcontract.Copy());

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SHIPTO_BY_SALES_DISTRICT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Shipto By Sales District")]
    public string GET_SHIPTO_BY_SALES_DISTRICT(string SALES_DISTRICT = "")
    {
        string condition = " AND PE_SEARCH_TERM LIKE '%" + SALES_DISTRICT + "%'";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.getShiptoBySaleDistrict(condition);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Truck")]
    public string GET_TRUCK(string HEADLICENSEPLATE = "", string VENDOR_ID = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dt = AccidentBLL.Instance.TruckSelect(HEADLICENSEPLATE, VENDOR_ID);
                DataTable dtCopy = dt.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SURPRISECHECK_YEAR_LIST_FOR_VENDOR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Surprisecheck Year list For Vendor")]
    public string GET_SURPRISECHECK_YEAR_LIST_FOR_VENDOR(string I_VENDORID = "", string I_APPOINTDATE = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND SVENDORID = '" + I_VENDORID + "' AND (((TO_CHAR(APPOINTDATE,'DD/MM/YYYY') = '" + I_APPOINTDATE + "')  OR TRIM('" + I_APPOINTDATE + "') IS NULL ) OR ( APPOINTLOCATE LIKE '%" + I_APPOINTDATE + "%' OR TRIM('" + I_APPOINTDATE + "') IS NULL)) ORDER BY APPOINTDATE DESC");
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtSurpriseCheckYearList.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                return JsonConvert.SerializeObject(ds, jsonSettings);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SURPRISECHECK_YEAR_DATA_FOR_VENDOR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Surprisecheck Year Data For Vendor")]
    public string GET_SURPRISECHECK_YEAR_Data_FOR_VENDOR(string I_ID = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dt = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND ID = " + I_ID);
                DataTable dtTeamCheckData = SurpriseCheckYearBLL.Instance.SurpriseCheckTeamSelect(" AND DOC_ID = " + I_ID);
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dt.Copy());
                ds.Tables.Add(dtTeamCheckData.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                return JsonConvert.SerializeObject(ds, jsonSettings);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SURPRISECHECK_YEAR_LIST_FOR_PTT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Surprisecheck Year list For PTT")]
    public string GET_SURPRISECHECK_YEAR_LIST_FOR_PTT(string I_VENDORID = "", string I_CONTRACTID = "", string I_APPOINTDATE = "", string I_USERCHECKID = "")
    {
        string condition = ConfigurationManager.AppSettings["Checker_Group_ID"];
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(this.GetCondition(I_VENDORID, I_CONTRACTID, I_APPOINTDATE, I_USERCHECKID));
                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtSurpriseCheckYearList.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                return JsonConvert.SerializeObject(ds, jsonSettings);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    private string GetCondition(string I_VENDORID = "", string I_CONTRACTID = "", string I_APPOINTDATE = "", string I_USERCHECKID = "")
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            if (!string.Equals(I_VENDORID, string.Empty))
                sb.Append(" AND SVENDORID = '" + I_VENDORID + "'");
            if (!string.Equals(I_CONTRACTID, string.Empty))
                sb.Append(" AND SCONTRACTID = '" + I_CONTRACTID + "'");
            if (!string.Equals(I_APPOINTDATE, string.Empty))
                sb.Append(" AND TO_CHAR(APPOINTDATE,'DD/MM/YYYY') = '" + I_APPOINTDATE + "' OR ( SVENDORNAME LIKE '%" + I_APPOINTDATE + "%' OR TRIM('" + I_APPOINTDATE + "') IS NULL) OR ( APPOINTLOCATE LIKE '%" + I_APPOINTDATE + "%' OR TRIM('" + I_APPOINTDATE + "') IS NULL)");
            if (!string.Equals(I_USERCHECKID, string.Empty))
                sb.Append(" AND (USERCHECK = '" + I_USERCHECKID + "' OR TEAMCHECK = '" + I_USERCHECKID + "')");
            sb.Append(" AND IS_ACTIVE IN ( 0,1,2 ) ORDER BY APPOINTDATE DESC");
            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region SAVE_CONFIRM_CANCEL_SURPRISECHECK_YEAR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Confirm Status Surprisecheck Year For Vendor")]
    public string SAVE_CONFIRM_CANCEL_SURPRISECHECK_YEAR(string ID, string VENDOR_ID, string VENDOR_NAME, string CONTRACT_ID, string CONTRACT_NO, string TOTAL_CAR, string APPOINT_DATE, string APPOINT_LOCATE, string I_USERCHECK, string I_TEAMCHECK, string I_USERID, string I_USERFULLNAME, string I_REMARK, string STATUS_ID, string STATUS_NAME, string jdtTeamCheckData)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_CONFIRM_CANCEL_SURPRISECHECK_YEAR", ID, VENDOR_ID, VENDOR_NAME, CONTRACT_ID, CONTRACT_NO, TOTAL_CAR, APPOINT_DATE, APPOINT_LOCATE, I_USERCHECK, I_TEAMCHECK, I_USERID, I_USERFULLNAME, I_REMARK, STATUS_ID, STATUS_NAME, jdtTeamCheckData);
                DataTable dt = new DataTable();
                DataTable dtTeamCheckData = new DataTable();
                string team = string.Empty;
                string email_cc = string.Empty;
                if (!string.Equals(jdtTeamCheckData, string.Empty))
                    dtTeamCheckData = (DataTable)JsonConvert.DeserializeObject(jdtTeamCheckData, (typeof(DataTable)));
                DataSet ds = new DataSet("DS");
                dtTeamCheckData.TableName = "DT";
                ds.Tables.Add(dtTeamCheckData.Copy());
                if (dtTeamCheckData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTeamCheckData.Rows.Count; i++)
                    {
                        if (i == 0)
                            team = dtTeamCheckData.Rows[i]["SUID"].ToString();
                        else
                            team = team + "," + dtTeamCheckData.Rows[i]["SUID"].ToString();
                    }
                }
                dt = UserBLL.Instance.UserSelectBLL(" AND SUID in (" + team + ") ");
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i == 0)
                            email_cc = dt.Rows[i]["SEMAIL"].ToString();
                        else
                            email_cc = email_cc + "," + dt.Rows[i]["SEMAIL"].ToString();
                    }
                }
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearSaveBLL(ID, TOTAL_CAR, VENDOR_ID, CONTRACT_ID, APPOINT_DATE, APPOINT_LOCATE, I_USERCHECK, I_TEAMCHECK, I_USERID, STATUS_ID, I_REMARK, ds);
                this.SendEmailYear(127, ID, VENDOR_ID, CONTRACT_NO, VENDOR_NAME, TOTAL_CAR, APPOINT_DATE, APPOINT_LOCATE, I_USERID, I_USERFULLNAME, I_REMARK, STATUS_NAME, email_cc);
                if (!string.Equals(STATUS_ID, "2"))
                    this.SEND_BOOKING(ID, CONTRACT_NO, VENDOR_ID, VENDOR_NAME, TOTAL_CAR, APPOINT_DATE, APPOINT_DATE, APPOINT_LOCATE, I_USERCHECK, STATUS_NAME, I_REMARK, email_cc);
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Confirm Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);

                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SUPRISECHECK_VENDOR_SELECT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurpriseCheck List Vendor")]
    public string GET_SUPRISECHECK_VENDOR_SELECT(string SVENDORID = "", string I_TEXTSEARCH = "", string I_YEARID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetSurpriseCheckVendorSelect(SVENDORID, I_TEXTSEARCH, I_YEARID);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SUPRISECHECK_PTT_CONFIRM_SELECT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurpriseCheck List PTT Confirm")]
    public string GET_SUPRISECHECK_PTT_CONFIRM_SELECT(string I_TEXTSEARCH = "", string I_YEARID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetSurpriseCheckPTTConfirmSelect(I_TEXTSEARCH, I_YEARID);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck")]
    public string SAVE_SURPRISECHECK(string I_SCONTRACTID, string I_STRUCKID, string I_SHEADREGISTERNO, string I_STRAILERREGISTERNO, string I_SCREATE, string I_DSURPRISECHECK, string I_SSURPRISECHECKPLACE, string I_SSURPRISECHECKBY, string I_CNOCHECK)
    {
        string condition = "AND STRUCKID = '" + I_STRUCKID + "'";
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISECHECK", I_SCONTRACTID, I_STRUCKID, I_SHEADREGISTERNO, I_STRAILERREGISTERNO, I_SCREATE, I_DSURPRISECHECK, I_SSURPRISECHECKPLACE, I_SSURPRISECHECKBY, I_CNOCHECK);
                string GenID;
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();

                    GenID = CommonFunction.Gen_ID(con, "SELECT NSURPRISECHECKID FROM (SELECT NSURPRISECHECKID FROM TSURPRISECHECKHISTORY ORDER BY NSURPRISECHECKID DESC)  WHERE ROWNUM <= 1");
                    string strsqlSave = @"INSERT INTO TSURPRISECHECKHISTORY (NSURPRISECHECKID, SCONTRACTID, DCHECK, STRUCKID,  SHEADREGISTERNO, STRAILERREGISTERNO, SCREATE,  DCREATE, COK, DSURPRISECHECK,  TTIME, SSURPRISECHECKPLACE, SSURPRISECHECKBY, 
                CNOCHECK,  SUPDATE, DUPDATE, CUPDATE) VALUES (:NSURPRISECHECKID, :SCONTRACTID, SYSDATE, :STRUCKID,  :SHEADREGISTERNO, :STRAILERREGISTERNO, :SCREATE,  SYSDATE, '1', TO_DATE(:DSURPRISECHECK, 'DD/MM/YYYY', 'NLS_DATE_LANGUAGE = ENGLISH'),  :TTIME, :SSURPRISECHECKPLACE, :SSURPRISECHECKBY, 
                :CNOCHECK,  :SUPDATE, SYSDATE, '1')";

                    using (OracleCommand com = new OracleCommand(strsqlSave, con))
                    {
                        DateTime DateTime;
                        com.Parameters.Clear();
                        com.Parameters.Add(":NSURPRISECHECKID", OracleType.Number).Value = GenID;
                        com.Parameters.Add(":SCONTRACTID", OracleType.Number).Value = I_SCONTRACTID;
                        com.Parameters.Add(":STRUCKID", OracleType.VarChar).Value = I_STRUCKID;
                        com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = I_SHEADREGISTERNO;
                        com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = I_STRAILERREGISTERNO;
                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = I_SCREATE;
                        com.Parameters.Add(":DSURPRISECHECK", OracleType.VarChar).Value = I_DSURPRISECHECK.Split(' ')[0]; //.ToString("YYYY-mm-dd HH:MM:ss");DateTime.TryParse(dteCheck.Value + "", out DateTime) ? DateTime : DateTime.Now;
                        com.Parameters.Add(":TTIME", OracleType.VarChar).Value = I_DSURPRISECHECK.Split(' ')[1];
                        com.Parameters.Add(":SSURPRISECHECKPLACE", OracleType.VarChar).Value = I_SSURPRISECHECKPLACE;
                        com.Parameters.Add(":SSURPRISECHECKBY", OracleType.VarChar).Value = I_SSURPRISECHECKBY;
                        com.Parameters.Add(":CNOCHECK", OracleType.Char).Value = I_CNOCHECK == "0" ? "0" : "1";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = I_SCREATE;
                        com.ExecuteNonQuery();
                    }
                }

                LogUser("27", "I", "บันทึกข้อมูลหน้า Surprise check", GenID + "", I_SCREATE);

                DataSet ds = WebServiceBLL.Instance.GetTruckList(condition);
                DataTable dt = ds.Tables[0];
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("SCONTRACTID");
                dtResult.Columns.Add("SCONTRACTNO");
                dtResult.Columns.Add("STRUCKID");
                dtResult.Columns.Add("SHEADREGISTERNO");
                dtResult.Columns.Add("STRAILERREGISTERNO");
                dtResult.Columns.Add("DSURPRISECHECK");
                dtResult.Columns.Add("GenID");
                dtResult.Columns.Add("VENDORNAME");
                dtResult.Columns.Add("VENDORID");
                dtResult.Columns.Add("NSURPRISECHECKID");
                dtResult.Rows.Add(I_SCONTRACTID, dt.Rows[0]["CONTRACT_NO"].ToString(), I_STRUCKID, I_SHEADREGISTERNO, I_STRAILERREGISTERNO, I_DSURPRISECHECK, GenID, dt.Rows[0]["VENDOR_NAME"].ToString(), dt.Rows[0]["VENDOR_ID"].ToString(), GenID);

                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);

                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(dsResult, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_FOR_PTT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck For PTT")]
    public string SAVE_SURPRISE_FOR_PTT(string I_SCHECKID, string I_PLANTID, string I_VENDORID, string I_VENDORNAME, string I_CONTRACTID, string I_CONTRACTNO, string I_STRUCKID, string I_USERID, string I_SHEADREGISTERNO, string I_STRAILREGISTERNO, string I_CHECKERDATE, string YEARID, string I_NSURPRISECHECKID, string I_PATH_LICENSE_PTT = "", string I_PATH_LICENSE_VENDOR = "", string jdtFileStep1 = "", string jdtFileStep2 = "", string jdtCheckList = "", string jdtCheckTruck = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISE_FOR_PTT", I_SCHECKID, I_PLANTID, I_VENDORID, I_VENDORNAME, I_CONTRACTID, I_CONTRACTNO, I_STRUCKID, I_USERID, I_SHEADREGISTERNO, I_STRAILREGISTERNO, I_CHECKERDATE, YEARID, I_NSURPRISECHECKID, I_PATH_LICENSE_PTT, I_PATH_LICENSE_VENDOR, jdtFileStep1, jdtFileStep2, jdtCheckList, jdtCheckTruck);
                DataTable dtFileStep1 = new DataTable();
                DataTable dtFileStep2 = new DataTable();
                DataTable dtCheckList = new DataTable();
                DataTable dtCheckTruck = new DataTable();
                DataTable dt = new DataTable();


                #region Save1
                if (!string.Equals(jdtFileStep1, string.Empty))
                    dtFileStep1 = (DataTable)JsonConvert.DeserializeObject(jdtFileStep1, (typeof(DataTable)));
                if (!string.Equals(jdtFileStep2, string.Empty))
                    dtFileStep2 = (DataTable)JsonConvert.DeserializeObject(jdtFileStep2, (typeof(DataTable)));
                if (!string.Equals(jdtCheckList, string.Empty))
                    dtCheckList = (DataTable)JsonConvert.DeserializeObject(jdtCheckList, (typeof(DataTable)));
                if (!string.Equals(jdtCheckTruck, string.Empty))
                    dtCheckTruck = (DataTable)JsonConvert.DeserializeObject(jdtCheckTruck, (typeof(DataTable)));
                DataRow[] drs = null;
                DataRow dr = null;
                double NPOINT = 0;


                using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    connection.Open();
                    OracleTransaction oTransaction = connection.BeginTransaction();
                    try
                    {

                        TCHECKTRUCK ctrck = new TCHECKTRUCK(connection, oTransaction);
                        TCHECKTRUCKITEM ctrckItem = new TCHECKTRUCKITEM(connection, oTransaction);
                        TREDUCEPOINT reduce = new TREDUCEPOINT(connection, oTransaction);
                        foreach (DataRow item in dtFileStep1.Select("NID = 0"))
                        {
                            drs = dtFileStep1.Select("NID > 0 AND SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                            if (!drs.Any())
                            {
                                drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                                if (drs.Any())
                                {
                                    dr = drs[0];
                                    #region TCHECKTRUCK
                                    ///TCHECKTRUCK
                                    ctrck.SCHECKID = I_SCHECKID;//send blank for gen new id
                                    ctrck.NPLANID = I_PLANTID; /////NPLANID
                                    ctrck.STERMINALID = I_VENDORID;
                                    ctrck.SCONTRACTID = I_CONTRACTID;         /////SCONTRACTID
                                    ctrck.CGROUPCHECK = "3";//fix หน้านี้เป็นการทำงานของ หน้าคลัง
                                    ctrck.CCLEAN = "0";
                                    ctrck.STRUCKID = I_STRUCKID; /////STRUCKID
                                    ctrck.SHEADERREGISTERNO = I_SHEADREGISTERNO == null ? "" : I_SHEADREGISTERNO; /////SHEADERREGISTERNO
                                    ctrck.STRAILERREGISTERNO = I_STRAILREGISTERNO; /////STRAILERREGISTERNO
                                    ctrck.CMA = (("" + dr["CBAN"] == "1") ? "2" : (("" + dr["CCUT"] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                    ctrck.NDAY_MA = (("" + dr["NDAY_MA"] == "") ? "0" : "" + dr["NDAY_MA"]);
                                    ctrck.SCREATE = I_USERID;
                                    ctrck.SUPDATE = I_USERID;
                                    ctrck.IS_YEAR = YEARID;
                                    ctrck.NSURPRISECHECKID = I_NSURPRISECHECKID;
                                    ctrck.Insert();
                                    I_SCHECKID = ctrck.SCHECKID;
                                    #endregion
                                    #region TCHECKTRUCKITEM
                                    ctrckItem.SCHECKID = I_SCHECKID;
                                    ctrckItem.SCHECKLISTID = "" + dr["SCHECKLISTID"];
                                    ctrckItem.SVERSIONLIST = "" + dr["SVERSIONLIST"];
                                    ctrckItem.NDAY_MA = (("" + dr["NDAY_MA"] == "") ? "0" : "" + dr["NDAY_MA"]);
                                    ctrckItem.NPOINT = ("" + dr["NPOINT"] != "") ? ("" + dr["NPOINT"]) : "0";
                                    ctrckItem.CMA = (("" + dr["CCUT"] == "1") ? "2" : (("" + dr["CCUT"] == "1") ? "1" : "0"));//2 hold ,1 mainternent ,0 ปกติ
                                    ctrckItem.COTHER = "0";
                                    ctrckItem.SOTHERREMARK = "";
                                    ctrckItem.STYPECHECKLISTID = "" + dr["STYPECHECKLISTID"];
                                    ctrckItem.SCREATE = I_USERID;
                                    ctrckItem.SUPDATE = I_USERID;
                                    ctrckItem.IS_YEAR = YEARID;
                                    ctrckItem.Insert();
                                    #endregion
                                    #region บันทึกห้ามวิ่ง+ตัดแต้ม

                                    if ("" + dr["CBAN"] == "1" && string.IsNullOrEmpty(YEARID))
                                    {//ถ้าเป้นเคส ห้ามวิ่ง 
                                        #region//บันทึกสถานะ ห้ามวิ่ง TTRUCK
                                        using (OracleConnection con = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                                        {
                                            if (con.State == ConnectionState.Closed) con.Open();
                                            OracleCommand ora_cmd = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_HEADERREGISTERNO", con);
                                            ora_cmd.Parameters.Add(":S_HEADERREGISTERNO", OracleType.VarChar).Value = ctrck.SHEADERREGISTERNO.Trim();
                                            ora_cmd.ExecuteNonQuery();

                                            OracleCommand ora_trail = new OracleCommand("UPDATE TTRUCK SET CHOLD='1',CACTIVE = 'N',DUPDATE = SYSDATE WHERE SHEADREGISTERNO=:S_TRAILERREGISTERNO", con);
                                            ora_trail.Parameters.Add(":S_TRAILERREGISTERNO", OracleType.VarChar).Value = ctrck.STRAILERREGISTERNO.Trim();
                                            ora_trail.ExecuteNonQuery();

                                            AlertTruckOnHold onHold = new AlertTruckOnHold(con);
                                            onHold.VehNo = ctrck.SHEADERREGISTERNO;
                                            onHold.TUNo = ctrck.STRAILERREGISTERNO;
                                            onHold.Subject = "การตรวจสภาพรถ/ละเลยการแก้สภาพรถเกินตามที่กำหนดหรือโดยประการใดๆซึ่งเป็นผลต่อการห้ามวิ่ง";
                                            onHold.VehID = ctrck.STRUCKID;
                                            onHold.SendTo();
                                        }
                                        #endregion
                                    }
                                    if ("" + dr["CCUT"] == "1")
                                    {//ถ้าตัดคะแนนทันที
                                        //บันทึกตัดแต้มในตาราง TREDUCEPOINT 
                                        ReducePoint(connection, oTransaction, "01", "040", I_USERID, "" + dr["SCHECKLISTNAME"], "" + dr["SCHECKLISTID"], "" + dr["STOPICID"], "" + dr["SVERSIONLIST"], "" + (("" + dr["NPOINT"] != "") ? ("" + dr["NPOINT"]) : "0")
                                    , I_SCHECKID, ctrck.SCONTRACTID, ctrck.STRUCKID, ctrck.SHEADERREGISTERNO, "", ctrck.STRAILERREGISTERNO, "");

                                    }
                                    #endregion
                                    NPOINT += (ctrckItem.NPOINT != "") ? double.Parse(ctrckItem.NPOINT) : 0;

                                    if (string.Equals(YEARID, "1") && countLoop == 0)
                                    {
                                        string I_PATH_LICENSE_PTT_NEW = I_PATH_LICENSE_PTT.Replace("iTMS", "Upload");
                                        string I_PATH_LICENSE_VENDOR_NEW = I_PATH_LICENSE_VENDOR.Replace("iTMS", "Upload");
                                        SurpriseCheckBLL.Instance.CheckTruckUpdatePath(I_PATH_LICENSE_PTT_NEW, I_PATH_LICENSE_VENDOR_NEW, I_SHEADREGISTERNO);
                                        this.CHANGE_PATH(dt, I_PATH_LICENSE_PTT);
                                        this.CHANGE_PATH(dt, I_PATH_LICENSE_VENDOR);
                                        countLoop += 1;
                                    }
                                }

                            }

                        }
                        #region Update คะแนนรวมที่ตัดที่ รายการหลัก
                        ctrck.SCHECKID = I_SCHECKID;
                        ctrck.UpdateMA();
                        #endregion
                        oTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        oTransaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }


                }
                drs = dtFileStep1.Select("NID = 0");
                if (drs.Any())
                {
                    DataSet ds = new DataSet("DS");
                    dtFileStep1.TableName = "DT";
                    ds.Tables.Add(dtFileStep1.Copy());

                    this.CHANGE_PATH(dtFileStep1);

                    if (dtFileStep2.Rows.Count == 0)
                        SurpriseCheckBLL.Instance.CheckTruckFileSave(I_SCHECKID, "1", YEARID == "1" ? I_NSURPRISECHECKID : "", ds);
                    var dtGroup = drs.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
                    string TitleName = string.Empty;
                    if (dtGroup.Any())
                    {
                        foreach (var item in dtGroup)
                        {
                            drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                            if (drs.Any())
                                TitleName += "," + drs[0]["SCHECKLISTNAME"];
                        }
                        SaveLog(ConfigValue.SurpriseCheck1, TitleName, "แจ้งปัญหาสภาพรถ", "red", I_SHEADREGISTERNO, I_STRAILREGISTERNO, I_USERID, I_VENDORNAME, I_CONTRACTID, I_STRUCKID, I_STRAILREGISTERNO, I_CHECKERDATE, I_NSURPRISECHECKID, I_CONTRACTNO, I_VENDORNAME, I_VENDORID, dtCheckTruck);
                    }

                }
                #endregion
                #region Save2
                DataRow[] drsData2 = dtFileStep1.Select("CCHECKED > -1");
                if (drsData2.Any() && dtFileStep2.Rows.Count > 0)
                {
                    var drs2 = drsData2.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
                    if (drs2.Any())
                    {
                        DataTable dtCHECKTRUCKITEM = drs2.CopyToDataTable();
                        dtCHECKTRUCKITEM.TableName = "DT";
                        DataSet ds = new DataSet("DS");
                        ds.Tables.Add(dtCHECKTRUCKITEM.Copy());
                        SurpriseCheckBLL.Instance.CheckTruckItemUpdate(YEARID == "1" ? I_NSURPRISECHECKID : "", ds);

                        string CheckItemRepair = @"SELECT TCT.STRUCKID,TCL.CBAN FROM TCHECKTRUCK TCT
LEFT JOIN TCHECKTRUCKITEM TCI
ON TCI.SCHECKID = TCT.SCHECKID
LEFT JOIN TCHECKLIST TCL
ON TCL.SCHECKLISTID = TCI.SCHECKLISTID
WHERE STRUCKID = '" + I_STRUCKID + "' AND TCI.CCHECKED ='0' AND TCL.CBAN ='1'";

                        using (OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                        {
                            DataTable dt_RepairTruck = CommonFunction.Get_Data(connection, CheckItemRepair);
                            if (dt_RepairTruck.Rows.Count == 0)
                            {
                                DataTable dt_Type = CommonFunction.Get_Data(connection, "SELECT SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(I_STRUCKID) + "'");
                                if (dt_Type.Rows.Count > 0)
                                {
                                    string SCARTYPE = dt_Type.Rows[0]["SCARTYPEID"] + "";
                                    AlertTruckOnHold AT = new AlertTruckOnHold(connection);
                                    AT.Lock_SAP(I_STRUCKID, SCARTYPE);
                                }

                            }
                        }

                        var dtGroup = drs2;
                        string TitleName = string.Empty, StatusName = string.Empty;
                        if (dtGroup.Any())
                        {
                            foreach (var item in dtGroup)
                            {
                                DataRow[] drGroups = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                                if (drGroups.Any())
                                {
                                    TitleName += "," + drGroups[0]["SCHECKLISTNAME"];
                                    if (item["CCHECKED"] + string.Empty == "1")
                                    {
                                        StatusName += ",อนุมัติผลการแก้ไข";
                                    }
                                    else
                                    {
                                        StatusName += ",ไม่อนุมัติ";
                                    }
                                }

                            }
                            SaveLog(ConfigValue.SurpriseCheck3, TitleName, "พิจารณาแก้ไขสภาพรถ", StatusName == ",ไม่อนุมัติ" ? "red" : "green", I_SHEADREGISTERNO, I_STRAILREGISTERNO, I_USERID, I_VENDORNAME, I_CONTRACTID, I_STRUCKID, I_STRAILREGISTERNO, I_CHECKERDATE, I_NSURPRISECHECKID, I_CONTRACTNO, I_VENDORNAME, I_VENDORID, dtCheckTruck, StatusName);
                        }
                    }
                }
                #endregion
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_FOR_VENDOR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck For VENDOR")]
    public string SAVE_SURPRISE_FOR_VENDOR(string I_SCHECKID, string I_SHEADREGISTERNO, string I_STRAILREGISTERNO, string YEARID, string jdtFileStep2, string jdtCheckList, string I_USERID, string I_VENDORNAME, string I_CONTRACTID, string I_STRUCKID, string I_CHECKERDATE, string I_NSURPRISECHECKID, string I_CONTRACTNO, string I_VENDORID, string jdtCheckTruck)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISE_FOR_PTT", I_SCHECKID, I_SHEADREGISTERNO, I_STRAILREGISTERNO, YEARID, jdtFileStep2, jdtCheckList, I_USERID, I_VENDORNAME, I_CONTRACTID, I_STRUCKID, I_CHECKERDATE, I_NSURPRISECHECKID, I_CONTRACTNO, I_VENDORID, jdtCheckTruck);
                DataTable dtFileStep2 = new DataTable();
                DataTable dtCheckList = new DataTable();
                DataTable dtCheckTruck = new DataTable();

                if (!string.Equals(jdtFileStep2, string.Empty))
                    dtFileStep2 = (DataTable)JsonConvert.DeserializeObject(jdtFileStep2, (typeof(DataTable)));
                if (!string.Equals(jdtCheckList, string.Empty))
                    dtCheckList = (DataTable)JsonConvert.DeserializeObject(jdtCheckList, (typeof(DataTable)));
                if (!string.Equals(jdtCheckTruck, string.Empty))
                    dtCheckTruck = (DataTable)JsonConvert.DeserializeObject(jdtCheckTruck, (typeof(DataTable)));
                #region Save1
                DataRow[] drs = dtFileStep2.Select("NID = 0");
                if (drs.Any())
                {
                    DataSet ds = new DataSet("DS");
                    dtFileStep2.TableName = "DT";

                    this.CHANGE_PATH(dtFileStep2);

                    ds.Tables.Add(dtFileStep2.Copy());
                    SurpriseCheckBLL.Instance.CheckTruckFileSave(I_SCHECKID, "2", YEARID, ds);
                    var dtGroup = drs.GroupBy(it => it["SCHECKLISTID"]).Select(it => it.First());
                    string TitleName = string.Empty;
                    if (dtGroup.Any())
                    {
                        foreach (var item in dtGroup)
                        {
                            drs = dtCheckList.Select("SCHECKLISTID = " + item["SCHECKLISTID"] + " AND STYPECHECKLISTID = " + item["STYPECHECKLISTID"] + " AND SVERSIONLIST = " + item["SVERSIONLIST"] + "");
                            if (drs.Any())
                                TitleName += "," + drs[0]["SCHECKLISTNAME"];
                        }
                        SaveLog(ConfigValue.SurpriseCheck2, TitleName, "แก้ไขปัญหาสภาพรถ", "orange", I_SHEADREGISTERNO, I_STRAILREGISTERNO, I_USERID, I_VENDORNAME, I_CONTRACTID, I_STRUCKID, I_STRAILREGISTERNO, I_CHECKERDATE, I_NSURPRISECHECKID, I_CONTRACTNO, I_VENDORNAME, I_VENDORID, dtCheckTruck);
                    }
                }

                #endregion

                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_YEAR
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck Year")]
    public string SAVE_SURPRISECHECK_YEAR(string ID, string VENDOR_ID, string VENDOR_NAME, string CONTRACT_ID, string CONTRACT_NO, string TOTAL_CAR, string APPOINT_DATE, string APPOINT_LOCATE, string I_USERCHECK, string I_USERFULLNAME, string I_TEAMCHECK, string I_USERID, string jdtTeamCheckData)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISECHECK_YEAR", ID, VENDOR_ID, VENDOR_NAME, CONTRACT_ID, CONTRACT_NO, TOTAL_CAR, APPOINT_DATE, APPOINT_LOCATE, I_USERCHECK, I_USERFULLNAME, I_TEAMCHECK, I_USERID, jdtTeamCheckData);
                DataTable dtTeamCheckData = new DataTable();
                string team = string.Empty;
                string email_cc = string.Empty;
                if (!string.Equals(jdtTeamCheckData, string.Empty))
                    dtTeamCheckData = (DataTable)JsonConvert.DeserializeObject(jdtTeamCheckData, (typeof(DataTable)));
                if (dtTeamCheckData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTeamCheckData.Rows.Count; i++)
                    {
                        if (i == 0)
                            team = dtTeamCheckData.Rows[i]["SUID"].ToString();
                        else
                            team = team + "," + dtTeamCheckData.Rows[i]["SUID"].ToString();
                    }
                }
                dtTeamCheckData = UserBLL.Instance.UserSelectBLL(" AND SUID in (" + team + ") ");
                if (dtTeamCheckData.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTeamCheckData.Rows.Count; i++)
                    {
                        if (i == 0)
                            email_cc = dtTeamCheckData.Rows[i]["SEMAIL"].ToString();
                        else
                            email_cc = email_cc + "," + dtTeamCheckData.Rows[i]["SEMAIL"].ToString();
                    }
                }
                DataSet ds = new DataSet("DS");
                dtTeamCheckData.TableName = "DT";
                ds.Tables.Add(dtTeamCheckData.Copy());
                DataTable dtData = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSaveBLL(ID, TOTAL_CAR, VENDOR_ID, CONTRACT_ID, APPOINT_DATE, APPOINT_LOCATE, I_USERCHECK, I_TEAMCHECK, I_USERID, "0", string.Empty, ds);
                if (dtData.Rows.Count > 0)
                    this.SendEmailYear(128, dtData.Rows[0]["doc_id"].ToString(), VENDOR_ID, CONTRACT_NO, VENDOR_NAME, TOTAL_CAR, APPOINT_DATE, APPOINT_LOCATE, I_USERID, I_USERFULLNAME, string.Empty, string.Empty, email_cc);
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion
    protected bool ReducePoint(OracleConnection connection, OracleTransaction oTransaction, string REDUCETYPE, string PROCESSID, string I_USERID, params string[] sArrayParams)
    {
        bool IsReduce = true;
        TREDUCEPOINT repoint = new TREDUCEPOINT(connection, oTransaction);
        repoint.NREDUCEID = "";
        repoint.CACTIVE = "1";
        repoint.SREDUCEBY = I_USERID;
        repoint.SREDUCETYPE = REDUCETYPE;
        repoint.SPROCESSID = PROCESSID;
        repoint.SREDUCENAME = "" + sArrayParams[0];
        repoint.SCHECKLISTID = "" + sArrayParams[1];
        repoint.STOPICID = "" + sArrayParams[2];
        repoint.SVERSIONLIST = "" + sArrayParams[3];
        repoint.NPOINT = "" + sArrayParams[4];
        repoint.SREFERENCEID = "" + sArrayParams[5];
        repoint.SCONTRACTID = "" + sArrayParams[6];
        repoint.SHEADID = "" + sArrayParams[7];
        repoint.SHEADREGISTERNO = "" + sArrayParams[8];
        repoint.STRAILERID = "" + sArrayParams[9];
        repoint.STRAILERREGISTERNO = "" + sArrayParams[10];
        repoint.Insert();
        return IsReduce;
    }
    private string SaveLog(int TemplateID, string TitleName, string HeadName, string CssColor, string SHEADREGISTERNO, string STRAILREGISTERNO, string I_USERID, string VENDOR_NAME, string CONTRACT_ID, string STRUCKID, string STRAILERREGISTERNO, string CHECKERDATE, string NSURPRISECHECKID, string SCONTRACTNO, string SVENDORNAME, string SVENDORID, DataTable dtCheckTruck, string StatusName = "")
    {
        if (!string.IsNullOrEmpty(TitleName))
        {

            TitleName = (TitleName).Remove(0, 1);
            LOG.Instance.SaveLog(VENDOR_NAME + " " + HeadName + " Surprise Check", SHEADREGISTERNO, I_USERID, "<span class=\"" + CssColor + "\">" + TitleName + "</span>", StatusName);
            #region บันทึกหางลาก Log
            if (!string.IsNullOrEmpty(STRAILREGISTERNO))
            {
                LOG.Instance.SaveLog(VENDOR_NAME + " " + HeadName + " Surprise Check", STRAILREGISTERNO, I_USERID, "<span class=\"" + CssColor + "\">" + TitleName + "</span>", StatusName);
            }
            #endregion

            if (SendEmail(TemplateID, TitleName, StatusName, CONTRACT_ID, STRUCKID, SHEADREGISTERNO, STRAILERREGISTERNO, CHECKERDATE, NSURPRISECHECKID, SCONTRACTNO, SVENDORNAME, SVENDORID, I_USERID, dtCheckTruck))
            {
                messSendEnail += MessEmailSuccess;
            }
            else
            {
                messSendEnail += MessEmailFail;
            }
        }
        return string.Empty;
    }

    #region SendEmail

    private void SendEmailCheckContractTruck(int TemplateID, string StatusName, string CONTRACT_NO, string TYPE, string CHASSIS_H, string CHASSIS_T, string SHEADREGISTERNO, string STRAILERREGISTERNO, string CHECKDATE, string PLACE, string RESULT, string SVENDORNAME, string SVENDORID, string USERID, string REMARK = "", string ID = "")
    {
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == 130)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACT_NO);
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", TYPE == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", TYPE == "1" ? CHASSIS_H : CHASSIS_H + "/" + CHASSIS_T);
                    Body = Body.Replace("{LICENSE}", TYPE == "1" ? SHEADREGISTERNO : SHEADREGISTERNO + "/" + STRAILERREGISTERNO);
                    Body = Body.Replace("{APPOINTDATE}", CHECKDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", PLACE);
                    Body = Body.Replace("{STATUS}", StatusName);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);
                    string Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(RESULT);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, true, true), Subject, Body, "", "SurpriseCheckContrack1", "EMAIL_TEST");
                }
                else if (TemplateID == 131)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACT_NO);
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", TYPE == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", TYPE == "1" ? CHASSIS_H : CHASSIS_H + "/" + CHASSIS_T);
                    Body = Body.Replace("{LICENSE}", TYPE == "1" ? SHEADREGISTERNO : SHEADREGISTERNO + "/" + STRAILERREGISTERNO);
                    Body = Body.Replace("{APPOINTDATE}", CHECKDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", PLACE);
                    Body = Body.Replace("{STATUS}", StatusName);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);
                    Body = Body.Replace("{REMARK}", REMARK);

                    string Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckContractAddEdit.aspx?str=" + MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, true, true), Subject, Body, "", "SurpriseCheckContrack2", "EMAIL_TEST");
                }
                else if (TemplateID == 134)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACT_NO);
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", TYPE == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", TYPE == "1" ? CHASSIS_H : CHASSIS_H + "/" + CHASSIS_T);
                    Body = Body.Replace("{LICENSE}", TYPE == "1" ? SHEADREGISTERNO : SHEADREGISTERNO + "/" + STRAILERREGISTERNO);
                    Body = Body.Replace("{APPOINTDATE}", CHECKDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", PLACE);
                    Body = Body.Replace("{STATUS}", StatusName);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);
                    string Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(RESULT);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, true, true), Subject, Body, "", "SurpriseCheckContrack1", "EMAIL_TEST");
                }
                else if (TemplateID == 132)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACT_NO);
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", TYPE == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", TYPE == "1" ? CHASSIS_H : CHASSIS_H + "/" + CHASSIS_T);
                    Body = Body.Replace("{LICENSE}", TYPE == "1" ? SHEADREGISTERNO : SHEADREGISTERNO + "/" + STRAILERREGISTERNO);
                    Body = Body.Replace("{REMARK}", REMARK);
                    Body = Body.Replace("{STATUS}", StatusName);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);

                    string Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckContractAddEdit.aspx?str=" + MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, true, true), Subject, Body, "", "SurpriseCheckContrack1", "EMAIL_TEST");
                }
                else if (TemplateID == 133)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACT_NO);
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", TYPE == "1" ? "รถเดี่ยว" : "รถกึ่งพ่วง");
                    Body = Body.Replace("{CHASSIS}", TYPE == "1" ? CHASSIS_H : CHASSIS_H + "/" + CHASSIS_T);
                    Body = Body.Replace("{LICENSE}", TYPE == "1" ? SHEADREGISTERNO : SHEADREGISTERNO + "/" + STRAILERREGISTERNO);
                    Body = Body.Replace("{STATUS}", StatusName);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);

                    string Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckContractAddEdit.aspx?str=" + MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, true, true), Subject, Body, "", "SurpriseCheckContrack1", "EMAIL_TEST");
                }

            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }
    private bool SendEmail(int TemplateID, string TitleName, string StatusName, string CONTRACT_ID, string STRUCKID, string SHEADREGISTERNO, string STRAILERREGISTERNO, string CHECKERDATE, string NSURPRISECHECKID, string SCONTRACTNO, string SVENDORNAME, string SVENDORID, string USERID, DataTable dtCheckTruck)
    {
        try
        {


            bool isREs = false;
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                string EmailList = string.Empty, Link = string.Empty, str = string.Empty, status = string.Empty;
                byte[] plaintextBytes;

                if (TemplateID == ConfigValue.SurpriseCheck1 || TemplateID == ConfigValue.SurpriseCheck2)
                {
                    str = CONTRACT_ID + "&" + STRUCKID + "&" + SHEADREGISTERNO + "&" + STRAILERREGISTERNO + "&" + CHECKERDATE + "&" + NSURPRISECHECKID + "&" + SCONTRACTNO + "&" + SVENDORNAME + "&" + SVENDORID;
                }
                else if (TemplateID == ConfigValue.SurpriseCheck3)
                {
                    str = CONTRACT_ID + "&" + STRUCKID + "&" + SHEADREGISTERNO + "&" + STRAILERREGISTERNO + "&" + CHECKERDATE + "&" + NSURPRISECHECKID + "&" + SCONTRACTNO + "&" + SVENDORNAME + "&" + SVENDORID;
                    status = StatusName;
                    if (!string.IsNullOrEmpty(status))
                    {
                        status = status.Remove(0, 1);
                        string[] statuss = status.Split(',');
                        if (!string.IsNullOrEmpty(TitleName))
                        {
                            string[] stringSeparators = new string[] { "]," };
                            string[] titles = TitleName.Split(stringSeparators, StringSplitOptions.None);
                            status = string.Empty;
                            for (int i = 0; i < titles.Count(); i++)
                            {
                                TitleName = titles[i];
                                status += TitleName + " สถานะ : " + statuss[i] + "<br/>";
                            }
                        }
                    }
                }
                plaintextBytes = Encoding.UTF8.GetBytes(str);
                str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                if (TemplateID == ConfigValue.SurpriseCheck3)
                    Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckAddEdit.aspx?str=" + str + "&modehis=1";
                else
                    Link = "https://ptttms.pttplc.com/TMS/SurpriseCheckAddEdit.aspx?str=" + str;
                #region SurpriseCheck
                EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), SVENDORID, false, false);
                //Body = Body.Replace("{Email}", "");

                DataTable dtemail = SurpriseCheckBLL.Instance.CheckTruckEmailSelectBLL(NSURPRISECHECKID);

                if (dtCheckTruck.Rows.Count > 0)
                {
                    if (dtemail.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(EmailList) && !string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()) && !string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += "," + dtCheckTruck.Rows[0]["SEMAIL"].ToString() + "," + dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()) && !string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtCheckTruck.Rows[0]["SEMAIL"].ToString() + "," + dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtemail.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtemail.Rows[0]["SEMAIL"].ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(EmailList) && !string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += "," + dtCheckTruck.Rows[0]["SEMAIL"].ToString();
                        }
                        else if (!string.IsNullOrEmpty(dtCheckTruck.Rows[0]["SEMAIL"].ToString()))
                        {
                            EmailList += dtCheckTruck.Rows[0]["SEMAIL"].ToString();
                        }
                    }
                }

                string VENDORACCOUNTNAME = string.Empty;
                string sConfTruck = @"SELECT DISTINCT TUSER.SUID , TUSER.SVENDORID ,TUSER.SFIRSTNAME||' '||TUSER.SLASTNAME SFULLNAME
                                ,CASE TUSER.CGROUP 
                                WHEN '0' THEN TVENDOR.SABBREVIATION 
                                WHEN '1' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '2' THEN TTERMINAL.SABBREVIATION
                                WHEN '3' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '4' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '5' THEN NVL(TRIM(DIVISION_NAME)||TRIM(DEPARTMENT_NAME),TUNIT.UNITNAME )
                                WHEN '6' THEN M_SOLD_TO.SOLD_NAME
                                END  SUNITNAME
                                FROM TUSER 
                                LEFT JOIN TVENDOR ON TUSER.SVENDORID=TVENDOR.SVENDORID
                                LEFT JOIN TTERMINAL ON TUSER.SVENDORID=TTERMINAL.STERMINALID
                                LEFT JOIN TUNIT ON TUSER.SVENDORID=TUNIT.UNITCODE
                                LEFT JOIN M_DEPARTMENT ON TUSER.DEPARTMENT_ID = M_DEPARTMENT.DEPARTMENT_ID 
	                            LEFT JOIN M_DIVISION ON TUSER.DIVISION_ID = M_DIVISION.DIVISION_ID 
								LEFT JOIN M_SOLD_TO ON TUSER.SVENDORID = M_SOLD_TO.SOLD_ID
                                WHERE SUID='{0}'";
                using (OracleConnection OraConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
                {
                    DataTable dtConfTruck = CommonFunction.Get_Data(OraConnection, string.Format(sConfTruck, "" + USERID));
                    if (dtConfTruck.Rows.Count > 0)
                    {
                        VENDORACCOUNTNAME = dtConfTruck.Rows[0]["SUNITNAME"].ToString();
                    }
                }


                //EmailList += "zsuntipab.k@pttdigital.com";
                //EmailList += "TMS_terapat.p@pttplc.com, TMS_atit.l@pttplc.com, TMS_wasupol.p@pttplc.com";
                Body = Body.Replace("{Title}", TitleName);
                Body = Body.Replace("{SCONTRACTID}", SCONTRACTNO);
                Body = Body.Replace("{SHEADREGISTERNO}", SHEADREGISTERNO + (!string.IsNullOrEmpty(STRAILERREGISTERNO) && STRAILERREGISTERNO != "-" ? " - " + STRAILERREGISTERNO : ""));
                Body = Body.Replace("{Vendor}", SVENDORNAME);
                Body = Body.Replace("{Status}", status);
                Body = Body.Replace("{VendorAccountName}", VENDORACCOUNTNAME + string.Empty);


                //byte[] plaintextBytes = Encoding.UTF8.GetBytes(SEMPLOYEEID);
                //string PK = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                //plaintextBytes = Encoding.UTF8.GetBytes(REQ_ID);
                Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                MailService.SendMail(EmailList, Subject, Body, "", "SURPRISE_CHECK", "EMAIL_TEST");
                #endregion


                isREs = true;

            }
            return isREs;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private void SendEmailYear(int TemplateID, string Doc_id = "", string SVENDORID = "", string SCONTRACTNO = "", string SVENDORNAME = "", string TOTALCAR = "", string APPOINTDATE = "", string APPOINTLOCATE = "", string USERID = "", string USERFULLNAME = "", string REMARK = "", string STATUS = "", string CC_EMAIL = "")
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                DataTable dt = VendorBLL.Instance.VendorSelectBLL(" AND SVENDORID = '" + SVENDORID + "'");
                if (TemplateID == 128)
                {
                    Body = Body.Replace("{CONTRACT}", SCONTRACTNO);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);
                    Body = Body.Replace("{TOTALCAR}", TOTALCAR);
                    Body = Body.Replace("{APPOINTDATE}", APPOINTDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", APPOINTLOCATE);
                    Body = Body.Replace("{USERCHECK}", USERFULLNAME);
                    string Link = "https://ptttms.pttplc.com/TMS/surpriseChk_Vendor.aspx?str=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Doc_id), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), dt.Rows[0]["EMAIL"].ToString(), false, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), dt.Rows[0]["EMAIL"].ToString(), false, true), Subject, Body, CC_EMAIL, "SurpriseCheck3", "EMAIL_TEST");
                }
                else if (TemplateID == 127)
                {
                    Body = Body.Replace("{CONTRACT}", SCONTRACTNO);
                    Body = Body.Replace("{VENDOR}", SVENDORNAME);
                    Body = Body.Replace("{TOTALCAR}", TOTALCAR);
                    Body = Body.Replace("{APPOINTDATE}", APPOINTDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", APPOINTLOCATE);
                    Body = Body.Replace("{STATUS}", STATUS);
                    Body = Body.Replace("{REMARK}", REMARK);
                    string Link = "https://ptttms.pttplc.com/TMS/surpriseChk_ShowData.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Doc_id), MachineKeyProtection.All);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), string.Empty, false, true), Subject, Body, CC_EMAIL, "SurpriseCheck3", "EMAIL_TEST");
                }
                else if (TemplateID == 135)
                {
                    Subject = Subject.Replace("{CAR}", Doc_id);
                    Subject = Subject.Replace("{DATE}", DateTime.Now.ToString("dd/MM/yyyy"));
                    Subject = Subject.Replace("{TIME}", DateTime.Now.ToString("HH:mm:ss"));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,kritsada.h@pttor.com,bodin.a@pttor.com,pancheewa.b@pttor.com,wasupol.p@pttor.com,surapol.s@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERID), string.Empty, false, true), Subject, Body, CC_EMAIL, "", "EMAIL_TEST", REMARK);
                }
            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }

    #endregion

    #region LogUser
    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID, string UserID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = UserID;
        trace.SMENUID = MENUID;
        trace.SCREATE = UserID;
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    #endregion

    #region GET_SURPRISECHECK_SELECT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurpriseCheck Checklist all")]
    public string GET_SURPRISECHECK_SELECT(string I_SCONTRACTID, string I_STRUCKID, string I_SHEADREGISTERNO, string I_STRAILERREGISTERNO, string I_CheckerDate, string I_NSURPRISECHECKID, string I_SCONTRACTNO, string I_SVENDORNAME, string I_SVENDORID, string I_YEARID, string I_MODE)
    {
        string condition = string.Empty;
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtFileStep1 = new DataTable();
                DataTable dtFileStep1History = new DataTable();
                DataTable dtFileStep2 = new DataTable();
                DataTable dtFileStep2History = new DataTable();
                DataTable dtSurpriseCheckYearList = new DataTable();
                DataTable dtCheckList = SurpriseCheckBLL.Instance.CheckListSelect();
                string modehis = "0";
                if (!string.Equals(I_MODE, string.Empty))
                {
                    string mode = I_MODE;
                    modehis = mode;
                }
                DataTable dtCheckTruck = SurpriseCheckBLL.Instance.CheckTruckSelect(I_STRUCKID, I_YEARID, modehis);
                DataTable dtFileStep1Sum = SurpriseCheckBLL.Instance.CheckTruckFileSelect(I_STRUCKID, "1", I_YEARID);
                DataTable dtFileStep2Sum = SurpriseCheckBLL.Instance.CheckTruckFileSelect(I_STRUCKID, "2", I_YEARID);
                DataTable dtFileOther = SurpriseCheckBLL.Instance.CheckTruckFileOtherSelect(I_STRUCKID, I_SCONTRACTID, I_YEARID);
                if (string.Equals(I_YEARID, "1"))
                    dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSelectBLL(" AND STRUCKID = '" + I_STRUCKID + "' AND APPOINTYEAR = '" + DateTime.Now.Year.ToString() + "'");

                DataRow[] drs = dtFileStep1Sum.Select("CCHECKED <> 1");
                if (drs.Any())
                {
                    dtFileStep1 = drs.CopyToDataTable();
                }
                else
                {
                    dtFileStep1 = dtFileStep1Sum.Clone();
                }
                drs = dtFileStep1Sum.Select("CCHECKED = 1");
                if (drs.Any())
                {
                    dtFileStep1History = drs.CopyToDataTable();
                }
                else
                {
                    dtFileStep1History = dtFileStep1Sum.Clone();
                }

                drs = dtFileStep2Sum.Select("CCHECKED <> 1");
                if (drs.Any())
                {
                    dtFileStep2 = drs.CopyToDataTable();
                }
                else
                {
                    dtFileStep2 = dtFileStep2Sum.Clone();
                }
                drs = dtFileStep2Sum.Select("CCHECKED = 1");
                if (drs.Any())
                {
                    dtFileStep2History = drs.CopyToDataTable();
                }
                else
                {
                    dtFileStep2History = dtFileStep2Sum.Clone();
                }

                DataSet ds = new DataSet();

                ds.DataSetName = "DS";
                ds.Tables.Add(dtCheckList.Copy());
                ds.Tables[0].TableName = "CheckList";
                ds.Tables.Add(dtCheckTruck.Copy());
                ds.Tables[1].TableName = "CheckTruck";
                ds.Tables.Add(dtFileStep1.Copy());
                ds.Tables[2].TableName = "FileStep1";
                ds.Tables.Add(dtFileStep1History.Copy());
                ds.Tables[3].TableName = "FileStep1History";
                ds.Tables.Add(dtFileStep2.Copy());
                ds.Tables[4].TableName = "FileStep2";
                ds.Tables.Add(dtFileStep2History.Copy());
                ds.Tables[5].TableName = "FileStep2History";
                ds.Tables.Add(dtFileOther.Copy());
                ds.Tables[6].TableName = "FileOther";
                ds.Tables.Add(dtSurpriseCheckYearList.Copy());
                ds.Tables[7].TableName = "SurpriseCheckYear";

                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;

                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region GET_SUPRISECHECK_PTT_SELECT
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurpriseCheck Checklist History all")]
    public string GET_SUPRISECHECK_PTT_SELECT(string I_TEXTSEARCH, string I_DATESTART, string I_TERMINALID, string I_CHECKER)
    {
        string condition = string.Empty;
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {

                DataSet ds = WebServiceBLL.Instance.GetSurpriseCheckPTTSelect(I_TEXTSEARCH, I_DATESTART, I_TERMINALID, I_CHECKER);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;

                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region Private Method
    private bool AuthenticationWebService(TMSAuthentication authenticationInfo)
    {
        if (authenticationInfo == null)
        {
            throw new AuthenticationException("Please provide a Username and Password.");
        }

        if (string.IsNullOrEmpty(authenticationInfo.Username) || string.IsNullOrEmpty(authenticationInfo.Password))
        {
            throw new AuthenticationException("Please provide a Username and Password.");
        }

        if (!authenticationInfo.IsUserValid())
        {
            throw new AuthenticationException("Invalid Username or Password.");
        }

        return true;
    }

    private string GenerateDriverCondition(string vendorID, string permission, string workContract, string searchText, string documentTypeID, string documentExpiredDay, string documentExpiredDateFrom, string documentExpiredDateTo, string statusexpired)
    {
        try
        {
            string condition = "";

            if (!string.IsNullOrEmpty(documentExpiredDateFrom) && !string.IsNullOrEmpty(documentExpiredDateTo))
                condition = " AND TO_DATE(STOP_DATE, 'DD/MM/YYYY') BETWEEN TO_DATE( '" + documentExpiredDateFrom + "','DD/MM/YYYY') AND  TO_DATE( '" + documentExpiredDateTo + "','DD/MM/YYYY') ";

            if (!string.IsNullOrEmpty(vendorID))
                condition += " AND STRANS_ID = '" + vendorID + "'";

            if (!string.IsNullOrEmpty(searchText))
                condition += " AND SEMPLOYEEID||' '||sName||' '||PERS_CODE LIKE '%" + CommonFunction.ReplaceInjection(searchText) + "%' ";

            if (!string.IsNullOrEmpty(documentTypeID))
                condition += " AND UPLOAD_ID = " + documentTypeID + "";

            if (!string.IsNullOrEmpty(documentExpiredDay))
                condition += "AND (NVL(TRUNC ((SYSDATE) - TO_DATE (STOP_DATE, 'DD/MM/YYYY')),0) BETWEEN -1 * " + documentExpiredDay + " AND 0 )";

            if (!string.IsNullOrEmpty(statusexpired))
                condition += " AND IS_EXPIRE = " + statusexpired + "";

            if (!string.IsNullOrEmpty(permission))
            {
                string checkAllow = "";
                string checkDisallow = "";
                string checkDisabled = "", checkCactive9 = string.Empty;
                //string checkOver = "";
                string[] permissions = permission.Split('|');
                foreach (string item in permissions)
                {
                    if (item.Trim().ToUpper() == "1")
                    {
                        checkAllow = "1";
                    }
                    else if (item.Trim().ToUpper() == "0")
                    {
                        checkDisallow = "0";
                    }
                    else if (item.Trim().ToUpper() == "2")
                    {
                        checkDisabled = "2";
                    }
                }
                condition += " AND CACTIVE in('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "')";
            }

            if (!string.IsNullOrEmpty(workContract))
                condition += " AND INUSE = " + workContract + "";

            if (string.IsNullOrEmpty(documentTypeID))
                condition += " AND UPLOAD_ID = 31";

            return condition;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message + ":" + ex.StackTrace);
        }
    }

    private string GenerateKMCondition(string topicID, string authorName, string knowlwdgeTypeID, string tagHDText, string tagItemText, string departmentID, string divisionID, string keyWord, string year, string dateFrom, string dateTo, string userID, string check)
    {
        try
        {
            int department = string.IsNullOrEmpty(departmentID) ? 0 : Convert.ToInt32(departmentID);
            int division = string.IsNullOrEmpty(divisionID) ? 0 : Convert.ToInt32(divisionID);

            StringBuilder sb = new StringBuilder();
            if (!string.Equals(topicID.Trim(), ""))
            {
                sb.Append(" AND TOPIC_ID LIKE '%" + topicID.Trim() + "%'");
            }

            if (!string.Equals(authorName.Trim(), ""))
            {
                sb.Append(" AND FULLNAME LIKE '%" + authorName.Trim() + "%'");
            }

            if (!string.Equals(knowlwdgeTypeID.Trim(), ""))
            {
                sb.Append(" AND KNOWLEDGE_TYPE_ID = '" + knowlwdgeTypeID + "'");
            }

            if (department > 0)
            {
                sb.Append(" AND DEPARTMENT_ID = '" + departmentID + "'");
            }

            if (!string.IsNullOrEmpty(tagHDText))
            {
                if (!string.IsNullOrEmpty(tagItemText))
                {
                    sb.Append(" AND UPPER(TAGS) LIKE '%" + tagHDText.ToUpper() + "-" + tagItemText.ToUpper() + "%'");
                }
                else
                {
                    sb.Append(" AND UPPER(TAGS) LIKE '%" + tagHDText.ToUpper() + "%'");
                }
            }

            if (division > 0)
            {
                sb.Append(" AND DIVISION_ID = '" + divisionID + "'");
            }

            if (!string.Equals(keyWord.Trim(), ""))
            {
                sb.Append(" AND UPPER(OTHER_KEYWORDS) LIKE '%" + keyWord.Trim().ToUpper() + "%'");
            }

            if (!string.Equals(year, "") && !string.Equals(dateFrom, "") && !string.Equals(dateTo, ""))
                throw new Exception("กรุณาเลือก Created Date หรือ Year อย่างใดอย่างหนึ่ง");

            if (!string.Equals(year, ""))
            {
                sb.Append(" AND TO_CHAR(CREATE_DATETIME,'yyyy') = '" + year + "' ");
            }
            else
            {
                string DateStart = string.Empty;
                string DateEnd = string.Empty;
                try
                {
                    DateStart = DateTime.ParseExact(dateFrom.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy");
                    DateEnd = DateTime.ParseExact(dateTo.Trim(), "dd/MM/yyyy", null).ToString("dd/MM/yyyy");
                }
                catch
                {
                    throw new Exception("กรุณากรอกวันที่ให้ครบถ้วน");
                }

                sb.Append(" AND TO_DATE(TO_CHAR(CREATE_DATETIME,'dd/mm/yyyy'),'dd/mm/yyyy') BETWEEN TO_DATE('" + DateStart + "','dd/mm/yyyy') AND TO_DATE('" + DateEnd + "','dd/mm/yyyy') ");
            }
            string KMCheck = KMCheckShare(userID);
            sb.Append(" AND STATUS_ID != 2 AND ((KM_ID IN (" + (KMCheck == "" ? "-1" : KMCheck) + ") and STATUS_ID = 1) OR (STATUS_ID = 0 AND CREATE_BY = " + int.Parse(userID) + ") OR (STATUS_ID = 1 AND CREATE_BY = " + int.Parse(userID) + ")) ");


            if (check == "1")
            {
                string condition = string.Empty;
                sb.Clear();
                DataTable dtBook = KMBLL.Instance.SelectKMBookBLL(" AND USER_ID = " + int.Parse(userID));
                if (dtBook.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBook.Rows.Count; i++)
                    {
                        if (i == 0)
                            condition += dtBook.Rows[i]["KM_ID"].ToString();
                        else
                            condition += "," + dtBook.Rows[i]["KM_ID"].ToString();
                    }
                    sb.Append(" AND STATUS_ID != 2 AND KM_ID IN (" + condition + ") ");
                }
                else
                {
                    sb.Append(" AND 0=1 ");
                }
            }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message + ":" + ex.StackTrace);
        }
    }

    private string GenerateTruckCondition(string workgroup, string vendorID, string contract, string licensePlate, string permission, string truckAge, string documnettype, string documentExpire, string startdate, string stopdate, string statusexpired)
    {
        try
        {
            //documentExpire
            string condition = "";
            if (!string.IsNullOrEmpty(vendorID))
                condition += " AND VENDOR_ID = '" + vendorID + "'";

            if (!string.IsNullOrEmpty(permission))
            {
                string checkAllow = "";
                string checkDisallow = "";
                string checkDisabled = "";
                //string checkOver = "";
                string[] permissions = permission.Split('|');
                foreach (string item in permissions)
                {
                    if (item.Trim().ToUpper() == "Y")
                    {
                        checkAllow = "Y";
                    }
                    else if (item.Trim().ToUpper() == "N")
                    {
                        checkDisallow = "N";
                    }
                    else if (item.Trim().ToUpper() == "D")
                    {
                        checkDisabled = "D";
                    }
                }
                condition += " AND STATUS_VALUE IN('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "')";
            }
            if (!string.IsNullOrEmpty(licensePlate))
                condition += " AND (SHEADREGISTERNO LIKE'%" + licensePlate + "%' OR STRAILERREGISTERNO LIKE'%" + licensePlate + "%')";

            if (!string.IsNullOrEmpty(contract))
                condition += " AND CONTRACT_ID = " + contract + "";

            if (!string.IsNullOrEmpty(documnettype))
                condition += " AND UPLOAD_ID = " + documnettype + "";

            if (!string.IsNullOrEmpty(statusexpired))
                condition += " AND IS_EXPIRE = " + statusexpired + "";

            if (!string.IsNullOrEmpty(startdate) && !string.IsNullOrEmpty(stopdate))
                condition += " AND TO_DATE(STOP_DATE,'dd/mm/yyyy') BETWEEN TO_DATE('" + startdate + "','dd/mm/yyyy') AND TO_DATE('" + stopdate + "','dd/mm/yyyy')";

            if (!string.IsNullOrEmpty(truckAge) && truckAge != "0")
                condition += " AND TRUCK_AGE_YEAR = " + truckAge + "";

            if (!string.IsNullOrEmpty(workgroup))
                condition += " AND GROUPNAME = '" + workgroup + "'";

            if (!string.IsNullOrEmpty(documentExpire))
            {
                condition += " AND (NVL(TRUNC ((SYSDATE) - TO_DATE (" + (documnettype == "0" ? "DWATEREXPIRE" : "STOP_DATE") + ", 'DD/MM/YYYY')),0) BETWEEN -1 * " + documentExpire + " AND 0 )";
            }


            return condition;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message + ":" + ex.StackTrace);
        }
    }

    private string KMCheckShare(string userID)
    {
        try
        {
            string KMCheck = "";
            DataTable dtUser = UserBLL.Instance.UserSelectBLL(" AND SUID = '" + userID + "'");
            if (dtUser != null && dtUser.Rows != null && dtUser.Rows.Count > 0)
            {
                DataRow dr = dtUser.Rows[0];
                DataTable KMCheckShare = new DataTable();
                if (string.Equals(dr["CGROUP"].ToString().Trim(), "0") || string.Equals(dr["CGROUP"].ToString().Trim(), "6"))
                    KMCheckShare = KMBLL.Instance.SelectKMCheckShareBLL(2, int.Parse(userID));
                else
                    KMCheckShare = KMBLL.Instance.SelectKMCheckShareBLL(1, int.Parse(userID));

                if (KMCheckShare.Rows.Count > 0)
                {
                    for (int i = 0; i < KMCheckShare.Rows.Count; i++)
                    {
                        KMCheck = KMCheck + "," + KMCheckShare.Rows[i]["KM_ID"].ToString();
                    }
                }
                if (KMCheck != "")
                    KMCheck = KMCheck.Substring(1);
            }
            return KMCheck;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private bool CheckTableNotEmpty(params DataTable[] dts)
    {
        bool result = true;
        try
        {
            foreach (DataTable dt in dts)
            {
                try
                {
                    if (dt == null || dt.Rows == null || dt.Rows.Count <= 0)
                    {
                        result = false;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            result = false;
        }
        return result;
    }

    private DataTable CopySomeColumns(DataTable dt, params string[] columns)
    {
        DataTable dtTemp = dt.Copy();
        foreach (DataColumn dc in dt.Columns)
        {
            if (!columns.Contains(dc.ColumnName))
            {
                dtTemp.Columns.Remove(dc.ColumnName);
            }
        }
        return dtTemp.Copy();
    }

    private DataTable GetKMType()
    {
        DataTable dt = CopySomeColumns(KnowledgeTypeBLL.Instance.KnowledgeTypeSelectBLL(""), "KNOWLEDGE_TYPE_ID", "KNOWLEDGE_TYPE_NAME");
        dt.Columns["KNOWLEDGE_TYPE_ID"].ColumnName = "ID";
        dt.Columns["KNOWLEDGE_TYPE_NAME"].ColumnName = "Name";
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "KnowledgeType";
        }
        else
        {
            dt = new DataTable("KnowledgeType");
        }
        return dt;
    }

    private DataTable GetTagHD()
    {
        DataTable dt = CopySomeColumns(ProductTagBLL.Instance.ProductTagHeadSelectBLL(""), "KM_TAG_LVL1_NAME_1", "KM_TAG_LVL1_NAME");
        dt.Columns["KM_TAG_LVL1_NAME_1"].ColumnName = "ID";
        dt.Columns["KM_TAG_LVL1_NAME"].ColumnName = "Name";
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "KeywordsLevel1";
        }
        else
        {
            dt = new DataTable("KeywordsLevel1");
        }
        return dt;
    }

    private DataTable GetTagItems()
    {
        DataTable dt = CopySomeColumns(ProductTagBLL.Instance.ProductTagItemSelectBLL(""), "KM_TAG_LVL2_NAME_2", "KM_TAG_LVL2_NAME", "KM_TAG_LVL1_NAME_1", "KM_TAG_LVL1_NAME");
        dt.Columns["KM_TAG_LVL2_NAME_2"].ColumnName = "ID";
        dt.Columns["KM_TAG_LVL2_NAME"].ColumnName = "Name";
        dt.Columns["KM_TAG_LVL1_NAME_1"].ColumnName = "Lv1ID";
        dt.Columns["KM_TAG_LVL1_NAME"].ColumnName = "Lv1Name";
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "KeywordLevel2";
        }
        else
        {
            dt = new DataTable("KeywordLevel2");
        }
        return dt;
    }

    private DataTable GetDivision()
    {
        DataTable dt = CopySomeColumns(WebServiceBLL.Instance.GetDivision(), "DIVISION_ID", "DIVISION_NAME", "DEPARTMENT_ID");
        dt.Columns["DIVISION_ID"].ColumnName = "ID";
        dt.Columns["DIVISION_NAME"].ColumnName = "Name";
        dt.Columns["DEPARTMENT_ID"].ColumnName = "DepartmentID";
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "Division";
        }
        else
        {
            dt = new DataTable("Division");
        }
        return dt;
    }

    private DataTable GetDepartment()
    {
        DataTable dt = CopySomeColumns(WebServiceBLL.Instance.GetDepartment(), "DEPARTMENT_ID", "DEPARTMENT_NAME");
        dt.Columns["DEPARTMENT_ID"].ColumnName = "ID";
        dt.Columns["DEPARTMENT_NAME"].ColumnName = "Name";
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "Department";
        }
        else
        {
            dt = new DataTable("Department");
        }
        return dt;
    }

    private DataTable GetContentFilePath(string topicID)
    {
        string condition = " AND REF_STR = '" + topicID + "'";
        DataTable dt = CopySomeColumns(WebServiceBLL.Instance.GetContentFilePath(condition), "FILENAME_SYSTEM", "FILENAME_USER", "ISACTIVE", "FULLPATH");
        if (CheckTableNotEmpty(dt))
        {
            dt.TableName = "ContentFile";
        }
        else
        {
            dt = new DataTable("ContentFile");
        }
        return dt;
    }

    private string GetDownloadServerUrl()
    {
        string url = HttpContext.Current.Request.Url.ToString();
        string webserviceUrl = HttpContext.Current.Request.RawUrl.ToString();
        string appPath = HttpContext.Current.Request.ApplicationPath.ToString();
        string serverUrl = url.Replace(webserviceUrl, "");
        serverUrl = serverUrl + appPath + "/DownloadFile.ashx";
        return serverUrl;
    }

    private string GetLink(string fullPath, string userFileName, string serverUrl)
    {
        try
        {
            string serverPath = Server.MapPath("~");
            string dir = Path.GetDirectoryName(fullPath);
            int index1 = dir.IndexOf("UploadFile", 0);
            string uploadDir = dir.Substring(index1);
            string fileName = Path.GetFileName(fullPath);
            string filePath = Path.Combine(uploadDir, fileName);
            filePath = string.Format("{0}{1}{2}", serverUrl, "?link=", filePath);
            return filePath;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public string StripHTML(string input)
    {
        return Regex.Replace(input, "<.*?>", " ");
    }
    #endregion

    #region GET_MASTER_FILTER
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Master Filter")]
    public string GET_MASTER_FILTER(string Type)
    {
        string condition = string.Empty;
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = WebServiceBLL.Instance.GetMasterFilter(Type);
                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save File Upload")]
    public string SAVE_FILE(byte[] f, string fileName)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                MemoryStream ms = new MemoryStream(f);

                // instance a filestream pointing to the
                // storage folder, use the original file name
                // to name the resulting file

                string imgURL = Server.MapPath("./checktruck/iTMS/") + fileName;
                imgURL = imgURL.Replace("WebService", "UploadFile");
                imgURL = imgURL.Replace("webservice", "UploadFile");
                FileStream fs = new FileStream(imgURL, FileMode.Create);

                // write the memory stream containing the original
                // file as a byte array to the filestream
                ms.WriteTo(fs);

                // clean up
                ms.Close();
                fs.Close();
                fs.Dispose();

                // return OK if we made it this far
                DataTable dt = new DataTable();
                dt.Columns.Add("Path");
                dt.Rows.Add("UploadFile/checktruck/iTMS/" + fileName);
                dt.TableName = "FileInfo";
                DataTable dtCopy = dt.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
    #endregion

    #region DELETE_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Delete File Upload")]
    public string DELETE_FILE(string fileName)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                string imgURL = Server.MapPath("./checktruck/Upload/") + fileName;
                imgURL = imgURL.Replace("WebService", "UploadFile");
                imgURL = imgURL.Replace("webservice", "UploadFile");

                if (File.Exists(imgURL))
                    File.Delete(imgURL);

                DataTable dt = new DataTable();
                dt.Columns.Add("Path");
                dt.Rows.Add("UploadFile/checktruck/Upload/" + fileName);
                dt.TableName = "FileInfo";
                DataTable dtCopy = dt.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);

                return JsonConvert.SerializeObject(ds, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
    #endregion

    #region GET_CHECKCONTRACT_TRUCK_LIST
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get CheckContract Truck List")]
    public string GET_CHECKCONTRACT_TRUCK_LIST(string LICENSE = "", string VENDOR_ID = "", string CONTRACT_ID = "", string DATE_START = "", string DATE_END = "", string STATUS_ID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dt = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelect(LICENSE, VENDOR_ID, CONTRACT_ID, DATE_START, DATE_END, STATUS_ID);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_TRUCK_DATA_BY_CHASSIS
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get Truck Data By Chassis")]
    public string GET_TRUCK_DATA_BY_CHASSIS(string CHASSIS = "", string VENDOR_ID = "", string TRUCK_TYPE = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                if (!string.Equals(TRUCK_TYPE, string.Empty))
                {
                    if (TRUCK_TYPE == "1")
                        TRUCK_TYPE = "0";
                    else if (TRUCK_TYPE == "2")
                        TRUCK_TYPE = "3";
                }
                DataTable dt = SurpriseCheckContractBLL.Instance.ServiceTruckSelectByChasis(CHASSIS, VENDOR_ID, TRUCK_TYPE);
                DataSet ds = new DataSet();
                ds.Tables.Add(dt.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_CHECKCONTRACT_TRUCK_LIST_TAB2
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get CheckContract Truck List Tab2")]
    public string GET_CHECKCONTRACT_TRUCK_LIST_TAB2(string ID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractListSelectByID(ID);
                ds.Tables[0].TableName = "cmdPreContract";
                ds.Tables[1].TableName = "cmdPreContractFile";
                DataSet ds2 = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelectByID(ID);
                DataTable dtMCheckList = SurpriseCheckContractBLL.Instance.MCheckContractListSelect();
                ds2.Tables[0].TableName = "cmdPreContractData";
                ds.Tables.Add(ds2.Tables[0].Copy());
                dtMCheckList.TableName = "cmdPreContractCheckList";
                ds.Tables.Add(dtMCheckList.Copy());
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_CHECKCONTRACT_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get CheckContract Truck")]
    public string GET_CHECKCONTRACT_TRUCK(string ID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataSet ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelectByID(ID);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_SURPRISECHECKYEAR_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurprisecheckYear Truck")]
    public string GET_SURPRISECHECKYEAR_TRUCK(string DocID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSelectBLL(" AND DOC_ID = " + DocID);
                DataTable dtCopy = dtSurpriseCheckYearList.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_SURPRISECHECKYEAR_TRUCK_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurprisecheckYear TruckFile")]
    public string GET_SURPRISECHECKYEAR_TRUCK_FILE(string DocID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtFileStep1 = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckFileSelectBLL(" AND DOC_ID = " + DocID);
                DataTable dtCopy = dtFileStep1.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_SURPRISECHECKYEAR_TRUCK_DATA
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurprisecheckYear TruckData")]
    public string GET_SURPRISECHECKYEAR_TRUCK_DATA(string VENDORID = "", string SCONTRACTNO = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtTruck = SurpriseCheckYearBLL.Instance.DataTruckSelectBLL(" AND SVENDORID = '" + VENDORID + "' AND SCONTRACTNO = '" + SCONTRACTNO + "'");
                DataTable dtCopy = dtTruck.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GET_SURPRISECHECKYEAR_TRUCK_DETAIL
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Get SurprisecheckYear Truck Detail")]
    public string GET_SURPRISECHECKYEAR_TRUCK_DETAIL(string DocID = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dtSurpriseCheckYear = SurpriseCheckYearBLL.Instance.SurpriseCheckYearSelect(" AND ID = " + DocID);
                DataTable dtTeamCheckData = SurpriseCheckYearBLL.Instance.SurpriseCheckTeamSelect(" AND DOC_ID = " + DocID);
                DataTable dtCopy = dtSurpriseCheckYear.Copy();
                DataTable dtCopy2 = dtTeamCheckData.Copy();
                DataSet ds = new DataSet();
                ds.DataSetName = "DS";
                ds.Tables.Add(dtCopy);
                ds.Tables.Add(dtCopy2);
                var jsonSettings = new JsonSerializerSettings();
                jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
                jsonSettings.FloatParseHandling = FloatParseHandling.Double;
                jsonSettings.Formatting = Formatting.Indented;
                string json = JsonConvert.SerializeObject(ds, jsonSettings);
                return json.Replace(".0", "");
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_YEAR_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck Year truck")]
    public string SAVE_SURPRISECHECK_YEAR_TRUCK(string DocID, string SHEADREGISTERNO, string STRAILERREGISTERNO, string USERID)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISECHECK_YEAR_TRUCK", DocID, SHEADREGISTERNO, STRAILERREGISTERNO, USERID);
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSaveBLL(DocID, SHEADREGISTERNO, STRAILERREGISTERNO, "0", USERID, "1");
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_YEAR_DELETE_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck Year Delete truck")]
    public string SAVE_SURPRISECHECK_YEAR_DELETE_TRUCK(string DocID, string SHEADREGISTERNO, string USERID)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISECHECK_YEAR_DELETE_TRUCK", DocID, SHEADREGISTERNO, USERID);
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSaveBLL(DocID, SHEADREGISTERNO, string.Empty, string.Empty, USERID, "2");
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_YEAR_CHANGE_STATUS_TRUCK
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck Year Change Status truck")]
    public string SAVE_SURPRISECHECK_YEAR_CHANGE_STATUS_TRUCK(string ID, string USERID, string STATUS, string PATH_LICENSE_PTT, string PATH_LICENSE_VENDOR = "")
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                DataTable dt = new DataTable();
                this.WRITE_LOG("SAVE_SURPRISECHECK_YEAR_CHANGE_STATUS_TRUCK", ID, USERID, STATUS, PATH_LICENSE_PTT, PATH_LICENSE_VENDOR);
                string I_PATH_LICENSE_PTT_NEW = PATH_LICENSE_PTT.Replace("iTMS", "Upload");
                string I_PATH_LICENSE_VENDOR_NEW = PATH_LICENSE_VENDOR.Replace("iTMS", "Upload");
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckUpdateStatusBLL(ID, USERID, STATUS, I_PATH_LICENSE_PTT_NEW, I_PATH_LICENSE_VENDOR_NEW);
                this.CHANGE_PATH(dt, PATH_LICENSE_PTT);
                this.CHANGE_PATH(dt, PATH_LICENSE_VENDOR);
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_SURPRISECHECK_YEAR_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Surprisecheck Year Save File For PTT")]
    public string SAVE_SURPRISECHECK_YEAR_FILE(string DOC_ID, string jdtFileStep1)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_SURPRISECHECK_YEAR_FILE", DOC_ID, jdtFileStep1);
                DataTable dtFileStep1 = new DataTable();
                if (!string.Equals(jdtFileStep1, string.Empty))
                    dtFileStep1 = (DataTable)JsonConvert.DeserializeObject(jdtFileStep1, (typeof(DataTable)));
                DataSet ds = new DataSet("DS");
                dtFileStep1.TableName = "FILE";
                ds.Tables.Add(dtFileStep1.Copy());
                this.CHANGE_PATH(dtFileStep1);
                SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckFileSaveBLL(DOC_ID, ds);
                DataTable dtResult = new DataTable();
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_CHECKCONTRACT_TRUCK_CHANGE_STATUS
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save Contract Truck Change Status")]
    public string SAVE_CHECKCONTRACT_TRUCK_CHANGE_STATUS(string ID, string USERID, string REMARK, string STATUS, string SCONTRACTNO, string TRUCK_TYPE, string CHASIS_H, string CHASIS_T, string SHEADREGISTERNO, string STRAILERREGISTERNO, string CHECK_DATE, string PLACE, string SVENDORNAME, string SVENDORID)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_CHECKCONTRACT_TRUCK_CHANGE_STATUS", ID, USERID, REMARK, STATUS);
                SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(ID), int.Parse(STATUS), int.Parse(USERID), REMARK);
                DataTable dtResult = new DataTable();
                string STATUS_NAME = string.Empty;
                dtResult.Columns.Add("Status");
                dtResult.Rows.Add("Success");
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult);
                if (STATUS == "5")
                    STATUS_NAME = "ยืนยันนัดหมาย";
                else if (STATUS == "6")
                    STATUS_NAME = "ยกเลิกนัดหมาย";
                this.SendEmailCheckContractTruck(131, STATUS_NAME, SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, dtResult.Rows[0]["Status"].ToString(), SVENDORNAME, SVENDORID, USERID);
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_CHECKCONTRACT_TRUCK_SAVE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save CheckContract Truck Save")]
    public string SAVE_CHECKCONTRACT_TRUCK_SAVE(string ID, string CHECK_DATE, string TRUCK_TYPE, string CHASIS_H, string CHASIS_T, string SHEADREGISTERNO, string STRAILERREGISTERNO, string SVENDORID, string SVENDORNAME, string SCONTRACTID, string SCONTRACTNO, string PLACE, string REQUEST_BY, string STATUS, string USERID, string jdtUpload, string jdtUploadExpire, string SAVE_TYPE)
    {
        try
        {
            if (AuthenticationWebService(AuthenticationInfo))
            {
                this.WRITE_LOG("SAVE_CHECKCONTRACT_TRUCK_SAVE", ID, CHECK_DATE, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, PLACE, REQUEST_BY, STATUS, USERID, jdtUpload, jdtUploadExpire);
                DataTable dtUpload = new DataTable();
                DataTable dtUploadExpired = new DataTable();
                if (!string.Equals(jdtUpload, string.Empty))
                    dtUpload = (DataTable)JsonConvert.DeserializeObject(jdtUpload, (typeof(DataTable)));
                if (!string.Equals(jdtUploadExpire, string.Empty))
                    dtUploadExpired = (DataTable)JsonConvert.DeserializeObject(jdtUploadExpire, (typeof(DataTable)));

                DataTable dt = new DataTable("DT");
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("CHECK_DATE", typeof(string));
                dt.Columns.Add("TRUCK_TYPE", typeof(int));
                dt.Columns.Add("CHASIS_H", typeof(string));
                dt.Columns.Add("CHASIS_T", typeof(string));
                dt.Columns.Add("SHEADREGISTERNO", typeof(string));
                dt.Columns.Add("STRAILERREGISTERNO", typeof(string));
                dt.Columns.Add("SVENDORID", typeof(string));
                dt.Columns.Add("SCONTRACTID", typeof(int));
                dt.Columns.Add("PLACE", typeof(string));
                dt.Columns.Add("REQUEST_BY", typeof(int));
                dt.Columns.Add("IS_ACTIVE", typeof(int));
                dt.Columns.Add("CREATE_BY", typeof(int));
                dt.Columns.Add("UPDATE_BY", typeof(int));

                DataRow dr = dt.NewRow();
                dr["ID"] = ID;
                dr["CHECK_DATE"] = CHECK_DATE;
                dr["TRUCK_TYPE"] = TRUCK_TYPE;
                dr["CHASIS_H"] = CHASIS_H;
                dr["CHASIS_T"] = CHASIS_T;
                dr["SHEADREGISTERNO"] = SHEADREGISTERNO;
                dr["STRAILERREGISTERNO"] = STRAILERREGISTERNO;
                dr["SVENDORID"] = SVENDORID;
                dr["SCONTRACTID"] = SCONTRACTID;
                //dr["STRUCKID"] = ddlSHEADREGISTERNO.SelectedValue;
                dr["PLACE"] = PLACE;
                dr["REQUEST_BY"] = REQUEST_BY;
                dr["CREATE_BY"] = USERID;
                dr["UPDATE_BY"] = USERID;
                dr["IS_ACTIVE"] = STATUS;
                dt.Rows.Add(dr);

                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dt.Copy());
                dtUpload.TableName = "FILE";
                DataTable dtFile = dtUpload.Clone();
                if (!dtFile.Columns.Contains("FILE_TYPE"))
                {
                    dtFile.Columns.Add("FILE_TYPE");
                }
                foreach (DataRow item in dtUpload.Rows)
                {
                    dr = dtFile.NewRow();
                    dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
                    dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
                    dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
                    dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
                    dr["REMARK"] = item["REMARK"] + string.Empty;
                    dr["FILE_TYPE"] = "NOT_EXPIRED";
                    dtFile.Rows.Add(dr);
                }

                foreach (DataRow item in dtUploadExpired.Rows)
                {
                    dr = dtFile.NewRow();
                    dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
                    dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
                    dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
                    dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
                    dr["REMARK"] = item["REMARK"] + string.Empty;
                    dr["FILE_TYPE"] = "EXPIRED";
                    dtFile.Rows.Add(dr);
                }

                dtFile.TableName = "FILE";
                ds.Tables.Add(dtFile.Copy());

                this.CHANGE_PATH(dtFile);

                DataTable dtResult = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSave(ds);
                if (SAVE_TYPE == "1")
                    this.SendEmailCheckContractTruck(130, "ยื่นคำขอ", SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, dtResult.Rows[0]["RESULT"].ToString(), SVENDORNAME, SVENDORID, USERID);
                DataSet dsResult = new DataSet();
                dtResult.TableName = "Result";
                dsResult.Tables.Add(dtResult.Copy());
                return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
            }
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region SAVE_CHECKCONTRACT_TRUCK_SAVE_TAB2
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Save CheckContract Truck Save Tab2")]
    public string SAVE_CHECKCONTRACT_TRUCK_SAVE_TAB2(string ID, string CHECK_DATE, string TRUCK_TYPE, string CHASIS_H, string CHASIS_T, string SHEADREGISTERNO, string STRAILERREGISTERNO, string SVENDORID, string SVENDORNAME, string SCONTRACTID, string SCONTRACTNO, string PLACE, string REQUEST_BY, string USERID, string USERGROUP_ID, string jdtTCheckList, string jdtFileStep1, string jdtMCheckList)
    {
        try
        {
            //if (AuthenticationWebService(AuthenticationInfo))
            //{
            this.WRITE_LOG("SAVE_CHECKCONTRACT_TRUCK_SAVE_TAB2", ID, CHECK_DATE, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, SVENDORID, SVENDORNAME, SCONTRACTID, SCONTRACTNO, PLACE, REQUEST_BY, USERID, USERGROUP_ID, jdtTCheckList, jdtFileStep1, jdtMCheckList);
            DataSet ds = new DataSet("DS");
            DataTable dtTCheckList = new DataTable();
            DataTable dtTCheckListCopy = new DataTable();
            DataTable dtFileStep1 = new DataTable();
            DataTable dtMCheckList = new DataTable();
            if (!string.Equals(jdtTCheckList, string.Empty))
                dtTCheckList = (DataTable)JsonConvert.DeserializeObject(jdtTCheckList, (typeof(DataTable)));
            if (!string.Equals(jdtFileStep1, string.Empty))
                dtFileStep1 = (DataTable)JsonConvert.DeserializeObject(jdtFileStep1, (typeof(DataTable)));
            if (!string.Equals(jdtMCheckList, string.Empty))
                dtMCheckList = (DataTable)JsonConvert.DeserializeObject(jdtMCheckList, (typeof(DataTable)));
            int STATUS_ID = 2;//กรณีผ่านหมดทุกข้อ
            if (dtTCheckList != null)
            {
                dtTCheckListCopy = dtTCheckList.Clone();
                foreach (DataRow item in dtMCheckList.Rows)
                {
                    DataRow[] drData = dtTCheckList.Select("M_CHECK_CONTRACT_LIST_ID ='" + item["ID"].ToString() + "'");
                    DataRow dr = dtTCheckListCopy.NewRow();
                    int tmp = 0;
                    string Remark = string.Empty;
                    dr["M_CHECK_CONTRACT_LIST_ID"] = item["ID"] + string.Empty;
                    dr["T_CHECK_CONTRACT_ID"] = ID;
                    if (drData.Length > 0)
                    {
                        Remark = drData[0]["REMARK"] + "";
                        tmp = drData[0]["IS_ACTIVE"] + "" == "" ? 0 : int.Parse(drData[0]["IS_ACTIVE"] + "");
                    }
                    dr["REMARK"] = Remark;
                    if (tmp == 0)
                        dr["IS_ACTIVE"] = (object)DBNull.Value;
                    else
                        dr["IS_ACTIVE"] = tmp;
                    dr["CREATE_BY"] = USERID;
                    dr["UPDATE_BY"] = USERID;
                    dtTCheckListCopy.Rows.Add(dr);
                    if (drData.Length > 0)
                    {
                        if (drData[0]["IS_ACTIVE"].ToString() == "4")
                        {
                            STATUS_ID = 4;//กรณีไม้ผ่านแม้แต่ข้อเดว
                        }
                        if (STATUS_ID != 4)
                        {
                            if (drData[0]["IS_ACTIVE"] + "" == "3" || string.IsNullOrEmpty(drData[0]["IS_ACTIVE"] + ""))
                            {
                                STATUS_ID = 3;//กรณีไม้ผ่านแม้แต่ข้อเดว
                            }
                        }
                    }
                }
            }
            if (!dtTCheckListCopy.Columns.Contains("STATUS_ID"))
            {
                dtTCheckListCopy.Columns.Add("STATUS_ID");
            }

            this.CHANGE_PATH(dtFileStep1);
            dtTCheckListCopy.Rows[0]["STATUS_ID"] = STATUS_ID + string.Empty;
            ds = new DataSet("DS");
            dtTCheckListCopy.TableName = "DT";
            ds.Tables.Add(dtTCheckListCopy.Copy());
            dtFileStep1.TableName = "FILE";
            ds.Tables.Add(dtFileStep1.Copy());

            SurpriseCheckContractBLL.Instance.SurpriseCheckContractListSave(ds);

            if (USERGROUP_ID == "3")
            {
                if (STATUS_ID == 3)
                {
                    this.SendEmailCheckContractTruck(133, "แก้ไข", SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, "SUCCESS", SVENDORNAME, SVENDORID, USERID);
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Status");
                    dtResult.Rows.Add("Success");
                    DataSet dsResult = new DataSet();
                    dtResult.TableName = "Result";
                    dsResult.Tables.Add(dtResult);
                    return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
                }
            }
            else
            {
                if (STATUS_ID == 2)
                {
                    string Remark = string.Empty;
                    for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                        if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "2")
                        {
                            DataRow[] drData = dtMCheckList.Select("ID ='" + dtTCheckList.Rows[i]["M_CHECK_CONTRACT_LIST_ID"].ToString() + "'");
                            if (string.Equals(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "", ""))
                            {
                                if (Remark == string.Empty || Remark == "")
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark = drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                }
                                else
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                }
                            }
                        }
                    SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(ID), 2, int.Parse(USERID), Remark);
                    this.SendEmailCheckContractTruck(132, "ผ่าน", SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, "SUCCESS", SVENDORNAME, SVENDORID, USERID, Remark);
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Status");
                    dtResult.Rows.Add("Success");
                    DataSet dsResult = new DataSet();
                    dtResult.TableName = "Result";
                    dsResult.Tables.Add(dtResult);
                    return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
                }
                else if (STATUS_ID == 3)
                {
                    string Remark = string.Empty;
                    for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                        if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "3")
                        {
                            DataRow[] drData = dtMCheckList.Select("ID ='" + dtTCheckList.Rows[i]["M_CHECK_CONTRACT_LIST_ID"].ToString() + "'");

                            if (!string.Equals(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "", ""))
                            {
                                if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && string.Equals(Topicc, string.Empty))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark = drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark = drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") == dateCheck && string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && !string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") == dateCheck && !string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                            }
                        }
                    SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(ID), 3, int.Parse(USERID), Remark);
                    this.SendEmailCheckContractTruck(132, "แก้ไข", SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, "SUCCESS", SVENDORNAME, SVENDORID, USERID, Remark, ID);
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Status");
                    dtResult.Rows.Add("Success");
                    DataSet dsResult = new DataSet();
                    dtResult.TableName = "Result";
                    dsResult.Tables.Add(dtResult);
                    return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
                }
                else if (STATUS_ID == 4)
                {
                    string Remark = string.Empty;
                    for (int i = 0; i < dtTCheckList.Rows.Count; i++)
                        if (dtTCheckList.Rows[i]["IS_ACTIVE"].ToString() == "4")
                        {
                            DataRow[] drData = dtMCheckList.Select("ID ='" + dtTCheckList.Rows[i]["M_CHECK_CONTRACT_LIST_ID"].ToString() + "'");
                            if (!string.Equals(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "", ""))
                            {
                                if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && string.Equals(Topicc, string.Empty))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark = drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark = drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();
                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") == dateCheck && string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && !string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                                else if (DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "") > dateCheck && !string.Equals(Topicc, drData[0]["ID"] + ""))
                                {
                                    if (!string.Equals(dtTCheckList.Rows[i]["REMARK"].ToString(), string.Empty))
                                        Remark += ",<br /> " + drData[0]["DETAIL"] + " : " + dtTCheckList.Rows[i]["REMARK"].ToString();

                                    dateCheck = DateTime.Parse(dtTCheckList.Rows[i]["CREATE_DATETIME"] + "");
                                    Topicc = drData[0]["ID"] + "";
                                }
                            }
                        }
                    SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(ID), 4, int.Parse(USERID), Remark);
                    this.SendEmailCheckContractTruck(132, "ไม่ผ่านกรุณานัดหมายใหม่", SCONTRACTNO, TRUCK_TYPE, CHASIS_H, CHASIS_T, SHEADREGISTERNO, STRAILERREGISTERNO, CHECK_DATE, PLACE, "SUCCESS", SVENDORNAME, SVENDORID, USERID, Remark);
                    DataTable dtResult = new DataTable();
                    dtResult.Columns.Add("Status");
                    dtResult.Rows.Add("Success");
                    DataSet dsResult = new DataSet();
                    dtResult.TableName = "Result";
                    dsResult.Tables.Add(dtResult);
                    return JsonConvert.SerializeObject(dsResult, Formatting.Indented);
                }
            }
            //}
            return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion

    #region EXPORT_DOC_FILE
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Export File Document")]
    public string EXPORT_DOC_FILE(string I_STRUCKID, string I_USERID)
    {
        try
        {
            int count = 0;
            int checkzero = 1;
            this.WRITE_LOG("EXPORT_DOC_FILE", I_STRUCKID, I_USERID);
            //if (AuthenticationWebService(AuthenticationInfo))
            //{
            DataTable dtCheckList = SurpriseCheckBLL.Instance.CheckListSelect();
            DataTable dtCheckTruck = SurpriseCheckBLL.Instance.CheckTruckSelect(I_STRUCKID, "1", "");
            DataTable dtFileStep1Sum = SurpriseCheckBLL.Instance.CheckTruckFileSelect(I_STRUCKID, "1", "1");
            DataTable dtSurpriseCheckYearList = SurpriseCheckYearBLL.Instance.SurpriseCheckYearTruckSelectBLL(" AND STRUCKID = '" + I_STRUCKID + "' AND APPOINTYEAR = '" + DateTime.Now.Year.ToString() + "'");
            if (dtSurpriseCheckYearList.Rows.Count == 0)
                return "ไม่พบข้อมูลรถ ที่ทำการนัดหมายในปี " + DateTime.Now.Year.ToString();
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("Topic");
            dt2.Columns.Add("StausPass");
            dt2.Columns.Add("StatusNotPass");
            dt2.Columns.Add("Detail");
            for (int i = 0; i < dtCheckList.Rows.Count; i++)
            {
                string SEVIDENCENAME = string.Empty;
                DataRow[] dr = dtFileStep1Sum.Select("SCHECKLISTID ='" + dtCheckList.Rows[i]["SCHECKLISTID"].ToString() + "'");
                foreach (DataRow row in dr)
                {
                    SEVIDENCENAME = row["SEVIDENCENAME"].ToString();
                }
                if (dr.Length > 0)
                    if (!string.Equals(dtCheckList.Rows[i]["NDAY_MA"].ToString(), string.Empty))
                    {
                        if (string.Equals(dtCheckList.Rows[i]["NDAY_MA"].ToString(), "0"))
                        {
                            checkzero = 0;
                        }
                        else
                        {
                            if (count == 0)
                                count = int.Parse(dtCheckList.Rows[i]["NDAY_MA"].ToString());
                            else
                                count = int.Parse(dtCheckList.Rows[i]["NDAY_MA"].ToString()) < count ? int.Parse(dtCheckList.Rows[i]["NDAY_MA"].ToString()) : count;
                        }
                    }
                dt2.Rows.Add(dtCheckList.Rows[i]["SCHECKLISTNAME"].ToString(), dr.Length > 0 ? "" : "/", dr.Length > 0 ? "/" : "", SEVIDENCENAME);
            }

            if (dt2 == null || dt2.Rows.Count == 0)
            {
                return "ไม่มีข้อมูลที่ต้องการ Export";
            }

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/SurpriseCheck.xlsx"));
            //ExcelFile workbook = ExcelFile.Load("https://hq-srvtst-s08.pttplc.com/tms_dup/FileFormat/Admin/SurpriseCheck.xlsx");
            ExcelWorksheet worksheet = workbook.Worksheets["Sheet1"];
            DataTable dtFinal = dt2.Copy();

            if (dtCheckTruck.Rows.Count > 0)
                this.SetFormatCell(worksheet.Cells[1, 3], DateTime.Parse(dtCheckTruck.Rows[0]["DCHECK"].ToString()).ToString("dd  /  MM  /  yyyy  HH : mm : ss"), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);
            else
                this.SetFormatCell(worksheet.Cells[1, 3], DateTime.Parse(dtSurpriseCheckYearList.Rows[0]["DUPDATE"].ToString()).ToString("dd  /  MM  /  yyyy  HH : mm : ss"), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);
            this.SetFormatCell(worksheet.Cells[1, 7], dtSurpriseCheckYearList.Rows[0]["SABBREVIATION"].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);
            this.SetFormatCell(worksheet.Cells[1, 11], (dtSurpriseCheckYearList.Rows[0]["STRAILERREGISTERNO"].ToString() == "" ? dtSurpriseCheckYearList.Rows[0]["SHEADREGISTERNO"].ToString() : dtSurpriseCheckYearList.Rows[0]["SHEADREGISTERNO"].ToString() + "/" + dtSurpriseCheckYearList.Rows[0]["STRAILERREGISTERNO"].ToString()), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);
            this.SetFormatCell(worksheet.Cells[2, 3], dtSurpriseCheckYearList.Rows[0]["APPOINTLOCATE"].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);

            string wheel = string.Empty;
            if (!string.Equals(dtSurpriseCheckYearList.Rows[0]["NWHEELS"].ToString(), string.Empty))
            {
                if (!string.Equals(dtSurpriseCheckYearList.Rows[0]["STRAILERREGISTERNO"].ToString(), string.Empty))
                {
                    wheel = "22 ล้อ";
                }
                else
                {
                    if (string.Equals(dtSurpriseCheckYearList.Rows[0]["NWHEELS"].ToString(), "10"))
                    {
                        wheel = "10 ล้อ";
                    }
                    else if (string.Equals(dtSurpriseCheckYearList.Rows[0]["NWHEELS"].ToString(), "12"))
                    {
                        wheel = "12 ล้อ";
                    }
                }
            }

            this.SetFormatCell(worksheet.Cells[2, 11], wheel, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);

            for (int i = 5; i <= dtFinal.Rows.Count + 4; i++)
            {
                for (int j = 2; j <= dtFinal.Columns.Count + 1; j++)
                {//Export Detail
                    if (j == 2)
                        this.SetFormatCell(worksheet.Cells[i, j], dtFinal.Rows[i - 5][j - 2].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, true, 24);
                    else
                        this.SetFormatCell(worksheet.Cells[i, j + 6], dtFinal.Rows[i - 5][j - 2].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);
                }
            }
            string imgURL1 = string.Empty;
            if (!string.Equals(dtSurpriseCheckYearList.Rows[0]["SPATH_PTT"].ToString(), string.Empty))
            {
                imgURL1 = Server.MapPath("") + dtSurpriseCheckYearList.Rows[0]["SPATH_PTT"].ToString();
                imgURL1 = imgURL1.Replace("WebService", "");
                imgURL1 = imgURL1.Replace("webservice", "");
                imgURL1 = imgURL1.Replace("/", "\\");
            }
            string imgURL2 = string.Empty;
            if (!string.Equals(dtSurpriseCheckYearList.Rows[0]["SPATH_VENDOR"].ToString(), string.Empty))
            {
                imgURL2 = Server.MapPath("") + dtSurpriseCheckYearList.Rows[0]["SPATH_VENDOR"].ToString();
                imgURL2 = imgURL2.Replace("WebService", "");
                imgURL2 = imgURL2.Replace("webservice", "");
                imgURL2 = imgURL2.Replace("/", "\\");
            }

            if (!string.Equals(imgURL1, string.Empty))
                worksheet.Pictures.Add(imgURL1, "B40", "D41");
            if (!string.Equals(imgURL2, string.Empty))
                worksheet.Pictures.Add(imgURL2, "H40", "I41");

            if (dtCheckTruck.Rows.Count > 0) 
                this.SetFormatCell(worksheet.Cells[36, 3], ((dtFileStep1Sum.Rows.Count > 0 && checkzero == 0 ? "หยุดรถเพื่อแก้ไขทันที" : (dtFileStep1Sum.Rows.Count > 0 && count > 0 ? "แก้ไขให้เสร็จภายในวันที่ " + DateTime.Parse(dtCheckTruck.Rows[0]["DCHECK"].ToString()).AddDays(count).ToString("dd  /  MM  /  yyyy") + "  (ให้นับวันที่ตรวจพบเป็นวันแรกของการแก้ไขด้วย)" : "ปกติ"))), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, true, 26);
            else
                this.SetFormatCell(worksheet.Cells[36, 3], ((dtFileStep1Sum.Rows.Count > 0 && checkzero == 0 ? "หยุดรถเพื่อแก้ไขทันที" : (dtFileStep1Sum.Rows.Count > 0 && count > 0 ? "แก้ไขให้เสร็จภายในวันที่ " + DateTime.Parse(dtSurpriseCheckYearList.Rows[0]["DUPDATE"].ToString()).AddDays(count).ToString("dd  /  MM  /  yyyy") + "  (ให้นับวันที่ตรวจพบเป็นวันแรกของการแก้ไขด้วย)" : "ปกติ"))), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, true, 26);
           
            this.SetFormatCell(worksheet.Cells[41, 1], dtSurpriseCheckYearList.Rows[0]["FULLNAME"].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, true, 24);

            int columnCount = worksheet.CalculateMaxUsedColumns();

            string Path = this.CheckPath();
            string FileName = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".pdf";

            workbook.Save(Path + "\\" + FileName);

            this.SendEmailYear(135, dtSurpriseCheckYearList.Rows[0]["SHEADREGISTERNO"].ToString(), "", "", "", "", "", "", I_USERID, "", Path + "\\" + FileName);

            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("Link");
            dtResult.Rows.Add("https://ptttms.pttplc.com/TMS/Export/" + FileName);
            DataSet dsResult = new DataSet();
            dtResult.TableName = "cmdForm";
            dsResult.Tables.Add(dtResult);
            var jsonSettings = new JsonSerializerSettings();
            jsonSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
            jsonSettings.FloatParseHandling = FloatParseHandling.Double;
            jsonSettings.Formatting = Formatting.Indented;

            return JsonConvert.SerializeObject(dsResult, jsonSettings);
            //}
            //return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText, int FontSize)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
            cell.Style.Font.Size = FontSize * 10;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    #endregion

    #region SEND_BOOKING
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Send Bookinng Calendar")]
    public string SEND_BOOKING(string DOC_ID, string CONTRACTNO, string VENDOR_ID, string VENDORNAME, string TOTALCAR, string APPOINTDATE, string APPOINTDATEEND, string APPOINTLOCATION, string USERCHECKID, string STATUS, string REMARK, string CC_EMAIL = "")
    {
        int TemplateID = 127;
        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);

            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();
                DataTable dt = VendorBLL.Instance.VendorSelectBLL(" AND SVENDORID = '" + VENDOR_ID + "'");
                if (TemplateID == 127)
                {
                    Body = Body.Replace("{CONTRACT}", CONTRACTNO);
                    Body = Body.Replace("{VENDOR}", VENDORNAME);
                    Body = Body.Replace("{TOTALCAR}", TOTALCAR);
                    Body = Body.Replace("{APPOINTDATE}", APPOINTDATE);
                    Body = Body.Replace("{APPOINTLOCATE}", APPOINTLOCATION);
                    Body = Body.Replace("{STATUS}", STATUS);
                    Body = Body.Replace("{REMARK}", REMARK);
                    string Link = "https://ptttms.pttplc.com/TMS/surpriseChk_ShowData.aspx?DocID=" + MachineKey.Encode(Encoding.UTF8.GetBytes(DOC_ID), MachineKeyProtection.All);
                    //Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    Body = Regex.Replace(Body, "<.*?>", String.Empty);
                    Body = Body.Replace("&nbsp;", " ");
                    Body = Body.Replace("{LINK}", "");
                    //Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
                    MailService.SendBooking(DateTime.ParseExact(APPOINTDATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-7), DateTime.ParseExact(APPOINTDATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-14), DateTime.ParseExact(APPOINTDATEEND, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-7), Subject, Body, APPOINTLOCATION, ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(USERCHECKID), dt.Rows[0]["EMAIL"].ToString(), false, true), CC_EMAIL, "SurpriseCheck3", "EMAIL_TEST");
                }
            }
            //MailService.SendBooking(DateTime.ParseExact(DATESTART, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-7), DateTime.ParseExact(UTC, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-14), DateTime.ParseExact(DATEEND, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture).AddHours(-7), SUBJECT, BODY, LOCATION, ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), string.Empty, false, true),string.Empty,);
            return "";
            //return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }

    #endregion

    #region WRITE_LOG
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Write log text file")]
    public void WRITE_LOG(string SERVICE_NAME = "", string PARAMETER1 = "", string PARAMETER2 = "", string PARAMETER3 = "", string PARAMETER4 = "", string PARAMETER5 = "", string PARAMETER6 = "", string PARAMETER7 = "", string PARAMETER8 = "", string PARAMETER9 = "", string PARAMETER10 = "", string PARAMETER11 = "", string PARAMETER12 = "", string PARAMETER13 = "", string PARAMETER14 = "", string PARAMETER15 = "", string PARAMETER16 = "", string PARAMETER17 = "", string PARAMETER18 = "", string PARAMETER19 = "", string PARAMETER20 = "")
    {
        try
        {
            string text = PARAMETER1 + ",\n" + PARAMETER2 + ",\n" + PARAMETER3 + ",\n" + PARAMETER4 + ",\n" + PARAMETER5 + ",\n" + PARAMETER6 + ",\n" + PARAMETER7 + ",\n" + PARAMETER8 + ",\n" + PARAMETER9 + ",\n" + PARAMETER10 + ",\n" + PARAMETER11 + ",\n" + PARAMETER12 + ",\n" + PARAMETER13 + ",\n" + PARAMETER14 + ",\n" + PARAMETER15 + ",\n" + PARAMETER16 + ",\n" + PARAMETER17 + ",\n" + PARAMETER18 + ",\n" + PARAMETER19 + ",\n" + PARAMETER20;
            System.IO.File.WriteAllText(Server.MapPath("~/Log/") + SERVICE_NAME + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + ".txt", text);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    #endregion

    #region CHANGE_PATH
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Change Path Keep file")]
    public void CHANGE_PATH(DataTable dt, string PATH = "")
    {
        try
        {
            if (dt.Rows.Count > 0)
            {
                if (dt.Columns.Contains("FULLPATH"))
                {
                    dt.Columns["FULLPATH"].ColumnName = "SFILEPATH";
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string sourceFile = Server.MapPath(dt.Rows[i]["SFILEPATH"].ToString());
                    sourceFile = sourceFile.Replace("WebService\\", "");
                    sourceFile = sourceFile.Replace("webservice\\", "");
                    string destinationFile = Server.MapPath(dt.Rows[i]["SFILEPATH"].ToString());
                    destinationFile = destinationFile.Replace("WebService\\", "");
                    destinationFile = destinationFile.Replace("webservice\\", "");
                    bool tmp = sourceFile.Contains("iTMS");
                    if (tmp == true)
                    {
                        destinationFile = destinationFile.Replace("iTMS", "Upload");
                        System.IO.File.Move(sourceFile, destinationFile);
                    }
                }
            }
            if (!string.Equals(PATH, string.Empty))
            {
                string sourceFile = Server.MapPath(PATH);
                sourceFile = sourceFile.Replace("WebService\\", "");
                sourceFile = sourceFile.Replace("webservice\\", "");
                string destinationFile = Server.MapPath(PATH);
                destinationFile = destinationFile.Replace("WebService\\", "");
                destinationFile = destinationFile.Replace("webservice\\", "");
                bool tmp = sourceFile.Contains("iTMS");
                if (tmp == true)
                {
                    destinationFile = destinationFile.Replace("iTMS", "Upload");
                    System.IO.File.Move(sourceFile, destinationFile);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    #endregion
}

public class TMSAuthentication : SoapHeader
{
    //private static string User = "TMS";
    //private static string Key = "9e5dfc0ebc078e4f03650d778f13d412";
    private string user = ConfigurationManager.AppSettings["WebServiceUser"];
    private string key = ConfigurationManager.AppSettings["WebServiceKey"];

    public string Username;
    public string Password;
    public string AuthenticatedToken;

    public bool IsUserValid()
    {
        if (Username == user && Password == key)
        {
            // Create and store the AuthenticatedToken before returning it
            AuthenticatedToken = Guid.NewGuid().ToString();
            HttpRuntime.Cache.Add(
                AuthenticatedToken,
                Username,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(60),
                System.Web.Caching.CacheItemPriority.NotRemovable,
                null);

            return true;
        }
        else
        {
            throw new AuthenticationException("Please provide a Username and Password.");
        }
    }
}
