﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rpt_WorkSheet
/// </summary>
public class rpt_WorkSheet : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand PageHeader;
    private XRLabel xrLabel28;
    private XRLabel xrLabel27;
    private XRLabel xrLabel29;
    private XRLabel xrLabel31;
    private XRLabel xrLabel30;
    private XRLabel xrLabel23;
    private XRLabel xrLabel22;
    private XRLabel xrLabel24;
    private XRLabel xrLabel26;
    private XRLabel xrLabel25;
    private XRLabel xrLabel32;
    private XRLabel xrLabel39;
    private XRLabel xrLabel38;
    private XRLabel xrLabel40;
    private XRLabel xrLabel42;
    private XRLabel xrLabel41;
    private XRLabel xrLabel34;
    private XRLabel xrLabel33;
    private XRLabel xrLabel35;
    private XRLabel xrLabel37;
    private XRLabel xrLabel36;
    private XRLabel xrLabel7;
    private XRLabel xrLabel6;
    private XRLabel xrLabel8;
    private XRLabel xrLabel10;
    private XRLabel xrLabel9;
    private XRLabel xrLabel2;
    private XRLabel xrLabel1;
    private XRLabel xrLabel3;
    private XRLabel xrLabel5;
    private XRLabel xrLabel4;
    private XRLabel xrLabel11;
    private XRLabel xrLabel18;
    private XRLabel xrLabel17;
    private XRLabel xrLabel19;
    private XRLabel xrLabel21;
    private XRLabel xrLabel20;
    private XRLabel xrLabel13;
    private XRLabel xrLabel12;
    private XRLabel xrLabel14;
    private XRLabel xrLabel16;
    private XRLabel xrLabel15;
    private XRLabel xrLabel48;
    private XRLabel xrLabel47;
    private XRLabel xrLabel46;
    private XRLabel xrLabel45;
    private XRLabel xrLabel44;
    private XRLabel xrLabel43;
    private XRLabel xrLabel49;
    private XRLabel xrLabel57;
    private XRLabel xrLabel56;
    private XRLabel xrLabel55;
    private XRLabel xrLabel54;
    private XRLabel xrLabel53;
    private XRLabel xrLabel52;
    private XRLabel xrLabel51;
    private XRLabel xrLabel50;
    private XRLabel xrLabel63;
    private XRLabel xrLabel62;
    private XRLabel xrLabel60;
    private XRLabel xrLabel61;
    private XRLabel xrLabel58;
    private XRLabel xrLabel59;
    private XRLabel xrLabel64;
    private XRTable xrTable1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTable xrTable2;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell6;
    private PageFooterBand PageFooter;
    private XRLabel xrLabel70;
    private XRLabel xrLabel69;
    private XRLabel xrLabel68;
    private XRLabel xrLabel73;
    private XRLabel xrLabel72;
    private XRLabel xrLabel71;
    private XRLabel xrLabel67;
    private XRTable xrTable3;
    private XRTableRow xrTableRow3;
    private XRTableCell xrTableCell7;
    private XRTableCell xrTableCell8;
    private XRLabel xrLabel65;
    private XRLabel xrLabel66;
    private XRLabel xrLabel78;
    private XRLabel xrLabel77;
    private XRLabel xrLabel76;
    private XRLabel xrLabel75;
    private XRLabel xrLabel74;
    private ds_WaterSheet ds_WaterSheet1;
    private XRLabel xrLabel84;
    private XRLabel xrLabel83;
    private XRLabel xrLabel82;
    private XRLabel xrLabel81;
    private XRLabel xrLabel80;
    private XRLabel xrLabel79;
    private XRLabel xrLabel86;
    private XRLabel xrLabel85;
    private ReportFooterBand ReportFooter;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public rpt_WorkSheet()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        string resourceFileName = "rpt_WorkSheet.resx";
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
        this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
        this.ds_WaterSheet1 = new ds_WaterSheet();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.ds_WaterSheet1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.Detail.HeightF = 25F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.StylePriority.UseFont = false;
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable2
        // 
        this.xrTable2.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
        this.xrTable2.SizeF = new System.Drawing.SizeF(437.5F, 25F);
        this.xrTable2.StylePriority.UseFont = false;
        this.xrTable2.StylePriority.UseTextAlignment = false;
        this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Weight = 1D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DS_WORK_SHEET.SLOT_NO")});
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.StylePriority.UseBorders = false;
        this.xrTableCell4.Text = "xrTableCell4";
        this.xrTableCell4.Weight = 1D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DS_WORK_SHEET.LEVEL_NO")});
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.StylePriority.UseBorders = false;
        this.xrTableCell5.Text = "xrTableCell5";
        this.xrTableCell5.Weight = 1D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DS_WORK_SHEET.CAPACITY", "{0:#,#}")});
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.StylePriority.UseBorders = false;
        this.xrTableCell6.Text = "xrTableCell6";
        this.xrTableCell6.Weight = 2.375D;
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 46.24999F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 28.74998F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel84,
            this.xrLabel83,
            this.xrLabel82,
            this.xrLabel81,
            this.xrLabel80,
            this.xrLabel79,
            this.xrTable1,
            this.xrLabel64,
            this.xrLabel63,
            this.xrLabel62,
            this.xrLabel60,
            this.xrLabel61,
            this.xrLabel58,
            this.xrLabel59,
            this.xrLabel57,
            this.xrLabel56,
            this.xrLabel55,
            this.xrLabel54,
            this.xrLabel53,
            this.xrLabel52,
            this.xrLabel51,
            this.xrLabel50,
            this.xrLabel49,
            this.xrLabel48,
            this.xrLabel47,
            this.xrLabel46,
            this.xrLabel45,
            this.xrLabel44,
            this.xrLabel43,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel29,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel24,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel32,
            this.xrLabel39,
            this.xrLabel38,
            this.xrLabel40,
            this.xrLabel42,
            this.xrLabel41,
            this.xrLabel34,
            this.xrLabel33,
            this.xrLabel35,
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel8,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel3,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel11,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel19,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel14,
            this.xrLabel16,
            this.xrLabel15});
        this.PageHeader.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.PageHeader.HeightF = 573.9583F;
        this.PageHeader.Name = "PageHeader";
        this.PageHeader.StylePriority.UseFont = false;
        // 
        // xrLabel84
        // 
        this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel84.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(625.0001F, 277.1249F);
        this.xrLabel84.Name = "xrLabel84";
        this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel84.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel84.StylePriority.UseBorders = false;
        this.xrLabel84.StylePriority.UseFont = false;
        this.xrLabel84.StylePriority.UseTextAlignment = false;
        this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel83
        // 
        this.xrLabel83.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(525F, 277.125F);
        this.xrLabel83.Name = "xrLabel83";
        this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel83.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel83.StylePriority.UseFont = false;
        this.xrLabel83.StylePriority.UseTextAlignment = false;
        this.xrLabel83.Text = "น้ำหนักบรรทุก";
        this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel82
        // 
        this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel82.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(381.25F, 277.125F);
        this.xrLabel82.Name = "xrLabel82";
        this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel82.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel82.StylePriority.UseBorders = false;
        this.xrLabel82.StylePriority.UseFont = false;
        this.xrLabel82.StylePriority.UseTextAlignment = false;
        this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel81
        // 
        this.xrLabel81.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(265.625F, 277.125F);
        this.xrLabel81.Name = "xrLabel81";
        this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel81.SizeF = new System.Drawing.SizeF(115.625F, 23F);
        this.xrLabel81.StylePriority.UseFont = false;
        this.xrLabel81.StylePriority.UseTextAlignment = false;
        this.xrLabel81.Text = "น้ำหนักรถ(หัวเทรลเลอร์)";
        this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel80
        // 
        this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel80.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(121.875F, 277.125F);
        this.xrLabel80.Name = "xrLabel80";
        this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel80.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel80.StylePriority.UseBorders = false;
        this.xrLabel80.StylePriority.UseFont = false;
        this.xrLabel80.StylePriority.UseTextAlignment = false;
        this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel79
        // 
        this.xrLabel79.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(0F, 277.125F);
        this.xrLabel79.Name = "xrLabel79";
        this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel79.SizeF = new System.Drawing.SizeF(121.875F, 23F);
        this.xrLabel79.StylePriority.UseFont = false;
        this.xrLabel79.StylePriority.UseTextAlignment = false;
        this.xrLabel79.Text = "น้ำหนักรถ(หางเทรลเลอร์)";
        this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTable1
        // 
        this.xrTable1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
        this.xrTable1.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 548.9582F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
        this.xrTable1.SizeF = new System.Drawing.SizeF(437.5F, 25F);
        this.xrTable1.StylePriority.UseBackColor = false;
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.StylePriority.UseTextAlignment = false;
        this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Weight = 1D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.StylePriority.UseBorders = false;
        this.xrTableCell1.StylePriority.UseTextAlignment = false;
        this.xrTableCell1.Text = "ช่องที่";
        this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.xrTableCell1.Weight = 1D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.StylePriority.UseBorders = false;
        this.xrTableCell2.Text = "แป้นที่";
        this.xrTableCell2.Weight = 1D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
        this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.StylePriority.UseBackColor = false;
        this.xrTableCell3.StylePriority.UseBorders = false;
        this.xrTableCell3.Text = "ความจุที่ระดับแป้น(ลิตร)";
        this.xrTableCell3.Weight = 2.375D;
        // 
        // xrLabel64
        // 
        this.xrLabel64.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Underline);
        this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(0F, 517.0001F);
        this.xrLabel64.Name = "xrLabel64";
        this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel64.SizeF = new System.Drawing.SizeF(134.375F, 22.99994F);
        this.xrLabel64.StylePriority.UseFont = false;
        this.xrLabel64.StylePriority.UseTextAlignment = false;
        this.xrLabel64.Text = "ความจุในแต่ละช่อง";
        this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel63
        // 
        this.xrLabel63.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(34.37502F, 486.2917F);
        this.xrLabel63.Name = "xrLabel63";
        this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel63.SizeF = new System.Drawing.SizeF(736.625F, 23F);
        this.xrLabel63.StylePriority.UseFont = false;
        this.xrLabel63.StylePriority.UseTextAlignment = false;
        this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel62
        // 
        this.xrLabel62.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Underline);
        this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(0F, 456.2083F);
        this.xrLabel62.Name = "xrLabel62";
        this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel62.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel62.StylePriority.UseFont = false;
        this.xrLabel62.StylePriority.UseTextAlignment = false;
        this.xrLabel62.Text = "สาเหตุที่เข้าวัดน้ำ";
        this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel60
        // 
        this.xrLabel60.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(468.75F, 419.7917F);
        this.xrLabel60.Name = "xrLabel60";
        this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel60.SizeF = new System.Drawing.SizeF(31.25F, 23F);
        this.xrLabel60.StylePriority.UseFont = false;
        this.xrLabel60.StylePriority.UseTextAlignment = false;
        this.xrLabel60.Text = "โทร.";
        this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel61
        // 
        this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel61.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(500F, 419.7917F);
        this.xrLabel61.Name = "xrLabel61";
        this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel61.SizeF = new System.Drawing.SizeF(214.5833F, 23F);
        this.xrLabel61.StylePriority.UseBorders = false;
        this.xrLabel61.StylePriority.UseFont = false;
        this.xrLabel61.StylePriority.UseTextAlignment = false;
        this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel58
        // 
        this.xrLabel58.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(0F, 419.7917F);
        this.xrLabel58.Name = "xrLabel58";
        this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel58.SizeF = new System.Drawing.SizeF(37.5F, 23F);
        this.xrLabel58.StylePriority.UseFont = false;
        this.xrLabel58.StylePriority.UseTextAlignment = false;
        this.xrLabel58.Text = "ที่อยู่";
        this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel59
        // 
        this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel59.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(37.5F, 419.7917F);
        this.xrLabel59.Name = "xrLabel59";
        this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel59.SizeF = new System.Drawing.SizeF(408.3333F, 23F);
        this.xrLabel59.StylePriority.UseBorders = false;
        this.xrLabel59.StylePriority.UseFont = false;
        this.xrLabel59.StylePriority.UseTextAlignment = false;
        this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel57
        // 
        this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel57.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(428.1249F, 385.375F);
        this.xrLabel57.Name = "xrLabel57";
        this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel57.SizeF = new System.Drawing.SizeF(332.8752F, 23F);
        this.xrLabel57.StylePriority.UseBorders = false;
        this.xrLabel57.StylePriority.UseFont = false;
        this.xrLabel57.StylePriority.UseTextAlignment = false;
        this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel56
        // 
        this.xrLabel56.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(354.1667F, 385.375F);
        this.xrLabel56.Name = "xrLabel56";
        this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel56.SizeF = new System.Drawing.SizeF(73.95816F, 23F);
        this.xrLabel56.StylePriority.UseFont = false;
        this.xrLabel56.StylePriority.UseTextAlignment = false;
        this.xrLabel56.Text = "ขนส่งในนาม";
        this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel55
        // 
        this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel55.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(63.54167F, 385.375F);
        this.xrLabel55.Name = "xrLabel55";
        this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel55.SizeF = new System.Drawing.SizeF(273.9584F, 23F);
        this.xrLabel55.StylePriority.UseBorders = false;
        this.xrLabel55.StylePriority.UseFont = false;
        this.xrLabel55.StylePriority.UseTextAlignment = false;
        this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel54
        // 
        this.xrLabel54.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0F, 385.375F);
        this.xrLabel54.Name = "xrLabel54";
        this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel54.SizeF = new System.Drawing.SizeF(63.54167F, 23F);
        this.xrLabel54.StylePriority.UseFont = false;
        this.xrLabel54.StylePriority.UseTextAlignment = false;
        this.xrLabel54.Text = "ชื่อเจ้าของรถ";
        this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel53
        // 
        this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel53.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(400F, 347.875F);
        this.xrLabel53.Name = "xrLabel53";
        this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel53.SizeF = new System.Drawing.SizeF(280.2083F, 23F);
        this.xrLabel53.StylePriority.UseBorders = false;
        this.xrLabel53.StylePriority.UseFont = false;
        this.xrLabel53.StylePriority.UseTextAlignment = false;
        this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel52
        // 
        this.xrLabel52.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(262.5F, 347.875F);
        this.xrLabel52.Name = "xrLabel52";
        this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel52.SizeF = new System.Drawing.SizeF(137.5F, 23F);
        this.xrLabel52.StylePriority.UseFont = false;
        this.xrLabel52.StylePriority.UseTextAlignment = false;
        this.xrLabel52.Text = "ประเภทผู้ประกอบการขนส่ง";
        this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel51
        // 
        this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel51.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(63.54167F, 347.875F);
        this.xrLabel51.Name = "xrLabel51";
        this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel51.SizeF = new System.Drawing.SizeF(168.75F, 23F);
        this.xrLabel51.StylePriority.UseBorders = false;
        this.xrLabel51.StylePriority.UseFont = false;
        this.xrLabel51.StylePriority.UseTextAlignment = false;
        this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel50
        // 
        this.xrLabel50.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(0F, 347.875F);
        this.xrLabel50.Name = "xrLabel50";
        this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel50.SizeF = new System.Drawing.SizeF(63.54167F, 23F);
        this.xrLabel50.StylePriority.UseFont = false;
        this.xrLabel50.StylePriority.UseTextAlignment = false;
        this.xrLabel50.Text = "รหัสผู้ขนส่ง";
        this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel49
        // 
        this.xrLabel49.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Underline);
        this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(0F, 311.4167F);
        this.xrLabel49.Name = "xrLabel49";
        this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel49.SizeF = new System.Drawing.SizeF(228.125F, 23F);
        this.xrLabel49.StylePriority.UseFont = false;
        this.xrLabel49.StylePriority.UseTextAlignment = false;
        this.xrLabel49.Text = "รายละเอียดเกี่ยวกับผู้ประกอบการขนส่ง";
        this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel48
        // 
        this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel48.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(644.9583F, 241.625F);
        this.xrLabel48.Name = "xrLabel48";
        this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel48.SizeF = new System.Drawing.SizeF(126.0417F, 23.00002F);
        this.xrLabel48.StylePriority.UseBorders = false;
        this.xrLabel48.StylePriority.UseFont = false;
        this.xrLabel48.StylePriority.UseTextAlignment = false;
        this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel47
        // 
        this.xrLabel47.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(544.9583F, 241.625F);
        this.xrLabel47.Name = "xrLabel47";
        this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel47.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel47.StylePriority.UseFont = false;
        this.xrLabel47.StylePriority.UseTextAlignment = false;
        this.xrLabel47.Text = "วิธีเติมน้ำมัน";
        this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel46
        // 
        this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel46.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(354.1667F, 241.625F);
        this.xrLabel46.Name = "xrLabel46";
        this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel46.SizeF = new System.Drawing.SizeF(176.0416F, 23.00002F);
        this.xrLabel46.StylePriority.UseBorders = false;
        this.xrLabel46.StylePriority.UseFont = false;
        this.xrLabel46.StylePriority.UseTextAlignment = false;
        this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel45
        // 
        this.xrLabel45.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(254.1667F, 241.625F);
        this.xrLabel45.Name = "xrLabel45";
        this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel45.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel45.StylePriority.UseFont = false;
        this.xrLabel45.StylePriority.UseTextAlignment = false;
        this.xrLabel45.Text = "ประเภทสัญญา";
        this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel44
        // 
        this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel44.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(100F, 241.625F);
        this.xrLabel44.Name = "xrLabel44";
        this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel44.SizeF = new System.Drawing.SizeF(140.625F, 23.00002F);
        this.xrLabel44.StylePriority.UseBorders = false;
        this.xrLabel44.StylePriority.UseFont = false;
        this.xrLabel44.StylePriority.UseTextAlignment = false;
        this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel43
        // 
        this.xrLabel43.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 241.625F);
        this.xrLabel43.Name = "xrLabel43";
        this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel43.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel43.StylePriority.UseFont = false;
        this.xrLabel43.StylePriority.UseTextAlignment = false;
        this.xrLabel43.Text = "ชนิดน้ำมันที่บรรทุก";
        this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel28
        // 
        this.xrLabel28.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(279.1667F, 172.9167F);
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(67.70837F, 23F);
        this.xrLabel28.StylePriority.UseFont = false;
        this.xrLabel28.StylePriority.UseTextAlignment = false;
        this.xrLabel28.Text = "รุ่นเทรลเลอร์";
        this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel27
        // 
        this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel27.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(121.875F, 172.9167F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel27.SizeF = new System.Drawing.SizeF(157.2917F, 23F);
        this.xrLabel27.StylePriority.UseBorders = false;
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UseTextAlignment = false;
        this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel29
        // 
        this.xrLabel29.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(478.1249F, 172.9167F);
        this.xrLabel29.Name = "xrLabel29";
        this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel29.SizeF = new System.Drawing.SizeF(178.1251F, 23F);
        this.xrLabel29.StylePriority.UseFont = false;
        this.xrLabel29.StylePriority.UseTextAlignment = false;
        this.xrLabel29.Text = "วันที่จดทะเบียนครั้งแรก(หางเทรลเลอร์)";
        this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel31
        // 
        this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel31.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(658.3334F, 172.9167F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(112.6666F, 23F);
        this.xrLabel31.StylePriority.UseBorders = false;
        this.xrLabel31.StylePriority.UseFont = false;
        this.xrLabel31.StylePriority.UseTextAlignment = false;
        this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel30
        // 
        this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel30.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(346.8751F, 172.9167F);
        this.xrLabel30.Name = "xrLabel30";
        this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel30.SizeF = new System.Drawing.SizeF(131.2498F, 23F);
        this.xrLabel30.StylePriority.UseBorders = false;
        this.xrLabel30.StylePriority.UseFont = false;
        this.xrLabel30.StylePriority.UseTextAlignment = false;
        this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel23
        // 
        this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel23.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(625.0001F, 104.1667F);
        this.xrLabel23.Name = "xrLabel23";
        this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel23.SizeF = new System.Drawing.SizeF(138.7083F, 23F);
        this.xrLabel23.StylePriority.UseBorders = false;
        this.xrLabel23.StylePriority.UseFont = false;
        this.xrLabel23.StylePriority.UseTextAlignment = false;
        this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel22
        // 
        this.xrLabel22.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(445.8332F, 104.1667F);
        this.xrLabel22.Name = "xrLabel22";
        this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel22.SizeF = new System.Drawing.SizeF(176.2083F, 23F);
        this.xrLabel22.StylePriority.UseFont = false;
        this.xrLabel22.StylePriority.UseTextAlignment = false;
        this.xrLabel22.Text = "วันที่จดทะเบียนครั้งแรก(หัวเทรลเลอร์)";
        this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel24
        // 
        this.xrLabel24.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(519.7917F, 138.5417F);
        this.xrLabel24.Name = "xrLabel24";
        this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel24.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel24.StylePriority.UseFont = false;
        this.xrLabel24.StylePriority.UseTextAlignment = false;
        this.xrLabel24.Text = "ชนิดวัสดุที่ใช้ทำถังรถ";
        this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel26
        // 
        this.xrLabel26.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 172.9167F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel26.SizeF = new System.Drawing.SizeF(121.875F, 23F);
        this.xrLabel26.StylePriority.UseFont = false;
        this.xrLabel26.StylePriority.UseTextAlignment = false;
        this.xrLabel26.Text = "บริษัทผู้ผลิตถังเทรลเลอร์";
        this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel25
        // 
        this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel25.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 138.5417F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(151.2083F, 23F);
        this.xrLabel25.StylePriority.UseBorders = false;
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UseTextAlignment = false;
        this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel32
        // 
        this.xrLabel32.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(0F, 206.25F);
        this.xrLabel32.Name = "xrLabel32";
        this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel32.SizeF = new System.Drawing.SizeF(108.3333F, 23F);
        this.xrLabel32.StylePriority.UseFont = false;
        this.xrLabel32.StylePriority.UseTextAlignment = false;
        this.xrLabel32.Text = "ระบบการสั่นสะเทือน";
        this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel39
        // 
        this.xrLabel39.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(515.625F, 206.25F);
        this.xrLabel39.Name = "xrLabel39";
        this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel39.SizeF = new System.Drawing.SizeF(23.95844F, 23F);
        this.xrLabel39.StylePriority.UseFont = false;
        this.xrLabel39.StylePriority.UseTextAlignment = false;
        this.xrLabel39.Text = "มม.";
        this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel38
        // 
        this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel38.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(445.8332F, 206.25F);
        this.xrLabel38.Name = "xrLabel38";
        this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel38.SizeF = new System.Drawing.SizeF(69.79178F, 23F);
        this.xrLabel38.StylePriority.UseBorders = false;
        this.xrLabel38.StylePriority.UseFont = false;
        this.xrLabel38.StylePriority.UseTextAlignment = false;
        this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel40
        // 
        this.xrLabel40.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(550F, 206.25F);
        this.xrLabel40.Name = "xrLabel40";
        this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel40.SizeF = new System.Drawing.SizeF(57.29156F, 23F);
        this.xrLabel40.StylePriority.UseFont = false;
        this.xrLabel40.StylePriority.UseTextAlignment = false;
        this.xrLabel40.Text = "ที่ท้ายถัง";
        this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel42
        // 
        this.xrLabel42.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(669.7914F, 206.25F);
        this.xrLabel42.Name = "xrLabel42";
        this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel42.SizeF = new System.Drawing.SizeF(23.95844F, 23F);
        this.xrLabel42.StylePriority.UseFont = false;
        this.xrLabel42.StylePriority.UseTextAlignment = false;
        this.xrLabel42.Text = "มม.";
        this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel41
        // 
        this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel41.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(607.2916F, 206.25F);
        this.xrLabel41.Name = "xrLabel41";
        this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel41.SizeF = new System.Drawing.SizeF(62.49982F, 23F);
        this.xrLabel41.StylePriority.UseBorders = false;
        this.xrLabel41.StylePriority.UseFont = false;
        this.xrLabel41.StylePriority.UseTextAlignment = false;
        this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel34
        // 
        this.xrLabel34.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(200F, 206.25F);
        this.xrLabel34.Name = "xrLabel34";
        this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel34.SizeF = new System.Drawing.SizeF(92.70836F, 23F);
        this.xrLabel34.StylePriority.UseFont = false;
        this.xrLabel34.StylePriority.UseTextAlignment = false;
        this.xrLabel34.Text = "ความสูงจากพื้นถึง";
        this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel33
        // 
        this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel33.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 206.25F);
        this.xrLabel33.Name = "xrLabel33";
        this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel33.SizeF = new System.Drawing.SizeF(60.41666F, 23F);
        this.xrLabel33.StylePriority.UseBorders = false;
        this.xrLabel33.StylePriority.UseFont = false;
        this.xrLabel33.StylePriority.UseTextAlignment = false;
        this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel35
        // 
        this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel35.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(292.7084F, 206.25F);
        this.xrLabel35.Name = "xrLabel35";
        this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel35.SizeF = new System.Drawing.SizeF(61.45834F, 23F);
        this.xrLabel35.StylePriority.UseBorders = false;
        this.xrLabel35.StylePriority.UseFont = false;
        this.xrLabel35.StylePriority.UseTextAlignment = false;
        this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel37
        // 
        this.xrLabel37.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(400F, 206.25F);
        this.xrLabel37.Name = "xrLabel37";
        this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel37.SizeF = new System.Drawing.SizeF(45.83328F, 23F);
        this.xrLabel37.StylePriority.UseFont = false;
        this.xrLabel37.StylePriority.UseTextAlignment = false;
        this.xrLabel37.Text = "ที่หัวถัง";
        this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel36
        // 
        this.xrLabel36.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(354.1667F, 206.25F);
        this.xrLabel36.Name = "xrLabel36";
        this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel36.SizeF = new System.Drawing.SizeF(20.83337F, 23F);
        this.xrLabel36.StylePriority.UseFont = false;
        this.xrLabel36.StylePriority.UseTextAlignment = false;
        this.xrLabel36.Text = "ถัง";
        this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel7
        // 
        this.xrLabel7.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(325F, 70.91669F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(75F, 23F);
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.Text = "เลขเครื่องยนต์";
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel6
        // 
        this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel6.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 138.5417F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(112.5F, 23F);
        this.xrLabel6.StylePriority.UseBorders = false;
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel8
        // 
        this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel8.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(400F, 70.91669F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(115.625F, 23F);
        this.xrLabel8.StylePriority.UseBorders = false;
        this.xrLabel8.StylePriority.UseFont = false;
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel10
        // 
        this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel10.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(354.1667F, 138.5417F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel10.SizeF = new System.Drawing.SizeF(165.625F, 23F);
        this.xrLabel10.StylePriority.UseBorders = false;
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UseTextAlignment = false;
        this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel9
        // 
        this.xrLabel9.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(231.25F, 138.5417F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel9.SizeF = new System.Drawing.SizeF(122.9168F, 23F);
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "เลขแชชซีย์หางเทรลเลอร์";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel2
        // 
        this.xrLabel2.Font = new System.Drawing.Font("TH Sarabun New", 12F, System.Drawing.FontStyle.Underline);
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 37.5F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(134.375F, 23F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.Text = "รายละเอียดเกี่ยวกับรถ";
        // 
        // xrLabel1
        // 
        this.xrLabel1.Font = new System.Drawing.Font("TH Sarabun New", 12.75F, System.Drawing.FontStyle.Underline);
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(771F, 27.16667F);
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.StylePriority.UseTextAlignment = false;
        this.xrLabel1.Text = "ใบตรวจสภาพรถของ มว.";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 70.91669F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(37.5F, 23F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "รหัสรถ";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel5
        // 
        this.xrLabel5.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 138.5417F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(108.3333F, 23F);
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UseTextAlignment = false;
        this.xrLabel5.Text = "หมายเลขหางทะเบียน";
        this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel4
        // 
        this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel4.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(37.5F, 70.91669F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(70.83334F, 23F);
        this.xrLabel4.StylePriority.UseBorders = false;
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel11
        // 
        this.xrLabel11.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 104.1667F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(50.16667F, 23F);
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "ประเภท";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel18
        // 
        this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel18.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(625.0001F, 70.91669F);
        this.xrLabel18.Name = "xrLabel18";
        this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel18.SizeF = new System.Drawing.SizeF(145.9999F, 23F);
        this.xrLabel18.StylePriority.UseBorders = false;
        this.xrLabel18.StylePriority.UseFont = false;
        this.xrLabel18.StylePriority.UseTextAlignment = false;
        this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel17
        // 
        this.xrLabel17.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(515.625F, 70.91669F);
        this.xrLabel17.Name = "xrLabel17";
        this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel17.SizeF = new System.Drawing.SizeF(109.375F, 23F);
        this.xrLabel17.StylePriority.UseFont = false;
        this.xrLabel17.StylePriority.UseTextAlignment = false;
        this.xrLabel17.Text = "เลขแชชซีย์หัวเทรลเลอร์";
        this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel19
        // 
        this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel19.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(337.5F, 104.1667F);
        this.xrLabel19.Name = "xrLabel19";
        this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel19.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel19.StylePriority.UseBorders = false;
        this.xrLabel19.StylePriority.UseFont = false;
        this.xrLabel19.StylePriority.UseTextAlignment = false;
        this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel21
        // 
        this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel21.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(220.8333F, 70.91669F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel21.StylePriority.UseBorders = false;
        this.xrLabel21.StylePriority.UseFont = false;
        this.xrLabel21.StylePriority.UseTextAlignment = false;
        this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel20
        // 
        this.xrLabel20.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 70.91669F);
        this.xrLabel20.Name = "xrLabel20";
        this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel20.SizeF = new System.Drawing.SizeF(108.3333F, 23F);
        this.xrLabel20.StylePriority.UseFont = false;
        this.xrLabel20.StylePriority.UseTextAlignment = false;
        this.xrLabel20.Text = "ทะเบียนหัวเทรลเลอร์";
        this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel13
        // 
        this.xrLabel13.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(162.5F, 104.1667F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(35.41667F, 23F);
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "ชนิด";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel12
        // 
        this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel12.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(50.16667F, 104.1667F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(91.66666F, 23F);
        this.xrLabel12.StylePriority.UseBorders = false;
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel14
        // 
        this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel14.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(197.9167F, 104.1667F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(42.70836F, 23F);
        this.xrLabel14.StylePriority.UseBorders = false;
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel16
        // 
        this.xrLabel16.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(295.8334F, 104.1667F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(41.6666F, 23F);
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.StylePriority.UseTextAlignment = false;
        this.xrLabel16.Text = "ยี่ห้อ";
        this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel15
        // 
        this.xrLabel15.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(240.625F, 104.1667F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(28.125F, 23F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "ล้อ";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // PageFooter
        // 
        this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel86,
            this.xrLabel85});
        this.PageFooter.HeightF = 23F;
        this.PageFooter.Name = "PageFooter";
        // 
        // xrLabel86
        // 
        this.xrLabel86.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(671F, 0F);
        this.xrLabel86.Name = "xrLabel86";
        this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel86.SizeF = new System.Drawing.SizeF(100F, 23F);
        this.xrLabel86.StylePriority.UseFont = false;
        this.xrLabel86.StylePriority.UseTextAlignment = false;
        this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel85
        // 
        this.xrLabel85.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrLabel85.Name = "xrLabel85";
        this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel85.SizeF = new System.Drawing.SizeF(400F, 23F);
        this.xrLabel85.StylePriority.UseFont = false;
        this.xrLabel85.StylePriority.UseTextAlignment = false;
        this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrTable3
        // 
        this.xrTable3.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(100F, 0F);
        this.xrTable3.Name = "xrTable3";
        this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable3.SizeF = new System.Drawing.SizeF(337.5F, 25F);
        this.xrTable3.StylePriority.UseFont = false;
        this.xrTable3.StylePriority.UseTextAlignment = false;
        this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8});
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.Weight = 1D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.StylePriority.UseBorders = false;
        this.xrTableCell7.Text = "รวม";
        this.xrTableCell7.Weight = 1D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.StylePriority.UseBorders = false;
        this.xrTableCell8.Weight = 2.375D;
        // 
        // xrLabel65
        // 
        this.xrLabel65.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(416.8333F, 31.25F);
        this.xrLabel65.Name = "xrLabel65";
        this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel65.SizeF = new System.Drawing.SizeF(93.75003F, 23F);
        this.xrLabel65.StylePriority.UseFont = false;
        this.xrLabel65.StylePriority.UseTextAlignment = false;
        this.xrLabel65.Text = "ผู้ควบคุมปฏิบัติงาน";
        this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel66
        // 
        this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel66.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(510.5833F, 31.25F);
        this.xrLabel66.Name = "xrLabel66";
        this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel66.SizeF = new System.Drawing.SizeF(260.4167F, 23F);
        this.xrLabel66.StylePriority.UseBorders = false;
        this.xrLabel66.StylePriority.UseFont = false;
        this.xrLabel66.StylePriority.UseTextAlignment = false;
        this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel67
        // 
        this.xrLabel67.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(510.5833F, 64.49998F);
        this.xrLabel67.Name = "xrLabel67";
        this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel67.SizeF = new System.Drawing.SizeF(12.5F, 23F);
        this.xrLabel67.StylePriority.UseFont = false;
        this.xrLabel67.StylePriority.UseTextAlignment = false;
        this.xrLabel67.Text = "(";
        this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel68
        // 
        this.xrLabel68.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(523.0833F, 64.49998F);
        this.xrLabel68.Name = "xrLabel68";
        this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel68.SizeF = new System.Drawing.SizeF(236.4584F, 23F);
        this.xrLabel68.StylePriority.UseFont = false;
        this.xrLabel68.StylePriority.UseTextAlignment = false;
        this.xrLabel68.Text = "นายสิริชัย แก้วไพจิตร";
        this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel69
        // 
        this.xrLabel69.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(759.5416F, 64.49998F);
        this.xrLabel69.Name = "xrLabel69";
        this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel69.SizeF = new System.Drawing.SizeF(11.45837F, 23F);
        this.xrLabel69.StylePriority.UseFont = false;
        this.xrLabel69.StylePriority.UseTextAlignment = false;
        this.xrLabel69.Text = ")";
        this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel70
        // 
        this.xrLabel70.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(463.7082F, 97.91666F);
        this.xrLabel70.Name = "xrLabel70";
        this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel70.SizeF = new System.Drawing.SizeF(46.87512F, 22.99999F);
        this.xrLabel70.StylePriority.UseFont = false;
        this.xrLabel70.StylePriority.UseTextAlignment = false;
        this.xrLabel70.Text = "ตำแหน่ง";
        this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel71
        // 
        this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel71.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(510.5833F, 97.91666F);
        this.xrLabel71.Name = "xrLabel71";
        this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel71.SizeF = new System.Drawing.SizeF(260.4167F, 23F);
        this.xrLabel71.StylePriority.UseBorders = false;
        this.xrLabel71.StylePriority.UseFont = false;
        this.xrLabel71.StylePriority.UseTextAlignment = false;
        this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel72
        // 
        this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.xrLabel72.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(510.5833F, 97.91666F);
        this.xrLabel72.Name = "xrLabel72";
        this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel72.SizeF = new System.Drawing.SizeF(260.4167F, 23F);
        this.xrLabel72.StylePriority.UseBorders = false;
        this.xrLabel72.StylePriority.UseFont = false;
        this.xrLabel72.StylePriority.UseTextAlignment = false;
        this.xrLabel72.Text = "ผู้ควบคุมปฏิบัติงาน";
        this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel73
        // 
        this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel73.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(523.0833F, 64.49998F);
        this.xrLabel73.Name = "xrLabel73";
        this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel73.SizeF = new System.Drawing.SizeF(236.4583F, 23F);
        this.xrLabel73.StylePriority.UseBorders = false;
        this.xrLabel73.StylePriority.UseFont = false;
        this.xrLabel73.StylePriority.UseTextAlignment = false;
        this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel74
        // 
        this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel74.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(570.9998F, 135.375F);
        this.xrLabel74.Name = "xrLabel74";
        this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel74.SizeF = new System.Drawing.SizeF(36.29181F, 23F);
        this.xrLabel74.StylePriority.UseBorders = false;
        this.xrLabel74.StylePriority.UseFont = false;
        this.xrLabel74.StylePriority.UseTextAlignment = false;
        this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel75
        // 
        this.xrLabel75.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(607.2916F, 135.375F);
        this.xrLabel75.Name = "xrLabel75";
        this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel75.SizeF = new System.Drawing.SizeF(15.62506F, 23F);
        this.xrLabel75.StylePriority.UseFont = false;
        this.xrLabel75.StylePriority.UseTextAlignment = false;
        this.xrLabel75.Text = "/";
        this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel76
        // 
        this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel76.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(622.9167F, 135.375F);
        this.xrLabel76.Name = "xrLabel76";
        this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel76.SizeF = new System.Drawing.SizeF(54.16663F, 23F);
        this.xrLabel76.StylePriority.UseBorders = false;
        this.xrLabel76.StylePriority.UseFont = false;
        this.xrLabel76.StylePriority.UseTextAlignment = false;
        this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel77
        // 
        this.xrLabel77.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(676.2081F, 135.375F);
        this.xrLabel77.Name = "xrLabel77";
        this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel77.SizeF = new System.Drawing.SizeF(15.62506F, 23F);
        this.xrLabel77.StylePriority.UseFont = false;
        this.xrLabel77.StylePriority.UseTextAlignment = false;
        this.xrLabel77.Text = "/";
        this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel78
        // 
        this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
        this.xrLabel78.Font = new System.Drawing.Font("TH Sarabun New", 12F);
        this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(691.8332F, 135.375F);
        this.xrLabel78.Name = "xrLabel78";
        this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel78.SizeF = new System.Drawing.SizeF(48.79181F, 23F);
        this.xrLabel78.StylePriority.UseBorders = false;
        this.xrLabel78.StylePriority.UseFont = false;
        this.xrLabel78.StylePriority.UseTextAlignment = false;
        this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // ds_WaterSheet1
        // 
        this.ds_WaterSheet1.DataSetName = "ds_WaterSheet";
        this.ds_WaterSheet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // ReportFooter
        // 
        this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrLabel65,
            this.xrLabel66,
            this.xrLabel67,
            this.xrLabel71,
            this.xrLabel72,
            this.xrLabel73,
            this.xrLabel68,
            this.xrLabel69,
            this.xrLabel70,
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77,
            this.xrLabel78});
        this.ReportFooter.HeightF = 158.375F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // rpt_WorkSheet
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter});
        this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
        this.Margins = new System.Drawing.Printing.Margins(28, 28, 46, 29);
        this.PageHeight = 1169;
        this.PageWidth = 827;
        this.PaperKind = System.Drawing.Printing.PaperKind.A4;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.ds_WaterSheet1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion
}
