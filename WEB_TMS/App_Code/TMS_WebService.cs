﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using System.Xml;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for TMS_WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class TMS_WebService : System.Web.Services.WebService
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    public TMS_WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public class STATUS
    {
        public string CSTATUS;
        public string REMARK;
        public string ID;
    }

    [WebMethod]
    public STATUS DriverRegister_SYNC_IN(string DEPOT, string VEHICLE_HEAD, string VEHICLE_TAIL, string ITEM_NO, string SPERSONALNO, string SEMPLOYEENAME, string CREATEDATE, string SVENDORID, string DRIVER_NO)
    {
        string sError = "";
        STATUS sStatus = new STATUS();



        try
        {

            string checkFIFO = "";
            string checkEmployee = "";
            string _STERMINAL = CommonFunction.ReplaceInjection(DEPOT);
            string _SHEADREGISTERNO = CommonFunction.ReplaceInjection(VEHICLE_HEAD);
            string _STRAILERREGISTERNO = CommonFunction.ReplaceInjection(VEHICLE_TAIL);
            string _SPERSONALNO = CommonFunction.ReplaceInjection(SPERSONALNO);
            string _SEMPLOYEENAME = CommonFunction.ReplaceInjection(SEMPLOYEENAME);
            string _DDATE = CommonFunction.ReplaceInjection(CREATEDATE);
            string _SVENDORID = CommonFunction.ReplaceInjection(SVENDORID);
            string _SEMPLOYEEID = CommonFunction.ReplaceInjection(DRIVER_NO);
            string _ITEM_NO = CommonFunction.ReplaceInjection(ITEM_NO);

            if (_STERMINAL == "" || _SHEADREGISTERNO == "" || _SPERSONALNO == "" || _SEMPLOYEEID == "" || _SVENDORID == "")
            {
                sStatus.CSTATUS = "E";
                sStatus.ID = "";
                sStatus.REMARK = "Please fill Parameter";
                return sStatus;
            }


            if (_SHEADREGISTERNO == _STRAILERREGISTERNO)
            {
                _STRAILERREGISTERNO = "";
            }


            DateTime date;
            DateTime _FIFODATE = DateTime.TryParse(_DDATE, out date) ? date : DateTime.Now;
            if (_FIFODATE.Year < 1500)
            {
                _FIFODATE = _FIFODATE.AddYears(543);
            }

            checkFIFO = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%' || '" + _SHEADREGISTERNO + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND (CPLAN IS NULL OR CPLAN = '0')";
            checkEmployee = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SEMPLOYEEID LIKE '%' || '" + _SEMPLOYEEID + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND (CPLAN IS NULL OR CPLAN = '0')";


            if (CommonFunction.Count_Value(sql, checkFIFO) > 0)
            {
                sStatus.CSTATUS = "E";
                sStatus.ID = "";
                sStatus.REMARK = "รถทะเบียน " + _SHEADREGISTERNO + " ได้ทำการลงคิวแล้วและยังไม่ได้ถูกนำไปจัดแผน !";
                return sStatus;
            }

            if (CommonFunction.Count_Value(sql, checkEmployee) > 0)
            {
                sStatus.CSTATUS = "E";
                sStatus.ID = "";
                sStatus.REMARK = "รหัสพนักงาน " + _SEMPLOYEEID + " ได้ทำการลงคิวแล้ว !";
                return sStatus;
            }

            string genID;

            using (OracleConnection con = new OracleConnection(sql))
            {
                con.Open();


                genID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TFIFO ORDER BY NID DESC) WHERE ROWNUM <= 1");
                string genNO = "0";

                genNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STERMINAL = '" + _STERMINAL + "'  ORDER BY NNO DESC) WHERE ROWNUM <= 1");

                string strsql = "INSERT INTO TFIFO(NNO, STERMINAL, SHEADREGISTERNO, STRAILERREGISTERNO, SPERSONALNO, SEMPLOYEENAME, STEL, DDATE, SCREATE, DCREATE, SVENDORID,SEMPLOYEEID,ITEM_NO) VALUES (:NNO, :STERMINAL, :SHEADREGISTERNO, :STRAILERREGISTERNO, :SPERSONALNO, :SEMPLOYEENAME, :STEL, :DDATE, :SCREATE, SYSDATE, :SVENDORID,:SEMPLOYEEID,:ITEM_NO)";
                int nResult = 0;
                using (OracleCommand com = new OracleCommand(strsql, con))
                {


                    com.Parameters.Clear();
                    //com.Parameters.Add(":NID", OracleType.Number).Value = genID;
                    com.Parameters.Add(":NNO", OracleType.Number).Value = genNO;
                    com.Parameters.Add(":STERMINAL", OracleType.VarChar).Value = _STERMINAL;
                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = _SHEADREGISTERNO;
                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = _STRAILERREGISTERNO;
                    com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = _SPERSONALNO;
                    com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = _SEMPLOYEENAME;
                    com.Parameters.Add(":STEL", OracleType.VarChar).Value = "";
                    com.Parameters.Add(":DDATE", OracleType.DateTime).Value = _FIFODATE;
                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "sysadmin";
                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = _SVENDORID;
                    com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = _SEMPLOYEEID;
                    com.Parameters.Add(":ITEM_NO", OracleType.VarChar).Value = _ITEM_NO;
                    nResult = com.ExecuteNonQuery();

                }

                if (nResult > 0)
                {
                    sStatus.CSTATUS = "S";
                    sStatus.ID = genID;
                    sStatus.REMARK = "";
                }
                else
                {
                    sStatus.CSTATUS = "E";
                    sStatus.ID = "";
                    sStatus.REMARK = "No Row Insert";
                }
            }


        }
        catch (Exception ex)
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = ex.Message;
            //return sStatus;
        }
        finally
        {
            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();
                string IP_Data = @"INSERT INTO ITERMINAL_QBOOKING (EVENT_DATE, DEPOT, VEHICLE_HEAD, VEHICLE_TAIL, ITEM_NO, SPERSONALNO, SEMPLOYEENAME, CREATEDATE, SVENDORID, DRIVER_NO,RESULT_FLAG,RESULT_MSG) 
VALUES ( SYSDATE, :DEPOT, :VEHICLE_HEAD, :VEHICLE_TAIL, :ITEM_NO, :SPERSONALNO, :SEMPLOYEENAME, :CREATEDATE, :SVENDORID, :DRIVER_NO, :CSTATUS, :REMARK)";
                using (OracleCommand com = new OracleCommand(IP_Data, con))
                {
                    com.Parameters.Add(":DEPOT", OracleType.VarChar).Value = DEPOT;
                    com.Parameters.Add(":VEHICLE_HEAD", OracleType.VarChar).Value = VEHICLE_HEAD;
                    com.Parameters.Add(":VEHICLE_TAIL", OracleType.VarChar).Value = VEHICLE_TAIL;
                    com.Parameters.Add(":ITEM_NO", OracleType.Number).Value = ITEM_NO;
                    com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = SPERSONALNO;
                    com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = SEMPLOYEENAME;
                    com.Parameters.Add(":CREATEDATE", OracleType.DateTime).Value = DateTime.Now;
                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = SVENDORID;
                    com.Parameters.Add(":DRIVER_NO", OracleType.VarChar).Value = DRIVER_NO;
                    com.Parameters.Add(":CSTATUS", OracleType.VarChar).Value = sStatus.CSTATUS;
                    com.Parameters.Add(":REMARK", OracleType.VarChar).Value = sStatus.REMARK;
                    com.ExecuteNonQuery();
                }
            }
        }



        return sStatus;
    }

    [WebMethod]
    public STATUS PlanDO_SYNC_IN(string DELIV_DATE, string TIMEWINDOW, string SUPPLY_PLNT, string VEHICLE_HEAD, string VEHICLE_TAIL, string[] Drop_No, string[] Delivery, string[] SHIP_TO)
    {


        string sError = "";
        STATUS sStatus = new STATUS();

        string _DELIV_DATE = CommonFunction.ReplaceInjection(DELIV_DATE);
        string _TIMEWINDOW = CommonFunction.ReplaceInjection(TIMEWINDOW);
        string _SUPPLY_PLNT = CommonFunction.ReplaceInjection(SUPPLY_PLNT);
        string _VEHICLE_HEAD = CommonFunction.ReplaceInjection(VEHICLE_HEAD);
        string _VEHICLE_TAIL = CommonFunction.ReplaceInjection(VEHICLE_TAIL);
        string[] _Drop_No = Drop_No;
        string[] _Delivery = Delivery;
        string[] _SHIP_TO = SHIP_TO;

        for (int i = 0; i < Drop_No.Length; i++)
        {
            _Drop_No[i] = CommonFunction.ReplaceInjection(Drop_No[i]);
        }

        for (int i = 0; i < Delivery.Length; i++)
        {
            _Delivery[i] = CommonFunction.ReplaceInjection(Delivery[i]);
        }

        for (int i = 0; i < SHIP_TO.Length; i++)
        {
            _SHIP_TO[i] = CommonFunction.ReplaceInjection(SHIP_TO[i]);
        }


        if (_DELIV_DATE == "" || _TIMEWINDOW == "" || _SUPPLY_PLNT == "" || _VEHICLE_HEAD == "" || (_Drop_No.Length == 0) || (_Delivery.Length == 0) || (_SHIP_TO.Length == 0))
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = "Please fill Parameter";
            return sStatus;
        }

        if (!(_Drop_No.Length == _Delivery.Length && _Delivery.Length == _SHIP_TO.Length))
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = "Please fill Parameter";
            return sStatus;
        }



        if (_VEHICLE_HEAD == _VEHICLE_TAIL)
        {
            _VEHICLE_TAIL = "";
        }


        DateTime date;
        DateTime DDELIVERY = DateTime.TryParse(_DELIV_DATE, out date) ? date : DateTime.Now;
        if (DDELIVERY.Year < 1500)
        {
            DDELIVERY = DDELIVERY.AddYears(543);
        }

        try
        {

            StringBuilder DetailAlertList = new StringBuilder();

            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                string NPLANPLUS = "";


                DateTime datee;

                string tempEmployee = "";

                #region  for excel rows


                #region ถ้าเช็คFormat วันที่ขนส่ง ผ่าน
                DateTime datetime;

                string tmpTerminal = _SUPPLY_PLNT;
                #region ///รถคันดังกล่าวถูกยืนยัน มา ณ คลังที่ระบุหรือไม่
                string sqlCheckTruck = @"SELECT f.SHEADREGISTERNO,f.SEMPLOYEEID
                FROM  TFIFO f
                WHERE  NVL(f.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE '%" + VEHICLE_HEAD + "%' AND TO_CHAR(f.DDATE,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND f.STERMINAL LIKE '" + tmpTerminal + "'  GROUP BY  f.SHEADREGISTERNO";



                //                                    string sqlCheckTruck = @"SELECT Tc.SHEADREGISTERNO
                //                FROM  (TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
                //                LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  
                //                WHERE  TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TF.DDATE = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') -1  /* AND f.STERMINAL LIKE AND nvl(ct.STERMINALID,'" + tmpTerminal + "') LIKE '" + tmpTerminal + "' */  GROUP BY  Tc.SHEADREGISTERNO";
                DataTable numTruckConfirm = CommonFunction.Get_Data(con, sqlCheckTruck);
                if (numTruckConfirm.Rows.Count < 1)
                {

                    DetailAlertList.Append(",ยังไม่ได้ถูกจัดคิว");

                }
                else
                {
                    tempEmployee = numTruckConfirm.Rows[0]["SEMPLOYEEID"] + "";
                }
                #endregion

                string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND TO_CHAR(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STIMEWINDOW = '" + _TIMEWINDOW + "' AND STERMINALID = '" + _SUPPLY_PLNT + "' AND SHEADREGISTERNO = '" + _VEHICLE_HEAD + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + _VEHICLE_TAIL + "','1')");
                //ข็อมูล
                int num1 = 0;
                int num2 = 0;




                int num = 0;
                string OutboundPlus = "";

                #region ///วนข้อมูล ตาม [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL]


                #region เช็ค เลขDO DELIVERY ว่าใส่มาหรือไม่

                #region เช็ค เลขDO DELIVERY ซ้ำใน Excel

                #region จัดแผนซ้ำใน Excel (วันที่ขนส่ง+รถ+คลัง+TimeWindow+Drop)

                string _OldDrop = "";
                foreach (string sDrop in _Drop_No)
                {
                    if (_OldDrop != sDrop)
                    {
                        _OldDrop = sDrop;
                    }
                    else
                    {
                        DetailAlertList.Append(",Drop " + _OldDrop + " ซ้ำกัน");
                    }
                }


                #endregion

                foreach (string sDelivery in _Delivery)
                {
                    OutboundPlus += ",'" + sDelivery + "'";
                }

                if (chkNPLAN == "")
                {//ถ้า วันที่ขนส่ง+ทามวินโดว+คลัง+หัว+หาง ที่ระบุมาไม่มีอยู่ในระบบ
                    #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO ไม่เคยมีจัดแผน

                    string sDropNO = "";
                    foreach (string sValue in _Drop_No)
                    {
                        sDropNO += ",'" + sValue + "'";
                    }

                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '"
                        + _VEHICLE_HEAD + "' AND P.STIMEWINDOW = '" + _TIMEWINDOW
                        + "' AND TO_CHAR(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"))
                        + "' AND p.STERMINALID ='" + _SUPPLY_PLNT + "' " + ((sDropNO.Length > 0) ? "AND pl.NDROP IN (" + sDropNO.Remove(0, 1) + ")" : ""));
                    if (CheckCar > 0)
                    {
                        DetailAlertList.Append(",Drop " + sDropNO.Remove(0, 1) + " มีอยู่ในระบบแล้ว");
                    }
                    #endregion
                }
                else
                {
                    #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO เคยมีจัดแผน

                    OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO IN ('" + OutboundPlus.Remove(0, 1) + "') AND NPLANID != '" + chkNPLAN + "'", con);
                    int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
                    if (countDeliveryno > 0)
                    {
                        DetailAlertList.Append(",DELIVERYNO " + OutboundPlus.Remove(0, 1) + " มีอยู่ในระบบแล้ว");
                    }

                    #endregion
                }



                #endregion

                #endregion

                #endregion

                #region เช็คปริมานเกินความจุรถต่อเที่ยว

                string sDO_NO = "";

                foreach (string sDelivery in _Delivery)
                {
                    sDO_NO += "," + sDelivery + "";
                }



                sDO_NO = (sDO_NO.Length > 0) ? sDO_NO.Remove(0, 1) : "";
                string DO_TOTLE_VOLUMN = CommonFunction.Get_Value(con, "SELECT  SUM(nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0)) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO in ( '" + sDO_NO.Replace(",", "','") + "' )");

                string sTruck;

                if (!string.IsNullOrEmpty("" + _VEHICLE_TAIL))
                {
                    sTruck = _VEHICLE_TAIL;
                }
                else
                {
                    sTruck = _VEHICLE_HEAD;
                }
                double def_Double;
                string STOTALCAP = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + sTruck.Trim() + "' GROUP BY  T.NTOTALCAPACITY");
                double TRCK_TOTALCAP = double.TryParse(STOTALCAP, out def_Double) ? double.Parse(STOTALCAP) + 100 : 0.00;
                double DO_VOLUMN = double.TryParse(DO_TOTLE_VOLUMN, out def_Double) ? def_Double : 0.00;
                if (TRCK_TOTALCAP < DO_VOLUMN)
                {
                    DetailAlertList.Append(",เกินปริมาณความจุของรถ ความจุของรถ : " + ((TRCK_TOTALCAP > 0) ? (TRCK_TOTALCAP - 100) : 0) + " ปริมาณที่จัดแผน :" + DO_VOLUMN);

                }

                #endregion


                #endregion

                #endregion





                if (DetailAlertList.Length > 0)
                {
                    sStatus.CSTATUS = "E";
                    sStatus.ID = "";
                    sStatus.REMARK = DetailAlertList.ToString();
                    return sStatus;
                }





                #region หาขอบเขตของข้อมูลที่ต้องการนำเข้าและบันทึกลงดาต้าเบส

                //หาขอบเขตของข้อมูลที่ต้องการนำเข้า
                string strsql = "INSERT INTO TPLANSCHEDULE(SEMPLOYEEID,CCONFIRM,SVENDORID,nPlanID,nNo,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate,CFIFO) VALUES (:SEMPLOYEEID,'1',:SVENDORID,:nPlanID,1,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate,'1')";
                string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,NVALUE) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:NVALUE)";



                DateTime Now = DateTime.Now;
                DateTime DDELIVERYCONVERT = DDELIVERY.Date.Add(new TimeSpan(Now.Hour, Now.Minute, Now.Second));


                if (chkNPLAN == "") //ถ้าไม่มีรายการเดิม
                {
                    string genid = CommonFunction.Gen_ID(con, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
                    int nResult = 0;
                    using (OracleCommand com = new OracleCommand(strsql, con))
                    {

                        string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),'.','') = REPLACE(REPLACE('" + _VEHICLE_HEAD + "','-',''),'.','') ");

                        com.Parameters.Clear();
                        com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tempEmployee;
                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
                        com.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                        com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = _SUPPLY_PLNT + "";
                        com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
                        com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DDELIVERYCONVERT;
                        com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = "";
                        com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = DDELIVERYCONVERT;
                        com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = int.TryParse(_TIMEWINDOW, out num1) ? num1 : 0;
                        com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = _VEHICLE_HEAD + "";
                        com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = _VEHICLE_TAIL + "";
                        com.Parameters.Add(":sCreate", OracleType.VarChar).Value = "sysadmin";
                        com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = "sysadmin";
                        nResult = com.ExecuteNonQuery();

                        if (nResult > 0)
                        {
                            sStatus.CSTATUS = "S";
                            sStatus.ID = genid;
                            sStatus.REMARK = "";
                        }
                        else
                        {
                            sStatus.CSTATUS = "E";
                            sStatus.ID = "";
                            sStatus.REMARK = "No Row Insert";
                        }

                    }

                    //update fifo
                    string strsql2222 = "UPDATE TFIFO SET CPLAN = '1' WHERE NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%" + _VEHICLE_HEAD + "%' AND TO_CHAR(DDATE,'dd/MM/yyyy') = '" + DDELIVERYCONVERT.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND STERMINAL LIKE '" + _SUPPLY_PLNT + "'";
                    using (OracleCommand com2 = new OracleCommand(strsql2222, con))
                    {
                        com2.ExecuteNonQuery();
                    }

                    int idn = 0;

                    foreach (string sDrop in _Drop_No)
                    {


                        string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                        int NDROP = int.TryParse(sDrop + "", out num2) ? num2 : 0;

                        DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') = LPAD('" + _Delivery[idn] + "',10,'0')");

                        int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {

                            com1.Parameters.Clear();
                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = _Delivery[idn];
                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = _SHIP_TO[idn];
                            com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                            com1.ExecuteNonQuery();

                        }

                        idn++;
                    }
                }
                else // มีรายการเดิม
                {
                    sStatus.CSTATUS = "S";
                    sStatus.ID = chkNPLAN;
                    sStatus.REMARK = "";

                    using (OracleCommand delcom = new OracleCommand("DELETE FROM TPLANSCHEDULELIST WHERE NPLANID = '" + chkNPLAN + "'", con))
                    {
                        delcom.ExecuteNonQuery();
                    }

                    int idn = 0;

                    foreach (string sDrop in _Drop_No)
                    {


                        string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
                        int NDROP = int.TryParse(sDrop + "", out num2) ? num2 : 0;

                        DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') = LPAD('" + _Delivery[idn] + "',10,'0')");

                        int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {

                            com1.Parameters.Clear();
                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = chkNPLAN;
                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = _Delivery[idn];
                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = _SHIP_TO[idn];
                            com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
                            com1.ExecuteNonQuery();

                        }

                        idn++;
                    }

                }

                #endregion

            }
        }

        catch (Exception ex)
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = ex.Message;
            return sStatus;
        }

        return sStatus;
    }

    [WebMethod]
    public STATUS CancelPlanDO_SYNC_IN(string PlanID, string Delivery)
    {


        string sError = "";
        STATUS sStatus = new STATUS();

        string _PlanID = CommonFunction.ReplaceInjection(PlanID);
        string _Delivery = CommonFunction.ReplaceInjection(Delivery);



        if (_PlanID == "" || _Delivery == "")
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = "Please fill Parameter";
            return sStatus;
        }


        try
        {


            using (OracleConnection con = new OracleConnection(sql))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                string strsql1 = "UPDATE TPlanSchedulelist SET CACTIVE = '0' WHERE NPLANID = :NPLANID AND SDELIVERYNO = :SDELIVERYNO";
                int nResult = 0;
                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                {
                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = _PlanID;
                    com1.Parameters.Add(":SDELIVERYNO", OracleType.NVarChar).Value = _Delivery;
                    nResult = com1.ExecuteNonQuery();

                    if (nResult > 0)
                    {
                        sStatus.CSTATUS = "S";
                        sStatus.ID = _PlanID;
                        sStatus.REMARK = "";
                    }
                    else
                    {
                        sStatus.CSTATUS = "E";
                        sStatus.ID = "";
                        sStatus.REMARK = "No Row Insert";
                    }
                }

                string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND NPLANID = " + _PlanID);

                if (CountValue == "0")
                {

                    string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
                    using (OracleCommand com = new OracleCommand(strsql, con))
                    {
                        com.Parameters.Add(":NPLANID", OracleType.Number).Value = _PlanID;
                        com.ExecuteNonQuery();
                    }
                }

            }
        }

        catch (Exception ex)
        {
            sStatus.CSTATUS = "E";
            sStatus.ID = "";
            sStatus.REMARK = ex.Message;
            return sStatus;
        }

        return sStatus;
    }
}

//#region + BackUp +
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Services;
//using System.Data.OracleClient;
//using System.Web.Configuration;
//using System.Globalization;
//using System.Xml;
//using System.Data;
//using System.Text;

///// <summary>
///// Summary description for TMS_WebService
///// </summary>
//[WebService(Namespace = "http://tempuri.org/")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
//// [System.Web.Script.Services.ScriptService]
//public class TMS_WebService : System.Web.Services.WebService
//{
//    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
//    public TMS_WebService()
//    {

//        //Uncomment the following line if using designed components 
//        //InitializeComponent(); 
//    }

//    public class STATUS
//    {
//        public string CSTATUS;
//        public string REMARK;
//        public string ID;
//    }

//    [WebMethod]
//    public STATUS DriverRegister_SYNC_IN(string DEPOT, string VEHICLE_HEAD, string VEHICLE_TAIL, string ITEM_NO, string SPERSONALNO, string SEMPLOYEENAME, string CREATEDATE, string SVENDORID, string DRIVER_NO)
//    {
//        string sError = "";
//        STATUS sStatus = new STATUS();



//        try
//        {

//            string checkFIFO = "";
//            string checkEmployee = "";
//            string _STERMINAL = CommonFunction.ReplaceInjection(DEPOT);
//            string _SHEADREGISTERNO = CommonFunction.ReplaceInjection(VEHICLE_HEAD);
//            string _STRAILERREGISTERNO = CommonFunction.ReplaceInjection(VEHICLE_TAIL);
//            string _SPERSONALNO = CommonFunction.ReplaceInjection(SPERSONALNO);
//            string _SEMPLOYEENAME = CommonFunction.ReplaceInjection(SEMPLOYEENAME);
//            string _DDATE = CommonFunction.ReplaceInjection(CREATEDATE);
//            string _SVENDORID = CommonFunction.ReplaceInjection(SVENDORID);
//            string _SEMPLOYEEID = CommonFunction.ReplaceInjection(DRIVER_NO);
//            string _ITEM_NO = CommonFunction.ReplaceInjection(ITEM_NO);

//            if (_STERMINAL == "" || _SHEADREGISTERNO == "" || _SPERSONALNO == "" || _SEMPLOYEEID == "" || _SVENDORID == "")
//            {
//                sStatus.CSTATUS = "E";
//                sStatus.ID = "";
//                sStatus.REMARK = "Please fill Parameter";
//                return sStatus;
//            }


//            if (_SHEADREGISTERNO == _STRAILERREGISTERNO)
//            {
//                _STRAILERREGISTERNO = "";
//            }


//            DateTime date;
//            DateTime _FIFODATE = DateTime.TryParse(_DDATE, out date) ? date : DateTime.Now;
//            if (_FIFODATE.Year < 1500)
//            {
//                _FIFODATE = _FIFODATE.AddYears(543);
//            }

//            checkFIFO = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%' || '" + _SHEADREGISTERNO + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND (CPLAN IS NULL OR CPLAN = '0')";
//            checkEmployee = "Select * from TFIFO Where NVL(CACTIVE,'1') = '1' AND SEMPLOYEEID LIKE '%' || '" + _SEMPLOYEEID + "' || '%'  AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND (CPLAN IS NULL OR CPLAN = '0')";


//            if (CommonFunction.Count_Value(sql, checkFIFO) > 0)
//            {
//                sStatus.CSTATUS = "E";
//                sStatus.ID = "";
//                sStatus.REMARK = "รถทะเบียน " + _SHEADREGISTERNO + " ได้ทำการลงคิวแล้วและยังไม่ได้ถูกนำไปจัดแผน !";
//                return sStatus;
//            }

//            if (CommonFunction.Count_Value(sql, checkEmployee) > 0)
//            {
//                sStatus.CSTATUS = "E";
//                sStatus.ID = "";
//                sStatus.REMARK = "รหัสพนักงาน " + _SEMPLOYEEID + " ได้ทำการลงคิวแล้ว !";
//                return sStatus;
//            }

//            string genID;

//            using (OracleConnection con = new OracleConnection(sql))
//            {
//                con.Open();


//                genID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TFIFO ORDER BY NID DESC) WHERE ROWNUM <= 1");
//                string genNO = "0";

//                genNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TFIFO WHERE NVL(CACTIVE,'1') = '1' AND to_char(DDATE,'dd/MM/yyyy') = '" + _FIFODATE.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STERMINAL = '" + _STERMINAL + "'  ORDER BY NNO DESC) WHERE ROWNUM <= 1");

//                string strsql = "INSERT INTO TFIFO(NID, NNO, STERMINAL, SHEADREGISTERNO, STRAILERREGISTERNO, SPERSONALNO, SEMPLOYEENAME, STEL, DDATE, SCREATE, DCREATE, SVENDORID,SEMPLOYEEID,ITEM_NO) VALUES (:NID, :NNO, :STERMINAL, :SHEADREGISTERNO, :STRAILERREGISTERNO, :SPERSONALNO, :SEMPLOYEENAME, :STEL, :DDATE, :SCREATE, SYSDATE, :SVENDORID,:SEMPLOYEEID,:ITEM_NO)";
//                int nResult = 0;
//                using (OracleCommand com = new OracleCommand(strsql, con))
//                {


//                    com.Parameters.Clear();
//                    com.Parameters.Add(":NID", OracleType.Number).Value = genID;
//                    com.Parameters.Add(":NNO", OracleType.Number).Value = genNO;
//                    com.Parameters.Add(":STERMINAL", OracleType.VarChar).Value = _STERMINAL;
//                    com.Parameters.Add(":SHEADREGISTERNO", OracleType.VarChar).Value = _SHEADREGISTERNO;
//                    com.Parameters.Add(":STRAILERREGISTERNO", OracleType.VarChar).Value = _STRAILERREGISTERNO;
//                    com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = _SPERSONALNO;
//                    com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = _SEMPLOYEENAME;
//                    com.Parameters.Add(":STEL", OracleType.VarChar).Value = "";
//                    com.Parameters.Add(":DDATE", OracleType.DateTime).Value = _FIFODATE;
//                    com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = "sysadmin";
//                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = _SVENDORID;
//                    com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = _SEMPLOYEEID;
//                    com.Parameters.Add(":ITEM_NO", OracleType.VarChar).Value = _ITEM_NO;
//                    nResult = com.ExecuteNonQuery();

//                }

//                if (nResult > 0)
//                {
//                    sStatus.CSTATUS = "S";
//                    sStatus.ID = genID;
//                    sStatus.REMARK = "";
//                }
//                else
//                {
//                    sStatus.CSTATUS = "E";
//                    sStatus.ID = "";
//                    sStatus.REMARK = "No Row Insert";
//                }
//            }


//        }
//        catch (Exception ex)
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = ex.Message;
//            //return sStatus;
//        }
//        finally
//        {
//            using (OracleConnection con = new OracleConnection(sql))
//            {
//                if (con.State == ConnectionState.Closed) con.Open();
//                string IP_Data = @"INSERT INTO ITERMINAL_QBOOKING (EVENT_DATE, DEPOT, VEHICLE_HEAD, VEHICLE_TAIL, ITEM_NO, SPERSONALNO, SEMPLOYEENAME, CREATEDATE, SVENDORID, DRIVER_NO,RESULT_FLAG,RESULT_MSG) 
//VALUES ( SYSDATE, :DEPOT, :VEHICLE_HEAD, :VEHICLE_TAIL, :ITEM_NO, :SPERSONALNO, :SEMPLOYEENAME, :CREATEDATE, :SVENDORID, :DRIVER_NO, :CSTATUS, :REMARK)";
//                using (OracleCommand com = new OracleCommand(IP_Data, con))
//                {
//                    com.Parameters.Add(":DEPOT", OracleType.VarChar).Value = DEPOT;
//                    com.Parameters.Add(":VEHICLE_HEAD", OracleType.VarChar).Value = VEHICLE_HEAD;
//                    com.Parameters.Add(":VEHICLE_TAIL", OracleType.VarChar).Value = VEHICLE_TAIL;
//                    com.Parameters.Add(":ITEM_NO", OracleType.Number).Value = ITEM_NO;
//                    com.Parameters.Add(":SPERSONALNO", OracleType.VarChar).Value = SPERSONALNO;
//                    com.Parameters.Add(":SEMPLOYEENAME", OracleType.VarChar).Value = SEMPLOYEENAME;
//                    com.Parameters.Add(":CREATEDATE", OracleType.DateTime).Value = DateTime.Now;
//                    com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = SVENDORID;
//                    com.Parameters.Add(":DRIVER_NO", OracleType.VarChar).Value = DRIVER_NO;
//                    com.Parameters.Add(":CSTATUS", OracleType.VarChar).Value = sStatus.CSTATUS;
//                    com.Parameters.Add(":REMARK", OracleType.VarChar).Value = sStatus.REMARK;
//                    com.ExecuteNonQuery();
//                }
//            }
//        }



//        return sStatus;
//    }

//    [WebMethod]
//    public STATUS PlanDO_SYNC_IN(string DELIV_DATE, string TIMEWINDOW, string SUPPLY_PLNT, string VEHICLE_HEAD, string VEHICLE_TAIL, string[] Drop_No, string[] Delivery, string[] SHIP_TO)
//    {


//        string sError = "";
//        STATUS sStatus = new STATUS();

//        string _DELIV_DATE = CommonFunction.ReplaceInjection(DELIV_DATE);
//        string _TIMEWINDOW = CommonFunction.ReplaceInjection(TIMEWINDOW);
//        string _SUPPLY_PLNT = CommonFunction.ReplaceInjection(SUPPLY_PLNT);
//        string _VEHICLE_HEAD = CommonFunction.ReplaceInjection(VEHICLE_HEAD);
//        string _VEHICLE_TAIL = CommonFunction.ReplaceInjection(VEHICLE_TAIL);
//        string[] _Drop_No = Drop_No;
//        string[] _Delivery = Delivery;
//        string[] _SHIP_TO = SHIP_TO;

//        for (int i = 0; i < Drop_No.Length; i++)
//        {
//            _Drop_No[i] = CommonFunction.ReplaceInjection(Drop_No[i]);
//        }

//        for (int i = 0; i < Delivery.Length; i++)
//        {
//            _Delivery[i] = CommonFunction.ReplaceInjection(Delivery[i]);
//        }

//        for (int i = 0; i < SHIP_TO.Length; i++)
//        {
//            _SHIP_TO[i] = CommonFunction.ReplaceInjection(SHIP_TO[i]);
//        }


//        if (_DELIV_DATE == "" || _TIMEWINDOW == "" || _SUPPLY_PLNT == "" || _VEHICLE_HEAD == "" || (_Drop_No.Length == 0) || (_Delivery.Length == 0) || (_SHIP_TO.Length == 0))
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = "Please fill Parameter";
//            return sStatus;
//        }

//        if (!(_Drop_No.Length == _Delivery.Length && _Delivery.Length == _SHIP_TO.Length))
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = "Please fill Parameter";
//            return sStatus;
//        }



//        if (_VEHICLE_HEAD == _VEHICLE_TAIL)
//        {
//            _VEHICLE_TAIL = "";
//        }


//        DateTime date;
//        DateTime DDELIVERY = DateTime.TryParse(_DELIV_DATE, out date) ? date : DateTime.Now;
//        if (DDELIVERY.Year < 1500)
//        {
//            DDELIVERY = DDELIVERY.AddYears(543);
//        }

//        try
//        {

//            StringBuilder DetailAlertList = new StringBuilder();

//            using (OracleConnection con = new OracleConnection(sql))
//            {
//                if (con.State == ConnectionState.Closed) con.Open();

//                string NPLANPLUS = "";


//                DateTime datee;

//                string tempEmployee = "";

//                #region  for excel rows


//                #region ถ้าเช็คFormat วันที่ขนส่ง ผ่าน
//                DateTime datetime;

//                string tmpTerminal = _SUPPLY_PLNT;
//                #region ///รถคันดังกล่าวถูกยืนยัน มา ณ คลังที่ระบุหรือไม่
//                string sqlCheckTruck = @"SELECT f.SHEADREGISTERNO,f.SEMPLOYEEID
//                FROM  TFIFO f
//                WHERE  NVL(f.CACTIVE,'1') = '1' AND f.SHEADREGISTERNO LIKE '%" + VEHICLE_HEAD + "%' AND TO_CHAR(f.DDATE,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND f.STERMINAL LIKE '" + tmpTerminal + "'  GROUP BY  f.SHEADREGISTERNO";



//                //                                    string sqlCheckTruck = @"SELECT Tc.SHEADREGISTERNO
//                //                FROM  (TTRUCKCONFIRMLIST tc LEFT JOIN TTRUCKCONFIRM tf ON tc.NCONFIRMID = TF.NCONFIRMID) 
//                //                LEFT JOIN TCONTRACT ct ON TF.SCONTRACTID = CT.SCONTRACTID  
//                //                WHERE  TC.CCONFIRM = '1'  AND TC.SHEADREGISTERNO LIKE '%" + dt.Rows[i]["VEHICLE-HEAD"] + "%' AND TF.DDATE = To_Date('" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "','dd/MM/yyyy') -1  /* AND f.STERMINAL LIKE AND nvl(ct.STERMINALID,'" + tmpTerminal + "') LIKE '" + tmpTerminal + "' */  GROUP BY  Tc.SHEADREGISTERNO";
//                DataTable numTruckConfirm = CommonFunction.Get_Data(con, sqlCheckTruck);
//                if (numTruckConfirm.Rows.Count < 1)
//                {

//                    DetailAlertList.Append(",ยังไม่ได้ถูกจัดคิว");

//                }
//                else
//                {
//                    tempEmployee = numTruckConfirm.Rows[0]["SEMPLOYEEID"] + "";
//                }
//                #endregion

//                string chkNPLAN = CommonFunction.Get_Value(con, "SELECT nPlanID FROM TPLANSCHEDULE WHERE CACTIVE = '1' AND TO_CHAR(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "' AND STIMEWINDOW = '" + _TIMEWINDOW + "' AND STERMINALID = '" + _SUPPLY_PLNT + "' AND SHEADREGISTERNO = '" + _VEHICLE_HEAD + "' AND nvl(STRAILERREGISTERNO,'1') = nvl('" + _VEHICLE_TAIL + "','1')");
//                //ข็อมูล
//                int num1 = 0;
//                int num2 = 0;




//                int num = 0;
//                string OutboundPlus = "";

//                #region ///วนข้อมูล ตาม [DELIV DATE],[TIMEWINDOW],[SUPPLY PLNT],[VEHICLE-HEAD],[VEHICLE-TAIL]


//                #region เช็ค เลขDO DELIVERY ว่าใส่มาหรือไม่

//                #region เช็ค เลขDO DELIVERY ซ้ำใน Excel

//                #region จัดแผนซ้ำใน Excel (วันที่ขนส่ง+รถ+คลัง+TimeWindow+Drop)

//                string _OldDrop = "";
//                foreach (string sDrop in _Drop_No)
//                {
//                    if (_OldDrop != sDrop)
//                    {
//                        _OldDrop = sDrop;
//                    }
//                    else
//                    {
//                        DetailAlertList.Append(",Drop " + _OldDrop + " ซ้ำกัน");
//                    }
//                }


//                #endregion

//                foreach (string sDelivery in _Delivery)
//                {
//                    OutboundPlus += ",'" + sDelivery + "'";
//                }

//                if (chkNPLAN == "")
//                {//ถ้า วันที่ขนส่ง+ทามวินโดว+คลัง+หัว+หาง ที่ระบุมาไม่มีอยู่ในระบบ
//                    #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO ไม่เคยมีจัดแผน

//                    string sDropNO = "";
//                    foreach (string sValue in _Drop_No)
//                    {
//                        sDropNO += ",'" + sValue + "'";
//                    }

//                    int CheckCar = CommonFunction.Count_Value(sql, "SELECT PL.SDELIVERYNO FROM TPLANSCHEDULE p LEFT JOIN TPLANSCHEDULELIST pl ON P.NPLANID = PL.NPLANID WHERE  p.CACTIVE = '1' AND pl.CACTIVE = '1' AND P.SHEADREGISTERNO = '"
//                        + _VEHICLE_HEAD + "' AND P.STIMEWINDOW = '" + _TIMEWINDOW
//                        + "' AND TO_CHAR(DDELIVERY,'dd/MM/yyyy') = '" + DDELIVERY.ToString("dd/MM/yyyy", new CultureInfo("en-US"))
//                        + "' AND p.STERMINALID ='" + _SUPPLY_PLNT + "' " + ((sDropNO.Length > 0) ? "AND pl.NDROP IN (" + sDropNO.Remove(0, 1) + ")" : ""));
//                    if (CheckCar > 0)
//                    {
//                        DetailAlertList.Append(",Drop " + sDropNO.Remove(0, 1) + " มีอยู่ในระบบแล้ว");
//                    }
//                    #endregion
//                }
//                else
//                {
//                    #region//ถ้า DDELIVERY,STIMEWINDOW,STERMINALID,SHEADREGISTERNO,STRAILERREGISTERNO เคยมีจัดแผน

//                    OracleCommand comcount = new OracleCommand("SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND SDELIVERYNO IN ('" + OutboundPlus.Remove(0, 1) + "') AND NPLANID != '" + chkNPLAN + "'", con);
//                    int countDeliveryno = int.Parse("" + comcount.ExecuteScalar());
//                    if (countDeliveryno > 0)
//                    {
//                        DetailAlertList.Append(",DELIVERYNO " + OutboundPlus.Remove(0, 1) + " มีอยู่ในระบบแล้ว");
//                    }

//                    #endregion
//                }



//                #endregion

//                #endregion

//                #endregion

//                #region เช็คปริมานเกินความจุรถต่อเที่ยว

//                string sDO_NO = "";

//                foreach (string sDelivery in _Delivery)
//                {
//                    sDO_NO += "," + sDelivery + "";
//                }



//                sDO_NO = (sDO_NO.Length > 0) ? sDO_NO.Remove(0, 1) : "";
//                string DO_TOTLE_VOLUMN = CommonFunction.Get_Value(con, "SELECT  SUM(nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0)) AS NVALUE FROM TDELIVERY WHERE DELIVERY_NO in ( '" + sDO_NO.Replace(",", "','") + "' )");

//                string sTruck;

//                if (!string.IsNullOrEmpty("" + _VEHICLE_TAIL))
//                {
//                    sTruck = _VEHICLE_TAIL;
//                }
//                else
//                {
//                    sTruck = _VEHICLE_HEAD;
//                }
//                double def_Double;
//                string STOTALCAP = CommonFunction.Get_Value(sql, "SELECT nvl(T.NTOTALCAPACITY, SUM(nvl(tc.NCAPACITY,0))) As NCAPACITY FROM TTRUCK t LEFT JOIN (SELECT STRUCKID,NCOMPARTNO, MAX(nvl(NCAPACITY,0)) AS NCAPACITY FROM TTRUCK_COMPART GROUP BY STRUCKID,NCOMPARTNO) tc ON TC.STRUCKID = T.STRUCKID  WHERE t.SHEADREGISTERNO = '" + sTruck.Trim() + "' GROUP BY  T.NTOTALCAPACITY");
//                double TRCK_TOTALCAP = double.TryParse(STOTALCAP, out def_Double) ? double.Parse(STOTALCAP) + 100 : 0.00;
//                double DO_VOLUMN = double.TryParse(DO_TOTLE_VOLUMN, out def_Double) ? def_Double : 0.00;
//                if (TRCK_TOTALCAP < DO_VOLUMN)
//                {
//                    DetailAlertList.Append(",เกินปริมาณความจุของรถ ความจุของรถ : " + ((TRCK_TOTALCAP > 0) ? (TRCK_TOTALCAP - 100) : 0) + " ปริมาณที่จัดแผน :" + DO_VOLUMN);

//                }

//                #endregion


//                #endregion

//                #endregion





//                if (DetailAlertList.Length > 0)
//                {
//                    sStatus.CSTATUS = "E";
//                    sStatus.ID = "";
//                    sStatus.REMARK = DetailAlertList.ToString();
//                    return sStatus;
//                }





//                #region หาขอบเขตของข้อมูลที่ต้องการนำเข้าและบันทึกลงดาต้าเบส

//                //หาขอบเขตของข้อมูลที่ต้องการนำเข้า
//                string strsql = "INSERT INTO TPLANSCHEDULE(SEMPLOYEEID,CCONFIRM,SVENDORID,nPlanID,nNo,sTerminalID,dPlan,sPlanDate,sPlanTime,dDelivery,sTimeWindow,sHeadRegisterNo,sTrailerRegisterNo,cActive,dCreate,sCreate,dUpdate,sUpdate,CFIFO) VALUES (:SEMPLOYEEID,'1',:SVENDORID,:nPlanID,1,:sTerminalID,:dPlan,:sPlanDate,:sPlanTime,:dDelivery,:sTimeWindow,:sHeadRegisterNo,:sTrailerRegisterNo,'1',sysdate,:sCreate,sysdate,:sUpdate,'1')";
//                string strsql1 = "INSERT INTO TPLANSCHEDULELIST(sPlanListID,nPlanID,nDrop,sShipTo,sDeliveryNo,cActive,NVALUE) VALUES (:sPlanListID,:nPlanID,:nDrop,:sShipTo,:sDeliveryNo,'1',:NVALUE)";



//                DateTime Now = DateTime.Now;
//                DateTime DDELIVERYCONVERT = DDELIVERY.Date.Add(new TimeSpan(Now.Hour, Now.Minute, Now.Second));


//                if (chkNPLAN == "") //ถ้าไม่มีรายการเดิม
//                {
//                    string genid = CommonFunction.Gen_ID(con, "SELECT nPlanID FROM (SELECT nPlanID FROM TPLANSCHEDULE ORDER BY nPlanID DESC) WHERE ROWNUM <= 1");
//                    int nResult = 0;
//                    using (OracleCommand com = new OracleCommand(strsql, con))
//                    {

//                        string venderid = CommonFunction.Get_Value(con, "SELECT CASE WHEN C.SVENDORID IS NOT NULL THEN C.SVENDORID ELSE T.STRANSPORTID END AS SVENDORID FROM (TTRUCK t LEFT JOIN TCONTRACT_TRUCK ct ON T.STRUCKID = CT.STRUCKID)LEFT JOIN TCONTRACT c ON CT.SCONTRACTID = C.SCONTRACTID  WHERE REPLACE(REPLACE(T.SHEADREGISTERNO,'-',''),'.','') = REPLACE(REPLACE('" + _VEHICLE_HEAD + "','-',''),'.','') ");

//                        com.Parameters.Clear();
//                        com.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = tempEmployee;
//                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = venderid;
//                        com.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
//                        com.Parameters.Add(":sTerminalID", OracleType.VarChar).Value = _SUPPLY_PLNT + "";
//                        com.Parameters.Add(":dPlan", OracleType.DateTime).Value = DateTime.Now;
//                        com.Parameters.Add(":sPlanDate", OracleType.DateTime).Value = DDELIVERYCONVERT;
//                        com.Parameters.Add(":sPlanTime", OracleType.VarChar).Value = "";
//                        com.Parameters.Add(":dDelivery", OracleType.DateTime).Value = DDELIVERYCONVERT;
//                        com.Parameters.Add(":sTimeWindow", OracleType.Number).Value = int.TryParse(_TIMEWINDOW, out num1) ? num1 : 0;
//                        com.Parameters.Add(":sHeadRegisterNo", OracleType.VarChar).Value = _VEHICLE_HEAD + "";
//                        com.Parameters.Add(":sTrailerRegisterNo", OracleType.VarChar).Value = _VEHICLE_TAIL + "";
//                        com.Parameters.Add(":sCreate", OracleType.VarChar).Value = "sysadmin";
//                        com.Parameters.Add(":sUpdate", OracleType.VarChar).Value = "sysadmin";
//                        nResult = com.ExecuteNonQuery();

//                        if (nResult > 0)
//                        {
//                            sStatus.CSTATUS = "S";
//                            sStatus.ID = genid;
//                            sStatus.REMARK = "";
//                        }
//                        else
//                        {
//                            sStatus.CSTATUS = "E";
//                            sStatus.ID = "";
//                            sStatus.REMARK = "No Row Insert";
//                        }

//                    }

//                    //update fifo
//                    string strsql2222 = "UPDATE TFIFO SET CPLAN = '1' WHERE NVL(CACTIVE,'1') = '1' AND SHEADREGISTERNO LIKE '%" + _VEHICLE_HEAD + "%' AND TO_CHAR(DDATE,'dd/MM/yyyy') = '" + DDELIVERYCONVERT.ToString("dd/MM/yyyy", new CultureInfo("en-US")) + "'  AND STERMINAL LIKE '" + _SUPPLY_PLNT + "'";
//                    using (OracleCommand com2 = new OracleCommand(strsql2222, con))
//                    {
//                        com2.ExecuteNonQuery();
//                    }

//                    int idn = 0;

//                    foreach (string sDrop in _Drop_No)
//                    {


//                        string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
//                        int NDROP = int.TryParse(sDrop + "", out num2) ? num2 : 0;

//                        DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') = LPAD('" + _Delivery[idn] + "',10,'0')");

//                        int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

//                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
//                        {

//                            com1.Parameters.Clear();
//                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
//                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = genid;
//                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
//                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = _Delivery[idn];
//                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = _SHIP_TO[idn];
//                            com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
//                            com1.ExecuteNonQuery();

//                        }

//                        idn++;
//                    }
//                }
//                else // มีรายการเดิม
//                {
//                    sStatus.CSTATUS = "S";
//                    sStatus.ID = chkNPLAN;
//                    sStatus.REMARK = "";

//                    using (OracleCommand delcom = new OracleCommand("DELETE FROM TPLANSCHEDULELIST WHERE NPLANID = '" + chkNPLAN + "'", con))
//                    {
//                        delcom.ExecuteNonQuery();
//                    }

//                    int idn = 0;

//                    foreach (string sDrop in _Drop_No)
//                    {


//                        string genid1 = CommonFunction.Gen_ID(con, "SELECT sPlanListID FROM (SELECT sPlanListID FROM TPLANSCHEDULELIST ORDER BY sPlanListID DESC) WHERE ROWNUM <= 1");
//                        int NDROP = int.TryParse(sDrop + "", out num2) ? num2 : 0;

//                        DataTable dtData = CommonFunction.Get_Data(con, "SELECT SHIP_TO,nvl(ULG,0) + nvl(ULR,0) + nvl(HSD,0) + nvl(LSD,0) + nvl(IK,0) + nvl(GH,0) + nvl(PL,0) + nvl(GH95,0) + nvl(GH95E20,0) + nvl(GH95E85,0) + nvl(HSDB5,0) + nvl(OTHER,0) AS NVALUE FROM TDELIVERY WHERE LPAD(DELIVERY_NO,10,'0') = LPAD('" + _Delivery[idn] + "',10,'0')");

//                        int NNVALUE = (dtData.Rows.Count > 0) ? (int.TryParse("" + dtData.Rows[0][1], out num) ? num : 0) : 0;

//                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
//                        {

//                            com1.Parameters.Clear();
//                            com1.Parameters.Add(":sPlanListID", OracleType.Number).Value = genid1;
//                            com1.Parameters.Add(":nPlanID", OracleType.Number).Value = chkNPLAN;
//                            com1.Parameters.Add(":nDrop", OracleType.Number).Value = NDROP;
//                            com1.Parameters.Add(":sDeliveryNo", OracleType.VarChar).Value = _Delivery[idn];
//                            com1.Parameters.Add(":sShipTo", OracleType.VarChar).Value = _SHIP_TO[idn];
//                            com1.Parameters.Add(":NVALUE", OracleType.Number).Value = NNVALUE;
//                            com1.ExecuteNonQuery();

//                        }

//                        idn++;
//                    }

//                }

//                #endregion

//            }
//        }

//        catch (Exception ex)
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = ex.Message;
//            return sStatus;
//        }

//        return sStatus;
//    }

//    [WebMethod]
//    public STATUS CancelPlanDO_SYNC_IN(string PlanID, string Delivery)
//    {


//        string sError = "";
//        STATUS sStatus = new STATUS();

//        string _PlanID = CommonFunction.ReplaceInjection(PlanID);
//        string _Delivery = CommonFunction.ReplaceInjection(Delivery);



//        if (_PlanID == "" || _Delivery == "")
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = "Please fill Parameter";
//            return sStatus;
//        }


//        try
//        {


//            using (OracleConnection con = new OracleConnection(sql))
//            {
//                if (con.State == ConnectionState.Closed) con.Open();

//                string strsql1 = "UPDATE TPlanSchedulelist SET CACTIVE = '0' WHERE NPLANID = :NPLANID AND SDELIVERYNO = :SDELIVERYNO";
//                int nResult = 0;
//                using (OracleCommand com1 = new OracleCommand(strsql1, con))
//                {
//                    com1.Parameters.Add(":NPLANID", OracleType.Number).Value = _PlanID;
//                    com1.Parameters.Add(":SDELIVERYNO", OracleType.NVarChar).Value = _Delivery;
//                    nResult = com1.ExecuteNonQuery();

//                    if (nResult > 0)
//                    {
//                        sStatus.CSTATUS = "S";
//                        sStatus.ID = _PlanID;
//                        sStatus.REMARK = "";
//                    }
//                    else
//                    {
//                        sStatus.CSTATUS = "E";
//                        sStatus.ID = "";
//                        sStatus.REMARK = "No Row Insert";
//                    }
//                }

//                string CountValue = CommonFunction.Get_Value(sql, "SELECT COUNT(*) FROM TPLANSCHEDULELIST WHERE CACTIVE = '1' AND NPLANID = " + _PlanID);

//                if (CountValue == "0")
//                {

//                    string strsql = "UPDATE TPlanSchedule SET CACTIVE = '0' WHERE NPLANID = :NPLANID";
//                    using (OracleCommand com = new OracleCommand(strsql, con))
//                    {
//                        com.Parameters.Add(":NPLANID", OracleType.Number).Value = _PlanID;
//                        com.ExecuteNonQuery();
//                    }
//                }

//            }
//        }

//        catch (Exception ex)
//        {
//            sStatus.CSTATUS = "E";
//            sStatus.ID = "";
//            sStatus.REMARK = ex.Message;
//            return sStatus;
//        }

//        return sStatus;
//    }
//}

//#endregion