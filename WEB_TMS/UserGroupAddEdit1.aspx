﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" 
    CodeFile="UserGroupAddEdit1.aspx.cs" Inherits="UserGroupAddEdit1" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<%@ Register Src="~/UserControl/SuccessModel.ascx" TagPrefix="uc1" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style15
        {
            color: #03527C;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="container" style="width: 100%;">
        <div class="jumbotron" style="background-color: white">
            <asp:UpdatePanel ID="udpMain" runat="server">
                <ContentTemplate>
                    <h2 class="page-header" style="margin-top: 10px">
                        <font style="font-family: 'Angsana New'; color: #009120"> เพิ่ม - แก้ไข กลุ่มผู้ใช้งาน</font>
                    </h2>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <i class="fa fa-edit"></i><a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">แสดงข้อมูล&#8711;</a>
                            <input type="hidden" id="hiddencollapseFindContract" value=" " />
                        </div>
                        <div class="panel-collapse collapse in" id="collapseFindContract">
                            <div class="panel-body">
                                <asp:Table runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell Width="50%">
                                            <div class="form-group">
                                                <label style="font-family: 'Angsana New'; font-size: 20px; color: red">ชื่อกลุ่มผู้ใช้งาน *</label>
                                                <asp:TextBox ID="txtUserGroupName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </asp:TableCell>
                                        <asp:TableCell>&nbsp;</asp:TableCell>
                                        <asp:TableCell>
                                            <div class="form-group">
                                                <label style="font-family: 'Angsana New'; font-size: 20px; color: red">สถานะ *</label>
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Width="65%">
                                            <div class="form-group">
                                                <label style="font-family: 'Angsana New'; font-size: 20px; color: red">สถานะพิเศษ *</label>
                                                <asp:RadioButtonList ID="radIsAdmin" runat="server" RepeatDirection="Horizontal">
                                                   <%-- <asp:ListItem Value="0" Text="&nbsp;ผู้ดูแลระบบ&nbsp;&nbsp;&nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="&nbsp;ผู้ใช้งานกลุ่ม ปตท.&nbsp;&nbsp;&nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="&nbsp;ผู้ประกอบการขนส่ง&nbsp;&nbsp;&nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="&nbsp;ผจ. ขปน.&nbsp;&nbsp;&nbsp;"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="&nbsp;ผู้ใช้งานทั่วไป" Selected="True"></asp:ListItem>--%>
                                                </asp:RadioButtonList>
                                            </div>
                                        </asp:TableCell>
                                        <asp:TableCell>&nbsp;</asp:TableCell>

                                    </asp:TableRow>
                                </asp:Table>
                            </div>
                        </div>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-success" Width="90px" data-toggle="modal" data-target="#ModalConfirmBeforeSave" UseSubmitBehavior="false" />
                            <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" CssClass="btn btn-success" Width="90px" OnClick="cmdCancel_Click" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <uc1:ModalPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
                IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
                TextTitle="ยืนยันการทำงาน" TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" />
        </div>
    </div>
</asp:Content>
