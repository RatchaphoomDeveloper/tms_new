﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_terminal_remain_lst.aspx.cs" Inherits="vendor_terminal_remain_lst"
    StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="65%" class="style13">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td width="8%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" NullText="ค้นหาจากทะเบียนรถหรือคลังปลายทาง"
                                Style="margin-left: 0px" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds" SettingsPager-PageSize="10" OnAfterPerformCallback="gvw_AfterPerformCallback">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="2" FieldName="NCARBANID"
                                        Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่ขนส่ง" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="DDELIVERY">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Outbound No" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="SOUTBOUNDNO">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คลังปลายทาง" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                        Width="20%" FieldName="STERMINALNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เที่ยวที่" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                        Width="5%" FieldName="STIMEWINDOW">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภทผลิตภัณฑ์" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="SPRODUCTTYPENAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภทรถ" VisibleIndex="8" HeaderStyle-HorizontalAlign="Center"
                                        Width="10%" FieldName="SCARCATEGORY">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถหัว" VisibleIndex="9" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SHEADREGISTERNO" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถท้าย" VisibleIndex="10" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="STRAILERREGISTERNO" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="5%" CellStyle-Cursor="hand" VisibleIndex="11">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            <Image Width="16px" Height="16px" Url="Images/zoom.png" ></Image>
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <EditForm>
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table width="80%" style="text-align: left">
                                                        <tr>
                                                            <td>
                                                                ชื่อพนักงานขับรถ
                                                            </td>
                                                            <td colspan="3" align="left" style="padding-left: 12px">
                                                                <dx:ASPxLabel ID="lblEmployeeName" runat="server" Text='<%# Eval("EFULLNAME") %>'>
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#FFFFFF" class="style24" valign="top">
                                                                สาเหตุรถตกค้าง
                                                            </td>
                                                            <td align="left" bgcolor="#FFFFFF" class="style21" colspan="3">
                                                                <dx:ASPxCheckBoxList ID="chkBanCause" runat="server" RepeatColumns="2" EnableTheming="True"
                                                                    TextField="SCARBANCAUSENAME" ValueField="NCARBANCAUSEID" ValueType="System.Int32"
                                                                    Enabled="false" ItemSpacing="3px" Width="80%">
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxCheckBoxList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#FFFFFF" style="width: 15%">
                                                                วันที่บันทึกข้อมูล
                                                            </td>
                                                            <td style="width: 35%; padding-left: 12px">
                                                                <dx:ASPxLabel ID="lblDateCreate" runat="server" Text='<%# Eval("DCREATE") %>'>
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 15%;">
                                                                ผู้บันทึกข้อมูล
                                                            </td>
                                                            <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 35%;">
                                                                <dx:ASPxLabel ID="lblNameCreate" runat="server" Text='<%# Eval("FULLNAME") %>'>
                                                                </dx:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT ROW_NUMBER () OVER (ORDER BY C.NCARBANID) AS ID1, C.NCARBANID, p.DDELIVERY,C.SOUTBOUNDNO,TP.STERMINALNAME,p.STIMEWINDOW,PT.SPRODUCTTYPENAME,TT.SCARCATEGORY,C.SHEADREGISTERNO,C.STRAILERREGISTERNO,C.DCREATE,U.SFIRSTNAME || ' ' || U.SLASTNAME AS FULLNAME, CASE WHEN p.SEMPLOYEEID is null THEN 'ไม่มีข้อมูล' ELSE  EP.FNAME || ' ' || EP.LNAME END AS EFULLNAME 
FROM  (((((((TCARBAN c LEFT JOIN  ((SELECT PL.SDELIVERYNO,max(pl.NPLANID) AS PLANID FROM TPlanScheduleList pl WHERE CACTIVE = '1' Group by  PL.SDELIVERYNO) dd INNER JOIN TPLANSCHEDULE p ON P.NPLANID = dd.PLANID ) ON C.SOUTBOUNDNO = dd.SDELIVERYNO) 
LEFT JOIN TTERMINAL_SAP tp ON C.STERMINALID = TP.STERMINALID)LEFT JOIN TDELIVERY d ON dd.SDELIVERYNO = D.DELIVERY_NO) LEFT JOIN TPRODUCTTYPE pt ON D.SPRODUCTTYPEID = PT.SPRODUCTTYPEID) LEFT JOIN TTRUCK tr ON C.SHEADREGISTERNO = TR.SHEADREGISTERNO AND nvl(C.STRAILERREGISTERNO,1) = nvl(TR.STRAILERREGISTERNO,1)) 
LEFT JOIN TTRUCKTYPE tt ON TR.SCARTYPEID = TT.SCARTYPEID) LEFT JOIN TUSER u ON c.SCREATE = u.SUID) LEFT JOIN TEMPLOYEE_SAP ep ON p.SEMPLOYEEID = EP.SEMPLOYEEID WHERE p.CACTIVE = '1' AND C.SVENDORID = :oSVENDORID AND  (C.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR C.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TP.STERMINALNAME LIKE '%' || :oSearch || '%' OR C.SOUTBOUNDNO LIKE '%' || :oSearch || '%')"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch" PropertyName="Text" />
                                    <asp:SessionParameter Name="oSVENDORID" SessionField="SVDID" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
