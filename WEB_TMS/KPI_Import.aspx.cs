﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using GemBox.Spreadsheet;
using System.IO;

public partial class KPI_Import : PageBase
{
    #region + View State +
    private int TopicHeaderID
    {
        get
        {
            if ((int)ViewState["TopicHeaderID"] != null)
                return (int)ViewState["TopicHeaderID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicHeaderID"] = value;
        }
    }

    private int UserID
    {
        get
        {
            if ((int)ViewState["UserID"] != null)
                return (int)ViewState["UserID"];
            else
                return 0;
        }
        set
        {
            ViewState["UserID"] = value;
        }
    }

    private DataTable dtKPI
    {
        get
        {
            if ((DataTable)ViewState["dtKPI"] != null)
                return (DataTable)ViewState["dtKPI"];
            else
                return null;
        }
        set
        {
            ViewState["dtKPI"] = value;
        }
    }

    private DataTable dtIVMSTemp
    {
        get
        {
            if ((DataTable)ViewState["dtIVMSTemp"] != null)
                return (DataTable)ViewState["dtIVMSTemp"];
            else
                return null;
        }
        set
        {
            ViewState["dtIVMSTemp"] = value;
        }
    }

    private DataTable dtIVMS_A
    {
        get
        {
            if ((DataTable)ViewState["dtIVMS_A"] != null)
                return (DataTable)ViewState["dtIVMS_A"];
            else
                return null;
        }
        set
        {
            ViewState["dtIVMS_A"] = value;
        }
    }

    private DataTable dtIVMS_B
    {
        get
        {
            if ((DataTable)ViewState["dtIVMS_B"] != null)
                return (DataTable)ViewState["dtIVMS_B"];
            else
                return null;
        }
        set
        {
            ViewState["dtIVMS_B"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.InitialForm();
            this.AssignAuthen();
        }
        if (cmdSave.Enabled)
            cmdExport_Click(null, null);
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdExport.Enabled = false;
                cmdExportExcel.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdSave.Enabled = false;
            }
            cmdExportExcel.CssClass = "btn btn-md bth-hover btn-info";
            cmdExport.CssClass = "btn btn-md bth-hover btn-info";
            cmdSave.CssClass = "btn btn-md bth-hover btn-info";
            cmdCancel.CssClass = "btn btn-md bth-hover btn-info";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void InitialForm()
    {
        try
        {
            this.SetDrowDownList();

            dtIVMSTemp = new DataTable();
            dtIVMSTemp.Columns.Add("COLUMN_INDEX");
            dtIVMSTemp.Columns.Add("SELECTED_INDEX");

            UserID = int.Parse(Session["UserID"].ToString());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetDrowDownList()
    {
        try
        {
            for (int i = 2015; i < DateTime.Now.Year + 1; i++)
            {
                ddlYear.Items.Add(new ListItem()
                {
                    Text = i + string.Empty,
                    Value = i + string.Empty
                });
            }
            ddlYear.SelectedValue = DateTime.Now.ToString("yyyy", new CultureInfo("en-US"));

            ViewState["DataMonth"] = KPI2BLL.Instance.MonthSelect();
            ddlMonth.DataTextField = "NAME_TH";
            ddlMonth.DataValueField = "MONTH_ID";
            ddlMonth.DataSource = ViewState["DataMonth"];
            ddlMonth.DataBind();
            ddlMonth.SelectedValue = DateTime.Now.Month.ToString();

            this.GetKPIForm(null, null);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialGridView()
    {
        try
        {
            dgvKPI.Columns.Clear();

            dgvKPI.Columns.Add(this.GenerateColumnBoundField("SVENDORID", "SVENDORID", false, 0));
            dgvKPI.Columns.Add(this.GenerateColumnBoundField("SCONTRACTID", "SCONTRACTID", false, 0));
            dgvKPI.Columns.Add(this.GenerateColumnBoundField("NO", string.Empty, true, 65));
            dgvKPI.Columns.Add(this.GenerateColumnBoundField("SCONTRACTNO", string.Empty, true, 150));
            //dgvKPI.Columns.Add(this.GenerateColumnBoundField("NO", "ลำดับ", true));
            //dgvKPI.Columns.Add(this.GenerateColumnBoundField("SCONTRACTNO", "เลขที่สัญญา", true));
            //dgvKPI.Columns.Add(this.GenerateColumnBoundField("SABBREVIATION", "ผู้ขนส่ง", true));
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private BoundField GenerateColumnBoundField(string DataField, string HeaderText, bool Visible, int Width)
    {
        try
        {
            BoundField Col = new BoundField();
            Col.DataField = DataField;
            Col.ItemStyle.Width = Width;
            Col.HeaderText = HeaderText;
            Col.ItemStyle.Wrap = false;
            Col.Visible = Visible;

            return Col;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private BoundField GenerateColumnBoundField2(string DataField, string HeaderText, bool Visible, int Width)
    {
        try
        {
            BoundField Col = new BoundField();
            Col.DataField = DataField;
            Col.ItemStyle.Width = Width;
            Col.HeaderText = HeaderText;
            Col.ItemStyle.Wrap = false;
            Col.Visible = Visible;

            return Col;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private TemplateField GenerateColumnTemplateField(string DataField, string HeaderText, bool Visible)
    {
        try
        {
            TemplateField Col = new TemplateField();
            Col.HeaderText = HeaderText;
            Col.ItemStyle.Wrap = false;
            Col.Visible = Visible;

            return Col;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdExport_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateExport();

            ddlYear.Enabled = false;
            ddlMonth.Enabled = false;

            cmdSave.Enabled = true;
            cmdCancel.Enabled = true;

            this.InitialGridView();

            GridView HeaderGrid = new GridView();
            GridViewRow HeaderGridRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            TableCell HeaderCell;

            HeaderCell = new TableCell();
            HeaderCell.Text = "ลำดับ";
            HeaderCell.BackColor = Color.AliceBlue;
            HeaderGridRow.Cells.Add(HeaderCell);

            HeaderCell = new TableCell();
            HeaderCell.Text = "เลขที่สัญญา";
            HeaderCell.BackColor = Color.AliceBlue;
            HeaderGridRow.Cells.Add(HeaderCell);

            dtKPI = KPIBLL.Instance.KPIExportSelectBLL();
            DataTable dtForm = KPIBLL.Instance.KPIExportFormSelectBLL(TopicHeaderID);

            DataTable dtLastUpdate = KPIBLL.Instance.KPILastUpdateSelectBLL("IVMS", ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue));
            if (dtLastUpdate.Rows.Count > 0)
                txtIVMS.Text = dtLastUpdate.Rows[0]["IVMS_DATE"].ToString() + ", Version : " + dtLastUpdate.Rows[0]["KPI_VERSION"].ToString();
            else
                txtIVMS.Text = string.Empty;

            dtLastUpdate = KPIBLL.Instance.KPILastUpdateSelectBLL("SHIPMENT_COST", ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue));
            if (dtLastUpdate.Rows.Count > 0)
                txtShipmentCost.Text = dtLastUpdate.Rows[0]["SHIPMENT_COST_DATE"].ToString();
            else
                txtShipmentCost.Text = string.Empty;

            dtLastUpdate = KPIBLL.Instance.KPILastUpdateSelectBLL("KPI", ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue));
            if (dtLastUpdate.Rows.Count > 0)
                txtSaveDate.Text = dtLastUpdate.Rows[0]["KPI_DATE"].ToString();
            else
                txtSaveDate.Text = string.Empty;

            dtKPI.Rows.InsertAt(dtKPI.NewRow(), 0);

            for (int i = 0; i < dtForm.Rows.Count; i = i + 2)
            {
                string ColumnNameA = dtForm.Rows[i]["TOPIC_DETAIL_ID"].ToString() + "_A";
                string ColumnNameB = dtForm.Rows[i]["TOPIC_DETAIL_ID"].ToString() + "_B";

                dtKPI.Columns.Add(ColumnNameA);
                dtKPI.Rows[0][ColumnNameA] = dtForm.Rows[i]["VARIABLE_NAME"].ToString();

                if (!string.Equals(dtForm.Rows[i]["DEFAULT_VALUE"].ToString(), string.Empty))
                {
                    for (int j = 1; j < dtKPI.Rows.Count; j++)
                        dtKPI.Rows[j][ColumnNameA] = dtForm.Rows[i]["DEFAULT_VALUE"].ToString();
                }

                dgvKPI.Columns.Add(this.GenerateColumnBoundField(ColumnNameA, dtForm.Rows[i]["TOPIC_NAME"].ToString(), true, 80));

                dtKPI.Columns.Add(ColumnNameB);
                dtKPI.Rows[0][ColumnNameB] = dtForm.Rows[i + 1]["VARIABLE_NAME"].ToString();

                if (!string.Equals(dtForm.Rows[i + 1]["DEFAULT_VALUE"].ToString(), string.Empty))
                {
                    for (int j = 1; j < dtKPI.Rows.Count; j++)
                        dtKPI.Rows[j][ColumnNameB] = dtForm.Rows[i + 1]["DEFAULT_VALUE"].ToString();
                }

                dgvKPI.Columns.Add(this.GenerateColumnBoundField(ColumnNameB, string.Empty, true, 80));

                HeaderCell = new TableCell();
                HeaderCell.Text = dtForm.Rows[i]["TOPIC_NAME"].ToString();
                HeaderCell.HorizontalAlign = HorizontalAlign.Center;
                HeaderCell.ColumnSpan = 2;
                HeaderCell.BackColor = Color.AliceBlue;
                HeaderGridRow.Cells.Add(HeaderCell);
            }

            for (int i = 1; i < dtKPI.Rows.Count; i++)
                dtKPI.Rows[i]["NO"] = i.ToString();

            GridViewHelper.BindGridView(ref dgvKPI, dtKPI);
            this.CreateTextBoxGrid();

            dgvKPI.Controls[0].Controls.AddAt(0, HeaderGridRow);
            this.GenerateIVMSData();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        ddlYear.Enabled = true;
        ddlMonth.Enabled = true;

        cmdSave.Enabled = false;
        cmdCancel.Enabled = false;

        GridViewHelper.BindGridView(ref dgvKPI, null);
    }

    protected void GetKPIForm(object sender, EventArgs e)
    {
        try
        {
            cmdSave.Enabled = false;
            cmdCancel.Enabled = false;

            TopicHeaderID = -1;
            txtKPIForm.Text = string.Empty;

            DataSet ds = KPI2BLL.Instance.KPIMappingFormSelect(ddlYear.SelectedValue);
            DataTable dt = ds.Tables["MAPPING"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (string.Equals(dt.Rows[i]["MONTH_ID"].ToString(), ddlMonth.SelectedValue))
                {
                    TopicHeaderID = int.Parse(dt.Rows[i]["TOPIC_HEADER_ID"].ToString());
                    txtKPIForm.Text = dt.Rows[i]["HEADER_NAME"].ToString();
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateExport()
    {
        try
        {
            if (string.Equals(txtKPIForm.Text.Trim(), string.Empty))
                throw new Exception("ปี : " + ddlYear.SelectedValue + " เดือน : " + ddlMonth.SelectedItem.ToString() + " ยังไม่มีการ ตั้งค่าชุดประเมิน KPI");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvKPI_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvKPI.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvKPI, dtKPI);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void CreateTextBoxGrid()
    {
        try
        {
            int TopicDetailID = 0;
            DataTable dtMapping = new DataTable();
            string ColumnName = string.Empty;

            for (int i = 1; i < dgvKPI.Rows.Count; i++)
            {
                for (int j = 4; j < dgvKPI.Columns.Count; j++)
                {
                    if (string.Equals(dgvKPI.Rows[i].Cells[j].Text.Replace("&nbsp;", string.Empty), string.Empty))
                    {
                        TextBox txt = new TextBox();
                        txt.EnableViewState = true;
                        txt.ID = "TextBox_" + i.ToString() + "_" + j.ToString();
                        txt.CssClass = "form-control";
                        txt.Width = 80;
                        txt.MaxLength = 8;
                        dgvKPI.Rows[i].Cells[j].HorizontalAlign = HorizontalAlign.Center;
                        dgvKPI.Rows[i].Cells[j].Controls.Add(txt);

                        ColumnName = dtKPI.Columns[j].ToString();
                        TopicDetailID = int.Parse(ColumnName.Replace("_A", string.Empty).Replace("_B", string.Empty));

                        dtMapping = KPIBLL.Instance.KPIMappingFormSelectBLL(ddlYear.SelectedValue
                            , int.Parse(ddlMonth.SelectedValue)
                            , dtKPI.Rows[i]["SVENDORID"].ToString()
                            , int.Parse(dtKPI.Rows[i]["SCONTRACTID"].ToString())
                            , " AND TMP2.TOPIC_HEADER_ID = " + TopicHeaderID.ToString() + " AND TMP2.TOPIC_DETAIL_ID = " + TopicDetailID.ToString());

                        if (dtMapping.Rows.Count > 0)
                        {
                            if (ColumnName.Contains("_A"))
                                txt.Text = dtMapping.Rows[0]["VALUE_A"].ToString();
                            else if (ColumnName.Contains("_B"))
                                txt.Text = dtMapping.Rows[0]["VALUE_B"].ToString();
                        }
                    }
                }
            }

            for (int j = 1; j < dtKPI.Rows.Count; j++)
            {
                
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void GenerateIVMSData()
    {
        try
        {
            for (int i = 4; i < dgvKPI.Columns.Count; i = i + 2)
            {
                string Header_A = dgvKPI.Rows[0].Cells[i].Text;
                string Header_B = dgvKPI.Rows[0].Cells[i + 1].Text;

                DropDownList ddlIVMS_A = new DropDownList();
                ddlIVMS_A.ID = "ddlIVMS_Col" + i.ToString();
                ddlIVMS_A.AutoPostBack = true;
                ddlIVMS_A.SelectedIndexChanged += ddlIVMS_A_SelectedIndexChanged;

                DropDownList ddlIVMS_B = new DropDownList();
                ddlIVMS_B.ID = "ddlIVMS_Col" + (i + 1).ToString();
                ddlIVMS_B.AutoPostBack = true;
                ddlIVMS_B.SelectedIndexChanged += ddlIVMS_B_SelectedIndexChanged;

                ddlIVMS_A.Attributes.Add("class", "form-control");
                ddlIVMS_A.Attributes.Add("style", "width:100%");

                ddlIVMS_B.Attributes.Add("class", "form-control");
                ddlIVMS_B.Attributes.Add("style", "width:100%");

                dtIVMS_A = new DataTable();
                dtIVMS_A.Columns.Add("Value");
                dtIVMS_A.Columns.Add("Text");
                dtIVMS_A.Columns.Add("DataDate");

                dtIVMS_B = new DataTable();
                dtIVMS_B = dtIVMS_A.Clone();

                //Column A
                dtIVMS_A.Rows.Add("A0", Header_A, string.Empty);
                dtIVMS_A.Rows.Add("A1", "ขับรถเร็ว", string.Empty);                         //IVMS
                dtIVMS_A.Rows.Add("A2", "ไม่รูดบัตรและขับเกินเวลา", string.Empty);               //IVMS
                dtIVMS_A.Rows.Add("A3", "ขับประมาท", string.Empty);                       //IVMS
                dtIVMS_A.Rows.Add("A4", "อุบัติเหตุ", string.Empty);                        //TMS
                dtIVMS_A.Rows.Add("A5", "Loss", string.Empty);                          //Shipment Cost
                dtIVMS_A.Rows.Add("A6", "ค่าศูนย์", string.Empty);                          //Default value 0

                //Column B
                dtIVMS_B.Rows.Add("B0", Header_B, string.Empty);
                dtIVMS_B.Rows.Add("B1", "จำนวนรถ", string.Empty);                       //TMS
                dtIVMS_B.Rows.Add("B2", "ระยะทาง", string.Empty);                       //Shipment Cost
                dtIVMS_B.Rows.Add("B3", "Volumn", string.Empty);                        //Shipment Cost

                DropDownListHelper.BindDropDownList(ref ddlIVMS_A, dtIVMS_A, "Value", "Text", false);
                DropDownListHelper.BindDropDownList(ref ddlIVMS_B, dtIVMS_B, "Value", "Text", false);

                dgvKPI.Rows[0].Cells[i].Controls.Add(ddlIVMS_A);
                dgvKPI.Rows[0].Cells[i + 1].Controls.Add(ddlIVMS_B);
            }
        }
        catch (Exception ex)
        {
            new Exception(ex.Message);
        }
    }

    private void GetDataAll(object sender, string ColumnType)
    {
        try
        {
            DropDownList ddlIVMS = (DropDownList)sender;

            int ColumnIndex = int.Parse(ddlIVMS.ID.Replace("ddlIVMS_Col", string.Empty));
            int SelectedIndex = ddlIVMS.SelectedIndex;

            bool isDescrese = true;
            for (int i = 0; i < dtIVMSTemp.Rows.Count; i++)
            {
                if (string.Equals(dtIVMSTemp.Rows[i]["COLUMN_INDEX"].ToString(), ColumnIndex)
                 && string.Equals(dtIVMSTemp.Rows[i]["SELECTED_INDEX"].ToString(), SelectedIndex))
                {
                    isDescrese = false;
                    break;
                }
            }

            if (dtIVMSTemp.Rows.Count > 0)
            {
                if (isDescrese)
                {
                    DataRow[] rows;
                    rows = dtIVMSTemp.Select("COLUMN_INDEX = '" + ColumnIndex + "'");
                    foreach (DataRow row in rows)
                        dtIVMSTemp.Rows.Remove(row);
                }
            }

            if (isDescrese)
            {
                dtIVMSTemp.Rows.Add(ColumnIndex, SelectedIndex);
                //alertSuccess(ColumnIndex + "," + SelectedIndex);
                string Data = string.Empty;
                if (string.Equals(ColumnType, "A"))
                    this.GetDataA(SelectedIndex, ColumnIndex);
                if (string.Equals(ColumnType, "B"))
                    this.GetDataB(SelectedIndex, ColumnIndex);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void ddlIVMS_A_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.GetDataAll(sender, "A");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    void ddlIVMS_B_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.GetDataAll(sender, "B");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetDataA(int SelectedIndex, int ColumnIndex)
    {
        try
        {
            TextBox txt = new TextBox();
            DataTable dtIVMS = new DataTable();

            if (SelectedIndex == 1)
            {//Get data from IVMS (ขับรถเร็ว)
                //OVERSPEED_TR + OVERSPEED_SM
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetTotalIVMSBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                            {
                                txt.Text = dtIVMS.Rows[0]["CAR_FAST"].ToString();
                                if (string.Equals(txt.Text.Trim(), string.Empty))
                                    txt.Text = "0";

                                dtIVMS_A.Rows[SelectedIndex]["DataDate"] = dtIVMS.Rows[0]["REPORT_DATE_DISPLAY"].ToString();
                            }
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
            }
            else if (SelectedIndex == 2)
            {//Get data from IVMS (ไม่รูดบัตรและขับเกินเวลา)
                //UNREGIST_CARD + DRIVE_8HRS
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetTotalIVMSBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                            {
                                txt.Text = dtIVMS.Rows[0]["OVER_HOUR"].ToString();

                                if (string.Equals(txt.Text.Trim(), string.Empty))
                                    txt.Text = "0";

                                dtIVMS_A.Rows[SelectedIndex]["DataDate"] = dtIVMS.Rows[0]["REPORT_DATE_DISPLAY"].ToString();
                            }
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
                
            }
            else if (SelectedIndex == 3)
            {//Get data from IVMS (ขับประมาท)
                //SUDDEN_ACC + SUDDEN_BRAKE
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetTotalIVMSBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                            {
                                txt.Text = dtIVMS.Rows[0]["SUDDEN"].ToString();

                                if (string.Equals(txt.Text.Trim(), string.Empty))
                                    txt.Text = "0";

                                dtIVMS_A.Rows[SelectedIndex]["DataDate"] = dtIVMS.Rows[0]["REPORT_DATE_DISPLAY"].ToString();
                            }
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
            }
            else if (SelectedIndex == 4)
            {//Get data from TMS (อุบัติเหตุ)
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        string Tmp = KPIBLL.Instance.GetTotalAccidentBLL(ddlYear.SelectedValue
                            , int.Parse(ddlMonth.SelectedValue)
                            , int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString())
                            , dgvKPI.DataKeys[i]["SVENDORID"].ToString()
                            , string.Empty);

                        if (string.Equals(txt.Text.Trim(), string.Empty))
                        {
                            txt.Text = Tmp;

                            if (string.Equals(txt.Text.Trim(), string.Empty))
                                txt.Text = "0";
                        }
                    }
                }
            }
            else if (SelectedIndex == 5)
            {//Get data from Shipment Cost (Loss)
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetShipmentCostDataBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                            {
                                txt.Text = dtIVMS.Rows[0]["LOSS_QTY"].ToString().Replace("-", string.Empty);

                                if (string.Equals(txt.Text.Trim(), string.Empty))
                                    txt.Text = "0";
                            }
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
            }
            else if (SelectedIndex == 6)
            {//Default Value 0
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            txt.Text = "0";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetDataB(int SelectedIndex, int ColumnIndex)
    {
        try
        {
            TextBox txt = new TextBox();
            DataTable dtIVMS = new DataTable();

            if (SelectedIndex == 1)
            {//Get data from TMS (จำนวนรถ)
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        string Total = KPIBLL.Instance.GetTotalTruckBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dtKPI.Rows[i]["SCONTRACTID"].ToString()));
                        if (string.Equals(txt.Text.Trim(), string.Empty))
                            txt.Text = Total;
                    }
                }
            }
            else if (SelectedIndex == 2)
            {//Get data from Shipment Cost (ระยะทาง)
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetShipmentCostDataBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                                txt.Text = dtIVMS.Rows[0]["DISTANCE"].ToString();
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
            }
            else if (SelectedIndex == 3)
            {//Get data from Shipment Cost (Volumn)
                for (int i = 1; i < dgvKPI.Rows.Count; i++)
                {
                    txt = new TextBox();
                    txt = (TextBox)dgvKPI.Rows[i].FindControl("TextBox_" + i.ToString() + "_" + ColumnIndex);
                    if (txt != null)
                    {
                        dtIVMS = KPIBLL.Instance.GetShipmentCostDataBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), int.Parse(dgvKPI.DataKeys[i]["SCONTRACTID"].ToString()));
                        if (dtIVMS.Rows.Count > 0)
                        {
                            if (string.Equals(txt.Text.Trim(), string.Empty))
                                txt.Text = dtIVMS.Rows[0]["DELIVERY_QTY"].ToString();
                        }
                        //else
                        //    txt.Text = string.Empty;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave(true);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSave(bool IsSave)
    {
        try
        {
            TextBox txtA, txtB;
            int dtMappingRow = 0;

            for (int i = 1; i < dgvKPI.Rows.Count; i++)
            {
                dtMappingRow = 0;
                      
                for (int j = 4; j < dgvKPI.Columns.Count; j = j + 2)
                {   
                    txtA = new TextBox();
                    txtB = new TextBox();

                    if (dgvKPI.Rows[i].Cells[j].FindControl("TextBox_" + i.ToString() + "_" + j.ToString()) != null)
                        txtA = (TextBox)dgvKPI.Rows[i].Cells[j].FindControl("TextBox_" + i.ToString() + "_" + j.ToString());
                    else
                        txtA = null;

                    if (dgvKPI.Rows[i].Cells[j + 1].FindControl("TextBox_" + i.ToString() + "_" + (j + 1).ToString()) != null)
                        txtB = (TextBox)dgvKPI.Rows[i].Cells[j + 1].FindControl("TextBox_" + i.ToString() + "_" + (j + 1).ToString());
                    else
                        txtB = null;

                    if (txtA != null)
                    {
                        if (!this.IsDecimal(txtA.Text.Trim()))
                            throw new Exception("เลขที่สัญญา : " + dtKPI.Rows[i]["SCONTRACTNO"].ToString() + "<br />" +
                                                "ชื่อหัวข้อประเมิน : " + dgvKPI.Columns[j].HeaderText + "<br />" +
                                                dtKPI.Rows[0][j].ToString() + " จะต้องป้อนค่าเป็นตัวเลขเท่านั้น");
                    }

                    if (txtB != null)
                    {
                        if (!this.IsDecimal(txtB.Text.Trim()))
                            throw new Exception("เลขที่สัญญา : " + dtKPI.Rows[i]["SCONTRACTNO"].ToString() + "<br />" +
                                                "ชื่อหัวข้อประเมิน : " + dgvKPI.Columns[j].HeaderText + "<br />" +
                                                dtKPI.Rows[0][j + 1].ToString() + " จะต้องป้อนค่าเป็นตัวเลขเท่านั้น");
                    }

                    if (txtA != null && txtB != null)
                    {
                        if ((string.Equals(txtA.Text.Trim(), string.Empty) && !string.Equals(txtB.Text.Trim(), string.Empty))
                            || (!string.Equals(txtA.Text.Trim(), string.Empty) && string.Equals(txtB.Text.Trim(), string.Empty)))
                            throw new Exception("เลขที่สัญญา : " + dtKPI.Rows[i]["SCONTRACTNO"].ToString() + "<br />" +
                                                "ชื่อหัวข้อประเมิน : " + dgvKPI.Columns[j].HeaderText + "<br />" +
                                                "ในกรณีมีการใส่ค่า จะต้องใส่ค่า " + dtKPI.Rows[0][j].ToString() + " และ " + dtKPI.Rows[0][j + 1].ToString());
                    }

                    if (txtA != null)
                        dtKPI.Rows[i][j] = txtA.Text.Trim();

                    if (txtB != null)
                        dtKPI.Rows[i][j + 1] = txtB.Text.Trim();
                    else if (txtA != null && string.Equals(txtA.Text.Trim(), string.Empty))
                        dtKPI.Rows[i][j + 1] = string.Empty;

                    string TextB = string.Empty;
                    if (txtB == null)
                        TextB = dgvKPI.Rows[i].Cells[j + 1].Text;
                    else
                        TextB = txtB.Text.Trim();

                    if (txtA != null && !string.Equals(txtA.Text.Trim(), string.Empty) && !string.Equals(TextB, string.Empty))
                    {
                        //txtA.Text = Math.Round(decimal.Parse(txtA.Text.Trim()), 3).ToString();
                        this.Calculate(txtA, TextB, dtKPI.Rows[i]["SVENDORID"].ToString(), int.Parse(dtKPI.Rows[i]["SCONTRACTID"].ToString()), dtMappingRow, dgvKPI.Rows[i].Cells[3].Text, dgvKPI.Columns[j].HeaderText);
                    }

                    dtMappingRow++;
                }
            }

            string StatusID = "2";
            bool IsFound = false;

            dtKPI.Columns.Add("STATUS_ID");

            for (int i = 1; i < dtKPI.Rows.Count; i++)
            {
                StatusID = "2";
                IsFound = false;

                for (int j = 4; j < dtKPI.Columns.Count - 1; j++)
                {
                    if (!string.Equals(dtKPI.Rows[i][j].ToString(), string.Empty))
                        IsFound = true;
                    else
                        StatusID = "1";
                }

                if (!IsFound)
                    StatusID = string.Empty;

                dtKPI.Rows[i]["STATUS_ID"] = StatusID;
            }

            if (IsSave)
            {
                KPIBLL.Instance.KPISaveTransactionBatchBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), TopicHeaderID, dtKPI, UserID);
                alertSuccess("บันทึกข้อมูลเรียบร้อย");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Calculate(TextBox txtA, string TextB, string VendorID, int ContractID, int Row, string ContractName, string TopicName)
    {
        try
        {
            string PointBefore = string.Empty;

            DataTable dtMapping = KPIBLL.Instance.KPIMappingFormSelectBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), VendorID, ContractID, string.Empty);

            if (dtMapping.Rows.Count == 0)
                throw new Exception("ยังไม่ได้มีการ Mapping ข้อมูลเดือน กับ ชุดคำถาม");

            int FormulaID = int.Parse(dtMapping.Rows[Row]["FORMULA_ID"].ToString());

            KPI_Helper Helper = new KPI_Helper();
            string Point = Helper.Calculate(FormulaID, decimal.Parse(txtA.Text.Trim()), decimal.Parse(TextB), ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), VendorID, ContractID, dtMapping, Row, ref PointBefore);
            if (string.Equals(Point, string.Empty))
                throw new Exception("เลขที่สัญญา " + ContractName + "<br />"
                                  + "ชือหัวข้อประเมิน " + TopicName + "<br />"
                                  + "ไม่สามารถคำนวณคะแนนได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private bool IsDecimal(string str)
    {
        try
        {
            if (string.Equals(str, string.Empty))
                return true;

            decimal tmp;
            if (decimal.TryParse(str, out tmp))
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdExportExcel_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave(false);

            DataTable dtExport = dtKPI.Copy();

            dtExport.Columns["NO"].ColumnName = "ลำดับ";
            dtExport.Columns["SCONTRACTNO"].ColumnName = "เลขที่สัญญา";

            for (int i = 4; i < dgvKPI.Columns.Count; i++)
            {
                if (i % 2 == 0)
                    dtExport.Columns[i].ColumnName = dgvKPI.Columns[i].HeaderText + "_A";
                else
                    dtExport.Columns[i].ColumnName = dgvKPI.Columns[i - 1].HeaderText + "_B";
            }

            dtExport.Columns.Remove("SCONTRACTID");
            dtExport.Columns.Remove("SVENDORID");

            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/KPIFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["KPI"];
            worksheet.InsertDataTable(dtExport, new InsertDataTableOptions(0, 0) { ColumnHeaders = true });

            string Path = this.CheckPath();
            string FileName = "KPI_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}