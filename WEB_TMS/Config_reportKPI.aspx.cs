﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TMS_BLL.Master;

public partial class Config_reportKPI : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            SetinitalForm();
            
        }
    }

    private void SetinitalForm()
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref ddlYear, SetYear(), "value", "text", true);
            
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    
    #region Set Year
    private DataTable SetYear()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("value");
            dt.Columns.Add("text");
            dt.Rows.Add(int.Parse(DateTime.Now.Year.ToString()) - 1, int.Parse(DateTime.Now.Year.ToString()) - 1);
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add(int.Parse(DateTime.Now.Year.ToString()) + i, int.Parse(DateTime.Now.Year.ToString()) + i);
            }
            return dt;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region Event
    #region Dropdown Change
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetRowData();
        panal_Gridview.Visible = true;
        CheckHaveDataSave(ddlYear.SelectedValue.ToString());
    }

    
    #endregion
    #region SaveConfigInsert
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = KPIBLL.Instance.GetSaveConfig(ddlYear.SelectedValue.ToString(),SetGridToDataTable());
            if (string.Equals(dt.Rows[0][0].ToString(), "00"))
            {
                alertSuccess("บันทึกข้อมูลสำเร็จ");
            }
            else
            {
                alertFail(dt.Rows[0][0].ToString());
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #region Gridview RowDataBound
    protected void dgvMonthKpi_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataTable kpi_report = GetKpiReport();
            // Find the drop down list by name
            DropDownList ddl_report = (DropDownList)e.Row.FindControl("ddl_report");
            DropDownListHelper.BindDropDownList(ref ddl_report, kpi_report, "val", "Text", true);


        }
    }
    #endregion
    #region Click Black
    protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("Kpi_lst.aspx");
    }
    #endregion
    

    
    #endregion
    #region ตรวจสอบว่าเคยบันทึกปีนี้ก่อนหรือเปล่า
    private void CheckHaveDataSave(string Yearchose)
    {
        try
        {
            DataTable dtConfigKpi = KPIBLL.Instance.GetSelectYearConfig(Yearchose);
            if (dtConfigKpi.Rows.Count>0)
            {
                for (int i = 0; i < dtConfigKpi.Rows.Count; i++)
                {
                    DropDownList ddl_report = (DropDownList)dgvMonthKpi.Rows[i].Cells[3].FindControl("ddl_report");
                    ddl_report.SelectedValue = dtConfigKpi.Rows[i]["KPI_EVA_HEAD_ID"].ToString();
                    ddl_report.Enabled = string.Equals(dtConfigKpi.Rows[i]["LOCKDATA"].ToString(), "1") ? false : true;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region SetinitalRow
    private void SetRowData()
    {
        try
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("MONTH", typeof(string)));
            string[] month = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
            dr = dt.NewRow();
            foreach (string item in month)
            {
                dr = dt.NewRow();
                dr["MONTH"] = item;
                dt.Rows.Add(dr);
            }

            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            dgvMonthKpi.DataSource = dt;
            dgvMonthKpi.DataBind();

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    private DataTable GetKpiReport()
    {
        try
        {
            return TMS_BLL.Master.KPIBLL.Instance.GetKpiReport(ddlYear.SelectedValue.ToString());
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    #endregion
    #region SetDataGridviewToDataTable
    private DataTable SetGridToDataTable()
    {
        try
        { 
            DataTable dt_ConfigKPI = new DataTable();
            dt_ConfigKPI.Columns.Add("kpi_eva_head_id");
            dt_ConfigKPI.Columns.Add("year");
            dt_ConfigKPI.Columns.Add("month");
            dt_ConfigKPI.Columns.Add("userId");
            if (dgvMonthKpi.Rows.Count > 0)
            {
                for (int i = 0; i <= dgvMonthKpi.Rows.Count-1; i++)
                {

                    //extract the TextBox values
                    HiddenField val_month = (HiddenField)dgvMonthKpi.Rows[i].Cells[2].FindControl("val_month");
                    DropDownList ddl_report = (DropDownList)dgvMonthKpi.Rows[i].Cells[3].FindControl("ddl_report");
                    dt_ConfigKPI.Rows.Add(
                        ddl_report.SelectedValue.ToString(),
                        ddlYear.SelectedValue.ToString(),
                        val_month.Value.ToString(),
                        Session["UserID"].ToString()
                        ); 
                }
                return dt_ConfigKPI;

            }

            return new DataTable();


        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion

    
}