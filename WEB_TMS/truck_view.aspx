﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="truck_view.aspx.cs"
    Inherits="truck_view" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphHeader">
</asp:Content>
<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="cph_Main">
    <table border="0" cellspacing="2" cellpadding="3" width="100%">
        <tr>
            <td align="left" style="padding-left: 25px;" width="70%">
                <dx:ASPxLabel ID="xlbHead" runat="Server" CssClass="highlight" Text="รายละเอียดข้อมูลรถ" />
            </td>
            <td align="right">
                <dx:ASPxButton ID="xbnChangeMode" runat="Server" ClientInstanceName="xbnChangeMode" CausesValidation="False"
                    Text="เปลี่ยนเป็นโหมดแก้ไข" OnClick="xbnChangeMode_Click" />
            </td>
            <td align="right" style="padding-right: 25px;">
                <dx:ASPxButton ID="xbnLastHis" runat="Server" ClientInstanceName="xbnLastHis" CausesValidation="False"
                    Text="ประวัติรถที่สมบูรณ์" />
            </td>
        </tr>
    </table>
    <br />
    <div id="dvHead" runat="Server">
        <dx:ASPxRoundPanel ID="rpnHInfomation" runat="Server" ClientInstanceName="rpnHInfomation" Width="980px"
            Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctHInfomation" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%" style="background-color: #f9d0b5;">
                                ทะเบียนรถ : </td>
                            <td align="left" width="25%" style="background-color: #f9d0b5;">
                                <dx:ASPxLabel ID="xlbHRegNo" runat="Server" ClientInstanceName="xlbHRegNo" />
                            </td>
                            <td align="right" width="25%">
                                วันที่จดทะเบียน : </td>
                            <td align="left" width="25%">
                                <dx:ASPxLabel ID="xlbdHRegNo" runat="Server" ClientInstanceName="xlbdHRegNo" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                            <td align="right">
                                อายุการใช้งาน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHnPeriod" runat="Server" ClientInstanceName="xlbHnPeriod" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                หมายเลขแชสซีย์ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHsChasis" runat="Server" ClientInstanceName="xlbHsChasis" />
                            </td>
                            <td align="right">
                                หมายเลขเครื่องยนต์ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHsEngine" runat="Server" ClientInstanceName="xlbHsEngine" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ยี่ห้อ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHsBrand" runat="Server" ClientInstanceName="xlbHsBrand" />
                            </td>
                            <td align="right">
                                รุ่น : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHsModel" runat="Server" ClientInstanceName="xlbHsModel" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนล้อ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHnWheel" runat="Server" ClientInstanceName="xlbHnWheel" />
                                        </td>
                                        <td>
                                            &nbsp;ล้อ</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                กำลังเครื่องยนต์ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHPowermover" runat="Server" ClientInstanceName="xlbHPowermover" />
                                        </td>
                                        <td>
                                            &nbsp;แรงม้า</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHnShaftDriven" runat="Server" ClientInstanceName="xlbHnShaftDriven" />
                            </td>
                            <td align="right">
                                ระบบกันสะเทือน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHsVibration" runat="Server" ClientInstanceName="xlbHsVibration" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ปั๊ม Power ที่ตัวรถ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHPumpPower" runat="Server" ClientInstanceName="xlbHPumpPower" />
                            </td>
                            <td align="right">
                                ชนิดของปั๊ม Power : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHPumpPower_type" runat="Server" ClientInstanceName="xlbHPumpPower_type" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                            <td align="right">
                                วัสดุที่สร้าง Pressure ของปั๊ม Power : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHMaterialOfPressure" runat="Server" ClientInstanceName="xlbHMaterialOfPressure" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ชนิดของวาล์วใต้ท้อง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHValveType" runat="Server" ClientInstanceName="xlbHValveType" />
                            </td>
                            <td align="right">
                                ประเภทของเชื้อเพลิง : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHFuelType" runat="Server" ClientInstanceName="xlbHFuelType" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                บริษัทที่ให้บริการระบบGPS : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHGPSProvider" runat="Server" ClientInstanceName="xlbHGPSProvider" />
                            </td>
                            <td align="right">
                                น้ำหนักรถ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHnWeight" runat="Server" ClientInstanceName="xlbHnWeight" />
                                        </td>
                                        <td>
                                            &nbsp;กก.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                <br />
                                รายละเอียดความจุ : </td>
                            <td align="left" colspan="3">
                                <br />
                                <dx:ASPxLabel ID="xlbHnCap" runat="Server" ClientInstanceName="xlbHnCap" />
                                <dx:ASPxGridView ID="gvwHnCap" runat="Server" ClientInstanceName="gvwHnCap" SkinID="_gvw" Width="65%"
                                    Style="margin-top: 0px" AutoGenerateColumns="False" Styles-Cell-BackColor="#EEEEEE">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="Order" Caption="ช่อง" Width="5%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="nPanL1" Caption="ความจุที่ระดับแป้น1 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="nPanL2" Caption="ความจุที่ระดับแป้น2 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="nPanL3" Caption="ความจุที่ระดับแป้น3 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsPager AlwaysShowPager="False" />
                                </dx:ASPxGridView>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนช่อง : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHnSlot" runat="Server" ClientInstanceName="xlbHnSlot" />
                                        </td>
                                        <td>
                                            &nbsp;ช่อง</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                ความจุรวม : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHnTatolCapacity" runat="Server" ClientInstanceName="xlbHnTatolCapacity" />
                                        </td>
                                        <td>
                                            &nbsp;ลิตร</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                วัสดุที่ใช้ทำตัวถังบรรทุก : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHTankMaterial" runat="Server" ClientInstanceName="xlbHTankMaterial" />
                            </td>
                            <td align="right">
                                วิธีการเติมน้ำมัน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHLoadMethod" runat="Server" ClientInstanceName="xlbHLoadMethod" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                บริษัทผู้ผลิตตัวถัง : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHTank_Maker" runat="Server" ClientInstanceName="xlbHTank_Maker" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                กลุ่มผลิตภัณฑ์ที่บรรทุก : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHProdGRP" runat="Server" ClientInstanceName="xlbHProdGRP" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                น้ำหนักผลิตภัณฑ์ที่บรรทุก : </td>
                            <td align="left" colspan="3">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHnLoadWeight" runat="Server" ClientInstanceName="xlbHnLoadWeight" />
                                        </td>
                                        <td>
                                            &nbsp;กก.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                น้ำหนักรถรวมผลิตภัณฑ์ : </td>
                            <td align="left" colspan="3">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHCalcWeight" runat="Server" ClientInstanceName="xlbHCalcWeight" />
                                        </td>
                                        <td>
                                            &nbsp;กก.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารใบจดทะเบียนรถ : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHDoc01" runat="Server" ClientInstanceName="xlbHDoc01" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบหลักฐานการเสียภาษี : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHDoc02" runat="Server" ClientInstanceName="xlbHDoc02" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสาร Compartment : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHDoc03" runat="Server" ClientInstanceName="xlbHDoc03" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารใบจดทะเบียนรถครั้งแรก : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbHDoc04" runat="Server" ClientInstanceName="xlbHDoc04" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnHStatute" runat="Server" ClientInstanceName="rpnHStatute" Width="980px" Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctHStatute" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                บริษัทประกันภัย : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbHSCompany" runat="Server" ClientInstanceName="xlbHSCompany" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ระยะเวลาอายุ พรบ. : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHSDuration" runat="Server" ClientInstanceName="xlbHDuration" Text="- ถึง -" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                รายละเอียดเพิ่มเติม : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHSDetail" runat="Server" ClientInstanceName="xlbHSDetail" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHSDoc" runat="Server" ClientInstanceName="xlbHSDoc" Text="-" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnHInsurance" runat="Server" ClientInstanceName="rpnHInsurance" Width="980px"
            Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctHInsurance" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                บริษัทประกันภัย : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbHICompany" runat="Server" ClientInstanceName="xlbHICompany" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ทุนประกันภัย : </td>
                            <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHIBudget" runat="Server" ClientInstanceName="xlbHIBudget" Text="-" />
                                        </td>
                                        <td>
                                            &nbsp;บาท</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ประเภทประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHIType" runat="Server" ClientInstanceName="xlbHIType" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ความคุ้มครอง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHIHolding" runat="Server" ClientInstanceName="xlbHIHolding" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                เบี้ยประกันต่อปี : </td>
                            <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbHInInsure" runat="Server" ClientInstanceName="xlbHInInsure" Text="-" />
                                        </td>
                                        <td>
                                            &nbsp;บาท</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                รายละเอียดเพิ่มเติม : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHIDetail" runat="Server" ClientInstanceName="xlbHIDetail" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbHIDoc" runat="Server" ClientInstanceName="xlbHIDoc" Text="-" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
    </div>
    <div id="dvTrail" runat="Server">
        <dx:ASPxRoundPanel ID="rpnTInfomation" runat="Server" ClientInstanceName="rpnTInfomation" Width="980px"
            Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctTInfomation" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                ทะเบียนรถ : </td>
                            <td align="left" width="25%">
                                <dx:ASPxLabel ID="xlbRRegNo" runat="Server" ClientInstanceName="xlbRRegNo" />
                            </td>
                            <td align="right" width="25%">
                                วันที่จดทะเบียน : </td>
                            <td align="left" width="25%">
                                <dx:ASPxLabel ID="xlbdRRegNo" runat="Server" ClientInstanceName="xlbdRRegNo" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                            <td align="right">
                                อายุการใช้งาน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRnPeriod" runat="Server" ClientInstanceName="xlbRnPeriod" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                หมายเลขแชสซีย์ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRsChasis" runat="Server" ClientInstanceName="xlbRsChasis" />
                            </td>
                            <td align="right">
                                หมายเลขเครื่องยนต์ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRsEngine" runat="Server" ClientInstanceName="xlbRsEngine" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ยี่ห้อ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRsBrand" runat="Server" ClientInstanceName="xlbRsBrand" />
                            </td>
                            <td align="right">
                                รุ่น : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRsModel" runat="Server" ClientInstanceName="xlbRsModel" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนล้อ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRnWheel" runat="Server" ClientInstanceName="xlbRnWheel" />
                                        </td>
                                        <td>
                                            &nbsp;ล้อ</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                กำลังเครื่องยนต์ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRPowermover" runat="Server" ClientInstanceName="xlbRPowermover" />
                                        </td>
                                        <td>
                                            &nbsp;แรงม้า</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRnShaftDriven" runat="Server" ClientInstanceName="xlbRnShaftDriven" />
                            </td>
                            <td align="right">
                                ระบบกันสะเทือน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRsVibration" runat="Server" ClientInstanceName="xlbRsVibration" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ปั๊ม Power ที่ตัวรถ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRPumpPower" runat="Server" ClientInstanceName="xlbRPumpPower" />
                            </td>
                            <td align="right">
                                ชนิดของปั๊ม Power : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRPumpPower_type" runat="Server" ClientInstanceName="xlbRPumpPower_type" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                            <td align="right">
                                วัสดุที่สร้าง Pressure ของปั๊ม Power : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRMaterialOfPressure" runat="Server" ClientInstanceName="xlbRMaterialOfPressure" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ชนิดของวาล์วใต้ท้อง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRValveType" runat="Server" ClientInstanceName="xlbRValveType" />
                            </td>
                            <td align="right">
                                ประเภทของเชื้อเพลิง : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRFuelType" runat="Server" ClientInstanceName="xlbRFuelType" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                บริษัทที่ให้บริการระบบGPS : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRGPSProvider" runat="Server" ClientInstanceName="xlbRGPSProvider" />
                            </td>
                            <td align="right">
                                น้ำหนักรถ : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRnWeight" runat="Server" ClientInstanceName="xlbRnWeight" />
                                        </td>
                                        <td>
                                            &nbsp;กก.</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                <br />
                                รายละเอียดความจุ : </td>
                            <td align="left" colspan="3">
                                <br />
                                <dx:ASPxLabel ID="xlbRnCap" runat="Server" ClientInstanceName="xlbHnCap" />
                                <dx:ASPxGridView ID="gvwRnCap" runat="Server" ClientInstanceName="gvwRnCap" SkinID="_gvw" Width="65%"
                                    Style="margin-top: 0px" AutoGenerateColumns="False" Styles-Cell-BackColor="#EEEEEE">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="ช่อง" Width="5%">
                                            <DataItemTemplate>
                                                <dx:ASPxListBox ID="xlbOrder" runat="Server" ClientInstanceName="xlbOrder" />
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ความจุที่ระดับแป้น1 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ความจุที่ระดับแป้น2 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ความจุที่ระดับแป้น3 (ลิตร)" Width="20%">
                                            <PropertiesTextEdit DisplayFormatString="N0" />
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsPager AlwaysShowPager="False" />
                                </dx:ASPxGridView>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                จำนวนช่อง : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRnSlot" runat="Server" ClientInstanceName="xlbRnSlot" />
                                        </td>
                                        <td>
                                            &nbsp;ช่อง</td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right">
                                ความจุรวม : </td>
                            <td align="left">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRnTatolCapacity" runat="Server" ClientInstanceName="xlbRnTatolCapacity" />
                                        </td>
                                        <td>
                                            &nbsp;ลิตร</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                วัสดุที่ใช้ทำตัวถังบรรทุก : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRTankMaterial" runat="Server" ClientInstanceName="xlbRTankMaterial" />
                            </td>
                            <td align="right">
                                วิธีการเติมน้ำมัน : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRLoadMethod" runat="Server" ClientInstanceName="xlbRLoadMethod" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                บริษัทผู้ผลิตตัวถัง : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRTank_Maker" runat="Server" ClientInstanceName="xlbRTank_Maker" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                กลุ่มผลิตภัณฑ์ที่บรรทุก : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRProdGRP" runat="Server" ClientInstanceName="xlbRProdGRP" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                น้ำหนักผลิตภัณฑ์ที่บรรทุก : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRnLoadWeight" runat="Server" ClientInstanceName="xlbRnLoadWeight" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                น้ำหนักรถรวมผลิตภัณฑ์ : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRCalcWeight" runat="Server" ClientInstanceName="xlbRCalcWeight" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารใบจดทะเบียนรถ : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRDoc01" runat="Server" ClientInstanceName="xlbRDoc01" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบหลักฐานการเสียภาษี : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRDoc02" runat="Server" ClientInstanceName="xlbRDoc02" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสาร Compartment : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRDoc03" runat="Server" ClientInstanceName="xlbRDoc03" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารใบจดทะเบียนรถครั้งแรก : </td>
                            <td align="left" colspan="3">
                                <dx:ASPxLabel ID="xlbRDoc04" runat="Server" ClientInstanceName="xlbRDoc04" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnTStatute" runat="Server" ClientInstanceName="rpnTStatute" Width="980px" Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctTStatute" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                บริษัทประกันภัย : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbRSCompany" runat="Server" ClientInstanceName="xlbRSCompany" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ระยะเวลาอายุ พรบ. : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRSDuration" runat="Server" ClientInstanceName="xlbRSDuration" Text="- ถึง -" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                รายละเอียดเพิ่มเติม : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRSDetail" runat="Server" ClientInstanceName="xlbRSDetail" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRSDoc" runat="Server" ClientInstanceName="xlbRSDoc" Text="-" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnTInsurance" runat="Server" ClientInstanceName="rpnTInsurance" Width="980px"
            Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctTInsurance" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                บริษัทประกันภัย : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbRICompany" runat="Server" ClientInstanceName="xlbRICompany" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ทุนประกันภัย : </td>
                            <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRIBudget" runat="Server" ClientInstanceName="xlbRIBudget" Text="-" />
                                        </td>
                                        <td>
                                            &nbsp;บาท</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ประเภทประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRIType" runat="Server" ClientInstanceName="xlbRIType" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ความคุ้มครอง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRIHolding" runat="Server" ClientInstanceName="xlbRIHolding" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                เบี้ยประกันต่อปี : </td>
                            <td align="left">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ID="xlbRInInsure" runat="Server" ClientInstanceName="xlbRInInsure" Text="-" />
                                        </td>
                                        <td>
                                            &nbsp;บาท</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                รายละเอียดเพิ่มเติม : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRIDetail" runat="Server" ClientInstanceName="xlbRIDetail" Text="-" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                แนบเอกสารประกันภัย : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbRIDoc" runat="Server" ClientInstanceName="xlbRIDoc" Text="-" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
    </div>
    <div id="dvOther" runat="Server">
        <dx:ASPxRoundPanel ID="rpnWater" runat="Server" ClientInstanceName="rpnWater" HeaderText="ข้อมูลวัดน้ำ"
            Width="980px" Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctWater" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td align="right">
                                <table border="0">
                                    <tr>
                                        <td>
                                            <dx:ASPxButton ID="xbnAddWater" runat="server" ClientInstanceName="xbnAddWater" Text="เพิ่มประวัติวัดน้ำ">
                                            </dx:ASPxButton>
                                        </td>
                                        <td>
                                            <dx:ASPxButton ID="xbnViewWater" runat="server" ClientInstanceName="xbnViewWater" Text="ประวัติวัดน้ำ">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" width="25%">
                                สถานที่วัดน้ำ : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbPlaceMWater" runat="Server" ClientInstanceName="xlbPlaceMWater" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                รหัสวัดน้ำ/เลขที่เอกสารอ้างอิง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbsCar_Num" runat="Server" ClientInstanceName="xlbsCar_Num" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                วันหมดอายุวัดน้ำ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbdWaterExpire" runat="Server" ClientInstanceName="xlbdWaterExpire" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                เอกสารวัดน้ำ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbMWaterDoc" runat="Server" ClientInstanceName="xlbMWaterDoc" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnHolding" runat="Server" ClientInstanceName="rpnHolding" HeaderText="ข้อมูลการใช้งานและถือครอง"
            Width="980px" Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctHolding" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                เลขที่สัญญา : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbsContractNo" runat="Server" ClientInstanceName="xlbsContractNo" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                ผู้ขนส่ง : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbsTransport" runat="Server" ClientInstanceName="xlbsTransport" />
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
        <br />
        <dx:ASPxRoundPanel ID="rpnPermit" runat="Server" ClientInstanceName="rpnPermit" HeaderText="การอนุญาตรับงาน"
            Width="980px" Content-BackColor="#EEEEEE">
            <PanelCollection>
                <dx:PanelContent ID="pctPermit" runat="Server">
                    <table border="0" cellspacing="2" cellpadding="3" width="100%">
                        <tr>
                            <td align="right" width="25%">
                                การอนุญาตรับงาน : </td>
                            <td align="left" width="75%">
                                <dx:ASPxLabel ID="xlbPermit" runat="server" ClientInstanceName="xlbPermit" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                วันที่ปลดระงับ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbdBlackListExp" runat="server" ClientInstanceName="xlbdBlackListExp" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                สาเหตุการระงับ : </td>
                            <td align="left">
                                <dx:ASPxLabel ID="xlbBlackListCause" runat="server" ClientInstanceName="xlbBlackListCause" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                ประวัติการระงับใช้ : </td>
                            <td align="left">
                                <dx:ASPxGridView ID="gvwBlacklist" runat="Server" ClientInstanceName="gvwBlacklist" SkinID="_gvw" Width="60%"
                                    Style="margin-top: 0px" AutoGenerateColumns="False" Styles-Cell-BackColor="#EEEEEE">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="REMARK" Caption="หมายเหตุ" Width="20%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SDATE" Caption="วันที่ - เวลา" Width="20%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SBLACKLIST" Caption="ผู้บันทึก" Width="20%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsPager AlwaysShowPager="False" />
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
    </div>
</asp:Content>
