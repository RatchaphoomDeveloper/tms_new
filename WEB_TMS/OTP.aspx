﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="OTP.aspx.cs" Inherits="OTP" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td style="width: 100px">
                        Code Type :
                    </td>
                    <td style="width: 210px">
                        <asp:RadioButtonList ID="radCodeType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True" Value="1" Text="เรื่องร้องเรียน"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        OTP :
                    </td>
                    <td style="width: 210px">
                        <asp:TextBox ID="txtOTP" runat="server" Width="200px" Style="text-align: center"
                            CssClass="form-control" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="cmdGenerate" runat="server" Width="80px" Text="Generate" CssClass="btn btn-md btn-hover btn-info"
                            OnClick="cmdGenerate_Click" />
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="cmdSaveOTP" runat="server" Width="80px" Text="Save" CssClass="btn btn-md btn-hover btn-info"
                            OnClick="cmdSaveOTP_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
