﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Transaction.Accident;

public partial class AccidentType : PageBase
{
    DataTable dt = new DataTable();
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AddDataTable();
        }
    }
    #endregion

    #region gvAccidentType_RowDataBound
    protected void gvAccidentType_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            TextBox txt = (TextBox)e.Row.FindControl("txtAccidentTypeName");
            if (txt != null)
            {
                txt.Text = dr["NAME"] + string.Empty;
            }

            RadioButtonList rbl = (RadioButtonList)e.Row.FindControl("rblCactive");
            if (rbl != null)
            {
                rbl.SelectedValue = dr["CACTIVE"] + string.Empty;
            }
            HiddenField hid = (HiddenField)e.Row.FindControl("hidID");
            if (hid != null)
            {
                hid.Value = dr["ID"] + string.Empty;
            }
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        dt = AccidentBLL.Instance.AccidentTypeSelect(txtAccidentType.Text, rblCactive.SelectedValue);
        DataRow dr = dt.NewRow();
        dr["ID"] = 0;
        dr["NAME"] = "";
        dr["CACTIVE"] = "1";
        dt.Rows.InsertAt(dr, 0);

        gvAccidentType.DataSource = dt;
        gvAccidentType.DataBind();
    }
    #endregion

    #region AddDataTable
    private void AddDataTable()
    {

        dt.Columns.Add("ID", typeof(int));
        dt.Columns.Add("NAME", typeof(string));
        dt.Columns.Add("CACTIVE", typeof(string));
        dt.Rows.Add(0, "", "1");

        gvAccidentType.DataSource = dt;
        gvAccidentType.DataBind();
    }
    #endregion

    #region mpConfirmSave_ClickOK
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int index = int.Parse(btn.CommandArgument);
        GridViewRow gvr = gvAccidentType.Rows[index];
        int? ID = 0;
        string AccidentTypeName = string.Empty, CACTIVE = "1";
        TextBox txt = (TextBox)gvr.FindControl("txtAccidentTypeName");
        if (txt != null)
        {
            AccidentTypeName = txt.Text.Trim();
        }
        RadioButtonList rbl = (RadioButtonList)gvr.FindControl("rblCactive");
        if (rbl != null)
        {
            CACTIVE = rbl.SelectedValue;
        }
        HiddenField hid = (HiddenField)gvr.FindControl("hidID");
        if (hid != null)
        {
            ID = int.Parse(hid.Value);
        }
        bool isRes = false;
        try
        {
            if (AccidentBLL.Instance.AccidentTypeCheckValidate(ID.Value, AccidentTypeName))
            {
                alertFail("ขั้นตอนการขนส่งขณะเกิดอุบัติเหตุ " + AccidentTypeName + " มีอยู่แล้วในระบบ");

            }
            else
            {
                if (ID == 0)
                {
                    isRes = AccidentBLL.Instance.AccidentTypeInsert(AccidentTypeName, CACTIVE, Session["UserID"] + string.Empty);
                }
                else
                {
                    isRes = AccidentBLL.Instance.AccidentTypeUpdate(ID.Value, AccidentTypeName, CACTIVE, Session["UserID"] + string.Empty);
                }
                if (isRes)
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                    btnSearch_Click(null, null);
                }
                else
                {
                    alertFail("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>");
                }
            }

        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters("ไม่สารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล<br/>" + ex.Message));
        }

    }
    #endregion

}