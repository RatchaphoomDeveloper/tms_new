﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="doc_set.aspx.cs" Inherits="doc_set" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <table width="100%" border="0" cellpadding="5" cellspacing="1">
                    <tr>
                        <td align="right">
                            <dx:ASPxTextBox runat="server" ID="txtChkstate" ClientInstanceName="txtChkstate"
                                Width="35%" CssClass="dxeLineBreakFix" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox runat="server" ID="txtSearch" Width="35%" CssClass="dxeLineBreakFix"
                                NullText="ชื่อเอกสาร">
                            </dx:ASPxTextBox>
                            <dx:ASPxButton runat="server" ID="btnSerach" AutoPostBack="false" SkinID="_search"
                                CssClass="dxeLineBreakFix">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('search');}" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td height="30" align="left"><img src="images/Bullhorn24.png" width="24" height="24"
                        alt="" /> 1.ตรวจวัดน้ำ, 2.ตรวจสอบรับรองความถูกต้องและตีซีลใหม่ของรถน้ำมัน, 3.ขอสำเนาเอกสาร
                            การตรวจสอบปริมาตรถังน้ำมัน, 4.ขอพ่นสาระสำคัญ </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxGridView runat="server" ID="gvw" Width="100%" KeyFieldName="DOCTYPE_ID">
                                <Columns>
                                    <dx:GridViewDataColumn Caption="ที่" Width="5%">
                                        <DataItemTemplate>
                                            <%#Container.ItemIndex + 1 %>.</DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ชื่อเอกสาร" Width="50%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel runat="server" ID="lblDocName" Text='<%# Eval("DOC_DESCRIPTION") %>'>
                                            </dx:ASPxLabel>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewBandColumn Caption="ใช้กับประเภทคำขอ">
                                        <Columns>
                                            <dx:GridViewDataColumn Caption="1" Width="9%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="img1" ImageUrl='<%# Eval("DOC_01").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="2" Width="9%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="img2" ImageUrl='<%# Eval("DOC_02").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="3" Width="9%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="img3" ImageUrl='<%# Eval("DOC_03").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                            <dx:GridViewDataColumn Caption="4" Width="9%">
                                                <DataItemTemplate>
                                                    <dx:ASPxImage runat="server" ID="img4" ImageUrl='<%# Eval("DOC_04").ToString() == "Y" ? "images/action_check.png": "images/action_delete.png" %>'>
                                                    </dx:ASPxImage>
                                                </DataItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataColumn Width="5%" Caption="data1">
                                        <HeaderTemplate>  
                                              <table width="100%" cellpadding="3" cellspacing="1">                                       
                                                <tr>
                                                    <td> <dx:ASPxButton runat="server" AutoPostBack="false" ID="btnAdd" SkinID="_add" Enabled='<%# (CanWrite) ? true : false %>'>
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('add');}" />
                                            </dx:ASPxButton>   
                                                         </td>
                                             </tr>
                                         </table>                                           
                                        </HeaderTemplate>
                                        <DataItemTemplate>
                                             <table width="100%" cellpadding="3" cellspacing="1">                                       
                                                <tr>
                                                    <td>
                                            <dx:ASPxButton runat="server" AutoPostBack="false" ID="btnEdit" SkinID="_edit" Enabled='<%# Eval("MASTER_FLAG").ToString() == "Y" ? false: true %>'>
                                                <ClientSideEvents Click="function (s, e)  {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));  }" />
                                            </dx:ASPxButton>
                                                </td>
                                             </tr>
                                         </table>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn FieldName="DOCTYPE_ID" Visible="false">
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <Templates>
                                    <EditForm>
                                        <table width="80%" border="0" cellpadding="5" cellspacing="2">
                                            <tr>
                                                <td width="25%" bgcolor="#E3F7F8">ชื่อเอกสาร</td>
                                                <td width="75%" align="left">
                                                    <dx:ASPxTextBox runat="server" ID="txtDocName" Width="50%" Text='<%# Eval("DOC_DESCRIPTION") %>'>
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="ระบุชื่อ"
                                                            RequiredField-IsRequired="true" ValidationGroup="add">
                                                        </ValidationSettings>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%" bgcolor="#E3F7F8">ใช้กับประเภทคำขอ</td>
                                                <td width="75%" align="left">
                                                    <dx:ASPxCheckBox runat="server" ID="chk1" Text="ตรวจวัดน้ำ" Checked='<%# Eval("DOC_01") != null ? (Eval("DOC_01").ToString() == "Y" ? true: false) : false %>'>
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblReq1" SkinID="rblStatus" SelectedIndex='<%# Eval("CATTACH_DOC01") != null ? (Eval("CATTACH_DOC01").ToString() == "Y" ? 0: 1) : 0 %>'
                                                        Paddings-PaddingLeft="17px">
                                                        <Items>
                                                            <dx:ListEditItem Text="บังคับแนบเอกสาร" Value="Y" Selected="true" />
                                                            <dx:ListEditItem Text="ไม่บังคับแนบเอกสาร" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                    <dx:ASPxCheckBox runat="server" ID="chk2" Text="ตรวจสอบรับรองความถูกต้อง และตีซีลใหม่ของรถน้ำมัน"
                                                        Checked='<%# Eval("DOC_02") != null ? (Eval("DOC_02").ToString() == "Y" ? true: false) : false %>'>
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblReq2" SkinID="rblStatus" SelectedIndex='<%# Eval("CATTACH_DOC02") != null ? (Eval("CATTACH_DOC02").ToString() == "Y" ? 0: 1) : 0 %>'
                                                        Paddings-PaddingLeft="17px">
                                                        <Items>
                                                            <dx:ListEditItem Text="บังคับแนบเอกสาร" Value="Y" Selected="true" />
                                                            <dx:ListEditItem Text="ไม่บังคับแนบเอกสาร" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                    <dx:ASPxCheckBox runat="server" ID="chk3" Text="ขอสำเนาเอกสาร การตรวจสอบปริมาตรถังน้ำมัน"
                                                        Checked='<%# Eval("DOC_03") != null ? (Eval("DOC_03").ToString() == "Y" ? true: false) : false %>'>
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblReq3" SkinID="rblStatus" SelectedIndex='<%# Eval("CATTACH_DOC03") != null ? (Eval("CATTACH_DOC03").ToString() == "Y" ? 0: 1) : 0 %>'
                                                        Paddings-PaddingLeft="17px">
                                                        <Items>
                                                            <dx:ListEditItem Text="บังคับแนบเอกสาร" Value="Y" Selected="true" />
                                                            <dx:ListEditItem Text="ไม่บังคับแนบเอกสาร" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                    <dx:ASPxCheckBox runat="server" ID="chk4" Text="ขอพ่นสาระสำคัญ" Checked='<%# Eval("DOC_04") != null ? (Eval("DOC_04").ToString() == "Y" ? true: false) : false %>'>
                                                    </dx:ASPxCheckBox>
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblReq4" SkinID="rblStatus" SelectedIndex='<%# Eval("CATTACH_DOC04") != null ? (Eval("CATTACH_DOC04").ToString() == "Y" ? 0: 1) : 0 %>'
                                                        Paddings-PaddingLeft="17px">
                                                        <Items>
                                                            <dx:ListEditItem Text="บังคับแนบเอกสาร" Value="Y" Selected="true" />
                                                            <dx:ListEditItem Text="ไม่บังคับแนบเอกสาร" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td bgcolor="#E3F7F8">จำนวนเอกสาร</td>
                                                <td>
                                                    <dx:ASPxCheckBox runat="server" ID="chkdynamic" Text="เอกาสารสามารถเพิ่มได้มากกว่า 1"
                                                        CssClass="dxeLineBreakFix" Checked='<%# Eval("CDYNAMIC") != null ? (Eval("CDYNAMIC").ToString() == "Y" ? true: false) : false %>'>
                                                    </dx:ASPxCheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#E3F7F8">สถานะใช้งาน</td>
                                                <td align="left">
                                                    <dx:ASPxRadioButtonList runat="server" ID="rblActive" RepeatDirection="Horizontal"
                                                        SkinID="rblStatus" CssClass="dxeLineBreakFix" SelectedIndex='<%# Eval("ISACTIVE_FLAG") != null ? (Eval("ISACTIVE_FLAG").ToString() == "Y" ? 0: 1) : 0 %>'>
                                                        <Items>
                                                            <dx:ListEditItem Text="ใช้งาน" Value="Y" Selected="true" />
                                                            <dx:ListEditItem Text="ยกเลิก" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                    <dx:ASPxTextBox runat="server" ID="txtDescription" CssClass="dxeLineBreakFix" Width="50%"
                                                        Text='<%# Eval("DESCRIPTION") %>'>
                                                    </dx:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#E3F7F8">เอกสารของรถ</td>
                                                <td align="left">
                                                    <dx:ASPxCheckBoxList runat="server" ID="Chk_CarType" Border-BorderWidth="0" RepeatDirection="Horizontal"
                                                        DataSourceID="sds" TextField="CARCATE_NAME" ValueField="CARCATE_ID">
                                                        <Border BorderWidth="0px"></Border>
                                                    </dx:ASPxCheckBoxList>
                                                    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                        ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                        SelectCommand="SELECT &quot;CARCATE_ID&quot;, &quot;CARCATE_NAME&quot; FROM &quot;TBL_CARCATE&quot; WHERE (&quot;ISACTIVE_FLAG&quot; = :ISACTIVE_FLAG) ORDER BY &quot;ORDER_CAR&quot;">
                                                        <SelectParameters>
                                                            <asp:Parameter DefaultValue="Y" Name="ISACTIVE_FLAG" Type="String" />
                                                        </SelectParameters>
                                                    </asp:SqlDataSource>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2">
                                                    <dx:ASPxButton ID="btnSave" runat="server" ClientInstanceName="btnSave" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                        Text="บันทึก">
                                                        <ClientSideEvents Click="function (s, e) { if(!ASPxClientEdit.ValidateGroup('add')) return false; xcpn.PerformCallback('save;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); txtChkstate.SetText('save');}" />
                                                    </dx:ASPxButton>
                                                    <dx:ASPxButton runat="server" ID="btnCancel" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                        Text="ปิด">
                                                        <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('cancel'); }" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </EditForm>
                                </Templates>
                                <SettingsEditing NewItemRowPosition="Bottom" />
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
