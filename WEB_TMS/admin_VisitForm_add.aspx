﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="admin_VisitForm_add.aspx.cs" Inherits="admin_VisitForm_add_" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabtest').tab();
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <ul class="nav nav-tabs" runat="server" id="tabtest" clientidmode="Static">
        <li class="active" id="Tab1" runat="server" clientidmode="Static"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab" clientidmode="Static">ข้อมูลแบบฟอร์มการทำงาน</a></li>
        <li class="" id="Tab2" runat="server" clientidmode="Static"><a href="#TabGroup" data-toggle="tab" aria-expanded="false" runat="server" id="ProcessTab" clientidmode="Static">ข้อมูลหมวด</a></li>
        <li class="" id="Tab3" runat="server" clientidmode="Static"><a href="#TabItem" data-toggle="tab" aria-expanded="false" runat="server" id="FinalTab" clientidmode="Static">ข้อมูลคำถาม</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane active" id="TabGeneral" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="uplMain" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
                </Triggers>
                <ContentTemplate>
                    <div class="panel panel-info" style="margin-top: 20px">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลแบบฟอร์มการทำงาน
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="txtSearchFormName" class="col-md-2 control-label">ชื่อแบบฟอร์ม</label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtFormName" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group form-horizontal row">
                                        <label for="rdoStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                        <div class="col-md-3 radio">
                                            <asp:RadioButtonList ID="rdoStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                                <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <label for="rdoShowInList" class="col-md-2 control-label">แสดงในหน้าแรก</label>
                                        <div class="col-md-3 radio">
                                            <asp:CheckBox ID="chkShowInList" runat="server" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ระดับคะแนน
                        </div>
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <div class="form-group form-horizontal row">
                                    <label for="rdoOptionStatus" class="col-md-2 control-label">สถานะการใช้งาน</label>
                                    <div class="col-md-3 radio">
                                        <asp:RadioButtonList ID="rdoOptionStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                            <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                            <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                                <div class="form-group form-horizontal row">
                                    <label for="ddlRdo" class="col-md-2 control-label" clientidmode="Static">ระดับคะแนน</label>
                                    <div class="col-md-3">
                                        <asp:DropDownList ID="ddlRdo" runat="server" CssClass="form-control marginTp" ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </div>
                                    <label for="txtRdoPoint" class="col-md-2 control-label">คะแนน</label>
                                    <div class="col-md-2">
                                        <asp:TextBox ID="txtRdoPoint" runat="server" ClientIDMode="Static" CssClass="form-control input-md" MaxLength="7" autocomplete="off"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:Button ID="btnAdd" runat="server" Text="เพิ่มรายการ" CssClass="btn btn-hover btn-info" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnClear" runat="server" Text="เคลียร์" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnRowCreated="grvMain_RowCreated"
                                HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="NTYPEVISITLISTID,CACTIVE,NTYPEVISITFORMID,CONFIG_VALUE,NTYPEVISITLISTSCORE"
                                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ระดับ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="txtSTYPEVISITLISTNAME" runat="server" Text='<%# Eval("STYPEVISITLISTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="คะแนน" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="txtNTYPEVISITLISTSCORE" runat="server" Text='<%# Eval("NTYPEVISITLISTSCORE", "{0:n3}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แก้ไขข้อมูล" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif"
                                                Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="btnAddForm" runat="server" Text="บันทึกแบบฟอร์ม" CssClass="btn btn-hover btn-info" OnClick="btnAddForm_Click" />
                        <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="TabGroup" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="uplGroup" runat="server" UpdateMode="Always">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnGroupClose" />
                    <asp:AsyncPostBackTrigger ControlID="btnGroupAdd" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลหมวด
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="txtID" class="col-md-2 control-label">รหัสหมวด</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtGROUP_INDEX" Enabled="false" CssClass="form-control input-md" runat="server" Text="-"></asp:TextBox>
                                            <asp:TextBox ID="txtGroupID" Enabled="false" CssClass="form-control input-md" runat="server" Text="-" Visible="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="txtGroupName" class="col-md-2 control-label ">ชื่อหมวด/กลุ่ม</label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txtGroupName" runat="server" ClientIDMode="Static" CssClass="form-control input-md" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="rdoGroupStatus" class="col-md-2 control-label ">สถานะการใช้งาน</label>
                                        <div class="col-md-3 radio">
                                            <asp:RadioButtonList ID="rdoGroupStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                                <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-5 ">
                                            <asp:Button ID="btnGroupAdd" runat="server" Text="เพิ่มชื่อหมวด/กลุ่ม " CssClass="btn btn-hover btn-info" OnClick="btnGroupAdd_Click" />
                                            <asp:Button ID="btnGroupClear" runat="server" Text="เคลียร์" CssClass="btn btn-hover btn-info" OnClick="btnGroupClear_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลชื่อหมวด/กลุ่ม
                        </div>
                        <div class="panel-body">
                            <asp:GridView ID="GrvSub" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowCreated="GrvSub_RowCreated"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="GrvSub_RowCommand" OnRowDataBound="GrvSub_RowDataBound" DataKeyNames="NGROUPID,SGROUPNAME,CACTIVE,GROUP_INDEX"
                                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" Visible="false">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="10px" HeaderStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="รหัสหมวด">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("GROUP_INDEX") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="center" HeaderText="ชื่อหมวด/กลุ่ม">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblNAME" runat="server" Text='<%# Eval("SGROUPNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แก้ไขข้อมูล" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif"
                                                Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                            <div style="text-align: right;">
                                <asp:Button ID="btnGroupClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="tab-pane" id="TabItem" runat="server" clientidmode="Static">
            <br />
            <asp:UpdatePanel ID="uplItem" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlGroupNameSearch" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnItemAdd" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="grvItem" EventName="PageIndexChanging" />
                </Triggers>
                <ContentTemplate>
                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>ข้อมูลคำถาม
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=ddlGroupName.ClientID%>" class="col-md-2 control-label ">ชื่อหมวด/กลุ่ม</label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlGroupName" runat="server" CssClass="form-control marginTp">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=rdoItemStatus.ClientID %>" class="col-md-2 control-label ">สถานะการใช้งาน</label>
                                        <div class="col-md-3 radio">
                                            <asp:RadioButtonList ID="rdoItemStatus" ClientIDMode="Static" runat="server" RepeatColumns="2" CellPadding="2" CellSpacing="2" RepeatLayout="Flow">
                                                <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;&nbsp;&nbsp;ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="0">&nbsp;&nbsp;&nbsp;&nbsp;ไม่ใช้งาน</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>

                                    </div>
                                    <div class="form-group form-horizontal row">

                                        <label for="<%=txtFORM_CODE.ClientID %>" class="col-md-2 control-label ">รหัสคำถาม</label>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtFORM_CODE" runat="server" CssClass="form-control input-md"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=txtItemName.ClientID %>" class="col-md-2 control-label ">หัวข้อคำถาม</label>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtItemName" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label for="<%=txtItemPoint.ClientID %>" class="col-md-2 control-label">Weight</label>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtItemPoint" runat="server" ClientIDMode="Static" CssClass="form-control input-md" MaxLength="6" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group form-horizontal row">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-5 ">
                                            <asp:Button ID="btnItemAdd" runat="server" Text="เพิ่มข้อมูลคำถาม" CssClass="btn btn-hover btn-info" OnClick="btnItemAdd_Click" />
                                            <asp:Button ID="btnItemClear" runat="server" Text="เคลียร์" CssClass="btn btn-hover btn-info" OnClick="btnItemClear_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info" style="margin-top: 20px;">
                        <div class="panel-heading">
                            <i class="fa fa-table"></i>รายการคำถาม 
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <div class="panel-body">
                                    <div class="form-group form-horizontal row">
                                        <label for="ddlGroupNameSearch" class="col-md-2 control-label">ชื่อหมวด/กลุ่ม</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlGroupNameSearch" runat="server" CssClass="form-control marginTp" OnSelectedIndexChanged="ddlGroupName_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <asp:GridView ID="grvItem" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowCommand="grvItem_RowCommand" OnRowDataBound="grvItem_RowDataBound" OnRowCreated="grvItem_RowCreated"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPageIndexChanging="grvItem_PageIndexChanging"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" DataKeyNames="NVISITFORMID,NGROUPID,SVISITFORMNAME,CACTIVE,NWEIGHT,ITEM_INDEX,FORM_CODE,SGROUPNAME"
                                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="ลำดับ" Visible="false">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="รหัสหมวด">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Eval("GROUP_INDEX") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-CssClass="center" HeaderStyle-HorizontalAlign="Center" HeaderText="รหัสคำถาม" HeaderStyle-Width="20px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%# Eval("FORM_CODE") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="center" HeaderText="หัวข้อคำถาม">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNAME" runat="server" Text='<%# Eval("SVISITFORMNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Weight" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="txtNWEIGHT" runat="server" Text='<%# Eval("NWEIGHT", "{0:n2}") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkActive" runat="server" Checked="false" Enabled="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="แก้ไขข้อมูล" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageEdit" runat="server" ImageUrl="~/Images/document.gif"
                                                        Width="16px" Height="16px" BorderWidth="0" Style="cursor: pointer" CommandName="RowSelect" CausesValidation="False" CommandArgument='<%# DataBinder.Eval(Container, "RowIndex") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                    <div style="text-align: right">
                                        <asp:Button ID="Button3" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 100px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            // Stop user to press enter in textbox
            preventEnter();
        });


        function preventEnter() {
            $("input:text").keypress(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function (s, e) {
            preventEnter();
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

