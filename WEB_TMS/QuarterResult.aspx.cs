﻿using DevExpress.Web.ASPxGridView;
using GemBox.Spreadsheet;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class QuarterResult : PageBase
{
    DataTable dt, dtSave;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initalForm();
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExportExcel.Enabled = false;
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region initalForm
    private void initalForm()
    {
        try
        {

            DropDownListHelper.BindDropDownList(ref ddlYear, SetYear(), "value", "text", false);
            DataTable dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlVendor, dt_vendor, "SVENDORID", "SABBREVIATION", true);


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region SetYear
    private DataTable SetYear()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("value");
            dt.Columns.Add("text");
            DateTime date = new DateTime(2015, 1, 1);
            for (int i = 2015; i <= DateTime.Today.Year; i++)
            {
                date = new DateTime(i, 1, 1);
                dt.Rows.Add(date.Year, date.Year);
            }
            dt.DefaultView.Sort = "value DESC";
            return dt;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        gvDownload.CancelEdit();
        dt = GetDataQuarterByVendor();
        SetDataQuarterByVendor();
    }
    #endregion

    #region GetDataQuarterByVendor
    private DataTable GetDataQuarterByVendor()
    {
        ListItem item1 = cblStatus.Items.FindByValue("1");
        ListItem item0 = cblStatus.Items.FindByValue("0");
        string cactive = string.Empty;
        if (item1.Selected)
        {
            cactive = "1";
            if (item0.Selected)
            {
                cactive = string.Empty;
            }
        }
        else if (item0.Selected)
        {
            cactive = "0";
        }
        else
        {
            cactive = "2";
        }
        return QuarterReportBLL.Instance.GetDataQuarterByVendor(ddlYear.SelectedValue, ddlVendor.SelectedValue, ddlQuerter.SelectedValue, cactive);
    }
    #endregion

    #region SetDataQuarterByVendor
    private void SetDataQuarterByVendor()
    {

        gvDownload.DataSource = dt;
        gvDownload.DataBind();

    }
    #endregion

    #region GetDataUserLog
    private DataTable GetDataUserLog(string I_YEAR, string I_SVENDORID, string I_QUARTER, string I_LOGVERSION)
    {
        return QuarterReportBLL.Instance.GetDataQuarterLog(I_YEAR, I_SVENDORID, I_QUARTER, I_LOGVERSION);
    }
    #endregion

    #region SetDataUserLog
    private void SetDataUserLog(ASPxGridView gv)
    {

        gv.DataSource = dt;
        gv.DataBind();

    }
    #endregion

    #region btnExportExcel_Click
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = GetDataQuarterByVendor();
            dt.Columns.Remove("ROWNUMS");
            dt.Columns.Remove("SVENDORID");
            dt.Columns.Remove("DATESTR");
            dt.Columns.Remove("LOGVERSION");

            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/QuarteryResultFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["ConfirmTruck"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "Quartery_Result_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region gvDownload_AfterPerformCallback
    protected void gvDownload_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "STARTEDIT":
                ASPxGridView gvUser = (ASPxGridView)gvDownload.FindEditFormTemplateControl("gvUser");
                int visibleindex = gvDownload.EditingRowVisibleIndex;
                dynamic data = gvDownload.GetRowValues(visibleindex, "YEARS", "SVENDORID", "QUARTER", "LOGVERSION");
                dt = GetDataUserLog(data[0] + string.Empty, data[1] + string.Empty, data[2] + string.Empty, data[3] + string.Empty);
                SetDataUserLog(gvUser);
                break;
            default:
                break;
        }
    }
    protected void gvDownload_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //ASPxButton btnEdit = (ASPxButton)gvwTruck.FindRowCellTemplateControl(e.VisibleIndex, null, "btnEdit");
        Button btnDownload = gvDownload.FindRowCellTemplateControl(e.VisibleIndex, null, "btnDownload") as Button;
        
        if (!CanWrite)
        {
            btnDownload.Enabled = false;
        }
    }

    
    #endregion
    
    
    #region Download
    #region btnDownload_Click
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int index = int.Parse(btn.CommandArgument);
        SetDataToSave(index);
        string strFont = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor_Font.txt"));
        string font = strFont.Split('|')[0];
        string size = strFont.Split('|')[1];
        string detail = "<table style='width:100%;font-family:\"" + font + "\";'><tbody>";
        DataRow dr = dtSave.NewRow();
        for (int i = 0; i < dtSave.Rows.Count; i++)
        {
            dr = dtSave.Rows[i];
            detail += "<tr>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;width:5%;'>";
            detail += "&nbsp;";
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;width:20px'>";
            detail += (i + 1) + ".";
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
            detail += "เลขที่สัญญา";
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
            detail += dr["SCONTRACTNO"];
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
            detail += "ผลการประเมินได้";
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:24pt;'>";
            detail += (((decimal.Parse(dr["SCORE1"] + string.Empty) + decimal.Parse(dr["SCORE2"] + string.Empty) + decimal.Parse(dr["SCORE3"] + string.Empty)) / 3).ToString("N"));
            detail += "</td>";
            detail += "<td style='font-family:\"" + font + "\";font-size:" + size + "pt;'>";
            detail += "คะแนน";
            detail += "</td>";
            detail += "</tr>";
        }
        detail += "</tbody></table>";

        string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty, HEDERTITLE = string.Empty, TITLE = "บริษัท";
        if ((dr["SABBREVIATION"] + string.Empty).Trim().Contains("บริษัท"))
        {
            SVENDORNAME = (dr["SABBREVIATION"] + string.Empty).Trim().Replace("บริษัท", "");
            HEDERTITLE = "กรรมการผู้จัดการ";
        }
        else
        {
            SVENDORNAME = (dr["SABBREVIATION"] + string.Empty).Trim().Replace("ห้างหุ้นส่วนจำกัด", "");
            SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
            TITLE = "ห้าง";
            HEDERTITLE = "หุ้นส่วนผู้จัดการ";
        }
        string text = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/Quarter_AlertVendor.txt"));
        text = text.Replace("{DOCNO}", (dr["DOCNO"] + string.Empty).Trim());
        text = text.Replace("{DATESTR}", (dr["DATESTR"] + string.Empty).Trim());
        text = text.Replace("{QUARTER}", (dr["QUARTER"] + string.Empty).Trim());
        text = text.Replace("{YEAR}", ddlYear.SelectedValue.Trim());
        text = text.Replace("{SVENDORTITLE}", SVENDORTITLE);
        text = text.Replace("{TITLE}", TITLE);
        text = text.Replace("{HEDERTITLE}", HEDERTITLE);
        text = text.Replace("{SVENDORNAME}", SVENDORNAME);
        text = text.Replace("{DETAIL}", detail);
        int MarginSize = 45;
        HtmlToPdf converter = new HtmlToPdf();
        converter.Options.MarginTop = -10;
        converter.Options.MarginLeft = MarginSize;
        converter.Options.MarginBottom = -10;
        converter.Options.MarginRight = MarginSize;
        // convert the url to pdf 
        PdfDocument doc = converter.ConvertHtmlString(text);


        string Path = this.CheckPath();
        string FileName = "Quartery_Alert_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";

        doc.Save(Response, false, FileName);
        //this.DownloadFile(Path, FileName);
        doc.Close();
    }
    #endregion

    #region SetDataToSave
    private void SetDataToSave(int index)
    {
        dt = GetData(index);
        string Quarter = gvDownload.GetRowValues(index, "QUARTER") + string.Empty;
        dtSave = new DataTable();
        dtSave.Columns.Add("SCONTRACTID", typeof(string));
        dtSave.Columns.Add("SCONTRACTNO", typeof(string));
        dtSave.Columns.Add("SCORE1", typeof(string));
        dtSave.Columns.Add("SCORE2", typeof(string));
        dtSave.Columns.Add("SCORE3", typeof(string));
        dtSave.Columns.Add("DOCNO", typeof(string));
        dtSave.Columns.Add("DATESTR", typeof(string));
        dtSave.Columns.Add("QUARTER", typeof(string));
        dtSave.Columns.Add("SABBREVIATION", typeof(string));
        switch (Quarter)
        {
            case "1":
                AddData(ref dtSave, dt, "JAN", "FEB", "MAR", "DOCNO1", "DATESTR1", "1");
                break;
            case "2":
                AddData(ref dtSave, dt, "APR", "MAY", "JUN", "DOCNO2", "DATESTR2", "2");
                break;
            case "3":
                AddData(ref dtSave, dt, "JUL", "AUG", "SEP", "DOCNO3", "DATESTR3", "3");
                break;
            default:
                AddData(ref dtSave, dt, "OCT", "NOV", "DEC", "DOCNO4", "DATESTR4", "4");
                break;
        }
    }
    #endregion

    #region AddData
    private void AddData(ref DataTable dtSave, DataTable dt, string Column1, string Column2, string Column3, string Column4, string Column5, string Quarter)
    {
        foreach (DataRow item in dt.Rows)
        {
            dtSave.Rows.Add(item["SCONTRACTID"], item["SCONTRACTNO"], item[Column1], item[Column2], item[Column3], item[Column4], item[Column5], Quarter, item["SABBREVIATION"]);
        }
    }
    #endregion

    #region GetData
    private DataTable GetData(int index)
    {
        try
        {
            DataTable dtConfig = GetConfigData(index);
            string selectedValue = "";
            foreach (DataRow item in dtConfig.Rows)
            {
                selectedValue += ",'" + item["SPROCESSID"] + "'";
                
            }
            if (!string.IsNullOrEmpty(selectedValue))
                selectedValue = selectedValue.Substring(1);
            else
            {
                selectedValue = "''";
            }
            string I_SPROCESSID = selectedValue;
            dynamic data = gvDownload.GetRowValues(index, "YEARS", "SVENDORID");
            DataTable dt = QuarterReportBLL.Instance.GetData(data[0] + string.Empty, data[1] + string.Empty, I_SPROCESSID, 1);
            foreach (DataRow item in dt.Rows)
            {
                item["QUARTER1"] = ((decimal)((decimal.Parse(item["JAN"] + string.Empty) + decimal.Parse(item["FEB"] + string.Empty) + decimal.Parse(item["MAR"] + string.Empty)) / 3)).ToString("N");
                item["QUARTER2"] = ((decimal)((decimal.Parse(item["APR"] + string.Empty) + decimal.Parse(item["MAY"] + string.Empty) + decimal.Parse(item["JUN"] + string.Empty)) / 3)).ToString("N");
                item["QUARTER3"] = ((decimal)((decimal.Parse(item["JUL"] + string.Empty) + decimal.Parse(item["AUG"] + string.Empty) + decimal.Parse(item["SEP"] + string.Empty)) / 3)).ToString("N");
                item["QUARTER4"] = ((decimal)((decimal.Parse(item["OCT"] + string.Empty) + decimal.Parse(item["NOV"] + string.Empty) + decimal.Parse(item["DEC"] + string.Empty)) / 3)).ToString("N");
            }
            return dt;

            //Grid.HtmlRowCreated += new ASPxGridViewTableRowEventHandler(grid_HtmlRowCreated);
            //Grid.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region GetConfigData
    private DataTable GetConfigData(int index)
    {
        dynamic data = gvDownload.GetRowValues(index, "YEARS", "QUARTER");
        return QuarterReportBLL.Instance.ConfigQuarterSelect(data[0] + string.Empty, data[1] + string.Empty);
    }
    #endregion

    #endregion
}