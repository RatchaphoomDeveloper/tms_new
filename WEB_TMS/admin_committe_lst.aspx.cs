﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;

public partial class admin_committe_lst : PageBase
{

    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);

        #endregion
        if (!IsPostBack)
        {
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;

            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            sds.Select(new System.Web.UI.DataSourceSelectArguments());
            sds.DataBind();
            Session["ss_SSENTENCERCODE"] = null;

            LogUser("38", "R", "เปิดดูข้อมูลหน้า คณะกรรมการพิจารณาอุทธรณ์", "");
            this.AssignAuthen();
        }

    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
            }
            if (!CanWrite)
            {
                btnAdd.Enabled = false;
                btnDel.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {

        //switch (cbxGroup.SelectedIndex)
        //{
        //    case 0:
        //        sds.SelectCommand = "SELECT  ROW_NUMBER () OVER (ORDER BY C.SCATEGORYID) AS ID1, C.SCATEGORYID, C.SDEFINE, C.NIMPACTLEVEL, C.SIMPACTLEVEL, C.SCATEGORYNAME, CT.SCATEGORYTYPENAME, C.CACTIVE FROM TCATEGORY C  INNER JOIN  TCATEGORYTYPE CT ON C.SCATEGORYTYPEID = CT.SCATEGORYTYPEID WHERE C.SDEFINE LIKE '%' || :oSearch || '%'";
        //        break;
        //    case 1:
        //        sds.SelectCommand = "SELECT  ROW_NUMBER () OVER (ORDER BY C.SCATEGORYID) AS ID1, C.SCATEGORYID, C.SDEFINE, C.NIMPACTLEVEL, C.SIMPACTLEVEL, C.SCATEGORYNAME, CT.SCATEGORYTYPENAME, C.CACTIVE FROM TCATEGORY C  INNER JOIN  TCATEGORYTYPE CT ON C.SCATEGORYTYPEID = CT.SCATEGORYTYPEID WHERE CT.SCATEGORYTYPENAME LIKE '%' || :oSearch || '%'";
        //        break;
        //}
        sds.DataBind();
        gvw.DataBind();

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();

                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("SSENTENCERCODE", "CACTIVE")
                    .Cast<object[]>()
                    .Select(s => new { SSENTENCERCODE = s[0].ToString(), CACTIVE = s[1].ToString() });
                string delid = "";
                foreach (var l in ld)
                {
                    Session["ss_DelSSENTENCERCODE"] = l.SSENTENCERCODE;
                    sds.Delete();
                    sds.DataBind();
                    delid += l.SSENTENCERCODE + ",";
                }

                LogUser("38", "D", "ลบข้อมูลหน้า คณะกรรมการพิจารณาอุทธรณ์ รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");
                gvw.Selection.UnselectAll();
                break;
            case "edit":

                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SSENTENCERCODE");
                string stmID = Convert.ToString(data);

                Session["ss_SSENTENCERCODE"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_committe_add.aspx";

                break;

        }
    }
    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ASPxGridView xGridView = (ASPxGridView)sender;
        if (e.Column.Caption == "ที่")
        {
            e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
        }
        else if (e.Column.Caption == "ปี")
        {
            dynamic dataRowArray = xGridView.GetRowValues(e.VisibleRowIndex, "DBEGINTERM", "DENDTERM");
            e.DisplayText = "" + dataRowArray[0].ToString("yyyy");
        }
        else if (e.Column.Caption == "ดำรงตำแหน่ง")
        {
            dynamic dataRowArray = xGridView.GetRowValues(e.VisibleRowIndex, "DBEGINTERM", "DENDTERM");
            e.DisplayText = "" + dataRowArray[0].ToString("dd MMM yyyy") + " - " + dataRowArray[1].ToString("dd MMM yyyy");
        }

    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
        if (!CanWrite)
        {
            imbedit.Enabled = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin_committe_add.aspx");

    }
    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
