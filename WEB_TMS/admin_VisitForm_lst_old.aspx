﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_VisitForm_lst_old.aspx.cs" Inherits="admin_ChkList_lst" StylesheetTheme="Aqua" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.2" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style13
        {
            height: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="left" width="65%" class="style13">
                            <dx:ASPxButton ID="btnConfig" runat="server" Text="ตั้งค่าแบบฟอร์มประเมินการบริหารงานประจำปี" OnClick="btnConfig_Click" Style="margin-left: 10px"> 
                            </dx:ASPxButton>
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td width="8%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" NullText="กรุณาป้อนข้อมูลที่ต้องการค้นหา"
                                Style="margin-left: 0px" Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" 
                                SkinID="_gvw" DataSourceID="sds" SettingsPager-PageSize="10" KeyFieldName="NTYPEVISITFORMID">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="1%"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="2" FieldName="NTYPEVISITFORMID"
                                        Visible="false">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ชื่อแบบฟอร์ม" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center"
                                        Width="35%" FieldName="STYPEVISITFORMNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="จำนวน" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หมวดปัญหา" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                                FieldName="GROUPCOUNT" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="คำถาม" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                FieldName="VISITFORMCOUNT" Width="10%">
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataCheckColumn VisibleIndex="7" Width="10%" Caption="สถานะ" HeaderStyle-HorizontalAlign="Center">
                                        <DataItemTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox2" Checked='<%# (Boolean.Parse(Eval("CACTIVE").ToString()== "1"?"true":"false"))%>'
                                                runat="server" ReadOnly="true">
                                            </dx:ASPxCheckBox>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataCheckColumn>
                                    <dx:GridViewDataDateColumn VisibleIndex="8" Width="10%" Caption="ปรับปรุงข้อมูลล่าสุด"
                                        HeaderStyle-HorizontalAlign="Center" FieldName="DUPDATE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="9">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT V.NTYPEVISITFORMID,V.STYPEVISITFORMNAME, V.CACTIVE, V.DUPDATE ,(SELECT count(*) FROM TGROUPOFVISITFORM g WHERE G.NTYPEVISITFORMID = V.NTYPEVISITFORMID ) as GROUPCOUNT,(SELECT count(*) FROM TVISITFORM f WHERE F.NTYPEVISITFORMID =  V.NTYPEVISITFORMID ) as VISITFORMCOUNT  FROM   TTYPEOFVISITFORM v WHERE V.STYPEVISITFORMNAME LIKE '%' || :oSearch || '%'  ORDER BY v.DUPDATE DESC"
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch" PropertyName="Text" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
