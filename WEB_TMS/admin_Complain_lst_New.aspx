﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="admin_Complain_lst_New.aspx.cs" Inherits="admin_Complain_lst_New" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="server">
    <div class="container" style="width: 100%">
        <div class="container" style="width: 100%;">
            <div class="form-horizontal" style="background-color: white">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i>
                                <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ค้นหา&#8711;</a>
                                <input type="hidden" id="hiddencollapseFindContract" value=" " />
                            </div>
                            <div class="panel-collapse collapse in" id="collapseFindContract">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="เงื่อนไขค้นหา" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="cboSearchType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="True" Text="- เงื่อนไขค้นหา -" />
                                                    <asp:ListItem Text="หมายเลขเอกสาร" Value="DOC_ID" />
                                                    <asp:ListItem Text="เลขที่สัญญา" Value="SCONTRACTNO" />
                                                    <%--<asp:ListItem Text="ทะเบียนรถ" Value="SHEADREGISTERNO" />--%>
                                                    <asp:ListItem Text="ชื่อผู้ร้องเรียน" Value="SCOMPLAINFNAME" />
                                                    <asp:ListItem Text="ชื่อผู้ประกอบการ" Value="SVENDORNAME" />
                                                    <%--<asp:ListItem Text="กลุ่มที่" Value="M_GROUPS.NAME" />--%>
                                                    <%--<asp:ListItem Text="รหัส,ชื่อ-นามสกุล พขร." Value="DRIVER" />--%>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="ค้นหา" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="ชื่อผู้บันทึก" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:TextBox runat="server" ID="txtCreateBy" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="สถานะเอกสาร" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="cboStatus" runat="server" CssClass="form-control">
                                                    <Items>
                                                        <asp:ListItem Text="สถานะเอกสาร" Value="DOC_STATUS_NAME" />
                                                    </Items>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="หัวข้อร้องเรียน" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:DropDownList ID="cboComplainType" runat="server" CssClass="form-control">
                                                    <Items>
                                                        <asp:ListItem Text="หัวข้อร้องเรียน" Value="COMPLAIN_TYPE_NAME" />
                                                    </Items>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label ID="lblChkUrgent" Text="เอกสาร" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <%--<asp:CheckBox ID="chkIsCorrupt" runat="server" Text="เอกสารที่ พขร. ทุจริต&nbsp;" />--%>
                                                <asp:CheckBox ID="chkIsUrgent" runat="server" Text="เอกสารที่แจ้งล่าช้า" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="วันที่เกิดเหตุ(จาก)" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:TextBox runat="server" ID="dteStart" CssClass="datepicker" Style="text-align: center" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label col-md-4">
                                                <asp:Label Text="วันที่เกิดเหตุ(ถึง)" runat="server" />
                                            </label>
                                            <div class="col-md-8">
                                                <asp:TextBox runat="server" ID="dteEnd" CssClass="datepicker" Style="text-align: center" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-right" runat="server" id="divButton">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSearch" runat="server" Text="ค้นหา" OnClick="btnSearch_Click" CssClass="btn btn-info" />
                                        <asp:Button ID="btnClear" runat="server" Text="เคลียร์" OnClick="btnClear_Click" CssClass="btn btn-danger" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                                <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                            </div>
                            <div class="panel-collapse collapse in" id="collapseFindContract2">
                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <asp:Label ID="lblUrgent" runat="server" Text="เอกสารที่ตัวหนังสือสีแดง หมายถึง เอกสารแจ้งล่าช้า" ForeColor="Red" Font-Underline="true"></asp:Label>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:Label ID="lblContractID" runat="server" Text="ผลการค้นหา เลขที่สัญญา {0}" Visible="false" ForeColor="Green" Font-Underline="true"></asp:Label>
                                        </div>
                                        <div class="col-md-4" style="text-align: right">
                                            <asp:Label Text="จำนวนรายการเรื่องร้องเรียน&nbsp;" runat="server" />
                                            <asp:Label Text="0" runat="server" ID="lblCarCount" Font-Bold="true" ForeColor="Red" />
                                            <asp:Label Text="&nbsp;รายการ" runat="server" />
                                        </div>
                                    </div>
                                    <asp:GridView runat="server" DataKeyNames="DOC_ID" ID="dgvComplain" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                        CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                        AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvComplain_PageIndexChanging"
                                        OnRowUpdating="dgvComplain_RowUpdating">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/64/blue-23.png" Width="23"
                                                        Height="23" Style="cursor: pointer" CommandName="update" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DOC_ID" HeaderText="หมายเลขเอกสาร">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="GROUPNAME" HeaderText="กลุ่มที่">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียน">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DDELIVERY" HeaderText="วันที่เกิดเหตุ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SVENDORNAME" HeaderText="ผู้ประกอบการ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <%--<asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียนรถ (หัว)">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>--%>
                                            <asp:BoundField DataField="CREATENAME" HeaderText="ผู้บันทึก">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="STATUS" HeaderText="สถานะ">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="pagination-ys" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="panel-footer" style="text-align: right">
                                <asp:Button ID="btnAdd" runat="server" Text="เพิ่มรายการ" OnClick="btnAdd_Click" Width="90px" CssClass="btn btn-info"></asp:Button>
                                <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" Width="90px" CssClass="btn btn-danger"></asp:Button>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExport" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
