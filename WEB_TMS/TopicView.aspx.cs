﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Data;
using TMS_BLL.Master;

public partial class TopicView : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);

        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            //Cache.Remove(sds.CacheKeyDependency);
            //Cache[sds.CacheKeyDependency] = new object();
            //sds.Select(new System.Web.UI.DataSourceSelectArguments());
            //sds.DataBind();

            // DataSourceID="sds"

            TopicBLL topicBLL = new TopicBLL();
            gvw.DataSource = topicBLL.TopicSelect(string.Empty);
            gvw.DataBind();
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                sds.Select(new System.Web.UI.DataSourceSelectArguments());
                sds.DataBind();

                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "NID")
                    .Cast<object[]>()
                    .Select(s => new { ID1 = s[0].ToString(), NID = s[1].ToString() });

                string delid = "";

                foreach (var l in ld)
                {
                    Session["dNHOLIDAYID"] = l.NID;
                    sds.Delete();

                    delid += l.NID + ",";
                }

                //LogUser("41", "D", "ลบข้อมูลหน้า ข้อมูลวันหยุด รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "NID");
                string stmID = Convert.ToString(data);

                Session["oNHOLIDAYID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "admin_Holiday_add.aspx";

                break;

        }
    }
}
