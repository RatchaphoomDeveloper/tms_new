﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="ConfirmTruck.aspx.cs" Inherits="ConfirmTruck" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvw">
    </dx:ASPxGridViewExporter>
    <table width="100%">
        <tr>
            <td>
                <%--<dx:ASPxButton ID="btnExecl" runat="server" Image-Url="~/Images/ic_ms_excel.gif" Text=" Excel" />--%>
                <asp:LinkButton ID="btnExecl" runat="server"><img src='Images/ic_ms_excel.gif' border='0' /> Excel</asp:LinkButton>
            </td>
            <td align="right" valign="top">
                <dx:ASPxTextBox ID="txtSearch" runat="server" CssClass="dxeLineBreakFix" NullText="เลขที่สัญญา"
                    Width="150px" />
            </td>
            <td>
                คลัง
            </td>
            <td>
                <dx:ASPxComboBox ID="cbxTerminal" runat="server" CallbackPageSize="10" ClientInstanceName="cbxTerminal"
                    EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL"
                    SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="150px">
                    <Columns>
                        <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" Width="80px" />
                        <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                บริษัท
            </td>
            <td>
                <dx:ASPxComboBox ID="cbxVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cbxVendor"
                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                วันที่ยืนยัน
            </td>
            <td>
                <dx:ASPxDateEdit ID="dteSearch" runat="server" SkinID="xdte" CssClass="dxeLineBreakFix">
                </dx:ASPxDateEdit>
            </td>
            <td>
                <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix" />
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
        CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
        EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
    <dx:ASPxGridView ID="gvw" runat="server" SkinID="_gvw" DataSourceID="sds" AutoGenerateColumns="False">
        <Columns>
            <dx:GridViewDataTextColumn Caption="ที่" HeaderStyle-HorizontalAlign="Center" Width="5%"
                VisibleIndex="0">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="วันที่ยืนยัน" FieldName="DDATE" />
            <dx:GridViewDataTextColumn Caption="รหัสสัญญา" FieldName="SCONTRACTID" />
            <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" />
            <dx:GridViewDataTextColumn Caption="ชื่อผู้ประกอบบการ" FieldName="SABBREVIATION" />
            <dx:GridViewDataTextColumn Caption="รหัสผู้ประกอบการ" FieldName="SVENDORID" />
            <dx:GridViewDataTextColumn Caption="คลังที่ยืนยัน" FieldName="SNAMETERMINAL" />
            <dx:GridViewDataTextColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" />
            <dx:GridViewDataTextColumn Caption="ทะเบียนท้าย" FieldName="STRAILERREGISTERNO" />
            <dx:GridViewDataTextColumn Caption="ชื่อพขร" FieldName="SDRIVERNAME" />
        </Columns>
    </dx:ASPxGridView>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
