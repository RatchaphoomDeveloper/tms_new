﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="final-grade.aspx.cs" Inherits="final_grade" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlVendorSearch" EventName="SelectedIndexChanged" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlVendorSearch.ClientID %>" class="col-md-2 control-label">บริษัทผู้ขนส่ง</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorSearch_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlYearSearch.ClientID %>" class="col-md-2 control-label">ประจำปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlContract.ClientID %>" class="col-md-2 control-label">เลขที่สัญญา</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlGroup.ClientID %>" class="col-md-2 control-label">กลุ่มงาน</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                                    </asp:DropDownList>
                                </div>
                                <div id="divStatus" runat="server">
                                    <label for="<%=chkStatus.ClientID %>" class="col-md-2 control-label">สถานะเอกสาร</label>
                                    <div class="col-md-4 checkbox">
                                        <asp:CheckBoxList ID="chkStatus" runat="server" RepeatColumns="2">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row" style="text-align: right">
                                <asp:Button ID="btnNoticeHistory" runat="server" Text="ประวัติการแจ้งผล" CssClass="btn btn-hover btn-info" OnClientClick="openInNewTab('AnnualReportHistory.aspx')" OnClick="btnViewHistory_Click" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                <asp:Button ID="btnExport" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblHeaderGrid" runat="server" Text="สรุปผลประเมินการปฏิบัติงานในรอบ 12 เดือน จำนวน {0} สัญญา"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="form-group form-horizontal row" style="text-align: right; padding-right: 20px;">
                        <label class="control-label">O:การประเมินผลการทำงานขนส่ง, M:การประเมินผลการบริหารงาน , K:การประเมินผลดัชนีชีวัดประสิทธิภาพการทำงาน</label>
                    </div>
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" PageSize="50"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowUpdating="grvMain_RowUpdating" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="SCONTRACTNO,SABBREVIATION,SVENDORID" OnDataBound="grvMain_DataBound"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="บริษัทผู้ขนส่ง" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSABBREVIATION" runat="server" Text='<%# Eval("SABBREVIATION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="กลุ่มงาน"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblGROUPNAME" runat="server" Text='<%# Eval("GROUPNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เลขที่สัญญา"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSCONTRACTNO" runat="server" Text='<%# Eval("SCONTRACTNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblALL_QUARTER" runat="server" Text='<%# Eval("ALL_QUARTER") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:HyperLink runat="server" ID="lnkQuarteryReport" href="javascript:;">O (30%)</asp:HyperLink>
                                    </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="60px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblAUDITSCORE" runat="server" Text='<%# Eval("AUDIT_SCORE") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <a href="javascript:;" onclick="openInNewTab('questionnaire.aspx');">M (30%)</a>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblKPI_BY_YEAR" runat="server" Text='<%# Eval("KPI_SCORE_BY_YEAR") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:HyperLink runat="server" ID="lnkKPI" href="javascript:;">K (40%)</asp:HyperLink>
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สรุปคะแนน<br/>(100%)"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="100px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSUMSCORE" runat="server" Text='<%# Eval("SUM_ALL_SCORE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เกรด"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="30px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblGRADE" runat="server" Text='<%# Eval("GRADE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะเอกสาร"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("DOCSTATUSNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่ดำเนินการ"
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSEND_TO_VENDOR_DATE" runat="server" Text='<%# Eval("ACTION_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText=""
                                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderBtn" runat="server" Text=""></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="btnSaveMail" runat="server" Text="แจ้งผล" CssClass="btn btn-hover btn-info" CausesValidation="false" />
                                    <asp:Button ID="btnViewResult" runat="server" Text="ดูผลประเมิน" CssClass="btn btn-hover btn-info" CausesValidation="false" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div style="display: none;">
        <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" />
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
    </div>
    <script type="text/javascript">

        function gotoView(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnView.ClientID%>").click();
            openInNewTab('AnnualReportNotice.aspx');
        }

        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

