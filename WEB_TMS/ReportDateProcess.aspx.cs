﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Web.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxEditors;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;


public partial class ReportDateProcess : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private static List<SYEAR> _SYEAR = new List<SYEAR>();
    private static int nNextYeatCheckWater = 3;
    private static DataTable dtMainData = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        gvw.HtmlDataCellPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        if (!IsPostBack)
        {
            ListData();

        }
    }

    void gvw_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "ใบรับรอง")
        {
            ASPxTextBox txtChecking = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtChecking") as ASPxTextBox;
            txtChecking.ClientInstanceName = txtChecking.ID + "_" + e.VisibleIndex;
            ASPxTextBox txtRequestID = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "txtRequestID") as ASPxTextBox;
            txtRequestID.ClientInstanceName = txtRequestID.ID + "_" + e.VisibleIndex;
        }


    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search": ListData();
                break;
            //case "ReportPDF": ListReport("P");
            //    break;
            //case "ReportExcel": ListReport("E");
            //    break;

        }
    }

    void ListData()
    {

        string Condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            // Condition = " AND REQ.VEH_NO||REQ.TU_NO||VEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text) + "%'";
        }

        if (edtStart.Value != null && edtEnd.Value != null)
        {
            lblsTail.Text = edtStart.Text + " - " + edtEnd.Text;

            DateTime datestart = DateTime.Parse(edtStart.Value.ToString());
            DateTime dateend = DateTime.Parse(edtEnd.Value.ToString());

            Condition += " AND (TRUNC(TIC.EXAMDATE) BETWEEN TO_DATE('" + CommonFunction.ReplaceInjection(datestart.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY') AND  TO_DATE('" + CommonFunction.ReplaceInjection(dateend.ToString("dd/MM/yyyy", new CultureInfo("en-US"))) + "','DD/MM/YYYY')) ";
        }
        else
        {
            lblsTail.Text = " - ";
        }



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME
,TO_CHAR(TRUNC(add_months(REQ.REQUEST_DATE,6516)),'DD/MM/YYYY') as REQUEST_DATE
,TO_CHAR(TRUNC(add_months(REQ.APPROVE_DATE,6516)),'DD/MM/YYYY') as APPROVE_DATE
,TO_CHAR(TRUNC(add_months(REQ.ACCEPT_DATE,6516)),'DD/MM/YYYY') as ACCEPT_DATE
,TO_CHAR(TRUNC(add_months(REQ.SERVICE_DATE,6516)),'DD/MM/YYYY') as SERVICE_DATE
,CASE WHEN  NVL(TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal1
,CASE WHEN  NVL(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal2
,CASE WHEN  NVL(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal3
,CASE
 WHEN  
NVL((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')),1234)  <>  1234 
THEN 
((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')))||' วัน' ELSE '' END as SUMDATE
,NVL(REQ.EVENTMOVE,'x') as EVENTMOVE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
WHERE 1=1 " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvw.DataSource = dt;

        }
        gvw.DataBind();
    }

    private void ListReport(string Type)
    {

        string Condition = "";
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        //}
        //else
        //{

        //}



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME
,TO_CHAR(TRUNC(add_months(REQ.REQUEST_DATE,6516)),'DD/MM/YYYY') as REQUEST_DATE
,TO_CHAR(TRUNC(add_months(REQ.APPROVE_DATE,6516)),'DD/MM/YYYY') as APPROVE_DATE
,TO_CHAR(TRUNC(add_months(REQ.ACCEPT_DATE,6516)),'DD/MM/YYYY') as ACCEPT_DATE
,TO_CHAR(TRUNC(add_months(REQ.SERVICE_DATE,6516)),'DD/MM/YYYY') as SERVICE_DATE
,CASE WHEN  NVL(TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal1
,CASE WHEN  NVL(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal2
,CASE WHEN  NVL(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal3
,CASE
 WHEN  
NVL((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')),1234)  <>  1234 
THEN 
((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')))||' วัน' ELSE '' END as SUMDATE
,NVL(REQ.EVENTMOVE,'x') as EVENTMOVE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
    " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        //if (dt.Rows.Count > 0)
        //{
        //    gvw.DataSource = dt;
        //    gvw.DataBind();
        //}


        rpt_DateProcess report = new rpt_DateProcess();

        #region function report

        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    string Date = "1/" + cboMonth.Value + "/" + cboYear.Text + "";
        //    DateTime DMONTH = DateTime.Parse(Date);
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = DMONTH.ToString("MMMM", new CultureInfo("th-TH")) + " " + cboYear.Text;
        //}
        //else
        //{
        //    ((XRLabel)report.FindControl("xrLabel2", true)).Text = " - ";
        //}

        report.Name = "รายงานผลการดำเนินงานประจำเดือน";
        report.DataSource = dt;
        string fileName = "รายงานผลการดำเนินงานประจำเดือน_" + DateTime.Now.ToString("MMddyyyyHHmmss");

        MemoryStream stream = new MemoryStream();


        string sType = "";
        if (Type == "P")
        {
            report.ExportToPdf(stream);
            Response.ContentType = "application/pdf";
            sType = ".pdf";
        }
        else
        {
            report.ExportToXls(stream);
            Response.ContentType = "application/xls";
            sType = ".xls";
        }


        Response.AddHeader("Accept-Header", stream.Length.ToString());
        Response.AddHeader("Content-Disposition", "Attachment; filename=" + Server.UrlEncode(fileName) + sType);
        Response.AddHeader("Content-Length", stream.Length.ToString());
        Response.ContentEncoding = System.Text.Encoding.ASCII;
        Response.BinaryWrite(stream.ToArray());
        Response.End();
        #endregion
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        ExportToPdf();
    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        //ListReport("E");
        ExportToXls();
    }

    private void ExportToXls()
    {
        string Condition = "";
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        //}
        //else
        //{

        //}



        string QUERY = @"SELECT ROWNUM||'.' as NO
,REQ.VEH_NO
,REQ.TU_NO
,VEN.SABBREVIATION
,RQT.REQTYPE_NAME
,TO_CHAR(TRUNC(add_months(REQ.REQUEST_DATE,6516)),'DD/MM/YYYY') as REQUEST_DATE
,TO_CHAR(TRUNC(add_months(REQ.APPROVE_DATE,6516)),'DD/MM/YYYY') as APPROVE_DATE
,TO_CHAR(TRUNC(add_months(REQ.ACCEPT_DATE,6516)),'DD/MM/YYYY') as ACCEPT_DATE
,TO_CHAR(TRUNC(add_months(REQ.SERVICE_DATE,6516)),'DD/MM/YYYY') as SERVICE_DATE
,CASE WHEN  NVL(TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal1
,CASE WHEN  NVL(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal2
,CASE WHEN  NVL(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal3
,CASE
 WHEN  
NVL((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')),1234)  <>  1234 
THEN 
((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')))||' วัน' ELSE '' END as SUMDATE
,CASE WHEN NVL(REQ.EVENTMOVE,'x') = 'x' THEN '' ELSE '/' END as EVENTMOVE
,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
FROM TBL_REQUEST REQ
LEFT JOIN TVENDOR VEN
ON VEN.SVENDORID =  REQ.VENDOR_ID
LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
    " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        try
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=รายงานเวลาเข้ารับบริการแต่ละขั้นตอน.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";



            StringWriter sw = new StringWriter();

            using (HtmlTextWriter hw = new HtmlTextWriter(sw))
            {


                string Date = "";

                Label lblheader = new Label();
                string sHeader = "รายงานเวลาเข้ารับบริการแต่ละขั้นตอน";
                lblheader.Text = @"<table style='font-size: 12; font-family: Tahoma; width: 1000px;'>
            <tr>
                <td align='center'><b>รายงานเวลาเข้ารับบริการแต่ละขั้นตอน</b>
                </td>
            </tr>
             <tr>
                <td align='left'>รายงานเวลาเข้ารับบริการแต่ละขั้นตอน ระหว่างวันที่ " + Date + @"
                </td>
            </tr>
            <tr>
                <td align='center'>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <table style='font-size: 10; font-family: Tahoma;' width='100%' cellpadding='3' cellspacing='1' border='1'>
                        <tr>
                            <td width='5%' style='font-size: 10; font-family: Tahoma;' align='center'>ที่</td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>ทะเบียนหัว</td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>ทะเบียนท้าย</td>
                            <td width='15%' style='font-size: 10; font-family: Tahoma;' align='center'>บริษัทผู้ข่นส่ง</td>
                            <td width='16%' style='font-size: 10; font-family: Tahoma;' align='center'>ประเภทคำขอ</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>ยื่นคำขอ</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>รข.อนุมัติ</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>มว.จัดลงคิว</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>นัดหมาย</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>จำนวนวัน</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>เลื่อนคิว</td>
                        </tr>";
                lblheader.RenderControl(hw);

                string Html = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Html += @"<tr>
                            <td width='5%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["NO"] + @"</td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["VEH_NO"] + @"</td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["TU_NO"] + @"</td>
                            <td width='15%' style='font-size: 10; font-family: Tahoma;'>" + dt.Rows[i]["SABBREVIATION"] + @"</td>
                            <td width='16%' style='font-size: 10; font-family: Tahoma;'>" + dt.Rows[i]["REQTYPE_NAME"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["REQUEST_DATE"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["APPROVE_DATE"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["ACCEPT_DATE"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["SERVICE_DATE"] + @"</td>
                            <td width='8%' rowspan='2' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["SUMDATE"] + @"</td>
                            <td width='8%' rowspan='2' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["EVENTMOVE"] + @"</td>
                        </tr>
                         <tr>
                            <td width='5%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='10%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='15%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='16%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;'></td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal1"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal2"] + @"</td>
                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal3"] + @"</td>
                        </tr>";
                }





                Label lblDetail = new Label();
                lblDetail.Text = Html;
                lblDetail.RenderControl(hw);

                Label lblFooter = new Label();
                lblFooter.Text = "</table>";
                lblFooter.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.Close();
                Response.End();

            }

        }
        catch (DocumentException de)
        {
            System.Web.HttpContext.Current.Response.Write(de.Message);
        }
        catch (IOException ioEx)
        {
            System.Web.HttpContext.Current.Response.Write(ioEx.Message);
        }
        catch (Exception ex)
        {
            System.Web.HttpContext.Current.Response.Write(ex.Message);
        }
    }

    //    private void ExportToPdf()
    //    {
    //        string Condition = "";
    //        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
    //        //{
    //        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
    //        //}
    //        //else
    //        //{

    //        //}



    //        string QUERY = @"SELECT ROWNUM||'.' as NO
    //,REQ.VEH_NO
    //,REQ.TU_NO
    //,VEN.SABBREVIATION
    //,RQT.REQTYPE_NAME
    //,TO_CHAR(TRUNC(add_months(REQ.REQUEST_DATE,6516)),'DD/MM/YYYY') as REQUEST_DATE
    //,TO_CHAR(TRUNC(add_months(REQ.APPROVE_DATE,6516)),'DD/MM/YYYY') as APPROVE_DATE
    //,TO_CHAR(TRUNC(add_months(REQ.ACCEPT_DATE,6516)),'DD/MM/YYYY') as ACCEPT_DATE
    //,TO_CHAR(TRUNC(add_months(REQ.SERVICE_DATE,6516)),'DD/MM/YYYY') as SERVICE_DATE
    //,CASE WHEN  NVL(TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal1
    //,CASE WHEN  NVL(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal2
    //,CASE WHEN  NVL(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal3
    //,CASE
    // WHEN  
    //NVL((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
    //(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
    //(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')),1234)  <>  1234 
    //THEN 
    //((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
    //(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
    //(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')))||' วัน' ELSE '' END as SUMDATE
    //,CASE WHEN NVL(REQ.EVENTMOVE,'x') = 'x' THEN '' ELSE '/' END as EVENTMOVE
    //,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
    //FROM TBL_REQUEST REQ
    //LEFT JOIN TVENDOR VEN
    //ON VEN.SVENDORID =  REQ.VENDOR_ID
    //LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
    //    " + Condition + "";

    //        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
    //        try
    //        {
    //            //Response.ContentType = "application/pdf";
    //            //Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf");
    //            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //            //StringWriter sw = new StringWriter();

    //            //using (HtmlTextWriter hw = new HtmlTextWriter(sw))
    //            //{


    //            string Date = "";
    //            string Html = "";
    //            //Label lblheader = new Label();
    //            //string sHeader = "รายงานเวลาเข้ารับบริการแต่ละขั้นตอน";
    //            Html += @"<table style='font-size: 12; font-family: Tahoma; width: 1000px;'>
    //            <tr>
    //                <td align='center'><b>รายงานเวลาเข้ารับบริการแต่ละขั้นตอน</b>
    //                </td>
    //            </tr>
    //             <tr>
    //                <td align='left'>รายงานเวลาเข้ารับบริการแต่ละขั้นตอน ระหว่างวันที่ " + Date + @"
    //                </td>
    //            </tr>
    //            <tr>
    //                <td align='center'>&nbsp;
    //                </td>
    //            </tr>
    //            <tr>
    //                <td>
    //                    <table style='font-size: 10; font-family: Tahoma;' width='100%' cellpadding='3' cellspacing='1' border='1'>
    //                        <tr>
    //                            <td width='5%' style='font-size: 10; font-family: Tahoma;' align='center'>ที่</td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>ทะเบียนหัว</td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>ทะเบียนท้าย</td>
    //                            <td width='15%' style='font-size: 10; font-family: Tahoma;' align='center'>บริษัทผู้ข่นส่ง</td>
    //                            <td width='16%' style='font-size: 10; font-family: Tahoma;' align='center'>ประเภทคำขอ</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>ยื่นคำขอ</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>รข.อนุมัติ</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>มว.จัดลงคิว</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>นัดหมาย</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>จำนวนวัน</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>เลื่อนคิว</td>
    //                        </tr>";
    //            //lblheader.RenderControl(hw);



    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                Html += @"<tr>
    //                            <td width='5%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["NO"] + @"</td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["VEH_NO"] + @"</td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["TU_NO"] + @"</td>
    //                            <td width='15%' style='font-size: 10; font-family: Tahoma;'>" + dt.Rows[i]["SABBREVIATION"] + @"</td>
    //                            <td width='16%' style='font-size: 10; font-family: Tahoma;'>" + dt.Rows[i]["REQTYPE_NAME"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["REQUEST_DATE"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["APPROVE_DATE"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["ACCEPT_DATE"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["SERVICE_DATE"] + @"</td>
    //                            <td width='8%' rowspan='2' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["SUMDATE"] + @"</td>
    //                            <td width='8%' rowspan='2' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["EVENTMOVE"] + @"</td>
    //                        </tr>
    //                         <tr>
    //                            <td width='5%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='10%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='15%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='16%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;'></td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal1"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal2"] + @"</td>
    //                            <td width='8%' style='font-size: 10; font-family: Tahoma;' align='center'>" + dt.Rows[i]["DateCal3"] + @"</td>
    //                        </tr>";
    //            }





    //            //Label lblDetail = new Label();
    //            //lblDetail.Text = Html;
    //            //lblDetail.RenderControl(hw);

    //            // Label lblFooter = new Label();
    //            Html += "</table>";
    //            //lblFooter.RenderControl(hw);

    //            //StringReader sr = new StringReader(sw.ToString());
    //            //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);


    //            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
    //            //pdfDoc.Open();
    //            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
    //            //htmlparser.Parse(sr);
    //            //pdfDoc.Close();
    //            //Response.Write(pdfDoc);
    //            //Response.End();

    //            MemoryStream msOutput = new MemoryStream();
    //            TextReader reader = new StringReader(Html);

    //            // step 1: creation of a document-object
    //            Document document = new Document(PageSize.A4, 30, 30, 30, 30);

    //            // step 2:
    //            // we create a writer that listens to the document
    //            // and directs a XML-stream to a file
    //            PdfWriter writer = PdfWriter.GetInstance(document, msOutput);

    //            // step 3: we create a worker parse the document
    //            HTMLWorker worker = new HTMLWorker(document);

    //            // step 4: we open document and start the worker on the document
    //            document.Open();
    //            worker.StartDocument();

    //            // step 5: parse the html into the document
    //            worker.Parse(reader);

    //            // step 6: close the document and the worker
    //            worker.EndDocument();
    //            worker.Close();
    //            document.Close();


    //            //}

    //        }
    //        catch (DocumentException de)
    //        {
    //            System.Web.HttpContext.Current.Response.Write(de.Message);
    //        }
    //        catch (IOException ioEx)
    //        {
    //            System.Web.HttpContext.Current.Response.Write(ioEx.Message);
    //        }
    //        catch (Exception ex)
    //        {
    //            System.Web.HttpContext.Current.Response.Write(ex.Message);
    //        }

    //    }

    private void ExportToPdf()
    {
        #region Datatable
        string Condition = "";
        //if (!string.IsNullOrEmpty(cboMonth.Text) && !string.IsNullOrEmpty(cboYear.Text))
        //{
        //    Condition = "WHERE TO_CHAR(NVL(REQ.SERVICE_DATE,REQ.APPOINTMENT_DATE),'MM/yyyy') = '" + CommonFunction.ReplaceInjection((cboMonth.Value + "/" + cboYear.Value)) + "'";
        //}
        //else
        //{

        //}



        string QUERY = @"SELECT ROWNUM||'.' as NO
    ,REQ.VEH_NO
    ,REQ.TU_NO
    ,VEN.SABBREVIATION
    ,RQT.REQTYPE_NAME
    ,TO_CHAR(TRUNC(add_months(REQ.REQUEST_DATE,6516)),'DD/MM/YYYY') as REQUEST_DATE
    ,TO_CHAR(TRUNC(add_months(REQ.APPROVE_DATE,6516)),'DD/MM/YYYY') as APPROVE_DATE
    ,TO_CHAR(TRUNC(add_months(REQ.ACCEPT_DATE,6516)),'DD/MM/YYYY') as ACCEPT_DATE
    ,TO_CHAR(TRUNC(add_months(REQ.SERVICE_DATE,6516)),'DD/MM/YYYY') as SERVICE_DATE
    ,CASE WHEN  NVL(TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal1
    ,CASE WHEN  NVL(TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal2
    ,CASE WHEN  NVL(TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'),1234)  <>  1234 THEN (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY'))||' วัน' ELSE '' END as DateCal3
    ,CASE
     WHEN  
    NVL((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
    (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
    (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')),1234)  <>  1234 
    THEN 
    ((TO_DATE(REQ.APPROVE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.REQUEST_DATE, 'DD/MM/YYYY') )+
    (TO_DATE(REQ.ACCEPT_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.APPROVE_DATE, 'DD/MM/YYYY') )+
    (TO_DATE(REQ.SERVICE_DATE,'DD/MM/YYYY')  - TO_DATE(REQ.ACCEPT_DATE, 'DD/MM/YYYY')))||' วัน' ELSE '' END as SUMDATE
    ,CASE WHEN NVL(REQ.EVENTMOVE,'x') = 'x' THEN '' ELSE '/' END as EVENTMOVE
    ,NVL(REQ.REQUEST_ID,'xxx') as REQUEST_ID
    FROM TBL_REQUEST REQ
    LEFT JOIN TVENDOR VEN
    ON VEN.SVENDORID =  REQ.VENDOR_ID
    LEFT JOIN TBL_REQTYPE RQT ON RQT.REQTYPE_ID = REQ.REQTYPE_ID
        " + Condition + "";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY);
        #endregion

        //set font ในกรณีที่เป็นภาษาไทนแนะนำให้ใช้ เป็น font Sarabun ของ sipa
        BaseFont bf = BaseFont.CreateFont(Server.MapPath("~/Font/cordia.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

        Font font8 = new Font(bf, 10);
        Font header = new Font(bf, 14, Font.NORMAL);
        Font headTB = new Font(bf, 12, Font.NORMAL);
        Font headGroup = new Font(bf, 10, Font.NORMAL);
        Font headTBRed = new Font(bf, 12, Font.NORMAL, Color.RED);
        //string base64Image = txtHidePicture.Text;
        Document pdfDoc = new Document(PageSize.A4, 5, 5, 5, 5);
        string Date = lblsHead.Text + " " + lblsTail.Text;
        try
        {
            PdfWriter.GetInstance(pdfDoc, System.Web.HttpContext.Current.Response.OutputStream);
            pdfDoc.Open();

            Chunk sPace = new Chunk(" ", headTB);
            Paragraph psPace = new Paragraph();
            psPace.Alignment = Element.ALIGN_CENTER;
            psPace.Add(sPace);
            pdfDoc.Add(psPace);
            Chunk c = new Chunk("รายงานเวลาเข้ารับบริการแต่ละขั้นตอน", header);
            Paragraph p = new Paragraph();
            p.Alignment = Element.ALIGN_CENTER;
            p.Add(c);
            pdfDoc.Add(p);

            Chunk ccc = new Chunk(Date, headTB);
            Paragraph ppp = new Paragraph();
            ppp.Alignment = Element.ALIGN_CENTER;
            ppp.Add(ccc);
            pdfDoc.Add(ppp);

           
            psPace.Add(sPace);
            pdfDoc.Add(psPace);

            #region gvw1
            if (dt.Rows.Count > 0)
            {
                //Craete instance of the pdf table and set the number of column in that table 
                //PdfPTable PdfTable = new PdfPTable(11);
                PdfPTable PdfTable = new PdfPTable(11);
                PdfPCell PdfPCell = null;
                PdfTable.HeaderRows = 1;

                float[] widths = new float[] { 0.5f, 1f, 1f, 1.5f, 1.7f, 1f, 1f, 1f, 1f, 1f,1f };
                PdfTable.SetWidths(widths);

                //add head


                PdfPCell = new PdfPCell(new Phrase(new Chunk("ที่", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

                //PdfPCell = new PdfPCell(new Phrase(new Chunk("Unit", font8)));
                //PdfPCell.BackgroundColor = Color.ORANGE;
                //PdfPCell.HorizontalAlignment = 1;
                //PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk("ทะเบียนหัว", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk("ทะเบียนท้าย", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                PdfPCell = new PdfPCell(new Phrase(new Chunk("บริษัทผู้ข่นส่ง", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                PdfPCell = new PdfPCell(new Phrase(new Chunk("บริษัทผู้ข่นส่ง", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                PdfPCell = new PdfPCell(new Phrase(new Chunk("ยื่นคำขอ", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                PdfPCell = new PdfPCell(new Phrase(new Chunk("รข.อนุมัติ", font8)));
                //PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                PdfPCell = new PdfPCell(new Phrase(new Chunk("มว.จัดลงคิว", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk("นัดหมาย", font8)));
              //  PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk("จำนวนวัน", font8)));
                //PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);

                PdfPCell = new PdfPCell(new Phrase(new Chunk("เลื่อนคิว", font8)));
               // PdfPCell.BackgroundColor = Color.ORANGE;
                PdfPCell.HorizontalAlignment = 1;
                PdfTable.AddCell(PdfPCell);


                //PdfPTable nested = new PdfPTable(9);
                //PdfPTable nested2 = new PdfPTable(1);
                //PdfPTable nested3 = new PdfPTable(1);
                //PdfPTable nested4 = new PdfPTable(1);
                //PdfPTable nested5 = new PdfPTable(1);
                //PdfPTable nested6 = new PdfPTable(1);
                //PdfPTable nested7 = new PdfPTable(1);
                //PdfPTable nested8 = new PdfPTable(1);
                //PdfPTable nested9 = new PdfPTable(1);
                PdfPCell nesthousing = new PdfPCell();
                //add data
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    PdfPTable nested = new PdfPTable(9);

                    float[] widthss = new float[] { 0.5f, 1f, 1f, 1.5f, 1.7f, 1f, 1f, 1f, 1f };
                    nested.SetWidths(widthss);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["NO"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);



                    //PdfTable.AddCell(nested);


                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["VEH_NO"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);



                    //PdfTable.AddCell(nested2);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["TU_NO"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);


                    //PdfTable.AddCell(nested3);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["SABBREVIATION"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    nested.AddCell(PdfPCell);


                    // PdfTable.AddCell(nested4);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["REQTYPE_NAME"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    nested.AddCell(PdfPCell);

                    //PdfTable.AddCell(nested5);


                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["REQUEST_DATE"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    //PdfTable.AddCell(nested6);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["APPROVE_DATE"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    // PdfTable.AddCell(nested7);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["ACCEPT_DATE"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    //PdfTable.AddCell(nested8);
                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["SERVICE_DATE"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(" ", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["DateCal1"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["DateCal2"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    PdfPCell = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["DateCal3"] + "", font8)));
                    PdfPCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nested.AddCell(PdfPCell);

                    nesthousing = new PdfPCell(nested);
                    nesthousing.Padding = 0f;
                    nesthousing.Colspan = 9;
                    PdfTable.AddCell(nesthousing);

                    PdfPCell PdfPCellSUMDATE = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["SUMDATE"] + "", font8)));
                    PdfPCellSUMDATE.HorizontalAlignment = Element.ALIGN_CENTER;
                    PdfPCellSUMDATE.VerticalAlignment = Element.ALIGN_MIDDLE;
                    PdfTable.AddCell(PdfPCellSUMDATE);

                    PdfPCell PdfPCellEVENTMOVE = new PdfPCell(new Phrase(new Chunk(dt.Rows[i]["EVENTMOVE"] + "", font8)));
                    PdfPCellEVENTMOVE.HorizontalAlignment = Element.ALIGN_CENTER;
                    PdfPCellEVENTMOVE.VerticalAlignment = Element.ALIGN_MIDDLE;
                    PdfTable.AddCell(PdfPCellEVENTMOVE);




                    //PdfTable.AddCell(nesthousing);
                }


                pdfDoc.Add(PdfTable);  // add pdf table to the document  


            }

            #endregion


            //PdfPTable nested = new PdfPTable(1);
            //nested.AddCell("Nested Row 1");
            //nested.AddCell("Nested Row 2");
            //nested.AddCell("Nested Row 3");
            //PdfPCell nesthousing = new PdfPCell(nested);
            //nesthousing.Padding = 0f;
            //table.AddCell(nesthousing);
            //PdfPCell bottom = new PdfPCell(new Phrase("bottom"));
            //bottom.Colspan = 3;
            //table.AddCell(bottom);
            //doc.Add(table);

            pdfDoc.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename= รายงานเวลาเข้ารับบริการแต่ละขั้นตอน.pdf");
            System.Web.HttpContext.Current.Response.Write(pdfDoc);
            Response.Flush();
            Response.Close();
            Response.End();
            //HttpContext.Current.ApplicationInstance.CompleteRequest(); 
        }
        catch (DocumentException de)
        {
            System.Web.HttpContext.Current.Response.Write(de.Message);
        }
        catch (IOException ioEx)
        {
            System.Web.HttpContext.Current.Response.Write(ioEx.Message);
        }
        catch (Exception ex)
        {
            System.Web.HttpContext.Current.Response.Write(ex.Message);
        }
    }

    #region Structure
    public class SYEAR
    {
        public int NVALUE { get; set; }
        public string SVALUE { get; set; }
    }
    #endregion
}