﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_Complain_lst.aspx.cs" Inherits="admin_Complain_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxComboBox ID="cboSearchType" runat="server" ClientInstanceName="cboStatus"
                                Width="180px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="- เงื่อนไขค้นหา -" />
                                    <dx:ListEditItem Text="หมายเลขเอกสาร" Value="DOC_ID" />
                                    <dx:ListEditItem Text="เลขที่สัญญา" Value="SCONTRACTNO" />
                                    <dx:ListEditItem Text="ทะเบียนรถ" Value="SHEADREGISTERNO" />
                                    <dx:ListEditItem Text="ชื่อผู้ร้องเรียน" Value="SCOMPLAINFNAME" />
                                    <dx:ListEditItem Text="ชื่อผู้ประกอบการ" Value="SVENDORNAME" />
                                    <dx:ListEditItem Text="กลุ่มที่" Value="M_GROUPS.NAME" />
                                    <dx:ListEditItem Text="รหัส,ชื่อ-นามสกุล พขร." Value="DRIVER" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="150px">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtCreateBy" NullText="ชื่อผู้บันทึก" runat="server" Width="140px">
                            </dx:ASPxTextBox>
                        </td>
                        <td style="width:70px">
                            <asp:Label ID="lblDate" runat="server" Text="วันที่เกิดเหตุ"></asp:Label>
                        </td>
                        <td>
                           <%-- <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <%--<dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>--%>
                            <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                        </td>
                        
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" OnClick="btnSearch_Click">
                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" >
                                </ClientSideEvents>--%>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus" ValueField="DOC_STATUS_ID"
                                Width="180px" SelectedIndex="0">
                                <Columns>
                                    <dx:ListBoxColumn Caption="สถานะเอกสาร" FieldName="DOC_STATUS_NAME" Width="220px" />
                                </Columns>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboComplainType" runat="server" ClientInstanceName="cboComplainType" ValueField="COMPLAIN_TYPE_ID"
                                Width="150px" SelectedIndex="0">
                                <Columns>
                                    <dx:ListBoxColumn Caption="หัวข้อร้องเรียน" FieldName="COMPLAIN_TYPE_NAME" Width="600px" />
                                </Columns>
                            </dx:ASPxComboBox>
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="chkIsCorrupt" runat="server" Text="เอกสารที่ พขร. ทุจริต" />
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="chkIsUrgent" runat="server" Text="เอกสารที่แจ้งล่าช้า" />
                        </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                        <td align="left" style="padding-right:3px" colspan="2">
                            <asp:Label ID="lblUrgent" runat="server" Text="เอกสารที่ตัวหนังสือสีแดง หมายถึง เอกสารแจ้งล่าช้า" ForeColor="Red" Font-Underline="true"></asp:Label>
                        </td>
                        <td align="left" style="padding-right:3px" colspan="2">
                            <asp:Label ID="lblContractID" runat="server" Text="ผลการค้นหา เลขที่สัญญา {0}" Visible="false" ForeColor="Green" Font-Underline="true"></asp:Label>
                        </td>
                        <td align="right" style="padding-right:3px" colspan="7">
                            จำนวนรายการเรื่องร้องเรียน&nbsp;<dx:ASPxLabel ID="lblCarCount" ClientInstanceName="lblCarCount"
                                runat="server" Text="0" Font-Bold="true" ForeColor="Red">
                            </dx:ASPxLabel>
                            &nbsp;รายการ
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <asp:GridView ID="dgvComplain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                             ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false"
                                EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True"
                                OnPageIndexChanging="dgvComplain_PageIndexChanging" DataKeyNames="DOC_ID" 
                                OnRowUpdating="dgvComplain_RowUpdating">
<AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="DOC_ID" HeaderText="หมายเลขเอกสาร">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="DDATECOMPLAIN" HeaderText="วันที่รับเรื่อง">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="GROUPNAME" HeaderText="กลุ่มที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DDELIVERY" HeaderText="วันที่เกิดเหตุ">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SVENDORNAME" HeaderText="ผู้ประกอบการ">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียนรถ (หัว)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="STRAILERREGISTERNO" HeaderText="ทะเบียนรถ (หาง)">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="CREATENAME" HeaderText="ผู้บันทึก">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="STATUS" HeaderText="สถานะ">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/btnEdit.png" Width="23"
                                                Height="23" Style="cursor: pointer" CommandName="update" /><%--&nbsp;
                                            <asp:ImageButton ID="imgCancelDoc" runat="server" ImageUrl="~/Images/btnDelete.png"
                                                Width="23px" Height="23px" Style="cursor: pointer" CommandName="CancelDoc" />--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
<EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

<HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
<PagerStyle CssClass="pagination-ys" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                            </asp:GridView>
                            <br />

                            <%--<dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                KeyFieldName="SSERVICEID" SkinID="_gvw" Style="margin-top: 0px" Width="100%"
                                OnCellEditorInitialize="gvw_CellEditorInitialize" OnDataBound="gvw_DataBound"
                                OnRowInserted="gvw_RowInserted" OnRowInserting="gvw_RowInserting">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SSERVICEID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมายเลขเอกสาร" FieldName="DOC_ID" ShowInCustomizationForm="True"
                                        VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่รับเรื่อง" FieldName="DCREATE" VisibleIndex="3">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DDELIVERY" VisibleIndex="4">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="6">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="10">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="11">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="เรื่องที่ร้องเรียน" FieldName="SHEADCOMLIAINNAME"
                                        ShowInCustomizationForm="True" VisibleIndex="5" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัวข้อการหักคะแนน" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="ตัดคะแนน" FieldName="CUTSCORE"
                                     ShowInCustomizationForm="True" VisibleIndex="6" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ระยะเวลา<br>ดำเนินการ" FieldName="DCLOSE" ShowInCustomizationForm="True" 
                                    VisibleIndex="7" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้บันทึก" FieldName="CREATENAME" ShowInCustomizationForm="True"
                                        VisibleIndex="8">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" ShowInCustomizationForm="True"
                                        VisibleIndex="9">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="10" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="False" CausesValidation="False"
                                                Text="แสดงข้อมูล">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>


                            
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox runat="server" CheckState="Unchecked" ToolTip="Select/Unselect all rows on the page" ID="ASPxCheckBox1">
<ClientSideEvents CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }"></ClientSideEvents>
</dx:ASPxCheckBox>

                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หมายเลขเอกสาร" FieldName="DOC_ID" ShowInCustomizationForm="True"
                                        VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่รับเรื่อง" FieldName="DCREATE" VisibleIndex="3">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่เกิดเหตุ" FieldName="DDELIVERY" VisibleIndex="4">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                        VisibleIndex="6">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="7">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="10">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="11">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    
                                    
                                    <dx:GridViewDataTextColumn Caption="ผู้บันทึก" FieldName="CREATENAME" ShowInCustomizationForm="True"
                                        VisibleIndex="8">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" ShowInCustomizationForm="True"
                                        VisibleIndex="9">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="10" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton runat="server" AutoPostBack="False" CausesValidation="False" Text="แสดงข้อมูล" ID="imbedit">
<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback(&#39;edit;&#39;+s.name.substring(s.name.lastIndexOf(&#39;_&#39;)+1,s.name.length)); }"></ClientSideEvents>
</dx:ASPxButton>

                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>--%>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" OnDeleted="sds_Deleted" OnDeleting="sds_Deleting"
                                DeleteCommand="DELETE FROM TCOMPLAIN WHERE SSERVICEID = :SSERVICEID">
                                <DeleteParameters>
                                    <asp:SessionParameter Name="SSERVICEID" SessionField="delSSERVICEID" Type="String" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <%--<td>
                            <dx:ASPxButton ID="btnDel" runat="server" Text="ยกเลิกเอกสาร" ForeColor="Red">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>--%>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
