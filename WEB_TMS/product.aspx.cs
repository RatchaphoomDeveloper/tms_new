﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxGridView;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;

public partial class product : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;


    protected void Page_Init(object sender, EventArgs e)
    {
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
        //if (!IsPostBack)
        //{
        //    Listdata();
        //}
        Listdata();

    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //if (e.DataColumn.Caption == "กลุ่ม<br>ผลิตภัณฑ์")
        //{
        //    ASPxComboBox cbo = gvw.FindEditRowCellTemplateControl(null, "cboPROD_CATEGORY") as ASPxComboBox;
        //    cbo.SelectedIndex = 1;
        //}
    }

    void gvw_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        string Chkin = e.GetValue("CHECKIN") + "";
        if (Chkin == "1")
        {
            e.Row.BackColor = System.Drawing.Color.White;
        }
        else
        {
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        gvw.StartRowEditing += new DevExpress.Web.Data.ASPxStartRowEditingEventHandler(gvw_StartRowEditing);
        gvw.HtmlRowPrepared += new ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);


        if (!IsPostBack)
        {
            #region เช็ค Permission
            //  ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("48", "R", "เปิดดูข้อมูลหน้า ข้อมูลผลิตภัณฑ์", "");
           
            #endregion
            //Cache.Remove(sdspSap.CacheKeyDependency);
            //Cache[sdspSap.CacheKeyDependency] = new object();
            //sdspSap.Select(new System.Web.UI.DataSourceSelectArguments());
            //sdspSap.DataBind();
            ////cboSAP.TextField = "";
            ////cboSAP.ValueField = "PROD_ID";
            //cboSAP.DataBind();

            //cboSAP.DataBind();
            //cboSAP.Items.Insert(0, new ListEditItem("-เลือกกลุ่มผลิตภัณฑ์(SAP)-", "0"));
            cboTMS.DataBind();
            cboTMS.Items.Insert(0, new ListEditItem("-เลือกกลุ่มผลิตภัณฑ์(TMS)-", ""));
            cboTMS.SelectedIndex = 0;
        }
        Listdata();
    }

    void gvw_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {

        string ProID = gvw.GetRowValuesByKeyValue(e.EditingKeyValue, "PROD_ID") + "";

        #region checkว่าเคยเข้ามาดุข้อมูลไหม
        string strCheckdata = @"SELECT PROD_ID,CHECKIN
                                            FROM TPRODUCT WHERE PROD_ID = '" + CommonFunction.ReplaceInjection(ProID) + @"' AND CHECKIN ='1' ";


        DataTable dtchk = CommonFunction.Get_Data(conn, strCheckdata);
        if (dtchk.Rows.Count > 0)
        {

        }
        else
        {
            string strCompany = @"UPDATE TPRODUCT
                                         SET CHECKIN=:cCheckIn
                                         WHERE PROD_ID = '" + CommonFunction.ReplaceInjection(ProID) + "'";
            using (OracleConnection con = new OracleConnection(conn))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }
                DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    //AddhistoryVendor(ProID);
                    using (OracleCommand com1 = new OracleCommand(strCompany, con))
                    {
                        com1.Parameters.Clear();
                        com1.Parameters.Add(":cCheckIn", OracleType.Char).Value = "1";
                        com1.ExecuteNonQuery();
                    }
                }
            }
        }

        #endregion

    }

    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ความหนาแน่น<br>(Kg\\m3)")
        {
            if (string.IsNullOrEmpty(e.Value + ""))
            {

            }
            else
            {
                decimal Ovaluse = Convert.ToDecimal(e.Value);
                if (Ovaluse == 0)
                {
                    e.DisplayText = "";
                }
                else
                {
                    e.DisplayText = Ovaluse.ToString("#,##0.00");
                }
            }
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});");
        }
        else
        {
            string[] param = e.Parameter.Split(';');
            int num = 0;
            switch (param[0])
            {
                case "Search": Listdata();
                    break;
                case "CancelSearch":
                    txtSearch.Text = "";

                    //cboSAP.DataBind();
                    //cboSAP.Items.Insert(0, new ListEditItem("-เลือกกลุ่มผลิตภัณฑ์(SAP)-", "0"));
                    //cboTMS.DataBind();
                    //cboTMS.Items.Insert(0, new ListEditItem("-เลือกกลุ่มผลิตภัณฑ์(TMS)-", "0"));
                    cboTMS.SelectedIndex = 0;
                    cboSAP.Value = "";
                    Listdata();
                    break;
                case "viewHistoryPro":
                    int index = int.TryParse(param[1] + "", out num) ? num : 0;
                    dynamic ID = gvw.GetRowValues(index, "PROD_ID");

                    xcpn.JSProperties["cpRedirectTo"] = "product_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + ID));
                    break;
            }
        }
    }

    void Listdata()
    {
        string Condition = "";
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition = " AND PRO.PROD_ID|| PROSAP.PROD_NAME||PROSAP.PROD_GRP_NAME LIKE UPPER('%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%') OR PRO.PROD_ID|| PROSAP.PROD_NAME||PROSAP.PROD_GRP_NAME LIKE LOWER('%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%')";
        }

        string Condition2 = "";

        if (!string.IsNullOrEmpty(cboSAP.Value + ""))
        {
            Condition2 += " AND PRO.PROD_ID ='" + CommonFunction.ReplaceInjection(cboSAP.Value + "") + "'";
        }

        if (!string.IsNullOrEmpty(cboTMS.Value + ""))
        {
            Condition2 += " AND PRO.SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(cboTMS.Value + "") + "'";
        }

        string strsql = @"SELECT 
  PRO.PROD_ID, PROSAP.PROD_NAME,PROSAP.PROD_GRP_NAME,PRO.PROD_CATEGORY,PRO.SPRODUCTTYPEID
  ,PRO.PROD_ABBR,PRO.DENSITY,NVL(PRO.LANDTRANSPORT,'0') as LANDTRANSPORT,NVL(PRO.WATERTRANSPORT,'0') as WATERTRANSPORT
  ,NVL(PRO.PIPELINETRANSPORT,'0') as PIPELINETRANSPORT ,NVL(PRO.TRAINTRANSPORT,'0') AS TRAINTRANSPORT,PRO.LANDTRANSPORT AS LAND,PRO.WATERTRANSPORT AS WATER,PRO.PIPELINETRANSPORT AS PIPE,PRO.TRAINTRANSPORT AS TRAIN ,PRO.PROD_ABBR_V ,PRO.CHECKIN
FROM TPRODUCT PRO
LEFT JOIN TPRODUCT_SAP PROSAP
ON  PROSAP.PROD_ID = PRO.PROD_ID
LEFT JOIN  TPRODUCTTYPE STYPE
ON STYPE.SPRODUCTTYPEID = PRO.PROD_CATEGORY
WHERE 1=1 " + Condition + @" " + Condition2 + @" 
ORDER BY PRO.DATE_UPDATED DESC";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);

        gvw.DataSource = dt;
        gvw.DataBind();
    }

    protected void chk_Init(object sender, EventArgs e)
    {
        ASPxCheckBox chk = sender as ASPxCheckBox;
        GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;

        chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ cb.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue);
    }

    protected void gvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {


        switch (e.CallbackName)
        {
            case "STARTEDIT":
                // ASPxComboBox cbo = gvw.FindEditRowCellTemplateControl(null, "cboPROD_CATEGORY") as ASPxComboBox;

                //switch (Values)
                //{

                //}
                // cbo.Value = Values;

                //cbo.SelectedIndex = 0;
                break;
            case "CUSTOMCALLBACK":
                string[] param = e.Args[0].Split(';');
                int index = 0;
                if (!string.IsNullOrEmpty(param[1]))
                {
                    index = int.Parse(param[1]);
                }

                if (CanWrite)
                {
                    switch (param[0])
                    {

                        case "Startedit":

                           
                            gvw.StartEdit(index);
                            ASPxComboBox cbo = gvw.FindEditRowCellTemplateControl(null, "cboPROD_CATEGORY") as ASPxComboBox;
                            string Values = gvw.GetRowValues(index, "PROD_CATEGORY") + "";
                            // cbo.Value = Values;
                            cbo.Value = cbo.Items.FindByText(Values) + "" == "" ? "" : cbo.Items.FindByText(Values).Value;
                            break;
                        case "Updateedit":

                            gvw.UpdateEdit();

                            //gvw.CancelEdit();
                            break;
                        case "cancel":
                            break;
                    }
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(gvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

        }

    }

    protected void gvw_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        object keyValue = gvw.GetRowValues(gvw.EditingRowVisibleIndex, "PROD_ID");
        string sPROD_ID = keyValue + "";

        string USER = Session["UserID"] + "";
        ASPxLabel lblPROD_ID = gvw.FindEditRowCellTemplateControl(null, "lblPROD_ID") as ASPxLabel;
        ASPxLabel lblPROD_NAME = gvw.FindEditRowCellTemplateControl(null, "lblPROD_NAME") as ASPxLabel;
        ASPxLabel lblPROD_GRP_NAME = gvw.FindEditRowCellTemplateControl(null, "lblPROD_GRP_NAME") as ASPxLabel;
        //ASPxTextBox txtPROD_CATEGORY = gvw.FindEditRowCellTemplateControl(null, "txtPROD_CATEGORY") as ASPxTextBox;
        ASPxTextBox txtPROD_ABBR = gvw.FindEditRowCellTemplateControl(null, "txtPROD_ABBR") as ASPxTextBox;
        ASPxTextBox txtDENSITY = gvw.FindEditRowCellTemplateControl(null, "txtDENSITY") as ASPxTextBox;
        ASPxTextBox txtPROD_ABBR_V = gvw.FindEditRowCellTemplateControl(null, "txtPROD_ABBR_V") as ASPxTextBox;
        ASPxCheckBox chkLANDTRANSPORT = gvw.FindEditRowCellTemplateControl(null, "chkLANDTRANSPORT") as ASPxCheckBox;
        ASPxCheckBox chkWATERTRANSPORT = gvw.FindEditRowCellTemplateControl(null, "chkWATERTRANSPORT") as ASPxCheckBox;
        ASPxCheckBox chkPIPELINETRANSPORT = gvw.FindEditRowCellTemplateControl(null, "chkPIPELINETRANSPORT") as ASPxCheckBox;
        ASPxCheckBox chkTRAINTRANSPORT = gvw.FindEditRowCellTemplateControl(null, "chkTRAINTRANSPORT") as ASPxCheckBox;
        ASPxComboBox cboPROD_CATEGORY = gvw.FindEditRowCellTemplateControl(null, "cboPROD_CATEGORY") as ASPxComboBox;


        //string PROD_ID = lblPROD_ID.Text;
        //string PROD_NAME = lblPROD_NAME.Text;
        //string PROD_GRP = lblPROD_GRP_NAME.Text;
        //string PROD_CATEGORY = cboPROD_CATEGORY.Text;
        //string PROD_ABBR = txtPROD_ABBR.Text;
        //string DENSITY = txtDENSITY.Text;
        //string PROD_ABBR_V = txtPROD_ABBR_V.Text;
        if (CanWrite)
        {



            string Codition = "";
            string PROD_ID = lblPROD_ID.Text;
            if (!string.IsNullOrEmpty(PROD_ID))
            {
                Codition += " AND PRO.PROD_ID = '" + CommonFunction.ReplaceInjection(PROD_ID) + @"'";
            }

            string PROD_NAME = lblPROD_NAME.Text;

            if (!string.IsNullOrEmpty(PROD_NAME))
            {
                Codition += " AND PROSAP.PROD_NAME = '" + CommonFunction.ReplaceInjection(PROD_NAME) + @"'";
            }
            string PROD_GRP = lblPROD_GRP_NAME.Text;
            if (!string.IsNullOrEmpty(PROD_GRP))
            {
                Codition += " AND PROSAP.PROD_GRP_NAME = '" + CommonFunction.ReplaceInjection(PROD_GRP) + @"'";
            }

            string PROD_CATEGORY = cboPROD_CATEGORY.Text;
            if (!string.IsNullOrEmpty(PROD_CATEGORY))
            {
                Codition += " AND PRO.PROD_CATEGORY = '" + CommonFunction.ReplaceInjection(PROD_CATEGORY) + @"'";
            }

            string PROD_ABBR = txtPROD_ABBR.Text;
            if (!string.IsNullOrEmpty(PROD_GRP))
            {
                Codition += " AND PRO.PROD_ABBR = '" + CommonFunction.ReplaceInjection(PROD_ABBR) + @"'";
            }

            string DENSITY = txtDENSITY.Text;
            if (!string.IsNullOrEmpty(DENSITY))
            {
                Codition += " AND PRO.DENSITY = '" + CommonFunction.ReplaceInjection(DENSITY) + @"'";
            }

            string PROD_ABBR_V = txtPROD_ABBR_V.Text;
            if (!string.IsNullOrEmpty(PROD_ABBR_V))
            {
                Codition += " AND PRO.PROD_ABBR_V = '" + CommonFunction.ReplaceInjection(PROD_ABBR_V) + @"'";
            }


            dynamic TRANSPORT = gvw.GetRowValues(gvw.EditingRowVisibleIndex, "LAND", "WATER", "PIPE", "TRAIN");

            string LANDTRANSPORT = chkLANDTRANSPORT.Checked == true ? "1" : "0";
            if (!string.IsNullOrEmpty(TRANSPORT[0] + ""))
            {
                Codition += "AND PRO.LANDTRANSPORT ='" + CommonFunction.ReplaceInjection(LANDTRANSPORT) + @"'";
            }

            string WATERTRANSPORT = chkWATERTRANSPORT.Checked == true ? "1" : "0";
            if (!string.IsNullOrEmpty(TRANSPORT[1] + ""))
            {
                Codition += "AND PRO.WATERTRANSPORT ='" + CommonFunction.ReplaceInjection(WATERTRANSPORT) + @"'";
            }

            string PIPELINETRANSPORT = chkPIPELINETRANSPORT.Checked == true ? "1" : "0";
            if (!string.IsNullOrEmpty(TRANSPORT[2] + ""))
            {
                Codition += "AND PRO.PIPELINETRANSPORT ='" + CommonFunction.ReplaceInjection(PIPELINETRANSPORT) + @"'";
            }

            string TRAINTRANSPORT = chkTRAINTRANSPORT.Checked == true ? "1" : "0";
            if (!string.IsNullOrEmpty(TRANSPORT[3] + ""))
            {
                Codition += "AND PRO.TRAINTRANSPORT ='" + CommonFunction.ReplaceInjection(TRAINTRANSPORT) + @"'";
            }

            string SPRODUCTTYPEID = cboPROD_CATEGORY.Value + "" == "" ? null : cboPROD_CATEGORY.SelectedItem.Value.ToString();
            if (!string.IsNullOrEmpty(cboPROD_CATEGORY.Value + ""))
            {
                Codition += "  AND PRO.SPRODUCTTYPEID ='" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + "'";
            }


            dynamic productvalue = gvw.GetRowValues(gvw.VisibleStartIndex, "PROD_ID", "PROD_NAME", "PROD_GRP_NAME", "PROD_CATEGORY", "PROD_ABBR", "DENSITY", "PROD_ABBR_V");
            using (OracleConnection con = new OracleConnection(conn))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }

                //            string chkData = @"SELECT PRO.PROD_ID, PROSAP.PROD_NAME,PROSAP.PROD_GRP_NAME,PRO.PROD_CATEGORY
                //                                                      ,PRO.PROD_ABBR,PRO.DENSITY,PRO.LANDTRANSPORT,PRO.WATERTRANSPORT
                //                                                      ,PRO.PIPELINETRANSPORT,PRO.TRAINTRANSPORT,PRO.PROD_ABBR_V
                //                                                      FROM TPRODUCT PRO
                //                                                      LEFT JOIN TPRODUCT_SAP PROSAP
                //                                                      ON  PROSAP.PROD_ID = PRO.PROD_ID 
                //                                                      WHERE PRO.PROD_ID = '" + CommonFunction.ReplaceInjection(sPROD_ID) + "'";

                string chkUpdate = @"SELECT PRO.PROD_ID, PROSAP.PROD_NAME,PROSAP.PROD_GRP_NAME,PRO.PROD_CATEGORY
                                                        ,PRO.PROD_ABBR,PRO.DENSITY,PRO.LANDTRANSPORT,PRO.WATERTRANSPORT
                                                        ,PRO.PIPELINETRANSPORT,PRO.TRAINTRANSPORT,PRO.PROD_ABBR_V,PRO.SPRODUCTTYPEID
                                                        FROM TPRODUCT PRO
                                                        LEFT JOIN TPRODUCT_SAP PROSAP
                                                        ON  PROSAP.PROD_ID = PRO.PROD_ID
                                                        WHERE 1=1
                                                        " + Codition + "";

                //DataTable dt = new DataTable();
                //dt = CommonFunction.Get_Data(conn, chkData);

                DataTable dt2 = new DataTable();
                dt2 = CommonFunction.Get_Data(conn, chkUpdate);
                //if (dt.Rows.Count > 0)
                //{
                //ถ้ามีค่าให้ทำการอับเดทข้อมูล
                if (dt2.Rows.Count > 0)
                {

                }
                else
                {
                    AddhistoryPRODUCT(sPROD_ID);
                    LogUser("48", "E", "แก้ไขข้อมูลพนักงาน บันทึกผลิตภัณฑ์", sPROD_ID);
                    string sqlupdate = @"UPDATE TPRODUCT
                                         SET PROD_ABBR='" + CommonFunction.ReplaceInjection(PROD_ABBR) + @"'
                                            ,DENSITY='" + CommonFunction.ReplaceInjection(DENSITY) + @"'
                                            ,PROD_CATEGORY='" + CommonFunction.ReplaceInjection(PROD_CATEGORY) + @"'
                                            ,SPRODUCTTYPEID='" + CommonFunction.ReplaceInjection(SPRODUCTTYPEID) + @"'
                                            ,LANDTRANSPORT='" + CommonFunction.ReplaceInjection(LANDTRANSPORT) + @"'
                                            ,WATERTRANSPORT='" + CommonFunction.ReplaceInjection(WATERTRANSPORT) + @"'
                                            ,PIPELINETRANSPORT='" + CommonFunction.ReplaceInjection(PIPELINETRANSPORT) + @"'
                                            ,TRAINTRANSPORT='" + CommonFunction.ReplaceInjection(TRAINTRANSPORT) + @"'
                                            ,PROD_ABBR_V='" + CommonFunction.ReplaceInjection(PROD_ABBR_V) + @"'
                                            ,USER_UPDATED='" + CommonFunction.ReplaceInjection(USER) + @"'
                                            ,DATE_UPDATED = sysdate
                                         WHERE PROD_ID = '" + CommonFunction.ReplaceInjection(sPROD_ID) + "'";
                    using (OracleCommand com1 = new OracleCommand(sqlupdate, con))
                    {
                        com1.ExecuteNonQuery();
                    }
                }
                //}
                //else
                //{
                //    //ถ้าไม่มีค่าให้ทำการInsertข้อมูล
                //    string sqlinsert = "";
                //    using (OracleCommand com1 = new OracleCommand(sqlinsert, con))
                //    {


                //    }
                //}



            }
            Listdata();
            gvw.CancelEdit();
            e.Cancel = true;
        }
        else
        {
            CommonFunction.SetPopupOnLoad(gvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    private void AddhistoryPRODUCT(string sPROD_ID)
    {


        string senddatatohistory = @"SELECT PRO.PROD_ID, PROSAP.PROD_NAME,PROSAP.PROD_GRP_NAME,PRO.PROD_CATEGORY
                                                      ,PRO.PROD_ABBR,PRO.DENSITY,PRO.LANDTRANSPORT,PRO.WATERTRANSPORT
                                                      ,PRO.PIPELINETRANSPORT,PRO.TRAINTRANSPORT,PRO.PROD_ABBR_V
                                                      ,PRO.DATE_CREATED,PRO.USER_CREATED,PRO.DATE_UPDATED,PRO.USER_UPDATED,PRO.PORT_FLAG,PRO.SPRODUCTTYPEID
                                                      FROM TPRODUCT PRO
                                                      LEFT JOIN TPRODUCT_SAP PROSAP
                                                      ON  PROSAP.PROD_ID = PRO.PROD_ID 
                                                      WHERE PRO.PROD_ID = '" + CommonFunction.ReplaceInjection(sPROD_ID) + "'";




        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TPRODUCT_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT PROD_ID, PROD_ABBR, DENSITY, DATE_CREATED, USER_CREATED, DATE_UPDATED, 
                                        USER_UPDATED, PROD_CATEGORY, PORT_FLAG, PROD_ABBR_V, LANDTRANSPORT, WATERTRANSPORT, 
                                        PIPELINETRANSPORT, TRAINTRANSPORT, NVERSION ,SPRODUCTTYPEID
                                        FROM TPRODUCT_HISTORY
                                        WHERE PROD_ID = '" + CommonFunction.ReplaceInjection(sPROD_ID) + @"'
                                        ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";


                    string strQuery = @"INSERT INTO TPRODUCT_HISTORY(PROD_ID,PROD_ABBR,DENSITY,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED
                                      ,PROD_CATEGORY,PORT_FLAG,PROD_ABBR_V,LANDTRANSPORT,WATERTRANSPORT,PIPELINETRANSPORT,TRAINTRANSPORT,NVERSION,SPRODUCTTYPEID) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ABBR"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DENSITY"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_CATEGORY"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PORT_FLAG"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ABBR_V"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["LANDTRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["WATERTRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PIPELINETRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["TRAINTRANSPORT"] + "") + @"'                           
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPRODUCTTYPEID"] + "") + @"'     ) ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string strQuery = @"INSERT INTO TPRODUCT_HISTORY(PROD_ID,PROD_ABBR,DENSITY,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED
                                      ,PROD_CATEGORY,PORT_FLAG,PROD_ABBR_V,LANDTRANSPORT,WATERTRANSPORT,PIPELINETRANSPORT,TRAINTRANSPORT,NVERSION,SPRODUCTTYPEID) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ABBR"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DENSITY"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_CATEGORY"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PORT_FLAG"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PROD_ABBR_V"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["LANDTRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["WATERTRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PIPELINETRANSPORT"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["TRAINTRANSPORT"] + "") + @"'                           
                                  ,1
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPRODUCTTYPEID"] + "") + "') ";

                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void cboSAP_ItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        //        sdspSap.SelectCommand = @"SELECT PROD_ID , PROD_GRP_NAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY PROD_ID) AS RN , PROD_ID , PROD_GRP_NAME
        //          FROM TPRODUCT_SAP WHERE PROD_GRP_NAME LIKE :fillter   ) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdspSap.SelectCommand = @"SELECT PROD_ID , PROD_GRP_NAME FROM
 (
SELECT ROW_NUMBER() OVER(ORDER BY PROD_ID) AS RN , PROD_ID , PROD_GRP_NAME
FROM
 (
SELECT PROD_ID , PROD_GRP_NAME FROM TPRODUCT_SAP WHERE PROD_GRP_NAME IS NOT NULL
) WHERE PROD_GRP_NAME LIKE :fillter 
) WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdspSap.SelectParameters.Clear();
        sdspSap.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdspSap.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdspSap.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdspSap;
        comboBox.DataBind();

    }

    protected void cboSAP_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void grid_DataBinding(object sender, EventArgs e)
    {
        //var objHenTaiKham = gvw.GetRowValuesByKeyValue()(dtFromDate, dtToDate, intKhoa_Id, intNhanVien_Id, intNhomDichVu_Id);
        //(sender as ASPxGridView).DataSource = objHenTaiKham;
        //(sender as ASPxGridView).ForceDataRowType(typeof(YOUR_DATA_TYPE));
        //gvw.ForceDataRowType(typeof(System.Data.DataRowView));
        ////(sender as ASPxGridView).ForceDataRowType(typeof(string));
    }
}