﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Data;

public partial class LockCar_SAP : System.Web.UI.Page
{
    string cSAP_SYNC = WebConfigurationManager.AppSettings["SAP_SYNCOUT"].ToString();
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        string QUERY = @"SELECT REQUEST_ID,VEH_ID as STRUCKID,CASE WHEN WATEROFF_DATE  < SYSDATE AND STATUS_FLAG ='06' THEN 'N' ELSE 'Y' END as LOCK_REQ 
        FROM  TBL_REQUEST 
        WHERE CASE WHEN WATEROFF_DATE  < SYSDATE AND STATUS_FLAG ='06' THEN 'N' ELSE 'Y' END = 'N'";

        //string QUERY = @"SELECT STRUCKID FROM TTRUCK WHERE STRUCKID IN ('TR5501258','TR5501259')";
        //string QUERY = @"SELECT STRUCKID FROM TTRUCK WHERE STRUCKID IN ('TR5500478')";



        DataTable dt = CommonFunction.Get_Data(conn, QUERY);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string STRUCKID = dt.Rows[i]["STRUCKID"] + "";
                Update_TTRUCK(STRUCKID);
            }

            
        }
        else
        {

        }


        ScriptManager.RegisterStartupScript(Page, GetType(), "JsStatus", "window_close()", true);
    }

    private void Update_TTRUCK(string STRUCK)
    {
        string QUERY = @"UPDATE TTRUCK
                        SET    CACTIVE              = 'N'
                        WHERE  STRUCKID             = '" + CommonFunction.ReplaceInjection(STRUCK) + "'";

        SystemFunction.SQLExecuteNonQuery(conn, QUERY);

        string QUERY_TYPE = @"SELECT STRUCKID,SCARTYPEID FROM TTRUCK WHERE STRUCKID = '" + CommonFunction.ReplaceInjection(STRUCK) + "'";

        DataTable dt = CommonFunction.Get_Data(conn, QUERY_TYPE);

        if (dt.Rows.Count > 0)
        {
            string SCARTYPE = dt.Rows[0]["SCARTYPEID"] + "";
            Lock_SAP(STRUCK, SCARTYPE);

        }

       
    }

    private void Lock_SAP(string STRUCK, string ScarType)
    {
        string msg = "";
        string sMsg = "";

        //string VEH_NO = CommonFunction.Get_Value(new OracleConnection(conn), "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");

        DataTable dt_VEH = CommonFunction.Get_Data(conn, "SELECT SHEADREGISTERNO FROM TTRUCK WHERE STRUCKID='" + STRUCK + "'");
        string VEH_NO = dt_VEH.Rows.Count > 0 ? dt_VEH.Rows[0]["SHEADREGISTERNO"] + "" : "";
        if (VEH_NO.Trim() != "" && cSAP_SYNC == "1") // cSAP_SYNC = 1 ให้มีการ Syncข้อมูลไปยัง SAP
        {

           
            SAP_SYNCOUT_SI veh_syncout = new SAP_SYNCOUT_SI();
            veh_syncout.VEH_NO = VEH_NO + "";
            sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
            if (sMsg != "")
            {
                SAP_CREATE_VEH veh_create = new SAP_CREATE_VEH();
                //กรณีที่มีการอัพเดทข้อมูลรถ แต่รถยังไม่มีใน SAP
                if (sMsg.Contains("does not exist"))
                {
                  
                    //กรณีที่หัวไม่มีใน SAP
                    if (sMsg.Contains("Entry " + VEH_NO + " does not exist"))
                    {
                        //รถบรรทุก
                        if (ScarType == "0")
                        {
                            veh_create.sVehicle = VEH_NO + "";
                            sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                            if (sMsg.Contains("Transport unit " + VEH_NO + " already exists"))
                            {
                                veh_create.sVehicle = VEH_NO + "";
                                sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                                if (sMsg == "")
                                {
                                    sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                                }
                            }
                        }
                        else
                        {
                            veh_create.sVehicle = VEH_NO + "";
                            sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                            if (sMsg == "")
                            {
                                sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                            }
                        }
                    }
                    else
                    {
                        veh_create.sVehicle = VEH_NO + "";
                        sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                        if (sMsg == "")
                        {
                            sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                        }
                    }
                }
                else
                {
                    veh_create.sVehicle = VEH_NO + "";
                    sMsg = veh_create.CRT_Vehicle_SYNC_OUT_SI();
                    if (sMsg == "")
                    {
                        sMsg = veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
                    }
                }
            }
            SystemFunction.SynSapError("LockCar_SAP", sMsg);
            if (sMsg == "")
                msg += "ผ่าน";
            else
                msg += "ไม่ผ่าน";

        }
    }


}