﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ptttmsModel;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxCallbackPanel;
using System.Data.Common;
using System.Data.OracleClient;
using System.Web.Configuration;

public partial class depo_terminal_remain_lst : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        #region EventHandler
        gvw.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvw_CustomColumnDisplayText);
       
        #endregion
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            Cache.Remove(sds.CacheKeyDependency);
            Cache[sds.CacheKeyDependency] = new object();
            Session["NCARBANID"] = null;

            LogUser("16", "R", "เปิดดูข้อมูลหน้า รถตกค้างคลังปลายทาง", "");

        }

    }


    protected void xcpn_Load(object sender, EventArgs e)
    {
          
        gvw.DataBind();

    }
    protected void xcpn_Callback1(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {
            case "search":

                Cache.Remove(sds.CacheKeyDependency);
                Cache[sds.CacheKeyDependency] = new object();
                break;

            case "delete":
                var ld = gvw.GetSelectedFieldValues("ID1", "NCARBANID", "SOUTBOUNDNO", "SVENDORNAME", "SHEADREGISTERNO", "STRAILERREGISTERNO", "DBAN", "DUNBAN", "SREMARK")
                    .Cast<object[]>()
                    .Select(s => new {ID1 = s[0].ToString(), NCARBANID = s[1].ToString(), SOUTBOUNDNO = s[2].ToString(), SVENDORNAME = s[3].ToString(), SHEADREGISTERNO = s[4].ToString(), STRAILERREGISTERNO = s[5].ToString(), DBAN = s[6].ToString(), DUNBAN = s[7].ToString(), SREMARK = s[8].ToString() });
                
                    string delid = "";
                using (OracleConnection con = new OracleConnection(sql))
                {
                    con.Open();
                    foreach (var l in ld)
                    {
                        Session["oNCARBANID"] = l.NCARBANID;
                        sds.Delete();

                        delid += l.NCARBANID + ",";

                        string strsql1 = "DELETE FROM TCARBANLIST WHERE NCARBANID = " + l.NCARBANID ;
                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {
                            com1.ExecuteNonQuery();
                        }
                    }
                }

                LogUser("16", "D", "ลบข้อมูลหน้า รถตกค้างคลังปลายทาง รหัส" + ((delid != "") ? delid.Remove(delid.Length - 1) : ""), "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfo('" + Resources.CommonResource.Msg_HeadInfo + "','" + Resources.CommonResource.Msg_DeleteComplete + "');");

                gvw.Selection.UnselectAll();

                break;
            case "edit":


                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "NCARBANID");
                string stmID = Convert.ToString(data);

                Session["NCARBANID"] = stmID;
                xcpn.JSProperties["cpRedirectTo"] = "depo_terminal_remain_add.aspx";

                break;

        }
    }
   
    //กด แสดงเลข Record
    void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("depo_terminal_remain_add.aspx");

    }


    protected void sds_Deleted(object sender, SqlDataSourceStatusEventArgs e)
    {
        QueryProcessed(e);
    }
    protected void sds_Deleting(object sender, SqlDataSourceCommandEventArgs e)
    {
        QueryProcessing(e);
    }

    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
}
