﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using SelectPdf;
using System.IO;

public partial class AnnualReportEmailFormat : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReadText();
        }
    }
    private void ReadText()
    {
        try
        {
            string textForm = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_FORM.txt"));
            hdnDivContents.Value = textForm;

            string textEmail = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_Email.txt"));
            hdnEmailContent.Value = textEmail;
        }
        catch (Exception)
        {

        }


    }
    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {

            /*int MarginSize = 45;
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.MarginTop = -10;
            converter.Options.MarginLeft = MarginSize;
            converter.Options.MarginBottom = -10;
            converter.Options.MarginRight = MarginSize;
            var sb = new StringBuilder();
            DvSource.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
            ///exgap1ch.jpeg
            string s = sb.ToString();
            // convert the url to pdf 
            PdfDocument doc = converter.ConvertHtmlString(sb.ToString());

            string Path = this.CheckPath();
            string FileName = "AA_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";

            doc.Save(Response, false, FileName);
            //this.DownloadFile(Path, FileName);
            doc.Close();*/

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/msword";
            string strFileName = "docName" + ".doc";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

            StringBuilder strHTMLContent = new StringBuilder();
            strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
            strHTMLContent.Append(hdnDivContents.Value);
            strHTMLContent.Append("</body></html>");

            HttpContext.Current.Response.Write(strHTMLContent);
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush();
        }
        catch (Exception)
        {

            throw;
        }

    }
    protected void btnSaveForm_Click(object sender, EventArgs e)
    {
        try
        {
            File.WriteAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_FORM.txt"), hdnDivContents.Value);
            alertSuccess("บันทึกแบบฟอร์มเอกสารเรียบร้อย");
        }
        catch (Exception ex)
        {

            alertFail(ex.Message);
        }
    }
    protected void btnSaveMail_Click(object sender, EventArgs e)
    {
        try
        {
            File.WriteAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_Email.txt"), hdnEmailContent.Value);
            alertSuccess("บันทึกแบบฟอร์ม E-mail เรียบร้อย");
        }
        catch (Exception ex)
        {

            alertFail(ex.Message);
        }
    }
}