﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;

public partial class vendor_history : System.Web.UI.Page
{
    string con = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
       
        Listdata();
        
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] param = e.Parameter.Split(';');
        switch (param[0])
        {
            case "Search": Listdata();
                break;

            case "CancelSearch":
                txtSearch.Text = "";
                Listdata();
                break;
            
            case "edit":
                
                dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SVENDORID");
                xcpn.JSProperties["cpRedirectTo"] = "vendor_edit.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + data));
               
        
                
                break;

        }
       
        
    }

    void Listdata()
    {

         string Condition =txtSearch.Text;

         string strsql = @"SELECT vensap.DUPDATE, ven.SVENDORID,ven.SABBREVIATION,vensap.SNO ||' '|| vensap.SDISTRICT||' '|| vensap.SREGION ||' '|| vensap.SPROVINCE ||' '|| vensap.SPROVINCECODE as Address
, ven.STEL ,CASE ven.CACTIVE
  WHEN 'N' THEN 'ไม่อนุญาตให้ใช้งาน' 
  ELSE 'ใช้งาน' 
END AS CACTIVE
FROM TVendor ven
LEFT  JOIN TVENDOR_SAP vensap
ON ven.SVENDORID = vensap.SVENDORID
WHERE  ven.SVENDORID LIKE  '%" + Condition + "%' OR ven.SABBREVIATION LIKE '%" + Condition + "%'";

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(con, strsql);

        gvw.DataSource = dt;
        gvw.DataBind();
 
        
       

    }
}