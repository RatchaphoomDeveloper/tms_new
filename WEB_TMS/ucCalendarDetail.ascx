﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucCalendarDetail.ascx.cs"
    Inherits="ucCalendarDetail" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<link href="fullcalendar/lib/cupertino/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<link href='fullcalendar/fullcalendarDetail.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="fullcalendar/styleButton.css" rel="stylesheet" type="text/css" />
<script src='fullcalendar/lib/moment.min.js'></script>
<script src="fullcalendar/lang/th.js" type="text/javascript"></script>
<script src='fullcalendar/fullcalendar.min.js'></script>
<script src='fullcalendar/lib/jquery-ui.custom.min.js'></script>
<script type="text/javascript">

    $(document).ready(function (s) {
        $('tr[id$=trVisible]').attr('class', 'displayNone');

    });

    var month = "ม.ค.";
    function OnClick(s, e) {
        s.Focus();
        month = s.GetText();
    }
    function OnPrevClick(s, e) {
        lblYear.SetText(parseInt(lblYear.GetText()) - 1);
        month = "ม.ค.";
    }
    function OnNextClick(s, e) {
        lblYear.SetText(parseInt(lblYear.GetText()) + 1);
        month = "ม.ค.";
    }
    function OnOkClick(s, e) {
        dde.SetText(month + "-" + lblYear.GetText());
        dde.HideDropDown();
        $("input[id$=btnCallbackCalendar]").click();
    }


    function SetDataCalendar(sValue) {

        txtCalendarDateHide.SetValue(sValue + "");
        $("span[id$=lblDateCalendar1]").text(sValue + "");
        $("span[id$=lblDateCalendar2]").text(sValue + "");
        gvwCalendar1.PerformCallback();
        // $("input[id$=btnAddDataCalendar]").click();
        $('.fc-day[data-date]').css('background', '#ffffff');

        $('.fc-today[data-date]').css('background', '#ffef8f');


        $('.fc-day[data-date="' + PadRight(sValue) + '"]').css('background', '#9DD8FF');


    }


    function SetDataCalendarSubmit() {

        lblCalendarDate.SetValue(txtCalendarDateHide.GetValue());
        txtCalendarDate.SetValue(txtCalendarDateHide.GetValue());

        $('tr[id$=trVisible]').attr('class', 'displayNone');

    }

    function Close() {

        if (txtCalendarDateHide.GetValue() != "") {
            $('tr[id$=trVisible]').attr('class', 'displayNone');
        }
        else {
            dxWarning('แจ้งเตือน', 'ระบุวันที่เลื่อนนัดหมาย');
        }
    }

    //เช็ควนที่ว่าเปลี่ยนวันไหม
    function CheckDateChageCalendar() {

        //วันที่เลือก
        var getd = PadLeft(txtCalendarDateHide.GetValue());
        //วันที่เคยเลือก
        var gets = PadLeft(txtDateOld.GetText());



        if (getd === gets) {

            $("#trChagedate").hide();
            $("#trChagedateCause").hide();
            rblChangedate.SetEnabled(false);
            MemoChageDate.SetEnabled(false);
        }
        else {
            //เช็คสถานะว่าใครเป็นผู้ร้องของในกรณีเป็น มว. จะไม่แสดงเลื่อนนัดหมาย
        
            if (txtRkFlag.GetText() === "M") {

            }
            else {

                $("#trChagedate").show();
                $("#trChagedateCause").show();

                rblChangedate.SetEnabled(true);
                MemoChageDate.SetEnabled(true);
            }
        }
    }

    function PadLeft(Value) {
        var cdate = Value;
        var sv = "";
       
        if (Value === "-") {

        }
        else {
            var CV = cdate.toString();
            var _item = CV.split('/');

            var sss = _item[1].toString();
            if (sss.length > 1) {
                sv = _item[1] + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

            }
            else {
                sv = '0' + sss + '-' + _item[0] + '-' + (parseInt(_item[2]) - 543);

            }


        }
        return sv;
    }

    function PadRight(Value) {
        var cdate = Value;
        var CV = cdate.toString();
        var _item = CV.split('/');
        var sv = "";
        var sss = _item[1].toString();
        if (sss.length > 1) {

            var sdd = _item[0].toString();
            if (sdd.length > 1) {
                sv = (parseInt(_item[2]) - 543) + '-' + _item[1] + '-' + _item[0];
             
            }
            else {
                sv = (parseInt(_item[2]) - 543) + '-' + _item[1] + '-' + '0' + _item[0];
                
            }
        }
        else {
            var sss2 = _item[0].toString();

            if (sss2.length > 1) {
                sv = (parseInt(_item[2]) - 543) + '-' + '0' + sss + '-' + _item[0];
               
            }
            else {
                sv = (parseInt(_item[2]) - 543) + '-' + '0' + sss + '-' + '0' + _item[0];
             
            }
        }

        return sv;
    }

</script>
<style type="text/css">
    .buttonMonth
    {
        border-width: 1px;
        width: 50px;
        text-align: center;
    }
    .tab
    {
        width: 100%;
        border-color: #600;
        border-width: 0 0 1px 1px;
        border-style: solid;
    }
    .cell
    {
        border-color: #600;
        border-width: 1px 1px 0 0;
        border-style: solid;
        margin: 0;
        padding: 4px;
        background-color: #eee6a3;
    }
    
    .td_x
    {
        padding-bottom: 10px;
    }
    #loading
    {
        text-align: right;
        display: none;
        top: 10px;
        right: 10px;
    }
    
    .displayNone
    {
        display: none;
    }
    
    
    .displayTR
    {
        display: table-row;
    }
    
    /* .ui-state-highlight:hover, .ui-widget-content .ui-state-highlight:hover, .ui-widget-header .ui-state-highlight:hover
    {
        background-color: yellow !important;
        height: 112px !important;
    }*/
    td .fc-day:hover
    {
        border-top-color: #9DD8FF;
        border-top-width: 1px;
        border-bottom-color: #9DD8FF;
        border-bottom-width: 1px;
        border-left-color: #9DD8FF;
        border-left-width: 1px;
        border-right-color: #9DD8FF;
        border-right-width: 1px;
    }
</style>
<asp:Literal ID="ltrjQueryScript" runat="server"></asp:Literal>
<table width="100%" border="0" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="5" cellpadding="0" bgcolor="#FFFFFF">
                <tr>
                    <td>
                        <dx:ASPxTextBox ID="txtCalendarDateHide" runat="server" ClientVisible="false" ClientInstanceName="txtCalendarDateHide">
                        </dx:ASPxTextBox>
                        <dx:ASPxTextBox ID="txtValueCalendar" runat="server" ClientVisible="false" ClientInstanceName="txtValueCalendar">
                        </dx:ASPxTextBox>
                        <asp:Button ID="btnAddDataCalendar" runat="server" Style="display: none" OnClick="btnAddDataCalendar_Click" />
                        <%-- <table>
                            <tr>
                                <td>
                                    กรุณาเลือกวันที่ปฏิทิน :
                                </td>
                                <td>
                                    <dx:ASPxDropDownEdit ID="ddeCalendar" runat="server" OnInit="ddeCalendar_Init" Width="100"
                                        ClientInstanceName="dde">
                                    </dx:ASPxDropDownEdit>
                                    <asp:Button ID="btnCallbackCalendar" runat="server" OnClick="btnCallbackCalendar_Click"
                                        Style="display: none" />
                                   
                                    <dx:ASPxTextBox ID="txtValueCalendar" runat="server" ClientVisible="false" ClientInstanceName="txtValueCalendar">
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                        </table>--%>
                        <div style="width: 100%;">
                            <div style="width: 100%; text-align: center;">
                                <ul id="months-list" style="margin-bottom: -22px">
                                </ul>
                            </div>
                            <div id='calendar'>
                            </div>
                            <%--<asp:ImageButton ID="Call" ImageUrl=""  />--%>
                            <asp:Literal ID="ltrmonthcalendar" runat="server"></asp:Literal>
                            <asp:Literal ID="ltrday" runat="server"></asp:Literal>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="style28" style="background-color: White!important;">*การจองคิววัดน้ำจะสมบูรณ์เมื่อได้รับการยืนยันจาก
                        มว. </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellspacing="1" cellpadding="3" style="background-color: White!important;"
                            width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="8" align="left"><img alt="" src="images/action_check.png" width="16"
                                    height="16"> คิววัดน้ำประจำวันที่
                                        <asp:Label ID="lblDateCalendar1" runat="server"></asp:Label>
                                        ที่ตอบรับแล้ว </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxGridView runat="server" ID="gvwCalendar1" Width="100%" OnAfterPerformCallback="gvwCalendar1_AfterPerformCallback"
                                            SettingsBehavior-AllowSort="false" ClientInstanceName="gvwCalendar1">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="นัดหมาย" FieldName="APPOINTMENT_DATE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" Width="15%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภท" FieldName="STYPECAR" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUSREQ_NAME" Visible="false"
                                                    Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table border="0" cellspacing="1" style="background-color: White!important;" cellpadding="3"
                            width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="9" align="left"><img alt="" src="images/btnCon1.gif" width="23" height="23">
                                        คิววัดน้ำประจำวันที่
                                        <asp:Label ID="lblDateCalendar2" runat="server"></asp:Label>
                                        ที่ต้องการจัด </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxGridView runat="server" ID="gvwCalendar2" Width="100%" SettingsBehavior-AllowSort="false"
                                            ClientInstanceName="gvwCalendar2">
                                            <Columns>
                                                <dx:GridViewDataDateColumn Caption="ยื่นคำขอ" FieldName="REQUEST_DATE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="นัดหมาย" FieldName="APPOINTMENT_DATE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataDateColumn Caption="หมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataDateColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนหัว" FieldName="VEH_NO" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ทะเบียนท้าย" FieldName="TU_NO" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" FieldName="SABBREVIATION" Width="15%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภทคำขอ" FieldName="REQTYPE_NAME" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ประเภท" FieldName="STYPECAR" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="สถานะ" FieldName="STATUSREQ_NAME" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="btnSelectCalendar" runat="server" Text="จัดลงคิว" AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){ CheckDateChageCalendar(); SetDataCalendarSubmit();}" />
                                    </dx:ASPxButton>
                                </td>
                                <td>
                                    <dx:ASPxButton ID="btnCloseCalendar" runat="server" Text="ปิด" AutoPostBack="false">
                                        <ClientSideEvents Click="function(s,e){Close();}" />
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
            </dx:ASPxLoadingPanel>
        </td>
    </tr>
</table>
