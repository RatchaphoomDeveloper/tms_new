﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Configuration;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using DevExpress.XtraReports.UI;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.XtraPrinting;
using System.Drawing.Printing;

public partial class ReportVendorInfo : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Event
        gvwVendor.CustomColumnDisplayText += new ASPxGridViewColumnDisplayTextEventHandler(gvwVendor_CustomColumnDisplayText);
        gvwVendor.DataBinding += new EventHandler(gvwVendor_DataBinding);
        #endregion
        if (!IsPostBack)
        {
            lblReport.Text = GetMessage();
            ListData(txtKeyword.Text.Trim());
            gvwVendor.EncodeHtml = false;
        }
    }
    protected void xcpn_Callback(object sender, CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch ("" + paras[0])
        {
            case "SEARCH":
                lblReport.Text = GetMessage();
                ListData(txtKeyword.Text.Trim());
                break;
        }
    }
    protected void gvwVendor_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ลำดับที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }
    protected void gvwVendor_DataBinding(object sender, EventArgs e)
    {
        ASPxGridView __gvw = (ASPxGridView)sender;
        if (ViewState["ssVENDORINFO"] != null)
            __gvw.DataSource = (DataTable)ViewState["ssVENDORINFO"];
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WritePdfToResponse();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        PreparePrinting();
        gridExport.WriteXlsToResponse();
    }
    #region Other Function
    private void ListData(string KeyWord)
    {

        string _sql = "", _condi = "";

        if (!string.IsNullOrEmpty(KeyWord))
            _condi = " AND (VEN.SABBREVIATION LIKE '%" + CommonFunction.ReplaceInjection(KeyWord) + "%' OR TRU.SHEADREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(KeyWord) + "%' OR TRA.SHEADREGISTERNO LIKE '%" + CommonFunction.ReplaceInjection(KeyWord) + "%')";

        _sql = @"SELECT TRU.STRANSPORTID,VEN.SABBREVIATION,TRU.STRANSPORTTYPE,TYP.STRANS_TYPE_DESC,TRU.STRUCKID,TRU.SHEADREGISTERNO As HEAD_NO,TRA.STRUCKID As RTRUCKID,TRA.SHEADREGISTERNO As TRAIL_NO
,CASE WHEN TRA.NWHEELS IS NOT NULL THEN TO_CHAR(TRA.NWHEELS)||' ล้อ' ELSE TO_CHAR(TRU.NWHEELS)||' ล้อ' END As NWHEELS
,CON.SCONTRACTID
,CASE WHEN CON.SCONTRACTID IS NOT NULL AND NVL(CON.CSPACIALCONTRAC,'N')='N' AND NVL(CTR.CSTANDBY,'N')='N' THEN 'X' END As SCONTRACT
,CASE WHEN NVL(CTR.CSTANDBY,'N')='Y' THEN 'X' END As CSTANDBY
,CASE WHEN NVL(CON.CSPACIALCONTRAC,'N')='Y' AND NVL(CTR.CSTANDBY,'Y')!='Y' THEN 'X' END As CSPACIALCONTRAC
,CASE WHEN CON.SCONTRACTID IS NULL THEN 'X' END As OJOB
,CTY.SCONTRACTTYPENAME
,CASE WHEN CON.SCONTRACTTYPEID=1 THEN 'X' END As CUST
,CASE WHEN CON.SCONTRACTTYPEID=2 THEN 'X' END As WARE
,CASE WHEN CON.SCONTRACTTYPEID=3 THEN 'X' END As INDO
,CASE WHEN CON.SCONTRACTTYPEID NOT IN (1,2,3) THEN 'X' END As OTRAN
,CASE WHEN PRO.PROD_CATEGORY='น้ำมันใส' THEN 'X' END As LOIL
,CASE WHEN PRO.PROD_CATEGORY='LPG' THEN 'X' END As LPG
,CASE WHEN PRO.PROD_CATEGORY='น้ำมันเตา' THEN 'X' END As FOIL
,CASE WHEN PRO.PROD_CATEGORY='น้ำมันอากาศยาน' THEN 'X' END As FUEL
,CASE WHEN PRO.PROD_CATEGORY='ยางมะตอย ' THEN 'X' END As POL
,CASE WHEN PRO.PROD_CATEGORY NOT IN ('น้ำมันใส','LPG','น้ำมันเตา','น้ำมันอากาศยาน','ยางมะตอย') THEN 'X' END As OPROD
,TER.SABBREVIATION As FLEET
,CASE WHEN TRA.STRUCKID IS NOT NULL THEN TRA.NTOTALCAPACITY ELSE TRU.NTOTALCAPACITY END As NTOTALCAPACITY
,CASE WHEN TRA.STRUCKID IS NOT NULL THEN TRA.NTANK_HIGH_TAIL ELSE TRU.NTANK_HIGH_HEAD END As NTANK_HIGH
,CASE WHEN TRA.STRUCKID IS NOT NULL THEN TRA.NSLOT ELSE TRU.NSLOT END As NSLOT
FROM TTRUCK TRU
LEFT JOIN TTRUCK TRA ON TRU.STRAILERID=TRA.STRUCKID
LEFT JOIN TVendor VEN ON TRU.STRANSPORTID=VEN.SVENDORID
LEFT JOIN TTRANS_TYPE TYP ON TRU.STRANSPORTTYPE=TYP.STRANS_TYPE
LEFT JOIN TCONTRACT_TRUCK CTR ON TRU.STRUCKID=CTR.STRUCKID AND TRA.STRUCKID=CTR.STRAILERID
LEFT JOIN TCONTRACT CON ON CTR.SCONTRACTID=CON.SCONTRACTID
LEFT JOIN TCONTRACTTYPE CTY ON CON.SCONTRACTTYPEID=CTY.SCONTRACTTYPEID
LEFT JOIN TTERMINAL TER ON CON.STERMINALID=TER.STERMINALID
LEFT JOIN TPRODUCT PRO ON CASE WHEN TRU.SCARTYPEID IN ('1','3') THEN TRA.SPROD_GRP ELSE TRU.SPROD_GRP END = PRO.PROD_ID
WHERE TRU.SCARTYPEID NOT IN ('2','4')  " + _condi;

        DataTable dt = CommonFunction.Get_Data(conn, _sql);
        ViewState["ssVENDORINFO"] = dt;
        gvwVendor.DataBind();
    }
    private void PreparePrinting()
    {
        ListData(txtKeyword.Text.Trim());

        foreach (GridViewColumn gvc in gridExport.GridView.AllColumns)
        {
            gvc.Caption = gvc.Caption.Replace("<br/>", "");
        }

        PrivateFontCollection fontColl = new PrivateFontCollection();
        fontColl.AddFontFile(Server.MapPath("~/font/THSarabunNew.ttf"));

        gridExport.GridView.Columns[7].Visible = false;
        gridExport.Landscape = true;
        gridExport.PaperKind = PaperKind.A4;
        gridExport.PageHeader.Center = GetMessage();
        gridExport.PageHeader.Font.Name = fontColl.Families[0].Name;
        gridExport.PageHeader.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.PageHeader.Font.Bold = true;
        gridExport.TopMargin = 1;
        gridExport.RightMargin = 1;
        gridExport.BottomMargin = 1;
        gridExport.LeftMargin = 1;
        gridExport.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.True;
        gridExport.Styles.Default.Font.Name = fontColl.Families[0].Name;
        gridExport.Styles.Default.Font.Size = new System.Web.UI.WebControls.FontUnit(14F, UnitType.Em);
        gridExport.Styles.Header.Font.Bold = true;

        gridExport.FileName = "รายงานข้อมูลผู้ขนส่ง_" + DateTime.Now.ToString("MMddyyyyHHmmss");
    }
    private string GetMessage()
    {
        string sMessage = "";
        if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            sMessage = string.Format(@"รายงานข้อมูลผู้ขนส่ง ประเภท บริษัท อายุ กำหนดวัดน้ำ ผู้ขนส่ง คำค้นหา ""{1}"" (จัดทำรายงานวันที่ {0:d/M/yyyy})", DateTime.Now.Date, txtKeyword.Text.Trim());
        else
            sMessage = string.Format(@"รายงานข้อมูลผู้ขนส่ง ประเภท บริษัท อายุ กำหนดวัดน้ำ ผู้ขนส่ง (จัดทำรายงานวันที่ {0:d/M/yyyy})", DateTime.Now.Date);
        return sMessage;
    }
    #endregion
}