﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ComplaintTopicAddDetail : PageBase
{
    #region + View State +
    private int TopicID
    {
        get
        {
            if ((int)ViewState["TopicID"] != null)
                return (int)ViewState["TopicID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.LoadTopic();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadTopic()
    {
        try
        {
            DataTable dtTopic = TopicBLL.Instance.TopicSelectAllBLL(" AND ISACTIVE = 1 AND TYPE = '" + radType.SelectedValue + "'");
            DropDownListHelper.BindDropDownList(ref ddlTopic, dtTopic, "TOPIC_ID", "TOPIC_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void radType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.LoadTopic();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            TopicBLL.Instance.TopicAddDetailBLL(int.Parse(ddlTopic.SelectedValue), txtComplainName.Text.Trim(), int.Parse(radStatus.SelectedValue), int.Parse(Session["UserID"].ToString()), 0, 0);

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "ComplaintTopic.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ComplaintTopic.aspx");
    }

    private void ValidateSave()
    {
        try
        {
            if (ddlTopic.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก ประเภท");

            if (string.Equals(txtComplainName.Text.Trim(), string.Empty))
                throw new Exception("กรุณาใส่ข้อมูล หัวข้อ");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}