﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="OrderPlan_Jang.aspx.cs" Inherits="OrderPlan_Jang" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript" language="javascript">
        

        // <![CDATA[
        var uploadCompleteFlag;
        function FilesUploadComplete(s, e) {
            if (uclOrder.GetText() != "") {
                dxError('แจ้งเตือน', 'กรุณาปิดไฟล์ที่เปิดขณะอัพโหลดแผน');
            }
            uploadCompleteFlag = true;
            if (e.errorText != "") {
                alert('error');
            }
            else {

            }
        }
        function ShowMessage(message) {

            alert(message);
            //window.setTimeout("alert('" + message + "')", 0);
        }
        function FileUploadStart(s, e) {

            uploadCompleteFlag = false;
        }

        function ShowPopupProgressingPanel() {

            if (!uploadCompleteFlag) {
                pbProgressing1.SetPosition(0);

            }
            else {

            }
        }
        function UploadingProgressChanged(s, e) {

            pbProgressing1.SetPosition(e.progress);
            var info = e.currentFileName + "&emsp;[" + GetKBytes(e.uploadedContentLength) + " / " + GetKBytes(e.totalContentLength) + "] KBytes";
        }
        function GetKBytes(bytes) {

            return Math.floor(bytes / 1024);
        }
        // ]]> 

        function OpenPopup() {

            //            var x = screen.width / 2 - 400 / 2;
            //            var y = screen.height / 2 - 210 / 2;
            //            window.open('book-car-progress.aspx?v=' + rblImport.GetValue(), '', "width=400,height=230,scrollbars=1,left=" + x + ",top=" + y);

            window.location = 'OrderPlan-progress.aspx?v=' + rblChoice.GetValue();
        };

        function OpenPage(sUrl) {

            window.open(sUrl);
        }

    </script>
    <style type="text/css">
        div .sco
        {
            height: 250px;
            width: 250px;
            overflow: auto;
        }
        .Poop .dxcpLoadingPanelWithContent_Aqua
        {
            left: 360px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--  <dx:ASPxTextBox runat="server" ID="txtPahtExcel" ClientInstanceName="txtPahtExcel">
    </dx:ASPxTextBox>
    <dx:ASPxTextBox runat="server" ID="txtPahtPDF" ClientInstanceName="txtPahtPDF">
    </dx:ASPxTextBox>--%>
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" OnCallback="xcpn_Callback"
        ClientInstanceName="xcpn" CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%"
                    OnActiveTabChanged="ASPxPageControl1_ActiveTabChanged">
                    <%--<ClientSideEvents ActiveTabChanged="function (s, e) {gvw.CancelEdit();gvwT3.CancelEdit();gvwT4.CancelEdit();}" />--%>
                    <ClientSideEvents ActiveTabChanged="function (s, e) {xcpn.PerformCallback();}" />
                    <TabPages>
                        <dx:TabPage Name="p1" Text="รถที่ยืนยันตามสัญญา" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="10%" align="right">
                                                            ค้นหา :
                                                        </td>
                                                        <td width="30%" align="right">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="บริษัทขนส่ง,เลขที่สัญญา,ทะเบียนรถ"></dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            ชื่อคลัง :
                                                        </td>
                                                        <td width="15%">
                                                            <dx:ASPxComboBox runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="STERMINALID"
                                                                TextFormatString="{1}" Width="250px" Height="25px" ClientInstanceName="cboTeminal"
                                                                TextField="SABBREVIATION" CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboTeminal"
                                                                OnItemRequestedByValue="cboTeminal_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboTeminal_ItemsRequestedByFilterConditionSQL">
                                                                <Columns>
                                                                    <dx:ListBoxColumn FieldName="STERMINALID" Width="30%" Caption="#"></dx:ListBoxColumn>
                                                                    <dx:ListBoxColumn FieldName="SABBREVIATION" Width="70%" Caption="คลัง"></dx:ListBoxColumn>
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource runat="server" ID="sdsTeminal" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                        <td width="15%" align="right">
                                                            วันที่ยืนยันรถ:
                                                        </td>
                                                        <td width="10%" align="right">
                                                            <dx:ASPxDateEdit runat="server" ID="edtCFT" SkinID="xdte">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td width="10%">
                                                            <dx:ASPxButton runat="server" ID="btnSeachT1" AutoPostBack="false" SkinID="_search">
                                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Search'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right"><table width="100%"><tr><td style=" width:70%"  align="left" > 
                                                    <dx:ASPxHyperLink ID="lkbconfirmtruck" ClientInstanceName="lkbconfirmtruck" runat="server" Text="ดูรายการยืนยันรถตามสัญญาทั้งหมด" Cursor="pointer">
                                                    <ClientSideEvents Click= "function(s,e){window.location='ConfirmTruck.aspx?str=';}"/></dx:ASPxHyperLink>
                                                </td>
                                                <td style=" width:30%; white-space:nowrap;" align="right" >
                                                <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="จำนวนรถที่ยืนยัน" CssClass="dxeLineBreakFix"
                                                    ForeColor="Red">
                                                </dx:ASPxLabel>
                                                <dx:ASPxLabel runat="server" ID="lblDetailTeminal" CssClass="dxeLineBreakFix" ForeColor="Red">
                                                </dx:ASPxLabel>
                                                <dx:ASPxLabel runat="server" Text="คัน" ID="ASPxLabel2" CssClass="dxeLineBreakFix"
                                                    ForeColor="Red">
                                                </dx:ASPxLabel></td></tr></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxTextBox runat="server" ID="txtOrder" ClientInstanceName="txtOrder" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxGridView runat="server" ID="gvwT1" Width="100%" KeyFieldName="SVENDORID">
                                                    <Columns>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Width="25%" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"
                                                            FieldName="SABBREVIATION">
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel runat="server" ID="lblVen" Text="ผู้ขนส่ง" Font-Underline="false" Cursor="pointer"
                                                                    Width="100%">
                                                                    <ClientSideEvents Click="function(s,e){ txtOrder.SetText('VENDORNAME'); xcpn.PerformCallback('Search'); }" />
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="เลขที่สัญญา" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" FieldName="SCONTRACTNO">
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel runat="server" ID="lblContract" Text="เลขที่สัญญา" Font-Underline="false"
                                                                    Cursor="pointer" Width="100%">
                                                                    <ClientSideEvents Click="function(s,e){ txtOrder.SetText('CONTRACT'); xcpn.PerformCallback('Search'); }" />
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="คลังต้นทาง" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="STERMINALNAME" Settings-AllowSort="False">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"STERMINALNAME") %>'
                                                                    Cursor="pointer" Font-Underline="true">
                                                                    <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('0'); PopDetail.Show(); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <Settings AllowSort="False"></Settings>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="รถที่ยืนยัน" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="STRUCKREGIS" Settings-AllowSort="False">
                                                            <Settings AllowSort="False"></Settings>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="ปริมาตร" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="CAPACITY" Settings-AllowSort="False">
                                                            <Settings AllowSort="False"></Settings>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Right">
                                                            </CellStyle>
                                                            <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                            </PropertiesTextEdit>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Settings ShowGroupPanel="false" />
                                                    <SettingsPager PageSize="50">
                                                    </SettingsPager>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p2" Text="แบ่งงานให้ผู้ขนส่ง" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="22%" align="left">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                                                        ToolTips
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnExcelT2" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Cursor="pointer" Text="ดาวน์โหลดแบบฟอร์มการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_ms_excel.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnPDFT2" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Cursor="pointer" Text="ดาวน์โหลดตัวอย่างการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_pdf2.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="71%">
                                                            <table width="100%" cellpadding="2" cellspacing="1">
                                                                <tr>
                                                                    <td width="80%" align="center">
                                                                        <table width="100%" cellpadding="2" cellspacing="1">
                                                                            <tr>
                                                                                <td colspan="3" align="left">
                                                                                    <img src="images/ic_ms_excel.gif" />
                                                                                    Import Data<font color="red">*</font>(เฉพาะไฟล์Excel)<dx:ASPxTextBox ID="txtPermission"
                                                                                        ClientInstanceName="txtPermission" runat="server" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="50%">
                                                                                    <dx:ASPxUploadControl ID="uclOrder" runat="server" ClientInstanceName="uclOrder"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uclOrder_FileUploadComplete"
                                                                                        UploadMode="Advanced" Width="100%" CssClass="dxeLineBreakFix">
                                                                                        <ValidationSettings MaxFileSize="10000000" ShowErrors="false" AllowedFileExtensions=".xlsx,.xls">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents UploadingProgressChanged="function(s,e){UploadingProgressChanged(s,e);}"
                                                                                            FileUploadStart="function(s,e){FileUploadStart(s,e);}" FilesUploadComplete="function(s,e){ FilesUploadComplete(s,e);}"
                                                                                            FileUploadComplete="function(s, e) {if(e.callbackData ==''){OpenPopup();}else{ dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);} }">
                                                                                        </ClientSideEvents>
                                                                                        <BrowseButton Text="Browse..">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                </td>
                                                                                <td width="15%">
                                                                                    <dx:ASPxButton runat="server" ID="btnConfirm" AutoPostBack="false" Text="นำเข้าข้อมูล"
                                                                                        CssClass="dxeLineBreakFix" Width="100px">
                                                                                        <ClientSideEvents Click="function(s, e) { if(txtPermission.GetText() != ''){ uclOrder.Upload();} else{ dxWarning('แจ้งเตือน','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');}  }">
                                                                                        </ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td width="35%">
                                                                                    <dx:ASPxRadioButtonList runat="server" ID="rblChoice" RepeatDirection="Horizontal"
                                                                                        SkinID="rblStatus" ClientInstanceName="rblChoice">
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="ทุกรายการ" Value="A" />
                                                                                            <dx:ListEditItem Text="เฉพาะรายการใหม่" Value="N" />
                                                                                        </Items>
                                                                                    </dx:ASPxRadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" style="padding-left: 5px">
                                                                        <dx:ASPxProgressBar ID="pbProgressing1" ClientInstanceName="pbProgressing1" runat="server"
                                                                            Width="50%">
                                                                        </dx:ASPxProgressBar>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="10%">
                                                            Delivery Date :
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            -
                                                            <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td width="7%" align="right">
                                                            ค้นหา:
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch2" NullText="ผู้ขนส่ง, เลขที่สัญญา, ทะเบียนรถ, เลขที่ DO"
                                                                Width="100%">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            กะการขนส่ง:
                                                        </td>
                                                        <td width="23%">
                                                            <dx:ASPxComboBox runat="server" ID="cboWindowTime" DataSourceID="sdsWindowTime" TextField="SWINDOWTIMENAME"
                                                                ValueField="NLINE" Width="100%">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sdsWindowTime" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT NLINE,'เที่ยวที่ '||NLINE||' เวลา '||TSTART||'-'||TEND||' น.' as SWINDOWTIMENAME FROM TWINDOWTIME 
GROUP BY NLINE,'เที่ยวที่ '||NLINE||' เวลา '||TSTART||'-'||TEND||' น.' 
ORDER BY NLINE"></asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sdsConTract" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton runat="server" ID="btnSearch" AutoPostBack="false" SkinID="_search">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvw" ClientInstanceName="gvw" AutoGenerateColumns="false"
                                                    Width="100%" KeyFieldName="ORDERID">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                                    </ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งที่ยังไม่ได้จัดแผน">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="DO Date" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="กะขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="85px">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('1'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ประเภทการขนส่ง" FieldName="TRAN_TYPE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="120px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="150px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center" Width="120px">
                                                                    <DataItemTemplate>
                                                                        <%-- <dx:ASPxButton runat="server" ID="btnEdit" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('edit;'+txtIndx.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel" SkinID="_delete" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('delete;'+ txtIndx.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>--%>
                                                                        <dx:ASPxButton runat="server" ID="btnEdit" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('edit;'+txtIndx.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel" SkinID="_delete" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('delete;'+ txtIndx.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <table width="100%" cellpadding="3" cellspacing="1">
                                                                <%--            <tr>
                                                                    <td width="12%">
                                                                        Delivery No.
                                                                    </td>
                                                                    <td width="38%">
                                                                        <div style="float: left">
                                                                            <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" ClientInstanceName="cboDelivery"
                                                                                EnableCallbackMode="True" MaxLength="10" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                                                                OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="DELIVERY_NO" Width="130px">
                                                                              
                                                                                <Columns>
                                                                                    <dx:ListBoxColumn Caption="Outbound No." FieldName="DELIVERY_NO" Width="100px" />
                                                                                    <dx:ListBoxColumn Caption="Ship-to" FieldName="SHIP_TO" Width="80px" />
                                                                                    <dx:ListBoxColumn Caption="ชื่อลูกค้า" FieldName="SCUSTOMER" Width="150px" />
                                                                                    <dx:ListBoxColumn Caption="ปริมาณ" FieldName="NVALUE" Width="100px" />
                                                                                </Columns>
                                                                            </dx:ASPxComboBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <dx:ASPxButton ID="btnGetOutbound" runat="server" Width="20px" AutoPostBack="false"
                                                                                Text="Get" Title="ดึง Outbound จาก SAP Master">
                                                                                <ClientSideEvents Click="function(s ,e){xcpnOutBound.PerformCallback(cboDelivery.GetValue());cboDelivery.SetValue('');}" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxCallback ID="xcpnOutBound" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpnOutBound"
                                                                                OnCallback="xcpnOutBound_Callback">
                                                                            </dx:ASPxCallback>
                                                                        </div>
                                                                    </td>
                                                                    <td width="12%">
                                                                        ประเภทการขนส่ง
                                                                    </td>
                                                                    <td width="38%">
                                                                        <dx:ASPxComboBox ID="cboTRANTYPE" runat="server">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="งานขนส่งภายในวัน" Value="1" />
                                                                                <dx:ListEditItem Text="งานขนส่งล่วงหน้า" Value="2" />
                                                                            </Items>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>--%>
                                                                <tr>
                                                                    <td>
                                                                        ผู้ขนส่ง
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                                                            EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                            OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                            SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="100%">
                                                                            <ClientSideEvents ValueChanged="function(){  cboContract.PerformCallback(); }" TextChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendorname.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SABBREVIATION')) ; }else{txtVendorname.SetValue('') ;};}">
                                                                            </ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                                <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                            </Columns>
                                                                            <ValidationSettings RequiredField-ErrorText="ระบุบริษัท" RequiredField-IsRequired="true"
                                                                                ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                                <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        สัญญา
                                                                    </td>
                                                                    <td>
                                                                        <%-- <dx:ASPxComboBox ID="cboContract" runat="server" CallbackPageSize="30" ClientInstanceName="cboContract"
                                                        EnableCallbackMode="True" OnItemRequestedByValue="cboContract_OnItemRequestedByValueSQL"
                                                        OnItemsRequestedByFilterCondition="cboContract_OnItemsRequestedByFilterConditionSQL"
                                                        SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="80%">

                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                            <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                       
                                                        </Columns>
                                                        <ValidationSettings RequiredField-ErrorText="ระบุบริษัท" RequiredField-IsRequired="true"
                                                            ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                            <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                        </ValidationSettings>
                                                    </dx:ASPxComboBox>--%>
                                                                        <dx:ASPxComboBox runat="server" ID="cboContract" ClientInstanceName="cboContract"
                                                                            OnCallback="cboContract_Callback" TextField="SCONTRACTNO" ValueField="SCONTRACTID"
                                                                            Width="100%">
                                                                            <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false; gvw.PerformCallback('savegvw;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('cancel');}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowHorizontalScrollBar="True" UseFixedTableLayout="true" />
                                                </dx:ASPxGridView>
                                                <dx:ASPxTextBox ID="txtVendorname" ClientInstanceName="txtVendorname" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtIndx" ClientInstanceName="txtIndx" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p3" Text="งานที่ผู้ขนส่งจัดลงแผน" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%" style="display: none">
                                                    <tr>
                                                        <td width="22%" align="left">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                                                        ToolTips
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnExcelT3" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Cursor="pointer" Text="ดาวน์โหลดแบบฟอร์มการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_ms_excel.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnPdfT3" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Cursor="pointer" Text="ดาวน์โหลดตัวอย่างการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_pdf2.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="71%">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="10%">
                                                            Delivery Date :
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxDateEdit runat="server" ID="edtStartT3" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            -
                                                            <dx:ASPxDateEdit runat="server" ID="edtEndT3" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td width="7%" align="right">
                                                            ค้นหา:
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch2T3" NullText="ผู้ขนส่ง, เลขที่สัญญา, ทะเบียนรถ, เลขที่ DO"
                                                                Width="100%">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            กะการขนส่ง:
                                                        </td>
                                                        <td width="23%">
                                                            <dx:ASPxComboBox runat="server" ID="cboWindowTimeT3" DataSourceID="sdsWindowTime"
                                                                TextField="SWINDOWTIMENAME" ValueField="NLINE" Width="100%">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton runat="server" ID="ASPxButton4" AutoPostBack="false" SkinID="_search">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwT3" ClientInstanceName="gvwT3" AutoGenerateColumns="false"
                                                    Width="100%" KeyFieldName="ORDERID">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                                    </ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งที่จัดแผนแล้ว">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="DO Date" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="85px">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('2'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="150px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="TRANTRUCK" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="165px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="กะขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวการขนส่ง" FieldName="ROUND" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center" Width="120px">
                                                                    <DataItemTemplate>
                                                                        <%-- <dx:ASPxButton runat="server" ID="btnEdit2" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('editT3;'+txtIndxT3.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel2" SkinID="_delete" AutoPostBack="false"
                                                                            CssClass="dxeLineBreakFix" ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                           
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('deleteT3;'+ txtIndxT3.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>--%>
                                                                        <dx:ASPxButton runat="server" ID="btnEdit2" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('editT3;'+txtIndxT3.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel2" SkinID="_delete" AutoPostBack="false"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('deleteT3;'+ txtIndxT3.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <table width="100%" cellpadding="3" cellspacing="1">
                                                                <%--                                                                <tr>
                                                                    <td width="12%">
                                                                        Delivery No.
                                                                    </td>
                                                                    <td width="38%">
                                                                
                                                                        <div style="float: left">
                                                                            <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" ClientInstanceName="cboDelivery"
                                                                                EnableCallbackMode="True" MaxLength="10" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                                                                OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="DELIVERY_NO" Width="130px">
                                                                               
                                                                                <Columns>
                                                                                    <dx:ListBoxColumn Caption="Outbound No." FieldName="DELIVERY_NO" Width="100px" />
                                                                                    <dx:ListBoxColumn Caption="Ship-to" FieldName="SHIP_TO" Width="80px" />
                                                                                    <dx:ListBoxColumn Caption="ชื่อลูกค้า" FieldName="SCUSTOMER" Width="150px" />
                                                                                    <dx:ListBoxColumn Caption="ปริมาณ" FieldName="NVALUE" Width="100px" />
                                                                                </Columns>
                                                                            </dx:ASPxComboBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                             <dx:ASPxButton ID="btnGetOutbound" runat="server" Width="20px" AutoPostBack="false"
                                                                                Text="Get" Title="ดึง Outbound จาก SAP Master">
                                                                                <ClientSideEvents Click="function(s ,e){xcpnOutBound.PerformCallback(cboDelivery.GetValue());cboDelivery.SetValue('');}" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxCallback ID="xcpnOutBound" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpnOutBound"
                                                                                OnCallback="xcpnOutBound_Callback">
                                                                            </dx:ASPxCallback>
                                                                        </div>
                                                                    </td>
                                                                    <td width="12%">
                                                                        ประเภทการขนส่ง
                                                                    </td>
                                                                    <td width="38%">
                                                                        <dx:ASPxComboBox ID="cboTRANTYPE" runat="server">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="งานขนส่งภายในวัน" Value="1" />
                                                                                <dx:ListEditItem Text="งานขนส่งล่วงหน้า" Value="2" />
                                                                            </Items>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>--%>
                                                                <tr>
                                                                    <td>
                                                                        ผู้ขนส่ง
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                                                            EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                            OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                            SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="100%">
                                                                            <ClientSideEvents ValueChanged="function(){ cboContractT3.PerformCallback();  }"
                                                                                TextChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendornameT3.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SABBREVIATION')) ; }else{txtVendornameT3.SetValue('') ;};}">
                                                                            </ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                                <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                            </Columns>
                                                                            <ValidationSettings RequiredField-ErrorText="ระบุบริษัท" RequiredField-IsRequired="true"
                                                                                ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                                <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>
                                                                        สัญญา
                                                                    </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox runat="server" ID="cboContractT3" ClientInstanceName="cboContractT3"
                                                                            OnCallback="cboContractT3_Callback" TextField="SCONTRACTNO" ValueField="SCONTRACTID"
                                                                            Width="100%">
                                                                            <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false; gvwT3.PerformCallback('savegvwT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {gvwT3.PerformCallback('cancel');}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowHorizontalScrollBar="True" />
                                                </dx:ASPxGridView>
                                                <dx:ASPxTextBox ID="txtVendornameT3" ClientInstanceName="txtVendornameT3" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtIndxT3" ClientInstanceName="txtIndxT3" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p4" Text="แผนที่โดนดึงกลับ" Visible="true">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="10%">
                                                            Delivery Date :
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxDateEdit runat="server" ID="edtStartT4" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            -
                                                            <dx:ASPxDateEdit runat="server" ID="edtEndT4" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td width="7%" align="right">
                                                            ค้นหา:
                                                        </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch3T4" NullText="ผู้ขนส่ง, เลขที่สัญญา, ทะเบียนรถ, เลขที่ DO"
                                                                Width="100%">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            กะการขนส่ง:
                                                        </td>
                                                        <td width="23%">
                                                            <dx:ASPxComboBox runat="server" ID="cboWindowTimeT4" DataSourceID="sdsWindowTime"
                                                                TextField="SWINDOWTIMENAME" ValueField="NLINE" Width="100%">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton runat="server" ID="ASPxButton3" AutoPostBack="false" SkinID="_search">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwT4" ClientInstanceName="gvwT4" AutoGenerateColumns="false"
                                                    Width="100%" KeyFieldName="ORDERID">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                                    </ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="แผนที่โดนดึงกลับ">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="DO Date" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="85px">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Cursor="pointer" Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('3'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="150px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="TRANTRUCK" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="165px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="กะขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวการขนส่ง" FieldName="ROUND" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Settings ShowHorizontalScrollBar="True" UseFixedTableLayout="true" />
                                                </dx:ASPxGridView>
                                                <%--   <dx:ASPxTextBox ID="ASPxTextBox2" ClientInstanceName="txtVendornameT3" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="ASPxTextBox3" ClientInstanceName="txtIndxT3" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
                <dx:ASPxTextBox runat="server" ID="txtDeliveryNo" ClientInstanceName="txtDeliveryNo"
                    ClientVisible="false">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox runat="server" ID="txtSelectgvw" ClientInstanceName="txtSelectgvw"
                    ClientVisible="false">
                </dx:ASPxTextBox>
                <dx:ASPxPopupControl runat="server" ID="PopDetail" ClientInstanceName="PopDetail"
                    CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                    HeaderText="รายละเอียด" AllowDragging="True" PopupAnimationType="None" EnableViewState="False"
                    Width="800px" AutoUpdatePosition="true" CssClass="Poop">
                    <ClientSideEvents PopUp="function(s,e){ xcpnPopup.PerformCallback('ListData'); }" />
                    <ContentCollection>
                        <dx:PopupControlContentControl>
                            <dx:ASPxCallbackPanel ID="xcpnPopup" runat="server" HideContentOnCallback="False"
                                OnCallback="xcpnPopup_Callback" ClientInstanceName="xcpnPopup" CausesValidation="False"
                                OnLoad="xcpnPopup_Load" LoadingDivStyle-CssClass="SS">
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
                                </ClientSideEvents>
                                <LoadingDivStyle CssClass="SS">
                                </LoadingDivStyle>
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <asp:Literal runat="server" ID="ltr_PANT"></asp:Literal>
                                        <table width="100%" cellpadding="3" cellspacing="1" runat="server" id="tbl_DOSHOW"
                                            style="display: none;">
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel runat="server" ID="lblDoNo">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView runat="server" ID="gvwPop" Width="100%" ClientInstanceName="gvwPop">
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="ลำดับ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="ชื่อผลิตภัณฑ์" FieldName="MATERIAL_NAME" Width="60%"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="QTY_TRAN" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                </PropertiesTextEdit>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="วันที่ขนส่ง" FieldName="DEV_DATE" Width="15%"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                </PropertiesTextEdit>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
                <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
                </dx:ASPxLoadingPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
