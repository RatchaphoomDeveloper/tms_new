﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    EnableViewState="true" CodeFile="admin_Complain_add_Vendor.aspx.cs" Inherits="admin_Complain_add_Vendor"
    StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <br />
    <br />
    <ul class="nav nav-tabs" runat="server" id="tabtest">
        <li class="active" id="Tab1" runat="server"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab">รายละเอียดการตัดคะแนน</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="panel panel-info" id="divScore" runat="server">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><asp:Label ID="lblHeader" runat="server" Text="บันทึกการตัดคะแนน (หมายเลขเอกสาร {0})"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <div class="panel-body">
                                <asp:Table ID="Table222" runat="server" Width="100%">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label13" runat="server" Text="หักคะแนน"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtPointFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="lblShowSumPoint" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label14" runat="server" Text="ค่าปรับ"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtCostFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="lblShowSumCost" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label15" runat="server" Text="ระงับ พขร. (วัน)"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtDisableFinal" runat="server" Width="200px"
                                                    Enabled="false" Style="text-align: center"></asp:TextBox></asp:TableCell><asp:TableCell>
                                                        <asp:Label ID="lblShowSumDisable" Visible="false" runat="server" Text="(Sum : {0})"></asp:Label></asp:TableCell></asp:TableRow>
                                </asp:Table>
                                <asp:Table runat="server" ID="tblScoreFinal" Width="100%" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label3" runat="server" Text="ภาพลักษณ์องค์กร"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="ddlScore1" runat="server" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore1Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore1Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label4" runat="server" Text="ความพึงพอใจลูกค้า"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="ddlScore2" runat="server" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore2Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore2Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label5" runat="server" Text="กฎ/ระเบียบฯ"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="ddlScore3" runat="server" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore3Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore3Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label6" runat="server" Text="แผนงานขนส่ง"></asp:Label></asp:TableCell><asp:TableCell>
                                                <asp:DropDownList ID="ddlScore4" runat="server" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore4Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" AutoPostBack="true" onkeypress="__doPostBack(this.name,'txtScore4Final_OnKeyPress');" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label7" runat="server" Text="ผลคูณความรุนแรง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore5Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" /></asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Label ID="Label8" runat="server" Text="หักคะแนนต่อครั้ง"></asp:Label></asp:TableCell><asp:TableCell></asp:TableCell><asp:TableCell>
                                                <asp:TextBox CssClass="form-control" ID="txtScore6Final" runat="server" Width="200px"
                                                    ReadOnly="true" Style="text-align: center" /></asp:TableCell><asp:TableCell Style="padding-left: 5px"> </asp:TableCell></asp:TableRow>
                                </asp:Table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="text-align: right">
                        <asp:Button ID="cmd" runat="server" Text="ยื่นอุทธรณ์" CssClass="btn btn-md btn-hover btn-info"
                            UseSubmitBehavior="false" Style="width: 100px" />
                        <br />
                    </div>
                </div>
                <br />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </div>
</asp:Content>
