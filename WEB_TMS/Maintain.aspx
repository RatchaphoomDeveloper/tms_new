﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true" CodeFile="Maintain.aspx.cs" Inherits="Maintain" StylesheetTheme="Aqua" %>

<%--<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>--%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxGridView" Assembly="DevExpress.Web.ASPxGridView.v11.2" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="Maintain_GridViewPanel" runat="server" ClientInstanceName="Maintain_GridViewPanel"
        CausesValidation="False" >
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td colspan="8">
							<%--OnRowUpdating="Grid_RowUpdating" OnCustomCallback="Maintain_GridView_CustomCallback"--%>
                            <dx:ASPxGridView ID="Maintain_GridView" runat="server" AutoGenerateColumns="False" ClientInstanceName="Maintain_GridView" Style="margin-top: 0px"
                                Width="100%" SettingsPager-Mode="ShowAllRecords" OnDataBinding="Maintain_GridView_DataBinding"
								KeyFieldName="MAINTAIN_ID" EnableRowsCache="false">
                                <Columns>
									<%--<dx:GridViewCommandColumn VisibleIndex="0" EditButton-Visible="True" UpdateButton-Visible="True" EditButton-Text="แก้ไข" CancelButton-Visible="True" CancelButton-Text="ยกเลิก" UpdateButton-Text="บันทึก" Width="10%">
									</dx:GridViewCommandColumn>--%>
                                    <dx:GridViewDataColumn Caption="กลุ่มงาน" FieldName="GROUPNAME" VisibleIndex="1" ReadOnly="True" Width="15%" CellStyle-HorizontalAlign ="Center" HeaderStyle-HorizontalAlign="Center">
                                    </dx:GridViewDataColumn>
                                    <%--<dx:GridViewDataTextColumn Caption="ลักษณะงานขนส่ง" FieldName="WORKGROUPNAME" VisibleIndex="2" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>--%>
									<dx:GridViewDataTextColumn Caption="คลัง" FieldName="TERMINALNAME" VisibleIndex="2" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" VisibleIndex="3" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Center" />
										<CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="VENDORNAME" VisibleIndex="4" ReadOnly="True">
                                        <HeaderStyle HorizontalAlign="Center" />
										<CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn  Caption="จำนวน" FieldName="M_AMOUNT" VisibleIndex="5" Width="6%">
                                        <HeaderStyle HorizontalAlign="Center" />
										<CellStyle HorizontalAlign="Center">
                                        </CellStyle>
										<DataItemTemplate>
											<dx:ASPxTextBox ID="txt_M_AMOUNT" Width="100%" runat="server" HorizontalAlign="Center" Value='<%# Bind("M_AMOUNT") %>' ></dx:ASPxTextBox>
										</DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
									<dx:GridViewDataTextColumn  Caption="หมุนเวียน" FieldName="M_ALTERNATION" VisibleIndex="6" Width="6%">
                                        <HeaderStyle HorizontalAlign="Center" />
										<CellStyle HorizontalAlign="Center">
                                        </CellStyle>
										<DataItemTemplate>
											<dx:ASPxTextBox ID="txt_M_ALTERNATION" Width="100%" runat="server" HorizontalAlign="Center" Value='<%# Bind("M_ALTERNATION") %>'></dx:ASPxTextBox>
										</DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td>
							<div id="SaveButton" runat="server" style="text-align:center">
								<%--OnClick="cmdSave_Click"  data-toggle="modal" data-target="#ModalConfirmBeforeSave"--%>
								<asp:Button ID="btnSave" ClientIDMode="Inherit"  runat="server" Enabled="true" Text="บันทึก" CssClass="btn btn-md btn-hover btn-info" Style="width: 100px" OnClick="cmdSave_Click" />
								<asp:Button ID="btnCancel" ClientIDMode="Inherit"  runat="server" Enabled="true" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-warning" Style="width: 100px" onclick="cmdCancel_Click" />
							</div>    
						</td>
						<%--<td>
							<div>
								<uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click" TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกใช่หรือไม่ ?" />
							</div>
						</td>--%>
					</tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
