﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_list.aspx.cs" Inherits="vendor_list" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <style type="text/css">
        .dxrpcontent
        {
            padding: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <dx:ASPxRoundPanel ID="ASPxRoundPanel1" ClientInstanceName="rpn" runat="server" Width="980px"
                    HeaderText="จัดการข้อมูลผู้ขนส่ง">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="PanelContent1">
                            <table width="100%" runat="server">
                                <%-- <tr>
                        <td colspan="2">จัดการข้อมูลผู้ขนส่ง</td>
                    </tr>--%>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td width="7%">ใช้งาน :</td>
                                                <td width="28%">
                                                    <dx:ASPxRadioButtonList runat="server" ID="cblstatus" RepeatDirection="Horizontal">
                                                        <Items>
                                                            <dx:ListEditItem Text="ใช้งาน" Value="1" Selected="true"/>
                                                            <dx:ListEditItem Text="ยกเลิกใช้งาน" Value="0" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                                <td width="15%">การไม่อนุญาตใช้งาน : </td>
                                                <td width="50%">
                                                    <dx:ASPxRadioButtonList runat="server" ID="cblconfirm" RepeatDirection="Horizontal">
                                                        <Items>
                                                            <dx:ListEditItem Text="อนุญาต" Value="1" Selected="true"/>
                                                            <dx:ListEditItem Text="ไม่อนุญาต" Value="N" />
                                                        </Items>
                                                    </dx:ASPxRadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="13%">ค้นหาบริษัทผู้ขนส่ง :</td>
                                    <td width="87%">
                                        <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="รหัสบริษัทผู้ขนส่ง,ชื่อบริษัทผู้ขนส่ง"
                                            CssClass="dxeLineBreakFix" Width="220px">
                                        </dx:ASPxTextBox>
                                        <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnCancelSearch" runat="server" SkinID="_cancelsearch" CssClass="dxeLineBreakFix">
                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('CancelSearch'); }">
                                            </ClientSideEvents>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false" SkinID="_gvw">
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="วันที่อัพเดท<br>ข้อมูลล่าสุด" FieldName="DUPDATE"
                                                    HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" Width="11%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="รหัสผู้ขนส่ง" FieldName="SVENDORID" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Width="9%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="บริษัทผู้ขนส่ง" HeaderStyle-HorizontalAlign="Center"
                                                    Width="18%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHyperLink runat="server" ID="hplSVENDORID" Text='<%# Eval("SABBREVIATION") %>'
                                                            Cursor="pointer">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('view;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxHyperLink>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ที่อยู่" FieldName="ADDRESS" HeaderStyle-HorizontalAlign="Center"
                                                    Width="28%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL" HeaderStyle-HorizontalAlign="Center"
                                                    Width="11%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="ใช้งาน" FieldName="" HeaderStyle-HorizontalAlign="Center"
                                                    Width="5%" CellStyle-HorizontalAlign="Center">
                                                    <DataItemTemplate>
                                                        <%-- <dx:ASPxCheckBox ID="chk" runat="server">
                                                           
                                                        </dx:ASPxCheckBox>--%>
                                                        <dx:ASPxButton ID="btn" runat="server" AutoPostBack="false" CausesValidation="False"
                                                            EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                            CssClass="dxeLineBreakFix" Width="16px" Height="16px">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('ChkInUse;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="การไม่อนุญาต<br>ใช้งาน" FieldName="CACTIVE" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Width="8%">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Width="7%" CellStyle-Cursor="hand" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                                    CellStyle-HorizontalAlign="Center" Caption="การจัดการ">
                                                    <DataItemTemplate>
                                                        <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                            <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                        </dx:ASPxButton>
                                                    </DataItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                    <CellStyle Cursor="hand">
                                                    </CellStyle>
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="CHECKIN" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="NOTFILL" Visible="false">
                                                </dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn FieldName="INUSE" Visible="false">
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager PageSize="50">
                                            </SettingsPager>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
