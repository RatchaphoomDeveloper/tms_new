﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraEditors;
using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Popup;
using DevExpress.Web.ASPxEditors;
using System.Globalization;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;

public partial class ucCalendarDetail : System.Web.UI.UserControl
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //  dteStart.Value = DateTime.Now.Date;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            string sMonthYear = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month) + "-" + (year + 543);
            // this.ddeCalendar.Text = sMonthYear;
            Session["sCalendarDate"] = null;
            //รหัส;typeหน้า;ความจุ;SEAL_HIT
            //00001;01;170000;1
            // Session["sCalendar"] = "000002;02;17000;0";
            string[] sValue = ("" + Session["sCalendar"]).Split(';');

            string QUERY = "SELECT  REQUEST_ID,NVL(SERVICE_DATE,NVL(APPOINTMENT_DATE,REQUEST_DATE))   as sDate FROM TBL_REQUEST WHERE REQUEST_ID = '" + CommonFunction.ReplaceInjection(sValue[0]) + "'";
            DataTable dt = CommonFunction.Get_Data(conn, QUERY);
            string sDate = "";
            if (dt.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dt.Rows[0]["sDate"] + ""))
                {
                    sDate = DateTime.Parse(dt.Rows[0]["sDate"] + "").ToString("yyyy-MM-dd", new CultureInfo("en-Us"));
                }
            }

            Calendar.GenerateJavaScriptFullMonthCalendarDetail(this.ltrmonthcalendar, month, year,sDate);

          

            if (sValue.Length > 0)
            {
                gvwCalendar2.DataSource = BindDataGrid("", sValue[0], 2);
                gvwCalendar2.DataBind();
            }
        }



    }


    protected void btnAddDataCalendar_Click(object sender, EventArgs e)
    {
        string sValue = CommonFunction.ReplaceInjection(txtValueCalendar.Text);

        //รหัส;typeหน้า;ความจุ;SEAL_HIT
        //00001;01;170000;1
        //(Session["sCalendar"] + "")

        // dvCalendar.Visible = false;
        Session["sCalendarDate"] = sValue;
        ASPxLabel lblCalendarDate = (ASPxLabel)Parent.FindControl("lblCalendarDate");

        if (lblCalendarDate != null)
        {
            lblCalendarDate.Text = Convert.ToDateTime(sValue).ToString("d MMMM yyyy") + "";
        }
    }



    protected void gvwCalendar1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        switch (e.CallbackName)
        {
            case "CUSTOMCALLBACK":
                string sDate = CommonFunction.ReplaceInjection(txtCalendarDateHide.Text);

                gvwCalendar1.DataSource = BindDataGrid(sDate, "", 1);
                gvwCalendar1.DataBind();
            
                break;
        }
    }


    private DataTable BindDataGrid(string sDate, string sRequestID, int nType)
    {
        DataTable dt = new DataTable();
        DateTime dTemp;
        string sCondition = "";




        string sql = "";



        string sqlCheck = "SELECT MIN(NVL(BEGIN_CAP,0)) As  NMIN,MAX(NVL(END_CAP,0)) As NMAX,STYPECAP  FROM TBL_CONDITIONS WHERE NVL(ACTIVE_FLAG,'Y') = 'Y' AND STYPECAP = '1' GROUP BY STYPECAP ORDER BY STYPECAP";
        System.Data.DataTable dtt = CommonFunction.Get_Data(conn, sqlCheck);


        string sMINS = "-1";
        string sMAXS = "-1";

        //รถเล็ก
        if (dtt.Rows.Count > 0)
        {
            sMINS = dtt.Rows[0]["NMIN"] + "";
            sMAXS = dtt.Rows[0]["NMAX"] + "";
        }



        switch (nType)
        {
            case 1:

                if (sDate != "")
                {
                    DateTime date = DateTime.TryParse(sDate, out dTemp) ? dTemp : DateTime.Now;
                    sCondition = " AND cb.DBOOKING = TO_DATE('" + date.ToString("yyyy-MM-dd", new CultureInfo("en-Us")) + "', 'yyyy-MM-dd')";
                }


                if (sRequestID != "")
                {

                    sCondition += " AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sRequestID) + "'";
                }

                sql = @"SELECT CASE WHEN  TREQ.TOTLE_CAP >= " + sMINS + " AND TREQ.TOTLE_CAP <= " + sMAXS + @"  THEN  'รถเล็ก' ELSE 'รถใหญ่' END AS STYPECAR ,  TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUCK.DWATEREXPIRE,TVEN.SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_ID,TREQTYPE.REQTYPE_NAME ,TREQ.REQUEST_OTHER_FLAG, TREQ.APPOINTMENT_DATE
                        FROM 
                        TBL_CALENDAR_BOOKING  cb 
                        INNER JOIN
                        TBL_Request TREQ
                        ON cb.SREFERENTID = TREQ.REQUEST_ID
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID WHERE 1 = 1 " + sCondition;
                break;
            case 2:


                if (sRequestID != "")
                {

                    sCondition = " AND TREQ.REQUEST_ID = '" + CommonFunction.ReplaceInjection(sRequestID) + "'";
                }

                sql = @"SELECT CASE WHEN  TREQ.TOTLE_CAP >= " + sMINS + " AND TREQ.TOTLE_CAP <= " + sMAXS + @"  THEN  'รถเล็ก' ELSE 'รถใหญ่' END AS STYPECAR,  TREQ.REQUEST_ID,TREQ.VENDOR_ID,TREQ.VEH_No,TRUCK.SHEADREGISTERNO,TREQ.TU_No,TRUCK.STRAILERREGISTERNO,TRUNC(TREQ.REQUEST_DATE) as REQUEST_DATE
                        ,TREQ.Status_Flag,TRUCK.DWATEREXPIRE,TVEN.SABBREVIATION
                        ,TSTATUS.STATUSREQ_NAME,TREQTYPE.REQTYPE_ID,TREQTYPE.REQTYPE_NAME ,TREQ.REQUEST_OTHER_FLAG,TREQ.APPOINTMENT_DATE
                        FROM 
                        TBL_Request TREQ
                        LEFT JOIN 
                        (
                            SELECT SVENDORID,SABBREVIATION FROM  TVENDOR  WHERE 1=1 
                        ) TVEN
                        ON TVEN.SVENDORID = TREQ.VENDOR_ID
                        LEFT JOIN TBL_REQTYPE TREQTYPE
                        ON TREQTYPE.REQTYPE_ID = TREQ.REQTYPE_ID
                        LEFT JOIN TBL_STATUSREQ TSTATUS
                        ON TSTATUS.STATUSREQ_ID = TREQ.Status_Flag
                        --ใช้หัวของ TBL_REQUEST จอย
                        LEFT JOIN  
                        (
                            SELECT STRUCKID,SHEADREGISTERNO,STRAILERREGISTERNO,
                            CASE  SCARTYPEID
                            WHEN 0 THEN SHEADREGISTERNO
                            WHEN 3 THEN SHEADREGISTERNO|| '/' || STRAILERREGISTERNO 
                            ELSE ''
                            END REGISTERNO
                            ,CASE SCARTYPEID WHEN 0 THEN TRUCK.DWATEREXPIRE 
                            WHEN 3 THEN WATER.DWATEREXPIRE  ELSE null END   as DWATEREXPIRE
                            FROM
                            (
                                SELECT * FROM TTRUCK WHERE SCARTYPEID in ('0','3')
                             ) TRUCK
                             LEFT JOIN 
                             (
                                SELECT SHEADID, DWATEREXPIRE FROM TTRUCK --WHERE STRUCKID = STRUCKID
                             )WATER
                            ON  WATER.SHEADID = TRUCK.STRUCKID
                        )TRUCK
                        ON TRUCK.STRUCKID = TREQ.VEH_ID WHERE 1 = 1 " + sCondition;
                break;

        }


        if (sql.Length > 0) dt = CommonFunction.Get_Data(conn, sql);

        return dt;
    }



}