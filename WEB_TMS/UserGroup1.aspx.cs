﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Application.Helper;
using TMS_BLL.UserGroupSelectBLL;
using TMS_BLL.Master;

public partial class UserGroup1 : PageBase
{
    DataTable dt;

    #region + View State +
    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtData"] != null)
                return (DataTable)ViewState["dtData"];
            else
                return null;
        }
        set
        {
            ViewState["dtData"] = value;
        }
    }

    private int SystemID
    {
        get
        {
            if ((int)ViewState["SystemID"] != null)
                return (int)ViewState["SystemID"];
            else
                return 0;
        }
        set
        {
            ViewState["SystemID"] = value;
        }
    }
    #endregion

    #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    #endregion
       

    private void InitialForm()
    {
        try
        {
            //Helper.ActiveMenu.SetActiveMenu(Master, "liAdmin");
           
            DataTable dtUserLogin = (DataTable)Session["UserLogin"];

            //SystemID = int.Parse(dtUserLogin.Rows[0]["SYSTEM_ID"].ToString());

            this.GetData();
            this.InitialStatus();
            this.AssignAuthen();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private DateTime? ConvertToDateNull(string strDate, string format = "dd/MM/yyyy")
    {
        try
        {
            return DateTime.ParseExact(strDate, format, null, DateTimeStyles.None);
        }
        catch
        {
            return null;
        }
    }

    private void InitialStatus()
    {
        try
        {
            DropDownListHelper.BindDropDown(ref ddlStatus, ConfigValueHelper.GetDefaultStatus(), "StatusID", "StatusName", true,  ddlStatus.SelectedValue);

            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'SPECIAL_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlSpecialStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdAdd_Click(object sender, EventArgs e)
    {
        //FormHelper.OpenForm("UserGroupAddEdit1.aspx" + "?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes("add"), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All), Page);       
        Response.Redirect("UserGroupAddEdit1.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes("add"), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All));
       
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        this.GetData();
    }

    private void AssignAuthen()
    {
        try
        {
            //if (!CanWrite)
            //{
            //    dgvData.Columns[1].Visible = false;
            //    cmdAdd.Visible = false;
            //}

            //if (!CanRead)
            //    dgvData.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        this.GetData();
    }

    private void GetData()
    {
        try
        {
            dtData = TMS_BLL.UserGroupSelectBLL.UserGroupBLL.Instance.UserGroupSelectBLL(this.GetConditionSearch());
            // + " AND M_USERGROUP.SYSTEM_ID = " + SystemID.ToString()
            GridViewHelper.BindGridView(ref dgvData, dtData);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionSearch()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (!string.Equals(txtUserGroup.Text.Trim(), string.Empty))
            { sb.Append(" AND MU.USERGROUP_NAME LIKE '%" + txtUserGroup.Text.Trim() + "%'"); }

            if (ddlStatus.SelectedIndex > 0)
            { sb.Append(" AND MU.IS_ACTIVE = " + ddlStatus.SelectedValue); }

            if (ddlSpecialStatus.SelectedIndex > 0)
            { sb.Append(" AND MU.IS_ADMIN = " + ddlSpecialStatus.SelectedValue); }

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvData_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        try
        {
            ImageButton imgButton = e.CommandSource as ImageButton;
            if (imgButton != null)
            {
                int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;

                if (string.Equals(e.CommandName, "View"))
                {
                    FormHelper.OpenForm("UserGroupAddEdit1.aspx" + "?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes("view"), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes(dgvData.DataKeys[Index]["USERGROUP_ID"].ToString()), MachineKeyProtection.All), Page);
                }
                else if (string.Equals(e.CommandName, "EditData"))
                {
                    FormHelper.OpenForm("UserGroupAddEdit1.aspx" + "?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes("edit"), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes(dgvData.DataKeys[Index]["USERGROUP_ID"].ToString()), MachineKeyProtection.All), Page);
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvData.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvData, dtData);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitializeComponent()
    {

    }

  
   
}