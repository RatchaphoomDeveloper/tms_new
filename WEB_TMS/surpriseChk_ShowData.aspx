﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="surpriseChk_ShowData.aspx.cs" Inherits="surpriseChk_ShowData" StylesheetTheme="Aqua" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .grvHeaderFlag {
            text-align: center !important;
        }
    </style>

    <style type="text/css">
        .search {
            background: url(../Images/ic_search.gif) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .time {
            background: url(../Images/ico_time.png) no-repeat;
            background-position-y: center;
            background-position-x: right;
            padding-left: 18px;
            border: 1px solid #ccc;
            background-color: white;
        }

        .auto-style1 {
            height: 21px;
        }

        #file-input {
            cursor: pointer;
            outline: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 0;
            height: 0;
            overflow: hidden;
            filter: alpha(opacity=0); /* IE < 9 */
            opacity: 0;
        }

        .input-label {
            cursor: pointer;
            position: relative;
            display: inline-block;
        }
    </style>

    <style type="text/css">
        .GridPager a,
        .GridPager span {
            display: inline-block;
            padding: 0px 9px;
            margin-right: 4px;
            border-radius: 3px;
            border: solid 1px #c0c0c0;
            background: #e9e9e9;
            box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
            font-size: .875em;
            font-weight: bold;
            text-decoration: none;
            color: #717171;
            text-shadow: 0px 1px 0px rgba(255,255,255, 1);
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background: #616161;
            box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
            color: #f0f0f0;
            text-shadow: 0px 0px 3px rgba(0,0,0, .5);
            border: 1px solid #3AC0F2;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract">ข้อมูลทั่วไป&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract">
                    <div class="panel-body">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="วันที่นัดหมายตรวจประจำปี" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="AppointDate" runat="server" CssClass="form-control datetimepicker" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="ผลิตภัณฑ์" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TYPE_NAME" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="ชื่อผู้ประกอบการ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" Enabled="false" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="จำนวนรถที่นัดหมาย" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="TotalCar" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="เลขที่สัญญา" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="จำนวนตรวจที่รถจริง" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="RealCheck" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="กลุ่มงานขนส่ง" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="GROUPS_NAME" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="สถานะ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="STATUS" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                    <asp:Label Text="หมายเหตุ" runat="server" />
                                </label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="Remark" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-4">
                                </label>
                                <div class="col-md-8">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right" runat="server" id="divButton">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <a data-toggle="collapse" href="#collapseFindContract" id="acollapseFindContract1">ข้อมูลรถ&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract1" value=" " />
                </div>
                <asp:Panel runat="server" ID="plCheckList">
                    <div class="panel-collapse collapse in" id="collapseFindContract1">
                        <div class="panel-body">
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="control-label col-md-4">
                                        <asp:Label Text="ทะเบียนหัว" runat="server" />
                                    </label>
                                    <div class="col-md-8">
                                        <asp:DropDownList ID="ddlShead" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlShead_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label col-md-4">
                                        <asp:Label Text="ทะเบียนหาง" runat="server" />
                                    </label>
                                    <div class="col-md-8">
                                        <asp:TextBox ID="txtStrailer" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-right" runat="server" id="div1">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" runat="server" Text="เพิ่มทะเบียน" OnClick="btnSave_Click" CssClass="btn btn-info" Enabled="false" />
                                <asp:Button ID="btnClear" runat="server" Text="ยกเลิก" OnClick="btnClear_Click" CssClass="btn btn-danger" Enabled="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapseFindContract2" id="acollapseFindContract2">แสดงข้อมูล&#8711;</a>
                    <input type="hidden" id="hiddencollapseFindContract2" value=" " />
                </div>
                <div class="panel-collapse collapse in" id="collapseFindContract2">
                    <div class="panel-body">
                        <asp:GridView runat="server" DataKeyNames="ID,SHEADREGISTERNO,STRAILERREGISTERNO,IS_ACTIVE,REMARK,DOC_ID,SCONTRACTID,SCONTRACTNO,SVENDORID,SABBREVIATION,STATUS_NAME,STRUCKID,APPOINTDATE" ID="dgvSurpriseCheck" Width="100%" HeaderStyle-HorizontalAlign="Center"
                            CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                            AlternatingRowStyle-BackColor="White" RowStyle-ForeColor="#284775" OnPageIndexChanging="dgvSurpriseCheck_PageIndexChanging"
                            OnRowUpdating="dgvSurpriseCheck_RowUpdating" OnRowCommand="dgvSurpriseCheck_RowCommand">
                            <Columns>
                                <asp:TemplateField HeaderText="ลำดับที่">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SHEADREGISTERNO" HeaderText="ทะเบียนหัว">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="STRAILERREGISTERNO" HeaderText="ทะเบียนหาง">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ผ่าน">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label Text='<%# Eval("STATUS_NAME") %>' ID="lblSTATUS_NAME" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ผ่าน">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:CheckBox Checked='<%# Eval("IS_ACTIVE") + string.Empty == "1" ? true : false %>' runat="server" Enabled='<%# Eval("IS_ACTIVE") + string.Empty == "0" ? true : false %>' AutoPostBack="true" ID="cbIS_ACTIVE" OnCheckedChanged="cbIS_ACTIVE_CheckedChanged" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="แก้ไข">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/64/blue-23.png" Width="23"
                                            Height="23" Style="cursor: pointer" CommandName="update" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDel" runat="server" ImageUrl="~/Images/bin1.png" Width="23"
                                            Height="23" Style="cursor: pointer" CommandName="delete" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="REMARK" HeaderText="หมายเหตุ" Visible="false">
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="แนบไฟล์">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDoc" runat="server" ImageUrl="~/Images/blue-document-download-icon.png" Width="23"
                                            Height="23" Style="cursor: pointer" CommandName="file" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pagination-ys" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                        </asp:GridView>
                    </div>
                </div>
                <br />
                <br />
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="plStep1" CssClass="hidden">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse4" id="acollapse4">
                            <asp:Label Text="" ID="lblSCHECKLISTNAME" runat="server" /></a>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse in">
                        <div class="panel-body">

                            <div class="row form-group">
                                <label class="col-md-2 control-label">แนบเอกสาร<asp:Label ID="Label1" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                <div class="col-md-3">
                                    <asp:FileUpload ID="fileUploadStep1" accept="image/*" runat="server" />
                                </div>
                                <label class="col-md-2 control-label">รายละเอียด</label>
                                <div class="col-md-3">
                                    <asp:TextBox runat="server" ID="txtDetailStep1" CssClass="form-control " />
                                </div>

                                <div class="col-md-2">

                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <asp:Button runat="server" ID="btnAddStep1" Text="Add" OnClick="btnAddStep1_Click" CssClass="btn btn-md bth-hover btn-info " />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAddStep1" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                            <asp:GridView runat="server" ID="gvFileStep1"
                                Width="100%" HeaderStyle-HorizontalAlign="Center"
                                GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" AutoGenerateColumns="false"
                                HorizontalAlign="Center" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AllowPaging="false" DataKeyNames="ID" OnRowDeleting="gvFileStep1_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="ที่" ItemStyle-Wrap="false">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="รูปภาพ">
                                        <ItemTemplate>
                                            <a href='<%# Eval("FULLPATH") %>' target="_blank">
                                                <asp:Image ID="imgStep1" runat="server" ImageUrl='<%# Eval("FULLPATH") %>' Width="100%" /><br />

                                                <asp:Label Text='<%# "รายละเอียด : " + Eval("REMARK") %>' ID="lblDetail" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Button Text="ลบ" CssClass="btn btn-danger" CommandName="delete" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <asp:Button Text="บันทึกรายการตรวจสอบ" ID="Button1" OnClick="btnCloseStep1_Click" CssClass="btn btn-info" runat="server" />
                        <asp:Button Text="กลับ" ID="btnCloseStep1" OnClick="btnCloseStep1_Click" CssClass="btn btn-warning" runat="server" />
                    </div>
                    <asp:HiddenField runat="server" ID="hidcollapse4" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hidRowIndex" runat="server" />
            <asp:HiddenField ID="hidM_CHECK_CONTRACT_LIST_ID" runat="server" />
            <asp:HiddenField runat="server" ID="hidID" />
            <asp:HiddenField runat="server" ID="hidIS_ACTIVE" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


