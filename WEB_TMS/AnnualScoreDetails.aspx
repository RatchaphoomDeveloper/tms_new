﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AnnualScoreDetails.aspx.cs" Inherits="AnnualScoreDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
            font-size:11px !important;
        }

         .GridColorHeader td {
            text-align: center !important;
            align-content: center !important;
            font-size:11px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div style="display: none;">
                <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID">
                </asp:DropDownList><asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                </asp:DropDownList>
            </div>

            <div class="panel panel-info" style="margin-top: 20px; display: none;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlYearSearch.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearch" Enabled="false" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=txtCARREGISTERNO.ClientID %>" class="col-md-1 control-label">ทะเบียนรถ</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtCARREGISTERNO" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                                <label for="<%=txtEMPLOYEE.ClientID %>" class="col-md-2 control-label">ชื่อ พขร. / ID พขร.</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtEMPLOYEE" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row" style="display: none">
                                <label class="col-md-1 control-label">ช่วงเวลา </label>
                                <hr />
                            </div>
                            <div class="form-group form-horizontal row" style="display: none">

                                <label for="<%=rdoQuarter.ClientID %>" class="col-md-2 control-label">
                                    <asp:RadioButton ID="rdoQuarter" GroupName="TimeRange" Text="&nbsp;ไตรมาส" runat="server" /></label>
                                <div class="col-md-1">
                                    <asp:DropDownList ID="ddlQuarter" runat="server" CssClass="form-control marginTp">
                                        <asp:ListItem Text="1"></asp:ListItem>
                                        <asp:ListItem Text="2"></asp:ListItem>
                                        <asp:ListItem Text="3"></asp:ListItem>
                                        <asp:ListItem Text="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=rdoMonth.ClientID %>" class="col-md-1 control-label">
                                    <asp:RadioButton ID="rdoMonth" GroupName="TimeRange" Text="&nbsp;เดือน" runat="server" />
                                </label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control marginTp">
                                        <asp:ListItem Value="01" Text="มกราคม"></asp:ListItem>
                                        <asp:ListItem Value="02" Text="กุมภาพันธ์"></asp:ListItem>
                                        <asp:ListItem Value="03" Text="มีนาคม"></asp:ListItem>
                                        <asp:ListItem Value="04" Text="เมษายน"></asp:ListItem>
                                        <asp:ListItem Value="05" Text="พฤษภาคม"></asp:ListItem>
                                        <asp:ListItem Value="06" Text="มิถุนายน"></asp:ListItem>
                                        <asp:ListItem Value="07" Text="กรกฎาคม"></asp:ListItem>
                                        <asp:ListItem Value="08" Text="สิงหาคม"></asp:ListItem>
                                        <asp:ListItem Value="09" Text="กันยายน"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="ตุลาคม"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="พฤศจิกายน"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="ธันวาคม"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=rdoDate.ClientID %>" class="col-md-1 control-label">
                                    <asp:RadioButton ID="rdoDate" GroupName="TimeRange" Text="&nbsp;วันที่" runat="server" />
                                </label>
                                <div class="col-md-5">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtDateStart" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="control-label">&nbsp;ถึง&nbsp;</label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="form-group form-horizontal row" style="text-align: right">
                                <%--<asp:Button ID="btnExport" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>รายละเอียดข้อมูลการตัดคะแนน
                </div>
                <div class="panel-body">
                    <div class="form-group form-horizontal row" style="text-align: left">
                        <label for="<%=lblYear.ClientID %>" class="col-md-1 control-label">ปี:</label>
                        <div style="text-align: left" class="col-md-1 control-label">
                            <asp:Label runat="server" ID="lblYear"></asp:Label>
                        </div>
                        <label for="<%=lblVendor.ClientID %>" class="col-md-1 control-label">บริษัท:</label>
                        <div style="text-align: left" class="col-md-2 control-label">
                            <asp:Label runat="server" ID="lblVendor"></asp:Label>
                        </div>
                        <label for="<%=lblGroup.ClientID %>" class="col-md-1 control-label">กลุ่มงาน:</label>
                        <div style="text-align: left" class="col-md-1 control-label">
                            <asp:Label runat="server" ID="lblGroup"></asp:Label>
                        </div>
                        <label for="<%=lblContNo.ClientID %>" class="col-md-2 control-label">เลขที่สัญญา:</label>
                        <div style="text-align: left" class="col-md-3 control-label">
                            <asp:Label runat="server" ID="lblContNo"></asp:Label>
                        </div>
                    </div>
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div class="form-group form-horizontal row" style="text-align: right">
                                <asp:Button ID="btnExport" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                <asp:Button ID="btnClose" runat="server" Text="ปิด" UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" OnClick="btnClose_Click" Style="width: 80px" />
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" RowStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="NREDUCEID"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True" Font-Size="Medium">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" Width="30px" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="หัวข้อตัดคะแนน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"   />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSPROCESSNAME" runat="server" Text='<%# Eval("SPROCESSNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="center" HeaderText="รายละเอียด" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"  />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSTOPICNAME" runat="server" Text='<%# Eval("STOPICNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="วันที่ตัดคะแนน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"  />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDREDUCE" runat="server" Text='<%# Eval("DREDUCE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="ทะเบียนหัว" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"  />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSHEADREGISTERNO" runat="server" Text='<%# Eval("SHEADREGISTERNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="ทะเบียนหาง" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"  />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSTRAILERREGISTERNO" runat="server" Text='<%# Eval("STRAILERREGISTERNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="center" HeaderText="ชื่อ พขร." HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"/>
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSEMPLOYEENAME" runat="server" Text='<%# Eval("SEMPLOYEENAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="ระงับ (วัน)" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDISABLE_DRIVER" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="Hold" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblHOLD" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="Blacklist" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBLACKLIST" runat="server" Text='<%# Eval("BLACKLIST") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   ItemStyle-CssClass="center" HeaderText="ตัดคะแนน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lbSUMNPOINT" runat="server" Text='<%# Eval("SUMNPOINT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="ค่าปรับ+ค่าเสียหาย" DataField="COST" DataFormatString="{0:N}">
                                <ItemStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="ผู้พิจารณา" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"  />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol"  />
                                <ItemTemplate>
                                    <div style="align-content:center;text-align:center"><asp:Label ID="lblSREDUCEBY" runat="server" Text='<%# Eval("SREDUCEBY") %>'></asp:Label></div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="วันที่ตัดสิน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDREDUCE2" runat="server" Text='<%# Eval("APPROVE_DATE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-CssClass="center" HeaderText="การอุทธรณ์" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSTATUS" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

