﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="questionnaire_old.aspx.cs" Inherits="questionnaire" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2.Export" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; }"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td>
                            <div style="float: left">
                                <div style="float: left">
                                    <dx:ASPxButton ID="btnXlsExport" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                        OnClick="btnXlsExport_Click">
                                        <Image Width="22px" Height="22px" Url="Images/ic_ms_excel.gif">
                                        </Image>
                                    </dx:ASPxButton>
                                </div>
                                <div style="float: left">Excel</div>
                            </div>
                            <div style="float: left">
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            <div style="float: left">
                                <dx:ASPxButton ID="btnAddData" runat="server" Width="120px" Text="บันทึกผลการตรวจ"
                                    AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e) { ASPxClientEdit.ClearGroup('add'); window.location = 'questionnaire_form.aspx'; }" />
                                </dx:ASPxButton>
                            </div>
                            <div style="float: left;">&nbsp; บริษัทผู้ประกอบการขนส่ง  &nbsp;</div>
                            <div style="float: left">
                                <dx:ASPxComboBox ID="cmbVendor" runat="server" TextField="SABBREVIATION" ValueField="SVENDORID"></dx:ASPxComboBox>
                                <dx:ASPxTextBox Visible="false" ID="txtSearch" runat="server" Width="200px" NullText="ชื่อผู้ประกอบการขนส่ง">
                                </dx:ASPxTextBox>
                            </div>
                            <dx:ASPxTextBox ID="dateStart" ClientInstanceName="dateStart" Width="10px" ClientVisible="false"
                                runat="server">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="dateEnd" ClientInstanceName="dateStart" Width="10px" ClientVisible="false"
                                runat="server">
                            </dx:ASPxTextBox>
                        </td>
                        <td>ปี ที่ประเมิน
                        </td>
                        <td align="center">
                            <dx:ASPxComboBox ID="cmbYear" runat="server" Width="100px" SelectedIndex="0">
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td style="display: none;">
                            <dx:ASPxRadioButtonList ID="rblCheck" runat="server" SkinID="rblStatus">
                                <ClientSideEvents SelectedIndexChanged="function(s,e){cmbMonth.PerformCallback();}" />
                                <Items>
                                    <dx:ListEditItem Text="รายไตรมาส" Value="1" Selected="true" />
                                    <dx:ListEditItem Text="รายเดือน" Value="2" />
                                </Items>
                            </dx:ASPxRadioButtonList>
                        </td>
                        <td style="display: none;">
                            <dx:ASPxComboBox ID="cmbMonth" ClientInstanceName="cmbMonth" runat="server" Width="100px"
                                OnCallback="cmbMonth_Callback">
                            </dx:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%" SettingsPager-PageSize="15"
                                OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" OnAfterPerformCallback="gvw_AfterPerformCallback">
                                <ClientSideEvents EndCallback="function(s,e){if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}"></ClientSideEvents>
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" VisibleIndex="1" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SVENDORID" Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" FieldName="SVENDORNAME" VisibleIndex="3"
                                        Width="20%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานที่ตั้ง" FieldName="ADDRESS" VisibleIndex="4">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่ตรวจฯ" FieldName="DCHECK1" VisibleIndex="3"
                                        Width="100">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn FieldName="NQUESTIONNAIREID1" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NQUESTIONNAIREID2" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="NNOMAX" Visible="false">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="คะแนนรวม" FieldName="NTOTAL" VisibleIndex="7"
                                        Width="12%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn VisibleIndex="13" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbView1" runat="server" CausesValidation="False" AutoPostBack="false"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                <Image Width="16px" Height="16px" Url="Images/search.png">
                                                </Image>
                                            </dx:ASPxButton>
                                            &nbsp;
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand" VerticalAlign="Middle" HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT vs.SVENDORID,vs.SVENDORNAME,vs.SNO || ' ' || vs.SDISTRICT || ' ' || vs.SREGION || ' ' || vs.SPROVINCE || ' ' || VS.SPROVINCECODE AS ADDRESS,
MAX(CASE WHEN Q.NNO = 1 THEN Q.DCHECK END) AS DCHECK1 , MAX(CASE WHEN Q.NNO = 1 THEN Q.NQUESTIONNAIREID END ) AS NQUESTIONNAIREID1, 
MAX(CASE WHEN Q.NNO = 2 THEN Q.DCHECK END) AS DCHECK2 , MAX(CASE WHEN Q.NNO = 2 THEN Q.NQUESTIONNAIREID END) AS NQUESTIONNAIREID2, 
MAX(CASE WHEN Q.NNO = 3 THEN Q.DCHECK END) AS DCHECK3 , MAX(CASE WHEN Q.NNO = 3 THEN Q.NQUESTIONNAIREID END) AS NQUESTIONNAIREID3, 
SUM(Q.NVALUE) AS NTOTAL,MAX(Q.NNO) AS NNOMAX  FROM TVENDOR_SAP vs INNER JOIN TQUESTIONNAIRE q ON VS.SVENDORID = Q.SVENDORID WHERE (vs.SVENDORNAME LIKE '%' || :oSearch || '%' ) AND nvl(Q.DCHECK,sysdate) Between To_Date(:dStart,'dd/MM/yyyy') And To_Date(:dEnd,'dd/MM/yyyy') GROUP BY vs.SVENDORID , vs.SVENDORNAME, vs.SNO  || ' '   || vs.SDISTRICT   || ' '   || vs.SREGION   || ' '   || vs.SPROVINCE   || ' '   || VS.SPROVINCECODE  ORDER BY vs.SVENDORNAME "
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" DeleteCommand="DELETE FROM TCOMPLAIN WHERE SSERVICEID = :SSERVICEID">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="cmbVendor" Name="oSearch" PropertyName="Text" Type="String" />
                                    <asp:SessionParameter Name="dStart" SessionField="dateStart" />
                                    <asp:SessionParameter Name="dEnd" SessionField="dateEnd" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvw">
                                <Styles>
                                    <Header Font-Names="Tahoma" Font-Size="10">
                                    </Header>
                                    <Cell Font-Names="Tahoma" Font-Size="8">
                                    </Cell>
                                </Styles>
                            </dx:ASPxGridViewExporter>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
