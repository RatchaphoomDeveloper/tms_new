﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_Entity;
using Utility;
using TMS_BLL.Transaction.Complain;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Text;
using EmailHelper;
using SelectPdf;

public partial class AnnualReportNotice : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    string PageFileUpload = "ANNUAL_REPORT_FILE";
    string GroupPermission = "01";

    #region " Prop "

    protected DataTable SearhData
    {
        get { return (DataTable)ViewState[this.ToString() + "SearhData"]; }
        set { ViewState[this.ToString() + "SearhData"] = value; }
    }
    protected DataTable dtGrade
    {
        get { return (DataTable)ViewState[this.ToString() + "dtGrade"]; }
        set { ViewState[this.ToString() + "dtGrade"] = value; }
    }

    protected string ContentGrade
    {
        get
        {
            return @"<p class='MsoListParagraph' style='margin-top:0cm;margin-right:0cm;margin-bottom:
0cm;margin-left:54.0pt;margin-bottom:.0001pt;mso-add-space:auto;text-align:
justify;text-indent:-18.0pt;line-height:normal;mso-list:l0 level1 lfo1'><span style='font-size:16.0pt;font-family:&quot;Cordia New&quot;,&quot;sans-serif&quot;;mso-ascii-theme-font:
minor-bidi;mso-fareast-font-family:&quot;Cordia New&quot;;mso-fareast-theme-font:minor-bidi;
mso-hansi-theme-font:minor-bidi;mso-bidi-font-family:&quot;Cordia New&quot;;mso-bidi-theme-font:
minor-bidi'>{0}.<span style='font-variant-numeric: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang='TH' style='font-size:16.0pt;
font-family:&quot;Cordia New&quot;,&quot;sans-serif&quot;;mso-ascii-theme-font:minor-bidi;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-hansi-theme-font:minor-bidi'>เลขที่สัญญา
</span><span style='font-size:16.0pt;font-family:&quot;Cordia New&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-hansi-theme-font:minor-bidi'>{1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang='TH'>ผลการประเมินได้เกรด&nbsp; </span>{2}&nbsp;<o:p></o:p></span></p><p class='MsoListParagraphCxSpLast' style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:54.0pt;margin-bottom:.0001pt;mso-add-space:auto;
text-align:justify;text-indent:-18.0pt;line-height:normal;mso-list:l0 level1 lfo1'><span style='font-size:16.0pt;font-family:&quot;Cordia New&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-bidi;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-hansi-theme-font:minor-bidi'><o:p></o:p></span></p>";
        }
    }
    protected string ContentForm
    {
        get { return ViewState[this.ToString() + "ContentForm"].ToString(); }
        set { ViewState[this.ToString() + "ContentForm"] = value; }
    }

    protected string FileNameNotice
    {
        get { return ViewState[this.ToString() + "FileNameNotice"].ToString(); }
        set { ViewState[this.ToString() + "FileNameNotice"] = value; }
    }

    protected DataTable ReportData
    {
        get { return (DataTable)ViewState[this.ToString() + "ReportData"]; }
        set { ViewState[this.ToString() + "ReportData"] = value; }
    }

    protected FinalGade SearchCriteria
    {
        get { return (FinalGade)ViewState[this.ToString() + "SearchCriteria"]; }
        set { ViewState[this.ToString() + "SearchCriteria"] = value; }
    }

    protected List<T_ANNUAL_REPORT> lstAnnualDetail
    {
        get { return (List<T_ANNUAL_REPORT>)ViewState[this.ToString() + "SearchReportCriteria"]; }
        set { ViewState[this.ToString() + "SearchReportCriteria"] = value; }
    }

    protected T_ANNUAL_REPORT SearchReportCriteria
    {
        get { return (T_ANNUAL_REPORT)ViewState[this.ToString() + "SearchReportCriteria"]; }
        set { ViewState[this.ToString() + "SearchReportCriteria"] = value; }
    }
    protected USER_PERMISSION Page_Status
    {
        get { return (USER_PERMISSION)ViewState[this.ToString() + "Page_Status"]; }
        set { ViewState[this.ToString() + "Page_Status"] = value; }
    }
    protected PAGE_ACTION PageAction
    {
        get { return (PAGE_ACTION)ViewState[this.ToString() + "PageAction"]; }
        set { ViewState[this.ToString() + "PageAction"] = value; }
    }
    protected UserProfile user_profile
    {
        get { return (UserProfile)ViewState[this.ToString() + "user_profile"]; }
        set { ViewState[this.ToString() + "user_profile"] = value; }
    }
    protected string SPROCESSID
    {
        get { return ViewState[this.ToString() + "SPROCESSID"].ToString(); }
        set { ViewState[this.ToString() + "SPROCESSID"] = value; }
    }

    protected string QDocID
    {
        get { return ViewState[this.ToString() + "QDocID"].ToString(); }
        set { ViewState[this.ToString() + "QDocID"] = value; }
    }


    #endregion " Prop "
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            QDocID = "0";
            user_profile = SessionUtility.GetUserProfileSession();
            CheckPermission();

            if (Session["AARN_SABBREVIATION"] == null || Session["AARN_YEAR"] == null || Session["AARN_SVENDORID"] == null) btnClose_Click(null, null);
            else
            {

                if (Session["AARN_QDocID"] != null)
                {
                    QDocID = Session["AARN_QDocID"].ToString();
                    Session["AARN_QDocID"] = null;
                }

                SearchCriteria = new FinalGade()
                {
                    YEAR = Session["AARN_YEAR"].ToString(),
                    VENDORID = Session["AARN_SVENDORID"].ToString()
                };

                lblVendor.Text = Session["AARN_SABBREVIATION"].ToString();

                Session["AARN_SABBREVIATION"] = null;
                Session["AARN_YEAR"] = null;
                Session["AARN_SVENDORID"] = null;

                lblYear.Text = SearchCriteria.YEAR;

            }
            InitForm();
            LoadMain();
        }

        RegisterPostDownload();
    }
    public void CheckPermission()
    {
        if (Session["UserID"] == null)
        {
            GotoDefault();
            return;
        }

        //if (!GroupPermission.Contains(user_profile.CGROUP))
        //{
        //    GotoDefault();
        //    return;
        //}

        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            btnCancel.Visible = false;
            btnSave.Visible = false;
            btnPreview.Visible = false;
            btnExport.Visible = false;
            btnBfSend.Visible = false;
            btnSend.Visible = false;
            btnExportPdf.Visible = false;

            txtDOCDATE.Enabled = false;
            txtDOCNO.Enabled = false;

            divFormNotice.Visible = false;
            tblFormNotice.Visible = false;
            dvFlie.Visible = false;
            btnClose.Visible = false;
        }
        else
        {
            divVendorView.Visible = false;
            grvMain.Visible = false;
        }
    }

    public void GotoDefault()
    {
        Response.Redirect("default.aspx");
    }

    private void LoadGrvToDOwnload()
    {
        try
        {
            string _err = string.Empty;
            SearchReportCriteria.YEAR = ddlYearSearch.SelectedItem.Value;
            SearchReportCriteria.STATUS_ID = "3";
            ReportData = new FinalGradeBLL().GetReport(ref _err, SearchReportCriteria);
            /// รอแจ้งข้อมูลหัวข้อย่อย	1
            /// รอแจ้งผล (ข้อมูลครบ)	2
            /// แจ้งผลแล้ว	3
            /// ยกเลิกเอกสาร	4 
            grvMain.DataSource = ReportData;
            grvMain.DataBind();
        }
        catch (Exception ex)
        {

            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }
    private void LoadMain(bool IsNewPreview = false)
    {
        try
        {

            ContentForm = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_FORM.txt"));

            string _err = string.Empty;
            SearhData = new FinalGradeBLL().GetFinalScore(ref _err, SearchCriteria, SPROCESSID, false);
            dtGrade = new FinalGradeBLL().GetGrade(ref _err);
            if (SearhData != null)
            {
                foreach (DataRow dr in SearhData.Rows)
                {
                    var vendorID = dr["SVENDORID"].ToString();
                    var ContractID = int.Parse(dr["SCONTRACTID"].ToString());
                    bool ISPTT = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? true : false;
                    var KPIPoint = new KPI_Helper().GetKPIYear(SearchCriteria.YEAR, vendorID, ContractID, ISPTT);
                    dr["KPI_SCORE_BY_YEAR"] = KPIPoint.ToString("0");
                    dr["AUDIT_SCORE"] = decimal.Parse(dr["AUDIT_SCORE"].ToString()).ToString("0");
                    dr["ALL_QUARTER"] = decimal.Parse(dr["ALL_QUARTER"].ToString()).ToString("0");

                    dr["SUM_ALL_SCORE"] = (decimal.Parse(dr["AUDIT_SCORE"].ToString()) + decimal.Parse(dr["KPI_SCORE_BY_YEAR"].ToString()) + decimal.Parse(dr["ALL_QUARTER"].ToString())).ToString("0");

                    //GET GRADE 
                    string FinalPoint = dr["SUM_ALL_SCORE"].ToString();
                    string expression = string.Format(" '{0}' >= NSTARTPOINT and '{1}' <= NENDPOINT ", FinalPoint, FinalPoint);
                    DataRow[] foundRows = null;
                    foundRows = dtGrade.Select(expression);
                    if (foundRows != null) dr["GRADE"] = foundRows[0]["SGRADENAME"];
                }
            }

            string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty, HEDERTITLE = string.Empty, TITLE = "บริษัท";
            if (lblVendor.Text.Trim().Contains("บริษัท"))
            {
                SVENDORNAME = lblVendor.Text.Trim().Replace("บริษัท", "");
                HEDERTITLE = "กรรมการผู้จัดการ";
            }
            else
            {
                SVENDORNAME = lblVendor.Text.Trim().Replace("ห้างหุ้นส่วนจำกัด", "");
                SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
                TITLE = "ห้าง";
                HEDERTITLE = "หุ้นส่วนผู้จัดการ";
            }

            SearchReportCriteria = new T_ANNUAL_REPORT() { VENDORID = SearchCriteria.VENDORID, YEAR = SearchCriteria.YEAR };
            if (int.Parse(QDocID) > 0) SearchReportCriteria.ID = QDocID;
            if (user_profile.CGROUP == GROUP_PERMISSION.VENDOR) SearchReportCriteria.STATUS_ID = "3";
            ReportData = new FinalGradeBLL().GetReport(ref _err, SearchReportCriteria);

            /// รอแจ้งข้อมูลหัวข้อย่อย	1
            /// รอแจ้งผล (ข้อมูลครบ)	2
            /// แจ้งผลแล้ว	3
            /// ยกเลิกเอกสาร	4 
            if (ReportData.Rows.Count > 0)
            {
                if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
                {
                    grvMain.DataSource = ReportData;
                    grvMain.DataBind();
                }

                if (("34").Contains(ReportData.Rows[0]["STATUS_ID"].ToString()))
                {
                    btnBfSend.Enabled = false;
                    btnSend.Enabled = false;
                    if (ReportData.Rows[0]["STATUS_ID"].ToString() != "4")
                    {
                        btnCancel.Enabled = true;
                    }
                    btnPreview.Enabled = false;
                    txtDOCDATE.Enabled = false;
                    txtDOCNO.Enabled = false;
                }

                if (!IsNewPreview)
                {
                    FileNameNotice = ReportData.Rows[0]["FILE_NAME"].ToString();
                    if (!string.IsNullOrEmpty(FileNameNotice))
                    {
                        try
                        {
                            ContentForm = File.ReadAllText(Server.MapPath(String.Format("~/UploadFile/annual_report/notice/{0}.txt", FileNameNotice)));
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("ไม่พบข้อมูลเอกสารแนบ");
                        }
                    }

                    if (ReportData.Rows[0]["STATUS_ID"].ToString() != "4")
                    {
                        ContentForm = ContentForm.Replace("{DOCNO}", ReportData.Rows[0]["DOCNO"].ToString().Trim());
                        ContentForm = ContentForm.Replace("{DOCDATE}", ReportData.Rows[0]["DOC_DATE"].ToString().Trim());
                        QDocID = ReportData.Rows[0]["ID"].ToString();
                    }

                    DTFile = new FinalGradeBLL().GetUploadFile(ref _err, int.Parse(QDocID));
                }
                else
                {
                    ContentForm = ContentForm.Replace("{DOCNO}", txtDOCNO.Text.Trim());
                    ContentForm = ContentForm.Replace("{DOCDATE}", txtDOCDATE.Text.Trim());
                }
            }
            else
            {
                DTFile = new FinalGradeBLL().GetUploadFile(ref _err, int.Parse(QDocID));
                if (!string.IsNullOrEmpty(txtDOCDATE.Text))
                    ContentForm = ContentForm.Replace("{DOCDATE}", txtDOCDATE.Text.Trim());

                if (!string.IsNullOrEmpty(txtDOCNO.Text.Trim())) ContentForm = ContentForm.Replace("{DOCNO}", txtDOCNO.Text.Trim());
            }
            InitialUpload();

            ContentForm = ContentForm.Replace("{TITLE}", TITLE);
            ContentForm = ContentForm.Replace("{HEDERTITLE}", HEDERTITLE);
            ContentForm = ContentForm.Replace("{YEAR}", YearAdd543(lblYear.Text.Trim()));
            ContentForm = ContentForm.Replace("{SVENDORTITLE}", SVENDORTITLE);
            ContentForm = ContentForm.Replace("{SVENDORNAME}", SVENDORNAME);
            string detailGrade = string.Empty;

            for (var i = 1; i <= SearhData.Rows.Count; i++)
            {
                detailGrade += ContentGrade.Replace("{0}", i.ToString()).Replace("{1}", SearhData.Rows[i - 1]["SCONTRACTNO"].ToString()).Replace("{2}", SearhData.Rows[i - 1]["GRADE"].ToString());
            }

            ContentForm = ContentForm.Replace("{RESULTS}", detailGrade);
            divEmail.InnerHtml = ContentForm;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    private string YearAdd543(string Year)
    {
        return (!string.IsNullOrEmpty(Year) ? int.Parse(Year) + 543 + string.Empty : string.Empty);
    }

    private void InitForm()
    {
        try
        {
            if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
            {
                int StartYear = 2015;
                int cYear = DateTime.Today.Year;
                int endYear = cYear + 4;
                int index = 0;
                for (int i = StartYear; i <= endYear; i++)
                {
                    ddlYearSearch.Items.Insert(index, new ListItem((i).ToString(), (i).ToString()));
                }

                // เลือกปีปัจจุบัน
                ListItem sel = ddlYearSearch.Items.FindByValue(SearchCriteria.YEAR);
                ddlYearSearch.ClearSelection();
                if (sel != null) sel.Selected = true;
                else ddlYearSearch.SelectedIndex = 0;

                ddlYearSearch.Enabled = false;
            }

            DataTable dt = QuarterReportBLL.Instance.GetSPROCESSID();
            if (dt != null)
            {
                List<string> lstStirng = new List<string>();
                foreach (DataRow itm in dt.Rows) lstStirng.Add(itm["SPROCESSID"].ToString());

                SPROCESSID = string.Join(",", lstStirng.ToArray());
            }
            else
                SPROCESSID = "''";
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    #region " Upload File New "

    public List<F_UPLOAD> DTFile
    {
        get
        {
            if ((List<F_UPLOAD>)ViewState["dtUpload"] != null)
                return (List<F_UPLOAD>)ViewState["dtUpload"];
            else
                return null;
        }
        set { ViewState["dtUpload"] = value; }
    }
    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            DTFile.RemoveAt(e.RowIndex);
            dgvUploadFile.DataSource = DTFile;
            dgvUploadFile.DataBind();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridView gv = (GridView)sender;
        string FullPath = gv.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        string path = Path.GetDirectoryName(FullPath);
        string FileName = Path.GetFileName(FullPath);

        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(path + "\\" + FileName);
        Response.End();
    }

    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");

                if (imgDelete != null)
                {
                    imgDelete.Visible = user_profile.CGROUP != GROUP_PERMISSION.UNIT ? false : true;
                    imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
                }

                ImageButton imgView;
                imgView = (ImageButton)e.Row.Cells[0].FindControl("imgView");

                if (imgView != null)
                {
                    ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(imgView);
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    public void RegisterPostDownload()
    {
        foreach (GridViewRow row in dgvUploadFile.Rows)
        {
            if (row.RowType != DataControlRowType.DataRow) continue;

            ImageButton imgView = row.Cells[0].FindControl("imgView") as ImageButton;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(imgView);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private string CheckPath()
    {
        string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "AnnualReport" + "\\" + QDocID;
        try
        {
            if (!Directory.Exists(PathExport)) Directory.CreateDirectory(PathExport);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

        return PathExport;
    }

    private void InitialUpload()
    {
        try
        {
            dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("ANNUAL_REPORT_FILE");
            DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
            if (int.Parse(QDocID) > 0)
            {
                dgvUploadFile.DataSource = DTFile;
                dgvUploadFile.DataBind();
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }

    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                DTFile.Add(new F_UPLOAD()
                {
                    FILENAME_SYSTEM = System.IO.Path.GetFileName(Path + "\\" + FileNameSystem),
                    FULLPATH = Path + "\\" + FileNameSystem,
                    ID = 0,
                    FILENAME_USER = FileNameUser,
                    UPLOAD_ID = int.Parse(cboUploadType.SelectedValue),
                    UPLOAD_NAME = cboUploadType.SelectedItem.Text
                });

                dgvUploadFile.DataSource = DTFile;
                dgvUploadFile.DataBind();
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
        }
    }

    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            foreach (var item in DTFile)
            {
                data.Add(decimal.Parse(item.UPLOAD_ID.ToString()));
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    #endregion " Upload File New "
    protected void btnPreview_Click(object sender, EventArgs e)
    {
        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            LoadGrvToDOwnload();
        }
        else
        {
            LoadMain(true);
        }
    }


    #region " Send Email "

    private bool SaveNotice()
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            using (OracleTransaction tran = con.BeginTransaction())
            {
                try
                {
                    string _err = string.Empty;
                    if (int.Parse(QDocID) > 0)
                    {

                        if (btnPreview.Enabled != false)
                        {
                            string sqlMain = @" UPDATE T_ANNUAL_REPORT
                            SET UPDATE_DATE         = sysdate 
                            , UPDATE_BY             = :UPDATE_BY
                            , DOCNO                 = :DOCNO
                            , DOC_DATE              = :DOC_DATE
                            , SEND_TO_VENDOR_BY     = :SEND_TO_VENDOR_BY
                            , SEND_TO_VENDOR_DATE   = sysdate
                            , STATUS_ID = 3
                            , YEAR   =:YEAR , FIRST_VIEW = NULL
                            , VENDOR_ID=:VENDOR_ID ,FILE_NAME =:FILE_NAME
                            WHERE ID                = :ID ";

                            using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                            {
                                com1.Transaction = tran;
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                                com1.Parameters.Add(":DOCNO", OracleType.NVarChar).Value = txtDOCNO.Text.Trim();
                                com1.Parameters.Add(":DOC_DATE", OracleType.NVarChar).Value = txtDOCDATE.Text.Trim();
                                com1.Parameters.Add(":SEND_TO_VENDOR_BY", OracleType.Number).Value = user_profile.USER_ID;
                                com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                com1.Parameters.Add(":YEAR", OracleType.NVarChar).Value = SearchCriteria.YEAR;
                                com1.Parameters.Add(":VENDOR_ID", OracleType.NVarChar).Value = SearchCriteria.VENDORID;
                                com1.Parameters.Add(":FILE_NAME", OracleType.NVarChar).Value = FileNameNotice;
                                com1.ExecuteNonQuery();
                            }

                            tran.Commit();
                        }

                    }
                    else
                    {

                        if (btnPreview.Enabled != false)
                        {
                            int? refID = new FinalGradeBLL().GetDocID(ref _err);
                            string sqlMain = @" INSERT INTO T_ANNUAL_REPORT 
                            ( ID ,UPDATE_DATE, UPDATE_BY , DOCNO, DOC_DATE,
                                SEND_TO_VENDOR_BY, SEND_TO_VENDOR_DATE, STATUS_ID,CREATE_DATE,CREATE_BY,
                                YEAR,VENDOR_ID,FILE_NAME)
                            VALUES
                            (:NID, sysdate, :UPDATE_BY,:DOCNO ,:DOC_DATE,
                                :SEND_TO_VENDOR_BY,sysdate ,3,sysdate, :UPDATE_BY,
                                :NYEAR,:VENDOR_ID,:NFILE) ";

                            using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                            {
                                com1.Transaction = tran;
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":NID", OracleType.Number).Value = refID;
                                com1.Parameters.Add(":UPDATE_BY", OracleType.NVarChar).Value = user_profile.USER_ID;
                                com1.Parameters.Add(":DOCNO", OracleType.NVarChar).Value = txtDOCNO.Text.Trim();
                                com1.Parameters.Add(":DOC_DATE", OracleType.NVarChar).Value = txtDOCDATE.Text.Trim();
                                com1.Parameters.Add(":SEND_TO_VENDOR_BY", OracleType.NVarChar).Value = user_profile.USER_ID;
                                com1.Parameters.Add(":NYEAR", OracleType.NVarChar).Value = SearchCriteria.YEAR;
                                com1.Parameters.Add(":VENDOR_ID", OracleType.NVarChar).Value = SearchCriteria.VENDORID;
                                com1.Parameters.Add(":NFILE", OracleType.NVarChar).Value = FileNameNotice;
                                com1.ExecuteNonQuery();
                            }

                            tran.Commit();
                            QDocID = refID.ToString();
                            Session["AARN_QDocID"] = QDocID;

                        }

                    }

                    return true;

                }
                catch (Exception ex)
                {
                    alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                    return false;
                }

            }

        }
    }

    private bool SaveFile()
    {
        try
        {
            FileNameNotice = "annual_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            File.WriteAllText(Server.MapPath(string.Format("~/UploadFile/annual_report/notice/{0}.txt", FileNameNotice)), divEmail.InnerHtml);
            return true;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
            return false;
        }
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        //string
        try
        {

            if (!SaveFile()) return;
            string Mess = string.Empty;
            Mess = "แจ้งผู้ขนส่งสำเร็จ";
            if (SendEmail())
            {
                Mess += "<br/>ส่งอีเมลล์สำเร็จ";
                SaveNotice();

                btnCancel.Enabled = true;
                btnSend.Enabled = false;
                btnPreview.Enabled = false;
                btnBfSend.Enabled = false;
                txtDOCDATE.Enabled = false;
                txtDOCNO.Enabled = false;
            }
            else
            {
                Mess += "<br/>ไม่สามารถส่งอีเมลล์ได้";
            }
            alertSuccess(Mess, "AnnualReportNotice.aspx");

            Session["AARN_SABBREVIATION"] = lblVendor.Text;
            Session["AARN_YEAR"] = SearchCriteria.YEAR;
            Session["AARN_SVENDORID"] = SearchCriteria.VENDORID;
        }
        catch (Exception ex)
        {
            alertFail("แจ้งผู้ขนส่งไม่สำเร็จเนื่องจาก : " + RemoveSpecialCharacters(ex.Message));
        }
    }

    #region SendEmail
    private bool SendEmail()
    {
        bool isREs = false;
        try
        {

            DataTable dtComplainEmail = new DataTable();
            string Subject = "แจ้งผลการประเมินการทำงานขนส่ง ประจำปี {YEAR}";
            Subject = Subject.Replace("{YEAR}", YearAdd543(lblYear.Text.Trim()));

            string SVENDORTITLE = "บริษัท", SVENDORNAME = string.Empty, HEDERTITLE = string.Empty, TITLE = "บริษัท";
            if (lblVendor.Text.Trim().Contains("บริษัท"))
            {
                SVENDORNAME = lblVendor.Text.Trim().Replace("บริษัท", "");
                HEDERTITLE = "กรรมการผู้จัดการ";
            }
            else
            {
                SVENDORNAME = lblVendor.Text.Trim().Replace("ห้างหุ้นส่วนจำกัด", "");
                SVENDORTITLE = "ห้างหุ้นส่วนจำกัด";
                TITLE = "ห้าง";
                HEDERTITLE = "หุ้นส่วนผู้จัดการ";
            }

            string ContentEmail = File.ReadAllText(Server.MapPath("~/FileFormat/Admin/ANNUAL_REPORT_Email.txt"));
            ContentEmail = ContentEmail.Replace("{YEAR}", YearAdd543(lblYear.Text.Trim()));
            ContentEmail = ContentEmail.Replace("{SVENDORTITLE}", SVENDORTITLE);
            ContentEmail = ContentEmail.Replace("{SVENDORNAME}", SVENDORNAME);

            #region VendorEmployeeAdd
            string EmailList = ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(user_profile.USER_ID), SearchCriteria.VENDORID, true, false);
            //EmailList += ",zsuntipab.k@pttict.com,zchanaphon.p@pttict.com";

            string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "final-grade.aspx";
            ContentEmail = ContentEmail.Replace("{LINK}", ConfigValue.GetClickHere(Link));
            MailService.SendMail(EmailList, Subject, ContentEmail, "", "VendorEmployeeAdd", ColumnEmailName);
            #endregion

            isREs = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return isREs;
    }

    #endregion


    #endregion " Send E-mail "
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("final-grade.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int? refID = 0;
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            using (OracleTransaction tran = con.BeginTransaction())
            {
                try
                {
                    string _err = string.Empty;
                    if (int.Parse(QDocID) > 0)
                    {

                        if (btnPreview.Enabled != false)
                        {
                            string sqlMain = @" UPDATE T_ANNUAL_REPORT
                            SET UPDATE_DATE         = sysdate 
                            , UPDATE_BY           = :UPDATE_BY
                            WHERE ID                = :ID ";

                            using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                            {
                                com1.Transaction = tran;
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                                com1.ExecuteNonQuery();
                            }
                        }

                        #region  " Edit "

                        #region " File "

                        var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);
                        if (delFile != null)
                        {
                            string _strCondition = string.Join("','", DTFile.Select(r => r.ID));
                            string _whereCondition = string.Empty;
                            if (!string.IsNullOrEmpty(_strCondition))
                            {
                                _whereCondition = string.Format(" ID NOT IN ('{0}') and ", _strCondition);
                            }
                            using (OracleCommand comDel1 = new OracleCommand(string.Format(@"UPDATE F_UPLOAD SET ISACTIVE = 0 WHERE {0} ref_int = {1} and ref_str = '{2}'", _whereCondition, QDocID, PageFileUpload), con))
                            {
                                comDel1.Transaction = tran;
                                comDel1.Parameters.Clear();
                                comDel1.ExecuteNonQuery();
                            }
                        }

                        foreach (var item in DTFile)
                        {
                            if (item.ID == 0)
                            {
                                using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                    com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                    com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                    com1.Parameters.Add(":REF_INT", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                    com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                    com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }

                        #endregion " File "

                        #endregion " Edit "

                        tran.Commit();
                    }
                    else
                    {

                        refID = new FinalGradeBLL().GetDocID(ref _err);
                        string sqlMain = @" INSERT INTO T_ANNUAL_REPORT 
                            ( ID ,UPDATE_DATE, UPDATE_BY ,CREATE_DATE,CREATE_BY,YEAR,VENDOR_ID)
                            VALUES
                            (:ID, sysdate, :UPDATE_BY ,sysdate ,:UPDATE_BY,:YEAR,:VENDOR_ID ) ";

                        using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                        {
                            com1.Transaction = tran;
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":ID", OracleType.Number).Value = refID;
                            com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                            com1.Parameters.Add(":YEAR", OracleType.NVarChar).Value = SearchCriteria.YEAR;
                            com1.Parameters.Add(":VENDOR_ID", OracleType.NVarChar).Value = SearchCriteria.VENDORID;
                            com1.ExecuteNonQuery();
                        }

                        #region " File "

                        var delFile = DTFile.Where(x => x.ID != 0).Select(r => r.ID);

                        foreach (var item in DTFile)
                        {
                            if (item.ID == 0)
                            {
                                using (OracleCommand com1 = new OracleCommand(@"INSERT INTO F_UPLOAD (FILENAME_SYSTEM, FILENAME_USER, UPLOAD_ID, REF_INT, REF_STR, ISACTIVE, CREATE_BY, FULLPATH) VALUES (:FILENAME_SYSTEM, :FILENAME_USER, :UPLOAD_ID, :REF_INT, :REF_STR, :ISACTIVE, :CREATE_BY, :FULLPATH)", con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":FILENAME_SYSTEM", OracleType.VarChar).Value = item.FILENAME_SYSTEM;
                                    com1.Parameters.Add(":FILENAME_USER", OracleType.VarChar).Value = item.FILENAME_USER;
                                    com1.Parameters.Add(":UPLOAD_ID", OracleType.VarChar).Value = item.UPLOAD_ID;
                                    com1.Parameters.Add(":REF_INT", OracleType.Number).Value = refID;
                                    com1.Parameters.Add(":REF_STR", OracleType.VarChar).Value = PageFileUpload;
                                    com1.Parameters.Add(":ISACTIVE", OracleType.Number).Value = "1";
                                    com1.Parameters.Add(":CREATE_BY", OracleType.VarChar).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":FULLPATH", OracleType.VarChar).Value = item.FULLPATH;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }

                        #endregion " File "

                        tran.Commit();
                    }

                    alertSuccess(" บันทึกเอกสารแนบเรียบร้อย ", "AnnualReportNotice.aspx");

                    Session["AARN_SABBREVIATION"] = lblVendor.Text;
                    Session["AARN_YEAR"] = SearchCriteria.YEAR;
                    Session["AARN_SVENDORID"] = SearchCriteria.VENDORID;
                    if (int.Parse(QDocID) == 0) QDocID = refID.ToString();
                    Session["AARN_QDocID"] = QDocID;

                }
                catch (Exception ex)
                {
                    alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                }
            }

        }
    }

    #region " EXPORT "
    protected void btnExport_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.ContentType = "application/msword";
        string strFileName = "AnnualFileForm" + ".doc";
        HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);

        StringBuilder strHTMLContent = new StringBuilder();
        strHTMLContent.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head></head><body>");
        strHTMLContent.Append(ContentForm);
        strHTMLContent.Append("</body></html>");

        HttpContext.Current.Response.Write(strHTMLContent);
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Flush();

        //var sb = new StringBuilder();
        //sb = sb.Append(ContentForm);
        //divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
        //string Path = this.CheckPath();
        //string FileName = "AnnualFileForm" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".doc";

        //string wordHeader = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" ";
        //wordHeader += "xmlns:w=\"urn:schemas-microsoft-com:office:word\" ";
        //wordHeader += "xmlns=\"http://www.w3.org/TR/REC-html40\"> ";
        //wordHeader += "<head><title>Document Title</title>";
        //wordHeader += "<!--[if gte mso 9]><xml><w:WordDocument><w:View>Print</w:View><w:Zoom>100</w:Zoom>";
        //wordHeader += "<w:DoNotOptimizeForBrowser/></w:WordDocument></xml><![endif]-->";
        //wordHeader += "<style> @page Section1 {";
        //wordHeader += "";
        //wordHeader += "margin:auto ; mso-header-margin:auto; ";
        //wordHeader += "mso-footer-margin:-5; mso-paper-source:0;} ";
        //wordHeader += "div.Section1 {page:Section1;} p.MsoFooter, li.MsoFooter, ";
        //wordHeader += "div.MsoFooter{margin:0in; margin-bottom:.0001pt; ";
        //wordHeader += "mso-pagination:widow-orphan; tab-stops:center 3.0in right 6.0in; ";
        //wordHeader += "} ";
        //wordHeader += "p.MsoHeader, li.MsoHeader, div.MsoHeader {margin:0in; ";
        //wordHeader += "margin-bottom:.0001pt; mso-pagination:widow-orphan; tab-stops:center ";
        //wordHeader += "3.0in right 6.0in;}--></style></head> ";

        //sb.Insert(0, wordHeader + "<body><div class=Section1>");
        //sb.AppendLine("</div></body></html>");
        //this.DownloadFileWord(Path, FileName, sb.ToString());
    }

    private void DownloadFileWord(string Path, string FileName, string doc)
    {

        //Response.Clear();
        //Response.ContentType = "application/msword";
        //Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.ClearContent();
        Response.Charset = "";
        Response.AppendHeader("content-disposition", "attachment;  filename=" + FileName);
        Response.ContentType = "application/vnd.ms-xpsdocument";
        Response.Write(doc);
        Response.End();
    }

    protected void btnExportPdf_Click(object sender, EventArgs e)
    {


        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            #region " Save Log For Vendor "
            try
            {
                string file_name = "view_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                File.WriteAllText(Server.MapPath(string.Format("~/UploadFile/annual_report/download/{0}.txt", file_name)), divEmail.InnerHtml);
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    using (OracleTransaction tran = con.BeginTransaction())
                    {
                        try
                        {
                            string _err = string.Empty;
                            if (int.Parse(QDocID) > 0)
                            {
                                string sqlMain = @" INSERT INTO T_ANNUAL_REPORT_HISTORY (UPDATE_DATE,UPDATE_BY,CREATE_DATE,CREATE_BY,FILENAME,REPORT_ID,DOCNO)
                             values (sysdate ,:UPDATE_BY,sysdate,:UPDATE_BY,:FILE_NAME, :ID,:DOCNO )";

                                using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":FILE_NAME", OracleType.NVarChar).Value = file_name;
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":DOCNO", OracleType.NVarChar).Value = ReportData.Rows[0]["DOCNO"].ToString().Trim();
                                    com1.ExecuteNonQuery();
                                }

                                string sqlUpdate = @" UPDATE T_ANNUAL_REPORT SET FIRST_VIEW =:FIRST_VIEW WHERE ID=:ID AND FIRST_VIEW IS NULL";

                                using (OracleCommand com1 = new OracleCommand(sqlUpdate, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":FIRST_VIEW", OracleType.NVarChar).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                    com1.ExecuteNonQuery();
                                }
                            }
                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            #endregion " Save Log For Vendor "

            //ContentForm = File.ReadAllText(Server.MapPath(String.Format("~/UploadFile/annual_report/notice/{0}.txt",FileNameNotice)));
            //divEmail.InnerHtml = ContentForm;
            //"~/UploadFile/annual_report/notice/{0}.txt" //FileNameNotice
            tblFormNotice.Visible = true;
            divEmail.Visible = true;
            grvMain.Visible = false;
        }
        else
        {

            int MarginSize = 45;
            HtmlToPdf converter = new HtmlToPdf();
            converter.Options.MarginTop = MarginSize;
            converter.Options.MarginLeft = MarginSize;
            converter.Options.MarginBottom = MarginSize;
            converter.Options.MarginRight = MarginSize;
            var sb = new StringBuilder();
            divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
            ///exgap1ch.jpeg
            string s = sb.ToString();
            // convert the url to pdf 
            PdfDocument doc = converter.ConvertHtmlString(ContentForm);

            string Path = this.CheckPath();
            string FileName = "AnnualFileForm.pdf";

            doc.Save(Response, false, FileName);
            //this.DownloadFile(Path, FileName);
            doc.Close();
        }

        //Byte[] res = null;
        //using (MemoryStream ms = new MemoryStream())
        //{
        //    //ContentForm = ContentForm.Replace("font-family: Tahoma", "font-family: AngsanaUPC");

        //    var _strExport = string.Format(@"<table ><tr align='center'><td align='center'><div runat='server' style='width: 768px'>{0}</div></td></tr></table>", ContentForm);

        //    var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(ContentForm, PdfSharp.PageSize.A4);
        //    pdf.Save(ms);
        //    res = ms.ToArray();
        //}

        //Response.Clear();
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("Content-Disposition", "attachment;filename=\"AnnualFileForm.pdf\"");

        //Response.BinaryWrite(res);
        //Response.Flush();
        //Response.End();




        /* Response.AddHeader("content-disposition",
 "attachment;filename=GridViewExport.pdf");
         Response.Cache.SetCacheability(HttpCacheability.NoCache);
         StringWriter sw = new StringWriter();
         HtmlTextWriter hw = new HtmlTextWriter(sw);
         divEmail.RenderControl(hw);
         StringReader sr = new StringReader(sw.ToString());
         Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
         HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
         PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
         pdfDoc.Open();
         htmlparser.Parse(sr);
         pdfDoc.Close();
         Response.Write(pdfDoc);
         Response.End(); */

        //// instantiate the html to pdf converter
        //HtmlToPdf converter = new HtmlToPdf();

        //// convert the url to pdf
        //PdfDocument doc = converter.ConvertUrl("LoadPDF.aspx");

        //// save pdf document
        //doc.Save();

        //// close pdf document
        //doc.Close();

        /*
        Byte[] res = null;
        using (MemoryStream ms = new MemoryStream())
        {
            var _strExport = string.Format(@"<table ><tr align='center'><td align='center'><div runat='server' style='width: 768px'>{0}</div></td></tr></table>", ContentForm);
            var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(_strExport, PdfSharp.PageSize.A4);
            pdf.Save(ms);
            res = ms.ToArray();
        }

        Response.Clear();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Disposition", "attachment;filename=\"AnnualFileForm.pdf\"");

        Response.BinaryWrite(res);
        Response.Flush();
        Response.End();*/
        //divBody.InnerHtml = ContentForm;
        //int MarginSize = 45;
        //HtmlToPdf converter = new HtmlToPdf();
        //converter.Options.MarginTop = MarginSize;
        //converter.Options.MarginLeft = MarginSize;
        //converter.Options.MarginBottom = MarginSize;
        //converter.Options.MarginRight = MarginSize;
        //var sb = new StringBuilder();
        //divBody.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
        /////exgap1ch.jpeg
        //string s = sb.ToString();
        //// convert the url to pdf 
        //PdfDocument doc = converter.ConvertHtmlString(sb.ToString());

        //string Path = this.CheckPath();
        //string FileName = "Annual_Repoert_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";

        //doc.Save(Response, false, FileName);
        ////this.DownloadFile(Path, FileName);
        //doc.Close();
    }

    #endregion " EXPORT "
    protected void btnBfSend_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtDOCDATE.Text))
            alertFail("กรุณาระบุเลขที่เอกสาร");

        else if (string.IsNullOrEmpty(txtDOCNO.Text))
            alertFail("กรุณาระบุวันที่เอกสาร ");
        else
        {
            btnPreview_Click(null, null);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "AlertSave()", true);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();

            using (OracleTransaction tran = con.BeginTransaction())
            {
                try
                {
                    string _err = string.Empty;
                    if (int.Parse(QDocID) > 0)
                    {
                        string sqlMain = @" UPDATE T_ANNUAL_REPORT
                            SET UPDATE_DATE         = sysdate 
                            , UPDATE_BY             = :UPDATE_BY
                            , STATUS_ID = 4
                            WHERE ID                = :ID ";

                        using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                        {
                            com1.Transaction = tran;
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                            com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                            com1.ExecuteNonQuery();
                        }

                        string sqlCancel = @" UPDATE T_ANNUAL_REPORT_HISTORY
                            SET UPDATE_DATE         = sysdate 
                            , UPDATE_BY             = :UPDATE_BY
                            , IS_ACTIVE = '0'
                            WHERE REPORT_ID                = :ID AND IS_ACTIVE='1'";

                        using (OracleCommand com1 = new OracleCommand(sqlCancel, con))
                        {
                            com1.Transaction = tran;
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                            com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                            com1.ExecuteNonQuery();
                        }

                        tran.Commit();
                        alertSuccess(" ยกเลิกเอกสารเรียบร้อย ", "AnnualReportNotice.aspx");

                        Session["AARN_SABBREVIATION"] = lblVendor.Text;
                        Session["AARN_YEAR"] = SearchCriteria.YEAR;
                        Session["AARN_SVENDORID"] = SearchCriteria.VENDORID;
                        Session["AARN_QDocID"] = "0";
                        LoadMain(true);
                    }
                }
                catch (Exception ex)
                {
                    alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                }

            }

        }
    }
    protected void grvMain_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var lblDocDesc = e.Row.Cells[1].FindControl("lblDocDesc") as Label;
            if (lblDocDesc != null)
            {
                lblDocDesc.Text = "ผลประเมินการปปฏิบัติงานในรอบ 12 เดือน ประจำปี " + DataBinder.Eval(e.Row.DataItem, "YEAR").ToString();
            }
        }
    }
    protected void grvMain_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Download")
        {
            btnExportPdf_Click(null, null);
        }
    }
    protected void grvMain_DataBound(object sender, EventArgs e)
    {

    }
    protected void btnVendorView_Click(object sender, EventArgs e)
    {
        if (user_profile.CGROUP != GROUP_PERMISSION.UNIT)
        {
            #region " Save Log For Vendor "
            try
            {
                string file_name = "view_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                File.WriteAllText(Server.MapPath(string.Format("~/UploadFile/annual_report/download/{0}.txt", file_name)), divEmail.InnerHtml);
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    using (OracleTransaction tran = con.BeginTransaction())
                    {
                        try
                        {
                            string _err = string.Empty;
                            if (int.Parse(QDocID) > 0)
                            {
                                string sqlMain = @" INSERT INTO T_ANNUAL_REPORT_HISTORY (UPDATE_DATE,UPDATE_BY,CREATE_DATE,CREATE_BY,FILENAME,REPORT_ID,DOCNO)
                             values (sysdate ,:UPDATE_BY,sysdate,:UPDATE_BY,:FILE_NAME, :ID,:DOCNO )";

                                using (OracleCommand com1 = new OracleCommand(sqlMain, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":UPDATE_BY", OracleType.Number).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":FILE_NAME", OracleType.NVarChar).Value = file_name;
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                    com1.Parameters.Add(":DOCNO", OracleType.NVarChar).Value = ReportData.Rows[0]["DOCNO"].ToString().Trim();
                                    com1.ExecuteNonQuery();
                                }

                                string sqlUpdate = @" UPDATE T_ANNUAL_REPORT SET FIRST_VIEW =:FIRST_VIEW WHERE ID=:ID AND FIRST_VIEW IS NULL";

                                using (OracleCommand com1 = new OracleCommand(sqlUpdate, con))
                                {
                                    com1.Transaction = tran;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":FIRST_VIEW", OracleType.NVarChar).Value = user_profile.USER_ID;
                                    com1.Parameters.Add(":ID", OracleType.Number).Value = QDocID;
                                    com1.ExecuteNonQuery();
                                }
                            }
                            tran.Commit();
                        }
                        catch (Exception ex)
                        {
                            alertFail(ex.Message + " <br/>" + System.Reflection.MethodBase.GetCurrentMethod().Name);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            #endregion " Save Log For Vendor "

            LoadMain(true);
            divEmail.Visible = true;
        }
    }
}