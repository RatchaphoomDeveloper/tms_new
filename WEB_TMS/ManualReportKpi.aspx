﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="ManualReportKpi.aspx.cs" Inherits="ManualReportKpi" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="JQuery/jquery.validate.min.js" type="text/javascript"></script>
        <script src="JQuery/messages_th.min.js" type="text/javascript"></script>
    <style type="text/css">
        th {
            text-align: center;
        }

        .error {
            color: red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery.validator.addMethod("CheckChar", function (value, element) {
                //^[0-9]*\.*[0-9]*\-*[0-9]*\.*[0-9]*
                if (/(?:\d*\.*)?\d+/.test(value)) {
                    return true;  // FAIL validation when REGEX matches
                } else {
                    return false;   // PASS validation otherwise
                };
            }, "เฉพาะตัวเลข จุดทศนิยม");            
            $("#<%=cmdSave.ClientID%>").click(function () {
                $('form').validate();
                //Add a custom class to your name mangled input and add rules like this
                $('.required').rules('add', {
                    required: true
                });
                $('.number').rules('add',
                    {
                        number: true
                    });
                $('.CheckChar').rules('add',
                    {
                        CheckChar: true
                    });
            });
            $("#<%=cmdTemp.ClientID%>,#cph_Main_mpConfirmBack_btnSave").click(function () {
                $("form").validate().cancelSubmit = true;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ผลประเมิน KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        ประจำปี :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblYear" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        เดือน :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblMonth" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        ชื่อแบบประเมิน :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblNameKpi" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        บริษัทผู้ขนส่ง :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblVendor" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-2 text-right">
                        เลขที่สัญญา :
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblContract" runat="server" />
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>

    <div class="">
        <%-- หน้าจอแสดงผู้ขนส่ง --%>
        <asp:GridView ID="gvd_manual" runat="server" Width="90%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnPreRender="gvd_manual_PreRender"
            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>

            <Columns>
                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-Width="2%">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataItemIndex + 1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EVA_NAME" HeaderText="ชื่อหัวข้อประเมิน" ItemStyle-Width="15%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="FREQUENCY" HeaderText="ความถี่">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="CALCULATE" HeaderText="วิธีคำนวณผลรวม" ItemStyle-Width="10%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="RANGE1" HeaderText="1" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="RANGE2" HeaderText="2" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="RANGE3" HeaderText="3" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="RANGE4" HeaderText="4" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="RANGE5" HeaderText="5" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                
                <asp:BoundField DataField="WEIGHT" HeaderText="Weight <br/> ผลรวมไม่เกิน 100" HtmlEncode="false" ItemStyle-Width="10%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="sumtotal" HeaderText="ผลรวมก่อนหน้า" ItemStyle-Width="5%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="คะแนนผลการประเมิน" ItemStyle-Width="15%">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:TextBox ID="txtscore" runat="server" CssClass="form-control required CheckChar" AutoPostBack="true" OnTextChanged="txtscore_TextChanged" MaxLength="5"></asp:TextBox>
                   <asp:HiddenField ID="kpi_eva_edtail_id" runat="server" Value='<%# Eval("KPI_EVA_EDTAIL_ID") %>' />
                        <asp:HiddenField ID="kpi_eva_head_id" runat="server" Value='<%# Eval("KPI_EVA_HEAD_ID") %>' />
                         </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="SCORE" HeaderText="คะแนนที่ได้" ItemStyle-Width="12%">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>                
            </Columns>

        </asp:GridView>
    </div>
    <p>&nbsp;</p>
    <div class="col-md-12">
        <div class="col-md-offset-2 col-md-4 text-right row">            
            <asp:Button runat="server" ID="cmdSave" Width="150px" ClientIDMode="Static" CssClass="btn btn-primary" Text="บันทึกผล" OnClick="cmdSave_Click" />
            <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-danger" style="width: 120px;" />
        </div>
        <div class="row">
            <div class="col-md-6">
                <asp:Button runat="server" ID="cmdTemp" Width="150px" ClientIDMode="Static" CssClass="btn btn-info" Text="บันทึกร่าง" OnClick="cmdSave_Click" />
            </div>
        </div>
    </div>
    <uc1:modelpopup runat="server" id="mpConfirmBack" idmodel="ModalConfirmBack" isbuttontypesubmit="true" isonclick="true" isshowclose="true" onclickok="mpConfirmBack_ClickOK" texttitle="ยืนยันการย้อนกลับ" textdetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />
</asp:Content>
