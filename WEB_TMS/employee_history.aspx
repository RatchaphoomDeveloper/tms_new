﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="employee_history.aspx.cs" Inherits="employee_history" Culture="en-US" UICulture="en-US" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<dx:ASPxRoundPanel ID="rpnInformation" ClientInstanceName="rpn" runat="server" Width="1000px"
                    HeaderText="ประวัติการเปลี่ยนแปลงข้อมูลพนักงาน">
                    <PanelCollection>
                        <dx:PanelContent runat="server" ID="arp">
                            <table id="Table1" runat="server" width="100%">
                                <tr>
                                    <td width="25%"  align="center">
                                        <dx:ASPxImage runat="server" ID="imgEmp" Width="175px" Height="200px" ClientInstanceName="imgEmp" Cursor="pointer" >
                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('hisPicEMP'); }"/>
                                        </dx:ASPxImage>
                                    </td>
                                    <td width="75%">
                                        <asp:Panel Enabled="false" runat="server">
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label">
                                                หมายเลขบัตรประชาชน :
                                            </label>
                                            <div class="col-md-6">
                                                <asp:TextBox runat="server" ID="txtEmpID" CssClass="form-control" Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label">
                                                ตำแหน่ง :
                                            </label>
                                            <div class="col-md-6  ">
                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlPosition">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                             <label class="col-md-4 control-label">
                                                คำนำหน้าชื่อ :
                                            </label>
                                            <div class="col-md-6 ">
                                                <asp:DropDownList CssClass="form-control" runat="server" ID="ddlTitle">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                             <label class="col-md-4 control-label">
                                                ชื่อ :
                                            </label>
                                            <div class="col-md-6 ">
                                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" placeholder="กรุณาระบุชื่อ" />

                                            </div>
                                        </div>
                                        <div class="row form-group">
                                             <label class="col-md-4 control-label">
                                                นามสกุล :
                                            </label>
                                            <div class="col-md-6">
                                                <asp:TextBox runat="server" ID="txtSurName" CssClass="form-control" placeholder="กรุณาระบุนามสกุล" />

                                            </div>
                                        </div>
                                            </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="false"  width="100%" SettingsPager-Mode="ShowAllRecords">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="UPDATE_DATETIME" Caption="วันที่อัพเดทข้อมูล" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" PropertiesTextEdit-DisplayFormatString="dd/MM/yyyy HH:mm">
                                                  <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="FULLNAME" Caption="ผู้อัพเดทข้อมูล"  HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Left">
                                                  <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DETAIL" Caption="ข้อมูล" HeaderStyle-HorizontalAlign="Center">
                                                <PropertiesTextEdit EncodeHtml="false"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
