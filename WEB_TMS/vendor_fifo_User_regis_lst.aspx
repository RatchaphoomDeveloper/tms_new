﻿<%@ Page Title="" MasterPageFile="~/Mp1.Master" Language="C#" AutoEventWireup="true"
    CodeFile="vendor_fifo_User_regis_lst.aspx.cs" Inherits="vendor_fifo_User_regis_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents  EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="90%">
                            <dx:ASPxButton ID="btnAdd" runat="server" Text="ลงคิวเข้ารับงาน" AutoPostBack="false">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('AddPage');}" />
                            </dx:ASPxButton>
                            <dx:ASPxTextBox ID="txtVendorID" runat="server" Width="50px" ClientVisible="false">
                            </dx:ASPxTextBox>
                            <dx:ASPxTextBox ID="txtUserID" runat="server" ClientVisible="False" 
                                Width="50px">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="NID"
                                SkinID="_gvw" DataSourceID="sds">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ลำดับ" VisibleIndex="0" FieldName="NNO"
                                        Width="3%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่ลงคิว" VisibleIndex="1" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DDATE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" HeaderStyle-HorizontalAlign="Center">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="หัว" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                            VisibleIndex="2" FieldName="SHEADREGISTERNO">
                                            <headerstyle horizontalalign="Center"></headerstyle>
                                            <cellstyle horizontalalign="Center">
                                        </cellstyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ท้าย" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                            VisibleIndex="3" FieldName="STRAILERREGISTERNO">
                                            <headerstyle horizontalalign="Center"></headerstyle>
                                            <cellstyle horizontalalign="Center">
                                        </cellstyle>
                                        </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewBandColumn Caption="ข้อมูลรถ" HeaderStyle-HorizontalAlign="Center">
                                      <Columns>
                                        <dx:GridViewDataTextColumn Caption="ความจุ" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center"
                                            Width="5%" FieldName="NCAPACITY">
                                            <headerstyle horizontalalign="Center"></headerstyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="ช่อง" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                            Width="5%" FieldName="NCOMPARTNO">
                                            <headerstyle horizontalalign="Center"></headerstyle>
                                        </dx:GridViewDataTextColumn>
                                          </Columns>
                                           <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SVENDORNAME" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn Caption="ชื่อพนักงานขับรถ" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="SEMPLOYEENAME" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn Caption="โทรศัพท์" VisibleIndex="8" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="STEL" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" CancelSelectOnNullParameter="false" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
