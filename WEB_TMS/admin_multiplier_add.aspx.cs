﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;

public partial class admin_multiplier_add : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblUser.Text = Session["UserName"] + "";

        if (!IsPostBack)
        {           
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
           
            if (Session["oNID"] != null)
            {
                listData();
            }
            this.AssignAuthen();
        }
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSubmit.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    void ClearControl()
    {

        Session["oNID"] = null;
        txtStartPoint.Text = "";
        txtEndPoint.Text = "";
        txtMultiple.Text = "";
        lblUser.Text = "";


    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');

        switch (paras[0])
        {

            case "Save":
                if (CanWrite)
                {
                    int num = 0;
                    int EndPoint = 0;
                    EndPoint = int.TryParse(txtEndPoint.Text, out num) ? num : 0;
                    if (Session["oNID"] == null)
                    {
                        string GenID;
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"INSERT INTO LSTMULTIPLEIMPACT ( NID, NSTART, NEND, NSCORE, SCREATE, DCREATE, SUPDATE, DUPDATE) VALUES ( :NID ,:NSTART ,:NEND ,:NSCORE ,:SCREATE ,SYSDATE ,:SUPDATE ,SYSDATE )";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                GenID = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM LSTMULTIPLEIMPACT ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = GenID;
                                com.Parameters.Add(":NSTART", OracleType.Number).Value = txtStartPoint.Text;
                                com.Parameters.Add(":NEND", OracleType.Number).Value = EndPoint;
                                com.Parameters.Add(":NSCORE", OracleType.Number).Value = txtMultiple.Text;
                                com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("40", "I", "บันทึกข้อมูลหน้า ข้อมูลผลคูณความรุนแรง", GenID + "");
                    }
                    else
                    {
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string strsql = @"UPDATE LSTMULTIPLEIMPACT SET NSTART  = :NSTART,NEND   = :NEND,NSCORE  = :NSCORE,SUPDATE = :SUPDATE,DUPDATE = SYSDATE WHERE  NID   = :NID";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NID", OracleType.Number).Value = Session["oNID"] + "";
                                com.Parameters.Add(":NSTART", OracleType.Number).Value = txtStartPoint.Text;
                                com.Parameters.Add(":NEND", OracleType.Number).Value = EndPoint;
                                com.Parameters.Add(":NSCORE", OracleType.Number).Value = txtMultiple.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.ExecuteNonQuery();
                            }
                        }

                        LogUser("40", "E", "แก้ไขข้อมูลหน้า ข้อมูลผลคูณความรุนแรง", Session["oNID"] + "");

                    }

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='admin_multiplier_lst.aspx';});");

                    ClearControl();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
                }
                break;

        }

    }


    void listData()
    {

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(sql, "SELECT NSTART, NEND, NSCORE FROM LSTMULTIPLEIMPACT WHERE NID = " + Session["oNID"]);
        if (dt.Rows.Count > 0)
        {
            txtStartPoint.Text = dt.Rows[0]["NSTART"] + "";
            txtEndPoint.Text = dt.Rows[0]["NEND"] + "";
            txtMultiple.Text = dt.Rows[0]["NSCORE"] + "";
            lblUser.Text = Session["UserName"] + "";
        }

    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

}