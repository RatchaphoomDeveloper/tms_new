﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_Complain_lst_Jang.aspx.cs" Inherits="admin_Complain_lst_Jang" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" language="javascript">
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo; if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined;}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td width="34%">
                            &nbsp;
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ทะเบียนรถ ,ชื่อพขร  ,ชื่อผู้ประกอบการ">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                          <dx:ASPxComboBox ID="cboStatus" runat="server" ClientInstanceName="cboStatus"
                                Width="100px" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="- ทุกสถานะ -" />
                                    <dx:ListEditItem Text="ดำเนินการ" Value="1" />
                                    <dx:ListEditItem Text="อุทธรณ์" Value="2" />
                                    <dx:ListEditItem Text="ปิดเรื่อง" Value="3" />
                                </Items>
                            </dx:ASPxComboBox>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" >
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                       <td align="right" colspan="8">
                         จำนวนรายการเรื่องร้องเรียน&nbsp;<dx:ASPxLabel ID="lblCarCount" ClientInstanceName="lblCarCount" runat="server" Text="0" Font-Bold="true" ForeColor="Red">
                           </dx:ASPxLabel> &nbsp;รายการ

                       </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%" >
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="2%">
                                        <HeaderTemplate>
                                            <dx:ASPxCheckBox ID="ASPxCheckBox1" runat="server" ToolTip="Select/Unselect all rows on the page"
                                                ClientSideEvents-CheckedChanged="function(s, e) { gvw.SelectAllRowsOnPage(s.GetChecked()); }">
                                            </dx:ASPxCheckBox>
                                        </HeaderTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="1"
                                        Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SSERVICEID" ShowInCustomizationForm="True"
                                        Visible="False" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันรับเรื่อง" FieldName="DDATECOMPLAIN" VisibleIndex="3" Width="6%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ประกอบการ" FieldName="SVENDORNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="4" Width="15%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewBandColumn Caption="ทะเบียนรถ" ShowInCustomizationForm="True" VisibleIndex="5">
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="หัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="10" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="ท้าย" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                VisibleIndex="11" Width="8%">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewBandColumn>
                                    <dx:GridViewDataTextColumn Caption="เรื่องที่ร้องเรียน" FieldName="SHEADCOMLIAINNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หัวข้อการหักคะแนน" FieldName="STOPICNAME" ShowInCustomizationForm="True"
                                        VisibleIndex="5" Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Caption="ตัดคะแนน" FieldName="CUTSCORE"
                                     ShowInCustomizationForm="True" VisibleIndex="6" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="ระยะเวลา<br>ดำเนินการ" FieldName="DCLOSE"
                                     ShowInCustomizationForm="True" VisibleIndex="7" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                    <dx:GridViewDataTextColumn Caption="สถานะ" FieldName="STATUS" ShowInCustomizationForm="True"
                                        VisibleIndex="8" Width="5%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="13" Width="5%">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="False" CausesValidation="False"
                                                Text="แก้ไข">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" 
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" 
                                DeleteCommand="DELETE FROM TCOMPLAIN WHERE SSERVICEID = :SSERVICEID">
                                 <DeleteParameters>
                                   <asp:SessionParameter Name="SSERVICEID" SessionField="delSSERVICEID" Type="String" />
                                </DeleteParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="btnDel" runat="server" SkinID="_delete">
                                <ClientSideEvents Click="function (s, e) { checkBeforeDeleteRowxPopupImg(gvw, function (s, e) { dxPopupConfirm.Hide(); xcpn.PerformCallback('delete'); },function(s, e) { dxPopupConfirm.Hide(); }); }">
                                </ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" OnClick="btnAdd_Click">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
