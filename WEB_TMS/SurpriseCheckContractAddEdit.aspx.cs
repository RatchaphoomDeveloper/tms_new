﻿using DevExpress.Web.ASPxEditors;
using EmailHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.SurpriseCheck;

public partial class SurpriseCheckContractAddEdit : PageBase
{
    DataTable dt;
    DataSet ds;
    DataRow dr;
    string MessSAPSuccess = "ข้อมูลพนักงานใน SAP สำเร็จ<br/ >";
    string MessSAPFail = "<span style=\"color:Red;\">{0}ข้อมูลพนักงานใน SAP ไม่สำเร็จ !!!{1}</span><br/ >";
    private DataTable dtResult
    {
        get
        {
            if ((DataTable)ViewState["dtResult"] != null)
                return (DataTable)ViewState["dtResult"];
            else
                return null;
        }
        set
        {
            ViewState["dtResult"] = value;
        }
    }
    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }
    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private DataTable dtRequestFile
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFile"] != null)
                return (DataTable)ViewState["dtRequestFile"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFile"] = value;
        }
    }

    private DataTable dtUploadExpired
    {
        get
        {
            if ((DataTable)ViewState["dtUploadExpired"] != null)
                return (DataTable)ViewState["dtUploadExpired"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadExpired"] = value;
        }
    }
    private DataTable dtUploadTypeExpired
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeExpired"] != null)
                return (DataTable)ViewState["dtUploadTypeExpired"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeExpired"] = value;
        }
    }

    private DataTable dtRequestFileExpired
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileExpired"] != null)
                return (DataTable)ViewState["dtRequestFileExpired"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileExpired"] = value;
        }
    }

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            //InitialRequireField();
            InitialRequireFile();
            InitialUpload();
            if (Request.QueryString["str"] != null)
            {
                string str = Request.QueryString["str"];
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                string strQuery = Encoding.UTF8.GetString(decryptedBytes);
                SetData(strQuery);

                if (Session["CGROUP"] + string.Empty == "0")
                {
                    btnSave.Visible = false;
                    txtRemark.Enabled = false;
                    btnConfirm.Visible = false;
                    btnCancel.Visible = false;
                    if (int.Parse(hidIS_ACTIVE.Value) == 2 || int.Parse(hidIS_ACTIVE.Value) == 3 || int.Parse(hidIS_ACTIVE.Value) == 4 || int.Parse(hidIS_ACTIVE.Value) == 5)
                        btnEdit.Visible = true;
                    else
                        btnEdit.Visible = false;
                }
                else
                {
                    if (int.Parse(hidIS_ACTIVE.Value) == 1)
                    {
                        btnSave.Visible = false;
                        btnConfirm.Visible = true;
                        btnCancel.Visible = true;
                        txtRemark.Enabled = true;
                        btnEdit.Visible = false;
                    }
                    else if (int.Parse(hidIS_ACTIVE.Value) == 2 || int.Parse(hidIS_ACTIVE.Value) == 3 || int.Parse(hidIS_ACTIVE.Value) == 4 || int.Parse(hidIS_ACTIVE.Value) == 5)
                    {
                        btnSave.Visible = false;
                        btnConfirm.Visible = false;
                        btnCancel.Visible = false;
                        txtRemark.Enabled = false;
                        btnEdit.Visible = true;
                    }
                    else
                    {
                        btnSave.Visible = false;
                        txtRemark.Enabled = false;
                        btnConfirm.Visible = false;
                        btnCancel.Visible = false;
                        btnEdit.Visible = false;
                    }
                }

                if (int.Parse(hidIS_ACTIVE.Value) > 1)
                {
                    liTab2.Visible = true;
                    GeneralTab2.HRef = "SurpriseCheckContractAddEditTab2.aspx?str=" + str;

                }

                txtHsChasis.Enabled = false;
                txtTsChasis.Enabled = false;
                txtSHEADREGISTERNO.Enabled = false;
                txtSTRAILERREGISTERNO.Enabled = false;
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    //ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                    ddlVendor.Enabled = false;
                    //ddlVendor_SelectedIndexChanged(null, null);
                    //mode = "view";
                }
            }
            else
            {
                btnConfirm.Visible = false;
                btnCancel.Visible = false;
                txtRemark.Enabled = false;
                btnEdit.Visible = false;
                txtUser.Text = Session["UserName"] + string.Empty;
                hidREQUEST_BY.Value = Session["UserID"] + string.Empty;
                hidIS_ACTIVE.Value = "1";
                if (Session["CGROUP"] + string.Empty == "0")
                {
                    ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                    ddlVendor.Enabled = false;
                    ddlVendor_SelectedIndexChanged(null, null);
                    //mode = "view";
                }

            }

        }
        this.AssignAuthen();
    }

    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                //btnAddParties.Enabled = false;
                //btnAddProduct.Enabled = false;
                //btnUpload.Enabled = false;
                //btnApprove.Disabled = true;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region SetData
    private void SetData(string ID)
    {
        hidID.Value = ID;
        ds = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelectByID(ID);
        if (ds.Tables.Count > 0)
        {
            DataRow dr = ds.Tables[0].Rows[0];
            txtCHECK_DATE.Text = dr["CHECK_DATE"] + string.Empty;
            hidIS_ACTIVE.Value = dr["IS_ACTIVE"] + string.Empty;
            //txtSTRAILERREGISTERNO.Text = dr["STRAILERREGISTERNO"] + string.Empty;
            txtPLACE.Text = dr["PLACE"] + string.Empty;
            txtUser.Text = dr["REQUEST_BY_NAME"] + string.Empty;
            hidREQUEST_BY.Value = dr["REQUEST_BY"] + string.Empty;
            ddlVendor.SelectedValue = dr["SVENDORID"] + string.Empty;
            ddlVendor_SelectedIndexChanged(null, null);
            ddlContract.SelectedValue = dr["SCONTRACTID"] + string.Empty;
            txtHsChasis.Text = dr["CHASIS_H"] + string.Empty;
            txtTsChasis.Text = dr["CHASIS_T"] + string.Empty;
            txtSHEADREGISTERNO.Text = dr["SHEADREGISTERNO"] + string.Empty;
            txtSTRAILERREGISTERNO.Text = dr["STRAILERREGISTERNO"] + string.Empty;
            txtRemark.Text = dr["REMARK"] + string.Empty;
            hidIS_ACTIVE.Value = dr["IS_ACTIVE"] + string.Empty;
            ds.Tables[1].Columns.Remove("T_CHECK_CONTRACT_ID");
            DataRow[] drs = ds.Tables[1].Select("FILE_TYPE = 'NOT_EXPIRED'");

            if (drs.Any())
            {
                dtUpload = drs.CopyToDataTable();
            }
            else
            {
                dtUpload = ds.Tables[1].Clone();
            }
            drs = ds.Tables[1].Select("FILE_TYPE = 'EXPIRED'");
            if (drs.Any())
            {
                dtUploadExpired = drs.CopyToDataTable();
            }
            else
            {
                dtUploadExpired = ds.Tables[1].Clone();
            }
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            GridViewHelper.BindGridView(ref dgvUploadFileExpired, dtUploadExpired);

            if (hidIS_ACTIVE.Value == "2" || hidIS_ACTIVE.Value == "4")
            {
                btnSave.Visible = false;

            }
        }

    }
    #endregion

    #region DrowDownList
    private void SetDrowDownList()
    {
        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;
    }
    #endregion

    #region btn
    #region mpSave_ClickOK
    protected void mpSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = Validate();
            //mess += ValidateSaveUpLoadFile();
            //mess += ValidateSaveUpLoadFileExpired();
            if (string.IsNullOrEmpty(mess))
            {
                SaveData();
                alertSuccess("บันทึกสำเร็จ", "SurpriseCheckContract.aspx");

            }
            else
            {
                alertFail(mess);
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    private void SendEmail(int TemplateID, string Status)
    {
        //return; // ไม่ต้องส่ง E-mail

        try
        {
            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtComplainEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == 130)
                {
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", rblTruckType.SelectedItem.ToString());
                    Body = Body.Replace("{CHASSIS}", txtHsChasis.Text + "/" + txtTsChasis.Text);
                    Body = Body.Replace("{LICENSE}", txtSHEADREGISTERNO.Text + "/" + txtSTRAILERREGISTERNO.Text);
                    Body = Body.Replace("{APPOINTDATE}", txtCHECK_DATE.Text);
                    Body = Body.Replace("{APPOINTLOCATE}", txtPLACE.Text);
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem.Text);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(dtResult.Rows[0]["RESULT"].ToString());
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body, "", "SurpriseCheckContrack1", ColumnEmailName);
                }
                else if (TemplateID == 131)
                {
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", rblTruckType.SelectedItem.ToString());
                    Body = Body.Replace("{CHASSIS}", txtHsChasis.Text + "/" + txtTsChasis.Text);
                    Body = Body.Replace("{LICENSE}", txtSHEADREGISTERNO.Text + "/" + txtSTRAILERREGISTERNO.Text);
                    Body = Body.Replace("{APPOINTDATE}", txtCHECK_DATE.Text);
                    Body = Body.Replace("{APPOINTLOCATE}", txtPLACE.Text);
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem.Text);
                    Body = Body.Replace("{REMARK}", txtRemark.Text);

                    string str = Request.QueryString["str"];
                    var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                    string strQuery = Encoding.UTF8.GetString(decryptedBytes);

                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(strQuery);
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body, "", "SurpriseCheckContrack2", ColumnEmailName);
                }
                else if (TemplateID == 134)
                {
                    Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());
                    Body = Body.Replace("{TOTALCAR}", "1");
                    Body = Body.Replace("{TYPE}", rblTruckType.SelectedItem.ToString());
                    Body = Body.Replace("{CHASSIS}", txtHsChasis.Text + "/" + txtTsChasis.Text);
                    Body = Body.Replace("{LICENSE}", txtSHEADREGISTERNO.Text + "/" + txtSTRAILERREGISTERNO.Text);
                    Body = Body.Replace("{APPOINTDATE}", txtCHECK_DATE.Text);
                    Body = Body.Replace("{APPOINTLOCATE}", txtPLACE.Text);
                    Body = Body.Replace("{STATUS}", Status);
                    Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem.Text);
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "SurpriseCheckContractAddEdit.aspx?str=" + ConfigValue.GetEncodeText(dtResult.Rows[0]["RESULT"].ToString());
                    Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));

                    //MailService.SendMail("nut.t@pttor.com,sake.k@pttor.com,apipat.k@pttor.com,thanyavit.k@pttor.com," + ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body);
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, true, true), Subject, Body, "", "SurpriseCheckContrack1", ColumnEmailName);
                }

            }
        }
        catch (Exception ex)
        {
            //alertFail(ex.Message);
        }
    }


    #region mpBack_ClickOK
    protected void mpBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("SurpriseCheckContract.aspx");

    }
    #endregion

    #endregion

    #region SaveData
    private string SaveData()
    {
        dt = new DataTable("DT");
        dt.Columns.Add("ID", typeof(string));
        dt.Columns.Add("CHECK_DATE", typeof(string));
        dt.Columns.Add("TRUCK_TYPE", typeof(int));
        dt.Columns.Add("CHASIS_H", typeof(string));
        dt.Columns.Add("CHASIS_T", typeof(string));
        dt.Columns.Add("SHEADREGISTERNO", typeof(string));
        dt.Columns.Add("STRAILERREGISTERNO", typeof(string));
        dt.Columns.Add("SVENDORID", typeof(string));
        dt.Columns.Add("SCONTRACTID", typeof(int));
        dt.Columns.Add("PLACE", typeof(string));
        dt.Columns.Add("REQUEST_BY", typeof(int));
        dt.Columns.Add("IS_ACTIVE", typeof(int));
        dt.Columns.Add("CREATE_BY", typeof(int));
        dt.Columns.Add("UPDATE_BY", typeof(int));

        dr = dt.NewRow();
        dr["ID"] = hidID.Value;
        dr["CHECK_DATE"] = txtCHECK_DATE.Text.Trim();
        dr["TRUCK_TYPE"] = rblTruckType.SelectedValue;
        dr["CHASIS_H"] = txtHsChasis.Text.Trim();
        dr["CHASIS_T"] = txtTsChasis.Text.Trim();
        dr["SHEADREGISTERNO"] = txtSHEADREGISTERNO.Text.Trim();
        dr["STRAILERREGISTERNO"] = txtSTRAILERREGISTERNO.Text.Trim();
        dr["SVENDORID"] = ddlVendor.SelectedIndex > 0 ? ddlVendor.SelectedValue : "";
        dr["SCONTRACTID"] = ddlContract.SelectedIndex > 0 ? ddlContract.SelectedValue : "";
        //dr["STRUCKID"] = ddlSHEADREGISTERNO.SelectedValue;
        dr["PLACE"] = txtPLACE.Text.Trim();
        dr["REQUEST_BY"] = hidREQUEST_BY.Value;
        dr["CREATE_BY"] = Session["UserID"] + string.Empty;
        dr["UPDATE_BY"] = Session["UserID"] + string.Empty;
        dr["IS_ACTIVE"] = hidIS_ACTIVE.Value;
        dt.Rows.Add(dr);

        DataSet ds = new DataSet("DS");
        ds.Tables.Add(dt.Copy());
        dtUpload.TableName = "FILE";
        DataTable dtFile = dtUpload.Clone();
        if (!dtFile.Columns.Contains("FILE_TYPE"))
        {
            dtFile.Columns.Add("FILE_TYPE");
        }
        foreach (DataRow item in dtUpload.Rows)
        {
            dr = dtFile.NewRow();
            dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
            dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
            dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
            dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
            dr["REMARK"] = item["REMARK"] + string.Empty;
            dr["FILE_TYPE"] = "NOT_EXPIRED";
            dtFile.Rows.Add(dr);
        }

        foreach (DataRow item in dtUploadExpired.Rows)
        {
            dr = dtFile.NewRow();
            dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
            dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
            dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
            dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
            dr["REMARK"] = item["REMARK"] + string.Empty;
            dr["FILE_TYPE"] = "EXPIRED";
            dtFile.Rows.Add(dr);
        }
        dtFile.TableName = "FILE";
        ds.Tables.Add(dtFile.Copy());
        dtResult = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSave(ds);
        this.SendEmail(130, "ยื่นคำขอ");

        return "";
    }
    #endregion

    #region Upload
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }

    protected void btnUploadExpired_Click(object sender, EventArgs e)
    {
        this.StartUploadExpired();
    }

    private void StartUpload()
    {
        try
        {
            if (fileUpload.HasFile && ddlUploadType.SelectedIndex > 0)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem.Text, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์และประเภทไฟล์เอกสาร");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private void StartUploadExpired()
    {
        try
        {
            if (fileUploadExpired.HasFile && ddlUploadTypeExpired.SelectedIndex > 0)
            {
                string FileNameUser = fileUploadExpired.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFileExpired(System.IO.Path.GetExtension(FileNameUser), fileUploadExpired.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUploadExpired.SaveAs(Path + "\\" + FileNameSystem);

                dtUploadExpired.Rows.Add(ddlUploadTypeExpired.SelectedValue, ddlUploadTypeExpired.SelectedItem.Text, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFileExpired, dtUploadExpired);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์และประเภทไฟล์เอกสาร");
            }
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "SurpriseCheckContract" + "\\" + Session["UserID"].ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[0]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateUploadFileExpired(string ExtentionUser, int FileSize)
    {
        try
        {
            //string Extention = dtUploadTypeExpired.Rows[ddlUploadTypeExpired.SelectedIndex]["Extention"].ToString();
            //int MaxSize = int.Parse(dtUploadTypeExpired.Rows[ddlUploadTypeExpired.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            //if (FileSize > MaxSize)
            //    throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadTypeExpired.Rows[0]["MAX_FILE_SIZE"].ToString() + " MB)");

            //if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
            //    throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFileExpired_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //string s = dgvUploadFile.DataKeys[e.RowIndex].Value.ToString();
        try
        {
            dtUploadExpired.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFileExpired, dtUploadExpired);
        }
        catch (Exception ex)
        {
            // XtraMessageBox.Show(ex.Message, "Error");
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadFileExpired_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFileExpired.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }
    private void DownloadFile(string Path, string FileName)
    {
        try
        {
            //if (File.Exists(Path + "\\" + FileName))
            //{
            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
            //}
            //else
            //{
            //    alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
            //}

        }
        catch (Exception ex)
        {
            alertFail("เอกสารแนบหายไป กรุณา upload เอกสารแนบใหม่");
        }

    }
    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            //XtraMessageBox.Show(ex.Message, "Error");
        }
    }
    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("T_CHECK_CONTRACT_FILE");
        DropDownListHelper.BindDropDownList(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);
        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
        dtUpload.Columns.Add("REF_INT");
        dtUpload.Columns.Add("REF_STR");
        dtUpload.Columns.Add("STATUS");
        dtUpload.Columns.Add("REMARK");

        GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

        dtUploadTypeExpired = UploadTypeBLL.Instance.UploadTypeSelectBLL("T_CHECK_CONTRACT_FILE_EXPIRED");
        DropDownListHelper.BindDropDownList(ref ddlUploadTypeExpired, dtUploadTypeExpired, "UPLOAD_ID", "UPLOAD_NAME", true);
        dtUploadExpired = new DataTable();
        dtUploadExpired.Columns.Add("UPLOAD_ID");
        dtUploadExpired.Columns.Add("UPLOAD_NAME");
        dtUploadExpired.Columns.Add("FILENAME_SYSTEM");
        dtUploadExpired.Columns.Add("FILENAME_USER");
        dtUploadExpired.Columns.Add("FULLPATH");
        dtUploadExpired.Columns.Add("REF_INT");
        dtUploadExpired.Columns.Add("REF_STR");
        dtUploadExpired.Columns.Add("STATUS");
        dtUploadExpired.Columns.Add("REMARK");

        GridViewHelper.BindGridView(ref dgvUploadFileExpired, dtUploadExpired);
    }

    private void InitialRequireFile()
    {
        dtRequestFile = AccidentBLL.Instance.AccidentRequestFile("T_CHECK_CONTRACT_FILE");
        GridViewHelper.BindGridView(ref dgvRequestFile, dtRequestFile);
        //dtRequestFileExpired = AccidentBLL.Instance.AccidentRequestFile("T_CHECK_CONTRACT_FILE_EXPIRED");
        //GridViewHelper.BindGridView(ref dgvRequestFileExpired, dtRequestFileExpired);
    }

    #endregion

    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {
            if (ddlVendor.SelectedIndex <= 0)
            {
                mess += "กรุณาเลือกชื่อผู้ขนส่ง<br/>";
            }
            if (string.IsNullOrEmpty(txtHsChasis.Text.Trim()))
            {
                mess += "กรุณาป้อนหมายเลขหมายเลขแชสซีย์(หัว)<br/>";
            }
            if (rblTruckType.SelectedValue == "2" && string.IsNullOrEmpty(txtTsChasis.Text.Trim()))
            {
                mess += "กรุณาป้อนหมายเลขหมายเลขแชสซีย์(หาง)<br/>";
            }
            if (string.IsNullOrEmpty(txtCHECK_DATE.Text.Trim()))
            {
                mess += "กรุณาป้อนวันที่นัดหมาย<br/>";
            }
            if (string.IsNullOrEmpty(txtPLACE.Text.Trim()))
            {
                mess += "กรุณาป้อนสถานที่<br/>";
            }
            return mess;
        }
        catch (Exception ex)
        {
            return RemoveSpecialCharacters(ex.Message);
            //throw new Exception(RemoveSpecialCharacters(ex.Message));
        }
    }

    #endregion

    #region ValidateSaveUpLoadFile
    private string ValidateSaveUpLoadFile()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string ValidateSaveUpLoadFileExpired()
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUploadExpired.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFileExpired.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFileExpired.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFileExpired.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFileExpired.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUploadExpired.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFileExpired.Rows[i]["UPLOAD_ID"].ToString(), dtUploadExpired.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUploadExpired.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFileExpired.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFileExpired.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    //#region SendEmail
    //private bool SendEmail(int TemplateID, string accID)
    //{
    //    try
    //    {
    //        bool isREs = false;
    //        DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
    //        DataTable dtComplainEmail = new DataTable();

    //        if (dtTemplate.Rows.Count > 0)
    //        {
    //            string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
    //            string Body = dtTemplate.Rows[0]["BODY"].ToString();
    //            string EmailList = string.Empty;
    //            if (TemplateID == ConfigValue.EmailAccident2)
    //            {
    //                //#region EmailAccident2
    //                //EmailList = ComplainBLL.Instance.GetEmailComplainBLL(ConfigValue.DeliveryDepartmentCodeAdmin, ConfigValue.DeliveryDepartmentCodeAdmin2, string.Empty, int.Parse(Session["UserID"].ToString()), ddlVendor.SelectedValue, false, true);
    //                //if (!string.IsNullOrEmpty(EmailList))
    //                //{
    //                //    EmailList += ",";
    //                //}
    //                ////                    EmailList += @"TMS_komkrit.c@pttor.com,TMS_somchai.k@pttor.com,TMS_yutasak.c@pttor.com
    //                ////,TMS_kittipong.l@pttor.com,TMS_suphakit.k@pttor.com
    //                ////,TMS_apipat.k@pttor.com
    //                ////,TMS_nut.t@pttor.com";

    //                //EmailList += @"suphakit.k@pttor.com , nut.t@pttor.com,sake.k@pttor.com, terapat.p@pttor.com,  yutasak.c@pttor.com, apipat.k@pttor.com, wasupol.p@pttor.com,  bodin.a@pttor.com";
    //                //Subject = Subject.Replace("{ACCID}", accID);
    //                //Body = Body.Replace("{ACCID}", accID);
    //                //Body = Body.Replace("{CAR}", txtTruck.Text.Trim());
    //                //Body = Body.Replace("{VENDOR}", ddlVendor.SelectedItem != null ? ddlVendor.SelectedItem.Text.Trim() : "-");
    //                //Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem != null ? ddlContract.SelectedItem.Text.Trim() : "-");
    //                //Body = Body.Replace("{DRIVER}", txtEMPNAME.Text.Trim());
    //                //Body = Body.Replace("{ACCSTATUS}", ddlAccidentType.SelectedItem != null ? ddlAccidentType.SelectedItem.Text.Trim() : "-");
    //                //Body = Body.Replace("{SOURCE}", txtSABBREVIATION.Text.Trim());
    //                //Body = Body.Replace("{ACCPOINT}", txtLocation.Text.Trim());
    //                //Body = Body.Replace("{GPS}", txtGPSL.Text.Trim() + "," + txtGPSR.Text.Trim());
    //                ////Body = Body.Replace("{CREATE_DEPARTMENT}", Session["vendoraccountname"] + string.Empty);
    //                //if (!string.IsNullOrEmpty(txtAccidentDate.Text.Trim()))
    //                //{
    //                //    string[] AccidentDate = txtAccidentDate.Text.Trim().Split(' ');
    //                //    if (AccidentDate.Any())
    //                //    {
    //                //        Body = Body.Replace("{DATE}", AccidentDate[0]);
    //                //        Body = Body.Replace("{TIME}", AccidentDate[1]);
    //                //    }

    //                //}
    //                //byte[] plaintextBytes = Encoding.UTF8.GetBytes(accID);
    //                //string ID = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
    //                //string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "AccidentTab3_Vendor.aspx?str=" + ID;
    //                //Body = Body.Replace("{LINK}", ConfigValue.GetClickHere(Link));
    //                //MailService.SendMail(EmailList, Subject, Body);
    //                //#endregion

    //            }
    //            else if (TemplateID == ConfigValue.EmailAccident3)
    //            {


    //            }
    //            isREs = true;

    //        }
    //        return isREs;
    //    }
    //    catch (Exception ex)
    //    {
    //        return false;
    //    }
    //}

    //#endregion

    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void btnHsChasisSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtHsChasis.Text.Trim()))
        {
            string vendor = string.Empty;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                vendor = Session["SVDID"] + string.Empty;
            }
            dt = SurpriseCheckContractBLL.Instance.TruckSelectByChasis(txtHsChasis.Text.Trim(), vendor);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtSHEADREGISTERNO.Text = dt.Rows[0]["SHEADREGISTERNO"] + string.Empty;
            }
        }
    }

    protected void btnTsChasis_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtTsChasis.Text.Trim()))
        {
            string vendor = string.Empty;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                vendor = Session["SVDID"] + string.Empty;
            }
            dt = SurpriseCheckContractBLL.Instance.TruckSelectByChasis(txtTsChasis.Text.Trim(), vendor);
            if (dt != null && dt.Rows.Count > 0)
            {
                txtSTRAILERREGISTERNO.Text = dt.Rows[0]["SHEADREGISTERNO"] + string.Empty;
            }
        }
    }

    protected void rblTruckType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblTruckType.SelectedValue == "2")
        {
            lblReqtxtTsChasis.Visible = true;
            txtTsChasis.Enabled = true;
            txtSTRAILERREGISTERNO.Enabled = true;
        }
        else
        {
            txtTsChasis.Enabled = false;
            txtSTRAILERREGISTERNO.Enabled = false;
            lblReqtxtTsChasis.Visible = false;
        }
    }
    protected void mpConfirm_ClickOK(object sender, EventArgs e)
    {
        try
        {
            SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(hidID.Value), 5, int.Parse(Session["UserID"] + string.Empty), txtRemark.Text);
            this.SendEmail(131, "ยืนยัน");
            alertSuccess("ยืนยันสำเร็จ", "SurpriseCheckContract.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void mpCancel_ClickOK(object sender, EventArgs e)
    {
        try
        {
            if (string.Equals(txtRemark.Text.Trim(), string.Empty))
                throw new Exception("กรุณา กรอกหมายเหตุ");
            SurpriseCheckContractBLL.Instance.SurpriseCheckContractChangeStatusSave(int.Parse(hidID.Value), 6, int.Parse(Session["UserID"] + string.Empty), txtRemark.Text);
            this.SendEmail(131, "ยกเลิก");
            alertSuccess("ยกเลิกสำเร็จ", "SurpriseCheckContract.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void mpEdit_ClickOK(object sender, EventArgs e)
    {
        try
        {
            string mess = Validate();
            //mess += ValidateSaveUpLoadFile();
            //mess += ValidateSaveUpLoadFileExpired();
            if (string.IsNullOrEmpty(mess))
            {
                dt = new DataTable("DT");
                dt.Columns.Add("ID", typeof(string));
                dt.Columns.Add("CHECK_DATE", typeof(string));
                dt.Columns.Add("TRUCK_TYPE", typeof(int));
                dt.Columns.Add("CHASIS_H", typeof(string));
                dt.Columns.Add("CHASIS_T", typeof(string));
                dt.Columns.Add("SHEADREGISTERNO", typeof(string));
                dt.Columns.Add("STRAILERREGISTERNO", typeof(string));
                dt.Columns.Add("SVENDORID", typeof(string));
                dt.Columns.Add("SCONTRACTID", typeof(int));
                dt.Columns.Add("PLACE", typeof(string));
                dt.Columns.Add("REQUEST_BY", typeof(int));
                dt.Columns.Add("IS_ACTIVE", typeof(int));
                dt.Columns.Add("CREATE_BY", typeof(int));
                dt.Columns.Add("UPDATE_BY", typeof(int));

                dr = dt.NewRow();
                dr["ID"] = hidID.Value;
                dr["CHECK_DATE"] = txtCHECK_DATE.Text.Trim();
                dr["TRUCK_TYPE"] = rblTruckType.SelectedValue;
                dr["CHASIS_H"] = txtHsChasis.Text.Trim();
                dr["CHASIS_T"] = txtTsChasis.Text.Trim();
                dr["SHEADREGISTERNO"] = txtSHEADREGISTERNO.Text.Trim();
                dr["STRAILERREGISTERNO"] = txtSTRAILERREGISTERNO.Text.Trim();
                dr["SVENDORID"] = ddlVendor.SelectedIndex > 0 ? ddlVendor.SelectedValue : "";
                dr["SCONTRACTID"] = ddlContract.SelectedIndex > 0 ? ddlContract.SelectedValue : "";
                //dr["STRUCKID"] = ddlSHEADREGISTERNO.SelectedValue;
                dr["PLACE"] = txtPLACE.Text.Trim();
                dr["REQUEST_BY"] = hidREQUEST_BY.Value;
                dr["CREATE_BY"] = Session["UserID"] + string.Empty;
                dr["UPDATE_BY"] = Session["UserID"] + string.Empty;
                dr["IS_ACTIVE"] = hidIS_ACTIVE.Value;
                dt.Rows.Add(dr);

                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dt.Copy());
                dtUpload.TableName = "FILE";
                DataTable dtFile = dtUpload.Clone();
                if (!dtFile.Columns.Contains("FILE_TYPE"))
                {
                    dtFile.Columns.Add("FILE_TYPE");
                }
                foreach (DataRow item in dtUpload.Rows)
                {
                    dr = dtFile.NewRow();
                    dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
                    dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
                    dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
                    dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
                    dr["REMARK"] = item["REMARK"] + string.Empty;
                    dr["FILE_TYPE"] = "NOT_EXPIRED";
                    dtFile.Rows.Add(dr);
                }

                foreach (DataRow item in dtUploadExpired.Rows)
                {
                    dr = dtFile.NewRow();
                    dr["UPLOAD_ID"] = item["UPLOAD_ID"] + string.Empty;
                    dr["FILENAME_SYSTEM"] = item["FILENAME_SYSTEM"] + string.Empty;
                    dr["FILENAME_USER"] = item["FILENAME_USER"] + string.Empty;
                    dr["FULLPATH"] = item["FULLPATH"] + string.Empty;
                    dr["REMARK"] = item["REMARK"] + string.Empty;
                    dr["FILE_TYPE"] = "EXPIRED";
                    dtFile.Rows.Add(dr);
                }
                dtFile.TableName = "FILE";
                ds.Tables.Add(dtFile.Copy());
                dtResult = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSave(ds);
                this.SendEmail(134, (hidIS_ACTIVE.Value == "2" ? "ผ่าน" : (hidIS_ACTIVE.Value == "3" ? "แก้ไข" : (hidIS_ACTIVE.Value == "4" ? "นัดหมายใหม่" : (hidIS_ACTIVE.Value == "5" ? "ยืนยัน" : "ยกเลิก")))));
                alertSuccess("บันทึกสำเร็จ", "SurpriseCheckContract.aspx");
            }
            else
            {
                alertFail(mess);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}