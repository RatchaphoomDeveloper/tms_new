﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Data;
using DevExpress.Web.ASPxUploadControl;
using System.Configuration;
using System.Data.OracleClient;
using System.Web.Configuration;
using System.Globalization;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System.Text.RegularExpressions;

public partial class AdminHelper : System.Web.UI.Page
{
    int defInt;
    // Connection 
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private OracleConnection connection = new OracleConnection(ConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString);
    const string UploadDirectory = "~/UploadFile/EvidenceOfWrongdoing/";
    const string UploadConfirmTruckDirectory = "~/UploadFile/ConfirmTruck/";
    const string UploadGPSDirectory = "~/UploadFile/AdminHelper/";
    const int ThumbnailSize = 100;
    string[] ArrayGPSType = { "XLS", "PIC" };
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UserID"] = "441";
        Session["CGROUP"] = "1";
    }

    string SavePostedFiles(UploadedFile uploadedFile)
    {
        string PathUploadDirectory = UploadDirectory + "/" + "";
        dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
        string IssueData = "" + Session["IssueData"];
        /*
         *0     14$58
         *1     58
         *2     "TR4901473"
         *3     "กท.75-0887"
         *4     ""
         *5     "#"
        */
        string[] ArrayIssueData = IssueData.Split('^');
        PathUploadDirectory = ArrayIssueData[1] + "/" + ArrayIssueData[2] + "/" + Convert.ToDateTime("" + MasterData[1]).ToString("MMddyyyy", new CultureInfo("en-US")) + "/";

        if (!uploadedFile.IsValid)
            return string.Empty;

        FileInfo fileInfo = new FileInfo(uploadedFile.FileName);
        if (!Directory.Exists(UploadDirectory + PathUploadDirectory)) Directory.CreateDirectory(MapPath(UploadDirectory + PathUploadDirectory));
        string resFileName = MapPath(UploadDirectory + PathUploadDirectory) + fileInfo.Name;

        if (File.Exists(resFileName))
        {
            string sPath = Path.GetDirectoryName(resFileName)
                , sFileName = Path.GetFileNameWithoutExtension(resFileName)
                , sFileType = Path.GetExtension(resFileName);
            int nExists = Directory.GetFiles(Path.GetDirectoryName(resFileName), "*" + Path.GetFileName(resFileName)).Length;
            sPath = sPath + "\\" + "(" + nExists + ")" + sFileName + sFileType;
            File.Copy(resFileName, sPath, true);
        }

        string fileLabel = fileInfo.Name;
        string fileLength = uploadedFile.ContentLength / 1024 + "K";
        #region temp Data Attechment
        {//temp Data Attechment 
            DataTable dtAttechment = new DataTable();
            #region prepaire temp data Attechment storage
            if (Session["DataAttechment"] == null)
            {
                dtAttechment.Columns.Add("SCHECKID", typeof(string));
                dtAttechment.Columns.Add("NATTACHMENT", typeof(string));
                dtAttechment.Columns.Add("STRUCKID", typeof(string));
                dtAttechment.Columns.Add("SCONTRACTID", typeof(string));
                dtAttechment.Columns.Add("SATTACHTYPEID", typeof(string));
                dtAttechment.Columns.Add("SPATH", typeof(string));
                dtAttechment.Columns.Add("SFILE", typeof(string));
                dtAttechment.Columns.Add("SSYSFILE", typeof(string));
                dtAttechment.Columns.Add("DCREATE", typeof(string));
                dtAttechment.Columns.Add("SCREATE", typeof(string));
                dtAttechment.Columns.Add("DUPDATE", typeof(string));
                dtAttechment.Columns.Add("SUPDATE", typeof(string));
                dtAttechment.Columns.Add("cRowFlag", typeof(string));
            }
            else
            {
                dtAttechment = (DataTable)Session["DataAttechment"];
                foreach (DataRow drRemove in dtAttechment.Select("cRowFlag='Page' AND SFILE='" + fileInfo.Name + "'")) dtAttechment.Rows[dtAttechment.Rows.IndexOf(drRemove)].Delete();

            }
            #endregion
            DataRow drAttechment;

            drAttechment = dtAttechment.NewRow();
            drAttechment["SCHECKID"] = "";
            drAttechment["NATTACHMENT"] = dtAttechment.Rows.Count + 1;
            drAttechment["SCONTRACTID"] = "" + ArrayIssueData[1];
            drAttechment["STRUCKID"] = "" + ArrayIssueData[2];
            drAttechment["SATTACHTYPEID"] = "";
            drAttechment["SPATH"] = "" + UploadDirectory + PathUploadDirectory;
            drAttechment["SFILE"] = "" + fileInfo.Name;
            drAttechment["SSYSFILE"] = "" + fileInfo.Name;
            drAttechment["DCREATE"] = "" + Convert.ToDateTime("" + MasterData[1]).ToString("MM/dd/yyyy", new CultureInfo("en-US"));
            drAttechment["SCREATE"] = "" + Session["SVDID"];
            drAttechment["DUPDATE"] = "";
            drAttechment["SUPDATE"] = "";
            drAttechment["cRowFlag"] = "Page";

            dtAttechment.Rows.Add(drAttechment);
            Session["DataAttechment"] = dtAttechment;

        }
        #endregion

        uploadedFile.SaveAs(resFileName);

        return string.Format("{0} ({1})|{2}#{3}", fileLabel, fileLength, fileInfo.Name, (UploadDirectory + PathUploadDirectory).Remove(0, 2));
    }
    private string UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile, string mode)
    {
        string ServerMapPath = Server.MapPath("./") + pathFile.Replace("/", "\\");
        FileInfo File = new FileInfo(ServerMapPath + ful.FileName);
        string ResultFileName = ServerMapPath + File.Name;
        string sPath = Path.GetDirectoryName(ResultFileName)
                , sFileName = Path.GetFileNameWithoutExtension(ResultFileName)
                , sFileType = Path.GetExtension(ResultFileName);

        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(ServerMapPath))
            {
                Directory.CreateDirectory(ServerMapPath);
            }
            #endregion
            string fileName = (GenFileName + "" + sFileType.Trim());
            ful.SaveAs(ServerMapPath + fileName);
            string uploadmode = (System.IO.File.Exists(ServerMapPath + fileName) && pathFile.Replace("\\", "/").Split('/')[2] + "" == "Temp_Excel") ? "Excel" : "Picture";
            //LogUser("1", "I", "อัพโหลดไฟล์(" + uploadmode + ") หน้ายืนยันรถตามสัญญา", "");///แก้ไขให้ระบบเก็บLoglว่าอัพโหลดExcel||Picture
            if (System.IO.File.Exists(ServerMapPath + fileName) && pathFile.Replace("\\", "/").Split('/')[2] + "" == "Temp_Excel")
            {
                OpenExcel2DataTable(ServerMapPath, "UPLOADED", fileName, mode);
            }
            return fileName + "$" + sFileName.Replace("$", "") + sFileType;
        }
        else
            return "$";
    }
    private string GetExcelSheet(OleDbConnection excelcon, string _tableName)
    {
        // หาชื่อ sheet
        string _exxcelsheet = "";
        DataRowCollection _ar_dr = excelcon.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" }).Rows;
        foreach (DataRow _dr in _ar_dr)
        {
            if (!_dr["TABLE_NAME"].ToString().IndexOf("$").Equals(-1) && !string.IsNullOrEmpty("" + _dr["TABLE_NAME"]) && _dr["TABLE_NAME"].ToString().Equals(_tableName + "$"))
            {
                _exxcelsheet = _dr["TABLE_NAME"].ToString();
                break;
            }
        }
        return _exxcelsheet;
    }
    private string GetConnectionForFileExcel(FileInfo file)
    {


        // Connection String to Excel Workbook
        // Connection String < v.2007
        string excelConnectionString = "Provider='Microsoft.Jet.OLEDB.4.0';Data Source='" + file.FullName + "';Extended Properties='Excel 8.0;HDR=Yes'";
        //if (file.Extension.ToLower().Equals(".xlsx"))
        //{
        // Connection String = v.2007
        excelConnectionString = "Provider='Microsoft.ACE.OLEDB.12.0';Data Source='" + file.FullName + "';Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;ImportMixedTypes=Text'";
        // }
        return excelConnectionString;
    }
    protected void OpenExcel2DataTable(string path_upload, string GPSMode, string SystemFileName, string TableName)
    {
        Session["ss_DataGPSExcel"] = null;

        StringBuilder _err = new StringBuilder();
        int VisibleIndex = xgvw.EditingRowVisibleIndex;
        dynamic MasterData = xgvw.GetRowValues(VisibleIndex, "SCONTRACTID", "DDATE");
        FileInfo _fi = new FileInfo(path_upload + "/" + SystemFileName);// Declare FileInfo
        string excelConnectionString = this.GetConnectionForFileExcel(_fi);// Create Connection to Excel Workbook
        #region OpenExcel2DT
        using (OleDbConnection excelcon = new OleDbConnection(excelConnectionString))
        {
            excelcon.Open();

            // หาชื่อ sheet
            string _exxcelsheet = this.GetExcelSheet(excelcon, TableName);
            if (_exxcelsheet.Equals("")) { excelcon.Close(); _err.AppendFormat("Not found sheet({0})<br />", _fi.Name); }
            else
            {


                //WHERE [ตำแหน่ง] <> ''
                OleDbCommand command = new OleDbCommand("SELECT * FROM [" + _exxcelsheet + "]", excelcon);

                // Find effective date
                string _effdate_en = "";
                string _effdate_th = "";
                DataSet _ds = new DataSet();
                new OleDbDataAdapter(command).Fill(_ds);
                if (_ds.Tables.Count > 0)
                {
                    DataTable dt_CheckNull = _ds.Tables[0];

                    //เช็คว่ามี Row ไหนที่มีค่าว่างทั้งหมด ให้นำออก
                    foreach (var row in dt_CheckNull.Rows.Cast<DataRow>())
                    {
                        var isEmpty = row.ItemArray.All(x => x == null || (x != null && string.IsNullOrWhiteSpace(x.ToString())));
                        if (isEmpty)
                        {
                            dt_CheckNull = dt_CheckNull.Rows.Cast<DataRow>().Where(rows => !rows.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                            break;
                        }
                    }

                    Session["ss_DataGPSExcel"] = Session["ss_GPSExcel"] = dt_CheckNull;
                    //CheckDataTempExcel(_ds.Tables[0]);
                }
                else { Session["ss_GPSExcel"] = null; Session["ss_DataGPSExcel"] = null; }
                /// Close connect file
                excelcon.Close();

                /// Clear object
                _ds.Dispose();
                command.Dispose();
            }
        }
        #endregion
    }
    protected void uclGPSxls_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/AdminHelper/Temp_Excel/", "Employee");
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }
    protected void xgvw_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string CallbackName = "CUSTOMCALLBACK";
        ASPxGridView xgvwCar = ((ASPxGridView)sender);
        //BackUpData(xgvw.EditingRowVisibleIndex, ((ASPxGridView)sender));
        int idxMaster = xgvw.EditingRowVisibleIndex;
        int idxParent = 0;
        string[] param = e.Args[0].ToString().Split('$');
        e.Args[0] = param[0];
        CallbackName = param[1];
        if (e.Args.Length > 0) idxParent = (int.TryParse("" + e.Args[0], out defInt)) ? int.Parse("" + e.Args[0]) : 0;
        switch (CallbackName.ToUpper())
        {
            case "ISSUE":
            case "HIDEALLDETAIL":
            case "HIDEDETAILROW":
            case "SHOWDETAILROW":
            case "CUSTOMCALLBACK":
                break;

            case "EMPLOYEE":
                #region EMPLOYEE
                if (Session["ss_GPSExcel"] == null)
                {
                    CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการที่การ Upload กรุณาทำการอัพโหลดใหม่อีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                }
                //else
                //{
                //    CheckDataTempExcel((DataTable)Session["ss_GPSExcel"]);
                //}
                Session["ss_GPSExcel"] = null;//Clear Session first for safty
                if (Session["ss_DataGPSExcel"] != null)
                {
                    DataTable dt_DataGPSExcel = (DataTable)Session["ss_DataGPSExcel"];
                    if (dt_DataGPSExcel.Columns.Count > 0)
                    {
                        dt_DataGPSExcel.Columns.Add(new DataColumn("Status", typeof(string)));
                    }
                    string sInsFormat_SAP = @"INSERT INTO TEMPLOYEE_SAP ( SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, DUPDATE, SUPDATE, PERS_CODE, CARRIER, LICENSE_NO, DRVSTATUS) 
VALUES ( :sID , '{1}', '{2}', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2', '{9}', '{8}',  '{10}', '{13}' );";
                    string sInsFormat = @"INSERT INTO TEMPLOYEE ( SEMPLOYEEID, STRANS_ID, STRANTYPE, SEMPTPYE, SDRIVERNO , DBIRTHDATE, SPERSONELNO, DDRIVEBEGIN, DDRIVEEXPIRE, STEL, STEL2, SMAIL,  SMAIL2,
CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) 
VALUES ( :sID , '{8}', null, '{0}', '{10}' , TO_DATE('{3}','yyyy-mm-dd'), '{9}', '{11}', '{12}', '{4}', '{5}', '{6}',  '{7}', '1', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2');";
                    if (dt_DataGPSExcel.Rows.Count > 0)
                    {
                        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID)
WHERE  1=1 
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE 1=1";
                        sdsVendor.SelectParameters.Clear();
                        sdsVendor.DataBind();
                        DataTable dtVendor = ((DataView)sdsVendor.Select(new DataSourceSelectArguments())).ToTable();

                        StringBuilder stbd = new StringBuilder();
                        StringBuilder stbd_Error = new StringBuilder();
                        foreach (DataRow dr_DataGPSExcel in dt_DataGPSExcel.Rows)
                        {

                            var TRANSPORTER = dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "'").ToList().FirstOrDefault();
                            string SVENDORNAME = TRANSPORTER != null ? TRANSPORTER.ItemArray[0] + "" : "";
                            bool HasID = false;
                            string sMissText = "";
                            int nRowIndex = dt_DataGPSExcel.Rows.IndexOf(dr_DataGPSExcel);
                            sMissText += (dr_DataGPSExcel["ตำแหน่ง"] + "" == "") ? ",ตำแหน่ง" : "";
                            sMissText += (dr_DataGPSExcel["ชื่อ"] + "" == "") ? ",ชื่อ" : "";
                            sMissText += (dr_DataGPSExcel["นามสกุล"] + "" == "") ? ",นามสกุล" : "";
                            sMissText += (dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "" == "") ? ",บริษัทผู้ขนส่ง" : "";
                            sMissText += (dr_DataGPSExcel["Carrier Code"] + "" == "") ? ",CarrierCode" : "";//sMissText += (dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "'").Length == 0) ? ",ไม่พบ บริษัทผู้ขนส่ง" : "";
                            sMissText += (dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "" == "") ? ",หมายเลขบัตรประชาชน" : "";

                            /*
    +		[0]	    {ตำแหน่ง}	object {System.Data.DataColumn}
    +		[1]	    {ชื่อ}	object {System.Data.DataColumn}
    +		[2]	    {นามสกุล}	object {System.Data.DataColumn}
    +		[3]	    {วันเกิด}	object {System.Data.DataColumn}
    +		[4]	    {หมายเลขโทรศัพท์หลัก}	object {System.Data.DataColumn}
    +		[5]	    {หมายเลขโทรศัพท์สำรอง}	object {System.Data.DataColumn}
    +		[6]	    {อีเมล์หลัก}	object {System.Data.DataColumn}
    +		[7]	    {อีเมล์สำรอง}	object {System.Data.DataColumn}
    +		[8]	    {บริษัทผู้ขนส่ง}	object {System.Data.DataColumn}
    +		[9]	    {หมายเลขบัตรประชาชน}	object {System.Data.DataColumn}
    +		[10]	{หมายเลขใบขับขี่ประเภท 4}	object {System.Data.DataColumn}
    +		[11]	{อนุญาตใบขับขี่ประเภท 4 _ตั้งแต่วันที่}	object {System.Data.DataColumn}
    +		[12]	{อนุญาตใบขับขี่ประเภท 4 _หมดอายุวันที่}	object {System.Data.DataColumn}
            {13}    DRVSTATUS

                             */
                            TBL_Vendor imp_vend = new TBL_Vendor(Page, connection);
                            imp_vend.PERS_CODE = dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "";
                            if (sMissText != "")
                            {
                                stbd_Error.Append("^Line " + nRowIndex + ":" + sMissText);
                                dr_DataGPSExcel.BeginEdit();
                                dr_DataGPSExcel["Status"] = ">" + stbd_Error;
                                dr_DataGPSExcel.EndEdit();

                            }
                            else
                            {
                                imp_vend.Open();
                                HasID = (!string.IsNullOrEmpty(imp_vend.EMPLOYEEID));

                                //FC_GENID_EMPLOYEEID()

                                imp_vend.EMPTPYE = dr_DataGPSExcel["ตำแหน่ง"] + "";
                                imp_vend.FNAME = dr_DataGPSExcel["ชื่อ"] + "";
                                imp_vend.LNAME = dr_DataGPSExcel["นามสกุล"] + "";
                                imp_vend.TRANS_ID = (dr_DataGPSExcel["Carrier Code"] + "").Trim().PadLeft(10, '0');//SVENDORNAME;
                                imp_vend.PERS_CODE = (dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "").Trim();
                                imp_vend.BIRTHDATE = dr_DataGPSExcel["วันเกิด"] + "";
                                imp_vend.TEL = dr_DataGPSExcel["หมายเลขโทรศัพท์หลัก"] + "";
                                imp_vend.TEL2 = dr_DataGPSExcel["หมายเลขโทรศัพท์สำรอง"] + "";
                                imp_vend.MAIL = dr_DataGPSExcel["อีเมล์หลัก"] + "";
                                imp_vend.MAIL2 = dr_DataGPSExcel["อีเมล์สำรอง"] + "";
                                imp_vend.LICENSE_NO = dr_DataGPSExcel["หมายเลขใบขับขี่ประเภท 4"] + "";
                                imp_vend.DRIVERNO_BEGIN = dr_DataGPSExcel["อนุญาตใบขับขี่ประเภท 4 _ตั้งแต่วันที่"] + "";
                                imp_vend.DRIVERNO_Expire = dr_DataGPSExcel["อนุญาตใบขับขี่ประเภท 4 _หมดอายุวันที่"] + "";
                                imp_vend.DRVSTATUS = "1";
                                string sExecute = "";
                                if (HasID)
                                {
                                    sExecute = @"UPDATE TEMPLOYEE_SAP 
SET FNAME='{1}', LNAME='{2}', DUPDATE=sysdate, SUPDATE='IMP_TMS2', PERS_CODE='{9}', CARRIER='{8}', LICENSE_NO='{10}', DRVSTATUS ='{13}'
WHERE SEMPLOYEEID='" + imp_vend.EMPLOYEEID + @"' AND PERS_CODE='{9}';UPDATE TEMPLOYEE SET STRANS_ID='{8}', SEMPTPYE='{0}', SDRIVERNO='{10}' , DBIRTHDATE={3}, SPERSONELNO='{9}'
, DDRIVEBEGIN={11}, DDRIVEEXPIRE={12}, STEL='{4}', STEL2='{5}', SMAIL='{6}',  SMAIL2='{7}',
CACTIVE='{13}', DUPDATE=sysdate, SUPDATE='IMP_TMS2' WHERE SEMPLOYEEID='" + imp_vend.EMPLOYEEID + @"' AND SPERSONELNO='{9}';";
                                }
                                else
                                {
//                                    sExecute = @"EID := FC_GENID_EMPLOYEEID(); INSERT INTO TEMPLOYEE_SAP ( SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, DUPDATE, SUPDATE, PERS_CODE, CARRIER, LICENSE_NO, DRVSTATUS) 
//VALUES ( EID , '{1}', '{2}', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2', '{9}', '{8}',  '{10}', '{13}' );INSERT INTO TEMPLOYEE ( SEMPLOYEEID, STRANS_ID, STRANTYPE, SEMPTPYE, SDRIVERNO , DBIRTHDATE, SPERSONELNO, DDRIVEBEGIN, DDRIVEEXPIRE, STEL, STEL2, SMAIL,  SMAIL2, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) 
//VALUES ( EID , '{8}', null, '{0}', '{10}' , TO_DATE('{3}','yyyy-mm-dd'), '{9}', TO_DATE('{11}','yyyy-mm-dd'), TO_DATE('{12}','yyyy-mm-dd'), '{4}', '{5}', '{6}',  '{7}', '1', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2'); COMMIT;";
                                    sExecute = @"EID := FC_GENID_EMPLOYEEID(); INSERT INTO TEMPLOYEE_SAP ( SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, DUPDATE, SUPDATE, PERS_CODE, CARRIER, LICENSE_NO, DRVSTATUS) 
VALUES ( EID , '{1}', '{2}', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2', '{9}', '{8}',  '{10}', '{13}' );INSERT INTO TEMPLOYEE ( SEMPLOYEEID, STRANS_ID, STRANTYPE, SEMPTPYE, SDRIVERNO , DBIRTHDATE, SPERSONELNO, DDRIVEBEGIN, DDRIVEEXPIRE, STEL, STEL2, SMAIL,  SMAIL2, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) 
VALUES ( EID , '{8}', null, '{0}', '{10}' ,{3}, '{9}', {11}, {12}, '{4}', '{5}', '{6}',  '{7}', '1', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2'); COMMIT;";

                                }
                                stbd.AppendFormat(sExecute, imp_vend.EMPTPYE, imp_vend.FNAME, imp_vend.LNAME, StringToSQL(imp_vend.BIRTHDATE, "D"), imp_vend.TEL, imp_vend.TEL2, imp_vend.MAIL, imp_vend.MAIL2, imp_vend.TRANS_ID
                                    , imp_vend.PERS_CODE, imp_vend.LICENSE_NO, StringToSQL(imp_vend.DRIVERNO_BEGIN, "D"), StringToSQL(imp_vend.DRIVERNO_Expire, "D"), imp_vend.DRVSTATUS);
                            }
                        }
                        Session["ss_DataGPSExcel"] = dt_DataGPSExcel;
                        dtVendor.Dispose();
                        if (stbd_Error.ToString() == "")
                        {
                            string sExec = stbd.ToString();
                            string CHK = "DECLARE  EID VARCHAR2(10); BEGIN " + sExec + " END;";
                            SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  EID VARCHAR2(10); BEGIN " + sExec + " END;");
                            CommonFunction.SetPopupOnLoad(xgvwCar, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
                        }
                        else
                        {
                            CommonFunction.SetPopupOnLoad(xgvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาตรวจสอบความถูกต้องของข้อมูลที่จะนำเข้า " + stbd_Error.ToString().Replace("^", "<br />") + "!');");

                            BindTruckWithDataFromExcel(xgvwCar, (DataTable)Session["ss_DataGPSExcel"], "STARTEDIT");//BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                        }
                    }
                }
                else
                {
                    ((ASPxButton)xgvwCar.FindFooterCellTemplateControl((GridViewColumn)xgvwCar.Columns["#"], "btnsubmit")).ClientVisible = false;
                }
                #endregion
                break;
            case "VENDOR":
                #region VENDOR
                if (Session["ss_GPSExcel"] == null)
                {
                    CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการที่การ Upload กรุณาทำการอัพโหลดใหม่อีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                }
                Session["ss_GPSExcel"] = null;//Clear Session first for safty
                if (Session["ss_DataGPSExcel"] != null)
                {
                    {
                        DataTable dt_DataGPSExcel = (DataTable)Session["ss_DataGPSExcel"];
                        if (dt_DataGPSExcel.Columns.Count > 0)
                        {
                            dt_DataGPSExcel.Columns.Add(new DataColumn("Status", typeof(string)));
                        }
                        if (dt_DataGPSExcel.Rows.Count > 0)
                        {

                            DataTable dt_DataMain = CommonFunction.GroupDataTable(dt_DataGPSExcel, "ชื่อบริษัท,ที่อยู่,ตำบล,อำเภอ,จังหวัด,รหัสไปรษณีย์,หมายเลขโทรศัพท์,วันที่_ลงทะเบียนคู่ค้า,ทุนจดทะเบียน_(บาท),อายุใบอนุญาตขนส่ง_ตั้งแต่วันที่,อายุใบอนุญาตขนส่ง_ถึงวันที่,อายุมาตรา12_ตั่งแต่วันที่,อายุมาตรา12_ถึงวันที่");

                            StringBuilder stbd = new StringBuilder();
                            StringBuilder stbd_Error = new StringBuilder();

                            foreach (DataRow dr_DataGPSExcel in dt_DataMain.Rows)
                            {
                                sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM TVENDOR_SAP WHERE  1=1 and SVENDORNAME= '" + (dr_DataGPSExcel["ชื่อบริษัท"] + "").Replace("&", "' || chr(38) || '") + "' ";
                                sdsVendor.SelectParameters.Clear();
                                sdsVendor.DataBind();
                                DataTable dtVendor = ((DataView)sdsVendor.Select(new DataSourceSelectArguments())).ToTable();

                                var data = dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["ชื่อบริษัท"] + "'").ToList().FirstOrDefault();
                                string SVENDORID = (data != null) ? (data.ItemArray.Length > 0 ? data.ItemArray[0] + "" : "") : "";
                                string sMissText = "";
                                bool HasID = (!string.IsNullOrEmpty(SVENDORID));
                                /*
        +		[0]	    {ชื่อบริษัท}	object {System.Data.DataColumn}
        +		[1]	    {ที่อยู่}	object {System.Data.DataColumn}
        +		[2]	    {ตำบล}	object {System.Data.DataColumn}
        +		[3]	    {อำเภอ}	object {System.Data.DataColumn}
        +		[4]	    {จังหวัด}	object {System.Data.DataColumn}
        +		[5]	    {วันที่ _ลงทะเบียนคู่ค้า}	object {System.Data.DataColumn}
        +		[6]	    {ทุนจดทะเบียน _(บาท)}	object {System.Data.DataColumn}
        +		[7]	    {อายุใบอนุญาตขนส่ง _ตั้งแต่วันที่}	object {System.Data.DataColumn}
        +		[8]	    {อายุใบอนุญาตขนส่ง _ถึงวันที่}	object {System.Data.DataColumn}
        +		[9]	    {อายุมาตรา 12 _ตั่งแต่วันที่}	object {System.Data.DataColumn}
        +		[10]	{อายุมาตรา 12 _ถึงวันที่}	object {System.Data.DataColumn}
        +		[11]	{ลำดับ}	object {System.Data.DataColumn}
        +		[12]	{ชื่อ - นามสกุล}	object {System.Data.DataColumn}
        +		[13]	{หมายเลขโทรศัพท์หลัก}	object {System.Data.DataColumn}
        +		[14]	{หมายเลขโทรศัพท์รอง}	object {System.Data.DataColumn}
        +		[15]	{e-mail}	object {System.Data.DataColumn} 
                                 */
                                if (HasID)
                                {
                                    sMissText += (dr_DataGPSExcel["ชื่อบริษัท"] + "" == "") ? ",ชื่อบริษัท" : "";
                                    sMissText += (dr_DataGPSExcel["ที่อยู่"] + "" == "") ? ",ที่อยู่" : "";
                                    sMissText += (dr_DataGPSExcel["ตำบล"] + "" == "") ? ",ตำบล" : "";
                                    sMissText += (dr_DataGPSExcel["อำเภอ"] + "" == "") ? ",อำเภอ" : "";
                                    sMissText += (dr_DataGPSExcel["จังหวัด"] + "" == "") ? ",จังหวัด" : "";
                                    //sMissText += (dr_DataGPSExcel["รหัสไปรษณีย์"] + "" == "") ? ",รหัสไปรษณีย์" : "";
                                    //sMissText += (dr_DataGPSExcel["หมายเลขโทรศัพท์"] + "" == "") ? ",หมายเลขโทรศัพท์" : "";
                                    //sMissText += (dr_DataGPSExcel["วันที่_ลงทะเบียนคู่ค้า"] + "" == "") ? ",วันที่ลงทะเบียนคู่ค้า" : "";
                                    //sMissText += (dr_DataGPSExcel["ทุนจดทะเบียน_(บาท)"] + "" == "") ? ",ทุนจดทะเบียน(บาท)" : "";
                                    //sMissText += (dr_DataGPSExcel["อายุใบอนุญาตขนส่ง_ตั้งแต่วันที่"] + "" == "") ? ",อายุใบอนุญาตขนส่งตั้งแต่วันที่" : "";
                                    //sMissText += (dr_DataGPSExcel["อายุใบอนุญาตขนส่ง_ถึงวันที่"] + "" == "") ? ",อายุใบอนุญาตขนส่งถึงวันที่" : "";
                                    sMissText += (dr_DataGPSExcel["อายุมาตรา 12_ตั่งแต่วันที่"] + "" == "") ? ",อายุมาตรา12ตั่งแต่วันที่" : "";
                                    //sMissText += (dr_DataGPSExcel["อายุมาตรา 12_ถึงวันที่"] + "" == "") ? ",อายุมาตรา12ถึงวันที่" : "";
                                    sMissText += (dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["ชื่อบริษัท"] + "'").Length == 0) ? ",ไม่พบ ชื่อบริษัท" : "";
                                }
                                /*
                                 sMissText += (dr_DataGPSExcel["ลำดับ"] + "" == "") ? ",ลำดับ" : "";
                                 sMissText += (dr_DataGPSExcel["ชื่อ - นามสกุล"] + "" == "") ? ",ชื่อ - นามสกุล" : "";
                                 sMissText += (dr_DataGPSExcel["หมายเลขโทรศัพท์หลัก"] + "" == "") ? ",หมายเลขโทรศัพท์หลัก" : "";
                                 sMissText += (dr_DataGPSExcel["หมายเลขโทรศัพท์รอง"] + "" == "") ? ",หมายเลขโทรศัพท์รอง" : "";
                                 sMissText += (dr_DataGPSExcel["e-mail"] + "" == "") ? ",e-mail" : "";
                                 */
                                int nRowIndex = dt_DataGPSExcel.Rows.IndexOf(dr_DataGPSExcel);
                                if (sMissText != "")
                                {
                                    stbd_Error.Append("^Line " + nRowIndex + ":" + sMissText);
                                    //dr_DataGPSExcel.BeginEdit();
                                    //dr_DataGPSExcel["Status"] = ">" + stbd_Error;
                                    //dr_DataGPSExcel.EndEdit();
                                }
                                else
                                {
                                    string sExecute = "";
                                    TBL_Transporter vend = new TBL_Transporter(Page, connection);
                                    vend.VENDORID = SVENDORID;
                                    vend.VENDORNAME = dr_DataGPSExcel["ชื่อบริษัท"] + "";
                                    vend.NO = dr_DataGPSExcel["ที่อยู่"] + "";
                                    vend.DISTRICT = dr_DataGPSExcel["ตำบล"] + "";
                                    vend.REGION = dr_DataGPSExcel["อำเภอ"] + "";
                                    vend.PROVINCE = dr_DataGPSExcel["จังหวัด"] + "";
                                    vend.PROVINCECODE = dr_DataGPSExcel["รหัสไปรษณีย์"] + "";
                                    vend.ACTIVE = "1"; vend.SAPStatus = "";
                                    vend.Regis_date = StringToSQL(dr_DataGPSExcel["วันที่_ลงทะเบียนคู่ค้า"] + "", "DV");
                                    vend.Capital = dr_DataGPSExcel["ทุนจดทะเบียน_(บาท)"] + "";
                                    vend.Cert_Begin = StringToSQL(dr_DataGPSExcel["อายุใบอนุญาตขนส่ง_ตั้งแต่วันที่"] + "", "DV");
                                    vend.Cert_End = StringToSQL(dr_DataGPSExcel["อายุใบอนุญาตขนส่ง_ถึงวันที่"] + "", "DV");
                                    vend.Statute_Begin = StringToSQL(dr_DataGPSExcel["อายุมาตรา 12_ตั่งแต่วันที่"] + "", "DV");
                                    vend.Statute_End = StringToSQL(dr_DataGPSExcel["อายุมาตรา 12_ถึงวันที่"] + "", "DV");
                                    if (HasID)
                                    {

                                        sExecute = @"UPDATE TVENDOR_SAP
SET    SVENDORNAME   = '{1}',
       SNO           = '{2}',
       SDISTRICT     = '{3}',
       SREGION       = '{4}',
       SPROVINCE     = '{5}',
       SPROVINCECODE = '{6}',
       CACTIVE       = '{7}', 
       DUPDATE       = sysdate,
       SUPDATE       = 'IMP_TMS',
       CSAP          = '{8}'
WHERE  SVENDORID     = '{0}';UPDATE TVENDOR
SET    SABBREVIATION    = '{1}',
       STEL             = '{9}',
       SFAX             = '{10}',  
       DBEGINTRANSPORT  = TO_DATE('{11}','dd/MM/yyyy'),
       DEXPIRETRANSPORT = TO_DATE('{12}','dd/MM/yyyy'),
       DBEGIN13BIS      = TO_DATE('{13}','dd/MM/yyyy') ,
       DEXPIRE13BIS     = TO_DATE('{14}','dd/MM/yyyy') ,
       CACTIVE          = '{15}', 
       SUPDATE          = 'IMP_TMS',
       DUPDATE          = sysdate
WHERE  SVENDORID        = '{0}';";
                                    }
                                    else
                                    {

                                        sExecute = @"VID  := LPAD( FC_GENID_VENDORID(),10,'0');INSERT INTO TVENDOR_SAP (SVENDORID, SVENDORNAME, SNO, SDISTRICT, SREGION, SPROVINCE, SPROVINCECODE, CACTIVE, DCREATE, SCREATE,  CSAP) 
VALUES (  VID ,  '{1}' ,  '{2}' ,  '{3}' ,  '{4}' ,  '{5}' , '{6}' ,  '{7}' ,  sysdate ,  'IMP_TMS' ,  '{8}'  );INSERT INTO TVENDOR (SVENDORID, SABBREVIATION, STEL, SFAX,  DBEGINTRANSPORT, DEXPIRETRANSPORT,/*DSTARTPTT, NCAPITAL,*/ DBEGIN13BIS, DEXPIRE13BIS, CACTIVE, DCREATE, SCREATE ) 
VALUES (  VID , '{1}' , '{9}' , '{10}' ,  TO_DATE('{11}','dd/MM/yyyy') ,  TO_DATE('{12}','dd/MM/yyyy') ,TO_DATE('{13}','dd/MM/yyyy') , TO_DATE('{14}','dd/MM/yyyy')  ,  '{15}' , sysdate, 'IMP_TMS'  );";


                                    }
                                    stbd.AppendFormat(sExecute, vend.VENDORID, vend.VENDORNAME, vend.NO, vend.DISTRICT, vend.REGION, vend.PROVINCE
                                        , vend.PROVINCECODE, vend.ACTIVE, vend.SAPStatus, vend.PHONE, vend.PHONE2, CheckDate(vend.Cert_Begin, "V")
                                        , CheckDate(vend.Cert_End, "V"), CheckDate(vend.Statute_Begin, "V"), CheckDate(vend.Statute_End, "V"), vend.ACTIVE
                                         );
                                    string SIGNER = "";
                                    int idx = 0;
                                    foreach (DataRow _dr in dt_DataGPSExcel.Select("ชื่อบริษัท='" + vend.VENDORNAME + "'"))
                                    {
                                        idx++;
                                        if (_dr["ชื่อ - นามสกุล"] + "" != "")
                                        {
                                            SIGNER += string.Format(@"INSERT INTO TVENDOR_SIGNER ( SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2,EMAIL, PICTURE, IS_ACTIVE, DATE_CREATED, USER_CREATED ) 
VALUES ( {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}',  '{7}',  '{8}', sysdate, 'IMP_TMS');", (HasID ? "'" + vend.VENDORID + "'" : "VID"), idx, _dr["ชื่อ - นามสกุล"] + "", _dr["ตำแหน่ง"] + "", _dr["หมายเลขโทรศัพท์หลัก"] + "", _dr["หมายเลขโทรศัพท์รอง"] + "", _dr["e-mail"] + "", "", "");
                                            //, vend.LINE_NO, vend.NAME, vend.POSITION, vend.PHONE, vend.PHONE2, vend.EMAIL, "", vend.IS_ACTIVE
                                        }

                                    }
                                    if (SIGNER != "")
                                    {//
                                        stbd.Append(@"DELETE TVENDOR_SIGNER WHERE SVENDORID =  " + (HasID ? "'" + vend.VENDORID + "'" : "VID") + @";" + SIGNER);
                                    }

                                }
                                stbd.Append("COMMIT;");
                            }
                            Session["ss_DataGPSExcel"] = dt_DataGPSExcel;
                            // dtVendor.Dispose();
                            if (stbd_Error.ToString() == "")
                            {
                                string sExec = stbd.ToString().Replace("&", "'|| chr(38) ||'");
                                // string System = "DECLARE  VID VARCHAR2(10); BEGIN " + sExec + " END;";
                                SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  VID VARCHAR2(10); BEGIN " + sExec + " END;");
                                CommonFunction.SetPopupOnLoad(xgvw, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
                            }
                            else
                            {
                                CommonFunction.SetPopupOnLoad(xgvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาตรวจสอบความถูกต้องของข้อมูลที่จะนำเข้า " + stbd_Error.ToString().Replace("^", "<br />") + "!');");

                                BindTruckWithDataFromExcel(xgvwCar, (DataTable)Session["ss_DataGPSExcel"], "STARTEDIT");//BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                            }
                        }
                    }
                }

                #endregion
                break;
            case "CONTRACT":
                #region CONTRACT
                if (Session["ss_GPSExcel"] == null)
                {
                    CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการที่การ Upload กรุณาทำการอัพโหลดใหม่อีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                }
                Session["ss_GPSExcel"] = null;//Clear Session first for safty
                if (Session["ss_DataGPSExcel"] != null)
                {
                    DataTable dt_DataGPSExcel = (DataTable)Session["ss_DataGPSExcel"];
                    if (dt_DataGPSExcel.Columns.Count > 0)
                    {
                        dt_DataGPSExcel.Columns.Add(new DataColumn("Status", typeof(string)));
                    }
                    if (dt_DataGPSExcel.Rows.Count > 0)
                    {

                        //ประเภทการจัดจ้าง,เลขที่สัญญา,ผู้ขนส่ง,รูปแบบการขนส่ง,ประเภทสัญญา,จำนวนรถในสัญญา,วันเริ่มต้นสัญญา,วันสิ้นสุดสัญญา,รหัสคลังต้นทางหลัก,รหัสคลังต้นทางอื่นๆ,กลุ่มผลิตภัณฑ์
                        DataTable dt_DataMain = CommonFunction.GroupDataTable(dt_DataGPSExcel, "เลขที่สัญญา,ผู้ขนส่ง");

                        StringBuilder stbd = new StringBuilder();
                        StringBuilder stbd_Error = new StringBuilder();


                        string NotExists = CheckTransporter(dt_DataMain, "");
                        //if (NotExists != "")
                        if (false)
                        {
                            CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ไม่พบรายชื่อผู้ขนส่ง ดังนี้<br />" + NotExists.Remove(0, 1).Replace(",", "<br>-") + "')"); return;
                        }
                        else
                        {
                            string sExecCont = "", sExecCont_Trck = "", sMsgError = "";
                            int idx = 0;
                            foreach (DataRow dr_DataMain in dt_DataMain.Select("เลขที่สัญญา <>''"))
                            {
                                string sMissText = "";

                                #region Set Data
                                /* ประเภทการจัดจ้าง	
                             * เลขที่สัญญา	
                             * ผู้ขนส่ง	
                             * รูปแบบการขนส่ง	
                             * ประเภทสัญญา	
                             * จำนวนรถในสัญญา	
                             * วันเริ่มต้นสัญญา	
                             * วันสิ้นสุดสัญญา	
                             * รหัสคลังต้นทางหลัก	
                             * รหัสคลังต้นทางอื่นๆ	
                             * กลุ่มผลิตภัณฑ์	
                             * 
                             * ทะบียน(หัว)	
                             * ทะบียน(หาง)	
                             * ประเภท
                             */
                                DataRow[] dr_DataCont = dt_DataGPSExcel.Select("เลขที่สัญญา='" + dr_DataMain["เลขที่สัญญา"] + "'");
                                DataRow dr_GrpDataCont = dr_DataCont[0];
                                string
                                IN_CONTRACTID = "",
                                IP_SCONTRACTTYPEID = "" + dr_GrpDataCont["ประเภทสัญญา"],
                                IP_SCONTRACTNO = "" + dr_GrpDataCont["เลขที่สัญญา"],
  IP_SVENDORID = (dr_GrpDataCont["Carrier Code"] + "").PadLeft(10, '0'),//CommonFunction.Get_Value(connection, "SELECT SVENDORID FROM TVENDOR WHERE SABBREVIATION= '" + dr_GrpDataCont["ผู้ขนส่ง"] + "'"),
  IP_DBEGIN = FunctionUtilities.ConvertToDateTime("" + dr_GrpDataCont["วันเริ่มต้นสัญญา"], "yyyy-MM-dd", "en-US", true),
  IP_DEND = FunctionUtilities.ConvertToDateTime("" + dr_GrpDataCont["วันสิ้นสุดสัญญา"], "yyyy-MM-dd", "en-US", true),
  IP_SDETAIL = "",
  IP_NTRUCK = "" + dr_GrpDataCont["จำนวนรถในสัญญา"],
  IP_SREMARK = "",
  IP_CACTIVE = "Y",
  IP_DCREATE = "",
  IP_SCREATE = "",
  IP_DUPDATE = "",
  IP_SUPDATE = "",
  IP_CGROUPCONTRACT = "" + dr_GrpDataCont["รูปแบบการขนส่ง"],
  IP_CSPACIALCONTRAC = (("" + dr_GrpDataCont["ประเภทการจัดจ้าง"] == "สัญญา") ? "N" : "Y"),
  IP_STERMINALID = "" + dr_GrpDataCont["รหัสคลังต้นทางหลัก"],
  IP_COIL = "Y",
  IP_CGAS = "N",
  IP_SCONT_PLNT = "" + dr_GrpDataCont["รหัสคลังต้นทางอื่นๆ"],
  IP_RENEWFROM = "",
  IP_GPRODUCT = "" + dr_GrpDataCont["กลุ่มผลิตภัณฑ์"];
                                #endregion
                                #region Check Contract Data
                                sMissText += (IP_SVENDORID == "") ? ",ไม่พบผู้ขนส่งในระบบ" : "";
                                sMissText += (IP_SCONTRACTNO == "") ? ",ระบุเลขที่สัญญา" : "";
                                sMissText += (IP_DBEGIN == "") ? ",ระบุวันเริ่มต้นสัญญา" : "";
                                sMissText += (IP_DEND == "") ? ",ระบุวันสิ้นสุดสัญญา" : "";
                                sMissText += (IP_NTRUCK == "") ? ",ระบุจำนวนรถในสัญญา" : "";
                                sMissText += (IP_CGROUPCONTRACT == "") ? ",ระบุรูปแบบการขนส่ง" : "";
                                sMissText += (IP_CSPACIALCONTRAC == "") ? ",ประเภทการจัดจ้าง" : "";
                                sMissText += (IP_STERMINALID == "") ? ",ระบุรหัสคลังต้นทางหลัก" : "";
                                // sMissText += (IP_SCONT_PLNT == "") ? ",รหัสคลังต้นทางอื่นๆ" : "";
                                sMissText += (IP_GPRODUCT == "") ? ",กลุ่มผลิตภัณฑ์" : "";
                                #endregion
                                string CONT_ID = CommonFunction.Get_Value(connection, "SELECT SCONTRACTID FROM TCONTRACT WHERE SCONTRACTNO='" + dr_GrpDataCont["เลขที่สัญญา"] + "'");
                                if (sMissText == "")
                                {
                                    //DataTable dt_GenID = CommonFunction.Get_Data(sql, "SELECT FC_GENID_TCONTRACT('') as CID FROM DUAL");
                                    string SetID = "";
                                    //if (dt_GenID.Rows.Count > 0)
                                    //{
                                    SetID = "CID";
                                    //}

                                    #region Contract&PrdGroup
                                    sExecCont += string.Format(@"" + ((CONT_ID == "") ? "" : "DELETE TCONTRACT_TRUCK WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @";DELETE TCONTRACT_GPRODUCT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @";DELETE TCONTRACT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @";") + @"CID := FC_GENID_TCONTRACT('');INSERT INTO TCONTRACT (
   SCONTRACTID, SCONTRACTTYPEID, SCONTRACTNO, 
   SVENDORID, DBEGIN, DEND, 
   SDETAIL, NTRUCK, SREMARK, 
   CACTIVE, DCREATE, SCREATE, 
   DUPDATE, SUPDATE, CGROUPCONTRACT, 
   CSPACIALCONTRAC, STERMINALID, COIL, 
   CGAS, SCONT_PLNT, RENEWFROM ) 
VALUES (  " + ((CONT_ID == "") ? SetID : CONT_ID) + @" , '{1}' , '{2}' ,
  '{3}' ,  TO_DATE('{4}','yyyy-MM-dd') ,  TO_DATE('{5}','yyyy-MM-dd') ,
   '', '{6}' , '{7}' ,
   '{8}' ,  sysdate ,  'IMP_TMS' ,
    sysdate , 'IMP_TMS' , '{9}' ,
   '{10}', '{11}' , '{12}' ,
   '{13}', '{14}' , '{15}' );" + ((IP_GPRODUCT != "") ? @"INSERT INTO TCONTRACT_GPRODUCT(SCONTRACTID,SPRODUCTTYPEID,SPRODUCTTYPENAME,DCREATE,SCREATE,DUPDATE,SUPDATE,CACTIVE) SELECT " + ((CONT_ID == "") ? SetID : CONT_ID) + @" ,SPRODUCTTYPEID,SPRODUCTTYPENAME,SYSDATE,'IMP_TMS',SYSDATE,'IMP_TMS','1' FROM TPRODUCTTYPE WHERE SPRODUCTTYPEID IN('" + IP_GPRODUCT.Replace(",", "','") + "');" : "") + @"", "", "" + IP_SCONTRACTTYPEID, IP_SCONTRACTNO, IP_SVENDORID, IP_DBEGIN, IP_DEND, IP_NTRUCK, IP_SREMARK, IP_CACTIVE, IP_CGROUPCONTRACT, IP_CSPACIALCONTRAC
, IP_STERMINALID, IP_COIL, IP_CGAS, IP_SCONT_PLNT, IP_RENEWFROM);
                                    #endregion

                                    foreach (DataRow dr_DataTruck in dt_DataGPSExcel.Select("เลขที่สัญญา='" + IP_SCONTRACTNO + "'"))
                                    {
                                        string
  IP_STRUCKID = CommonFunction.Get_Value(connection, "SELECT STRUCKID FROM TTRUCK WHERE SHEADREGISTERNO='" + dr_DataTruck["ทะบียน(หัว)"] + "' AND SCARTYPEID IN('0','3')"),
  IP_STRAILERID = CommonFunction.Get_Value(connection, "SELECT STRUCKID FROM TTRUCK WHERE SHEADREGISTERNO='" + dr_DataTruck["ทะบียน(หาง)"] + "' AND SCARTYPEID IN('4')"),
  IP_CSTANDBY = "" + dr_DataTruck["ประเภท"];
                                        #region CONTRACT TRUCK
                                        sExecCont_Trck = "";
                                        #region Check Contract Data
                                        sMissText += (IP_STRUCKID == "") ? ",ไม่พบทะบียน(หัว)ในระบบ" : "";
                                        //sMissText += (IP_STRAILERID == "") ? ",ไม่พบทะบียน(หาง)ในระบบ" : "";
                                        sMissText += (IP_CSTANDBY == "") ? ",ระบุประเภท" : "";
                                        #endregion
                                        sExecCont_Trck += string.Format(@"INSERT INTO TCONTRACT_TRUCK ( SCONTRACTID,  STRUCKID, STRAILERID, CSTANDBY, DCREATE, SCREATE, DUPDATE, SUPDATE ) 
VALUES ( " + ((CONT_ID == "") ? SetID : CONT_ID) + @" , '{1}' , '{2}' , '{3}' , sysdate , 'IMP_TMS' , sysdate , 'IMP_TMS');", "", IP_STRUCKID, IP_STRAILERID, IP_CSTANDBY);
                                        sExecCont += sExecCont_Trck + "";
                                        #endregion
                                    }



                                    #region รหัสคลังต้นทางหลัก
                                    sExecCont += "DELETE TCONTRACT_PLANT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @";";
                                    if (!string.IsNullOrEmpty(IP_STERMINALID.Trim()))
                                    {
//                                        string MAINPNT = @"DELETE TCONTRACT_PLANT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @" AND CMAIN_PLANT = '1';INSERT INTO TCONTRACT_PLANT (SCONTRACTID, STERMINALID, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, CMAIN_PLANT) 
//VALUES ( " + ((CONT_ID == "") ? SetID : CONT_ID) + @",'" + IP_STERMINALID + "','1',SYSDATE,'IMP_TMS', SYSDATE,'IMP_TMS','1');";
                                        string MAINPNT = @"INSERT INTO TCONTRACT_PLANT (SCONTRACTID, STERMINALID, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, CMAIN_PLANT) 
VALUES ( " + ((CONT_ID == "") ? SetID : CONT_ID) + @",'" + IP_STERMINALID + "','1',SYSDATE,'IMP_TMS', SYSDATE,'IMP_TMS','1');";
                                        sExecCont += MAINPNT;
                                    }
                                    else
                                    {
                                       // sExecCont += "DELETE TCONTRACT_PLANT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @";";
                                    }
                                    #endregion

                                    #region  รหัสคลังต้นทางอื่นๆ
                                    if (!string.IsNullOrEmpty(IP_SCONT_PLNT.Trim()))
                                    {
                                        string OTHERPNT = "";
                                        //OTHERPNT += @"DELETE TCONTRACT_PLANT WHERE SCONTRACTID =  " + ((CONT_ID == "") ? SetID : CONT_ID) + @" AND CMAIN_PLANT = '0';";
                                        string[] arrayPNT = IP_SCONT_PLNT.Split(',');
                                        for (int i = 0; i < arrayPNT.Length; i++)
                                        {
                                            string MAINPNT = @"INSERT INTO TCONTRACT_PLANT (SCONTRACTID, STERMINALID, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE, CMAIN_PLANT) 
VALUES ( " + ((CONT_ID == "") ? SetID : CONT_ID) + @",'" + arrayPNT[i].Trim() + "','1',SYSDATE,'IMP_TMS', SYSDATE,'IMP_TMS','0');";
                                            OTHERPNT += MAINPNT;
                                        }
                                        sExecCont += OTHERPNT;
                                    }

                                    #endregion


                                    sExecCont += "COMMIT;";

                                }
                                if (sMissText != "")
                                {
                                    sMsgError = "<li>" + (idx + 1) + "เลขที่สัญญา " + IP_SCONTRACTNO + sMissText.Remove(0, 1).Replace(",", "<br>-");
                                }
                            }
                            if (sMsgError == "")
                            {
                                string sExec = "DECLARE  CID NUMBER; BEGIN " + sExecCont + " END;";
                                SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  CID NUMBER; BEGIN " + sExec + " END;");
                                CommonFunction.SetPopupOnLoad(xgvwCar, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
                            }
                            else
                            {
                                //CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ข้อมูลไม่เพียงพอต่อการนำเข้า กรุณาตรวจสอบ ข้อมูลดังนี้<br />" + NotExists.Remove(0, 1).Replace(",", "<br>-") + "')"); return;
                                CommonFunction.SetPopupOnLoad(xgvw, @"dxWarning('แจ้งเตือนจากระบบ','ข้อมูลไม่เพียงพอต่อการนำเข้า กรุณาตรวจสอบ ข้อมูลดังนี้<br />" + sMsgError + "')"); return;

                            }

                        }
                    }
                }
                #endregion
                break;
            case "TRUCK":
                #region TRUCK
                if (Session["ss_GPSExcel"] == null)
                {
                    CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ระบบไม่พบรายการที่การ Upload กรุณาทำการอัพโหลดใหม่อีกครั้ง! <hr /><font size=""1"" >ข้อแนะนำควรทำการรายการหลังทำการอัพโหลดให้เสร็จสิ้นถายใน 30 นาที</font>')");
                }
                Session["ss_GPSExcel"] = null;//Clear Session first for safty
                if (Session["ss_DataGPSExcel"] != null)
                {
                    DataTable dt_DataGPSExcel = (DataTable)Session["ss_DataGPSExcel"];
                    if (dt_DataGPSExcel.Columns.Count > 0)
                    {
                        dt_DataGPSExcel.Columns.Add(new DataColumn("Status", typeof(string)));
                    }
                    if (dt_DataGPSExcel.Rows.Count > 0)
                    {
                        //ประเภทการจัดจ้าง,เลขที่สัญญา,ผู้ขนส่ง,รูปแบบการขนส่ง,ประเภทสัญญา,จำนวนรถในสัญญา,วันเริ่มต้นสัญญา,วันสิ้นสุดสัญญา,รหัสคลังต้นทางหลัก,รหัสคลังต้นทางอื่นๆ,กลุ่มผลิตภัณฑ์
                        DataTable dt_DataMain = dt_DataGPSExcel;

                        string INSERT_TTRUCK = @"INSERT INTO TTRUCK (STRUCKID, SHEADREGISTERNO, SCARTYPEID,  NSLOT, SHEADID, STRAILERID, STRAILERREGISTERNO, STRANSPORTID, STRANSPORTTYPE, SCAR_NUM, SENGINE, SCHASIS, 
SOWNER_NAME,SBRAND,  SMODEL,NWHEELS,NTOTALCAPACITY,NWEIGHT,NLOAD_WEIGHT,SVIBRATION,STANK_MATERAIL,STANK_MAKER,SLOADING_METHOD,DREGISTER,SPROD_GRP,CACTIVE,SREMARK,DPREV_SERV,DNEXT_SERV,
SPREV_REQ_ID,SLAST_REQ_ID,DWATEREXPIRE,DSIGNIN,NSHAFTDRIVEN,POWERMOVER,PUMPPOWER,PUMPPOWER_TYPE,MATERIALOFPRESSURE,VALVETYPE,FUELTYPE,GPS_SERVICE_PROVIDER,PLACE_WATER_MEASURE,
TRUCK_CATEGORY,VOL_UOM,VEH_TEXT,CARCATE_ID, DCREATE, SCREATE,CLASSGRP)
VALUES({0},{1},{2},{3}, {4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},
{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45}, sysdate , 'IMP_TMS',{46});";

                        string UPDATE_TTRUCK = @"UPDATE TTRUCK
SET SHEADREGISTERNO         = {1},
       SCARTYPEID           = {2},
       NSLOT                = {3},
       SHEADID              = {4},
       STRAILERID           = {5},
       STRAILERREGISTERNO   = {6},
       STRANSPORTID         = {7},
       STRANSPORTTYPE       = {8},
       SCAR_NUM             = {9},
       SENGINE              = {10},
       SCHASIS              = {11},
       SOWNER_NAME          = {12},
       SBRAND               = {13},
       SMODEL               = {14},
       NWHEELS              = {15},
       NTOTALCAPACITY       = {16},
       NWEIGHT              = {17},
       NLOAD_WEIGHT         = {18},
       SVIBRATION           = {19},
       STANK_MATERAIL       = {20},
       STANK_MAKER          = {21},
       SLOADING_METHOD      = {22},
       DREGISTER            = {23},
       SPROD_GRP            = {24},
       CACTIVE              = {25},
       SREMARK              = {26},
       DPREV_SERV           = {27},
       DNEXT_SERV           = {28},
       SPREV_REQ_ID         = {29},
       SLAST_REQ_ID         = {30},
       DWATEREXPIRE         = {31},
       DSIGNIN              = {32},
       NSHAFTDRIVEN         = {33},
       POWERMOVER           = {34},
       PUMPPOWER            = {35},
       PUMPPOWER_TYPE       = {36},
       MATERIALOFPRESSURE   = {37},
       VALVETYPE            = {38},
       FUELTYPE             = {39},
       GPS_SERVICE_PROVIDER = {40},
       PLACE_WATER_MEASURE  = {41},
       TRUCK_CATEGORY       = {42},
       VOL_UOM              = {43},
       VEH_TEXT             = {44},
       CARCATE_ID           = {45},
       CLASSGRP             = {46},
       DUPDATE              = SYSDATE ,
       SUPDATE              = 'IMP_TMS'
WHERE  STRUCKID             = {0};";



                        string[] sColumnName = new string[] { "รหัสหัว", "ทะเบียนหัว", "ประเภทรถ", "จำนวนช่อง", "รหัสหัว", "รหัสหาง", "ทะเบียนหาง", "รหัสผู้ขนส่ง", "ชื่อผู้ขนส่ง", "ประเภทขนส่ง"
                            , "หมายเลขข้างรถ", "หมายเลขเครื่องยนต์", "หมายเลขแชชซีย์", "ผู้ถือกรรมสิทธิ์ (ตามหลักฐานการจดทะเบียน)", "ยี่ห้อ", "รุ่น", "จำนวนล้อ", "ความจุรวม", "นํ้าหนักรถ(นร.)", "น้ำหนักรถรวมผลิตภัณฑ์(นบ.)"
                            , "ระบบกันสะเทือน", "วัสดุที่ใช้ทำตัวถังบรรทุก", "บริษัทผู้ผลิตตัวถัง", "วิธีการเติมน้ำมัน", "วันที่จดทะเบียนครั้งแรก", "ผลิตภัณฑ์หลักที่บรรทุก", "สถานะการใช้งาน", "หมายเหตุ" , "วันที่วัดน้ำครั้งก่อน"
                            , "วันที่วัดน้ำครั้งต่อไป", "เลขที่วัดน้ำครั้งก่อน", "เลขที่วัดน้ำครั้งล่าสุด", "วันหมดอายุวัดน้ำ", "วันที่ขึ้นทะเบียนกับปตท", "จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก", "กำลังเครื่องยนต์"
                            , "ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)", "ชนิดของปั๊มPower", "วัสดุที่สร้าง Pressure ของปั๊ม Power", "ชนิดของวาล์วใต้ท้อง", "ประเภทเชื้อเพลิง", "บริษัทที่ให้บริการระบบGPS", "สถานที่วัดน้ำครั้งล่าสุด"
                            ,"Vehicle type","หน่วยบรรทุก","Vehicle header text","Vehicle Group" };

                        string[] RequireColumn = new string[] { "SHEADREGISTERNO", "SCARTYPEID", "NSLOT", "STRAILERREGISTERNO", "STRANSPORTER", "SCAR_NUM", "SENGINE", "SCHASIS"
                            ,"SOWNER_NAME", "SBRAND" , "SMODEL", "NWHEELS", "NTOTALCAPACITY", "NWEIGHT", "NLOAD_WEIGHT", "DREGISTER", "DWATEREXPIRE", "DSIGNIN", "NSHAFTDRIVEN"
                            , "POWERMOVER","TRUCK_CATEGORY", "VOL_UOM", "VEH_TEXT", "CARCATE_ID"};

                        //หัวไม่ต้องมี
                        string[] RequireTruckHead = new string[] { "NSLOT", "SCAR_NUM", "NTOTALCAPACITY", "NLOAD_WEIGHT", "DWATEREXPIRE" };

                        string MSG_ERROR = "";
                        string MSG_REQUIRE = "";
                        string MSG_VEN = "";
                        if (dt_DataMain.Rows.Count > 0)
                        {


                            for (int i = 0; i < sColumnName.Count(); i++)
                            {
                                string COLUMN = (sColumnName[i] + "").Replace(" ", "").ToLower();
                                string FCOLUMN = (dt_DataMain.Rows[0][i] + "").Replace("\n", "").Replace(" ", "").ToLower();
                                if (COLUMN != FCOLUMN)
                                {

                                    MSG_ERROR += "  " + FCOLUMN + "<br>";
                                }
                            }

                            if (!string.IsNullOrEmpty(MSG_ERROR))
                            {
                                CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','รูปแบบคอลัมน์ไม่ตรงกับระบบ<br>" + MSG_ERROR + "')");
                                return;
                            }


                            string TRUCK_ID = "";
                            string QUERY = "";

                            for (int i = 2; i < dt_DataMain.Rows.Count; i++)
                            {


                                string QUERY_HEAD = "";
                                string QUERY_TAIL = "";
                                string UPDATEHEADTAIL = "";
                                string DELETE_COMPART = "";
                                string ADD_COMPART = "";


                                string CARTYPE = dt_DataMain.Rows[i][2] + "";
                                TRUCK_ID += "," + "'" + dt_DataMain.Rows[i][1] + "'";
                                #region Check ข้อมูล

                                for (int j = 0; j < 46; j++)
                                {

                                    if (RequireColumn.Contains(dt_DataMain.Rows[1][j] + ""))
                                    {

                                        if (!string.IsNullOrEmpty(dt_DataMain.Rows[i][j] + ""))
                                        {
                                            string value = dt_DataMain.Rows[i][j] + "";
                                        }
                                        else
                                        {
                                            //เช็คว่าถ้าเป็นรถสิบล้อไม่ต้องมีหางก้ได้
                                            if (CARTYPE == "0" && dt_DataMain.Rows[1][j] + "" == "STRAILERREGISTERNO")
                                            {

                                            }
                                            else
                                            {
                                                //เช็คว่าถ้าเป็นรถสิบล้อไม่ต้องมีหางก้ได้
                                                if (CARTYPE == "3" && RequireTruckHead.Contains(dt_DataMain.Rows[1][j] + ""))
                                                {

                                                }
                                                else
                                                {
                                                    MSG_REQUIRE += NameRequire(dt_DataMain.Rows[1][j] + "");
                                                }
                                            }
                                        }
                                    }
                                }
                                //ข้อมูล Required
                                if (!string.IsNullOrEmpty(MSG_REQUIRE))
                                {
                                    CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ข้อมูลไม่เพียงพอต่อการนำเข้า กรุณาตรวจสอบ ข้อมูลดังนี้<br />" + MSG_REQUIRE + "')");
                                    return;
                                }
                                #endregion

                                //ข้อมูล Required
                                #region เช็คจากชื่อบริษัท
                                //string SVENDORID = "";
                                //if (!string.IsNullOrEmpty(dt_DataMain.Rows[i][8] + ""))
                                //{
                                //    DataTable dtVen = CommonFunction.Get_Data(sql, "SELECT SVENDORID FROM TVENDOR WHERE SABBREVIATION LIKE '%" + dt_DataMain.Rows[i][8] + "%'");
                                //    if (dtVen.Rows.Count > 0)
                                //    {
                                //        SVENDORID = dtVen.Rows[0]["SVENDORID"] + "";
                                //    }
                                //    else
                                //    {
                                //        MSG_VEN = dt_DataMain.Rows[i][8] + "";
                                //        CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ไม่พบข้อมูลบริษัท  " + MSG_VEN + " ในระบบ<br>กรุณาตรวจสอบอีกครั้ง')"); return;
                                //        return;
                                //    }
                                //}
                                #endregion

                                #region เช็คจากไอดีบริษัท
                                string SVENDORID = "";
                                if (!string.IsNullOrEmpty(dt_DataMain.Rows[i][7] + ""))
                                {
                                    string VEN_S = !string.IsNullOrEmpty(dt_DataMain.Rows[i][7] + "") ? (dt_DataMain.Rows[i][7] + "").Trim().PadLeft(10, '0') : "";
                                    DataTable dtVen = CommonFunction.Get_Data(sql, "SELECT SVENDORID FROM TVENDOR WHERE SVENDORID = '" + VEN_S + "'");
                                    if (dtVen.Rows.Count > 0)
                                    {
                                        SVENDORID = dtVen.Rows[0]["SVENDORID"] + "";
                                    }
                                    else
                                    {
                                        MSG_VEN = dt_DataMain.Rows[i][7] + "";
                                        CommonFunction.SetPopupOnLoad(xgvwCar, @"dxWarning('แจ้งเตือนจากระบบ','ไม่พบรหัสบริษัทนี้ " + MSG_VEN + " ในระบบ<br>กรุณาตรวจสอบอีกครั้ง')");
                                        return;
                                    }
                                }
                                #endregion

                                string STRUCKID = "";
                                string SHEADID = "";
                                string STRAILERID = "";
                                string SHEADREGISTERNO = "";

                                //กรณีรถ 10 ล้อ
                                if (CARTYPE == "0")
                                {
                                    SHEADREGISTERNO = dt_DataMain.Rows[i][1] + "";
                                    DataTable dt_Truck = CommonFunction.Get_Data(sql, "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + SHEADREGISTERNO + "" + "%'");
                                    //เช็คว่ามีหัวแล้วหรือยังถ้ามีอัพเดท ไม่มีให้ Insert
                                    if (dt_Truck.Rows.Count > 0)
                                    {
                                        STRUCKID = dt_Truck.Rows[0]["STRUCKID"] + "";
                                        #region UpDate
                                        QUERY_HEAD = string.Format(UPDATE_TTRUCK,
                                           StringToSQL(STRUCKID, "S"),/*รหัสหัว*/
                                           StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหัว*/
                                           StringToSQL(dt_DataMain.Rows[i][2] + "", "N"),/*ประเภทรถ*/
                                           StringToSQL(dt_DataMain.Rows[i][3] + "", "N"),/*จำนวนช่อง*/
                                           (CARTYPE == "4" ? StringToSQL(SHEADID, "S") : "null"),/*รหัสหัว*/
                                           (CARTYPE == "3" ? StringToSQL(STRAILERID, "S") : "null"),/*รหัสหาง*/
                                           StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหาง*/
                                           StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                           StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                           StringToSQL(dt_DataMain.Rows[i][10] + "", "S"),/*หมายเลขข้างรถ*/
                                           StringToSQL(dt_DataMain.Rows[i][11] + "", "S"),/*หมายเลขเครื่องยนต์*/
                                           StringToSQL(dt_DataMain.Rows[i][12] + "", "S"),/*หมายเลขแชชซีย์*/
                                           StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                           StringToSQL(dt_DataMain.Rows[i][14] + "", "S"),/*ยี่ห้อ*/
                                           StringToSQL(dt_DataMain.Rows[i][15] + "", "S"),/*รุ่น*/
                                           StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                           StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                           StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                           StringToSQL(dt_DataMain.Rows[i][19] + "", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                           StringToSQL(dt_DataMain.Rows[i][20] + "", "S"),/*ระบบกันสะเทือน*/
                                           StringToSQL(dt_DataMain.Rows[i][21] + "", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                           StringToSQL(dt_DataMain.Rows[i][22] + "", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                           StringToSQL(dt_DataMain.Rows[i][23] + "", "S"),/*วิธีการเติมน้ำมัน*/
                                           StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                           StringToSQL(dt_DataMain.Rows[i][25] + "", "S"),/*ผลิตภัณฑ์หลัก*/
                                           StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                           StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                           StringToSQL(dt_DataMain.Rows[i][28] + "", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                           StringToSQL(dt_DataMain.Rows[i][29] + "", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                           StringToSQL(dt_DataMain.Rows[i][30] + "", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                           StringToSQL(dt_DataMain.Rows[i][31] + "", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                           StringToSQL(dt_DataMain.Rows[i][32] + "", "D"),/*วันหมดอายุวัดน้ำ*/
                                           StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                           StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                           StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                           StringToSQL(dt_DataMain.Rows[i][36] + "", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                           StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                           StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                           StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                           StringToSQL(dt_DataMain.Rows[i][40] + "", "S"),/*ประเภทเชื้อเพลิง*/
                                           StringToSQL(dt_DataMain.Rows[i][41] + "", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                           StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                           StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                           StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                           StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                           StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                           StringToSQL(dt_DataMain.Rows[i][57] + "", "S"),/*ClassGroup*/
                                           StringToSQL(dt_DataMain.Rows[i][58] + "", "S")/*หมายเลขแชชซีย์หาง*/);
                                        #endregion
                                    }
                                    else
                                    {
                                        #region InSert
                                        QUERY_HEAD += "TID := FC_GENID_TTRUCK(0);";
                                        QUERY_HEAD += string.Format(INSERT_TTRUCK,
                                            "TID",/*รหัสหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][2] + "", "N"),/*ประเภทรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][3] + "", "N"),/*จำนวนช่อง*/
                                            StringToSQL("", "S"),/*รหัสหัว*/
                                            StringToSQL("", "S"),/*รหัสหาง*/
                                            StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหาง*/
                                            StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][10] + "", "S"),/*หมายเลขข้างรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][11] + "", "S"),/*หมายเลขเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][12] + "", "S"),/*หมายเลขแชชซีย์*/
                                            StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                            StringToSQL(dt_DataMain.Rows[i][14] + "", "S"),/*ยี่ห้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][15] + "", "S"),/*รุ่น*/
                                            StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                            StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][19] + "", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                            StringToSQL(dt_DataMain.Rows[i][20] + "", "S"),/*ระบบกันสะเทือน*/
                                            StringToSQL(dt_DataMain.Rows[i][21] + "", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][22] + "", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                            StringToSQL(dt_DataMain.Rows[i][23] + "", "S"),/*วิธีการเติมน้ำมัน*/
                                            StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                            StringToSQL(dt_DataMain.Rows[i][25] + "", "S"),/*ผลิตภัณฑ์หลัก*/
                                            StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                            StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                            StringToSQL(dt_DataMain.Rows[i][28] + "", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                            StringToSQL(dt_DataMain.Rows[i][29] + "", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                            StringToSQL(dt_DataMain.Rows[i][30] + "", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                            StringToSQL(dt_DataMain.Rows[i][31] + "", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][32] + "", "D"),/*วันหมดอายุวัดน้ำ*/
                                            StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                            StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                            StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][36] + "", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                            StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                            StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                            StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                            StringToSQL(dt_DataMain.Rows[i][40] + "", "S"),/*ประเภทเชื้อเพลิง*/
                                            StringToSQL(dt_DataMain.Rows[i][41] + "", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                            StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                            StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                            StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                            StringToSQL(dt_DataMain.Rows[i][57] + "", "S"),/*ClassGroup*/
                                            StringToSQL(dt_DataMain.Rows[i][58] + "", "S")/*หมายเลขแชชซีย์หาง*/);

                                        #endregion
                                    }
                                }
                                else if (CARTYPE == "3")//กรณีรถเซมิเทลเลอร์
                                {
                                    SHEADREGISTERNO = dt_DataMain.Rows[i][1] + "";
                                    DataTable dt_TruckH = CommonFunction.Get_Data(sql, "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + SHEADREGISTERNO + "" + "%'");
                                    //เช็คว่ามีหัวแล้วหรือยังถ้ามีอัพเดท ไม่มีให้ Insert
                                    if (dt_TruckH.Rows.Count > 0)
                                    {
                                        STRUCKID = dt_TruckH.Rows[0]["STRUCKID"] + "";
                                        SHEADID = dt_TruckH.Rows[0]["SHEADID"] + "";
                                        STRAILERID = dt_TruckH.Rows[0]["STRAILERID"] + "";

                                        #region Update HEAD
                                        QUERY_HEAD = string.Format(UPDATE_TTRUCK,
                                            StringToSQL(STRUCKID, "S"),/*รหัสหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][2] + "", "N"),/*ประเภทรถ*/
                                            StringToSQL("", "N"),/*จำนวนช่อง*/
                                            StringToSQL("", "S"),/*รหัสหัว*/
                                            (dt_DataMain.Rows[i][2] + "" == "3" ? StringToSQL(STRAILERID, "S") : "null"),/*รหัสหาง*/
                                            StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหาง*/
                                            StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                            StringToSQL("", "S"),/*หมายเลขข้างรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][11] + "", "S"),/*หมายเลขเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][12] + "", "S"),/*หมายเลขแชชซีย์*/
                                            StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                            StringToSQL(dt_DataMain.Rows[i][14] + "", "S"),/*ยี่ห้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][15] + "", "S"),/*รุ่น*/
                                            StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                            StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                            StringToSQL("", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                            StringToSQL("", "S"),/*ระบบกันสะเทือน*/
                                            StringToSQL("", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                            StringToSQL("", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                            StringToSQL("", "S"),/*วิธีการเติมน้ำมัน*/
                                            StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                            StringToSQL("", "S"),/*ผลิตภัณฑ์หลัก*/
                                            StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                            StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                            StringToSQL("", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                            StringToSQL("", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                            StringToSQL("", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                            StringToSQL("", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL("", "D"),/*วันหมดอายุวัดน้ำ*/
                                            StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                            StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                            StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][36] + "", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                            StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                            StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                            StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                            StringToSQL(dt_DataMain.Rows[i][40] + "", "S"),/*ประเภทเชื้อเพลิง*/
                                            StringToSQL(dt_DataMain.Rows[i][41] + "", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                            StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                            StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                            StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                            StringToSQL(dt_DataMain.Rows[i][57] + "", "S"),/*ClassGroup*/
                                            StringToSQL(dt_DataMain.Rows[i][58] + "", "S")/*หมายเลขแชชซีย์หาง*/);

                                        #endregion
                                    }
                                    else
                                    {

                                        #region InSert HEAD
                                        QUERY_HEAD += "TID := FC_GENID_TTRUCK(0);";
                                        QUERY_HEAD += string.Format(INSERT_TTRUCK,
                                            "TID",/*รหัสหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][2] + "", "N"),/*ประเภทรถ*/
                                            StringToSQL("", "N"),/*จำนวนช่อง*/
                                            StringToSQL("", "S"),/*รหัสหัว*/
                                            (dt_DataMain.Rows[i][2] + "" == "3" ? StringToSQL(STRAILERID, "S") : "null"),/*รหัสหาง*/
                                            StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหาง*/
                                            StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                            StringToSQL("", "S"),/*หมายเลขข้างรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][11] + "", "S"),/*หมายเลขเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][12] + "", "S"),/*หมายเลขแชชซีย์*/
                                            StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                            StringToSQL(dt_DataMain.Rows[i][14] + "", "S"),/*ยี่ห้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][15] + "", "S"),/*รุ่น*/
                                            StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                            StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                            StringToSQL("", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                            StringToSQL("", "S"),/*ระบบกันสะเทือน*/
                                            StringToSQL("", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                            StringToSQL("", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                            StringToSQL("", "S"),/*วิธีการเติมน้ำมัน*/
                                            StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                            StringToSQL("", "S"),/*ผลิตภัณฑ์หลัก*/
                                            StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                            StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                            StringToSQL("", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                            StringToSQL("", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                            StringToSQL("", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                            StringToSQL("", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL("", "D"),/*วันหมดอายุวัดน้ำ*/
                                            StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                            StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                            StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][36] + "", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                            StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                            StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                            StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                            StringToSQL(dt_DataMain.Rows[i][40] + "", "S"),/*ประเภทเชื้อเพลิง*/
                                            StringToSQL(dt_DataMain.Rows[i][41] + "", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                            StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                            StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                            StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                            StringToSQL(dt_DataMain.Rows[i][57] + "", "S"),/*ClassGroup*/
                                            StringToSQL(dt_DataMain.Rows[i][58] + "", "S")/*หมายเลขแชชซีย์หาง*/);
                                        QUERY_HEAD += "COMMIT;";
                                        #endregion

                                    }


                                    //เช็คว่ามีหางแล้วหรือยังถ้ามีอัพเดท ไม่มีให้ Insert
                                    SHEADREGISTERNO = dt_DataMain.Rows[i][6] + "";
                                    DataTable dt_TruckR = CommonFunction.Get_Data(sql, "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + SHEADREGISTERNO + "" + "%'");
                                    if (dt_TruckR.Rows.Count > 0)
                                    {
                                        #region Update Tail
                                        QUERY_TAIL = string.Format(UPDATE_TTRUCK,
                                            StringToSQL(dt_TruckR.Rows[0]["STRUCKID"] + "", "S"),/*รหัสหัว*/
                                            StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหัว*/
                                            StringToSQL("4", "N"),/*ประเภทรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][3] + "", "N"),/*จำนวนช่อง*/
                                            (dt_DataMain.Rows[i][2] + "" == "3" ? StringToSQL(STRUCKID, "S") : "null"),/*รหัสหัว*/
                                            StringToSQL("", "S"),/*รหัสหาง*/
                                            StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหาง*/
                                            StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                            StringToSQL(dt_DataMain.Rows[i][10] + "", "S"),/*หมายเลขข้างรถ*/
                                            StringToSQL("", "S"),/*หมายเลขเครื่องยนต์*/
                                            StringToSQL(dt_DataMain.Rows[i][58] + "", "S"),/*หมายเลขแชชซีย์*/
                                            StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                            StringToSQL("", "S"),/*ยี่ห้อ*/
                                            StringToSQL("", "S"),/*รุ่น*/
                                            StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                            StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                            StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                            StringToSQL(dt_DataMain.Rows[i][19] + "", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                            StringToSQL(dt_DataMain.Rows[i][20] + "", "S"),/*ระบบกันสะเทือน*/
                                            StringToSQL(dt_DataMain.Rows[i][21] + "", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][22] + "", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                            StringToSQL(dt_DataMain.Rows[i][23] + "", "S"),/*วิธีการเติมน้ำมัน*/
                                            StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                            StringToSQL(dt_DataMain.Rows[i][25] + "", "S"),/*ผลิตภัณฑ์หลัก*/
                                            StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                            StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                            StringToSQL(dt_DataMain.Rows[i][28] + "", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                            StringToSQL(dt_DataMain.Rows[i][29] + "", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                            StringToSQL(dt_DataMain.Rows[i][30] + "", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                            StringToSQL(dt_DataMain.Rows[i][31] + "", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][32] + "", "D"),/*วันหมดอายุวัดน้ำ*/
                                            StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                            StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                            StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                            StringToSQL("", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                            StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                            StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                            StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                            StringToSQL("", "S"),/*ประเภทเชื้อเพลิง*/
                                            StringToSQL("", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                            StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                            StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                            StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                            StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                            StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                            StringToSQL(dt_DataMain.Rows[i][57] + "", "S")/*ClassGroup*/);
                                        #endregion
                                    }
                                    else
                                    {
                                        #region InSert Tail
                                        QUERY_TAIL += "TID2 := FC_GENID_TTRUCK(0);";
                                        QUERY_TAIL += string.Format(INSERT_TTRUCK,
                                        "TID2",/*รหัสหัว*/
                                        StringToSQL(dt_DataMain.Rows[i][6] + "", "S"),/*ทะเบียนหัว*/
                                        StringToSQL("4", "N"),/*ประเภทรถ*/
                                        StringToSQL(dt_DataMain.Rows[i][3] + "", "N"),/*จำนวนช่อง*/
                                        (dt_DataMain.Rows[i][2] + "" == "3" ? StringToSQL(STRUCKID, "S") : "null"),/*รหัสหัว*/
                                        StringToSQL("", "S"),/*รหัสหาง*/
                                        StringToSQL(dt_DataMain.Rows[i][1] + "", "S"),/*ทะเบียนหาง*/
                                        StringToSQL(SVENDORID, "S"),/*รหัสผู้ขนส่ง*/
                                            // dt_DataMain.Rows[i][8] + "",/*ชื่อผู้ขนส่ง*/
                                        StringToSQL(dt_DataMain.Rows[i][9] + "", "N"),/*ประเภทขนส่ง*/
                                        StringToSQL(dt_DataMain.Rows[i][10] + "", "S"),/*หมายเลขข้างรถ*/
                                        StringToSQL("", "S"),/*หมายเลขเครื่องยนต์*/
                                        StringToSQL(dt_DataMain.Rows[i][58] + "", "S"),/*หมายเลขแชชซีย์*/
                                        StringToSQL(dt_DataMain.Rows[i][13] + "", "S"),/*ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)*/
                                        StringToSQL("", "S"),/*ยี่ห้อ*/
                                        StringToSQL("", "S"),/*รุ่น*/
                                        StringToSQL(dt_DataMain.Rows[i][16] + "", "N"),/*จำนวนล้อ*/
                                        StringToSQL(dt_DataMain.Rows[i][17] + "", "N"),/*ความจุรวม*/
                                        StringToSQL(dt_DataMain.Rows[i][18] + "", "N"),/*นํ้าหนักรถ*/
                                        StringToSQL(dt_DataMain.Rows[i][19] + "", "N"),/*น้ำหนักรถรวมผลิตภัณฑ์(นบ.)*/
                                        StringToSQL(dt_DataMain.Rows[i][20] + "", "S"),/*ระบบกันสะเทือน*/
                                        StringToSQL(dt_DataMain.Rows[i][21] + "", "S"),/*วัสดุที่ใช้ทำตัวถังบรรทุก*/
                                        StringToSQL(dt_DataMain.Rows[i][22] + "", "S"),/*บริษัทผู้ผลิตตัวถัง*/
                                        StringToSQL(dt_DataMain.Rows[i][23] + "", "S"),/*วิธีการเติมน้ำมัน*/
                                        StringToSQL(dt_DataMain.Rows[i][24] + "", "D"),/*วันที่จดทะเบียนครั้งแรก*/
                                        StringToSQL(dt_DataMain.Rows[i][25] + "", "S"),/*ผลิตภัณฑ์หลัก*/
                                        StringToSQL("Y", "S"),/*สถานะการใช้งาน*/
                                        StringToSQL(dt_DataMain.Rows[i][27] + "", "S"),/*หมายเหตุ*/
                                        StringToSQL(dt_DataMain.Rows[i][28] + "", "D"),/*วันที่วัดน้ำครั้งก่อน*/
                                        StringToSQL(dt_DataMain.Rows[i][29] + "", "D"),/*วันที่วัดน้ำครั้งต่อไป*/
                                        StringToSQL(dt_DataMain.Rows[i][30] + "", "S"),/*เลขที่วัดน้ำครั้งก่อน*/
                                        StringToSQL(dt_DataMain.Rows[i][31] + "", "S"),/*เลขที่วัดน้ำครั้งล่าสุด*/
                                        StringToSQL(dt_DataMain.Rows[i][32] + "", "D"),/*วันหมดอายุวัดน้ำ*/
                                        StringToSQL(dt_DataMain.Rows[i][33] + "", "D"),/*วันที่ขึ้นทะเบียนกับปตท*/
                                        StringToSQL(dt_DataMain.Rows[i][34] + "", "N"),/*จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก*/
                                        StringToSQL(dt_DataMain.Rows[i][35] + "", "N"),/*กำลังเครื่องยนต์*/
                                        StringToSQL("", "S"),/*ปั๊ม Power ที่ตัวรถ(มี/ไม่มี)*/
                                        StringToSQL(dt_DataMain.Rows[i][37] + "", "S"),/*ชนิดของปั๊มPower*/
                                        StringToSQL(dt_DataMain.Rows[i][38] + "", "S"),/*วัสดุที่สร้าง Pressure ของปั๊ม Power*/
                                        StringToSQL(dt_DataMain.Rows[i][39] + "", "S"),/*ชนิดของวาล์ว*/
                                        StringToSQL("", "S"),/*ประเภทเชื้อเพลิง*/
                                        StringToSQL("", "S"),/*บริษัทที่ให้บริการระบบGPS*/
                                        StringToSQL(dt_DataMain.Rows[i][42] + "", "S"),/*สถานที่วัดน้ำครั้งล่าสุด*/
                                        StringToSQL(dt_DataMain.Rows[i][43] + "", "S"),/*Vehicle type*/
                                        StringToSQL(dt_DataMain.Rows[i][44] + "", "S"),/*หน่วยบรรทุก*/
                                        StringToSQL(dt_DataMain.Rows[i][45] + "", "S"),/*Vehicle header text*/
                                        StringToSQL(dt_DataMain.Rows[i][46] + "", "S"),/*Vehicle Group*/
                                        StringToSQL(dt_DataMain.Rows[i][57] + "", "S")/*ClassGroup*/);
                                        QUERY_TAIL += "COMMIT;";
                                        #endregion
                                    }


                                }

                                //ในกรณีที่เป็นการสร้าง รถเซมิเทเลอร์จะต้องอัพเดทหัวหางเพิ่อสแตมไอดี
                                if (CARTYPE == "3")
                                {
                                    //หาไอดีหางที่เซฟลงเบสเพื่อไปสร้าง ความจุ
                                    STRUCKID = GetTruckID(dt_DataMain.Rows[i][6] + "");

                                    //UPDATEHEADTAIL = @"UPDATE TTRUCK SET STRAILERID  = TID2 WHERE  STRUCKID = TID;UPDATE TTRUCK SET SHEADID  = TID WHERE  STRUCKID = TID2;";

                                    UPDATEHEADTAIL += UpdateDataTruck(dt_DataMain.Rows[i][1] + "");
                                    UPDATEHEADTAIL += UpdateDataTruck(dt_DataMain.Rows[i][6] + "");
                                }
                                else
                                {
                                    //หาไอดีหางที่เซฟลงเบสเพื่อไปสร้าง ความจุ
                                    STRUCKID = GetTruckID(dt_DataMain.Rows[i][1] + "");
                                }

                                //ไม่ว่าจะเป็นอัพเดทหรือ INS จะลบและแอดเข้าไปใหม่ ไม่มีการเก็บประวัติ
                                #region COMPART
                                string COMPART = "";
                                if (!string.IsNullOrEmpty(STRUCKID))
                                {
                                    //string SSTRUCKID = CARTYPE == "0" ? STRUCKID : STRAILERID;

                                    DataTable dt_Compart = CommonFunction.Get_Data(sql, "SELECT STRUCKID FROM TTRUCK_COMPART WHERE  STRUCKID = '" + STRUCKID + "'");
                                    if (dt_Compart.Rows.Count > 0)
                                    {
                                        //SystemFunction.SQLExecuteNonQuery(sql, "DELETE FROM TTRUCK_COMPART  WHERE  STRUCKID = '" + STRUCKID + "';");
                                        DELETE_COMPART = "DELETE FROM TTRUCK_COMPART  WHERE  STRUCKID = '" + STRUCKID + "';";
                                    }


                                    for (int k = 1; k < 11; k++)
                                    {
                                        int COL = (k + 46);
                                        // CHECK += SCOMPART(dt_DataMain.Rows[i][COL] + "", STRUCKID, k + "");
                                        COMPART += SCOMPART(dt_DataMain.Rows[i][COL] + "", STRUCKID, k + "");
                                    }

                                    ADD_COMPART = COMPART;
                                    // SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  VID VARCHAR2(10); BEGIN " + COMPART + " END;");
                                }
                                else
                                {
                                    string TID = "";
                                    if (CARTYPE == "3")
                                    {
                                        TID = "TID2";
                                    }
                                    else
                                    {
                                        TID = "TID";
                                    }

                                    for (int k = 1; k < 11; k++)
                                    {
                                        int COL = (k + 46);
                                        // CHECK += SCOMPART(dt_DataMain.Rows[i][COL] + "", STRUCKID, k + "");
                                        COMPART += SCOMPART(dt_DataMain.Rows[i][COL] + "", TID, k + "");
                                    }

                                    ADD_COMPART = COMPART;
                                }

                                QUERY += QUERY_HEAD + QUERY_TAIL + UPDATEHEADTAIL + DELETE_COMPART + ADD_COMPART + "COMMIT;";
                                #endregion


                                if (i % 100 == 0)
                                {
                                    string ss = "DECLARE  TID VARCHAR2(10);TID2 VARCHAR2(10); BEGIN " + QUERY + " END;";
                                    SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  TID VARCHAR2(10);TID2 VARCHAR2(10);  BEGIN " + QUERY + " END;");
                                    QUERY = "";
                                }

                            }
                            //เก็บข้อมูลลงเบส

                            if (string.IsNullOrEmpty(MSG_ERROR) && string.IsNullOrEmpty(MSG_REQUIRE) && string.IsNullOrEmpty(MSG_VEN))
                            {
                                string ss = "DECLARE  TID VARCHAR2(10);TID2 VARCHAR2(10); BEGIN " + QUERY + " END;";
                                SystemFunction.SQLExecuteNonQuery(sql, "DECLARE  TID VARCHAR2(10);TID2 VARCHAR2(10);  BEGIN " + QUERY + " END;");
                                CommonFunction.SetPopupOnLoad(xgvwCar, "dxInfo('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "')");
                            }
                        }
                        else
                        {
                            //ไม่พบข้อมูล
                        }
                    }
                }
                #endregion
                break;
        }
    }
    protected void BindTruckWithDataFromExcel(ASPxGridView _gvw, DataTable _dtTruck, params string[] sArrayParams)
    {
        int visibleindex = (xgvw.IsEditing) ? xgvw.EditingRowVisibleIndex : 0;
        DataView _dvTruck = new DataView(_dtTruck);
        //_dvTruck.Sort = "SHEADREGISTERNO ASC";
        _gvw.DataSource = _dvTruck;
        _gvw.DataBind();

    }
    protected string ImportVendor(DataTable _dtVendor)
    {
        string vendor = "";
        {
            DataTable dt_DataGPSExcel = (DataTable)Session["ss_DataGPSExcel"];
            if (dt_DataGPSExcel.Columns.Count > 0)
            {
                dt_DataGPSExcel.Columns.Add(new DataColumn("Status", typeof(string)));
            }
            string sInsFormat_SAP = @"INSERT INTO TEMPLOYEE_SAP ( SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, DUPDATE, SUPDATE, PERS_CODE, CARRIER, LICENSE_NO, DRVSTATUS) 
VALUES ( :sID , '{1}', '{2}', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2', '{9}', '{8}',  '{10}', '{13}' );";
            string sInsFormat = @"INSERT INTO TEMPLOYEE ( SEMPLOYEEID, STRANS_ID, STRANTYPE, SEMPTPYE, SDRIVERNO , DBIRTHDATE, SPERSONELNO, DDRIVEBEGIN, DDRIVEEXPIRE, STEL, STEL2, SMAIL,  SMAIL2,
CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) 
VALUES ( :sID , '{8}', null, '{0}', '{10}' , TO_DATE('{3}','yyyy-mm-dd'), '{9}', '{11}', '{12}', '{4}', '{5}', '{6}',  '{7}', '1', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2');";
            if (dt_DataGPSExcel.Rows.Count > 0)
            {
                sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID)
WHERE  1=1 
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE 1=1";
                sdsVendor.SelectParameters.Clear();
                sdsVendor.DataBind();
                DataTable dtVendor = ((DataView)sdsVendor.Select(new DataSourceSelectArguments())).ToTable();

                StringBuilder stbd = new StringBuilder();
                StringBuilder stbd_Error = new StringBuilder();
                foreach (DataRow dr_DataGPSExcel in dt_DataGPSExcel.Rows)
                {
                    var TRANSPORTER = dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "'").ToList().FirstOrDefault();
                    string SVENDORNAME = TRANSPORTER.ItemArray[0] + "";
                    bool HasID = false;
                    string sMissText = "";
                    int nRowIndex = dt_DataGPSExcel.Rows.IndexOf(dr_DataGPSExcel);
                    sMissText += (dr_DataGPSExcel["ตำแหน่ง"] + "" == "") ? ",ตำแหน่ง" : "";
                    sMissText += (dr_DataGPSExcel["ชื่อ"] + "" == "") ? ",ชื่อ" : "";
                    sMissText += (dr_DataGPSExcel["นามสกุล"] + "" == "") ? ",นามสกุล" : "";
                    sMissText += (dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "" == "") ? ",บริษัทผู้ขนส่ง" : "";
                    sMissText += (dtVendor.Select("SVENDORNAME='" + dr_DataGPSExcel["บริษัทผู้ขนส่ง"] + "'").Length == 0) ? ",ไม่พบ บริษัทผู้ขนส่ง" : "";
                    sMissText += (dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "" == "") ? ",หมายเลขบัตรประชาชน" : "";

                    /*
+		[0]	    {ตำแหน่ง}	object {System.Data.DataColumn}
+		[1]	    {ชื่อ}	object {System.Data.DataColumn}
+		[2]	    {นามสกุล}	object {System.Data.DataColumn}
+		[3]	    {วันเกิด}	object {System.Data.DataColumn}
+		[4]	    {หมายเลขโทรศัพท์หลัก}	object {System.Data.DataColumn}
+		[5]	    {หมายเลขโทรศัพท์สำรอง}	object {System.Data.DataColumn}
+		[6]	    {อีเมล์หลัก}	object {System.Data.DataColumn}
+		[7]	    {อีเมล์สำรอง}	object {System.Data.DataColumn}
+		[8]	    {บริษัทผู้ขนส่ง}	object {System.Data.DataColumn}
+		[9]	    {หมายเลขบัตรประชาชน}	object {System.Data.DataColumn}
+		[10]	{หมายเลขใบขับขี่ประเภท 4}	object {System.Data.DataColumn}
+		[11]	{อนุญาตใบขับขี่ประเภท 4 _ตั้งแต่วันที่}	object {System.Data.DataColumn}
+		[12]	{อนุญาตใบขับขี่ประเภท 4 _หมดอายุวันที่}	object {System.Data.DataColumn}
    {13}    DRVSTATUS

                     */
                    TBL_Vendor imp_vend = new TBL_Vendor(Page, connection);
                    imp_vend.PERS_CODE = dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "";
                    if (sMissText != "")
                    {
                        stbd_Error.Append("^Line " + nRowIndex + ":" + sMissText);
                        dr_DataGPSExcel.BeginEdit();
                        dr_DataGPSExcel["Status"] = ">" + stbd_Error;
                        dr_DataGPSExcel.EndEdit();

                    }
                    else
                    {
                        imp_vend.Open();
                        HasID = (!string.IsNullOrEmpty(imp_vend.EMPLOYEEID));

                        //FC_GENID_EMPLOYEEID()

                        imp_vend.EMPTPYE = dr_DataGPSExcel["ตำแหน่ง"] + "";
                        imp_vend.FNAME = dr_DataGPSExcel["ชื่อ"] + "";
                        imp_vend.LNAME = dr_DataGPSExcel["นามสกุล"] + "";
                        imp_vend.TRANS_ID = SVENDORNAME;
                        imp_vend.PERS_CODE = dr_DataGPSExcel["หมายเลขบัตรประชาชน"] + "";
                        imp_vend.BIRTHDATE = dr_DataGPSExcel["วันเกิด"] + "";
                        imp_vend.TEL = dr_DataGPSExcel["หมายเลขโทรศัพท์หลัก"] + "";
                        imp_vend.TEL2 = dr_DataGPSExcel["หมายเลขโทรศัพท์สำรอง"] + "";
                        imp_vend.MAIL = dr_DataGPSExcel["อีเมล์หลัก"] + "";
                        imp_vend.MAIL2 = dr_DataGPSExcel["อีเมล์สำรอง"] + "";
                        imp_vend.LICENSE_NO = dr_DataGPSExcel["หมายเลขใบขับขี่ประเภท 4"] + "";
                        imp_vend.DRIVERNO_BEGIN = dr_DataGPSExcel["อนุญาตใบขับขี่ประเภท 4 _ตั้งแต่วันที่"] + "";
                        imp_vend.DRIVERNO_Expire = dr_DataGPSExcel["อนุญาตใบขับขี่ประเภท 4 _หมดอายุวันที่"] + "";
                        imp_vend.DRVSTATUS = "1";
                        string sExecute = "";
                        if (HasID)
                        {
                            sExecute = @"UPDATE TEMPLOYEE_SAP 
SET FNAME='{1}', LNAME='{2}', DUPDATE=sysdate, SUPDATE='IMP_TMS2', PERS_CODE='{9}', CARRIER='{8}', LICENSE_NO='{10}', DRVSTATUS ='{13}'
WHERE SEMPLOYEEID='" + imp_vend.EMPLOYEEID + @"' AND PERS_CODE='{9}' ;
UPDATE TEMPLOYEE SET STRANS_ID='{8}', SEMPTPYE='{0}', SDRIVERNO='{10}' , DBIRTHDATE=TO_DATE('{3}','yyyy-mm-dd'), SPERSONELNO='{9}'
, DDRIVEBEGIN=TO_DATE('{11}','yyyy-mm-dd'), DDRIVEEXPIRE=TO_DATE('{12}','yyyy-mm-dd'), STEL='{4}', STEL2='{5}', SMAIL='{6}',  SMAIL2='{7}',
CACTIVE='{13}', DUPDATE=sysdate, SUPDATE='IMP_TMS2'
WHERE SEMPLOYEEID='" + imp_vend.EMPLOYEEID + @"' AND PERS_CODE='{9}' ;
";
                        }
                        else
                        {
                            sExecute = @"DECLARE EMP_ID varchar2(20);
BEGIN
EMP_ID :=FC_GENID_EMPLOYEEID();
INSERT INTO TEMPLOYEE_SAP ( SEMPLOYEEID, FNAME, LNAME, DCREATE, SCREATE, DUPDATE, SUPDATE, PERS_CODE, CARRIER, LICENSE_NO, DRVSTATUS) 
VALUES ( EMP_ID , '{1}', '{2}', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2', '{9}', '{8}',  '{10}', '{13}' );
INSERT INTO TEMPLOYEE ( SEMPLOYEEID, STRANS_ID, STRANTYPE, SEMPTPYE, SDRIVERNO , DBIRTHDATE, SPERSONELNO, DDRIVEBEGIN, DDRIVEEXPIRE, STEL, STEL2, SMAIL,  SMAIL2, CACTIVE, DCREATE, SCREATE, DUPDATE, SUPDATE) 
VALUES ( EMP_ID , '{8}', null, '{0}', '{10}' , TO_DATE('{3}','yyyy-mm-dd'), '{9}', TO_DATE('{11}','yyyy-mm-dd'), TO_DATE('{12}','yyyy-mm-dd'), '{4}', '{5}', '{6}',  '{7}', '1', sysdate, 'IMP_TMS2', sysdate, 'IMP_TMS2');
END;
";

                        }
                        stbd.AppendFormat(sExecute, imp_vend.EMPTPYE, imp_vend.FNAME, imp_vend.LNAME, imp_vend.BIRTHDATE, imp_vend.TEL, imp_vend.TEL2, imp_vend.MAIL, imp_vend.MAIL2, imp_vend.TRANS_ID
                            , imp_vend.PERS_CODE, imp_vend.LICENSE_NO, imp_vend.DRIVERNO_BEGIN, imp_vend.DRIVERNO_Expire, imp_vend.DRVSTATUS);
                    }
                }
                Session["ss_DataGPSExcel"] = dt_DataGPSExcel;
                dtVendor.Dispose();
                if (stbd_Error.ToString() == "")
                {
                    string sExec = stbd.ToString();
                }
                else
                {
                    CommonFunction.SetPopupOnLoad(xgvw, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','กรุณาตรวจสอบความถูกต้องของข้อมูลที่จะนำเข้า " + stbd_Error.ToString().Replace("^", "<br />") + "!');");

                    //BindTruckWithDataFromExcel(xgvwCar, (DataTable)Session["ss_DataGPSExcel"], "STARTEDIT");//BindTruckWithDataInGridEditing(xgvwCar, "STARTEDIT");
                }
            }
        }

        return vendor;
    }
    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM ((((TVENDOR_SAP v LEFT JOIN TCONTRACT c ON V.SVENDORID = C.SVENDORID )LEFT JOIN TCONTRACTTYPE ct ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID) 
LEFT JOIN TCONTRACT_TRUCK ct ON C.SCONTRACTID = CT.SCONTRACTID)LEFT JOIN TTRUCK t ON CT.STRUCKID = T.STRUCKID)
WHERE  V.SVENDORNAME  like :fillter 
GROUP BY  v.SVENDORID, v.SVENDORNAME )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();


    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }
    protected void uclVender_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/AdminHelper/Temp_Excel/", "Vendor");
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }

    }
    protected void uclContract_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/AdminHelper/Temp_Excel/", "Contract");
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }

    }
    protected void uclTruck_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            dynamic MasterData = xgvw.GetRowValues(xgvw.EditingRowVisibleIndex, "SCONTRACTID", "DDATE");
            e.CallbackData = UploadFile2Server(e.UploadedFile, "" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "By" + Session["UserID"], "UploadFile/AdminHelper/Temp_Excel/", "Truck");
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }

    }

    protected string CheckTransporter(DataTable _dtData, string _out)
    {
        var ListVendor = new List<string>();
        _dtData.Columns["ผู้ขนส่ง"].ColumnName = "VenderName";
        string sTranName = "", sWhere = "";
        int nCount = 0;
        foreach (DataRow _drData in _dtData.Rows)
        {
            if (_drData["VenderName"] + "" != "")
            {
                nCount++;
                sWhere += string.Format(" OR NVL(SABBREVIATION,'') LIKE '{0}'", _drData["VenderName"] + "");
                ListVendor.Add("" + _drData["VenderName"]);
            }

        }
        sWhere = sWhere.StartsWith(" OR") ? " AND ( " + sWhere.Remove(0, 3) + ")" : sWhere;
        DataTable _dtVedor = CommonFunction.Get_Data(sql, "SELECT SVENDORID ,SABBREVIATION SVENDORNAME," + nCount + " nVend  FROM TVENDOR  WHERE 1=1  " + sWhere + " ");

        foreach (string VendExcel in ListVendor)
        {
            DataRow[] _drVedor = _dtVedor.Select("SVENDORNAME='" + VendExcel + "'");
            _out += (_drVedor.Length <= 0) ? "," + VendExcel : "";
        }

        return _out;
    }

    private string CheckNull(string VALUE)
    {
        string Result = "";

        if (!string.IsNullOrEmpty(VALUE))
        {
            Result = VALUE;
        }
        else
        {
            Result = "null";
        }
        return Result;
    }

    private string StringToSQL(string VALUE, string STYPE)
    {
        string Result = "";
        DateTime DateSet;
        switch (STYPE)
        {
            case "S": Result = !string.IsNullOrEmpty(VALUE) ? "'" + VALUE + "'" : "null";
                break;
            case "N": Result = !string.IsNullOrEmpty(VALUE) ? "" + VALUE.Replace(",", "") + "" : "null";
                break;
            case "D":
                //เช็คว่าวันที่มีค่าว่างไหม
                string DATE = "";
                if (!string.IsNullOrEmpty(VALUE))
                {
                    //try//เช็คว่าถ้ามันสามารถแปลงเป็นวันที่ได้
                    //{
                    int nYear_Todate = int.Parse(DateTime.Now.ToString("yyyy", new CultureInfo("en-US"))) + 100;

                    if (DateTime.TryParseExact(VALUE, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "M/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "MM/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "M/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "M/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "MM/d/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                    {
                        DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                    }
                    else
                    {
                        DATE = "";
                    }

                    if (!string.IsNullOrEmpty(DATE))
                    {
                        //string[] dDelivery = ("" + DATE).Split('/');
                        string[] dDelivery = ("" + DATE).Split('/');
                        string[] dDelivery2 = ("" + DATE).Split('-');
                        string YYYY = dDelivery.Length > 2 ? dDelivery[2] + "" : dDelivery2[2] + "";
                        string formateDelivery = "";
                        if (YYYY.StartsWith("30"))
                        {
                            formateDelivery = (YYYY.StartsWith("30")) ? "en-US" : "th-TH";
                        }
                        else if (int.Parse((YYYY).Substring(0, 2)) > 30)//กรณีที่ค่าปีเริ่มต้นมากกว่า30
                        {
                            formateDelivery = "en-US";
                        }
                        else if (int.Parse((YYYY)) > nYear_Todate)//กรณีที่มากกว่า ค.ศ.+100ปี
                        {
                            formateDelivery = "en-US";
                        }
                        //else if (int.Parse((YYYY)) < nYear_Todate)//กรณีที่ค่าน้อยกว่า ค.ศ.+100ปี
                        //{
                        //    formateDelivery = "en-US";
                        //}
                        else
                        {
                            formateDelivery = "th-TH";
                        }

                        try
                        {
                            DATE = DateTime.Parse(DATE).ToString("dd/MM/yyyy", new CultureInfo(formateDelivery));
                        }
                        catch
                        {
                            if (DateTime.TryParseExact(DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out DateSet))
                            {
                                DATE = DateSet.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                            }
                        }
                    }
                    //}
                    //catch
                    //{

                    //}
                }

                Result = !string.IsNullOrEmpty(DATE) ? "TO_DATE('" + DATE + "','dd/MM/yyyy ')" : "null";

                //string sDate = "";
                //if (DateTime.TryParseExact(VALUE, "MM/dd/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
                //{
                //    sDate = CheckDateTime(DateSet);
                //    Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
                //}
                //else if (DateTime.TryParseExact(VALUE, "MM/dd/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
                //{
                //    sDate = CheckDateTime(DateSet);
                //    Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
                //}
                //else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
                //{
                //    sDate = CheckDateTime(DateSet);
                //    Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
                //}
                //else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
                //{
                //    sDate = CheckDateTime(DateSet);
                //    Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
                //}
                //else
                //{
                //    Result = "null";
                //}
                break;
            case "DV":

                string sDay = !string.IsNullOrEmpty(VALUE) ? DateTime.Parse(VALUE).ToString("dd/MM/yyyy", new CultureInfo("en-US")) : "null";
                Result = !string.IsNullOrEmpty(VALUE) ? sDay : "null";
                break;
        }


        return Result;

    }

    private string NameRequire(string Value)
    {
        string Result = "";

        switch (Value)
        {
            case "SHEADREGISTERNO":
                Result = " ทะเบียนหัว<br>";
                break;
            case "SCARTYPEID":
                Result = " ประเภทรถ<br>";
                break;
            case "NSLOT":
                Result = " จำนวนช่อง<br>";
                break;
            case "STRAILERREGISTERNO":
                Result = " ทะเบียนหาง<br>";
                break;
            case "STRANSPORTER":
                Result = " ชื่อผู้ขนส่ง<br>";
                break;
            case "SCAR_NUM":
                //Result = " หมายเลขข้างรถ<br>";
                break;
            case "SENGINE":
                //Result = " หมายเลขเครื่องยนต์<br>";
                break;
            case "SCHASIS":
                Result = " หมายเลขแชชซีย์<br>";
                break;
            case "SOWNER_NAME":
                // Result = " ผู้ถือกรรมสิทธิ์(ตามหลักฐานการจดทะเบียน)<br>";
                break;
            case "SBRAND":
                Result = " ยี่ห้อ<br>";
                break;
            case "SMODEL":
                //Result = " รุ่น<br>";
                break;
            case "NWHEELS":
                //Result = " จำนวนล้อ<br>";
                break;
            case "NTOTALCAPACITY":
                Result = " ความจุรวม<br>";
                break;
            case "NWEIGHT":
                //Result = " นํ้าหนักรถ(นร.)<br>";
                break;
            case "NLOAD_WEIGHT":
                //Result = " น้ำหนักรถรวมผลิตภัณฑ์(นบ.)<br>";
                break;
            case "DREGISTER":
                Result = " วันที่จดทะเบียนครั้งแรก<br>";
                break;
            case "DWATEREXPIRE":
                //Result = " วันหมดอายุวัดน้ำ<br>";
                break;
            case "DSIGNIN":
                //Result = " วันที่ขึ้นทะเบียนกับปตท<br>";
                break;
            case "NSHAFTDRIVEN":
                Result = " จำนวนเพลาขับเคลื่อนหรือรับน้ำหนัก<br>";
                break;
            case "POWERMOVER":
                Result = " กำลังเครื่องยนต์<br>";
                break;
            case "TRUCK_CATEGORY":
                Result = " Vehicle type<br>";
                break;
            case "VOL_UOM":
                Result = " หน่วยบรรทุก<br>";
                break;
            case "VEH_TEXT":
                Result = " Vehicle header text<br>";
                break;
            case "CARCATE_ID":
                Result = " Vehicle Group<br>";
                break;
            case "CLS Group":
                Result = " Class Group<br>";
                break;

        }

        return Result;
    }

    private string UpdateDataTruck(string TRUCK_ID)
    {
        string QUERY = "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO = '" + TRUCK_ID + "' AND SCARTYPEID <> '0'";
        DataTable dt = CommonFunction.Get_Data(sql, QUERY);
        string QUERY_HEAD_UPDATE = "";
        if (dt.Rows.Count > 0)
        {
            //นำทัเบียนรถที่มีการอัพมาวนเพื่อเช็คอัพเดทหัวหาง
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                //นำทะเบียน STRAILERREGISTERNO ไปหาเพราะ หัวจะเก็บหางไว้และหางจะเก็บหัวไว้เสมอ
                DataTable dt_Trailer_Head = CommonFunction.Get_Data(sql, "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + dt.Rows[i]["STRAILERREGISTERNO"] + "" + "%'");
                if (dt_Trailer_Head.Rows.Count > 0)
                {
                    string Codition = "";
                    //เช็คว่าหัวเอาหางไปอัพหางเอาหัวไปอับ
                    switch (dt.Rows[i]["SCARTYPEID"] + "")
                    {
                        case "3": Codition = "SET STRAILERID  = '" + dt_Trailer_Head.Rows[0]["STRUCKID"] + @"'";
                            break;
                        case "4": Codition = "SET SHEADID  = '" + dt_Trailer_Head.Rows[0]["STRUCKID"] + @"'";
                            break;
                    }

                    QUERY_HEAD_UPDATE = @"UPDATE TTRUCK
                                          " + Codition + @"
                                          WHERE  STRUCKID = '" + dt.Rows[i]["STRUCKID"] + @"';";

                    //AddTODB(QUERY_HEAD_UPDATE);

                }
            }
        }
        else
        {

            QUERY_HEAD_UPDATE = @"UPDATE TTRUCK SET STRAILERID  = TID2 WHERE  STRUCKID = TID;UPDATE TTRUCK SET SHEADID  = TID WHERE  STRUCKID = TID2;";

        }
        return QUERY_HEAD_UPDATE;
    }

    private void AddTODB(string strQuery)
    {
        if (!string.IsNullOrEmpty(strQuery))
        {
            using (OracleConnection con = new OracleConnection(sql))
            {

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                else
                {

                }

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }

                con.Close();

            }
        }
    }

    private string GetTruckID(string TRUCK_ID)
    {
        string QUERY = "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + TRUCK_ID + "%'";
        DataTable dt = CommonFunction.Get_Data(sql, QUERY);
        if (dt.Rows.Count > 0)
        {
            return dt.Rows[0]["STRUCKID"] + "";
        }
        else
        {
            return "";
        }

    }

    private void UpdateData(string TRUCK_ID)
    {
        string QUERY = "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO IN (" + TRUCK_ID + ") AND SCARTYPEID <> '0'";
        DataTable dt = CommonFunction.Get_Data(sql, QUERY);
        if (dt.Rows.Count > 0)
        {
            //นำทัเบียนรถที่มีการอัพมาวนเพื่อเช็คอัพเดทหัวหาง
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                //นำทะเบียน STRAILERREGISTERNO ไปหาเพราะ หัวจะเก็บหางไว้และหางจะเก็บหัวไว้เสมอ
                DataTable dt_Trailer_Head = CommonFunction.Get_Data(sql, "SELECT STRUCKID,SCARTYPEID,SHEADID,STRAILERID,STRAILERREGISTERNO FROM TTRUCK WHERE SHEADREGISTERNO LIKE '%" + dt.Rows[i]["STRAILERREGISTERNO"] + "" + "%'");
                if (dt_Trailer_Head.Rows.Count > 0)
                {
                    string Codition = "";
                    //เช็คว่าหัวเอาหางไปอัพหางเอาหัวไปอับ
                    switch (dt.Rows[i]["SCARTYPEID"] + "")
                    {
                        case "3": Codition = "SET STRAILERID  = '" + dt_Trailer_Head.Rows[0]["STRUCKID"] + @"'";
                            break;
                        case "4": Codition = "SET SHEADID  = '" + dt_Trailer_Head.Rows[0]["STRUCKID"] + @"'";
                            break;
                    }

                    string QUERY_HEAD_UPDATE = @"

                                                                             UPDATE TTRUCK
                                                                             " + Codition + @"
                                                                             WHERE  STRUCKID = '" + dt.Rows[i]["STRUCKID"] + @"'

                                                ";

                    SystemFunction.SQLExecuteNonQuery(sql, QUERY_HEAD_UPDATE);
                }
            }
        }
    }

    private string SCOMPART(string VALUE, string STRUCKID, string NCOMPARTNO)
    {
        string Result = "";

        //        string COMPART = @"INSERT INTO TTRUCK_COMPART (STRUCKID, NCOMPARTNO, NPANLEVEL, NCAPACITY, DCREATE, SCREATE, DUPDATE, SUPDATE) 
        //VALUES ( {0}, /* NCOMPARTNO */,/* NPANLEVEL */, /* NCAPACITY */, /* DCREATE */, /* SCREATE */,/* DUPDATE */, /* SUPDATE */ );";
        decimal value;
        string SID = "";
        switch (STRUCKID)
        {
            case "TID":
                SID = "TID";
                break;
            case "TID2":
                SID = "TID2";
                break;
            default: SID = "'" + STRUCKID + "'";
                break;
        }

        if (!string.IsNullOrEmpty(VALUE))
        {
            string[] NCAPACITY = VALUE.Split('/');
            int NPAN = NCAPACITY.Length > 3 ? 3 : NCAPACITY.Length;
            for (int i = 0; i < NPAN; i++)
            {
                if (Decimal.TryParse(NCAPACITY[i] + "", out value))
                {
                    Result += @"INSERT INTO TTRUCK_COMPART (STRUCKID, NCOMPARTNO, NPANLEVEL, NCAPACITY, DCREATE, SCREATE, DUPDATE, SUPDATE) 
VALUES ( " + SID + ", " + NCOMPARTNO + "," + (i + 1) + ", " + (int.Parse(NCAPACITY[i]) * 1000) + ", sysdate, 'IMP_TMS',sysdate, 'IMP_TMS');";
                }
            }
        }
        return Result;

    }

    private string CheckDate(string _DDATE, string Type)
    {
        if (!string.IsNullOrEmpty(_DDATE))
        {
            string FormateDate = "";
            switch (Type)
            {
                case "V": FormateDate = "dd/MM/yyyy";
                    break;
                case "E": FormateDate = "yyyy-MM-dd";
                    break;
            }

            DateTime date;
            DateTime _FIFODATE = DateTime.TryParse(_DDATE, out date) ? date : DateTime.Now;
            if (_FIFODATE.Year < 1500)
            {
                //_FIFODATE = _FIFODATE.AddYears(543);
            }
            else
            {
                _FIFODATE = _FIFODATE.AddYears(-543);
            }

            return _FIFODATE.ToString(FormateDate);
        }
        else
        {
            return "";
        }
    }

    private string CheckDateTime(DateTime sDate)
    {
        int nWork = 543 * 2;

        int nYear = sDate.Year;
        int NextYear = DateTime.Now.Year + 543;
        int Next2Year = DateTime.Now.Year + (nWork);
        if (1900 < nYear && nYear < 2000)//เป็น ค.ศ.
        {
            return String.Format("{0:dd/MM/yyyy}", sDate.AddYears(-543));
        }
        else if (1900 < nYear && nYear < NextYear)//เป็น พ.ศ. เพื่อหา ค.ศ.
        {
            return String.Format("{0:dd/MM/yyyy}", sDate.AddYears(-nWork));
        }
        else if (NextYear < nYear && nYear <= Next2Year)//ถ้าปีถูกเบิ้ลมา 2 ครั้ง เพื่อหา ค.ศ.
        {
            return String.Format("{0:dd/MM/yyyy}", sDate.AddYears(-(nWork + 543)));
        }
        else
        {
            return "";
        }
    }


    //private string CheckFormateDate()
    //{
    //    if (DateTime.TryParseExact(VALUE, "MM/dd/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
    //    {
    //        sDate = CheckDateTime(DateSet);
    //        Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
    //    }
    //    else if (DateTime.TryParseExact(VALUE, "MM/dd/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
    //    {
    //        sDate = CheckDateTime(DateSet);
    //        Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
    //    }
    //    else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy HH:mm:ss", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
    //    {
    //        sDate = CheckDateTime(DateSet);
    //        Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
    //    }
    //    else if (DateTime.TryParseExact(VALUE, "dd/MM/yyyy", new CultureInfo(formateDelivery), DateTimeStyles.None, out DateSet))
    //    {
    //        sDate = CheckDateTime(DateSet);
    //        Result = !string.IsNullOrEmpty(sDate) ? "TO_DATE('" + sDate + "','dd/MM/yyyy ')" : "null";
    //    }
    //    else
    //    {
    //        Result = "null";
    //    }
    //}

}