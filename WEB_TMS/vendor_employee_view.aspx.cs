﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DevExpress.Web.ASPxUploadControl;
using System.Globalization;
using System.IO;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;


public partial class vendor_employee_view : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;

    //อับโลหดไฟล์ไปที่Te,p
    const string TempDirectory = "UploadFile/EmpployeeDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory2 = "UploadFile/EmpployeeDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory3 = "UploadFile/EmpployeeDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory4 = "UploadFile/EmpployeeDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory5 = "UploadFile/EmpployeeDoc/Temp/{0}/{2}/{1}/";
    //อับโลหดไฟล์ไปที่Saver
    const string SaverDirectory = "UploadFile/EmpployeeDoc/IDcard/{0}/{2}/{1}/";
    const string SaverDirectory2 = "UploadFile/EmpployeeDoc/Registration/{0}/{2}/{1}/";
    const string SaverDirectory3 = "UploadFile/EmpployeeDoc/Driverlicense/{0}/{2}/{1}/";
    const string SaverDirectory4 = "UploadFile/EmpployeeDoc/Other/{0}/{2}/{1}/";
    const string SaverDirectory5 = "UploadFile/EmpployeeDoc/PictureEMP/{0}/{2}/{1}/";
    //อับโลหดไฟล์History
    const string HistoryDirectory = "UploadFile/EmpployeeDoc/History/IDcard/{0}/{2}/{1}/";
    const string HistoryDirectory2 = "UploadFile/EmpployeeDoc/History/Registration/{0}/{2}/{1}/";
    const string HistoryDirectory3 = "UploadFile/EmpployeeDoc/History/Driverlicense/{0}/{2}/{1}/";
    const string HistoryDirectory4 = "UploadFile/EmpployeeDoc/History/Other/{0}/{2}/{1}/";
    const string HistoryDirectory5 = "UploadFile/EmpployeeDoc/History/PictureEMP/{0}/{2}/{1}/";

    static List<ListGridDoc> listGriddoc = new List<ListGridDoc>();
    static List<sEmployee> listEmp = new List<sEmployee>();
    static List<historyEmployee> historyEmp = new List<historyEmployee>();



    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            listGriddoc.RemoveAll(o => 1 == 1);
            string str = Request.QueryString["str"];
            string[] strQuery;
            string VenId = "";
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                ViewState["setdata"] = strQuery[0];
                VenId = strQuery[0];
            }
            else
            {
                ViewState["setdata"] = Session["Backpage"] + "";
                Session.Remove("Backpage");
            }

            #region เช็ค Permission
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("47", "R", "เปิดดูข้อมูลหน้า เรียกดูข้อมูลพนักงาน", ViewState["setdata"] + "");

            #endregion

            cboPosition.DataSource = sds;
            cboPosition.DataBind();
            cboPosition.Items.Insert(0, new ListEditItem("-เลือกตำแหน่ง-", null));
            cboPosition.SelectedIndex = 0;

            //cboCompany.DataSource = sdsCompany;
            //cboCompany.DataBind();
            //cboCompany.Items.Insert(0, new ListEditItem("-เลือกบริษัท-"));
            //cboCompany.SelectedIndex = 0;
            setdata();
            VisibleControlUpload();

            #region checkว่าเคยเข้ามาดุข้อมูลไหม
            //ViewState["CheckIn"] = "1";
            string strCheckdata = @"SELECT SEMPLOYEEID,CHECKIN
                                            FROM TEMPLOYEE WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(VenId) + @"' AND CHECKIN ='1' ";


            DataTable dtchk = CommonFunction.Get_Data(conn, strCheckdata);
            if (dtchk.Rows.Count > 0)
            {

            }
            else
            {
                string strCompany = @"UPDATE TEMPLOYEE
                                         SET CHECKIN=:cCheckIn
                                         WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(VenId) + "'";
                using (OracleConnection con = new OracleConnection(conn))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    else
                    {

                    }
                    DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                    if (dt.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        //AddhistoryVendor(VenId);
                        using (OracleCommand com1 = new OracleCommand(strCompany, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":cCheckIn", OracleType.Char).Value = "1";
                            com1.ExecuteNonQuery();
                        }
                    }
                }
            }

            #endregion
        }



    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Setdata();
     
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] param = e.Parameter.Split(';');
        string EMPID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        //string Line_no = "";
        string SDOCID = ViewState["SDOCID"] + "";
        string SDOCTYPE = ViewState["SDOCTYPE"] + "";
        string SDOCID2 = ViewState["SDOCID2"] + "";
        string SDOCTYPE2 = ViewState["SDOCTYPE2"] + "";
        string SDOCID3 = ViewState["SDOCID3"] + "";
        string SDOCTYPE3 = ViewState["SDOCTYPE3"] + "";
        string SDOCID4 = ViewState["SDOCID4"] + "";
        string SDOCTYPE4 = ViewState["SDOCTYPE4"] + "";


        switch (param[0])
        {


            case "Save":

                using (OracleConnection con = new OracleConnection(conn))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    else
                    {

                    }

                    string chkFILL = "";
                    DateTime? BirthDay = null;
                    DateTime? startPercode = null;
                    DateTime? endPercode = null;
                    if (!string.IsNullOrEmpty(dedtBirthDay.Text))
                    {
                        BirthDay = DateTime.Parse(dedtBirthDay.Text);

                    }
                    if (!string.IsNullOrEmpty(dedtstartPercode.Text))
                    {
                        startPercode = DateTime.Parse(dedtstartPercode.Text);
                    }
                    if (!string.IsNullOrEmpty(dedtendPercode.Text))
                    {
                        endPercode = DateTime.Parse(dedtendPercode.Text);
                    }
                    if (chkNotfill.Checked == true)
                    {
                        chkFILL = "1";
                    }
                    else
                    {
                        chkFILL = "0";
                    }
                    string CAUSESAPCOMMIT = "";
                    string BANSTATUS = "";
                    string CAUSESAP = "";
                    string CANCELSTATUS = "";
                    string CAUSESAPCANCEL = "";
                    switch (rblStatus.SelectedIndex)
                    {   //อนุญาติ
                        case 0:
                            CAUSESAPCOMMIT = txtConfirm.Text;

                            break;
                        //ระงับ
                        case 1:
                            if (cboStatus.SelectedItem != null)
                            {
                                BANSTATUS = cboStatus.SelectedItem.Value + "";
                            }
                            else
                            {

                            }
                            CAUSESAP = txtComment.Text;
                            break;
                        //ยกเลิก
                        case 2:
                            if (cboStatus2.SelectedItem != null)
                            {
                                CANCELSTATUS = cboStatus2.SelectedItem.Value + "";
                            }
                            else
                            {
                            }
                            CAUSESAPCANCEL = txtstatus2.Text;
                            break;
                    }


                    var chkEmps = listEmp.ToList().Where(w => w.SEMPLOYEEID == EMPID && w.DBIRTHDATE == BirthDay && w.PERSONEL_BEGIN == startPercode && w.PERSONEL_EXPIRE == endPercode
                     && w.STEL == txtTel.Text && w.STEL2 == txtTel2.Text && w.SMAIL == txtMail.Text
                     && w.SMAIL2 == txtMail2.Text && w.SEMPTPYE == cboPosition.Value + ""
                     && w.CAUSESAP == CAUSESAP && w.CAUSEOVER == txtComment2.Text
                     && w.SFILENAME == txtEMPFileName.Text && w.SSYSFILENAME == txtEMPSysfilename.Text && w.SPATH == txtEMPTruePath.Text && w.NOTFILL == chkFILL
                     && w.BANSTATUS == BANSTATUS && w.CANCELSTATUS == CANCELSTATUS && w.CAUSESAPCANCEL == CAUSESAPCANCEL && w.CAUSESAPCOMMIT == CAUSESAPCOMMIT);


                    if (chkEmps.Count() > 0)
                    {

                    }
                    else
                    {
                        //เช็คว่ามีข้อมูลในList หรือไม่ถ้ามีแสดงว่ามีการอัพรูปประจำตัว
                        if (listGriddoc.Count > 0)
                        {
                            //เช็คว่ามีรูปประจำตัวอยู่แล้วหรือไม่
                            if (!string.IsNullOrEmpty(UpEMP.Text))
                            {
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory5, Session["SVDID"] + "", "uploadEMP", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = UpEMP.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(UpEMP.Text, HistoryPath, OldFile[ncol]);
                                AddhistoryEmployeeAvatarDoc(EMPID, USER);
                            }
                        }
                        AddhistoryEMP(EMPID);



                        string UpdateEmployee = @"UPDATE TEMPLOYEE
                                         SET SEMPTPYE=:SEMPTPYE
                                        ,DBIRTHDATE=:DBIRTHDATE
                                        ,STEL=:STEL
                                        ,STEL2=:STEL2
                                        ,SMAIL=:SMAIL
                                        ,SMAIL2=:SMAIL2
                                        ,PERSONEL_BEGIN=:PERSONEL_BEGIN
                                        ,PERSONEL_EXPIRE=:PERSONEL_EXPIRE
                                        ,DUPDATE=sysdate
                                        ,SUPDATE=:SUPDATE
                                        ,CACTIVE=:CACTIVE
                                        ,CAUSESAP=:CAUSESAP
                                        ,CAUSEOVER=:CAUSEOVER
                                        ,SFILENAME=:SFILENAME
                                        ,SSYSFILENAME=:SSYSFILENAME
                                        ,SPATH=:SPATH
                                        ,NOTFILL=:NOTFILL
                                        ,BANSTATUS=:BANSTATUS
                                        ,CAUSESAPCANCEL=:CAUSESAPCANCEL
                                        ,CANCELSTATUS=:CANCELSTATUS
                                        ,CAUSESAPCOMMIT=:CAUSESAPCOMMIT                
                                         WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(EMPID) + "'";

                        using (OracleCommand com1 = new OracleCommand(UpdateEmployee, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SEMPTPYE", OracleType.Number).Value = cboPosition.Value != null ? cboPosition.Value : DBNull.Value;
                            com1.Parameters.Add(":DBIRTHDATE", OracleType.DateTime).Value = dedtBirthDay.Value != null ? dedtBirthDay.Value : DBNull.Value;
                            com1.Parameters.Add(":STEL", OracleType.VarChar).Value = txtTel.Text;
                            com1.Parameters.Add(":STEL2", OracleType.VarChar).Value = txtTel2.Text;
                            com1.Parameters.Add(":SMAIL", OracleType.VarChar).Value = txtMail.Text;
                            com1.Parameters.Add(":SMAIL2", OracleType.VarChar).Value = txtMail2.Text;
                            com1.Parameters.Add(":PERSONEL_BEGIN", OracleType.DateTime).Value = dedtstartPercode.Value != null ? dedtstartPercode.Value : DBNull.Value;
                            com1.Parameters.Add(":PERSONEL_EXPIRE", OracleType.DateTime).Value = dedtendPercode.Value != null ? dedtendPercode.Value : DBNull.Value;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = rblStatus.Value + "";
                            com1.Parameters.Add(":CAUSEOVER", OracleType.VarChar).Value = txtComment2.Text;
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtEMPFileName.Text;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtEMPSysfilename.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtEMPTruePath.Text;
                            com1.Parameters.Add(":NOTFILL", OracleType.VarChar).Value = chkNotfill.Checked == true ? "1" : "0";

                            //ระงับการใช้งาน
                            com1.Parameters.Add(":BANSTATUS", OracleType.VarChar).Value = BANSTATUS;
                            com1.Parameters.Add(":CAUSESAP", OracleType.VarChar).Value = CAUSESAP;

                            //กรณียกเลิกการใช้งาน
                            com1.Parameters.Add(":CANCELSTATUS", OracleType.VarChar).Value = CANCELSTATUS;
                            com1.Parameters.Add(":CAUSESAPCANCEL", OracleType.VarChar).Value = CAUSESAPCANCEL;

                            //กรณีอนุญาต
                            com1.Parameters.Add(":CAUSESAPCOMMIT", OracleType.VarChar).Value = CAUSESAPCOMMIT;
                            com1.ExecuteNonQuery();
                        }

                    }




                    #region saveUpload


                    string InsUpload = @"INSERT INTO TEMPLOYEE_DOC(SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE ,SFILENAME
                                                           , SDESCRIPTION, CACTIVE ,DCREATE ,SCREATE , DEXPIRE,SPATH ,SSYSFILENAME) 
                                                           VALUES (:SEMPLOYEEID,FC_GENID_EMPLOYEE_DOC(), :SDOCVERSION , :SDOCTYPE ,:SFILENAME
                                                           ,  :SDESCRIPTION, :CACTIVE, sysdate,:SCREATE ,:DEXPIRE,:SPATH,:SSYSFILENAME)";

                    string UpdateUpload = @"UPDATE TEMPLOYEE_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "'";

                    #region Upload1
                    string CheckUpload1 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename.Text) + "'";


                    if (!string.IsNullOrEmpty(txtFileName.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload1);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate.Text))
                            {

                                //AddhistoryVendorDoc(VendorID, SDOCID, SDOCTYPE);
                                AddhistoryEmployeeDoc(EMPID, SDOCID, SDOCTYPE);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;

                                    com1.ExecuteNonQuery();

                                }

                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up1.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "1";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารสำเนาบัตรประชาชน";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        AddhistoryEmployeeDoc(EMPID, SDOCID, SDOCTYPE);
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;

                            com1.ExecuteNonQuery();

                        }
                    }

                    #endregion

                    #region Upload2

                    string UpdateUpload2 = @"UPDATE TEMPLOYEE_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE2) + "'";

                    string CheckUpload2 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE2) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName2.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath2.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename2.Text) + "'";

                    if (!string.IsNullOrEmpty(txtFileName2.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload2);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate2.Text))
                            {


                                AddhistoryEmployeeDoc(EMPID, SDOCID2, SDOCTYPE2);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                    com1.ExecuteNonQuery();

                                }
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up2.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "2";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารทะเบียนบ้าน";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        AddhistoryEmployeeDoc(EMPID, SDOCID2, SDOCTYPE2);
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                            com1.ExecuteNonQuery();

                        }
                    }
                    #endregion

                    #region Upload3
                    string UpdateUpload3 = @"UPDATE TEMPLOYEE_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE3) + "'";

                    string CheckUpload3 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE3) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName3.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath3.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename3.Text) + "'";
                    if (!string.IsNullOrEmpty(txtFileName3.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload3);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate3.Text))
                            {
                                AddhistoryEmployeeDoc(EMPID, SDOCID3, SDOCTYPE3);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                    com1.ExecuteNonQuery();

                                }
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up3.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "3";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารใบขับขี่ประเภท4";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        AddhistoryEmployeeDoc(EMPID, SDOCID3, SDOCTYPE3);
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;

                            com1.ExecuteNonQuery();

                        }
                    }
                    #endregion

                    #region Upload4
                    string UpdateUpload4 = @"UPDATE TEMPLOYEE_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID4) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE4) + @"'";

                    string CheckUpload4 = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TEMPLOYEE_DOC 
                                            WHERE SEMPLOYEEID ='" + CommonFunction.ReplaceInjection(EMPID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID4) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE4) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName4.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath4.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename4.Text) + "'";

                    if (!string.IsNullOrEmpty(txtFileName4.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload4);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate4.Text))
                            {
                                AddhistoryEmployeeDoc(EMPID, SDOCID4, SDOCTYPE4);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;
                                    com1.ExecuteNonQuery();

                                }
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploader4", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up4.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up4.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SEMPLOYEEID", OracleType.VarChar).Value = EMPID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "4";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "แนบเอกสารสำคัญอื่นๆ";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }

                        //ListgvwDoc();
                        //gvwDoc.DataBind();

                    }
                    else
                    {
                        AddhistoryEmployeeDoc(EMPID, SDOCID4, SDOCTYPE4);
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                        {

                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;

                            com1.ExecuteNonQuery();

                        }
                    }
                    #endregion


                    #endregion



                    FileToServer();
                    con.Close();
                    VisibleControlUpload();
                    setdata();
                    con.Close();

                    xcpn.JSProperties["cpRedirectTo"] = "vendor_employee.aspx";
                }
                break;
            #region ลบไฟล์ที่อับโหลด
            case "deleteFile":

                string FilePath = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                }

                string cNo = param[2];
                if (cNo == "1")
                {
                    txtFileName.Text = "";
                    txtFilePath.Text = "";
                    txtTruePath.Text = "";
                    txtSysfilename.Text = "";
                }
                txtValidate.Text = "";

                VisibleControlUpload();
                break;

            case "deleteFile2":

                string FilePath2 = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath2.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath2.Replace("/", "\\"));
                }

                string cNo2 = param[2];
                if (cNo2 == "1")
                {
                    txtFileName2.Text = "";
                    txtFilePath2.Text = "";
                    txtTruePath2.Text = "";
                    txtSysfilename2.Text = "";
                    //dedtUpload2.Value = "";
                }

                txtValidate2.Text = "";
                VisibleControlUpload();
                break;

            case "deleteFile3":

                string FilePath3 = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath3.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath3.Replace("/", "\\"));
                }

                string cNo3 = param[2];
                if (cNo3 == "1")
                {
                    txtFileName3.Text = "";
                    txtFilePath3.Text = "";
                    txtTruePath3.Text = "";
                    txtSysfilename3.Text = "";
                    //dedtUpload3.Value = "";
                }

                VisibleControlUpload();

                break;

            case "deleteFile4":

                string FilePath4 = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath4.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath4.Replace("/", "\\"));
                }

                string cNo4 = param[2];
                if (cNo4 == "1")
                {
                    txtFileName4.Text = "";
                    txtFilePath4.Text = "";
                    txtTruePath4.Text = "";
                    txtSysfilename4.Text = "";
                }

                VisibleControlUpload();
                break;



            #endregion

            #region chagevalidate

            case "chagevalidate":
                string Cbovalue = "";
                if (!string.IsNullOrEmpty(cboPosition.Value.ToString()))
                {
                    Cbovalue = cboPosition.Value + "";
                }

                switch (Cbovalue)
                {
                    //พขร. ประจำรถ
                    case "11":
                        //บอร์โทรศัพท์หลัก
                        txtTel.ValidationSettings.RequiredField.IsRequired = false;
                        //txtTel.ValidationSettings.Display = Display.Dynamic;
                        //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtTel.ValidationSettings.SetFocusOnError = true;
                        //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                        txtTel.ValidationSettings.ValidationGroup = "add";
                        //E-Mail หลัก
                        txtMail.ValidationSettings.RequiredField.IsRequired = false;
                        //txtMail.ValidationSettings.Display = Display.Dynamic;
                        //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtMail.ValidationSettings.SetFocusOnError = true;
                        //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                        txtMail.ValidationSettings.ValidationGroup = "add";
                        //หมายเลขใบขับขี่ประเภท 4
                        txtdrivelicence.ValidationSettings.RequiredField.IsRequired = true;
                        //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                        //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                        //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                        txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                        dedtstartlicence.ValidationSettings.RequiredField.IsRequired = true;
                        //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                        dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                        //dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                        dedtEndlicence.ValidationSettings.RequiredField.IsRequired = true;
                        //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                        dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                        //เอกสารใบขับขี่ประเภท 4
                        txtValidate3.ValidationSettings.RequiredField.IsRequired = true;
                        //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                        //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtValidate3.ValidationSettings.SetFocusOnError = true;
                        //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                        txtValidate3.ValidationSettings.ValidationGroup = "add";
                        break;
                    //พนักงานติดรถ
                    case "12":
                        //บอร์โทรศัพท์หลัก
                        txtTel.ValidationSettings.RequiredField.IsRequired = false;
                        //txtTel.ValidationSettings.Display = Display.Dynamic;
                        //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtTel.ValidationSettings.SetFocusOnError = true;
                        // txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                        txtTel.ValidationSettings.ValidationGroup = "add";
                        //E-Mail หลัก
                        txtMail.ValidationSettings.RequiredField.IsRequired = false;
                        //txtMail.ValidationSettings.Display = Display.Dynamic;
                        //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtMail.ValidationSettings.SetFocusOnError = true;
                        //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                        txtMail.ValidationSettings.ValidationGroup = "add";
                        //หมายเลขใบขับขี่ประเภท 4
                        txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                        //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                        //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                        //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                        txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                        dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                        dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                        dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                        dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                        //เอกสารใบขับขี่ประเภท 4
                        txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                        //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                        //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtValidate3.ValidationSettings.SetFocusOnError = true;
                        //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                        txtValidate3.ValidationSettings.ValidationGroup = "add";
                        break;
                    //ผู้ประสานงาน
                    case "10":
                        //บอร์โทรศัพท์หลัก
                        //txtTel.ValidationSettings.RequiredField.IsRequired = true;
                        //txtTel.ValidationSettings.Display = Display.Dynamic;
                        //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        txtTel.ValidationSettings.SetFocusOnError = true;
                        //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                        txtTel.ValidationSettings.ValidationGroup = "add";
                        //E-Mail หลัก
                        txtMail.ValidationSettings.RequiredField.IsRequired = true;
                        //txtMail.ValidationSettings.Display = Display.Dynamic;
                        //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtMail.ValidationSettings.SetFocusOnError = true;
                        //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                        //txtMail.ValidationSettings.ValidationGroup = "add";
                        //หมายเลขใบขับขี่ประเภท 4
                        txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                        //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                        //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                        //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                        txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                        dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                        dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                        dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                        dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                        //เอกสารใบขับขี่ประเภท 4
                        txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                        //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                        //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtValidate3.ValidationSettings.SetFocusOnError = true;
                        //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                        txtValidate3.ValidationSettings.ValidationGroup = "add";
                        break;
                    //ตัวแทนผู้ขนส่ง
                    case "13":
                        //บอร์โทรศัพท์หลัก
                        txtTel.ValidationSettings.RequiredField.IsRequired = true;
                        //txtTel.ValidationSettings.Display = Display.Dynamic;
                        //txtTel.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtTel.ValidationSettings.SetFocusOnError = true;
                        //txtTel.ValidationSettings.ErrorText = "กรุณาระบุเบอร์โทรศัพท์หลัก";
                        txtTel.ValidationSettings.ValidationGroup = "add";
                        //E-Mail หลัก
                        txtMail.ValidationSettings.RequiredField.IsRequired = true;
                        //txtMail.ValidationSettings.Display = Display.Dynamic;
                        //txtMail.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtMail.ValidationSettings.SetFocusOnError = true;
                        //txtMail.ValidationSettings.ErrorText = "กรุณาระบุ E-Mail หลัก";
                        txtMail.ValidationSettings.ValidationGroup = "add";
                        //หมายเลขใบขับขี่ประเภท 4
                        txtdrivelicence.ValidationSettings.RequiredField.IsRequired = false;
                        //txtdrivelicence.ValidationSettings.Display = Display.Dynamic;
                        //txtdrivelicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtdrivelicence.ValidationSettings.SetFocusOnError = true;
                        //txtdrivelicence.ValidationSettings.ErrorText = "กรุณาระบุหมายเลขใบขับขี่ประเภท 4";
                        txtdrivelicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลาอนุญาตใบขับขี่ประเภท 4
                        dedtstartlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtstartlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtstartlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtstartlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtstartlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลาอนุญาตใบขับขี่ประเภท 4";
                        dedtstartlicence.ValidationSettings.ValidationGroup = "add";
                        //ช่วงระยะเวลายกเลิกอนุญาตใบขับขี่ประเภท 4
                        dedtEndlicence.ValidationSettings.RequiredField.IsRequired = false;
                        //dedtEndlicence.ValidationSettings.Display = Display.Dynamic;
                        //dedtEndlicence.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //dedtEndlicence.ValidationSettings.SetFocusOnError = true;
                        //dedtEndlicence.ValidationSettings.ErrorText = "กรุณาระบุเวลายกเลิกอนุญาตใบขับขี่ประเภท 4";
                        dedtEndlicence.ValidationSettings.ValidationGroup = "add";
                        //เอกสารใบขับขี่ประเภท 4
                        txtValidate3.ValidationSettings.RequiredField.IsRequired = false;
                        //txtValidate3.ValidationSettings.Display = Display.Dynamic;
                        //txtValidate3.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        //txtValidate3.ValidationSettings.SetFocusOnError = true;
                        //txtValidate3.ValidationSettings.ErrorText = "กรุณาแนบเอกสารใบขับขี่ประเภท4";
                        txtValidate3.ValidationSettings.ValidationGroup = "add";
                        break;
                    default:
                        //drivelicence.Visible = true;
                        //datedrivelicence.Visible = true;
                        //txtdrivelicence.ValidationSettings.RequiredField.IsRequired = true;
                        //dedtstartlicence.ValidationSettings.RequiredField.IsRequired = true;
                        //dedtEndlicence.ValidationSettings.RequiredField.IsRequired = true;
                        break;

                }

                break;
            #endregion
            case "viewHistoryemp":
                Session["Backpage"] = EMPID;
                xcpn.JSProperties["cpRedirectTo"] = "employee_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + EMPID));
                break;

            case "viewHistory":
                Session["Backpage"] = EMPID;
                xcpn.JSProperties["cpRedirectTo"] = "document_employee_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + EMPID));
                break;
        }

    }

    void setdata()
    {

        string SEMPLOYEEID = ViewState["setdata"] + "";
        string strsql = @"SELECT EMPSAP.SEMPLOYEEID
, EMP.SEMPTPYE
,EMPSAP.FNAME
,EMPSAP.LNAME
,EMP.STEL
,EMP.STEL2
,EMP.SMAIL
,EMP.SMAIL2
,EMP.DBIRTHDATE
,ven.SABBREVIATION
,EMPSAP.PERS_CODE
,EMP.PERSONEL_BEGIN
,EMP.PERSONEL_EXPIRE
,EMP.SDRIVERNO
,EMP.DDRIVEBEGIN
,EMP.DDRIVEEXPIRE
,EMP.DCREATE
,EMP.SCREATE
,EMP.DUPDATE
,EMP.SUPDATE
,EMP.CAUSESAP
,EMP.CAUSEOVER
,EMP.CACTIVE
,EMP.SFILENAME
,EMP.SSYSFILENAME
,EMP.SPATH
,EMP.NOTFILL
,EMP.BANSTATUS
,EMP.CANCELSTATUS
,EMP.CAUSESAPCANCEL
,EMP.CAUSESAPCOMMIT
,EMP.DATE_LASTJOB
FROM  TEMPLOYEE_SAP EMPSAP
LEFT JOIN TEMPLOYEE EMP
ON EMPSAP.SEMPLOYEEID = EMP.SEMPLOYEEID
LEFT JOIN TVENDOR  ven
ON EMP.STRANS_ID = ven.SVENDORID
WHERE EMPSAP.SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";

        DateTime? DBIRTHDATE = null;
        DateTime? PERSONEL_BEGIN = null;
        DateTime? PERSONEL_EXPIRE = null;
        DateTime? DDRIVEBEGIN = null;
        DateTime? DDRIVEEXPIRE = null;
        DateTime? DUPDATE = null;
        DateTime? DCREATE = null;
        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        if (dt.Rows.Count > 0)
        {
            txtEmpID.Text = dt.Rows[0]["SEMPLOYEEID"] + "";
            if (!string.IsNullOrEmpty(dt.Rows[0]["SEMPTPYE"] + ""))
            {
                // cboPosition.SelectedItem.Value = dt.Rows[0]["SEMPTPYE"] + "";
                cboPosition.SelectedItem = cboPosition.Items.FindByValue(dt.Rows[0]["SEMPTPYE"] + "");
            }
            else
            {
                cboPosition.SelectedIndex = 0;
            }
            txtName.Text = dt.Rows[0]["FNAME"] + "";
            txtSurName.Text = dt.Rows[0]["LNAME"] + "";
            txtTel.Text = dt.Rows[0]["STEL"] + "";
            txtTel2.Text = dt.Rows[0]["STEL2"] + "";
            txtMail.Text = dt.Rows[0]["SMAIL"] + "";
            txtMail2.Text = dt.Rows[0]["SMAIL2"] + "";
            if (!string.IsNullOrEmpty(dt.Rows[0]["SABBREVIATION"] + ""))
            {
                cboCompany.Value = dt.Rows[0]["SABBREVIATION"] + "";
            }
            else
            {
                cboCompany.SelectedIndex = 0;
            }
            txtPercode.Text = dt.Rows[0]["PERS_CODE"] + "";


            if (!string.IsNullOrEmpty(dt.Rows[0]["DBIRTHDATE"] + ""))
            {
                DBIRTHDATE = DateTime.Parse(dt.Rows[0]["DBIRTHDATE"] + "").Date;
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["PERSONEL_BEGIN"] + ""))
            {
                PERSONEL_BEGIN = DateTime.Parse(dt.Rows[0]["PERSONEL_BEGIN"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["PERSONEL_EXPIRE"] + ""))
            {
                PERSONEL_EXPIRE = DateTime.Parse(dt.Rows[0]["PERSONEL_EXPIRE"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DDRIVEBEGIN"] + ""))
            {
                DDRIVEBEGIN = DateTime.Parse(dt.Rows[0]["DDRIVEBEGIN"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DDRIVEEXPIRE"] + ""))
            {
                DDRIVEEXPIRE = DateTime.Parse(dt.Rows[0]["DDRIVEEXPIRE"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DCREATE"] + ""))
            {
                DCREATE = DateTime.Parse(dt.Rows[0]["DCREATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DUPDATE"] + ""))
            {
                DUPDATE = DateTime.Parse(dt.Rows[0]["DUPDATE"] + "");
            }

            dedtBirthDay.Value = DBIRTHDATE;
            dedtstartPercode.Value = PERSONEL_BEGIN;
            dedtendPercode.Value = PERSONEL_EXPIRE;
            dedtstartlicence.Value = DDRIVEBEGIN;
            dedtEndlicence.Value = DDRIVEEXPIRE;
            // dedtDEXPIRE13BIS.Value = DEXPIRE13BIS;
            if (!string.IsNullOrEmpty(dt.Rows[0]["CACTIVE"] + ""))
            {
                switch (int.Parse(dt.Rows[0]["CACTIVE"] + ""))
                {
                    //กรณีระงับการใช้งาน
                    case 0: rblStatus.SelectedIndex = 1;
                        Typedefault.Visible = false;
                        Type.Visible = true;
                        status.Visible = true;
                        Type2.Visible = false;
                        status2.Visible = false;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["BANSTATUS"] + ""))
                        {
                            string sBANSTATUS = dt.Rows[0]["BANSTATUS"] + "";
                            //int nBANSTATUS = int.Parse(dt.Rows[0]["BANSTATUS"] + "");
                            cboStatus.DataBind();
                            cboStatus.Value = cboStatus.Items.FindByValue(sBANSTATUS).Value;
                        }
                        else
                        {

                        }
                        txtComment.Text = dt.Rows[0]["CAUSESAP"] + "";

                        //สาเหตุการอนุญาติ
                        //txtConfirm.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtConfirm.ValidationSettings.Display = Display.Dynamic;
                        ////txtConfirm.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtConfirm.ValidationSettings.SetFocusOnError = true;
                        ////txtConfirm.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุการอนุญาต";
                        ////txtConfirm.ValidationSettings.ValidationGroup = "add";
                        ////สาเหตุที่ระงับจากระบบ SAP
                        cboStatus.ValidationSettings.RequiredField.IsRequired = true;
                        ////cboStatus.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus.ValidationSettings.ErrorText = "กรุณาเลือกสาเหตุที่ระงับ";
                        ////cboStatus.ValidationSettings.ValidationGroup = "add";
                        //txtComment.ValidationSettings.RequiredField.IsRequired = true;
                        ////txtComment.ValidationSettings.Display = Display.Dynamic;
                        ////txtComment.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtComment.ValidationSettings.SetFocusOnError = true;
                        ////txtComment.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ระงับ";
                        ////txtComment.ValidationSettings.ValidationGroup = "add";
                        ////ประเภทการยกเลิกใช้งาน
                        cboStatus2.ValidationSettings.RequiredField.IsRequired = false;
                        ////cboStatus2.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus2.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus2.ValidationSettings.ErrorText = "กรุณาเลือกประเภทการยกเลิกใช้งาน";
                        ////txtComment.ValidationSettings.ValidationGroup = "add";
                        //txtComment.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtstatus2.ValidationSettings.Display = Display.Dynamic;
                        ////txtstatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtstatus2.ValidationSettings.SetFocusOnError = true;
                        ////txtstatus2.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ยกเลิกใช้งาน";
                        ////txtstatus2.ValidationSettings.ValidationGroup = "add";
                        break;
                    case 1: rblStatus.SelectedIndex = 0;
                        Typedefault.Visible = true;
                        Type.Visible = false;
                        status.Visible = false;
                        Type2.Visible = false;
                        status2.Visible = false;
                        txtConfirm.Text = dt.Rows[0]["CAUSESAPCOMMIT"] + "";
                        //สาเหตุการอนุญาติ
                        //txtConfirm.ValidationSettings.RequiredField.IsRequired = true;
                        ////txtConfirm.ValidationSettings.Display = Display.Dynamic;
                        ////txtConfirm.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtConfirm.ValidationSettings.SetFocusOnError = true;
                        ////txtConfirm.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุการอนุญาติ ";
                        ////txtConfirm.ValidationSettings.ValidationGroup = "add";
                        ////สาเหตุที่ระงับจากระบบ SAP
                        cboStatus.ValidationSettings.RequiredField.IsRequired = false;
                        ////cboStatus.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus.ValidationSettings.ErrorText = "กรุณาเลือกสาเหตุที่ระงับ";
                        ////cboStatus.ValidationSettings.ValidationGroup = "add";
                        //txtComment.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtComment.ValidationSettings.Display = Display.Dynamic;
                        ////txtComment.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtComment.ValidationSettings.SetFocusOnError = true;
                        ////txtComment.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ระงับ";
                        ////txtComment.ValidationSettings.ValidationGroup = "add";
                        ////ประเภทการยกเลิกใช้งาน
                        cboStatus2.ValidationSettings.RequiredField.IsRequired = false;
                        ////cboStatus2.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus2.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus2.ValidationSettings.ErrorText = "กรุณาเลือกประเภทการยกเลิกใช้งาน";
                        ////cboStatus2.ValidationSettings.ValidationGroup = "add";
                        //txtstatus2.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtstatus2.ValidationSettings.Display = Display.Dynamic;
                        ////txtstatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtstatus2.ValidationSettings.SetFocusOnError = true;
                        ////txtstatus2.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ยกเลิกใช้งาน";
                        ////txtstatus2.ValidationSettings.ValidationGroup = "add";
                        break;
                    case 2: rblStatus.SelectedIndex = 2;
                        Typedefault.Visible = false;
                        Type.Visible = false;
                        status.Visible = false;
                        Type2.Visible = true;
                        status2.Visible = true;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["CANCELSTATUS"] + ""))
                        {
                            //cboStatus.Value = cboStatus.Items.FindByValue(dt.Rows[0]["CANCELSTATUS"] + "");
                            string sCANCELSTATUS = dt.Rows[0]["CANCELSTATUS"] + "";
                            cboStatus2.DataBind();
                            cboStatus2.Value = cboStatus2.Items.FindByValue(sCANCELSTATUS).Value;
                        }
                        else
                        {

                        }
                        txtstatus2.Text = dt.Rows[0]["CAUSESAPCANCEL"] + "";
                        //สาเหตุการอนุญาติ
                        //txtConfirm.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtConfirm.ValidationSettings.Display = Display.Dynamic;
                        ////txtConfirm.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtConfirm.ValidationSettings.SetFocusOnError = true;
                        ////txtConfirm.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุการอนุญาติ";
                        ////txtConfirm.ValidationSettings.ValidationGroup = "add";
                        ////สาเหตุที่ระงับจากระบบ SAP
                        cboStatus.ValidationSettings.RequiredField.IsRequired = false;
                        ////cboStatus.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus.ValidationSettings.ErrorText = "กรุณาเลือกสาเหตุที่ระงับ";
                        ////cboStatus.ValidationSettings.ValidationGroup = "add";
                        //txtComment.ValidationSettings.RequiredField.IsRequired = false;
                        ////txtComment.ValidationSettings.Display = Display.Dynamic;
                        ////txtComment.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtComment.ValidationSettings.SetFocusOnError = true;
                        ////txtComment.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ระงับ";
                        ////txtComment.ValidationSettings.ValidationGroup = "add";
                        ////ประเภทการยกเลิกใช้งาน
                        cboStatus2.ValidationSettings.RequiredField.IsRequired = true;
                        ////cboStatus2.ValidationSettings.Display = Display.Dynamic;
                        ////cboStatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////cboStatus2.ValidationSettings.SetFocusOnError = true;
                        ////cboStatus2.ValidationSettings.ErrorText = "กรุณาเลือกประเภทการยกเลิกใช้งาน";
                        ////cboStatus2.ValidationSettings.ValidationGroup = "add";
                        //txtstatus2.ValidationSettings.RequiredField.IsRequired = true;
                        ////txtstatus2.ValidationSettings.Display = Display.Dynamic;
                        ////txtstatus2.ValidationSettings.ErrorDisplayMode = ErrorDisplayMode.ImageWithTooltip;
                        ////txtstatus2.ValidationSettings.SetFocusOnError = true;
                        ////txtstatus2.ValidationSettings.ErrorText = "กรุณาระบุสาเหตุที่ยกเลิกใช้งาน";
                        ////txtstatus2.ValidationSettings.ValidationGroup = "add";
                        break;
                }

            }

            txtComment2.Text = dt.Rows[0]["CAUSEOVER"] + "";
            if (!string.IsNullOrEmpty(dt.Rows[0]["SFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SSYSFILENAME"] + "") && !string.IsNullOrEmpty(dt.Rows[0]["SPATH"] + ""))
            {
                imgEmp.ImageUrl = dt.Rows[0]["SPATH"] + "" + dt.Rows[0]["SSYSFILENAME"] + "";
            }
            else
            {
                if (!string.IsNullOrEmpty(txtEMPTruePath.Text) && !string.IsNullOrEmpty(txtEMPSysfilename.Text))
                {
                    imgEmp.ImageUrl = txtEMPTruePath.Text + txtEMPSysfilename.Text;
                }
                else
                {
                    imgEmp.ImageUrl = "images/Avatar.png";
                }
            }

            UpEMP.Text = dt.Rows[0]["SPATH"] + "" + dt.Rows[0]["SSYSFILENAME"] + "";
            txtEMPFilePath.Text = dt.Rows[0]["SPATH"] + "";
            txtEMPFileName.Text = dt.Rows[0]["SFILENAME"] + "";
            txtEMPSysfilename.Text = dt.Rows[0]["SSYSFILENAME"] + "";
            txtEMPTruePath.Text = dt.Rows[0]["SPATH"] + "";
            lblDUPDATE.Text = dt.Rows[0]["DUPDATE"] + "" != "" ? dt.Rows[0]["DUPDATE"] + "" + " น." : "-";
            lblSUPDATE.Text = dt.Rows[0]["FNAME"] + "" != "" ? dt.Rows[0]["FNAME"] + "" + " " + dt.Rows[0]["LNAME"] + "" : "-";
            if (dt.Rows[0]["NOTFILL"] + "" == "1")
            {
                chkNotfill.Checked = true;
            }
            else
            {
                chkNotfill.Checked = false;
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DATE_LASTJOB"] + ""))
            {
                lblDayuse.Text = dt.Rows[0]["DATE_LASTJOB"] + "";
                DateTime sDate = DateTime.Parse(dt.Rows[0]["DATE_LASTJOB"] + "");
                string ssDate = sDate.Day + "/" + sDate.Month + "/" + sDate.Year + "";

                string DateRange = "SELECT TRUNC(sysdate) - TRUNC(TO_DATE('" + CommonFunction.ReplaceInjection(ssDate) + "','DD/MM/YYYY')) AS DateLast FROM Dual";
                DataTable dtRange = new DataTable();

                dtRange = CommonFunction.Get_Data(conn, DateRange);
                //DateTime Date = DateTime.Parse(dtRange.Rows[0]["DateLast"] + "");

                if (!string.IsNullOrEmpty(dtRange.Rows[0]["DateLast"] + ""))
                {
                    lbldateuse.Text = dtRange.Rows[0]["DateLast"] + "";
                }
                else
                {
                    lbldateuse.Text = "-";
                }


            }
            else
            {
                lblDayuse.Text = "-";
                lbldateuse.Text = "-";
            }




            #region AdddatatoList Employee
            if (listEmp.Count > 0)
            {
                listEmp.RemoveAt(0);
            }

            listEmp.Add(new sEmployee
            {
                SEMPLOYEEID = dt.Rows[0]["SEMPLOYEEID"] + "",
                SEMPTPYE = dt.Rows[0]["SEMPTPYE"] + "",
                FNAME = dt.Rows[0]["FNAME"] + "",
                LNAME = dt.Rows[0]["LNAME"] + "",
                STEL = dt.Rows[0]["STEL"] + "",
                STEL2 = dt.Rows[0]["STEL2"] + "",
                SMAIL = dt.Rows[0]["SMAIL"] + "",
                SMAIL2 = dt.Rows[0]["SMAIL2"] + "",
                DBIRTHDATE = DBIRTHDATE,
                SABBREVIATION = dt.Rows[0]["SABBREVIATION"] + "",
                PERS_CODE = dt.Rows[0]["PERS_CODE"] + "",
                PERSONEL_BEGIN = PERSONEL_BEGIN,
                PERSONEL_EXPIRE = PERSONEL_EXPIRE,
                SDRIVERNO = dt.Rows[0]["SDRIVERNO"] + "",
                DDRIVEBEGIN = DDRIVEBEGIN,
                DDRIVEEXPIRE = DDRIVEEXPIRE,
                DCREATE = DCREATE,
                SCREATE = dt.Rows[0]["SCREATE"] + "",
                DUPDATE = DUPDATE,
                SUPDATE = dt.Rows[0]["SUPDATE"] + "",
                CAUSESAP = dt.Rows[0]["CAUSESAP"] + "",
                CAUSEOVER = dt.Rows[0]["CAUSEOVER"] + "",
                CACTIVE = dt.Rows[0]["CACTIVE"] + "",
                SPATH = dt.Rows[0]["SPATH"] + "",
                SFILENAME = dt.Rows[0]["SFILENAME"] + "",
                SSYSFILENAME = dt.Rows[0]["SSYSFILENAME"] + "",
                NOTFILL = dt.Rows[0]["NOTFILL"] + "",
                BANSTATUS = dt.Rows[0]["BANSTATUS"] + "",
                CANCELSTATUS = dt.Rows[0]["CANCELSTATUS"] + "",
                CAUSESAPCANCEL = dt.Rows[0]["CAUSESAPCANCEL"] + "",
                CAUSESAPCOMMIT = dt.Rows[0]["CAUSESAPCOMMIT"] + "",
            });
            #endregion
        }


        string HistoryEmp = @"SELECT 
  SEMPLOYEEID, SEMPTPYE, FNAME, 
   LNAME, DBIRTHDATE, STEL, 
   STEL2, SMAIL, SMAIL2, 
   SABBREVIATION, PERS_CODE, PERSONEL_BEGIN, 
   PERSONEL_EXPIRE, SDRIVERNO, DDRIVEBEGIN, 
   DDRIVEEXPIRE, DCREATE, SCREATE, 
   DUPDATE, SUPDATE , NVERSION, 
   SFILENAME, SSYSFILENAME, SPATH,CAUSESAP,CAUSEOVER,CACTIVE,NOTFILL
FROM TEMPLOYEE_HISTORY WHERE SEMPLOYEEID='" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";

        DataTable dt2 = new DataTable();
        dt2 = CommonFunction.Get_Data(conn, HistoryEmp);

        if (dt2.Rows.Count > 0)
        {

            DateTime? sDBIRTHDATE = null;
            DateTime? sPERSONEL_BEGIN = null;
            DateTime? sPERSONEL_EXPIRE = null;
            DateTime? sDDRIVEBEGIN = null;
            DateTime? sDDRIVEEXPIRE = null;
            DateTime? sDUPDATE = null;
            DateTime? sDCREATE = null;

            if (!string.IsNullOrEmpty(dt2.Rows[0]["DBIRTHDATE"] + ""))
            {
                sDBIRTHDATE = DateTime.Parse(dt2.Rows[0]["DBIRTHDATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["PERSONEL_BEGIN"] + ""))
            {
                sPERSONEL_BEGIN = DateTime.Parse(dt2.Rows[0]["PERSONEL_BEGIN"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["PERSONEL_EXPIRE"] + ""))
            {
                sPERSONEL_EXPIRE = DateTime.Parse(dt2.Rows[0]["PERSONEL_EXPIRE"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DDRIVEBEGIN"] + ""))
            {
                sDDRIVEBEGIN = DateTime.Parse(dt2.Rows[0]["DDRIVEBEGIN"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DDRIVEEXPIRE"] + ""))
            {
                sDDRIVEEXPIRE = DateTime.Parse(dt2.Rows[0]["DDRIVEEXPIRE"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DCREATE"] + ""))
            {
                sDCREATE = DateTime.Parse(dt2.Rows[0]["DCREATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DUPDATE"] + ""))
            {
                sDUPDATE = DateTime.Parse(dt2.Rows[0]["DUPDATE"] + "");
            }

            if (dt2.Rows.Count > 0)
            {
                int i;
                for (i = 0; i <= dt2.Rows.Count - 1; i++)
                {
                    #region ADDDATA Hisotry

                    historyEmp.Add(new historyEmployee
                    {

                        SEMPLOYEEID = dt2.Rows[i]["SEMPLOYEEID"] + "",
                        SEMPTPYE = dt2.Rows[i]["SEMPTPYE"] + "",
                        FNAME = dt2.Rows[i]["FNAME"] + "",
                        LNAME = dt2.Rows[i]["LNAME"] + "",
                        DBIRTHDATE = sDBIRTHDATE,
                        STEL = dt2.Rows[i]["STEL"] + "",
                        STEL2 = dt2.Rows[i]["STEL2"] + "",
                        SMAIL = dt2.Rows[i]["SMAIL"] + "",
                        SMAIL2 = dt2.Rows[i]["SMAIL2"] + "",
                        SABBREVIATION = dt2.Rows[i]["SABBREVIATION"] + "",
                        PERS_CODE = dt2.Rows[i]["PERS_CODE"] + "",
                        PERSONEL_BEGIN = sPERSONEL_BEGIN,
                        PERSONEL_EXPIRE = sPERSONEL_EXPIRE,
                        SDRIVERNO = dt2.Rows[i]["SDRIVERNO"] + "",
                        DDRIVEBEGIN = sDDRIVEBEGIN,
                        DDRIVEEXPIRE = sDDRIVEEXPIRE,
                        DCREATE = sDCREATE,
                        SCREATE = dt2.Rows[i]["SCREATE"] + "",
                        DUPDATE = sDUPDATE,
                        SUPDATE = dt2.Rows[i]["SUPDATE"] + "",
                        CAUSESAP = dt2.Rows[i]["CAUSESAP"] + "",
                        CAUSEOVER = dt2.Rows[i]["CAUSEOVER"] + "",
                        CACTIVE = dt2.Rows[i]["CACTIVE"] + "",
                        NVERSION = int.Parse(dt2.Rows[i]["NVERSION"] + ""),
                        SFILENAME = dt2.Rows[i]["SFILENAME"] + "",
                        SSYSFILENAME = dt2.Rows[i]["SSYSFILENAME"] + "",
                        SPATH = dt2.Rows[i]["SPATH"] + "",
                        NOTFILL = dt2.Rows[i]["NOTFILL"] + ""
                    });



                    #endregion
                }
            }
        }


        #region รายละเอียดUpload



        //string VendorID = ViewState["setdata"] + "";
        if (!string.IsNullOrEmpty(SEMPLOYEEID))
        {
            string strUpload = @"SELECT 
   SEMPLOYEEID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, NVERSION, SPATH
FROM TEMPLOYEE_DOC
WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "' ORDER BY SDOCTYPE ASC";
            DataTable dt3 = CommonFunction.Get_Data(conn, strUpload);


            DateTime? DEXPIRE2 = null;
            DateTime? DEXPIRE3 = null;

            //if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
            //{
            //    DEXPIRE3 = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
            //}


            #region setUpload1
            DataRow[] dtrow = dt3.Select("SDOCTYPE = '1'");
            if (dtrow.Count() > 0)
            {
                txtFileName.Text = dtrow[0][4] + "";
                txtFilePath.Text = dtrow[0][14] + "";
                txtTruePath.Text = dtrow[0][14] + "";
                txtSysfilename.Text = dtrow[0][5] + "";
                if (!string.IsNullOrEmpty(txtSysfilename.Text))
                {
                    txtValidate.Text = ".";
                }
                else
                {
                    txtValidate.Text = "";
                }

                Up1.Text = dtrow[0][14] + "" + dtrow[0][5] + "";
                txtchkUpdate.Text = "1";
                ViewState["SDOCID"] = dtrow[0][1] + "";
                ViewState["SDOCTYPE"] = dtrow[0][3] + "";
            }
            else
            {
                txtFileName.Text = "";
                txtFilePath.Text = "";
                txtTruePath.Text = "";
                txtSysfilename.Text = "";
                Up1.Text = "";
                ViewState["SDOCID"] = "";
                ViewState["SDOCTYPE"] = "";
                txtchkUpdate.Text = "";
            }
            #endregion

            #region setupload2
            DataRow[] dtrow2 = dt3.Select("SDOCTYPE = '2'");
            if (dtrow2.Count() > 0)
            {
                txtFileName2.Text = dtrow2[0][4] + "";
                txtFilePath2.Text = dtrow2[0][14] + "";
                txtTruePath2.Text = dtrow2[0][14] + "";
                txtSysfilename2.Text = dtrow2[0][5] + "";
                if (!string.IsNullOrEmpty(txtSysfilename2.Text))
                {
                    txtValidate2.Text = ".";
                }
                else
                {
                    txtValidate2.Text = "";
                }
                Up2.Text = dtrow2[0][14] + "" + dtrow2[0][5] + "";
                txtchkUpdate2.Text = "1";
                ViewState["SDOCID2"] = dtrow2[0][1] + "";
                ViewState["SDOCTYPE2"] = dtrow2[0][3] + "";
                if (!string.IsNullOrEmpty(dtrow2[0][12] + ""))
                {
                    DEXPIRE2 = DateTime.Parse(dtrow2[0][12] + "");
                    //dedtUpload2.Value = DEXPIRE2;
                }

            }
            else
            {
                txtFileName2.Text = "";
                txtFilePath2.Text = "";
                txtTruePath.Text = "";
                txtSysfilename.Text = "";
                Up2.Text = "";
                ViewState["SDOCID2"] = "";
                ViewState["SDOCTYPE2"] = "";
                //dedtUpload2.Value = "";
                txtchkUpdate2.Text = "";

            }
            #endregion

            #region setupload3
            DataRow[] dtrow3 = dt3.Select("SDOCTYPE = '3'");
            if (dtrow3.Count() > 0)
            {
                txtFileName3.Text = dtrow3[0][4] + "";
                txtFilePath3.Text = dtrow3[0][14] + "";
                txtTruePath3.Text = dtrow3[0][14] + "";
                txtSysfilename3.Text = dtrow3[0][5] + "";
                Up3.Text = dtrow3[0][14] + "" + dtrow3[0][5] + "";
                txtchkUpdate3.Text = "1";
                ViewState["SDOCID3"] = dtrow3[0][1] + "";
                ViewState["SDOCTYPE3"] = dtrow3[0][3] + "";
                if (!string.IsNullOrEmpty(dtrow3[0][12] + ""))
                {
                    DEXPIRE3 = DateTime.Parse(dtrow3[0][12] + "");
                    //dedtUpload3.Value = DEXPIRE3;
                }
            }
            else
            {
                txtFileName3.Text = "";
                txtFilePath3.Text = "";
                txtTruePath3.Text = "";
                txtSysfilename3.Text = "";
                Up3.Text = "";
                ViewState["SDOCID3"] = "";
                ViewState["SDOCTYPE3"] = "";
                // dedtUpload3.Value = "";
                txtchkUpdate3.Text = "";
            }
            #endregion

            #region setupload4

            DataRow[] dtrow4 = dt3.Select("SDOCTYPE = '4'");
            if (dtrow4.Count() > 0)
            {
                txtFileName4.Text = dtrow4[0][4] + "";
                txtFilePath4.Text = dtrow4[0][14] + "";
                txtTruePath4.Text = dtrow4[0][14] + "";
                txtSysfilename4.Text = dtrow4[0][5] + "";
                Up4.Text = dtrow4[0][14] + "" + dtrow4[0][5] + "";
                txtchkUpdate4.Text = "1";
                ViewState["SDOCID4"] = dtrow4[0][1] + "";
                ViewState["SDOCTYPE4"] = dtrow4[0][3] + "";
            }
            else
            {
                txtFileName4.Text = "";
                txtFilePath4.Text = "";
                txtTruePath4.Text = "";
                txtSysfilename4.Text = "";
                Up4.Text = "";
                ViewState["SDOCID4"] = "";
                ViewState["SDOCTYPE4"] = "";
                txtchkUpdate4.Text = "";
            }

            #endregion

        }
        #endregion
    }

    #region แนบเอกสารสำเนาบัตรประชาชน

    protected void upload_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region แนบเอกสารทะเบียนบ้าน

    protected void upload2_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region แนบเอกสารใบขับขี่ประเภท4

    protected void upload3_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region แนบเอกสารสำคัญอื่นๆ

    protected void upload4_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];

            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region รูปประจำตัว

    protected void uploadEMP_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFileEmp(e.UploadedFile, genName, string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;
                //  ShowEmpImage(data2, genName + "." + _Filename[count]);
                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGriddoc.RemoveAll(o => 1 == 1);
                listGriddoc.Add(new ListGridDoc
                {
                    SFILENAME = e.UploadedFile.FileName + "",
                    SSYSFILENAME = genName + "." + _Filename[count] + "",
                    SDESCRIPTION = "รูปประจำตัว",
                    SPATH = data2
                });
            }
        }
        else
        {

            return;

        }
    }

    #endregion

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void VisibleControlUpload()
    {
        if (!(string.IsNullOrEmpty(txtFilePath.Text)))
        {
            bool visible = string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = visible;
            txtFileName.ClientVisible = !visible;
            btnView.ClientEnabled = !visible;
            btnDelFile.ClientEnabled = !visible;


            chkUpload1.Value = "1";
        }
        else
        {
            bool visible = !string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = !visible;
            txtFileName.ClientVisible = visible;
            btnView.ClientEnabled = visible;
            btnDelFile.ClientEnabled = visible;
            chkUpload1.Value = "";
        }

        if (!(string.IsNullOrEmpty(txtFilePath2.Text)))
        {
            bool visible2 = string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = visible2;
            txtFileName2.ClientVisible = !visible2;
            btnView2.ClientEnabled = !visible2;
            btnDelFile2.ClientEnabled = !visible2;


            chkUpload2.Value = "1";
        }
        else
        {
            bool visible2 = !string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = !visible2;
            txtFileName2.ClientVisible = visible2;
            btnView2.ClientEnabled = visible2;
            btnDelFile2.ClientEnabled = visible2;
            chkUpload2.Value = "";
        }


        if (!(string.IsNullOrEmpty(txtFilePath3.Text)))
        {
            bool visible3 = string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = visible3;
            txtFileName3.ClientVisible = !visible3;
            btnView3.ClientEnabled = !visible3;
            btnDelFile3.ClientEnabled = !visible3;


            chkUpload3.Value = "1";
        }
        else
        {
            bool visible3 = !string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = !visible3;
            txtFileName3.ClientVisible = visible3;
            btnView3.ClientEnabled = visible3;
            btnDelFile3.ClientEnabled = visible3;
            chkUpload3.Value = "";
        }


        if (!(string.IsNullOrEmpty(txtFilePath4.Text)))
        {

            bool visible4 = string.IsNullOrEmpty(txtFilePath4.Text);
            uploader4.ClientVisible = visible4;
            txtFileName4.ClientVisible = !visible4;
            btnView4.ClientEnabled = !visible4;
            btnDelFile4.ClientEnabled = !visible4;


            chkUpload4.Value = "1";
        }
        else
        {
            bool visible4 = !string.IsNullOrEmpty(txtFilePath4.Text);
            uploader4.ClientVisible = !visible4;
            txtFileName4.ClientVisible = visible4;
            btnView4.ClientEnabled = visible4;
            btnDelFile4.ClientEnabled = visible4;
            chkUpload4.Value = "";
        }

    }

    private void AddhistoryEMP(string EMPID)
    {
        var SendData = listEmp.Where(w => w.SEMPLOYEEID == EMPID).FirstOrDefault();

        //แปลงค่า SEMPTPYE
        string SEMPTPYE = "";
        if (!string.IsNullOrEmpty(SendData + ""))
        {
            int num = 0;
            int EMPTPYE = int.TryParse(SendData.SEMPTPYE, out num) ? num : 0;

            if (EMPTPYE == 0)
            {
                SEMPTPYE = "null";
            }
            else
            {
                SEMPTPYE = EMPTPYE + "";
            }


        }
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            //เช็คว่ามีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (SendData != null)
            {
                var chkHistory = historyEmp.Where(w => w.SEMPLOYEEID == EMPID).OrderByDescending(o => o.NVERSION).FirstOrDefault();



                //เช็คว่า HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (chkHistory != null)
                {
                    //ถ้าเคยมีข้อมูลแล้ว

                    int NVERSION = int.Parse(chkHistory.NVERSION + "") + 1;


                    string strQuery = @"INSERT INTO TEMPLOYEE_HISTORY(SEMPLOYEEID,SEMPTPYE,FNAME,LNAME,DBIRTHDATE,STEL,STEL2
                                      ,SMAIL,SMAIL2,SABBREVIATION,PERS_CODE,PERSONEL_BEGIN,PERSONEL_EXPIRE,SDRIVERNO,DDRIVEBEGIN,DDRIVEEXPIRE
                                      ,DCREATE,SCREATE,DUPDATE,SUPDATE,NVERSION,SFILENAME,SSYSFILENAME,SPATH,CAUSESAP,CAUSEOVER,CACTIVE,NOTFILL,BANSTATUS,CANCELSTATUS,CAUSESAPCANCEL,CAUSESAPCOMMIT) 
                                 VALUES('" + CommonFunction.ReplaceInjection(SendData.SEMPLOYEEID) + @"'
                                  ," + CommonFunction.ReplaceInjection(SEMPTPYE) + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.FNAME) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.LNAME) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DBIRTHDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL2) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL2) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SABBREVIATION) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.PERS_CODE) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_BEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_EXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SDRIVERNO) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEBEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEEXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DCREATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SCREATE) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DUPDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SUPDATE) + @"'
                                  ," + CommonFunction.ReplaceInjection(NVERSION + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SFILENAME) + @"' 
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SSYSFILENAME) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SPATH) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAP) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSEOVER) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CACTIVE) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.NOTFILL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.BANSTATUS) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CANCELSTATUS) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCANCEL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCOMMIT) + @"')";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล

                    //DateTime? DSTARTPTT = null;
                    //DateTime? DBEGINTRANSPORT = null;
                    //DateTime? DEXPIRETRANSPORT = null;
                    //DateTime? DBEGIN13BIS = null;
                    //DateTime? DEXPIRE13BIS = null;


                    string strQuery = @"INSERT INTO TEMPLOYEE_HISTORY(SEMPLOYEEID,SEMPTPYE,FNAME,LNAME,DBIRTHDATE,STEL,STEL2
                                      ,SMAIL,SMAIL2,SABBREVIATION,PERS_CODE,PERSONEL_BEGIN,PERSONEL_EXPIRE,SDRIVERNO,DDRIVEBEGIN,DDRIVEEXPIRE
                                      ,DCREATE,SCREATE,DUPDATE,SUPDATE,NVERSION,SFILENAME,SSYSFILENAME,SPATH,CAUSESAP,CAUSEOVER,CACTIVE,NOTFILL,BANSTATUS,CANCELSTATUS,CAUSESAPCANCEL,CAUSESAPCOMMIT) 
                                  VALUES('" + CommonFunction.ReplaceInjection(SendData.SEMPLOYEEID) + @"'
                                  ," + CommonFunction.ReplaceInjection(SEMPTPYE) + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.FNAME) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.LNAME) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DBIRTHDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.STEL2) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SMAIL2) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SABBREVIATION) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.PERS_CODE) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_BEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.PERSONEL_EXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SDRIVERNO) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEBEGIN + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DDRIVEEXPIRE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DCREATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SCREATE) + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((SendData.DUPDATE + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )" + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SUPDATE) + @"'
                                  ," + CommonFunction.ReplaceInjection("1") + @"
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SFILENAME) + @"' 
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SSYSFILENAME) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.SPATH) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAP) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSEOVER) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CACTIVE) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.NOTFILL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.BANSTATUS) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CANCELSTATUS) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCANCEL) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SendData.CAUSESAPCOMMIT) + @"')";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void UploadFile2History(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath);
            }
        }
    }

    private void UploadFileToServer(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath + filename))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old + filename, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath + filename.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath + filename);
            }
        }
    }

    void FileToServer()
    {
        #region เก็บPathลงviewstate
        if (txtFilePath.Text != "")
        {

            UploadFileToServer(txtFilePath.Text, txtTruePath.Text, txtSysfilename.Text);
        }
        if (txtFilePath2.Text != "")
        {


            UploadFileToServer(txtFilePath2.Text, txtTruePath2.Text, txtSysfilename2.Text);
        }
        if (txtFilePath3.Text != "")
        {


            UploadFileToServer(txtFilePath3.Text, txtTruePath3.Text, txtSysfilename3.Text);
        }
        if (txtFilePath4.Text != "")
        {

            UploadFileToServer(txtFilePath4.Text, txtTruePath4.Text, txtSysfilename4.Text);
        }
        #endregion
    }

    private bool UploadFileEmp(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void AddhistoryEmployeeDoc(string SEMPLOYEEID, string SDOCID, string SDOCTYPE)
    {
        string senddatatohistory = @"SELECT  SEMPLOYEEID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
                                      FROM TEMPLOYEE_DOC WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "' ";

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT SEMPLOYEEID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                      SCREATE, DUPDATE, SUPDATE,DEXPIRE, NVERSION,DATERECEIVE
                                      FROM TEMPLOYEE_DOC_HISTORY WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"' 
                                      ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                DateTime? DCREATE = null;
                DateTime? DUPDATE = null;
                if (!string.IsNullOrEmpty(dt2.Rows[0]["DCREATE"] + ""))
                {
                    DCREATE = DateTime.Parse(dt2.Rows[0]["DCREATE"] + "");
                }
                if (!string.IsNullOrEmpty(dt2.Rows[0]["DUPDATE"] + ""))
                {
                    DUPDATE = DateTime.Parse(dt2.Rows[0]["DUPDATE"] + "");
                }

                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

                    string strQuery = @"INSERT INTO TEMPLOYEE_DOC_HISTORY(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SEMPLOYEEID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TEMPLOYEE_DOC_HISTORY(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE ,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SEMPLOYEEID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void AddhistoryEmployeeAvatarDoc(string SEMPLOYEEID, string USER)
    {
        string senddatatohistory = @"SELECT 
   ROWID, SEMPLOYEEID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH, NVERSION
FROM TEMPLOYEE_AVATAR_DOC
WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"' ORDER BY NVERSION DESC ";

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            //var item = listGriddoc.FirstOrDefault();
            // var SendData = listEmp.Where(w => w.SEMPLOYEEID == SEMPLOYEEID).FirstOrDefault();
            string query = @"SELECT 
   SEMPLOYEEID,SFILENAME,SSYSFILENAME,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,SPATH
FROM TEMPLOYEE where SEMPLOYEEID='" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";

            DataTable dt2 = CommonFunction.Get_Data(con, query);

            DateTime? DCREATE = null;
            DateTime? DUPDATE = null;
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DCREATE"] + ""))
            {
                DCREATE = DateTime.Parse(dt2.Rows[0]["DCREATE"] + "");
            }
            if (!string.IsNullOrEmpty(dt2.Rows[0]["DUPDATE"] + ""))
            {
                DUPDATE = DateTime.Parse(dt2.Rows[0]["DUPDATE"] + "");
            }

            if (dt.Rows.Count > 0)
            {
                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ

                //ถ้าเคยมีข้อมูลแล้ว
                string nversion = (int.Parse(dt.Rows[0]["NVERSION"] + "") + 1) + "";
                string SDOCID = (int.Parse(dt.Rows[0]["SDOCID"] + "") + 1) + "";
                string SDOCVERSION = (int.Parse(dt.Rows[0]["SDOCVERSION"] + "") + 1) + "";

                string strQuery = @"INSERT INTO TEMPLOYEE_AVATAR_DOC(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE ,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'
                                  ,'" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                  ," + CommonFunction.ReplaceInjection(SDOCVERSION) + @"
                                  ,'" + CommonFunction.ReplaceInjection("5") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection("รูปประจำตัว") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SUPDATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }

            }
            else
            {
                //ถ้าไม่่เคยข้อมูล
                string strQuery = @"INSERT INTO TEMPLOYEE_AVATAR_DOC(SEMPLOYEEID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE ,SPATH,NVERSION,DATERECEIVE) 
                                  VALUES('" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"'
                                  ," + CommonFunction.ReplaceInjection("1") + @"
                                  ," + CommonFunction.ReplaceInjection("1") + @"
                                  ,'" + CommonFunction.ReplaceInjection("5") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection("รูปประจำตัว") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["CACTIVE"] + "") + @"'
                                 ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DCREATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SCREATE"] + "") + @"'
                                 ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DUPDATE + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SUPDATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt2.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection("1") + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(DateTime.Now + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6516 )) ";

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }
            }




        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    [Serializable]
    class sEmployee
    {
        public string SEMPLOYEEID { get; set; }
        public string SEMPTPYE { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string STEL { get; set; }
        public string STEL2 { get; set; }
        public string SMAIL { get; set; }
        public string SMAIL2 { get; set; }
        public DateTime? DBIRTHDATE { get; set; }
        public string SABBREVIATION { get; set; }
        public string PERS_CODE { get; set; }
        public DateTime? PERSONEL_BEGIN { get; set; }
        public DateTime? PERSONEL_EXPIRE { get; set; }
        public string SDRIVERNO { get; set; }
        public DateTime? DDRIVEBEGIN { get; set; }
        public DateTime? DDRIVEEXPIRE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string CAUSESAP { get; set; }
        public string CAUSEOVER { get; set; }
        public string CACTIVE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SPATH { get; set; }
        public string NOTFILL { get; set; }
        public string BANSTATUS { get; set; }
        public string CAUSESAPCANCEL { get; set; }
        public string CANCELSTATUS { get; set; }
        public string CAUSESAPCOMMIT { get; set; }

    }

    [Serializable]
    class historyEmployee
    {
        public string SEMPLOYEEID { get; set; }
        public string SEMPTPYE { get; set; }
        public string FNAME { get; set; }
        public string LNAME { get; set; }
        public string STEL { get; set; }
        public string STEL2 { get; set; }
        public string SMAIL { get; set; }
        public string SMAIL2 { get; set; }
        public DateTime? DBIRTHDATE { get; set; }
        public string SABBREVIATION { get; set; }
        public string PERS_CODE { get; set; }
        public DateTime? PERSONEL_BEGIN { get; set; }
        public DateTime? PERSONEL_EXPIRE { get; set; }
        public string SDRIVERNO { get; set; }
        public DateTime? DDRIVEBEGIN { get; set; }
        public DateTime? DDRIVEEXPIRE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public Decimal NVERSION { get; set; }
        public string CAUSESAP { get; set; }
        public string CAUSEOVER { get; set; }
        public string CACTIVE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SPATH { get; set; }
        public string NOTFILL { get; set; }
        public string BANSTATUS { get; set; }
        public string CAUSESAPCANCEL { get; set; }
        public string CANCELSTATUS { get; set; }
        public string CAUSESAPCOMMIT { get; set; }
    }

    [Serializable]
    class ListGridDoc
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }

    }

}