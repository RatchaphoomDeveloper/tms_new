﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="admin_TimeLine_lst.aspx.cs" Inherits="admin_TimeLine_lst" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>

    <style type="text/css">
        .style13
        {
            height: 23px;
        }
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;}" />
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr align="right">
                        <td align="right" width="90%" class="style13">
                            <dx:ASPxTextBox ID="txtSearch" runat="server" ClientInstanceName="txtSearch" 
                                NullText="กรุณาป้อนชื่อคลัง" Style="margin-left: 0px" 
                                Width="220px">
                            </dx:ASPxTextBox>
                        </td>
                        <td align="right">
                            &nbsp; </td>
                        <td align="left" class="style13">
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search" CausesValidation="False"
                                Style="margin-left: 10px">
                                <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }" />
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px"></td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="ID1"
                                SkinID="_gvw" DataSourceID="sds"  OnHtmlDataCellPrepared="gvw_HtmlDataCellPrepared" >
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption = "ที่" HeaderStyle-HorizontalAlign="Center" Width="5%" VisibleIndex="1">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="1" 
                                        FieldName="STERMINALID" Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="หน่วยงาน" VisibleIndex="1" HeaderStyle-HorizontalAlign="Center"
                                        Width="25%" FieldName="STERMINALNAME">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ยืนยันรถตามสัญญา" VisibleIndex="2" HeaderStyle-HorizontalAlign="Center" Width="10%" FieldName="TCARCONFIRM">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("TCARCONFIRM") %>' runat="server" ID="xlbldCarConFirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ยืนยันรถตามแผน" VisibleIndex="3" HeaderStyle-HorizontalAlign="Center" Width="10%" FieldName="TPLANCONFIRM">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("TPLANCONFIRM")%>' runat="server" ID="xlbldPlanConfirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn Caption="เวลาปิดรับ Order" VisibleIndex="4" HeaderStyle-HorizontalAlign="Center" Width="10%" FieldName="TCLOSEORDER">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("TCLOSEORDER")%>' runat="server" ID="xlbldPlanConfirm" />
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ปรับปรุงข้อมูลล่าสุด" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                        FieldName="DUPDATE" Width="25%">
                                        <DataItemTemplate>
                                            <dx:ASPxLabel Text='<%# Eval("DUPDATE", "{0:dd/MM/yyyy - โดย }") %>' runat="server" ID="xlbldupdate" /><dx:ASPxLabel Text='<%# Eval("SUPDATE") %>' runat="server" ID="xlbsupdate" />
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataColumn Width="8%" CellStyle-Cursor="hand" VisibleIndex="6">
                                        <DataItemTemplate>
                                            <dx:ASPxButton ID="imbedit" runat="server" SkinID="_edit" CausesValidation="False">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('edit;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                            </dx:ASPxButton>
                                        </DataItemTemplate>
                                        <CellStyle Cursor="hand">
                                        </CellStyle>
                                    </dx:GridViewDataColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>

                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="sds" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>" 
                                CancelSelectOnNullParameter="False" EnableCaching="True" CacheKeyDependency="ckdUser"
                                SelectCommand="SELECT  ROW_NUMBER () OVER (ORDER BY T.STERMINALID) AS ID1, T.STERMINALID, TS.STERMINALNAME, nvl(T.TCARCONFIRM,'&nbsp;') AS TCARCONFIRM,nvl(T.TPLANCONFIRM,'&nbsp;') AS TPLANCONFIRM,nvl(T.TCLOSEORDER, '&nbsp;') AS TCLOSEORDER,T.DUPDATE,U.SFIRSTNAME || ' ' || U.SLASTNAME AS SUPDATE FROM (TTERMINAL T INNER JOIN TTERMINAL_SAP TS ON T.STERMINALID = TS.STERMINALID) LEFT JOIN TUSER u ON T.SUPDATE = U.SUID WHERE (TS.STERMINALID LIKE 'H%' OR TS.STERMINALID LIKE 'J%' OR TS.STERMINALID LIKE 'K%') AND TS.STERMINALNAME LIKE '%' || :oSearch || '%'"
                                OnDeleted="sds_Deleted" OnDeleting="sds_Deleting" 
                                
                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                               
                                <SelectParameters>
                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch"  PropertyName="Text"/>
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td align="right" width="60%">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                        <td align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
