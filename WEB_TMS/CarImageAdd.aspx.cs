﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Web.Configuration;

public partial class CarImageAdd : System.Web.UI.Page
{
    string strConn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    private const string Path = @"UploadFile/UploadedImagesCAR/";
    private const string PathTemp = @"UploadFile/temp/tempuploads";
    private static string USER_ID = "";
    private static string sREQUESTID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        gvwWaiting.AfterPerformCallback += new DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventHandler(gvwWaiting_AfterPerformCallback);
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Session["UserID"] + ""))
            {

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }
            try
            {

                #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
                if (!Directory.Exists(Server.MapPath("./") + PathTemp.Replace("/", "\\")))
                {
                    Directory.CreateDirectory(Server.MapPath("./") + PathTemp.Replace("/", "\\"));
                }
                //if (!Directory.Exists(Server.MapPath("./") + Path.Replace("/", "\\")))
                //{
                //    Directory.CreateDirectory(Server.MapPath("./") + Path.Replace("/", "\\"));
                //}
                #endregion
            }
            catch
            {

            }

            string strREQID = Request.QueryString["strRQID"];
            if (!string.IsNullOrEmpty(strREQID + ""))
            {
                USER_ID = Session["UserID"] + "";
                
                string[] arrREQID = STCrypt.DecryptURL(strREQID);
                Session["REQ_ID"] = arrREQID[0];
                sREQUESTID = arrREQID[0];
                Session["SPATH"] = Path + sREQUESTID;
                if (!Directory.Exists(Server.MapPath("./") + (Path + sREQUESTID).Replace("/", "\\")))
                {
                    Directory.CreateDirectory(Server.MapPath("./") + (Path + sREQUESTID).Replace("/", "\\"));
                }
                ListData(sREQUESTID);

                string NVER = "SELECT REQUEST_ID,NVERSION FROM TBL_CAR_IMAGE WHERE  REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQUESTID) + "' ORDER BY NVERSION DESC";
                DataTable dt = CommonFunction.Get_Data(strConn, NVER);
                if (dt.Rows.Count > 0)
                {
                    Session["NVERSION"] = !string.IsNullOrEmpty(dt.Rows[0]["NVERSION"] + "") ? dt.Rows[0]["NVERSION"] + "" : "0";
                }
                else
                {
                    Session["NVERSION"] = "0";
                }
            }
            else
            {
                //Session["REQ_ID"] = "1409/01-0001";
                //Session["NVERSION"] = "0";
                //Session["SPATH"] = Path;
            }
        }
        else { }
        
    }

    void gvwWaiting_AfterPerformCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        int Inx = 0;
        if (!string.IsNullOrEmpty(txtIdx.Text))
        {
            Inx = int.Parse(txtIdx.Text);
        }
        switch (e.CallbackName)
        {
            //เมื่อ gvw callback
            case "CUSTOMCALLBACK":

                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {
                    case "delete":
                        dynamic data = gvwWaiting.GetRowValues(Inx, "CARIMAGE_ID", "REQUEST_ID", "NVERSION");

                        string QUERYUPDATE = @"UPDATE TBL_CAR_IMAGE
SET   
       ISACTIVE_FLAG = 'N',
       UPDATE_CODE   = '" + CommonFunction.ReplaceInjection(USER_ID) + @"',
       UPDATE_DATE   = SYSDATE
WHERE  CARIMAGE_ID   = " + data[0] + @"
AND    REQUEST_ID    = '" + data[1] + @"'
AND    NVERSION      = " + data[2] + @"";



                        AddTODB(QUERYUPDATE);
                        ListData(sREQUESTID);
                        break;


                }
                break;
        }
    }

    // ส่วนข้างล่าง
    protected void xcpn_Load(object sender, EventArgs e)
    {
        ListData(sREQUESTID);
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxSession('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession + "',function(){window.location='default.aspx';});"); return;
        }

        string[] spara = e.Parameter.Split(';');
        int Inx = 0;
        if (!string.IsNullOrEmpty(txtIdx.Text))
        {
            Inx = int.Parse(txtIdx.Text);
        }

        switch (spara[0])
        {
            case "RedirectT1": xcpn.JSProperties["cpRedirectTo"] = "result-add.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "RedirectImg": xcpn.JSProperties["cpRedirectTo"] = "CarImageAdd.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            case "delete":
                dynamic data = gvwWaiting.GetRowValues(Inx, "CARIMAGE_ID", "REQUEST_ID", "NVERSION");

                string QUERYUPDATE = @"UPDATE TBL_CAR_IMAGE
SET   
       ISACTIVE_FLAG = 'N',
       UPDATE_CODE   = '" + CommonFunction.ReplaceInjection(USER_ID) + @"',
       UPDATE_DATE   = SYSDATE
WHERE  CARIMAGE_ID   = " + data[0] + @"
AND    REQUEST_ID    = '" + data[1] + @"'
AND    NVERSION      = " + data[2] + @"";



                AddTODB(QUERYUPDATE);
                ListData(sREQUESTID);
                break;
            //case "RedirectT2": xcpn.JSProperties["cpRedirectTo"] = "result-add2.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            //case "RedirectT3": xcpn.JSProperties["cpRedirectTo"] = "result-add3.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            //case "RedirectT4": xcpn.JSProperties["cpRedirectTo"] = "result-add4.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
            //case "RedirectT5": xcpn.JSProperties["cpRedirectTo"] = "result-add5.aspx?strRQID=" + Server.UrlEncode(STCrypt.Encrypt(sREQUESTID)); break;
        };
    }

    protected void btnClickComplete_Click(object sender, EventArgs e)
    {
        ListData(sREQUESTID);
    }

    private void ListData(string sREQ_ID)
    {
        string QUERY = @"SELECT CARIMAGE_ID, REQUEST_ID, FILE_NAME, FILE_PATH||FILE_SYSNAME as IMG, ISACTIVE_FLAG, NVERSION,CREATE_DATE
FROM TBL_CAR_IMAGE
WHERE ISACTIVE_FLAG = 'Y' AND REQUEST_ID = '" + CommonFunction.ReplaceInjection(sREQ_ID) + "' ORDER BY CARIMAGE_ID";

        DataTable dt = CommonFunction.Get_Data(strConn, QUERY);
        if (dt.Rows.Count > 0)
        {
            gvwWaiting.DataSource = dt;
        }
        gvwWaiting.DataBind();
    }

    private void AddTODB(string strQuery)
    {
        using (System.Data.OracleClient.OracleConnection con = new System.Data.OracleClient.OracleConnection(strConn))
        {

            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            using (System.Data.OracleClient.OracleCommand com = new System.Data.OracleClient.OracleCommand(strQuery, con))
            {
                com.ExecuteNonQuery();
            }

            con.Close();

        }
    }
}