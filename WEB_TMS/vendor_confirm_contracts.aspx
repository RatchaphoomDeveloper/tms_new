﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" EnableEventValidation="false"
    ValidateRequest="false"
    CodeFile="vendor_confirm_contracts.aspx.cs" Inherits="vendor_confirm_contracts" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxUploadControl" Assembly="DevExpress.Web.v11.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        // <![CDATA[

        function OpenNewWindow(sEncrypt) {
            alert('OpenNewWindow');
            window.open('checktruck.aspx?str=' + sEncrypt, '', 'width=800,height=600');
        }
        function GoToHome() {
            window.location = 'default.aspx';
        }
        function Set_txtnTruckConfirm(s, e) {
            //alert(e);
            var text = s.GetText();
            if (s.GetText() != "") {
                s.SetText(text);
            } else {
                s.SetText(e);
            }
        }
        function ShowLoginWindow() {
            pcLogin.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
        function BackupConfirmTruckID(ctrl, checkid, mode) {
            if (mode == "") {
                document.getElementById(ctrl).value += '' + checkid;
            }
            else {
                var struckid = document.getElementById(ctrl).value;
                if (struckid.indexOf(checkid) > -1) {
                    document.getElementById(ctrl).value = struckid.replace(checkid, '');
                }
            }
        }

        //Upload File Control
        var fieldSeparator = "|";
        var fieldPath = "#";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexPath = e.callbackData.indexOf(fieldPath);
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + (fieldSeparator.length), indexPath);
                var sPath = e.callbackData.substring(indexPath + (fieldPath.length));
                var date = new Date();
                var imgSrc = sPath + pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }

        function UpdateCheckListItem(ctrlid, contractid, truckid, checklistid, scheckid) {
            var inner_HTML = $('#' + ctrlid)[0].innerHTML;
            $('#' + ctrlid)[0].innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            $('#' + ctrlid).css({ 'text-decoration': 'none' });
            $('#' + ctrlid).addClass('loading_algRight');
            var datas = contractid + '^' + truckid + '^' + checklistid + '^' + scheckid;
            jQuery.ajax({
                type: "POST",
                url: "ashx/UpdateCheckLists.ashx",
                cache: false,
                async: false,
                dataType: "html",
                data: { contractid: encodeURIComponent(contractid), data: encodeURIComponent(datas) },
                error: function (response) {
                    $('#' + ctrlid).removeClass('loading_algRight');
                    alert(response);
                },
                success: function (response) {
                    if (response == '1') {
                        $('#' + ctrlid)[0].innerHTML = inner_HTML;
                        $('#' + ctrlid).hide();
                    }

                    $('#' + ctrlid).removeClass('loading_algRight');
                }
            });
        }
        function ddlOnChange(value, index, Type) {
            xgvwCar.PerformCallback(index + '$ddlOnChange$' + value + "$" + Type);
            //var ddlDeliveryPart = $('.xgvwCar' + indexgrid).find('.ddlDeliveryPart' + index);
            //$(ddlDeliveryPart).empty();
            //if (value == "Y") {
            //    var myOptions = {
            //        "": '--เลือก--',
            //        ไม่ระบุ: 'ไม่ระบุ'
            //    };
            //    $.each(myOptions, function (val, text) {
            //        ddlDeliveryPart.append(
            //            $('<option></option>').val(val).html(text)
            //        );
            //    });
            //}
            //else if (value == "N") {
            //    var myOptions = {
            //        "": '--เลือก--',
            //        กลางวัน: 'กลางวัน',
            //        กลางคืน: 'กลางคืน'
            //    };
            //    $.each(myOptions, function (val, text) {
            //        ddlDeliveryPart.append(
            //            $('<option></option>').val(val).html(text)
            //        );
            //    });
            //}
            //else {
            //    var myOptions = {
            //        "": '--เลือก--',
            //        ไม่ระบุ: 'ไม่ระบุ',
            //        กลางวัน: 'กลางวัน',
            //        กลางคืน: 'กลางคืน'
            //    };
            //    $.each(myOptions, function (val, text) {
            //        ddlDeliveryPart.append(
            //            $('<option></option>').val(val).html(text)
            //        );
            //    });
            //}

        }

        // ]]> 
    </script>
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style>
        .wrapper1, .wrapper2 {
            width: 100%;
            border: none 0px RED;
            overflow-x: scroll;
            overflow-y: hidden;
        }

        .wrapper1 {
            height: 20px;
        }

        .div1 {
            width: 100%;
            height: 20px;
        }

        .div2 {
            width: 100%;
            overflow: auto;
        }
        /*tr.dxgvSelectedRow_Aqua {
            background: none ;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" CausesValidation="False">
        <ClientSideEvents EndCallback="function(s, e){ eval(s.cpPopup);  s.cpPopup=''; inEndRequestHandler(); setdatepicker(); }"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
                <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
                <table border="0" cellspacing="2" cellpadding="5" width="100%">
                    <tbody>
                        <tr>
                            <td align="left">
                                <dx:ASPxButton ID="btnConfirm" ClientInstanceName="btnConfirm" runat="server" SkinID="_send">
                                    <ClientSideEvents Click="function(s,e){if(xgvw.GetSelectedRowCount()>0){ xcpn.PerformCallback('checkdata');}else{ dxInfo('ข้อความจากระบบ ','กรุณาเลือกสัญญาที่ท่านต้องการส่งข้อมูลการยืนยันรถให้ ปตท.!'); return false; } }"></ClientSideEvents>
                                </dx:ASPxButton>
                            </td>
                            <td align="">
                                <table>
                                    <tr>
                                        <td width="30%">
                                            <asp:TextBox runat="server" ID="txtKeyword" CssClass="form-control" placeholder="เลขที่สัญญา" />
                                        </td>
                                        <td style="white-space: nowrap;">
                                            <span>วันที่เข้ารับสินค้า </span>
                                            <div style="display: none;">
                                            </div>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="dteEnd" CssClass="form-control datepicker dteEnd" placeholder="เลขที่สัญญา" />
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="cmbStatus" CssClass="form-control">
                                                <asp:ListItem Text="สถานะ" Value="" />
                                                <asp:ListItem Text="ยืนยันรถ" Value="1" />
                                                <asp:ListItem Text="รอส่งข้อมูล" Value="1" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="70" align="left">
                                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search"
                                    CausesValidation="true" ValidationGroup="search">
                                    <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('search'); }"></ClientSideEvents>
                                </dx:ASPxButton>
                                
                                 
                            </td>
                            <td>
                                <a href="ConfirmTruckDetailCheckIVMS.aspx" target="_blank" class="btn btn-md bth-hover btn-info" style="background-color:#ffeb00;border-color:antiquewhite;color:black;">
                            ตรวจสอบ IVMS รายทะเบียน
                        </a>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td class="active" colspan="4" align="right">
                                <asp:TextBox ID="txtClientClickID" runat="server" Style="display: none;"></asp:TextBox>
                                รอยืนยันรถตามสัญญา
                                <dx:ASPxLabel ID="lblRecord" ClientInstanceName="lblRecord" runat="server" Text="0"
                                    title="n">
                                </dx:ASPxLabel>
                                รายการ
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr style="height: 1px;">
                        <td bgcolor="#0e4999"></td>
                    </tr>
                </table>
                <table border="0" cellspacing="1" cellpadding="1" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <dx:ASPxGridView ID="xgvw" ClientInstanceName="xgvw" runat="server" SkinID="_gvw"
                                    AutoGenerateColumns="False" KeyFieldName="SKEYID"
                                    OnAfterPerformCallback="xgvw_AfterPerformCallback" OnHtmlRowPrepared="xgvw_HtmlRowPrepared" OnCustomColumnDisplayText="xgvw_CustomColumnDisplayText">
                                    <ClientSideEvents RowClick="function (s,e) { xgvw.StartEditRow(e.visibleIndex); } "></ClientSideEvents>
                                    <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="1%">
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllCheckBox" ClientInstanceName="SelectAllCheckBox" runat="server"
                                                    ToolTip="Select/Unselect all rows on the page" CheckState="Unchecked">
                                                    <ClientSideEvents CheckedChanged="function(s, e) { xgvw.SelectAllRowsOnPage(s.GetChecked()); }" />
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="ที่.">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataColumn Width="7%" CellStyle-Cursor="hand" HeaderStyle-HorizontalAlign="Center"
                                            CellStyle-HorizontalAlign="Center" Caption="Export<br/>Excel" Visible="false">

                                            <DataItemTemplate>
                                                <dx:ASPxButton ID="imbedit" runat="server" Text="Export" CausesValidation="False" OnClick="imbedit_Click" AutoPostBack="true" CommandArgument='<%# Eval("SCONTRACTID") + "|" + Eval("SCONTRACTNO") %>'>
                                                </dx:ASPxButton>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            <CellStyle Cursor="hand">
                                            </CellStyle>
                                        </dx:GridViewDataColumn>



                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา" ReadOnly="True">
                                            <DataItemTemplate>
                                                <table width="99%">
                                                    <tr>
                                                        <td style="width: 80%;">
                                                            <dx:ASPxLabel ID="lblSCONTRACTNO" runat="server" Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                            </dx:ASPxLabel>
                                                        </td>
                                                        <td align="right">
                                                            <%--<dx:ASPxButton ID="btncontractDetail" runat="server" Image-Url="~/Images/comment.png" AutoPostBack="false" EnableDefaultAppearance="False"  SkinID="NoSkind" Cursor="pointer">
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('ContractDetail;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                             </dx:ASPxButton>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Left" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEV_DATE" Caption="วันที่<br/>เข้ารับสินค้า" ReadOnly="True" Visible="false">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SABBREVIATION" Caption="คลังต้นทาง<br/>ตามสัญญา" ReadOnly="True">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center" Wrap="True">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DDATE" Caption="วันที่ยืนยัน" ReadOnly="True" Visible="false" Width="8%">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="จำนวนรถ">
                                            <Columns>
                                                <dx:GridViewBandColumn Caption="รถในสัญญา">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn FieldName="NONHAND" Caption="ตามสัญญา" ReadOnly="True"
                                                            Width="4%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn FieldName="NONHANDSYSTEM" Caption="ในระบบ" ReadOnly="True"
                                                            Width="4%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                            </PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="มีปัญหา" Visible="False" Width="4%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewBandColumn>
                                                <dx:GridViewBandColumn Caption="รถสำรอง">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn FieldName="NOUTHAND" Caption="ตามสัญญา" ReadOnly="True" Width="4%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn FieldName="NSTANDBYSYSTEM" Caption="ในระบบ" ReadOnly="True" Width="4%">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewBandColumn>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewBandColumn Caption="สถานะรถ">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="NUNAVAILABLE" Caption="ไม่พร้อม<br/>ใช้งาน" ReadOnly="True" Width="4%">
                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="NTRANSIT" Caption="อยู่ระหว่าง<br/>ขนส่ง" ReadOnly="True" Width="4%" Visible="false">
                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="พร้อม<br/>ใช้งาน" ReadOnly="True" Width="4%" Visible="false">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel ID="txtnTruckConfirm" ClientInstanceName="txtnTruckConfirm" runat="server"
                                                            EnableViewState="true" Width="35px" Text='<%# Eval("NAVAILABLE") %>'
                                                            HorizontalAlign="Center">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ยืนยันรถ<br/>(รวม)" ReadOnly="True" Width="4%" Name="txtnTruckConfirmSum">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel ID="txtnTruckConfirmSum" ClientInstanceName="txtnTruckConfirmSum" runat="server"
                                                            EnableViewState="true" Width="35px" ClientEnabled='false' Text='<%# Eval("NAVAILABLE") %>' ForeColor="Red"
                                                            HorizontalAlign="Center">
                                                        </dx:ASPxLabel>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="ยืนยันก่อน" Width="10%">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy hh:mm">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONFIRM" Caption="สถานะ" Width="8%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CCONFIRM" Caption="CCONFIRM" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NCONFIRM" Caption="NCONFIRM" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="NISSUE" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NREJECT" Caption="NREJECT" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="DEXPIRE" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NCONFIRMID" Caption="NCONFIRMID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="COIL" Caption="COIL" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="CGAS" Caption="COIL" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NHOLD" Caption="NHOLD" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NTRUCK" Caption="NTRUCK" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="PTT_LOCK" Caption="PTT_LOCK" Visible="false" />
                                    </Columns>
                                    <Settings></Settings>
                                    <Templates>
                                        <EditForm>
                                            <dx:ASPxCallbackPanel ID="xcpnGPS" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpnGPS"
                                                OnCallback="xcpnGPS_Callback" CausesValidation="False" Visible="false">
                                                <PanelCollection>
                                                    <dx:PanelContent ID="pncGPS" Visible="false" runat="server">
                                                        <table width="99%">
                                                            <tr>
                                                                <td style="width: 30%">แนบไฟล์รายงานสถานะรถจากระบบ GPS (Excel)<font color='red'>*</font>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxUploadControl ID="uclGPSxls" ClientInstanceName="uclGPSxls" runat="server"
                                                                        Width="70%" NullText="Click here to browse files..." OnFileUploadComplete="uclGPSxls_FileUploadComplete">
                                                                        <ClientSideEvents FileUploadComplete="function(s, e) { txtSystemFileName.SetValue(e.callbackData.split('$')[0]); txtOriginalFileName.SetValue(e.callbackData.split('$')[1]); txtGPSMode.SetValue('UPLAODED'); xgvwCar.PerformCallback(s.name.substring(s.name.split('btnGPSconfirm')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]+'$BINDDATA'); } " />
                                                                        <ValidationSettings AllowedFileExtensions=".xls,.xlsx" ShowErrors="true" NotAllowedFileExtensionErrorText="กรุณาระบถไฟล์ ที่มีนามสกุล .xls หรือ .xlsx เท่านั้น!"
                                                                            MaxFileSize="4194304">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxUploadControl>
                                                                    <dx:ASPxTextBox runat="server" ID="txtSystemFileName" ClientInstanceName="txtSystemFileName"
                                                                        ClientVisible="false" />
                                                                    <dx:ASPxTextBox runat="server" ID="txtOriginalFileName" ClientInstanceName="txtOriginalFileName"
                                                                        ClientVisible="false" />
                                                                    <dx:ASPxTextBox runat="server" ID="txtGPSMode" ClientInstanceName="txtGPSMode" ClientVisible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>แนบไฟล์หน้าจอสถานะรถจากระบบ GPS (Image)<font color='red'>*</font>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxUploadControl ID="uclGPSpic" ClientInstanceName="uclGPSpic" runat="server"
                                                                        CssClass="dxeLineBreakFix" Width="70%" NullText="Click here to browse files..."
                                                                        OnFileUploadComplete="uclGPSpic_FileUploadComplete">
                                                                        <ClientSideEvents FileUploadStart="function(s, e) { }" FileUploadComplete="function(s, e) {  txtSystemPICName.SetValue(e.callbackData.split('$')[0]); txtOriginalPICName.SetValue(e.callbackData.split('$')[1]); txtGPSPICMode.SetValue('UPLAODED'); if(txtSystemPICName.GetValue()!= undefined ){btnViewGPSpic.SetClientVisible(true);btnDelGPSSpic.SetClientVisible(true); }else{btnViewGPSpic.SetClientVisible(false); btnDelGPSSpic.SetClientVisible(false); }} " />
                                                                        <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>"
                                                                            AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxUploadControl>
                                                                    <dx:ASPxButton ID="btnViewGPSpic" ClientInstanceName="btnViewGPSpic" runat="server"
                                                                        CausesValidation="False" AutoPostBack="false" EnableTheming="False" EnableDefaultAppearance="False"
                                                                        SkinID="dd" Cursor="pointer" CssClass="dxeLineBreakFix" Width="25px" ClientVisible="false">
                                                                        <ClientSideEvents Click="function(s,e){window.open('openFile.aspx?str=UploadFile/GPS/Temp_PIC/'+ txtSystemPICName.GetValue());}" />
                                                                        <Image Width="25px" Height="25px" Url="Images/view1.png">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnDelGPSSpic" runat="server" CausesValidation="False" AutoPostBack="false"
                                                                        EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                                        ClientInstanceName="btnDelGPSSpic" CssClass="dxeLineBreakFix" Width="25px" ClientVisible="false">
                                                                        <ClientSideEvents Click="function (s, e) { xcpnGPS.PerformCallback('deletepic;'+ txtSystemPICName.GetValue()+';'+txtGPSPICMode.GetValue() +';1');}" />
                                                                        <Image Width="25px" Height="25px" Url="Images/bin1.png">
                                                                        </Image>
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxTextBox runat="server" ID="txtSystemPICName" ClientInstanceName="txtSystemPICName"
                                                                        ClientVisible="false" />
                                                                    <dx:ASPxTextBox runat="server" ID="txtOriginalPICName" ClientInstanceName="txtOriginalPICName"
                                                                        ClientVisible="false" />
                                                                    <dx:ASPxTextBox runat="server" ID="txtGPSPICMode" ClientInstanceName="txtGPSPICMode"
                                                                        ClientVisible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top">หมายเหตุเพิ่มเติม
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxMemo ID="txtremark" runat="server" Rows="5" Width="70%">
                                                                    </dx:ASPxMemo>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" align="center">
                                                                    <dx:ASPxCheckBox ID="cbGPSXLS" runat="server" ClientVisible="false">
                                                                    </dx:ASPxCheckBox>
                                                                    <dx:ASPxCheckBox ID="cbGPSPIC" runat="server" ClientVisible="false">
                                                                    </dx:ASPxCheckBox>
                                                                    <dx:ASPxButton ID="btnGPSconfirm" runat="server" Text=" ยืนยันข้อมูล " class="dxeLineBreakFix"
                                                                        AutoPostBack="false" CausesValidation="true">
                                                                        <ClientSideEvents Click="function(s,e){var msg=''; if(uclGPSxls.GetText()==''){msg+='<br>แนบไฟล์รายงานสถานะรถจากระบบ GPS (Excel)';} if(uclGPSpic.GetText()==''&& txtSystemPICName.GetText()==''){msg+='<br>แนบไฟล์รายงานสถานะรถจากระบบ GPS (Image)';} if(msg!=''){ dxWarning('แจ้งเตือน','กรุณาระบุ'+msg); return false;}{uclGPSpic.Upload();uclGPSxls.Upload(); } }" />
                                                                    </dx:ASPxButton>
                                                                    <dx:ASPxButton ID="btnGPScancel" runat="server" class="dxeLineBreakFix" Text=" ยกเลิกข้อมูล "
                                                                        AutoPostBack="false" CausesValidation="False">
                                                                        <ClientSideEvents Click="function(s,e){ xcpnGPS.PerformCallback('GPSCANCEL'); }" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </dx:PanelContent>
                                                </PanelCollection>
                                            </dx:ASPxCallbackPanel>


                                            <table id="tbl_xgvwCar" width="100%" runat="server">
                                                <tr>
                                                    <td colspan="2">

                                                        <dx:ASPxButton ID="btnsubmit" ClientInstanceName="btnsubmit" runat="server" SkinID="_submit"
                                                            CssClass="dxeLineBreakFix" Enabled="false">
                                                            <ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('CONFIRMSAVE;' + s.name.substring(s.name.split('btnsubmit')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]); }" />
                                                        </dx:ASPxButton>
                                                        <dx:ASPxButton ID="btnclose" ClientInstanceName="btnclose" runat="server" SkinID="_close"
                                                            CssClass="dxeLineBreakFix">
                                                            <ClientSideEvents Click="function (s, e) { xgvw.CancelEdit(); }" />
                                                        </dx:ASPxButton>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <td colspan="2">
                                                            <asp:Label ForeColor="Red" Text="" ID="lblError" runat="server" />
                                                        </td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <%--<div class="wrapper1">
    <div class="div1">
    </div>
</div>--%>

                                                        <dx:ASPxGridView ID="xgvwCar" ClientInstanceName="xgvwCar" runat="server" CssClass='<%# "xgvwCar"+Container.VisibleIndex.ToString() %>'
                                                            AutoGenerateColumns="false" KeyFieldName="ROWNUM" Width="100%" OnHtmlDataCellPrepared="xgvwCar_HtmlDataCellPrepared" OnHtmlRowPrepared="xgvwCar_HtmlRowPrepared"
                                                            OnAfterPerformCallback="xgvwCar_AfterPerformCallback">

                                                            <ClientSideEvents EndCallback="function(s, e){ eval(s.cpPopup); s.cpPopup=''; if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen ,'', 'width=1050,height=600,scrollbars=yes,resizable=1'); s.cpRedirectOpen = undefined}" SelectionChanged=""></ClientSideEvents>
                                                            <Columns>

                                                                <dx:GridViewDataTextColumn Caption="ที่" Name="NO">
                                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="รหัสรถ" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewBandColumn Caption="ประเภทรถ" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="CSTANBY" Name="CARTYPE" Caption="ประเภทรถ" ReadOnly="True">
                                                                            <HeaderTemplate>
                                                                                <asp:DropDownList runat="server" ID="ddlCarTypeHeader" Width="100%" onchange='<%# "ddlOnChange($(this).val(),-1,\"CARTYPEHEADER\");" %>'>
                                                                                    <Items>
                                                                                        <asp:ListItem Text="--เลือก--" Value="" Selected="True" />
                                                                                        <asp:ListItem Text="รถปกติ" Value="Y" />
                                                                                        <asp:ListItem Text="รถหมุนเวียน" Value="N" />
                                                                                    </Items>
                                                                                </asp:DropDownList>
                                                                            </HeaderTemplate>
                                                                            <DataItemTemplate>
                                                                                <asp:DropDownList runat="server" ID="ddlCarType" Width="100%" index='<%# Container.VisibleIndex %>' onchange='<%# "ddlOnChange($(this).val()," + Container.VisibleIndex + ",\"CarType\");" %>'>
                                                                                    <Items>
                                                                                        <asp:ListItem Text="--เลือก--" Value="" Selected="True" />
                                                                                        <asp:ListItem Text="รถปกติ" Value="Y" />
                                                                                        <asp:ListItem Text="รถหมุนเวียน" Value="N" />
                                                                                    </Items>
                                                                                </asp:DropDownList>
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Left" />
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DELIVERY_PART" Caption="รอบการขนส่ง" ReadOnly="True">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <DataItemTemplate>

                                                                        <asp:DropDownList runat="server" ID='ddlDeliveryPart' CssClass='<%# "ddlDeliveryPart"+Container.VisibleIndex.ToString() %>' Width="100%" Enabled="false">
                                                                            <Items>
                                                                                <asp:ListItem Text="--เลือก--" Value="" Selected="True" />
                                                                                <asp:ListItem Text="ไม่ระบุ" Value="ไม่ระบุ" />
                                                                                <asp:ListItem Text="กลางวัน" Value="กลางวัน" />
                                                                                <asp:ListItem Text="กลางคืน" Value="กลางคืน" />
                                                                            </Items>
                                                                        </asp:DropDownList>
                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewBandColumn Caption="ข้อมูลรถในสัญญา">
                                                                    <Columns>
                                                                        <dx:GridViewBandColumn Caption="ทะเบียนรถ">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="หัว" ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ท้าย"
                                                                                    ReadOnly="True">
                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewBandColumn>

                                                                        <dx:GridViewDataTextColumn FieldName="TTRUCK_TYPE_NAME" Caption="ประเภท" ReadOnly="True"
                                                                            Width="5%">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblSTANDBY" ClientInstanceName="lblSTANDBY" runat="server" Text='<%#  ""+Eval("TTRUCK_TYPE_NAME") %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                        </dx:GridViewDataTextColumn>

                                                                    </Columns>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dx:GridViewBandColumn>

                                                                <dx:GridViewBandColumn Caption="รถขนส่ง" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="CAR_STATUS_ID" Caption="สถานะรถขนส่ง" ReadOnly="True">
                                                                            <HeaderTemplate>
                                                                                <asp:DropDownList runat="server" ID="ddlCarConfirmStatusHeader" Width="100%" onchange='<%# "ddlOnChange($(this).val(),-1,\"CAR_STATUS_IDHEADER\");" %>'>
                                                                                </asp:DropDownList>
                                                                            </HeaderTemplate>
                                                                            <DataItemTemplate>
                                                                                <asp:DropDownList runat="server" ID="ddlCarConfirmStatus" Width="100%" onchange='<%# "ddlOnChange($(this).val()," + Container.VisibleIndex + ",\"CAR_STATUS_ID\");" %>'>
                                                                                </asp:DropDownList>
                                                                            </DataItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Left" />
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewBandColumn Caption="ประสงค์เข้ารับงาน" HeaderStyle-HorizontalAlign="Center">
                                                                    <Columns>
                                                                        <dx:GridViewBandColumn Caption="คลังต้นทาง" HeaderStyle-HorizontalAlign="Center">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn FieldName="STERMINALID" Caption="คลังต้นทาง" PropertiesTextEdit-EncodeHtml="false"
                                                                                    ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:DropDownList runat="server" ID="ddlWareHouseHeader" Width="100%" onchange='<%# "ddlOnChange($(this).val(),-1,\"STERMINALIDHEADER\");" %>'>
                                                                                        </asp:DropDownList>
                                                                                    </HeaderTemplate>
                                                                                    <DataItemTemplate>
                                                                                        <asp:DropDownList runat="server" ID="ddlWareHouse" Width="100%" onchange='<%# "ddlOnChange($(this).val()," + Container.VisibleIndex + ",\"STERMINALID\");" %>'>
                                                                                            <Items>
                                                                                            </Items>
                                                                                        </asp:DropDownList>
                                                                                    </DataItemTemplate>
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                        </dx:GridViewBandColumn>
                                                                        <dx:GridViewBandColumn Caption="เที่ยวที่รับงาน" HeaderStyle-HorizontalAlign="Center">
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn FieldName="DELIVERY_NUM" Caption="เที่ยวที่<br/>รับงาน" PropertiesTextEdit-EncodeHtml="false"
                                                                                    ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBoxList ID="cblDeliveryNumHeader" runat="server" RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Text="1" Value="1" />
                                                                                            <asp:ListItem Text="2" Value="2" />
                                                                                            <asp:ListItem Text="3" Value="3" />
                                                                                            <asp:ListItem Text="4" Value="4" />
                                                                                        </asp:CheckBoxList>
                                                                                    </HeaderTemplate>
                                                                                    <DataItemTemplate>
                                                                                        <asp:CheckBoxList ID="cblDeliveryNum" runat="server" RepeatDirection="Horizontal">
                                                                                            <asp:ListItem Text="1" Value="1" />
                                                                                            <asp:ListItem Text="2" Value="2" />
                                                                                            <asp:ListItem Text="3" Value="3" />
                                                                                            <asp:ListItem Text="4" Value="4" />
                                                                                        </asp:CheckBoxList>

                                                                                    </DataItemTemplate>
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                        </dx:GridViewBandColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>

                                                                <dx:GridViewDataTextColumn FieldName="REMARK" Caption="หมายเหตุ" ReadOnly="True">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Left" />
                                                                    <DataItemTemplate>
                                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="3" onblur='<%# "ddlOnChange($(this).val()," + Container.VisibleIndex + ",\"REMARK\");" %>' Text='<%# Eval("REMARK") %>' />

                                                                    </DataItemTemplate>
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn Caption="การอนุญาต<br/>ใช้งาน" PropertiesTextEdit-EncodeHtml="false" FieldName="CACTIVE">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblStatusCar" ClientInstanceName="lblStatusCar" runat="server"
                                                                            Text='<%#  ""+Eval("CACTIVE") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewBandColumn Caption="" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                        <center>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td style="width:50%">
                                                                                    <center>
ข้อมูลจากระบบ IVMS
                                                                                    </center>

                                                                                </td>
                                                                                <td style="width:50%">
                                                                                    <center>
<dx:ASPxButton ID="btnIVMSCheck" ClientInstanceName="btnIVMSCheck" runat="server" Text="ตรวจสอบสถานะ" Border-BorderColor="Black" ForeColor="Black" BackColor="GreenYellow" AutoPostBack="false" SkinID="null"  >
                                                                            <ClientSideEvents Click="function (s, e) {  xgvw.PerformCallback('IVMSCHECK'); }" />
                                                                        </dx:ASPxButton>
                                                                                        </center>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                        </center>
                                                                    </HeaderTemplate>
                                                                    <Columns>
                                                                        
                                                                        <dx:GridViewDataTextColumn FieldName="IS_SELECT" PropertiesTextEdit-EncodeHtml="false">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="cbSelectHeader" runat="server" AutoPostBack="false"/>
                                                                            </HeaderTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <DataItemTemplate>
                                                                                <asp:CheckBox ID="cbSelect" runat="server" AutoPostBack="false"/>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="IVMS_GPS_STAT" Caption="GPS" PropertiesTextEdit-EncodeHtml="false" CellStyle-HorizontalAlign="Center"
                                                                            ReadOnly="True">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblIVMS_GPS_STAT" ClientInstanceName="lblIVMS_GPS_STAT" runat="server"
                                                                                    Text='<%#  ""+Eval("IVMS_GPS_STAT")=="1"?"Yes": ""+Eval("IVMS_GPS_STAT")=="0" ? "No" : "" %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="IVMS_MDVR_STAT" Caption="กล้อง" PropertiesTextEdit-EncodeHtml="false" CellStyle-HorizontalAlign="Center"
                                                                            ReadOnly="True">
                                                                            <DataItemTemplate>
                                                                                <dx:ASPxLabel ID="lblIVMS_MDVR_STAT" ClientInstanceName="lblIVMS_MDVR_STAT" runat="server"
                                                                                    Text='<%#  ""+Eval("IVMS_MDVR_STAT")=="1"?"Yes": ""+Eval("IVMS_MDVR_STAT")=="0"? "No" : "" %>'>
                                                                                </dx:ASPxLabel>
                                                                            </DataItemTemplate>
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="IVMS_STATUS" Caption="สถานะ"  CellStyle-HorizontalAlign="Center">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn FieldName="IVMS_RADIUS_KM" Caption="การกระจัด(กม.)" PropertiesTextEdit-EncodeHtml="false" CellStyle-HorizontalAlign="Center">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataDateColumn FieldName="IVMS_CHECKDATE" Caption="วันที่อัพเดต" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy HH:mm:ss" CellStyle-HorizontalAlign="Center">
                                                                        </dx:GridViewDataDateColumn>
                                                                    </Columns>
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewDataColumn CellStyle-Cursor="hand" FieldName="IS_CONFIRM" Name="IS_CONFIRM" Caption="ยืนยันรถ">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblStatus" runat="server" EncodeHtml="false" Text='<%# (""+Eval("CCONFIRM")=="0" ||  string.IsNullOrEmpty(Eval("CCONFIRM") + string.Empty) ?"<font color=red >ปฏิเสธ</font>":"ยืนยัน")%>'>
                                                                        </dx:ASPxLabel>
                                                                        <dx:ASPxTextBox ID="txtconfirm" runat="server" ClientInstanceName="txtconfirm" Text='<%# (""+Eval("CCONFIRM")=="0"?"":"1")%>'
                                                                            ClientVisible="false">
                                                                        </dx:ASPxTextBox>
                                                                        <dx:ASPxButton ID="imbconfirm" runat="server" SkinID="_confirm" ClientInstanceName="imbconfirm"
                                                                            CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" ClientVisible="false">
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="imbcancel" runat="server" SkinID="_disconfirm" ClientInstanceName="imbcancel"
                                                                            CausesValidation="False" CssClass="dxeLineBreakFix" ClientVisible="false">
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <FooterTemplate>
                                                                    </FooterTemplate>
                                                                    <CellStyle Cursor="hand" HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <FooterCellStyle HorizontalAlign="Right" />
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataTextColumn FieldName="SCARTYPENAME" Caption="SCARTYPENAME" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DDATE" Caption="DDATE" Visible="false">
                                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="STRAILERID" Caption="STRAILERID" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="DFINAL_MA" Caption="DFINAL_MA" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NDAY_MA" Caption="NDAY_MA" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="CPASSED" Caption="CPASSED" Visible="false">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="CMA" Caption="CMA" Visible="false">
                                                                </dx:GridViewDataTextColumn>

                                                            </Columns>
                                                            <SettingsPager Mode="ShowAllRecords" />
                                                            <SettingsBehavior AllowSort="false" />
                                                        </dx:ASPxGridView>

                                                    </td>
                                                </tr>
                                            </table>

                                        </EditForm>
                                    </Templates>
                                    <Settings ShowFooter="True" />
                                </dx:ASPxGridView>
                                <asp:DataGrid ID="__dgd" runat="server">
                                </asp:DataGrid>
                                <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
                                    HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true"
                                    SkinID="popUp">
                                    <ContentCollection>
                                        <dx:PopupControlContentControl>
                                            <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                                                <tr>
                                                    <td width='20' background='images/bd001_a.jpg' height='9'></td>
                                                    <td background='images/bd001_b.jpg'>&nbsp;
                                                    </td>
                                                    <td width='20' height='20' align='left' valign='top' background='images/bd001_c.jpg'></td>
                                                </tr>
                                                <tr>
                                                    <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                                                        <img src='images/bd001_d.jpg' width='20' height='164'>
                                                    </td>
                                                    <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                                                        <table>
                                                            <tr>
                                                                <td>รายละเอียดสัญญา
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                                                    </td>
                                                    <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                                        <img src='images/bd001_e.jpg' width='20' height='164'>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width='20'>
                                                        <img src='images/bd001_f.jpg' width='20' height='20'>
                                                    </td>
                                                    <td background='images/bd001_g.jpg'>
                                                        <img src='images/bd001_g.jpg' width='20' height='20'>
                                                    </td>
                                                    <td width='20'>
                                                        <img src='images/bd001_h.jpg' width='20' height='20'>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dx:PopupControlContentControl>
                                    </ContentCollection>
                                </dx:ASPxPopupControl>

                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CacheKeyDependency="ckdTruck" EnableCaching="True" CancelSelectOnNullParameter="False"
                                    SelectCommand="SELECT CONT_TRCK.STRUCKID  ,VEH_TRCK.SHEADREGISTERNO  
FROM TCONTRACT CONT
LEFT JOIN TCONTRACT_TRUCK CONT_TRCK ON CONT.SCONTRACTID =CONT_TRCK.SCONTRACTID
LEFT JOIN TVENDOR VEND ON CONT.SVENDORID=VEND.SVENDORID
LEFT JOIN TTRUCK VEH_TRCK ON CONT_TRCK.STRUCKID=VEH_TRCK.STRUCKID
LEFT JOIN TTRUCK TU_TRCK ON CONT_TRCK.STRAILERID =TU_TRCK.STRUCKID
 
WHERE 1=1 AND NVL(CONT.CACTIVE,'Y') !='N'
AND CONT.SVENDORID IN (SELECT SVENDORID FROM TUSER WHERE 1=1 and SUID= :usr_SUID )">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="usr_SUID" SessionField="UserID" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:SqlDataSource ID="sdsCheckLists" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CacheKeyDependency="ckdCheckLists" EnableCaching="True" CancelSelectOnNullParameter="False"
                                    SelectCommand="SELECT rownum SKEYID, TOCL.STYPECHECKLISTID,TOCL.STYPECHECKLISTNAME,TOCL.CACTIVE TOCL_CACTIVE,TOCL.CCASE,TOCL.CDESPOA,TOCL.CDESPOB,TOCL.CSUREPRISE,TOCL.CVENDOR,TOCL.CTYPE,TOCL.SMENUID
 ,GOCL.SGROUPID ,GOCL.SGROUPNAME ,GOCL.CGAS ,GOCL.COIL ,GOCL.CACTIVE GOCL_CACTIVE
 ,CL.SVERSIONLIST ,CL.SVERSION ,CL.STOPICID ,CL.SCHECKLISTNAME ,CL.SCHECKLISTID ,CL.NLIST ,CL.NDAY_MA ,CL.CCUT ,CL.CBAN ,CL.CACTIVE CL_CACTIVE ,TP.NPOINT
FROM TTYPEOFCHECKLIST TOCL
LEFT JOIN TGROUPOFCHECKLIST GOCL ON  TOCL.STYPECHECKLISTID=GOCL.STYPECHECKLISTID
LEFT JOIN TCHECKLIST CL ON  TOCL.STYPECHECKLISTID=CL.STYPECHECKLISTID AND GOCL.SGROUPID=CL.SGROUPID
LEFT JOIN TTOPIC TP ON CL.STOPICID=TP.STOPICID
where 1=1 
AND NVL(TOCL.CACTIVE,'0')='1' AND NVL(GOCL.CACTIVE,'0')='1' AND NVL(CL.CACTIVE,'0')='1' AND NVL(TP.CACTIVE,'0')='1'
AND tocl.ctype ='1' AND TOCL.CVENDOR='1'
--AND TOCL.STYPECHECKLISTID='1'"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <table>
                    <tr>
                        <td>สถานะของสี
                        </td>
                        <td style="border-style: solid; border-width: 1px; border-color: Black; background-color: #F5F5F5"
                            width="15px" title="ยังไม่ยืนยันรถ/สามารถยืนยันรายการได้">&nbsp;
                        </td>
                        <td>= ยังไม่ยืนยันรถ
                        </td>
                        <td style="background-color: #CEECF5" width="15px" title="ยืนยันรถแล้ว"></td>
                        <td>= ยืนยันรถแล้ว
                        </td>
                        <td style="background-color: #F6E3CE" width="15px" title="เลยระยะเวลายืนยันรถ/ไม่สามารถยืนยันรายการได้"></td>
                        <td>= เลยระยะเวลายืนยันรถ
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
    <script type="text/javascript">
        setdatepicker();
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            setdatepicker();
        }
        function setdatepicker() {
            $('.dteEnd').parent().click(function () {
                var date = new Date();
                date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=dteEnd.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(date);
            });

            $(".wrapper1").on("scroll", function () {
                alert('aaaaa');
                $(".wrapper2")
                    .scrollLeft($(".wrapper1").scrollLeft());
            });
            $(".wrapper2").on("scroll", function () {
                $(".wrapper1")
                    .scrollLeft($(".wrapper2").scrollLeft());
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
