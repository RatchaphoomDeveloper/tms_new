﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Kpi_process : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetinitalForm();
        }
    }
    #region InitalForm
    private void SetinitalForm()
    {
        try
        {
            this.SetinitalControl();
            this.SetLoadData();

        }
        catch (Exception ex)
        {

            throw;
        }
    }

    private void SetLoadData()
    {
        try
        {
            gvd_kpi.DataSource = KPIBLL.Instance.GetVendorContract(string.Empty);
            gvd_kpi.DataBind();

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void SetinitalControl()
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref ddlYear, SetYear(), "value", "text", false);
            DataTable dtVendor = VendorBLL.Instance.TVendorSapSelect();
            DropDownListHelper.BindDropDownList(ref ddl_vendor, dtVendor, "SVENDORID", "SABBREVIATION", true);
            DataTable dtContract = KPIBLL.Instance.GetAllContract();
            DropDownListHelper.BindDropDownList(ref ddl_contract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    
    
    #region Event
        #region ประเมิน KPI button
        protected void gvd_kpi_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Detail"))
            {
                int index = Convert.ToInt32(e.CommandArgument);
                KeyRow.Value = index + "";
                DropDownListHelper.BindDropDownList(ref ddlMonth, SetMonth(), "ID", "MONTH", false);
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "Showmodal();", true);
                
            }
        }
        #endregion
        #region Dropdown ผู้ขนส่งไปหาสัญญา
        protected void ddl_vendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtContract = TMS_BLL.Master.ContractBLL.Instance.ConTractSelectByVendor(ddl_vendor.SelectedValue.ToString());
            if (dtContract.Rows.Count > 0)
            {
                DropDownListHelper.BindDropDownList(ref ddl_contract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dtContract = KPIBLL.Instance.GetAllContract();
                DropDownListHelper.BindDropDownList(ref ddl_contract, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        #endregion
        #region Search Data
        protected void cmdSearch_Click(object sender, EventArgs e)
            {
                try
                {
                    string Condition = string.Empty;
                    if(ddl_vendor.SelectedIndex >0)
                    {
                        Condition += " AND V.SVENDORID ='" + ddl_vendor.SelectedValue.ToString() + "'";
                    }
                    if(ddl_contract.SelectedIndex>0)
                    {
                        Condition += " AND SCONTRACTID ='" + ddl_contract.SelectedValue.ToString() +"'";
                    }
                    
                        gvd_kpi.DataSource = KPIBLL.Instance.GetVendorContract(Condition);
                        gvd_kpi.DataBind();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        #endregion
        #region  ClickManual
        protected void cmdManual_Click(object sender, EventArgs e)
        {
            string url = "ManualReportKpi.aspx?str=";

            string strData = ddlYear.Text + "&" + ddlMonth.SelectedValue.ToString() + "&" + gvd_kpi.DataKeys[int.Parse(KeyRow.Value)].Value.ToString()
                + "&" + gvd_kpi.Rows[int.Parse(KeyRow.Value)].Cells[0].Text.ToString() + "&" + gvd_kpi.Rows[int.Parse(KeyRow.Value)].Cells[1].Text.ToString();
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
            string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            url += encryptedValue;
            Response.Redirect(url);
        }
    #endregion
        #region ClickExcel
        protected void cmdImport_Click(object sender, EventArgs e)
        {
            string url = "ExcelReportKpi.aspx?str=";

            string strData = ddlYear.Text + "&" + ddlMonth.SelectedValue.ToString() + "&" + gvd_kpi.DataKeys[int.Parse(KeyRow.Value)].Value.ToString()
                + "&" + gvd_kpi.Rows[int.Parse(KeyRow.Value)].Cells[0].Text.ToString() + "&" + gvd_kpi.Rows[int.Parse(KeyRow.Value)].Cells[1].Text.ToString();
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
            string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
            url += encryptedValue;
            Response.Redirect(url);
        }
        #endregion
        #endregion
        #region Miscellaneous
        #region SetMonth
        private DataTable SetMonth()
            {
                DataTable dt = new DataTable();
                DataRow dr = null;
                dt.Columns.Add(new DataColumn("ID", typeof(int)));
                dt.Columns.Add(new DataColumn("MONTH", typeof(string)));
                
                int[] Id = { 1,2,3,4,5,6,7,8,9,10,11,12 };
                string[] month = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
                dr = dt.NewRow();
                for (int i = 0; i < Id.Length; i++)
                {
                    dr = dt.NewRow();
                    dr["ID"] = Id[i];
                    dr["MONTH"] = month[i];
                    dt.Rows.Add(dr);
                }                
                return dt;
            }
        #endregion        
        #region Set Year
            private DataTable SetYear()
            {
                try
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("value");
                    dt.Columns.Add("text");
                    dt.Rows.Add(int.Parse(DateTime.Now.Year.ToString()) - 1, int.Parse(DateTime.Now.Year.ToString()) - 1);
                    for (int i = 0; i < 5; i++)
                    {
                        dt.Rows.Add(int.Parse(DateTime.Now.Year.ToString()) + i, int.Parse(DateTime.Now.Year.ToString()) + i);
                    }
                    return dt;

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        #endregion
    #endregion

            
}