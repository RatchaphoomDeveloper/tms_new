﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="DetailDefectMonitoring.aspx.cs" Inherits="DetailDefectMonitoring" StylesheetTheme="Aqua" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <link href="Css/ccs_thm.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" OnCallback="xcpn_Callback1" ClientInstanceName="xcpn"
        CausesValidation="False" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent2" runat="server">
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <table width="100%">
                    <tr>
                        <td colspan="8" align="right">
                            <table cellpadding="0" cellspacing="0" style="margin-bottom: 16px">
                                <tr>
                                    <td style="padding-right: 4px">
                                        <div style="float: left">
                                            <dx:ASPxButton ID="btnPdfExport2" ToolTip="Export to PDF" runat="server" UseSubmitBehavior="False"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                OnClick="btnPdfExport2_Click">
                                                <Image Width="16px" Height="16px" Url="Images/ic_pdf2.gif">
                                                </Image>
                                            </dx:ASPxButton>
                                        </div>
                                    </td>
                                    &nbsp
                                    <td style="padding-right: 4px">
                                        <div style="float: left">
                                            <dx:ASPxButton ID="btnXlsExport2" ToolTip="Export to Excel" runat="server" UseSubmitBehavior="False"
                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="dd" Cursor="pointer"
                                                OnClick="btnXlsExport2_Click">
                                                <Image Width="16px" Height="16px" Url="Images/ic_ms_excel.gif">
                                                </Image>
                                            </dx:ASPxButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">
                            &nbsp;
                        </td>
                        <td>
                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ผู้ขนส่ง,เลขที่สัญญา,ทะเบียน">
                            </dx:ASPxTextBox>
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                            </dx:ASPxDateEdit>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <dx:ASPxComboBox ID="cboType" runat="server" ValueField="SPROCESSID" TextField="SPROCESSNAME"
                                DataSourceID="sdsType" ClientInstanceName="cboStatus" Width="140px" SelectedIndex="0">
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="sdsType" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser" SelectCommand="SELECT SPROCESSID,SPROCESSNAME FROM TPROCESS">
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }"></ClientSideEvents>
                            </dx:ASPxButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="8">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw"
                                DataSourceID="sds" OnAfterPerformCallback="gvw_AfterPerformCallback" OnCustomColumnDisplayText="gvw_CustomColumnDisplayText"
                                KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px" Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="ที่" ShowInCustomizationForm="True" VisibleIndex="0"
                                        Width="1%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="รหัส" FieldName="SREFERENCEID" Visible="False"
                                        VisibleIndex="1">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ประเภท" FieldName="SPROCESSID" Visible="False"
                                        VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สาเหตุที่หัก" FieldName="SREDUCENAME" Visible="False"
                                        VisibleIndex="2">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="วันที่หักคะแนน" FieldName="DREDUCE" VisibleIndex="1"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" VisibleIndex="2"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สัญญา" FieldName="SCONTRACTNO" VisibleIndex="3"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ(หัว)" FieldName="SHEADREGISTERNO" VisibleIndex="4"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ(หาง)" FieldName="STRAILERREGISTERNO"
                                        VisibleIndex="5" Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="สาเหตุ" FieldName="SPROCESSNAME" VisibleIndex="6"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <DataItemTemplate>
                                            <dx:ASPxHyperLink ID="hlProcessName" ClientInstanceName="hlProcessName" runat="server"
                                                Text='<%# Eval("SPROCESSNAME") %>'>
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('hlProcessNameClick;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length))}" />
                                            </dx:ASPxHyperLink>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Delivery Number" FieldName="DELIVIERYNO" VisibleIndex="7"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ต้นทาง" FieldName="STARTTERMINAL" VisibleIndex="8"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ปลายทาง" FieldName="ENDTERMINAL" VisibleIndex="8"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ผู้บันทึก" FieldName="REDUCEBY" VisibleIndex="9"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                                <SettingsPager AlwaysShowPager="True">
                                </SettingsPager>
                            </dx:ASPxGridView>

                              <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvw" PageHeader-Font-Names="Tahoma"
                                            PageHeader-Font-Bold="true">
                                            <Styles>
                                                <Header Font-Names="Tahoma" Font-Size="10">
                                                </Header>
                                                <Cell Font-Names="Tahoma" Font-Size="8">
                                                </Cell>
                                            </Styles>
                                            <PageHeader>
                                                <Font Bold="True" Names="Tahoma"></Font>
                                            </PageHeader>
                                        </dx:ASPxGridViewExporter>

                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                EnableCaching="True" CacheKeyDependency="ckdUser"></asp:SqlDataSource>
                            <dx:ASPxPopupControl ID="popupControl" runat="server" CloseAction="OuterMouseClick"
                                HeaderText="รายละเอียด" ClientInstanceName="popupControl" Width="600px" Modal="true"
                                SkinID="popUp">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                        <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>
                                            <tr>
                                                <td width='20' background='images/bd001_a.jpg' height='9'>
                                                </td>
                                                <td background='images/bd001_b.jpg'>
                                                    &nbsp;
                                                </td>
                                                <td width='20' height='20' align='left' valign='top' background='images/bd001_c.jpg'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='20' align='left' valign='top' background='images/bd001_dx.jpg'>
                                                    <img src='images/bd001_d.jpg' width='20' height='164'>
                                                </td>
                                                <td align='left' valign='top' background='images/bd001_i.jpg' bgcolor='#FFFFFF' style='background-repeat: repeat-x'>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                รายละเอียด
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
                                                </td>
                                                <td width='20' align='left' valign='top' background='images/bd001_ex.jpg'>
                                                    <img src='images/bd001_e.jpg' width='20' height='164'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width='20'>
                                                    <img src='images/bd001_f.jpg' width='20' height='20'>
                                                </td>
                                                <td background='images/bd001_g.jpg'>
                                                    <img src='images/bd001_g.jpg' width='20' height='20'>
                                                </td>
                                                <td width='20'>
                                                    <img src='images/bd001_h.jpg' width='20' height='20'>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                            </dx:ASPxPopupControl>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
