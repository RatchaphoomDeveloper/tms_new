﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AnnualScoreMain.aspx.cs" Inherits="AnnualScoreMain" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"], .radio input[type="radio"], .radio-inline input[type="radio"] {
            margin-left: 0px;
        }

        .checkbox label, .radio label {
            padding-right: 10px;
            padding-left: 15px;
        }

        .required.control-label:after {
            content: "*";
            color: red;
        }

        .form-horizontal .control-label.text-left {
            text-align: left;
            left: 0px;
        }

        .GridColorHeader th {
            text-align: inherit !important;
            align-content: inherit !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grvMain" EventName="PageIndexChanging" />
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlVendorSearch" EventName="SelectedIndexChanged" />
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
        <ContentTemplate>
            <div class="panel panel-info" style="margin-top: 20px;">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>ค้นหาข้อมูลทั่วไป
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body" style="padding-bottom: 0px;">
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlVendorSearch.ClientID %>" class="col-md-2 control-label">ผู้ประกอบการขนส่ง</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlVendorSearch" runat="server" DataTextField="SABBREVIATION" CssClass="form-control marginTp" DataValueField="SVENDORID" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorSearch_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlYearSearch.ClientID %>" class="col-md-1 control-label">ปี</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control marginTp">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=ddlContract.ClientID %>" class="col-md-2 control-label">เลขที่สัญญา</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label for="<%=ddlGroup.ClientID %>" class="col-md-2 control-label">กลุ่มงาน</label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control marginTp" DataTextField="NAME" DataValueField="ID">
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=txtCARREGISTERNO.ClientID %>" class="col-md-1 control-label">ทะเบียนรถ</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtCARREGISTERNO" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                                <label for="<%=txtEMPLOYEE.ClientID %>" class="col-md-2 control-label">ชื่อ พขร. / ID พขร.</label>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtEMPLOYEE" runat="server" ClientIDMode="Static" CssClass="form-control input-md"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group form-horizontal row">
                                <label class="col-md-1 control-label">ช่วงเวลา </label>
                                <hr />
                            </div>
                            <div class="form-group form-horizontal row">

                                <label for="<%=rdoQuarter.ClientID %>" class="col-md-2 control-label">
                                    <asp:RadioButton ID="rdoQuarter" GroupName="TimeRange" Text="&nbsp;ไตรมาส" runat="server" /></label>
                                <div class="col-md-1">
                                    <asp:DropDownList ID="ddlQuarter" runat="server" CssClass="form-control marginTp">
                                        <asp:ListItem Text="1"></asp:ListItem>
                                        <asp:ListItem Text="2"></asp:ListItem>
                                        <asp:ListItem Text="3"></asp:ListItem>
                                        <asp:ListItem Text="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=rdoMonth.ClientID %>" class="col-md-1 control-label">
                                    <asp:RadioButton ID="rdoMonth" GroupName="TimeRange" Text="&nbsp;เดือน" runat="server" />
                                </label>
                                <div class="col-md-2">
                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control marginTp">
                                        <asp:ListItem Value="01" Text="มกราคม"></asp:ListItem>
                                        <asp:ListItem Value="02" Text="กุมภาพันธ์"></asp:ListItem>
                                        <asp:ListItem Value="03" Text="มีนาคม"></asp:ListItem>
                                        <asp:ListItem Value="04" Text="เมษายน"></asp:ListItem>
                                        <asp:ListItem Value="05" Text="พฤษภาคม"></asp:ListItem>
                                        <asp:ListItem Value="06" Text="มิถุนายน"></asp:ListItem>
                                        <asp:ListItem Value="07" Text="กรกฎาคม"></asp:ListItem>
                                        <asp:ListItem Value="08" Text="สิงหาคม"></asp:ListItem>
                                        <asp:ListItem Value="09" Text="กันยายน"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="ตุลาคม"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="พฤศจิกายน"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="ธันวาคม"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label for="<%=rdoDate.ClientID %>" class="col-md-1 control-label">
                                    <asp:RadioButton ID="rdoDate" GroupName="TimeRange" Text="&nbsp;วันที่" runat="server" />
                                </label>
                                <div class="col-md-5">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtDateStart" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                            <td>
                                                <label class="control-label">&nbsp;ถึง&nbsp;</label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDateEnd" runat="server" CssClass="datepicker"
                                                    Style="text-align: center"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                            <div class="form-group form-horizontal row"  id="dvChkaccident2" runat="server">
                                <label class="col-md-1 control-label">หัวข้อ </label>
                                <hr />
                            </div>
                            <div class="form-group form-horizontal row"> 
                                    <label for="<%=Chkaccident.ClientID %>" class="col-md-1 control-label"></label>
                                    <div class="col-md-5 checkbox">
                                        <asp:CheckBoxList ID="Chkaccident" runat="server" RepeatColumns="2">
                                        </asp:CheckBoxList>
                                    </div> 
                                <div class="col-md-5" style="text-align: right;">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-hover btn-info" OnClick="btnSearch_Click" />
                                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-hover btn-info" OnClick="btnClear_Click" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export Excel" CssClass="btn btn-hover btn-info" OnClick="btnExport_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label runat="server" ID="lblHeader">ข้อมูลการตัดคะแนน จำนวน {0} สัญญา</asp:Label>
                </div>
                <div class="panel-body">
                    <asp:GridView ID="grvMain" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" OnRowCommand="grvMain_RowCommand" OnRowDataBound="grvMain_RowDataBound" OnPageIndexChanging="grvMain_PageIndexChanging" DataKeyNames="SCONTRACTNO,SABBREVIATION,GROUPNAME" PageSize="50"
                        EmptyDataText="[ ไม่มีข้อมูล ]" AllowPaging="True">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ลำดับ" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="150px" ItemStyle-CssClass="center" HeaderText="บริษัทผู้ขนส่ง" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Left" Width="150px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSABBREVIATION" runat="server" Text='<%# Eval("SABBREVIATION") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-CssClass="center" HeaderText="กลุ่มงาน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblGROUPNAME" runat="server" Text='<%# Eval("GROUPNAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="100px" ItemStyle-CssClass="center" HeaderText="เลขที่สัญญา" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSCONTRACTNO" runat="server" Text='<%# Eval("SCONTRACTNO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="ระงับ (วัน)" DataField="DISABLE_DRIVER" DataFormatString="{0:N}">
                                <ItemStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-CssClass="center" HeaderText="ตัดคะแนน" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="10px" />
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemTemplate>
                                    <asp:Label ID="lbSUMNPOINT" runat="server" Text='<%# Eval("SUMNPOINT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="ค่าปรับ + ค่าเสียหาย" DataField="COST" DataFormatString="{0:N}">
                                <ItemStyle HorizontalAlign="Center" CssClass="StaticCol" Width="50px"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="เรียกดูข้อมูล" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center" CssClass="StaticCol" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/btnSearch.png" Width="16px" Height="16px" Style="cursor: pointer" CausesValidation="False" BorderWidth="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                        <HeaderStyle HorizontalAlign="Center" CssClass="GridColorHeader"></HeaderStyle>
                        <PagerStyle CssClass="pagination-ys" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <asp:Button ID="btnView" runat="server" OnClick="btnView_Click" />
        <input type="text" name="hdnNum" id="hdnNum" runat="server" value="0">
    </div>
    <script type="text/javascript">
        function gotoView(num) {
            $('#<%=hdnNum.ClientID%>').attr('value', num);
            document.getElementById("<%=btnView.ClientID%>").click();
            openInNewTab('AnnualScoreDetails.aspx');
        }
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

