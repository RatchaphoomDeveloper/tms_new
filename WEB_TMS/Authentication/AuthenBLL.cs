﻿using TMS_DAL.Authentication;
using System;
using System.Data;

namespace TMS_BLL.Authentication
{
    public class AuthenBLL
    {
        public DataTable AuthenSelectBLL(string UserGroupID)
        {
            try
            {
                return AuthenDAL.Instance.AuthenSelectDAL(UserGroupID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void AuthenInsertBLL(string UserGroupID, DataTable dtAuthen, int UserID)
        {
            try
            {
                AuthenDAL.Instance.AuthenInsertDAL(UserGroupID, dtAuthen, UserID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable AuthenSelectTemplateBLL()
        {
            try
            {
                return AuthenDAL.Instance.AuthenSelectTemplateDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static AuthenBLL _instance;
        public static AuthenBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AuthenBLL();
                }
                return _instance;
            }
        }
        #endregion
    }
}