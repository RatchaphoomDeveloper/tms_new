﻿<%@ Page MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true" EnableViewState="true"
    CodeFile="Truck_History_View.aspx.cs" Inherits="Truck_History_View" StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
<link href="Css/jquery.auto-complete.css" rel="stylesheet" type="text/css" />
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <script src="JQuery/jquery.auto-complete.min.js" type="text/javascript"></script>
    <script src="JQuery/jquery.validate.min.js" type="text/javascript"></script>
    <script src="JQuery/messages_th.min.js" type="text/javascript"></script>
<%--        <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
    <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script> --%>
    <style type="text/css">
    .error
    {
        color: red;
        }
       
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $(".placehoderKg").attr("placeholder", "KG");            
            
            if ($('#<%=rblRValveType.ClientID %> input:checked').val() == '3') {
                $("#<%=txtRValveType.ClientID%>").attr("Readonly", false);
            } else {
                $("#<%=txtRValveType.ClientID%>").attr("Readonly", true);
            }
            if ($('#<%=rblRFuelType.ClientID %> input:checked').val() == '3') {
                $("#<%=txtRFuelType.ClientID%>").attr("Readonly", false);
            } else {
                $("#<%=txtRFuelType.ClientID%>").attr("Readonly", true);
            }
            $("#<%= rblRValveType.ClientID %> input").addClass("required");
            $("#<%= rblRFuelType.ClientID %> input").addClass("required");
            $("#<%= rblSemiPumpPower.ClientID %> input").addClass("required");
            $('#<%=rblRValveType.ClientID %>').click(function () {
                switch ($('#<%=rblRValveType.ClientID %> input:checked').val()) {
                    case '3':
                        $("#<%=txtRValveType.ClientID%>").attr("Readonly", false);
                        $("#<%=txtRValveType.ClientID%>").addClass("required");
                        break;
                    default:
                        $("#<%=txtRValveType.ClientID%>").attr("Readonly", true);
                        $("#<%=txtRValveType.ClientID%>").removeClass("required");
                        break;
                };
            });
            //ชนิดน้ำมัน 
            $('#<%=rblRFuelType.ClientID %>').click(function () {
                switch ($('#<%=rblRFuelType.ClientID %> input:checked').val()) {
                    case '3':
                        $("#<%=txtRFuelType.ClientID%>").attr("Readonly", false);
                        $("#<%=txtRFuelType.ClientID%>").addClass("required");
                        break;
                    default:
                        $("#<%=txtRFuelType.ClientID%>").attr("Readonly", true);
                        $("#<%=txtRFuelType.ClientID%>").removeClass("required");
                        break;
                };
            });
            $("#<%=cmbHsHolder.ClientID%>,#<%=cmbSemiHolder.ClientID%>").autoComplete({
                source: function (term, response) {
                    $.ajax({
                        type: "POST",
                        //getListOfCars is my webmethod   
                        url: "truck_edit.aspx/VendorJson",
                        data: '{Vendor: "' + term + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        error: OnErrorCall
                    });
                    function OnSuccess(data) {
                        response(data.d);
                    }
                    function OnErrorCall(data)
                    { alert(data); }
                }
            });
            
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">    
    <h5>รายละเอียดข้อมูลรถ</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ชนิดรถ
        </div>
        <div class="panel-body">            
            <asp:Table ID="Table3" runat="server" Width="958px" Height="76px">
                <asp:TableRow ID="TableRow13" runat="server">
                <asp:TableCell ID="TableCell53" Width="20%"  runat="server" HorizontalAlign="Right">
                    ชนิดรถ:
                    <asp:Label ID="Label28" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell54" Width="30%"  runat="server">
                    <asp:DropDownList ID="cmbCarcate" AutoPostBack="true" runat="server" CssClass="form-control" >
                                                
                    </asp:DropDownList>
                </asp:TableCell>
                 <asp:TableCell ID="TableCell55" Width="2%"  runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell56" Width="18%"  runat="server" HorizontalAlign="Right">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell57" Width="30%"  runat="server">
                   &nbsp;
                </asp:TableCell>
            </asp:TableRow> 
                
                <asp:TableRow ID="TableRow29" runat="server">
                        <asp:TableCell ID="TableCell110" Width="20%" runat="server" HorizontalAlign="Right">
                        
                        <asp:Label ID="Label39" runat="server" Visible="true" ForeColor="Red" Text=""></asp:Label>
                            &nbsp;&nbsp;
                        </asp:TableCell>
                        <asp:TableCell ID="TableCell111" Width="30%" runat="server">
                            <asp:RadioButtonList ID="radSpot" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                        </asp:TableCell>
                        <asp:TableCell ID="TableCell112" Width="2%" runat="server">
                        &nbsp;
                        </asp:TableCell>
                        <asp:TableCell ID="TableCell113" Width="18%" runat="server" HorizontalAlign="Right">
                        &nbsp;
                        </asp:TableCell>
                        <asp:TableCell ID="TableCell114" Width="30%" runat="server">
                       &nbsp;
                        </asp:TableCell>
                    </asp:TableRow>
            </asp:Table>
        </div>
    </div>
 <br />
<div id="Truck" runat="server" class="panel panel-info" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลรถ</i>
    </div>
    <div class="panel-body">
        <asp:Table ID="Table1" runat="server" Width="958px" Height="76px">
            
            <asp:TableRow runat="server">
                <asp:TableCell ID="TableCell1" Width="20%"  runat="server" HorizontalAlign="Right">
                    ทะเบียนรถ:
                    <asp:Label ID="Label3" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell2" Width="30%"  runat="server">
                    <asp:TextBox ID="txtHRegNo" runat="server" Text="" CssClass="form-control required" MaxLength="10" ReadOnly="true" PlaceHolder="xxx.-xxxx"></asp:TextBox>                    
                </asp:TableCell>
                 <asp:TableCell ID="TableCell9" Width="2%"  runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell3" Width="18%"  runat="server" HorizontalAlign="Right">
                    ผู้ถือกรรมสิทธิ์:: 
                    <asp:Label ID="Label4" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell4" Width="30%"  runat="server">                
                  <asp:TextBox ID="cmbHsHolder" runat="server" CssClass="form-control required" ClientIDMode="Static" MaxLength="99" ></asp:TextBox>

                  </asp:TableCell>
                  
            </asp:TableRow>
            <asp:TableRow ID="TableRow2"  runat="server">
                <asp:TableCell ID="TableCell5"  runat="server" HorizontalAlign="Right">
                    Route:: 
                    <asp:Label ID="Label5" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell6"  runat="server">
                    <asp:DropDownList ID="cmbHRoute" runat="server" class="form-control required" Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell163"  runat="server" HorizontalAlign="Right">
                        หมายเลขเครื่องยนต์::  
                    <asp:Label ID="Label67" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell164"  runat="server">
                    <asp:TextBox ID="txtHsEngine" runat="server" Text="" CssClass="form-control required"></asp:TextBox>
                </asp:TableCell>                
            </asp:TableRow>
            <asp:TableRow ID="TableRow14"   runat="server">
                <asp:TableCell ID="TableCell66"  runat="server" HorizontalAlign="Right">
                    ประเภทรถ(SAP):: 
                    <asp:Label ID="Label2" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="cmbHTru_Cate" runat="server" CssClass="form-control required">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell67"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell93"  runat="server" HorizontalAlign="Right">
                        รุ่น ::  
                    <asp:Label ID="Label23" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell94"  runat="server">
                
                    <asp:DropDownList ID="txtHsModel" runat="server" CssClass="form-control required"></asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow3"   runat="server">
                <asp:TableCell ID="TableCell7"  runat="server" HorizontalAlign="Right">
                    หมายเลขแซสซีย์:: 
                    <asp:Label ID="Label7" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:TextBox ID="txtHsChasis" runat="server" Text="" CssClass="form-control required" MaxLength="20" ReadOnly="true"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell8"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell  runat="server" HorizontalAlign="Right">
                        ระบบกันสะเทือน ::   
                    <asp:Label ID="lblred" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell10"  runat="server">
                    <asp:DropDownList ID="cboHsVibration" runat="server" class="form-control required" Width="100%">
                    <asp:ListItem Value=""> - เลือก - </asp:ListItem>
                    <asp:ListItem Value="แหนบ">แหนบ</asp:ListItem>
                    <asp:ListItem Value="ถุงลม">ถุงลม</asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow4"   runat="server">
                <asp:TableCell ID="TableCell11" Width="150px" runat="server" HorizontalAlign="Right">
                    จำนวนล้อ:: 
                    <asp:Label ID="Label11" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="wheel" runat="server" CssClass="form-control required">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell12"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell13"  runat="server" HorizontalAlign="Right">
                        วัสดุที่สร้าง Pressure ของปั้ม Power ::   
                    <asp:Label ID="Label10" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell14"  runat="server">
                    <asp:DropDownList ID="cboPressure" runat="server" class="form-control required" Width="100%">
                        <asp:ListItem Value=""> - เลือก - </asp:ListItem>
                        <asp:ListItem Value="ทองหลือง">ทองหลือง</asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow5"   runat="server">
                <asp:TableCell ID="TableCell15" runat="server" HorizontalAlign="Right">
                    จำนวนเพลาขับเคลื่อน::  
                    <asp:Label ID="Label9" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;&nbsp;&nbsp;*&nbsp;"></asp:Label>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="txtRnShaftDriven" runat="server" CssClass="form-control required">
                    </asp:DropDownList>
                    
                </asp:TableCell>
                <asp:TableCell ID="TableCell16"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell17"  runat="server" HorizontalAlign="Right">
                    น้ำหนักรถ::  
                    <asp:Label ID="Label12" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell18"  runat="server">
                    <asp:TextBox ID="txtRnWeight" runat="server" Text="" CssClass="form-control required number" MaxLength="8" PlaceHolder="KG"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow42"   runat="server">
                <asp:TableCell ID="TableCell159" runat="server" HorizontalAlign="Right">
                    จำนวนเพลารับน้ำหนัก::  
                    <asp:Label ID="Label6" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;&nbsp;&nbsp;*&nbsp;"></asp:Label>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="txtRwightDriven" CssClass="form-control required" runat="server">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell160"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell19"  runat="server" HorizontalAlign="Right">
                        ชนิดของปั้ม Power::  
                    <asp:Label ID="Label14" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="cboHPumpPower_type" runat="server" class="form-control required" Width="100%">
                    <asp:ListItem Value="" >- เลือก - </asp:ListItem>
                    <asp:ListItem Value="เกียร์ปั๊ม" >เกียร์ปั๊ม</asp:ListItem>
                    <asp:ListItem Value="ไฟเบอร์โลตารี">ไฟเบอร์โลตารี</asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow6"   runat="server">
                <asp:TableCell ID="TableCell20"  runat="server" HorizontalAlign="Right">
                        หน่วยความจุเชื้อเพลิง::  
                    <asp:Label ID="Label13" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                   <asp:DropDownList ID="txtVEH_Volume" runat="server" CssClass="form-control required">
                   <asp:ListItem Value="L" Text="L"></asp:ListItem>
                   <asp:ListItem Value="KG" Text="KG"></asp:ListItem>
                   </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell26"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell27"  runat="server" HorizontalAlign="Right">
                        บริษัททีให้บริการ GPS::  
                    <asp:Label ID="Label16" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:DropDownList ID="cboHGPSProvider" runat="server" class="form-control required" Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="companyTank"   runat="server" Visible="false">
                 <asp:TableCell ID="TableCell96"  runat="server" HorizontalAlign="Right">
                       บริษัทผู้ผลิตถัง :: 
                    <asp:Label ID="Label32" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;&nbsp;"></asp:Label>
                     &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell101"  runat="server">
                    <asp:TextBox ID="TruckTank" runat="server" CssClass="form-control required" MaxLength="29"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell102" runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell103"  runat="server" HorizontalAlign="Right">
                       วัสดุที่ผลิตถัง ::
                       <asp:Label ID="Label36" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>                        
                </asp:TableCell>
                <asp:TableCell ID="TableCell104"  runat="server">
                    <asp:DropDownList ID="materialTank" runat="server" CssClass="form-control required">
                     <asp:ListItem Value="" Text=" - เลือก - "></asp:ListItem>
                     <asp:ListItem Value="STEEL" Text="STEEL"></asp:ListItem>
                     <asp:ListItem Value="Aluminium" Text="Aluminium"></asp:ListItem>
                     </asp:DropDownList> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow1" runat="server">                
                <asp:TableCell ID="TableCell92"  Width="18%"  runat="server" HorizontalAlign="Right">
                    วันที่จดทะเบียนครั้งแรก:: 
                    <asp:Label ID="Label31"  runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell95"  Width="30%"  runat="server">                
                  <asp:TextBox ID="first_regis" runat="server" CssClass="datepicker form-control required " ClientIDMode="Static" MaxLength="99" ></asp:TextBox>

                  </asp:TableCell>
                 <asp:TableCell ID="TableCell91"  Width="2%"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell83"  Width="20%"  runat="server" HorizontalAlign="Right">
                    กำลังเครื่องยนต์ :: 
                    <asp:Label ID="Label20" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>                  
                </asp:TableCell>
                <asp:TableCell ID="TableCell84"  Width="30%"  runat="server">
                       <asp:TextBox ID="txtHPowermover" runat="server" CssClass="form-control required number " ClientIDMode="Static" MaxLength="99" ></asp:TextBox>           
                </asp:TableCell>
                  
            </asp:TableRow>
            
           <%-- สร้างข้อมูลใหม่ --%>
            <asp:TableRow ID="TableRow8"   runat="server">
                <asp:TableCell ID="TableCell28"  runat="server" HorizontalAlign="Right">
                        ชนิดของวาล์วใต้ท้อง::  
                    <asp:Label ID="Label17" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                   <asp:RadioButtonList ID="rblRValveType" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal" CssClass="from-control required">
                        <asp:ListItem Value="1">&nbsp;&nbsp;วาล์วสลึง</asp:ListItem>
                        <asp:ListItem Value="2">&nbsp;&nbsp;วาล์วลม</asp:ListItem>
                        <asp:ListItem Value="3">&nbsp;&nbsp;วาล์วอื่นๆ โปรดระบุ</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell29"  runat="server" ColumnSpan="2">
                    <asp:TextBox ID="txtRValveType" runat="server" CssClass="form-control" MaxLength="15" ></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell30"  runat="server">             
                    
                </asp:TableCell>
                <asp:TableCell ID="TableCell31"  runat="server">
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow9"   runat="server">
                <asp:TableCell ID="TableCell32"  runat="server" HorizontalAlign="Right">
                        ประเภทของเชื้อเพลิง::  
                    <asp:Label ID="Label19" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:RadioButtonList ID="rblRFuelType" ClientIDMode="Static" runat="server" RepeatDirection="Horizontal" CssClass="required">
                    <asp:ListItem Value="1">DIESEL</asp:ListItem>
                    <asp:ListItem Value="2">NGV</asp:ListItem>
                    <asp:ListItem Value="3">อื่นๆ โปรดระบุ</asp:ListItem>
                    </asp:RadioButtonList>
                   
                </asp:TableCell>
                <asp:TableCell ID="TableCell33"  runat="server" ColumnSpan="2">
                    <asp:TextBox ID="txtRFuelType" runat="server" CssClass="form-control" MaxLength="15"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell34"  runat="server">
                      
                    
                </asp:TableCell>
                <asp:TableCell ID="TableCell35"  runat="server">
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow runat="server">
                <asp:TableCell runat="server" HorizontalAlign="Right">
                        &nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell runat="server" HorizontalAlign="Right">
                        
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow runat="server" ID="owncar">
                <asp:TableCell runat="server" HorizontalAlign="Right" ColumnSpan="3">
                        สถานะรถภายในระบบ::  
                    <asp:Label ID="Label22" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell  ColumnSpan="2">
                    <asp:RadioButtonList ID="type_vendor" ClientIDMode="Static" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="1" Text="รถในสังกัด"></asp:ListItem>
                    <asp:ListItem Value="0" Text="ปลดรถในสังกัด"></asp:ListItem>
                    </asp:RadioButtonList>
                </asp:TableCell>                
            </asp:TableRow>                                         
        </asp:Table>       
        </div>
    </div>
    <%-- End Truck--%><%-- ข้อมูลรถเซมิเทรลเลอร์ --%>
 <div id="semitruck" class="panel panel-info" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลรถหางลาก</i>
    </div>
    <div class="panel-body">
        <asp:Table ID="Table5" runat="server" Width="958px" Height="76px">
            
            <asp:TableRow ID="TableRow7" runat="server">
                <asp:TableCell ID="TableCell58" Width="20%"  runat="server" HorizontalAlign="Right">
                    ทะเบียนรถ:
                    <asp:Label ID="Label30" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell59" Width="30%"  runat="server">
                    <asp:TextBox ID="txtSemiNo" runat="server" Text="" CssClass="form-control required" MaxLength="10" ReadOnly="true" PlaceHolder="xxx.-xxxx"></asp:TextBox>
                </asp:TableCell>
                 <asp:TableCell ID="TableCell60" Width="2%"  runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell61" Width="18%"  runat="server" HorizontalAlign="Right">
                    ผู้ถือกรรมสิทธิ์:: 
                    <asp:Label ID="Label33" runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell62" Width="30%"  runat="server">

                    <asp:TextBox ID="cmbSemiHolder" CssClass="form-control required" runat="server" MaxLength="99"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow17"  runat="server">
                <asp:TableCell ID="TableCell70"  runat="server" HorizontalAlign="Right">
                    ประเภทรถ (SAP):: 
                    <asp:Label ID="Label34" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell71"  runat="server">
                    <asp:DropDownList ID="cmbCarSemiSap" runat="server" class="form-control required" DataTextField="STERMINALNAME"
                        DataValueField="STERMINALID" Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell72"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell73"  runat="server" HorizontalAlign="Right">
                       หมายเลขแซสซีย์ :: 
                    <asp:Label ID="Label35" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;&nbsp;" ></asp:Label>
                     &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell74"  runat="server">
                    <asp:TextBox ID="txtSemiChasis" runat="server" CssClass="form-control required" MaxLength="29" ReadOnly="true"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow20"  runat="server">
                <asp:TableCell ID="TableCell49"  runat="server" HorizontalAlign="Right">
                    รุ่น:: 
                    <asp:Label ID="Label27" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell50"  runat="server">
                    <asp:DropDownList ID="txtsemimodal" runat="server" class="form-control required" Width="100%">
                    </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell52"  runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell75" Width="150px" runat="server" HorizontalAlign="Right">
                    จำนวนล้อ:: 
                    <asp:Label ID="Label29" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                     <asp:DropDownList ID="SemiWheel" runat="server" CssClass="form-control required">
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow18"   runat="server">
             <asp:TableCell ID="TableCell23" Width="150px" runat="server" HorizontalAlign="Right">
                     น้ำหนักรถ::  
                    <asp:Label ID="Label40" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell25"  runat="server">
                    <asp:TextBox ID="txtSemiWeight" runat="server" Text="" CssClass="form-control required number" MaxLength="8" PlaceHolder="KG"></asp:TextBox>
                </asp:TableCell>
                 <asp:TableCell ID="TableCell76"  runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell21"  runat="server" HorizontalAlign="Right">
                        จำนวนเพลารับน้ำหนัก :: 
                    <asp:Label ID="Label15" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell22"  runat="server">
                    <asp:DropDownList ID="txtSemiShaftDriven" runat="server" CssClass="form-control required">
                    </asp:DropDownList>                    
                </asp:TableCell>                 
            </asp:TableRow>
            <asp:TableRow ID="TableRow24"   runat="server">
                 <asp:TableCell ID="TableCell77"  runat="server" HorizontalAlign="Right">
                       บริษัทผู้ผลิตถัง :: 
                    <asp:Label ID="Label21" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;&nbsp;"></asp:Label>
                     &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell78"  runat="server">
                    <asp:TextBox ID="txtownTank" runat="server" CssClass="form-control required" MaxLength="29"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell79" runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell80"  runat="server" HorizontalAlign="Right">
                       วัสดุที่ผลิตถัง ::                        
                </asp:TableCell>
                <asp:TableCell ID="TableCell81"  runat="server">
                    <asp:DropDownList ID="cboRTankMaterial" runat="server" CssClass="form-control required">
                     <asp:ListItem Value="" Text=" - เลือก - "></asp:ListItem>
                     <asp:ListItem Value="STEEL" Text="STEEL"></asp:ListItem>
                     <asp:ListItem Value="Aluminium" Text="Aluminium"></asp:ListItem>
                     </asp:DropDownList> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow19" runat="server">
                <asp:TableCell ID="TableCell45"  Width="18%"  runat="server" HorizontalAlign="Right">
                    วันที่จดทะเบียนครั้งแรก:: 
                    <asp:Label ID="Label25"  runat="server" Visible="false" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell48"  Width="30%"  runat="server">                
                  <asp:TextBox ID="semifirst_regis" runat="server" CssClass="datepicker form-control required " ClientIDMode="Static" MaxLength="99" ></asp:TextBox>

                  </asp:TableCell>                 
                 <asp:TableCell ID="TableCell44"  Width="2%"  runat="server">
                    &nbsp;
                </asp:TableCell>
                 <asp:TableCell ID="TableCell42"  Width="20%"  runat="server" HorizontalAlign="Right">
                    &nbsp;                    
                </asp:TableCell>
                <asp:TableCell ID="TableCell43"  Width="30%"  runat="server">
                                     
                </asp:TableCell>               
            </asp:TableRow>
            <asp:TableRow ID="TableRow21"   runat="server">
                <asp:TableCell ID="TableCell87"  runat="server" HorizontalAlign="Right">
                        ปั้ม Power ที่ตัวรถ::  
                    <asp:Label ID="Label42" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;&nbsp;*&nbsp;"></asp:Label>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    <asp:RadioButtonList ID="rblSemiPumpPower" runat="server" RepeatDirection="Horizontal" CssClass="required">
                        <asp:ListItem>&nbsp;&nbsp;มี</asp:ListItem>
                        <asp:ListItem>&nbsp;&nbsp;ไม่มี</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell24" runat="server">
                    &nbsp;
                </asp:TableCell>
              <asp:TableCell ID="TableCell82" runat="server">
                    &nbsp;
                </asp:TableCell>                   
            </asp:TableRow>
            <asp:TableRow ID="TableRow12" runat="server">
                <asp:TableCell  runat="server" HorizontalAlign="Right">
                        
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell runat="server">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell  runat="server" HorizontalAlign="Right">
                        
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell >
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow runat="server" ID="semiowncar">
                <asp:TableCell ID="TableCell41" runat="server" HorizontalAlign="Right" ColumnSpan="3">
                        สถานะรถภายในระบบ::  
                    <asp:Label ID="Label8" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell  ColumnSpan="2">
                    <asp:RadioButtonList ID="semitype_vendor" ClientIDMode="Static" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="1" Text="รถในสังกัด"></asp:ListItem>
                    <asp:ListItem Value="0" Text="ปลดรถในสังกัด"></asp:ListItem>
                    </asp:RadioButtonList>
                </asp:TableCell>                
            </asp:TableRow>                               
        </asp:Table>           
        <br />        
    </div>
</div>
    <%--ข้อมูลการอัพโหลดเอกสาร--%>
    <div id="document" class="panel panel-info" runat="server" visible="false">
        <div class="panel-heading">
            <i class="fa fa-table">เอกสารสำคัญของรถขนส่ง (ไม่ระบุวันหมดอายุ)</i>
        </div>
        <div class="panel-body">
            <div class="row form-group">
                <%--<div class="col-md-12 ">
                    <font color="black">คำแนะนำระเบียบการใช้งาน
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สามารถอัพโหลดไฟล์ .jpg,.jpeg,.jpe,.gif,.png,.doc,.docx,.xlsx,.xls,.txt,.pdf,.ppt,.pptx,.mm,.zip,.rar
                                    เท่านั้น และขนาดไม่เกิน 1 MB</font>
                </div>--%>
            </div>

            <div class="row form-group" align="center">
                <asp:GridView ID="dgvRequestFile" runat="server" Width="95%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูลเอกสารที่ Require ]">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="แนบเอกสาร">
                            <HeaderStyle Width="110px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />   
                            </ItemTemplate>   
                        </asp:TemplateField>
                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ" ItemStyle-Width="150px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB" ItemStyle-Width="160px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>

            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    
                </div>
                <div class="col-md-4">
                    <asp:Label ID="lblSuggest" runat="server" Text="ชื่อไฟล์สำหรับอัพโหลด"></asp:Label>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    <asp:DropDownList runat="server" ID="ddlUploadType" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUploadType_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-md-4">
                    <asp:TextBox ID="txtSuggestFileName" runat="server" CssClass="form-control" ReadOnly="true" BackColor="White"></asp:TextBox>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                </div>
                <div class="col-md-4  ">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    <asp:FileUpload ID="fileUpload" runat="server" />
                </div>
                <div class="col-md-4  ">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-left padding-top9">
                    <asp:Button Text="Upload" ID="btnUpload" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUpload_Click" />
                </div>
                <div class="col-md-4  ">
                   
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12  ">
                    <asp:GridView ID="dgvUploadFile" runat="server" CssClass="table table-hover" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                        HorizontalAlign="Center" AutoGenerateColumns="false"
                        CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH,FILENAME_USER" OnRowDeleting="dgvUploadFile_RowDeleting"
                        OnRowUpdating="dgvUploadFile_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFile_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:TemplateField HeaderText="No.">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" Visible="false" />
                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />
                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                        Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <%--ข้อมูลการอัพโหลดเอกสาร ธพ.น--%>
    <div id="documentTPN" class="panel panel-info" runat="server">
        <div class="panel-heading">
            <i class="fa fa-table">เอกสารสำคัญของรถขนส่ง (ระบุวันหมดอายุ)</i>
        </div>
        <div class="panel-body">
            <%--<div class="row form-group">
                <div class="col-md-12 ">
                    <font color="black">คำแนะนำระเบียบการใช้งาน
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;สามารถอัพโหลดไฟล์ .jpg,.jpeg,.jpe,.gif,.png,.doc,.docx,.xlsx,.xls,.txt,.pdf,.ppt,.pptx,.mm,.zip,.rar
                                    เท่านั้น และขนาดไม่เกิน 1 MB</font>
                </div>
            </div>--%>

            <div class="row form-group" align="center">
                <asp:Label ID="lblWarning" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <br />

                <asp:GridView ID="dgvRequestFileTPN" runat="server" Width="95%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูลเอกสารที่ Require ]">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="แนบเอกสาร">
                            <HeaderStyle Width="110px" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />   
                            </ItemTemplate>   
                        </asp:TemplateField>
                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ" ItemStyle-Width="150px">
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB" ItemStyle-Width="160px">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>

            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                </div>
                <div class="col-md-4  ">
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    
                </div>
                <div class="col-md-4">
                    <asp:Label ID="Label38" runat="server" Text="ชื่อไฟล์สำหรับอัพโหลด"></asp:Label>
                </div>
                <div class="col-md-4">
                    <asp:Label ID="lblPlaceMV" runat="server" Text="สถานที่วัดน้ำ"></asp:Label>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    <asp:DropDownList runat="server" ID="ddlUploadTypeTPN" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlUploadTypeTPN_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-md-4  ">
                    <asp:TextBox ID="txtSuggestFileNameTPN" runat="server" CssClass="form-control" ReadOnly="true" BackColor="White"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <asp:RadioButtonList ID="radPlaceMV" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="วัดน้ำภายใน ปตท.&nbsp;&nbsp;&nbsp;" Value="ปตท."></asp:ListItem>
                        <asp:ListItem Text="วัดน้ำภายนอก ปตท.&nbsp;&nbsp;&nbsp;" Value=""></asp:ListItem>
                        <asp:ListItem Text="ยังไม่ได้วัดน้ำ" Value="1"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    <asp:TextBox ID="txtDocUploadTPN" runat="server" placeholder="หมายเลขเอกสาร" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <asp:DropDownList ID="ddlYearTPN" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlYearTPN_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-md-4  ">
                    <table width="100%" style="align-content: center">
                        <tr>
                            <td style="width: 70px">
                                <asp:Label ID="lblDate" runat="server" Text="ตั้งแต่วันที่"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtdateStartTPN" runat="server" CssClass="datepicker" Style="text-align: center" Enabled="false"></asp:TextBox>
                            </td>
                            <td>-
                            </td>
                            <td>
                                <asp:TextBox ID="txtdateEndTPN" runat="server" CssClass="datepicker" Style="text-align: center" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-right padding-top9">
                    <asp:FileUpload ID="fileUploadTPN" runat="server" />
                </div>
                <div class="col-md-4  ">

                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4 text-left padding-top9">
                    <asp:Button Text="Upload" ID="btnUploadTPN" runat="server" CssClass="btn btn-md bth-hover btn-info" UseSubmitBehavior="false" OnClick="btnUploadTPN_Click" />
                </div>
                <div class="col-md-4  ">
                    
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12  ">
                    <asp:GridView ID="dgvUploadFileTPN" runat="server" CssClass="table table-hover" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                        HorizontalAlign="Center" AutoGenerateColumns="false"
                        CellPadding="4" GridLines="None" DataKeyNames="UPLOAD_ID,FULLPATH,FILENAME_USER" OnRowDeleting="dgvUploadFileTPN_RowDeleting"
                        OnRowUpdating="dgvUploadFileTPN_RowUpdating" ForeColor="#333333" OnRowDataBound="dgvUploadFileTPN_RowDataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:TemplateField HeaderText="No.">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="UPLOAD_ID" Visible="false" />
                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />
                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)" Visible="false" />
                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)" />

                            <asp:BoundField DataField="YEAR" HeaderText="ปี" />
                            <asp:BoundField DataField="DOC_NUMBER" HeaderText="หมายเลขเอกสาร" />
                            <asp:BoundField DataField="START_DATE" HeaderText="วันเริ่มต้น" />
                            <asp:BoundField DataField="STOP_DATE" HeaderText="วันหมดอายุ" />
                            

                            <asp:BoundField DataField="FULLPATH" Visible="false" />
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                        Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                            <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>

                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>             
    
    <%--เอกสาร มว. --%>
    <div id="Div1" class="panel panel-info" runat="server" visible="true">
        <div class="panel-heading">
            <i class="fa fa-table">เอกสาร มว.</i>
        </div>
        <div class="panel-body">
            <div class="row form-group" align="center">
                <asp:GridView ID="dgvMV" runat="server" CssClass="table table-hover" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-CssClass="GridColorHeader"
                    HorizontalAlign="Center" AutoGenerateColumns="false" Width="95%"
                    CellPadding="4" GridLines="None" DataKeyNames="FULLPATH,FILENAME_USER"
                    OnRowUpdating="dgvMV_RowUpdating" ForeColor="#333333">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร" />

                        <asp:BoundField DataField="DOC_NUMBER" HeaderText="หมายเลขเอกสาร" />
                        <asp:BoundField DataField="START_DATE" HeaderText="วันเริ่มต้น" />
                        <asp:BoundField DataField="STOP_DATE" HeaderText="วันหมดอายุ" />

                        <asp:BoundField DataField="FULLPATH" Visible="false" />
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                    Height="25px" Style="cursor: pointer" CommandName="Update" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                </asp:GridView>
            </div>
        </div>
    </div>
            
 <%--ข้อมูลการอัพโหลดเอกสาร--%>  
 <div id="insurance" class="panel panel-info" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลประกันภัย</i>
    </div>
    <div class="panel-body">
        <asp:Table ID="Table4" runat="server" Width="100%">
            <asp:TableRow ID="TableRow10" runat="server">
                <asp:TableCell ID="TableCell36"  Width="20%"  runat="server" HorizontalAlign="Right">
                    บริษัทประกันภัย:
                    <asp:Label ID="Label18" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell37"   runat="server" ColumnSpan="4">
                    <asp:TextBox ID="company_name" runat="server" CssClass="form-control required" MaxLength="25" ></asp:TextBox>
                </asp:TableCell>                
               
            </asp:TableRow>
            <asp:TableRow ID="TableRow11" runat="server">
                <asp:TableCell ID="TableCell38" runat="server" HorizontalAlign="Right">
                    ระยะเวลาอายุ พรบ. :
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell39" runat="server" Width="30%">
                    <asp:TextBox ID="start_duration" ClientIDMode="Static" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell ID="TableCell40" runat="server" HorizontalAlign="Center">
                    ถึงวันที่ : &nbsp;
                </asp:TableCell>                    
                <asp:TableCell ID="TableCell46" runat="server" Width="30%">
                    <asp:TextBox ID="end_duration" ClientIDMode="Static"  runat="server" CssClass="form-control datepicker"></asp:TextBox>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">
                    ทุนประกัน. :
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell Width="30%">
                    <asp:TextBox ID="budget" runat="server" CssClass="form-control number" MaxLength="7"></asp:TextBox>
                    
                </asp:TableCell> 
                <asp:TableCell HorizontalAlign="Left" Font-Size="Medium">
                    &nbsp;&nbsp;บาท
                </asp:TableCell>               
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Right">
                    เบี้ยประกันต่อปี. :
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell Width="30%">
                    <asp:TextBox ID="ninsure" runat="server" CssClass="form-control  number" MaxLength="7"></asp:TextBox>
                     
                </asp:TableCell> 
                <asp:TableCell HorizontalAlign="Left" Font-Size="Medium">
                    &nbsp;&nbsp;บาท
                </asp:TableCell>               
            </asp:TableRow>
            <asp:TableRow ID="TableRow26" runat="server">
                <asp:TableCell ID="TableCell47"  Width="20%"  runat="server" HorizontalAlign="Right">
                    ประเภทประกันภัย.:
                    
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell105"  Width="30%"  runat="server" >
                    <asp:DropDownList ID="ddlTypeInsurance" runat="server" CssClass="form-control" >
                        <asp:ListItem Value="" Text="--กรุณาเลือก--"></asp:ListItem>
                        <asp:ListItem Value="1" Text="ประกันภัยชั้น1"></asp:ListItem>
                        <asp:ListItem Value="2" Text="ประกันภัยชั้น3"></asp:ListItem>
                    </asp:DropDownList>                    
                </asp:TableCell>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow27" runat="server">
                <asp:TableCell ID="TableCell106"  Width="20%"  runat="server" HorizontalAlign="Right">
                    ความคุ้มครอง.:
                    
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell107"  Width="30%"  runat="server" ColumnSpan="3">
                    <asp:CheckBoxList ID="cbHolding" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="ตามชนิดรถ"></asp:ListItem>
                        <asp:ListItem Value="2" Text="บุคคลที่3"></asp:ListItem>
                        <asp:ListItem Value="3" Text="สินค้า"></asp:ListItem>
                    </asp:CheckBoxList>                    
                </asp:TableCell>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow>              
            <asp:TableRow ID="TableRow28" runat="server">
                <asp:TableCell ID="TableCell108"  Width="20%"  runat="server" HorizontalAlign="Right">
                    รายละเอียดเพิ่มเติม.:
                    
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell109"  Width="30%"  runat="server" ColumnSpan="3">
                    <asp:TextBox ID="detailinsurance" runat="server" Text="" CssClass="form-control" TextMode="MultiLine" MaxLength="200" PlaceHolder="รายละเอียดข้อมูลประกัน"></asp:TextBox>
                    
                </asp:TableCell>
                <asp:TableCell>
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow>
                      
        </asp:Table>
      </div>
</div>
    <%-- Insurance --%>
<div class="panel panel-info" id="carrier" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลผู้ขนส่ง</i>
    </div>
    <div class="panel-body">
     <asp:Table ID="Table2" runat="server" Width="958px" Height="76px">
     <asp:TableRow ID="TableRow16" runat="server">
                <asp:TableCell ID="TableCell68"  Width="20%"  runat="server" HorizontalAlign="Right">
                    ผู้ขนส่ง:
                    <asp:Label ID="Label24" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;*"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell69"  Width="30%"  runat="server" ColumnSpan="2">
                    <asp:Label ID="NameVendor" runat="server" Visible="true"  Text="&nbsp;*"></asp:Label>
                    <asp:DropDownList ID="STRANSPORTID" runat="server" CssClass="form-control required">
                    </asp:DropDownList>
                </asp:TableCell>                 
               <asp:TableCell ID="TableCell85" Width="18%"  runat="server" HorizontalAlign="Right">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell86" Width="30%"  runat="server">
                &nbsp;                    
                </asp:TableCell>                
            </asp:TableRow>                    
    </asp:Table>
    </div>
</div>
    <%--ข้อมูลผู้ขนส่ง --%>
<div id="capacity" class="panel panel-info" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลช่องเติม</i>
    </div>
    <div class="panel-body">
        <asp:Table ID="Table6" runat="server" Width="958px" Height="76px">
            <asp:TableRow ID="TableRow22"   runat="server">
                <asp:TableCell ID="TableCell63"  runat="server" Width="20%" HorizontalAlign="Right">
                    วิธีการเติมน้ำมัน::  
                    <asp:Label ID="Label1" runat="server" Visible="true" ForeColor="Red" Text="*&nbsp;"></asp:Label>
                    &nbsp;&nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell64" Width="30%"  runat="server">
                    <asp:DropDownList ID="cboHLoadMethod" runat="server" class="form-control required" Width="100%">
                        <asp:ListItem Value=""> - เลือก - </asp:ListItem>
                        <asp:ListItem Value="Top Loading">Top Loading</asp:ListItem>
                        <asp:ListItem Value="Bottom Loading"> Bottom Loading</asp:ListItem>                        
                    </asp:DropDownList> 
                </asp:TableCell>
                <asp:TableCell ID="TableCell88" Width="2%" runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell89"  Width="18%" runat="server" HorizontalAlign="Right">
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell ID="TableCell90" Width="30%"  runat="server">
                    &nbsp;
                </asp:TableCell>
            </asp:TableRow>            
            <asp:TableRow ID="TableRow25"   runat="server">
                <asp:TableCell ID="TableCell97"  runat="server" HorizontalAlign="Right">
                        จำนวนช่องเติม ::  
                    <asp:Label ID="Label46" runat="server" Visible="true" ForeColor="Red" Text="&nbsp;&nbsp;*&nbsp;"></asp:Label>
                    &nbsp;
                </asp:TableCell>
                <asp:TableCell >
                        <asp:DropDownList ID="ddl_addition" runat="server" CssClass="form-control">
                        
                        </asp:DropDownList>
                </asp:TableCell>
                <asp:TableCell ID="TableCell98" runat="server">
                    &nbsp;
                </asp:TableCell>
               <asp:TableCell ID="TableCell99"  runat="server" HorizontalAlign="Right">
                       จำนวนแป้นเติม ::
                </asp:TableCell>
                <asp:TableCell ID="TableCell100"  runat="server">
                <asp:DropDownList ID="capacityCell" runat="server" class="form-control" DataTextField="STERMINALNAME"
                        DataValueField="STERMINALID" Width="100%">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow ID="TableRow15"   runat="server" >
                <asp:TableCell ID="TableCell65"  runat="server" HorizontalAlign="Center" ColumnSpan="5">
                        &nbsp;
                </asp:TableCell>                
            </asp:TableRow>
            <asp:TableRow ID="TableRow23"   runat="server" >
                <asp:TableCell ID="TableCell51"  runat="server" HorizontalAlign="Center" ColumnSpan="5">                
                        <asp:Table ID="table_capacity" runat="server" Width="100%" BackColor="White" BorderColor="Black" BorderWidth="1" ForeColor="Black" GridLines="Both" BorderStyle="Solid">
                        </asp:Table>
                        
                </asp:TableCell>
                
            </asp:TableRow>   
            
      </asp:Table> 
    </div>
</div>
<br />
<br />
<%--ข้อมูลการวัดน้ำ --%>
<div id ="scalewater" runat="server" class="panel panel-info" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ข้อมูลวัดน้ำ</i>
    </div>
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                 สถานที่วัดน้ำ &nbsp;&nbsp;
                </div>
                <div class="col-md-9">
                    <asp:RadioButtonList ID="placewater" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="INTERNAL" Text="วัดน้ำภายในปตท."></asp:ListItem>
                        <asp:ListItem Value="EXTERNAL" Text="วัดน้ำภายนอกปตท."></asp:ListItem>
                    </asp:RadioButtonList> 
                </div>
                <div class="col-md-3">
                 รหัสวัดน้ำ &nbsp;&nbsp;
                </div>
                <div class="col-md-9">
                    <asp:TextBox ID="txtcodewater" runat="server" CssClass="form-control"></asp:TextBox>     
                </div>
                <div class="col-md-12">
                &nbsp;
                </div>
                <div class="col-md-3">
                 วันที่วัดน้ำครั้งก่อนหน้า &nbsp;&nbsp;
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="scale_start" runat="server" CssClass="datepicker form-control"></asp:TextBox>     
                </div>
                <div class="col-md-3">
                วันที่กำหนดวัดน้ำครั้งต่อไป 
                </div>
                <div class="col-md-3">
                    <asp:TextBox ID="scale_end" runat="server" CssClass="form-control datepicker"></asp:TextBox>
                </div>
            </div>            
        </div>       
    </div>
</div>
<div id="divcomment" class="panel panel-info" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">ส่งเรื่องกลับ ขอข้อมูลเพิ่มเติม </i>
    </div>
    <div class="panel-body">
        <asp:TextBox ID="txtcomment" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
    </div>
</div>
<%--<div id="ConfrimStatus" class="panel panel-info" runat="server" visible="false">
    <div class="panel-heading">
        <i class="fa fa-table">การใช้งานรถ <asp:CheckBox ID="chcekapprove" runat="server" ClientIDMode="Static" Text="ยกเลิกรถชั่วคราว"/></i>
    </div>
    <div class="panel-body">
        <div class="body-approve" style="display:none">
        <table border="0" cellspacing="2" cellpadding="3" width="100%">
            <tr>
                <td align="right" width="25%">
                    การอนุญาตรับงาน <font color="#FF0000">*</font>: </td>
                <td align="left" width="75%" colspan="3">
                    <asp:DropDownList ID="active_blacklist" runat="server" CssClass="form-control" Width="20%">
                    <asp:ListItem Value="N" Text="ระงับ" />
                    <asp:ListItem Value="D" Text="ยกเลิก" />
                    </asp:DropDownList>                    
                </td>
            </tr>        
            <tr id="permission_approve" runat="server" visible="false">
                    <td align="right" width="20%">
                        วันที่เริ่มต้นระงับ <font color="#FF0000">*</font>: 
                    </td>
                    <td align="left" width="25%">
                        
                        <asp:TextBox ID="date_start" ClientIDMode="Static" runat="server" CssClass="form-control datepicker" ></asp:TextBox> 
                                              
                    </td>
                    <td align="right" width="20%">
                        วันที่สิ้นสุดระงับ <font color="#FF0000">*</font>: 
                    </td>
                    <td align="left" width="25%">
                        
                        <asp:TextBox ID="date_end" ClientIDMode="Static" runat="server" CssClass="form-control datepicker" ></asp:TextBox> 
                                      
                    </td>
                </tr>            
                <tr>
                    <td align="right" width="25%">
                        สาเหตุการระงับ <font color="#FF0000">*</font>: 
                    </td>
                    <td align="left" width="35%" colspan="3">
                        <asp:TextBox ID="detaill_cencal" ClientIDMode="Static" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>                                        
                    </td>
                </tr>            
        </table>
    </div>
        <br />
        <asp:GridView ID="Data_cencal" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="No.">
                    <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:BoundField DataField="TYPE" HeaderText="ประเภท"  ItemStyle-Width="20%" >
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="REMARK" HeaderText="หมายเหตุ"  ItemStyle-Width="20%" >
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DBLACKLIST_START" HeaderText="วันที่ - เวลา(ถูกระงับ)" ItemStyle-Width="20%"  >
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DBLACKLIST_END" HeaderText="วันที่ - เวลา(ปลดระงับ)" ItemStyle-Width="20%" >
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>                    
                </Columns>
                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
            </asp:GridView>
    </div>
</div>--%>
	<asp:HiddenField ID="cActive" runat="server" />
<div id="SaveButton" runat="server" style="text-align:center">
<asp:Button ID="cmdCancelDoc" runat="server" Enabled="true" Text="ยกเลิก" CssClass="btn btn-md btn-hover btn-danger" Style="width: 120px;" OnClick="ResponseMenu" />
 </div>    
 </asp:Content>
