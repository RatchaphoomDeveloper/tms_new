﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Application.Helper;
using TMS_BLL.UserGroupSelectBLL;

public partial class UserGroupAddEdit1 : PageBase
{
    #region + View State +
    private string ActionType
    {
        get
        {
            if ((string)ViewState["ActionType"] != null)
                return (string)ViewState["ActionType"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionType"] = value;
        }
    }

    private string ActionKey
    {
        get
        {
            if ((string)ViewState["ActionKey"] != null)
                return (string)ViewState["ActionKey"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["ActionKey"] = value;
        }
    }

    private string TableSelect
    {
        get
        {
            if ((string)ViewState["TableSelect"] != null)
                return (string)ViewState["TableSelect"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["TableSelect"] = value;
        }
    }

    private string FieldSelect
    {
        get
        {
            if ((string)ViewState["FieldSelect"] != null)
                return (string)ViewState["FieldSelect"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["FieldSelect"] = value;
        }
    }

    private int UserID
    {
        get
        {
            if ((int)ViewState["UserID"] != null)
                return (int)ViewState["UserID"];
            else
                return 0;
        }
        set
        {
            ViewState["UserID"] = value;
        }
    }

    private int SystemID
    {
        get
        {
            if ((int)ViewState["SystemID"] != null)
                return (int)ViewState["SystemID"];
            else
                return 0;
        }
        set
        {
            ViewState["SystemID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
                this.InitialForm();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            //Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster");

            DataTable dtUserLogin = (DataTable)Session["UserLogin"];

            UserID = int.Parse(dtUserLogin.Rows[0]["SUID"].ToString());
            UserID = int.Parse(Session["UserID"].ToString());
            //SystemID = int.Parse(dtUserLogin.Rows[0]["SYSTEM_ID"].ToString());

            InitialStatus(radIsAdmin, " AND STATUS_TYPE = 'SPECIAL_STATUS'");

            this.CheckQueryString();
            this.InitialStatus();
            this.GetData();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialStatus()
    {
        try
        {
            DropDownListHelper.BindDropDownList(ref ddlStatus, ConfigValueHelper.GetDefaultStatus(), "StatusID", "StatusName", true);
            //dtDomain = DomainBLL.Instance.DomainSelectBLL(" AND M_DOMAIN.IS_ACTIVE = 1");
            //DropDownListHelper.BindDropDownList(ref cboDomain, dtDomain, "ID", "DOMAIN_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetData()
    {
        try
        {
            if (string.Equals(ActionType, "add"))
            {
                txtUserGroupName.Enabled = true;
            }
            else
            {
                txtUserGroupName.Enabled = false;

                DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(this.GetCondition());
                if (dtUserGroup.Rows.Count > 0)
                {
                    txtUserGroupName.Text = dtUserGroup.Rows[0]["USERGROUP_NAME"].ToString();

                    ddlStatus.SelectedValue = dtUserGroup.Rows[0]["IS_ACTIVE"].ToString();

                    radIsAdmin.SelectedValue = dtUserGroup.Rows[0]["IS_ADMIN"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void CheckQueryString()
    {
        try
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                ActionType = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["type"], MachineKeyProtection.All));
                ActionKey = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["id"], MachineKeyProtection.All));

                if (string.Equals(ActionType, "view"))
                {
                    DisableControl.DisableControls(Page);
                    cmdSave.Visible = false;
                    cmdCancel.Visible = false;
                }
            }
            else
                Response.Redirect("../Other/NotAuthorize.aspx");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetCondition()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND USERGROUP_ID = '" + ActionKey + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionUserName()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND USERGROUP_NAME = '" + txtUserGroupName.Text.Trim() + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSave();
            string docPrefix = "";
            string getIsAdmin = radIsAdmin.SelectedValue;
            if (getIsAdmin == "0")
            {
                docPrefix = "00"; //ผู้ประกอบการขนส่ง
            }
            else if (getIsAdmin == "1" || getIsAdmin == "3" || getIsAdmin == "4" || getIsAdmin == "5")
            {
                docPrefix = "01"; //เจ้าหน้าที่ ปตท. >> ผู้ดูแลระบบ, ผจ.รข.ขปน., BSA >> เห็นเท่า พนักงาน ปตท
            }
            else if (getIsAdmin  == "2")
            {
                docPrefix = "02"; //พนักงานคลัง
            }
            else
            {
                docPrefix = "00"; // ลุูกค้า
            }

            UserGroupBLL.Instance.UserGroupAddBLL(int.Parse(ActionKey), txtUserGroupName.Text.Trim(), radIsAdmin.SelectedValue, ddlStatus.SelectedValue, UserID, TableSelect, FieldSelect, docPrefix);

            alertSuccess(AuthenResources.SaveSuccess, "UserGroup1.aspx");
        }

        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserGroup1.aspx");
    }

    private void ValidateSave()
    {
        try
        {
            if (string.Equals(txtUserGroupName.Text.Trim(), string.Empty))
                throw new Exception(string.Format(AuthenResources.TextBoxRequire, "กลุ่มผู้ใช้งาน"));

            if (ddlStatus.SelectedIndex == 0)
                throw new Exception(string.Format(AuthenResources.DropDownRequire, "สถานะ"));

            if (radIsAdmin.SelectedValue == "")
                throw new Exception(string.Format(AuthenResources.DropDownRequire, "สถานะพิเศษ"));

            if ((ActionType != "edit") && checkIsDupplicateName(txtUserGroupName.Text))
            {
                throw new Exception(string.Format("ชื่อกลุ่มผู้ใช้งานนี้ มีซ้ำกับในระบบแล้ว กรุณาตั้งชื่อใหม่", "กลุ่มผู้ใช้งาน"));
            }

            if (string.Equals(radIsAdmin.SelectedValue, "0"))
            {
                TableSelect = "TVENDOR";
                FieldSelect = "SABBREVIATION ";
            }
            else if (string.Equals(radIsAdmin.SelectedValue, "1"))
            {
                TableSelect = "TUNIT";
                FieldSelect = "UNITNAME";
            }
            else if (string.Equals(radIsAdmin.SelectedValue, "2"))
            {
                TableSelect = "TTERMINAL";
                FieldSelect = "SABBREVIATION";
            }
            else if (string.Equals(radIsAdmin.SelectedValue, "3"))
            {
                TableSelect = "TUNIT";
                FieldSelect = "UNITNAME";
            }
            else if (string.Equals(radIsAdmin.SelectedValue, "4"))
            {
                TableSelect = "TUNIT";
                FieldSelect = "UNITNAME";
            }
            else if (string.Equals(radIsAdmin.SelectedValue, "5"))
            {
                TableSelect = "TUNIT";
                FieldSelect = "UNITNAME";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private Boolean checkIsDupplicateName(string groupName)
    {
        DataTable dtUserGroupName = UserGroupBLL.Instance.getUserGroupNameBLL(this.GetConditionUserName());
        if (dtUserGroupName.Rows.Count > 0)
        {
            return true; //dupplicate name
        }
        else
        {
            return false;
        }
                
    }
}