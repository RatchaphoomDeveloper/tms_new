﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;
using TMS_BLL.Transaction.SurpriseCheck;

public partial class SurpriseCheckContract : PageBase
{
    DataTable dt;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                ddlVendor.Enabled = false;
                ddlVendor_SelectedIndexChanged(null, null);
                //mode = "view";
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                btnExport.CssClass = "btn btn-md bth-hover btn-info";
            }
            if (!CanWrite)
            {
                aAdd.Disabled = true;
                aAdd.HRef = "javascript:void(0);";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region DrowDownList
    private void SetDrowDownList()
    {

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });

        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        ddlStatus.DataTextField = "STATUS_NAME";
        ddlStatus.DataValueField = "STATUS_VALUE";
        ddlStatus.DataSource = SurpriseCheckContractBLL.Instance.StatusSelectByType("SURPRISE_CHECK_CONTRACT");
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
    }

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            gvSurpriseCheckContract.DataSource = dt;
            gvSurpriseCheckContract.DataBind();
            lblItem.Text = dt.Rows.Count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetData
    private void GetData()
    {
        dt = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelect(txtSearch.Text.Trim()
            , ddlVendor.SelectedIndex > 0 ? ddlVendor.SelectedValue : ""
            , ddlContract.SelectedIndex > 0 ? ddlContract.SelectedValue : ""
            , txtDate.Text
            , txtDateTo.Text
            , ddlStatus.SelectedIndex > 0 ? ddlStatus.SelectedValue : "");
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = string.Empty;
        txtDateTo.Text = string.Empty;
        //rblType.ClearSelection();
        if (Session["CGROUP"] + string.Empty != "0")
        {
            ddlVendor.SelectedIndex = 0;
        }

        ddlVendor_SelectedIndexChanged(null, null);
        txtSearch.Text = string.Empty;
        ddlStatus.SelectedIndex = 0;
        gvSurpriseCheckContract.DataSource = null;
        gvSurpriseCheckContract.DataBind();
        lblItem.Text = "0";
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            dt.Columns.Remove("SYS_TIME");
            dt.Columns.Remove("REPORT_PTT");
            dt.Columns.Remove("REPORTER");
            dt.Columns.Remove("VENDOR_REPORT_TIME");
            dt.Columns.Remove("SEND_CONSIDER");
            dt.Columns.Remove("PTT_APPROVE");
            dt.Columns.Remove("INFORMER_NAME");
            dt.Columns.Remove("CACTIVE");
            dt.Columns.Remove("CREATE_DATE");
            dt.Columns.Remove("CREATE_BY");
            dt.Columns.Remove("UPDATE_DATE");
            dt.Columns.Remove("UPDATE_BY");
            dt.Columns.Remove("ACCIDENTTYPE_ID");
            dt.Columns.Remove("SHIPMENT_NO");
            dt.Columns.Remove("STRUCKID");
            dt.Columns.Remove("SVENDORID");
            dt.Columns.Remove("GROUPID");
            dt.Columns.Remove("SCONTRACTID");
            dt.Columns.Remove("SEMPLOYEEID");
            dt.Columns.Remove("SOURCE");
            dt.Columns.Remove("LOCATIONS");
            dt.Columns.Remove("GPSL");
            dt.Columns.Remove("GPSR");
            dt.Columns.Remove("PRODUCT_ID");
            dt.Columns.Remove("WORKGROUPNAME");
            dt.Columns.Remove("SERIOUS");
            dt.Columns.Remove("EVALUATION");
            //dt.Columns.Remove("TERMINALNAME");
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "Accident_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion

    #region gvSurpriseCheckContract_RowUpdating
    protected void gvSurpriseCheckContract_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strData = gvSurpriseCheckContract.DataKeys[e.RowIndex]["ID"] + string.Empty, page = string.Empty;
        byte[] plaintextBytes = Encoding.UTF8.GetBytes(strData);
        string encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        if (gvSurpriseCheckContract.DataKeys[e.RowIndex]["IS_ACTIVE"] + string.Empty == "1")
            page = "SurpriseCheckContractAddEdit";
        else
            page = "SurpriseCheckContractAddEditTab2";
        Page.ClientScript.RegisterStartupScript(
   this.GetType(), "OpenWindow", "window.open('" + page + ".aspx?str=" + encryptedValue + "','_blank');", true);
    }
    #endregion

    #region gvSurpriseCheckContract_PageIndexChanging
    protected void gvSurpriseCheckContract_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSurpriseCheckContract.PageIndex = e.NewPageIndex;
        btnSearch_Click(null, null);
    }
    #endregion
}