﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="SPC_Calendar.aspx.cs" Inherits="SPC_Calendar" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">

    
    <script type="text/javascript" src="plugin/fullcalendar/fullcalendar.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            InitializeCalendar();
        });
        function InitializeCalendar() {
            //-- start date and end date criteria.. you can get it from user input.. 
            var startDate = "2018-06-01";
            var endDate = "2050-12-31";
            //-- start date and end date criteria.. you can get it from user input.. 

            //-- ajax call to fetch calendar data from database
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: "{ 'StartDate': '" + startDate + "', 'EndDate': '" + endDate + "' }",
                url: "SPC_Calendar.aspx/GetCalendarData",
                dataType: "json",
                success: function (data) {
                    $('#calendar').empty();
                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        weekNumbers: true,
                        height: 800,
                        allDayText: 'Events',
                        selectable: true,
                        overflow: 'auto',
                        editable: true,
                        eventLimit: true,
                        firstDay: 1,
                        events: $.map(data.d, function (item, i) {
                            //-- here is the event details to show in calendar view.. the data is retrieved via ajax call will be accessed here
                            var eventStartDate = new Date(parseInt(item.EventStartDate.substr(6)));
                            var eventEndDate = new Date(parseInt(item.EventEndDate.substr(6)));

                            var event = new Object();
                            event.id = item.EventId;
                            event.start = new Date(eventStartDate.getFullYear(), eventStartDate.getMonth(), eventStartDate.getDate(), eventStartDate.getHours(), 0, 0, 0);
                            event.end = new Date(eventEndDate.getFullYear(), eventEndDate.getMonth(), eventEndDate.getDate(), eventEndDate.getHours() + 1, 0, 0, 0);
                            event.title = item.Title;
                            event.allDay = item.AllDayEvent;
                            return event;
                        })
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //-- log error
                }
            });
        }
    </script>

    <div style="font-family: 'Century Gothic'">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id='calendar'></div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
