﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="OrderPlan.aspx.cs" Inherits="OrderPlan" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.2" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2" Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxRoundPanel" Assembly="DevExpress.Web.v11.2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript" language="javascript">
        function Set_txtnTruckConfirm(s, e) {
            //alert(e);
            var text = s.GetText();
            if (s.GetText() != "") {
                s.SetText(text);
            } else {
                s.SetText(e);
            }
        }

        // <![CDATA[
        var uploadCompleteFlag;
        function FilesUploadComplete(s, e) {
            if (uclOrder.GetText() != "") {
                dxError('แจ้งเตือน', 'กรุณาปิดไฟล์ที่เปิดขณะอัพโหลดแผน');
            }
            uploadCompleteFlag = true;
            if (e.errorText != "") {
                alert('error');
            }
            else {

            }
        }
        function ShowMessage(message) {

            alert(message);
            //window.setTimeout("alert('" + message + "')", 0);
        }
        function FileUploadStart(s, e) {

            uploadCompleteFlag = false;
        }

        function ShowPopupProgressingPanel() {

            if (!uploadCompleteFlag) {
                pbProgressing1.SetPosition(0);

            }
            else {

            }
        }
        function UploadingProgressChanged(s, e) {

            pbProgressing1.SetPosition(e.progress);
            var info = e.currentFileName + "&emsp;[" + GetKBytes(e.uploadedContentLength) + " / " + GetKBytes(e.totalContentLength) + "] KBytes";
        }
        function GetKBytes(bytes) {

            return Math.floor(bytes / 1024);
        }
        // ]]> 

        function OpenPopup() {

            //            var x = screen.width / 2 - 400 / 2;
            //            var y = screen.height / 2 - 210 / 2;
            //            window.open('book-car-progress.aspx?v=' + rblImport.GetValue(), '', "width=400,height=230,scrollbars=1,left=" + x + ",top=" + y);

            window.location = 'OrderPlan-progress.aspx?v=' + rblChoice.GetValue();
        };

        function OpenPage(sUrl) {

            window.open(sUrl);
        }
        function ddlOnChange(value, index, Type,me) {
            if (value) {
                $(me).parents('tbody').find('input[type=checkbox]').prop("checked", true);
            }
            else {
                $(me).parents('tbody').find('input[type=checkbox]').prop("checked", false);
            }
            //xcpn.PerformCallback('DDLONCHANGE;' + index + ';' + value);
            //gvw.PerformCallback('DDLONCHANGE;' + index + ';' + value);

        }
    </script>
    <style type="text/css">
        div .sco {
            height: 250px;
            width: 250px;
            overflow: auto;
        }

        .Poop .dxcpLoadingPanelWithContent_Aqua {
            left: 360px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--  <dx:ASPxTextBox runat="server" ID="txtPahtExcel" ClientInstanceName="txtPahtExcel">
    </dx:ASPxTextBox>
    <dx:ASPxTextBox runat="server" ID="txtPahtPDF" ClientInstanceName="txtPahtPDF">
    </dx:ASPxTextBox>--%>
    <dx:aspxcallbackpanel id="xcpn" runat="server" hidecontentoncallback="False" oncallback="xcpn_Callback"
        clientinstancename="xcpn" causesvalidation="False">
        <ClientSideEvents EndCallback="function(s,e){inEndRequestHandler();setdatepicker();eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); inEndRequestHandler(); s.cpRedirectOpen = undefined; }"></ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">
                    <%--<ClientSideEvents ActiveTabChanged="function (s, e) {gvw.CancelEdit();gvwT3.CancelEdit();gvwT4.CancelEdit();}" />--%>
                    <ClientSideEvents />
                    <TabPages>
                        <dx:TabPage Name="p1" Text="รถที่ยืนยันตามสัญญา">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                    <asp:Table runat="server" Width="100%">
                                        <asp:TableRow>


                                            <asp:TableCell Width="15%">
                                                <asp:Label ID="lblDateConfirm" runat="server" Text="วันที่ยืนยันรถ"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="30%">
                                                <asp:TextBox ID="txtDateConfirm" runat="server" CssClass="datepicker"></asp:TextBox>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="lblWarehouse" runat="server" Text="คลังต้นทางประสงค์เข้ารับงาน"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <dx:ASPxComboBox ID="ddlWarehouse" ClientInstanceName="ddlWarehouse" runat="server" Width="100%"
                                                    CssClass="form-control" TextFormatString="{1}" ValueField="STERMINALID" IncrementalFilteringMode="Contains">
                                                    <Columns>
                                                        <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                                        <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="SABBREVIATION" />
                                                    </Columns>
                                                </dx:ASPxComboBox>

                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Width="15%">
                                                <asp:Label ID="lblCompany" runat="server" Text="บริษัทผู้ขนส่ง"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell Width="30%">
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                            <asp:TableCell Width="10%">&nbsp;</asp:TableCell>
                                            <asp:TableCell Width="15%">
                                                <asp:Label ID="lblContract" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </asp:TableCell>

                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell>ทะเบียนรถ(หัว/หาง)</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox runat="server" ID="txtSheadregisterno" CssClass="form-control" />
                                            </asp:TableCell><asp:TableCell>&nbsp;</asp:TableCell><asp:TableCell>
                                                <dx:ASPxCheckBox runat="server" ID="cbCSTANBY" Text="รถหมุนเวียน" CssClass="dxeLineBreakFix">
                                                </dx:ASPxCheckBox>
                                            </asp:TableCell><asp:TableCell HorizontalAlign="Right">
                                                <dx:ASPxButton runat="server" ID="btnSeachT1" Visible="true" AutoPostBack="false" SkinID="_search" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Search'); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton runat="server" ID="btnClaer" Visible="true" AutoPostBack="false" Text="Claer" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('Claer'); }" />
                                                </dx:ASPxButton>
                                                <asp:Button ID="cmdSearchTab1" runat="server" Visible="false" Text="ค้นหา" Width="100px" class="btn btn-md btn-hover btn-info" UseSubmitBehavior="false" OnClick="cmdSearchTab1_Click" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="10%" align="right">
                                                            <%--ค้นหา :--%>
                                                        </td>
                                                        <td width="30%" align="right">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch" NullText="บริษัทขนส่ง,เลขที่สัญญา,ทะเบียนรถ" Visible="false">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            <%--ชื่อคลัง :--%>
                                                        </td>
                                                        <td width="15%">
                                                            <dx:ASPxComboBox Visible="false" runat="server" CallbackPageSize="30" EnableCallbackMode="True" ValueField="STERMINALID"
                                                                TextFormatString="{1}" Width="250px" Height="25px" ClientInstanceName="cboTeminal"
                                                                TextField="SABBREVIATION" CssClass="dxeLineBreakFix" SkinID="xcbbATC" ID="cboTeminal"
                                                                OnItemRequestedByValue="cboTeminal_OnItemRequestedByValueSQL" OnItemsRequestedByFilterCondition="cboTeminal_ItemsRequestedByFilterConditionSQL">
                                                                <Columns>
                                                                    <dx:ListBoxColumn FieldName="STERMINALID" Width="30%" Caption="#"></dx:ListBoxColumn>
                                                                    <dx:ListBoxColumn FieldName="SABBREVIATION" Width="70%" Caption="คลัง"></dx:ListBoxColumn>
                                                                </Columns>
                                                                <ItemStyle Wrap="True"></ItemStyle>
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource runat="server" ID="sdsTeminal" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                                                        </td>
                                                        <td width="15%" align="right">
                                                            <%--วันที่ยืนยันรถ:--%>
                                                        </td>
                                                        <td width="10%" align="right">
                                                            <dx:ASPxDateEdit runat="server" ID="edtCFT" SkinID="xdte" Visible="false">
                                                            </dx:ASPxDateEdit>
                                                        </td>
                                                        <td width="10%"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 70%" align="left">
                                                            <dx:ASPxHyperLink ID="lkbconfirmtruck" ClientInstanceName="lkbconfirmtruck" runat="server"
                                                                Text="ดูรายการยืนยันรถตามสัญญาทั้งหมด">
                                                                <ClientSideEvents Click="function(s,e){window.location='ConfirmTruck.aspx?str=';}" />
                                                            </dx:ASPxHyperLink>
                                                        </td>
                                                        <td style="width: 30%; white-space: nowrap;" align="right">
                                                            <dx:ASPxLabel runat="server" ID="ASPxLabel3" Text="จำนวนรถหมุนเวียน" CssClass="dxeLineBreakFix"
                                                                ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            <dx:ASPxLabel runat="server" ID="lblCountCStanby" CssClass="dxeLineBreakFix" ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            <dx:ASPxLabel runat="server" Text="คัน" ID="lblCountCStanby2" CssClass="dxeLineBreakFix"
                                                                ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <dx:ASPxLabel runat="server" ID="ASPxLabel1" Text="จำนวนรถที่ยืนยัน" CssClass="dxeLineBreakFix"
                                                                ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            <dx:ASPxLabel runat="server" ID="lblDetailTeminal" CssClass="dxeLineBreakFix" ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                            <dx:ASPxLabel runat="server" Text="คัน" ID="ASPxLabel2" CssClass="dxeLineBreakFix"
                                                                ForeColor="Red">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <dx:ASPxGridView ID="xgvw" ClientInstanceName="xgvw" runat="server" SkinID="_gvw"
                                                    AutoGenerateColumns="False" KeyFieldName="SCONTRACTNO"
                                                    Width="99%" OnAfterPerformCallback="xgvw_AfterPerformCallback">
                                                    <ClientSideEvents RowClick="function (s,e) { xgvw.StartEditRow(e.visibleIndex); } "></ClientSideEvents>
                                                    <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                                                    <Columns>
                                                        <%--<dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="1%">
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllCheckBox" ClientInstanceName="SelectAllCheckBox" runat="server"
                                                    ToolTip="Select/Unselect all rows on the page" CheckState="Unchecked">
                                                    <ClientSideEvents CheckedChanged="function(s, e) { xgvw.SelectAllRowsOnPage(s.GetChecked()); }" />
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>--%>

                                                        <dx:GridViewDataTextColumn FieldName="VENDOR_NAME" Caption="ผู้ขนส่ง" ReadOnly="True">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา" ReadOnly="True">
                                                            <DataItemTemplate>
                                                                <table width="99%">
                                                                    <tr>
                                                                        <td style="width: 80%;">
                                                                            <dx:ASPxLabel ID="lblSCONTRACTNO" runat="server" Text='<%# ""+Eval("SCONTRACTNO") %>'>
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td align="right">
                                                                            <%--<dx:ASPxButton ID="btncontractDetail" runat="server" Image-Url="~/Images/comment.png" AutoPostBack="false" EnableDefaultAppearance="False"  SkinID="NoSkind" >
                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('ContractDetail;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                             </dx:ASPxButton>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Left" />
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="DEV_DATE" Caption="วันที่เข้ารับสินค้า" ReadOnly="True" Visible="false"
                                                            Width="8%">
                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn FieldName="SABBREVIATION" Caption="คลังต้นทาง<br/>ตามสัญญา" CellStyle-Wrap="True" ReadOnly="True">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" Wrap="True">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="DDATE" Caption="วันที่ยืนยัน" ReadOnly="True"
                                                            Visible="false" Width="8%">
                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn Caption="จำนวนรถ">
                                                            <Columns>
                                                                <dx:GridViewBandColumn Caption="รถในสัญญา">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="NONHAND" Caption="ตามสัญญา" ReadOnly="True"
                                                                            Width="4%">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                            </PropertiesTextEdit>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewDataTextColumn FieldName="NONHANDSYSTEM" Caption="ในระบบ" ReadOnly="True"
                                                                            Width="4%">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                            </PropertiesTextEdit>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="มีปัญหา" Visible="False" Width="4%">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                            </PropertiesTextEdit>
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dx:GridViewBandColumn>
                                                                <dx:GridViewBandColumn Caption="รถสำรอง">
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn FieldName="NOUTHAND" Caption="ตามสัญญา" ReadOnly="True" Width="4%">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                            </PropertiesTextEdit>
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewDataTextColumn FieldName="NSTANDBYSYSTEM" Caption="ในระบบ" ReadOnly="True" Width="4%">
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                            </PropertiesTextEdit>
                                                                            <CellStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <CellStyle HorizontalAlign="Center">
                                                                            </CellStyle>
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                </dx:GridViewBandColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewBandColumn Caption="สถานะรถ">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn FieldName="NUNAVAILABLE" Caption="ไม่พร้อม<br/>ใช้งาน" ReadOnly="True" Width="4%">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                    </PropertiesTextEdit>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn FieldName="NTRANSIT" Caption="อยู่ระหว่าง<br/>ขนส่ง" ReadOnly="True" Width="4%">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                                    </PropertiesTextEdit>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn Caption="พร้อม<br/>ใช้งาน" ReadOnly="True" Width="4%">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="txtnTruckConfirm" ClientInstanceName="txtnTruckConfirm" runat="server"
                                                                            EnableViewState="true" Width="35px" Text='<%# Eval("NAVAILABLE") %>'
                                                                            HorizontalAlign="Center">
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="ยืนยันรถ<br/>(รวม)" ReadOnly="True" Width="4%" Name="txtnTruckConfirmSum">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="txtnTruckConfirmSum" ClientInstanceName="txtnTruckConfirmSum" runat="server"
                                                                            EnableViewState="true" Width="35px" ClientEnabled='false' Text='<%# Eval("NSUM") %>' ForeColor="Red"
                                                                            HorizontalAlign="Center">
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="ยืนยันก่อน" Width="">
                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy hh:mm">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="DUPDATE" Caption="แก้ไขล่าสุด" Width="">
                                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy hh:mm">
                                                            </PropertiesTextEdit>
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="SCONFIRM" Caption="สถานะ" Width="6%">
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="CCONFIRM" Caption="CCONFIRM" Visible="false">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NCONFIRM" Caption="NCONFIRM" Visible="false">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="NISSUE" Visible="false">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="NREJECT" Caption="NREJECT" Visible="false">
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="DEXPIRE" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NCONFIRMID" Caption="NCONFIRMID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="COIL" Caption="COIL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="CGAS" Caption="COIL" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NHOLD" Caption="NHOLD" Visible="false" />
                                                        <dx:GridViewDataTextColumn FieldName="NTRUCK" Caption="NTRUCK" Visible="false" />
                                                    </Columns>
                                                    <Settings ShowFooter="True"></Settings>
                                                    <Templates>
                                                        <EditForm>

                                                            <table id="tbl_xgvwCar" width="100%" runat="server">
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <dx:ASPxButton ID="btnsubmit" ClientInstanceName="btnsubmit" Visible="false" runat="server" SkinID="_submit"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {  xgvw.PerformCallback(s.name.substring(s.name.split('btnsubmit')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnclose" ClientInstanceName="btnclose" runat="server" SkinID="_close"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { xgvw.CancelEdit(); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnExportExcel" ClientInstanceName="btnExportExcel" runat="server" Text="Export Excel"
                                                                            OnClick="imbedit_Click" AutoPostBack="true" CommandArgument='<%# Eval("SCONTRACTID") + "|" + Eval("SCONTRACTNO") %>' CssClass="dxeLineBreakFix">
                                                                        </dx:ASPxButton>
                                                                    </td>

                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <dx:ASPxGridView ID="xgvwCar" Width="100%" ClientInstanceName="xgvwCar" runat="server" SkinID="_gvwChild"
                                                                            AutoGenerateColumns="true" KeyFieldName="STRUCKID" OnHtmlDataCellPrepared="xgvwCar_HtmlDataCellPrepared">
                                                                            <ClientSideEvents EndCallback="function(s, e){ eval(s.cpPopup); s.cpPopup=''; if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen ,'', 'width=1050,height=600,scrollbars=yes,resizable=1'); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:GridViewDataTextColumn Caption="ที่">
                                                                                    <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="รหัสรถ" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="CARTYPE" Caption="ประเภทรถ" ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="DELIVERY_PART" Caption="รอบการขนส่ง" ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewBandColumn Caption="ข้อมูลรถในสัญญา">
                                                                                    <Columns>
                                                                                        <dx:GridViewBandColumn Caption="ทะเบียนรถ">
                                                                                            <Columns>
                                                                                                <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="หัว" ReadOnly="True">
                                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                                </dx:GridViewDataTextColumn>
                                                                                                <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ท้าย"
                                                                                                    ReadOnly="True">
                                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                                </dx:GridViewDataTextColumn>
                                                                                            </Columns>
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewBandColumn>

                                                                                        <dx:GridViewDataTextColumn FieldName="CSTANDBY" Caption="ประเภท" ReadOnly="True"
                                                                                            Width="5%">
                                                                                            <DataItemTemplate>
                                                                                                <dx:ASPxLabel ID="lblSTANDBY" ClientInstanceName="lblSTANDBY" runat="server" Text='<%#  ""+Eval("CSTANDBY")=="N"?"ในสัญญา":"สำรอง" %>'>
                                                                                                </dx:ASPxLabel>
                                                                                            </DataItemTemplate>
                                                                                            <CellStyle HorizontalAlign="Center" />
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewDataTextColumn>

                                                                                    </Columns>
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewBandColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="CAR_STATUS" Caption="สถานะรถขนส่ง" ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewBandColumn Caption="ประสงค์เข้ารับงาน" HeaderStyle-HorizontalAlign="Center">
                                                                                    <Columns>
                                                                                        <dx:GridViewDataTextColumn FieldName="WAREHOUSE" Caption="คลังต้นทาง" PropertiesTextEdit-EncodeHtml="false"
                                                                                            ReadOnly="True">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <CellStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewDataTextColumn>
                                                                                        <dx:GridViewDataTextColumn FieldName="DELIVERY_NUM" Caption="เที่ยวที่รับงาน" PropertiesTextEdit-EncodeHtml="false"
                                                                                            ReadOnly="True">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <CellStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                </dx:GridViewBandColumn>
                                                                                <dx:GridViewBandColumn Caption="ระบบ IVMS">
                                                                                    <Columns>
                                                                                        <dx:GridViewDataTextColumn FieldName="GPS_STATUS" Caption="สถานะ GPS" ReadOnly="True">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <CellStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewDataTextColumn>
                                                                                        <dx:GridViewDataTextColumn FieldName="CCTV_STATUS" Caption="สถานะ กล้อง" ReadOnly="True">
                                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                                            <CellStyle HorizontalAlign="Center" />
                                                                                        </dx:GridViewDataTextColumn>
                                                                                    </Columns>
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewBandColumn>
                                                                                <%--<dx:GridViewBandColumn Caption="ตำแหน่งพิกัด GPS">
                                                                        <Columns>
                                                                            <dx:GridViewDataTextColumn FieldName="LAT_LONG" Caption="ละติจูด/ลองติจูด" ReadOnly="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <CellStyle HorizontalAlign="Left" />
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="KM2PLANT" Caption="ระยะทางถึงคลัง (กม.)" ReadOnly="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <CellStyle HorizontalAlign="Right" />
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="HR2PLANT" Caption="เวลาถึงคลัง (ชม.)" ReadOnly="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <CellStyle HorizontalAlign="Right" />
                                                                            </dx:GridViewDataTextColumn>
                                                                            <dx:GridViewDataTextColumn FieldName="ADDRESS_CURRENT" Caption="สถานที่อยู่ปัจจุบัน" ReadOnly="True">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <CellStyle HorizontalAlign="Left" />
                                                                            </dx:GridViewDataTextColumn>
                                                                        </Columns>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewBandColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="PERSONAL_CODE" Caption="รหัสบัตรประชาชน พขร."
                                                                        ReadOnly="True">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>--%>
                                                                                <dx:GridViewDataTextColumn FieldName="REMARK" Caption="หมายเหตุ" ReadOnly="True">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <CellStyle HorizontalAlign="Left" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="แจ้งปัญหา<br/>สภาพรถ" ReadOnly="True" Width="8%" Visible="false">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxTextBox ID="txtPassed" runat="server" ClientInstanceName="txtPassed" Text='<%# (""+Eval("CPASSED")=="0")?"0":"1"  %>'
                                                                                            ClientVisible="false">
                                                                                        </dx:ASPxTextBox>
                                                                                        <dx:ASPxButton ID="imbPassed" runat="server" SkinID="_passed" ClientInstanceName="imbPassed"
                                                                                            CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" Text=" ผ่าน "
                                                                                            ClientVisible="false">
                                                                                        </dx:ASPxButton>
                                                                                        <dx:ASPxButton ID="imbIssue" runat="server" SkinID="_issue" ClientInstanceName="imbIssue"
                                                                                            AutoPostBack="false" CausesValidation="False" CssClass="dxeLineBreakFix">
                                                                                            <ClientSideEvents Click="function(s,e){ var index= s.name.substring(s.name.split('imbIssue')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]; xgvwCar.PerformCallback(index+'$ISSUE'); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </DataItemTemplate>
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn Caption="ตรวจ<br/>สภาพรถ" PropertiesTextEdit-EncodeHtml="false" ReadOnly="True">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxLabel ID="lblStatusCar" ClientInstanceName="lblStatusCar" runat="server" Text='<%#  ""+Eval("CMA")=="3"?"ตกค้าง":(""+Eval("CMA")=="2"?"ห้ามวิ่ง":(""+Eval("CMA")=="1"?"แก้ไขภายใน "+Eval("NDAY_MA")+" วัน":(""+Eval("CMA")=="0"?"ปกติ":"ปกติ!"))) %>'>
                                                                                        </dx:ASPxLabel>
                                                                                    </DataItemTemplate>
                                                                                    <CellStyle HorizontalAlign="Center" />
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataColumn CellStyle-Cursor="hand" Caption="สถานะ<br/>ยืนยันรถ">
                                                                                    <DataItemTemplate>
                                                                                        <dx:ASPxLabel ID="lblStatus" runat="server" EncodeHtml="false" Text='<%# (""+Eval("IS_CONFIRM")=="0"?"<font color=red >ปฏิเสธ</font>":"ยืนยัน")%>'>
                                                                                        </dx:ASPxLabel>
                                                                                        <dx:ASPxTextBox ID="txtconfirm" runat="server" ClientInstanceName="txtconfirm" Text='<%# (""+Eval("IS_CONFIRM")=="0"?"":"1")%>'
                                                                                            ClientVisible="false">
                                                                                        </dx:ASPxTextBox>
                                                                                        <dx:ASPxButton ID="imbconfirm" runat="server" SkinID="_confirm" ClientInstanceName="imbconfirm"
                                                                                            CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" ClientVisible="false">
                                                                                        </dx:ASPxButton>
                                                                                        <dx:ASPxButton ID="imbcancel" runat="server" SkinID="_disconfirm" ClientInstanceName="imbcancel"
                                                                                            CausesValidation="False" CssClass="dxeLineBreakFix" ClientVisible="false">
                                                                                        </dx:ASPxButton>
                                                                                    </DataItemTemplate>
                                                                                    <FooterTemplate>
                                                                                    </FooterTemplate>
                                                                                    <CellStyle Cursor="hand" HorizontalAlign="Center" />
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <FooterCellStyle HorizontalAlign="Right" />
                                                                                </dx:GridViewDataColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="SHEADREGISTERNO"
                                                                                    Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="STRAILERREGISTERNO"
                                                                                    Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="SCARTYPENAME" Caption="SCARTYPENAME" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="CSTANDBY" Caption="CSTANDBY" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="DDATE" Caption="DDATE" Visible="false">
                                                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                                    </PropertiesTextEdit>
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="STRAILERID" Caption="STRAILERID" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="CMA" Caption="CMA" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="DFINAL_MA" Caption="DFINAL_MA" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="NDAY_MA" Caption="NDAY_MA" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="CPASSED" Caption="CPASSED" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="TCONFLST_CCONFIRM" Caption="TCONFLST_CCONFIRM"
                                                                                    Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="IS_CONFIRM" Caption="IS_CONFIRM" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                                <dx:GridViewDataTextColumn FieldName="PERSONAL_CODE" Caption="PERSONAL_CODE" Visible="false">
                                                                                </dx:GridViewDataTextColumn>
                                                                            </Columns>
                                                                            <SettingsPager Mode="ShowAllRecords" />
                                                                            <SettingsBehavior AllowSort="false" />
                                                                            <SettingsDetail ShowDetailRow="true" ShowDetailButtons="false" />
                                                                            <Settings ShowFooter="false" />
                                                                        </dx:ASPxGridView>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                    <Settings ShowFooter="True" />
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <dx:ASPxTextBox runat="server" ID="txtOrder" ClientInstanceName="txtOrder" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxGridView runat="server" ID="gvwT1" Width="100%"
                                                    KeyFieldName="SVENDORID" Visible="False">
                                                    <Columns>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Width="25%" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center"
                                                            FieldName="SABBREVIATION">
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel runat="server" ID="lblVen" Text="ผู้ขนส่ง" Font-Underline="false"
                                                                    Width="100%">
                                                                    <ClientSideEvents Click="function(s,e){ txtOrder.SetText('VENDORNAME'); xcpn.PerformCallback('Search'); }" />
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="เลขที่สัญญา" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                            CellStyle-HorizontalAlign="Center" FieldName="SCONTRACTNO">
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel runat="server" ID="lblContract" Text="เลขที่สัญญา" Font-Underline="false"
                                                                    Width="100%">
                                                                    <ClientSideEvents Click="function(s,e){ txtOrder.SetText('CONTRACT'); xcpn.PerformCallback('Search'); }" />
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Left">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="คลังต้นทาง" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="STERMINALNAME" Settings-AllowSort="False">
                                                            <DataItemTemplate>
                                                                <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"STERMINALNAME") %>'
                                                                    Font-Underline="true">
                                                                    <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('0'); PopDetail.Show(); }" />
                                                                </dx:ASPxLabel>
                                                            </DataItemTemplate>
                                                            <Settings AllowSort="False"></Settings>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn Caption="รถที่ยืนยัน" Width="20%" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="STRUCKREGIS" Settings-AllowSort="False">
                                                            <Settings AllowSort="False"></Settings>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataTextColumn Caption="ปริมาตร" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                            FieldName="CAPACITY" Settings-AllowSort="False">
                                                            <Settings AllowSort="False"></Settings>
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <CellStyle HorizontalAlign="Right">
                                                            </CellStyle>
                                                            <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                            </PropertiesTextEdit>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Settings ShowGroupPanel="false" />
                                                    <SettingsPager PageSize="50">
                                                    </SettingsPager>
                                                </dx:ASPxGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p2" Text="จัดสรรงานให้ ผขส.">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="22%" align="left">
                                                            <table class="hidden">
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                                                        ToolTips </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnExcelT2" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Text="ดาวน์โหลดแบบฟอร์มการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_ms_excel.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnPDFT2" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Text="ดาวน์โหลดตัวอย่างการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_pdf2.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="71%">
                                                            <table width="100%" cellpadding="2" cellspacing="1">
                                                                <tr>
                                                                    <td width="80%" align="center">
                                                                        <table width="100%" cellpadding="2" cellspacing="1">
                                                                            <tr>
                                                                                <td colspan="3" align="left">
                                                                                    <img src="images/ic_ms_excel.gif" />
                                                                                    Import Data<font color="red">*</font>(เฉพาะไฟล์Excel)<dx:ASPxTextBox ID="txtPermission"
                                                                                        ClientInstanceName="txtPermission" runat="server" ClientVisible="false">
                                                                                    </dx:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="50%">
                                                                                    <dx:ASPxUploadControl ID="uclOrder" runat="server" ClientInstanceName="uclOrder"
                                                                                        NullText="Click here to browse files..." Size="35" OnFileUploadComplete="uclOrder_FileUploadComplete"
                                                                                        UploadMode="Standard" Width="100%" CssClass="dxeLineBreakFix">
                                                                                        <ValidationSettings MaxFileSize="10000000" ShowErrors="false" AllowedFileExtensions=".xlsx,.xls">
                                                                                        </ValidationSettings>
                                                                                        <ClientSideEvents UploadingProgressChanged="function(s,e){UploadingProgressChanged(s,e);}"
                                                                                            FileUploadStart="function(s,e){FileUploadStart(s,e);}" FilesUploadComplete="function(s,e){ FilesUploadComplete(s,e);}"
                                                                                            FileUploadComplete="function(s, e) {if(e.callbackData ==''){OpenPopup();}else{ dxWarning('แจ้งเตือน', (e.callbackData+'').split('|')[0]);} }"></ClientSideEvents>
                                                                                        <BrowseButton Text="Browse..">
                                                                                        </BrowseButton>
                                                                                    </dx:ASPxUploadControl>
                                                                                </td>
                                                                                <td width="15%">
                                                                                    <dx:ASPxButton runat="server" ID="btnConfirm" AutoPostBack="false" Text="นำเข้าข้อมูล"
                                                                                        CssClass="dxeLineBreakFix" Width="100px">
                                                                                        <ClientSideEvents Click="function(s, e) { if(txtPermission.GetText() != ''){ uclOrder.Upload();} else{ dxWarning('แจ้งเตือน','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');}  }"></ClientSideEvents>
                                                                                    </dx:ASPxButton>
                                                                                </td>
                                                                                <td width="35%">
                                                                                    <dx:ASPxRadioButtonList runat="server" ID="rblChoice" RepeatDirection="Horizontal"
                                                                                        SkinID="rblStatus" ClientInstanceName="rblChoice">
                                                                                        <Items>
                                                                                            <dx:ListEditItem Text="ทุกรายการ" Value="A" />
                                                                                            <dx:ListEditItem Text="เฉพาะรายการใหม่" Value="N" />
                                                                                        </Items>
                                                                                    </dx:ASPxRadioButtonList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-left: 5px">
                                                                        <dx:ASPxProgressBar ID="pbProgressing1" ClientInstanceName="pbProgressing1" runat="server"
                                                                            Width="50%">
                                                                        </dx:ASPxProgressBar>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="10%" align="right">วันที่รับงาน : </td>
                                                        <td width="25%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            - <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="edtStart" runat="server" CssClass="edtStart datepicker" Style="text-align: center"></asp:TextBox>

                                                        </td>
                                                        <td width="7%" align="right">ถึง: </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="edtEnd" runat="server" CssClass="edtEnd datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>

                                                        <td width="10%" align="right">เที่ยวที่ขนส่ง: </td>
                                                        <td width="23%">
                                                            <asp:DropDownList runat="server" ID="cboWindowTime" CssClass="form-control">
                                                                <asp:ListItem Text="ทั้งหมด" Value="" />
                                                                <asp:ListItem Text="1" Value="1" />
                                                                <asp:ListItem Text="2" Value="2" />
                                                                <asp:ListItem Text="3" Value="3" />
                                                                <asp:ListItem Text="4" Value="4" />
                                                            </asp:DropDownList>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">กลุ่มงานที่:
                                                        </td>
                                                        <td align="right">
                                                            <asp:DropDownList runat="server" ID="ddlGroup" DataTextField="NAME" DataValueField="ID" CssClass="form-control"></asp:DropDownList>
                                                        </td>
                                                        <td align="right">ผู้ขนส่ง: </td>
                                                        <td align="right">
                                                            <dx:ASPxComboBox ID="cboVendorSearch" runat="server" CallbackPageSize="30" CssClass="form-control" ClientInstanceName="cboVendorSearch" EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID" Width="100%">
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                </Columns>
                                                                <ClientSideEvents ValueChanged="function(){  cboContractSearch.PerformCallback('Search'); }"></ClientSideEvents>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td align="right">เลขที่สัญญา: </td>
                                                        <td align="right">

                                                            <dx:ASPxComboBox runat="server" ID="cboContractSearch" CssClass="form-control" ClientInstanceName="cboContractSearch" TextField="SCONTRACTNO" ValueField="SCONTRACTID" OnCallback="cboContract_Callback"
                                                                Width="100%">
                                                                <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                            </dx:ASPxComboBox>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td align="right">คลังต้นทาง:
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="ddlSTERMINAL" ClientInstanceName="ddlSTERMINAL" runat="server" Width="100%" TextFormatString="{1}" ValueField="STERMINALID" IncrementalFilteringMode="Contains" CssClass="form-control">
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                                                    <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="SABBREVIATION" />
                                                                </Columns>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td width="10%" align="right">Delivery No.: </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="txtSearch2" runat="server" CssClass="form-control" placeholder="เลขที่ DO" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="color: red">*กดค้นหาเพื่อแสดงข้อมูล</td>
                                                        <td align="right">
                                                            <dx:ASPxButton runat="server" ID="ASPxButton1" AutoPostBack="false" Text="เพิ่มแผนงาน" CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function(s,e)
                                                        {
                                                        $('#addData').removeClass('hidden');
                                                                    $('.addwidth').find('input').css('width','100%');
                                                        }"></ClientSideEvents>
                                                            </dx:ASPxButton>

                                                            <dx:ASPxButton runat="server" ID="btnSearch" AutoPostBack="false" SkinID="_search" CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton runat="server" ID="btnExport1" AutoPostBack="true" Text="Export" CssClass="dxeLineBreakFix" OnClick="btnExport1_Click">
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton runat="server" ID="btnClear" AutoPostBack="false" Text="Clear" CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Claer');}"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton runat="server" ID="ASPxButton2" AutoPostBack="false" CssClass="dxeLineBreakFix" Text="ตรวจสอบเวลาที่รถต้องถึงคลัง">
                                                                <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('IVMS');}"></ClientSideEvents>
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="addData" class="addData form-group hidden">
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading">
                                                            <i class="fa fa-table"></i>
                                                            <asp:Label ID="lblHeaderTab1" runat="server" Text="เพิ่มข้อมูล"></asp:Label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="dataTable_wrapper">
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal">
                                                                        <div class="row form-group">
                                                                            <label class="col-md-2 control-label">Delivery No.:</label>
                                                                            <div class="col-md-4">

                                                                                <asp:TextBox ID="txtDeliveryNoAdd" runat="server" CssClass="form-control" Style="text-align: center"></asp:TextBox>
                                                                            </div>
                                                                            <label class="col-md-2 control-label"></label>
                                                                            <div class="col-md-4">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <label class="col-md-2 control-label">วันที่รับงาน:</label>
                                                                            <div class="col-md-4">
                                                                                <asp:TextBox ID="txtDate" runat="server" CssClass="datepicker txtDate" Style="text-align: center"></asp:TextBox>
                                                                            </div>
                                                                            <label class="col-md-2 control-label">เที่ยวที่ขนส่ง:</label>
                                                                            <div class="col-md-4">
                                                                                <asp:DropDownList runat="server" ID="ddlNLINE" CssClass="form-control">
                                                                                    <asp:ListItem Text="1" Value="1" />
                                                                                    <asp:ListItem Text="2" Value="2" />
                                                                                    <asp:ListItem Text="3" Value="3" />
                                                                                    <asp:ListItem Text="4" Value="4" />
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <label class="col-md-2 control-label">ผู้ขนส่ง:</label>
                                                                            <div class="col-md-4">
                                                                                <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" Width="100%" CssClass="form-control addwidth" ClientInstanceName="cboVendorAdd" EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                                    SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID">
                                                                                    <Columns>
                                                                                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                                    </Columns>
                                                                                    <ClientSideEvents ValueChanged="function(){  cboContractAdd.PerformCallback('Add'); }"></ClientSideEvents>
                                                                                </dx:ASPxComboBox>
                                                                            </div>
                                                                            <label class="col-md-2 control-label">เลขที่สัญญา:</label>
                                                                            <div class="col-md-4">
                                                                                <dx:ASPxComboBox runat="server" ID="cboContract" ClientInstanceName="cboContractAdd" TextField="SCONTRACTNO" ValueField="SCONTRACTID" OnCallback="cboContract_Callback" Width="100%" CssClass="form-control addwidth">
                                                                                    <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                                                </dx:ASPxComboBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <label class="col-md-2 control-label">วันที่ถึงปลายทาง:</label>
                                                                            <div class="col-md-4">
                                                                                <asp:TextBox ID="txtLateDay" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                            </div>
                                                                            <label class="col-md-2 control-label">เวลาที่ถึงปลายทาง:</label>
                                                                            <div class="col-md-4">
                                                                                <asp:TextBox ID="txtLateTime" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <center>
                                                                            <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('saveAdd'); }" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                                                <ClientSideEvents Click="function (s, e) {$('#addData').addClass('hidden');}" />
                                                                            </dx:ASPxButton>
                                                                        </center>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <dx:ASPxGridView runat="server" ID="gvw" ClientInstanceName="gvw" AutoGenerateColumns="false" SettingsBehavior-AllowSort="false"
                                                    Width="100%" KeyFieldName="SDELIVERYNO">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งที่ยังไม่ได้จัดแผน">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataColumn FieldName="CHECKLATE" >
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="cbSelectHeader" runat="server" AutoPostBack="false"/>
                                                                                
                                                                            </HeaderTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <DataItemTemplate>
                                                                                <asp:CheckBox ID="cbSelect" runat="server" AutoPostBack="false"/>
                                                                            </DataItemTemplate>
                                                                            <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                        </dx:GridViewDataColumn>

                                                                <dx:GridViewDataDateColumn Caption="วันที่รับงาน" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวที่ขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="กลุ่มงานที่" FieldName="GROUPNAME" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('1'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="คลังต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn Caption="วันที่กำหนด<br/>ถึงปลายทาง" FieldName="LATEDATE" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>

                                                                <dx:GridViewDataColumn Caption="เวลาที่กำหนด<br/>ถึงปลายทาง" FieldName="LATETIME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="เวลาที่รถ<br/>ต้องถึงคลัง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDATETIMEEXPECT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMEEXPECT") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ประเภทการขนส่ง" FieldName="TRAN_TYPE" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <%-- <dx:ASPxButton runat="server" ID="btnEdit" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('edit;'+txtIndx.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel" SkinID="_delete" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('delete;'+ txtIndx.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>--%>
                                                                        <dx:ASPxButton runat="server" ID="btnEdit" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('edit;'+txtIndx.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel" SkinID="_delete" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndx.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('delete;'+ txtIndx.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NLINE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="TSTART" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="TEND" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="LATEDAY" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SPLNT_CODE" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SHIP_TO" Visible="false"></dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <table width="100%" cellpadding="3" cellspacing="1">
                                                                <%--            <tr>
                                                                    <td width="12%">
                                                                        Delivery No.
                                                                    </td>
                                                                    <td width="38%">
                                                                        <div style="float: left">
                                                                            <dx:ASPxComboBox ID="cboDelivery" runat="server" CallbackPageSize="30" ClientInstanceName="cboDelivery"
                                                                                EnableCallbackMode="True" MaxLength="10" OnItemRequestedByValue="cboDelivery_OnItemRequestedByValueSQL"
                                                                                OnItemsRequestedByFilterCondition="cboDelivery_OnItemsRequestedByFilterConditionSQL"
                                                                                SkinID="xcbbATC" TextFormatString="{0}" ValueField="DELIVERY_NO" Width="130px">
                                                                              
                                                                                <Columns>
                                                                                    <dx:ListBoxColumn Caption="Outbound No." FieldName="DELIVERY_NO" Width="100px" />
                                                                                    <dx:ListBoxColumn Caption="Ship-to" FieldName="SHIP_TO" Width="80px" />
                                                                                    <dx:ListBoxColumn Caption="ชื่อลูกค้า" FieldName="SCUSTOMER" Width="150px" />
                                                                                    <dx:ListBoxColumn Caption="ปริมาณ" FieldName="NVALUE" Width="100px" />
                                                                                </Columns>
                                                                            </dx:ASPxComboBox>
                                                                        </div>
                                                                        <div style="float: left">
                                                                            <dx:ASPxButton ID="btnGetOutbound" runat="server" Width="20px" AutoPostBack="false"
                                                                                Text="Get" Title="ดึง Outbound จาก SAP Master">
                                                                                <ClientSideEvents Click="function(s ,e){xcpnOutBound.PerformCallback(cboDelivery.GetValue());cboDelivery.SetValue('');}" />
                                                                            </dx:ASPxButton>
                                                                            <dx:ASPxCallback ID="xcpnOutBound" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpnOutBound"
                                                                                OnCallback="xcpnOutBound_Callback">
                                                                            </dx:ASPxCallback>
                                                                        </div>
                                                                    </td>
                                                                    <td width="12%">
                                                                        ประเภทการขนส่ง
                                                                    </td>
                                                                    <td width="38%">
                                                                        <dx:ASPxComboBox ID="cboTRANTYPE" runat="server">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="งานขนส่งภายในวัน" Value="1" />
                                                                                <dx:ListEditItem Text="งานขนส่งล่วงหน้า" Value="2" />
                                                                            </Items>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>--%>
                                                                <tr>
                                                                    <td>ผู้ขนส่ง </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                                                            EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                            OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                            SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="100%">
                                                                            <ClientSideEvents ValueChanged="function(){  cboContract.PerformCallback(); }" TextChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendorname.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SABBREVIATION')) ; }else{txtVendorname.SetValue('') ;};}"></ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                                <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                            </Columns>
                                                                            <ValidationSettings RequiredField-ErrorText="ระบุบริษัท" RequiredField-IsRequired="true"
                                                                                ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                                <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>สัญญา </td>
                                                                    <td>

                                                                        <dx:ASPxComboBox runat="server" ID="cboContract" ClientInstanceName="cboContract"
                                                                            OnCallback="cboContract_Callback" TextField="SCONTRACTNO" ValueField="SCONTRACTID"
                                                                            Width="100%">
                                                                            <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>วันที่รับงาน </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="datepicker txtDateGrid" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                    <td>เที่ยวที่ขนส่ง </td>
                                                                    <td>
                                                                        <asp:DropDownList runat="server" ID="ddlNLINE" CssClass="form-control">
                                                                            <asp:ListItem Text="1" Value="1" />
                                                                            <asp:ListItem Text="2" Value="2" />
                                                                            <asp:ListItem Text="3" Value="3" />
                                                                            <asp:ListItem Text="4" Value="4" />
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>วันที่ถึงปลายทาง </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLateDay" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                    <td>เวลาที่ถึงปลายทาง </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLateTime" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false; gvw.PerformCallback('savegvw;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {gvw.PerformCallback('cancel');}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>

                                                </dx:ASPxGridView>
                                                <dx:ASPxTextBox ID="txtVendorname" ClientInstanceName="txtVendorname" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtIndx" ClientInstanceName="txtIndx" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p3" Text="ผขส. ยืนยันแผนงาน">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" width="100%" style="display: none">
                                                    <tr>
                                                        <td width="22%" align="left">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/arr.gif" width="7" height="7" alt="" />
                                                                        ToolTips </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnExcelT3" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Text="ดาวน์โหลดแบบฟอร์มการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_ms_excel.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <dx:ASPxButton runat="server" ID="btnPdfT3" AutoPostBack="false" EnableTheming="false"
                                                                            EnableDefaultAppearance="false" Text="ดาวน์โหลดตัวอย่างการแบ่งงาน"
                                                                            Width="100%" HorizontalAlign="Left">
                                                                            <Image Url="images/ic_pdf2.gif">
                                                                            </Image>
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="71%"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr>
                                                        <td width="10%" align="right">วันที่รับงาน : </td>
                                                        <td width="25%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="edtStart" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            - <dx:ASPxDateEdit runat="server" ID="edtEnd" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="edtStartT3" runat="server" CssClass="edtStart datepicker" Style="text-align: center"></asp:TextBox>

                                                        </td>
                                                        <td width="7%" align="right">ถึง: </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="edtEndT3" runat="server" CssClass="edtEnd datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>

                                                        <td width="10%" align="right">เที่ยวที่ขนส่ง: </td>
                                                        <td width="23%">
                                                            <asp:DropDownList runat="server" ID="ddlNLINET3" CssClass="form-control">
                                                                <asp:ListItem Text="ทั้งหมด" Value="" />
                                                                <asp:ListItem Text="1" Value="1" />
                                                                <asp:ListItem Text="2" Value="2" />
                                                                <asp:ListItem Text="3" Value="3" />
                                                                <asp:ListItem Text="4" Value="4" />
                                                            </asp:DropDownList>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">กลุ่มงานที่:
                                                        </td>
                                                        <td align="right">
                                                            <asp:DropDownList runat="server" ID="ddlGroupT3" DataTextField="NAME" DataValueField="ID" CssClass="form-control"></asp:DropDownList>
                                                        </td>
                                                        <td align="right">ผู้ขนส่ง: </td>
                                                        <td align="right">
                                                            <dx:ASPxComboBox ID="cboVendorSearchT3" runat="server" CallbackPageSize="30" CssClass="form-control" ClientInstanceName="cboVendorSearchT3" EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                SkinID="xcbbATC" TextFormatString="{1}" ValueField="SVENDORID" Width="100%">
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                    <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                </Columns>
                                                                <ClientSideEvents ValueChanged="function(){  cboContractSearchT3.PerformCallback('Search'); }"></ClientSideEvents>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td align="right">เลขที่สัญญา: </td>
                                                        <td align="right">

                                                            <dx:ASPxComboBox runat="server" ID="cboContractSearchT3" CssClass="form-control" ClientInstanceName="cboContractSearchT3" TextField="SCONTRACTNO" ValueField="SCONTRACTID" OnCallback="cboContract_Callback"
                                                                Width="100%">
                                                                <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                            </dx:ASPxComboBox>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td align="right">คลังต้นทาง:
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="ddlSTERMINALT3" ClientInstanceName="ddlSTERMINALT3" runat="server" Width="100%" TextFormatString="{1}" ValueField="STERMINALID" IncrementalFilteringMode="Contains" CssClass="form-control">
                                                                <Columns>
                                                                    <dx:ListBoxColumn Caption="รหัสคลัง" FieldName="STERMINALID" />
                                                                    <dx:ListBoxColumn Caption="ชื่อคลัง" FieldName="SABBREVIATION" />
                                                                </Columns>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td width="10%" align="right">ค้นหา: </td>
                                                        <td width="25%">
                                                            <asp:TextBox ID="txtSearch3" runat="server" CssClass="form-control" placeholder="Delivery No.,ทะเบียนรถ" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton runat="server" ID="ASPxButton4" AutoPostBack="false" SkinID="_search" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                                <dx:ASPxButton runat="server" ID="btnExport2" AutoPostBack="true" Text="Export" CssClass="dxeLineBreakFix" OnClick="btnExport2_Click">
                                                </dx:ASPxButton>
                                                <dx:ASPxButton runat="server" ID="btnClearT3" AutoPostBack="false" Text="Clear" CssClass="dxeLineBreakFix">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Claer');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwT3" ClientInstanceName="gvwT3" AutoGenerateColumns="false"
                                                    Width="100%" KeyFieldName="ORDERID">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="งานขนส่งที่จัดแผนแล้ว">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataDateColumn Caption="วันที่รับงาน" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวที่ขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>


                                                                <dx:GridViewDataColumn Caption="กลุ่มงานที่" FieldName="GROUPNAME" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('1'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="คลังต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataDateColumn Caption="วันที่กำหนดถึง<br/>ปลายทาง" FieldName="LATEDATE" HeaderStyle-HorizontalAlign="Center" PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataDateColumn>

                                                                <dx:GridViewDataColumn Caption="เวลาที่กำหนดถึง<br/>ปลายทาง" FieldName="LATETIME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="เวลาที่รถ<br/>ต้องถึงคลัง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDATETIMEEXPECT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMEEXPECT") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="คาดว่ารถจะ<br/>ถึงคลังต้นทาง" HeaderStyle-HorizontalAlign="Center">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="txtLate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DATETIMETOTERMINAL") %>'>
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="TRANTRUCK" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                </dx:GridViewDataColumn>

                                                                <dx:GridViewDataColumn Caption="Drop No." FieldName="ROUND" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="การจัดการ" HeaderStyle-HorizontalAlign="Center" Width="120px">
                                                                    <DataItemTemplate>
                                                                        <%-- <dx:ASPxButton runat="server" ID="btnEdit2" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix"
                                                                            ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('editT3;'+txtIndxT3.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel2" SkinID="_delete" AutoPostBack="false"
                                                                            CssClass="dxeLineBreakFix" ClientEnabled='<%#  Eval("SHIPMENT").ToString() == "xxx"  ? true :false  %>'>
                                                                           
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('deleteT3;'+ txtIndxT3.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>--%>
                                                                        <dx:ASPxButton runat="server" ID="btnEdit2" SkinID="_edit" AutoPostBack="false" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); xcpn.PerformCallback('editT3;'+txtIndxT3.GetValue()); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton runat="server" ID="btnDel2" SkinID="_delete" AutoPostBack="false"
                                                                            CssClass="dxeLineBreakFix">
                                                                            <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('delete;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                            <ClientSideEvents Click="function (s, e) { txtIndxT3.SetValue(s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); dxConfirm('ยืนยันการทำรายการ','คุณต้องการลบข้อมูลหรือไม่', function () {    xcpn.PerformCallback('deleteT3;'+ txtIndxT3.GetValue() + ';' + s.name); dxPopupConfirm.Hide(); } ,function () {dxPopupConfirm.Hide();} );  }" />
                                                                        </dx:ASPxButton>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NLINE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="TSTART" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="TEND" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="LATEDAY" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SPLNT_CODE" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SHIP_TO" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="FULLNAME" Visible="false"></dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SEMAIL" Visible="false"></dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Templates>
                                                        <EditForm>
                                                            <table width="100%" cellpadding="3" cellspacing="1">
                                                                <tr>
                                                                    <td>ผู้ขนส่ง </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox ID="cboVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cboVendor"
                                                                            EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                                                                            OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL"
                                                                            SkinID="xcbbATC" TextFormatString="{0} - {1}" ValueField="SVENDORID" Width="100%">
                                                                            <ClientSideEvents ValueChanged="function(){ cboContractT3.PerformCallback();  }"
                                                                                TextChanged="function (s, e) {if(s.GetSelectedIndex() + '' != '-1'){txtVendornameT3.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SABBREVIATION')) ; }else{txtVendornameT3.SetValue('') ;};}"></ClientSideEvents>
                                                                            <Columns>
                                                                                <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                                                                                <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SABBREVIATION" Width="400px" />
                                                                            </Columns>
                                                                            <ValidationSettings RequiredField-ErrorText="ระบุบริษัท" RequiredField-IsRequired="true"
                                                                                ValidationGroup="add" ErrorDisplayMode="ImageWithTooltip">
                                                                                <RequiredField IsRequired="True" ErrorText="ระบุบริษัท"></RequiredField>
                                                                            </ValidationSettings>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td>สัญญา </td>
                                                                    <td>
                                                                        <dx:ASPxComboBox runat="server" ID="cboContractT3" ClientInstanceName="cboContractT3"
                                                                            OnCallback="cboContractT3_Callback" TextField="SCONTRACTNO" ValueField="SCONTRACTID"
                                                                            Width="100%">
                                                                            <ClientSideEvents BeginCallback="function(s, e) {  LoadingPanel.Show(); }" EndCallback="function(s, e) {  LoadingPanel.Hide(); }" />
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>วันที่รับงาน </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="datepicker txtDateGrid" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                    <td>เที่ยวที่ขนส่ง </td>
                                                                    <td>
                                                                        <asp:DropDownList runat="server" ID="ddlNLINE" CssClass="form-control">
                                                                            <asp:ListItem Text="1" Value="1" />
                                                                            <asp:ListItem Text="2" Value="2" />
                                                                            <asp:ListItem Text="3" Value="3" />
                                                                            <asp:ListItem Text="4" Value="4" />
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>วันที่ถึงปลายทาง </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLateDay" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                    <td>เวลาที่ถึงปลายทาง </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLateTime" runat="server" CssClass="txtDateGrid form-control" Style="text-align: center"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" align="center">
                                                                        <dx:ASPxButton ID="btnSubmit" runat="server" SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function(s,e){if(!ASPxClientEdit.ValidateGroup('add')) return false; gvwT3.PerformCallback('savegvwT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                        </dx:ASPxButton>
                                                                        <dx:ASPxButton ID="btnCancel" runat="server" SkinID="_close" CssClass="dxeLineBreakFix">
                                                                            <ClientSideEvents Click="function (s, e) {gvwT3.PerformCallback('cancel');}" />
                                                                        </dx:ASPxButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EditForm>
                                                    </Templates>
                                                </dx:ASPxGridView>
                                                <dx:ASPxTextBox ID="txtVendornameT3" ClientInstanceName="txtVendornameT3" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="txtIndxT3" ClientInstanceName="txtIndxT3" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Name="p4" Text="แผนงานที่ถูกยกเลิก">
                            <ContentCollection>
                                <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True">
                                    <table width="100%" cellpadding="3" cellspacing="1">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="3" cellspacing="1">
                                                    <tr valign="middle">
                                                        <td width="10%">Delivery Date : </td>
                                                        <td width="25%">
                                                            <%--<dx:ASPxDateEdit runat="server" ID="edtStartT4" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>
                                                            - <dx:ASPxDateEdit runat="server" ID="edtEndT4" SkinID="xdte" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="edtStartT4" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                            <asp:TextBox ID="edtEndT4" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td width="7%" align="right">ค้นหา: </td>
                                                        <td width="25%">
                                                            <dx:ASPxTextBox runat="server" ID="txtSearch3T4" NullText="ผู้ขนส่ง, เลขที่สัญญา, ทะเบียนรถ, เลขที่ DO"
                                                                Width="100%">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td width="10%" align="right">กะการขนส่ง: </td>
                                                        <td width="23%">
                                                            <dx:ASPxComboBox runat="server" ID="cboWindowTimeT4" DataSourceID="sdsWindowTime"
                                                                TextField="SWINDOWTIMENAME" ValueField="NLINE" Width="100%">
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <dx:ASPxButton runat="server" ID="ASPxButton3" AutoPostBack="false" SkinID="_search">
                                                    <ClientSideEvents Click="function(s,e){xcpn.PerformCallback('Search');}"></ClientSideEvents>
                                                </dx:ASPxButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dx:ASPxGridView runat="server" ID="gvwT4" ClientInstanceName="gvwT4" AutoGenerateColumns="false"
                                                    Width="100%" KeyFieldName="ORDERID">
                                                    <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                                    <Columns>
                                                        <dx:GridViewBandColumn HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <dx:ASPxLabel ID="lblHead" runat="server" Text="แผนที่โดนดึงกลับ">
                                                                </dx:ASPxLabel>
                                                            </HeaderTemplate>
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="DO Date" FieldName="DDELIVERY" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Delivery No." FieldName="SDELIVERYNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="85px">
                                                                    <DataItemTemplate>
                                                                        <dx:ASPxLabel ID="lblDo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"SDELIVERYNO") %>'
                                                                            Font-Underline="true">
                                                                            <ClientSideEvents Click="function(s,e){ var inx = s.name.substring(s.name.lastIndexOf('_')+1,s.name.length); txtDeliveryNo.SetText(inx);  txtSelectgvw.SetText('3'); PopDetail.Show(); }" />
                                                                        </dx:ASPxLabel>
                                                                    </DataItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ต้นทาง" FieldName="STERMINALNAME_FROM" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ปลายทาง" FieldName="STERMINALNAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="NVALUE" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="75px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                    <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                    </PropertiesTextEdit>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataColumn Caption="ผู้ขนส่ง" FieldName="SABBREVIATION" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="180px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="150px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="ทะเบียนรถ" FieldName="TRANTRUCK" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="165px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="กะขนส่ง" FieldName="SWINDOWTIMENAME" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="เที่ยวการขนส่ง" FieldName="ROUND" HeaderStyle-HorizontalAlign="Center"
                                                                    Width="100px">
                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    <CellStyle HorizontalAlign="Center">
                                                                    </CellStyle>
                                                                </dx:GridViewDataColumn>
                                                            </Columns>
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="NWINDOWTIMEID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="STERMINALID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="SCONTRACTID" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                        <dx:GridViewDataColumn FieldName="ORDERTYPE" Visible="false">
                                                        </dx:GridViewDataColumn>
                                                    </Columns>
                                                    <Settings ShowHorizontalScrollBar="True" UseFixedTableLayout="true" />
                                                </dx:ASPxGridView>
                                                <%--   <dx:ASPxTextBox ID="ASPxTextBox2" ClientInstanceName="txtVendornameT3" runat="server"
                                                    ClientVisible="false">
                                                </dx:ASPxTextBox>
                                                <dx:ASPxTextBox ID="ASPxTextBox3" ClientInstanceName="txtIndxT3" runat="server" ClientVisible="false">
                                                </dx:ASPxTextBox>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
                <dx:ASPxTextBox runat="server" ID="txtDeliveryNo" ClientInstanceName="txtDeliveryNo"
                    ClientVisible="false">
                </dx:ASPxTextBox>
                <dx:ASPxTextBox runat="server" ID="txtSelectgvw" ClientInstanceName="txtSelectgvw"
                    ClientVisible="false">
                </dx:ASPxTextBox>
                <dx:ASPxPopupControl runat="server" ID="PopDetail" ClientInstanceName="PopDetail"
                    CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
                    HeaderText="รายละเอียด" AllowDragging="True" PopupAnimationType="None" EnableViewState="False"
                    Width="800px" AutoUpdatePosition="true" CssClass="Poop">
                    <ClientSideEvents PopUp="function(s,e){ xcpnPopup.PerformCallback('ListData'); }" />
                    <ContentCollection>
                        <dx:PopupControlContentControl>
                            <dx:ASPxCallbackPanel ID="xcpnPopup" runat="server" HideContentOnCallback="False"
                                OnCallback="xcpnPopup_Callback" ClientInstanceName="xcpnPopup" CausesValidation="False"
                                OnLoad="xcpnPopup_Load" LoadingDivStyle-CssClass="SS">
                                <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;if(s.cpRedirectOpen != undefined) window.open(s.cpRedirectOpen); s.cpRedirectOpen = undefined}"></ClientSideEvents>
                                <LoadingDivStyle CssClass="SS">
                                </LoadingDivStyle>
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <asp:Literal runat="server" ID="ltr_PANT"></asp:Literal><table width="100%" cellpadding="3" cellspacing="1" runat="server" id="tbl_DOSHOW"
                                            style="display: none;">
                                            <tr>
                                                <td>
                                                    <dx:ASPxLabel runat="server" ID="lblDoNo">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <dx:ASPxGridView runat="server" ID="gvwPop" Width="100%" ClientInstanceName="gvwPop">
                                                        <Columns>
                                                            <dx:GridViewDataColumn Caption="ลำดับ" Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataColumn Caption="ชื่อผลิตภัณฑ์" FieldName="MATERIAL_NAME" Width="60%"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                            </dx:GridViewDataColumn>
                                                            <dx:GridViewDataTextColumn Caption="ปริมาณ" FieldName="QTY_TRAN" Width="15%" HeaderStyle-HorizontalAlign="Center"
                                                                CellStyle-HorizontalAlign="Center">
                                                                <PropertiesTextEdit DisplayFormatString="{0:n0}">
                                                                </PropertiesTextEdit>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="วันที่ขนส่ง" FieldName="DEV_DATE" Width="15%"
                                                                HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                </PropertiesTextEdit>
                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxCallbackPanel>
                        </dx:PopupControlContentControl>
                    </ContentCollection>
                </dx:ASPxPopupControl>
                <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel">
                </dx:ASPxLoadingPanel>
                <asp:SqlDataSource ID="sdsWindowTime" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                    SelectCommand="SELECT NLINE,'เที่ยวที่ '||NLINE||' เวลา '||TSTART||'-'||TEND||' น.' as SWINDOWTIMENAME FROM TWINDOWTIME 
GROUP BY NLINE,'เที่ยวที่ '||NLINE||' เวลา '||TSTART||'-'||TEND||' น.' 
ORDER BY NLINE"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsConTract" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sdsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"></asp:SqlDataSource>
            </dx:PanelContent>
        </PanelCollection>
    </dx:aspxcallbackpanel>
    <script type="text/javascript">
        setdatepicker();
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            setdatepicker();
        }
        function setdatepicker() {
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    return false;
                }
            });
            $('.edtStart').parent().click(function () {
                var date = new Date();
                //date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=edtStart.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(date);
            });
            $('.edtEnd').parent().click(function () {
                var date = new Date();
                var dates = new Date();
                $("#<%=edtEnd.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(dates);
                date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=edtEnd.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(date);
            });
            $('.txtDate').parent().click(function () {
                var date = new Date();
                var dates = new Date();
                $("#<%=txtDate.ClientID %>").parent('.input-group.date').datepicker('update').setStartDate(dates);
                date.setDate(date.getDate() + 6);
                //alert(date);
                $("#<%=txtDate.ClientID %>").parent('.input-group.date').datepicker('update').setEndDate(date);
            });

            $('.txtDateGrid').parent().click(function () {
                var date = new Date();
                var dates = new Date();
                $(this).find('input').parent('.input-group.date').datepicker('update').setStartDate(dates);
                date.setDate(date.getDate() + 6);
                //alert(date);
                $(this).find('input').parent('.input-group.date').datepicker('update').setEndDate(date);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
