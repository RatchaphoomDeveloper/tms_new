﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ComplaintTopicAdd : PageBase
{
    #region + View State +
    private int TopicID
    {
        get
        {
            if ((int)ViewState["TopicID"] != null)
                return (int)ViewState["TopicID"];
            else
                return 0;
        }
        set
        {
            ViewState["TopicID"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            TopicBLL.Instance.TopicAddBLL(txtTopicName.Text.Trim(), int.Parse(radStatus.SelectedValue), int.Parse(Session["UserID"].ToString()));

            alertSuccess("บันทึกข้อมูลเรียบร้อย", "ComplaintTopic.aspx");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ComplaintTopic.aspx");
    }
}