﻿<%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    EnableViewState="true" CodeFile="ReportMonthly.aspx.cs" Inherits="ReportMonthly"
    StylesheetTheme="Aqua" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <%--<asp:Panel ID="pnlMain" runat="server" EnableViewState="true"></asp:Panel>--%>
    <%--<asp:Button ID="cmdTest" runat="server" Text="Get" onclick="cmdTest_Click" />--%>
    <br />
    <br />
    <div style="text-align: right; width: 100%; color: Blue; padding-right: 20px">
        <asp:Label ID="lblDateNow" runat="server" Text="วันที่ : {0}" ForeColor="Green"></asp:Label>
    </div>
    <ul class="nav nav-tabs" runat="server" id="tabtest">
        <li class="active" id="Tab1" runat="server"><a href="#TabGeneral" data-toggle="tab"
            aria-expanded="true" runat="server" id="GeneralTab">รายงานประจำเดือน</a></li>
        <li class="" id="Tab2" runat="server"><a href="#TabProcess" data-toggle="tab" aria-expanded="false"
            runat="server" id="ProcessTab">รายงานประจำ 3 เดือน</a></li>
        <li class="" id="Tab3" runat="server"><a href="#TabFinal" data-toggle="tab" aria-expanded="false"
            runat="server" id="FinalTab">รายงานประจำ 6 เดือน</a></li>
        <li class="" id="Tab4" runat="server"><a href="#TabScore" data-toggle="tab" aria-expanded="false"
            runat="server" id="ScoreTab">ตรวจสอบรายงาน</a></li>
        <li class="" id="Tab5" runat="server"><a href="#TabReport" data-toggle="tab" aria-expanded="false"
            runat="server" id="ReportTab">สรุปรายงาน (PTT)</a></li>
        <li class="" id="Tab6" runat="server"><a href="#TabFinalVendor" data-toggle="tab"
            aria-expanded="false" runat="server" id="ReportFinalVendor">สรุปรายงาน</a></li>
    </ul>
    <div class="tab-content" style="padding-left: 20px; padding-right: 20px;">
        <div class="tab-pane fade active in" id="TabGeneral">
            <br />
            <div style="width: 100%; color: Green; padding-left: 20px">
                <asp:Label ID="lblSendReportDescTab1" runat="server" Text=""></asp:Label>
            </div>
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblVendorTab1" Text="เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)" runat="server"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="dgvRequestFileTab1" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ" ItemStyle-Width="150px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB" ItemStyle-Width="160px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblTab1" runat="server" Text="แนบไฟล์เอกสาร {0}"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table3" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                เลขที่สัญญา
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboContractTab1" runat="server" class="form-control" DataTextField="SCONTRACTNO"
                                            AutoPostBack="true" Width="350px" DataValueField="SCONTRACTID" OnSelectedIndexChanged="cboContractTab1_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                ประเภทไฟล์เอกสาร
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadTypeTab1" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:FileUpload ID="fileUploadTab1" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUploadTab1" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdUploadTab1_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <div style="overflow-x: scroll; width: 100%">
                                <asp:GridView ID="dgvUploadFileTab1" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="True" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="False"
                                    EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                    CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowDeleting="dgvUploadFileTab1_RowDeleting"
                                    OnRowUpdating="dgvUploadFileTab1_RowUpdating" OnRowDataBound="dgvUploadFileTab1_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Width="80px" Wrap="False"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" >
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                    </asp:BoundField>--%>
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLPATH" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSaveTab1" runat="server" Text="ส่งรายงาน" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-info" Style="width: 120px" data-toggle="modal"
                                data-target="#ModalConfirmBeforeSaveTab1" />
                            &nbsp;
                            <asp:Button ID="cmdSaveTab1Draft" runat="server" Text="บันทึก (ร่าง)" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-warning" Style="width: 120px" OnClick="cmdSaveTab1Draft_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <br />
        </div>
        <div class="tab-pane fade" id="TabProcess">
            <br />
            <div style="width: 100%; color: Green; padding-left: 20px">
                <asp:Label ID="lblSendReportDescTab2" runat="server" Text=""></asp:Label>
            </div>
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblVendorTab2" Text="เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)" runat="server"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="dgvRequestFileTab2" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ" ItemStyle-Width="150px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB" ItemStyle-Width="160px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblTab2" runat="server" Text="แนบไฟล์เอกสาร {0}"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table1" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                เลขที่สัญญา
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboContractTab2" runat="server" class="form-control" DataTextField="SCONTRACTNO"
                                            AutoPostBack="true" Width="350px" DataValueField="SCONTRACTID" OnSelectedIndexChanged="cboContractTab2_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                ประเภทไฟล์เอกสาร
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadTypeTab2" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:FileUpload ID="fileUploadTab2" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUploadTab2" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdUploadTab2_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <div style="overflow-x: scroll; width: 100%">
                                <asp:GridView ID="dgvUploadFileTab2" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                    EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                    CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowDeleting="dgvUploadFileTab2_RowDeleting"
                                    OnRowUpdating="dgvUploadFileTab2_RowUpdating" OnRowDataBound="dgvUploadFileTab2_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Width="80px" Wrap="False"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" >
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                    </asp:BoundField>--%>
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLPATH" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSaveTab2" runat="server" Text="ส่งรายงาน" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-info" Style="width: 120px" data-toggle="modal"
                                data-target="#ModalConfirmBeforeSaveTab2" />
                            &nbsp;
                            <asp:Button ID="cmdSaveTab2Draft" runat="server" Text="บันทึก (ร่าง)" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-warning" Style="width: 120px" OnClick="cmdSaveTab2Draft_Click" />
                            &nbsp;
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <br />
        </div>
        <div class="tab-pane fade" id="TabFinal">
            <br />
            <div style="width: 100%; color: Green; padding-left: 20px">
                <asp:Label ID="lblSendReportDescTab3" runat="server" Text=""></asp:Label>
            </div>
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblVendorTab3" Text="เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ)" runat="server"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="dgvRequestFileTab3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                <Columns>
                                    <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ" ItemStyle-Width="150px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB" ItemStyle-Width="160px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>
                    <asp:Label ID="lblTab3" runat="server" Text="แนบไฟล์เอกสาร {0}"></asp:Label>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table2" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                เลขที่สัญญา
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboContractTab3" runat="server" class="form-control" DataTextField="SCONTRACTNO"
                                            AutoPostBack="true" Width="350px" DataValueField="SCONTRACTID" OnSelectedIndexChanged="cboContractTab3_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                                ประเภทไฟล์เอกสาร
                                    </asp:TableCell><asp:TableCell>
                                        <asp:DropDownList ID="cboUploadTypeTab3" runat="server" class="form-control" DataTextField="UPLOAD_NAME"
                                            Width="350px" DataValueField="UPLOAD_ID">
                                        </asp:DropDownList>
                                    </asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="35%">
                                        <asp:FileUpload ID="fileUploadTab3" runat="server" Width="100%" />
                                    </asp:TableCell><asp:TableCell>
                                        <asp:Button ID="cmdUploadTab3" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-md btn-hover btn-info"
                                            UseSubmitBehavior="false" Style="width: 100px" OnClick="cmdUploadTab3_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <br />
                            <div style="overflow-x: scroll; width: 100%">
                                <asp:GridView ID="dgvUploadFileTab3" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="White" HorizontalAlign="Center" AutoGenerateColumns="false"
                                    EmptyDataText="[ ไม่มีข้อมูล ]" DataKeyNames="UPLOAD_ID,FULLPATH" ForeColor="#333333"
                                    CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader" OnRowDeleting="dgvUploadFileTab3_RowDeleting"
                                    OnRowUpdating="dgvUploadFileTab3_RowUpdating" OnRowDataBound="dgvUploadFileTab3_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/view1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Update" />&nbsp;
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/bin1.png" Width="25px"
                                                    Height="25px" Style="cursor: pointer" CommandName="Delete" />
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Width="80px" Wrap="False"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="UPLOAD_ID" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา" >
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                    </asp:BoundField>--%>
                                        <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FULLPATH" Visible="false">
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSaveTab3" runat="server" Text="ส่งรายงาน" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-info" Style="width: 120px" data-toggle="modal"
                                data-target="#ModalConfirmBeforeSaveTab3" />
                            &nbsp;
                            <asp:Button ID="cmdSaveTab3Draft" runat="server" Text="บันทึก (ร่าง)" UseSubmitBehavior="false"
                                CssClass="btn btn-md btn-hover btn-warning" Style="width: 120px" OnClick="cmdSaveTab3Draft_Click" />
                            &nbsp;
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <br />
        </div>
        <div class="tab-pane fade" id="TabScore">
            <br />
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>เงื่อนไขรายงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="lblYear" runat="server" Text="ปี (ค.ศ.)"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="lblMonth" runat="server" Text="เดือน"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="lblVendor" runat="server" Text="ผู้ประกอบการขนส่ง"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label18" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlContract" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlContract_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSelect" runat="server" Text="แสดงข้อมูล" CssClass="btn btn-md btn-hover btn-info"
                        Width="120px" OnClick="cmdSelect_Click" />
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>การตัดคะแนน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <%--<a href="ReportMonthly.aspx#TabFinal">Tab 2</a>--%>
                            <%--<a href="ReportMonthly.aspx#TabGeneral">Gallery</a>--%>
                            <asp:Table ID="tblCusScore" runat="server" Width="100%" Visible="false">
                                <asp:TableRow ID="rowReport1_1" runat="server" Visible="false">
                                    <asp:TableCell Width="17%">
                                        <asp:Label ID="lblReport1" runat="server" Text="รายงานประจำเดือน" Font-Underline="true"
                                            ForeColor="Blue"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="100%">
                                        <asp:RadioButtonList ID="radReport1Score" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="&nbsp;ตรวจสอบ (ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="&nbsp;ตรวจสอบ (ไม่ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="-1" Text="&nbsp;รอพิจารณา" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport1_2" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport1IsComplete" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport1_3" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport1Status" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport1_4" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <input id="txtReport1Remark" runat="server" style="width: 100%" class="form-control"
                                            placeholder="หมายเหตุ" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                <asp:TableRow ID="rowReport2_1" runat="server" Visible="false">
                                    <asp:TableCell Width="17%">
                                        <asp:Label ID="lblReport2" runat="server" Text="รายงานประจำ 3 เดือน" Font-Underline="true"
                                            ForeColor="Blue"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="100%">
                                        <asp:RadioButtonList ID="radReport2Score" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="&nbsp;ตรวจสอบ (ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="&nbsp;ตรวจสอบ (ไม่ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="-1" Text="&nbsp;รอพิจารณา" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport2_2" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport2IsComplete" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport2_3" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport2Status" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport2_4" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <input id="txtReport2Remark" runat="server" style="width: 100%" class="form-control"
                                            placeholder="หมายเหตุ" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                <asp:TableRow ID="rowReport3_1" runat="server" Visible="false">
                                    <asp:TableCell Width="17%">
                                        <asp:Label ID="lblReport3" runat="server" Text="รายงานประจำ 6 เดือน" Font-Underline="true"
                                            ForeColor="Blue"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="100%">
                                        <asp:RadioButtonList ID="radReport3Score" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Text="&nbsp;ตรวจสอบ (ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="0" Text="&nbsp;ตรวจสอบ (ไม่ผ่าน)"></asp:ListItem>
                                            <asp:ListItem Value="-1" Text="&nbsp;รอพิจารณา" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport3_2" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport3IsComplete" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport3_3" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblReport3Status" runat="server" Text=""></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow ID="rowReport3_4" runat="server" Visible="false">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <input id="txtReport3Remark" runat="server" style="width: 100%" class="form-control"
                                            placeholder="หมายเหตุ" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                    <ContentTemplate>
                        <div class="panel-footer" style="text-align: right">
                            <asp:Button ID="cmdSaveTab4" runat="server" Text="บันทึก (ยืนยัน)" Enabled="false"
                                UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-info" Style="width: 120px"
                                data-toggle="modal" data-target="#ModalConfirmBeforeSaveTab4" />
                            &nbsp;
                            <asp:Button ID="cmdSaveTab4Draft" runat="server" Text="บันทึก (ร่าง)" Enabled="false"
                                UseSubmitBehavior="false" CssClass="btn btn-md btn-hover btn-warning" Style="width: 120px"
                                OnClick="cmdSaveTab4Draft_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <br />
        </div>
        <div class="tab-pane fade" id="TabReport">
            <br />
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>เงื่อนไขรายงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table4" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label1" runat="server" Text="ปี (ค.ศ.)"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlYearTab5" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlYearTab5_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label2" runat="server" Text="เดือน"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlMonthTab5" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlMonthTab5_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label3" runat="server" Text="ผู้ประกอบการขนส่ง"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlVendorTab5" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorTab5_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label4" runat="server" Text="สถานะ"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSelectTab5" runat="server" Text="แสดงข้อมูล" CssClass="btn btn-md btn-hover btn-info"
                        Width="120px" OnClick="cmdSelectTab5_Click" />
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>สรุปการส่งรายงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:GridView ID="dgvTab5" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                <HeaderStyle HorizontalAlign="Center" />
                                <Columns>
                                    <%--<asp:BoundField DataField="NAME_TH" HeaderText="เดือนปัจจุบัน">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NAME_TH_CURRENT" HeaderText="ข้อมูล เดือน">
                                        </asp:BoundField>--%>
                                    <asp:BoundField DataField="SABBREVIATION" HeaderText="ผู้ประกอบการขนส่ง"></asp:BoundField>
                                    <asp:BoundField DataField="REPORT_1" HeaderText="รายงานประจำเดือน">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REPORT_2" HeaderText="รายงานประจำ 3 เดือน">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REPORT_3" HeaderText="รายงานประจำ 6 เดือน">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Label ID="Label5" runat="server" ForeColor="Green" Text="(O+) = ส่งรายงาน (ผ่าน),"></asp:Label>
                    &nbsp;&nbsp;<asp:Label ID="Label8" runat="server" ForeColor="Red" Text="(O-) = ส่งรายงาน (ไม่ผ่าน),"></asp:Label>
                    &nbsp;&nbsp;<asp:Label ID="Label13" runat="server" ForeColor="Blue" Text="(X)=ไม่ส่งรายงาน,"></asp:Label>
                    &nbsp;&nbsp;<asp:Label ID="Label15" runat="server" ForeColor="Brown" Text="(P) = อยู่ระหว่างการตรวจสอบ,"></asp:Label>
                    &nbsp;&nbsp;<asp:Label ID="Label14" runat="server" ForeColor="Black" Text="ค่าว่าง=ไม่ต้องส่งรายงาน"></asp:Label>
                </div>
            </div>
            <br />
            <br />
        </div>
        <div class="tab-pane fade" id="TabFinalVendor">
            <br />
            <br />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>เงื่อนไขรายงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <asp:Table ID="Table5" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label6" runat="server" Text="ปี (ค.ศ.)"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlYearTab6" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlYearTab6_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="Label7" runat="server" Text="เดือน"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlMonthTab6" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlMonthTab6_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="lblVendorTab6" runat="server" Text="ผู้ประกอบการขนส่ง"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlVendorTab6" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorTab5_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                    <asp:TableCell Width="5%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="15%">
                                        <asp:Label ID="lblStatusTab6" runat="server" Text="สถานะ"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="35%">
                                        <asp:DropDownList ID="ddlStatusTab6" runat="server" CssClass="form-control" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: right">
                    <asp:Button ID="cmdSelectTab6" runat="server" Text="แสดงข้อมูล" CssClass="btn btn-md btn-hover btn-info"
                        Width="120px" OnClick="cmdSelectTab6_Click" />
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-table"></i>สรุปการส่งรายงาน
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div class="panel-body">
                            <div style="overflow-x: scroll; width: 100%">
                                <asp:GridView ID="dgvTab6" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center"
                                    CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                    AlternatingRowStyle BackColor="White" ForeColor="#284775">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="SEND_MONTH" HeaderText="เดือนปัจจุบัน">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DATA_MONTH" HeaderText="ข้อมูล เดือน">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SABBREVIATION" HeaderText="ผู้ประกอบการขนส่ง">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SCONTRACTNO" HeaderText="เลขที่สัญญา">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REPORT_1" HeaderText="ประจำเดือน">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="REPORT_1_DATE" HeaderText="วันที่ส่ง">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REPORT_2" HeaderText="3 เดือน">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REPORT_2_DATE" HeaderText="วันที่ส่ง">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REPORT_3" HeaderText="6 เดือน">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REPORT_3_DATE" HeaderText="วันที่ส่ง">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                            <HeaderStyle Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer" style="text-align: left">
                <asp:Table runat="server" Width="400px">
                    <asp:TableRow>
                        <asp:TableCell style="width:50%">
                            <asp:Label ID="Label9" runat="server" ForeColor="Green" Text="O+ = ส่งรายงาน (ผ่าน)"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell style="width:50%">
                            <asp:Label ID="Label12" runat="server" ForeColor="Red" Text="O- = ส่งรายงาน (ไม่ผ่าน)"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="Label10" runat="server" ForeColor="Blue" Text="X = ไม่ส่งรายงาน"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="Label17" runat="server" ForeColor="Orange" Text="X- = ส่งรายงาน (เอกสารไม่ครบ)"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="Label16" runat="server" ForeColor="Brown" Text="P = อยู่ระหว่างการตรวจสอบ"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="Label11" runat="server" ForeColor="Black" Text="ค่าว่าง = ไม่ต้องส่งรายงาน"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </div>
            </div>
            <br />
            <br />
        </div>
    </div>
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab1" IDModel="ModalConfirmBeforeSaveTab1"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab1_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล รายงานประจำเดือน ใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab2" IDModel="ModalConfirmBeforeSaveTab2"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab2_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล รายงานประจำ 3 เดือน ใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab3" IDModel="ModalConfirmBeforeSaveTab3"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab3_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล รายงานประจำ 6 เดือน ใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpConfirmSaveTab4" IDModel="ModalConfirmBeforeSaveTab4"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSaveTab4_Click"
        TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล การตัดคะแนน ใช่หรือไม่ ?" />
</asp:Content>
