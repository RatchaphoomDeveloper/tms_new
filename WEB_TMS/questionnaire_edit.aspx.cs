﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;

public partial class questionnaire_edit : PageBase
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/questionnaire/{0}/{2}/{1}/";

    #region " Page Prop "
    public string sNTYPEVISITFORMID
    {
        get { return ViewState[this.ToString() + "sNTYPEVISITFORMID"].ToString(); }
        set { ViewState[this.ToString() + "sNTYPEVISITFORMID"] = value; }
    }
    #endregion " Page Prop "
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            sNTYPEVISITFORMID = Session["sNTYPEVISITFORMID"] == null ? "" : Session["sNTYPEVISITFORMID"].ToString();
            if (Session["NQUESTIONNAIREID"] != null)
            {
                QDocID = int.Parse(Session["NQUESTIONNAIREID"].ToString());
            }

            bool chkurl = false;
            Session["CheckPermission"] = null;
            string AddEdit = "";
            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "93")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
            DataRow dr = dtVendor.NewRow();
            dr["SABBREVIATION"] = "- เลือกข้อมูล -";
            dtVendor.Rows.InsertAt(dr, 0);
            cmbVendor.DataSource = dtVendor;
            cmbVendor.DataBind();


            string _err = string.Empty;
            cmbForm.ValueField = "VAL";
            cmbForm.TextField = "FORMNAME";
            DataTable dt = new QuestionnaireBLL().GetFormNameForInput(ref _err);
            DataRow drF = dt.NewRow();
            drF["FORMNAME"] = "- เลือกข้อมูล -";
            dt.Rows.InsertAt(drF, 0);
            cmbForm.DataSource = dt;
            cmbForm.DataBind();

            if (!string.IsNullOrEmpty(sNTYPEVISITFORMID))
            {
                DataRow row = dt.AsEnumerable().Where(r => r.Field<decimal?>("NTYPEVISITFORMID") == decimal.Parse(sNTYPEVISITFORMID.ToString())).FirstOrDefault();
                DevExpress.Web.ASPxEditors.ListEditItem selectedListItem = cmbForm.Items.FindByValue(row["VAL"].ToString());
                if (selectedListItem != null) selectedListItem.Selected = true;
            }

            if (Session["oSSVENDORID"] == null)
            {
                grid.DataBind();
                grid.DetailRows.ExpandAllRows();

                var dt1 = new List<DT1>();
                var dt2 = new List<DT2>();
                var dtFile = new List<dtFile>();

                Session["dtFile"] = dtFile;

                for (int i = 1; i <= 3; i++)
                {
                    dt1.Add(new DT1
                    {
                        dtsID = i,
                        dtsName = "",
                    });
                }

                for (int i = 1; i <= 3; i++)
                {
                    dt2.Add(new DT2
                    {
                        dtsID = i,
                        dtsName = "",
                    });
                }

                sgvw.DataSource = dt1;
                sgvw.DataBind();

                sgvw1.DataSource = dt2;
                sgvw1.DataBind();

                Session["dt1"] = dt1;
                Session["dt2"] = dt2;


            }
            else
            {
                string MaxNo = (Session["oSSVENDORID"] + "").Split(';')[2] + "";
                int num = 0;
                int Max = int.TryParse(MaxNo, out num) ? num : 0;
                for (int i = 1; i <= Max; i++)
                {

                    rblNO.Items.Add(new ListEditItem("ครั้งที่" + i, i));
                }
                if (Max != 0)
                {
                    rblNO.SelectedIndex = Max - 1;
                }
                ListData();

                lbltmp.Visible = false;
            }


            InitialUpload();
        }
    }

    #region " Upload File New "

    private DataTable dtUpload
    {
        get
        {
            if ((DataTable)ViewState["dtUpload"] != null)
                return (DataTable)ViewState["dtUpload"];
            else
                return null;
        }
        set
        {
            ViewState["dtUpload"] = value;
        }
    }

    protected void dgvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtUpload.Rows.RemoveAt(e.RowIndex);
            GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void dgvUploadFile_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();
        this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
    }

    protected void dgvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    protected void cmdUpload_Click(object sender, EventArgs e)
    {
        this.StartUpload();
    }
    private string DocID
    {
        get
        {
            if ((string)ViewState["DocID"] != null)
                return (string)ViewState["DocID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["DocID"] = value;
        }
    }

    private DataTable dtUploadType
    {
        get
        {
            if ((DataTable)ViewState["dtUploadType"] != null)
                return (DataTable)ViewState["dtUploadType"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadType"] = value;
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "Quesrionnaire" + "\\" + DocID;
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialUpload()
    {
        dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL("AUDIT_FILE");
        DropDownListHelper.BindDropDownList(ref cboUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true);

        dtUpload = new DataTable();
        dtUpload.Columns.Add("UPLOAD_ID");
        dtUpload.Columns.Add("UPLOAD_NAME");
        dtUpload.Columns.Add("FILENAME_SYSTEM");
        dtUpload.Columns.Add("FILENAME_USER");
        dtUpload.Columns.Add("FULLPATH");
    }

    private void StartUpload()
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);

            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSaveUpload()
    {
        try
        {
            List<decimal> data = new List<decimal>();
            for (int i = 0; i < dtUpload.Rows.Count; i++)
                data.Add(decimal.Parse(dtUpload.Rows[i]["UPLOAD_ID"].ToString()));

            //DataTable dt = this.GetFilteredData(dtRequestFile, data);
            //if (dt.Rows.Count > 0)
            //{
            //    StringBuilder sb = new StringBuilder();
            //    sb.Append("เอกสารที่ต้องทำการอัพโหลด : ");
            //    sb.Append("\r\n");
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        sb.Append("\t- ");
            //        sb.Append(dt.Rows[i]["UPLOAD_NAME"].ToString());
            //        sb.Append("\r\n");
            //    }
            //    sb.Append("จึงจะทำการบันทึกข้อมูลได้");
            //    sb.Append("\r\n");
            //    sb.Append("ถ้าเอกสารยังไม่ครบ กรุณากดปุ่ม บันทึก(ร่าง) เพื่อบันทึกข้อมูลไว้ก่อน");

            //    throw new Exception(sb.ToString());
            //}
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion " Upload File New "
    void ClearControl()
    {



    }

    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if ("" + Session["CheckPermission"] == "1")
        {
            switch (paras[0])
            {


                case "Save":

                    if (Session["NQUESTIONNAIREID"] != null)
                    {
                        decimal TotalPoint = 0;
                        using (OracleConnection con = new OracleConnection(sql))
                        {
                            if (con.State == ConnectionState.Closed) con.Open();

                            string NQUESTIONNAIREID = Session["NQUESTIONNAIREID"] + "";

                            using (OracleCommand comDel = new OracleCommand("DELETE FROM TQUESTIONNAIRE_BY WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                            {
                                comDel.ExecuteNonQuery();
                            }

                            string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, NID, SNAME,  DCREATE, SCREATE)
VALUES ( :NQUESTIONNAIREID , :NID ,:SNAME , SYSDATE , :SCREATE  )";

                            for (int i = 0; i < sgvw.VisibleRowCount; i++)
                            {

                                using (OracleCommand com1 = new OracleCommand(strsql1, con))
                                {
                                    string GenID1 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_BY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                    com1.Parameters.Add(":NID", OracleType.Number).Value = GenID1;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.ExecuteNonQuery();
                                }
                            }

                            using (OracleCommand comDel1 = new OracleCommand("DELETE FROM TQUESTIONNAIRE_AS WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                            {
                                comDel1.ExecuteNonQuery();
                            }

                            string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS ( NID, NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE) VALUES (:NID ,:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE)";

                            for (int i = 0; i < sgvw1.VisibleRowCount; i++)
                            {

                                using (OracleCommand com1 = new OracleCommand(strsql2, con))
                                {
                                    string GenID2 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_AS ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                    com1.Parameters.Add(":NID", OracleType.Number).Value = GenID2;
                                    com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                    com1.ExecuteNonQuery();
                                }
                            }


                            var listImport = (List<dtFile>)Session["dtFile"];

                            if (listImport != null)
                            {
                                using (OracleCommand comDel2 = new OracleCommand("DELETE FROM TQUESTIONNAIREIMPORTFILE WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                                {
                                    comDel2.ExecuteNonQuery();
                                }

                                string strsql3 = @"INSERT INTO TQUESTIONNAIREIMPORTFILE (NID, NQUESTIONNAIREID, SEVIDENCENAME,SFILENAME, SGENFILENAME, SFILEPATH,  SCREATE, DUPDATE) 
                         VALUES (:NID ,:NQUESTIONNAIREID,:SEVIDENCENAME ,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE )";


                                foreach (var list in listImport)
                                {
                                    string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIREIMPORTFILE ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                    using (OracleCommand com2 = new OracleCommand(strsql3, con))
                                    {
                                        com2.Parameters.Clear();
                                        com2.Parameters.Add(":NID", OracleType.Number).Value = GenIDImport;
                                        com2.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                        com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                                        com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                                        com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                                        com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                                        com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                        com2.ExecuteNonQuery();
                                    }
                                }
                            }

                            using (OracleCommand comDel3 = new OracleCommand("DELETE FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                            {
                                comDel3.ExecuteNonQuery();
                            }

                            string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,
                         NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";

                            for (int i = 0; i < grid.VisibleRowCount; i++)
                            {
                                ASPxGridView detailGrid = grid.FindDetailRowTemplateControl(i, "detailGrid") as ASPxGridView;
                                for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                                {
                                    using (OracleCommand com3 = new OracleCommand(strsql4, con))
                                    {
                                        decimal NVALUE = decimal.Parse(((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum")).Text + "");
                                        decimal NVALUEITEM = decimal.Parse(((ASPxRadioButtonList)detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT")).Value + "");

                                        TotalPoint += (NVALUE * NVALUEITEM); // Recalculate ;

                                        dynamic DATA = detailGrid.GetRowValues(o, "NVISITFORMID", "NGROUPID", "NTYPEVISITFORMID");
                                        com3.Parameters.Clear();
                                        com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                        com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = DATA[0] + "";
                                        com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = DATA[1] + "";
                                        com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = DATA[2] + "";
                                        com3.Parameters.Add(":NVALUE", OracleType.Number).Value = NVALUE;
                                        com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = NVALUEITEM;
                                        com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtRemark")).Text + "";

                                        com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                        com3.ExecuteNonQuery();
                                    }
                                }
                            }

                            string strsql = @"UPDATE TQUESTIONNAIRE SET SVENDORID = :SVENDORID,DCHECK   = :DCHECK,SADDRESS   = :SADDRESS,SREMARK    = :SREMARK,DUPDATE  = SYSDATE,SUPDATE  = :SUPDATE,NVALUE   = :NVALUE WHERE  NQUESTIONNAIREID = :NQUESTIONNAIREID ";
                            using (OracleCommand com = new OracleCommand(strsql, con))
                            {
                                com.Parameters.Clear();
                                com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                                com.Parameters.Add(":DCHECK", OracleType.DateTime).Value = dteDateStart1.Value + "";
                                com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                                com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                                com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com.Parameters.Add(":NVALUE", OracleType.Number).Value = TotalPoint;
                                com.ExecuteNonQuery();
                            }

                        }// using OracleConnection

                        LogUser("28", "E", "แก้ไขข้อมูลหน้า ประเมินสถานประกอบการ", Session["NQUESTIONNAIREID"] + "");
                        CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='questionnaire.aspx';});");
                    }

                    ClearControl();

                    break;


                case "NOChange":
                    ListData();
                    break;

            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            string[] _Filename = e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;

                string genName = "questionnaire" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
                {
                    string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                    e.CallbackData = data;

                    Session["FileUpload"] = e.UploadedFile.FileName + ";" + genName + "." + _Filename[1] + ";" + data;

                }
            }
            else
            {

                return;

            }
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }


    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        Session["ONGROUPID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    [Serializable]
    struct DT1
    {
        public int dtsID { get; set; }
        public string dtsName { get; set; }
    }

    struct DT2
    {
        public int dtsID { get; set; }
        public string dtsName { get; set; }
    }

    struct dtFile
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
    }
    protected void detailGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "#")
        {
            //string keyValue = ((sender) as ASPxGridView).GetMasterRowKeyValue() + "";
            //int VisibleIndex = e.VisibleIndex;
            //ASPxRadioButtonList rblWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "rblWEIGHT") as ASPxRadioButtonList;
            //ASPxTextBox txtWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtWEIGHT") as ASPxTextBox;
            //ASPxTextBox txtOldSum = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtOldSum") as ASPxTextBox;

            //rblWEIGHT.ClientInstanceName = rblWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            //txtWEIGHT.ClientInstanceName = txtWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            //txtOldSum.ClientInstanceName = txtOldSum.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;

            //rblWEIGHT.ClientSideEvents.SelectedIndexChanged = "function (s,e) {var sum = (" + rblWEIGHT.ClientInstanceName + ".GetValue() * " + txtWEIGHT.ClientInstanceName + ".GetValue()) ;txtTotal.SetValue((txtTotal.GetValue() - " + txtOldSum.ClientInstanceName + ".GetValue()) + sum); " + txtOldSum.ClientInstanceName + ".SetValue(sum)}";
        }

        if (e.DataColumn.Caption == "SVISITFORMNAME")
        {
            var lblSVISITFORMNAME = ((sender) as ASPxGridView).FindRowCellTemplateControl(e.VisibleIndex, e.DataColumn, "lblSVISITFORMNAME") as Label;
            if (lblSVISITFORMNAME != null)
            {
                string _str = e.GetValue("SVISITFORMNAME").ToString().Replace("\n", "<br />");
                lblSVISITFORMNAME.Text = _str;
            }
        }


    }

    void ListData()
    {

        using (OracleConnection con = new OracleConnection(sql))
        {
            if (con.State == ConnectionState.Closed) con.Open();
            int NNO = 0;
            if ("" + rblNO.Value == "1")
            {
                NNO = 1;
            }
            else if ("" + rblNO.Value == "2")
            {
                NNO = 2;
            }
            string[] data = (Session["oSSVENDORID"] + "").Split(';');

            DateTime date;
            string YearCheck = (DateTime.TryParse(data[1] + "", out date) ? date : DateTime.Now).Year.ToString();

            DataTable dt = CommonFunction.Get_Data(con, "SELECT q.NQUESTIONNAIREID, q.SVENDORID, q.DCHECK, q.SADDRESS, q.SREMARK, q.NVALUE, VS.SVENDORNAME FROM TQUESTIONNAIRE q LEFT JOIN TVENDOR_SAP vs ON Q.SVENDORID = VS.SVENDORID WHERE q.SVENDORID = '" + data[0] + "' AND To_Char(q.DCHECK,'YYYY') = '" + YearCheck + "' AND q.NNO = " + NNO);

            if (dt.Rows.Count > 0)
            {

                DataTable dt1 = CommonFunction.Get_Data(con, "SELECT NID, SNAME FROM TQUESTIONNAIRE_BY WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);
                DataTable dt2 = CommonFunction.Get_Data(con, "SELECT NID, SNAME FROM TQUESTIONNAIRE_AS WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);
                DataTable dt3 = CommonFunction.Get_Data(con, "SELECT NID, SEVIDENCENAME, SFILENAME, SGENFILENAME, SFILEPATH FROM TQUESTIONNAIREIMPORTFILE WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);
                DataTable dt4 = CommonFunction.Get_Data(con, "SELECT NVISITFORMID, NGROUPID, NTYPEVISITFORMID, NVALUE, SREMARK,NVALUEITEM FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = " + dt.Rows[0]["NQUESTIONNAIREID"]);

                Session["NQUESTIONNAIREID"] = dt.Rows[0]["NQUESTIONNAIREID"] + "";
                txtVendor.Text = dt.Rows[0]["SVENDORNAME"] + "";
                txtVendorID.Text = dt.Rows[0]["SVENDORID"] + "";
                dteDateStart1.Value = DateTime.TryParse(dt.Rows[0]["DCHECK"] + "", out date) ? date : DateTime.Now;
                txtAddress.Value = dt.Rows[0]["SADDRESS"] + "";
                txtDetail.Text = dt.Rows[0]["SREMARK"] + "";
                txtTotal.Text = dt.Rows[0]["NVALUE"] + "";

                decimal TotalPoint = 0;
                if (dt1.Rows.Count > 0)
                {
                    List<DT1> data1 = new List<DT1>(dt1.Rows.Count);

                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        data1.Add(new DT1
                        {
                            dtsID = Convert.ToInt32(dr1["NID"] + ""),
                            dtsName = dr1["SNAME"] + ""
                        });
                    }
                    sgvw.DataSource = data1;
                    sgvw.DataBind();

                    Session["dt1"] = data1;
                }

                if (dt2.Rows.Count > 0)
                {
                    List<DT2> data2 = new List<DT2>(dt2.Rows.Count);

                    foreach (DataRow dr1 in dt2.Rows)
                    {
                        data2.Add(new DT2
                        {
                            dtsID = Convert.ToInt32(dr1["NID"] + ""),
                            dtsName = dr1["SNAME"] + ""
                        });
                    }

                    sgvw1.DataSource = data2;
                    sgvw1.DataBind();

                    Session["dt2"] = data2;
                }

                if (dt3.Rows.Count > 0)
                {
                    List<dtFile> data3 = new List<dtFile>(dt3.Rows.Count);

                    foreach (DataRow dr1 in dt3.Rows)
                    {
                        data3.Add(new dtFile
                        {
                            dtID = Convert.ToInt32(dr1["NID"] + ""),
                            dtEvidenceName = dr1["SEVIDENCENAME"] + "",
                            dtFileName = dr1["SFILENAME"] + "",
                            dtFilePath = dr1["SFILEPATH"] + "",
                            dtGenFileName = dr1["SGENFILENAME"] + ""
                        });
                    }

                    sgvwFile.DataSource = data3;
                    sgvwFile.DataBind();
                    Session["dtFile"] = data3;
                }

                if (dt4.Rows.Count > 0)
                {
                    Session["sNTYPEVISITFORMID"] = dt4.Rows[0]["NTYPEVISITFORMID"] + "";
                    sdsMaster.DataBind();
                    grid.DataBind();
                    grid.DetailRows.ExpandAllRows();

                    for (int i = 0; i < grid.VisibleRowCount; i++)
                    {
                        string NGROUPID = grid.GetRowValues(i, "NGROUPID") + "";
                        DataRow[] dr = dt4.Select("NGROUPID =" + NGROUPID);
                        if (dr.Count() > 0)
                        {
                            ASPxGridView detailGrid = (ASPxGridView)grid.FindDetailRowTemplateControl(i, "detailGrid");
                            for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                            {
                                string NVISITFORMID = detailGrid.GetRowValues(o, "NVISITFORMID") + "";
                                DataRow[] drr = dt4.Select("NGROUPID =" + NGROUPID + "and NVISITFORMID=" + NVISITFORMID);

                                if (drr.Count() > 0)
                                {
                                    ASPxRadioButtonList rblWEIGHT = detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT") as ASPxRadioButtonList;
                                    ASPxTextBox txtOldSum = detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum") as ASPxTextBox;
                                    ASPxTextBox txtRemark = detailGrid.FindRowCellTemplateControl(o, null, "txtRemark") as ASPxTextBox;

                                    rblWEIGHT.Value = drr[0]["NVALUEITEM"] + "";
                                    txtOldSum.Text = drr[0]["NVALUE"] + "";
                                    txtRemark.Text = drr[0]["SREMARK"] + "";

                                    /// Add by ORN.K Recalculate Point
                                    TotalPoint += drr[0]["NVALUE"] == null ? 0 : decimal.Parse(drr[0]["NVALUE"].ToString()) * decimal.Parse(drr[0]["NVALUEITEM"].ToString());
                                }
                            }
                        }
                    }
                }

                txtTotal.Text = TotalPoint.ToString();
            }
            else
            {

            }
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    protected void sgvwFile_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] data = e.Args;
        string[] Para = data[0].Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (Para[0])
            {
                case "Upload":
                    string dtr = txtEvidence.Text;
                    var addData = (List<dtFile>)Session["dtFile"];
                    int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                    var fileupload = (Session["FileUpload"] + "").Split(';');

                    if (fileupload.Count() == 3)
                    {
                        addData.Add(new dtFile
                        {
                            dtID = max,
                            dtEvidenceName = txtEvidence.Text,
                            dtFileName = fileupload[0],
                            dtGenFileName = fileupload[1],
                            dtFilePath = fileupload[2]
                        });

                        Session["dtFile"] = addData;
                        sgvwFile.DataSource = addData;
                        sgvwFile.DataBind();
                    }
                    break;

                case "DeleteFileList":

                    var deldt = (List<dtFile>)Session["dtFile"];
                    int index1 = Convert.ToInt32(Para[1]);
                    string FilePath1 = sgvwFile.GetRowValues(index1, "dtFilePath") + "";

                    if (File.Exists(Server.MapPath("./") + FilePath1.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath1.Replace("/", "\\"));
                    }

                    deldt.RemoveAt(index1);

                    sgvwFile.DataSource = deldt;
                    sgvwFile.DataBind();
                    Session["dtFile"] = deldt;

                    break;

                case "ViewFileList":

                    int index = Convert.ToInt32(Para[1]);
                    string FilePath = sgvwFile.GetRowValues(index, "dtFilePath") + "";

                    sgvwFile.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + FilePath;

                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
    protected void sgvw1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] data = e.Args;
        string[] Para = data[0].Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (Para[0])
            {
                case "AddClick1":
                    var sdt2 = (List<DT2>)Session["dt2"];

                    if (sdt2.Count > 0)
                    {
                        int id = sdt2.Count;
                        sdt2.Clear();

                        int indexgvw = sgvw1.VisibleRowCount - 1;

                        for (int i = 0; i <= indexgvw; i++)
                        {
                            sdt2.Add(new DT2
                            {
                                dtsID = Convert.ToInt32(sgvw1.GetRowValues(i, "dtsID")),
                                dtsName = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "",
                            });
                        }
                    }

                    int maxQ2 = (sdt2.Count > 0) ? sdt2.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                    sdt2.Add(new DT2
                    {
                        dtsID = maxQ2,
                        dtsName = "",
                    });

                    sgvw1.DataSource = sdt2;
                    sgvw1.DataBind();

                    Session["dt2"] = sdt2;
                    break;

                case "deleteList1":

                    if (sgvw1.VisibleRowCount > 1)
                    {
                        var delsdt2 = (List<DT2>)Session["dt2"];
                        delsdt2.RemoveAt(Convert.ToInt32(Para[1]));

                        sgvw1.DataSource = delsdt2;
                        sgvw1.DataBind();

                        Session["dt2"] = delsdt2;
                    }

                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
    protected void sgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        try
        {
            string[] data = e.Args;
            string[] Para = data[0].Split(';');

            if ("" + Session["CheckPermission"] == "1")
            {
                switch (Para[0])
                {
                    case "AddClick":
                        var sdt1 = (List<DT1>)Session["dt1"];

                        if (sdt1.Count > 0)
                        {
                            int id = sdt1.Count;
                            sdt1.Clear();

                            int indexgvw = sgvw.VisibleRowCount - 1;

                            for (int i = 0; i <= indexgvw; i++)
                            {
                                sdt1.Add(new DT1
                                {
                                    dtsID = Convert.ToInt32(sgvw.GetRowValues(i, "dtsID")),
                                    dtsName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "",
                                });
                            }
                        }

                        int maxQ = (sdt1.Count > 0) ? sdt1.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                        sdt1.Add(new DT1
                        {
                            dtsID = maxQ,
                            dtsName = "",
                        });

                        sgvw.DataSource = sdt1;
                        sgvw.DataBind();

                        Session["dt1"] = sdt1;
                        break;

                    case "deleteList":

                        if (sgvw.VisibleRowCount > 1)
                        {
                            var delsdt1 = (List<DT1>)Session["dt1"];
                            delsdt1.RemoveAt(Convert.ToInt32(Para[1]));

                            sgvw.DataSource = delsdt1;
                            sgvw.DataBind();

                            Session["dt1"] = delsdt1;
                        }
                        break;
                }

            }
            else
            {
                CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
            }
        }
        catch (Exception)
        {

            throw;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if ("" + Session["CheckPermission"] == "1")
        {
            if (QDocID.HasValue) //Edit
            {
                #region " Edit "

                //QuestionnaireBLL.Instance.AttachInsertBLL(dtUpload, DocID, int.Parse(Session["UserID"].ToString()), "COMPLAIN");

                decimal TotalPoint = 0;
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();

                    string NQUESTIONNAIREID = Session["NQUESTIONNAIREID"] + "";

                    using (OracleCommand comDel = new OracleCommand("DELETE FROM TQUESTIONNAIRE_BY WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                    {
                        comDel.ExecuteNonQuery();
                    }

                    string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, NID, SNAME,  DCREATE, SCREATE)
VALUES ( :NQUESTIONNAIREID , :NID ,:SNAME , SYSDATE , :SCREATE  )";

                    for (int i = 0; i < sgvw.VisibleRowCount; i++)
                    {

                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {
                            string GenID1 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_BY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                            com1.Parameters.Add(":NID", OracleType.Number).Value = GenID1;
                            com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "";
                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com1.ExecuteNonQuery();
                        }
                    }

                    using (OracleCommand comDel1 = new OracleCommand("DELETE FROM TQUESTIONNAIRE_AS WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                    {
                        comDel1.ExecuteNonQuery();
                    }

                    string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS ( NID, NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE) VALUES (:NID ,:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE)";

                    for (int i = 0; i < sgvw1.VisibleRowCount; i++)
                    {

                        using (OracleCommand com1 = new OracleCommand(strsql2, con))
                        {
                            string GenID2 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_AS ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                            com1.Parameters.Add(":NID", OracleType.Number).Value = GenID2;
                            com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "";
                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com1.ExecuteNonQuery();
                        }
                    }


                    var listImport = (List<dtFile>)Session["dtFile"];

                    if (listImport != null)
                    {
                        using (OracleCommand comDel2 = new OracleCommand("DELETE FROM TQUESTIONNAIREIMPORTFILE WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                        {
                            comDel2.ExecuteNonQuery();
                        }

                        string strsql3 = @"INSERT INTO TQUESTIONNAIREIMPORTFILE (NID, NQUESTIONNAIREID, SEVIDENCENAME,SFILENAME, SGENFILENAME, SFILEPATH,  SCREATE, DUPDATE) 
                         VALUES (:NID ,:NQUESTIONNAIREID,:SEVIDENCENAME ,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE )";


                        foreach (var list in listImport)
                        {
                            string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIREIMPORTFILE ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            using (OracleCommand com2 = new OracleCommand(strsql3, con))
                            {
                                com2.Parameters.Clear();
                                com2.Parameters.Add(":NID", OracleType.Number).Value = GenIDImport;
                                com2.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                                com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                                com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                                com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                                com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                com2.ExecuteNonQuery();
                            }
                        }
                    }

                    using (OracleCommand comDel3 = new OracleCommand("DELETE FROM TQUESTIONNAIRELIST WHERE NQUESTIONNAIREID = " + NQUESTIONNAIREID, con))
                    {
                        comDel3.ExecuteNonQuery();
                    }

                    string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,
                         NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";

                    for (int i = 0; i < grid.VisibleRowCount; i++)
                    {
                        ASPxGridView detailGrid = grid.FindDetailRowTemplateControl(i, "detailGrid") as ASPxGridView;
                        for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                        {
                            using (OracleCommand com3 = new OracleCommand(strsql4, con))
                            {
                                decimal NVALUE = decimal.Parse(((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum")).Text + "");
                                decimal NVALUEITEM = decimal.Parse(((ASPxRadioButtonList)detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT")).Value + "");

                                TotalPoint += (NVALUE * NVALUEITEM); // Recalculate ;

                                dynamic DATA = detailGrid.GetRowValues(o, "NVISITFORMID", "NGROUPID", "NTYPEVISITFORMID");
                                com3.Parameters.Clear();
                                com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                                com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = DATA[0] + "";
                                com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = DATA[1] + "";
                                com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = DATA[2] + "";
                                com3.Parameters.Add(":NVALUE", OracleType.Number).Value = NVALUE;
                                com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = NVALUEITEM;
                                com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtRemark")).Text + "";

                                com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                com3.ExecuteNonQuery();
                            }
                        }
                    }

                    string strsql = @"UPDATE TQUESTIONNAIRE SET SVENDORID = :SVENDORID,DCHECK   = :DCHECK,SADDRESS   = :SADDRESS,SREMARK    = :SREMARK,DUPDATE  = SYSDATE,SUPDATE  = :SUPDATE,NVALUE   = :NVALUE WHERE  NQUESTIONNAIREID = :NQUESTIONNAIREID ";
                    using (OracleCommand com = new OracleCommand(strsql, con))
                    {
                        com.Parameters.Clear();
                        com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = NQUESTIONNAIREID;
                        com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = txtVendorID.Text;
                        com.Parameters.Add(":DCHECK", OracleType.DateTime).Value = dteDateStart1.Value + "";
                        com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                        com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                        com.Parameters.Add(":NVALUE", OracleType.Number).Value = TotalPoint;
                        com.ExecuteNonQuery();
                    }

                }

                LogUser("28", "E", "แก้ไขข้อมูลหน้า ประเมินสถานประกอบการ", Session["NQUESTIONNAIREID"] + "");
                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='questionnaire.aspx';});");
                #endregion " Edit "
            }
            else
            {
                #region " Add "
                string GenID;
                using (OracleConnection con = new OracleConnection(sql))
                {
                    if (con.State == ConnectionState.Closed) con.Open();
                    DateTime dd;
                    string strsql = @"INSERT INTO TQUESTIONNAIRE (NQUESTIONNAIREID, SVENDORID, DCHECK, SADDRESS, SREMARK, DCREATE, SCREATE, DUPDATE, SUPDATE,NVALUE,NNO) VALUES (:NQUESTIONNAIREID,:SVENDORID,:DCHECK,:SADDRESS,:SREMARK,SYSDATE,:SCREATE,SYSDATE,:SUPDATE, :NVALUE, :NNO)";
                    GenID = CommonFunction.Gen_ID(con, "SELECT NQUESTIONNAIREID FROM (SELECT NQUESTIONNAIREID FROM TQUESTIONNAIRE ORDER BY NQUESTIONNAIREID DESC) WHERE ROWNUM <= 1");
                    string GenNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TQUESTIONNAIRE WHERE To_Char(DCHECK,'YYYY') = '" + (DateTime.TryParse(dteDateStart1.Value + "", out dd) ? dd : DateTime.Today).Year);// + "'  AND SVENDORID = '" + cboVendor.Value + "' ORDER BY NNO DESC) WHERE ROWNUM <= 1");


                    if (Convert.ToInt16(GenNO) > 2)
                    {
                        CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ระบบอนุญาตให้ทำการประเมินสถานประกอบการได้ ปีละไม่เกิน 2 ครั้ง')");
                        return;
                    }

                    using (OracleCommand com = new OracleCommand(strsql, con))
                    {

                        com.Parameters.Clear();
                        com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                        //com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                        com.Parameters.Add(":DCHECK", OracleType.DateTime).Value = dteDateStart1.Value + "";
                        com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                        com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                        com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                        com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                        com.Parameters.Add(":NVALUE", OracleType.Number).Value = txtTotal.Text;
                        com.Parameters.Add(":NNO", OracleType.Number).Value = GenNO;
                        com.ExecuteNonQuery();
                    }

                    string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, NID, SNAME,  DCREATE, SCREATE)
VALUES ( :NQUESTIONNAIREID , :NID ,:SNAME , SYSDATE , :SCREATE  )";

                    for (int i = 0; i < sgvw.VisibleRowCount; i++)
                    {

                        using (OracleCommand com1 = new OracleCommand(strsql1, con))
                        {
                            string GenID1 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_BY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                            com1.Parameters.Add(":NID", OracleType.Number).Value = GenID1;
                            com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "";
                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com1.ExecuteNonQuery();
                        }
                    }

                    string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS ( NID, NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE) VALUES (:NID ,:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE)";

                    for (int i = 0; i < sgvw1.VisibleRowCount; i++)
                    {

                        using (OracleCommand com1 = new OracleCommand(strsql2, con))
                        {
                            string GenID2 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_AS ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                            com1.Parameters.Add(":NID", OracleType.Number).Value = GenID2;
                            com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "";
                            com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com1.ExecuteNonQuery();
                        }
                    }

                    string strsql3 = @"INSERT INTO TQUESTIONNAIREIMPORTFILE (NID, NQUESTIONNAIREID, SEVIDENCENAME,SFILENAME, SGENFILENAME, SFILEPATH,  SCREATE, DUPDATE) 
                         VALUES (:NID ,:NQUESTIONNAIREID,:SEVIDENCENAME ,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE )";

                    var listImport = (List<dtFile>)Session["dtFile"];

                    foreach (var list in listImport)
                    {
                        string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIREIMPORTFILE ORDER BY NID DESC) WHERE ROWNUM <= 1");
                        using (OracleCommand com2 = new OracleCommand(strsql3, con))
                        {
                            com2.Parameters.Clear();
                            com2.Parameters.Add(":NID", OracleType.Number).Value = GenIDImport;
                            com2.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                            com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                            com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                            com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                            com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                            com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                            com2.ExecuteNonQuery();
                        }
                    }

                    string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,
                         NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";

                    for (int i = 0; i < grid.VisibleRowCount; i++)
                    {
                        ASPxGridView detailGrid = grid.FindDetailRowTemplateControl(i, "detailGrid") as ASPxGridView;
                        for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                        {
                            using (OracleCommand com3 = new OracleCommand(strsql4, con))
                            {
                                dynamic DATA = detailGrid.GetRowValues(o, "NVISITFORMID", "NGROUPID", "NTYPEVISITFORMID");
                                com3.Parameters.Clear();
                                com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                                com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = DATA[0] + "";
                                com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = DATA[1] + "";
                                com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = DATA[2] + "";
                                com3.Parameters.Add(":NVALUE", OracleType.Number).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum")).Text + "";
                                com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtRemark")).Text + "";
                                com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = ((ASPxRadioButtonList)detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT")).Value + "";
                                com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                com3.ExecuteNonQuery();
                            }
                        }
                    }
                }

                LogUser("28", "I", "บันทึกข้อมูลหน้า ประเมินสถานประกอบการ", GenID + "");

                CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='questionnaire.aspx';});");
                #endregion " Add "
            }

            ClearControl();
        }
    }

    /// <summary>
    /// NQUESTIONNAIREID
    /// </summary>
    private int? QDocID
    {
        get
        {
            return int.Parse(ViewState["QDocID"].ToString());
        }
        set
        {
            ViewState["QDocID"] = value;
        }
    }
    protected void btnClose_Click(object sender, EventArgs erblWEIGHT_SelectedIndexChanged)
    {

    }
    protected void rblWEIGHT_SelectedIndexChanged(object sender, EventArgs e)
    {
        ASPxRadioButtonList rdo = (ASPxRadioButtonList)sender;


        //for (int i = 0; i < grid.VisibleRowCount; i++)
        //{
        //    //Do something here
        //}
    }
    protected void cmbForm_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["CheckPermission"] = null;
        string AddEdit = "";
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == "93")
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            chkurl = true;

                            break;

                        case "2":
                            chkurl = true;
                            AddEdit = "1";
                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        if (chkurl == false)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }

        Session["CheckPermission"] = AddEdit;

        var dt1 = new List<DT1>();
        var dt2 = new List<DT2>();
        var dtFile = new List<dtFile>();
        Session["dtFile"] = dtFile;

        for (int i = 1; i <= 3; i++)
        {
            dt1.Add(new DT1
            {
                dtsID = i,
                dtsName = "",
            });
        }

        for (int i = 1; i <= 3; i++)
        {
            dt2.Add(new DT2
            {
                dtsID = i,
                dtsName = "",
            });
        }

        sgvw.DataSource = dt1;
        sgvw.DataBind();

        sgvw1.DataSource = dt2;
        sgvw1.DataBind();

        Session["dt1"] = dt1;
        Session["dt2"] = dt2;
        string _err = string.Empty;
        Session["sNTYPEVISITFORMID"] = cmbForm.SelectedItem.Value.ToString().Split(':').Last().Trim().ToString();
        //DataTable dt = new QuestionnaireBLL().GetQuestionnaire(ref _err, int.Parse(Session["sNTYPEVISITFORMID"].ToString()));
        //grid.DataSource = dt;
        grid.DataBind();
        grid.DetailRows.ExpandAllRows();

        uplMain.Update();
    }
    protected void detailGrid_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        ASPxGridView grv = (ASPxGridView)sender;
        try
        {
            if (e.CommandArgs.CommandName == "ChangeScore")
            {
                var Id = grv.GetRowValuesByKeyValue(e.KeyValue, "ID");
            }
        }
        catch (Exception ex)
        {

            alertFail(ex.Message);
        }
    }
}