﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data;
using DevExpress.Web.ASPxEditors;
using System.Data.OracleClient;
using TMS_BLL.Master;
using GemBox.Spreadsheet;
using System.Text;
using System.Drawing;
using System.IO;
using System.Web.Security;
using DevExpress.Web.ASPxGridView;

public partial class vendor_employee : PageBase
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        string rawURL = HttpContext.Current.Request.ServerVariables["query_string"];
        string paramBl = HttpUtility.ParseQueryString(HttpContext.Current.Request.RawUrl).Get("bl");
        gvw.HtmlRowPrepared += new DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventHandler(gvw_HtmlRowPrepared);
        gvw.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(gvw_HtmlDataCellPrepared);
        
        if (!IsPostBack)
        {            
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
            cboVendor.DataSource = ViewState["DataVendor"];
            cboVendor.DataBind();
            chkAllow.Checked = true;
            chkDisabled.Checked = true;
            chkDisallow.Checked = true;
            if (Session["CGROUP"] + string.Empty == "0")
            {
                cboVendor.Enabled = false;
                cboVendor.Value = Session["SVDID"] + string.Empty;

                //cblstatus.Value = "1";
                //cblstatus.Enabled = false;

                radStatusDriver.SelectedValue = "1";
                radStatusDriver.Enabled = false;

                imbadd.Visible = false;
                
                gvw.Columns[11].Visible = false;             
                gvw.AllColumns[12].SetColVisible(false);
                if (Request.QueryString["bl"] != null && Request.QueryString["bl"] == "bl")
                {
                    chkAllow.Checked = false;
                    chkAllow.Enabled = false;
                    chkDisallow.Checked = true;
                    chkDisallow.Enabled = false;
                    chkDisabled.Checked = true;
                    chkDisabled.Enabled = false;
                    gvw.AllColumns[5].Caption = "วันที่เริ่มระงับ<br>ใน SAP";
                    gvw.AllColumns[7].Caption = "วันที่เริ่มระงับ<br>ใน TMS";
                }
            }
            else if (Request.QueryString["bl"] != null && Request.QueryString["bl"] == "bl")
            {
                //cblstatus.Value = "1";
                //cblstatus.Enabled = false;

                radStatusDriver.SelectedValue = "1";
                radStatusDriver.Enabled = false;

                imbadd.Visible = false;
                chkAllow.Checked = false;
                chkAllow.Enabled = false;
                chkDisallow.Checked = true;
                chkDisallow.Enabled = false;
                chkDisabled.Checked = true;
                chkDisabled.Enabled = false;
                gvw.AllColumns[5].Caption = "วันที่เริ่มระงับ<br>ใน SAP";
                gvw.AllColumns[7].Caption = "วันที่เริ่มระงับ<br>ใน TMS";
                gvw.AllColumns[12].SetColVisible(false);
                gvw.AllColumns[13].SetColVisible(false);
            }
            else if (Request.QueryString["bl"] != null && Request.QueryString["bl"] == "blEdit")
            {
                //cblstatus.Value = "1";
                //cblstatus.Enabled = false;

                radStatusDriver.SelectedValue = "1";
                radStatusDriver.Enabled = false;

                imbadd.Visible = false;
                //chkAllow.Checked = false;
                //chkAllow.Enabled = false;
                //chkDisallow.Checked = true;
                //chkDisallow.Enabled = false;
                //chkDisabled.Checked = true;
                //chkDisabled.Enabled = false;
                gvw.AllColumns[5].Caption = "วันที่เริ่มระงับ<br>ใน SAP";
                gvw.AllColumns[7].Caption = "วันที่เริ่มระงับ<br>ใน TMS";
                //gvw.AllColumns[12].SetColVisible(false);
                gvw.AllColumns[13].SetColVisible(false);
            }
            this.AssignAuthen();

            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
        }
        else
        {
            cboVendor.DataSource = ViewState["DataVendor"];
            cboVendor.DataBind();
        }
        //if (chkOver.Checked == true)
        //{
        //    txtdateover.Enabled = true;
        //    txtdateover.ValidationSettings.RequiredField.IsRequired = false;
        //}
        //else
        //{
        //    txtdateover.Enabled = false;
        //    txtdateover.ValidationSettings.RequiredField.IsRequired = true;
        //}
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                gvw.Columns[11].Visible = false;
            }
            if (!CanWrite)
            {
                imbadd.Enabled = false;
                imbadd.Visible = true;
                gvw.Columns[12].Visible = false;
            }
           
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    void gvw_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {       
        string INUSE = e.GetValue("INUSE") + "";
        ASPxButton btn = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "btn") as ASPxButton;
        if (INUSE == "1")
        {
            e.Row.BackColor = System.Drawing.Color.White;
            // chk.Checked = true;
            try
            {
                btn.Image.Url = "Images/k2.png";
            }
            catch
            {
            }
        }
        else
        {
            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#f9d0b5");
            try
            {
                btn.Image.Url = "Images/x2.png";
            }
            catch
            {

            }
            //chk.Checked = false;
        }
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
        if (Session["CGROUP"] + string.Empty == "0" && imbedit != null)
        {
            imbedit.Text = "แก้ไข";
        }


        ASPxButton imbeditStatus = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbeditStatus") as ASPxButton;
        if (!CanWrite) {
           // imbeditStatus.Enabled = false;
        }
    }

    void gvw_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        ASPxButton imbedit = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbedit") as ASPxButton;
        ASPxButton imbeditStatus = gvw.FindRowCellTemplateControl(e.VisibleIndex, null, "imbeditStatus") as ASPxButton;
        if (!CanWrite)
        {
            if (imbedit != null) { imbedit.Enabled = false; }
            if (imbeditStatus != null) { imbeditStatus.Enabled = false; }
        }
    }
    protected void xcpn_Load(object sender, EventArgs e)
    {
               
        Listdata();
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_Endsession2 + "',function(){window.location='admin_user_lst.aspx';});");

        }
        else
        {
            byte[] plaintextBytes;
            string encryptedValue;
            string[] param = e.Parameter.Split(';');
            switch (param[0])
            {
                case "Search": Listdata();
                    xcpn.JSProperties["cpSetjava"] = "2";
                    break;
                case "Claer": cboVendor.Value = null;
                    if (Session["CGROUP"] + string.Empty == "0")
                    {
                        cboVendor.Value = Session["SVDID"] + string.Empty;
                    }
                    //cblstatus.SelectedIndex = 0;

                    radStatusDriver.SelectedIndex = 0;

                    dedtStart.Text = string.Empty;
                    dedtEnd.Text = string.Empty;
                    chkAllow.Checked = true;
                    chkDisallow.Checked = true;
                    chkDisabled.Checked = true;
                    txtSearch.Text = string.Empty;
                    break;
                case "CancelSearch":
                    txtSearch.Text = "";
                    cboVendor.Text = "";
                    chkAllow.Checked = true;
                    chkDisallow.Checked = true;
                    chkDisabled.Checked = true;
                    //chkOver.Checked = true;
                    dedtStart.Text = "";
                    dedtEnd.Text = "";
                    //dedtStart2.Text = "";
                    //dedtEnd2.Text = "";
                    //txtdateover.Text = "";
                    //cblstatus.Value = "";
                    Listdata();
                    break;

                case "edit":
                    dynamic data = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID");
                     plaintextBytes = Encoding.UTF8.GetBytes(data + string.Empty);
                 encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                 xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?str=" + encryptedValue + "&bl=" + Request.QueryString["bl"];
                    break;
                case "editStatus":
                    dynamic dataStatus = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID");
                    plaintextBytes = Encoding.UTF8.GetBytes(dataStatus + string.Empty);
                 encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                 xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?editStatus=" + encryptedValue + "&bl=" + Request.QueryString["bl"];
                    break;
                case "add":
                    xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx" + "?bl=" + Request.QueryString["bl"];
                    break;
                case "searchDoc":
                     dynamic data2 = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID");
                     plaintextBytes = Encoding.UTF8.GetBytes(data2 + string.Empty);
                     encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                     xcpn.JSProperties["cpRedirectTo"] = "EmployeeExpire.aspx?str=" + encryptedValue + "&bl=" + Request.QueryString["bl"];
                    // Go to new doc page
                     break;
                case "view":
                    dynamic view = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID");
                    plaintextBytes = Encoding.UTF8.GetBytes(view + string.Empty);
                 encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                 xcpn.JSProperties["cpRedirectTo"] = "vendor_employee_add1.aspx?str=" + encryptedValue + "&mode=view" + "&bl=" + Request.QueryString["bl"];
                    break;

                case "ChkInUse":

                    dynamic SEMPLOYEEID = gvw.GetRowValues(int.Parse(e.Parameter.Split(';')[1]), "SEMPLOYEEID");


                    string strCheckdata = @"SELECT * FROM TEMPLOYEE WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + @"' AND INUSE ='1' ";
                    string strCompany = "";
                    DataTable dt = CommonFunction.Get_Data(conn, strCheckdata);
                    if (dt.Rows.Count > 0)
                    {
                        strCompany = @"UPDATE TEMPLOYEE
                                         SET INUSE='0'
                                         WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";
                    }
                    else
                    {
                        strCompany = @"UPDATE TEMPLOYEE
                                         SET INUSE='1'
                                         WHERE SEMPLOYEEID = '" + CommonFunction.ReplaceInjection(SEMPLOYEEID) + "'";
                    }

                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }

                        using (OracleCommand com1 = new OracleCommand(strCompany, con))
                        {
                            com1.ExecuteNonQuery();
                        }

                    }


                    Listdata();
                    break;
            }

        }
    }

    private DataTable GetData()
    {
        string Condition = "";

        if (!string.IsNullOrEmpty(dedtStart.Text) && !string.IsNullOrEmpty(dedtEnd.Text))
        {

            Condition = " AND TO_DATE(EMPSAP.DUPDATE, 'DD/MM/YYYY') >= TO_DATE( '" + dedtStart.Text + "','DD/MM/YYYY') AND  TO_DATE(EMPSAP.DUPDATE, 'DD/MM/YYYY')  <= TO_DATE( '" + dedtEnd.Text + "','DD/MM/YYYY') ";
        }

        if (radSpot.SelectedIndex > -1)
        {
            Condition += " AND NVL(EMP.SPOT_STATUS, 0) = " + radSpot.SelectedValue;
        }

        //if (!string.IsNullOrEmpty(dedtStart2.Text) && !string.IsNullOrEmpty(dedtEnd2.Text))
        //{
        //    Condition = " AND TO_DATE(EMP.DUPDATE , 'DD/MM/YYYY') >= TO_DATE( '" + dedtStart2.Date.Day + "/" + dedtStart2.Date.Month + "/" + dedtStart2.Date.Year + "','DD/MM/YYYY') AND  TO_DATE(EMP.DUPDATE , 'DD/MM/YYYY')  <= TO_DATE( '" + dedtEnd2.Date.Day + "/" + dedtEnd2.Date.Month + "/" + dedtEnd2.Date.Year + "','DD/MM/YYYY') ";
        //}

        if (!string.IsNullOrEmpty(cboVendor.Text))
        {
            Condition += " AND EMP.STRANS_ID = '" + cboVendor.Value + "'";
        }
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            Condition += " AND EMPSAP.SEMPLOYEEID||' '||EMPSAP.FNAME||' '||EMPSAP.LNAME||' '||EMPSAP.PERS_CODE LIKE '%" + CommonFunction.ReplaceInjection(txtSearch.Text.Trim()) + "%'";
        }

        if (chkAllow.Checked == true || chkDisallow.Checked == true || chkDisabled.Checked == true)
        {
            string checkAllow = "";
            string checkDisallow = "";
            string checkDisabled = "", checkCactive9 = string.Empty;
            //string checkOver = "";
            if (chkAllow.Checked == true)
            {
                checkAllow = "1";
            }
            if (chkDisallow.Checked == true)
            {
                checkDisallow = "0";
            }
            if (chkDisabled.Checked == true)
            {
                checkDisabled = "2";
            }
            //if (chkCactive9.Checked)
            //{
            //    checkCactive9 = "9";
            //}
            //if (chkOver.Checked == true)
            //{
            //    checkOver = "3";
            //}

            //Condition += "AND EMP.CACTIVE in('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "' ,'" + CommonFunction.ReplaceInjection(checkOver) + "')";

            Condition += " AND EMP.CACTIVE in('" + CommonFunction.ReplaceInjection(checkAllow) + "','" + CommonFunction.ReplaceInjection(checkDisallow) + "' ,'" + CommonFunction.ReplaceInjection(checkDisabled) + "','" + CommonFunction.ReplaceInjection(checkCactive9) + "')";
        }

        //if (chkOver.Checked == true)
        //{
        //    if (!string.IsNullOrEmpty(txtdateover.Text))
        //    {
        //        Condition += " AND (SELECT TRUNC(sysdate) - TRUNC(TO_DATE(EMP.DATE_LASTJOB)) AS DateLast FROM Dual) < " + CommonFunction.ReplaceInjection(txtdateover.Text) + "";
        //    }

        //}

        //switch (cblstatus.Value + "")
        //{
        //    case "0": Condition += " AND EMP.INUSE = '0'";
        //        break;

        //    case "1": Condition += " AND EMP.INUSE = '1'";
        //        break;
        //}

        switch (radStatusDriver.SelectedValue + "")
        {
            case "0":
                Condition += " AND EMP.INUSE = '0'";
                break;

            case "1":
                Condition += " AND EMP.INUSE = '1'";
                break;
        }


        return VendorBLL.Instance.VendorList(Condition);
    }
    void Listdata()
    {


        gvw.DataSource = GetData();
        gvw.DataBind();
        if (Request.QueryString["bl"] != null && Request.QueryString["bl"] == "bl")
        {
            lblCount.Text = "จำนวนทั้งหมด " + gvw.VisibleRowCount + " คน";
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtFinal = GetData();
            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            workbook.Worksheets.Add("Export");
            ExcelWorksheet worksheet = workbook.Worksheets["Export"];

            dtFinal.Columns["SEMPLOYEEID"].ColumnName = "รหัสพนักงาน";
            dtFinal.Columns["SNAME"].ColumnName = "ชื่อ - นามสกุล";
            dtFinal.Columns["PERS_CODE"].ColumnName = "เลขบัตรประชาชน";
            dtFinal.Columns["SABBREVIATION"].ColumnName = "ชื่อบริษัทผู้ขนส่ง";
            dtFinal.Columns["SEMPTPYE"].ColumnName = "ตำแหน่ง";
            dtFinal.Columns["CACTIVE"].ColumnName = "การไม่อนุญาตใช้งาน";
            dtFinal.Columns["DUPDATEEMP"].ColumnName = "วันที่อัพเดท";
            dtFinal.Columns["SUPDATEEMP"].ColumnName = "ผู้อัพเดทข้อมูล";
            dtFinal.Columns["INUSENAME"].ColumnName = "การจ้างงาน";
            dtFinal.Columns.Remove("STRANS_ID");
            dtFinal.Columns.Remove("DUPDATESAP");
            dtFinal.Columns.Remove("SUPDATESAP");
            dtFinal.Columns.Remove("CHECKIN");
            dtFinal.Columns.Remove("NOTFILL");
            dtFinal.Columns.Remove("INUSE");
            dtFinal.Columns.Remove("LASTJOB");
            
            DataRow dr = dtFinal.Rows[0];
            
            worksheet.InsertDataTable(dtFinal, new InsertDataTableOptions(1, 0) { ColumnHeaders = false });
            for (int j = 0; j < dtFinal.Columns.Count; j++)
            {//Export Detail
                this.SetFormatCell(worksheet.Cells[0, j], dtFinal.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
                worksheet.Columns[j].AutoFit();
            }
            //for (int i = 2; i <= dtFinal.Rows.Count; i++)
            //{
            //    dr = dtFinal.Rows[i - 1];
            //    for (int j = 0; j < dtFinal.Columns.Count; j++)
            //    {//Export Detail
            //        this.SetFormatCell(worksheet.Cells[i, j], dr[j].ToString(), VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Left, true);
            //        worksheet.Columns[j].AutoFit();
            //    }
            //}
            string Path = this.CheckPath();
            string FileName = "Vendor_Employee_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    protected void imbeditStatus_Click(object sender, EventArgs e)
    {
        ASPxButton btn = (ASPxButton)sender;
        dynamic dataStatus = gvw.GetRowValues(int.Parse(btn.CommandArgument), "SEMPLOYEEID");
        var plaintextBytes = Encoding.UTF8.GetBytes(dataStatus + string.Empty);
        var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        Response.Redirect("~/vendor_employee_add1.aspx?editStatus=" + encryptedValue + "&bl=" + Request.QueryString["bl"]);
    }
}