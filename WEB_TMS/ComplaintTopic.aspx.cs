﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ComplaintTopic : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
        }
    }

    private void InitialForm()
    {
        try
        {
            this.SearchData(string.Empty);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SearchData(string Condition)
    {
        try
        {
            DataTable dtTopic = TopicBLL.Instance.TopicSelectAllBLL(string.Empty);
            GridViewHelper.BindGridView(ref dgvTopic, dtTopic);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void dgvTopic_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            var plaintextBytes = Encoding.UTF8.GetBytes(dgvTopic.DataKeys[e.RowIndex]["TOPIC_ID"].ToString());
            var encryptedValue = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);

            MsgAlert.OpenForm("ComplaintTopicEditDetail.aspx?TOPIC_ID=" + encryptedValue, Page);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdAddComplain_Click(object sender, EventArgs e)
    {
        Response.Redirect("ComplaintTopicAdd.aspx");
    }
    protected void cmdAddDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("ComplaintTopicAddDetail.aspx");
    }
}