﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class EditReportKpi : PageBase
{
    #region Viewstate
    private DataTable CurrentTable
    {
        get
        {
            if ((DataTable)ViewState["CurrentTable"] != null)
                return (DataTable)ViewState["CurrentTable"];
            else
                return null;
        }
        set
        {
            ViewState["CurrentTable"] = value;
        }
    }
    private string kpi_eva_head_id
    {
        get
        {
            if ((string)ViewState["kpi_eva_head_id"] != null)
                return (string)ViewState["kpi_eva_head_id"];
            else
                return null;
        }
        set
        {
            ViewState["kpi_eva_head_id"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
            {
                var decryptedBytes = MachineKey.Decode(Request.QueryString["str"], MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string str = decryptedValue;
                kpi_eva_head_id = str;
                CurrentTable = TMS_BLL.Master.KPIBLL.Instance.GetReportById(str);
                SetDataToGridview(CurrentTable);



            }
        }
    }

    private void SetDataToGridview(DataTable dtCurrentTable)
    {
        try
        {
            txtNameForm.Text = dtCurrentTable.Rows[0]["NAME"].ToString();
            txtDescription.Text = dtCurrentTable.Rows[0]["DESCRIPTION"].ToString();
            rdostatus.SelectedValue = dtCurrentTable.Rows[0]["STATUS"].ToString();
            dgvnewkpi.DataSource = dtCurrentTable;
            dgvnewkpi.DataBind();
            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    #region Event
        #region Gridview Event
        protected void dgvnewkpi_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DataTable dt_frequency = Setfrequency();
                DataTable dt_sum_average = SetSumAverage();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Find the drop down list by name
                    DropDownList ddl_frequency = (DropDownList)e.Row.FindControl("ddl_frequency");
                    DropDownList ddl_sum = (DropDownList)e.Row.FindControl("ddl_sum");
                    DropDownListHelper.BindDropDownList(ref ddl_frequency, dt_frequency, "Values", "Text", false);
                    DropDownListHelper.BindDropDownList(ref ddl_sum, dt_sum_average, "Values", "Text", false);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region Click Event
        #region Click Black
        protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
        {
            Response.Redirect("report_kpi.aspx");
        }
        #endregion
        #region EditData
        protected void cmdEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                SetGridToDataTable();
                this.SetValidate(CurrentTable);
                dt = KPIBLL.Instance.GetEditDataKPI(kpi_eva_head_id, kpi_eva_head(), CurrentTable);
                if (string.Equals(dt.Rows[0][0].ToString(), "00"))
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    alertFail(dt.Rows[0][0].ToString());
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
            
        }
        #endregion
        #region Addrow
        protected void ADD_Topic_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        #endregion
        #region DeleteRow
        protected void Delrow_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                if (btn != null)
                {
                    DataTable dtCurrentTable = SetGridToDataTable();
                    if (dtCurrentTable.Rows.Count >= 2)
                    {
                        string command = btn.CommandArgument;
                        dtCurrentTable.Rows.RemoveAt(int.Parse(command));
                        CurrentTable = dtCurrentTable;
                        dgvnewkpi.DataSource = dtCurrentTable;
                        dgvnewkpi.DataBind();
                        SetPreviousData();
                    }
                    else
                    {
                        alertFail("แถวกรอกข้อมูลเท่ากับ 1");
                    }

                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);

            }

        }
        #endregion
        
        #endregion

    #endregion
    #region AddRowForClick
    private void AddNewRowToGrid()
    {

        try
        {

            int rowIndex = 0;

            if (CurrentTable != null)
            {
                DataTable dtCurrentTable = (DataTable)CurrentTable;
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {

                        //extract the TextBox values
                        TextBox NameReport = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("NameReport");
                        DropDownList ddl_frequency = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[2].FindControl("ddl_frequency");
                        DropDownList ddl_sum = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[3].FindControl("ddl_sum");
                        TextBox Id1 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[4].FindControl("Id1");
                        TextBox Id2 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[5].FindControl("Id2");
                        TextBox Id3 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[6].FindControl("Id3");
                        TextBox Id4 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[7].FindControl("Id4");
                        TextBox Id5 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[8].FindControl("Id5");
                        TextBox txtWeight = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("txtWeight");
                        drCurrentRow = dtCurrentTable.NewRow();

                        dtCurrentTable.Rows[i - 1]["EVA_NAME"] = NameReport.Text;
                        dtCurrentTable.Rows[i - 1]["FREQUENCY"] = ddl_frequency.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["CALCULATE"] = ddl_sum.Text;
                        dtCurrentTable.Rows[i - 1]["RANGE1"] = Id1.Text;
                        dtCurrentTable.Rows[i - 1]["RANGE2"] = Id2.Text;
                        dtCurrentTable.Rows[i - 1]["RANGE3"] = Id3.Text;
                        dtCurrentTable.Rows[i - 1]["RANGE4"] = Id4.Text;
                        dtCurrentTable.Rows[i - 1]["RANGE5"] = Id5.Text;
                        dtCurrentTable.Rows[i - 1]["WEIGHT"] = string.IsNullOrEmpty(txtWeight.Text.ToString()) ? decimal.Parse("0").ToString() : txtWeight.Text.ToString();
                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    CurrentTable = dtCurrentTable;
                    dgvnewkpi.DataSource = dtCurrentTable;
                    dgvnewkpi.DataBind();
                }
            }            
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region SetDataGridviewToDataTable
    private DataTable SetGridToDataTable()
    {
        try
        {

            int rowIndex = 0;

            DataTable dtCurrentTable = (DataTable)CurrentTable;
            if (dgvnewkpi.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {

                    //extract the TextBox values
                    TextBox NameReport = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("NameReport");
                    DropDownList ddl_frequency = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[2].FindControl("ddl_frequency");
                    DropDownList ddl_sum = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[3].FindControl("ddl_sum");
                    TextBox Id1 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[4].FindControl("Id1");
                    TextBox Id2 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[5].FindControl("Id2");
                    TextBox Id3 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[6].FindControl("Id3");
                    TextBox Id4 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[7].FindControl("Id4");
                    TextBox Id5 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[8].FindControl("Id5");
                    TextBox txtWeight = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("txtWeight");
                    dtCurrentTable.Rows[i - 1]["eva_name"] = NameReport.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["frequency"] = ddl_frequency.SelectedValue.ToString();
                    dtCurrentTable.Rows[i - 1]["calculate"] = ddl_sum.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["range1"] = Id1.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["range2"] = Id2.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["range3"] = Id3.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["range4"] = Id4.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["range5"] = Id5.Text.ToString();
                    dtCurrentTable.Rows[i - 1]["weight"] = string.IsNullOrEmpty(txtWeight.Text.ToString()) ? decimal.Parse("0").ToString() : txtWeight.Text.ToString();
                    rowIndex++;

                }

                return dtCurrentTable;

            }

            return new DataTable();


        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }

    }
    #endregion
    #region Set Gridfor postback
    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (CurrentTable != null)
        {
            DataTable dt = (DataTable)CurrentTable;
            if (dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //extract the TextBox values
                    TextBox NameReport = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("NameReport");
                    DropDownList ddl_frequency = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[2].FindControl("ddl_frequency");
                    DropDownList ddl_sum = (DropDownList)dgvnewkpi.Rows[rowIndex].Cells[3].FindControl("ddl_sum");
                    TextBox Id1 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[4].FindControl("Id1");
                    TextBox Id2 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[5].FindControl("Id2");
                    TextBox Id3 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[6].FindControl("Id3");
                    TextBox Id4 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[7].FindControl("Id4");
                    TextBox Id5 = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[8].FindControl("Id5");
                    TextBox txtWeight = (TextBox)dgvnewkpi.Rows[rowIndex].Cells[1].FindControl("txtWeight");

                    //Set DataTORow
                    NameReport.Text = dt.Rows[i]["EVA_NAME"].ToString();
                    ddl_frequency.SelectedValue = dt.Rows[i]["FREQUENCY"].ToString();
                    ddl_sum.SelectedValue = dt.Rows[i]["CALCULATE"].ToString();
                    Id1.Text = dt.Rows[i]["RANGE1"].ToString();
                    Id2.Text = dt.Rows[i]["RANGE2"].ToString();
                    Id3.Text = dt.Rows[i]["RANGE3"].ToString();
                    Id4.Text = dt.Rows[i]["RANGE4"].ToString();
                    Id5.Text = dt.Rows[i]["RANGE5"].ToString();
                    txtWeight.Text = dt.Rows[i]["WEIGHT"].ToString();
                    rowIndex++;
                }
            }
        }
    }
    #endregion        
    #region FixData
    private DataTable Setfrequency()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Text", typeof(string)));
            dt.Columns.Add(new DataColumn("Values", typeof(string)));
            dt.Rows.Add("รายเดือน", "M");
            dt.Rows.Add("รายปี", "Y");
            return dt;
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    private DataTable SetSumAverage()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Text", typeof(string)));
            dt.Columns.Add(new DataColumn("Values", typeof(string)));
            dt.Rows.Add("ค่าเฉลี่ย", "Avg");
            dt.Rows.Add("ค่าสูงสุด", "Max");
            dt.Rows.Add("ค่าต่ำสุด", "Min");
            return dt;
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    #endregion
    #region ValidateData
    private void SetValidate(DataTable DataKpiDetail)
    {
        try
        {
            ValidateNullRow();
            ValidateRange(CurrentTable);
            int totalWeight = ValidateWightMax(DataKpiDetail);
            ValidateEquals(totalWeight);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    private void ValidateEquals(int totalWeight)
    {
        try
        {
            if (totalWeight != 100)
            {
                throw new Exception("จำนวน Weight ผลรวมไม่เท่ากับ 100");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    private void ValidateRange(DataTable Datakpi)
    {
        try
        {
            decimal Minvalue = 0;
            decimal Maxvalue = 0;

            for (int i = 0; i < Datakpi.Rows.Count; i++)
            {
                Minvalue = 0;
                Maxvalue = 0;
                for (int j = 6; j < 10; j++)
                {
                    string valueCell = Datakpi.Rows[i][j].ToString();
                    string[] datavelue = valueCell.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                    if (datavelue.Length > 2)
                    {
                        throw new Exception("แถวที่ " + i + 1 + " คุณกรอกช่วงไม่ถูกต้อง");
                    }
                    if (datavelue.Length > 1)
                    {
                        //ตรวจสอบตัดตัวเลขแล้วค่าแรกมากกว่าค่าหลัง
                        if (decimal.Parse(datavelue[0]) < decimal.Parse(datavelue[1]))
                        {
                            if (Minvalue >= decimal.Parse(datavelue[0]) || Maxvalue >= decimal.Parse(datavelue[0]))
                            {
                                throw new Exception("แถวที่ " + (i + 1) + " คุณกรอกช่วงไม่ถูกต้อง");
                            }
                            else
                            {
                                Minvalue = decimal.Parse(datavelue[0]);
                            }
                            if (Maxvalue >= decimal.Parse(datavelue[1]))
                            {
                                throw new Exception("แถวที่ " + (i + 1) + " คุณกรอกช่วงไม่ถูกต้อง");
                            }
                            else
                            {
                                Maxvalue = decimal.Parse(datavelue[1]);
                            }
                        }
                        else
                        {
                            throw new Exception("แถวที่ " + (i+1) + " คุณกรอกช่วงไม่ถูกต้อง");
                        }
                    }
                    else
                    {
                        if (Minvalue >= decimal.Parse(datavelue[0]))
                        {
                            throw new Exception("แถวที่ " + (i + 1) + " คุณกรอกช่วงไม่ถูกต้อง");
                        }
                        else
                        {
                            Minvalue = decimal.Parse(datavelue[0]);
                        }
                        if (Maxvalue >= decimal.Parse(datavelue[0]))
                        {
                            throw new Exception("แถวที่ " + (i + 1) + " คุณกรอกช่วงไม่ถูกต้อง");
                        }
                        else
                        {
                            Maxvalue = decimal.Parse(datavelue[0]);
                        }
                    }
                }
            }
        }
        catch (FormatException ex)
        {
            throw new Exception("คุณกรอกรูปแบบไม่ถูกต้อง");
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    private void ValidateNullRow()
    {
        try
        {

            if (dgvnewkpi.Rows.Count < 0)
                throw new Exception("กรุณาเพิ่มหัวข้อการประเมิน");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private int ValidateWightMax(DataTable DataKpiDetail)
    {
        try
        {
            int sum = 0;
            foreach (DataRow dr in DataKpiDetail.Rows)
            {
                sum += string.IsNullOrEmpty(dr["weight"].ToString()) ? 0 : int.Parse(dr["weight"].ToString());
                if (sum > 100)
                {
                    throw new Exception("Weight ต้องไม่เกิน 100");
                    break;
                }
            }
            return sum;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region InitalSaveData
    public DataTable kpi_eva_head()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("txtNameForm");
        dt.Columns.Add("txtDescription");
        dt.Columns.Add("rdostatus");
        dt.Columns.Add("userid");
        dt.Rows.Add(
            txtNameForm.Text.Trim().ToString(),
            txtDescription.Text.Trim().ToString(),
            rdostatus.SelectedValue.ToString(),
            Session["UserID"].ToString()
            );
        return dt;
    }

    #endregion
}
