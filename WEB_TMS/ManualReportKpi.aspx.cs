﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ManualReportKpi : PageBase
{
    #region Viewstate
    private DataTable CurrentTable
    {
        get
        {
            if ((DataTable)ViewState["CurrentTable"] != null)
                return (DataTable)ViewState["CurrentTable"];
            else
                return null;
        }
        set
        {
            ViewState["CurrentTable"] = value;
        }
    }
    #region Viewstate ForSave
    private string MonthId
    {
        get
        {
            if ((string)ViewState["MonthId"] != null)
                return (string)ViewState["MonthId"];
            else
                return null;
        }
        set
        {
            ViewState["MonthId"] = value;
        }
    }
    private string Contract_id
    {
        get
        {
            if ((string)ViewState["Contract_id"] != null)
                return (string)ViewState["Contract_id"];
            else
                return null;
        }
        set
        {
            ViewState["Contract_id"] = value;
        }
    }
    #endregion
    
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            string str = Request.QueryString["str"];
            var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
            string strQuery = Encoding.UTF8.GetString(decryptedBytes);
            string[] listStr = strQuery.Split('&');
            string Year = listStr[0], Month = listStr[1], Contractid = listStr[2], VendorName = listStr[3],ContractName = listStr[4];
            MonthId = Month;
            Contract_id = Contractid;
            SetInitalLoad(Year , Month , Contractid , VendorName , ContractName );
        }
    }
    #region SetIntial FormLoad
    private void SetInitalLoad(string Year ,string  Month ,string Contractid ,string VendorName ,string ContractName )
        {
            CurrentTable = TMS_BLL.Master.KPIBLL.Instance.GetLoadFromKPIByYearMonth(Year, Month, Contractid);
            if (CurrentTable.Rows.Count > 0)
            {
                SetDataForm(Year, Month, Contractid, VendorName, ContractName);
            }
            else
            {
                alertSuccess("ท่านยังไม่ได้ตั้งค่าเอกสารประเมิน","Config_reportKPI.aspx");
            }
        }

    private void SetDataForm(string Year, string Month, string Contractid, string VendorName, string ContractName)
    {
        try
        {
            lblYear.Text = Year;
            lblMonth.Text = GetMonthNameByNo(int.Parse(Month));
            lblVendor.Text = VendorName;
            lblContract.Text = ContractName;
            
            lblNameKpi.Text = CurrentTable.Rows[0][0].ToString();
            gvd_manual.DataSource = CurrentTable;
            gvd_manual.DataBind();
            //Bindding dataลง Textbox
            SetTextBoxGridview(CurrentTable);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SetTextBoxGridview(DataTable CurrentTable)
    {
        try
        {
            for (int i = 0; i < gvd_manual.Rows.Count; i++)
            {
                TextBox txtscore = (TextBox)gvd_manual.Rows[i].Cells[1].FindControl("txtscore");
                txtscore.Text = CurrentTable.Rows[i]["INPUT_DATA"] + "";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region Event
        #region Click
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = KPIBLL.Instance.GetKpiScoreInsert(TableSetDelete(),TableData());
                if (string.Equals(dt.Rows[0][0].ToString(), "00"))
                {
                    alertSuccess("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    alertFail(dt.Rows[0][0].ToString());
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
            
        }

        
        protected void cmdTemp_Click(object sender, EventArgs e)
        {

        }
        protected void mpConfirmBack_ClickOK(object sender ,EventArgs e)
        {
            Response.Redirect("Kpi_process.aspx");
        }
        #endregion
        #region TextChange
        protected void txtscore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox tb = (TextBox)sender;
                GridViewRow gvr = (GridViewRow)tb.Parent.Parent;
                int rowindex = gvr.RowIndex;
                if (!string.IsNullOrEmpty(tb.Text.ToString()))
                {
                    gvd_manual.Rows[rowindex].Cells[12].Text = SetValueScore(rowindex, decimal.Parse(tb.Text.ToString()));
                }
                else
                {
                    gvd_manual.Rows[rowindex].Cells[12].Text = string.Empty;
                }
            }
            catch(FormatException)
            {
                alertFail("ท่านกรอกรูปแบบไม่ถูกต้อง");
            }
            catch (Exception ex)
            {

                alertFail(ex.Message);
            }
                 
        }
        #endregion
   
    #endregion

    #region Miscellanous
    #region GetMonthByNo
    private string GetMonthNameByNo(int monthId)
    {
        string[] month = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
        return month[monthId-1];
    }
    #endregion
    
    #region Get ValueScore
    #region Set ValueScore
    private string SetValueScore(int RowNumber,decimal inputval)
    {
        string val = string.Empty;
        decimal beforeValue = decimal.Parse("0.00");
        beforeValue = GetValueFrequency(RowNumber,inputval);
        val = GetValueScoreKPI(RowNumber, beforeValue); 
        return val;
    }

    
    #endregion
    private decimal GetValueFrequency(int RowNumber, decimal inputval)
    {
        try
        {
            decimal R_val=0;
            string valueCell = gvd_manual.Rows[RowNumber].Cells[10].Text.ToString();
            string[] datavelue = valueCell.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (datavelue.Length <= 1)
            {
                R_val += inputval;
            }
            if (datavelue.Length > 1)
            {
                R_val += inputval + decimal.Parse(datavelue[1]);
            }
            return R_val;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private string GetValueScoreKPI(int RowNumber, decimal inputval)
    {
        try
        {
            int Value = 0;
            decimal[] Range = new decimal[2];
            for (int i = 4; i < 9; i++)
            {
                string valueCell = gvd_manual.Rows[RowNumber].Cells[i].Text.ToString();
                string[] datavelue = valueCell.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                if (datavelue.Length <= 1)
                {
                    Range[0] = decimal.Parse(datavelue[0]);
                    Range[1] = decimal.Parse(datavelue[0]);
                }
                if (datavelue.Length > 1)
                {
                    Range[0] = decimal.Parse(datavelue[0]);
                    Range[1] = decimal.Parse(datavelue[1]);
                }
                
                switch (i)
                {
                    case 4:
                        if (inputval >= Range[0] && inputval <= Range[1])
                        {
                            Value = 1;
                            
                        }
                        
                        break;
                    case 5:
                        if (inputval >= Range[0] && inputval <= Range[1])
                        {
                            Value = 2;
                        }
                        
                        break;
                    case 6:
                        if (inputval >= Range[0] && inputval <= Range[1])
                        {
                            Value = 3;
                        }
                        
                        break;
                    case 7:
                        if (inputval >= Range[0] && inputval <= Range[1])
                        {
                            Value = 4;
                        }
                        
                        break;
                    case 8:
                        if (inputval >= Range[0] && inputval <= Range[1])
                        {
                            Value = 5;
                        }
                        
                        break;
                    
                }              

            }
                return Value.ToString();
        }
        catch (Exception)
        {
            
            throw;
        }
    }
    #endregion
    #endregion
    #region InitalSaveData
    private DataTable TableData()
    {
        try
        {
            
            for (int i = 0; i < gvd_manual.Rows.Count; i++)
            {
                CurrentTable.Rows[i]["INPUT_DATA"] = string.IsNullOrEmpty(((TextBox)gvd_manual.Rows[i].FindControl("txtscore")).Text) ? decimal.Parse("0") : decimal.Parse(((TextBox)gvd_manual.Rows[i].FindControl("txtscore")).Text);
                    
                CurrentTable.Rows[i]["SCORE"] = string.IsNullOrEmpty(gvd_manual.Rows[i].Cells[12].Text)? decimal.Parse("0") : decimal.Parse(gvd_manual.Rows[i].Cells[12].Text);
            }
            return CurrentTable;
        }
        catch (Exception ex)
        {

            throw;
        }
    }
    private DataTable TableSetDelete()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("year");
            dt.Columns.Add("month");
            dt.Columns.Add("contract_no");
            dt.Columns.Add("UserId");
            dt.Rows.Add(
                lblYear.Text,
                MonthId,
                Contract_id,
                Session["UserID"].ToString()
                );

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion



    protected void gvd_manual_PreRender(object sender, EventArgs e)
    {
        var gridView = (GridView)sender;
        var header = (GridViewRow)gridView.Controls[0].Controls[0];
        header.Cells[10].Visible = false;
        header.Cells[11].ColumnSpan = 2;
        header.Cells[11].Text = "ผลการประเมิน";
    }
}