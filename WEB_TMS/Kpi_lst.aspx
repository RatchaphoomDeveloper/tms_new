﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="Kpi_lst.aspx.cs" Inherits="Kpi_lst" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
    <style type="text/css">
    th
    {
        text-align:center;
    }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <p>&nbsp;</p>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>ดัชนีชี้วัดประสิทธิภาพการทำงาน (KPI)
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">&nbsp;</div>
                <div class="col-md-6">
                    <asp:Button runat="server" CssClass="btn btn-default" ID="list_kpi" Text="จัดการชุดคำถาม" OnClick="list_kpi_Click" />
                    &nbsp; &nbsp;
                    <asp:Button runat="server" CssClass="btn btn-default" ID="config_kpi" Text="ตั้งค่าการประเมิน"  OnClick="config_kpi_Click"/>
                    &nbsp; &nbsp;
                    <asp:Button runat="server" CssClass="btn btn-default" id="save_kpi" Text="บันทึกผลประเมิน KPI" OnClick="save_kpi_Click" />
                    &nbsp; &nbsp;
                    <asp:Button runat="server" CssClass="btn btn-default" ID="view_kpi" Text="ดูผลการประเมิน" />
                </div>
                
                <div class="col-md-2">&nbsp;</div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <asp:GridView ID="gvd_kpi" runat="server" Width="100%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="Both" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]" OnPreRender="gvd_kpi_PreRender" OnDataBound="gvd_kpi_DataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField DataField="ROWCOLUMN" HeaderText="">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField> 
                        <asp:BoundField DataField="ROWMERGE" HeaderText="" >
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>                        
                        <asp:BoundField DataField="JAN" HeaderText="ม.ค" ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FEB" HeaderText="ก.พ." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAR" HeaderText="มี.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="APR" HeaderText="เม.ย." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAY" HeaderText="พ.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JUN" HeaderText="มิ.ย." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="JUL" HeaderText="ก.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AUG" HeaderText="ส.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SEP" HeaderText="ก.ย." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="OCT" HeaderText="ต.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOV" HeaderText="พ.ย." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DEC" HeaderText="ธ.ค." ControlStyle-Width="20px">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>  
</asp:Content>


