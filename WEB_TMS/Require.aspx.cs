﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class Require : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            if (Request.QueryString["id"] != null)
            {
                
            }
            string str = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(str))
            {
                var decryptedBytes = MachineKey.Decode(str, MachineKeyProtection.All);
                var decryptedValue = Encoding.UTF8.GetString(decryptedBytes);
                string[] ids = decryptedValue.Split('_');
                hidM_COMPLAIN_REQUIRE_FIELD.Value = ids[0];
                ddlFieldType.SelectedValue = ids[1];
                //DataTable dt = RequireBLL.Instance.RequireSelect(null, ids[0], null, ids[1]);
                
                //BindData(dt);
               
            }
        }
    }

    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {
            bool isRes = RequireBLL.Instance.RequireSave(ddlField.SelectedValue, rblActive.SelectedValue, rblStatus.SelectedValue);
            if (isRes)
            {
                alertSuccess("แก้ไขข้อมูลสำเร็จ", "Require_Lst.aspx");
            }
            else
            {
                alertFail("แก้ไขข้อมูลไม่สำเร็จ");
            }
        }
        catch (Exception ex)
        {
            alertFail("แก้ไขข้อมูลไม่สำเร็จ : " + ex.Message);
        }
    }
    protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("~/Require_Lst.aspx");
    }

    private void BindData(DataTable dt)
    {
        foreach (DataRow item in dt.Rows)
        {
            ddlTopic.Items.Clear();
            ddlTopic.Items.Add(new ListItem() {
                Text = item["TOPIC_NAME"] + string.Empty,
                Value = item["TOPIC_ID"] + string.Empty,
            });

            ddlComplainType.Items.Clear();
            ddlComplainType.Items.Add(new ListItem()
            {
                Text = item["COMPLAIN_TYPE_NAME"] + string.Empty,
                Value = item["COMPLAIN_TYPE_ID"] + string.Empty,
            });

            ddlField.Items.Clear();
            ddlField.Items.Add(new ListItem()
            {
                Text = item["DESCRIPTION"] + string.Empty,
                Value = item["M_COMPLAIN_REQUIRE_FIELD"] + string.Empty
            });
            rblStatus.SelectedValue = item["REQUIRE_TYPE"] + string.Empty;
            rblActive.SelectedValue = item["ISACTIVE"] + string.Empty;
            break;
        }
    }
    
}