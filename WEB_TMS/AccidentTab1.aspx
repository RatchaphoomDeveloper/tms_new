﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AccidentTab1.aspx.cs" Culture="en-US" UICulture="en-US" Inherits="AccidentTab1" %>

<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphIE" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Panel runat="server" ID="plTab1">
                <div class="form-horizontal">

                    <ul class="nav nav-tabs" runat="server" id="tabtest">
                        <li class="active" id="liTab1" runat="server"><a href="#TabGeneral" data-toggle="tab" aria-expanded="true" runat="server" id="GeneralTab1">แจ้งเรื่องอุบัติเหตุ</a></li>
                        <li class="" id="liTab2" visible="false" runat="server"><a href="#TabGeneral2" runat="server" id="GeneralTab2">รายงานเบื้องต้น</a></li>
                        <li class="" id="liTab3" visible="false" runat="server"><a href="#TabGeneral3" runat="server" id="GeneralTab3">รายงานวิเคราะห์สาเหตุ</a></li>
                        <li class="" id="liTab4" visible="false" runat="server"><a href="#TabGeneral4" runat="server" id="GeneralTab4">รายงานผลการพิจารณา</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="TabGeneral">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse1" id="acollapse1">ข้อมูลการเกิดอุบัติเหตุ</a>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">Accident ID</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtAccidentID" CssClass="form-control" ReadOnly="true" Text="Generate by System" />
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-1">
                                            </div>
                                            <label class="col-md-2 control-label">วันที่-เวลาเกิดเหตุ<asp:Label ID="Label4" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtAccidentDate" CssClass="form-control datetimepicker" />
                                            </div>
                                            <label class="col-md-2 control-label">วันที่-เวลาที่แจ้งเรื่องในระบบ</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtAccidentDateSystem" CssClass="form-control datetimepicker" ReadOnly="true" />
                                            </div>

                                        </div>
                                        <div class="row form-group" id="divREPORTER">
                                            <label class="col-md-3 control-label">ผขส. แจ้งเหตุให้ ปตท.</label>
                                            <div class="col-md-3">
                                                <div class="col-md-4 PaddingLeftRight0">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblREPORTER" OnSelectedIndexChanged="rblREPORTER_SelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Text="&nbsp;No&nbsp;" Value="0" Selected="True" />
                                                        <asp:ListItem Text="&nbsp;Yes&nbsp;" Value="1" />
                                                    </asp:RadioButtonList>
                                                </div>
                                                <div class="col-md-8 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtAccidentDatePtt" CssClass="form-control datetimepicker" ReadOnly="true" />
                                                </div>

                                            </div>
                                            <label class="col-md-2 control-label">ชื่อผู้แจ้ง(ผู้ขนส่ง)</label>
                                            <div class="col-md-3">
                                                <asp:TextBox runat="server" ID="txtAccidentName" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="hidcollapse1" />
                            </div>

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse2" id="acollapse2">ข้อมูลเบื้องต้นของอุบัติเหตุ</a>
                                    <asp:HiddenField runat="server" ID="hidcollapse2" />
                                </div>
                                <div id="collapse2" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">ขั้นตอนการขนส่งขณะเกิดเหตุ<asp:Label ID="lblReqddlAccidentType" Text="&nbsp;*" ForeColor="Red" runat="server" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlAccidentType" OnSelectedIndexChanged="ddlAccidentType_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>

                                            <label class="col-md-2 control-label">ขนส่ง B100/Ethanol<asp:Label ID="Label5" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:CheckBox Text="" ID="cbB100" runat="server" OnCheckedChanged="cbB100_CheckedChanged" AutoPostBack="true" />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">อ้างอิงหมายเลข Shipment<asp:Label ID="lblReqtxtShipmentNo" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <div class="col-md-9 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtShipmentNo" CssClass="form-control" ReadOnly="true" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnShipmentNoSearch" runat="server" Text="..." Width="30px"
                                                        OnClick="btnShipmentNoSearch_Click" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnShipmentNoSearchClear" runat="server" Text="X" Width="30px" OnClick="btnShipmentNoSearchClear_Click" />
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">ทะเบียนรถ<asp:Label ID="lblReqtxtTruck" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">

                                                <div class="col-md-9 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtTruck" CssClass="form-control" ReadOnly="true" />
                                                    <asp:HiddenField runat="server" ID="hidSTRUCKID" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTruck" runat="server" Text="..." Width="30px"
                                                        OnClick="btnTruck_Click" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTruckClear" runat="server" Text="X" Width="30px" OnClick="btnTruckClear_Click" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">ชื่อผู้ขนส่ง<asp:Label ID="lblReqddlVendor" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlVendor" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">เลขที่สัญญาจ้างขนส่ง<asp:Label ID="lblReqddlContract" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlContract" OnSelectedIndexChanged="ddlContract_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">กลุ่มงาน<asp:Label ID="lblReqddlWorkGroup" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlWorkGroup" OnSelectedIndexChanged="ddlWorkGroup_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มที่<asp:Label ID="lblReqddlGroup" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">
                                                <asp:DropDownList runat="server" CssClass="form-control" ID="ddlGroup">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse21" id="acollapse21">พนักงานคนที่ 1</a>
                                                <asp:HiddenField runat="server" ID="HiddenField1" />
                                            </div>
                                            <div id="collapse21" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE" runat="server" Text="..." Width="30px"
                                                                    OnClick="btnPERS_CODE_Click" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODEClear" runat="server" Text="X" Width="30px" OnClick="btnPERS_CODEClear_Click" />
                                                            </div>
                                                        </div>
                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" Checked="true" GroupName="DRIVER_NO" ID="rbDRIVER_NO1" runat="server" />
                                                        </div>

                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse22" id="acollapse22">พนักงานคนที่ 2</a>
                                                <asp:HiddenField runat="server" ID="HiddenField2" />
                                            </div>
                                            <div id="collapse22" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE2" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID2" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE2" runat="server" Text="..." Width="30px"
                                                                    OnClick="btnPERS_CODE2_Click" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE2Clear" runat="server" Text="X" Width="30px" OnClick="btnPERS_CODE2Clear_Click" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO2" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME2" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE2" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE2" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse23" id="acollapse23">พนักงานขับรถคนที่ 3</a>
                                                <asp:HiddenField runat="server" ID="HiddenField4" />
                                            </div>
                                            <div id="collapse23" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE3" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE3" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID3" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE3" runat="server" Text="..." Width="30px"
                                                                    OnClick="btnPERS_CODE3_Click" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE3Clear" runat="server" Text="X" Width="30px" OnClick="btnPERS_CODE3Clear_Click" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO3" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME3" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE3" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE3" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-warning">
                                            <div class="panel-heading">
                                                <i class="fa fa-table"></i><a data-toggle="collapse" href="#collapse24" id="acollapse24">พนักงานขับรถคนที่ 4</a>
                                                <asp:HiddenField runat="server" ID="HiddenField6" />
                                            </div>
                                            <div id="collapse24" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">เลขที่บัตรประชาชน<asp:Label ID="lblReqtxtPERS_CODE4" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <div class="col-md-9 PaddingLeftRight0">
                                                                <asp:TextBox runat="server" ID="txtPERS_CODE4" CssClass="form-control" ReadOnly="true" />
                                                                <asp:HiddenField runat="server" ID="hidSEMPLOYEEID4" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE4" runat="server" Text="..." Width="30px"
                                                                    OnClick="btnPERS_CODE4_Click" />
                                                            </div>
                                                            <div class="col-md-1">
                                                                <asp:Button ID="btnPERS_CODE4Clear" runat="server" Text="X" Width="30px" OnClick="btnPERS_CODE4Clear_Click" />
                                                            </div>
                                                        </div>

                                                        <label class="col-md-2 control-label"></label>
                                                        <div class="col-md-3">
                                                            <asp:RadioButton Text="พนักงานขับรถ" ID="rbDRIVER_NO4" GroupName="DRIVER_NO" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <label class="col-md-3 control-label">ชื่อพนักงานขับรถ</label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox runat="server" ID="txtEMPNAME4" CssClass="form-control" ReadOnly="true" />
                                                        </div>
                                                        <label class="col-md-2 control-label">อายุ(ปี)<asp:Label ID="lblReqtxtAGE4" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                                        <div class="col-md-3">
                                                            <asp:TextBox Text="" runat="server" ID="txtAGE4" CssClass="form-control number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">จุดรับ-จ่ายผลิตภัณฑ์ต้นทาง<asp:Label ID="lblReqtxtSABBREVIATION" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-3">

                                                <div class="col-md-9 PaddingLeftRight0">
                                                    <asp:TextBox runat="server" ID="txtSABBREVIATION" CssClass="form-control" />
                                                    <asp:HiddenField runat="server" ID="hidSTERMINALID" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTERMINAL" runat="server" Text="..." Width="30px"
                                                        OnClick="btnTERMINAL_Click" />
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button ID="btnTERMINALClear" runat="server" Text="X" Width="30px" OnClick="btnTERMINALClear_Click" />
                                                </div>
                                            </div>
                                            <label class="col-md-1 control-label">พิกัด GPS<asp:Label ID="lblReqtxtGPS" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <label class="col-md-1 control-label">Latitude</label>
                                            <div class="col-md-1">
                                                <asp:TextBox runat="server" ID="txtGPSL" CssClass="form-control " />
                                            </div>
                                            <label class="col-md-1 control-label">Longitude</label>
                                            <div class="col-md-1">
                                                <asp:TextBox runat="server" ID="txtGPSR" CssClass="form-control " />
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label">สถานที่เกิดเหตุ<asp:Label ID="lblReqtxtLocation" Text="&nbsp;*" ForeColor="Red" runat="server" Visible="false" /></label>
                                            <div class="col-md-8">
                                                <asp:TextBox runat="server" ID="txtLocation" CssClass="form-control" TextMode="MultiLine" Rows="5" />
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12">
                                                <asp:GridView runat="server" ID="gvDO"
                                                    Width="100%" HeaderStyle-HorizontalAlign="Center" BorderWidth="1"
                                                    GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                                    ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" HeaderStyle-BorderWidth="1"
                                                    HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                                    AllowPaging="false" RowStyle-Width="1" Font-Names="Cordia New" Font-Size="16">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="เลข DO" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1" DataField="DELIVERY_NO" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="ผลิตภัณฑ์" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1" DataField="MATERIAL_NAME" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="ปริมาณ" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1" DataField="DLV" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="ปลายทาง" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1" DataField="CUST_NAME" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundField HeaderText="LOADING DATE" HeaderStyle-BorderWidth="1" ItemStyle-BorderWidth="1" DataField="LOADING_END" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <asp:Button Text="บันทึกและส่งข้อมูล" runat="server" class="btn btn-md bth-hover btn-info " ID="btnSave" OnClick="btnSaveSend_Click" ClientIDMode="Static" />
                                    <input id="btnSave2" type="button" value="บันทึกและส่งข้อมูล" data-toggle="modal" data-target="#ModalConfirmBeforeSave" class="btn btn-md bth-hover btn-info hidden" />

                                    <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-info" />
                                    <input id="btnApprove" runat="server" type="button" value="บันทึก(กรณีแก้ไขข้อมูล)" data-toggle="modal" data-target="#ModalConfirmBeforeApprove" class="btn btn-md bth-hover btn-info" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="ShowShipmentSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updShipmentSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="lblDriverSearchTitle" runat="server" Text="ค้นหา Shipment"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtShipmentSearch" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnShipmentSearch" runat="server" Text="ค้นหา" OnClick="btnShipmentSearch_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvShipmentSearch"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvShipmentSearch_PageIndexChanging"
                                            DataKeyNames="SHIPMENT_NO" OnRowUpdating="gvShipmentSearch_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="Shipment No" DataField="SHIPMENT_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ทะเบียน(หัว)" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ทะเบียน(หาง)" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="บริษัท" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="พนักงานขับรถ" DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ต้นทาง" DataField="TTERMINAL_NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="กลุ่มที่" DataField="GROUPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSHIPMENT_NO" Value='<%# Eval("SHIPMENT_NO") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSHEADREGISTERNO" Value='<%# Eval("SHEADREGISTERNO") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSTRUCKID" Value='<%# Eval("STRUCKID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSCONTRACTID" Value='<%# Eval("SCONTRACTID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSVENDORID" Value='<%# Eval("SVENDORID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidGROUPID" Value='<%# Eval("GROUPID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidWORKGROUPID" Value='<%# Eval("WORKGROUPID") %>' />

                                                        <asp:HiddenField runat="server" ID="hidSTERMINALID" Value='<%# Eval("STERMINALID") %>' />

                                                        <asp:HiddenField runat="server" ID="hidTTERMINAL_NAME" Value='<%# Eval("TTERMINAL_NAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" Value='<%# Eval("SEMPLOYEEID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME" Value='<%# Eval("EMPNAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE" Value='<%# Eval("PERS_CODE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE" Value='<%# Eval("AGE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID2" Value='<%# Eval("SEMPLOYEEID2") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME2" Value='<%# Eval("EMPNAME2") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE2" Value='<%# Eval("PERS_CODE2") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE2" Value='<%# Eval("AGE2") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID3" Value='<%# Eval("SEMPLOYEEID3") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME3" Value='<%# Eval("EMPNAME3") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE3" Value='<%# Eval("PERS_CODE3") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE3" Value='<%# Eval("AGE3") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID4" Value='<%# Eval("SEMPLOYEEID4") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME4" Value='<%# Eval("EMPNAME4") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE4" Value='<%# Eval("PERS_CODE4") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE4" Value='<%# Eval("AGE4") %>' />
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseTruck" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowTruckSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updTruckSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label1" runat="server" Text="ค้นหาทะเบียนรถ"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtTruckSearch" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnTruckSearch" runat="server" Text="ค้นหา" OnClick="btnTruckSearch_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvTruckSearch"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvTruckSearch_PageIndexChanging"
                                            DataKeyNames="SHEADREGISTERNO" OnRowUpdating="gvTruckSearch_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ทะเบียน(หัว)" DataField="SHEADREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="ทะเบียน(ห่าง)" DataField="STRAILERREGISTERNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="เลขที่สัญญา" DataField="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="บริษัท" DataField="SABBREVIATION" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="กลุ่มที่" DataField="GROUPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSHEADREGISTERNO" Value='<%# Eval("SHEADREGISTERNO") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSTRUCKID" Value='<%# Eval("STRUCKID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSCONTRACTID" Value='<%# Eval("SCONTRACTID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSVENDORID" Value='<%# Eval("SVENDORID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidGROUPID" Value='<%# Eval("GROUPID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidWORKGROUPID" Value='<%# Eval("WORKGROUPID") %>' />
                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseTruck" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowPERS_CODESearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updPERS_CODESearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label2" runat="server" Text="ค้นหาพนักงาน"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtEmpSearch" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnEmpSearch" runat="server" Text="ค้นหา" OnClick="btnEmpSearch_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvEmpSearch"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvEmpSearch_PageIndexChanging"
                                            DataKeyNames="PERS_CODE" OnRowUpdating="gvEmpSearch_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ชื่อ" DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขบัตรประชาชน" DataField="PERS_CODE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขใบขับขี่ประเภท 4" DataField="LICENSE_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="อายุ" DataField="AGE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" Value='<%# Eval("SEMPLOYEEID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME" Value='<%# Eval("EMPNAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE" Value='<%# Eval("PERS_CODE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE" Value='<%# Eval("AGE") %>' />

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>


    <div class="modal fade" id="ShowPERS_CODE2Search" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updPERS_CODE2Search" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label6" runat="server" Text="ค้นหาพนักงาน"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtEmp2Search" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnEmp2Search" runat="server" Text="ค้นหา" OnClick="btnEmp2Search_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvEmp2Search"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvEmp2Search_PageIndexChanging"
                                            DataKeyNames="PERS_CODE" OnRowUpdating="gvEmp2Search_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ชื่อ" DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขบัตรประชาชน" DataField="PERS_CODE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขใบขับขี่ประเภท 4" DataField="LICENSE_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="อายุ" DataField="AGE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" Value='<%# Eval("SEMPLOYEEID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME" Value='<%# Eval("EMPNAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE" Value='<%# Eval("PERS_CODE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE" Value='<%# Eval("AGE") %>' />

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowPERS_CODE3Search" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updPERS_CODE3Search" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label7" runat="server" Text="ค้นหาพนักงาน"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtEmp3Search" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnEmp3Search" runat="server" Text="ค้นหา" OnClick="btnEmp3Search_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvEmp3Search"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvEmp3Search_PageIndexChanging"
                                            DataKeyNames="PERS_CODE" OnRowUpdating="gvEmp3Search_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ชื่อ" DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขบัตรประชาชน" DataField="PERS_CODE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขใบขับขี่ประเภท 4" DataField="LICENSE_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="อายุ" DataField="AGE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" Value='<%# Eval("SEMPLOYEEID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME" Value='<%# Eval("EMPNAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE" Value='<%# Eval("PERS_CODE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE" Value='<%# Eval("AGE") %>' />

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowPERS_CODE4Search" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updPERS_CODE4Search" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label8" runat="server" Text="ค้นหาพนักงาน"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtEmp4Search" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnEmp4Search" runat="server" Text="ค้นหา" OnClick="btnEmp4Search_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvEmp4Search"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvEmp4Search_PageIndexChanging"
                                            DataKeyNames="PERS_CODE" OnRowUpdating="gvEmp4Search_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="ชื่อ" DataField="EMPNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขบัตรประชาชน" DataField="PERS_CODE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="หมายเลขใบขับขี่ประเภท 4" DataField="LICENSE_NO" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                                <asp:BoundField HeaderText="อายุ" DataField="AGE" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSEMPLOYEEID" Value='<%# Eval("SEMPLOYEEID") %>' />
                                                        <asp:HiddenField runat="server" ID="hidEMPNAME" Value='<%# Eval("EMPNAME") %>' />
                                                        <asp:HiddenField runat="server" ID="hidPERS_CODE" Value='<%# Eval("PERS_CODE") %>' />
                                                        <asp:HiddenField runat="server" ID="hidAGE" Value='<%# Eval("AGE") %>' />

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseEmp" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="modal fade" id="ShowTTERMINALSearch" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <asp:UpdatePanel ID="updTTERMINALSearch" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id="Button1" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;</button><h4 class="modal-title">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Picture/imgInformation.png"
                                        Width="48" Height="48" />
                                    <asp:Label ID="Label3" runat="server" Text="ค้นหาคลังต้นทาง"></asp:Label></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="row form-group">
                                    <label class="col-md-4 control-label">ค้นหา</label>

                                    <div class="col-md-4">
                                        <asp:TextBox runat="server" ID="txtTTERMINALSearch" CssClass="form-control" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnTTERMINALSearch" runat="server" Text="ค้นหา" OnClick="btnTTERMINALSearch_Click"
                                            class="btn btn-md btn-hover btn-info" />
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <asp:GridView runat="server" ID="gvTTERMINALSearch"
                                            Width="100%" HeaderStyle-HorizontalAlign="Center"
                                            GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                                            ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White"
                                            HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]"
                                            AllowPaging="True" OnPageIndexChanging="gvTTERMINALSearch_PageIndexChanging"
                                            DataKeyNames="STERMINALID" OnRowUpdating="gvTTERMINALSearch_RowUpdating">
                                            <Columns>
                                                <asp:BoundField HeaderText="คลัง" DataField="SABBREVIATION" HtmlEncode="true" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

                                                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hidSABBREVIATION" Value='<%# Eval("SABBREVIATION") %>' />
                                                        <asp:HiddenField runat="server" ID="hidSTERMINALID" Value='<%# Eval("STERMINALID") %>' />

                                                        <div class="col-sm-1">
                                                            <asp:LinkButton ID="lnkChooseTERMINAL" runat="server" Text="เลือกข้อมูล" CommandName="update"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <uc1:ModelPopup runat="server" ID="mpSave" IDModel="ModalConfirmBeforeApprove" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSave_ClickOK" TextTitle="ยืนยันการบันทึก(กรณีแก้ไขข้อมูล)" TextDetail="คุณต้องการบันทึก(กรณีแก้ไขข้อมูล)ใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpSaveSend" IDModel="ModalConfirmBeforeSave" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpSaveSend_ClickOK" TextTitle="ยืนยันการบันทึกและส่งข้อมูล" TextDetail="คุณต้องการบันทึกและส่งข้อมูลใช่หรือไม่ ?" />
    <uc1:ModelPopup runat="server" ID="mpBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpBack_ClickOK" TextTitle="ยืนยันการยกเลิก" TextDetail="คุณต้องการยกเลิกใช่หรือไม่ ?" />
    <asp:HiddenField ID="hidID" runat="server" Value="0" />
    <asp:HiddenField ID="hidCactive" runat="server" />
    <asp:HiddenField ID="hidCGROUP" runat="server" />
    <script type="text/javascript">
        function SetEnabledControlByID(id) {
            if ($('#<%=hidCGROUP.ClientID%>').val() == '0') {
                $('#divREPORTER').addClass("hide");
            }
            if ($('#<%=hidCGROUP.ClientID%>').val() == '0') {
                $('#btnSave,#btnApprove').prop('disabled', true);
            }
            else if (id == '0' || id == '1') {
                $('#btnApprove').prop('disabled', true);
                $('#btnSave').prop('disabled', false);
            }
            else if (id == '2') {
                $('#btnApprove').prop('disabled', false);
                $('#btnSave').prop('disabled', true);
            }
            else {
                $('#btnSave,#btnApprove').prop('disabled', true);
            }

            var last = $('#<%= hidcollapse1.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse1").removeClass('in');
            }
            last = $('#<%= hidcollapse2.ClientID %>').val();
            if (last.indexOf("in") > -1) {
                $("#collapse2").removeClass('in');
            }
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(EndRequestHandler);
        $(document).ready(function () {

            fcollapse();
        });


        function EndRequestHandler(sender, args) {
            fcollapse();
        }
        function fcollapse() {
            $("#acollapse1").on('click', function () {
                var active = $("#collapse1").attr('class');
                //console.log(active);
                $('#<%= hidcollapse1.ClientID %>').val(active);
            });
            $("#acollapse2").on('click', function () {
                var active = $("#collapse2").attr('class');
                //console.log(active);
                $('#<%= hidcollapse2.ClientID %>').val(active);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>

