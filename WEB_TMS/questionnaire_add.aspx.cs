﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Data;
using System.Data.OracleClient;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;

public partial class questionnaire_add : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    const string TempDirectory = "UploadFile/questionnaire/{0}/{2}/{1}/";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Session["CheckPermission"] = null;
            string AddEdit = "";
            bool chkurl = false;
            if (Session["cPermission"] != null)
            {
                string[] url = (Session["cPermission"] + "").Split('|');
                string[] chkpermision;
                bool sbreak = false;

                foreach (string inurl in url)
                {
                    chkpermision = inurl.Split(';');
                    if (chkpermision[0] == "93")
                    {
                        switch (chkpermision[1])
                        {
                            case "0":
                                chkurl = false;

                                break;
                            case "1":
                                chkurl = true;

                                break;

                            case "2":
                                chkurl = true;
                                AddEdit = "1";
                                break;
                        }
                        sbreak = true;
                    }

                    if (sbreak == true) break;
                }
            }

            if (chkurl == false)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            }

            Session["CheckPermission"] = AddEdit;

            var dt1 = new List<DT1>();
            var dt2 = new List<DT2>();
            var dtFile = new List<dtFile>();
            Session["dtFile"] = dtFile;

            for (int i = 1; i <= 3; i++)
            {
                dt1.Add(new DT1
                {
                    dtsID = i,
                    dtsName = "",
                });
            }

            for (int i = 1; i <= 3; i++)
            {
                dt2.Add(new DT2
                {
                    dtsID = i,
                    dtsName = "",
                });
            }

            sgvw.DataSource = dt1;
            sgvw.DataBind();

            sgvw1.DataSource = dt2;
            sgvw1.DataBind();

            Session["dt1"] = dt1;
            Session["dt2"] = dt2;

            grid.DataBind();
            grid.DetailRows.ExpandAllRows();
        }
    }

    void ClearControl()
    {



    }


    protected void xcpn_Load(object sender, EventArgs e)
    {

    }
    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {
        string[] paras = e.Parameter.Split(';');
        if ("" + Session["CheckPermission"] == "1")
        {
            switch (paras[0])
            {


                case "Save":
                    string GenID;
                    using (OracleConnection con = new OracleConnection(sql))
                    {
                        if (con.State == ConnectionState.Closed) con.Open();
                        DateTime dd;
                        string strsql = @"INSERT INTO TQUESTIONNAIRE (NQUESTIONNAIREID, SVENDORID, DCHECK, SADDRESS, SREMARK, DCREATE, SCREATE, DUPDATE, SUPDATE,NVALUE,NNO) VALUES (:NQUESTIONNAIREID,:SVENDORID,:DCHECK,:SADDRESS,:SREMARK,SYSDATE,:SCREATE,SYSDATE,:SUPDATE, :NVALUE, :NNO)";
                        GenID = CommonFunction.Gen_ID(con, "SELECT NQUESTIONNAIREID FROM (SELECT NQUESTIONNAIREID FROM TQUESTIONNAIRE ORDER BY NQUESTIONNAIREID DESC) WHERE ROWNUM <= 1");
                        string GenNO = CommonFunction.Gen_ID(con, "SELECT NNO FROM (SELECT NNO FROM TQUESTIONNAIRE WHERE To_Char(DCHECK,'YYYY') = '" + (DateTime.TryParse(dteDateStart1.Value + "", out dd) ? dd : DateTime.Today).Year + "'  AND SVENDORID = '" + cboVendor.Value + "' ORDER BY NNO DESC) WHERE ROWNUM <= 1");


                        if (Convert.ToInt16(GenNO) > 2)
                        {
                            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ระบบอนุญาตให้ทำการประเมินสถานประกอบการได้ ปีละไม่เกิน 2 ครั้ง')");
                            return;
                        }

                        using (OracleCommand com = new OracleCommand(strsql, con))
                        {

                            com.Parameters.Clear();
                            com.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                            com.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = cboVendor.Value + "";
                            com.Parameters.Add(":DCHECK", OracleType.DateTime).Value = dteDateStart1.Value + "";
                            com.Parameters.Add(":SADDRESS", OracleType.VarChar).Value = txtAddress.Text;
                            com.Parameters.Add(":SREMARK", OracleType.VarChar).Value = txtDetail.Text;
                            com.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = Session["UserID"] + "";
                            com.Parameters.Add(":NVALUE", OracleType.Number).Value = txtTotal.Text;
                            com.Parameters.Add(":NNO", OracleType.Number).Value = GenNO;
                            com.ExecuteNonQuery();
                        }

                        string strsql1 = @"INSERT INTO TQUESTIONNAIRE_BY ( NQUESTIONNAIREID, NID, SNAME,  DCREATE, SCREATE)
VALUES ( :NQUESTIONNAIREID , :NID ,:SNAME , SYSDATE , :SCREATE  )";

                        for (int i = 0; i < sgvw.VisibleRowCount; i++)
                        {

                            using (OracleCommand com1 = new OracleCommand(strsql1, con))
                            {
                                string GenID1 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_BY ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                                com1.Parameters.Add(":NID", OracleType.Number).Value = GenID1;
                                com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "";
                                com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com1.ExecuteNonQuery();
                            }
                        }

                        string strsql2 = @"INSERT INTO TQUESTIONNAIRE_AS ( NID, NQUESTIONNAIREID, SNAME,
                  DCREATE, SCREATE) VALUES (:NID ,:NQUESTIONNAIREID ,:SNAME, SYSDATE , :SCREATE)";

                        for (int i = 0; i < sgvw1.VisibleRowCount; i++)
                        {

                            using (OracleCommand com1 = new OracleCommand(strsql2, con))
                            {
                                string GenID2 = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIRE_AS ORDER BY NID DESC) WHERE ROWNUM <= 1");
                                com1.Parameters.Clear();
                                com1.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                                com1.Parameters.Add(":NID", OracleType.Number).Value = GenID2;
                                com1.Parameters.Add(":SNAME", OracleType.VarChar).Value = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "";
                                com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"] + "";
                                com1.ExecuteNonQuery();
                            }
                        }

                        string strsql3 = @"INSERT INTO TQUESTIONNAIREIMPORTFILE (NID, NQUESTIONNAIREID, SEVIDENCENAME,SFILENAME, SGENFILENAME, SFILEPATH,  SCREATE, DUPDATE) 
                         VALUES (:NID ,:NQUESTIONNAIREID,:SEVIDENCENAME ,:SFILENAME,:SGENFILENAME,:SFILEPATH,:SCREATE,SYSDATE )";

                        var listImport = (List<dtFile>)Session["dtFile"];

                        foreach (var list in listImport)
                        {
                            string GenIDImport = CommonFunction.Gen_ID(con, "SELECT NID FROM (SELECT NID FROM TQUESTIONNAIREIMPORTFILE ORDER BY NID DESC) WHERE ROWNUM <= 1");
                            using (OracleCommand com2 = new OracleCommand(strsql3, con))
                            {
                                com2.Parameters.Clear();
                                com2.Parameters.Add(":NID", OracleType.Number).Value = GenIDImport;
                                com2.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                                com2.Parameters.Add(":SEVIDENCENAME", OracleType.VarChar).Value = list.dtEvidenceName;
                                com2.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = list.dtFileName;
                                com2.Parameters.Add(":SGENFILENAME", OracleType.VarChar).Value = list.dtGenFileName;
                                com2.Parameters.Add(":SFILEPATH", OracleType.VarChar).Value = list.dtFilePath;
                                com2.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                com2.ExecuteNonQuery();
                            }
                        }

                        string strsql4 = @"INSERT INTO TQUESTIONNAIRELIST (NQUESTIONNAIREID, NVISITFORMID, NGROUPID,
                         NTYPEVISITFORMID, NVALUE, SREMARK, SCREATE, DCREATE,NVALUEITEM) VALUES (:NQUESTIONNAIREID ,:NVISITFORMID ,:NGROUPID ,:NTYPEVISITFORMID ,:NVALUE ,:SREMARK ,:SCREATE ,SYSDATE ,:NVALUEITEM)";

                        for (int i = 0; i < grid.VisibleRowCount; i++)
                        {
                            ASPxGridView detailGrid = grid.FindDetailRowTemplateControl(i, "detailGrid") as ASPxGridView;
                            for (int o = 0; o < detailGrid.VisibleRowCount; o++)
                            {
                                using (OracleCommand com3 = new OracleCommand(strsql4, con))
                                {
                                    dynamic DATA = detailGrid.GetRowValues(o, "NVISITFORMID", "NGROUPID", "NTYPEVISITFORMID");
                                    com3.Parameters.Clear();
                                    com3.Parameters.Add(":NQUESTIONNAIREID", OracleType.Number).Value = GenID;
                                    com3.Parameters.Add(":NVISITFORMID", OracleType.Number).Value = DATA[0] + "";
                                    com3.Parameters.Add(":NGROUPID", OracleType.Number).Value = DATA[1] + "";
                                    com3.Parameters.Add(":NTYPEVISITFORMID", OracleType.Number).Value = DATA[2] + "";
                                    com3.Parameters.Add(":NVALUE", OracleType.Number).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtOldSum")).Text + "";
                                    com3.Parameters.Add(":SREMARK", OracleType.VarChar).Value = ((ASPxTextBox)detailGrid.FindRowCellTemplateControl(o, null, "txtRemark")).Text + "";
                                    com3.Parameters.Add(":NVALUEITEM", OracleType.Number).Value = ((ASPxRadioButtonList)detailGrid.FindRowCellTemplateControl(o, null, "rblWEIGHT")).Value + "";
                                    com3.Parameters.Add(":SCREATE", OracleType.VarChar).Value = Session["UserID"];
                                    com3.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    LogUser("28", "I", "บันทึกข้อมูลหน้า ประเมินสถานประกอบการ", GenID + "");

                    CommonFunction.SetPopupOnLoad(xcpn, "dxInfoRedirect('" + Resources.CommonResource.Msg_Alert_Title_Complete + "','" + Resources.CommonResource.Msg_SaveComplete + "',function(){window.location='questionnaire.aspx';});");
                    ClearControl();

                    break;

                #region " Comment Old "
                //case "AddClick":
                //    var sdt1 = (List<DT1>)Session["dt1"];

                //    if (sdt1.Count > 0)
                //    {
                //        int id = sdt1.Count;
                //        sdt1.Clear();

                //        int indexgvw = sgvw.VisibleRowCount - 1;

                //        for (int i = 0; i <= indexgvw; i++)
                //        {
                //            sdt1.Add(new DT1
                //            {
                //                dtsID = Convert.ToInt32(sgvw.GetRowValues(i, "dtsID")),
                //                dtsName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "",
                //            });
                //        }
                //    }

                //    int maxQ = (sdt1.Count > 0) ? sdt1.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                //    sdt1.Add(new DT1
                //    {
                //        dtsID = maxQ,
                //        dtsName = "",
                //    });

                //    sgvw.DataSource = sdt1;
                //    sgvw.DataBind();

                //    Session["dt1"] = sdt1;
                //    break;

                //case "deleteList":

                //    if (sgvw.VisibleRowCount > 1)
                //    {
                //        var delsdt1 = (List<DT1>)Session["dt1"];
                //        delsdt1.RemoveAt(Convert.ToInt32(paras[1]));

                //        sgvw.DataSource = delsdt1;
                //        sgvw.DataBind();

                //        Session["dt1"] = delsdt1;
                //    }

                //    break;

                //case "AddClick1":
                //    var sdt2 = (List<DT2>)Session["dt2"];

                //    if (sdt2.Count > 0)
                //    {
                //        int id = sdt2.Count;
                //        sdt2.Clear();

                //        int indexgvw = sgvw1.VisibleRowCount - 1;

                //        for (int i = 0; i <= indexgvw; i++)
                //        {
                //            sdt2.Add(new DT2
                //            {
                //                dtsID = Convert.ToInt32(sgvw1.GetRowValues(i, "dtsID")),
                //                dtsName = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "",
                //            });
                //        }
                //    }

                //    int maxQ2 = (sdt2.Count > 0) ? sdt2.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                //    sdt2.Add(new DT2
                //    {
                //        dtsID = maxQ2,
                //        dtsName = "",
                //    });

                //    sgvw1.DataSource = sdt2;
                //    sgvw1.DataBind();

                //    Session["dt2"] = sdt2;
                //    break;

                //case "deleteList1":

                //    if (sgvw1.VisibleRowCount > 1)
                //    {
                //        var delsdt2 = (List<DT2>)Session["dt2"];
                //        delsdt2.RemoveAt(Convert.ToInt32(paras[1]));

                //        sgvw1.DataSource = delsdt2;
                //        sgvw1.DataBind();

                //        Session["dt2"] = delsdt2;
                //    }

                //    break;

                //case "Upload":
                //    string dtr = txtEvidence.Text;
                //    var addData = (List<dtFile>)Session["dtFile"];
                //    int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                //    var fileupload = (Session["FileUpload"] + "").Split(';');

                //    if (fileupload.Count() == 3)
                //    {
                //        addData.Add(new dtFile
                //        {
                //            dtID = max,
                //            dtEvidenceName = txtEvidence.Text,
                //            dtFileName = fileupload[0],
                //            dtGenFileName = fileupload[1],
                //            dtFilePath = fileupload[2]
                //        });

                //        Session["dtFile"] = addData;
                //        sgvwFile.DataSource = addData;
                //        sgvwFile.DataBind();
                //    }
                //    break;

                //case "DeleteFileList":

                //    var deldt = (List<dtFile>)Session["dtFile"];
                //    int index1 = Convert.ToInt32(paras[1]);
                //    string FilePath1 = sgvwFile.GetRowValues(index1, "dtFilePath") + "";

                //    if (File.Exists(Server.MapPath("./") + FilePath1.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePath1.Replace("/", "\\"));
                //    }

                //    deldt.RemoveAt(index1);

                //    sgvwFile.DataSource = deldt;
                //    sgvwFile.DataBind();
                //    Session["dtFile"] = deldt;

                //    break;

                //case "ViewFileList":

                //    int index = Convert.ToInt32(paras[1]);
                //    string FilePath = sgvwFile.GetRowValues(index, "dtFilePath") + "";

                //    xcpn.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + FilePath;

                //    break;

                #endregion " Comment Old "


            }
        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }

    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            string[] _Filename = e.UploadedFile.FileName.Split('.');

            if (_Filename[0] != "")
            {
                ASPxUploadControl upl = (ASPxUploadControl)sender;
                int count = _Filename.Count() - 1;

                string genName = "questionnaire" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
                if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
                {
                    string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                    e.CallbackData = data;

                    Session["FileUpload"] = e.UploadedFile.FileName + ";" + genName + "." + _Filename[1] + ";" + data;

                }
            }
            else
            {

                return;

            }
        }
        catch (Exception ex)
        {
            e.IsValid = false;
            e.ErrorText = ex.Message;
        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    protected void cboVendor_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsVendor.SelectCommand = @"SELECT SVENDORID,SVENDORNAME FROM (SELECT ROW_NUMBER()OVER(ORDER BY v.SVENDORID) AS RN , v.SVENDORID, v.SVENDORNAME FROM TVENDOR_SAP v WHERE nvl(v.SVENDORID,'Y') != 'N' AND v.SVENDORNAME LIKE :fillter )
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVendor.SelectParameters.Clear();
        sdsVendor.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVendor.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVendor.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVendor;
        comboBox.DataBind();

    }
    protected void cboVendor_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    }

    protected void gvw_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "ที่") e.DisplayText = "" + (e.VisibleRowIndex + 1) + ".";
    }

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        Session["ONGROUPID"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    [Serializable]
    struct DT1
    {
        public int dtsID { get; set; }
        public string dtsName { get; set; }
    }

    struct DT2
    {
        public int dtsID { get; set; }
        public string dtsName { get; set; }
    }

    struct dtFile
    {
        public int dtID { get; set; }
        public string dtEvidenceName { get; set; }
        public string dtFileName { get; set; }
        public string dtGenFileName { get; set; }
        public string dtFilePath { get; set; }
    }
    protected void detailGrid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "#")
        {
            string keyValue = ((sender) as ASPxGridView).GetMasterRowKeyValue() + "";
            int VisibleIndex = e.VisibleIndex;
            ASPxRadioButtonList rblWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "rblWEIGHT") as ASPxRadioButtonList;
            ASPxTextBox txtWEIGHT = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtWEIGHT") as ASPxTextBox;
            ASPxTextBox txtOldSum = ((sender) as ASPxGridView).FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtOldSum") as ASPxTextBox;

            rblWEIGHT.ClientInstanceName = rblWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            txtWEIGHT.ClientInstanceName = txtWEIGHT.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;
            txtOldSum.ClientInstanceName = txtOldSum.ClientInstanceName + "_" + VisibleIndex + "_" + keyValue;

            rblWEIGHT.ClientSideEvents.SelectedIndexChanged = "function (s,e) {var sum = (" + rblWEIGHT.ClientInstanceName + ".GetValue() * " + txtWEIGHT.ClientInstanceName + ".GetValue()) ;txtTotal.SetValue((txtTotal.GetValue() - " + txtOldSum.ClientInstanceName + ".GetValue()) + sum); " + txtOldSum.ClientInstanceName + ".SetValue(sum)}";

            double num = 0.0;
            double sum = 0.0;
            double value = 0.0;
            double total = 0.0;
            double WEIGHT = double.TryParse("" + e.GetValue("NWEIGHT"), out num) ? num : 0.0;

            if (rblSelectAll.SelectedIndex == 0)
            {

                rblWEIGHT.SelectedIndex = 0;
                value = double.TryParse("" + rblWEIGHT.Value, out num) ? num : 0.0;
                sum = WEIGHT * value;
                txtOldSum.Text = sum + "";

                total = double.TryParse("" + txtTotal.Text, out num) ? num : 0.0;
                total = total + sum;
                txtTotal.Text = total + "";
            }
            else if (rblSelectAll.SelectedIndex == 1)
            {
                rblWEIGHT.SelectedIndex = 1;
                value = double.TryParse("" + rblWEIGHT.Value, out num) ? num : 0.0;
                sum = WEIGHT * value;
                txtOldSum.Text = sum + "";

                total = double.TryParse("" + txtTotal.Text, out num) ? num : 0.0;
                total = total + sum;
                txtTotal.Text = total + "";
            }
            else if (rblSelectAll.SelectedIndex == 2)
            {
                rblWEIGHT.SelectedIndex = 2;
                value = double.TryParse("" + rblWEIGHT.Value, out num) ? num : 0.0;
                sum = WEIGHT * value;
                txtOldSum.Text = sum + "";

                total = double.TryParse("" + txtTotal.Text, out num) ? num : 0.0;
                total = total + sum;
                txtTotal.Text = total + "";
            }

        }
    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, sql);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }
    protected void sgvwFile_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] data = e.Args;
        string[] Para = data[0].Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (Para[0])
            {
                case "Upload":
                    string dtr = txtEvidence.Text;
                    var addData = (List<dtFile>)Session["dtFile"];
                    int max = (addData.Count > 0) ? addData.Select(s => s.dtID).OrderByDescending(o => o).First() + 1 : 1;
                    var fileupload = (Session["FileUpload"] + "").Split(';');

                    if (fileupload.Count() == 3)
                    {
                        addData.Add(new dtFile
                        {
                            dtID = max,
                            dtEvidenceName = txtEvidence.Text,
                            dtFileName = fileupload[0],
                            dtGenFileName = fileupload[1],
                            dtFilePath = fileupload[2]
                        });

                        Session["dtFile"] = addData;
                        sgvwFile.DataSource = addData;
                        sgvwFile.DataBind();
                    }
                    break;

                case "DeleteFileList":

                    var deldt = (List<dtFile>)Session["dtFile"];
                    int index1 = Convert.ToInt32(Para[1]);
                    string FilePath1 = sgvwFile.GetRowValues(index1, "dtFilePath") + "";

                    if (File.Exists(Server.MapPath("./") + FilePath1.Replace("/", "\\")))
                    {
                        File.Delete(Server.MapPath("./") + FilePath1.Replace("/", "\\"));
                    }

                    deldt.RemoveAt(index1);

                    sgvwFile.DataSource = deldt;
                    sgvwFile.DataBind();
                    Session["dtFile"] = deldt;

                    break;

                case "ViewFileList":

                    int index = Convert.ToInt32(Para[1]);
                    string FilePath = sgvwFile.GetRowValues(index, "dtFilePath") + "";

                    sgvwFile.JSProperties["cpRedirectOpen"] = "openFile.aspx?str=" + FilePath;

                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
    protected void sgvw1_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] data = e.Args;
        string[] Para = data[0].Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (Para[0])
            {
                case "AddClick1":
                    var sdt2 = (List<DT2>)Session["dt2"];

                    if (sdt2.Count > 0)
                    {
                        int id = sdt2.Count;
                        sdt2.Clear();

                        int indexgvw = sgvw1.VisibleRowCount - 1;

                        for (int i = 0; i <= indexgvw; i++)
                        {
                            sdt2.Add(new DT2
                            {
                                dtsID = Convert.ToInt32(sgvw1.GetRowValues(i, "dtsID")),
                                dtsName = ((ASPxTextBox)sgvw1.FindRowCellTemplateControl(i, null, "txtName1")).Text + "",
                            });
                        }
                    }

                    int maxQ2 = (sdt2.Count > 0) ? sdt2.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                    sdt2.Add(new DT2
                    {
                        dtsID = maxQ2,
                        dtsName = "",
                    });

                    sgvw1.DataSource = sdt2;
                    sgvw1.DataBind();

                    Session["dt2"] = sdt2;
                    break;

                case "deleteList1":

                    if (sgvw1.VisibleRowCount > 1)
                    {
                        var delsdt2 = (List<DT2>)Session["dt2"];
                        delsdt2.RemoveAt(Convert.ToInt32(Para[1]));

                        sgvw1.DataSource = delsdt2;
                        sgvw1.DataBind();

                        Session["dt2"] = delsdt2;
                    }

                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
    protected void sgvw_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {
        string[] data = e.Args;
        string[] Para = data[0].Split(';');

        if ("" + Session["CheckPermission"] == "1")
        {
            switch (Para[0])
            {
                case "AddClick":
                    var sdt1 = (List<DT1>)Session["dt1"];

                    if (sdt1.Count > 0)
                    {
                        int id = sdt1.Count;
                        sdt1.Clear();

                        int indexgvw = sgvw.VisibleRowCount - 1;

                        for (int i = 0; i <= indexgvw; i++)
                        {
                            sdt1.Add(new DT1
                            {
                                dtsID = Convert.ToInt32(sgvw.GetRowValues(i, "dtsID")),
                                dtsName = ((ASPxTextBox)sgvw.FindRowCellTemplateControl(i, null, "txtName")).Text + "",
                            });
                        }
                    }

                    int maxQ = (sdt1.Count > 0) ? sdt1.Select(s => s.dtsID).OrderByDescending(o => o).First() + 1 : 1;

                    sdt1.Add(new DT1
                    {
                        dtsID = maxQ,
                        dtsName = "",
                    });

                    sgvw.DataSource = sdt1;
                    sgvw.DataBind();

                    Session["dt1"] = sdt1;
                    break;

                case "deleteList":

                    if (sgvw.VisibleRowCount > 1)
                    {
                        var delsdt1 = (List<DT1>)Session["dt1"];
                        delsdt1.RemoveAt(Convert.ToInt32(Para[1]));

                        sgvw.DataSource = delsdt1;
                        sgvw.DataBind();

                        Session["dt1"] = delsdt1;
                    }

                    break;
            }

        }
        else
        {
            CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','คุณไม่ได้รับสิทธิ์ในการบันทึกหรือแก้ไขรายการ!');");
        }
    }
}