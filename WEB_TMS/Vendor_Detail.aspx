﻿    <%@ Page Title="" MasterPageFile="~/Mp.Master" Language="C#" AutoEventWireup="true"
    CodeFile="Vendor_Detail.aspx.cs" Inherits="Vendor_Detail" StylesheetTheme="Aqua" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
    <style type="text/css">
        .style14 {
            height: 25px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" OnLoad="xcpn_Load">
        <ClientSideEvents EndCallback="function(s,e){eval(s.cpPopup); s.cpPopup = '';if(s.cpRedirectTo != undefined) window.location = s.cpRedirectTo;inEndRequestHandler();}" />
        <PanelCollection>
            <dx:PanelContent>
                <script src="Javascript/DevExpress/1_27.js" type="text/javascript"></script>
                <script src="Javascript/DevExpress/2_15.js" type="text/javascript"></script>
                <script>
                    function comfirmcilck(s, e) {

                        dxConfirm('แจ้งเตือน', 'ท่านต้องการยกเลิกการจ้างงานใช่หรอไม่ <br/><span style="color:Red;">*** หากกด เลิกจ้างงานไปแล้ว ข้อมูลของพนักงานจะไม่ปรากฎให้เห็นในหน้านี้แล้ว</span>' ,function(ss,ee){ dxPopupConfirm.Hide(); 
                            if(!xcpn.InCallback()) 
                                xcpn.PerformCallback('ChkInUse;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));
                        } ,function(ss,ee){ dxPopupConfirm.Hide(); });
                              
                    }
                    function runScript1(e) {    
                        if (e.keyCode == 13) {
                            $("#cph_Main_xcpn_ASPxPageControl1_btnSearch_CD").click(); //jquery
                            document.getElementById("cph_Main_xcpn_ASPxPageControl1_btnSearch_CD").click(); //javascript
                        }
                    }
                    function runScript2(e) {
                        if (e.keyCode == 13) {
                            $("#cph_Main_xcpn_ASPxPageControl1_ASPxButton1_CD").click(); //jquery
                            document.getElementById("cph_Main_xcpn_ASPxPageControl1_ASPxButton1_CD").click(); //javascript
                        }
                    }
                    function runScript3(e) {
                        if (e.keyCode == 13) {
                            $("#cph_Main_xcpn_ASPxPageControl1_btnSearch2_CD").click(); //jquery
                            document.getElementById("cph_Main_xcpn_ASPxPageControl1_btnSearch2_CD").click(); //javascript
                        }
                    }
                    function runScript4(e) {
                        if (e.keyCode == 13) {
                            $("#cph_Main_xcpn_ASPxPageControl1_btnSearch1_CD").click(); //jquery
                            document.getElementById("cph_Main_xcpn_ASPxPageControl1_btnSearch1_CD").click(); //javascript
                        }
                    } 
                </script>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#0E4999">
                            <img src="images/spacer.GIF" width="250px" height="1px">
                            <dx:ASPxTextBox ID="txtVendorID" runat="server" Width="200px" ClientVisible="false">
                            </dx:ASPxTextBox>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="3" cellspacing="2" style="margin-right: 0px">
                    <tr>
                        <td bgcolor="#FFFFFF">
                            <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">
                                <TabPages>
                                    <dx:TabPage Name="Tab1" Text="ข้อมูลผู้ขนส่ง">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                    <tr class="ShowText">
                                                        <td align="left" bgcolor="#D0E4ED" height="25" style="text-align: left" width="19%">
                                                            <span class="style24">ชื่อ</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" style="text-align: left; width: 69%;">
                                                            <dx:ASPxLabel ID="lblName" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#D0E4ED" class="style14" width="19%">
                                                            <span class="style24">ชื่อย่อบริษัท</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style14">
                                                            <dx:ASPxLabel ID="lblSubname" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#D0E4ED" height="25" width="19%">
                                                            <span class="style24 style24">ที่อยู่</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF">
                                                            <dx:ASPxLabel ID="lblAddress" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#D0E4ED" class="style14" width="19%">
                                                            <span class="style24">หมายเลขโทรศัพท์</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF" class="style14">
                                                            <dx:ASPxLabel ID="lblTelephone" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" bgcolor="#D0E4ED" height="25" width="19%">
                                                            <span class="style24">หมายเลขโทรสาร</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF">
                                                            <dx:ASPxLabel ID="lblFax" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" bgcolor="#D0E4ED" height="25" width="19%">
                                                            <span class="style24">อีเมล์</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF">
                                                            <dx:ASPxLabel ID="lblEmail" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#D0E4ED" height="25" width="19%">
                                                            <span class="style24">ชื่อผู้ประสานงาน</span>
                                                        </td>
                                                        <td align="left" bgcolor="#FFFFFF">
                                                            <span class="style24"></span>
                                                            <dx:ASPxLabel ID="lblCo" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#D0E4ED" height="25" width="19%">
                                                            <span class="style24">วันที่เริ่มต้นทำงานกับปตท.</span>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <span class="style24"></span>
                                                            <dx:ASPxLabel ID="lblDate" runat="server" Text="">
                                                            </dx:ASPxLabel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <dx:ASPxButton runat="server" ID="btnEditT1" AutoPostBack="false" Text="ยื่นคำร้อง">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('EditT1'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="Tab2" Text="ข้อมูลสัญญา">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="25%"></td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtSearch" runat="server" Width="200px" NullText="ค้นหาจากเลขที่สัญญา, ประเภทสัญญา " onkeypress="runScript1(event)">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <%--<dx:ASPxDateEdit ID="dteStart" runat="server" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dteStart" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td>-
                                                        </td>
                                                        <td>
                                                            <%--<dx:ASPxDateEdit ID="dteEnd" runat="server" SkinID="xdte">
                                                            </dx:ASPxDateEdit>--%>
                                                            <asp:TextBox ID="dteEnd" runat="server" CssClass="datepicker" Style="text-align: center"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxComboBox ID="cboGroup" runat="server" Width="120px" SelectedIndex="0">
                                                                <Items>
                                                                    <dx:ListEditItem Text="อยู่ในสัญญา" Value="Y" />
                                                                    <dx:ListEditItem Text="หมดอายุสัญญา" Value="N" />
                                                                </Items>
                                                            </dx:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnSearch" runat="server" SkinID="_search">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7">
                                                            <dx:ASPxGridView ID="gvw" runat="server" AutoGenerateColumns="False" EnableCallBacks="true"
                                                                Style="margin-top: 0px" ClientInstanceName="gvw" Width="100%" KeyFieldName="SCONTRACTID"
                                                                SkinID="_gvw" DataSourceID="sds">
                                                                <Columns>
                                                                    <dx:GridViewDataColumn FieldName="SVENDORID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัส" VisibleIndex="0" FieldName="SCONTRACTID"
                                                                        Visible="false">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" VisibleIndex="1" Width="20%" FieldName="SCONTRACTNO">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ประเภทสัญญา" HeaderStyle-HorizontalAlign="Center"
                                                                        Width="10%" VisibleIndex="2" FieldName="SCONTRACTTYPENAME">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="วันที่เริ่มสัญญา" VisibleIndex="3" Width="10%"
                                                                        HeaderStyle-HorizontalAlign="Center" FieldName="DBEGIN">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataDateColumn Caption="วันสิ้นสุดสัญญา" VisibleIndex="4" Width="10%"
                                                                        HeaderStyle-HorizontalAlign="Center" FieldName="DEND">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataDateColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รถในสัญญา" VisibleIndex="5" HeaderStyle-HorizontalAlign="Center"
                                                                        Width="5%" FieldName="NTRUCK">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รถสำรอง" VisibleIndex="6" HeaderStyle-HorizontalAlign="Center"
                                                                        Width="5%" FieldName="NSTANDBYTRUCK">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="สถานะสัญญา" VisibleIndex="7" HeaderStyle-HorizontalAlign="Center"
                                                                        FieldName="SCD" Width="10%">
                                                                        <DataItemTemplate>
                                                                            <asp:Label ID="dd" runat="server" Text='<%# Eval("CACTIVE").ToString()== "Y"?"Active":"InActive"%>'></asp:Label>
                                                                        </DataItemTemplate>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ผู้ประสานงานสัญญา" VisibleIndex="8" HeaderStyle-HorizontalAlign="Center"
                                                                        FieldName="SCONAMEAPPEND" Width="14%">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataColumn Width="11%" CellStyle-Cursor="hand" VisibleIndex="9">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxButton ID="imbedit" runat="server" AutoPostBack="false" Cursor="pointer"
                                                                                Text="ยื่นคำร้อง">
                                                                                <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('view;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                                <%--    <Image Height="16px" Url="Images/search.png" Width="16px">
                                                                                </Image>--%>
                                                                            </dx:ASPxButton>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <EditForm>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table width="85%" border="0" cellpadding="2" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="4" align="left" background="images/intra_dpy020_b.gif" bgcolor="#FFFFFF">
                                                                                                <span class="style26">รายละเอียดสัญญา</span><span class="style26"></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">รหัสบริษัทคู่สัญญา</span>
                                                                                            </td>
                                                                                            <td width="19%" align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SVENDORID")%></span>
                                                                                            </td>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ชื่อบริษัทคู่สัญญา</span>
                                                                                            </td>
                                                                                            <td width="32%" align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SVENDORNAME")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">สัญญาเลขที่</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SCONTRACTNO")%></span>
                                                                                            </td>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ประเภทสัญญาจ้าง</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SCONTRACTTYPENAME")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">วันที่เริ่มสัญญา</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("DBEGIN","{0:dd/MM/yyyy}")%></span>
                                                                                            </td>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">วันที่สิ้นสุดสัญญา</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("DEND", "{0:dd/MM/yyyy}")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">จุดขนส่ง</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SABBREVIATION")%></span>
                                                                                            </td>
                                                                                            <td width="15%" bgcolor="#FFEBD7">
                                                                                                <span class="style24">สถานะสัญญา</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("CACTIVE").ToString()== "Y"?"Active":"InActive"%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="85%" border="0" cellpadding="2" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="5" align="left" class="style26" background="images/intra_dpy020_b.gif">หลักประกันสัญญา
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="5%" align="center" bgcolor="#FFEBD7" class="style24">ที่
                                                                                            </td>
                                                                                            <td width="19%" align="center" bgcolor="#FFEBD7" class="style24">ประเภท
                                                                                            </td>
                                                                                            <td width="42%" align="center" bgcolor="#FFEBD7" class="style24">เลขที่หนังสือ
                                                                                            </td>
                                                                                            <td width="15%" align="center" bgcolor="#FFEBD7" class="style24">จำนวนเงิน
                                                                                            </td>
                                                                                            <td width="17%" align="center" bgcolor="#FFEBD7" class="style24">หมายเหตุ
                                                                                            </td>
                                                                                        </tr>
                                                                                        <asp:Literal ID="ltlGuarantee" runat="server"></asp:Literal>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width="85%" border="0" cellpadding="2" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="8" align="left" bgcolor="#FFFFFF" class="style26" background="images/intra_dpy020_b.gif">รถในสัญญา
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="3%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ที่</span>
                                                                                            </td>
                                                                                            <td width="10%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ทะเบียนรถ (หัว) </span>
                                                                                            </td>
                                                                                            <td width="10%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ทะเบียนรถ (ท้าย) </span>
                                                                                            </td>
                                                                                            <td width="12%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ประเภทรถ</span>
                                                                                            </td>
                                                                                            <td width="12%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">รูปแบบรถ</span>
                                                                                            </td>
                                                                                            <td width="15%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">หมายเลขแชชซีย์</span>
                                                                                            </td>
                                                                                            <td width="7%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ความจุรวม</span>
                                                                                            </td>
                                                                                            <td width="7%" align="center" bgcolor="#FFEBD7">
                                                                                                <span class="style24">จำนวนช่อง</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <asp:Literal ID="ltlTruck" runat="server"></asp:Literal>
                                                                                        <tr>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="8" align="right">
                                                                                                <dx:ASPxButton ID="btnReq" runat="server" Text="ส่งคำร้อง" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                                </dx:ASPxButton>
                                                                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){gvw.CancelEdit() ;}" />
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EditForm>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME ,C.DBEGIN, C.DEND, C.NTRUCK,SUM(CASE WHEN  NVL(TCT.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END) NSTANDBYTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ,REPLACE(wm_concat(' ' || CTR.SCOORDINATORNAME),',',NULL) As SCONAMEAPPEND 
                                        FROM (TCONTRACT C LEFT JOIN TCONTRACTTYPE CT ON C.SCONTRACTTYPEID = CT.SCONTRACTTYPEID LEFT JOIN TVENDOR_SAP V ON C.SVENDORID = V.SVENDORID LEFT JOIN TTERMINAL T ON C.STERMINALID = T.STERMINALID)
                                        LEFT JOIN TCO_CONTRACT CTR ON C.SCONTRACTID = CTR.SCONTRACTID LEFT JOIN TCONTRACT_TRUCK TCT ON C.SCONTRACTID = TCT.SCONTRACTID
                                        WHERE 1 = 1  AND c.SVENDORID = :oSVENDORID AND c.cActive = 'Y'  
                                         GROUP BY C.SCONTRACTID,C.SVENDORID,C.SCONTRACTNO, CT.SCONTRACTTYPENAME, C.DBEGIN, C.DEND, C.NTRUCK, C.CACTIVE, T.SABBREVIATION, V.SVENDORNAME ">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter Name="oSVENDORID" ControlID="txtVendorID" PropertyName="Text" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="Tab3" Text="ข้อมูลรถ">
                                        <ContentCollection>
                                            <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="630px">
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtvehicle" runat="server" Width="210px" NullText="ค้นหาจากทะเบียนรถ, หมายเลขแชชซีย์" onkeypress="runScript2(event)">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="ASPxButton1" runat="server" SkinID="_search">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('SearchVehicle'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <dx:ASPxGridView ID="vehiclegrid" runat="server" AutoGenerateColumns="False" ClientInstanceName="vehiclegrid"
                                                                DataSourceID="dts1" KeyFieldName="STRUCKID" SkinID="_gvw" Style="margin-top: 0px"
                                                                Width="100%" OnHtmlRowPrepared="vehiclegrid_HtmlRowPrepared">
                                                                <Columns>
                                                                    
                                                                    <dx:GridViewDataTextColumn Caption="รหัสรถ" FieldName="STRUCKID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="1">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ชนิดรถ" FieldName="SCARTYPEID" ShowInCustomizationForm="True"
                                                                        Visible="false" VisibleIndex="2">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รูปแบบรถ" FieldName="SCARTYPE" ShowInCustomizationForm="True"
                                                                        VisibleIndex="3" Width="12%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" ShowInCustomizationForm="True"
                                                                        VisibleIndex="4" Width="11%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ทะเบียนหัว" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                                        VisibleIndex="5" Width="11%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ทะเบียนหาง" FieldName="STRAILERREGISTERNO" ShowInCustomizationForm="True"
                                                                        Visible="true" VisibleIndex="6">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>                                                                    
                                                                    <dx:GridViewDataTextColumn Caption="ประเภทรถ" FieldName="SCARTYPENAME" ShowInCustomizationForm="True"
                                                                        VisibleIndex="7" Width="11%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>                                                                    
                                                                    <dx:GridViewDataTextColumn Caption="จำนวนช่อง" FieldName="NSLOT" ShowInCustomizationForm="True"
                                                                        VisibleIndex="8" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                     <dx:GridViewDataTextColumn Caption="ปริมาตร" FieldName="NTOTALCAPACITY" ShowInCustomizationForm="True"
                                                                        VisibleIndex="9" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="วันที่อัพเดตล่าสุด" FieldName="DUPDATE" ShowInCustomizationForm="True"
                                                                        VisibleIndex="10" Width="5%">
                                                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                                        </PropertiesTextEdit>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="อนุญาติใช้งาน" FieldName="STATUS" ShowInCustomizationForm="True"
                                                                        Visible="true" VisibleIndex="11" Width="10%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="วันหมดอายุวัดน้ำ" FieldName="DWATEREXPIRE" ShowInCustomizationForm="True"
                                                                        VisibleIndex="12" Width="5%">
                                                                        <PropertiesTextEdit DisplayFormatString="{0:dd/MM/yyyy}">
                                                                        </PropertiesTextEdit>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>                                                                     
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="13" Width="10%"
                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <dx:ASPxButton ID="btnAdd" runat="server" Width="50px" CausesValidation="False" Enabled='<%# (CanWrite) ? true : false %>'
                                                                                            AutoPostBack="false" Text="จับคู่TC">
                                                                                            <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('AddVehicle;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <DataItemTemplate>                                                                                                                                      
                                                                            <dx:ASPxButton ID="Editvehicle" runat="server" AutoPostBack="false" CausesValidation="False"
                                                                                Cursor="pointer" Text="แยกหัวหาง">
                                                                                <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditVehicle;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                <%--<ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('view1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                                <%-- <Image Height="16px" Url="Images/search.png" Width="16px">
                                                                                </Image>--%>
                                                                            </dx:ASPxButton>                                                                                                                                                      
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle Cursor="hand" HorizontalAlign="Center">

                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <EditForm>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table width="85%" border="0" cellpadding="1" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">
                                                                                                รายละเอียดรถ
                                                                                            </td>
                                                                                        </tr>
                                                                                        <asp:Literal ID="ltlTruckTotal" runat="server"></asp:Literal>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ความจุในแต่ละช่อง</span>
                                                                                            </td>
                                                                                            <td colspan="3" align="left">
                                                                                                <span class="style24"></span><span class="style24"></span>
                                                                                                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                                    <tr>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                                            ช่องที่
                                                                                                        </td>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                                            ระดับแป้นที่
                                                                                                        </td>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">
                                                                                                            ความจุที่ระดับแป้น (ลิตร)
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <asp:Literal ID="ltlTruckTotal1" runat="server"></asp:Literal>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4" align="right">
                                                                                                <dx:ASPxButton ID="btnReq" runat="server" Text="ส่งคำร้อง" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                                </dx:ASPxButton>
                                                                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){gvw1.CancelEdit() ;}" />
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EditForm>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="dts1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT DISTINCT TTR.STRUCKID,TTR.VIEHICLE_TYPE,
  TTR.STRAILERID,TC.SCONTRACTNO,
  CASE 
        WHEN TTR.CACTIVE ='N' THEN
        GET_C_CONFIG('10','TTRUCK_STATUS','1')    
        WHEN TTR.CACTIVE ='Y' THEN
        GET_C_CONFIG('11','TTRUCK_STATUS','1')            
    END STATUS,
  TTR.SCARTYPEID,
  SHEADREGISTERNO,
  TTR.STRAILERREGISTERNO,
   CASE 
  WHEN TTR.VIEHICLE_TYPE IS NULL AND TTR.SCARTYPEID = '0' THEN
  GET_C_CONFIG('00','VICHICLE_TYPE','1')
  WHEN TTR.VIEHICLE_TYPE IS NULL AND TTR.SCARTYPEID = '3' THEN
  GET_C_CONFIG('34','VICHICLE_TYPE','1')
  ELSE
  TT.CONFIG_NAME
  END AS SCARTYPENAME,
  CASE TCT.CSTANDBY
    WHEN 'N'
    THEN 'รถในสัญญา'
    ELSE 'รถสำรอง'
  END SCARTYPE,
  TTR.NSLOT,TTR.NTOTALCAPACITY,NVL(TTR.DUPDATE,TTR.DCREATE) DUPDATE ,TTR.DWATEREXPIRE
FROM TTRUCK TTR
LEFT JOIN C_CONFIG TT
ON TTR.VIEHICLE_TYPE =TT.CONFIG_VALUE 
AND TT.CONFIG_TYPE ='VICHICLE_TYPE'
LEFT JOIN TContract_Truck TCT
ON TTR.STRUCKID = TCT.STRUCKID
AND NVL(TTR.STRAILERID,'xxx')= NVL(TCT.STRAILERID,'xxx')
LEFT JOIN TContract TC
ON TCT.SCONTRACTID        = TC.SCONTRACTID
AND NVL(TC.CACTIVE,'xxx') = 'Y'
LEFT JOIN
  (SELECT STRUCKID,
    COUNT(NCOMPARTNO) NSLOT,
    SUM(NCAPACITY) NTOTALCAPACITY
  FROM
    (SELECT STRUCKID,
      NCOMPARTNO,
      MAX(NCAPACITY) NCAPACITY
    FROM TTRUCK_COMPART TTC
    GROUP BY STRUCKID,
      NCOMPARTNO
    )
  GROUP BY STRUCKID
  ) TTC
ON TCT.STRAILERID      = TTC.STRUCKID
WHERE TTR.LOGDATA='1' AND TTR.STRANSPORTID = :oTrans
AND ((TTR.SCARTYPEID IN ('0','3') AND VIEHICLE_TYPE IN ('00','34') ) OR VIEHICLE_TYPE IS NULL)
AND (TTR.SHEADREGISTERNO LIKE '%'
  || :oSearch
  || '%'
OR TTR.STRAILERREGISTERNO LIKE '%'
  || :oSearch
  || '%'
OR TTR.SCHASIS LIKE '%'
  || :oSearch
  || '%')
ORDER BY TTR.STRUCKID DESC">

                                                                <SelectParameters>
                                                                    <asp:ControlParameter Name="oTrans" ControlID="txtVendorID" PropertyName="Text" />
                                                                    <asp:ControlParameter Name="oSearch" ControlID="txtvehicle" PropertyName="Text" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="Tab4" Text="ข้อมูลพนักงาน">
                                        <ContentCollection>
                                            <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="630px"></td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtSearch2" runat="server" Width="210px" NullText="ค้นหาจากชื่อ, เลขที่บัตรประชาชน" onkeypress="runScript3(event)" >
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnSearch2" runat="server" SkinID="_search">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search2'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <dx:ASPxGridView ID="gvw2" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw2"
                                                                DataSourceID="sds2" KeyFieldName="ID1" SkinID="_gvw" Style="margin-top: 0px"
                                                                Width="100%" OnHtmlRowPrepared="gvw2_HtmlRowPrepared">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="เลขที่บัตรประชาชน" FieldName="SPERSONELNO" ShowInCustomizationForm="True"
                                                                        VisibleIndex="0" Width="14%">
                                                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ชื่อพนักงาน" FieldName="FULLNAME" ShowInCustomizationForm="True"
                                                                        VisibleIndex="1" Width="14%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="left">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ประเภท พขร." FieldName="PERSON_TYPE_DESC" ShowInCustomizationForm="True"
                                                                        VisibleIndex="2" Width="10%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="หมายเลขโทรศัพท์" FieldName="STEL" ShowInCustomizationForm="True"
                                                                        VisibleIndex="3" Width="13%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataDateColumn Caption="วันที่ใบขับขี่หมดอายุ" FieldName="DDRIVEEXPIRE"
                                                                        ShowInCustomizationForm="True" VisibleIndex="4" Width="10%">
                                                                        <CellStyle HorizontalAlign="Center"></CellStyle>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        
                                                                    </dx:GridViewDataDateColumn>

                                                                    <dx:GridViewDataColumn Caption="การอนุญาตใช้งาน" FieldName="" HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Width="10%"
                                                                         CellStyle-HorizontalAlign="Center">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxLabel ID="lblCACTIVE" runat="server" CssClass="dxeLineBreakFix" >
                                                                            </dx:ASPxLabel>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn Caption="การจ้างงาน" FieldName="" HeaderStyle-HorizontalAlign="Center" VisibleIndex="6"
                                                                        Width="5%" CellStyle-HorizontalAlign="Center">
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxButton ID="btn" runat="server" AutoPostBack="false" CausesValidation="False"
                                                                                EnableTheming="False" EnableDefaultAppearance="False" SkinID="NoSkind" Cursor="pointer"
                                                                                CssClass="dxeLineBreakFix" Width="16px" Height="16px">
                                                                                <ClientSideEvents Click="comfirmcilck" />
                                                                            </dx:ASPxButton>
                                                                        </DataItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="7" Width="9%">
                                                                        <HeaderTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" Width="50px" CausesValidation="False" Enabled='<%# (CanWrite) ? true : false %>'
                                                                                            AutoPostBack="false" Text="เพิ่มTU">
                                                                                            <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('AddT4;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxButton ID="imbedit0" runat="server" AutoPostBack="false" CausesValidation="False"
                                                                                Cursor="pointer" Text="ยื่นคำร้อง">
                                                                                <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT4;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                <%--<ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('view2;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                                <%-- <Image Height="16px" Url="Images/search.png" Width="16px">
                                                                                </Image>--%>
                                                                            </dx:ASPxButton>
                                                                        </DataItemTemplate>
                                                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="SEMPLOYEEID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="FNAME" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="LNAME" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="EMPSAPID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="STRANS_ID" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="SDRIVERNO" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="STEL" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="STEL2" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="DDRIVEBEGIN" Visible="false" >
                                                                        
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="DDRIVEEXPIRE" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="CACTIVE" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="INUSE" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                    <dx:GridViewDataColumn FieldName="DRVSTATUS" Visible="false">
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <EditForm>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table width="85%" border="0" cellpadding="2" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">รายละเอียดพนักงานขับรถ
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">รหัสพนักงานขับรถ</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SEMPLOYEEID")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ชื่อ - สกุล </span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("FULLNAME")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ประเภท พขร</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("PERSON_TYPE_DESC")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ที่อยู่</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("ADDRESS")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="20%" align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">โทรศัพท์</span>
                                                                                            </td>
                                                                                            <td width="30%" align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("STEL")%></span>
                                                                                            </td>
                                                                                            <td width="20%" align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">โทรสาร</span>
                                                                                            </td>
                                                                                            <td width="30%" align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SFAX")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">เลขที่บัตรประชาชน</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SPERSONELNO")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">วันที่ออกบัตร-วันที่หมดอายุ</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("PERSONEL_BEGIN", "{0:dd/MM/yyyy}")%>
                                                                                                    -
                                                                                                    <%# Eval("PERSONEL_EXPIRE", "{0:dd/MM/yyyy}")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">เลขที่ใบขับขี่ประเภท 4 </span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SDRIVERNO")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">วันที่ออกบัตร-วันที่หมดอายุ</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("DDRIVEBEGIN", "{0:dd/MM/yyyy}")%>
                                                                                                    -
                                                                                                    <%# Eval("DDRIVEEXPIRE", "{0:dd/MM/yyyy}")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">วันเดือนปีเกิด </span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("DBIRTHDATE", "{0:dd/MM/yyyy}")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">สัญชาติ</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SNATIONNAL")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">เชื้อชาติ</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SORIGIN")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ศาสนา</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SRELIGION")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">รหัสผู้ขนส่ง</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("STRANS_ID")%></span>
                                                                                            </td>
                                                                                            <td align="left" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ชื่อผู้ขนส่ง</span>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <span class="style24">
                                                                                                    <%# Eval("SABBREVIATION")%></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4" align="right">
                                                                                                <dx:ASPxButton ID="btnReq" runat="server" Text="ส่งคำร้อง" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT4;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                                </dx:ASPxButton>
                                                                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){gvw2.CancelEdit() ;}" />
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EditForm>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="sds2" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT  ROW_NUMBER () OVER (ORDER BY E.SEMPLOYEEID) AS ID1,E.SEMPLOYEEID, E.INAME || ES.FNAME || ' ' || ES.LNAME AS FULLNAME,ET.PERSON_TYPE_DESC,E.SADDRESS || ' ' || E.TUMBON || ' ' || E.AMPHUR || ' ' || PV.SPROVINCE AS ADDRESS,E.STEL, E.SFAX ,E.SPERSONELNO,E.PERSONEL_BEGIN,E.PERSONEL_EXPIRE,E.SDRIVERNO,E.DDRIVEBEGIN,E.DDRIVEEXPIRE,E.DBIRTHDATE,E.SNATIONNAL,E.SORIGIN,E.SRELIGION,E.STRANS_ID,V.SABBREVIATION,E.SREGIDTERNO,TR.STRAILERREGISTERNO ,E.CACTIVE,E.INUSE, ES.FNAME, ES.LNAME,ES.EMPSAPID,E.STEL2,ES.DRVSTATUS
FROM ((((((TEMPLOYEE e INNER JOIN TEMPLOYEE_SAP es ON E.SEMPLOYEEID = ES.SEMPLOYEEID)
left JOIN TContract_Truck tct ON E.STRUCKID = TCT.STRUCKID)
 left JOIN TContract tc ON TCT.SCONTRACTID = TC.SCONTRACTID)
 LEFT JOIN TEMPLOYEETYPES et ON E.SEMPTPYE = ET.PERSON_TYPE)
LEFT JOIN TVENDOR v ON E.STRANS_ID = V.SVENDORID) 
LEFT JOIN LSTPROVINCES pv ON E.SPROVINCEID = PV.NPROVINCEID) 
LEFT JOIN TTRUCK tr ON E.STRUCKID = TR.STRUCKID
WHERE E.Inuse != '0' AND V.SVENDORID  = :oTrans 
AND (E.INAME || ES.FNAME || ' ' || ES.LNAME LIKE '%' || :oSearch || '%'  OR E.SPERSONELNO LIKE '%' || :oSearch || '%'  OR E.SEMPLOYEEID LIKE '%' || :oSearch || '%')">
                                                                <SelectParameters>
                                                                    <asp:ControlParameter Name="oTrans" ControlID="txtVendorID" PropertyName="Text" />
                                                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch2" PropertyName="Text" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                    <dx:TabPage Name="Tab5" Text="ข้อมูลTruck Component(TC)">
                                        <ContentCollection>
                                            <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="630px"></td>
                                                        <td width="630px" align="right">
                                                            <asp:CheckBox ID="FloatCar" runat="server" />ค้นหารถไม่มีสังกัด&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <dx:ASPxTextBox ID="txtSearch1" runat="server" Width="210px" NullText="ค้นหาจากทะเบียนรถ, หมายเลขแชชซีย์" onkeypress="runScript4(event)">
                                                            </dx:ASPxTextBox>
                                                        </td>
                                                        <td>
                                                            <dx:ASPxButton ID="btnSearch1" runat="server" SkinID="_search">
                                                                <ClientSideEvents Click="function (s, e) {xcpn.PerformCallback('Search1'); }" />
                                                            </dx:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <dx:ASPxGridView ID="gvw1" runat="server" AutoGenerateColumns="False" ClientInstanceName="gvw1"
                                                                DataSourceID="sds1" KeyFieldName="STRUCKID" SkinID="_gvw" Style="margin-top: 0px"
                                                                Width="100%">
                                                                <Columns>
                                                                    
                                                                    
                                                                    <dx:GridViewDataTextColumn Caption="รหัสรถ" FieldName="STRUCKID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="1">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="รหัสรถ" FieldName="SCARTYPEID" ShowInCustomizationForm="True"
                                                                        Visible="False" VisibleIndex="2">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" HeaderStyle-HorizontalAlign="Center" Width="10%"
                                                                        VisibleIndex="3">
                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ทะเบียนรถ" FieldName="SHEADREGISTERNO" ShowInCustomizationForm="True"
                                                                        VisibleIndex="4" Width="8%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="ประเภทรถTC" FieldName="SCARTYPENAME" ShowInCustomizationForm="True"
                                                                        VisibleIndex="5" Width="9%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="จำนวนช่อง" FieldName="NSLOT" ShowInCustomizationForm="True"
                                                                        VisibleIndex="6" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>                                                                    
                                                                    <dx:GridViewDataTextColumn Caption="ปริมาตร" FieldName="NTOTALCAPACITY" ShowInCustomizationForm="True"
                                                                        VisibleIndex="7" Width="5%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="วันที่อัพเดตล่าสุด" FieldName="DUPDATE" ShowInCustomizationForm="True"
                                                                        VisibleIndex="8" Width="5%">
                                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                                        </PropertiesTextEdit>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="การอนุญาติใช้งาน" FieldName="STATUS" ShowInCustomizationForm="True"
                                                                        Visible="true" VisibleIndex="9" Width="7%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="สังกัดรถ" FieldName="ISUSE" ShowInCustomizationForm="false"
                                                                        Visible="true" VisibleIndex="10" Width="8%">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                        <DataItemTemplate>
                                                                            <%# Eval("ISUSE").ToString()== "1"?"รถในสังกัด":"รถไม่มีสังกัด"%>
                                                                        </DataItemTemplate>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataColumn ShowInCustomizationForm="True" VisibleIndex="12" Width="10%"
                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <dx:ASPxButton ID="btnAdd" runat="server" SkinID="_add" Width="50px" CausesValidation="False" Enabled='<%# (CanWrite) ? true : false %>'
                                                                                            AutoPostBack="false">
                                                                                            <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('AddT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />
                                                                                        </dx:ASPxButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </HeaderTemplate>
                                                                        <DataItemTemplate>
                                                                            <dx:ASPxButton ID="imbedit0" runat="server" AutoPostBack="false" CausesValidation="False"
                                                                                Cursor="pointer" Text="แก้ไขข้อมูล">
                                                                                <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                <%--<ClientSideEvents Click="function (s, e) { xcpn.PerformCallback('view1;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length)); }" />--%>
                                                                                <%-- <Image Height="16px" Url="Images/search.png" Width="16px">
                                                                                </Image>--%>
                                                                            </dx:ASPxButton>
                                                                        </DataItemTemplate>

                                                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                                        <CellStyle Cursor="hand" HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataColumn>
                                                                </Columns>
                                                                <SettingsPager AlwaysShowPager="True">
                                                                </SettingsPager>
                                                                <Templates>
                                                                    <EditForm>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <table width="85%" border="0" cellpadding="1" cellspacing="1">
                                                                                        <tr>
                                                                                            <td colspan="4" align="left" background="images/intra_dpy020_b.gif" class="style26">รายละเอียดรถ
                                                                                            </td>
                                                                                        </tr>
                                                                                        <asp:Literal ID="ltlTruckTotal" runat="server"></asp:Literal>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" bgcolor="#FFEBD7">
                                                                                                <span class="style24">ความจุในแต่ละช่อง</span>
                                                                                            </td>
                                                                                            <td colspan="3" align="left">
                                                                                                <span class="style24"></span><span class="style24"></span>
                                                                                                <table width="100%" border="0" cellpadding="2" cellspacing="1">
                                                                                                    <tr>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">ช่องที่
                                                                                                        </td>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">ระดับแป้นที่
                                                                                                        </td>
                                                                                                        <td align="center" bgcolor="#FFEBD7" class="style24">ความจุที่ระดับแป้น (ลิตร)
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <asp:Literal ID="ltlTruckTotal1" runat="server"></asp:Literal>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="4" align="right">
                                                                                                <dx:ASPxButton ID="btnReq" runat="server" Text="ส่งคำร้อง" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){ xcpn.PerformCallback('EditT3;'+s.name.substring(s.name.lastIndexOf('_')+1,s.name.length));}" />
                                                                                                </dx:ASPxButton>
                                                                                                <dx:ASPxButton ID="btnCancel" runat="server" Text="ปิด" Width="80px" AutoPostBack="False"
                                                                                                    CssClass="dxeLineBreakFix">
                                                                                                    <ClientSideEvents Click="function (s,e){gvw1.CancelEdit() ;}" />
                                                                                                </dx:ASPxButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EditForm>
                                                                </Templates>
                                                            </dx:ASPxGridView>
                                                            <asp:SqlDataSource ID="sds1" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                                                CancelSelectOnNullParameter="False" ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                                                SelectCommand="SELECT DISTINCT TTR.STRUCKID,TTR.VIEHICLE_TYPE,
  TTR.STRAILERID,TC.SCONTRACTNO,
  CASE 
        WHEN TTR.CACTIVE ='N' THEN
        GET_C_CONFIG('10','TTRUCK_STATUS','1')    
        WHEN TTR.CACTIVE ='Y' THEN
        GET_C_CONFIG('11','TTRUCK_STATUS','1')         
    END STATUS,
  TTR.SCARTYPEID,
  SHEADREGISTERNO,
  TTR.STRAILERREGISTERNO,
  TT.CONFIG_NAME AS SCARTYPENAME,
  CASE TCT.CSTANDBY
    WHEN 'N'
    THEN 'รถในสัญญา'
    ELSE 'รถสำรอง'
  END SCARTYPE,
  TTR.SCHASIS,
  (CASE SCARTYPEID 
  WHEN 3 THEN
  0
  ELSE
  TTR.NSLOT
  END)  NSLOT,(CASE SCARTYPEID 
  WHEN 3 THEN
    0
  ELSE
    TTR.NTOTALCAPACITY
  END) NTOTALCAPACITY,NVL(TTR.DUPDATE,TTR.DCREATE) DUPDATE,TTR.ISUSE 
FROM TTRUCK TTR
LEFT JOIN C_CONFIG TT
ON TTR.SCARTYPEID      =TT.CONFIG_VALUE AND TT.CONFIG_TYPE = 'VICHICLE'
LEFT JOIN TContract_Truck TCT
ON TTR.STRUCKID = TCT.STRUCKID AND NVL(TTR.STRAILERID,'xxx')= NVL(TCT.STRAILERID,'xxx')
LEFT JOIN TContract TC
ON TCT.SCONTRACTID        = TC.SCONTRACTID
AND NVL(TC.CACTIVE,'xxx') = 'Y'
LEFT JOIN
  (SELECT STRUCKID,
    COUNT(NCOMPARTNO) NSLOT,
    SUM(NCAPACITY) NTOTALCAPACITY
  FROM
    (SELECT STRUCKID,
      NCOMPARTNO,
      MAX(NCAPACITY) NCAPACITY
    FROM TTRUCK_COMPART TTC
    GROUP BY STRUCKID,
      NCOMPARTNO
    )
  GROUP BY STRUCKID
  ) TTC
ON TCT.STRAILERID      = TTC.STRUCKID
WHERE TTR.ISUSE ='1' AND TTR.LOGDATA='1'
AND TTR.STRANSPORTID = :oTrans
AND (TTR.SHEADREGISTERNO LIKE '%'
  || :oSearch
  || '%'
OR TTR.STRAILERREGISTERNO LIKE '%'
  || :oSearch
  || '%'
OR TTR.SCHASIS LIKE '%'
  || :oSearch
  || '%')
ORDER BY TTR.STRUCKID DESC">
                                                                <%--SELECT distinct TTR.STRUCKID, TTR.STRAILERID, NVL(TTR.STRANSPORTTYPE,1) AS STRANSPORTTYPE, TTR.SCARTYPEID, TTR.SHEADREGISTERNO, TTR.STRAILERREGISTERNO, TT.SCARTYPENAME, 
                                                                                CASE TCT.CSTANDBY WHEN 'N' THEN 'รถในสัญญา' ELSE 'รถสำรอง' END SCARTYPE, TTR.SCHASIS,
                                                                                CASE WHEN TTR.SCARTYPEID = 0 THEN TTR.NSLOT ELSE TTC.NSLOT END NSLOT,
                                                                                CASE WHEN TTR.SCARTYPEID = 0 THEN TTR.NTOTALCAPACITY ELSE TTC.NTOTALCAPACITY END NTOTALCAPACITY
                                                                                FROM  ((TTRUCK TTR INNER JOIN TTRUCKTYPE TT ON  TTR.SCARTYPEID = TT.SCARTYPEID) 
                                                                                            INNER JOIN TContract_Truck TCT ON TTR.STRUCKID = TCT.STRUCKID) 
                                                                                INNER JOIN TContract TC ON TCT.SCONTRACTID = TC.SCONTRACTID  AND NVL(TC.CACTIVE,'xxx') = 'Y'
                                                                                LEFT JOIN (SELECT STRUCKID, COUNT(NCOMPARTNO) NSLOT, SUM(NCAPACITY)  NTOTALCAPACITY
                                                                                                FROM(SELECT STRUCKID, NCOMPARTNO, MAX(NCAPACITY) NCAPACITY
                                                                                                            FROM TTRUCK_COMPART TTC
                                                                                                            GROUP BY STRUCKID, NCOMPARTNO       
                                                                                                ) 
                                                                                                GROUP BY STRUCKID
                                                                                ) TTC on TCT.STRAILERID = TTC.STRUCKID
                                                                                WHERE  TC.SVENDORID  = :oTrans AND (TTR.SHEADREGISTERNO LIKE '%' || :oSearch || '%' OR TTR.STRAILERREGISTERNO LIKE '%' || :oSearch || '%' OR TTR.SCHASIS LIKE '%' || :oSearch || '%') 
                                                                                ORDER BY SCARTYPE --%>
                                                                <SelectParameters>
                                                                    <asp:ControlParameter Name="oTrans" ControlID="txtVendorID" PropertyName="Text" />
                                                                    <asp:ControlParameter Name="oSearch" ControlID="txtSearch1" PropertyName="Text" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>                                    
                                </TabPages>
                            </dx:ASPxPageControl>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="#FFFFFF">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#0E4999">
                                        <img src="images/spacer.GIF" width="250px" height="1px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
