﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="vendor_confirm_contract.aspx.cs" Inherits="vendor_confirm_contract" %>

<%@ Register Assembly="DevExpress.Web.v11.2, Version=11.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
    <script type="text/javascript">
        // <![CDATA[
        function Set_txtnTruckConfirm(s, e) {
            //alert(e);
            var text = s.GetText();
            if (s.GetText() != "") {
                s.SetText(text);
            } else {
                s.SetText(e);
            }
        }
        function ShowLoginWindow() {
            pcLogin.Show();
        }
        function ShowCreateAccountWindow() {
            pcCreateAccount.Show();
            tbUsername.Focus();
        }
        function BackupConfirmTruckID(ctrl, checkid, mode) {
            if (mode == "") {
                document.getElementById(ctrl).value += '' + checkid;
            }
            else {
                var struckid = document.getElementById(ctrl).value;
                if (struckid.indexOf(checkid) > -1) {
                    document.getElementById(ctrl).value = struckid.replace(checkid, '');
                }
            }
        }

        //Upload File Control
        var fieldSeparator = "|";
        var fieldPath = "#";
        function FileUploadStart() {
            document.getElementById("uploadedListFiles").innerHTML = "";
        }
        function FileUploaded(s, e) {
            if (e.isValid) {
                var linkFile = document.createElement("a");
                var indexPath = e.callbackData.indexOf(fieldPath);
                var indexSeparator = e.callbackData.indexOf(fieldSeparator);
                var fileName = e.callbackData.substring(0, indexSeparator);
                var pictureUrl = e.callbackData.substring(indexSeparator + (fieldSeparator.length), indexPath);
                var sPath = e.callbackData.substring(indexPath + (fieldPath.length));
                var date = new Date();
                var imgSrc = sPath+ pictureUrl + "?dx=" + date.getTime();
                linkFile.innerHTML = fileName;
                linkFile.setAttribute("href", imgSrc);
                linkFile.setAttribute("target", "_blank");
                var container = document.getElementById("uploadedListFiles");
                container.appendChild(linkFile);
                container.appendChild(document.createElement("br"));
            }
        }

        function UpdateCheckListItem(ctrlid, contractid, truckid, checklistid, scheckid) {
            var inner_HTML = $('#' + ctrlid)[0].innerHTML;
            $('#' + ctrlid)[0].innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            $('#' + ctrlid).css({ 'text-decoration': 'none' });
            $('#' + ctrlid).addClass('loading_algRight');
            var datas = contractid + '^' + truckid + '^' + checklistid + '^' + scheckid;
            jQuery.ajax({
                type: "POST",
                url: "ashx/UpdateCheckLists.ashx",
                cache: false,
                async: false,
                dataType: "html",
                data: { contractid: encodeURIComponent(contractid), data: encodeURIComponent(datas) },
                error: function (response) {
                    $('#' + ctrlid).removeClass('loading_algRight');
                    alert(response);
                },
                success: function (response) {
                    if (response == '1') {
                        $('#' + ctrlid)[0].innerHTML = inner_HTML;
                        $('#' + ctrlid).hide();
                    }

                    $('#' + ctrlid).removeClass('loading_algRight');
                }
            });
        }

       
        // ]]> 
    </script>
    <script src="Javascript/Common/common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxCallbackPanel ID="xcpn" runat="server" HideContentOnCallback="False" ClientInstanceName="xcpn"
        OnCallback="xcpn_Callback" CausesValidation="False">
        <ClientSideEvents EndCallback="function(s, e){ eval(s.cpPopup);  s.cpPopup='';  }">
        </ClientSideEvents>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent" runat="server">
                <script language="javascript" type="text/javascript" src="Javascript/DevExpress/1_27.js"></script>
                <script language="javascript" type="text/javascript" src="Javascript/DevExpress/2_15.js"></script>
                <table border="0" cellspacing="2" cellpadding="5" width="100%">
                    <tbody>
                        <tr>
                            <td align="left">
                                <dx:ASPxButton ID="btnConfirm" ClientInstanceName="btnConfirm" runat="server" SkinID="_send">
                                    <ClientSideEvents Click="function(s,e){ xcpn.PerformCallback('checkdata'); }"></ClientSideEvents>
                                </dx:ASPxButton>
                            </td>
                            <td align="right" colspan="3">
                                <table>
                                    <tr>
                                        <td width="50%" align="right">
                                            <dx:ASPxTextBox ID="txtKeyword" runat="server" ClientInstanceName="xtxtKeyword" CssClass="dxeLineBreakFix"
                                                Width="250px" NullText="เลขที่สัญญา" NullTextStyle-HorizontalAlign="Left">
<NullTextStyle HorizontalAlign="Left"></NullTextStyle>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td style="white-space: nowrap;"><span> วันที่เข้ารับสินค้า </span>
                                        <div style="display:none;">
                                            <dx:ASPxDateEdit ID="dteStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                                                SkinID="xdte" NullText="ระหว่างวันที่">
                                                <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit></div>
                                        </td>
                                        <td>
                                            <dx:ASPxDateEdit ID="dteEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                                                SkinID="xdte" NullText="ถึงวันที่">
                                                <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                                                    RequiredField-IsRequired="true" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="ระบุ" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cmbStatus" runat="server" ClientInstanceName="cmbStatus" SelectedIndex="0"
                                                CssClass="dxeLineBreakFix" Width="80px">
                                                <Items>
                                                    <dx:ListEditItem Selected="True" Text="สถานะ" Value="" />
                                                    <dx:ListEditItem Text="ยืนยันรถ" Value="1" />
                                                    <dx:ListEditItem Text="รอส่งข้อมูล" Value="0" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="70" align="left">
                                <dx:ASPxButton ID="btnSearch" ClientInstanceName="btnSearch" runat="server" SkinID="_search"
                                    CausesValidation="true" ValidationGroup="search"> 
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp; </td>
                            <td class="active" colspan="4" align="right">
                                <asp:TextBox ID="txtClientClickID" runat="server" Style="display: none;"></asp:TextBox>
                                รอยืนยันรถตามสัญญา 
                                <dx:ASPxLabel ID="lblRecord" ClientInstanceName="lblRecord" runat="server" Text="0"
                                    title="n">
                                </dx:ASPxLabel>
                                รายการ</td>
                        </tr>
                    </tbody>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr style="height: 1px;">
                        <td bgcolor="#0e4999">
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="1" cellpadding="1" width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <dx:ASPxGridView ID="xgvw" ClientInstanceName="xgvw" runat="server" SkinID="_gvw"
                                    AutoGenerateColumns="False" KeyFieldName="SKEYID" DataSourceID="sdsContract"
                                    OnAfterPerformCallback="xgvw_AfterPerformCallback">
                                    <ClientSideEvents RowClick="function (s,e) {xgvw.StartEditRow(e.visibleIndex); } ">
                                    </ClientSideEvents>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="1%">
                                            <HeaderTemplate>
                                                <dx:ASPxCheckBox ID="SelectAllCheckBox" ClientInstanceName="SelectAllCheckBox" runat="server"
                                                    ToolTip="Select/Unselect all rows on the page" CheckState="Unchecked">
                                                    <ClientSideEvents CheckedChanged="function(s, e) { xgvw.SelectAllRowsOnPage(s.GetChecked()); }" />
                                                </dx:ASPxCheckBox>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="ที่." Width="4%">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                            </CellStyle>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา" ReadOnly="True"
                                            Width="20%">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Left" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="วันที่สัญญา">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="DBEGIN" Caption="เริ่มต้น" ReadOnly="True"
                                                    Width="8%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="DEND" Caption="สิ้นสุด" ReadOnly="True" Width="8%">
                                                    <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEV_DATE" Caption="วันที่เข้ารับสินค้า" ReadOnly="True"
                                            Width="8%">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DDATE" Caption="วันที่ยืนยัน" ReadOnly="True" Visible="false"
                                            Width="8%">
                                            <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                            </PropertiesTextEdit>
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewBandColumn Caption="จำนวนรถ">
                                            <Columns>
                                                <dx:GridViewDataTextColumn FieldName="NONHAND" Caption="ในสัญญา" ReadOnly="True" Width="4%">
                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                    </PropertiesTextEdit>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="NOUTHAND" Caption="สำรอง" ReadOnly="True" Width="4%">
                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="มีปัญหา" ReadOnly="True" Width="4%">
                                                    <PropertiesTextEdit DisplayFormatString="{0:#,0}">
                                                    </PropertiesTextEdit>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="ยืนยันแล้ว" ReadOnly="True" Width="4%">
                                                    <DataItemTemplate>
                                                        <dx:ASPxTextBox ID="txtnTruckConfirm" ClientInstanceName="txtnTruckConfirm" runat="server"
                                                            EnableViewState="true" Width="35px" ClientEnabled='false' Text='0' ForeColor="Red" HorizontalAlign="Center" 
                                                            >
                                                        </dx:ASPxTextBox>
                                                    </DataItemTemplate>
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewBandColumn>
                                        <dx:GridViewDataTextColumn FieldName="SCONFIRM" Caption="สถานะ" Width="8%">
                                            <CellStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="CCONFIRM" Caption="CCONFIRM" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NCONFIRM" Caption="NCONFIRM" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NISSUE" Caption="NISSUE" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="NREJECT" Caption="NREJECT" Visible="false">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="DEXPIRE" Caption="DEXPIRE" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NCONFIRMID" Caption="NCONFIRMID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="COIL" Caption="COIL" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="CGAS" Caption="COIL" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NHOLD" Caption="NHOLD" Visible="false" />
                                        <dx:GridViewDataTextColumn FieldName="NTRUCK" Caption="NHOLD" Visible="false" />
                                    </Columns>
                                    <Settings ShowFooter="True"></Settings>
                                    <Templates>
                                        <EditForm>
                                            <dx:ASPxGridView ID="xgvwCar" ClientInstanceName="xgvwCar" runat="server" SkinID="_gvwChild"
                                                AutoGenerateColumns="FALSE" KeyFieldName="STRUCKID" OnHtmlDataCellPrepared="xgvwCar_HtmlDataCellPrepared"
                                                OnAfterPerformCallback="xgvwCar_AfterPerformCallback">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ลำดับที่" Width="1%">
                                                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRUCKID" Caption="รหัสรถ" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewBandColumn Caption="ข้อมูลรถในสัญญา">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ(หัว)" ReadOnly="True"
                                                                Width="8%">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <CellStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนรถ(ท้าย)"
                                                                ReadOnly="True" Width="8%">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="สถานะรถ" ReadOnly="True" Width="10%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel ID="lblStatusCar" ClientInstanceName="lblStatusCar" runat="server"
                                                                        Width="100px" Text='<%# (""+Eval("CMA")=="2"?"ห้ามวิ่ง":(""+Eval("CMA")=="1"?"แก้ไขภายใน "+Eval("NDAY_MA")+" วัน":(""+Eval("CMA")=="0"?"ปกติ":"ปกติ!"))) %>'>
                                                                    </dx:ASPxLabel>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewBandColumn Caption="รถที่ผู้ประกอบการยืนยัน">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="SHEADREGISTERNO"
                                                                Visible="false">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ(หัว) "
                                                                ReadOnly="True" Width="8%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxComboBox ID="cmbSHEADREGISTERNO" ClientInstanceName="cmbSHEADREGISTERNO"
                                                                        runat="server" SkinID="xcbbATC" Width="100px" ValueField="STRUCKID" TextFormatString="{1}"
                                                                        CallbackPageSize="30" EnableCallbackMode="true" OnItemsRequestedByFilterCondition="cmbSHEADREGISTERNO_OnItemsRequestedByFilterConditionSQL"
                                                                        OnItemRequestedByValue="cmbSHEADREGISTERNO_OnItemRequestedByValueSQL">
                                                                        <%--<ClientSideEvents  SelectedIndexChanged="function(s, e){ cmbSHEADREGISTERNO.SetValue(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHEADREGISTERNO')); }" />--%>
                                                                        <ClientSideEvents ValueChanged="function(s, e){   var msg ='';  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')!=''){ msg='\n ท่านไม่สามารถเลือกรถที่ติดสถานะ ห้ามวิ่ง จากระบบ '+s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')+'ได้!'; }  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('CONFHEAD')!='N'){ msg='\n ท่านไม่สามารถเลือกรถที่ ถุูกยืนยัน ไว้ในสัญญาอื่นแล้ว ได้!'; }  if(msg!=''){dxWarning('แจ้งเตือนจากระบบ', ''+msg);return false; } }" />
                                                                        <Columns>
                                                                            <dx:ListBoxColumn FieldName="SSTANDBY" Caption="ประเภทรถ" />
                                                                            <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ(หัว)" />
                                                                            <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนรถ(ท้าย)" />
                                                                            <dx:ListBoxColumn FieldName="SHOLD" Caption="ห้ามวิ่งจาก" Width="70px" />
                                                                            <dx:ListBoxColumn FieldName="CONFHEAD" Caption="ใช้งาน" Width="30px" />
                                                                            <dx:ListBoxColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="SCONTRACTID" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="STRAILERID" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="STRUCKID" Visible="false" />
                                                                        </Columns>
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                            EnableCustomValidation="true">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนรถ(ท้าย) "
                                                                ReadOnly="True" Width="8%">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxComboBox ID="cmbSTRAILERREGISTERNO" ClientInstanceName="cmbSTRAILERREGISTERNO"
                                                                        runat="server" SkinID="xcbbATC" Width="100px" ValueField="STRAILERID" TextFormatString="{2}"
                                                                        CallbackPageSize="30" EnableCallbackMode="true" OnItemsRequestedByFilterCondition="cmbSTRAILERREGISTERNO_OnItemsRequestedByFilterConditionSQL"
                                                                        OnItemRequestedByValue="cmbSTRAILERREGISTERNO_OnItemRequestedByValueSQL">
                                                                        <ClientSideEvents ValueChanged="function(s, e){   var msg ='';  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')!=''){ msg='\n ท่านไม่สามารถเลือกรถที่ติดสถานะ ห้ามวิ่ง จากระบบ '+s.GetItem(s.GetSelectedIndex()).GetColumnText('SHOLD')+'ได้!'; }  if(s.GetItem(s.GetSelectedIndex()).GetColumnText('CONFTRAIL')!='N'){ msg='\n ท่านไม่สามารถเลือกหางรถที่ ถุูกยืนยัน ไว้ในสัญญาอื่นแล้ว ได้!'; }  if(msg!=''){ alert('แจ้งเตือนจากระบบ'+msg);return false; } }" />
                                                                        <Columns>
                                                                            <dx:ListBoxColumn FieldName="SSTANDBY" Caption="ประเภทรถ" />
                                                                            <dx:ListBoxColumn FieldName="SHEADREGISTERNO" Caption="ทะเบียนรถ(หัว)" />
                                                                            <dx:ListBoxColumn FieldName="STRAILERREGISTERNO" Caption="ทะเบียนรถ(ท้าย)" />
                                                                            <dx:ListBoxColumn FieldName="SHOLD" Caption="ห้ามวิ่งจาก" Width="70px" /> 
                                                                            <dx:ListBoxColumn FieldName="CONFTRAIL" Caption="ใช้งาน" Width="30px" />
                                                                            <dx:ListBoxColumn FieldName="SCONTRACTNO" Caption="เลขที่สัญญา" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="SCONTRACTID" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="STRUCKID" Visible="false" />
                                                                            <dx:ListBoxColumn FieldName="STRAILERID" Visible="false" />
                                                                        </Columns>
                                                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                                                                            EnableCustomValidation="true">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewDataTextColumn Caption="ใบตรวจสภาพรถ" ReadOnly="True" Width="18%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox ID="txtPassed" runat="server" ClientInstanceName="txtPassed" Text='<%# (""+Eval("CPASSED")=="0")?"0":"1"  %>'
                                                                ClientVisible="false">
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxButton ID="imbPassed" runat="server" SkinID="_passed" ClientInstanceName="imbPassed"
                                                                CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false" Text=" ผ่าน ">
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="imbIssue" runat="server" SkinID="_issue" ClientInstanceName="imbIssue"
                                                                CausesValidation="False" CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function(s,e){ var index= s.name.substring(s.name.split('imbIssue')[0].lastIndexOf('_')+1,s.name.length).split('_')[1]; xgvwCar.ExpandDetailRow(index); }" />
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataColumn Width="18%" CellStyle-Cursor="hand" Caption="#">
                                                        <DataItemTemplate>
                                                            <dx:ASPxTextBox ID="txtconfirm" runat="server" ClientInstanceName="txtconfirm" Text='<%# (""+Eval("TCONFLST_CCONFIRM")=="0"?"0":"1")%>'
                                                                ClientVisible="False"> 
                                                            </dx:ASPxTextBox>
                                                            <dx:ASPxButton ID="imbconfirm" runat="server" SkinID="_confirm" ClientInstanceName="imbconfirm"
                                                                CausesValidation="False" CssClass="dxeLineBreakFix" ClientEnabled="false">
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="imbcancel" runat="server" SkinID="_disconfirm" ClientInstanceName="imbcancel"
                                                                CausesValidation="False" CssClass="dxeLineBreakFix">
                                                            </dx:ASPxButton>
                                                        </DataItemTemplate>
                                                        <FooterTemplate>
                                                            <dx:ASPxButton ID="btnsubmit" ClientInstanceName="btnsubmit" runat="server" SkinID="_submit"
                                                                CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function (s, e) {  xgvw.PerformCallback(s.name.substring(s.name.split('btnsubmit')[0].lastIndexOf('_')-1,s.name.length).split('_')[0]); }" />
                                                            </dx:ASPxButton>
                                                            <dx:ASPxButton ID="btnclose" ClientInstanceName="btnclose" runat="server" SkinID="_close"
                                                                CssClass="dxeLineBreakFix">
                                                                <ClientSideEvents Click="function (s, e) { xgvw.CancelEdit(); }" />
                                                            </dx:ASPxButton>
                                                        </FooterTemplate>
                                                        <CellStyle Cursor="hand" HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <FooterCellStyle HorizontalAlign="Right" />
                                                    </dx:GridViewDataColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SHEADREGISTERNO" Caption="SHEADREGISTERNO" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRAILERREGISTERNO" Caption="STRAILERREGISTERNO" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SCARTYPENAME" Caption="SCARTYPENAME" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CSTANDBY" Caption="CSTANDBY" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="DDATE" Caption="DDATE" Visible="false">
                                                        <PropertiesTextEdit DisplayFormatString="dd/MM/yyyy">
                                                        </PropertiesTextEdit>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="SCONTRACTID" Caption="SCONTRACTID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="STRAILERID" Caption="STRAILERID" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CMA" Caption="CMA" Visible="false">
                                                    </dx:GridViewDataTextColumn> 
                                                    <dx:GridViewDataTextColumn FieldName="DFINAL_MA" Caption="DFINAL_MA" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="NDAY_MA" Caption="NDAY_MA" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="CPASSED" Caption="CPASSED" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn FieldName="TCONFLST_CCONFIRM" Caption="TCONFLST_CCONFIRM" Visible="false">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsPager Mode="ShowAllRecords" />
                                                <SettingsBehavior AllowSort="false" />
                                                <SettingsDetail ShowDetailRow="true" ShowDetailButtons="false" />
                                                <Settings ShowFooter="True" />
                                                <Templates>
                                                    <DetailRow>
                                                        <asp:Literal ID="ltrHiddenIndex" runat='server' Visible="false"></asp:Literal>
                                                        <asp:Literal ID="ltrTruckData" runat='server' Visible="false"></asp:Literal>
                                                        <div style="padding: 3px 3px 2px 3px">
                                                            <dx:ASPxPageControl ID="pageControl" ClientInstanceName="pageControl" runat="server"
                                                                EnableViewState="true" EnableCallBacks="false" Width="100%">
                                                               <ClientSideEvents TabClick="function(s,e){  }" />
                                                                <TabPages>
                                                                    <dx:TabPage Text="ปัญหาการขนส่ง" Visible="true">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ctctrl1" runat="server">
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dx:ASPxGridView ID="gvwGroupCheckList" runat="server" ClientInstanceName="gvwGroupCheckList" Border-BorderStyle="None" 
                                                                                                SkinID="_gvwChild" AutoGenerateColumns="FALSE" KeyFieldName="SKEYID" OnHtmlDataCellPrepared="gvwGroupCheckList_HtmlDataCellPrepared" >
                                                                                                <Columns>
                                                                                                    <dx:GridViewDataTextColumn Caption="ปัญหาการขนส่ง" ReadOnly="True" Width="18%">
                                                                                                        <DataItemTemplate>
                                                                                                            <dx:ASPxLabel ID="lblGroupname" runat="server" ClientInstanceName="lblGroupname"
                                                                                                                Font-Bold="true" Text='<%# Eval("SGROUPNAME") %>'>
                                                                                                            </dx:ASPxLabel>
                                                                                                            <br />
                                                                                                            <dx:ASPxGridView ID="gvwItemCheckList" runat="server" ClientInstanceName="gvwItemCheckList" Width="99%" 
                                                                                                                SkinID="_gvwChild" AutoGenerateColumns="FALSE" KeyFieldName="SKEYID" SettingsPager-Visible="False">
                                                                                                                <Columns>
                                                                                                                    <dx:GridViewDataTextColumn VisibleIndex="0">
                                                                                                                        <DataItemTemplate>
                                                                                                                            <table width="100%">
                                                                                                                                <tr>
                                                                                                                                    <td style="width: 40%;">
                                                                                                                                        <dx:ASPxCheckBox ID="cbxSCHECKLISTID" ClientInstanceName="cbxSCHECKLISTID" runat='server'
                                                                                                                                            Text='<%# Eval("SCHECKLISTNAME") %>'>
                                                                                                                                            <ClientSideEvents CheckedChanged="function(s,e){ if (s.GetValue() == true) { var cflag = document.getElementById(s.name.replace('cbxSCHECKLISTID', 'txtSCHECKLISTID') + '_I').value;  if (cflag == '2') { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = ''; }else if (cflag == '1') { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = ''; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none'; } else { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none';} }else { document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblCHOLD')).style.display = 'none'; document.getElementById(s.name.replace('cbxSCHECKLISTID', 'lblMAnDay')).style.display = 'none'; }  }" />
                                                                                                                                        </dx:ASPxCheckBox> 
                                                                                                                                        <dx:ASPxTextBox ID="txtIsChecked" runat="server" ClientVisible="false" Text="">
                                                                                                                                        </dx:ASPxTextBox>
                                                                                                                                        <dx:ASPxTextBox ID="txtSCHECKLISTID" runat="server" Text='<%# (Eval("CBAN")+""=="1")?"2":((Eval("NDAY_MA")+""!="")?"1":"0") %>'
                                                                                                                                            ClientVisible="false">
                                                                                                                                        </dx:ASPxTextBox>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 25%; text-align: center;">
                                                                                                                                        <dx:ASPxLabel ID="lblCHOLD" ClientInstanceName="lblCHOLD" ForeColor="Red" runat="server"
                                                                                                                                            Text="ห้ามวิ่ง" ClientVisible="false">
                                                                                                                                        </dx:ASPxLabel>
                                                                                                                                        <dx:ASPxLabel ID="lblMAnDay" ClientInstanceName="lblMAnDay" ForeColor="#fecd85" runat="server"
                                                                                                                                            Text='<%# (Eval("NDAY_MA")+""=="")?"":"แก้ไขภายใน "+Eval("NDAY_MA")+" วัน" %>'
                                                                                                                                            ClientVisible="false">
                                                                                                                                        </dx:ASPxLabel>
                                                                                                                                    </td>
                                                                                                                                    <td style="width: 35%; text-align: right;" class="divTable">
                                                                                                                                        <asp:Label ID="lblCheckBy" runat="server"></asp:Label>
                                                                                                                                        <dx:ASPxHyperLink ID="lnkEdited" ClientInstanceName="lnkEdited" Cursor="pointer"
                                                                                                                                            ImageUrl="~/Images/btnCon1.gif" runat="server">
                                                                                                                                        </dx:ASPxHyperLink>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>
                                                                                                                        </DataItemTemplate>
                                                                                                                        <FooterTemplate>
                                                                                                                        </FooterTemplate>
                                                                                                                    </dx:GridViewDataTextColumn>
                                                                                                                    <dx:GridViewDataTextColumn Caption="SVERSIONLIST" FieldName="SVERSIONLIST" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="STYPECHECKLISTID" FieldName="STYPECHECKLISTID" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="SVERSION" FieldName="SVERSION" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="STOPICID" FieldName="STOPICID" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="SCHECKLISTNAME" FieldName="SCHECKLISTNAME" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="SCHECKLISTID" FieldName="SCHECKLISTID" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="NLIST" FieldName="NLIST" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="NDAY_MA" FieldName="NDAY_MA" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="NPOINT" FieldName="NPOINT" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="CCUT" FieldName="CCUT" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="CBAN" FieldName="CBAN" Visible="false" />
                                                                                                                    <dx:GridViewDataTextColumn Caption="CL_CACTIVE" FieldName="CL_CACTIVE" Visible="false" />
                                                                                                                </Columns>
                                                                                                                <Settings ShowColumnHeaders="false" VerticalScrollBarStyle="Virtual" />
                                                                                                                <SettingsPager Mode="ShowAllRecords" ></SettingsPager>
                                                                                                            </dx:ASPxGridView>
                                                                                                        </DataItemTemplate>
                                                                                                    </dx:GridViewDataTextColumn>
                                                                                                    <dx:GridViewDataTextColumn Caption="SGROUPID" FieldName="SGROUPID" Visible="false" />
                                                                                                    <dx:GridViewDataTextColumn Caption="SGROUPNAME" FieldName="SGROUPNAME" Visible="false" />
                                                                                                    <dx:GridViewDataTextColumn Caption="COIL" FieldName="COIL" Visible="false" />
                                                                                                    <dx:GridViewDataTextColumn Caption="CGAS" FieldName="CGAS" Visible="false" />
                                                                                                    <dx:GridViewDataTextColumn Caption="GOCL_CACTIVE" FieldName="GOCL_CACTIVE" Visible="false" />
                                                                                                </Columns>
                                                                                            </dx:ASPxGridView>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right">
                                                                                            <dx:ASPxButton ID="btnsubmit" ClientInstanceName="btnsubmit" runat="server" SkinID="_submit"
                                                                                                CssClass="dxeLineBreakFix">
                                                                                                <ClientSideEvents Click="function (s, e) { xgvwCar.PerformCallback(s.name.substring(s.name.split('btnclose')[0].lastIndexOf('_')+1,s.name.length).split('_')+'$1'); }" />
                                                                                            </dx:ASPxButton>
                                                                                            <dx:ASPxButton ID="btnclose" ClientInstanceName="btnclose" runat="server" SkinID="_close"
                                                                                                CssClass="dxeLineBreakFix">
                                                                                                <ClientSideEvents Click="function (s, e) { xgvwCar.CollapseAllDetailRows(s.name.substring(s.name.split('btnclose')[0].lastIndexOf('_')+1,s.name.length).split('_')); }" />
                                                                                            </dx:ASPxButton>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                    <dx:TabPage Text="หลักฐานการกระทำผิดและปัญหาอื่นๆ" Visible="true">
                                                                        <ContentCollection>
                                                                            <dx:ContentControl ID="ctctrl2" runat="server">
                                                                            
                                                                                            <table cellpadding="2" cellspacing="1" style="border-collapse: collapse; width: 100%">
                                                                                                <tr>
                                                                                                    <td style="width: 48%">
                                                                                                        <b>ปัญหาอื่นๆ</b></td>
                                                                                                    <td style="width: 50%">
                                                                                                        <b>รายละเอียดปัญหา</b> <%--ต้องแก้ไขสภาพภายใน--%>
                                                                                                        <dx:ASPxTextBox ID="txtNMA" runat="server" Width="25px" Text="0" CssClass="dxeLineBreakFix" ClientVisible="false">
                                                                                                        </dx:ASPxTextBox>
                                                                                                       <%-- วัน --%></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <dx:ASPxCheckBox ID="cbxOtherIssue" runat='server' Text="ปัญหาอื่นๆ ระบุปัญหา">
                                                                                                        </dx:ASPxCheckBox>
                                                                                                        <br />
                                                                                                        <dx:ASPxMemo ID="txtOtherIssue" ClientInstanceName="txtOtherIssue" Height="90px"
                                                                                                            Width="75%" runat="server" MaxLength="2000">
                                                                                                        </dx:ASPxMemo>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <br />
                                                                                                        <br />
                                                                                                        <dx:ASPxMemo ID="txtDetailIssue" ClientInstanceName="txtDetailIssue" Height="90px"
                                                                                                            Width="75%" runat="server" MaxLength="2000">
                                                                                                        </dx:ASPxMemo>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" style="width: 50%">
                                                                                                        <b>หลักฐานการกระทำผิด</b></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td valign="top">  <div title="Allowed file types: <%= Resources.CommonResource.FileUploadType.Replace("."," ") %>">Allowed file types: <%= Resources.CommonResource.FileUploadType.Replace("."," ").Substring(0,30) %>ฯลฯ<br>Max file size: <%= Resources.CommonResource.TooltipMaxFileSize1MB %></div>
                                                                                                        <dx:ASPxUploadControl ID="UploadControl" runat="server" ShowAddRemoveButtons="True"
                                                                                                            Width="95%" ShowUploadButton="True" AddUploadButtonsHorizontalPosition="Right" 
                                                                                                            ShowClearFileSelectionButton='true' ShowProgressPanel="True" ClientInstanceName="UploadControl"
                                                                                                            OnFileUploadComplete="UploadControl_FileUploadComplete" FileInputCount="3" AddButton-ImagePosition="Right">
                                                                                                            <ValidationSettings MaxFileSize="<%$ Resources:CommonResource, UploadMaxFileSize1MB %>" AllowedFileExtensions="<%$ Resources:CommonResource, FileUploadType %>">
                                                                                                            </ValidationSettings>
                                                                                                            <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
                                                                                                        </dx:ASPxUploadControl>
                                                                                                      
                                                                                                        <%--FileUploadComplete="function(s, e) { FileUploaded(s, e) }"--%>
                                                                                                    </td>
                                                                                                    <td valign="top">
                                                                                                        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="45%" ClientInstanceName="RoundPanel"
                                                                                                            HeaderText="Uploaded files" Height="100%" CssClass="dxeLineBreakFix">
                                                                                                            <PanelCollection>
                                                                                                                <dx:PanelContent ID="PanelContent1" runat="server">
                                                                                                                    <div id="uploadedListFiles"  style="height: 150px; font-family: Arial; ">
                                                                                                                    </div> 
                                                                                                                    <%--<div id="dvClearFile" style="vertical-align: bottom; text-align: right;">
                                                                                                            <a href="javascript:void(0);" onclick="javascript: document.getElementById('uploadedListFiles').innerHTML=''; ">
                                                                                                                Clear</a>
                                                                                                        </div>--%>
                                                                                                                </dx:PanelContent>
                                                                                                            </PanelCollection>
                                                                                                        </dx:ASPxRoundPanel>
                                                                                                        <dx:ASPxRoundPanel ID="rplAttachmented" runat="server" Width="45%" ClientInstanceName="rplAttachmented"
                                                                                                            HeaderText="Attachmented files" Height="100%" CssClass="dxeLineBreakFix">
                                                                                                            <PanelCollection>
                                                                                                                <dx:PanelContent ID="pnctTab2" runat="server">
                                                                                                                    <div id="AttachmentedListFiles" runat="server" style="height: 150px; font-family: Arial; ">
                                                                                                                    </div> 
                                                                                                                </dx:PanelContent>
                                                                                                            </PanelCollection>
                                                                                                        </dx:ASPxRoundPanel>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td colspan="2" align="right">
                                                                                                        <dx:ASPxButton ID="btnsubmitTab2" ClientInstanceName="btnsubmitTab2" runat="server"
                                                                                                            SkinID="_submit" CssClass="dxeLineBreakFix">
                                                                                                            <ClientSideEvents Click="function (s, e) {  xgvwCar.PerformCallback(s.name.substring(s.name.split('btnclose')[0].lastIndexOf('_')+1,s.name.length).split('_')+'$2'); }" />
                                                                                                        </dx:ASPxButton>
                                                                                                        <dx:ASPxButton ID="btncloseTab2" ClientInstanceName="btncloseTab2" runat="server"
                                                                                                            SkinID="_close" CssClass="dxeLineBreakFix">
                                                                                                            <ClientSideEvents Click="function (s, e) { xgvwCar.CollapseAllDetailRows(); }" />
                                                                                                        </dx:ASPxButton>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                            </dx:ContentControl>
                                                                        </ContentCollection>
                                                                    </dx:TabPage>
                                                                </TabPages>
                                                            </dx:ASPxPageControl>
                                                        </div>
                                                    </DetailRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </EditForm>
                                    </Templates>
                                    <Settings ShowFooter="True" />
                                </dx:ASPxGridView> 
                                <asp:SqlDataSource ID="sdsContract" runat="server" EnableCaching="True" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CancelSelectOnNullParameter="False" CacheKeyDependency="ckdContract" SelectCommand="SELECT  rownum SKEYID,CONT.NCONFIRMID, CONT.SCONTRACTID, CONT.DDATE, CONT.DDATE+1 DEV_DATE,CONT.NTRUCK,NVL(truk.NONHAND,0) NONHAND,truk.NSTANDBY NOUTHAND,CONT.CCONFIRM,CONT.COIL,CONT.CGAS
                                    ,CASE WHEN NVL(conflst.NCONFIRM,0)=0 THEN  NVL(truk.NONHAND,0)-NVL(truk.NHOLD,0) ELSE NVL(conflst.NCONFIRM,0)  END NCONFIRM
                                    ,CASE WHEN CONT.CCONFIRM='1' THEN 'ยืนยันรถ' ELSE 'รอส่งข้อมูล' END SCONFIRM
                                    ,CONT.CACTIVE,CONT.SREMARK,CONT.TCARCONFIRM,CONT.TPLANCONFIRM,truk.NREJECT ,CONT.DBEGIN,CONT.DEND, CONT.DEXPIRE,CONT.SCONTRACTNO
                                     , (NVL(TRUK.NINACTIVE,0)+ NVL(chktrck.NISSUE,0)) NISSUE,NVL(NHOLD,0) NHOLD
                                    FROM( 
                                            SELECT conf.NCONFIRMID,conf.SCONTRACTID,conf.DDATE,conf.NTRUCK,conf.NONHAND,conf.NOUTHAND,NVL(conf.CCONFIRM,'0') CCONFIRM,conf.DCONFIRM,conf.CACTIVE,conf.SREMARK ,TERM.TCARCONFIRM,term.TPLANCONFIRM,CONT.DBEGIN,CONT.DEND, CONF.DEXPIRE,CONT.SCONTRACTNO,CONT.COIL,CONT.CGAS
                                            FROM  TTRUCKCONFIRM conf 
                                            LEFT JOIN TCONTRACT cont ON conf.SCONTRACTID=cont.SCONTRACTID 
                                           LEFT JOIN (SELECT * FROM TUSER WHERE 1=1 and SUID=:usr_SUID ) usr ON cont.SVENDORID=usr.SVENDORID 
                                            LEFT JOIN TTERMINAL term ON cont.STERMINALID=TERM.STERMINALID 
                                            WHERE 1=1 AND  NVL(cont.ntruck,0) != 0 AND CONT.CSPACIALCONTRAC='N' 
                                            AND usr.SUID=:usr_SUID AND TO_DATE(CONF.DEXPIRE,'dd/mm/yyyy') BETWEEN :DSTART AND :DEND  
                                        ) cont 
                                        LEFT JOIN ( 
                                            SELECT truk.SCONTRACTID ,SUM(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN 0 ELSE 1 END) NONHAND ,SUM(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN 1 ELSE 0 END) NSTANDBY ,SUM(CASE WHEN NVL(truk.CREJECT,'N')='Y' THEN 1 ELSE 0 END) NREJECT ,SUM( CASE WHEN NVL(truk.CSTANDBY,'N')='N' THEN (CASE WHEN NVL(TRCK.CACTIVE,'N')='N' THEN 1 ELSE 0 END) ELSE  0 END ) NINACTIVE
                                             ,SUM(
                                            NVL( CASE WHEN NVL(truk.CSTANDBY,'N')='N' THEN
                                                CASE WHEN NVL(TRCK.CHOLD,'0')='0' THEN 
                                                    CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 0 ELSE 1 END
                                                ELSE 1 END
                                           ELSE 0 END ,0)
                                            ) NHOLD 
                                             ,SUM(
                                            NVL(CASE WHEN NVL(truk.CSTANDBY,'N')='Y' THEN
                                                CASE WHEN NVL(TRCK.CHOLD,'0')='0' THEN 
                                                    CASE WHEN TVEHICLE.VEH_NO IS NULL THEN 0 ELSE 1 END
                                                ELSE 1 END
                                             ELSE 0 END,0)
                                            ) NHOLD_OUT
                                            FROM TCONTRACT_TRUCK truk 
                                            LEFT JOIN TTRUCK trck ON  TRUK.STRUCKID=TRCK.STRUCKID
                                            LEFT JOIN  (SELECT * FROM TVEHICLES WHERE VEH_STATUS IS NOT NULL)TVEHICLE  ON REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(trck.SHEADREGISTERNO,'(',''),')',''),'.',''),'-',''),',','') =REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TVEHICLE.VEH_NO,'(',''),')',''),'.',''),'-',''),',','') 
                                            WHERE 1=1  AND NVL(trck.CACTIVE,'N') ='Y' 
                                            GROUP BY  truk.SCONTRACTID  
                                        ) truk ON cont.SCONTRACTID=truk.SCONTRACTID
                                        LEFT JOIN (  
                                            SELECT SCONTRACTID,SUM(nDFINAL_MA+nCMA) NISSUE FROM 
                                            ( 
                                                SELECT TCheckTruck.SCONTRACTID  ,(CASE WHEN MIN(TCheckTruck.DFINAL_MA) &lt; SYSDATE THEN 1 ELSE 0 END)  nDFINAL_MA ,(CASE WHEN TCheckTruck.CMA='2' THEN 1 ELSE 0 END) nCMA  
                                                FROM TCheckTruck 
                                                LEFT JOIN TCONTRACT_TRUCK  ON TCheckTruck.SCONTRACTID=TCONTRACT_TRUCK.SCONTRACTID AND TCheckTruck.STRUCKID=TCONTRACT_TRUCK.STRUCKID
                                                LEFT JOIN TCheckTruckITEM  ON TCheckTruck.SCHECKID=TCheckTruckITEM.SCHECKID
                                                WHERE 1=1 AND TCheckTruck.cClean='0' AND NVL(TCONTRACT_TRUCK.CSTANDBY,'N')='N' AND NVL(TCheckTruckITEM.COTHER,'0')!='1'
                                                GROUP BY TCheckTruck.SCONTRACTID,TCheckTruck.CMA
                                            ) ctrck GROUP BY SCONTRACTID 
                                        ) chktrck  ON cont.SCONTRACTID=chktrck.SCONTRACTID 
                                        LEFT JOIN (
                                            SELECT NCONFIRMID ,COUNT(NCONFIRMID) NCONFIRM FROM TTRUCKCONFIRMLIST WHERE 1=1 AND CCONFIRM='1' AND NVL(CSTANBY,'N')='N'  GROUP BY NCONFIRMID
                                        ) conflst ON cont.NCONFIRMID=conflst.NCONFIRMID
                                        WHERE 1=1 
                                        AND CONT.scontractno LIKE '%'|| NVL(:CONT_SCONTRACTNO,CONT.scontractno) ||'%'  AND NVL(CONT.CCONFIRM,'0') LIKE '%' || NVL(:CCONFIRM,CONT.CCONFIRM) || '%' 
                                        AND TO_DATE(CONT.DEXPIRE,'dd/mm/yyyy') BETWEEN :DSTART AND :DEND    ">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="usr_SUID" SessionField="UserID" />
                                        <asp:ControlParameter Name="CONT_SCONTRACTNO" ControlID="txtKeyword" PropertyName="Text" />
                                        <asp:ControlParameter Name="DSTART" ControlID="dteEnd" PropertyName="Value" />
                                        <asp:ControlParameter Name="DEND" ControlID="dteEnd" PropertyName="Value" />
                                        <asp:ControlParameter Name="CCONFIRM" ControlID="cmbStatus" PropertyName="Value" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:SqlDataSource ID="sdsTruck" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CacheKeyDependency="ckdTruck" EnableCaching="True" CancelSelectOnNullParameter="False" />
                                <asp:SqlDataSource ID="sdsCheckLists" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>"
                                    CacheKeyDependency="ckdCheckLists" EnableCaching="True" CancelSelectOnNullParameter="False"
                                    SelectCommand="SELECT rownum SKEYID, TOCL.STYPECHECKLISTID,TOCL.STYPECHECKLISTNAME,TOCL.CACTIVE TOCL_CACTIVE,TOCL.CCASE,TOCL.CDESPOA,TOCL.CDESPOB,TOCL.CSUREPRISE,TOCL.CVENDOR,TOCL.CTYPE,TOCL.SMENUID
 ,GOCL.SGROUPID ,GOCL.SGROUPNAME ,GOCL.CGAS ,GOCL.COIL ,GOCL.CACTIVE GOCL_CACTIVE
 ,CL.SVERSIONLIST ,CL.SVERSION ,CL.STOPICID ,CL.SCHECKLISTNAME ,CL.SCHECKLISTID ,CL.NLIST ,CL.NDAY_MA ,CL.CCUT ,CL.CBAN ,CL.CACTIVE CL_CACTIVE ,TP.NPOINT
FROM TTYPEOFCHECKLIST TOCL
LEFT JOIN TGROUPOFCHECKLIST GOCL ON  TOCL.STYPECHECKLISTID=GOCL.STYPECHECKLISTID
LEFT JOIN TCHECKLIST CL ON  TOCL.STYPECHECKLISTID=CL.STYPECHECKLISTID AND GOCL.SGROUPID=CL.SGROUPID
LEFT JOIN TTOPIC TP ON CL.STOPICID=TP.STOPICID
where 1=1 
AND NVL(TOCL.CACTIVE,'0')='1' AND NVL(GOCL.CACTIVE,'0')='1' AND NVL(CL.CACTIVE,'0')='1' AND NVL(TP.CACTIVE,'0')='1'
AND tocl.ctype ='1' AND TOCL.CVENDOR='1'
--AND TOCL.STYPECHECKLISTID='1'"></asp:SqlDataSource>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                       <table><tr>
                       <td >สถานะของสี</td>
                       <td style=" border-style:solid; border-width:1px; border-color:Black; background-color: #F5F5F5" width="15px" title="ยังไม่ยืนยันรถ/สามารถยืนยันรายการได้">&nbsp;</td>
                       <td> = ยังไม่ยืนยันรถ</td>
                       <td style=" background-color: #CEECF5" width="15px" title="ยืนยันรถแล้ว"></td>
                       <td> = ยืนยันรถแล้ว</td>
                       <td style=" background-color: #F6E3CE" width="15px" title="เลยระยะเวลายืนยันรถ/ไม่สามารถยืนยันรายการได้"></td>
                       <td> = เลยระยะเวลายืนยันรถ</td>
                       </tr></table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>
