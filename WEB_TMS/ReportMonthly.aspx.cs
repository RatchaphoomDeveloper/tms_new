﻿using System;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Complain;
using TMS_BLL.Transaction.Report;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Drawing;
using EmailHelper;

public partial class ReportMonthly : PageBase
{

    #region + View State +
    private string VendorID
    {
        get
        {
            if ((string)ViewState["VendorID"] != null)
                return (string)ViewState["VendorID"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["VendorID"] = value;
        }
    }

    private string Year
    {
        get
        {
            if ((string)ViewState["Year"] != null)
                return (string)ViewState["Year"];
            else
                return string.Empty;
        }
        set
        {
            ViewState["Year"] = value;
        }
    }

    private int MonthID
    {
        get
        {
            if ((int)ViewState["MonthID"] != null)
                return (int)ViewState["MonthID"];
            else
                return 0;
        }
        set
        {
            ViewState["MonthID"] = value;
        }
    }

    private DataTable dtRequestFileTab1
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTab1"] != null)
                return (DataTable)ViewState["dtRequestFileTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTab1"] = value;
        }
    }

    private DataTable dtRequestFileTab2
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTab2"] != null)
                return (DataTable)ViewState["dtRequestFileTab2"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTab2"] = value;
        }
    }

    private DataTable dtRequestFileTab3
    {
        get
        {
            if ((DataTable)ViewState["dtRequestFileTab3"] != null)
                return (DataTable)ViewState["dtRequestFileTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtRequestFileTab3"] = value;
        }
    }

    private DataTable dtUploadTab1
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab1"] != null)
                return (DataTable)ViewState["dtUploadTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab1"] = value;
        }
    }

    private DataTable dtUploadTab2
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab2"] != null)
                return (DataTable)ViewState["dtUploadTab2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab2"] = value;
        }
    }

    private DataTable dtUploadTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTab3"] != null)
                return (DataTable)ViewState["dtUploadTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTab3"] = value;
        }
    }

    private DataTable dtUploadTypeTab1
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab1"] != null)
                return (DataTable)ViewState["dtUploadTypeTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab1"] = value;
        }
    }

    private DataTable dtUploadTypeTab2
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab2"] != null)
                return (DataTable)ViewState["dtUploadTypeTab2"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab2"] = value;
        }
    }

    private DataTable dtUploadTypeTab3
    {
        get
        {
            if ((DataTable)ViewState["dtUploadTypeTab3"] != null)
                return (DataTable)ViewState["dtUploadTypeTab3"];
            else
                return null;
        }
        set
        {
            ViewState["dtUploadTypeTab3"] = value;
        }
    }

    private DataTable dtContract
    {
        get
        {
            if ((DataTable)ViewState["dtContract"] != null)
                return (DataTable)ViewState["dtContract"];
            else
                return null;
        }
        set
        {
            ViewState["dtContract"] = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";
        if (!IsPostBack)
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            this.InitialForm();
            this.AssignAuthen();
        }

        //cmdSaveTab1.Visible = true;
        //cmdSaveTab1.Enabled = true;

        //cmdSaveTab2.Visible = true;
        //cmdSaveTab2.Enabled = true;

        //cmdSaveTab3.Visible = true;
        //cmdSaveTab3.Enabled = true;

        //cmdSaveTab4.Visible = true;
        //cmdSaveTab4.Enabled = true;
    }
    bool Permissions(string MENUID)
    {
        bool chkurl = false;
        if (Session["cPermission"] != null)
        {
            string[] url = (Session["cPermission"] + "").Split('|');
            string[] chkpermision;
            bool sbreak = false;

            foreach (string inurl in url)
            {
                chkpermision = inurl.Split(';');
                if (chkpermision[0] == MENUID)
                {
                    switch (chkpermision[1])
                    {
                        case "0":
                            chkurl = false;

                            break;
                        case "1":
                            Session["chkurl"] = "1";
                            chkurl = true;

                            break;

                        case "2":
                            Session["chkurl"] = "2";
                            chkurl = true;

                            break;
                    }
                    sbreak = true;
                }

                if (sbreak == true) break;
            }
        }

        return chkurl;
    }

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                cmdSelectTab5.Enabled = false;
                cmdSelectTab6.Enabled = false;
            }
            if (!CanWrite)
            {
                cmdUploadTab1.Enabled = false;
                cmdUploadTab2.Enabled = false;
                cmdUploadTab3.Enabled = false;
                cmdSaveTab1.Enabled = false;
                cmdSaveTab1Draft.Enabled = false;
                cmdSaveTab2.Enabled = false;
                cmdSaveTab2Draft.Enabled = false;
                cmdSaveTab3.Enabled = false;
                cmdSaveTab3Draft.Enabled = false;
                cmdSaveTab4.Enabled = false;
                cmdSaveTab4Draft.Enabled = false;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void InitialForm()
    {
        try
        {
            lblTab1.Text = "แนบไฟล์เอกสาร";
            lblTab2.Text = lblTab1.Text;
            lblTab3.Text = lblTab1.Text;

            cmdSaveTab1Draft.Enabled = false;
            cmdSaveTab1.Enabled = false;
            cmdUploadTab1.Enabled = false;

            cmdSaveTab2Draft.Enabled = false;
            cmdSaveTab2.Enabled = false;
            cmdUploadTab2.Enabled = false;

            cmdSaveTab3Draft.Enabled = false;
            cmdSaveTab3.Enabled = false;
            cmdUploadTab3.Enabled = false;

            DataTable dtLogin = new DataTable();
            dtLogin = ComplainBLL.Instance.LoginSelectBLL(" AND SUID = '" + Session["UserID"].ToString() + "'");

            ((HtmlAnchor)this.FindControlRecursive(Page, "ReportTab")).Visible = false;

            #region + Tab 6 +
            DataTable dtYearTab6 = MonthlyReportBLL.Instance.YearSelectBLL();
            DropDownListHelper.BindDropDownList(ref ddlYearTab6, dtYearTab6, "UPLOAD_YEAR", "UPLOAD_YEAR", true);

            DataTable dtVendorTab6 = ComplainBLL.Instance.VendorSelectBLL();
            DropDownListHelper.BindDropDownList(ref ddlVendorTab6, dtVendorTab6, "SVENDORID", "SABBREVIATION", true);

            DataTable dtStatusTab6 = new DataTable();
            dtStatusTab6.Columns.Add("DISPLAY");
            dtStatusTab6.Columns.Add("VALUE");
            dtStatusTab6.Rows.Add("ส่งรายงาน", "1");
            dtStatusTab6.Rows.Add("ไม่ส่งรายงาน", "0");
            DropDownListHelper.BindDropDownList(ref ddlStatusTab6, dtStatusTab6, "VALUE", "DISPLAY", true);
            #endregion

            //ถ้าไม่เป็น รข
            if ((!string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCode)) && (!string.Equals(dtLogin.Rows[0]["SVENDORID"].ToString(), ConfigValue.DeliveryDepartmentCodeAdmin2)))
            {
                //this.ScheduleReport();

                ((HtmlAnchor)this.FindControlRecursive(Page, "ScoreTab")).Visible = false;
                //((HtmlAnchor)this.FindControlRecursive(Page, "ReportTab")).Visible = false;
                //((HtmlAnchor)this.FindControlRecursive(Page, "ReportFinalVendor")).Visible = true;

                VendorID = Session["SVDID"].ToString();
                ddlVendorTab6.SelectedValue = VendorID;
                ddlVendorTab6.Enabled = false;

                if (ddlVendorTab6.SelectedIndex == 0)
                    alertSuccess("ไม่มีสิทธิ์ในการใช้งาน", "vendor_home.aspx");

                this.ContractSelect(VendorID);

                DataTable dt = new DataTable();
                dt = MonthlyReportBLL.Instance.CanUploadSelectBLL();

                if (dt.Rows.Count > 0)
                {
                    Year = dt.Rows[0]["YEAR"].ToString();
                    MonthID = int.Parse(dt.Rows[0]["MONTH"].ToString());

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (string.Equals(dt.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly1.ToString()))
                        {
                            lblSendReportDescTab1.Text = string.Format(dt.Rows[i]["DESCRIPTION_DETAIL"].ToString(), dt.Rows[i]["DESCRIPTION"].ToString(), dt.Rows[i]["START_UPLOAD_DAY"].ToString(), dt.Rows[i]["END_UPLOAD_DAY"].ToString());

                            lblTab1.Text = "แนบไฟล์เอกสาร {0}";
                            lblTab1.Text = string.Format(lblTab1.Text, dt.Rows[i]["DESCRIPTION"].ToString());
                            cmdSaveTab1Draft.Enabled = true;
                            cmdSaveTab1.Enabled = true;
                            cmdUploadTab1.Enabled = true;
                        }
                        else if (string.Equals(dt.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly2.ToString()))
                        {
                            lblSendReportDescTab2.Text = string.Format(dt.Rows[i]["DESCRIPTION_DETAIL"].ToString(), dt.Rows[i]["DESCRIPTION"].ToString(), dt.Rows[i]["START_UPLOAD_DAY"].ToString(), dt.Rows[i]["END_UPLOAD_DAY"].ToString());

                            lblTab2.Text = "แนบไฟล์เอกสาร {0}";
                            lblTab2.Text = string.Format(lblTab2.Text, dt.Rows[i]["DESCRIPTION"].ToString());
                            cmdSaveTab2Draft.Enabled = true;
                            cmdSaveTab2.Enabled = true;
                            cmdUploadTab2.Enabled = true;
                        }
                        else if (string.Equals(dt.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly3.ToString()))
                        {
                            lblSendReportDescTab3.Text = string.Format(dt.Rows[i]["DESCRIPTION_DETAIL"].ToString(), dt.Rows[i]["DESCRIPTION"].ToString(), dt.Rows[i]["START_UPLOAD_DAY"].ToString(), dt.Rows[i]["END_UPLOAD_DAY"].ToString());

                            lblTab3.Text = "แนบไฟล์เอกสาร {0}";
                            lblTab3.Text = string.Format(lblTab3.Text, dt.Rows[i]["DESCRIPTION"].ToString());
                            cmdSaveTab3Draft.Enabled = true;
                            cmdSaveTab3.Enabled = true;
                            cmdUploadTab3.Enabled = true;
                        }
                    }
                }
            }
            else
            {
                ((HtmlAnchor)this.FindControlRecursive(Page, "ScoreTab")).Visible = true;
                //((HtmlAnchor)this.FindControlRecursive(Page, "ReportTab")).Visible = true;
                //((HtmlAnchor)this.FindControlRecursive(Page, "ReportFinalVendor")).Visible = false;

                DataTable dtYear = MonthlyReportBLL.Instance.YearSelectBLL();
                DropDownListHelper.BindDropDownList(ref ddlYear, dtYear, "UPLOAD_YEAR", "UPLOAD_YEAR", true);

                dtYear = MonthlyReportBLL.Instance.YearSelectBLL();
                DropDownListHelper.BindDropDownList(ref ddlYearTab5, dtYear, "UPLOAD_YEAR", "UPLOAD_YEAR", true);

                DataTable dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DropDownListHelper.BindDropDownList(ref ddlVendor, dtVendor, "SVENDORID", "SABBREVIATION", true);

                dtVendor = ComplainBLL.Instance.VendorSelectBLL();
                DropDownListHelper.BindDropDownList(ref ddlVendorTab5, dtVendor, "SVENDORID", "SABBREVIATION", true);

                DataTable dtStatusTab5 = new DataTable();
                dtStatusTab5.Columns.Add("DISPLAY");
                dtStatusTab5.Columns.Add("VALUE");
                dtStatusTab5.Rows.Add("ส่งรายงาน", "1");
                dtStatusTab5.Rows.Add("ไม่ส่งรายงาน", "0");
                DropDownListHelper.BindDropDownList(ref ddlStatus, dtStatusTab5, "VALUE", "DISPLAY", true);
            }

            dtRequestFileTab1 = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'REPORT_MONTHLY_1'");
            GridViewHelper.BindGridView(ref dgvRequestFileTab1, dtRequestFileTab1);

            dtRequestFileTab2 = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'REPORT_MONTHLY_3'");
            GridViewHelper.BindGridView(ref dgvRequestFileTab2, dtRequestFileTab2);

            dtRequestFileTab3 = MonthlyReportBLL.Instance.MonthlyReportRequestFileBLL(" AND M_COMPLAIN_REQUEST_FILE.REQUEST_TYPE = 'REPORT_MONTHLY_6'");
            GridViewHelper.BindGridView(ref dgvRequestFileTab3, dtRequestFileTab3);

            dtUploadTypeTab1 = UploadTypeBLL.Instance.UploadTypeSelectBLL("REPORT_MONTHLY_1");
            DropDownListHelper.BindDropDownList(ref cboUploadTypeTab1, dtUploadTypeTab1, "UPLOAD_ID", "UPLOAD_NAME", true);

            dtUploadTypeTab2 = UploadTypeBLL.Instance.UploadTypeSelectBLL("REPORT_MONTHLY_3");
            DropDownListHelper.BindDropDownList(ref cboUploadTypeTab2, dtUploadTypeTab2, "UPLOAD_ID", "UPLOAD_NAME", true);

            dtUploadTypeTab3 = UploadTypeBLL.Instance.UploadTypeSelectBLL("REPORT_MONTHLY_6");
            DropDownListHelper.BindDropDownList(ref cboUploadTypeTab3, dtUploadTypeTab3, "UPLOAD_ID", "UPLOAD_NAME", true);

            DataTable dtDateNow = MonthlyReportBLL.Instance.GetDateNowBLL();
            lblDateNow.Text = string.Format(lblDateNow.Text, dtDateNow.Rows[0]["TODAY"].ToString());
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ContractSelect(string VendorID)
    {
        try
        {
            dtContract = ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + VendorID + "'");
            DropDownListHelper.BindDropDownList(ref cboContractTab1, dtContract, "SCONTRACTID", "SCONTRACTNO", true);

            dtContract = ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + VendorID + "'");
            DropDownListHelper.BindDropDownList(ref cboContractTab2, dtContract, "SCONTRACTID", "SCONTRACTNO", true);

            dtContract = ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + VendorID + "'");
            DropDownListHelper.BindDropDownList(ref cboContractTab3, dtContract, "SCONTRACTID", "SCONTRACTNO", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cboContractTab1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((cboContractTab1.SelectedIndex > 0) && (cmdSaveTab1.Enabled))
            {
                dtUploadTab1 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(Year + "-" + MonthID.ToString() + "-" + ConfigValue.Monthly1 + "-" + VendorID, "REPORT_MONTHLY_1", " AND TCONTRACT.SCONTRACTID = " + cboContractTab1.SelectedValue);
                dtUploadTab1.Columns.Remove("IS_VENDOR_DOWNLOAD");
                GridViewHelper.BindGridView(ref dgvUploadFileTab1, dtUploadTab1);
            }
            else
            {
                dtUploadTab1 = null;
                GridViewHelper.BindGridView(ref dgvUploadFileTab1, dtUploadTab1);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cboContractTab2_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((cboContractTab2.SelectedIndex > 0) && (cmdSaveTab2.Enabled))
            {
                dtUploadTab2 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(Year + "-" + MonthID.ToString() + "-" + ConfigValue.Monthly2 + "-" + VendorID, "REPORT_MONTHLY_3", " AND TCONTRACT.SCONTRACTID = " + cboContractTab2.SelectedValue);
                dtUploadTab2.Columns.Remove("IS_VENDOR_DOWNLOAD");
                GridViewHelper.BindGridView(ref dgvUploadFileTab2, dtUploadTab2);
            }
            else
            {
                dtUploadTab2 = null;
                GridViewHelper.BindGridView(ref dgvUploadFileTab2, dtUploadTab2);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cboContractTab3_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((cboContractTab3.SelectedIndex > 0) && (cmdSaveTab3.Enabled))
            {
                dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(Year + "-" + MonthID.ToString() + "-" + ConfigValue.Monthly3 + "-" + VendorID, "REPORT_MONTHLY_6", " AND TCONTRACT.SCONTRACTID = " + cboContractTab3.SelectedValue);
                dtUploadTab3.Columns.Remove("IS_VENDOR_DOWNLOAD");
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
            }
            else
            {
                dtUploadTab3 = null;
                GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ScheduleReport()
    {
        try
        {
            DataTable dtSendAll = MonthlyReportBLL.Instance.CanUploadSelectAllBLL();
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            for (int i = 0; i < dtSendAll.Rows.Count; i++)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(string.Format(dtSendAll.Rows[i]["DESCRIPTION_DETAIL"].ToString(), dtSendAll.Rows[i]["DESCRIPTION"].ToString(), dtSendAll.Rows[i]["START_UPLOAD_DAY"].ToString(), dtSendAll.Rows[i]["END_UPLOAD_DAY"].ToString()));
                sb.Append("</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            alertSuccess(sb.ToString());
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }

    private void ValidateUploadFile(string ExtentionUser, int FileSize, DataTable dtUploadType, DropDownList cboUploadType)
    {
        try
        {
            string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
            int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

            if (FileSize > MaxSize)
                throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

            if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "MonthlyReport" + "\\" + Year + "\\" + MonthID.ToString();
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void StartUpload(DataTable dtUploadType, DataTable dtUpload, GridView dgvUploadFile, DropDownList cboUploadType, FileUpload fileUpload, DropDownList cboContract)
    {
        try
        {
            if (cboUploadType.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกประเภทไฟล์เอกสาร");
                return;
            }

            if (cboContract.SelectedIndex == 0)
            {
                alertFail("กรุณาเลือกเลขที่สัญญา");
                return;
            }

            if ((dtUpload.Rows.Count > 0) && (dtUpload.Rows[0]["REF_INT"].ToString() != cboContract.SelectedValue))
            {
                alertFail("อัพโหลดเอกสารได้ครั้งละ 1 เลขที่สัญญา");
                return;
            }

            if (fileUpload.HasFile)
            {
                string FileNameUser = fileUpload.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                this.ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength, dtUploadType, cboUploadType);

                string Path = this.CheckPath();
                fileUpload.SaveAs(Path + "\\" + FileNameSystem);

                //dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);

                //if ((dtUpload == null) || (dtUpload.Columns.Count == 0))
                //{
                //    dtUpload = new DataTable();
                //    dtUpload.Columns.Add("UPLOAD_ID");
                //    dtUpload.Columns.Add("UPLOAD_NAME");
                //    dtUpload.Columns.Add("FILENAME_SYSTEM");
                //    dtUpload.Columns.Add("FILENAME_USER");
                //    dtUpload.Columns.Add("FULLPATH");
                //}

                dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem, cboContract.SelectedValue, cboContract.SelectedItem.ToString());
                GridViewHelper.BindGridView(ref dgvUploadFile, dtUpload);
            }
            else
            {
                alertFail("กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.ContentType = "APPLICATION/OCTET-STREAM";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private void RemoveUpload(DataTable dtUpload, GridView dgvUpload, int RowIndex)
    {
        try
        {
            dtUpload.Rows.RemoveAt(RowIndex);
            GridViewHelper.BindGridView(ref dgvUpload, dtUpload);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void UpdateUpload(GridView dgvUpload, int RowIndex)
    {
        try
        {
            string FullPath = dgvUpload.DataKeys[RowIndex]["FULLPATH"].ToString();
            this.DownloadFile(Path.GetDirectoryName(FullPath), Path.GetFileName(FullPath));
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ConfirmUpload(GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgDelete;
                imgDelete = (ImageButton)e.Row.Cells[0].FindControl("imgDelete");
                imgDelete.Attributes.Add("onclick", "javascript:if(confirm('ต้องการลบข้อมูลไฟล์นี้\\nใช่หรือไม่ ?')==false){return false;}");
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdUploadTab1_Click(object sender, EventArgs e)
    {
        this.StartUpload(dtUploadTypeTab1, dtUploadTab1, dgvUploadFileTab1, cboUploadTypeTab1, fileUploadTab1, cboContractTab1);
    }

    protected void dgvUploadFileTab1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        this.RemoveUpload(dtUploadTab1, dgvUploadFileTab1, e.RowIndex);
    }

    protected void dgvUploadFileTab1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.UpdateUpload(dgvUploadFileTab1, e.RowIndex);
    }

    protected void dgvUploadFileTab1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        this.ConfirmUpload(e);
    }

    protected void cmdUploadTab2_Click(object sender, EventArgs e)
    {
        this.StartUpload(dtUploadTypeTab2, dtUploadTab2, dgvUploadFileTab2, cboUploadTypeTab2, fileUploadTab2, cboContractTab2);
    }

    protected void dgvUploadFileTab2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        this.RemoveUpload(dtUploadTab2, dgvUploadFileTab2, e.RowIndex);
    }

    protected void dgvUploadFileTab2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.UpdateUpload(dgvUploadFileTab2, e.RowIndex);
    }

    protected void dgvUploadFileTab2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        this.ConfirmUpload(e);
    }

    protected void cmdUploadTab3_Click(object sender, EventArgs e)
    {
        this.StartUpload(dtUploadTypeTab3, dtUploadTab3, dgvUploadFileTab3, cboUploadTypeTab3, fileUploadTab3, cboContractTab3);
    }

    protected void dgvUploadFileTab3_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        this.RemoveUpload(dtUploadTab3, dgvUploadFileTab3, e.RowIndex);
    }

    protected void dgvUploadFileTab3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        this.UpdateUpload(dgvUploadFileTab3, e.RowIndex);
    }

    protected void dgvUploadFileTab3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        this.ConfirmUpload(e);
    }

    protected void cmdSaveTab1Draft_Click(object sender, EventArgs e)
    {
        this.SaveData(false, ConfigValue.MonthlyStatus1, ConfigValue.Monthly1, dtUploadTab1, -1, 0, 0, dtRequestFileTab1, cboContractTab1);
    }

    protected void cmdSaveTab1_Click(object sender, EventArgs e)
    {
        this.SaveData(true, ConfigValue.MonthlyStatus2, ConfigValue.Monthly1, dtUploadTab1, -1, 1, 0, dtRequestFileTab1, cboContractTab1);
    }

    protected void cmdSaveTab2Draft_Click(object sender, EventArgs e)
    {
        this.SaveData(false, ConfigValue.MonthlyStatus1, ConfigValue.Monthly2, dtUploadTab2, -1, 0, 0, dtRequestFileTab2, cboContractTab2);
    }

    protected void cmdSaveTab2_Click(object sender, EventArgs e)
    {
        this.SaveData(true, ConfigValue.MonthlyStatus2, ConfigValue.Monthly2, dtUploadTab2, -1, 1, 0, dtRequestFileTab2, cboContractTab2);
    }

    protected void cmdSaveTab3Draft_Click(object sender, EventArgs e)
    {
        this.SaveData(false, ConfigValue.MonthlyStatus1, ConfigValue.Monthly3, dtUploadTab3, -1, 0, 0, dtRequestFileTab3, cboContractTab3);
    }

    protected void cmdSaveTab3_Click(object sender, EventArgs e)
    {
        this.SaveData(true, ConfigValue.MonthlyStatus2, ConfigValue.Monthly3, dtUploadTab3, -1, 1, 0, dtRequestFileTab3, cboContractTab3);
    }

    private void SaveData(bool IsValidate, int StatusID, int ReportID, DataTable dtUpload, int IsPass, int IsUploadComplete, int CusScore, DataTable dtRequestFile, DropDownList cboContract)
    {
        try
        {
            //if (IsValidate)
            //    this.ValidateSaveData(dtUpload, dtRequestFile);

            if (cboContract.SelectedIndex < 1)
            {
                alertFail("กรุณาเลือกเลขที่สัญญา");
                return;
            }

            int isSaveConfirmDate = 0;
            if (StatusID == ConfigValue.MonthlyStatus2)
                isSaveConfirmDate = 1;

            MonthlyReportBLL.Instance.ReportSaveBLL(true, dtUpload, Year, MonthID, VendorID, int.Parse(Session["UserID"].ToString()), StatusID, ReportID, IsPass, IsUploadComplete, CusScore, isSaveConfirmDate, cboContract.SelectedValue);
            alertSuccess("บันทึกข้อมูลเรียบร้อย");
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSaveData(DataTable dtUpload, DataTable dtRequestFile)
    {
        try
        {
            //dtRequestFile, dtUpload
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            if (dtUpload.Rows.Count == 0)
            {
                for (int j = 0; j < dtRequestFile.Rows.Count; j++)
                {
                    if (j == 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>โปรดแนบเอกสาร</td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr>");
                        sb.Append("<td></td>");
                        sb.Append("<td>");
                        sb.Append(">" + dtRequestFile.Rows[j]["UPLOAD_NAME"].ToString());
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            else
            {
                for (int i = 0; i < dtRequestFile.Rows.Count; i++)
                {
                    for (int j = 0; j < dtUpload.Rows.Count; j++)
                    {
                        if (string.Equals(dtRequestFile.Rows[i]["UPLOAD_ID"].ToString(), dtUpload.Rows[j]["UPLOAD_ID"].ToString()))
                            break;

                        if (j == dtUpload.Rows.Count - 1)
                        {
                            if (string.Equals(sb.ToString(), "<table>"))
                            {
                                sb.Append("<tr>");
                                sb.Append("<td>โปรดแนบเอกสาร</td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                            else
                            {
                                sb.Append("<tr>");
                                sb.Append("<td></td>");
                                sb.Append("<td>");
                                sb.Append(">" + dtRequestFile.Rows[i]["UPLOAD_NAME"].ToString());
                                sb.Append("</td>");
                                sb.Append("</tr>");
                            }
                        }
                    }
                }
            }
            sb.Append("</table>");

            if (!string.Equals(sb.ToString(), "<table></table>"))
            {
                throw new Exception(sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtMonth = MonthlyReportBLL.Instance.MonthSelectBLL(ddlYear.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlMonth, dtMonth, "MONTH_ID", "NAME_TH", true);

            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ShowRowReport(false);
            this.LoadContract();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void LoadContract()
    {
        try
        {
            DataTable dtContract = new DataTable();
            if (ddlVendor.SelectedIndex > 0)
                DropDownListHelper.BindDropDownList(ref ddlContract, ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + ddlVendor.SelectedValue + "'"), "SCONTRACTID", "SCONTRACTNO", true);
            else
                DropDownListHelper.BindDropDownList(ref ddlContract, null, string.Empty, string.Empty, false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadContractTab()
    {
        try
        {
            DataTable dtContract = new DataTable();
            if (ddlVendor.SelectedIndex > 0)
            {
                DropDownListHelper.BindDropDownList(ref cboContractTab1, ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + ddlVendor.SelectedValue + "'"), "SCONTRACTID", "SCONTRACTNO", true);
                DropDownListHelper.BindDropDownList(ref cboContractTab2, ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + ddlVendor.SelectedValue + "'"), "SCONTRACTID", "SCONTRACTNO", true);
                DropDownListHelper.BindDropDownList(ref cboContractTab3, ComplainBLL.Instance.ContractSelectBLL(" AND TCONTRACT.SVENDORID = '" + ddlVendor.SelectedValue + "'"), "SCONTRACTID", "SCONTRACTNO", true);

                cboContractTab1.SelectedValue = ddlContract.SelectedValue;
                cboContractTab2.SelectedValue = ddlContract.SelectedValue;
                cboContractTab3.SelectedValue = ddlContract.SelectedValue;

                this.ShowRowReport(false);
            }
            else
            {
                DropDownListHelper.BindDropDownList(ref cboContractTab1, null, string.Empty, string.Empty, false);
                DropDownListHelper.BindDropDownList(ref cboContractTab2, null, string.Empty, string.Empty, false);
                DropDownListHelper.BindDropDownList(ref cboContractTab3, null, string.Empty, string.Empty, false);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSelect_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSelect();

            tblCusScore.Visible = true;
            this.ShowRowReport(false);

            radReport1Score.Enabled = true;
            radReport2Score.Enabled = true;
            radReport3Score.Enabled = true;

            lblVendorTab1.Text = "เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ) ({0})";
            lblVendorTab2.Text = "เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ) ({0})";
            lblVendorTab3.Text = "เอกสารที่ต้องแนบประกอบการพิจารณา (บังคับ) ({0})";

            lblVendorTab1.Text = string.Format(lblVendorTab1.Text, ddlVendor.SelectedItem);
            lblVendorTab2.Text = string.Format(lblVendorTab2.Text, ddlVendor.SelectedItem);
            lblVendorTab3.Text = string.Format(lblVendorTab3.Text, ddlVendor.SelectedItem);

            cmdSaveTab4.Enabled = false;
            cmdSaveTab4Draft.Enabled = false;

            this.LoadContractTab();

            dtUploadTab1 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(ddlYear.SelectedValue + "-" + ddlMonth.SelectedValue + "-" + ConfigValue.Monthly1 + "-" + ddlVendor.SelectedValue, "REPORT_MONTHLY_1", " AND F_UPLOAD.REF_INT = " + ddlContract.SelectedValue);
            GridViewHelper.BindGridView(ref dgvUploadFileTab1, dtUploadTab1);

            dtUploadTab2 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(ddlYear.SelectedValue + "-" + ddlMonth.SelectedValue + "-" + ConfigValue.Monthly2 + "-" + ddlVendor.SelectedValue, "REPORT_MONTHLY_3", " AND F_UPLOAD.REF_INT = " + ddlContract.SelectedValue);
            GridViewHelper.BindGridView(ref dgvUploadFileTab2, dtUploadTab2);

            dtUploadTab3 = ComplainImportFileBLL.Instance.ImportFileSelectBLL(ddlYear.SelectedValue + "-" + ddlMonth.SelectedValue + "-" + ConfigValue.Monthly3 + "-" + ddlVendor.SelectedValue, "REPORT_MONTHLY_6", " AND F_UPLOAD.REF_INT = " + ddlContract.SelectedValue);
            GridViewHelper.BindGridView(ref dgvUploadFileTab3, dtUploadTab3);

            DataTable dtCusScore = MonthlyReportBLL.Instance.CusScoreSelectBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), ddlVendor.SelectedValue, ddlContract.SelectedValue);
            for (int i = 0; i < dtCusScore.Rows.Count; i++)
            {
                if (!string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus4.ToString()))
                {
                    cmdSaveTab4.Enabled = true;
                    cmdSaveTab4Draft.Enabled = true;
                }
            }

            for (int i = 0; i < dtCusScore.Rows.Count; i++)
            {
                if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly1.ToString()))
                {
                    rowReport1_1.Visible = true;
                    rowReport1_2.Visible = true;
                    rowReport1_3.Visible = true;
                    rowReport1_4.Visible = true;

                    if (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y"))
                        radReport1Score.SelectedValue = dtCusScore.Rows[i]["IS_PASS"].ToString();
                    else
                    {
                        radReport1Score.Enabled = false;
                        radReport1Score.SelectedValue = "0";
                    }

                    txtReport1Remark.Value = dtCusScore.Rows[i]["DESCRIPTION"].ToString();

                    lblReport1IsComplete.Text = (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "1")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "5")
                                             && !string.Equals(dtCusScore.Rows[i]["CONFIRM_DATETIME"].ToString(), string.Empty)) ? "- อัพโหลดเอกสาร : ครบ" : "- อัพโหลดเอกสาร : ไม่ครบ";
                    lblReport1Status.Text = "- สถานะเอกสาร : " + dtCusScore.Rows[i]["DOC_STATUS_NAME"].ToString();

                    if (string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus1.ToString()) || string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus5.ToString()))
                    {
                        radReport1Score.Enabled = false;
                        radReport1Score.SelectedValue = "0";
                    }
                }
                else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly2.ToString()))
                {
                    rowReport2_1.Visible = true;
                    rowReport2_2.Visible = true;
                    rowReport2_3.Visible = true;
                    rowReport2_4.Visible = true;

                    if (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y"))
                        radReport2Score.SelectedValue = dtCusScore.Rows[i]["IS_PASS"].ToString();
                    else
                    {
                        radReport2Score.Enabled = false;
                        radReport2Score.SelectedValue = "0";
                    }

                    txtReport2Remark.Value = dtCusScore.Rows[i]["DESCRIPTION"].ToString();

                    lblReport2IsComplete.Text = (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "1")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "5")
                                             && !string.Equals(dtCusScore.Rows[i]["CONFIRM_DATETIME"].ToString(), string.Empty)) ? "- อัพโหลดเอกสาร : ครบ" : "- อัพโหลดเอกสาร : ไม่ครบ";
                    lblReport2Status.Text = "- สถานะเอกสาร : " + dtCusScore.Rows[i]["DOC_STATUS_NAME"].ToString();

                    if (string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus1.ToString()) || string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus5.ToString()))
                    {
                        radReport2Score.Enabled = false;
                        radReport2Score.SelectedValue = "0";
                    }
                }
                else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly3.ToString()))
                {
                    rowReport3_1.Visible = true;
                    rowReport3_2.Visible = true;
                    rowReport3_3.Visible = true;
                    rowReport3_4.Visible = true;

                    if (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y"))
                        radReport3Score.SelectedValue = dtCusScore.Rows[i]["IS_PASS"].ToString();
                    else
                    {
                        radReport3Score.Enabled = false;
                        radReport3Score.SelectedValue = "0";
                    }

                    txtReport3Remark.Value = dtCusScore.Rows[i]["DESCRIPTION"].ToString();

                    lblReport3IsComplete.Text = (string.Equals(dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString(), "Y")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "1")
                                             && !string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), "5")
                                             && !string.Equals(dtCusScore.Rows[i]["CONFIRM_DATETIME"].ToString(), string.Empty)) ? "- อัพโหลดเอกสาร : ครบ" : "- อัพโหลดเอกสาร : ไม่ครบ";
                    lblReport3Status.Text = "- สถานะเอกสาร : " + dtCusScore.Rows[i]["DOC_STATUS_NAME"].ToString();

                    if (string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus1.ToString()) || string.Equals(dtCusScore.Rows[i]["DOC_STATUS_ID"].ToString(), ConfigValue.MonthlyStatus5.ToString()))
                    {
                        radReport3Score.Enabled = false;
                        radReport3Score.SelectedValue = "0";
                    }
                }

                if (!cmdSaveTab4.Enabled)
                {
                    radReport1Score.Enabled = false;
                    radReport2Score.Enabled = false;
                    radReport3Score.Enabled = false;

                    txtReport1Remark.Disabled = true;
                    txtReport2Remark.Disabled = true;
                    txtReport3Remark.Disabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            tblCusScore.Visible = false;
            alertFail(ex.Message);
        }
    }

    private void ShowRowReport(bool Visible)
    {
        rowReport1_1.Visible = Visible;
        rowReport1_2.Visible = Visible;
        rowReport1_3.Visible = Visible;
        rowReport1_4.Visible = Visible;

        radReport1Score.SelectedValue = null;
        txtReport1Remark.Value = string.Empty;
        lblReport1IsComplete.Text = "- อัพโหลดเอกสาร : ";
        lblReport1Status.Text = "- สถานะเอกสาร : ";

        rowReport2_1.Visible = Visible;
        rowReport2_2.Visible = Visible;
        rowReport2_3.Visible = Visible;
        rowReport2_4.Visible = Visible;

        radReport2Score.SelectedValue = null;
        txtReport2Remark.Value = string.Empty;
        lblReport2IsComplete.Text = "- อัพโหลดเอกสาร : ";
        lblReport2Status.Text = "- สถานะเอกสาร : ";

        rowReport3_1.Visible = Visible;
        rowReport3_2.Visible = Visible;
        rowReport3_3.Visible = Visible;
        rowReport3_4.Visible = Visible;

        radReport3Score.SelectedValue = null;
        txtReport3Remark.Value = string.Empty;
        lblReport3IsComplete.Text = "- อัพโหลดเอกสาร : ";
        lblReport3Status.Text = "- สถานะเอกสาร : ";
    }

    private void ValidateSelect()
    {
        try
        {
            if (ddlYear.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ปี (ค.ศ.)");

            if (ddlMonth.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก เดือน");

            if (ddlVendor.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ผู้ประกอบการขนส่ง");

            if (ddlContract.SelectedIndex < 1)
                throw new Exception("กรุณาเลือก เลขที่สัญญา");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSaveTab4Draft_Click(object sender, EventArgs e)
    {
        this.SaveDataTab4(false, ConfigValue.MonthlyStatus3);
    }

    protected void cmdSaveTab4_Click(object sender, EventArgs e)
    {
        this.SaveDataTab4(true, ConfigValue.MonthlyStatus4);
    }

    private void SaveDataTab4(bool IsValidate, int StatusID)
    {
        try
        {
            if (IsValidate)
                this.ValidateSaveTab4();

            DataTable dt = new DataTable();
            dt.Columns.Add("REPORT_ID");
            dt.Columns.Add("IS_PASS");
            dt.Columns.Add("DESCRIPTION");
            dt.Columns.Add("CUS_SCORE");
            dt.Columns.Add("IS_UPLOAD_COMPLETE");

            string IsUploadCompleteReport1 = string.Empty;
            string IsUploadCompleteReport2 = string.Empty;
            string IsUploadCompleteReport3 = string.Empty;
            DataTable dtCusScore = MonthlyReportBLL.Instance.CusScoreSelectBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), ddlVendor.SelectedValue, ddlContract.SelectedValue);
            for (int i = 0; i < dtCusScore.Rows.Count; i++)
            {
                if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly1.ToString()))
                    IsUploadCompleteReport1 = dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString();
                else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly2.ToString()))
                    IsUploadCompleteReport2 = dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString();
                else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly3.ToString()))
                    IsUploadCompleteReport3 = dtCusScore.Rows[i]["IS_UPLOAD_COMPLETE"].ToString();
            }

            if (rowReport1_1.Visible)
                dt.Rows.Add(ConfigValue.Monthly1, radReport1Score.SelectedValue, txtReport1Remark.Value.Trim(), (radReport1Score.SelectedValue) == "0" ? "1" : "0", IsUploadCompleteReport1);

            if (rowReport2_1.Visible)
                dt.Rows.Add(ConfigValue.Monthly2, radReport2Score.SelectedValue, txtReport2Remark.Value.Trim(), (radReport2Score.SelectedValue) == "0" ? "1" : "0", IsUploadCompleteReport2);

            if (rowReport3_1.Visible)
                dt.Rows.Add(ConfigValue.Monthly3, radReport3Score.SelectedValue, txtReport3Remark.Value.Trim(), (radReport3Score.SelectedValue) == "0" ? "1" : "0", IsUploadCompleteReport3);

            MonthlyReportBLL.Instance.ReportSaveTab4BLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), ddlVendor.SelectedValue, StatusID, dt, int.Parse(Session["UserID"].ToString()), IsValidate, ddlContract.SelectedValue);
            alertSuccess("บันทึกข้อมูลเรียบร้อย");

            if (IsValidate)
                this.SendEmail(ConfigValue.EmailReportMonthlyNotPass);

            this.ClearScreen();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void ValidateSaveTab4()
    {
        try
        {
            if (rowReport1_1.Visible)
            {
                if (radReport1Score.SelectedValue == "-1")
                    throw new Exception("กรุณาเลือกผลการตัดคะแนน รายงานประจำเดือน");

                if ((radReport1Score.SelectedValue == "0") && (radReport1Score.Enabled) && (string.Equals(txtReport1Remark.Value.Trim(), string.Empty)))
                    throw new Exception("รายงานประจำเดือน ในกรณีตัดคะแนน ให้ระบุหมายเหตุ");
            }

            if (rowReport2_1.Visible)
            {
                if (radReport2Score.SelectedValue == "-1")
                    throw new Exception("กรุณาเลือกผลการตัดคะแนน รายงานประจำ 3 เดือน");

                if ((radReport2Score.SelectedValue == "0") && (radReport2Score.Enabled) && (string.Equals(txtReport2Remark.Value.Trim(), string.Empty)))
                    throw new Exception("รายงานประจำ 3 เดือน ในกรณีตัดคะแนน ให้ระบุหมายเหตุ");
            }

            if (rowReport3_1.Visible)
            {
                if (radReport3Score.SelectedValue == "-1")
                    throw new Exception("กรุณาเลือกผลการตัดคะแนน รายงานประจำ 6 เดือน");

                if ((radReport3Score.SelectedValue == "0") && (radReport3Score.Enabled) && (string.Equals(txtReport3Remark.Value.Trim(), string.Empty)))
                    throw new Exception("รายงานประจำ 6 เดือน ในกรณีตัดคะแนน ให้ระบุหมายเหตุ");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ClearScreen()
    {
        try
        {
            ddlYear.SelectedIndex = 0;
            ddlMonth.SelectedIndex = 0;
            ddlVendor.SelectedIndex = 0;
            ddlContract.SelectedIndex = 0;

            GridViewHelper.BindGridView(ref dgvUploadFileTab1, null);
            GridViewHelper.BindGridView(ref dgvUploadFileTab2, null);
            GridViewHelper.BindGridView(ref dgvUploadFileTab3, null);

            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYearTab5_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtMonth = MonthlyReportBLL.Instance.MonthSelectBLL(ddlYearTab5.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlMonthTab5, dtMonth, "MONTH_ID", "NAME_TH", true);

            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void ddlMonthTab5_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab5();
    }

    protected void ddlVendorTab5_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab5();
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab5();
    }

    private void ClearTab5()
    {
        try
        {
            GridViewHelper.BindGridView(ref dgvTab5, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSelectTab5_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSelectTab5();

            DataTable dtData = MonthlyReportBLL.Instance.ReportCheckSelectBLL(ddlYearTab5.SelectedValue, int.Parse(ddlMonthTab5.SelectedValue), this.GetConditionSearchTab5());

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                dtData.Rows[i]["REPORT_1"] = MonthlyReportBLL.Instance.ReportCheckSelectDetailBLL(ddlYearTab5.SelectedValue, int.Parse(ddlMonthTab5.SelectedValue), this.GetConditionSearchDetailTab5(dtData.Rows[i]["SVENDORID"].ToString(), "1"));
                dtData.Rows[i]["REPORT_2"] = MonthlyReportBLL.Instance.ReportCheckSelectDetailBLL(ddlYearTab5.SelectedValue, int.Parse(ddlMonthTab5.SelectedValue), this.GetConditionSearchDetailTab5(dtData.Rows[i]["SVENDORID"].ToString(), "2"));
                dtData.Rows[i]["REPORT_3"] = MonthlyReportBLL.Instance.ReportCheckSelectDetailBLL(ddlYearTab5.SelectedValue, int.Parse(ddlMonthTab5.SelectedValue), this.GetConditionSearchDetailTab5(dtData.Rows[i]["SVENDORID"].ToString(), "3"));
            }

            if (ddlStatus.SelectedIndex > 0)
            {
                string strCheck = (string.Equals(ddlStatus.SelectedValue, "1")) ? "O" : "X";

                DataTable dt = new DataTable();
                dt = dtData.Clone();

                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if (string.Equals(dtData.Rows[i]["REPORT_1"].ToString(), strCheck) || string.Equals(dtData.Rows[i]["REPORT_2"].ToString(), strCheck) || string.Equals(dtData.Rows[i]["REPORT_3"].ToString(), strCheck))
                        dt.Rows.Add(dtData.Rows[i]["SVENDORID"].ToString(), dtData.Rows[i]["SABBREVIATION"].ToString(), dtData.Rows[i]["REPORT_1"].ToString(), dtData.Rows[i]["REPORT_2"].ToString(), dtData.Rows[i]["REPORT_3"].ToString());
                }
                GridViewHelper.BindGridView(ref dgvTab5, dt);
            }
            else
                GridViewHelper.BindGridView(ref dgvTab5, dtData);

            for (int i = 0; i < dgvTab5.Rows.Count; i++)
            {
                dgvTab5.Rows[i].Cells[1].ForeColor = (string.Equals(dgvTab5.Rows[i].Cells[1].Text, "O")) ? Color.Green : Color.Red;
                dgvTab5.Rows[i].Cells[2].ForeColor = (string.Equals(dgvTab5.Rows[i].Cells[2].Text, "O")) ? Color.Green : Color.Red;
                dgvTab5.Rows[i].Cells[3].ForeColor = (string.Equals(dgvTab5.Rows[i].Cells[3].Text, "O")) ? Color.Green : Color.Red;
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionSearchTab5()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlVendorTab5.SelectedIndex > 0)
                sb.Append(" AND VENDOR_ID = '" + ddlVendorTab5.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionSearchDetailTab5(string VendorID, string ReportID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND VENDOR_ID = '" + VendorID + "'");
            sb.Append(" AND REPORT_ID = '" + ReportID + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSelectTab5()
    {
        try
        {
            if (ddlYearTab5.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ปี (ค.ศ.)");

            if (ddlMonthTab5.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก เดือน");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void ddlYearTab6_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtMonth = MonthlyReportBLL.Instance.MonthSelectBLL(ddlYearTab6.SelectedValue);
            DropDownListHelper.BindDropDownList(ref ddlMonthTab6, dtMonth, "MONTH_ID", "NAME_TH", true);

            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void ddlMonthTab6_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab6();
    }

    protected void ddlVendorTab6_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab6();
    }

    protected void ddlStatusTab6_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ClearTab6();
    }

    private void ClearTab6()
    {
        try
        {
            GridViewHelper.BindGridView(ref dgvTab6, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    protected void cmdSelectTab6_Click(object sender, EventArgs e)
    {
        try
        {
            this.ValidateSelectTab6();

            DataTable dtData = MonthlyReportBLL.Instance.ReportCheckSelectTab6BLL(ddlYearTab6.SelectedValue, this.GetConditionSearchTab6());
            string Status = string.Empty;
            string StatusDate = string.Empty;
            string RefString1 = string.Empty;
            string RefString2 = string.Empty;
            string RefString3 = string.Empty;

            for (int i = 0; i < dtData.Rows.Count; i++)
            {
                RefString1 = ddlYearTab6.SelectedValue + "-" + dtData.Rows[i]["MONTH_ID"].ToString() + "-" + ConfigValue.Monthly1.ToString() + "-" + dtData.Rows[i]["SVENDORID"].ToString();
                RefString2 = ddlYearTab6.SelectedValue + "-" + dtData.Rows[i]["MONTH_ID"].ToString() + "-" + ConfigValue.Monthly2.ToString() + "-" + dtData.Rows[i]["SVENDORID"].ToString();
                RefString3 = ddlYearTab6.SelectedValue + "-" + dtData.Rows[i]["MONTH_ID"].ToString() + "-" + ConfigValue.Monthly3.ToString() + "-" + dtData.Rows[i]["SVENDORID"].ToString();

                MonthlyReportBLL.Instance.ReportCheckSelectDetailTab6BLL(ddlYearTab6.SelectedValue, int.Parse(dtData.Rows[i]["MONTH_ID"].ToString()), this.GetConditionSearchDetailTab6(dtData.Rows[i]["SVENDORID"].ToString(), "1", dtData.Rows[i]["SCONTRACTID"].ToString()), ref Status, ref StatusDate, "REPORT_MONTHLY_1", RefString1, int.Parse(dtData.Rows[i]["SCONTRACTID"].ToString()));
                dtData.Rows[i]["REPORT_1"] = Status;
                dtData.Rows[i]["REPORT_1_DATE"] = StatusDate;

                Status = string.Empty;
                StatusDate = string.Empty;
                MonthlyReportBLL.Instance.ReportCheckSelectDetailTab6BLL(ddlYearTab6.SelectedValue, int.Parse(dtData.Rows[i]["MONTH_ID"].ToString()), this.GetConditionSearchDetailTab6(dtData.Rows[i]["SVENDORID"].ToString(), "2", dtData.Rows[i]["SCONTRACTID"].ToString()), ref Status, ref StatusDate, "REPORT_MONTHLY_3", RefString2, int.Parse(dtData.Rows[i]["SCONTRACTID"].ToString()));
                dtData.Rows[i]["REPORT_2"] = Status;
                dtData.Rows[i]["REPORT_2_DATE"] = StatusDate;

                Status = string.Empty;
                StatusDate = string.Empty;
                MonthlyReportBLL.Instance.ReportCheckSelectDetailTab6BLL(ddlYearTab6.SelectedValue, int.Parse(dtData.Rows[i]["MONTH_ID"].ToString()), this.GetConditionSearchDetailTab6(dtData.Rows[i]["SVENDORID"].ToString(), "3", dtData.Rows[i]["SCONTRACTID"].ToString()), ref Status, ref StatusDate, "REPORT_MONTHLY_6", RefString3, int.Parse(dtData.Rows[i]["SCONTRACTID"].ToString()));
                dtData.Rows[i]["REPORT_3"] = Status;
                dtData.Rows[i]["REPORT_3_DATE"] = StatusDate;
            }

            if (ddlStatusTab6.SelectedIndex > 0)
            {
                string strCheck = (string.Equals(ddlStatusTab6.SelectedValue, "1")) ? "O" : "X";
                string strProcess = (string.Equals(ddlStatusTab6.SelectedValue, "1")) ? "P" : "X";

                DataTable dt = new DataTable();
                dt = dtData.Clone();

                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if ((!string.Equals(dtData.Rows[i]["REPORT_1"].ToString(), string.Empty) && (string.Equals(dtData.Rows[i]["REPORT_1"].ToString().Substring(0, 1), strCheck) || string.Equals(dtData.Rows[i]["REPORT_1"].ToString().Substring(0, 1), strProcess)))
                        || (!string.Equals(dtData.Rows[i]["REPORT_2"].ToString(), string.Empty) && (string.Equals(dtData.Rows[i]["REPORT_2"].ToString().Substring(0, 1), strCheck) || string.Equals(dtData.Rows[i]["REPORT_2"].ToString().Substring(0, 1), strProcess)))
                        || (!string.Equals(dtData.Rows[i]["REPORT_3"].ToString(), string.Empty) && (string.Equals(dtData.Rows[i]["REPORT_3"].ToString().Substring(0, 1), strCheck) || string.Equals(dtData.Rows[i]["REPORT_3"].ToString().Substring(0, 1), strProcess))))
                        dt.Rows.Add(dtData.Rows[i]["MONTH_ID"].ToString(), dtData.Rows[i]["SEND_MONTH"].ToString(), dtData.Rows[i]["DATA_MONTH"].ToString(), dtData.Rows[i]["SVENDORID"].ToString(), dtData.Rows[i]["SABBREVIATION"].ToString(), dtData.Rows[i]["REPORT_1"].ToString(), dtData.Rows[i]["REPORT_1_DATE"].ToString(), dtData.Rows[i]["REPORT_2"].ToString(), dtData.Rows[i]["REPORT_2_DATE"].ToString(), dtData.Rows[i]["REPORT_3"].ToString(), dtData.Rows[i]["REPORT_3_DATE"].ToString());
                }
                GridViewHelper.BindGridView(ref dgvTab6, dt);
            }
            else
                GridViewHelper.BindGridView(ref dgvTab6, dtData);

            for (int i = 0; i < dgvTab6.Rows.Count; i++)
            {
                int[] cell = { 3, 5, 7 };
                for (int j = 0; j < cell.Length; j++)
                {
                    if (string.Equals(dgvTab6.Rows[i].Cells[cell[j]].Text, "O+"))
                        dgvTab6.Rows[i].Cells[cell[j]].ForeColor = Color.Green;
                    else if (string.Equals(dgvTab6.Rows[i].Cells[cell[j]].Text, "O-"))
                        dgvTab6.Rows[i].Cells[cell[j]].ForeColor = Color.Red;
                    else if (string.Equals(dgvTab6.Rows[i].Cells[cell[j]].Text, "P"))
                        dgvTab6.Rows[i].Cells[cell[j]].ForeColor = Color.Brown;
                    else if (string.Equals(dgvTab6.Rows[i].Cells[cell[j]].Text, "X"))
                        dgvTab6.Rows[i].Cells[cell[j]].ForeColor = Color.Blue;
                }

                //dgvTab6.Rows[i].Cells[3].ForeColor = (string.Equals(dgvTab6.Rows[i].Cells[3].Text, "O")) ? Color.Green : Color.Blue;
                //dgvTab6.Rows[i].Cells[5].ForeColor = (string.Equals(dgvTab6.Rows[i].Cells[5].Text, "O")) ? Color.Green : Color.Blue;
                //dgvTab6.Rows[i].Cells[7].ForeColor = (string.Equals(dgvTab6.Rows[i].Cells[7].Text, "O")) ? Color.Green : Color.Blue;
            }

            if (string.Equals(Session["CGROUP"].ToString(), ConfigValue.UserGroup1.ToString()))
                dgvTab6.Columns[2].Visible = false;
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private string GetConditionSearchTab6()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlMonthTab6.SelectedIndex > 0)
                sb.Append(" AND M_MONTH.MONTH_ID = '" + ddlMonthTab6.SelectedValue + "'");

            if (ddlVendorTab6.SelectedIndex > 0)
                sb.Append(" AND VENDOR_ID = '" + ddlVendorTab6.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private string GetConditionSearchDetailTab6(string VendorID, string ReportID, string SCONTRACTID)
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" AND VENDOR_ID = '" + VendorID + "'");
            sb.Append(" AND REPORT_ID = '" + ReportID + "'");
            sb.Append(" AND SCONTRACTID = '" + SCONTRACTID + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void ValidateSelectTab6()
    {
        try
        {
            if (ddlYearTab6.SelectedIndex == 0)
                throw new Exception("กรุณาเลือก ปี (ค.ศ.)");

            if ((ddlMonthTab6.SelectedIndex == 0) && (ddlVendorTab6.Enabled) && (ddlVendorTab6.SelectedIndex == 0))
                throw new Exception("กรุณาเลือก เดือน หรือ ผู้ขนส่ง");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void SendEmail(int TemplateID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailReportMonthlyNotPass)
                {
                    string MonthName1 = string.Empty;
                    string MonthName2 = string.Empty;
                    string MonthName3 = string.Empty;

                    DataTable dtCusScore = MonthlyReportBLL.Instance.CusScoreSelectBLL(ddlYear.SelectedValue, int.Parse(ddlMonth.SelectedValue), ddlVendor.SelectedValue, ddlContract.SelectedValue);
                    for (int i = 0; i < dtCusScore.Rows.Count; i++)
                    {
                        if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly1.ToString()))
                            MonthName1 = dtCusScore.Rows[i]["REPORT_MONTH"].ToString();
                        else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly2.ToString()))
                            MonthName2 = dtCusScore.Rows[i]["REPORT_MONTH"].ToString();
                        else if (string.Equals(dtCusScore.Rows[i]["REPORT_ID"].ToString(), ConfigValue.Monthly3.ToString()))
                            MonthName3 = dtCusScore.Rows[i]["REPORT_MONTH"].ToString();
                    }

                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(string.Empty, ddlVendor.SelectedValue);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);

                    if ((rowReport1_1.Visible) && (string.Equals(radReport1Score.SelectedValue, "0")) && (radReport1Score.Enabled))
                    {
                        Body = Body.Replace("{Vendor}", ddlVendor.SelectedItem.ToString());
                        Body = Body.Replace("{Month}", MonthName1);
                        Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                        Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());

                        MailService.SendMail(EmailList, Subject, Body, "", "ReportWarning", ColumnEmailName);
                    }
                    if ((rowReport2_1.Visible) && (string.Equals(radReport2Score.SelectedValue, "0")) && (radReport2Score.Enabled))
                    {
                        Body = Body.Replace("{Vendor}", ddlVendor.SelectedItem.ToString());
                        Body = Body.Replace("{Month}", MonthName2);
                        Body = Body.Replace("{Remark}", txtReport2Remark.Value.Trim());
                        Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());

                        MailService.SendMail(EmailList, Subject, Body, "", "ReportWarning", ColumnEmailName);
                    }
                    if ((rowReport3_1.Visible) && (string.Equals(radReport3Score.SelectedValue, "0")) && (radReport3Score.Enabled))
                    {
                        Body = Body.Replace("{Vendor}", ddlVendor.SelectedItem.ToString());
                        Body = Body.Replace("{Month}", MonthName3);
                        Body = Body.Replace("{Remark}", txtReport3Remark.Value.Trim());
                        Body = Body.Replace("{CONTRACT}", ddlContract.SelectedItem.ToString());

                        MailService.SendMail(EmailList, Subject, Body, "", "ReportWarning", ColumnEmailName);
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            this.ShowRowReport(false);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
}