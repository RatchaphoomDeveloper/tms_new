﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using System.Linq;
using System.Drawing;
using TMS_BLL.Master;
using System.Web.UI.HtmlControls;

public partial class ProductTagHead : PageBase
{
    DataSet ds;
    #region + View State +
    private DataTable dtData
    {
        get
        {
            if ((DataTable)ViewState["dtDataTab1"] != null)
                return (DataTable)ViewState["dtDataTab1"];
            else
                return null;
        }
        set
        {
            ViewState["dtDataTab1"] = value;
        }
    }

    private DataTable dt
    {
        get
        {
            if ((DataTable)ViewState["dt"] != null)
                return (DataTable)ViewState["dt"];
            else
                return null;
        }
        set
        {
            ViewState["dt"] = value;
        }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Culture = "en-US";
        this.UICulture = "en-US";

        if (!IsPostBack)
        {
            this.InitialForm();
            //this.AssignAuthen();
        }
    }

    //private void AssignAuthen()
    //{
    //    try
    //    {
    //        if (!CanRead)
    //        {

    //        }
    //        if (!CanWrite)
    //        {

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    private void InitialForm()
    {
        try
        {
            this.LoadProductTagHead();
            this.LoadStatus();
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private void LoadStatus()
    {
        try
        {
            DataTable dtStatus = UserBLL.Instance.StatusSelectBLL(" AND IS_ACTIVE = 1 AND STATUS_TYPE = 'DEPARTMENT_STATUS'");
            DropDownListHelper.BindDropDownList(ref ddlStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void LoadProductTagHead()
    {
        try
        {
            DataTable dtProductTagHead = ProductTagBLL.Instance.ProductTagHeadSelectBLL(" AND CACTIVE = 1");
            DropDownListHelper.BindDropDownList(ref ddlProductTagHead, dtProductTagHead, "KM_TAG_LVL1_ID", "KM_TAG_LVL1_NAME", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Search(GridView dgv, DataTable dtData, string Condition)
    {
        try
        {
            dtData = ProductTagBLL.Instance.ProductTagHeadSelectBLL(Condition);
            if (dtData.Rows.Count > 0)
                GridViewHelper.BindGridView(ref dgv, dtData);
            else
                GridViewHelper.BindGridView(ref dgv, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    private string GetConditionSearch()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlProductTagHead.SelectedIndex > 0)
                sb.Append(" AND KM_TAG_LVL1_ID = '" + ddlProductTagHead.SelectedValue + "'");

            if (ddlStatus.SelectedIndex > 0)
                sb.Append(" AND CACTIVE = '" + ddlStatus.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            dgvData.PageIndex = e.NewPageIndex;
            GridViewHelper.BindGridView(ref dgvData, dtData);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void dgvData_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (string.Equals(e.CommandName, "view") || string.Equals(e.CommandName, "editData"))
        {
            int Index = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
            string ID = dgvData.DataKeys[Index].Value.ToString();
            ID = MachineKey.Encode(Encoding.UTF8.GetBytes(ID), MachineKeyProtection.All);

            switch (e.CommandName)
            {
                case "view": Response.Redirect("ProductTagHeadAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.View.ToString()), MachineKeyProtection.All) + "&id=" + ID); break;
                case "editData": Response.Redirect("ProductTagHeadAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Edit.ToString()), MachineKeyProtection.All) + "&id=" + ID); break;

                default:
                    break;
            }
        }
    }
    protected void cmdAdd_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("ProductTagHeadAddEdit.aspx?type=" + MachineKey.Encode(Encoding.UTF8.GetBytes(Mode.Add.ToString()), MachineKeyProtection.All) + "&id=" + MachineKey.Encode(Encoding.UTF8.GetBytes("0"), MachineKeyProtection.All));
    }
    protected void cmdClear_Click(object sender, EventArgs e)
    {
        try
        {
            //dtDataTab1 = UserBLL.Instance.UserSelectBLL(string.Empty);
            ddlProductTagHead.ClearSelection();
            ddlStatus.ClearSelection();
            GridViewHelper.BindGridView(ref dgvData, null);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        this.Search(dgvData, dtData, this.GetConditionSearch());
    }
}