﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TMS_BLL.Master;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TMS_BLL.Transaction.Complain;
using System.Text;
using EmailHelper;
using TMS_BLL.Transaction.Report;
using TMS_BLL.Transaction.SurpriseCheck;

public partial class Vehicle : PageBase
{
    #region ViewState    
    public string VendorId
    {
        get
        {
            if (ViewState["VendorId"] != null)
                return (string)ViewState["VendorId"];
            else
                return "";
        }
        set
        {
            ViewState["VendorId"] = value;
        }
    }
    public string CGROUP
    {
        get
        {
            if (ViewState["CGROUP"] != null)
                return (string)ViewState["CGROUP"];
            else
                return "";
        }
        set
        {
            ViewState["CGROUP"] = value;
        }
    }
    #endregion
    private string registerno = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Culture = "en-US";
        this.UICulture = "en-US";
        //Get User expire
        if (Session["UserID"] == null || Session["UserID"] + "" == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "ssUserIDExpire", "<script>window.location='default.aspx';<script>"); return;

        }
        else
        {
            VendorId = Session["SVDID"].ToString();
            CGROUP = Session["CGROUP"].ToString();
        }


        if (!IsPostBack)
        {
            this.initalFrom(CGROUP);
            this.AssignAuthen();
            InitialStatus(radSpot, " AND STATUS_TYPE = 'SPOT_STATUS'");
            DataTable dt = VehicleBLL.Instance.ClassGroupSelectBLL(string.Empty);
            DropDownListHelper.BindDropDownList(ref ddlClassGroup, dt, "CLASS_GROUP_CODE", "CLASS_GROUP_CODE", true);
        }
    }
    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
            }
            if (!CanWrite)
            {
                btnSave.Enabled = false;

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #region EVENT     
    protected void ChoseTypeCar(object sender, EventArgs e)
    {

        if (radSpot.SelectedIndex == 0)
        {
            ddlClassGroup.SelectedIndex = 0;
            ddlClassGroup.Enabled = false;
        }
        else
        {
            if (string.Equals(CGROUP, "1"))
                ddlClassGroup.Enabled = true;
        }

        if (!string.Equals(CGROUP, "1"))
        {
            VendorId = VendorId;
            DataTable LoadDataContrat = new DataTable();
            DataTable dt_vehicle_car = VehicleBLL.Instance.LoadDataVehicleCar();
            DataTable dt_vehicle_car1 = new DataTable();
            if (dt_vehicle_car1.Rows.Count == 0)
                dt_vehicle_car1 = dt_vehicle_car;
            switch (Rdo_TypeCar.SelectedIndex)
            {
                case 0:
                    LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, VendorId, radSpot.SelectedValue);
                    DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(VendorId, radSpot.SelectedValue);
                    DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", true);
                    DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);
                    DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                    VEH_TEXT.Text = string.Empty;
                    divtruck.Visible = true;
                    divSemi.Visible = false;
                    break;
                case 1:
                    LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, VendorId, radSpot.SelectedValue);
                    DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(VendorId, radSpot.SelectedValue);
                    DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(VendorId, radSpot.SelectedValue);
                    DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", true);
                    DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", true);
                    DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);
                    DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car1, "config_value", "config_name", true);
                    semi_veh_text.Text = string.Empty;
                    divSemi.Visible = true;
                    divtruck.Visible = false;
                    break;
                default:
                    divtruck.Visible = false;
                    divSemi.Visible = false;
                    break;
            }
        }
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case 1:
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
    }

    protected void ddlTruckSelectedValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlVendorTruck.SelectedValue);

    }

    protected void ddlSemiSelectValue(object sender, EventArgs e)
    {
        this.initalTruck(ddlvendorSemiTruck.SelectedValue);
    }

    protected void initalTruck(string value)
    {
        DataTable LoadDataContrat = VehicleBLL.Instance.LoadDataContrat(CGROUP, value, radSpot.SelectedValue);
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                DataTable dt_truck = VehicleBLL.Instance.LoadDataTruck(value, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddl_truck, dt_truck, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", true);
                divtruck.Visible = true;
                divSemi.Visible = false;
                break;
            case 1:
                DataTable dt_headcar = VehicleBLL.Instance.LoadDataHeadCar(value, radSpot.SelectedValue);
                DataTable dt_semi = VehicleBLL.Instance.LoadDataSemi(value, radSpot.SelectedValue);
                DropDownListHelper.BindDropDownList(ref truck_head, dt_headcar, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref Truck_Semi, dt_semi, "STRUCKID", "SHEADREGISTERNO", true);
                DropDownListHelper.BindDropDownList(ref TruckSemi_contract, LoadDataContrat, "SCONTRACTID", "SCONTRACTNO", false);
                divSemi.Visible = true;
                divtruck.Visible = false;
                break;
            default:
                divtruck.Visible = false;
                divSemi.Visible = false;
                break;
        }
    }
    protected void TruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(ddl_truck.SelectedValue);
        int totalcapacity = 0;
        string veh_text = "";
        int ncapacity = 0;
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            if (string.Equals(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString(), string.Empty))
            {
                ncapacity = 0;
            }
            else
            {
                ncapacity = (int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString()) / 1000);
            }
            totalcapacity += int.Parse(string.IsNullOrEmpty(LoadGetVeh_Text.Rows[i]["NCAPACITY"] + string.Empty) ? "0" : LoadGetVeh_Text.Rows[i]["NCAPACITY"] + string.Empty);
            veh_text += ncapacity + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        VEH_TEXT.Text = "SU" + totalcapacity + "(" + veh_text + ")";
    }
    #region คำนวณ VEH_TEXT Semi


    protected void SemitTruckVeh_Text(object sender, EventArgs e)
    {
        DataTable LoadGetVeh_Text = VehicleBLL.Instance.LoadGetVeh_Text(Truck_Semi.SelectedValue);
        int totalcapacity = 0;
        string veh_text = "";
        int ncapacity = 0;
        for (int i = 0; i < LoadGetVeh_Text.Rows.Count; i++)
        {
            if (string.Equals(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString(), string.Empty))
            {
                ncapacity = 0;
            }
            else
            {
                ncapacity = (int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString()) / 1000);
            }
            totalcapacity += int.Parse(LoadGetVeh_Text.Rows[i]["NCAPACITY"].ToString());
            veh_text += ncapacity + ",";
        }
        veh_text = veh_text.TrimEnd(',');
        semi_veh_text.Text = "SM" + totalcapacity + "(" + veh_text + ")";
    }
    #endregion
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        #region บันทึกข้อมูลลงSap
        string ClassGroup = string.Empty;

        if (string.Equals(CGROUP, "1") && ddlClassGroup.Enabled && ddlClassGroup.SelectedIndex < 1)
        {//ปตท.
            throw new Exception("กรุณาเลือก Class Group");
        }

        if (string.Equals(CGROUP, "1"))
            ClassGroup = ddlClassGroup.SelectedItem.Text;


        try
        {
            //บันทึกข้อมูลลงTMS
            int ContractID = 0;
            string ContractName = string.Empty;
            if (Rdo_TypeCar.SelectedIndex == 0)
            {
                if (Truck_contract.SelectedIndex < 0)
                    throw new Exception("กรุณาเลือกสัญญาก่อน");

                ContractID = int.Parse(Truck_contract.SelectedValue);
                ContractName = Truck_contract.SelectedItem.Text;
            }
            else
            {
                if (TruckSemi_contract.SelectedIndex < 0)
                    throw new Exception("กรุณาเลือกสัญญาก่อน");

                ContractID = int.Parse(TruckSemi_contract.SelectedValue);
                ContractName = TruckSemi_contract.SelectedItem.Text;
            }
            //#region ตรวจสอบการตรวจสภาพรถก่อนเข้าสัญญา
            //if (radSpot.SelectedValue != "1")//ต้องไม่ใช่สัญญาจ้างพิเศษ
            //{
            //    string truckname = string.Empty;
            //    if (Rdo_TypeCar.SelectedIndex == 0)
            //    {
            //        truckname = ddl_truck.SelectedItem.Text.Trim();
            //    }
            //    else
            //    {
            //        truckname = truck_head.SelectedItem.Text.Trim();
            //    }
            //    if (!string.IsNullOrEmpty(truckname))
            //    {
            //        DataTable dt = SurpriseCheckContractBLL.Instance.SurpriseCheckContractSelect(truckname
            //   , ""
            //   , ""
            //   , ""
            //   , ""
            //   , "");
            //        if (dt.Rows.Count > 0)
            //        {
            //            if (dt.Select("IS_ACTIVE = 1").Any() || dt.Select("IS_ACTIVE = 3").Any())
            //            {
            //                throw new Exception("รถทะเบียน " + truckname + " ยังทำการตรวจสภาพรถก่อนเข้าสัญญาไม่ผ่านรบกวนทำการตรวจสภาพรถก่อนเข้าสัญญา");
            //            }
            //        }
            //        else
            //        {
            //            throw new Exception("รถทะเบียน " + truckname + " ยังไม่ได้รับการตรจสภาพรถก่อนเข้าสัญญารบกวนทำการตรวจสภาพรถก่อนเข้าสัญญา");
            //        }
            //    }
            //}


            //#endregion


            DataTable valuereturn = VehicleBLL.Instance.SaveVehicle(Rdo_TypeCar.SelectedIndex.ToString(), DataToDatatable(), radSpot.SelectedValue, ClassGroup, ContractID);
            if (string.Equals(CGROUP, "1"))
            {
                if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        this.GetTypeTruckSave(Rdo_TypeCar.SelectedIndex.ToString());
                        SystemFunction.AddToTREQ_DATACHANGE(valuereturn.Rows[0][1].ToString(), "H", Session["UserID"] + "", "5", string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)", valuereturn.Rows[0][2].ToString(), "Y");
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ", "truck_info.aspx");
                    }
                    catch (Exception)
                    {

                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ", "truck_info.aspx");
                    }

                    //บันทึกข้อมูลสำเร็จ
                    //Auto Approve case PTT
                    if (radSpot.SelectedValue == "1")           //SPOT
                    {
                        this.AutoApprove(valuereturn.Rows[0][1].ToString());
                        LOG.Instance.SaveLog("นำรถเข้าสัญญา", valuereturn.Rows[0][1].ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + ContractName + "</span>", "");
                    }
                }
                else
                {
                    alertFail(valuereturn.Rows[0][0].ToString());
                }
            }
            else
            {
                if (string.Equals(valuereturn.Rows[0][0].ToString(), "01"))
                {
                    try
                    {
                        this.GetTypeTruckSave(Rdo_TypeCar.SelectedIndex.ToString());
                        this.registerno = string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)";
                        this.SendEmail(ConfigValue.EmailVehicleAdd, valuereturn.Rows[0][1].ToString());
                        SystemFunction.AddToTREQ_DATACHANGE(valuereturn.Rows[0][1].ToString(), "H", Session["UserID"] + "", "0", string.Equals(Rdo_TypeCar.SelectedValue.ToString(), "0") ? ddl_truck.SelectedItem.Text + " (รถเดี่ยว)" : truck_head.SelectedItem.Text + '/' + Truck_Semi.SelectedItem.Text + " (รถกึ่งพวง)", valuereturn.Rows[0][2].ToString(), "Y", "");
                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง สำเร็จ <br /> รอการอนุมัติ", "Vendor_Detail.aspx");
                    }
                    catch (Exception)
                    {

                        alertSuccess("แก้ไขข้อมูลรถใน TMS สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ <br /> รอการอนุมัติ", "Vendor_Detail.aspx");
                    }

                }
                else
                {
                    alertFail(valuereturn.Rows[0][0].ToString());
                }
            }
        }
        catch (Exception ex)
        {
            alertFail("แก้ไขข้อมูลรถใน TMS ไม่สำเร็จ <br /> ส่ง email ถึงผู้ที่เกี่ยวข้อง ไม่สำเร็จ <br/>" + ex.Message);
        }


        #endregion


    }

    private void AutoApprove(string STTRUCKID)
    {
        try
        {
            string URL = "Vehicle_approve.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedIndex.ToString());
            Session["AutoApprove"] = true;
            Response.Redirect(URL);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void GetTypeTruckSave(string p)
    {
        switch (p)
        {
            case "0":
                #region บันทึก Log
                LOG.Instance.SaveLog("ผขส. ขอจับคู่ข้อมูล TC หมายเลขทะเบียนรถ", ddl_truck.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + ddl_truck.SelectedItem.Text + string.Empty + "</span>", "");
                #endregion
                break;
            default:
                #region บันทึก Logหัว
                LOG.Instance.SaveLog("ผขส. ขอจับคู่ข้อมูล TC หมายเลขทะเบียนรถ", truck_head.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก) <br/>" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก)</span>", "");
                #endregion
                #region บันทึก Logหาง
                LOG.Instance.SaveLog("ผขส. ขอจับคู่ข้อมูล TC หมายเลขทะเบียนรถ", Truck_Semi.SelectedItem.Text.ToString(), Session["UserID"].ToString(), "<span class=\"green\">" + Truck_Semi.SelectedItem.Text.ToString() + " (หางลาก) <br/>" + truck_head.SelectedItem.Text.ToString() + " (หัวลาก)</span>", "");
                #endregion
                break;
        }
    }

    #endregion
    #region InitalData
    private void initalFrom(string CGROUP)
    {
        DataTable dt_vehicle_car = VehicleBLL.Instance.LoadDataVehicleCar();
        DataTable dt_vehicle_car1 = new DataTable();
        if (dt_vehicle_car1.Rows.Count == 0)
            dt_vehicle_car1 = dt_vehicle_car;
        string condition = VendorId;
        DataTable dt_vendor;
        ddlVendorTruck.Items.Clear();
        ddlvendorSemiTruck.Items.Clear();
        rdo_contratType.Items.Clear();
        rdo_contratType1.Items.Clear();
        switch (CGROUP)
        {

            case "1":
                dt_vendor = VendorBLL.Instance.SelectName(string.Empty);
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", true);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType, dt_vehicle_car, "config_value", "config_name", true);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car1, "config_value", "config_name", false);

                break;
            default:
                dt_vendor = VendorBLL.Instance.SelectName(condition);
                DropDownListHelper.BindDropDownList(ref ddlVendorTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref ddlvendorSemiTruck, dt_vendor, "SVENDORID", "SABBREVIATION", false);
                DropDownListHelper.BindDropDownList(ref rdo_contratType1, dt_vehicle_car1, "config_value", "config_name", true);
                break;
        }

    }
    #endregion
    #region Micellouse
    private Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn = FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    private string GetConditionVendor()
    {
        try
        {
            StringBuilder sb = new StringBuilder();

            if (ddlVendorTruck.SelectedIndex > 0)
                sb.Append(" AND TCONTRACT.SVENDORID = '" + Truck_contract.SelectedValue + "'");

            return sb.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #region บันทึกข้อมูลลง
    protected DataTable DataToDatatable()
    {
        DataTable dt;
        switch (Rdo_TypeCar.SelectedIndex)
        {
            case 0:
                dt = new DataTable();
                dt.Columns.Add("typecar");
                dt.Columns.Add("transportid");
                dt.Columns.Add("contrattype");
                dt.Columns.Add("truck");
                dt.Columns.Add("contratid");
                dt.Columns.Add("veh_text");
                dt.Rows.Add(Rdo_TypeCar.SelectedIndex, ddlVendorTruck.SelectedValue, rdo_contratType.SelectedValue, ddl_truck.SelectedItem.Text.Trim(), Truck_contract.SelectedValue, VEH_TEXT.Text.Trim()
                );

                break;
            default:
                dt = new DataTable();
                dt.Columns.Add("typecar");
                dt.Columns.Add("transportid");
                dt.Columns.Add("contrattype");
                dt.Columns.Add("truck");
                dt.Columns.Add("semitruck");
                dt.Columns.Add("contratid");
                dt.Columns.Add("Semiveh_text");
                dt.Rows.Add(Rdo_TypeCar.SelectedIndex, ddlvendorSemiTruck.SelectedValue, rdo_contratType1.SelectedValue, truck_head.SelectedItem.Text.Trim(), Truck_Semi.SelectedItem.Text.Trim(), TruckSemi_contract.SelectedValue, semi_veh_text.Text.Trim());
                break;
        }
        return dt;

    }
    #endregion
    #region SentMail
    private void SendEmail(int TemplateID, string STTRUCKID)
    {
        try
        {
            DataSet dsSendEmail = new DataSet();
            string EmailList = string.Empty;

            DataTable dtTemplate = EmailTemplateBLL.Instance.EmailTemplateSelectBLL(TemplateID);
            DataTable dtEmail = new DataTable();
            if (dtTemplate.Rows.Count > 0)
            {
                string Subject = dtTemplate.Rows[0]["SUBJECT"].ToString();
                Subject = Subject.Replace("{regisNo}", registerno);
                string Body = dtTemplate.Rows[0]["BODY"].ToString();

                if (TemplateID == ConfigValue.EmailVehicleAdd)
                {
                    string MonthName1 = string.Empty;
                    string MonthName2 = string.Empty;
                    string MonthName3 = string.Empty;
                    string Link = Request.Url.GetLeftPart(UriPartial.Authority).ToString() + Request.ApplicationPath + "/" + "Vehicle_approve.aspx?str=" + ConfigValue.GetEncodeText("str") + "&STTRUCKID=" + ConfigValue.GetEncodeText(STTRUCKID) + "&CarTypeID=" + ConfigValue.GetEncodeText(Rdo_TypeCar.SelectedIndex.ToString());
                    dsSendEmail = MonthlyReportBLL.Instance.GetEmailReportMonthlyNotPassBLL(ConfigValue.DeliveryDepartmentCode, string.Equals(CGROUP, "1") ? ddlVendorTruck.SelectedValue.ToString() : VendorId);
                    EmailList = ConvertDataSetToList.Convert(dsSendEmail);
                    Body = Body.Replace("{contract}", Session["vendoraccountname"].ToString());
                    Body = Body.Replace("{regisNo}", registerno);
                    Body = Body.Replace("{Comment}", "");
                    Body = Body.Replace("{link}", ConfigValue.GetClickHere(Link));
                    //Body = Body.Replace("{Remark}", txtReport1Remark.Value.Trim());
                    MailService.SendMail(ComplainBLL.Instance.GetEmailComplainBLL(string.Empty, string.Empty, string.Empty, 0, VendorId, true, false), Subject, Body, "", "VendorEmployeeAdd", ColumnEmailName);

                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    #endregion
    #region ResponseMenu
    protected void ResponseMenu(object sender, EventArgs e)
    {
        if (string.Equals(CGROUP.ToString(), "1"))
        {
            Response.Redirect("approve_pk.aspx");
        }
        else
        {
            Response.Redirect("vendor_request.aspx");
        }
    }
    #endregion

    protected void Rdo_TypeCar_SelectedIndexChanged(object sender, EventArgs e)
    {
        rowSpot.Visible = true;
        rowClassGroup.Visible = true;

        if (radSpot.SelectedIndex > -1)
            ChoseTypeCar(null, null);
    }
}