﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Configuration;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Globalization;
using System.Data.OracleClient;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class vendor_view : System.Web.UI.Page
{
    string conn = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    static List<ListGrid> listGrid = new List<ListGrid>();
    static List<ListGridother> listGridother = new List<ListGridother>();
    static List<ListGridDoc> listGriddoc = new List<ListGridDoc>();
    static List<ListGridsigh> listGridsigh = new List<ListGridsigh>();
    static List<DeleteItem> DeleteGrid = new List<DeleteItem>();
    static List<DeleteFilegvwsigh> Filegvwsigh = new List<DeleteFilegvwsigh>();
    static List<Deletegvwsigh> DeleteGridsigh = new List<Deletegvwsigh>();
    static List<gvwsighListColumn> Listindex = new List<gvwsighListColumn>();
    //อับโลหดไฟล์ไปที่Te,p
    const string TempDirectory = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory2 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory3 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory4 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    const string TempDirectory5 = "UploadFile/VendorDoc/Temp/{0}/{2}/{1}/";
    //อับโลหดไฟล์ไปที่Saver
    const string SaverDirectory = "UploadFile/VendorDoc/Lettercarrier/{0}/{2}/{1}/";
    const string SaverDirectory2 = "UploadFile/VendorDoc/Transportlicense/{0}/{2}/{1}/";
    const string SaverDirectory3 = "UploadFile/VendorDoc/TaxDocumentssection12/{0}/{2}/{1}/";
    const string SaverDirectory4 = "UploadFile/VendorDoc/Counterfoil/{0}/{2}/{1}/";
    const string SaverDirectory5 = "UploadFile/VendorDoc/Signature/{0}/{2}/{1}/";
    const string SaverDirectory6 = "UploadFile/VendorDoc/Other/{0}/{2}/{1}/";
    //อับโลหดไฟล์History
    const string HistoryDirectory = "UploadFile/VendorDoc/History/Lettercarrier/{0}/{2}/{1}/";
    const string HistoryDirectory2 = "UploadFile/VendorDoc/History/Transportlicense/{0}/{2}/{1}/";
    const string HistoryDirectory3 = "UploadFile/VendorDoc/History/TaxDocumentssection12/{0}/{2}/{1}/";
    const string HistoryDirectory4 = "UploadFile/VendorDoc/History/Counterfoil/{0}/{2}/{1}/";
    const string HistoryDirectory5 = "UploadFile/VendorDoc/History/Signature/{0}/{2}/{1}/";
    const string HistoryDirectory6 = "UploadFile/VendorDoc/History/Other/{0}/{2}/{1}/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Session["UserID"] + ""))
        {
            //CommonFunction.SetPopupOnLoad(dxPopupInfo, "dxWarning('" + Resources.CommonResource.Msg_HeadInfo + "','กรุณาทำการ เข้าใช้งานระบบ อีกครั้ง');");
            //  Response.Redirect("default.aspx");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
        }
        if (!IsPostBack)
        {
            
            string str = Request.QueryString["str"];
            string[] strQuery;
            if (!string.IsNullOrEmpty(str))
            {
                strQuery = STCrypt.DecryptURL(str);
                ViewState["setdata"] = strQuery[0];
               
            }
            else
            {
                ViewState["setdata"] = Session["Backpage"] + "";
                Session.Remove("Backpage");
            }


            #region เช็ค Permission
            // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ssUserIDExpireInMP", "window.location='default.aspx';", true); return;
            LogUser("46", "R", "เปิดดูข้อมูลหน้า เรียกดูข้อมูลผู้ขนส่ง", ViewState["setdata"]+"");

            #endregion


            Setdata();
            Listgvwsign();
            ListgvwDoc();
            VisibleControlUpload();
            listGrid.RemoveAll(o => 1 == 1);
            Setdata();

        }
        Listgvwdoc4();
    }

    protected void xcpn_Load(object sender, EventArgs e)
    {
        //Setdata();
      
    }

    protected void xcpn_Callback(object sender, DevExpress.Web.ASPxClasses.CallbackEventArgsBase e)
    {

        string[] param = e.Parameter.Split(';');
        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        //string Line_no = "";
        string SDOCID = ViewState["SDOCID"] + "";
        string SDOCTYPE = ViewState["SDOCTYPE"] + "";
        string SDOCID2 = ViewState["SDOCID2"] + "";
        string SDOCTYPE2 = ViewState["SDOCTYPE2"] + "";
        string SDOCID3 = ViewState["SDOCID3"] + "";
        string SDOCTYPE3 = ViewState["SDOCTYPE3"] + "";
        string SDOCID4 = ViewState["SDOCID4"] + "";
        string SDOCTYPE4 = ViewState["SDOCTYPE4"] + "";



        switch (param[0])
        {



            case "viewHistory":
                Session["Backpage"] = VendorID;
                xcpn.JSProperties["cpRedirectTo"] = "document_history.aspx?str=" + Server.UrlEncode(STCrypt.Encrypt("" + VendorID));
                break;

            case "Save":

                using (OracleConnection con = new OracleConnection(conn))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    else
                    {

                    }
                    #region Saveข้อมูลบริษัท

                    string condition = "";
                    if (!string.IsNullOrEmpty(txtSABBREVIATION.Text))
                    {
                        condition += " AND SABBREVIATION = '" + CommonFunction.ReplaceInjection(txtSABBREVIATION.Text) + "'";
                    }
                    if (!string.IsNullOrEmpty(txtSTEL.Text))
                    {
                        condition += " AND STEL = '" + CommonFunction.ReplaceInjection(txtSTEL.Text) + "'";
                    }
                    if (!string.IsNullOrEmpty(txtSFAX.Text))
                    {
                        condition += " AND SFAX = '" + CommonFunction.ReplaceInjection(txtSFAX.Text) + "'";
                    }
                    if (!string.IsNullOrEmpty(dedtDSTARTPTT.Text))
                    {
                        condition += " AND TRUNC(DSTARTPTT) =   TO_DATE('" + CommonFunction.ReplaceInjection(dedtDSTARTPTT.Date.Day + "/" + dedtDSTARTPTT.Date.Month + "/" + dedtDSTARTPTT.Date.Year) + @"','dd/mm/yyyy')";
                    }
                    if (!string.IsNullOrEmpty(dedtDBEGINTRANSPORT.Text))
                    {
                        condition += " AND TRUNC(DBEGINTRANSPORT) =   TO_DATE('" + CommonFunction.ReplaceInjection(dedtDBEGINTRANSPORT.Date.Day + "/" + dedtDBEGINTRANSPORT.Date.Month + "/" + dedtDBEGINTRANSPORT.Date.Year) + @" ','dd/mm/yyyy')";
                    }
                    if (!string.IsNullOrEmpty(dedtDEXPIRETRANSPORT.Text))
                    {
                        condition += " AND TRUNC(DEXPIRETRANSPORT) =  TO_DATE('" + CommonFunction.ReplaceInjection((dedtDEXPIRETRANSPORT.Date.Day + "/" + dedtDEXPIRETRANSPORT.Date.Month + "/" + dedtDEXPIRETRANSPORT.Date.Year)) + @"','dd/mm/yyyy')";
                    }
                    if (!string.IsNullOrEmpty(dedtDBEGIN13BIS.Text))
                    {
                        condition += " AND TRUNC(DBEGIN13BIS) =   TO_DATE('" + CommonFunction.ReplaceInjection((dedtDBEGIN13BIS.Date.Day + "/" + dedtDBEGIN13BIS.Date.Month + "/" + dedtDBEGIN13BIS.Date.Year)) + @" ','dd/mm/yyyy')";
                    }
                    if (!string.IsNullOrEmpty(dedtDEXPIRE13BIS.Text))
                    {
                        condition += " AND TRUNC(DEXPIRE13BIS) =  TO_DATE('" + CommonFunction.ReplaceInjection((dedtDEXPIRE13BIS.Date.Day + "/" + dedtDEXPIRE13BIS.Date.Month + "/" + dedtDEXPIRE13BIS.Date.Year)) + @"','dd/mm/yyyy')";
                    }

                    if (!string.IsNullOrEmpty(txtNCAPITAL.Text))
                    {
                        condition += " AND nCapital =  " + CommonFunction.ReplaceInjection(txtNCAPITAL.Text) + "";
                    }
                    else
                    {
                        condition += " AND nCapital = 0";
                    }
                    if (!string.IsNullOrEmpty(txtDescriptionSigh.Text))
                    {
                        condition += " AND DESCRIPTION ='" + CommonFunction.ReplaceInjection(txtDescriptionSigh.Text) + "'";
                    }


                    string strCheckdata = @"SELECT SVENDORID, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS,DESCRIPTION
                                            FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                           " + condition + "";




                    string strCompany = @"UPDATE TVendor
                                         SET dStartPTT=:dStartPTT
                                            ,nCapital=:nCapital
                                            ,dBeginTransport=:dBeginTransport
                                            ,dExpireTransport=:dExpireTransport
                                            ,dBegin13bis=:dBegin13bis
                                            ,dExpire13bis=:dExpire13bis
                                            ,DESCRIPTION=:DESCRIPTION
                                            ,SUPDATE=:SUPDATE
                                            ,DUPDATE=:DUPDATE
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'";

                    DataTable dt = CommonFunction.Get_Data(con, strCheckdata);
                    if (dt.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        AddhistoryVendor(VendorID);

                        decimal num = 0;
                        DateTime? Date;
                        using (OracleCommand com1 = new OracleCommand(strCompany, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":dStartPTT", OracleType.DateTime).Value = dedtDSTARTPTT.Value != null ? dedtDSTARTPTT.Value : DBNull.Value;
                            com1.Parameters.Add(":nCapital", OracleType.Number).Value = decimal.TryParse(txtNCAPITAL.Text, out num) ? num : 0;
                            com1.Parameters.Add(":dBeginTransport", OracleType.DateTime).Value = dedtDBEGINTRANSPORT.Value != null ? dedtDBEGINTRANSPORT.Value : DBNull.Value;
                            com1.Parameters.Add(":dExpireTransport", OracleType.DateTime).Value = dedtDEXPIRETRANSPORT.Value != null ? dedtDEXPIRETRANSPORT.Value : DBNull.Value;
                            com1.Parameters.Add(":dBegin13bis", OracleType.DateTime).Value = dedtDBEGIN13BIS.Value != null ? dedtDBEGIN13BIS.Value : DBNull.Value;
                            com1.Parameters.Add(":dExpire13bis", OracleType.DateTime).Value = dedtDEXPIRE13BIS.Value != null ? dedtDEXPIRE13BIS.Value : DBNull.Value;
                            com1.Parameters.Add(":DESCRIPTION", OracleType.VarChar).Value = txtDescriptionSigh.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DUPDATE", OracleType.DateTime).Value = DateTime.Now;
                            com1.ExecuteNonQuery();
                        }
                    }



                    #endregion

                    #region saveUpload


                    string InsUpload = @"INSERT INTO TVENDOR_DOC(SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE ,SFILENAME
                                                           , SDESCRIPTION, CACTIVE ,DCREATE ,SCREATE , DEXPIRE,SPATH ,SSYSFILENAME) 
                                                           VALUES (:SVENDORID,FC_GENID_TVENDOR_DOC(), :SDOCVERSION , :SDOCTYPE ,:SFILENAME
                                                           ,  :SDESCRIPTION, :CACTIVE, sysdate,:SCREATE ,:DEXPIRE,:SPATH,:SSYSFILENAME)";

                    string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "'";

                    #region Upload1
                    string CheckUpload1 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename.Text) + "'";


                    if (!string.IsNullOrEmpty(txtFileName.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload1);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate.Text))
                            {

                                AddhistoryVendorDoc(VendorID, SDOCID, SDOCTYPE);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;

                                    com1.ExecuteNonQuery();

                                }

                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up1.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "1";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "หนังสือรับรองผู้ขนส่ง";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename.Text;

                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory, Session["SVDID"] + "", "uploader1", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า
                        string[] OldFile = Up1.Text.Split('/');
                        int ncol = OldFile.Length - 1;
                        UploadFile2History(Up1.Text, HistoryPath, OldFile[ncol]);
                    }

                    #endregion

                    #region Upload2

                    string UpdateUpload2 = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE2) + "'";

                    string CheckUpload2 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID2) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE2) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName2.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath2.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename2.Text) + "'";

                    if (!string.IsNullOrEmpty(txtFileName2.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload2);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate2.Text))
                            {

                                AddhistoryVendorDoc(VendorID, SDOCID2, SDOCTYPE2);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload2.Value != null ? dedtUpload2.Value : DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                    com1.ExecuteNonQuery();

                                }
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up2.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "2";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "ใบอนุญาตขนส่ง";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload2.Value != null ? dedtUpload2.Value : DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {

                        using (OracleCommand com1 = new OracleCommand(UpdateUpload2, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName2.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath2.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename2.Text;

                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า
                        string[] OldFile = Up2.Text.Split('/');
                        int ncol = OldFile.Length - 1;
                        UploadFile2History(Up2.Text, HistoryPath, OldFile[ncol]);
                    }
                    #endregion

                    #region Upload3
                    string UpdateUpload3 = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE3) + "'";

                    string CheckUpload3 = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                            SCREATE, DUPDATE, SUPDATE, DEXPIRE,SPATH
                                            FROM TVENDOR_DOC 
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                            AND SDOCID = '" + CommonFunction.ReplaceInjection(SDOCID3) + @"'
                                            AND SDOCTYPE = '" + CommonFunction.ReplaceInjection(SDOCTYPE3) + @"'
                                            AND SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName3.Text) + @"'
                                            AND SPATH = '" + CommonFunction.ReplaceInjection(txtTruePath3.Text) + @"'
                                            AND SSYSFILENAME = '" + CommonFunction.ReplaceInjection(txtSysfilename3.Text) + "'";
                    if (!string.IsNullOrEmpty(txtFileName3.Text))
                    {

                        DataTable dtUpload = CommonFunction.Get_Data(con, CheckUpload3);

                        if (dtUpload.Rows.Count > 0)
                        {


                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(txtchkUpdate3.Text))
                            {
                                AddhistoryVendorDoc(VendorID, SDOCID3, SDOCTYPE3);
                                using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                                {
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                    com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload3.Value != null ? dedtUpload2.Value : DBNull.Value;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                    com1.ExecuteNonQuery();

                                }
                                //สร้างญPath
                                string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                                //splitเอาชื่อไฟล์เก่า
                                string[] OldFile = Up3.Text.Split('/');
                                int ncol = OldFile.Length - 1;
                                UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                            }
                            else
                            {
                                //ไม่เคยมีข้อมูลทำการเซฟข้อมูลลงเบส
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "3";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารภาษีอากรมาตรา12";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = dedtUpload3.Value != null ? dedtUpload2.Value : DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;
                                    com1.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                    else
                    {
                        using (OracleCommand com1 = new OracleCommand(UpdateUpload3, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName3.Text;
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath3.Text;
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename3.Text;

                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า
                        string[] OldFile = Up3.Text.Split('/');
                        int ncol = OldFile.Length - 1;
                        UploadFile2History(Up3.Text, HistoryPath, OldFile[ncol]);
                    }
                    #endregion

                    #region Upload4

                    var chknewDoc = listGrid.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
                    if (chknewDoc.Count() > 0)
                    {

                        foreach (var item in chknewDoc)
                        {
                            //int index = int.Parse(item.INDEX + "");

                            ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilename4") as ASPxTextBox;
                            ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFileName4") as ASPxTextBox;
                            ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtTruePath4") as ASPxTextBox;


                            using (OracleConnection con2 = new OracleConnection(conn))
                            {

                                if (con2.State == ConnectionState.Closed)
                                {
                                    con2.Open();
                                }
                                else
                                {

                                }
                                using (OracleCommand com1 = new OracleCommand(InsUpload, con2))
                                {
                                    decimal num = 0;
                                    com1.Parameters.Clear();
                                    com1.Parameters.Add(":SVENDORID", OracleType.VarChar).Value = VendorID;
                                    com1.Parameters.Add(":SDOCVERSION", OracleType.Number).Value = decimal.TryParse("1", out num) ? num : 0;
                                    com1.Parameters.Add(":SDOCTYPE", OracleType.VarChar).Value = "4";
                                    com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = txtFileName4.Text;
                                    com1.Parameters.Add(":SDESCRIPTION", OracleType.VarChar).Value = "เอกสารใบประจำต่อ";
                                    com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "1";
                                    com1.Parameters.Add(":SCREATE", OracleType.VarChar).Value = USER;
                                    com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                                    com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = txtTruePath4.Text;
                                    com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = txtSysfilename4.Text;

                                    com1.ExecuteNonQuery();
                                }
                                con2.Close();
                            }


                        }
                        //เมื่อทำการเซฟข้อมูลแล้ว1ครั้งจะมีใส่ค่า S
                        //FileToServer();
                        //txtUploadchk4.Text = "S";
                        //listGrid.RemoveAll(o => 1 == 1);
                        //Setdata();
                        //Listgvwdoc4();
                    }
                    else
                    {
                        Listgvwdoc4();
                    }
                    #endregion

                    #endregion

                    FileToServer();
                    con.Close();
                    VisibleControlUpload();
                    ListgvwDoc();
                    gvwDoc.DataBind();
                    txtUploadchk4.Text = "S";
                    listGrid.RemoveAll(o => 1 == 1);
                    Setdata();
                    Listgvwdoc4();
                    break;
                }

            #region ลบไฟล์ที่อับโหลด
            case "deleteFile":

                string FilePath = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath.Replace("/", "\\"));
                }

                string cNo = param[2];
                if (cNo == "1")
                {
                    txtFileName.Text = "";
                    txtFilePath.Text = "";
                    txtTruePath.Text = "";
                    txtSysfilename.Text = "";
                }


                VisibleControlUpload();
                break;

            case "deleteFile2":

                string FilePath2 = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath2.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath2.Replace("/", "\\"));
                }

                string cNo2 = param[2];
                if (cNo2 == "1")
                {
                    txtFileName2.Text = "";
                    txtFilePath2.Text = "";
                    txtTruePath2.Text = "";
                    txtSysfilename2.Text = "";
                    dedtUpload2.Value = "";
                }


                VisibleControlUpload();
                break;

            case "deleteFile3":

                string FilePath3 = param[1];

                if (File.Exists(Server.MapPath("./") + FilePath3.Replace("/", "\\")))
                {
                    File.Delete(Server.MapPath("./") + FilePath3.Replace("/", "\\"));
                }

                string cNo3 = param[2];
                if (cNo3 == "1")
                {
                    txtFileName3.Text = "";
                    txtFilePath3.Text = "";
                    txtTruePath3.Text = "";
                    txtSysfilename3.Text = "";
                    dedtUpload3.Value = "";
                }

                VisibleControlUpload();

                break;

                //case "deleteFile4":

                //    string FilePath4 = param[1];

                //    if (File.Exists(Server.MapPath("./") + FilePath4.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePath4.Replace("/", "\\"));
                //    }

                //    string cNo4 = param[2];
                //    if (cNo4 == "1")
                //    {
                //        txtFileName4.Text = "";
                //        txtFilePath4.Text = "";
                //        txtTruePath4.Text = "";
                //        txtSysfilename4.Text = "";
                //    }

                //    VisibleControlUpload();
                //    break;



            #endregion

                #region การทำงานในgvwDoc
                //case "deleteFilegvwDoc":
                //    string FilePathgvwDoc = param[1];
                //    //string view = param[4];


                //    ASPxTextBox txtFileNamegvwgvwDoc = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, param[4]) as ASPxTextBox;
                //    ASPxTextBox txtFilePathgvwgvwDoc = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, param[5]) as ASPxTextBox;
                //    ASPxButton btnViewgvwgvwDoc = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, param[6]) as ASPxButton;
                //    ASPxButton btnDelFilegvwDoc = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, param[7]) as ASPxButton;


                //    if (File.Exists(Server.MapPath("./") + FilePathgvwsign.Replace("/", "\\")))
                //    {
                //        File.Delete(Server.MapPath("./") + FilePathgvwsign.Replace("/", "\\"));
                //    }

                //    string cNogvwDoc = param[2];
                //    if (cNogvwsign == "1")
                //    {
                //        txtFilePathgvwsign.Text = "";
                //        txtFileNamegvwsign.Text = "";
                //        btnViewgvwsign.Visible = false;
                //        btnDelFilegvwsign.Visible = false;
                //    }

                using (OracleConnection con = new OracleConnection(conn))
                {

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    else
                    {

                    }

                    //                    int Numgvw = gvwsign.VisibleRowCount + 1;

                    //                    string SVENDORID = gvwsign.GetRowValues(int.Parse(param[3]), "SVENDORID").ToString();
                    //                    string LINE_NO = gvwsign.GetRowValues(int.Parse(param[3]), "LINE_NO").ToString();

                    //                    string strQuery = @"UPDATE TVENDOR_SIGNER
                    //SET PICTURE='" + CommonFunction.ReplaceInjection(txtFileNamegvwsign.Text) + @"',SFILENAME='" + CommonFunction.ReplaceInjection(txtFileNamegvwsign.Text) + @"',SPATH='" + CommonFunction.ReplaceInjection(txtFilePathgvwsign.Text) + @"'
                    //WHERE SVENDORID='" + SVENDORID + "' AND LINE_NO = '" + LINE_NO + "' ";

                    //                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    //                    {
                    //                        com.ExecuteNonQuery();
                    //                    }

                }
                break;
                #endregion
        }

    }

    void Setdata()
    {

        string SVENDORID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        #region set รายละเอียดบริษัท

        string strsql = @"SELECT vensap.DUPDATE, ven.SVENDORID,ven.SABBREVIATION,vensap.SNO , vensap.SDISTRICT, vensap.SREGION , vensap.SPROVINCE
, vensap.SPROVINCECODE , ven.STEL , ven.SFAX,ven.DSTARTPTT,ven.NCAPITAL,ven.DBEGINTRANSPORT,ven.DEXPIRETRANSPORT,ven.DBEGIN13BIS,ven.DEXPIRE13BIS,ven.DESCRIPTION,ven.NOTFILL
,ven.CACTIVE,ven.CAUSESAPCOMMIT,ven.CAUSESAPCANCEL,ven.CAUSESAP
FROM TVendor ven
LEFT  JOIN TVENDOR_SAP vensap
ON ven.SVENDORID = vensap.SVENDORID
WHERE ven.SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "'";

        DateTime? DSTARTPTT = null;
        DateTime? DBEGINTRANSPORT = null;
        DateTime? DEXPIRETRANSPORT = null;
        DateTime? DBEGIN13BIS = null;
        DateTime? DEXPIRE13BIS = null;

        DataTable dt = new DataTable();
        dt = CommonFunction.Get_Data(conn, strsql);
        if (dt.Rows.Count > 0)
        {
            txtSVENDORID.Text = dt.Rows[0]["SVENDORID"] + "";
            txtSABBREVIATION.Text = dt.Rows[0]["SABBREVIATION"] + "";
            txtSNO.Text = dt.Rows[0]["SNO"] + "";
            txtSDISTRICT.Text = dt.Rows[0]["SDISTRICT"] + "";
            txtSREGION.Text = dt.Rows[0]["SREGION"] + "";
            txtSPROVINCE.Text = dt.Rows[0]["SPROVINCE"] + "";
            txtSPROVINCECODE.Text = dt.Rows[0]["SPROVINCECODE"] + "";
            txtSTEL.Text = dt.Rows[0]["STEL"] + "";
            txtSFAX.Text = dt.Rows[0]["SFAX"] + "";

            if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
            {
                DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
            {
                DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
            {
                DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
            {
                DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
            }
            if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
            {
                DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
            }
            //if (dt.Rows[0]["NOTFILL"] + "" == "1")
            //{
            //    chkNotfill.Checked = true;
            //}
            //else
            //{
            //    chkNotfill.Checked = false;
            //}

            dedtDSTARTPTT.Value = DSTARTPTT;
            txtNCAPITAL.Text = dt.Rows[0]["NCAPITAL"] + "";
            dedtDBEGINTRANSPORT.Value = DBEGINTRANSPORT;
            dedtDEXPIRETRANSPORT.Value = DEXPIRETRANSPORT;
            dedtDBEGIN13BIS.Value = DBEGIN13BIS;
            dedtDEXPIRE13BIS.Value = DEXPIRE13BIS;
            txtDescriptionSigh.Text = dt.Rows[0]["DESCRIPTION"] + "";


            switch (dt.Rows[0]["CACTIVE"] + "")
            {
                case "0": rblStatus.SelectedIndex = 1;
                    //Typedefault.Visible = true;
                    //status.Style. = false;
                    //status2.Visible = false;
                    break;
                case "1": rblStatus.SelectedIndex = 0;
                    //Typedefault.Visible = false;
                    //status.Visible = true;
                    //status2.Visible = false;
                    break;
                case "2": rblStatus.SelectedIndex = 2;
                    //Typedefault.Visible = false;
                    //status.Visible = false;
                    //status2.Visible = true;
                    break;
                default: rblStatus.SelectedIndex = 0;
                    //Typedefault.Visible = true;
                    //status.Visible = false;
                    //status2.Visible = false;
                    break;
            }

            txtConfirm.Text = dt.Rows[0]["CAUSESAPCOMMIT"] + "";
            txtstatus2.Text = dt.Rows[0]["CAUSESAPCANCEL"] + "";
            txtComment.Text = dt.Rows[0]["CAUSESAP"] + "";

        }
        #endregion

        #region รายละเอียดUpload



        //string VendorID = ViewState["setdata"] + "";
        if (!string.IsNullOrEmpty(SVENDORID))
        {
            string strUpload = @"SELECT 
    SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL  ORDER BY SDOCTYPE ASC";
            DataTable dt2 = CommonFunction.Get_Data(conn, strUpload);


            DateTime? DEXPIRE2 = null;
            DateTime? DEXPIRE3 = null;

            //if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
            //{
            //    DEXPIRE3 = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
            //}


            #region setUpload1
            DataRow[] dtrow = dt2.Select("SDOCTYPE = '1'");
            if (dtrow.Count() > 0)
            {
                txtFileName.Text = dtrow[0][4] + "";
                txtFilePath.Text = dtrow[0][13] + "";
                txtTruePath.Text = dtrow[0][13] + "";
                txtSysfilename.Text = dtrow[0][5] + "";
                Up1.Text = dtrow[0][13] + "" + dtrow[0][5] + "";
                txtchkUpdate.Text = "1";
                ViewState["SDOCID"] = dtrow[0][1] + "";
                ViewState["SDOCTYPE"] = dtrow[0][3] + "";
            }
            else
            {
                txtFileName.Text = "";
                txtFilePath.Text = "";
                txtTruePath.Text = "";
                txtSysfilename.Text = "";
                Up1.Text = "";
                ViewState["SDOCID"] = "";
                ViewState["SDOCTYPE"] = "";
                txtchkUpdate.Text = "";
            }
            #endregion

            #region setupload2
            DataRow[] dtrow2 = dt2.Select("SDOCTYPE = '2'");
            if (dtrow2.Count() > 0)
            {

                if (!string.IsNullOrEmpty(dtrow2[0][12] + ""))
                {
                    DEXPIRE2 = DateTime.Parse(dtrow2[0][12] + "");

                }

                if (DEXPIRE2 <= DateTime.Now.Date)
                {
                    AddhistoryVendorDoc(dtrow2[0][0] + "", dtrow2[0][1] + "", dtrow2[0][3] + "");


                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }
                        string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(dtrow2[0][0] + "") + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(dtrow2[0][1] + "") + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(dtrow2[0][3] + "") + "'";

                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "0";
                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory2, Session["SVDID"] + "", "uploader2", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า

                        UploadFile2History(dtrow2[0][13] + "" + dtrow2[0][5] + "", HistoryPath, dtrow2[0][5] + "");
                    }
                }
                else
                {
                    txtFileName2.Text = dtrow2[0][4] + "";
                    txtFilePath2.Text = dtrow2[0][13] + "";
                    txtTruePath2.Text = dtrow2[0][13] + "";
                    txtSysfilename2.Text = dtrow2[0][5] + "";
                    Up2.Text = dtrow2[0][13] + "" + dtrow2[0][5] + "";
                    txtchkUpdate2.Text = "1";
                    ViewState["SDOCID2"] = dtrow2[0][1] + "";
                    ViewState["SDOCTYPE2"] = dtrow2[0][3] + "";
                    dedtUpload2.Value = DEXPIRE2;

                    //if (!string.IsNullOrEmpty(txtSysfilename2.Text))
                    //{
                    //    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                    //    {
                    //        txtValidation1.Text = ".";
                    //        txtValidation2.Text = ".";
                    //        txtValidation1.ClientVisible = false;
                    //        txtValidation2.ClientVisible = false;
                    //    }
                    //    else
                    //    {
                    //        txtValidation1.Text = ".";
                    //        txtValidation2.Text = "";
                    //        txtValidation1.ClientVisible = false;
                    //        txtValidation2.ClientVisible = true;
                    //    }
                    //}
                    //else
                    //{
                    //    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                    //    {
                    //        txtValidation1.Text = "";
                    //        txtValidation2.Text = ".";
                    //        txtValidation1.ClientVisible = true;
                    //        txtValidation2.ClientVisible = false;
                    //    }
                    //    else
                    //    {
                    //        txtValidation1.Text = ".";
                    //        txtValidation2.Text = ".";
                    //        txtValidation1.ClientVisible = false;
                    //        txtValidation2.ClientVisible = false;
                    //    }
                    //}



                }
            }
            else
            {
                txtFileName2.Text = "";
                txtFilePath2.Text = "";
                txtTruePath2.Text = "";
                txtSysfilename2.Text = "";
                Up2.Text = "";
                ViewState["SDOCID2"] = "";
                ViewState["SDOCTYPE2"] = "";
                dedtUpload2.Value = "";
                txtchkUpdate2.Text = "";

                //if (!string.IsNullOrEmpty(txtSysfilename2.Text))
                //{
                //    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                //    {
                //        txtValidation1.Text = ".";
                //        txtValidation2.Text = ".";
                //        txtValidation1.ClientVisible = false;
                //        txtValidation2.ClientVisible = false;
                //    }
                //    else
                //    {
                //        txtValidation1.Text = ".";
                //        txtValidation2.Text = "";
                //        txtValidation1.ClientVisible = false;
                //        txtValidation2.ClientVisible = true;
                //    }
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(dedtUpload2.Text))
                //    {
                //        txtValidation1.Text = "";
                //        txtValidation2.Text = ".";
                //        txtValidation1.ClientVisible = true;
                //        txtValidation2.ClientVisible = false;
                //    }
                //    else
                //    {
                //        txtValidation1.Text = ".";
                //        txtValidation2.Text = ".";
                //        txtValidation1.ClientVisible = false;
                //        txtValidation2.ClientVisible = false;
                //    }
                //}


            }
            #endregion

            #region setupload3
            DataRow[] dtrow3 = dt2.Select("SDOCTYPE = '3'");
            if (dtrow3.Count() > 0)
            {
                if (!string.IsNullOrEmpty(dtrow3[0][12] + ""))
                {
                    DEXPIRE3 = DateTime.Parse(dtrow3[0][12] + "");

                }

                if (DEXPIRE3 <= DateTime.Now.Date)
                {
                    AddhistoryVendorDoc(dtrow3[0][0] + "", dtrow3[0][1] + "", dtrow3[0][3] + "");


                    using (OracleConnection con = new OracleConnection(conn))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        else
                        {

                        }
                        string UpdateUpload = @"UPDATE TVENDOR_DOC
                                            SET SFILENAME=:SFILENAME
                                               ,SPATH=:SPATH
                                               ,DUPDATE=sysdate
                                               ,SUPDATE=:SUPDATE
                                               ,DEXPIRE=:DEXPIRE
                                               ,SSYSFILENAME=:SSYSFILENAME
                                               ,CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(dtrow3[0][0] + "") + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(dtrow3[0][1] + "") + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(dtrow3[0][3] + "") + "'";

                        using (OracleCommand com1 = new OracleCommand(UpdateUpload, con))
                        {
                            com1.Parameters.Clear();
                            com1.Parameters.Add(":SFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SPATH", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":SUPDATE", OracleType.VarChar).Value = USER;
                            com1.Parameters.Add(":DEXPIRE", OracleType.DateTime).Value = DBNull.Value;
                            com1.Parameters.Add(":SSYSFILENAME", OracleType.VarChar).Value = "";
                            com1.Parameters.Add(":CACTIVE", OracleType.VarChar).Value = "0";

                            com1.ExecuteNonQuery();

                        }
                        //สร้างญPath
                        string HistoryPath = string.Format(HistoryDirectory3, Session["SVDID"] + "", "uploader3", Session["UserID"] + "");
                        //splitเอาชื่อไฟล์เก่า

                        UploadFile2History(dtrow3[0][13] + "" + dtrow3[0][5] + "", HistoryPath, dtrow3[0][5] + "");
                    }
                }
                else
                {
                    txtFileName3.Text = dtrow3[0][4] + "";
                    txtFilePath3.Text = dtrow3[0][13] + "";
                    txtTruePath3.Text = dtrow3[0][13] + "";
                    txtSysfilename3.Text = dtrow3[0][5] + "";
                    Up3.Text = dtrow3[0][13] + "" + dtrow3[0][5] + "";
                    txtchkUpdate3.Text = "1";
                    ViewState["SDOCID3"] = dtrow3[0][1] + "";
                    ViewState["SDOCTYPE3"] = dtrow3[0][3] + "";
                    dedtUpload3.Value = DEXPIRE3;
                    //ใช้ค่าลงtext เพื่อเช็ค validation
                    if (!string.IsNullOrEmpty(txtSysfilename3.Text))
                    {
                        //if (!string.IsNullOrEmpty(dedtUpload3.Text))
                        //{
                        //    txtValidation3.Text = ".";
                        //    txtValidation4.Text = ".";
                        //    txtValidation3.ClientVisible = false;
                        //    txtValidation4.ClientVisible = false;
                        //}
                        //else
                        //{
                        //    txtValidation3.Text = ".";
                        //    txtValidation4.Text = "";
                        //    txtValidation3.ClientVisible = false;
                        //    txtValidation4.ClientVisible = true;
                        //}
                    }
                    else
                    {
                        //if (!string.IsNullOrEmpty(dedtUpload3.Text))
                        //{
                        //    txtValidation3.Text = "";
                        //    txtValidation4.Text = ".";
                        //    txtValidation3.ClientVisible = true;
                        //    txtValidation4.ClientVisible = false;
                        //}
                        //else
                        //{
                        //    txtValidation3.Text = ".";
                        //    txtValidation4.Text = ".";
                        //    txtValidation3.ClientVisible = false;
                        //    txtValidation4.ClientVisible = false;
                        //}
                    }
                }
            }
            else
            {
                txtFileName3.Text = "";
                txtFilePath3.Text = "";
                txtTruePath3.Text = "";
                txtSysfilename3.Text = "";
                Up3.Text = "";
                ViewState["SDOCID3"] = "";
                ViewState["SDOCTYPE3"] = "";
                dedtUpload3.Value = "";
                txtchkUpdate3.Text = "";
                //ใช้ค่าลง text เพื่อเช็ค validation
                if (!string.IsNullOrEmpty(txtSysfilename3.Text))
                {
                    //if (!string.IsNullOrEmpty(dedtUpload3.Text))
                    //{
                    //    txtValidation3.Text = ".";
                    //    txtValidation4.Text = ".";
                    //    txtValidation3.ClientVisible = false;
                    //    txtValidation4.ClientVisible = false;
                    //}
                    //else
                    //{
                    //    txtValidation3.Text = ".";
                    //    txtValidation4.Text = "";
                    //    txtValidation3.ClientVisible = false;
                    //    txtValidation4.ClientVisible = true;
                    //}
                }
                else
                {
                    //if (!string.IsNullOrEmpty(dedtUpload3.Text))
                    //{
                    //    txtValidation3.Text = "";
                    //    txtValidation4.Text = ".";
                    //    txtValidation3.ClientVisible = true;
                    //    txtValidation4.ClientVisible = false;
                    //}
                    //else
                    //{
                    //    txtValidation3.Text = ".";
                    //    txtValidation4.Text = ".";
                    //    txtValidation3.ClientVisible = false;
                    //    txtValidation4.ClientVisible = false;
                    //}
                }
            }
            #endregion

        }
        #endregion

        //เก็บข้อมูลจากเบสลง List listgrid
        string sql = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                          SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH 
                          FROM TVENDOR_DOC 
                          WHERE CACTIVE ='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL AND SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND SDOCTYPE ='4'";
        DataTable dtDoc4 = CommonFunction.Get_Data(conn, sql);
        //เก็บข้อมูลเข้า list เพื่อสแดงใน Upload4
        if (dtDoc4.Rows.Count > 0)
        {
            DateTime? DCREATE = null;
            DateTime? DUPDATE = null;
            DateTime? DEXPIRE = null;

            int i;
            for (i = 0; i <= dtDoc4.Rows.Count - 1; i++)
            {

                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DCREATE"] + ""))
                {
                    DCREATE = DateTime.Parse(dtDoc4.Rows[i]["DCREATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DUPDATE"] + ""))
                {
                    DUPDATE = DateTime.Parse(dtDoc4.Rows[i]["DUPDATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDoc4.Rows[i]["DEXPIRE"] + ""))
                {
                    DEXPIRE = DateTime.Parse(dtDoc4.Rows[i]["DEXPIRE"] + "");
                }


                listGrid.Add(new ListGrid
                {
                    SVENDORID = dtDoc4.Rows[i]["SVENDORID"] + "",
                    SDOCID = dtDoc4.Rows[i]["SDOCID"] + "",
                    SDOCVERSION = int.Parse(dtDoc4.Rows[i]["SDOCVERSION"] + ""),
                    SDOCTYPE = dtDoc4.Rows[i]["SDOCTYPE"] + "",
                    SFILENAME = dtDoc4.Rows[i]["SFILENAME"] + "",
                    SSYSFILENAME = dtDoc4.Rows[i]["SSYSFILENAME"] + "",
                    SDESCRIPTION = dtDoc4.Rows[i]["SDESCRIPTION"] + "",
                    CACTIVE = dtDoc4.Rows[i]["CACTIVE"] + "",
                    DCREATE = DCREATE,
                    SCREATE = dtDoc4.Rows[i]["SCREATE"] + "",
                    DUPDATE = DUPDATE,
                    SUPDATE = dtDoc4.Rows[i]["SUPDATE"] + "",
                    SPATH = dtDoc4.Rows[i]["SPATH"] + ""
                });
            }
        }
        txtUploadchk4.Text = "";

        //เก็บข้อมูลเข้า list เพื่อสแดงใน Uploadother

        string sqlother = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                          SCREATE, DUPDATE, SUPDATE, DEXPIRE, SPATH 
                          FROM TVENDOR_DOC 
                          WHERE CACTIVE ='1' AND  SFILENAME IS NOT NULL AND SSYSFILENAME  IS NOT NULL AND  SPATH  IS NOT NULL AND SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND SDOCTYPE ='5'";
        DataTable dtDocother = CommonFunction.Get_Data(conn, sqlother);
        if (dtDocother.Rows.Count > 0)
        {
            DateTime? DCREATE = null;
            DateTime? DUPDATE = null;
            DateTime? DEXPIRE = null;

            int i;
            for (i = 0; i <= dtDocother.Rows.Count - 1; i++)
            {

                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DCREATE"] + ""))
                {
                    DCREATE = DateTime.Parse(dtDocother.Rows[i]["DCREATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DUPDATE"] + ""))
                {
                    DUPDATE = DateTime.Parse(dtDocother.Rows[i]["DUPDATE"] + "");
                }
                if (!string.IsNullOrEmpty(dtDocother.Rows[i]["DEXPIRE"] + ""))
                {
                    DEXPIRE = DateTime.Parse(dtDocother.Rows[i]["DEXPIRE"] + "");
                }


                listGridother.Add(new ListGridother
                {
                    SVENDORID = dtDocother.Rows[i]["SVENDORID"] + "",
                    SDOCID = dtDocother.Rows[i]["SDOCID"] + "",
                    SDOCVERSION = int.Parse(dtDocother.Rows[i]["SDOCVERSION"] + ""),
                    SDOCTYPE = dtDocother.Rows[i]["SDOCTYPE"] + "",
                    SFILENAME = dtDocother.Rows[i]["SFILENAME"] + "",
                    SSYSFILENAME = dtDocother.Rows[i]["SSYSFILENAME"] + "",
                    SDESCRIPTION = dtDocother.Rows[i]["SDESCRIPTION"] + "",
                    CACTIVE = dtDocother.Rows[i]["CACTIVE"] + "",
                    DCREATE = DCREATE,
                    SCREATE = dtDocother.Rows[i]["SCREATE"] + "",
                    DUPDATE = DUPDATE,
                    SUPDATE = dtDocother.Rows[i]["SUPDATE"] + "",
                    SPATH = dtDocother.Rows[i]["SPATH"] + ""
                });
            }
        }
        txtUploadother.Text = "";


        //เก็บข้อมูลเข้า list เพื่อสแดงใน gvwDoc
        if (!string.IsNullOrEmpty(SVENDORID))
        {
            string strsqldoc = @"SELECT 
    SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SSYSFILENAME IS NOT NULL ORDER BY SDOCTYPE ASC";
            DataTable dtDoc = CommonFunction.Get_Data(conn, strsqldoc);
            //var dblist = listGrid;

            if (dtDoc.Rows.Count > 0)
            {
                DateTime? DCREATE = null;
                DateTime? DUPDATE = null;
                DateTime? DEXPIRE = null;

                int i;
                for (i = 0; i <= dtDoc.Rows.Count - 1; i++)
                {

                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DCREATE"] + ""))
                    {
                        DCREATE = DateTime.Parse(dtDoc.Rows[i]["DCREATE"] + "");
                    }
                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DUPDATE"] + ""))
                    {
                        DUPDATE = DateTime.Parse(dtDoc.Rows[i]["DUPDATE"] + "");
                    }
                    if (!string.IsNullOrEmpty(dtDoc.Rows[i]["DEXPIRE"] + ""))
                    {
                        DEXPIRE = DateTime.Parse(dtDoc.Rows[i]["DEXPIRE"] + "");
                    }


                    listGriddoc.Add(new ListGridDoc
                    {
                        SVENDORID = dtDoc.Rows[i]["SVENDORID"] + "",
                        SDOCID = dtDoc.Rows[i]["SDOCID"] + "",
                        SDOCVERSION = int.Parse(dtDoc.Rows[i]["SDOCVERSION"] + ""),
                        SDOCTYPE = dtDoc.Rows[i]["SDOCTYPE"] + "",
                        SFILENAME = dtDoc.Rows[i]["SFILENAME"] + "",
                        SSYSFILENAME = dtDoc.Rows[i]["SSYSFILENAME"] + "",
                        SDESCRIPTION = dtDoc.Rows[i]["SDESCRIPTION"] + "",
                        CACTIVE = dtDoc.Rows[i]["CACTIVE"] + "",
                        DCREATE = DCREATE,
                        SCREATE = dtDoc.Rows[i]["SCREATE"] + "",
                        DUPDATE = DUPDATE,
                        SUPDATE = dtDoc.Rows[i]["SUPDATE"] + "",
                        SPATH = dtDoc.Rows[i]["SPATH"] + ""
                    });
                }
            }
        }



    }

    #region อัพโหลดแนบหนังสือรับรองผู้ขนส่ง
    protected void upload_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }


    #endregion

    #region อัพโหลดใบอนุญาตขนส่ง

    protected void upload2_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory2, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region อัพโหลดเอกสารภาษีอากรมาตรา12

    protected void upload3_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory3, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];


            }
        }
        else
        {

            return;

        }
    }

    #endregion

    #region อัพโหลดเอกสารใบประจำต่อ
    protected void gvwdoc4_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePath4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePath4") as ASPxTextBox;
            ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileName4") as ASPxTextBox;
            ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtTruePath4") as ASPxTextBox;
            ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtSysfilename4") as ASPxTextBox;
            ASPxLabel lblData2 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwdoc4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwdoc4") as ASPxButton;
            ASPxButton btnDelFilegvwdoc4 = gvwdoc4.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwdoc4") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePath4.Text))
            {
                btnViewgvwdoc4.ClientVisible = true;
                btnDelFilegvwdoc4.ClientVisible = true;
                btnViewgvwdoc4.ClientEnabled = true;
                btnDelFilegvwdoc4.ClientEnabled = true;

            }
            else
            {
                btnViewgvwdoc4.ClientVisible = true;
                btnDelFilegvwdoc4.ClientVisible = true;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileName4.ClientInstanceName = txtFileName4.ClientInstanceName + "_" + VisibleIndex;
            txtFilePath4.ClientInstanceName = txtFilePath4.ClientInstanceName + "_" + VisibleIndex;
            txtTruePath4.ClientInstanceName = txtTruePath4.ClientInstanceName + "_" + VisibleIndex;
            txtSysfilename4.ClientInstanceName = txtSysfilename4.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwdoc4.ClientInstanceName = btnViewgvwdoc4.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwdoc4.ClientInstanceName = btnDelFilegvwdoc4.ClientInstanceName + "_" + VisibleIndex;

            // Add Event
            btnViewgvwdoc4.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePath4.ClientInstanceName + ".GetValue()+" + txtSysfilename4.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwdoc4.ClientSideEvents.Click = "function (s, e) { gvwdoc4.PerformCallback(\"deleteUpload4; " + txtFilePath4.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + ";" + txtFileName4.ClientInstanceName.ToString() + ";" + txtFilePath4.ClientInstanceName.ToString() + " ;" + btnViewgvwdoc4.ClientInstanceName.ToString() + " ; " + btnDelFilegvwdoc4.ClientInstanceName.ToString() + "\");}";
            //btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void uploaderDoc4_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        int ncol = _Filename.Length - 1;

        //if (_Filename[ncol] == "xlsx" || _Filename[ncol] == "xls" || _Filename[ncol] == "doc" || _Filename[ncol] == "docx" || _Filename[ncol] == "pdf" || _Filename[ncol] == "jpg" || _Filename[ncol] == "jpeg")
        //{

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory4, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGrid.Add(new ListGrid
                {
                    SFILENAME = e.UploadedFile.FileName,
                    SSYSFILENAME = genName + "." + _Filename[count],
                    SPATH = data,
                    STRUEPATH = data2,
                    SVENDORID = VendorID,
                    NEWPICTRUEFLAG = "1",
                    INDEX = listGrid.Count
                });
            }
        }
        else
        {

            return;

        }
        //}
        //else
        //{
        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอับเอกสารประเภทนี้ได้');");
        //}
    }



    protected void gvwdoc4_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxUploadControl Upload = gvwdoc4.FindEditRowCellTemplateControl(null, "uploaderDoc4") as ASPxUploadControl;
        Upload.ClientVisible = true;
        e.Cancel = true;
        //Listgvwdoc4();
        gvwdoc4.CancelEdit();

    }
    #endregion

    #region อัพโหลดในEditFrom

    protected void uploadgvw5_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string[] _Filename = e.UploadedFile.FileName.Split('.');

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory5, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "") + genName + "." + _Filename[count];
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName;


            }
        }
        else
        {

            return;

        }

    }

    private void VisibleControlUpload5()
    {
        #region เก็ตคอนโทรน
        ASPxUploadControl uploader5 = (ASPxUploadControl)gvwsign.FindEditFormTemplateControl("uploader5");
        ASPxTextBox txtFileName5 = (ASPxTextBox)gvwsign.FindEditFormTemplateControl("txtFileName5");
        ASPxUploadControl suploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
        ASPxTextBox schkUpload5 = gvwsign.FindEditFormTemplateControl("chkUpload5") as ASPxTextBox;
        ASPxTextBox txtsFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
        ASPxTextBox txtsFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
        ASPxButton btnView5 = gvwsign.FindEditFormTemplateControl("btnView5") as ASPxButton;
        ASPxButton btnDelFile5 = gvwsign.FindEditFormTemplateControl("btnDelFile5") as ASPxButton;
        #endregion
        bool visible = string.IsNullOrEmpty(txtsFilePath5.Text);
        suploader5.Visible = true;
        txtsFileName5.ClientVisible = !visible;
        btnView5.ClientEnabled = !visible;
        btnDelFile5.ClientEnabled = !visible;

        if (!(string.IsNullOrEmpty(txtsFilePath5.Text)))
        {
            schkUpload5.Value = "1";
        }
        else
        {
            schkUpload5.Value = "";
        }


    }
    #endregion

    #region อัพโหลดเอกสารอื่นๆ
    protected void gvwother_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathother") as ASPxTextBox;
            ASPxTextBox txtFileNameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNameother") as ASPxTextBox;
            ASPxTextBox txtTruePathother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtTruePathother") as ASPxTextBox;
            ASPxTextBox txtSysfilenameother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtSysfilenameother") as ASPxTextBox;
            ASPxLabel lblData2 = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwother") as ASPxButton;
            ASPxButton btnDelFilegvwother = gvwother.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwother") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathother.Text))
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                btnViewgvwother.ClientEnabled = true;
                btnDelFilegvwother.ClientEnabled = true;

            }
            else
            {
                btnViewgvwother.ClientVisible = true;
                btnDelFilegvwother.ClientVisible = true;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileNameother.ClientInstanceName = txtFileNameother.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathother.ClientInstanceName = txtFilePathother.ClientInstanceName + "_" + VisibleIndex;
            txtTruePathother.ClientInstanceName = txtTruePathother.ClientInstanceName + "_" + VisibleIndex;
            txtSysfilenameother.ClientInstanceName = txtSysfilenameother.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwother.ClientInstanceName = btnViewgvwother.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwother.ClientInstanceName = btnDelFilegvwother.ClientInstanceName + "_" + VisibleIndex;

            // Add Event
            btnViewgvwother.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathother.ClientInstanceName + ".GetValue()+" + txtSysfilenameother.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwother.ClientSideEvents.Click = "function (s, e) { gvwother.PerformCallback(\"deleteUploadother; " + txtFilePathother.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + ";" + txtFileNameother.ClientInstanceName.ToString() + ";" + txtFilePathother.ClientInstanceName.ToString() + " ;" + btnViewgvwother.ClientInstanceName.ToString() + " ; " + btnDelFilegvwother.ClientInstanceName.ToString() + "\");}";
            //btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    protected void uploaderother_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        string[] _Filename = e.UploadedFile.FileName.Split('.');
        int ncol = _Filename.Length - 1;

        //if (_Filename[ncol] == "xlsx" || _Filename[ncol] == "xls" || _Filename[ncol] == "doc" || _Filename[ncol] == "docx" || _Filename[ncol] == "pdf" || _Filename[ncol] == "jpg" || _Filename[ncol] == "jpeg")
        //{

        if (_Filename[0] != "")
        {
            int count = _Filename.Count() - 1;
            ASPxUploadControl upl = (ASPxUploadControl)sender;

            string genName = "Vendor" + DateTime.Now.ToString("_dMyyHHmmss", new CultureInfo("th-TH"));
            if (UploadFile2Server(e.UploadedFile, genName, string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "")))
            {
                string data = string.Format(TempDirectory, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                string data2 = string.Format(SaverDirectory6, Session["SVDID"] + "", upl.ClientInstanceName, Session["UserID"] + "");
                //e.CallbackData = data;

                e.CallbackData = data + "|" + e.UploadedFile.FileName + "|" + data2 + "|" + genName + "." + _Filename[count];
                listGridother.Add(new ListGridother
                {
                    SFILENAME = e.UploadedFile.FileName,
                    SSYSFILENAME = genName + "." + _Filename[count],
                    SPATH = data,
                    STRUEPATH = data2,
                    SVENDORID = VendorID,
                    NEWPICTRUEFLAG = "1",
                    SDOCTYPE = "5",
                    INDEX = listGridother.Count
                });
            }
        }
        else
        {

            return;

        }
        //}
        //else
        //{
        //    CommonFunction.SetPopupOnLoad(xcpn, "dxWarning('" + Resources.CommonResource.Msg_Alert_Title_Error + "','ไม่สามารถอับเอกสารประเภทนี้ได้');");
        //}
    }



    protected void gvwother_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxUploadControl Upload = gvwother.FindEditRowCellTemplateControl(null, "uploaderother") as ASPxUploadControl;
        Upload.ClientVisible = true;
        e.Cancel = true;
        //Listgvwdoc4();
        gvwother.CancelEdit();

    }
    #endregion


    private void VisibleControlUpload()
    {
        if (!(string.IsNullOrEmpty(txtFilePath.Text)))
        {
            bool visible = string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = visible;
            txtFileName.ClientVisible = !visible;
            btnView.ClientEnabled = !visible;
            btnDelFile.ClientEnabled = !visible;


            chkUpload1.Value = "1";
        }
        else
        {
            bool visible = !string.IsNullOrEmpty(txtFilePath.Text);
            uploader1.ClientVisible = !visible;
            txtFileName.ClientVisible = visible;
            btnView.ClientEnabled = visible;
            btnDelFile.ClientEnabled = visible;
            chkUpload1.Value = "";
        }

        if (!(string.IsNullOrEmpty(txtFilePath2.Text)))
        {
            bool visible2 = string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = visible2;
            txtFileName2.ClientVisible = !visible2;
            btnView2.ClientEnabled = !visible2;
            btnDelFile2.ClientEnabled = !visible2;


            chkUpload2.Value = "1";
        }
        else
        {
            bool visible2 = !string.IsNullOrEmpty(txtFilePath2.Text);
            uploader2.ClientVisible = !visible2;
            txtFileName2.ClientVisible = visible2;
            btnView2.ClientEnabled = visible2;
            btnDelFile2.ClientEnabled = visible2;
            chkUpload2.Value = "";
        }


        if (!(string.IsNullOrEmpty(txtFilePath3.Text)))
        {
            bool visible3 = string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = visible3;
            txtFileName3.ClientVisible = !visible3;
            btnView3.ClientEnabled = !visible3;
            btnDelFile3.ClientEnabled = !visible3;


            chkUpload3.Value = "1";
        }
        else
        {
            bool visible3 = !string.IsNullOrEmpty(txtFilePath3.Text);
            uploader3.ClientVisible = !visible3;
            txtFileName3.ClientVisible = visible3;
            btnView3.ClientEnabled = visible3;
            btnDelFile3.ClientEnabled = visible3;
            chkUpload3.Value = "";
        }


        //if (!(string.IsNullOrEmpty(txtFilePath4.Text)))
        //{

        //    bool visible4 = string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = visible4;
        //    txtFileName4.ClientVisible = !visible4;
        //    btnView4.ClientEnabled = !visible4;
        //    btnDelFile4.ClientEnabled = !visible4;


        //    chkUpload4.Value = "1";
        //}
        //else
        //{
        //    bool visible4 = !string.IsNullOrEmpty(txtFilePath4.Text);
        //    uploader4.ClientVisible = !visible4;
        //    txtFileName4.ClientVisible = visible4;
        //    btnView4.ClientEnabled = visible4;
        //    btnDelFile4.ClientEnabled = visible4;
        //    chkUpload4.Value = "";
        //}

    }

    void FileToServer()
    {
        #region เก็บPathลงviewstate
        if (txtFilePath.Text != "")
        {

            UploadFileToServer(txtFilePath.Text, txtTruePath.Text, txtSysfilename.Text);
        }
        if (txtFilePath2.Text != "")
        {


            UploadFileToServer(txtFilePath2.Text, txtTruePath2.Text, txtSysfilename2.Text);
        }
        if (txtFilePath3.Text != "")
        {


            UploadFileToServer(txtFilePath3.Text, txtTruePath3.Text, txtSysfilename3.Text);
        }
        var chknewDoc = listGrid.Where(w => w.NEWPICTRUEFLAG == "1").ToList();
        if (chknewDoc.Count() > 0)
        {

            foreach (var item in chknewDoc)
            {
                ASPxTextBox txtSysfilename4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtSysfilename4") as ASPxTextBox;
                ASPxTextBox txtFileName4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFileName4") as ASPxTextBox;
                ASPxTextBox txtTruePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtTruePath4") as ASPxTextBox;
                ASPxTextBox txtFilePath4 = gvwdoc4.FindRowCellTemplateControl(item.INDEX, null, "txtFilePath4") as ASPxTextBox;
                if (txtFilePath4.Text != "")
                {

                    UploadFileToServer(txtFilePath4.Text, txtTruePath4.Text, txtSysfilename4.Text);
                }
            }
        }
        #endregion
    }

    private void UploadFileToServer(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath + filename))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old + filename, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath + filename.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath + filename);
            }
        }
    }

    private bool UploadFile2Server(UploadedFile ful, string GenFileName, string pathFile)
    {
        string[] fileExt = ful.FileName.Split('.');
        int ncol = fileExt.Length - 1;
        if (ful.FileName != string.Empty) //ถ้ามีการแอดไฟล์
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)

            if (!Directory.Exists(Server.MapPath("./") + pathFile.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + pathFile.Replace("/", "\\"));
            }
            #endregion

            string fileName = (GenFileName + "." + fileExt[ncol].ToLower().Trim());

            ful.SaveAs(Server.MapPath("./") + pathFile + fileName);
            return true;
        }
        else
            return false;
    }

    private void UploadFile2History(string OldPath, string NewPath, string filename)
    {

        if (File.Exists(Server.MapPath("./") + OldPath))
        {
            #region Create Directory (เช็ค และสร้างไดเร็คเตอรี)
            if (!Directory.Exists(Server.MapPath("./") + NewPath.Replace("/", "\\")))
            {
                Directory.CreateDirectory(Server.MapPath("./") + NewPath.Replace("/", "\\"));
            }
            string Old = Server.MapPath("./") + OldPath.Replace("/", "\\");
            string New = Server.MapPath("./") + NewPath.Replace("/", "\\");

            #endregion

            File.Move(Old, New + filename);
            //เช็คว่าไฟมีไหมถ้าไม่มีให้ลบ
            if (Directory.Exists(Server.MapPath("./") + OldPath.Replace("/", "\\")))
            {
                File.Delete(Server.MapPath("./") + OldPath);
            }
        }
    }

    void Listgvwsign()
    {
        string VendorID = ViewState["setdata"] + "";
        if (!string.IsNullOrEmpty(VendorID))
        {
            string strsql = @"SELECT 
  SVENDORID, LINE_NO, SNAME, 
   SPOSITION, PHONE, PHONE2, 
   EMAIL, PICTURE, IS_ACTIVE, 
   DATE_CREATED, USER_CREATED, DATE_UPDATED, 
   USER_UPDATED, SFILENAME, SPATH,SSYSFILENAME
FROM TVENDOR_SIGNER WHERE SVENDORID = '" + VendorID + "' AND IS_ACTIVE='1'  ORDER BY LINE_NO DESC";


            DataTable dt = CommonFunction.Get_Data(conn, strsql);


            if (dt.Rows.Count > 0)
            {
                gvwsign.DataSource = dt;
                gvwsign.DataBind();
            }
            else
            {
                gvwsign.DataSource = dt;
                gvwsign.DataBind();
                //gvwsign.AddNewRow();
            }


        }
    }

    void ListgvwDoc()
    {
        string VendorID = ViewState["setdata"] + "";
        if (!string.IsNullOrEmpty(VendorID))
        {
            string strsql = @"SELECT 
    SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH 
FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "' AND CACTIVE = '1' AND SSYSFILENAME IS NOT NULL ORDER BY SDOCTYPE ASC";
            DataTable dt = CommonFunction.Get_Data(conn, strsql);


            if (dt.Rows.Count > 0)
            {
                gvwDoc.DataSource = dt;
                gvwDoc.DataBind();
            }
            else
            {
                gvwsign.DataSource = dt;
                gvwsign.DataBind();
            }
        }
    }

    protected void gvwsign_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("รูปภาพ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwsign") as ASPxTextBox;
            ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwsign") as ASPxTextBox;
            ASPxLabel lblData = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData") as ASPxLabel;
            ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwsign") as ASPxButton;
            ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwsign") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathgvwsign.Text))
            {
                btnViewgvwsign.ClientVisible = true;
                btnDelFilegvwsign.ClientVisible = false;
                btnViewgvwsign.ClientEnabled = true;
                btnDelFilegvwsign.ClientEnabled = true;

            }
            else
            {
                btnViewgvwsign.ClientVisible = false;
                btnDelFilegvwsign.ClientVisible = false;
                lblData.ClientVisible = true;
                //lblData.Text = string.Empty;

            }
            txtFileNamegvwsign.ClientInstanceName = txtFileNamegvwsign.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathgvwsign.ClientInstanceName = txtFilePathgvwsign.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwsign.ClientInstanceName = btnViewgvwsign.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwsign.ClientInstanceName = btnDelFilegvwsign.ClientInstanceName + "_" + VisibleIndex;

            //Add Event
            btnViewgvwsign.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathgvwsign.ClientInstanceName + ".GetValue()+" + txtFileNamegvwsign.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwsign.ClientSideEvents.Click = "function (s, e) { gvwsign.PerformCallback(\"deleteFilegvwsign; " + txtFilePathgvwsign.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwsign.ClientInstanceName.ToString() + ";" + txtFilePathgvwsign.ClientInstanceName.ToString() + " ;" + btnViewgvwsign.ClientInstanceName.ToString() + " ; " + btnDelFilegvwsign.ClientInstanceName.ToString() + "\");}";
            // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }
    //protected void gvwsign_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    //{
    //    if (e.DataColumn.Caption.Equals("รูปภาพ"))
    //    {
    //        int VisibleIndex = e.VisibleIndex;
    //        ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwsign") as ASPxTextBox;
    //        ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwsign") as ASPxTextBox;
    //        ASPxLabel lblData = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData") as ASPxLabel;
    //        ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwsign") as ASPxButton;
    //        ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwsign") as ASPxButton;
    //        if (!string.IsNullOrEmpty(txtFilePathgvwsign.Text))
    //        {
    //            btnViewgvwsign.ClientVisible = true;
    //            btnDelFilegvwsign.ClientVisible = true;
    //            btnViewgvwsign.ClientEnabled = true;
    //            btnDelFilegvwsign.ClientEnabled = true;

    //        }
    //        else
    //        {
    //            btnViewgvwsign.ClientVisible = false;
    //            btnDelFilegvwsign.ClientVisible = false;
    //            lblData.ClientVisible = true;
    //            lblData.Text = string.Empty;

    //        }
    //        txtFileNamegvwsign.ClientInstanceName = txtFileNamegvwsign.ClientInstanceName + "_" + VisibleIndex;
    //        txtFilePathgvwsign.ClientInstanceName = txtFilePathgvwsign.ClientInstanceName + "_" + VisibleIndex;
    //        btnViewgvwsign.ClientInstanceName = btnViewgvwsign.ClientInstanceName + "_" + VisibleIndex;
    //        btnDelFilegvwsign.ClientInstanceName = btnDelFilegvwsign.ClientInstanceName + "_" + VisibleIndex;

    //        //Add Event
    //        btnViewgvwsign.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathgvwsign.ClientInstanceName + ".GetValue());}";
    //        btnDelFilegvwsign.ClientSideEvents.Click = "function (s, e) { xcpn.PerformCallback(\"deleteFilegvwsign; " + txtFilePathgvwsign.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwsign.ClientInstanceName.ToString() + ";" + txtFilePathgvwsign.ClientInstanceName.ToString() + " ;" + btnViewgvwsign.ClientInstanceName.ToString() + " ; " + btnDelFilegvwsign.ClientInstanceName.ToString() + "\");}";
    //        // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
    //    }
    //}

    protected void gvwDoc_OnHtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption.Equals("การจัดการ"))
        {
            int VisibleIndex = e.VisibleIndex;
            ASPxTextBox txtFilePathgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFilePathgvwDoc") as ASPxTextBox;
            ASPxTextBox txtFileNamegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "txtFileNamegvwDoc") as ASPxTextBox;
            ASPxLabel lblData2 = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "lblData2") as ASPxLabel;

            ASPxButton btnViewgvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnViewgvwDoc") as ASPxButton;
            ASPxButton btnDelFilegvwDoc = gvwDoc.FindRowCellTemplateControl(VisibleIndex, e.DataColumn, "btnDelFilegvwDoc") as ASPxButton;
            if (!string.IsNullOrEmpty(txtFilePathgvwDoc.Text))
            {
                btnViewgvwDoc.ClientVisible = true;
                btnDelFilegvwDoc.ClientVisible = true;
                btnViewgvwDoc.ClientEnabled = true;
                btnDelFilegvwDoc.ClientEnabled = true;

            }
            else
            {
                btnViewgvwDoc.ClientVisible = false;
                btnDelFilegvwDoc.ClientVisible = false;
                lblData2.ClientVisible = true;
                lblData2.Text = string.Empty;

            }
            txtFileNamegvwDoc.ClientInstanceName = txtFileNamegvwDoc.ClientInstanceName + "_" + VisibleIndex;
            txtFilePathgvwDoc.ClientInstanceName = txtFilePathgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnViewgvwDoc.ClientInstanceName = btnViewgvwDoc.ClientInstanceName + "_" + VisibleIndex;
            btnDelFilegvwDoc.ClientInstanceName = btnDelFilegvwDoc.ClientInstanceName + "_" + VisibleIndex;

            //Add Event
            btnViewgvwDoc.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + txtFilePathgvwDoc.ClientInstanceName + ".GetValue() +" + txtFileNamegvwDoc.ClientInstanceName + ".GetValue());}";
            btnDelFilegvwDoc.ClientSideEvents.Click = "function (s, e) { xcpn.PerformCallback(\"deleteFilegvwsign; " + txtFilePathgvwDoc.ClientInstanceName + ".GetValue() ;1;" + VisibleIndex + " ;" + txtFileNamegvwDoc.ClientInstanceName.ToString() + ";" + txtFilePathgvwDoc.ClientInstanceName.ToString() + " ;" + btnViewgvwDoc.ClientInstanceName.ToString() + " ; " + btnDelFilegvwDoc.ClientInstanceName.ToString() + "\");}";
            // btnViewgvwsign.ClientSideEvents.Click = "function (s, e) { " + btnViewgvwsign.ClientInstanceName + ".SetValue('1'); s.SetEnabled(false); " + imbcancel.ClientInstanceName + ".SetEnabled(true);  }";
        }
    }

    private void AddhistoryVendorsigner(string VendorID, string LINE_NO)
    {


        #region เก็ตคอนโทรน
        //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
        //ASPxUploadControl uploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
        ASPxTextBox txtName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
        ASPxTextBox txtPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
        ASPxTextBox txtTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
        ASPxTextBox txtTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
        ASPxTextBox txtEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
        ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
        ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
        #endregion


        string senddatatohistory = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH
                                      FROM TVENDOR_SIGNER WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND LINE_NO ='" + CommonFunction.ReplaceInjection(LINE_NO + "") + "'";




        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_signer มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH ,NVERSION
                                      FROM TVENDOR_SIGNER_HISTORY 
                                      WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'  AND LINE_NO ='" + CommonFunction.ReplaceInjection(LINE_NO + "") + @"' 
                                      ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

                    string strQuery = @"INSERT INTO TVENDOR_SIGNER_HISTORY(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED,SFILENAME,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["LINE_NO"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SNAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPOSITION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE2"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["EMAIL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PICTURE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["IS_ACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_SIGNER_HISTORY(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,DATE_UPDATED,USER_UPDATED,SFILENAME,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["LINE_NO"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SNAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPOSITION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PHONE2"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["EMAIL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["PICTURE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["IS_ACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_CREATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_CREATED"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DATE_UPDATED"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["USER_UPDATED"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void AddhistoryVendorDoc(string VendorID, string SDOCID, string SDOCTYPE)
    {
        string senddatatohistory = @"SELECT  SVENDORID, SDOCID, SDOCVERSION, 
   SDOCTYPE, SFILENAME, SSYSFILENAME, 
   SDESCRIPTION, CACTIVE, DCREATE, 
   SCREATE, DUPDATE, SUPDATE, 
   DEXPIRE, SPATH
                                      FROM TVENDOR_DOC WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + "' ";

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_DOC มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {


                string datatohistory = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                      SCREATE, DUPDATE, SUPDATE,DEXPIRE, NVERSION
                                      FROM TVENDOR_DOC_HISTORY WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                      AND SDOCID ='" + CommonFunction.ReplaceInjection(SDOCID) + @"'
                                      AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(SDOCTYPE) + @"' 
                                      ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_DOC_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";

                    string strQuery = @"INSERT INTO TVENDOR_DOC_HISTORY(SVENDORID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_DOC_HISTORY(SVENDORID,SDOCID,SDOCVERSION,SDOCTYPE,SFILENAME,SSYSFILENAME,SDESCRIPTION
                                        ,CACTIVE,DCREATE,SCREATE,DUPDATE,SUPDATE,DEXPIRE ,SPATH,NVERSION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCID"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCVERSION"] + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDOCTYPE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SSYSFILENAME"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SDESCRIPTION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DEXPIRE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SPATH"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + ") ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    private void AddhistoryVendor(string VendorID)
    {
        string senddatatohistory = @"SELECT SVENDORID, SABBREVIATION, STEL, SFAX, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, 
                                     DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS, NCAR, NDRIVER, NEXT_CAR, NEXT_DRIVER, 
                                     NSALARYPERMONTH, NSALARYPERTRIP, NBONUS, NTEAMPERYEAR, CACTIVE, DCREATE, SCREATE, 
                                     SUPDATE, DUPDATE, CCATEGORY ,DESCRIPTION
                                     FROM TVENDOR WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'";



        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            DataTable dt = CommonFunction.Get_Data(con, senddatatohistory);

            //เช็คว่า TVendor_signer มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
            if (dt.Rows.Count > 0)
            {
                string datatohistory = @"SELECT SVENDORID, SABBREVIATION, STEL, SFAX, DSTARTPTT, NCAPITAL, DBEGINTRANSPORT, 
                                     DEXPIRETRANSPORT, DBEGIN13BIS, DEXPIRE13BIS, NCAR, NDRIVER, NEXT_CAR, NEXT_DRIVER, 
                                     NSALARYPERMONTH, NSALARYPERTRIP, NBONUS, NTEAMPERYEAR, CACTIVE, DCREATE, SCREATE, 
                                     SUPDATE, DUPDATE, CCATEGORY, NVERSION,DESCRIPTION
                                     FROM TVENDOR_HISTORY 
                                     WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'
                                     ORDER BY NVERSION DESC";

                DataTable dt2 = CommonFunction.Get_Data(con, datatohistory);
                //เช็คว่า TVendor_signer_HISTORY มีข้อมูลไหมถ้ามีก็ให้เอาไปเก็บ
                if (dt2.Rows.Count > 0)
                {
                    //ถ้าเคยมีข้อมูลแล้ว
                    int num = 0;
                    DateTime? DSTARTPTT = null;
                    DateTime? DBEGINTRANSPORT = null;
                    DateTime? DEXPIRETRANSPORT = null;
                    DateTime? DBEGIN13BIS = null;
                    DateTime? DEXPIRE13BIS = null;

                    int NCAPITAL = int.TryParse((dt.Rows[0]["NCAPITAL"] + ""), out num) ? num : 0;
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
                    {
                        DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
                    {
                        DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
                    {
                        DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
                    {
                        DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
                    {
                        DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
                    }
                    int NCAR = int.TryParse((dt.Rows[0]["NCAR"] + ""), out num) ? num : 0;
                    int NDRIVER = int.TryParse((dt.Rows[0]["NDRIVER"] + ""), out num) ? num : 0;
                    int NEXT_CAR = int.TryParse((dt.Rows[0]["NEXT_CAR"] + ""), out num) ? num : 0;
                    int NEXT_DRIVER = int.TryParse((dt.Rows[0]["NEXT_DRIVER"] + ""), out num) ? num : 0;
                    int NSALARYPERMONTH = int.TryParse((dt.Rows[0]["NSALARYPERMONTH"] + ""), out num) ? num : 0;
                    int NSALARYPERTRIP = int.TryParse((dt.Rows[0]["NSALARYPERTRIP"] + ""), out num) ? num : 0;
                    int NBONUS = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    int NTEAMPERYEAR = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    string nversion = (int.Parse(dt2.Rows[0]["NVERSION"] + "") + 1) + "";


                    string strQuery = @"INSERT INTO TVENDOR_HISTORY(SVENDORID,SABBREVIATION,STEL,SFAX,DSTARTPTT,NCAPITAL,DBEGINTRANSPORT
                                      ,DEXPIRETRANSPORT,DBEGIN13BIS,DEXPIRE13BIS,NCAR,NDRIVER,NEXT_CAR,NEXT_DRIVER,NSALARYPERMONTH,NSALARYPERTRIP
                                      ,NBONUS,NTEAMPERYEAR,CACTIVE,DCREATE,SCREATE,SUPDATE,DUPDATE,CCATEGORY,NVERSION,DESCRIPTION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["STEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFAX"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DSTARTPTT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ," + CommonFunction.ReplaceInjection(NCAPITAL + "") + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGINTRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRETRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGIN13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRE13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ," + CommonFunction.ReplaceInjection(NCAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NDRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_CAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_DRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERMONTH + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERTRIP + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NBONUS + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NTEAMPERYEAR + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months(TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months(TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CCATEGORY"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DESCRIPTION"] + "") + "') ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
                else
                {
                    //ถ้าไม่่เคยข้อมูล
                    int num = 0;
                    DateTime? DSTARTPTT = null;
                    DateTime? DBEGINTRANSPORT = null;
                    DateTime? DEXPIRETRANSPORT = null;
                    DateTime? DBEGIN13BIS = null;
                    DateTime? DEXPIRE13BIS = null;

                    int NCAPITAL = int.TryParse((dt.Rows[0]["NCAPITAL"] + ""), out num) ? num : 0;
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DSTARTPTT"] + ""))
                    {
                        DSTARTPTT = DateTime.Parse(dt.Rows[0]["DSTARTPTT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGINTRANSPORT"] + ""))
                    {
                        DBEGINTRANSPORT = DateTime.Parse(dt.Rows[0]["DBEGINTRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRETRANSPORT"] + ""))
                    {
                        DEXPIRETRANSPORT = DateTime.Parse(dt.Rows[0]["DEXPIRETRANSPORT"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DBEGIN13BIS"] + ""))
                    {
                        DBEGIN13BIS = DateTime.Parse(dt.Rows[0]["DBEGIN13BIS"] + "");
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["DEXPIRE13BIS"] + ""))
                    {
                        DEXPIRE13BIS = DateTime.Parse(dt.Rows[0]["DEXPIRE13BIS"] + "");
                    }
                    int NCAR = int.TryParse((dt.Rows[0]["NCAR"] + ""), out num) ? num : 0;
                    int NDRIVER = int.TryParse((dt.Rows[0]["NDRIVER"] + ""), out num) ? num : 0;
                    int NEXT_CAR = int.TryParse((dt.Rows[0]["NEXT_CAR"] + ""), out num) ? num : 0;
                    int NEXT_DRIVER = int.TryParse((dt.Rows[0]["NEXT_DRIVER"] + ""), out num) ? num : 0;
                    int NSALARYPERMONTH = int.TryParse((dt.Rows[0]["NSALARYPERMONTH"] + ""), out num) ? num : 0;
                    int NSALARYPERTRIP = int.TryParse((dt.Rows[0]["NSALARYPERTRIP"] + ""), out num) ? num : 0;
                    int NBONUS = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    int NTEAMPERYEAR = int.TryParse((dt.Rows[0]["NBONUS"] + ""), out num) ? num : 0;
                    string nversion = "1";
                    string strQuery = @"INSERT INTO TVENDOR_HISTORY(SVENDORID,SABBREVIATION,STEL,SFAX,DSTARTPTT,NCAPITAL,DBEGINTRANSPORT
                                      ,DEXPIRETRANSPORT,DBEGIN13BIS,DEXPIRE13BIS,NCAR,NDRIVER,NEXT_CAR,NEXT_DRIVER,NSALARYPERMONTH,NSALARYPERTRIP
                                      ,NBONUS,NTEAMPERYEAR,CACTIVE,DCREATE,SCREATE,SUPDATE,DUPDATE,CCATEGORY,NVERSION,DESCRIPTION) 
                                  VALUES('" + CommonFunction.ReplaceInjection(dt.Rows[0]["SVENDORID"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SABBREVIATION"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["STEL"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SFAX"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DSTARTPTT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ," + CommonFunction.ReplaceInjection(NCAPITAL + "") + @"
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGINTRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRETRANSPORT + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DBEGIN13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection((DEXPIRE13BIS + "").Replace("0:00:00", "00:00:00")) + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ," + CommonFunction.ReplaceInjection(NCAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NDRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_CAR + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NEXT_DRIVER + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERMONTH + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NSALARYPERTRIP + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NBONUS + "") + @"
                                  ," + CommonFunction.ReplaceInjection(NTEAMPERYEAR + "") + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CACTIVE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DCREATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SCREATE"] + "") + @"'
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["SUPDATE"] + "") + @"'
                                  ,add_months (TO_DATE('" + CommonFunction.ReplaceInjection(dt.Rows[0]["DUPDATE"] + "") + @"','dd/mm/yyyy HH24:MI:SS'),-6515 )
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["CCATEGORY"] + "") + @"'
                                  ," + CommonFunction.ReplaceInjection(nversion) + @"
                                  ,'" + CommonFunction.ReplaceInjection(dt.Rows[0]["DESCRIPTION"] + "") + "') ";
                    using (OracleCommand com = new OracleCommand(strQuery, con))
                    {
                        com.ExecuteNonQuery();
                    }
                }
            }
            else
            {

            }




        }
    }

    protected void gvwsign_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";
        string Line_no = "";
        string SDOCID = ViewState["SDOCID"] + "";
        string SDOCTYPE = ViewState["SDOCTYPE"] + "";
        string SDOCID2 = ViewState["SDOCID2"] + "";
        string SDOCTYPE2 = ViewState["SDOCTYPE2"] + "";
        string SDOCID3 = ViewState["SDOCID3"] + "";
        string SDOCTYPE3 = ViewState["SDOCTYPE3"] + "";
        string SDOCID4 = ViewState["SDOCID4"] + "";
        string SDOCTYPE4 = ViewState["SDOCTYPE4"] + "";

        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":


                string[] param = e.Args[0].Split(';');
                if (param[0] == "editgvwsign")
                {
                    Line_no = gvwsign.GetRowValues(int.Parse(param[1]), "LINE_NO") + "";
                }

                switch (param[0])
                {
                    case "New": gvwsign.AddNewRow();
                        VisibleControlUpload();
                        break;

                    #region การทำงานใน gvwsigh
                    case "Savegvw":

                        #region เก็ตคอนโทรน
                        //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                        //ASPxUploadControl uploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
                        ASPxTextBox txtName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                        ASPxTextBox txtPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
                        ASPxTextBox txtTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
                        ASPxTextBox txtTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
                        ASPxTextBox txtEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
                        ASPxTextBox txtFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
                        ASPxTextBox txtFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
                        ASPxTextBox txtLINE_NO = gvwsign.FindEditFormTemplateControl("txtLINE_NO") as ASPxTextBox;
                        #endregion

                        string Checkitem = @"SELECT SVENDORID||LINE_NO||SNAME||SPOSITION||PHONE||PHONE2||EMAIL||PICTURE||SFILENAME||SPATH  FROM TVENDOR_SIGNER
                                    WHERE SVENDORID||LINE_NO||SNAME||SPOSITION||PHONE||PHONE2||EMAIL||PICTURE||SFILENAME||SPATH = '" + CommonFunction.ReplaceInjection(VendorID) + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + CommonFunction.ReplaceInjection(txtName.Text) + CommonFunction.ReplaceInjection(txtPosition.Text) + CommonFunction.ReplaceInjection(txtTel.Text) + CommonFunction.ReplaceInjection(txtTel2.Text) + CommonFunction.ReplaceInjection(txtEmail.Text) + CommonFunction.ReplaceInjection(txtFileName5.Text) + CommonFunction.ReplaceInjection(txtFileName5.Text) + CommonFunction.ReplaceInjection(txtFilePath5.Text) + "'";



                        using (OracleConnection con = new OracleConnection(conn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }

                            DataTable chkgvw = CommonFunction.Get_Data(con, Checkitem);

                            int Numgvw = 0;
                            //เช็คว่าข้ามูบที่แสดงกับข้อมูลในเบสว่าตรงกันไหม
                            if (chkgvw.Rows.Count > 0)
                            {


                            }
                            else
                            {
                                //เช็คว่ามีคนที่บันทุกข้อมูลเป็นใครถ้ามีอยู่แล้วให้อับเดทข้อมูลและเก็บลงประวัติ แต่ถ้าไม่มีให้เซฟเข้าไป

                                string chkVendor = "";
                                if (txtLINE_NO.Text != "")
                                {
                                    chkVendor = @"SELECT * FROM TVENDOR_SIGNER
                                            WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                            AND LINE_NO ='" + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + @"'
                                            ORDER BY LINE_NO DESC";
                                }
                                else
                                {
                                    chkVendor = @"SELECT * FROM TVENDOR_SIGNER
                                            WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + @"'  
                                            ORDER BY LINE_NO DESC";
                                }
                                DataTable dt = CommonFunction.Get_Data(con, chkVendor);
                                ///เช็คว่าจะเซฟหรืออับเดท ถ้าtxtLINE_NOเป็นค่าว่างเป็นเซฟ แล้วดึงค่า LINE_NO ที่มากที่สุดมาสร้างไอดีใหม่
                                if (txtLINE_NO.Text != "")
                                {

                                }
                                else
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        Numgvw = int.Parse(dt.Rows[0]["LINE_NO"] + "") + 1;
                                    }
                                    else
                                    {
                                        Numgvw = 1;
                                    }
                                }

                                if (txtLINE_NO.Text != "")
                                {
                                    //แอดข้อมูลลง History
                                    AddhistoryVendorsigner(VendorID, txtLINE_NO.Text);
                                    //อัพเดทข้อมูล
                                    string strUpdatevendor = @"UPDATE TVENDOR_SIGNER
                                         SET SNAME='" + CommonFunction.ReplaceInjection(txtName.Text) + @"'
                                            ,SPOSITION='" + CommonFunction.ReplaceInjection(txtPosition.Text) + @"'
                                            ,PHONE='" + CommonFunction.ReplaceInjection(txtTel.Text) + @"'
                                            ,PHONE2='" + CommonFunction.ReplaceInjection(txtTel2.Text) + @"'
                                            ,EMAIL='" + CommonFunction.ReplaceInjection(txtEmail.Text) + @"'
                                            ,PICTURE='" + CommonFunction.ReplaceInjection(txtFileName5.Text) + @"'
                                            ,IS_ACTIVE='1'
                                            ,DATE_UPDATED= sysdate
                                            ,USER_UPDATED='" + CommonFunction.ReplaceInjection(USER) + @"'
                                            ,SFILENAME = '" + CommonFunction.ReplaceInjection(txtFileName5.Text) + @"'
                                            ,SPATH = '" + CommonFunction.ReplaceInjection(txtFilePath5.Text) + @"'
                                         WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "' AND LINE_NO = '" + CommonFunction.ReplaceInjection(txtLINE_NO.Text) + "'";

                                    using (OracleCommand com = new OracleCommand(strUpdatevendor, con))
                                    {
                                        com.ExecuteNonQuery();
                                    }

                                }
                                else
                                {
                                    //ถ้าไม่มีข้อมูลจะบันทึกทันที
                                    string strQuery = @"INSERT INTO TVENDOR_SIGNER(SVENDORID,LINE_NO,SNAME,SPOSITION,PHONE,PHONE2,EMAIL,PICTURE,IS_ACTIVE,DATE_CREATED,USER_CREATED,SFILENAME,SPATH) 
                                       VALUES('" + CommonFunction.ReplaceInjection(VendorID) + "'," + CommonFunction.ReplaceInjection(Numgvw + "") + ",'" + CommonFunction.ReplaceInjection(txtName.Text) + "','" + CommonFunction.ReplaceInjection(txtPosition.Text) + "','" + CommonFunction.ReplaceInjection(txtTel.Text) + "','" + CommonFunction.ReplaceInjection(txtTel2.Text) + "','" + CommonFunction.ReplaceInjection(txtEmail.Text) + "','" + CommonFunction.ReplaceInjection(txtFileName5.Text) + "','1',SYSDATE,'" + CommonFunction.ReplaceInjection(USER) + "','" + CommonFunction.ReplaceInjection(txtFileName5.Text) + "','" + CommonFunction.ReplaceInjection(txtFilePath5.Text) + "') ";

                                    using (OracleCommand com = new OracleCommand(strQuery, con))
                                    {
                                        com.ExecuteNonQuery();
                                    }

                                }
                            }
                            con.Close();
                            gvwsign.CancelEdit();
                            Listgvwsign();
                            gvwsign.DataBind();
                        }
                        break;


                    //เมื่อกดแก้ไขในข้อมูลผู้ลงนาม
                    case "editgvwsign":

                        int index = int.Parse(param[1]);
                        gvwsign.StartEdit(index);

                        //ค้นหาข้อมูลโดยใช้ VendorId และ LINE_NO เพื่อเซ็ตค่า
                        string strsetdata = @"SELECT SVENDORID, LINE_NO, SNAME, SPOSITION, PHONE, PHONE2, EMAIL, PICTURE, IS_ACTIVE, 
                                      DATE_CREATED, USER_CREATED, DATE_UPDATED, USER_UPDATED, SFILENAME, SPATH
                                      FROM TVENDOR_SIGNER WHERE SVENDORID = '" + CommonFunction.ReplaceInjection(VendorID) + "'  AND LINE_NO ='" + CommonFunction.ReplaceInjection(Line_no + "") + "'";



                        using (OracleConnection con = new OracleConnection(conn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }
                            #region เก็ตคอนโทรน
                            //ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                            ASPxUploadControl suploader5 = gvwsign.FindEditFormTemplateControl("uploader5") as ASPxUploadControl;
                            ASPxTextBox txtsName = gvwsign.FindEditFormTemplateControl("txtName") as ASPxTextBox;
                            ASPxTextBox txtsPosition = gvwsign.FindEditFormTemplateControl("txtPosition") as ASPxTextBox;
                            ASPxTextBox txtsTel = gvwsign.FindEditFormTemplateControl("txtTel") as ASPxTextBox;
                            ASPxTextBox txtsTel2 = gvwsign.FindEditFormTemplateControl("txtTel2") as ASPxTextBox;
                            ASPxTextBox txtsEmail = gvwsign.FindEditFormTemplateControl("txtEmail") as ASPxTextBox;
                            ASPxTextBox txtsFilePath5 = gvwsign.FindEditFormTemplateControl("txtFilePath5") as ASPxTextBox;
                            ASPxTextBox txtsFileName5 = gvwsign.FindEditFormTemplateControl("txtFileName5") as ASPxTextBox;
                            ASPxTextBox txtsLINE_NO = gvwsign.FindEditFormTemplateControl("txtLINE_NO") as ASPxTextBox;
                            ASPxButton btnView5 = gvwsign.FindEditFormTemplateControl("btnView5") as ASPxButton;
                            ASPxButton btnDelFile5 = gvwsign.FindEditFormTemplateControl("btnDelFile5") as ASPxButton;
                            #endregion


                            DataTable data = CommonFunction.Get_Data(con, strsetdata);
                            if (data.Rows.Count > 0)
                            {
                                txtsLINE_NO.Text = data.Rows[0]["LINE_NO"] + "";
                                txtsName.Text = data.Rows[0]["SNAME"] + "";
                                txtsPosition.Text = data.Rows[0]["SPOSITION"] + "";
                                txtsTel.Text = data.Rows[0]["PHONE"] + "";
                                txtsTel2.Text = data.Rows[0]["PHONE2"] + "";
                                txtsEmail.Text = data.Rows[0]["EMAIL"] + "";

                                if (!string.IsNullOrEmpty(data.Rows[0]["SPATH"] + ""))
                                {

                                    txtsFileName5.Text = data.Rows[0]["SFILENAME"] + "";
                                    txtsFilePath5.Text = data.Rows[0]["SPATH"] + "";
                                    suploader5.ClientVisible = false;
                                    txtsFilePath5.ClientVisible = false;
                                    btnView5.Enabled = true;
                                    btnDelFile5.Enabled = true;
                                    txtsFileName5.Enabled = false;
                                    txtsFileName5.ClientVisible = true;
                                    btnView5.ClientSideEvents.Click = "function(s,e){window.open('openFile.aspx?str='+ " + data.Rows[0]["SPATH"] + "" + ");}";
                                }
                                else
                                {
                                    suploader5.ClientVisible = true;
                                    txtsFilePath5.ClientVisible = false;
                                    btnView5.ClientVisible = false;
                                    btnDelFile5.ClientVisible = false;
                                }

                            }
                            else
                            {

                            }
                            con.Close();
                        }
                        break;



                    case "deleteFilegvwsign":
                        string FilePathgvwsign = param[1];
                        //string view = param[4];


                        ASPxTextBox txtFileNamegvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "txtFileNamegvwsign") as ASPxTextBox;
                        ASPxTextBox txtFilePathgvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "txtFilePathgvwsign") as ASPxTextBox;
                        ASPxButton btnViewgvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "btnViewgvwsign") as ASPxButton;
                        ASPxButton btnDelFilegvwsign = gvwsign.FindRowCellTemplateControl(int.Parse(param[3]), null, "btnDelFilegvwsign") as ASPxButton;


                        if (File.Exists(Server.MapPath("./") + FilePathgvwsign.Replace("/", "\\")))
                        {
                            File.Delete(Server.MapPath("./") + FilePathgvwsign.Replace("/", "\\"));
                        }

                        string cNogvwsign = param[2];
                        if (cNogvwsign == "1")
                        {
                            txtFilePathgvwsign.Text = "";
                            txtFileNamegvwsign.Text = "";
                            btnViewgvwsign.Visible = false;
                            btnDelFilegvwsign.Visible = false;
                        }

                        using (OracleConnection con = new OracleConnection(conn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }

                            int Numgvw = gvwsign.VisibleRowCount + 1;

                            string SVENDORID = gvwsign.GetRowValues(int.Parse(param[3]), "SVENDORID").ToString();
                            string LINE_NO = gvwsign.GetRowValues(int.Parse(param[3]), "LINE_NO").ToString();

                            string strQuery = @"UPDATE TVENDOR_SIGNER
SET PICTURE='" + CommonFunction.ReplaceInjection(txtFileNamegvwsign.Text) + @"',SFILENAME='" + CommonFunction.ReplaceInjection(txtFileNamegvwsign.Text) + @"',SPATH='" + CommonFunction.ReplaceInjection(txtFilePathgvwsign.Text) + @"'
WHERE SVENDORID='" + SVENDORID + "' AND LINE_NO = '" + LINE_NO + "' ";

                            using (OracleCommand com = new OracleCommand(strQuery, con))
                            {
                                com.ExecuteNonQuery();
                            }
                            con.Close();
                        }
                        break;


                    case "Cancelgvw": gvwsign.CancelEdit();
                        break;


                    case "deletegvwsign":
                        //int index = int.Parse(param[1]);
                        string DSVENDORID = gvwsign.GetRowValues(int.Parse(param[1]), "SVENDORID").ToString();
                        string DLINE_NO = gvwsign.GetRowValues(int.Parse(param[1]), "LINE_NO").ToString();

                        using (OracleConnection con = new OracleConnection(conn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }

                            string steDelete = @"UPDATE TVENDOR_SIGNER SET IS_ACTIVE='0' WHERE SVENDORID='" + DSVENDORID + "' AND LINE_NO = '" + DLINE_NO + "' ";
                            using (OracleCommand com = new OracleCommand(steDelete, con))
                            {
                                com.ExecuteNonQuery();
                            }
                            con.Close();
                        }
                        Listgvwsign();
                        gvwsign.DataBind();

                        break;

                    #endregion
                }


                break;





        }

    }

    protected void gvwdoc4_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";


        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":


                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {

                    case "Newgvwdoc4": gvwdoc4.AddNewRow();
                        gvwdoc4.Visible = true;


                        //VisibleControlUpload();
                        break;
                    case "deleteUpload4":
                        dynamic delete = gvwdoc4.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SDOCTYPE");

                        AddhistoryVendorDoc(delete[0] + "", delete[1] + "", delete[2] + "");

                        string UpdateUpload4 = @"UPDATE TVENDOR_DOC
                                            SET CACTIVE=:CACTIVE
                                            WHERE SVENDORID ='" + CommonFunction.ReplaceInjection(delete[0] + "") + @"' 
                                            AND SDOCID ='" + CommonFunction.ReplaceInjection(delete[1] + "") + @"'
                                            AND SDOCTYPE ='" + CommonFunction.ReplaceInjection(delete[2] + "") + "'";

                        using (OracleConnection con = new OracleConnection(conn))
                        {

                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            else
                            {

                            }
                            using (OracleCommand com1 = new OracleCommand(UpdateUpload4, con))
                            {

                                com1.Parameters.Clear();
                                com1.Parameters.Add(":CACTIVE", OracleType.Char).Value = "0";
                                com1.ExecuteNonQuery();
                            }
                        }


                        int index = int.Parse(param[3]);
                        listGrid.RemoveAt(index);
                        Listgvwdoc4();
                        break;
                }


                break;





        }

    }

    void Listgvwdoc4()
    {
        string SVENDORID = ViewState["setdata"] + "";

        //เช็คว่าค่าในList กับในดาต้าเบสเท่ากันไหม ถ้าเมท่ากันคือSaveแล้วให้Setdata ใหม่ แต่ถ้าไม่เท่ากันสแดงว่ายังไม่ได้เซฟให้เอาลิสมาริสข้อมูลใหม่
        if (txtUploadchk4.Text != "S")
        {
            string chkDocInDB = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                  SCREATE, DUPDATE, SUPDATE,  DEXPIRE, SPATH FROM TVENDOR_DOC 
                                  WHERE  SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SDOCTYPE = '4'";
            DataTable chkData = new DataTable();
            chkData = CommonFunction.Get_Data(conn, chkDocInDB);
            var dblist = listGrid.Where(w => w.SDOCTYPE == "4").ToList();
            if (listGrid.Count > 0)
            {//เช็คว่าข้อมูลมีในเบสไหม




                if (dblist.Count > 0)
                {//เช็คว่าในลิสมีข้อมูลไหม ถ้าในเบสมีข้อมูลแต่ถ้าใน List ไม่มี คือบายข้อมูลผิด

                    gvwdoc4.DataSource = dblist;
                    gvwdoc4.DataBind();

                }
                else
                {
                    gvwdoc4.DataBind();
                    gvwdoc4.Visible = true;
                    gvwdoc4.AddNewRow();

                }

            }
            else
            {
                //if (ViewState["nosavedelete4"] + "" == "deletenotsave")//เช็คว่าลบแล้วแต่ยังไม่ได้เซฟ
                //{
                var dblist2 = listGrid.ToList();
                if (dblist2.Count > 0)
                {//เช็คว่าข้อมูลมีในเบสไหม
                    gvwdoc4.DataSource = dblist2;
                    gvwdoc4.DataBind();




                }
                else
                {
                    gvwdoc4.DataSource = dblist2;
                    gvwdoc4.DataBind();
                    gvwdoc4.Visible = true;
                    gvwdoc4.AddNewRow();

                }

                //}
                //else
                //{
                //    if (chkData.Rows.Count > 0)//เช็คจากข้อมูลในเบสจริงว่ามีจริงถ้ามีจริงให้ลิสแต่ถ้า
                //    {
                //        Setdata();
                //    }
                //    else
                //    {
                //        gvwdoc4.DataSource = dblist;
                //        gvwdoc4.DataBind();
                //        gvwdoc4.Visible = true;
                //        gvwdoc4.AddNewRow();
                //    }
                //}

            }
        }

    }

    void Listgvwother()
    {
        string SVENDORID = ViewState["setdata"] + "";

        //เช็คว่าค่าในList กับในดาต้าเบสเท่ากันไหม ถ้าเมท่ากันคือSaveแล้วให้Setdata ใหม่ แต่ถ้าไม่เท่ากันสแดงว่ายังไม่ได้เซฟให้เอาลิสมาริสข้อมูลใหม่
        if (txtUploadother.Text != "S")
        {
            string chkDocInDB = @"SELECT SVENDORID, SDOCID, SDOCVERSION, SDOCTYPE, SFILENAME, SSYSFILENAME, SDESCRIPTION, CACTIVE, DCREATE, 
                                  SCREATE, DUPDATE, SUPDATE,  DEXPIRE, SPATH FROM TVENDOR_DOC 
                                  WHERE  SVENDORID='" + CommonFunction.ReplaceInjection(SVENDORID) + "' AND CACTIVE = '1' AND SDOCTYPE = '5'";
            DataTable chkData = new DataTable();
            chkData = CommonFunction.Get_Data(conn, chkDocInDB);
            var dblist = listGridother.Where(w => w.SDOCTYPE == "5").ToList();
            if (listGridother.Count > 0)
            {//เช็คว่าข้อมูลมีในเบสไหม




                if (dblist.Count > 0)
                {//เช็คว่าในลิสมีข้อมูลไหม ถ้าในเบสมีข้อมูลแต่ถ้าใน List ไม่มี คือบายข้อมูลผิด

                    gvwother.DataSource = dblist;
                    gvwother.DataBind();

                }
                else
                {
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

            }
            else
            {
                //if (ViewState["nosavedeleteother"] + "" == "deletenotsave")//เช็คว่าลบแล้วแต่ยังไม่ได้เซฟ
                //{
                var dblist2 = listGridother.ToList();
                if (dblist2.Count > 0)
                {//เช็คว่าข้อมูลมีในเบสไหม
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();




                }
                else
                {
                    gvwother.DataSource = dblist2;
                    gvwother.DataBind();
                    gvwother.Visible = true;
                    gvwother.AddNewRow();

                }

                //}
                //else
                //{
                //    if (chkData.Rows.Count > 0)//เช็คจากข้อมูลในเบสจริงว่ามีจริงถ้ามีจริงให้ลิสแต่ถ้า
                //    {
                //        Setdata();
                //    }
                //    else
                //    {
                //        gvwother.DataSource = dblist;
                //        gvwother.DataBind();
                //        gvwother.Visible = true;
                //        gvwother.AddNewRow();
                //    }
                //}

            }
        }

    }

    void Deletefilegvwsigh()
    {


        var dblist = Filegvwsigh.ToList();
        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }
            foreach (var item in dblist)
            {
                string SVENDORID = item.SVENDORID;
                string LINE_NO = item.LINE_NO;
                int index = int.Parse(item.Index);

                AddhistoryVendorsigner(SVENDORID, LINE_NO);
                string HistoryPath = string.Format(HistoryDirectory4, Session["SVDID"] + "", "uploaderDoc4", Session["UserID"] + "");
                UploadFile2History(item.SPATH, HistoryPath, item.SSYSFILENAME);
                //string SVENDORID = gvwsign.GetRowValues(index, "SVENDORID").ToString();
                //string LINE_NO = gvwsign.GetRowValues(index, "LINE_NO").ToString();

                string strQuery = @"UPDATE TVENDOR_SIGNER
SET PICTURE='" + CommonFunction.ReplaceInjection("") + @"'
,SFILENAME='" + CommonFunction.ReplaceInjection("") + @"'
,SPATH='" + CommonFunction.ReplaceInjection("") + @"'
,SSYSFILENAME='" + CommonFunction.ReplaceInjection("") + @"'
WHERE SVENDORID='" + SVENDORID + "' AND LINE_NO = '" + LINE_NO + "' ";

                using (OracleCommand com = new OracleCommand(strQuery, con))
                {
                    com.ExecuteNonQuery();
                }
            }



            // int Numgvw = gvwsign.VisibleRowCount + 1;


            con.Close();
        }
    }

    void Deletedatagvwsigh()
    {

        using (OracleConnection con = new OracleConnection(conn))
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            else
            {

            }

            foreach (var item in DeleteGridsigh.ToList())
            {

                string steDelete = @"UPDATE TVENDOR_SIGNER SET IS_ACTIVE='0' WHERE SVENDORID='" + item.SVENDORID + "' AND LINE_NO = '" + item.LINE_NO + "' ";
                using (OracleCommand com = new OracleCommand(steDelete, con))
                {
                    com.ExecuteNonQuery();
                }
            }
            con.Close();
        }
    }

    void deleteFiletemp()
    {
        string VendorID = Session["SVDID"] + "";


        string deletepath = (Server.MapPath("./") + "UploadFile/VendorDoc/Temp/" + CommonFunction.ReplaceInjection(VendorID) + "").Replace("/", "\\");
        if (Directory.Exists(deletepath))
        {
            try
            {
                System.IO.Directory.Delete(deletepath.Replace("/", "\\"), true);
            }
            catch
            {

            }
        }



    }

    protected void gvwother_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
    {

        string VendorID = ViewState["setdata"] + "";
        string USER = Session["UserID"] + "";


        switch (e.CallbackName)
        {

            case "CUSTOMCALLBACK":


                string[] param = e.Args[0].Split(';');

                switch (param[0])
                {

                    case "Newgvwother": gvwother.AddNewRow();
                        gvwother.Visible = true;


                        //VisibleControlUpload();
                        break;
                    case "deleteUploadother":
                        dynamic delete = gvwother.GetRowValues(int.Parse(param[3] + ""), "SVENDORID", "SDOCID", "SDOCTYPE", "SPATH", "SSYSFILENAME");

                        try
                        {
                            DeleteGrid.Add(new DeleteItem
                            {
                                SVENDORID = delete[0],
                                SDOCID = delete[1],
                                SDOCTYPE = delete[2],
                                SPATHALL = delete[3] + delete[4]
                            });

                            int index = int.Parse(param[3]);
                            listGridother.RemoveAt(index);
                            ViewState["nosavedeleteother"] = "deletenotsave";
                            Listgvwother();
                        }
                        catch
                        {

                        }

                        break;
                }


                break;





        }

    }

    private void LogUser(string MENUID, string TYPE, string DESCEIPTION, string REFERENTID)
    {
        UserTrace trace = new UserTrace(this, conn);
        trace.SUID = Session["UserID"] + "";
        trace.SMENUID = MENUID;
        trace.SCREATE = Session["UserID"] + "";
        trace.STYPE = TYPE;
        trace.SDESCRIPTION = DESCEIPTION;
        trace.SREFERENTID = REFERENTID;
        trace.Insert();
    }

    [Serializable]
    class ListGrid
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }

    [Serializable]
    class ListGridother
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }

    [Serializable]
    class ListGridDoc
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public int SDOCVERSION { get; set; }
        public string SDOCTYPE { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
        public string SDESCRIPTION { get; set; }
        public string CACTIVE { get; set; }
        public DateTime? DCREATE { get; set; }
        public string SCREATE { get; set; }
        public DateTime? DUPDATE { get; set; }
        public string SUPDATE { get; set; }
        public DateTime? DEXPIRE { get; set; }
        public string SPATH { get; set; }
        public string STRUEPATH { get; set; }
        public string NEWPICTRUEFLAG { get; set; }
        public int INDEX { get; set; }
    }
    [Serializable]
    class ListGridsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }
        public string SNAME { get; set; }
        public string SPOSITION { get; set; }
        public string PHONE { get; set; }
        public string PHONE2 { get; set; }
        public string EMAIL { get; set; }
        public string PICTURE { get; set; }
        public string IS_ACTIVE { get; set; }
        public DateTime? DATE_CREATED { get; set; }
        public string USER_CREATED { get; set; }
        public DateTime? DATE_UPDATED { get; set; }
        public string USER_UPDATED { get; set; }
        public string SFILENAME { get; set; }
        public string SPATH { get; set; }
        public string SSYSFILENAME { get; set; }

    }

    [Serializable]
    class DeleteItem
    {
        public string SVENDORID { get; set; }
        public string SDOCID { get; set; }
        public string SDOCTYPE { get; set; }
        public string SPATHALL { get; set; }

    }

    [Serializable]
    class DeleteFilegvwsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }
        public string Index { get; set; }
        public string SPATH { get; set; }
        public string SFILENAME { get; set; }
        public string SSYSFILENAME { get; set; }
    }

    [Serializable]
    class Deletegvwsigh
    {
        public string SVENDORID { get; set; }
        public string LINE_NO { get; set; }

    }


    [Serializable]
    class gvwsighListColumn
    {
        public string Index { get; set; }
    }

}