﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="Report.aspx.cs" Inherits="home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
    <dx:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" HeaderText="รายงาน รข.">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent4" runat="server" SupportsDisabledAttribute="True">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="15%"></td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="300px" HeaderText="รายงานทั่วไป">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportGrade.aspx'>หนังสือแจ้งบริษัท</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportSuppliant.aspx'>สรุปข้อมูลการขออุทธรณ์</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportReducePoint.aspx'>สรุปข้อมูลการผิดสัญญา</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportExpireEmployee.aspx'>รายงานค้นหา พขร. ตามอายุ</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportExpireResourceMonitoring.aspx'>รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportExpireResource.aspx'>รายงานค้นหารถตามอายุ</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='DetailDefectMonitoring.aspx'>รายงานสำหรับติดตามปัญหาการขนส่ง</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportQuarterLetter.aspx'>รายงานแจ้งผลการประเมินประจำไตรมาส</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='Defect_Point_Quarter.aspx'>รายงานประเมินผลการทำงาน</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" Width="300px" HeaderText="รายงานเชิงวิเคราะห์">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent2" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportDemandPerformance.aspx'>รายงาน DemandPerformance</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='Reportsolvepurchase.aspx'>รายงาน SolvePurchase</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportDefectPercentage.aspx'>รายงาน Defect Percentage</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" Width="300px" HeaderText="รายการปฎิบัติการ">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent3" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="reportsDetailConfirmContact.aspx">รายงานยืนยันรถตามสัญญา</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="reportDetailPlanning.aspx">รายงานการจัดแผนงาน</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="reportDetailConfirmPlan.aspx">รายงานยืนยันตามแผนงาน</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="reportMonthly.aspx">รายงานการขนส่ง</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td width="15%"></td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <br />
    <dx:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" HeaderText="รายงาน มว.">
        <PanelCollection>
            <dx:PanelContent ID="PanelContent5" runat="server" SupportsDisabledAttribute="True">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="15%"></td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" Width="300px" HeaderText="Operation Report">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent6" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportAlertServices.aspx">ใบงาน แจ้งเตือนเข้ารับบริการ (ตารางงานประจำวัน)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportBillandCharge.aspx">ชำระค่าธรรมเนียม (ใบเรียกเก็บ)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportOperationMonths.aspx">สถานะการรับบริการวันนั้น</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportResultWater.aspx">ใบรับรองผลสอบเทียบวัดน้ำ (Certificate)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportTeabpans.aspx">ใบเทียบแป้น</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportWatnumcause.aspx">รายงานรถวัดน้ำไม่ผ่าน แจ้ง รข. (แยกบริษัท)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportMonthslyIncomeDetail.aspx">รายได้แจกแจงรายคัน</a></li></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportDateProcess.aspx">รายงานเวลาเข้ารับบริการแต่ละขั้นตอน</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportStatusInMonths.aspx">Daily Status</a></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportCauseCarNotComplete.aspx">รายงานรถไม่ผ่าน รวมสาเหตุ(ภาพรวม) แยกบริษัท</a></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportNotEndProcess.aspx">รายงานรถที่ยังไม่ปิดงาน (จ่าย/ไม่จ่าย)</a>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportNotPayFee.aspx">รถค้างค่าธรรมเนียมเพิ่ม</a>
                                                </td>
                                                <td></td>
                                            </tr>
                                               <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportCompareDate.aspx">รายงานเปรียบเทียบระยะเวลายื่นคำขอ</a>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportCarRequest.aspx">จำนวนรถตามคำขอ</a>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel7" runat="server" Width="300px" HeaderText="Completely reports">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent7" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='#'>ใบเทียบแป้น</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportCheckVeh_His.aspx'>รายงานผลตรวจสอบรายคัน ประวัติเก่า</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='#'>รายได้ รายจ่าย กำไร(ตาราง/กราฟ)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportPerformance.aspx'>รายงานสรุปผลการปฏิบัติงาน (รายเดือน)</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='ReportVendorInfo.aspx' title="อายุ กำหนดวัดน้ำ ผู้ขนส่ง">รายงานข้อมูลผู้ขนส่ง
                                                        ประเภท บริษัท ...</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href='#'>Capacity รวม</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td style="width: 300px; vertical-align: top;">
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel8" runat="server" Width="300px" HeaderText="Management reports">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent8" runat="server" SupportsDisabledAttribute="True">
                                        <table width="100%">
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportTruckWaterExp.aspx">รายงานจำนวนรถหมดอายุวัดน้ำ</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <li><a href="ReportTruckWaterMPY.aspx">รถวัดน้ำเกิน 1 ครั้ง/ปี สาเหตุ</a></li>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>&nbsp; </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </td>
                        <td width="15%"></td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <!--
   <table width="100%" cellpadding="1" cellspacing="3">
     <tr>
       <td></td>
       <td style=" font-size:18px">&nbsp;</td>
     </tr>
     <tr>
       <td width="10%"></td>
       <td width="40%"><li><a href='ReportGrade.aspx' >หนังสือแจ้งบริษัท</a></li></td>
       <td width="10%"></td>
       <td width="40%"><li><a href='ReportReducePoint.aspx' >สรุปข้อมูลการผิดสัญญา</a></li></td>
     </tr>
      <tr>
       <td></td>
       <td><li><a href='ReportSuppliant.aspx' >สรุปข้อมูลการขออุทธรณ์</a></li></td>
       <td></td>
       <td><li><a href="reportsDetailConfirmContact.aspx">รายงานยืนยันรถตามสัญญา</a></li></td>
     </tr>
       
      <tr>
       <td>&nbsp;</td>
       <td>
           <li><a href="reportDetailPlanning.aspx">รายงานการจัดแผนงาน</a></li>
          </td>
       <td>&nbsp;</td>
       <td>
           <li><a href="reportDetailConfirmPlan.aspx">รายงานยืนยันตามแผนงาน</a></li>
          </td>
     </tr>
     <tr>
        <td></td>
       <td><li><a href='ReportExpireEmployee.aspx' >รายงานค้นหา พขร. ตามอายุ</a></li></td>
       <td></td>
       <td><li><a href='ReportExpireResourceMonitoring.aspx' >รายงานรถที่ใกล้หมดสภาพการใช้งาน</a></li></td>
     </tr>
     <tr>
        <td></td>
        <td><li><a href='ReportExpireResource.aspx' >รายงานค้นหารถตามอายุ</a></li></td>
        <td></td>
        <td><li><a href='DetailDefectMonitoring.aspx' >รายงานสำหรับติดตามปัญหาการขนส่ง</a></li>  </td>
     </tr>
     <tr>
       <td></td><td><li><a href='ReportDemandPerformance.aspx' >รายงาน DemandPerformance</a></li></td><td></td>
       <td><li><a href='Reportsolvepurchase.aspx' >รายงาน SolvePurchase</a></li></td>
     </tr>
     <tr>
       <td></td><td><li><a href='ReportDefectPercentage.aspx' >รายงาน Defect Percentage</a></li></td> 
         <td></td>
        <td><li><a href='ReportQuarterLetter.aspx' >รายงานแจ้งผลการประเมินประจำไตรมาส</a></li>  </td>
     </tr>
      <tr>
       <td></td><td><li><a href='Defect_Point_Quarter.aspx' >รายงานประเมินผลการทำงาน</a></li></td> 
         <td></td>
        <td> </td>
     </tr>
   </table>
   -->
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" Runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" Runat="Server">
</asp:Content>--%>
