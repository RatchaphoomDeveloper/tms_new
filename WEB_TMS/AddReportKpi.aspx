﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true" CodeFile="AddReportKpi.aspx.cs" Inherits="AddReportKpi" %>
<%@ Register Src="~/UserControl/ModelPopup.ascx" TagPrefix="uc1" TagName="ModelPopup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" Runat="Server">
        <script src="JQuery/jquery.validate.min.js" type="text/javascript"></script>
        <script src="JQuery/messages_th.min.js" type="text/javascript"></script>
    <style type="text/css">
    th
    {
        text-align:center;
    }
    .error
    {
        color: red;
    }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery.validator.addMethod("CheckChar", function (value, element) {
                //^[0-9]*\.*[0-9]*\-*[0-9]*\.*[0-9]*
                if (/(?:\d*\.*\-*)?\d+/.test(value)) {
                    return true;  // FAIL validation when REGEX matches
                } else {
                    return false;   // PASS validation otherwise
                };
            }, "เฉพาะตัวเลข จุดทศนิยม ขีดกลาง");
            $("#<%=cmdSave.ClientID%>").click(function () {
                $('form').validate();
                //Add a custom class to your name mangled input and add rules like this
                $('.required').rules('add', {
                    required: true
                });
                $('.number').rules('add',
                    {
                        number: true
                    });
                $('.CheckChar').rules('add',
                    {
                        CheckChar: true
                    });
            });
            //เวลามีการอนุมัติหรือกลับสู่หน้าหลัก
            $(".Delrow,#cph_Main_mpConfirmBack_btnSave").click(function () {
                $("form").validate().cancelSubmit = true;
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cph_Main" Runat="Server">
    <br />
    <h5>รายละเอียดแบบฟอร์ม KPI</h5>
    <div class="panel panel-info">
        <div class="panel-heading">
            <i class="fa fa-table"></i>แบบฟอร์ม KPI
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            ชื่อแบบประเมิน :
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtNameForm" runat="server" CssClass="form-control required" MaxLength="99"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            รายละเอียดแบบประเมิน :
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control required" MaxLength="99"></asp:TextBox>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2 text-right">
                            สถานะ :
                        </div>
                        <div class="col-md-4">
                            <asp:RadioButtonList ID="rdostatus" runat="server" CssClass="required" RepeatDirection="Horizontal" >
                                <asp:ListItem Text="ใช้งาน" Selected="True" Value="1" />
                                <asp:ListItem Text="ไม่ใช้งาน" Value="0" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p>&nbsp;</p>
                <div class="col-md-12">
                    <asp:GridView ID="dgvnewkpi" runat="server" Width="90%" HeaderStyle-HorizontalAlign="Center" CellPadding="4" GridLines="None" CssClass="table table-hover" HeaderStyle-CssClass="GridColorHeader"
                        ShowHeaderWhenEmpty="true" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="White" OnRowDataBound="dgvnewkpi_RowDataBound"
                        HorizontalAlign="Center" AutoGenerateColumns="false" EmptyDataText="[ ไม่มีข้อมูล ]">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="ลำดับ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                     <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ชื่อหัวข้อประเมิน">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                     <asp:TextBox ID="NameReport" runat="server" CssClass="form-control required" MaxLength="99" TextMode="MultiLine"></asp:TextBox>   
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ความถี่" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_frequency" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="วิธีคำนวณผลรวม" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_sum" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="1" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="Id1" runat="server" CssClass="form-control required CheckChar" MaxLength="15"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="2" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="Id2" runat="server" CssClass="form-control required CheckChar" MaxLength="15"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="3" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="Id3" runat="server" CssClass="form-control required CheckChar" MaxLength="15"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="4" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="Id4" runat="server" CssClass="form-control required CheckChar" MaxLength="15"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="5" ItemStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="Id5" runat="server" CssClass="form-control required CheckChar" MaxLength="15"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weight <br/> ผลรวมไม่เกิน 100" ItemStyle-Width="8%">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control required number" MaxLength="3"></asp:TextBox>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="การจัดการ">
                                <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Button ID="Delrow" runat="server"  Text="ลบหัวข้อ" CommandArgument="<%# Container.DataItemIndex %>" CssClass="btn btn-info Delrow" OnClick="Delrow_Click"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>
                            <EmptyDataRowStyle HorizontalAlign="Center" ForeColor="White"></EmptyDataRowStyle>
                    </asp:GridView>
                </div>
                <div class="col-md-12">
                    <asp:Button runat="server" ID="ADD_Topic" CssClass="btn btn-success" Text="เพิ่มหัวข้อ" OnClick="ADD_Topic_Click"/>
                </div>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <div class="col-md-12">
                    <div class="col-md-offset-4 col-md-2 text-right row">
                        <asp:Button runat="server" ID="cmdSave" Width="150px" ClientIDMode="Static" CssClass="btn btn-primary" Text="บันทึก" OnClick="cmdSave_Click" />
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <input id="btnBack" type="button" value="ยกเลิก" data-toggle="modal" data-target="#ModalConfirmBack" class="btn btn-md bth-hover btn-danger" Style="width: 120px;" />
                    </div>
                    </div>
                </div>
            </div>
        </div> 
        <uc1:ModelPopup runat="server" ID="mpConfirmBack" IDModel="ModalConfirmBack" IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="mpConfirmBack_ClickOK" TextTitle="ยืนยันการย้อนกลับ" TextDetail="คุณต้องการย้อนกลับใช่หรือไม่ ?" />       
    </div>
</asp:Content>


