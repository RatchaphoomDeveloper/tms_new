﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;

public partial class ContractTied : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DrowDownList();
            string str = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(str))
            {
                string[] QueryString = STCrypt.DecryptURL(str);
                string id = QueryString[0];
                hidSCONTRACTID.Value = id;
                DataTable dt = ContractTiedBLL.Instance.MContractSelect(string.Empty, string.Empty, string.Empty, id);
                BindData(dt);
            }
        }
        else
        {
            cmbVendor.DataSource = ViewState["DataTVendorSap"];
            cmbVendor.DataBind();
        }
    }
    private void DrowDownList()
    {
        ddlWordGroup.DataTextField = "NAME";
        ddlWordGroup.DataValueField = "ID";
        ddlWordGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWordGroup.DataBind();
        ddlWordGroup.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มงาน",
            Value = "0"
        });

        //ddlGroups.DataTextField = "NAME";
        //ddlGroups.DataValueField = "ID";
        //ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect();
        //ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });

        ViewState["DataTVendorSap"] = VendorBLL.Instance.TVendorSapSelect();
        cmbVendor.DataSource = ViewState["DataTVendorSap"];
        cmbVendor.DataBind();
    }
    protected void mpConfirmSave_ClickOK(object sender, EventArgs e)
    {
        try
        {

            string mass = Validate();
            if (!string.IsNullOrEmpty(mass))
            {
                alertFail(mass);
                return;
            }
            DataTable dt = ContractTiedBLL.Instance.MContractSave(hidSCONTRACTID.Value,txtSCONTRACTNO.Text, ddlGroups.SelectedValue, cmbVendor.Value + string.Empty);
            if (dt.Rows[0]["MESSAGE"] == DBNull.Value)
            {
                alertSuccess("บันทึกสำเร็จ", "ContractTied_Lst.aspx");
            }
            else if ((dt.Rows[0]["MESSAGE"] + "").Trim() == "HAVE")
            {
                alertFail("สัญญา : " + txtSCONTRACTNO.Text + " มีอยู่แล้วในระบบ");
            }
        }
        catch (Exception ex)
        {
            alertFail("บันทึกไม่สำเร็จ : " + ex.Message);
        }
    }
    protected void mpConfirmBack_ClickOK(object sender, EventArgs e)
    {
        Response.Redirect("~/ContractTied_Lst.aspx");
    }

    private void BindData(DataTable dt)
    {
        foreach (DataRow item in dt.Rows)
        {
            hidSCONTRACTID.Value = item["ID"] + string.Empty;
            txtSCONTRACTNO.Text = item["NAME"] + string.Empty;
            ddlWordGroup.SelectedValue = item["WORKGROUPID"] + string.Empty;
            ddlGroupsDataBind(ddlWordGroup.SelectedValue);
            ddlGroups.SelectedValue = item["GROUPID"] + string.Empty;
            cmbVendor.Value = item["SVENDORID"] + string.Empty;
            break;
        }
    }
    protected void ddlWordGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWordGroup.SelectedValue);
    }

    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroups.DataTextField = "NAME";
        ddlGroups.DataValueField = "ID";
        ddlGroups.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroups.DataBind();
        ddlGroups.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
    }
    #region Validate
    private string Validate()
    {
        string mess = string.Empty;
        try
        {
            if (string.IsNullOrEmpty(txtSCONTRACTNO.Text))
            {
                mess = "กรุณาป้อน สัญญา !!!<br/>";
                return mess;
            }
            if (ddlWordGroup.SelectedIndex == 0)
                mess += "กรุณาเลือก กลุ่มงาน !!!<br/>";
            if (ddlGroups.SelectedIndex == 0)
                mess += "กรุณาเลือก กลุ่มที่ !!!<br/>";
            if (string.IsNullOrEmpty(cmbVendor.Value + string.Empty))
                mess += "กรุณาเลือก Vendor !!!<br/>";
            return mess;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
}