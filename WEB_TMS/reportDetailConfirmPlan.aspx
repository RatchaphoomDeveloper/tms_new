﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MP.master" AutoEventWireup="true"
    CodeFile="reportDetailConfirmPlan.aspx.cs" Inherits="reportDetailConfirmPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" Runat="Server">
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphBeforeForm" runat="Server">
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_Main" runat="Server">
<script type="text/javascript" src="Javascript/DevExpress/DevExpress.js" > </script>
    <table>
        <tr>
            <td style="background-color: #D3D3D3">
                ช่วงเวลาที่ต้องการค้นหา : </td>
            <td colspan="2">
                <div style="float: left">
                    <dx:ASPxDateEdit ID="adeStart" runat="server" ClientInstanceName="dteStart" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ระหว่างวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </div>
                <div style="float: left">
                    <dx:ASPxDateEdit ID="adeEnd" runat="server" ClientInstanceName="dteEnd" CssClass="dxeLineBreakFix"
                        SkinID="xdte" NullText="ถึงวันที่">
                        <ValidationSettings Display="Dynamic" ValidationGroup="search" ErrorDisplayMode="ImageWithTooltip"
                            RequiredField-IsRequired="true" SetFocusOnError="true">
                            <RequiredField IsRequired="true" ErrorText="ระบุ" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </div>
            </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                คลังต้นทาง</td>
            <td>
                <dx:ASPxComboBox ID="cbxTerminal" runat="server" CallbackPageSize="10" ClientInstanceName="cbxTerminal"
                    EnableCallbackMode="True" OnItemRequestedByValue="TP01RouteOnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="TP01RouteOnItemsRequestedByFilterConditionSQL" nulltext="-- ทั้งหมด --"
                    SkinID="xcbbATC" TextFormatString="{0} {1}" ValueField="STERMINALID" Width="150px">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อคลังต้นทาง" FieldName="STERMINALNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสคลังต้นทาง" FieldName="STERMINALID" Width="80px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsOrganiz" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                ผู้ขนส่ง</td>
            <td>
                <dx:ASPxComboBox ID="cbxVendor" runat="server" CallbackPageSize="30" ClientInstanceName="cbxVendor"
                    EnableCallbackMode="True" OnItemRequestedByValue="cboVendor_OnItemRequestedByValueSQL"
                    OnItemsRequestedByFilterCondition="cboVendor_OnItemsRequestedByFilterConditionSQL" nulltext="-- ทั้งหมด --"
                    SkinID="xcbbATC" TextFormatString="{0}" ValueField="SVENDORID" Width="180px">
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/>
                    <Columns>
                        <dx:ListBoxColumn Caption="ชื่อผู้ขนส่ง" FieldName="SVENDORNAME" Width="100px" />
                        <dx:ListBoxColumn Caption="รหัสบริษัท" FieldName="SVENDORID" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sdsVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp; </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                สัญญา </td>
            <td>
                <dx:ASPxComboBox ID="cboSCONTRACTNO" runat="server" CallbackPageSize="30" ClientInstanceName="cbxVendor"
                    EnableCallbackMode="True" SkinID="xcbbATC" TextFormatString="{0}" ValueField="SCONTRACTNO" nulltext="-- ทั้งหมด --"
                    Width="180px" OnItemRequestedByValue="cboSCONTRACTNO_ItemRequestedByValue" OnItemsRequestedByFilterCondition="cboSCONTRACTNO_ItemsRequestedByFilterCondition">
                    
                    <ClientSideEvents Init="OnInit" LostFocus="OnLostFocus" GotFocus="OnGotFocus"/><Columns>
                        <dx:ListBoxColumn Caption="เลขที่สัญญา" FieldName="SCONTRACTNO" Width="100px" />
                    </Columns>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="dscboSCONTRACTNO" runat="server" ConnectionString="<%$ ConnectionStrings:ORA10GTMSConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ORA10GTMSConnectionString.ProviderName %>">
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp; </td>
        </tr>
        <tr>
            <td style="background-color: #D3D3D3">
                Export File </td>
            <td>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="btnSubmit_Click" Text="Pdf"
                        Width="100" Style="height: 25px" ValidationGroup="search">
                    </dx:ASPxButton>
                </div>
                <div style="float: left">
                    &nbsp;</div>
                <div style="float: left">
                    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Excel" Width="100" OnClick="ASPxButton2_Click"
                        ValidationGroup="search">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<%--<asp:Content ID="Content4" ContentPlaceHolderID="cphAfterForm" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphAfterBody" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="cphAfterHtml" runat="Server">
</asp:Content>--%>
