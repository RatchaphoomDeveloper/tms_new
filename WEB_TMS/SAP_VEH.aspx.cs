﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_D_UpdateTU_SYNC_OUT_SI;
using TMS_D_UpdateVehicle_SYNC_OUT_SI;
using System.Data.Common;
using DevExpress.Web.ASPxEditors;
using System.Web.Configuration;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.OracleClient;
using TMS_D_CreateVehicle_SYNC_OUT_SI;
using TMS_D_CreateTU_SYNC_OUT_SI;
using System.Web.Mail;

public partial class SAP_VEH : System.Web.UI.Page
{
    string sql = WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString;
    List<TU_Assignment> TU_Assignment_Items;
    [Serializable]
    struct TU_Assignment
    {
        public string VEHICLE { get; set; }
        public string TU_NUMBER { get; set; }
        public string SEQ_NMBR { get; set; }
        public string VEH_STATUS { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }

    }
    void QueryProcessing(SqlDataSourceCommandEventArgs e)
    {
        DbCommand command = e.Command;
        DbConnection cx = command.Connection;
        cx.Open();
        DbTransaction tx = cx.BeginTransaction();
        command.Transaction = tx;
    }
    void QueryProcessed(SqlDataSourceStatusEventArgs e)
    {
        DbCommand command = e.Command;
        DbTransaction tx = command.Transaction;

        bool OtherProcessSucceeded = true;

        if (OtherProcessSucceeded)
        {
            tx.Commit();
        }
        else
        {
            tx.Rollback();
        }
    }

    protected void cboHeadRegist_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();


    }
    protected void cboHeadRegist_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

    }
    protected void cboHeadRegist0_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck.SelectParameters.Clear();
        sdsTruck.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsTruck.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsTruck.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsTruck;
        comboBox.DataBind();


    }
    protected void cboHeadRegist0_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck0.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsTruck0.SelectParameters.Clear();
        sdsTruck0.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Value));
        sdsTruck0.SelectParameters.Add("startIndex", TypeCode.Int64, (0 + 1).ToString());
        sdsTruck0.SelectParameters.Add("endIndex", TypeCode.Int64, (9 + 1).ToString());

        comboBox.DataSource = sdsTruck0;
        comboBox.DataBind();
    }
    protected void cboHeadRegist0_Init(object sender, EventArgs e)
    {
        sdsTruck0.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsTruck0.SelectParameters.Clear();
        sdsTruck0.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", ""));
        sdsTruck0.SelectParameters.Add("startIndex", TypeCode.Int64, (0 + 1).ToString());
        sdsTruck0.SelectParameters.Add("endIndex", TypeCode.Int64, (9 + 1).ToString());

        cboHeadRegist0.DataSource = sdsTruck0;
        cboHeadRegist0.DataBind();

        ASPxComboBox cmb = sender as ASPxComboBox;
        SqlDataSource sqlDs = sdsTruck0 as SqlDataSource;
        if (sqlDs == null) return;

        DataView dv = (DataView)sdsTruck0.Select(DataSourceSelectArguments.Empty);

        foreach (DataColumn dc in dv.Table.Columns)
        {
            cmb.Columns.Add(dc.ColumnName, SplitPascalCaseString(dc.ColumnName));
        }
    }
    private String SplitPascalCaseString(String value)
    {
        value = Regex.Replace(value, "(\\p{Ll})(\\p{Lu})", "$1 $2");
        value = Regex.Replace(value, "(\\p{Lu}{2})(\\p{Lu}\\p{Ll}{2})", "$1 $2");
        return value;
    }
    protected void UDT_VEH(string veh, string vehicleStatus)
    {
        using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port"))
        {
            //var dClient = new UpdateVehicle_SYNC_OUT_SIClient("HTTP_Port");
            dClient.ClientCredentials.UserName.UserName = ConfigurationSettings.AppSettings["SAP_SYNCOUT_USR"] + "";
            dClient.ClientCredentials.UserName.Password = ConfigurationSettings.AppSettings["SAP_SYNCOUT_PWD"] + "";
            dClient.Open();
            var tuAssignment = new UpdateVehicle_Src_Req_DTItem[TU_Assignment_Items.Count];

            var tud = TU_Assignment_Items.Select(s => new UpdateVehicle_Src_Req_DTItem { VEHICLE = veh, TU_NUMBER = s.TU_NUMBER, SEQ_NMBR = s.SEQ_NMBR }).ToArray();
            tuAssignment = tud;
            //for (int SEQ_No = 0; SEQ_No < TU_Assignment_Items.Count; SEQ_No++)
            //{
            //    var tu = new UpdateVehicle_Src_Req_DTItem()
            //    {
            //        VEHICLE = veh,
            //        TU_NUMBER = TU_Assignment_Items[SEQ_No].TU_NUMBER + "",
            //        SEQ_NMBR = TU_Assignment_Items[SEQ_No].SEQ_NMBR + ""
            //    };
            //    tuAssignment[SEQ_No] = tu;
            //}

            var vehicle = new UpdateVehicle_Src_Req_DT()
            {
                VEHICLE = veh,
                CARRIER = txtCARRIER.Text.Trim().Equals("") ? "#!" : txtCARRIER.Text,
                CLASS_GRP = txtCLASS_GRP.Text.Trim().Equals("") ? "#!" : txtCLASS_GRP.Text,
                DEPOT = txtDEPOT.Text.Trim().Equals("") ? "#!" : txtDEPOT.Text,
                ROUTE = txtROUTE.Text.Trim().Equals("") ? "#!" : txtROUTE.Text,
                TPPOINT = txtTPPOINT.Text.Trim().Equals("") ? "#!" : txtTPPOINT.Text,
                VEH_STATUS = vehicleStatus,
                VEH_TEXT = txtVEH_TEXT.Text.Trim().Equals("") ? "#!" : txtVEH_TEXT.Text,
                VHCLSIGN = txtVHCLSIGN.Text.Trim().Equals("") ? "#!" : txtVHCLSIGN.Text,
                TU_ITEM = tuAssignment
            };

            var x = new UpdateVehicle_Src_Req_DTItem();

            var response = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);
            //dClient.Close();
        }
    }
    protected void btnUpdateVehicle_Click(object sender, EventArgs e)
    {
        string[] Data_Value = ((cboHeadRegist.Value.ToString().Length > 0) ? cboHeadRegist.Value.ToString() : (":")).Split(':');
        if (Data_Value.Length > 0)
        {
            if (Data_Value[1] + "" != "" || Data_Value[2] + "" != "")
            {
                //var Data_TU_Items = new List<TU_Assignment>();  
                TU_Assignment_Items = new List<TU_Assignment>();
                TU_Assignment_Items.Add(new TU_Assignment() { VEHICLE = Data_Value[1] + "", TU_NUMBER = Data_Value[2] + "", SEQ_NMBR = "1", VEH_STATUS = Data_Value[0] + "" });

                SAP_SYNCOUT_SI SAP_VEH_SI = new SAP_SYNCOUT_SI();
                SAP_VEH_SI.VEH_NO = "" + Data_Value[1] + "";
                Response.Write(SAP_VEH_SI.UDT_Vehicle_SYNC_OUT_SI());

                //UDT_VEH(Data_Value[1] + "", Data_Value[0] + "");
            }
        }
    }
    protected void btnUpdateVehicleByClass_Click(object sender, EventArgs e)
    {

        string[] Data_Value = ((cboHeadRegist.Value.ToString().Length > 0) ? cboHeadRegist.Value.ToString() : (":")).Split(':');
        if (Data_Value.Length > 0)
        {
            if (Data_Value[1] + "" != "" || Data_Value[2] + "" != "")
            {
                SAP_SYNCOUT_SI veh_syncout = new SAP_SYNCOUT_SI();
                veh_syncout.VEH_NO = Data_Value[1] + "";
                veh_syncout.UDT_Vehicle_SYNC_OUT_SI();
            }
        }
    }
    protected void btnUpdateTUByClass_Click(object sender, EventArgs e)
    {
        using (var dClient = new UpdateVehicle_SYNC_OUT_SIClient())
        {
            dClient.ClientCredentials.UserName.UserName = "S_IF_TMS";
            dClient.ClientCredentials.UserName.Password = "SmtPas99";
            string veh = "ตต.12-9999";
            var tuAssignment = new UpdateVehicle_Src_Req_DTItem[1];
            var tu = new UpdateVehicle_Src_Req_DTItem()
            {
                VEHICLE = veh,
                TU_NUMBER = "จอ.12-9999",
                SEQ_NMBR = "1"
            };
            tuAssignment[0] = tu;

            var vehicle = new UpdateVehicle_Src_Req_DT()
            {
                VEHICLE = veh,
                CARRIER = "#!",
                CLASS_GRP = "#!",
                DEPOT = "#!",
                ROUTE = "#!",
                TPPOINT = "#!",
                VEH_STATUS = " ",
                VEH_TEXT = "#!",
                VHCLSIGN = "#!",
                TU_ITEM = tuAssignment

            };

            var x = new UpdateVehicle_Src_Req_DTItem();

            var response = dClient.UpdateVehicle_SYNC_OUT_SI(vehicle);
        }
    }

    protected void cbbVehicle_Create_Init(object sender, EventArgs e)
    {

        sdsVehicle_Create.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex";

        sdsVehicle_Create.SelectParameters.Clear();
        sdsVehicle_Create.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", ""));
        sdsVehicle_Create.SelectParameters.Add("startIndex", TypeCode.Int64, (0 + 1).ToString());
        sdsVehicle_Create.SelectParameters.Add("endIndex", TypeCode.Int64, (9 + 1).ToString());

        cbbVehicle_Create.DataSource = sdsVehicle_Create;
        cbbVehicle_Create.DataBind();

        ASPxComboBox cmb = sender as ASPxComboBox;
        SqlDataSource sqlDs = sdsVehicle_Create as SqlDataSource;
        if (sqlDs == null) return;

        DataView dv = (DataView)sdsVehicle_Create.Select(DataSourceSelectArguments.Empty);

        foreach (DataColumn dc in dv.Table.Columns)
        {
            cmb.Columns.Add(dc.ColumnName, SplitPascalCaseString(dc.ColumnName));
        }
    }
    protected void cbbVehicle_Create_OnItemRequestedByValueSQL(object source, ListEditItemRequestedByValueEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck0.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVehicle_Create.SelectParameters.Clear();
        sdsVehicle_Create.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Value));
        sdsVehicle_Create.SelectParameters.Add("startIndex", TypeCode.Int64, (0 + 1).ToString());
        sdsVehicle_Create.SelectParameters.Add("endIndex", TypeCode.Int64, (9 + 1).ToString());

        comboBox.DataSource = sdsVehicle_Create;
        comboBox.DataBind();
    }
    protected void cbbVehicle_Create_OnItemsRequestedByFilterConditionSQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {

        ASPxComboBox comboBox = (ASPxComboBox)source;
        sdsTruck.SelectCommand = @"SELECT SHEADREGISTERNO , STRAILERREGISTERNO ,STRUCKID ,CASE WHEN NVL(CACTIVE ,'Y')='Y' THEN '1' ELSE '0' END||':'||SHEADREGISTERNO||':'||STRAILERREGISTERNO IsAvailable
FROM (
    SELECT ROW_NUMBER()OVER(ORDER BY TRCK.SHEADREGISTERNO) AS RN , TRCK.SHEADREGISTERNO ,TRCK.STRAILERREGISTERNO
    ,TRCK.STRUCKID ,TRCK.CACTIVE
    FROM TTRUCK TRCK
    WHERE TRCK.SCARTYPEID IN('0','3') AND TRCK.SHEADREGISTERNO LIKE :fillter  
) TBL 
WHERE RN BETWEEN :startIndex AND :endIndex ";

        sdsVehicle_Create.SelectParameters.Clear();
        sdsVehicle_Create.SelectParameters.Add("fillter", TypeCode.String, String.Format("%{0}%", e.Filter));
        sdsVehicle_Create.SelectParameters.Add("startIndex", TypeCode.Int64, (e.BeginIndex + 1).ToString());
        sdsVehicle_Create.SelectParameters.Add("endIndex", TypeCode.Int64, (e.EndIndex + 1).ToString());

        comboBox.DataSource = sdsVehicle_Create;
        comboBox.DataBind();

    }
    protected void btnCreateVehicle_Click(object sender, EventArgs e)
    {
        using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        {
            string OP_Result = "";
            SAP_CREATE_VEH req_veh = new SAP_CREATE_VEH();
            req_veh.sVehicle = cbbVehicle_Create.Text;
            //            DataTable dt = new DataTable();
            //            new OracleDataAdapter(@"SELECT TRCK.STRUCKID ,TRCK.SHEADREGISTERNO ,TRCK.SCARTYPEID  ,NVL(TU.NSLOT,TRCK.NSLOT) NSLOT,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  1  ELSE 2 END ) TU_Assignment_Items
            //,TU.STRUCKID STRAILERID ,TU.SHEADREGISTERNO STRAILERREGISTERNO ,TRCK.SCHASIS VEH_ID  ,TU.SCHASIS TU_ID,TRCK.STRANSPORTID CARRIER
            //,TRCK.VEH_TEXT , TU.VEH_TEXT TU_TEXT
            //,NVL(TU.STERMINALID ,TRCK.STERMINALID ) DEPOT ,NVL(TU.NTOTALCAPACITY ,TRCK.NTOTALCAPACITY ) NTOTALCAPACITY ,NVL(TU.NWEIGHT ,TRCK.NWEIGHT ) NWEIGHT 
            //,NVL(TU.NCALC_WEIGHT ,TRCK.NCALC_WEIGHT ) NCALC_WEIGHT ,NVL(TU.NLOAD_WEIGHT ,TRCK.NLOAD_WEIGHT ) NLOAD_WEIGHT 
            //, NVL(TU.DWATEREXPIRE ,TRCK.DWATEREXPIRE ) DWATEREXPIRE ,NVL(TU.FUELTYPE ,TRCK.FUELTYPE ) FUELTYPE
            //,(CASE WHEN NVL(TRCK.CACTIVE,'Y')='Y' THEN '1' ELSE '0' END) VEH_STATUS
            //,TRCKCOMP.NCOMPARTNO,TRCKCOMP.NCAPACITY
            //,(CASE WHEN TRCK.SCARTYPEID ='0' THEN  TRCK.TRUCK_CATEGORY  ELSE TU.TRUCK_CATEGORY END ) TRUCK_CATEGORY 
            //,TRCK.TRUCK_CATEGORY CLASS_GRP
            //,TU.TRUCK_CATEGORY TU_CLASS_GRP ,'' TPPOINT , TRCK.VEH_TEXT ,TU.VEH_TEXT TU_TEXT
            //,NVL(TU.VOL_UOM, TRCK.VOL_UOM) VOL_UOM 
            //,NVL(TU.SLAST_REQ_ID ,TRCK.SLAST_REQ_ID) SLAST_REQ_ID 
            //,NVL(TU.SLAST_REQ_ID,TRCK.DPREV_SERV) DPREV_SERV  
            //AND TRCK.SHEADREGISTERNO ='" + req_veh.sVehicle + "'", connection).Fill(dt);
            //if (dt.Rows.Count >= 1)
            //{
            req_veh.sLanguage = "TH";
            req_veh.sVeh_Text = txtVEH_TEXT_Create.Text;
            req_veh.sVeh_Type = txtVEH_TYPE_Create.Text;
            req_veh.sRoute = txtROUTE_Create.Text;
            req_veh.sDepot = txtDEPOT_create.Text;
            req_veh.sTPPoint = txtTPPOINT_create.Text;
            req_veh.sVhclSign = txtVHCLSIGN_Create.Text;
            req_veh.sClass_Grp = txtCLASS_GRP_Create.Text;
            req_veh.sCarrier = txtCARRIER_Create.Text;
            req_veh.sVeh_Status = txtVEH_STATUS_Create.Text;
            req_veh.sVeh_ID = txtVEH_ID_Create.Text;
            req_veh.sReg_Date = txtREG_DATE_Create.Text;
            OP_Result = req_veh.CRT_Vehicle_SYNC_OUT_SI();

            memNote.Text = "" + OP_Result;
            //}
        }//Connection
    }
    protected void btnCreateVehicle1_Click1(object sender, EventArgs e)
    {
        var veh = "งง. 5555";
        using (var dClient = new CreateVehicle_SYNC_OUT_SIClient("HTTP_Port1"))
        {
            dClient.ClientCredentials.UserName.UserName = "S_IF_TMS";
            dClient.ClientCredentials.UserName.Password = "SmtPas99";

            var tuAssignment = new CreateVehicle_Src_Req_DTItem[1];
            var tu = new CreateVehicle_Src_Req_DTItem()
            {
                VEHICLE = veh,
                TU_NUMBER = "จอ.12-9999",
                SEQ_NMBR = "1"
            };
            tuAssignment[0] = tu;

            var vehicle = new CreateVehicle_Src_Req_DT()
            {
                VEHICLE = veh,
                VEH_TYPE = "A110",
                CARRIER = "PE00545306",
                CLASS_GRP = "O120",
                DEPOT = "H102",
                ROUTE = "ZTD001",
                TPPOINT = "H102",
                VEH_STATUS = " ",
                VEH_TEXT = "ทดสอบครับ",
                VEH_ID = "123",
                REG_DATE = "2014-11-11",
                //TU_ITEM = tuAssignment
            };

            var response = dClient.CreateVehicle_SYNC_OUT_SI(vehicle);
            memNote.Text = "" + response.GT_RETURN[0].MSGTYP + ": " + response.GT_RETURN[0].MESSAGE;
            //response.GT_RETURN[0].MSGTYP
        }
    }

    protected void btnCreateTUFix_Click(object sender, EventArgs e)
    {
        {
            var tu = "3กฉ. 6300";
            using (var dClient = new CreateTU_SYNC_OUT_SIClient("HTTP_Port2"))
            {
                dClient.ClientCredentials.UserName.UserName = "S_IF_TMS";
                dClient.ClientCredentials.UserName.Password = "SmtPas99";

                var cmp = new CreateTU_Src_Req_DTItem[3];
                cmp[0] = new CreateTU_Src_Req_DTItem()
                {
                    TU_NUMBER = tu,
                    SEQ_NMBR = "1",
                    LOAD_SEQ = "2",
                    COM_NUMBER = "1",
                    CMP_MAXVOL = "3000"
                };
                cmp[1] = new CreateTU_Src_Req_DTItem()
                {
                    TU_NUMBER = tu,
                    SEQ_NMBR = "2",
                    LOAD_SEQ = "1",
                    COM_NUMBER = "2",
                    CMP_MAXVOL = "4000"
                };
                cmp[2] = new CreateTU_Src_Req_DTItem()
                {
                    TU_NUMBER = tu,
                    SEQ_NMBR = "3",
                    LOAD_SEQ = "3",
                    COM_NUMBER = "3",
                    CMP_MAXVOL = "5000"
                };

                var update_tu = new CreateTU_Src_Req_DT()
                {
                    TU_NUMBER = tu,
                    LANGUAGE = "T",
                    TU_TYPE = "A110",
                    WGT_UOM = "KG",
                    VOL_UOM = "L30",
                    TU_UNLWGT = "#!",
                    TU_TEXT = "Testtttt",
                    TU_STATUS = " ",
                    TU_MAXWGT = "#!",
                    L_NUMBER = "55555",
                    ZSTART_DAT = "2013-08-19",
                    EXPIRE_DAT = "2014-08-19",
                    TU_ID = "TESTXXX",
                    CMP_ITEM = cmp
                };

                var response = dClient.CreateTU_SYNC_OUT_SI(update_tu);
                memNote.Text = "" + response.GT_RETURN_TU[0].MSGTYP + ": " + response.GT_RETURN_TU[0].MESSAGE;

            }
        }
    }
    protected void btnTestTruckOnHold_Click(object sender, EventArgs e)
    {
        //if (txtVENNO_OnHold.Text != "")
        //{
        //    using (OracleConnection connection = new OracleConnection(WebConfigurationManager.ConnectionStrings["ORA10GTMSConnectionString"].ConnectionString))
        //    {

        //        AlertTruckOnHold trckOnHold = new AlertTruckOnHold(Page, connection);
        //        trckOnHold.VehNo = txtVENNO_OnHold.Text;
        //        trckOnHold.TUNo = txtTUNO_OnHold.Text;
        //        string result = trckOnHold.SendTo();
        //    }
        //}
    }
    protected void btnTestmail_Click(object sender, EventArgs e)
    {
        MailMessage oMsg = new MailMessage();

        // TODO: Replace with sender e-mail address. 
        oMsg.From = "" + ConfigurationSettings.AppSettings["SystemMail"];

        // TODO: Replace with recipient e-mail address.
        oMsg.To = txtMailTo.Text;
        oMsg.Subject = "Test Mail Form:" + ConfigurationSettings.AppSettings["smtpmail"] + " ";

        // SEND IN HTML FORMAT (comment this line to send plain text).
        oMsg.BodyFormat = MailFormat.Html;

        // HTML Body (remove HTML tags for plain text).
        oMsg.Body = "<HTML><BODY>test</BODY></HTML>";

        SmtpMail.SmtpServer = ConfigurationManager.AppSettings["smtpmail"];
        SmtpMail.Send(oMsg);
    }
    protected void btnCreateDriver_Click(object sender, EventArgs e)
    {
        //SAP_Create_Driver req_dvr = new SAP_Create_Driver();
        //req_dvr.CARRIER = txtDrvCARRIER.Text;
        ////req_dvr.DRIVER_CODE = txt;
        //req_dvr.DRV_STATUS = " ";
        //req_dvr.FIRST_NAME = txtDrvFIRST_NAME.Text;
        //req_dvr.LAST_NAME = txtDrvLAST_NAME.Text;
        //req_dvr.LICENSE_NO = txtDrvLICENSE_NO.Text;
        //req_dvr.LICENSENO_TO = txtDrvLICENSENOE_TO.Text;
        //req_dvr.LICENSENOE_FROM = txtDrvLICENSENOE_FROM.Text;
        //req_dvr.PERS_CODE = txtDrvPERS_CODE.Text;
        //req_dvr.Phone_1 = txtDrvPhone_1.Text;
        //req_dvr.Phone_2 = txtDrvPhone_2.Text;
        //string OP_Result = req_dvr.CRT_Driver_SYNC_OUT_SI();
        ////>DRIVERCODE:
        //txtDriver_output.Text = "" + OP_Result;
    }
    protected void btnUpdateDriver_Click(object sender, EventArgs e)
    {/*4700021913	ที	เจตนา	1/18/2013 2:57:25 PM	SP_TMS	4/29/2014 1:00:02 AM	SP_TMS	3301400871168	PHTDPRT	6นม.00048/54	1*/
        //SAP_Create_Driver req_dvr = new SAP_Create_Driver();
        //string[] DRV_Data = req_dvr.HasDriver(txtDrvLICENSE_NO.Text, txtDrvPERS_CODE.Text);
        //if (DRV_Data[0] == "Y" && DRV_Data[1] != "")
        //{
        //    req_dvr.DRIVER_CODE = DRV_Data[1];
        //    req_dvr.CARRIER = txtDrvCARRIER.Text;
        //    req_dvr.DRV_STATUS = " ";
        //    req_dvr.FIRST_NAME = txtDrvFIRST_NAME.Text;
        //    req_dvr.LAST_NAME = txtDrvLAST_NAME.Text;
        //    req_dvr.LICENSE_NO = txtDrvLICENSE_NO.Text;
        //    req_dvr.LICENSENO_TO = txtDrvLICENSENOE_TO.Text;
        //    req_dvr.LICENSENOE_FROM = txtDrvLICENSENOE_FROM.Text;
        //    req_dvr.PERS_CODE = txtDrvPERS_CODE.Text;
        //    req_dvr.Phone_1 = txtDrvPhone_1.Text;
        //    req_dvr.Phone_2 = txtDrvPhone_2.Text;
        //    string OP_Result = req_dvr.UDP_Driver_SYNC_OUT_SI();

        //    txtDriver_output.Text = "" + OP_Result;
        //}
    }
}