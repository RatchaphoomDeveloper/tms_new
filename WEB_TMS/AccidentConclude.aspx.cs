﻿using DevExpress.Web.ASPxGridView;
using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TMS_BLL.Master;
using TMS_BLL.Transaction.Accident;

public partial class AccidentConclude : PageBase
{
    DataTable dt;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDrowDownList();
            if (Session["CGROUP"] + string.Empty == "0")
            {
                ddlVendor.SelectedValue = Session["SVDID"] + string.Empty;
                ddlVendor.Enabled = false;
                ddlVendor_SelectedIndexChanged(null, null);
                //mode = "view";
            }
            this.AssignAuthen();
        }
    }
    #endregion

    private void AssignAuthen()
    {
        try
        {
            if (!CanRead)
            {
                btnSearch.Enabled = false;
                btnExport.Enabled = false;
                btnSearch.CssClass = "btn btn-md bth-hover btn-info";
                btnExport.CssClass = "btn btn-md bth-hover btn-info";
            }
            if (!CanWrite)
            {
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #region DrowDownList
    private void SetDrowDownList()
    {

        ViewState["DataVendor"] = VendorBLL.Instance.TVendorSapSelect();
        ddlVendor.DataTextField = "SABBREVIATION";
        ddlVendor.DataValueField = "SVENDORID";
        ddlVendor.DataSource = ViewState["DataVendor"];
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.DataTextField = "NAME";
        ddlWorkGroup.DataValueField = "ID";
        ddlWorkGroup.DataSource = ContractTiedBLL.Instance.WorkGroupSelect();
        ddlWorkGroup.DataBind();
        ddlWorkGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlWorkGroup.SelectedIndex = 0;
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlGroup.SelectedIndex = 0;
        ddlContract.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlContract.SelectedIndex = 0;

        ddlYear.DataTextField = "text";
        ddlYear.DataValueField = "value";
        ddlYear.DataSource = SetYear();
        ddlYear.DataBind();

        ddlMonth.DataTextField = "NAME_TH";
        ddlMonth.DataValueField = "MONTH_ID";
        ddlMonth.DataSource = KPI2BLL.Instance.MonthSelect();
        ddlMonth.DataBind();
        ddlMonth.Items.Insert(0, new ListItem()
        {
            Text = "--เลือก--",
            Value = "0"
        });
        ddlMonth.SelectedIndex = 0;
    }
    #region ddlWorkGroup_SelectedIndexChanged
    protected void ddlWorkGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroupsDataBind(ddlWorkGroup.SelectedValue + string.Empty);
    }
    #endregion

    #region ddlGroupsDataBind
    private void ddlGroupsDataBind(string WordGroupID)
    {
        ddlGroup.DataTextField = "NAME";
        ddlGroup.DataValueField = "ID";
        ddlGroup.DataSource = ContractTiedBLL.Instance.GroupSelect(WordGroupID);
        ddlGroup.DataBind();
        ddlGroup.Items.Insert(0, new ListItem()
        {
            Text = "เลือกกลุ่มที่",
            Value = "0"
        });
    }
    #endregion

    #region ddlVendor_SelectedIndexChanged
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlVendor.SelectedIndex > 0)
            {

                dt = AccidentBLL.Instance.ContractSelect(ddlVendor.SelectedValue);
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
            else
            {
                dt = new DataTable();
                dt.Columns.Add("SCONTRACTID", typeof(string));
                dt.Columns.Add("SCONTRACTNO", typeof(string));
                DropDownListHelper.BindDropDownList(ref ddlContract, dt, "SCONTRACTID", "SCONTRACTNO", true);
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion

    #region ddlContract_SelectedIndexChanged
    protected void ddlContract_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlContract.SelectedIndex > 0)
            {
                dt = AccidentBLL.Instance.GroupSelect(ddlContract.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    ddlWorkGroup.SelectedValue = dr["WORKGROUPID"] + string.Empty;
                    ddlWorkGroup_SelectedIndexChanged(null, null);
                    ddlGroup.SelectedValue = dr["GROUPID"] + string.Empty;
                }
            }
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }
    #endregion
    #endregion

    #region SetYear
    private DataTable SetYear()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("value");
            dt.Columns.Add("text");
            DateTime date = new DateTime(2015, 1, 1);
            for (int i = 2015; i <= DateTime.Today.Year; i++)
            {
                date = new DateTime(i, 1, 1);
                dt.Rows.Add(date.Year, date.Year);
            }
            dt.DefaultView.Sort = "value DESC";
            return dt;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion

    #region Btn_Cilck
    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            gvAccident.DataSource = dt;
            gvAccident.DataBind();
            if (!string.IsNullOrEmpty(rblSERIOUS.SelectedValue))
            {
                foreach (GridViewColumn item in gvAccident.AllColumns)
                {
                    item.Visible = true;
                    if ((item.Name == "0" && rblSERIOUS.SelectedValue == "1") || (item.Name == "1" && rblSERIOUS.SelectedValue == "0"))
                    {
                        item.Visible = false;
                    }
                }
            }
            if (ddlMonth.SelectedValue != "0")
            {
                foreach (GridViewColumn item in gvAccident.Columns)
                {
                    item.Visible = false;
                }
                gvAccident.Columns["SABBREVIATION"].Visible = true;
                gvAccident.Columns["SCONTRACTNO"].Visible = true;
                gvAccident.Columns["GROUPNAME"].Visible = true;
                switch (ddlMonth.SelectedValue)
                {
                    case "1":
                        gvAccident.Columns["JAN"].Visible = true;
                        break;
                    case "2":
                        gvAccident.Columns["FEB"].Visible = true;
                        break;
                    case "3":
                        gvAccident.Columns["MAR"].Visible = true;
                        break;
                    case "4":
                        gvAccident.Columns["APR"].Visible = true;
                        break;
                    case "5":
                        gvAccident.Columns["MAY"].Visible = true;
                        break;
                    case "6":
                        gvAccident.Columns["JUN"].Visible = true;
                        break;
                    case "7":
                        gvAccident.Columns["JUL"].Visible = true;
                        break;
                    case "8":
                        gvAccident.Columns["AUG"].Visible = true;
                        break;
                    case "9":
                        gvAccident.Columns["SEP"].Visible = true;
                        break;
                    case "10":
                        gvAccident.Columns["OCT"].Visible = true;
                        break;
                    case "11":
                        gvAccident.Columns["NOV"].Visible = true;
                        break;
                    case "12":
                        gvAccident.Columns["DEC"].Visible = true;
                        break;
                    default:
                        break;
                }
            }
            
            lblItem.Text = dt.Rows.Count + string.Empty;
        }
        catch (Exception ex)
        {
            alertFail(RemoveSpecialCharacters(ex.Message));
        }

    }
    #endregion

    #region GetData
    private void GetData()
    {
        dt = AccidentBLL.Instance.AccidentConcludeSelect(ddlYear.SelectedValue,ddlVendor.SelectedValue,ddlContract.SelectedValue,ddlGroup.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            DataRow dr = dt.NewRow();
            int TOTAL0 = 0, sumTOTAL0 = 0, TOTAL1 = 0, sumTOTAL1 = 0, JAN0 = 0, JAN1 = 0, FEB0 = 0, FEB1 = 0, MAR0 = 0, MAR1 = 0, APR0 = 0, APR1 = 0, MAY0 = 0, MAY1 = 0, JUN0 = 0, JUN1 = 0, JUL0 = 0, JUL1 = 0, AUG0 = 0, AUG1 = 0, SEP0 = 0, SEP1 = 0, OCT0 = 0, OCT1 = 0, NOV0 = 0, NOV1 = 0, DEC0 = 0, DEC1 = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dr = (DataRow)dt.Rows[i];
                JAN0 += (dr["JAN0"] + string.Empty != "-" ? int.Parse(dr["JAN0"] + string.Empty) : 0);
                JAN1 += (dr["JAN1"] + string.Empty != "-" ? int.Parse(dr["JAN1"] + string.Empty) : 0);
                FEB0 += (dr["FEB0"] + string.Empty != "-" ? int.Parse(dr["FEB0"] + string.Empty) : 0);
                FEB1 += (dr["FEB1"] + string.Empty != "-" ? int.Parse(dr["FEB1"] + string.Empty) : 0);
                MAR0 += (dr["MAR0"] + string.Empty != "-" ? int.Parse(dr["MAR0"] + string.Empty) : 0);
                MAR1 += (dr["MAR1"] + string.Empty != "-" ? int.Parse(dr["MAR1"] + string.Empty) : 0);
                APR0 += (dr["APR0"] + string.Empty != "-" ? int.Parse(dr["APR0"] + string.Empty) : 0);
                APR1 += (dr["APR1"] + string.Empty != "-" ? int.Parse(dr["APR1"] + string.Empty) : 0);
                MAY0 += (dr["MAY0"] + string.Empty != "-" ? int.Parse(dr["MAY0"] + string.Empty) : 0);
                MAY1 += (dr["MAY1"] + string.Empty != "-" ? int.Parse(dr["MAY1"] + string.Empty) : 0);
                JUN0 += (dr["JUN0"] + string.Empty != "-" ? int.Parse(dr["JUN0"] + string.Empty) : 0);
                JUN1 += (dr["JUN1"] + string.Empty != "-" ? int.Parse(dr["JUN1"] + string.Empty) : 0);
                JUL0 += (dr["JUL0"] + string.Empty != "-" ? int.Parse(dr["JUL0"] + string.Empty) : 0);
                JUL1 += (dr["JUL1"] + string.Empty != "-" ? int.Parse(dr["JUL1"] + string.Empty) : 0);
                AUG0 += (dr["AUG0"] + string.Empty != "-" ? int.Parse(dr["AUG0"] + string.Empty) : 0);
                AUG1 += (dr["AUG1"] + string.Empty != "-" ? int.Parse(dr["AUG1"] + string.Empty) : 0);
                SEP0 += (dr["SEP0"] + string.Empty != "-" ? int.Parse(dr["SEP0"] + string.Empty) : 0);
                SEP1 += (dr["SEP1"] + string.Empty != "-" ? int.Parse(dr["SEP1"] + string.Empty) : 0);
                OCT0 += (dr["OCT0"] + string.Empty != "-" ? int.Parse(dr["OCT0"] + string.Empty) : 0);
                OCT1 += (dr["OCT1"] + string.Empty != "-" ? int.Parse(dr["OCT1"] + string.Empty) : 0);
                NOV0 += (dr["NOV0"] + string.Empty != "-" ? int.Parse(dr["NOV0"] + string.Empty) : 0);
                NOV1 += (dr["NOV1"] + string.Empty != "-" ? int.Parse(dr["NOV1"] + string.Empty) : 0);
                DEC0 += (dr["DEC0"] + string.Empty != "-" ? int.Parse(dr["DEC0"] + string.Empty) : 0);
                DEC1 += (dr["DEC1"] + string.Empty != "-" ? int.Parse(dr["DEC1"] + string.Empty) : 0);
                TOTAL0 = (dr["JAN0"] + string.Empty != "-" ? int.Parse(dr["JAN0"] + string.Empty) : 0)
                            + (dr["FEB0"] + string.Empty != "-" ? int.Parse(dr["FEB0"] + string.Empty) : 0)
                            + (dr["MAR0"] + string.Empty != "-" ? int.Parse(dr["MAR0"] + string.Empty) : 0)
                            + (dr["APR0"] + string.Empty != "-" ? int.Parse(dr["APR0"] + string.Empty) : 0)
                            + (dr["MAY0"] + string.Empty != "-" ? int.Parse(dr["MAY0"] + string.Empty) : 0)
                            + (dr["JUN0"] + string.Empty != "-" ? int.Parse(dr["JUN0"] + string.Empty) : 0)
                            + (dr["JUL0"] + string.Empty != "-" ? int.Parse(dr["JUL0"] + string.Empty) : 0)
                            + (dr["AUG0"] + string.Empty != "-" ? int.Parse(dr["AUG0"] + string.Empty) : 0)
                            + (dr["SEP0"] + string.Empty != "-" ? int.Parse(dr["SEP0"] + string.Empty) : 0)
                            + (dr["OCT0"] + string.Empty != "-" ? int.Parse(dr["OCT0"] + string.Empty) : 0)
                            + (dr["NOV0"] + string.Empty != "-" ? int.Parse(dr["NOV0"] + string.Empty) : 0)
                            + (dr["DEC0"] + string.Empty != "-" ? int.Parse(dr["DEC0"] + string.Empty) : 0)
                    ;
                TOTAL1 = (dr["JAN1"] + string.Empty != "-" ? int.Parse(dr["JAN1"] + string.Empty) : 0)
                            + (dr["FEB1"] + string.Empty != "-" ? int.Parse(dr["FEB1"] + string.Empty) : 0)
                            + (dr["MAR1"] + string.Empty != "-" ? int.Parse(dr["MAR1"] + string.Empty) : 0)
                            + (dr["APR1"] + string.Empty != "-" ? int.Parse(dr["APR1"] + string.Empty) : 0)
                            + (dr["MAY1"] + string.Empty != "-" ? int.Parse(dr["MAY1"] + string.Empty) : 0)
                            + (dr["JUN1"] + string.Empty != "-" ? int.Parse(dr["JUN1"] + string.Empty) : 0)
                            + (dr["JUL1"] + string.Empty != "-" ? int.Parse(dr["JUL1"] + string.Empty) : 0)
                            + (dr["AUG1"] + string.Empty != "-" ? int.Parse(dr["AUG1"] + string.Empty) : 0)
                            + (dr["SEP1"] + string.Empty != "-" ? int.Parse(dr["SEP1"] + string.Empty) : 0)
                            + (dr["OCT1"] + string.Empty != "-" ? int.Parse(dr["OCT1"] + string.Empty) : 0)
                            + (dr["NOV1"] + string.Empty != "-" ? int.Parse(dr["NOV1"] + string.Empty) : 0)
                            + (dr["DEC1"] + string.Empty != "-" ? int.Parse(dr["DEC1"] + string.Empty) : 0)
                    ;
                dr["TOTAL0"] = TOTAL0;
                sumTOTAL0 += TOTAL0;
                dr["TOTAL1"] = TOTAL1;
                sumTOTAL1 += TOTAL1;
            }
           
            dr = dt.NewRow();
            dr["SABBREVIATION"] = "รวมทั้งสิ้นแยกตามรายเดือน";
            dr["TOTAL0"] = sumTOTAL0;
            dr["TOTAL1"] = sumTOTAL1;
            dr["JAN0"] = JAN0;
            dr["JAN1"] = JAN1;
            dr["FEB0"] = FEB0;
            dr["FEB1"] = FEB1;
            dr["MAR0"] = MAR0;
            dr["MAR1"] = MAR1;
            dr["APR0"] = APR0;
            dr["APR1"] = APR1;
            dr["MAY0"] = MAY0;
            dr["MAY1"] = MAY1;
            dr["JUN0"] = JUN0;
            dr["JUN1"] = JUN1;
            dr["JUL0"] = JUL0;
            dr["JUL1"] = JUL1;
            dr["AUG0"] = AUG0;
            dr["AUG1"] = AUG1;
            dr["SEP0"] = SEP0;
            dr["SEP1"] = SEP1;
            dr["OCT0"] = OCT0;
            dr["OCT1"] = OCT1;
            dr["NOV0"] = NOV0;
            dr["NOV1"] = NOV1;
            dr["DEC0"] = DEC0;
            dr["DEC1"] = DEC1;

            dt.Rows.InsertAt(dr, 0);
        }
    }
    #endregion

    #region btnClear_Click
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWorkGroup.SelectedIndex = 0;
        ddlWorkGroup_SelectedIndexChanged(null, null);

        ddlVendor.SelectedIndex = 0;
        ddlVendor_SelectedIndexChanged(null, null);
        gvAccident.DataSource = null;
        gvAccident.DataBind();
        lblItem.Text = "0";
        rblSERIOUS.ClearSelection();
        ddlMonth.SelectedIndex = 0;
        ddlYear.SelectedIndex = 0;
    }
    #endregion

    #region btnExport_Click
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            GetData();
            //dt.Columns.Remove("SYS_TIME");

            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/Admin/AccidentConcludeFormat.xlsx"));
            ExcelWorksheet worksheet = workbook.Worksheets["Accident"];
            worksheet.InsertDataTable(dt, new InsertDataTableOptions(2, 0) { ColumnHeaders = false });

            string Path = this.CheckPath();
            string FileName = "Accident_Conclude_Report_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }
        catch (Exception ex)
        {
            alertFail(ex.Message);
        }
    }

    private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
    {
        try
        {
            cell.Value = value;
            cell.Style.VerticalAlignment = VerticalAlign;
            cell.Style.HorizontalAlignment = HorizontalAlign;
            cell.Style.WrapText = WrapText;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void DownloadFile(string Path, string FileName)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
        Response.TransmitFile(Path + "\\" + FileName);
        Response.End();
    }

    private string CheckPath()
    {
        try
        {
            string PathExport = Server.MapPath("~") + "\\" + "Export";
            if (!Directory.Exists(PathExport))
                Directory.CreateDirectory(PathExport);

            return PathExport;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    #endregion
    #endregion   

    #region GridView_Event
    ASPxGridView grid;
    Dictionary<GridViewDataColumn, TableCell> mergedCells = new Dictionary<GridViewDataColumn, TableCell>();
    Dictionary<TableCell, int> cellRowSpans = new Dictionary<TableCell, int>();
    protected void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        //add the attribute that will be used to find which column the cell belongs to
        e.Cell.Attributes.Add("ci", e.DataColumn.VisibleIndex.ToString());

        if (cellRowSpans.ContainsKey(e.Cell))
        {
            e.Cell.RowSpan = cellRowSpans[e.Cell];
        }
        if (e.VisibleIndex == 0 || e.DataColumn.FieldName == "TOTAL0" || e.DataColumn.FieldName == "TOTAL1")
        {
                e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
        }
        
    }
    protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType == GridViewRowType.Data)
        {
            
            for (int i = e.Row.Cells.Count - 1; i >= 0; i--)
            {

                DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell dataCell = e.Row.Cells[i] as DevExpress.Web.ASPxGridView.Rendering.GridViewTableDataCell;
                if (dataCell != null && (i <= 1))
                {
                    MergeCells(dataCell.DataColumn, e.VisibleIndex, dataCell);
                }
            }
            //if (e.VisibleIndex == 1)
            //{
                e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#F6E3CE");
            //}
        }

    }

    protected void MergeCells(GridViewDataColumn column, int visibleIndex, TableCell cell)
    {
        bool isNextTheSame = IsNextRowHasSameData(column, visibleIndex);
        if (isNextTheSame)
        {
            if (!mergedCells.ContainsKey(column))
            {
                mergedCells[column] = cell;
            }
        }
        if (IsPrevRowHasSameData(column, visibleIndex))
        {
            ((TableRow)cell.Parent).Cells.Remove(cell);
            if (mergedCells.ContainsKey(column))
            {
                TableCell mergedCell = mergedCells[column];
                if (!cellRowSpans.ContainsKey(mergedCell))
                {
                    cellRowSpans[mergedCell] = 1;
                }
                cellRowSpans[mergedCell] = cellRowSpans[mergedCell] + 1;
            }
        }
        if (!isNextTheSame)
        {
            mergedCells.Remove(column);
        }
    }
    bool IsNextRowHasSameData(GridViewDataColumn column, int visibleIndex)
    {
        //is it the last visible row
        if (visibleIndex >= gvAccident.VisibleRowCount - 1)
            return false;

        return IsSameData(column.FieldName, visibleIndex, visibleIndex + 1);
    }
    bool IsPrevRowHasSameData(GridViewDataColumn column, int visibleIndex)
    {
        ASPxGridView grid = column.Grid;
        //is it the first visible row
        if (visibleIndex <= gvAccident.VisibleStartIndex)
            return false;

        return IsSameData(column.FieldName, visibleIndex, visibleIndex - 1);
    }
    bool IsSameData(string fieldName, int visibleIndex1, int visibleIndex2)
    {
        // is it a group row?
        if (gvAccident.GetRowLevel(visibleIndex2) != gvAccident.GroupCount)
            return false;

        return object.Equals(gvAccident.GetRowValues(visibleIndex1, fieldName), gvAccident.GetRowValues(visibleIndex2, fieldName));
    }
    #endregion
}